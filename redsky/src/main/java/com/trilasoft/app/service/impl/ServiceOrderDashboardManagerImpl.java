/**
 *  @Class Name	 ServiceOrderDashboardManagerImpl
 *  @author      Surya 
 *  @Date        14-Jul-2014
 */
package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CheckListDao;
import com.trilasoft.app.dao.ServiceOrderDashboardDao;
import com.trilasoft.app.model.ServiceOrderDashboard;
import com.trilasoft.app.service.ServiceOrderDashboardManager;



public class ServiceOrderDashboardManagerImpl extends GenericManagerImpl<ServiceOrderDashboard, Long> implements ServiceOrderDashboardManager{
 private ServiceOrderDashboardDao serviceOrderDashboardDao;
 public ServiceOrderDashboardManagerImpl(ServiceOrderDashboardDao serviceOrderDashboardDao) {
		super(serviceOrderDashboardDao);
		this.serviceOrderDashboardDao=serviceOrderDashboardDao;
	} 
 public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String cmpdiv,String billtoName,Boolean actstatus,String bookingAgentShipNumber,String serviceOrderMoveType,String recordLimit,String cityCountryZipOption,String originCityOrZip,String destinationCityOrZip,String originCountry,String destinationCountry,String salesMan,String certificateNumber,String daShipmentNumber,String estimator){
	   return serviceOrderDashboardDao.soDashboardSearchList(shipNumber,registrationNumber,job1, coordinator,status,corpId,role,lastName,firstName,cmpdiv,billtoName, actstatus,bookingAgentShipNumber,serviceOrderMoveType,recordLimit,cityCountryZipOption,originCityOrZip,destinationCityOrZip, originCountry,destinationCountry,salesMan,certificateNumber,daShipmentNumber,estimator);
}
}
