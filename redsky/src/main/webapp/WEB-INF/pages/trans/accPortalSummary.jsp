<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Account Summary Screen</title> 
<meta name="heading" content="Account Summary Screen"/> 

<style>
.table th.sorted {
    background-color: #BCD2EF;
    color: #15428B;
}
</style>

</head> 
<%-- Script Shifted from Top to Botton on 07-Sep-2012 By Kunal --%>
<script type="text/javascript">
function findCustomerOtherSO(position){
 var sid=document.forms['summaryForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['summaryForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }
function goToUrl(id)
{
	location.href = "findSummaryList.html?id="+id;
}
function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['summaryForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['summaryForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['summaryForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['summaryForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'findSummaryList.html?id='+results;
             }
        }
  
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();
</script>
<script language="javascript" >
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	window.onload = function() { 
	//trap();
	//document.forms['claimForm'].elements['addButton'].disabled=true;
}
</sec-auth:authComponent> 
function trap(){
if(document.images){
	  for(i=0;i<document.images.length;i++){
	      if(document.images[i].src.indexOf('nav')>0){
				document.images[i].onclick= right; 
	        	document.images[i].src = 'images/navarrow.gif';  
		  } 	  

	  }
 }
}
</script>
<%-- Shifting Closed Here --%>

<body>

<c:set var="noHeader" value="false" scope="session"/>
<div align="center"></div>
	
<div class="clear"></div>			
		
<table cellspacing="0" cellpadding="0" border="0" style="width:100%" >
<tr><td>
<table align="left"   cellspacing="0" cellpadding="0" border="0" style="margin: 0px; width:100%">
<tr>
<td align="left">		
<s:form id="summaryForm" name="summaryForm" action="findSummaryList" method="post" validate="true" >
<s:hidden name="customerFile.id"/>
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
   <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
   <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
   <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
   <c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
   <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>   
</c:when>
<c:when test="${gotoPageString == 'gototab.summary' }">
   <c:redirect url="/findSummaryList.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
   <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
   <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
   <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>


<s:set name="findSummaryDetailList" value="findSummaryDetailList" scope="request" id="findSummaryDetailList"/>
<div id="Layer1" style="width:100%" >
<div id="Layer3" style="width:100%;">
<div id="newmnav" style="float:left;">
            <ul>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		       <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
            </sec-auth:authComponent>
			<%-- <c:if test="${serviceOrder.job == 'OFF'}">
		      <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
		      <li><a href="operationResourceFromAcPortal.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
		      </sec-auth:authComponent>
            </c:if> --%>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
             </sec-auth:authComponent>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
             <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		     </c:choose> 
		     </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
		     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
		     <c:if test="${usertype!='ACCOUNT'}">
             <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
             </c:if>
             <c:if test="${usertype=='ACCOUNT' && serviceOrder.job !='RLO'}">
             <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
               <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                </c:if>
                </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.Summary.statusTab">
             <c:if test="${serviceOrder.job =='RLO'}"> 
             	 <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
             </c:if>
             <c:if test="${serviceOrder.job !='RLO'}"> 
             	<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>  
			</sec-auth:authComponent> 
			<sec-auth:authComponent componentId="module.tab.summary.summaryTab">
             	<li  id="newmnav1" style="background:#FFF"><a class="current"><span>Summary</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
             <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
             </sec-auth:authComponent>
             <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   			 <c:if test="${serviceOrder.job!='RLO'}">
   			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
      		 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  	         </sec-auth:authComponent>
             </c:if>
             </configByCorp:fieldVisibility>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
             <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
             </sec-auth:authComponent>
             
             <sec-auth:authComponent componentId="module.tab.trackingStatus.reportTab">
             <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Status&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=120, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
             </sec-auth:authComponent>
             <!--<li><a onclick="openWindow('auditList.html?id=${serviceOrder.id}&tableName=trackingstatus,miscellaneous&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>-->
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
           	 <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=trackingstatus,miscellaneous&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
           	  </sec-auth:authComponent>
           	  
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
           </ul>
           </div>
          <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;" border="0"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" style="vertical-align:top;!padding-top:1px;">
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
		
		</c:if></tr></table>
			<c:if test="${not empty serviceOrder.id }">
			<div class="spn">&nbsp;</div>					
			</c:if>
     </div> 
	<div class="spn">&nbsp;</div>
<div id="liquid-round-top" style="width:100%;!margin-top:5px;">
<div class="top"><span></span></div>
<div class="center-content">
<display:table name="findSummaryDetailList" class="table" requestURI="" id="findSummaryDetailList" style="width:100%;" defaultsort="1" pagesize="50" >
     <display:column sortable="true" title="Order&nbsp;Number" style="width:2%">
     <c:out value="${findSummaryDetailList.shipNumber}" />
     </display:column>
     <display:column property="lastName" sortable="true" title="Shipper&nbsp;Name" style="width:20%"/> 
     <display:column property="mode" sortable="true" title="Via" style="width:2%"/> 
   	 <display:column property="originCity" sortable="true" title="From"  style="width:15%"/>
	 <display:column property="destinationCity" sortable="true" title="To"  style="width:17%"/>
	 <c:if test="${serviceOrder.job!='RLO'}">
	 <display:column property="commodityName" sortable="true" title="Commodity" maxLength="20" style="width:17%"/>
	 </c:if>
	 <display:column property="accountName" sortable="true" title="Account" style="width:10%"/>
<c:if test="${findSummaryDetailList!='[]'}" >
	<sec-auth:authComponent componentId="module.tab.Summary.hideOnlyForVOER">
	<c:if test="${serviceOrder.job!='RLO'}">
	<c:choose>
      <c:when test='${findSummaryDetailList.unit1 == "Lbs"}'>
	   <display:column   sortable="true"  title="Est.Weight" sortProperty="estimateGrossWeight" style="width:5%">	        
           <c:out value="${findSummaryDetailList.estimateGrossWeight}" />
       </display:column>
	<display:column   sortable="true"  title="Act.Weight" sortProperty="actualNetWeight" style="width:5%">
	        
                 <c:out value="${findSummaryDetailList.actualNetWeight}"/>
        </display:column>
        </c:when>
        <c:otherwise>
        <display:column   sortable="true"  title="Est.Weight" sortProperty="estimateGrossWeightKilo" style="width:5%">
	        
                  <c:out value="${findSummaryDetailList.estimateGrossWeightKilo}" />
        </display:column>
	<display:column   sortable="true"  title="Act.Weight" sortProperty="actualNetWeightKilo" style="width:5%">
	        
                 <c:out value="${findSummaryDetailList.actualNetWeightKilo}"/>
        </display:column>
        </c:otherwise>
</c:choose>
<c:choose>
      <c:when test='${findSummaryDetailList.unit2 == "Cft"}'>
	   <display:column   sortable="true"  title="Est.&nbsp;Volume" sortProperty="estimateCubicFeet" style="width:5%">	        
           <c:out value="${findSummaryDetailList.estimateCubicFeet}" />
       </display:column>
		<display:column   sortable="true"  title="Act.&nbsp;Volume" sortProperty="netActualCubicFeet" style="width:5%">	        
            <c:out value="${findSummaryDetailList.netActualCubicFeet}"/>
        </display:column>
        </c:when>
        <c:otherwise>
        <display:column   sortable="true"  title="Est.&nbsp;Volume" sortProperty="estimateCubicMtr" style="width:5%">	        
             <c:out value="${findSummaryDetailList.estimateCubicMtr}" />
        </display:column>
		<display:column   sortable="true"  title="Act.&nbsp;Volume" sortProperty="netActualCubicMtr" style="width:5%">	        
             <c:out value="${findSummaryDetailList.netActualCubicMtr}"/>
        </display:column>
        </c:otherwise>
</c:choose>
</c:if>
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.tab.Summary.showOnlyForVOER">
<c:if test="${serviceOrder.job!='RLO'}">
<c:choose>
       <c:when test='${findSummaryDetailList.unit2 == "Cft"}'>
	   <display:column   sortable="true"  title="Charg.&nbsp;Volume" sortProperty="chargeableNetCubicFeet" style="width:5%">	        
           <c:out value="${findSummaryDetailList.chargeableNetCubicFeet}" />
       </display:column>
        </c:when>
        <c:otherwise>
        <display:column   sortable="true"  title="Charg.&nbsp;Volume" sortProperty="chargeableNetCubicMtr" style="width:5%">	        
             <c:out value="${findSummaryDetailList.chargeableNetCubicMtr}" />
        </display:column>
        </c:otherwise>
</c:choose>
</c:if>
</sec-auth:authComponent>
</c:if>
</display:table>
<div style="!margin-top:2px;"></div>
</div>
<div class="bottom-header" style="!margin-top:50px;margin-top:31px; width:100%"><span></span></div>
</div>
</td></tr>

<tr><td>
<div id="content" align="center" >
<div id="liquid-round-top" style="width:100%">
<div class="top" ><span></span></div>
<div class="center-content">
<table width="100%" cellpadding="1" cellspacing="0" border="0"  align="center" style="margin-bottom: 0px">
<tr><td colspan="4" align="left">
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin:0px;margin-bottom:10px;" >
<tr>
 <td class="headtab_left"> </td>
 <td NOWRAP class="headtab_center">&nbsp;Move Time Line</td>
  <td width="28" valign="top" class="headtab_bg"></td>
  <td class="headtab_bg_center">&nbsp; </td>
  <td class="headtab_right"> </td>
  </tr> 
</table>
</td>
</tr>

<tr><td width="" valign="top"  style="margin-top: 2px">
<c:if test="${serviceOrder.job!='RLO'}">
<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>  
  <li><a><span>Origin Services</span></a></li> 
 </ul> 
</div>

<div class="spn">&nbsp;</div>
<display:table name="findSummaryDetailList" class="table" requestURI="" id="findSummaryDetailList" style="width:100%" defaultsort="1">
     <display:column   title="Milestone" style="width:30%"/> 
   	 <display:column   title="Date"  format="{0,date,dd-MMM-yyyy}"  style="width:15%"/>
	 <display:column   title="Status"  style="width:10%"/>
<c:if test="${findSummaryDetailList!='[]'}">
<display:caption style="margin-top: -11px; !margin-top: -10px;  ">
<tr>
<td>

Survey Date</td>
<td>
${findSummaryDetailList.survey}</td>
<td>
${findSummaryDetailList.surveyAct}</td>
</tr>
<tr>
<td>
Packing Date</td>
<td>
${findSummaryDetailList.beginPacking}</td>
<td>
${findSummaryDetailList.packA}</td>
</tr>

<tr><td>

Loading Date</td>
<td>
${findSummaryDetailList.beginLoad}</td>
<td>
${findSummaryDetailList.loadA}</td>
</tr>
<c:if test="${findSummaryDetailList.sitOriginMsg=='Storage in Transit'}">
<tr><td>
${findSummaryDetailList.sitOriginMsg}</td>
<td>
${findSummaryDetailList.sitOriginYNActDate}</td>
<td>
${findSummaryDetailList.sitOriginYNAct}</td>
</tr>
</c:if>
</display:caption>
</c:if>
</display:table>
</c:if>

<c:if test="${serviceOrder.job=='RLO'}">
<display:table name="findSummaryReloList" class="table" requestURI="" id="findSummaryReloList" style="width:100%;text-align: center" >    
	     <display:column   title="Service Type"  property="serviceType" style="width:30%"/>  
		 <display:column   title="Start Date"  format="{0,date,dd-MMM-yyyy}" property="serviceStartDate" style="width:15%"/>
		 <display:column   title="End Date"  format="{0,date,dd-MMM-yyyy}" property="serviceEndDate" style="width:15%"  /> 
</display:table>
</c:if>

</td>

<td width="1px" ></td>
<c:if test="${serviceOrder.job!='RLO'}">
<td width="" valign="top" style="margin-top: 2px">
<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li><a><span>Destination Services</span></a></li>
 
 </ul> 
</div><div class="spn">&nbsp;</div>

<display:table name="findSummaryDetailList" class="table" requestURI="" id="findSummaryDetailList" style="width:100%;text-align: center" defaultsort="1">
     <display:column   title="Milestone" style="width:30%"/> 
   	 <display:column   title="Date"  format="{0,date,dd-MMM-yyyy}"  style="width:15%"/>
	 <display:column   title="Status"  style="width:10%"/>
 <c:if test="${findSummaryDetailList!='[]'}">
<display:caption style="margin-top: -11px; !margin-top: -10px; " >
<c:choose>
	<c:when test='${findSummaryDetailList.job == "UVL" || findSummaryDetailList.job == "MVL" || findSummaryDetailList.job == "ULL" || findSummaryDetailList.job == "MLL" || findSummaryDetailList.job == "DOM" || findSummaryDetailList.job == "LOC"}'>
	<tr>
	<td>
	ETA</td>
	<td>
	${findSummaryDetailList.ETA}</td>
	<td>
	${findSummaryDetailList.ETADATE}</td>
	</tr>
	<tr>
	<td>
	${findSummaryDetailList.lebel1}</td>
	<td>
	${findSummaryDetailList.deliveryADate}</td>
	<td>
	${findSummaryDetailList.deliveryActual}</td>
	</tr>
	</c:when>
				
	<c:otherwise> 
	    <c:if test="${findSummaryDetailList.job !='STF' && findSummaryDetailList.job !='STO' && findSummaryDetailList.job !='TPS' && findSummaryDetailList.job !='STL'}">
	<tr>
	<td>
	${findSummaryDetailList.arrivedAtDest}</td>
	<td>
	${findSummaryDetailList.atArrival}</td>
	<td>
	${findSummaryDetailList.arrivedAtDestAct}</td>
	</tr>
	</c:if>
	<c:if test="${findSummaryDetailList.job !='STF' && findSummaryDetailList.job !='STO' && findSummaryDetailList.job !='TPS' && findSummaryDetailList.job !='STL'}">
	<tr>
	<td>
	${findSummaryDetailList.customsClearance}</td>
	<td>
	${findSummaryDetailList.clearCustom}</td>
	<td>
	${findSummaryDetailList.clearCustomTA}</td>
	</tr>
	</c:if>
	<c:if test="${findSummaryDetailList.lebel =='Arrive at Agent Warehouse'}">
	<tr><td>
	${findSummaryDetailList.lebel}</td>
	<td>
	${findSummaryDetailList.containerRecivedWH}</td>
	<td>
	${findSummaryDetailList.target}</td>
	</tr>
	</c:if>
	<c:if test="${findSummaryDetailList.sitDestinMsg=='Storage in Transit'}">
	<tr><td>
	${findSummaryDetailList.sitDestinMsg}</td>
	<td>
	${findSummaryDetailList.sitDestinYNActDate}</td>
	<td>
	${findSummaryDetailList.sitDestinYNAct}</td>
	</tr>
	</c:if>
	<tr>
	<td>
	${findSummaryDetailList.lebel1}</td>
	<td>
	${findSummaryDetailList.deliveryADate}</td>
	<td>
	${findSummaryDetailList.deliveryActual}</td>
	</tr></c:otherwise>
	</c:choose>

</display:caption>
</c:if>
</display:table></td>
</c:if>
</tr>
<tr>
<td colspan="10">
<c:if test="${findSummaryDetailList!='[]'}">
<c:if test="${serviceOrder.job!='RLO'}">
<c:choose>
<c:when test='${findSummaryDetailList.job == "UVL" || findSummaryDetailList.job == "MVL" || findSummaryDetailList.job == "ULL" || findSummaryDetailList.job == "MLL" || findSummaryDetailList.job == "DOM" || findSummaryDetailList.job == "LOC"}'>

<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li><a><span>Transit Services</span></a></li> 
 </ul> 
</div><div class="spn">&nbsp;</div>
<s:set name="findSummaryDetailList" value="findSummaryDetailList" scope="request"/>
<display:table name="findSummaryDetailList" class="table" requestURI="" id="findSummaryDetailList" style="width:100%;!margin-left:0px" defaultsort="1">
     <display:column property="driverName" sortable="true" title="Driver" style="width:50%"/> 
   	 <display:column property="driverCell" sortable="true" title="Cell #"  style="width:50%"/>
</display:table>

</c:when>

<c:otherwise>
<c:if test="${findSummaryDetailList.job !='STF' && findSummaryDetailList.job !='STO' && findSummaryDetailList.job !='TPS' && findSummaryDetailList.job !='STL'}">

<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li><a><span>Transit Services</span></a></li> 
 </ul> 
</div><div class="spn">&nbsp;</div>
		<s:set name="findSummryViewCarrierList" value="findSummryViewCarrierList" scope="request"/>
<display:table name="findSummryViewCarrierList" class="table" requestURI="" id="findSummryViewCarrierList" style="width:100%;!margin-left:0px" defaultsort="1">
     <display:column property="carrierNumberss" sortable="true" title="#" style="width:3%"/> 
   	 <display:column property="carrierNamess" sortable="true" title="Carrier"  style="width:12%"/>
	 <display:column property="carrierDeparturess" sortable="true" title="Port of Lading"  style="width:18%"/>
	 <display:column property="etDepartss" sortable="true" title="Est.&nbsp;Date&nbsp;of&nbsp;Depart" format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="atDepartss" sortable="true" title="Act.&nbsp;Date&nbsp;of&nbsp;Depart"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="carrierArrivalss" sortable="true" title="Port of Entry"  style="width:18%"/>
	 <display:column property="etArrivalss" sortable="true" title="Est.&nbsp;Date&nbsp;of&nbsp;Arrival"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="atArrivalss" sortable="true" title="Act.&nbsp;Date&nbsp;of&nbsp;Arrival"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
</display:table>

</c:if>
</c:otherwise> 
</c:choose>
</c:if>
</c:if>
<c:if test="${findSummaryDetailList=='[]'}">
<c:if test="${serviceOrder.job!='RLO'}">
<c:choose>
<c:when test='${serviceOrder.job == "UVL" || serviceOrder.job == "MVL" || serviceOrder.job == "ULL" || serviceOrder.job == "MLL" || serviceOrder.job == "DOM" || serviceOrder.job == "LOC"}'>

<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li><a><span>Transit Services</span></a></li> 
 </ul> 
</div><div class="spn">&nbsp;</div>
<s:set name="findSummaryDetailList" value="findSummaryDetailList" scope="request"/>
<display:table name="findSummaryDetailList" class="table" requestURI="" id="findSummaryDetailList" style="width:100%;!margin-left:0px" defaultsort="1">
     <display:column property="driverName" sortable="true" title="Driver" style="width:50%"/> 
   	 <display:column property="driverCell" sortable="true" title="Cell #"  style="width:50%"/>
</display:table>

</c:when>

<c:otherwise>
<c:if test="${serviceOrder.job !='STF' && serviceOrder.job !='STO' && serviceOrder.job !='TPS' && serviceOrder.job !='STL'}">

<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li><a><span>Transit Services</span></a></li> 
 </ul> 
</div><div class="spn">&nbsp;</div>
		<s:set name="findSummryViewCarrierList" value="findSummryViewCarrierList" scope="request"/>
<display:table name="findSummryViewCarrierList" class="table" requestURI="" id="findSummryViewCarrierList" style="width:100%;!margin-left:0px" defaultsort="1">
     <display:column property="carrierNumberss" sortable="true" title="#" style="width:3%"/> 
   	 <display:column property="carrierNamess" sortable="true" title="Carrier"  style="width:12%"/>
	 <display:column property="carrierDeparturess" sortable="true" title="Port of Lading"  style="width:18%"/>
	 <display:column property="etDepartss" sortable="true" title="Est.&nbsp;Date&nbsp;of&nbsp;Depart" format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="atDepartss" sortable="true" title="Act.&nbsp;Date&nbsp;of&nbsp;Depart"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="carrierArrivalss" sortable="true" title="Port of Entry"  style="width:18%"/>
	 <display:column property="etArrivalss" sortable="true" title="Est.&nbsp;Date&nbsp;of&nbsp;Arrival"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
	 <display:column property="atArrivalss" sortable="true" title="Act.&nbsp;Date&nbsp;of&nbsp;Arrival"  format="{0,date,dd-MMM-yyyy}"  style="width:8%"/>
</display:table>

</c:if>
</c:otherwise> 
</c:choose>
</c:if>
</c:if>
</td>
</tr>
</table>
<%-- 
<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin:0px;">
<tr>
<td align="right" class="listwhitetext">Driver</td>
<td><s:textfield cssClass="input-text" name="miscellaneous.driverName"  size="40" maxlength="50" tabindex="20"/></td>
<td></td>
<td align="right" class="listwhitetext" width="45">Cell&nbsp;#</td>
<td><s:textfield cssClass="input-text" name="miscellaneous.driverCell"  size="19" maxlength="15" tabindex="21"/></td>
</tr>
</table>
--%>
</div>
<div class="bottom-header" style="!margin-top:50px;margin-top:31px; width:100%"><span></span></div>
</div>
</div>
</s:form>
</td>
</tr></table>
</td></tr>
</table>


</body>


<script type="text/javascript"> 
highlightTableRows("findSummaryDetailList");  

</script>  

		  	