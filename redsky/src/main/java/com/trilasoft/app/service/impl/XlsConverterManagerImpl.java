package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.XlsConverterDao;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.service.XlsConverterManager;

public class XlsConverterManagerImpl extends GenericManagerImpl<Location,Long> implements XlsConverterManager{

	private XlsConverterDao xlsConverterDao; 
	public XlsConverterManagerImpl(XlsConverterDao xlsConverterDao) {
		super(xlsConverterDao);
		this.xlsConverterDao=xlsConverterDao;

	}

	public String saveData(String sessionCorpID, String file, String userName) {
		return xlsConverterDao.saveData(sessionCorpID, file, userName);

	}

	public String saveStorageData(String sessionCorpID, String file,String userName) {
		return xlsConverterDao.saveStorageData(sessionCorpID, file, userName);
	}

	public String saveRateGridData(String sessionCorpID, String file, String userName,String charge, String contractName,String fileExt){
		return xlsConverterDao.saveRateGridData(sessionCorpID,file,userName,charge,contractName,fileExt);
	}
	public String saveDelRateGridData(String sessionCorpID, String file,String userName, String charge, String contractName, String tempIds,String fileExt){
		return xlsConverterDao.saveDelRateGridData( sessionCorpID, file,userName,charge,contractName,tempIds,fileExt);
	}
	
	public String saveXlsFileDefaultAccountline(String sessionCorpID, String file, String userName,String fileExt,Boolean costElementFlag){
		return xlsConverterDao.saveXlsFileDefaultAccountline( sessionCorpID, file,userName,fileExt,costElementFlag);
	}
	
	public void updateDefaultAccountlinethroughProcedure(String sessionCorpID,Boolean costElementFlag,String username){
		xlsConverterDao.updateDefaultAccountlinethroughProcedure(sessionCorpID,costElementFlag,username);
	}
}
