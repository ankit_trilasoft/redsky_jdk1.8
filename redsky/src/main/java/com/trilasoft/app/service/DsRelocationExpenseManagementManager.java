package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsRelocationExpenseManagement;

public interface DsRelocationExpenseManagementManager extends GenericManager<DsRelocationExpenseManagement, Long>{
	List getListById(Long id);
}
