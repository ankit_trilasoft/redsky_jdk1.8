package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CreditCard;

public interface CreditCardDao extends GenericDao<CreditCard, Long>{
    public List findMaxId();
    public List findByShipNumber(String shipNumber);
    public List getPrimaryFlagStatus(String shipNumber, String sessionCorpID);
    public List getCreditCardMasking(String number);
    public List getCreditcardNumber(Long id);
    public List getServiceOrder(String shipNumber, String sessionCorpID);
}