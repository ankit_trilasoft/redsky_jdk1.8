<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="reverseInvoiceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='reverseInvoiceList.heading'/>"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style> 
	
<script type="text/javascript">

    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	} 
	
	function fillReverseInvoice(){ 
       var invoiceNumber = valButton(document.forms['reverseInvoiceList'].elements['radiobilling']);   
	    if (invoiceNumber == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       {
	       //alert(invoiceNumber);
	        invoiceNumber = invoiceNumber.replace('/','');
	        //alert(invoiceNumber);
	        document.forms['reverseInvoiceList'].elements['recInvoiceNumber'].value=invoiceNumber;
	        //alert("invoiceNumber is set");
	        var agree = confirm("Press OK to proceed, or press Cancel.");
            if(agree)
             {
             //alert("ok in agree");
               document.forms['reverseInvoiceList'].action ='reverseInvoices.html?&btntype=yes&decorator=popup&popup=true';
               document.forms['reverseInvoiceList'].submit(); 
             }
             else {
             return false; 
             } 
           }  
      }
   
   
    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
   
   function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['reverseInvoiceList'].elements['radiobilling'].value; 
    } 
  }
     
  </script>  
</head>
<s:form id="reverseInvoiceList" action="updateSOfromaccountLines" method="post">  
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/> 
<s:hidden name="recInvoiceNumber" /> 
<s:hidden name="shipNumber" value="${shipNumber}"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
	<s:hidden name="rollUpInvoiceFlag" value="True" />
</configByCorp:fieldVisibility>

<s:set name="reverseInvoiceList" value="reverseInvoiceList" scope="request"/> 
<display:table name="reverseInvoiceList" class="table" requestURI="" id="reverseInvoiceList" style="width:400px" defaultsort="1" pagesize="100" >   
 		
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice#" style="width:70px" />
		 <display:column property="billToName" sortable="true" title="Bill To" maxLength="15"  style="width:120px"/> 
		 <display:column property="actualRevenueSum" sortable="true" title="Bill Amount" maxLength="15"  style="width:80px"/> 
	    <display:column   title="" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${reverseInvoiceList.recInvoiceNumber} value=${reverseInvoiceList.recInvoiceNumber}/>
 		</display:column>
	     
</display:table> 
<table> 
<tr>
<td>
<input type="button" name="Submit" onclick="return fillReverseInvoice();" value="Reverse Selected Invoice" class="cssbuttonA" style="width:170px; height:25px" >
</td>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td>
</tr>
</table>
<s:text id="recAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="recAccDateToFormat"/></s:text>
<s:hidden  id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  />
</s:form>
<script type="text/javascript">   

try{
if(document.forms['reverseInvoiceList'].elements['btntype'].value=='yes'){
pick();
}  
}
catch(e){} 
try{
document.forms['reverseInvoiceList'].elements['radiobilling'].checked=true;
}
catch(e){}
</script>  		  	