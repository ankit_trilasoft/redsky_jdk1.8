//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.07.19 at 03:05:44 PM EDT 
//


package com.trilasoft.qtg.wsclient.request.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Routing_Headers" type="{http://www.unigroupinc.com}Routing_Header"/>
 *         &lt;element name="Opportunity" type="{http://www.unigroupinc.com}ESOpportunity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "routingHeaders",
    "opportunity"
})
@XmlRootElement(name = "OpportunityData", namespace = "http://www.unigroupinc.com")
public class OpportunityData {

    @XmlElement(name = "Routing_Headers", namespace = "http://www.unigroupinc.com", required = true)
    protected RoutingHeader routingHeaders;
    @XmlElement(name = "Opportunity", namespace = "http://www.unigroupinc.com")
    protected ESOpportunity opportunity;

    /**
     * Gets the value of the routingHeaders property.
     * 
     * @return
     *     possible object is
     *     {@link RoutingHeader }
     *     
     */
    public RoutingHeader getRoutingHeaders() {
        return routingHeaders;
    }

    /**
     * Sets the value of the routingHeaders property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingHeader }
     *     
     */
    public void setRoutingHeaders(RoutingHeader value) {
        this.routingHeaders = value;
    }

    /**
     * Gets the value of the opportunity property.
     * 
     * @return
     *     possible object is
     *     {@link ESOpportunity }
     *     
     */
    public ESOpportunity getOpportunity() {
        return opportunity;
    }

    /**
     * Sets the value of the opportunity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ESOpportunity }
     *     
     */
    public void setOpportunity(ESOpportunity value) {
        this.opportunity = value;
    }

}
