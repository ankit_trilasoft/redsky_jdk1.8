<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
     
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerViewListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.aliasName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerViewListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerViewListForm'].elements['stateSearch'].value = '';
		document.forms['partnerViewListForm'].elements['countrySearch'].value = '';
		document.forms['partnerViewListForm'].elements['vanlineCode'].value = '';
		document.forms['partnerViewListForm'].elements['partner.isPrivateParty'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isAccount'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isAgent'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isVendor'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isCarrier'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isOwnerOp'].checked = false;
}

function activBtn(){
	document.forms['partnerViewListForm'].elements['addBtn'].disabled = false;
}

function seachValidate(){
		var pp =  document.forms['partnerViewListForm'].elements['partner.isPrivateParty'].checked;
		var ac =  document.forms['partnerViewListForm'].elements['partner.isAccount'].checked;
		var ag =  document.forms['partnerViewListForm'].elements['partner.isAgent'].checked;
		var vd =  document.forms['partnerViewListForm'].elements['partner.isVendor'].checked;
		var cr =  document.forms['partnerViewListForm'].elements['partner.isCarrier'].checked;
		var oo =  document.forms['partnerViewListForm'].elements['partner.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one partner type.');
			return false;
		} 
}

function openOriginLocation(address1,address2,city,zip,state,country) {
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
	}
	function findDefault(){
      var sid=document.forms['partnerViewListForm'].elements['sid'].value; 
 	  //openWindow('findVendorCode.html?sid='+sid+'&partnerType=DF&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription');
      document.forms['partnerViewListForm'].action ='findVendorCode.html?partnerType=DF&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription';
      document.forms['partnerViewListForm'].submit();  	  

}
</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
}

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj',{starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element){
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}


function addPartner(){
	var pType = document.forms['partnerViewListForm'].elements['partnerType'];
	for (var i=0; i < pType.length; i++){
   		if (pType[i].checked){
      		pType = pType[i].value;
      	}
   	}
   	if("${flag}" == "6"){
   		location.href='editPartnerFormPopup.html?origin=${origin}&partnerType='+pType+'&flag=${flag}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';  	
   	}else{
   		location.href='editPartnerFormPopup.html?destination=${destination}&partnerType='+pType+'&flag=${flag}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';  	
   	}
   	
}
</script>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:0px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
font-size:.85em;
}

</style>
</head>
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;"  key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerViewListForm" action='searchPartnerVanline.html?decorator=popup&popup=true' method="post" >  


	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	

<div id="layer1" style="width:100%">
<div id="otabs">
		<ul>
			<li><a class=""><span>Search</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:888px;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th>Vanline Code</th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Alias Name</th>
			<th>Last/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>
			
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="vanlineCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partner.firstName" size="17" cssClass="input-text" /></td>
			<td><s:textfield name="partner.aliasName" size="17" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="17" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="15" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="7" cssClass="input-text"/></td>
		</tr>
		<tr>
			<td colspan="10">
				<table style="margin:0px; padding:0px;" cellpadding="0" cellspacing="0" >
					<tbody>
						<tr>
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isPrivateParty}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border:none;">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAccount}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border:none;"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAgent}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border:none;"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
										
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isVendor}">
			<c:set var="ischecked" value="true"/>
			</c:if>					
			<td align="center" class="listwhitetext" width="100px" style="border:none;"><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
											
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isCarrier}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border:none;"><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
					
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isOwnerOp}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border:none;"><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
			
			<td width="160px" style="border:none;text-align:right;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
</td>
</tr>
</tbody>	
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>

<div id="layer2" style="width:100%">
<div id="otabs" style="margin-top: -15px;">
		<ul>
			<li><a class=""><span>Partner List</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 
<s:set name="partners" value="partners" scope="request"/>
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="false" defaultsort="1" pagesize="10" style="width:100%;margin-top:2px;margin-left:5px; " decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>	
	
		<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
		<display:column title="Alias Name" sortable="true" style="width:390px"><c:out value="${partnerList.aliasName}" /></display:column>
		<display:column title="P.Party" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isPrivateParty == true}">  
				<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=PP&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			</c:if>
		</display:column>
		<display:column title="Account" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isAccount == true}">  
	    		<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Agent" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isAgent == true}">  
	    		<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Carrier" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isCarrier == true}">  
	    		<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Vendor" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isVendor == true}">  
	    		<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Driver" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isOwnerOp == true}">  
	    		<a href="viewPartnerVanline.html?id=${partnerList.id}&sid=${sid}&partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		
	<display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.add1}','${partnerList.add2}','${partnerList.cityName}','${partnerList.zip}','${partnerList.stateName}','${partnerList.countryName}');"/></a></display:column>  
	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    </display:column>
    	
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
	<c:if test="${agentClassificationShow=='Y'}">
	<display:column title="Agent Classification" style="width:45px;" >
 	<c:if test="${partnerList.isAgent == true}">
    	<c:out value="${partnerList.agentClassification}" />
	</c:if>
	</display:column>
	</c:if>
    <display:column title="VanLine Code" style="width:160px"><c:out value="${partnerList.vanLineCode}" /></display:column>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<sec-auth:authComponent componentId="module.button.partner.view">  
	<table height="30px"  width="888px" cellpadding="0" cellspacing="2" style="background:#d8e4fd ; padding-left:1px;padding-top:1px; padding-bottom:1px;padding-right:1px;!width:720px;border:2px solid #99BBE8; ">
		<tbody>
			<s:form id="addPartner" action="editPartnerFormPopup.html?paramView=View" method="post" >  
				<tr>
					<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">Add New Partner :
						<s:radio name="partnerType" list="%{actionTypeView}" onclick="activBtn();"  />
					   	<input type="button" class="cssbutton" style="width:85px; height:25px; margin-left:50px;" name="addBtn" align="top" value=" Add Partner" onclick="addPartner();"/>   
					</td>
				</tr>	
			</s:form>
		</tbody>
	</table>
</sec-auth:authComponent>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${not empty param.popup && partnerType == 'PP' && (flag==0 || flag==1 || flag==2 || flag==3 || flag==4)}">  
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td><c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>
</s:form>
<script type="text/javascript">
try{
	document.forms['partnerViewListForm'].elements['addBtn'].disabled = true;
}
catch(e){}
</script>
