package org.appfuse.model;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.userdetails.UserDetails;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;



import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

import com.trilasoft.app.model.DataSecuritySet;

/**
 * This class represents the basic "user" object in AppFuse that allows for authentication
 * and user management.  It implements Acegi Security's UserDetails interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *         Updated by Dan Kibler (dan@getrolling.com)
 *  Extended to implement Acegi UserDetails interface
 *      by David Carter david@carter.net
 */
@Entity
@Table(name="app_user")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class User extends BaseObject implements Serializable, UserDetails {
    private static final long serialVersionUID = 3832626162173359411L;

    protected Long id;
    protected String username;                    // required
    protected String password;                    // required
    protected String confirmPassword;
    protected String passwordHintQues;
    protected String passwordHint;
    protected String userStatus;
    protected boolean passwordReset;
    protected String firstName;                   // required
    protected String lastName;                    // required
    protected String email;                       // required; unique
    protected String phoneNumber;
    protected String mobileNumber;					/* Ticket Number: 5946 */
    protected String website;
    
    
    protected Address address = new Address();
    protected Integer version;
    protected Set<Role> roles = new HashSet<Role>();
    protected Set<DataSecuritySet> datasecuritysets= new HashSet<DataSecuritySet>();
    protected boolean enabled;
    protected boolean accountExpired;
    protected boolean accountLocked;
    protected boolean credentialsExpired;
    protected boolean defaultSearchStatus;
    //extention of user Table
    protected String corpID;
    protected String supervisor;
    protected String initial;
    protected String warehouse;
    protected String branch;
    protected String department;
    protected String faxNumber;
    protected String rank;
    protected String signature;
    protected String digitalSignature;
    protected String userTitle;
    protected String location;
    private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	protected String defaultOperationCalendar;
	
	protected String addressHome;
    protected String cityHome;
    protected String cityHomeCode;
    protected String provinceHome;
    protected String countryHome;
    protected String countryHomeCode;
    protected String postalCodeHome;
	protected String phoneHome;
    protected String phoneCell;
    protected String workStartTime;
    protected String workEndTime;
    protected String reminderDuration;
    protected String defaultURL;
    protected String defaultSoURL;
    protected String defaultSortForJob;
    protected String defaultSortForCso;
    protected String defaultSortForTicket;
    protected String defaultSortForClaim;
    protected String sortOrderForJob;
    protected String sortOrderForCso;
    protected String sortOrderTicket;
    protected String sortOrderClaim;
    protected String alias;
    protected String userType;
    
    protected String jobType;
    
    private Date pwdexpiryDate;
    
    private String defaultCss;
    
    private String parentAgent;
    private String contract;
    private String functionalArea;
    private String skypeID;
    private String extractJobType;
    private String bookingAgent;
    private String specialAccessCorpIds;
    private String defaultWarehouse;
    private String gender;
    private String basedAt;
    private String basedAtName;
    private Boolean contact;
    private String jobFunction;
    private String networkCoordinator;
	private String companyDivision;
	private boolean newsUpdateFlag;
	private Boolean doNotEmail;
	private Date login_time;
	private String login_status;
	private Boolean mobile_access;
    private String macid;
    private String loginFailureReason;
    private String fileCabinetView;
    private Boolean acctDisplayEntitled = new Boolean(true);
    private Boolean acctDisplayEstimate = new Boolean(true);
    private Boolean acctDisplayRevision = new Boolean(true);
    
    private Boolean pricingRevision = new Boolean(false);
    private Boolean pricingActual = new Boolean(false);
    public  Map filterMap = new HashMap();
    private Date pricePointStartDate;
    private String moveType;
    private String defaultSortforFileCabinet;
	private String sortOrderForFileCabinet;
	private String soListView;
    private Boolean followUpEmailAlert = new Boolean(false);
    private Boolean salesPortalAccess = new Boolean(false);
    private Boolean relocationContact = new Boolean(false);
    private BigDecimal NPSscore= new BigDecimal(0);
    private String commodity;
    private String serviceType;
    private Boolean networkCoordinatorStatus;
    private String recordsForSoDashboard;
	private String additionalCommissionType;
	private Boolean isDefaultContactPerson;
	private String employee;
	@Transient
	public Map getFilterMap() {
		return filterMap;
	}

	public void setFilterMap(Map filterMap) {
		this.filterMap = filterMap;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("username", username).append("password", password)
				.append("confirmPassword", confirmPassword)
				.append("passwordHintQues", passwordHintQues)
				.append("passwordHint", passwordHint)
				.append("userStatus", userStatus)
				.append("passwordReset", passwordReset)
				.append("firstName", firstName).append("lastName", lastName)
				.append("email", email).append("phoneNumber", phoneNumber)
				.append("mobileNumber", mobileNumber)
				.append("website", website).append("address", address)
				.append("version", version).append("roles", roles)
				.append("datasecuritysets", datasecuritysets)
				.append("enabled", enabled)
				.append("accountExpired", accountExpired)
				.append("accountLocked", accountLocked)
				.append("credentialsExpired", credentialsExpired)
				.append("defaultSearchStatus", defaultSearchStatus)
				.append("corpID", corpID).append("supervisor", supervisor)
				.append("initial", initial).append("warehouse", warehouse)
				.append("branch", branch).append("department", department)
				.append("faxNumber", faxNumber).append("rank", rank)
				.append("signature", signature)
				.append("digitalSignature", digitalSignature)
				.append("userTitle", userTitle).append("location", location)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("addressHome", addressHome)
				.append("cityHome", cityHome)
				.append("cityHomeCode", cityHomeCode)
				.append("provinceHome", provinceHome)
				.append("countryHome", countryHome)
				.append("countryHomeCode", countryHomeCode)
				.append("postalCodeHome", postalCodeHome)
				.append("phoneHome", phoneHome).append("phoneCell", phoneCell)
				.append("workStartTime", workStartTime)
				.append("workEndTime", workEndTime)
				.append("reminderDuration", reminderDuration)
				.append("defaultURL", defaultURL)
				.append("defaultSoURL", defaultSoURL)
				.append("defaultSortForJob", defaultSortForJob)
				.append("defaultSortForCso", defaultSortForCso)
				.append("defaultSortForTicket", defaultSortForTicket)
				.append("defaultSortForClaim", defaultSortForClaim)
				.append("sortOrderForJob", sortOrderForJob)
				.append("sortOrderForCso", sortOrderForCso)
				.append("sortOrderTicket", sortOrderTicket)
				.append("sortOrderClaim", sortOrderClaim)
				.append("alias", alias).append("userType", userType)
				.append("jobType", jobType)
				.append("pwdexpiryDate", pwdexpiryDate)
				.append("defaultCss", defaultCss)
				.append("parentAgent", parentAgent)
				.append("contract", contract)
				.append("functionalArea", functionalArea)
				.append("skypeID", skypeID)
				.append("extractJobType", extractJobType)
				.append("bookingAgent", bookingAgent)
				.append("specialAccessCorpIds", specialAccessCorpIds)
				.append("defaultWarehouse", defaultWarehouse)
				.append("gender", gender).append("basedAt", basedAt)
				.append("basedAtName", basedAtName).append("contact", contact)
				.append("jobFunction", jobFunction)
				.append("networkCoordinator", networkCoordinator)
				.append("companyDivision", companyDivision)
				.append("newsUpdateFlag", newsUpdateFlag)
				.append("doNotEmail", doNotEmail)
				.append("login_time", login_time)
				.append("login_status", login_status)
				.append("mobile_access", mobile_access).append("macid", macid)
				.append("loginFailureReason", loginFailureReason)
				.append("fileCabinetView", fileCabinetView)
				.append("acctDisplayEntitled", acctDisplayEntitled)
				.append("acctDisplayEstimate", acctDisplayEstimate)
				.append("acctDisplayRevision", acctDisplayRevision)
				.append("pricingRevision",pricingRevision)
				.append("pricingActual",pricingActual)
				.append("pricePointStartDate",pricePointStartDate)
				.append("moveType",moveType).append("defaultSortforFileCabinet",defaultSortforFileCabinet)
				.append("sortOrderForFileCabinet",sortOrderForFileCabinet).append("soListView",soListView)
				.append("followUpEmailAlert",followUpEmailAlert)
				.append("salesPortalAccess",salesPortalAccess)
				.append("relocationContact",relocationContact)
				.append("NPSscore",NPSscore)
				.append("commodity",commodity)
				.append("serviceType",serviceType)
				.append("defaultOperationCalendar",defaultOperationCalendar)
				.append("networkCoordinatorStatus",networkCoordinatorStatus)
				.append("recordsForSoDashboard",recordsForSoDashboard)
				.append("additionalCommissionType",additionalCommissionType).append("isDefaultContactPerson",isDefaultContactPerson)
				.append("employee",employee).toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof User))
			return false;
		User castOther = (User) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(username, castOther.username)
				.append(password, castOther.password)
				.append(confirmPassword, castOther.confirmPassword)
				.append(passwordHintQues, castOther.passwordHintQues)
				.append(passwordHint, castOther.passwordHint)
				.append(userStatus, castOther.userStatus)
				.append(passwordReset, castOther.passwordReset)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(email, castOther.email)
				.append(phoneNumber, castOther.phoneNumber)
				.append(mobileNumber, castOther.mobileNumber)
				.append(website, castOther.website)
				.append(address, castOther.address)
				.append(version, castOther.version)
				.append(roles, castOther.roles)
				.append(datasecuritysets, castOther.datasecuritysets)
				.append(enabled, castOther.enabled)
				.append(accountExpired, castOther.accountExpired)
				.append(accountLocked, castOther.accountLocked)
				.append(credentialsExpired, castOther.credentialsExpired)
				.append(defaultSearchStatus, castOther.defaultSearchStatus)
				.append(corpID, castOther.corpID)
				.append(supervisor, castOther.supervisor)
				.append(initial, castOther.initial)
				.append(warehouse, castOther.warehouse)
				.append(branch, castOther.branch)
				.append(department, castOther.department)
				.append(faxNumber, castOther.faxNumber)
				.append(rank, castOther.rank)
				.append(signature, castOther.signature)
				.append(digitalSignature, castOther.digitalSignature)
				.append(userTitle, castOther.userTitle)
				.append(location, castOther.location)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(addressHome, castOther.addressHome)
				.append(cityHome, castOther.cityHome)
				.append(cityHomeCode, castOther.cityHomeCode)
				.append(provinceHome, castOther.provinceHome)
				.append(countryHome, castOther.countryHome)
				.append(countryHomeCode, castOther.countryHomeCode)
				.append(postalCodeHome, castOther.postalCodeHome)
				.append(phoneHome, castOther.phoneHome)
				.append(phoneCell, castOther.phoneCell)
				.append(workStartTime, castOther.workStartTime)
				.append(workEndTime, castOther.workEndTime)
				.append(reminderDuration, castOther.reminderDuration)
				.append(defaultURL, castOther.defaultURL)
				.append(defaultSoURL, castOther.defaultSoURL)
				.append(defaultSortForJob, castOther.defaultSortForJob)
				.append(defaultSortForCso, castOther.defaultSortForCso)
				.append(defaultSortForTicket, castOther.defaultSortForTicket)
				.append(defaultSortForClaim, castOther.defaultSortForClaim)
				.append(sortOrderForJob, castOther.sortOrderForJob)
				.append(sortOrderForCso, castOther.sortOrderForCso)
				.append(sortOrderTicket, castOther.sortOrderTicket)
				.append(sortOrderClaim, castOther.sortOrderClaim)
				.append(alias, castOther.alias)
				.append(userType, castOther.userType)
				.append(jobType, castOther.jobType)
				.append(pwdexpiryDate, castOther.pwdexpiryDate)
				.append(defaultCss, castOther.defaultCss)
				.append(parentAgent, castOther.parentAgent)
				.append(contract, castOther.contract)
				.append(functionalArea, castOther.functionalArea)
				.append(skypeID, castOther.skypeID)
				.append(extractJobType, castOther.extractJobType)
				.append(bookingAgent, castOther.bookingAgent)
				.append(specialAccessCorpIds, castOther.specialAccessCorpIds)
				.append(defaultWarehouse, castOther.defaultWarehouse)
				.append(gender, castOther.gender)
				.append(basedAt, castOther.basedAt)
				.append(basedAtName, castOther.basedAtName)
				.append(contact, castOther.contact)
				.append(jobFunction, castOther.jobFunction)
				.append(networkCoordinator, castOther.networkCoordinator)
				.append(companyDivision, castOther.companyDivision)
				.append(newsUpdateFlag, castOther.newsUpdateFlag)
				.append(doNotEmail, castOther.doNotEmail)
				.append(login_time, castOther.login_time)
				.append(login_status, castOther.login_status)
				.append(mobile_access, castOther.mobile_access)
				.append(macid, castOther.macid)
				.append(loginFailureReason, castOther.loginFailureReason)
				.append(fileCabinetView, castOther.fileCabinetView)
				.append(acctDisplayEntitled, castOther.acctDisplayEntitled)
				.append(acctDisplayEstimate, castOther.acctDisplayEstimate)
				.append(acctDisplayRevision, castOther.acctDisplayRevision)
				.append(pricingRevision,castOther.pricingRevision)
				.append(pricingActual,castOther.pricingActual)
				.append(pricePointStartDate,castOther.pricePointStartDate)
				.append(moveType, castOther.moveType).append(defaultSortforFileCabinet,castOther.defaultSortforFileCabinet)
				.append(sortOrderForFileCabinet,castOther.sortOrderForFileCabinet).append(soListView,castOther.soListView)
				.append(followUpEmailAlert, castOther.followUpEmailAlert).append(salesPortalAccess, castOther.salesPortalAccess)
				.append(relocationContact, castOther.relocationContact)
				.append(NPSscore, castOther.NPSscore)
				.append(commodity, castOther.commodity)
				.append("serviceType",castOther.serviceType)
				.append(defaultOperationCalendar, castOther.defaultOperationCalendar).append(networkCoordinatorStatus, castOther.networkCoordinatorStatus)
				.append(recordsForSoDashboard, castOther.recordsForSoDashboard)
				.append(additionalCommissionType, castOther.additionalCommissionType).append(isDefaultContactPerson, castOther.isDefaultContactPerson)
				.append(employee, castOther.employee).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(username)
				.append(password).append(confirmPassword)
				.append(passwordHintQues).append(passwordHint)
				.append(userStatus).append(passwordReset).append(firstName)
				.append(lastName).append(email).append(phoneNumber)
				.append(mobileNumber).append(website).append(address)
				.append(version).append(roles).append(datasecuritysets)
				.append(enabled).append(accountExpired).append(accountLocked)
				.append(credentialsExpired).append(defaultSearchStatus)
				.append(corpID).append(supervisor).append(initial)
				.append(warehouse).append(branch).append(department)
				.append(faxNumber).append(rank).append(signature)
				.append(digitalSignature).append(userTitle).append(location)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(addressHome).append(cityHome)
				.append(cityHomeCode).append(provinceHome).append(countryHome)
				.append(countryHomeCode).append(postalCodeHome)
				.append(phoneHome).append(phoneCell).append(workStartTime)
				.append(workEndTime).append(reminderDuration)
				.append(defaultURL).append(defaultSoURL)
				.append(defaultSortForJob).append(defaultSortForCso)
				.append(defaultSortForTicket).append(defaultSortForClaim)
				.append(sortOrderForJob).append(sortOrderForCso)
				.append(sortOrderTicket).append(sortOrderClaim).append(alias)
				.append(userType).append(jobType).append(pwdexpiryDate)
				.append(defaultCss).append(parentAgent).append(contract)
				.append(functionalArea).append(skypeID).append(extractJobType)
				.append(bookingAgent).append(specialAccessCorpIds)
				.append(defaultWarehouse).append(gender).append(basedAt)
				.append(basedAtName).append(contact).append(jobFunction)
				.append(networkCoordinator).append(companyDivision)
				.append(newsUpdateFlag).append(doNotEmail).append(login_time)
				.append(login_status).append(mobile_access).append(macid)
				.append(loginFailureReason).append(fileCabinetView)
				.append(acctDisplayEntitled).append(acctDisplayEntitled)
				.append(acctDisplayEstimate).append(acctDisplayEstimate)
				.append(acctDisplayRevision).append(acctDisplayRevision)
				.append(pricingRevision)
				.append(pricingActual)
				.append(pricePointStartDate)
				.append(moveType).append(defaultSortforFileCabinet)
		        .append(sortOrderForFileCabinet).append(soListView)
		        .append(followUpEmailAlert).append(salesPortalAccess)
		        .append(relocationContact)
		        .append(NPSscore)
		        .append(commodity)
		        .append(serviceType)
				.append(defaultOperationCalendar)
				.append(networkCoordinatorStatus)
				.append(recordsForSoDashboard)
				.append(additionalCommissionType)
				.append(isDefaultContactPerson)
				.append(employee)
				.toHashCode();
	}

	public User() {}

    public User(String username) {
        this.username = username;
    }

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @Column(nullable=false,length=82,unique=true)
    public String getUsername() {
        return username;
    }

    @Column(nullable=false)
    public String getPassword() {
        return password;
    }

    @Column(nullable=false)
    public String getConfirmPassword() {
        return confirmPassword;
    }

    @Column(name="password_hint")
    public String getPasswordHint() {
        return passwordHint;
    }

    @Column(name="first_name",nullable=false,length=80)
    public String getFirstName() {
        return firstName;
    }

    @Column(name="last_name",nullable=false,length=80)
    public String getLastName() {
        return lastName;
    }

    @Column(nullable=false,unique=true)
    public String getEmail() {
        return email;
    }

    @Column(name="phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    /**
     * Returns the full name.
     * @return firstName + ' ' + lastName
     */
    @Transient
    public String getFullName() {
			return firstName + ' ' + lastName;
    }
     

    @Embedded
    public Address getAddress() {
        return address;
    }

    @ManyToMany(fetch = FetchType.EAGER) 
    @JoinTable(
            name="user_role",
            joinColumns = { @JoinColumn( name="user_id") },
            inverseJoinColumns = @JoinColumn( name="role_id")
            
    ) 
    
    
    
   
    
    public Set<Role> getRoles() {
        return roles;
    }
    
    @ManyToMany(fetch = FetchType.EAGER) 
    @JoinTable(
            name="userdatasecurity",
            joinColumns = { @JoinColumn( name="user_id") },
            inverseJoinColumns = @JoinColumn( name="userdatasecurity_id")
            
    )
    public Set<DataSecuritySet> getDatasecuritysets() {
		return datasecuritysets;
	}

    
	

    /**
     * Convert user roles to LabelValue objects for convenience.
     * @return a list of LabelValue objects with role information
     */
    @Transient
    public List<LabelValue> getRoleList() {
        List<LabelValue> userRoles = new ArrayList<LabelValue>();

        if (this.roles != null) {
            for (Role role : roles) {
                // convert the user's roles to LabelValue Objects
                userRoles.add(new LabelValue(role.getName(), role.getName()));
            }
        }

        return userRoles;
    }

    /**
     * Adds a role for the user
     * @param role the fully instantiated role
     */
    public void addRole(Role role) {
        getRoles().add(role);
    }
    
    
    
    @Transient
    public List<LabelValue> getPermissionList() {
        List<LabelValue> permissions = new ArrayList<LabelValue>();

        if (this.datasecuritysets != null) {
            for (DataSecuritySet dataSecuritySet : datasecuritysets) {
                // convert the user's roles to LabelValue Objects
            	permissions.add(new LabelValue(dataSecuritySet.getName(), dataSecuritySet.getName()));
            }
        }

        return permissions;
    }

    /**
     * Adds a role for the user
     * @param role the fully instantiated role
     */
    public void addPermissions(DataSecuritySet dataSecuritySet) {
        getDatasecuritysets().add(dataSecuritySet);
    }
    
    public void removePermissions(DataSecuritySet dataSecuritySet) {
        getDatasecuritysets().remove(dataSecuritySet);
    }
    
    
    
    /**
     * @see org.acegisecurity.userdetails.UserDetails#getAuthorities()
     */
    @Transient
    public GrantedAuthority[] getAuthorities() {
        return roles.toArray(new GrantedAuthority[0]);
    }

    @Version
    public Integer getVersion() {
        return version;
    }
    
    @Column(name="account_enabled")
    public boolean isEnabled() {
        return enabled;
    }
    
    @Column(name="account_expired",nullable=false)
    public boolean isAccountExpired() {
        return accountExpired;
    }
    
    /**
     * @see org.acegisecurity.userdetails.UserDetails#isAccountNonExpired()
     */
    @Transient
    public boolean isAccountNonExpired() {
        return !isAccountExpired();
    }

    @Column(name="account_locked",nullable=false)
    public boolean isAccountLocked() {
        return accountLocked;
    }
    
    /**
     * @see org.acegisecurity.userdetails.UserDetails#isAccountNonLocked()
     */
    @Transient
    public boolean isAccountNonLocked() {
        return !isAccountLocked();
    }

    @Column(name="credentials_expired",nullable=false)
    public boolean isCredentialsExpired() {
        return credentialsExpired;
    }
    
    /**
     * @see org.acegisecurity.userdetails.UserDetails#isCredentialsNonExpired()
     */
    @Transient
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    
    public void setDatasecuritysets(Set<DataSecuritySet> datasecuritysets) {
		this.datasecuritysets = datasecuritysets;
	}

    public void setVersion(Integer version) {
        this.version = version;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setAccountExpired(boolean accountExpired) {
        this.accountExpired = accountExpired;
    }
    
    public void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public void setCredentialsExpired(boolean credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }
    @Column
    public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(nullable=false)
	public String getCorpID() {
		return corpID;
	}

    
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	
	@Column(length=10)
	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}
	
	 
    @Column(length=50)
    public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	@Column(length=20)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Column(length=20)
	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	@Column(length=15)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	@Column(length=10)
	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	
	@Column
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	
	@Column(length=50)
	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}
	
	@Column(length=25)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=25)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length=82)
	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	@Column
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	@Column
	public String getPasswordHintQues() {
		return passwordHintQues;
	}

	public void setPasswordHintQues(String passwordHintQues) {
		this.passwordHintQues = passwordHintQues;
	}
	@Column(name="password_reset",nullable=false)
	public boolean isPasswordReset() {
		return passwordReset;
	}

	public void setPasswordReset(boolean passwordReset) {
		this.passwordReset = passwordReset;
	}
	@Column
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	@Column(length=100)
	public String getAddressHome() {
		return addressHome;
	}

	public void setAddressHome(String addressHome) {
		this.addressHome = addressHome;
	}
	@Column(length=45)
	public String getCityHome() {
		return cityHome;
	}

	public void setCityHome(String cityHome) {
		this.cityHome = cityHome;
	}
	@Column(length=45)
	public String getCountryHome() {
		return countryHome;
	}

	public void setCountryHome(String countryHome) {
		this.countryHome = countryHome;
	}
	@Column(length=100)
	public String getDefaultURL() {
		return defaultURL;
	}

	public void setDefaultURL(String defaultURL) {
		this.defaultURL = defaultURL;
	}
	@Column(length=30)
	public String getPhoneCell() {
		return phoneCell;
	}

	public void setPhoneCell(String phoneCell) {
		this.phoneCell = phoneCell;
	}
	@Column(length=30)
	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}
	@Column(length=45)
	public String getPostalCodeHome() {
		return postalCodeHome;
	}
	public void setPostalCodeHome(String postalCodeHome) {
		this.postalCodeHome = postalCodeHome;
	}
	@Column(length=45)
	public String getProvinceHome() {
		return provinceHome;
	}

	public void setProvinceHome(String provinceHome) {
		this.provinceHome = provinceHome;
	}
	@Column(length=30)
	public String getReminderDuration() {
		return reminderDuration;
	}

	public void setReminderDuration(String reminderDuration) {
		this.reminderDuration = reminderDuration;
	}
	@Column(length=10)
	public String getWorkEndTime() {
		return workEndTime;
	}

	public void setWorkEndTime(String workEndTime) {
		this.workEndTime = workEndTime;
	}
	@Column(length=10)
	public String getWorkStartTime() {
		return workStartTime;
	}

	public void setWorkStartTime(String workStartTime) {
		this.workStartTime = workStartTime;
	}
	@Column(length=82)
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	@Column(length=30)
	public String getCityHomeCode() {
		return cityHomeCode;
	}

	public void setCityHomeCode(String cityHomeCode) {
		this.cityHomeCode = cityHomeCode;
	}
	@Column(length=10)
	public String getCountryHomeCode() {
		return countryHomeCode;
	}

	public void setCountryHomeCode(String countryHomeCode) {
		this.countryHomeCode = countryHomeCode;
	}
	@Column(length=20)
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the defaultSoURL
	 */
	@Column(length=100)
	public String getDefaultSoURL() {
		return defaultSoURL;
	}

	/**
	 * @param defaultSoURL the defaultSoURL to set
	 */
	public void setDefaultSoURL(String defaultSoURL) {
		this.defaultSoURL = defaultSoURL;
	}

	@Column(length=75)
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the defaultSortForJob
	 */
	@Column(length=30)
	public String getDefaultSortForJob() {
		return defaultSortForJob;
	}

	/**
	 * @param defaultSortForJob the defaultSortForJob to set
	 */
	public void setDefaultSortForJob(String defaultSortForJob) {
		this.defaultSortForJob = defaultSortForJob;
	}

	/**
	 * @return the defaultSortForClaim
	 */
	@Column(length=30)
	public String getDefaultSortForClaim() {
		return defaultSortForClaim;
	}

	/**
	 * @param defaultSortForClaim the defaultSortForClaim to set
	 */
	public void setDefaultSortForClaim(String defaultSortForClaim) {
		this.defaultSortForClaim = defaultSortForClaim;
	}

	/**
	 * @return the defaultSortForCso
	 */
	@Column(length=30)
	public String getDefaultSortForCso() {
		return defaultSortForCso;
	}

	/**
	 * @param defaultSortForCso the defaultSortForCso to set
	 */
	public void setDefaultSortForCso(String defaultSortForCso) {
		this.defaultSortForCso = defaultSortForCso;
	}

	/**
	 * @return the defaultSortForTicket
	 */
	@Column(length=30)
	public String getDefaultSortForTicket() {
		return defaultSortForTicket;
	}

	/**
	 * @param defaultSortForTicket the defaultSortForTicket to set
	 */
	public void setDefaultSortForTicket(String defaultSortForTicket) {
		this.defaultSortForTicket = defaultSortForTicket;
	}

	/**
	 * @return the sortOrderClaim
	 */
	@Column(length=30)
	public String getSortOrderClaim() {
		return sortOrderClaim;
	}

	/**
	 * @param sortOrderClaim the sortOrderClaim to set
	 */
	public void setSortOrderClaim(String sortOrderClaim) {
		this.sortOrderClaim = sortOrderClaim;
	}

	/**
	 * @return the sortOrderForCso
	 */
	@Column(length=30)
	public String getSortOrderForCso() {
		return sortOrderForCso;
	}

	/**
	 * @param sortOrderForCso the sortOrderForCso to set
	 */
	public void setSortOrderForCso(String sortOrderForCso) {
		this.sortOrderForCso = sortOrderForCso;
	}

	/**
	 * @return the sortOrderForJob
	 */
	@Column(length=30)
	public String getSortOrderForJob() {
		return sortOrderForJob;
	}

	/**
	 * @param sortOrderForJob the sortOrderForJob to set
	 */
	public void setSortOrderForJob(String sortOrderForJob) {
		this.sortOrderForJob = sortOrderForJob;
	}

	/**
	 * @return the sortOrderTicket
	 */
	@Column(length=30)
	public String getSortOrderTicket() {
		return sortOrderTicket;
	}

	/**
	 * @param sortOrderTicket the sortOrderTicket to set
	 */
	public void setSortOrderTicket(String sortOrderTicket) {
		this.sortOrderTicket = sortOrderTicket;
	}
	@Column
	public Date getPwdexpiryDate() {
		return pwdexpiryDate;
	}

	public void setPwdexpiryDate(Date pwdexpiryDate) {
		this.pwdexpiryDate = pwdexpiryDate;
	}
	@Column
	public String getDefaultCss() {
		return defaultCss;
	}

	public void setDefaultCss(String defaultCss) {
		this.defaultCss = defaultCss;
	}
	@Column
	public String getParentAgent() {
		return parentAgent;
	}

	public void setParentAgent(String parentAgent) {
		this.parentAgent = parentAgent;
	}
	@Column
	public String getFunctionalArea() {
		return functionalArea;
	}

	public void setFunctionalArea(String functionalArea) {
		this.functionalArea = functionalArea;
	}

	/**
	 * @return the skypeID
	 */
	@Column(length=60)
	public String getSkypeID() {
		return skypeID;
	}

	/**
	 * @param skypeID the skypeID to set
	 */
	public void setSkypeID(String skypeID) {
		this.skypeID = skypeID;
	}
	@Column
	public boolean isDefaultSearchStatus() {
		return defaultSearchStatus;
	}

	public void setDefaultSearchStatus(boolean defaultSearchStatus) {
		this.defaultSearchStatus = defaultSearchStatus;
	}
	@Column(length=15)
	public String getBookingAgent() {
		return bookingAgent;
	}

	public void setBookingAgent(String bookingAgent) {
		this.bookingAgent = bookingAgent;
	}
    @Column(length=20)
	public String getExtractJobType() {
		return extractJobType;
	}
    
	public void setExtractJobType(String extractJobType) {
		this.extractJobType = extractJobType;
	}
	@Column(length=100)
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public String getSpecialAccessCorpIds() {
		return specialAccessCorpIds;
	}

	public void setSpecialAccessCorpIds(String specialAccessCorpIds) {
		this.specialAccessCorpIds = specialAccessCorpIds;
	}
	@Column(length=5)
	public String getDefaultWarehouse() {
		return defaultWarehouse;
	}

	public void setDefaultWarehouse(String defaultWarehouse) {
		this.defaultWarehouse = defaultWarehouse;
	}
	@Column
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	@Column
	public String getBasedAt() {
		return basedAt;
	}

	public void setBasedAt(String basedAt) {
		this.basedAt = basedAt;
	}
	@Column
	public String getBasedAtName() {
		return basedAtName;
	}

	public void setBasedAtName(String basedAtName) {
		this.basedAtName = basedAtName;
	}
	 @Column(name="contact")
	public Boolean getContact() {
		return contact;
	}

	public void setContact(Boolean contact) {
		this.contact = contact;
	}
	@Column
	public String getJobFunction() {
		return jobFunction;
	}

	public void setJobFunction(String jobFunction) {
		this.jobFunction = jobFunction;
	}
	@Column
	public String getDigitalSignature() {
		return digitalSignature;
	}

	public void setDigitalSignature(String digitalSignature) {
		this.digitalSignature = digitalSignature;
	}
	@Column(length = 82)
	public String getNetworkCoordinator() {
		return networkCoordinator;
	}

	public void setNetworkCoordinator(String networkCoordinator) {
		this.networkCoordinator = networkCoordinator;
	}
	@Column
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public boolean isNewsUpdateFlag() {
		return newsUpdateFlag;
	}

	public void setNewsUpdateFlag(boolean newsUpdateFlag) {
		this.newsUpdateFlag = newsUpdateFlag;
	}
	@Column
	public Boolean getDoNotEmail() {
		return doNotEmail;
	}

	public void setDoNotEmail(Boolean doNotEmail) {
		this.doNotEmail = doNotEmail;
	}
	@Column
	public Date getLogin_time() {
		return login_time;
	}

	public void setLogin_time(Date login_time) {
		this.login_time = login_time;
	}
	@Column
	public String getLogin_status() {
		return login_status;
	}

	public void setLogin_status(String login_status) {
		this.login_status = login_status;
	}
	@Column
	public Boolean getMobile_access() {
		return mobile_access;
	}

	public void setMobile_access(Boolean mobile_access) {
		this.mobile_access = mobile_access;
	}
	@Column
	public String getMacid() {
		return macid;
	}

	public void setMacid(String macid) {
		this.macid = macid;
	}

	@Column
	public String getLoginFailureReason() {
		return loginFailureReason;
	}

	public void setLoginFailureReason(String loginFailureReason) {
		this.loginFailureReason = loginFailureReason;
	}
	@Column
	public String getFileCabinetView() {
		return fileCabinetView;
	}

	public void setFileCabinetView(String fileCabinetView) {
		this.fileCabinetView = fileCabinetView;
	}
	@Column
	public Boolean getAcctDisplayEntitled() {
		return acctDisplayEntitled;
	}

	public void setAcctDisplayEntitled(Boolean acctDisplayEntitled) {
		this.acctDisplayEntitled = acctDisplayEntitled;
	}
	@Column
	public Boolean getAcctDisplayEstimate() {
		return acctDisplayEstimate;
	}

	public void setAcctDisplayEstimate(Boolean acctDisplayEstimate) {
		this.acctDisplayEstimate = acctDisplayEstimate;
	}
	@Column
	public Boolean getAcctDisplayRevision() {
		return acctDisplayRevision;
	}

	public void setAcctDisplayRevision(Boolean acctDisplayRevision) {
		this.acctDisplayRevision = acctDisplayRevision;
	}
	@Column
	public Boolean getPricingRevision() {
		return pricingRevision;
	}

	public void setPricingRevision(Boolean pricingRevision) {
		this.pricingRevision = pricingRevision;
	}
	@Column
	public Boolean getPricingActual() {
		return pricingActual;
	}

	public void setPricingActual(Boolean pricingActual) {
		this.pricingActual = pricingActual;
	}
	@Column
	public Date getPricePointStartDate() {
		return pricePointStartDate;
	}

	public void setPricePointStartDate(Date pricePointStartDate) {
		this.pricePointStartDate = pricePointStartDate;
	}
	@Column
	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}
	@Column
	public String getDefaultSortforFileCabinet() {
		return defaultSortforFileCabinet;
	}

	public void setDefaultSortforFileCabinet(String defaultSortforFileCabinet) {
		this.defaultSortforFileCabinet = defaultSortforFileCabinet;
	}
	@Column
	public String getSortOrderForFileCabinet() {
		return sortOrderForFileCabinet;
	}

	public void setSortOrderForFileCabinet(String sortOrderForFileCabinet) {
		this.sortOrderForFileCabinet = sortOrderForFileCabinet;
	}

	@Column
	public String getSoListView() {
		return soListView;
	}

	public void setSoListView(String soListView) {
		this.soListView = soListView;
	}
	@Column
	public Boolean getFollowUpEmailAlert() {
		return followUpEmailAlert;
	}

	public void setFollowUpEmailAlert(Boolean followUpEmailAlert) {
		this.followUpEmailAlert = followUpEmailAlert;
	}
	@Column
	public Boolean getSalesPortalAccess() {
		return salesPortalAccess;
	}

	public void setSalesPortalAccess(Boolean salesPortalAccess) {
		this.salesPortalAccess = salesPortalAccess;
	}
	@Column
	public Boolean getRelocationContact() {
		return relocationContact;
	}

	public void setRelocationContact(Boolean relocationContact) {
		this.relocationContact = relocationContact;
	}
	@Column
	public BigDecimal getNPSscore() {
		return NPSscore;
	}

	public void setNPSscore(BigDecimal nPSscore) {
		NPSscore = nPSscore;
	}
	@Column
	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@Column
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	@Column
	public String getDefaultOperationCalendar() {
		return defaultOperationCalendar;
	}

	public void setDefaultOperationCalendar(String defaultOperationCalendar) {
		this.defaultOperationCalendar = defaultOperationCalendar;
	}
	
	@Column
	public Boolean getNetworkCoordinatorStatus() {
		return networkCoordinatorStatus;
	}

	public void setNetworkCoordinatorStatus(Boolean networkCoordinatorStatus) {
		this.networkCoordinatorStatus = networkCoordinatorStatus;
	}
	@Column
	public String getRecordsForSoDashboard() {
		return recordsForSoDashboard;
	}

	public void setRecordsForSoDashboard(String recordsForSoDashboard) {
		this.recordsForSoDashboard = recordsForSoDashboard;
	}
	@Column
	public String getAdditionalCommissionType() {
		return additionalCommissionType;
	}

	public void setAdditionalCommissionType(String additionalCommissionType) {
		this.additionalCommissionType = additionalCommissionType;
	}
	@Column
	public Boolean getIsDefaultContactPerson() {
		return isDefaultContactPerson;
	}

	public void setIsDefaultContactPerson(Boolean isDefaultContactPerson) {
		this.isDefaultContactPerson = isDefaultContactPerson;
	}
	@Column
	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
}
