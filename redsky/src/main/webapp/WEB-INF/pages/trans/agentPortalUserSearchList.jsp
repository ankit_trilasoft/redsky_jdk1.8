<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
 
    <title>Agent Request Search</title>   
    <meta name="heading" content="Agent Request Search"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	document.getElementsByName('partnerCode')[0].value='';
	document.getElementsByName('userName')[0].value='';
	document.getElementsByName('userEmailId')[0].value='';
	document.getElementsByName('phone_number')[0].value='';
}
 
 
 
function openBookingAgentPopWindow(){
	javascript:openWindow('bookingAgentPopup.html?partnerType=AG&customerVendor=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerName&fld_code=partnerCode');

	}


</script>


<s:form name="contactSearch" id="" action="searchAgentPortalUser"  method="post">
    <s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
	<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	</c:if>
	<s:hidden name="partnerName"/>
<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Partner Code</th>
		<th>User Name</th>
		<th>Email ID</th>
		<th>Phone Number</th>
		<th>Status</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:textfield name="partnerCode" size="30" required="true" cssClass="input-text" />
				<img id="contactSearch.bookingAgentPopUp" class="openpopup" width="17" height="20" style="vertical-align: bottom;" onclick="openBookingAgentPopWindow();document.forms['contactSearch'].elements['partnerCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
				
				</td>
								
				<td width="" align="left"><s:textfield name="userName" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="userEmailId" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="phone_number" size="30" required="true" cssClass="input-text" /></td>		
	            <td  width="" align="left">
	   			<s:select cssClass="list-menu" name="status" list="{'New','Approved','Rejected'}" cssStyle="width:103px" />
	   			</td>
				<td>
	       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search" />  
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div> 
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:6px;
!margin-bottom:2px;
margin-top:-20px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
float:right;
}

</style>


</div><div class="spn" style="width:100%">&nbsp;</div><br><br><br>

<s:set name="agentRequestSearchList" value="agentRequestSearchList" scope="request"/>  
 <display:table name="agentRequestSearchList" class="table" requestURI="" id="userContactList"  pagesize="10" style="width:100%;margin-top:-15px;" > 
  <display:column title="User Name" sortable="true" style="width:65px"><A onclick="location.href='<c:url value="/editUser.html?from=list&id=${userContactList.id}"/>'"><c:out value="${userContactList.username}" /></A></display:column>
 
<display:column sortable="true" title="Full Name" style="width:65px"> 
<%-- <a href="javascript: void(0)" onclick= "getValue('${fn:replace(userContactList.fullName,"'","\\'")}','${userContactList.email}','${userContactList.phoneNumber}','${userContactList.userType}','${userContactList.enabled}');"> --%> 
	    	 <c:out value="${userContactList.fullName}" />
	    </display:column>
        <display:column property="jobFunction" sortable="true" title="Job Function" style="width:65px"/>  
        <display:column property="createdOn"  style="background:#F59696;width:80px;" title="createdOn"  sortable="true" sortProperty="createdOn" format="{0,date,dd-MMM-yyyy}"/>	
        <display:column property="userTitle" sortable="true" title="Job Title" style="width:65px"/>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
<%-- 	    	<a href="javascript: void(0)" onclick= "sendEmail('${userContactList.email}');"> --%>
	    	 <c:out value="${userContactList.email}" />
	    </display:column>
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userContactList.phoneNumber}" />
	    </display:column>
	 <display:column property="parentAgent" sortable="true" title="Partner Code" style="width:65px"/>
	 <display:column title="Status" style="width:65px" >
						
                       
                        <c:if test="${status == 'Approved'}">
                         <c:out value="Approved" />
                         </c:if>
                         <c:if test="${status == 'New'}">
                         <c:out value="New" />
                         </c:if>
                          <c:if test="${status == 'Rejected'}">
                         <c:out value="Rejected" />
                         </c:if>
     </display:column>
    <display:column title="Action" style="width:45px;font-size: 9px;" >
		<div id="agentDetailDiv${userContactList.id}" class="modal fade" ></div>
   		<button type="button" style="width:6em;" class="btn btn-primary btn-xs" id="editProcess${userContactList.id}" onclick="getAgentDetails('${userContactList.id}','${userContactList.email}')" >Processed</button>	
	
	</display:column>
	

</display:table>  
 
</div> 
</s:form>
   <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
 
<script  type="text/javascript">
function getAgentDetails(id,email){
	var $j = jQuery.noConflict();
	   $j.ajax({
	  		 url: 'viewUserDetail.html?id='+id+'&email='+email+'&decorator=modal&popup=true',
	            success: function(data){
	            	$j("#agentDetailDiv"+id).modal({show:true,backdrop: 'static',keyboard: false});
					$j("#agentDetailDiv"+id).html(data);
            },
            error: function () {
                alert('Some error occurs');
                $j("#agentDetailDiv"+id).modal("hide");
             }   
	});  
} 	

</script>
