ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `dosscac` VARCHAR(4) DEFAULT '' AFTER `dosPartnerCode`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `costingDetailShow` BIT(1) NULL DEFAULT b'0'  AFTER `dosscac` ;

ALTER TABLE `redsky`.`reports` ADD COLUMN `extractTheme` VARCHAR(20) DEFAULT '' AFTER `formCondition`;

ALTER TABLE `redsky`.`customerfile` CHANGE COLUMN `homeState` `homeState` VARCHAR(3)  DEFAULT NULL  ;

ALTER TABLE `redsky`.`subcontractorcharges` ADD COLUMN `division` VARCHAR(10) AFTER `onAccount`;

update subcontractorcharges set division='00' where division is null;

