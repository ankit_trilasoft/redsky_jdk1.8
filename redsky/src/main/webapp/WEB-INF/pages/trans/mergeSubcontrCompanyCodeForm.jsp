<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title><fmt:message key="subcontrCompanyCodeForm.title" /></title>
<meta name="heading" content="<fmt:message key='subcontrCompanyCodeForm.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:488px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
.bgblue-left {
background:url("images/blue_band.jpg") no-repeat scroll 0 0 transparent;
color:#007A8D;
font-family:Arial,Helvetica,sans-serif;
font-size:12px;
font-weight:bold;
height:30px;
padding-left:40px;
padding-top:8px;

}
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">


function fillReverseInvoice(){ 
	var postDate = valButton1(document.forms['invoiceCompanyCodeForm'].elements['radiobilling1']);   
    if (postDate == null) 
	   {
          alert("Please select any radio button"); 
       	  return false;
       } 
       else 
       { 
        postDate = postDate.replace('/','');  
        document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value=postDate;
        //var companyCode = document.forms['invoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value; 
     var recPostingDate = document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value; 
     //companyCode=companyCode.trim();
     recPostingDate=recPostingDate.trim(); 
     if(recPostingDate!='')
     { 
        var agree = confirm("Invoice data for posting date " +recPostingDate+ " is under processing.");
     if(agree)
      {
    	 document.forms['invoiceCompanyCodeForm'].action = 'accDriverMergeExtract.html';
    	 document.forms['invoiceCompanyCodeForm'].submit();
      }
      else {
      return false; 
      }
    }  
} 
}
function valButton1(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['invoiceCompanyCodeForm'].elements['radiobilling1'].value; 
    } 
  }

function checkCompanyCode1()
{ 
 var companyCode = document.forms['invoiceCompanyCodeForm'].elements['subcontrCompanyCode'].value; 
 companyCode=companyCode.trim();
// alert(companyCode);
 if(companyCode=='')
 {
  document.forms['invoiceCompanyCodeForm'].elements['Extract'].disabled=true;
   
  
 }
else
{
document.forms['invoiceCompanyCodeForm'].elements['Extract'].disabled=false;
 
}
}


function refreshPostDate()
{
 document.forms['invoiceCompanyCodeForm'].elements['subcontrCompanyCode'].value='';
 document.forms['invoiceCompanyCodeForm'].elements['invoiceDate'].value='';
 document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value='';
document.forms['invoiceCompanyCodeForm'].action = 'subcontrMergeCompanyCode.html';
document.forms['invoiceCompanyCodeForm'].submit();
}

  

 
  function setCompanydiv1(companydivision,invoiceDate,temp)
  {
  document.forms['invoiceCompanyCodeForm'].elements['subcontrCompanyCode'].value= companydivision 
  document.forms['invoiceCompanyCodeForm'].elements['invoiceDate'].value= invoiceDate
  document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value= temp.value;  
  checkCompanyCode1();
  }
  
</script>
</head>
<%--<s:form id="subcontrCompanyCodeForm" name="subcontrCompanyCodeForm" action="subcontractorExtract" method="post">
<div id="Layer1" style="width:85%;">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Subcontractor Pay Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table  border="0" class="" style="width:80%;" cellspacing="0" cellpadding="2">
	
	<tr><td height="5"></td></tr>
		<tr>
	    <td width="5px"></td>
		<td align="left"  class="listwhitebox" width="200px">Please enter Company Group Code</td> 
		<td align="left" width="100px"><s:select list="%{subcontrCompanyCodeList}" cssClass="list-menu" cssStyle="width:100px" headerKey=" " headerValue=" " id="subcontrCompanyCode"  name="subcontrCompanyCode"  onchange="checkCompanyCode();"/></td>
		</tr> 
		<tr><td height="10"></td></tr>
		<tr>
		<td width="5px"></td>
		<td></td> 
        <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Extract" name="Extract"  cssStyle="width:70px;"  />  
        </td>
        <td align="left" ><input type="button" class="cssbutton1" name="AccDriverExtract"  style="width:150px; " onclick="fillReverseInvoice();" value="Acc Driver Extract"/>
        </td>
		</tr> 
		<tr><td height="15"></td></tr>
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
</s:form>
--%>

  
<div id="Layer1" style="width:100%;">
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
		<div class="bgblue-left">Subcontractor Extract</div>
		<div>
		<table border="0" style="margin-bottom: 3px">
			<tr> <td height="1px"></td></tr>
	<tr> 
		<td width="5px"></td>
		
	    <td align="left"><input type="button"  class="cssbutton1"  value="Refresh Date List for All Companies" name="refresh"  style="width:230px;" onclick="refreshPostDate();" />
		</td>
		</tr>
		</table>
		</div>
	</div>

       <div class="bottom-header" style="margin-top:40px;"><span></span></div>
       </div>
       </div>
       
<table border="0" width="100%">
 
<tr>
<td valign="top">

</td>
<td valign="top">

<s:form id="invoiceCompanyCodeForm" name="invoiceCompanyCodeForm" action="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:hidden cssClass="input-textUpper" id="recPostingDate" name="recPostingDate"  />
<s:hidden cssClass="input-textUpper" id="invoiceDate" name="invoiceDate"  />
<s:hidden cssClass="input-textUpper" id="subcontrCompanyCode" name="subcontrCompanyCode"  /> 
<table  border="0"  width="70%" cellspacing="0" cellpadding="0" >
  <tr>
    <td></td>
  </tr>
  <c:if test="${accInvoicePostDateList!='[]'}"> 
  <tr>
    <td>
	<table class="detailTabLabel" cellpadding="0" cellspacing="2" ><tr>
	<td align="right" class="listwhitetext"><b>The following posting dates are available, please select one to process</b></td> 
	</tr></table>
	</td>
  </tr>
  </c:if>
  <tr>
    <td valign="top">
	<table border="0" width="100%" style="margin:0px;">
		<tr><td>
		<s:set name="accInvoicePostDateList" value="accInvoicePostDateList" scope="request"/> 
        
        <display:table name="accInvoicePostDateList" class="table" requestURI="" id="accInvoicePostDateList" style="width:100%"  pagesize="100" > 
        <display:column property="invoiceDate"  title="Date&nbsp;Period"   style="width:140px"/>  
 		<display:column property="companyDivision" sortable="true" title="Company&nbsp;Division"  style="width:40px"/>
		<display:column property="recPostDate" sortable="true" title="Posting Date"   style="width:70px" />
		<display:column property="recPostDateCount" sortable="true" title="# Recs" style="width:50px"/> 
		<display:column   title="Select" style="width:10px" >
 			<input style="vertical-align:bottom;" type="radio" name="radiobilling1" id=${accInvoicePostDateList.recPostDate} value=${accInvoicePostDateList.recPostDate} onclick="setCompanydiv1('${accInvoicePostDateList.companyDivision}','${accInvoicePostDateList.invoiceDate}',this);"/>
 		 	<s:hidden id="recPostingDate1"  name="recPostingDate1"  value="${accInvoicePostDateList.recPostDate}"/>
 		 	<s:hidden id="invoiceCompanyCode1" name="invoiceCompanyCode1" value="${accInvoicePostDateList.companyDivision}"/>
 		</display:column>
 		
	    
</display:table> 
</td>
</tr>
</table>
	</td>
  </tr>
  <tr>
    <td>
	<table><tbody> 
		<tr>
		
        <td align="left"><input type="button"  class="cssbutton1"  value="Extract" name="Extract"  style="width:70px;" onclick="return fillReverseInvoice();" />  
        </td>
        
      

		</tr> 
	</tbody></table>
	</td>
  </tr>
  
</table>


</s:form>

</td>
</tr>
</table></div>
<script type="text/javascript"> 
try{

checkCompanyCode1();
}
catch(e){}
</script>