<%@ include file="/common/taglibs.jsp"%>  

<head>   
    <title><fmt:message key="costingGLList.title"/></title>   
    <meta name="heading" content="<fmt:message key='costingGLList.heading'/>"/>   
</head>   
  
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
 
<c:set var="buttons">  
	<input type="button" class="cssbutton" style="width:70px; height:25px" 
        onclick="location.href='<c:url value="/editCosting.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	<input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>   

      
<s:form id="costingForm" action="saveCosting" method="post" validate="true"> 
<div id="Layer1">
<table cellspacing="0" cellpadding="0" border="0" width="700px">
	<tbody>
	<tr><td height="10px"></td></tr>
	<tr><td>
	 <display:table name="costings" class="table" requestURI="" id="costingGLList" defaultsort="3" export="true" pagesize="10">   
     <display:column property="invoiceNumber" sortable="true" titleKey="costing.invoiceNumber" url="/editCosting.html" paramId="id" paramProperty="id"/>
    <display:column property="expPayeeType" sortable="true" titleKey="receivable.type"/>
    <display:column property="billToCode" sortable="true" titleKey="costing.billToCode"/>
    <display:column property="sentAccount" sortable="true" titleKey="costing.sentAccount" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="amount" sortable="true" titleKey="costing.amount"/>
    <display:column property="invoiceDate" sortable="true" titleKey="costing.invoiceDate" format="{0,date,dd-MMM-yyyy}"/> 
    <display:column property="poNumber" sortable="true" titleKey="costing.poNumber"/>
    <display:column property="description" sortable="true" titleKey="costing.description" maxLength="20"/>
    <display:column property="gl" sortable="true" titleKey="costing.gl"/>
    <display:column property="poNumber" sortable="true" titleKey="costing.poNumber"/>
        
    <display:setProperty name="paging.banner.item_name" value="costing"/>   
    <display:setProperty name="paging.banner.items_name" value="Invoice"/>   
  
    <display:setProperty name="export.excel.filename" value="Costing List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Costing List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Costing List.pdf"/>   
</display:table>   
  </td></tr></tbody></table>
  </div>
<c:out value="${buttons}" escapeXml="false" />   
</s:form>  

<script type="text/javascript">   
    highlightTableRows("costingList");   
</script>  
