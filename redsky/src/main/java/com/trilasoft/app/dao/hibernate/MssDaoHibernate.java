package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.MssDao;
import com.trilasoft.app.dao.hibernate.WorkTicketDaoHibernate.DestinationCityDTO;
import com.trilasoft.app.model.Mss;
import com.trilasoft.app.model.RefMaster;

public class MssDaoHibernate extends GenericDaoHibernate<Mss, Long> implements MssDao{

	public MssDaoHibernate() {
		super(Mss.class);		
	}
	public List findListByShipNumber(String shipNumber,String sessionCorpID){
		return getHibernateTemplate().find("from TrackingStatus where shipNumber='"+ shipNumber +"' and corpId='"+sessionCorpID +"' ");
	}
	public List checkById(Long wid) {
    	return getHibernateTemplate().find("select id from Mss where workTicketId=?",wid);
    }	
	public List findRecord(Long id,String sessionCorpID) {
		String query="from MssGrid where mssId='"+id+"' and corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;	
		
	}	
	public Map<String, String> findParentRecord(String sessionCorpID,String parameter) {
		String query="from RefMaster where parameter ='"+parameter+"' and corpId='"+sessionCorpID +"' and (flex1 is null or flex1='') and (language='en' or language='' or language is null ) ORDER BY description ";
		List<RefMaster> list=getSession().createQuery(query).list();		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;		
	}
	
	public Map<String, String> findChildRecord(String sessionCorpID,String parameter,String parentCode) {
		List parentList=new ArrayList();
		String parentList1="";
		String query="";
		parentList  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'MSS_SERVICE_CAT' and corpID = '"+sessionCorpID+"' and description = '"+parentCode+"' ").list();
		if(parentList!=null && !parentList.isEmpty() && parentList.get(0)!=null){
			parentList1= parentList.get(0).toString();
		}
		query="from RefMaster where parameter ='"+parameter+"' and corpId='"+sessionCorpID +"' and flex1='"+parentList1+"'  and (language='en' or language='' or language is null ) ORDER BY description ";
		List<RefMaster> list=getSession().createQuery(query).list();		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;		
	}
	
	public List findAllOriServiceRecord(Long id,String sessionCorpID) {
		String query="from MssOriginService where mssId='"+id+"' and corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;	
		
	}
	
	public List findAllDestinationServiceList(Long id,String sessionCorpID) {
		String query="from MssDestinationService where mssId='"+id+"' and corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;	
		
	}
	
	public Map<String, String> findDestinationParentRecord(String sessionCorpID,String parameter) {
		String query="from RefMaster where parameter ='"+parameter+"' and corpId='"+sessionCorpID +"' and (flex1 is null or flex1='') and (language='en' or language='' or language is null ) ORDER BY description ";
		List<RefMaster> list=getSession().createQuery(query).list();		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;		
	}
	
	public Map<String, String> findDestinationChildRecord(String sessionCorpID,String parameter,String parentCode) {
		List parentList=new ArrayList();
		String parentList1="";
		String query="";
		parentList  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'MSS_SERVICE_CAT' and corpID = '"+sessionCorpID+"' and description = '"+parentCode+"' ").list();
		if(parentList!=null && !parentList.isEmpty() && parentList.get(0)!=null){
			parentList1= parentList.get(0).toString();
		}
		query="from RefMaster where parameter ='"+parameter+"' and corpId='"+sessionCorpID +"' and flex1='"+parentList1+"'  and (language='en' or language='' or language is null ) ORDER BY description ";
		List<RefMaster> list=getSession().createQuery(query).list();		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;		
	}
	
	public List findValuesForQuotePDF(Long id,String TransportTypeRadio,String PurchaseOrderNumber,Long workTicketId,String sessionCorpID){
		List tRadioValue=new ArrayList();
		
		List list=this.getSession().createSQLQuery("select m.affiliatedTo,m.billingDivision,m.purchaseOrderNumber,m.weight,m.packingStartDate,m.loadingStartDate,m.deliveryStartDate,m.requestedDestinationDate,m.requestedOriginDate,m.shipperFirstName,m.shipperLastName" +
				",m.shipperOriginAddress1,m.shipperOriginAddress2,m.shipperOriginCity,m.shipperOriginState,m.shipperOriginZip," +
				"m.shipperDestinationAddress1,m.shipperDestinationAddress2,m.shipperDestinationCity,m.shipperDestinationState,m.shipperDestinationZip,m.urgent,m.vip,m.packingEndDate,m.loadingEndDate,m.deliveryEndDate,m.submitAs " +
				"from mss m " +
				"where m.corpid='"+sessionCorpID+"' and m.id='"+id+"' and m.transportTypeRadio='"+TransportTypeRadio+"' " +
				"and m.purchaseOrderNumber='"+PurchaseOrderNumber+"' " +
				"and workticketid='"+workTicketId+"'")
				.addScalar("m.affiliatedTo", Hibernate.INTEGER)
				.addScalar("m.billingDivision", Hibernate.INTEGER)
				.addScalar("m.purchaseOrderNumber", Hibernate.STRING)
				.addScalar("m.weight", Hibernate.INTEGER)
				.addScalar("m.packingStartDate", Hibernate.DATE)
				.addScalar("m.loadingStartDate", Hibernate.DATE)
				.addScalar("m.deliveryStartDate", Hibernate.DATE)
				.addScalar("m.requestedDestinationDate", Hibernate.DATE)
				.addScalar("m.requestedOriginDate", Hibernate.DATE)
				.addScalar("m.shipperFirstName", Hibernate.STRING)
				.addScalar("m.shipperLastName", Hibernate.STRING)
				.addScalar("m.shipperOriginAddress1", Hibernate.STRING)
				.addScalar("m.shipperOriginAddress2", Hibernate.STRING)
				.addScalar("m.shipperOriginCity", Hibernate.STRING)
				.addScalar("m.shipperOriginState", Hibernate.STRING)
				.addScalar("m.shipperOriginZip", Hibernate.STRING)
				.addScalar("m.shipperDestinationAddress1", Hibernate.STRING)
				.addScalar("m.shipperDestinationAddress2", Hibernate.STRING)
				.addScalar("m.shipperDestinationCity", Hibernate.STRING)
				.addScalar("m.shipperDestinationState", Hibernate.STRING)
				.addScalar("m.shipperDestinationZip", Hibernate.STRING)
				.addScalar("m.urgent", Hibernate.BOOLEAN)
				.addScalar("m.vip", Hibernate.BOOLEAN)
				.addScalar("m.packingEndDate", Hibernate.DATE)
				.addScalar("m.loadingEndDate", Hibernate.DATE)
				.addScalar("m.deliveryEndDate", Hibernate.DATE)
				.addScalar("m.submitAs",Hibernate.STRING)
				.list();
		Iterator it = list.iterator();
		MssDTO mssDTO=null;
		while (it.hasNext()) {
		Object[] obj = (Object[]) it.next();
		mssDTO=new MssDTO();
		mssDTO.setAffiliateId(obj[0]);
		mssDTO.setBillToId(obj[1]);
		mssDTO.setPurchaseOrderNumber(obj[2]);
		mssDTO.setEstimatedWeight(obj[3]);
		mssDTO.setPackDateStart(obj[4]);
		mssDTO.setLoadingStartDate(obj[5]);
		mssDTO.setDeliveryDateStart(obj[6]);
		mssDTO.setDestinationRequestedDate(obj[7]);
		mssDTO.setOriginRequestedDateStart(obj[8]);
		mssDTO.setShipperFirstName(obj[9]);
		mssDTO.setShipperLastName(obj[10]);
		mssDTO.setShipperOriginAddress1(obj[11]);
		mssDTO.setShipperOriginAddress2(obj[12]);
		mssDTO.setShipperOriginCity(obj[13]);
		mssDTO.setShipperOriginState(obj[14]);
		mssDTO.setShipperOriginZip(obj[15]);
		mssDTO.setShipperDestinationAddress1(obj[16]);
		mssDTO.setShipperDestinationAddress2(obj[17]);
		mssDTO.setShipperDestinationCity(obj[18]);
		mssDTO.setShipperDestinationState(obj[19]);
		mssDTO.setShipperDestinationZip(obj[20]);
		mssDTO.setUrgent(obj[21]);
		mssDTO.setVip(obj[22]);
		mssDTO.setPackingEndDate(obj[23]);
		mssDTO.setLoadingEndDate(obj[24]);
		mssDTO.setDeliveryEndDate(obj[25]);
		mssDTO.setCustomerContactID(obj[26]);
		List CustomerContact=this.getSession().createSQLQuery("select flex3 from refmaster where parameter='SUBMITAS' and code='"+obj[26]+"' and corpid='"+sessionCorpID+"'").list();
		if(CustomerContact!=null && !CustomerContact.isEmpty()){
			String CustomerEmail=CustomerContact.get(0).toString();
			mssDTO.setCustomerContactEmailAddress(CustomerEmail);
		}else{
			mssDTO.setCustomerContactEmailAddress("");
		}
		tRadioValue.add(mssDTO);
		}
	return tRadioValue;	
	}
	public List findCratingValue(Long id,String sessionCorpID){
		List cratingValue=new ArrayList();
		List list1=this.getSession().createSQLQuery("select transportType,description,length,width,height,cod,ply,appr,intlIspmis " +
				"from mssgrid " +
				"where mssid='"+id+"' and corpid='"+sessionCorpID+"'")
				.addScalar("transportType", Hibernate.STRING)
				.addScalar("description", Hibernate.STRING)
				.addScalar("length", Hibernate.DOUBLE)
				.addScalar("width", Hibernate.DOUBLE)
				.addScalar("height", Hibernate.DOUBLE)
				.addScalar("cod", Hibernate.BOOLEAN)
				.addScalar("ply", Hibernate.BOOLEAN)
				.addScalar("appr", Hibernate.BOOLEAN)
				.addScalar("intlIspmis", Hibernate.BOOLEAN)
				.list();
		Iterator it1 = list1.iterator();
		MssDTO mssCatDTO=null;
		while (it1.hasNext()) {
			Object[] obj = (Object[]) it1.next();
			mssCatDTO=new MssDTO();
			mssCatDTO.setTransportType(obj[0]);
			mssCatDTO.setDescription(obj[1]);
			mssCatDTO.setLength(obj[2]);
			mssCatDTO.setWidth(obj[3]);
			mssCatDTO.setHeight(obj[4]);
			mssCatDTO.setCod(obj[5]);
			mssCatDTO.setPly(obj[6]);
			mssCatDTO.setAppr(obj[7]);
			mssCatDTO.setIntlIspmis(obj[8]);
			cratingValue.add(mssCatDTO);
		}
		return cratingValue;
	}
	public List findOriginValue(Long id,String sessionCorpID){
		List originValue=new ArrayList();
		List list2=this.getSession().createSQLQuery("select oriItems,oricod,oriappr " +
				"from mssoriginservice " +
				"where mssid='"+id+"' and corpid='"+sessionCorpID+"'")
				.addScalar("oriItems", Hibernate.STRING)
				.addScalar("oricod", Hibernate.BOOLEAN)
				.addScalar("oriappr", Hibernate.BOOLEAN)
				.list();
		Iterator it2 = list2.iterator();
		MssDTO mssOrgDTO=null;
		while (it2.hasNext()) {
			Object[] obj = (Object[]) it2.next();
			mssOrgDTO=new MssDTO();
			List oriItems=this.getSession().createSQLQuery("select code from refmaster where parameter='MSS_ORIGIN_CAT' and description='"+obj[0]+"' and corpid='"+sessionCorpID+"'").list();
			if(oriItems!=null && !oriItems.isEmpty()){
			String oriItem=oriItems.get(0).toString();
			mssOrgDTO.setServiceNumber(Integer.parseInt(oriItem));
			}else{
			mssOrgDTO.setServiceNumber(0);
			}
			mssOrgDTO.setOricod(obj[1]);
			mssOrgDTO.setOriappr(obj[2]);
			originValue.add(mssOrgDTO);
		}
		return originValue;
	}
	public List findDestinationValue(Long id,String sessionCorpID){
		List destinationValue=new ArrayList();
		List list3=this.getSession().createSQLQuery("select destinationItems,destinationCod,destinationAppr " +
				"from mssdestinationservice " +
				"where mssid='"+id+"' and corpid='"+sessionCorpID+"'")
			     .addScalar("destinationItems", Hibernate.STRING)
				.addScalar("destinationCod", Hibernate.BOOLEAN)
				.addScalar("destinationAppr", Hibernate.BOOLEAN)
				.list();
		Iterator it3 = list3.iterator();
		MssDTO mssDestDTO=null;
		while (it3.hasNext()) {
			Object[] obj = (Object[]) it3.next();
			mssDestDTO=new MssDTO();
			List destinationItems=this.getSession().createSQLQuery("select code from refmaster where parameter='MSS_Destination_CAT' and description='"+obj[0]+"' and corpid='"+sessionCorpID+"'").list();
			if(destinationItems!=null && !destinationItems.isEmpty()){
				String destinationItem=destinationItems.get(0).toString();
				mssDestDTO.setDestServiceNumber(Integer.parseInt(destinationItem));
			}else{
				mssDestDTO.setDestServiceNumber(0);
			}
			mssDestDTO.setDestinationCod(obj[1]);
			mssDestDTO.setDestinationAppr(obj[2]);
			destinationValue.add(mssDestDTO);
		}
		return destinationValue;
	}
	public List getMssNotes(Long id,String PurchaseOrderNumber,String noteType,String sessionCorpID){
		List MssNotesValue=new ArrayList();
		List list4=this.getSession().createSQLQuery("select note from notes where notesid='"+PurchaseOrderNumber+"' and noteskeyid='"+id+"' and notesubtype='"+noteType+"' and corpid='"+sessionCorpID+"'")
		.addScalar("note", Hibernate.STRING)
		.list();
		Iterator it4 = list4.iterator();
		MssDTO mssNoteDTO=null;
		while (it4.hasNext()) {
			Object[] obj = (Object[]) it4.next();
			mssNoteDTO=new MssDTO();
			mssNoteDTO.setNote(obj[0].toString());
			MssNotesValue.add(mssNoteDTO);
		}
		return MssNotesValue;
	}
	public List getOriginPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID){
		List MssOriginValue=new ArrayList();
		List list5=this.getSession().createSQLQuery("select if(homePhone is not null and homePhone !='','1','') as phoneTypeId,homePhone from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'" +
				"union select if(originMobile is not null and originMobile !='','2','') as phoneTypeId,originMobile from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'" +
				"union select if(phone is not null and phone !='','5','') as phoneTypeId,phone from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'")
		        .addScalar("phoneTypeId", Hibernate.STRING)
				.addScalar("homePhone", Hibernate.STRING)
		.list();
		Iterator it5 = list5.iterator();
		MssDTO mssOriginPhone=null;
		while (it5.hasNext()) {
			Object[] obj = (Object[]) it5.next();
			mssOriginPhone=new MssDTO();
			if(obj[1]!=null && !obj[1].toString().equals("")){
			mssOriginPhone.setPhoneTypeId(obj[0]);
			mssOriginPhone.setHomePhone(obj[1]);
			MssOriginValue.add(mssOriginPhone);
			}
		}
		return MssOriginValue;
	}
	public List getdestinationPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID){
		List MssdestinationValue=new ArrayList();
		List list6=this.getSession().createSQLQuery("select if(destinationHomePhone is not null and destinationHomePhone !='','1','') as phoneTypeId,destinationHomePhone from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'" +
				"union select if(destinationMobile is not null and destinationMobile !='','2','') as phoneTypeId,destinationMobile from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'" +
				"union select if(destinationPhone is not null and destinationPhone !='','5','') as phoneTypeId,destinationPhone from workticket where id='"+workTicketId+"' and shipNumber='"+PurchaseOrderNumber+"' and corpid='"+sessionCorpID+"'")
		        .addScalar("phoneTypeId", Hibernate.STRING)
				.addScalar("destinationHomePhone", Hibernate.STRING)
		.list();
		Iterator it6 = list6.iterator();
		MssDTO mssdestinationPhone=null;
		while (it6.hasNext()) {
			Object[] obj = (Object[]) it6.next();
			mssdestinationPhone=new MssDTO();
			if(obj[1]!=null && !obj[1].toString().equals("")){
			mssdestinationPhone.setPhoneTypeId(obj[0]);
			mssdestinationPhone.setDestinationHomePhone(obj[1]);
			MssdestinationValue.add(mssdestinationPhone);
			}
		}
		return MssdestinationValue;
	}
	public void updateplaceOrderNumber(Long id, String soNumber,String workTicketNumber,String placeOrderNumber,String sessionCorpID){
		//String abc="update mss set mssOrderNumber='"+placeOrderNumber+"' where id='"+id+"' and soNumber='"+soNumber+"' and workTicketNumber='"+workTicketNumber+"' and corpId='"+sessionCorpID+"'";
		this.getSession().createSQLQuery("update mss set mssOrderNumber='"+placeOrderNumber+"' where id='"+id+"' and soNumber='"+soNumber+"' and workTicketNumber='"+workTicketNumber+"' and corpId='"+sessionCorpID+"'").executeUpdate();
	}
	public void updatefilePath(Long id, String soNumber,String workTicketNumber,String filePath,String sessionCorpID){
		//String abc="update mss set location='"+filePath+"' where id='"+id+"' and soNumber='"+soNumber+"' and workTicketNumber='"+workTicketNumber+"' and corpId='"+sessionCorpID+"'";
		this.getSession().createSQLQuery("update mss set location='"+filePath+"' where id='"+id+"' and soNumber='"+soNumber+"' and workTicketNumber='"+workTicketNumber+"' and corpId='"+sessionCorpID+"'").executeUpdate();	
	}
	public void updateRefmaster(String submitAs,String userName,String sessionCorpID){
		this.getSession().createSQLQuery("update refmaster set flex2='"+userName+"' where code='"+submitAs+"' and  parameter='SUBMITAS' and (flex2='' or flex2 is null) and corpId='"+sessionCorpID+"' ").executeUpdate();
	}
	public List getdefaultSubmitAs(String userName,String sessionCorpID){
		String query="select code from refmaster where parameter ='SUBMITAS' and corpId='"+sessionCorpID +"' and flex2='"+userName+"'  and (language='en' or language='' or language is null ) ORDER BY code";
		List list=this.getSession().createSQLQuery(query).list();
		return list;
	}
	public List getMsscustomerID(String companyDivision,String sessionCorpID){
		
		return getHibernateTemplate().find("select distinct mssCustomerID,mssPassword from CompanyDivision where companyCode='"+companyDivision+"' and corpid='"+sessionCorpID+"' and mssCustomerID is not null and mssCustomerID !='' and mssPassword is not null and mssPassword !='' ");
	}
	public List getSoDetails(String PurchaseOrderNumber,String sessionCorpID){
		List MssSoDetailsValue=new ArrayList();
		List SoDetails=this.getSession().createSQLQuery("select email,destinationemail from serviceorder where shipnumber='"+PurchaseOrderNumber+"' and corpId='"+sessionCorpID+"'")
		     .addScalar("email", Hibernate.STRING)
			.addScalar("destinationemail", Hibernate.STRING)
		    .list();
		Iterator it6 = SoDetails.iterator();
		MssDTO mssSoDetails=null;
		while (it6.hasNext()) {
			Object[] obj = (Object[]) it6.next();
			mssSoDetails=new MssDTO();
			if(obj[1]!=null && !obj[1].toString().equals("")){
				mssSoDetails.setSoEmail(obj[0]);
				mssSoDetails.setSoDestinationemail(obj[1]);
				MssSoDetailsValue.add(mssSoDetails);
			}
		}
		return MssSoDetailsValue;
	}
   public class MssDTO{
	    private Object soEmail;
	    private Object soDestinationemail;
		private Object affiliateId;
		private Object billToId;
		private Object purchaseOrderNumber;
		private Object EstimatedWeight;
		private Object packDateStart;
		private Object packingEndDate;
		private Object loadingStartDate;
		private Object loadingEndDate;
		private Object deliveryDateStart;
		private Object deliveryEndDate;
		private Object originRequestedDateStart;
		private Object destinationRequestedDate;
		private Object shipperFirstName;
		private Object shipperLastName;
		private Object shipperOriginAddress1;
		private Object shipperOriginAddress2;
		private Object shipperOriginCity;
		private Object shipperOriginState;
		private Object shipperOriginZip;
		private Object shipperDestinationAddress1;
		private Object shipperDestinationAddress2;
		private Object shipperDestinationCity;
		private Object shipperDestinationState;
		private Object shipperDestinationZip;
		private Object urgent;
		private Object vip;
		private Object transportType;
		private Object description;
		private Object length;
		private Object width;
		private Object height;
		private Object cod;
		private Object ply;
		private Object appr;
		private Object intlIspmis;
		private Object ServiceNumber;
		private Object oricod;
		private Object destinationCod;
		private Object destinationAppr;
		private Object destServiceNumber;
		private Object oriappr;
		private Object note;
		private Object phone; 
		private Object homePhone; 
		private Object originMobile; 
		private Object originFax; 
		private Object destinationPhone; 
		private Object destinationHomePhone; 
		private Object destinationMobile; 
		private Object destinationFax; 
		private Object customerContactID;
		private Object customerContactEmailAddress;
		private Object phoneTypeId; 
		
		public Object getPhoneTypeId() {
			return phoneTypeId;
		}
		public void setPhoneTypeId(Object phoneTypeId) {
			this.phoneTypeId = phoneTypeId;
		}
		public Object getAffiliateId() {
			return affiliateId;
		}
		public void setAffiliateId(Object affiliateId) {
			this.affiliateId = affiliateId;
		}
		public Object getBillToId() {
			return billToId;
		}
		public void setBillToId(Object billToId) {
			this.billToId = billToId;
		}
		public Object getPurchaseOrderNumber() {
			return purchaseOrderNumber;
		}
		public void setPurchaseOrderNumber(Object purchaseOrderNumber) {
			this.purchaseOrderNumber = purchaseOrderNumber;
		}
		public Object getEstimatedWeight() {
			return EstimatedWeight;
		}
		public void setEstimatedWeight(Object estimatedWeight) {
			EstimatedWeight = estimatedWeight;
		}
		public Object getPackDateStart() {
			return packDateStart;
		}
		public void setPackDateStart(Object packDateStart) {
			this.packDateStart = packDateStart;
		}
		public Object getDeliveryDateStart() {
			return deliveryDateStart;
		}
		public void setDeliveryDateStart(Object deliveryDateStart) {
			this.deliveryDateStart = deliveryDateStart;
		}
		public Object getOriginRequestedDateStart() {
			return originRequestedDateStart;
		}
		public void setOriginRequestedDateStart(Object originRequestedDateStart) {
			this.originRequestedDateStart = originRequestedDateStart;
		}
		public Object getDestinationRequestedDate() {
			return destinationRequestedDate;
		}
		public void setDestinationRequestedDate(Object destinationRequestedDate) {
			this.destinationRequestedDate = destinationRequestedDate;
		}
		public Object getShipperFirstName() {
			return shipperFirstName;
		}
		public void setShipperFirstName(Object shipperFirstName) {
			this.shipperFirstName = shipperFirstName;
		}
		public Object getShipperLastName() {
			return shipperLastName;
		}
		public void setShipperLastName(Object shipperLastName) {
			this.shipperLastName = shipperLastName;
		}
		public Object getShipperOriginAddress1() {
			return shipperOriginAddress1;
		}
		public void setShipperOriginAddress1(Object shipperOriginAddress1) {
			this.shipperOriginAddress1 = shipperOriginAddress1;
		}
		public Object getShipperOriginAddress2() {
			return shipperOriginAddress2;
		}
		public void setShipperOriginAddress2(Object shipperOriginAddress2) {
			this.shipperOriginAddress2 = shipperOriginAddress2;
		}
		public Object getShipperOriginCity() {
			return shipperOriginCity;
		}
		public void setShipperOriginCity(Object shipperOriginCity) {
			this.shipperOriginCity = shipperOriginCity;
		}
		public Object getShipperOriginState() {
			return shipperOriginState;
		}
		public void setShipperOriginState(Object shipperOriginState) {
			this.shipperOriginState = shipperOriginState;
		}
		public Object getShipperOriginZip() {
			return shipperOriginZip;
		}
		public void setShipperOriginZip(Object shipperOriginZip) {
			this.shipperOriginZip = shipperOriginZip;
		}
		public Object getShipperDestinationAddress1() {
			return shipperDestinationAddress1;
		}
		public void setShipperDestinationAddress1(Object shipperDestinationAddress1) {
			this.shipperDestinationAddress1 = shipperDestinationAddress1;
		}
		public Object getShipperDestinationAddress2() {
			return shipperDestinationAddress2;
		}
		public void setShipperDestinationAddress2(Object shipperDestinationAddress2) {
			this.shipperDestinationAddress2 = shipperDestinationAddress2;
		}
		public Object getShipperDestinationCity() {
			return shipperDestinationCity;
		}
		public void setShipperDestinationCity(Object shipperDestinationCity) {
			this.shipperDestinationCity = shipperDestinationCity;
		}
		public Object getShipperDestinationState() {
			return shipperDestinationState;
		}
		public void setShipperDestinationState(Object shipperDestinationState) {
			this.shipperDestinationState = shipperDestinationState;
		}
		public Object getShipperDestinationZip() {
			return shipperDestinationZip;
		}
		public void setShipperDestinationZip(Object shipperDestinationZip) {
			this.shipperDestinationZip = shipperDestinationZip;
		}
		public Object getLoadingStartDate() {
			return loadingStartDate;
		}
		public void setLoadingStartDate(Object loadingStartDate) {
			this.loadingStartDate = loadingStartDate;
		}
		public Object getTransportType() {
			return transportType;
		}
		public void setTransportType(Object transportType) {
			this.transportType = transportType;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getLength() {
			return length;
		}
		public void setLength(Object length) {
			this.length = length;
		}
		public Object getWidth() {
			return width;
		}
		public void setWidth(Object width) {
			this.width = width;
		}
		public Object getHeight() {
			return height;
		}
		public void setHeight(Object height) {
			this.height = height;
		}
		public Object getCod() {
			return cod;
		}
		public void setCod(Object cod) {
			this.cod = cod;
		}
		public Object getPly() {
			return ply;
		}
		public void setPly(Object ply) {
			this.ply = ply;
		}
		public Object getAppr() {
			return appr;
		}
		public void setAppr(Object appr) {
			this.appr = appr;
		}
		public Object getIntlIspmis() {
			return intlIspmis;
		}
		public void setIntlIspmis(Object intlIspmis) {
			this.intlIspmis = intlIspmis;
		}
		public Object getServiceNumber() {
			return ServiceNumber;
		}
		public void setServiceNumber(Object serviceNumber) {
			ServiceNumber = serviceNumber;
		}
		public Object getOricod() {
			return oricod;
		}
		public void setOricod(Object oricod) {
			this.oricod = oricod;
		}
		public Object getOriappr() {
			return oriappr;
		}
		public void setOriappr(Object oriappr) {
			this.oriappr = oriappr;
		}
		public Object getUrgent() {
			return urgent;
		}
		public void setUrgent(Object urgent) {
			this.urgent = urgent;
		}
		public Object getVip() {
			return vip;
		}
		public void setVip(Object vip) {
			this.vip = vip;
		}
		public Object getNote() {
			return note;
		}
		public void setNote(Object note) {
			this.note = note;
		}
		public Object getPhone() {
			return phone;
		}
		public void setPhone(Object phone) {
			this.phone = phone;
		}
		public Object getHomePhone() {
			return homePhone;
		}
		public void setHomePhone(Object homePhone) {
			this.homePhone = homePhone;
		}
		public Object getOriginMobile() {
			return originMobile;
		}
		public void setOriginMobile(Object originMobile) {
			this.originMobile = originMobile;
		}
		public Object getOriginFax() {
			return originFax;
		}
		public void setOriginFax(Object originFax) {
			this.originFax = originFax;
		}
		public Object getDestinationPhone() {
			return destinationPhone;
		}
		public void setDestinationPhone(Object destinationPhone) {
			this.destinationPhone = destinationPhone;
		}
		public Object getDestinationHomePhone() {
			return destinationHomePhone;
		}
		public void setDestinationHomePhone(Object destinationHomePhone) {
			this.destinationHomePhone = destinationHomePhone;
		}
		public Object getDestinationMobile() {
			return destinationMobile;
		}
		public void setDestinationMobile(Object destinationMobile) {
			this.destinationMobile = destinationMobile;
		}
		public Object getDestinationFax() {
			return destinationFax;
		}
		public void setDestinationFax(Object destinationFax) {
			this.destinationFax = destinationFax;
		}
		public Object getPackingEndDate() {
			return packingEndDate;
		}
		public void setPackingEndDate(Object packingEndDate) {
			this.packingEndDate = packingEndDate;
		}
		public Object getLoadingEndDate() {
			return loadingEndDate;
		}
		public void setLoadingEndDate(Object loadingEndDate) {
			this.loadingEndDate = loadingEndDate;
		}
		public Object getDeliveryEndDate() {
			return deliveryEndDate;
		}
		public void setDeliveryEndDate(Object deliveryEndDate) {
			this.deliveryEndDate = deliveryEndDate;
		}
		public Object getDestinationCod() {
			return destinationCod;
		}
		public void setDestinationCod(Object destinationCod) {
			this.destinationCod = destinationCod;
		}
		public Object getDestinationAppr() {
			return destinationAppr;
		}
		public void setDestinationAppr(Object destinationAppr) {
			this.destinationAppr = destinationAppr;
		}
		public Object getDestServiceNumber() {
			return destServiceNumber;
		}
		public void setDestServiceNumber(Object destServiceNumber) {
			this.destServiceNumber = destServiceNumber;
		}
		public Object getCustomerContactID() {
			return customerContactID;
		}
		public void setCustomerContactID(Object customerContactID) {
			this.customerContactID = customerContactID;
		}
		public Object getCustomerContactEmailAddress() {
			return customerContactEmailAddress;
		}
		public void setCustomerContactEmailAddress(Object customerContactEmailAddress) {
			this.customerContactEmailAddress = customerContactEmailAddress;
		}
		public Object getSoEmail() {
			return soEmail;
		}
		public void setSoEmail(Object soEmail) {
			this.soEmail = soEmail;
		}
		public Object getSoDestinationemail() {
			return soDestinationemail;
		}
		public void setSoDestinationemail(Object soDestinationemail) {
			this.soDestinationemail = soDestinationemail;
		}
	}
}

