package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import org.appfuse.model.BaseObject;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="operationsintelligence")
public class OperationsIntelligence extends BaseObject
implements Comparable<OperationsIntelligence> {
	private Long id;
	private Date date;
	private Long ticket;
	private String type;
	private String shipNumber;
	private String corpID;
	private String description;
	private BigDecimal quantitiy =  new BigDecimal(0);
	private Double esthours;
	private String comments;
	private BigDecimal estimatedquantity =  new BigDecimal(0);
	private BigDecimal revisionquantity =  new BigDecimal(0);
	private BigDecimal estimatedbuyrate= new BigDecimal(0);
	private BigDecimal revisionbuyrate= new BigDecimal(0);
	private BigDecimal estimatedexpense= new BigDecimal(0);
	private BigDecimal revisionexpense= new BigDecimal(0);
	private BigDecimal estimatedsellrate= new BigDecimal(0);
	private BigDecimal revisionsellrate= new BigDecimal(0);
	private BigDecimal estimatedrevenue= new BigDecimal(0);
	private BigDecimal revisionrevenue= new BigDecimal(0);
	private String workorder;
	private String workTicketId;
	private String scopeOfWorkOrder;
	private String itemsJbkEquipId;
	private String ticketStatus;
	private BigDecimal estimatedGrossMarginForOI = new BigDecimal(0);
	private BigDecimal estimatedTotalExpenseForOI = new BigDecimal(0);
	private BigDecimal estimatedTotalRevenueForOI = new BigDecimal(0);
	private BigDecimal revisedGrossMarginForOI = new BigDecimal(0);
	private BigDecimal revisedTotalExpenseForOI = new BigDecimal(0);
	private BigDecimal revisedTotalRevenueForOI = new BigDecimal(0);
	private BigDecimal estimatedGrossMarginPercentageForOI = new BigDecimal(0);
	private BigDecimal revisedGrossMarginPercentageForOI = new BigDecimal(0);
	private BigDecimal estmatedSalesTax = new BigDecimal(0);
	private BigDecimal revisionSalesTax = new BigDecimal(0);
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Integer displayPriority;
	private BigDecimal estmatedValuation  = new BigDecimal(0);
	private BigDecimal revisionValuation = new BigDecimal(0);
	private BigDecimal estmatedConsumables = new BigDecimal(0);
	private BigDecimal revisionConsumables = new BigDecimal(0);
	private Long serviceOrderId;
	private String numberOfComputer;
	private String numberOfEmployee;
	private String salesTax;
	private BigDecimal consumables = new BigDecimal(0);
	private Double revisionHours;
	private String revisionComment;
	private BigDecimal revisedQuantity =  new BigDecimal(0);
	private Boolean status = new Boolean(true);
	private String targetActual;
	private String revisionScopeOfWorkOrder;
	private String revisionScopeOfNumberOfComputer;
	private String revisionScopeOfNumberOfEmployee;
	private String revisionScopeOfSalesTax;
	private BigDecimal revisionScopeOfConsumables = new BigDecimal(0);
	private boolean invoiceNumber=false;
	private Boolean estimateConsumablePercentage = new Boolean(false);
	private Boolean revisionConsumablePercentage = new Boolean(false);
    private BigDecimal aB5Surcharge = new BigDecimal(0);
    private BigDecimal estmatedAB5Surcharge = new BigDecimal(0);
	private Boolean editAB5Surcharge = new Boolean(false);
    private BigDecimal revisionScopeOfAB5Surcharge = new BigDecimal(0);
    private Boolean revisionaeditAB5Surcharge = new Boolean(false);
    private BigDecimal revisionaB5Surcharge = new BigDecimal(0);
    private Long accountLineId;
    
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("date", date)
				.append("ticket", ticket)
				.append("type", type)
				.append("shipNumber", shipNumber)
				.append("corpID", corpID)
				.append("description", description)
				.append("quantitiy", quantitiy)
				.append("esthours", esthours)
				.append("comments", comments)
				.append("estimatedquantity", estimatedquantity)
				.append("revisionquantity", revisionquantity)
				.append("estimatedbuyrate", estimatedbuyrate)
				.append("revisionbuyrate", revisionbuyrate)
				.append("estimatedexpense", estimatedexpense)
				.append("revisionexpense", revisionexpense)
				.append("estimatedsellrate", estimatedsellrate)
				.append("revisionsellrate", revisionsellrate)
				.append("estimatedrevenue", estimatedrevenue)
				.append("revisionrevenue", revisionrevenue)
				.append("workorder", workorder)
				.append("workTicketId", workTicketId)
				.append("scopeOfWorkOrder", scopeOfWorkOrder)
				.append("itemsJbkEquipId", itemsJbkEquipId)
				.append("ticketStatus", ticketStatus)
				.append("estimatedGrossMarginForOI", estimatedGrossMarginForOI)
				.append("estimatedTotalExpenseForOI",
						estimatedTotalExpenseForOI)
				.append("estimatedTotalRevenueForOI",
						estimatedTotalRevenueForOI)
				.append("revisedGrossMarginForOI", revisedGrossMarginForOI)
				.append("revisedTotalExpenseForOI", revisedTotalExpenseForOI)
				.append("revisedTotalRevenueForOI", revisedTotalRevenueForOI)
				.append("estimatedGrossMarginPercentageForOI",
						estimatedGrossMarginPercentageForOI)
				.append("revisedGrossMarginPercentageForOI",
						revisedGrossMarginPercentageForOI)
				.append("estmatedSalesTax", estmatedSalesTax)
				.append("revisionSalesTax", revisionSalesTax)
				.append("createdBy",createdBy)
				.append("createdOn",createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("displayPriority",displayPriority)
				.append("estmatedValuation",estmatedValuation)
				.append("revisionValuation",revisionValuation)
				.append("estmatedConsumables",estmatedConsumables)
				.append("revisionConsumables",revisionConsumables)
				.append("serviceOrderId",serviceOrderId)
				.append("numberOfComputer",numberOfComputer)
				.append("numberOfEmployee",numberOfEmployee)
				.append("salesTax",salesTax)
				.append("consumables",consumables)
				.append("revisionHours", revisionHours)
				.append("revisionComment", revisionComment).append("revisedQuantity",revisedQuantity)
				.append("status", status)
				.append("targetActual",targetActual)
				.append("revisionScopeOfWorkOrder",revisionScopeOfWorkOrder)
				.append("revisionScopeOfNumberOfComputer",revisionScopeOfNumberOfComputer)
				.append("revisionScopeOfNumberOfEmployee",revisionScopeOfNumberOfEmployee)
				.append("revisionScopeOfSalesTax",revisionScopeOfSalesTax)
				.append("revisionScopeOfConsumables",revisionScopeOfConsumables)
				.append("estimateConsumablePercentage",estimateConsumablePercentage)
				.append("revisionConsumablePercentage",revisionConsumablePercentage)
				.append("aB5Surcharge",aB5Surcharge)
				.append("editAB5Surcharge",editAB5Surcharge)
				.append("estmatedAB5Surcharge",estmatedAB5Surcharge)
				.append("revisionaeditAB5Surcharge ",revisionaeditAB5Surcharge )
				.append("revisionaB5Surcharge ",revisionaB5Surcharge)
				.append("revisionScopeOfAB5Surcharge ",revisionScopeOfAB5Surcharge)
				.append("accountLineId",accountLineId)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof OperationsIntelligence))
			return false;
		OperationsIntelligence castOther = (OperationsIntelligence) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(date, castOther.date)
				.append(ticket, castOther.ticket)
				.append(type, castOther.type)
				.append(shipNumber, castOther.shipNumber)
				.append(corpID, castOther.corpID)
				.append(description, castOther.description)
				.append(quantitiy, castOther.quantitiy)
				.append(esthours, castOther.esthours)
				.append(comments, castOther.comments)
				.append(estimatedquantity, castOther.estimatedquantity)
				.append(revisionquantity, castOther.revisionquantity)
				.append(estimatedbuyrate, castOther.estimatedbuyrate)
				.append(revisionbuyrate, castOther.revisionbuyrate)
				.append(estimatedexpense, castOther.estimatedexpense)
				.append(revisionexpense, castOther.revisionexpense)
				.append(estimatedsellrate, castOther.estimatedsellrate)
				.append(revisionsellrate, castOther.revisionsellrate)
				.append(estimatedrevenue, castOther.estimatedrevenue)
				.append(revisionrevenue, castOther.revisionrevenue)
				.append(workorder, castOther.workorder)
				.append(workTicketId, castOther.workTicketId)
				.append(scopeOfWorkOrder, castOther.scopeOfWorkOrder)
				.append(itemsJbkEquipId, castOther.itemsJbkEquipId)
				.append(ticketStatus, castOther.ticketStatus)
				.append(estimatedGrossMarginForOI,
						castOther.estimatedGrossMarginForOI)
				.append(estimatedTotalExpenseForOI,
						castOther.estimatedTotalExpenseForOI)
				.append(estimatedTotalRevenueForOI,
						castOther.estimatedTotalRevenueForOI)
				.append(revisedGrossMarginForOI,
						castOther.revisedGrossMarginForOI)
				.append(revisedTotalExpenseForOI,
						castOther.revisedTotalExpenseForOI)
				.append(revisedTotalRevenueForOI,
						castOther.revisedTotalRevenueForOI)
				.append(estimatedGrossMarginPercentageForOI,
						castOther.estimatedGrossMarginPercentageForOI)
				.append(revisedGrossMarginPercentageForOI,
						castOther.revisedGrossMarginPercentageForOI)
				.append(estmatedSalesTax, castOther.estmatedSalesTax)
				.append(revisionSalesTax, castOther.revisionSalesTax)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(displayPriority, castOther.displayPriority)
				.append(estmatedValuation, castOther.estmatedValuation)
				.append(revisionValuation, castOther.revisionValuation)
				.append(estmatedConsumables, castOther.estmatedConsumables)
				.append(revisionConsumables, castOther.revisionConsumables)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(numberOfComputer, castOther.numberOfComputer)
				.append(numberOfEmployee, castOther.numberOfEmployee)
				.append(salesTax, castOther.salesTax)
				.append(consumables, castOther.consumables)
				.append(revisionHours, castOther.revisionHours)
				.append(revisionComment, castOther.revisionComment).append(revisedQuantity, castOther.revisedQuantity)
				.append(status, castOther.status)
				.append(targetActual, castOther.targetActual)
				.append(revisionScopeOfWorkOrder, castOther.revisionScopeOfWorkOrder)
				.append(revisionScopeOfNumberOfComputer, castOther.revisionScopeOfNumberOfComputer)
				.append(revisionScopeOfNumberOfEmployee, castOther.revisionScopeOfNumberOfEmployee)
				.append(revisionScopeOfSalesTax, castOther.revisionScopeOfSalesTax)
				.append(revisionScopeOfConsumables, castOther.revisionScopeOfConsumables)
				.append(estimateConsumablePercentage, castOther.estimateConsumablePercentage)
				.append(revisionConsumablePercentage, castOther.revisionConsumablePercentage)
				.append(aB5Surcharge, castOther.aB5Surcharge)
				.append(editAB5Surcharge, castOther.editAB5Surcharge)
				.append(estmatedAB5Surcharge, castOther.estmatedAB5Surcharge)
				.append(revisionaeditAB5Surcharge , castOther.revisionaeditAB5Surcharge )
				.append(revisionaB5Surcharge , castOther.revisionaB5Surcharge )
				.append(revisionScopeOfAB5Surcharge , castOther.revisionScopeOfAB5Surcharge )
				.append(accountLineId , castOther.accountLineId )
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(date).append(ticket)
				.append(type).append(shipNumber).append(corpID)
				.append(description).append(quantitiy).append(esthours)
				.append(comments).append(estimatedquantity)
				.append(revisionquantity).append(estimatedbuyrate)
				.append(revisionbuyrate).append(estimatedexpense)
				.append(revisionexpense).append(estimatedsellrate)
				.append(revisionsellrate).append(estimatedrevenue)
				.append(revisionrevenue).append(workorder).append(workTicketId)
				.append(scopeOfWorkOrder).append(itemsJbkEquipId)
				.append(ticketStatus).append(estimatedGrossMarginForOI)
				.append(estimatedTotalExpenseForOI)
				.append(estimatedTotalRevenueForOI)
				.append(revisedGrossMarginForOI)
				.append(revisedTotalExpenseForOI)
				.append(revisedTotalRevenueForOI)
				.append(estimatedGrossMarginPercentageForOI)
				.append(revisedGrossMarginPercentageForOI)
				.append(estmatedSalesTax).append(revisionSalesTax)
				.append(createdBy)
				.append(createdOn)
				.append(updatedBy)
				.append(updatedOn).append(displayPriority)
				.append(estmatedValuation)
				.append(revisionValuation)
				.append(estmatedConsumables)
				.append(revisionConsumables)
				.append(serviceOrderId)
				.append(numberOfComputer)
				.append(numberOfEmployee)
				.append(salesTax)
				.append(consumables)
				.append(revisionHours).append(revisionComment).append(revisedQuantity)
				.append(status)
				.append(targetActual)
				.append(revisionScopeOfWorkOrder).append(revisionScopeOfNumberOfComputer).append(revisionScopeOfNumberOfEmployee)
				.append(revisionScopeOfSalesTax).append(revisionScopeOfConsumables)
				.append(estimateConsumablePercentage).append(revisionConsumablePercentage).append(aB5Surcharge).append(editAB5Surcharge).append(estmatedAB5Surcharge)
				.append(revisionaeditAB5Surcharge )
				.append(revisionaB5Surcharge )
				.append(revisionScopeOfAB5Surcharge )
				.append(accountLineId) 
				.toHashCode();
	}
	public int compareTo(final OperationsIntelligence other) {
		return new CompareToBuilder().append(id, other.id)
				.append(date, other.date).append(ticket, other.ticket)
				.append(type, other.type).append(shipNumber, other.shipNumber)
				.append(corpID, other.corpID)
				.append(description, other.description)
				.append(quantitiy, other.quantitiy)
				.append(esthours, other.esthours)
				.append(comments, other.comments)
				.append(estimatedquantity, other.estimatedquantity)
				.append(revisionquantity, other.revisionquantity)
				.append(estimatedbuyrate, other.estimatedbuyrate)
				.append(revisionbuyrate, other.revisionbuyrate)
				.append(estimatedexpense, other.estimatedexpense)
				.append(revisionexpense, other.revisionexpense)
				.append(estimatedsellrate, other.estimatedsellrate)
				.append(revisionsellrate, other.revisionsellrate)
				.append(estimatedrevenue, other.estimatedrevenue)
				.append(revisionrevenue, other.revisionrevenue)
				.append(workorder, other.workorder)
				.append(workTicketId, other.workTicketId)
				.append(scopeOfWorkOrder, other.scopeOfWorkOrder)
				.append(createdBy, other.createdBy)
				.append(createdOn, other.createdOn)
				.append(updatedBy, other.updatedBy)
				.append(updatedOn, other.updatedOn).append(displayPriority, other.displayPriority)
				.append(estmatedValuation, other.estmatedValuation)
				.append(revisionValuation, other.revisionValuation)
				.append(estmatedConsumables, other.estmatedConsumables)
				.append(revisionConsumables, other.revisionConsumables)
				.append(serviceOrderId, other.serviceOrderId)
				.append(numberOfComputer, other.numberOfComputer)
				.append(numberOfEmployee, other.numberOfEmployee)
				.append(salesTax, other.salesTax)
				.append(consumables, other.consumables)
				.append(revisionHours, other.revisionHours)
				.append(revisionComment, other.revisionComment).append(revisedQuantity, other.revisedQuantity)
				.append(status, other.status)
				.append(targetActual, other.targetActual)
				.append(revisionScopeOfWorkOrder, other.revisionScopeOfWorkOrder)
				.append(revisionScopeOfNumberOfComputer, other.revisionScopeOfNumberOfComputer)
				.append(revisionScopeOfNumberOfEmployee, other.revisionScopeOfNumberOfEmployee)
				.append(revisionScopeOfSalesTax, other.revisionScopeOfSalesTax)
				.append(revisionScopeOfConsumables, other.revisionScopeOfConsumables)
				.append(estimateConsumablePercentage, other.estimateConsumablePercentage)
				.append(revisionConsumablePercentage, other.revisionConsumablePercentage)
				.append(aB5Surcharge, other.aB5Surcharge)
				.append(editAB5Surcharge, other.editAB5Surcharge).append(estmatedAB5Surcharge, other.estmatedAB5Surcharge)
				.toComparison();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public BigDecimal getQuantitiy() {
		return quantitiy;
	}
	public void setQuantitiy(BigDecimal quantitiy) {
		this.quantitiy = quantitiy;
	}
	@Column
	public Double getEsthours() {
		return esthours;
	}
	public void setEsthours(Double esthours) {
		this.esthours = esthours;
	}
	@Column
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Column
	public BigDecimal getEstimatedquantity() {
		return estimatedquantity;
	}
	public void setEstimatedquantity(BigDecimal estimatedquantity) {
		this.estimatedquantity = estimatedquantity;
	}
	@Column
	public BigDecimal getRevisionquantity() {
		return revisionquantity;
	}
	public void setRevisionquantity(BigDecimal revisionquantity) {
		this.revisionquantity = revisionquantity;
	}
	@Column
	public BigDecimal getEstimatedbuyrate() {
		return estimatedbuyrate;
	}
	public void setEstimatedbuyrate(BigDecimal estimatedbuyrate) {
		this.estimatedbuyrate = estimatedbuyrate;
	}
	@Column
	public BigDecimal getRevisionbuyrate() {
		return revisionbuyrate;
	}
	public void setRevisionbuyrate(BigDecimal revisionbuyrate) {
		this.revisionbuyrate = revisionbuyrate;
	}
	@Column
	public BigDecimal getEstimatedexpense() {
		return estimatedexpense;
	}
	public void setEstimatedexpense(BigDecimal estimatedexpense) {
		this.estimatedexpense = estimatedexpense;
	}
	@Column
	public BigDecimal getRevisionexpense() {
		return revisionexpense;
	}
	public void setRevisionexpense(BigDecimal revisionexpense) {
		this.revisionexpense = revisionexpense;
	}
	@Column
	public BigDecimal getEstimatedsellrate() {
		return estimatedsellrate;
	}
	public void setEstimatedsellrate(BigDecimal estimatedsellrate) {
		this.estimatedsellrate = estimatedsellrate;
	}
	@Column
	public BigDecimal getRevisionsellrate() {
		return revisionsellrate;
	}
	public void setRevisionsellrate(BigDecimal revisionsellrate) {
		this.revisionsellrate = revisionsellrate;
	}
	@Column
	public BigDecimal getEstimatedrevenue() {
		return estimatedrevenue;
	}
	public void setEstimatedrevenue(BigDecimal estimatedrevenue) {
		this.estimatedrevenue = estimatedrevenue;
	}
	@Column
	public BigDecimal getRevisionrevenue() {
		return revisionrevenue;
	}
	public void setRevisionrevenue(BigDecimal revisionrevenue) {
		this.revisionrevenue = revisionrevenue;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public void save(OperationsIntelligence operationsIntelligence) {
		// TODO Auto-generated method stub
		
	}
	@Column
	public String getWorkorder() {
		return workorder;
	}
	public void setWorkorder(String workorder) {
		this.workorder = workorder;
	}
	@Column
	public String getWorkTicketId() {
		return workTicketId;
	}
	public void setWorkTicketId(String workTicketId) {
		this.workTicketId = workTicketId;
	}
	@Column
	public String getScopeOfWorkOrder() {
		return scopeOfWorkOrder;
	}
	public void setScopeOfWorkOrder(String scopeOfWorkOrder) {
		this.scopeOfWorkOrder = scopeOfWorkOrder;
	}
	@Column
	public String getItemsJbkEquipId() {
		return itemsJbkEquipId;
	}
	public void setItemsJbkEquipId(String itemsJbkEquipId) {
		this.itemsJbkEquipId = itemsJbkEquipId;
	}
	@Column
	public BigDecimal getEstimatedGrossMarginForOI() {
		return estimatedGrossMarginForOI;
	}
	public void setEstimatedGrossMarginForOI(BigDecimal estimatedGrossMarginForOI) {
		this.estimatedGrossMarginForOI = estimatedGrossMarginForOI;
	}
	@Column
	public BigDecimal getEstimatedTotalExpenseForOI() {
		return estimatedTotalExpenseForOI;
	}
	public void setEstimatedTotalExpenseForOI(BigDecimal estimatedTotalExpenseForOI) {
		this.estimatedTotalExpenseForOI = estimatedTotalExpenseForOI;
	}
	@Column
	public BigDecimal getEstimatedTotalRevenueForOI() {
		return estimatedTotalRevenueForOI;
	}
	public void setEstimatedTotalRevenueForOI(BigDecimal estimatedTotalRevenueForOI) {
		this.estimatedTotalRevenueForOI = estimatedTotalRevenueForOI;
	}
	@Column
	public BigDecimal getRevisedGrossMarginForOI() {
		return revisedGrossMarginForOI;
	}
	public void setRevisedGrossMarginForOI(BigDecimal revisedGrossMarginForOI) {
		this.revisedGrossMarginForOI = revisedGrossMarginForOI;
	}
	@Column
	public BigDecimal getRevisedTotalExpenseForOI() {
		return revisedTotalExpenseForOI;
	}
	public void setRevisedTotalExpenseForOI(BigDecimal revisedTotalExpenseForOI) {
		this.revisedTotalExpenseForOI = revisedTotalExpenseForOI;
	}
	@Column
	public BigDecimal getRevisedTotalRevenueForOI() {
		return revisedTotalRevenueForOI;
	}
	public void setRevisedTotalRevenueForOI(BigDecimal revisedTotalRevenueForOI) {
		this.revisedTotalRevenueForOI = revisedTotalRevenueForOI;
	}
	@Column
	public BigDecimal getEstimatedGrossMarginPercentageForOI() {
		return estimatedGrossMarginPercentageForOI;
	}
	public void setEstimatedGrossMarginPercentageForOI(
			BigDecimal estimatedGrossMarginPercentageForOI) {
		this.estimatedGrossMarginPercentageForOI = estimatedGrossMarginPercentageForOI;
	}
	@Column
	public BigDecimal getRevisedGrossMarginPercentageForOI() {
		return revisedGrossMarginPercentageForOI;
	}
	public void setRevisedGrossMarginPercentageForOI(
			BigDecimal revisedGrossMarginPercentageForOI) {
		this.revisedGrossMarginPercentageForOI = revisedGrossMarginPercentageForOI;
	}
	@Column
	public String getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	@Column
	public BigDecimal getEstmatedSalesTax() {
		return estmatedSalesTax;
	}
	public void setEstmatedSalesTax(BigDecimal estmatedSalesTax) {
		this.estmatedSalesTax = estmatedSalesTax;
	}
	@Column
	public BigDecimal getRevisionSalesTax() {
		return revisionSalesTax;
	}
	public void setRevisionSalesTax(BigDecimal revisionSalesTax) {
		this.revisionSalesTax = revisionSalesTax;
	}
	
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Integer getDisplayPriority() {
		return displayPriority;
	}
	public void setDisplayPriority(Integer displayPriority) {
		this.displayPriority = displayPriority;
	}
	@Column
	public BigDecimal getEstmatedValuation() {
		return estmatedValuation;
	}
	public void setEstmatedValuation(BigDecimal estmatedValuation) {
		this.estmatedValuation = estmatedValuation;
	}
	@Column
	public BigDecimal getRevisionValuation() {
		return revisionValuation;
	}
	public void setRevisionValuation(BigDecimal revisionValuation) {
		this.revisionValuation = revisionValuation;
	}
	@Column
	public BigDecimal getEstmatedConsumables() {
		return estmatedConsumables;
	}
	public void setEstmatedConsumables(BigDecimal estmatedConsumables) {
		this.estmatedConsumables = estmatedConsumables;
	}
	@Column
	public BigDecimal getRevisionConsumables() {
		return revisionConsumables;
	}
	public void setRevisionConsumables(BigDecimal revisionConsumables) {
		this.revisionConsumables = revisionConsumables;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getNumberOfComputer() {
		return numberOfComputer;
	}
	public void setNumberOfComputer(String numberOfComputer) {
		this.numberOfComputer = numberOfComputer;
	}
	@Column
	public String getNumberOfEmployee() {
		return numberOfEmployee;
	}
	public void setNumberOfEmployee(String numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}
	@Column
	public String getSalesTax() {
		return salesTax;
	}
	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}
	@Column
	public BigDecimal getConsumables() {
		return consumables;
	}
	public void setConsumables(BigDecimal consumables) {
		this.consumables = consumables;
	}
	public Double getRevisionHours() {
		return revisionHours;
	}
	public void setRevisionHours(Double revisionHours) {
		this.revisionHours = revisionHours;
	}
	public String getRevisionComment() {
		return revisionComment;
	}
	public void setRevisionComment(String revisionComment) {
		this.revisionComment = revisionComment;
	}
	@Column
	public BigDecimal getRevisedQuantity() {
		return revisedQuantity;
	}
	public void setRevisedQuantity(BigDecimal revisedQuantity) {
		this.revisedQuantity = revisedQuantity;
	}
	public String getTargetActual() {
		return targetActual;
	}
	public void setTargetActual(String targetActual) {
		this.targetActual = targetActual;
	}
	
	@Column
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getRevisionScopeOfWorkOrder() {
		return revisionScopeOfWorkOrder;
	}
	public void setRevisionScopeOfWorkOrder(String revisionScopeOfWorkOrder) {
		this.revisionScopeOfWorkOrder = revisionScopeOfWorkOrder;
	}
	public String getRevisionScopeOfNumberOfComputer() {
		return revisionScopeOfNumberOfComputer;
	}
	public void setRevisionScopeOfNumberOfComputer(
			String revisionScopeOfNumberOfComputer) {
		this.revisionScopeOfNumberOfComputer = revisionScopeOfNumberOfComputer;
	}
	public String getRevisionScopeOfNumberOfEmployee() {
		return revisionScopeOfNumberOfEmployee;
	}
	public void setRevisionScopeOfNumberOfEmployee(
			String revisionScopeOfNumberOfEmployee) {
		this.revisionScopeOfNumberOfEmployee = revisionScopeOfNumberOfEmployee;
	}
	public String getRevisionScopeOfSalesTax() {
		return revisionScopeOfSalesTax;
	}
	public void setRevisionScopeOfSalesTax(String revisionScopeOfSalesTax) {
		this.revisionScopeOfSalesTax = revisionScopeOfSalesTax;
	}
	public BigDecimal getRevisionScopeOfConsumables() {
		return revisionScopeOfConsumables;
	}
	public void setRevisionScopeOfConsumables(BigDecimal revisionScopeOfConsumables) {
		this.revisionScopeOfConsumables = revisionScopeOfConsumables;
	}
	@Transient
	public boolean isInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(boolean invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Column
	public Boolean getEstimateConsumablePercentage() {
		return estimateConsumablePercentage;
	}
	public void setEstimateConsumablePercentage(Boolean estimateConsumablePercentage) {
		this.estimateConsumablePercentage = estimateConsumablePercentage;
	}
	@Column
	public Boolean getRevisionConsumablePercentage() {
		return revisionConsumablePercentage;
	}
	public void setRevisionConsumablePercentage(Boolean revisionConsumablePercentage) {
		this.revisionConsumablePercentage = revisionConsumablePercentage;
	}
	
	@Column
	public BigDecimal getaB5Surcharge() {
		return aB5Surcharge;
	}
	public void setaB5Surcharge(BigDecimal aB5Surcharge) {
		this.aB5Surcharge = aB5Surcharge;
	}
	@Column
	public Boolean getEditAB5Surcharge() {
		return editAB5Surcharge;
	}
	public void setEditAB5Surcharge(Boolean editAB5Surcharge) {
		this.editAB5Surcharge = editAB5Surcharge;
	}
	@Column
	public BigDecimal getEstmatedAB5Surcharge() {
		return estmatedAB5Surcharge;
	}
	public void setEstmatedAB5Surcharge(BigDecimal estmatedAB5Surcharge) {
		this.estmatedAB5Surcharge = estmatedAB5Surcharge;
	}
	@Column
	public BigDecimal getRevisionaB5Surcharge() {
		return revisionaB5Surcharge;
	}
	public void setRevisionaB5Surcharge(BigDecimal revisionaB5Surcharge) {
		this.revisionaB5Surcharge = revisionaB5Surcharge;
	}
	@Column
	public Boolean getRevisionaeditAB5Surcharge() {
		return revisionaeditAB5Surcharge;
	}
	public void setRevisionaeditAB5Surcharge(Boolean revisionaeditAB5Surcharge) {
		this.revisionaeditAB5Surcharge = revisionaeditAB5Surcharge;
	}
	@Column
	public BigDecimal getRevisionScopeOfAB5Surcharge() {
		return revisionScopeOfAB5Surcharge;
	}
	public void setRevisionScopeOfAB5Surcharge(BigDecimal revisionScopeOfAB5Surcharge) {
		this.revisionScopeOfAB5Surcharge = revisionScopeOfAB5Surcharge;
	}
	
	@Column
	public Long getAccountLineId() {
		return accountLineId;
	}
	public void setAccountLineId(Long accountLineId) {
		this.accountLineId = accountLineId;
	}

		
	
}