<%@ include file="/common/taglibs.jsp"%> 
<%@page import="java.math.BigDecimal"%>

<script type="text/javascript">
function feedback(targetElement){
	var year = targetElement; 
	window.open('getAgentRatingFeedback.html?partnerCode=${partnerCode}&year='+year+'&decorator=popup&popup=true','','height=500,width=850,top=100, left=80, scrollbars=yes,resizable=no') ;
}

function changeimage(url,year){
	var imageelement = null;
	imageelement = window.document.getElementById("image_"+year);
	imageelement.src = url;
}
</script>

<c:set var="partnerCode" value="<%= request.getParameter("partnerCode")%>" />
<s:hidden name="partnerCode" value="<%= request.getParameter("partnerCode")%>" />
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="margin: 0px; padding: 0px;">
	<tr style="margin: 0px; padding: 0px;">
		<td colspan="5" style="font-family: arial,verdana; font-size: 14px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>One Year Rating : ${agentOneYrAvg}</b></td>
	</tr>
	<tr style="margin: 0px; padding: 0px; height: 5px;">
		<td colspan="5"></td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em; " >
		<td width="70px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Period</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Positive</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Negative</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Feedback</b></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Rating</b></td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">Six Months</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentSixMnthPos}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentSixMnthNeg}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_${agentSixMnthPos}" width="17" height="20" onclick="feedback(183)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>" alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentSixMnthAvg}</td>
	
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">One Year</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentOneYrPos}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentOneYrNeg}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_${agentOneYrPos}" width="17" height="20" onclick="feedback(365)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>"  alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentOneYrAvg}</td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">Two Years</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentTwoYrPos}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentTwoYrNeg}</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_${agentTwoYrPos}" width="17" height="20" onclick="feedback(730)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>"  alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${agentTwoYrAvg}</td>
	</tr>	
</table>

