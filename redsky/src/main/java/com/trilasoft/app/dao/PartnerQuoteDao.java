

package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   
import com.trilasoft.app.model.PartnerQuote;   
import java.math.BigDecimal;  
import java.util.List;   
  
public interface PartnerQuoteDao extends GenericDao<PartnerQuote, Long> {   
	   
	public List findMaximumId();
	public int updateQuoteStatus(String mail, String reqSO );
	public int updateQuoteReminder(String mail );
	public List findPartnerCode(String partnerCode);
	
	public List <PartnerQuote> findByStatus(String quoteStatus,String sessionCorpID, String vendorCodeSet);
	public List findByVendorEmail(String email, String seqNum);
	
//************* For getting ServiceOrders******************
	
	public List findServiceOrders(String requestedSO,String sessionCorpID);
	
//	************* For getting ServiceOrders******************
// 	@@@@@@@@@@@@@@@@@@@@@ By Madhu - For Quation Management @@@@@@@@@@@@@@@@@@@@@@@@
	public List findByQuoteType(String quoteType, String sequenceNumber);
	public List findBySequenceNumber(String sequenceNumber);
// 	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Raj@@@@@@@@@@@@@@@@@@@
	public List <PartnerQuote> findByStatusSubmitted(String quoteStatus,String searchByCode,String searchByName,String shipNumber);
	public List getGeneralListVendor(String searchByCode,String searchByName,String searchByCountryCode,String searchByState,String searchByCity);
	public List vendorNamenEmail(String partnerCode);
	public List findSoList(String sequenceNumber, String corpId);
	public List findSoDetail(String shipNumber, String corpId);
}

