package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.activation.MimetypesFileTypeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PolicyDocument;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PolicyDocumentManager;

public class PolicyDocumentAction extends BaseAction implements Preparable{
	private String sessionCorpID;
	private PolicyDocument policyDocument;
	private PolicyDocumentManager policyDocumentManager;
	private Long fid;
	private File file;
	private Long id;
	private String hitFlag;
	private String fileNotExists;
	private String documentName;
	private String fileFileName;
	private MimetypesFileTypeMap mimetypesFileTypeMap;
	private PartnerPublicManager partnerPublicManager;
	private String partnerCode;
	
	public PolicyDocumentAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		mimetypesFileTypeMap = new MimetypesFileTypeMap();
		mimetypesFileTypeMap.addMimeTypes("text/plain txt text TXT");
		mimetypesFileTypeMap.addMimeTypes("application/msword doc");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-powerpoint ppt");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-excel xls xlsx");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-outlook msg");
		mimetypesFileTypeMap.addMimeTypes("application/pdf pdf");
		mimetypesFileTypeMap.addMimeTypes("image/jpeg jpg jpeg");
		mimetypesFileTypeMap.addMimeTypes("image/gif gif");
	}
	
	
	public void prepare() throws Exception {

}
	public String getComboList(String corpID){

	 	 return SUCCESS;
	}
	
	@SkipValidation
	public String saveuploadPolicy()throws Exception {
		PartnerPublic partnerPublicUpload = partnerPublicManager.getPartnerByCode(partnerCode);
		boolean isNew = (policyDocument.getId() == null);
		if(isNew){
			 //policyDocument=new PolicyDocument();
			if (file == null || file.length() > 10485760) {
				fileNotExists = "File cannot be uploaded as size of the file exceeded the maximum allowed 5MB";
				return INPUT;
			}	
			if (!file.exists()) {
				fileNotExists = "File cannot be uploaded as the file is invalid";
				return INPUT;

			}
			int i=policyDocumentManager.checkPolicyDocumentName(policyDocument.getDocumentName(),policyDocument.getSectionName(),policyDocument.getLanguage(),policyDocument.getPartnerCode(),sessionCorpID);
			if(i>0)			{
				fileNotExists = "This File Already Exists.";
				hitFlag="0";
				errorMessage("This File Already Exists.");
				return INPUT;
			}
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");	
			File dirPath = new File(uploadDir);
			if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			Long kid;
			if (policyDocumentManager.getMaxId().get(0) == null) {
				kid = 1L;
			} else {
				kid = Long.parseLong(policyDocumentManager.getMaxId().get(0).toString());
				kid = kid + 1;
			}
			InputStream stream = new FileInputStream(file);
			//write the file to the file specified
			OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			InputStream copyStream = new FileInputStream(file);

			OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
			int bytesReadCopy = 0;
			byte[] bufferCopy = new byte[8192];

			while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
				boscopy.write(bufferCopy, 0, bytesReadCopy);
			}

			boscopy.close();
			copyStream.close();
			getRequest().setAttribute("fileLocation", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
			policyDocument.setFile(file);
			FileSizeUtil fileSizeUtil = new FileSizeUtil();
			policyDocument.setFileSize(fileSizeUtil.FindFileSize(file));
			policyDocument.setFileName(fileFileName);
			policyDocument.setContentFileType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName));
			policyDocument.setFileLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
			if(partnerPublicUpload.getPartnerType()!=null && (!(partnerPublicUpload.getPartnerType().toString().trim().equals("")))  && partnerPublicUpload.getStatus().equalsIgnoreCase("Approved") && (partnerPublicUpload.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicUpload.getPartnerType().equalsIgnoreCase("DMM"))){
				policyDocument.setCorpID(partnerPublicUpload.getCorpID());
	       	}
	       	else{
	       		policyDocument.setCorpID(sessionCorpID);
	       	}
			policyDocument.setUpdatedOn(new Date());
			policyDocument.setUpdatedBy(getRequest().getRemoteUser());
			policyDocument.setCreatedOn(new Date());
			policyDocument.setCreatedBy(getRequest().getRemoteUser());
			policyDocumentManager.save(policyDocument);
			String key = (isNew) ? "Policy document has been added successfully." : "Policy document has been updated.";
			saveMessage(getText(key));
			
		}if(!isNew){
			if(partnerPublicUpload.getPartnerType()!=null && (!(partnerPublicUpload.getPartnerType().toString().trim().equals("")))  && partnerPublicUpload.getStatus().equalsIgnoreCase("Approved") && (partnerPublicUpload.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicUpload.getPartnerType().equalsIgnoreCase("DMM"))){
				policyDocument.setCorpID(partnerPublicUpload.getCorpID());
	       	}
			policyDocument.setUpdatedOn(new Date());
			policyDocument.setUpdatedBy(getRequest().getRemoteUser());
			policyDocument.setCreatedOn(new Date());
			policyDocument.setCreatedBy(getRequest().getRemoteUser());
			policyDocument.setDocumentName(policyDocument.getDocumentName());
			policyDocument.setDescription(policyDocument.getDescription());
			policyDocumentManager.save(policyDocument);
		}
	
		hitFlag="1";
		return SUCCESS;
	}
	@SkipValidation
	public String saveuploadAdminPolicy()throws Exception {
		//PartnerPublic partnerPublicUpload = partnerPublicManager.getPartnerByCode(partnerCode);
		boolean isNew = (policyDocument.getId() == null);
		if(isNew){
			 //policyDocument=new PolicyDocument();
			if (file == null || file.length() > 10485760) {
				fileNotExists = "File cannot be uploaded as size of the file exceeded the maximum allowed 5MB";
				return INPUT;
			}	
			if (!file.exists()) {
				fileNotExists = "File cannot be uploaded as the file is invalid";
				return INPUT;

			}
			int i=policyDocumentManager.checkPolicyDocumentName(policyDocument.getDocumentName(),policyDocument.getSectionName(),policyDocument.getLanguage(),"",sessionCorpID);
			if(i>0)			{
				fileNotExists = "This File Already Exists.";
				hitFlag="0";
				errorMessage("This File Already Exists.");
				return INPUT;
			}
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");	
			File dirPath = new File(uploadDir);
			if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			Long kid;
			if (policyDocumentManager.getMaxId().get(0) == null) {
				kid = 1L;
			} else {
				kid = Long.parseLong(policyDocumentManager.getMaxId().get(0).toString());
				kid = kid + 1;
			}
			InputStream stream = new FileInputStream(file);
			//write the file to the file specified
			OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			InputStream copyStream = new FileInputStream(file);

			OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
			int bytesReadCopy = 0;
			byte[] bufferCopy = new byte[8192];

			while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
				boscopy.write(bufferCopy, 0, bytesReadCopy);
			}

			boscopy.close();
			copyStream.close();
			getRequest().setAttribute("fileLocation", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
			policyDocument.setFile(file);
			FileSizeUtil fileSizeUtil = new FileSizeUtil();
			policyDocument.setFileSize(fileSizeUtil.FindFileSize(file));
			policyDocument.setFileName(fileFileName);
			policyDocument.setContentFileType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName));
			policyDocument.setFileLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
		
	        policyDocument.setCorpID(sessionCorpID);
	     
			policyDocument.setUpdatedOn(new Date());
			policyDocument.setUpdatedBy(getRequest().getRemoteUser());
			policyDocument.setCreatedOn(new Date());
			policyDocument.setCreatedBy(getRequest().getRemoteUser());
			policyDocumentManager.save(policyDocument);
			String key = (isNew) ? "Policy document has been added successfully." : "Policy document has been updated.";
			saveMessage(getText(key));
			
		}if(!isNew){
			 policyDocument.setCorpID(sessionCorpID);
			policyDocument.setUpdatedOn(new Date());
			policyDocument.setUpdatedBy(getRequest().getRemoteUser());
			policyDocument.setCreatedOn(new Date());
			policyDocument.setCreatedBy(getRequest().getRemoteUser());
			policyDocument.setDocumentName(policyDocument.getDocumentName());
			policyDocument.setDescription(policyDocument.getDescription());
			policyDocumentManager.save(policyDocument);
		}
	
		hitFlag="1";
		return SUCCESS;
	}
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public PolicyDocument getPolicyDocument() {
		return policyDocument;
	}


	public void setPolicyDocument(PolicyDocument policyDocument) {
		this.policyDocument = policyDocument;
	}


	public void setPolicyDocumentManager(PolicyDocumentManager policyDocumentManager) {
		this.policyDocumentManager = policyDocumentManager;
	}


	public Long getFid() {
		return fid;
	}


	public void setFid(Long fid) {
		this.fid = fid;
	}


	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}


	public String getFileNotExists() {
		return fileNotExists;
	}


	public void setFileNotExists(String fileNotExists) {
		this.fileNotExists = fileNotExists;
	}


	public String getDocumentName() {
		return documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getHitFlag() {
		return hitFlag;
	}


	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}


	public String getFileFileName() {
		return fileFileName;
	}


	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}


	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}


	public String getPartnerCode() {
		return partnerCode;
	}


	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	
}
