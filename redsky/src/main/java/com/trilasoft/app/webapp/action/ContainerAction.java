/**
 * @Class Name  Container Action
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.BrokerDetails;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InlandAgent;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.BrokerDetailsManager;
import com.trilasoft.app.service.BrokerExamDetailsManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.InlandAgentManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;

public class ContainerAction extends BaseAction implements Preparable{   
   
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private CustomerFile customerFile;
	private Container container;
	private Carton carton;
	private ServiceOrder serviceOrder;
	private Company company;
	private CompanyManager companyManager;
    private ContainerManager containerManager;
    private CartonManager cartonManager;
    private NotesManager notesManager;
    private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager; 
	private MiscellaneousManager miscellaneousManager;
	private CustomerFileManager customerFileManager;
    private RefMasterManager refMasterManager;
    private ToDoRuleManager toDoRuleManager;
    private  IntegrationLogInfoManager  integrationLogInfoManager;
    private List refreshWeights =new ArrayList();
    private int refreshContainer ;
    private List containers;
    private List auditContainers;
    private  List weightunits;
    private  List volumeunits;
    private  Map<String, String> EQUIP;
    private String hitFlag="";
    private Long autoId;
    private Long sid;
    private Long id;
    private Long soId;
    private Long IdMax;
    private String gotoPageString;
    private String validateFormNav;
    private String idNumber;
    private String sessionCorpID;
    private String countContainerNotes;
    private String weightType;
    private ExtendedPaginatedList forwardingExt;
    private PaginateListFactory paginateListFactory;
    private List forwardingList;
    private String containerNo;
    private String bookingNo;
    private String lastName;
    private String vesselFilight;
    private String caed;
    private String blNumber;
    private Date sailDate;
    private String shipNum;
	private String packingMode;
	private String containerNos;
	
	private String shipSize;
	private List customerSO;
	private String minShip;
	private String countShip;
	private String maxChild;
	private String countChild;
	private String minChild;
	private Long soIdNum;
	private Long sidNum;
	private long tempcontai;
	private List contaiSO;
	private String brokerCode;
	private SystemDefault systemDefault;
	private String voxmeIntergartionFlag;
	private boolean futureDateFlag=false;
	private Vehicle vehicle;
	private ServicePartner servicePartner;
	private List customs;
	private String containerStatus;
	private List inlandAgentList;
	private InlandAgentManager inlandAgentManager;
	private InlandAgent inlandAgent;
	private BrokerDetails brokerDetails;
	private BrokerDetailsManager brokerDetailsManager;
	private List brokerDetailsList;
	private BrokerExamDetailsManager  brokerExamDetailsManager;
	private  Map<String, String> refExamPay;
	private Set linkedshipNumber=new HashSet();;
	
	private CompanyDivisionManager companyDivisionManager;
	private String dashBoardHideJobsList;
	private String oiJobList;
//  A Method to Authenticate User, calling from main menu.
    
    public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public List getCustomerSO() {
		return customerSO;
	}
	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(ContainerAction.class);

	private String  usertype;
	private Boolean surveyTab = new Boolean(false);

	public ContainerAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
	}
//  End of Method.
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){ 
			getComboList(sessionCorpID);
		}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }

        if (company != null && company.getDashBoardHideJobs() != null) {
       					dashBoardHideJobsList = company.getDashBoardHideJobs();	 
       				}
		if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		if(company.getAllowFutureActualDateEntry()!=null){
			futureDateFlag = company.getAllowFutureActualDateEntry();
		}
		
		}
		try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
				 Long soId = null;
				 try {
					 soId = new Long(getRequest().getParameter("sid").toString());
					} catch (Exception e) {
						soId = new Long(getRequest().getParameter("id").toString());
						e.printStackTrace();
					}
			 if(soId!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(soId);
				 String corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(soId);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
//  Method used to save through tabs.
    
    public String saveOnTabChange() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	validateFormNav = "OK";
            String s = save();
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return gotoPageString;
    }
    
//End of Method.
    private String msgClicked;
    private List cartonTypeValue;
    private static List containerNumberListVehicle;
    private List containerNumberListCarton;
    private List containerNumberListRouting;
    private VehicleManager vehicleManager;
    private List servicePartnerss;
    private ServicePartnerManager servicePartnerManager;
    private List vehicles;
    private List cartons;
	private Billing billing;
	private BillingManager billingManager;
//  A Method to get the list.
    private List<SystemDefault> sysDefaultDetail;
    private  Boolean UnitType;
    private  Boolean VolumeType;
	private CustomManager customManager;
	private String countBondedGoods;
	private CustomerFileAction customerFileAction;
	private List inlandAgentsList;
	private List containerNumberListInlandAgent;
	private List brokerExamDetailsList;
    public String containerList() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	serviceOrder = serviceOrderManager.get(id);
    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    	trackingStatus=trackingStatusManager.get(id);
    	miscellaneous = miscellaneousManager.get(id);
    	billing = billingManager.get(id);
    	
    	try{
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> miscellaneousRecords=findMiscellaneousRecords(linkedShipNumber,serviceOrder);
	    		synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
	    	}
            Long StartTime = System.currentTimeMillis();
    		/*if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
    			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
    			if(agentShipNumber!=null && !agentShipNumber.equals(""))
    				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
       			
       			}
				
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
       
       	}catch(Exception ex){
    		System.out.println("\n\n\n\n\n checkRemoveLinks exception");
    		ex.printStackTrace();
    	}
    	trackingStatus=trackingStatusManager.get(id); 
    	customerFile = serviceOrder.getCustomerFile();
    	sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getWeightUnit().equals("Lbs")){
					UnitType=true;
				}else{
					UnitType=false;
				}
				if(systemDefault.getVolumeUnit().equals("Cft")){
					VolumeType=true;
				}else{
					VolumeType=false;
				}
			}
		}
    	container = new Container();
    	if(containerManager.getGross(serviceOrder.getShipNumber()).get(0)== null || containerManager.getNet(serviceOrder.getShipNumber()).get(0)== null ||containerManager.getVolume(serviceOrder.getShipNumber()).get(0) == null || containerManager.getPieces(serviceOrder.getShipNumber()).get(0)==null){
    		containers = containerManager.containerList(serviceOrder.getId());
            getSession().setAttribute("containers", containers);
    	} 
    	else{
    		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
    			for (SystemDefault systemDefault : sysDefaultDetail) {
    				if(systemDefault.getWeightUnit().equals("Lbs")){
    					UnitType=true;
    					container.setGrossWeightTotal(new BigDecimal((containerManager.getGross(serviceOrder.getShipNumber())).get(0).toString()));
    					container.setTotalNetWeight(new BigDecimal((containerManager.getNet(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					UnitType=false;
    					container.setGrossWeightTotal(new BigDecimal((containerManager.getGrossKilo(serviceOrder.getShipNumber())).get(0).toString()));
    			    	container.setTotalNetWeight(new BigDecimal((containerManager.getNetKilo(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    				if(systemDefault.getVolumeUnit().equals("Cft")){
    					VolumeType=true;
    					container.setTotalVolume(new BigDecimal((containerManager.getVolume(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					VolumeType=false;
    					container.setTotalVolume(new BigDecimal((containerManager.getVolumeCbm(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    			}
    		}		
    		
        container.setTotalPieces(Integer.parseInt((containerManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
    	
    	containers = containerManager.containerList(serviceOrder.getId());
        getSession().setAttribute("containers", containers);        
    	}
        if(serviceOrder.getGrpID()!=null && !(serviceOrder.toString().equalsIgnoreCase(""))){            	
        	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getGrpID().toString(),serviceOrder.getCorpID());	
        }else if(serviceOrder.getControlFlag()!=null && serviceOrder.getControlFlag().equalsIgnoreCase("G")){
        	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getId().toString(),serviceOrder.getCorpID());	
        }else{
        	usedVolume="";
        }
    	
    	// Carton List
    	carton = new Carton();
    	if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)== null || cartonManager.getNet(serviceOrder.getShipNumber()).get(0)== null ||cartonManager.getVolume(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getPieces(serviceOrder.getShipNumber()).get(0) == null || cartonManager.getTare(serviceOrder.getShipNumber()).get(0) == null){
    		cartons = new ArrayList( serviceOrder.getCartons());
      	   	getSession().setAttribute("cartons", cartons);
      	 		
    	}else{
    		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
    			for (SystemDefault systemDefault : sysDefaultDetail) {
    				if(systemDefault.getWeightUnit().equals("Lbs")){
    					UnitType=true;
    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGross(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTare(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNet(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					UnitType=false;
    					carton.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossKilo(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalTareWeight(new BigDecimal((cartonManager.getTareKilo(serviceOrder.getShipNumber())).get(0).toString()));
    					carton.setTotalNetWeight(new BigDecimal((cartonManager.getNetKilo(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    				if(systemDefault.getVolumeUnit().equals("Cft")){
    					VolumeType=true;
    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolume(serviceOrder.getShipNumber())).get(0).toString()));
    				}else{
    					VolumeType=false;
    					 carton.setTotalVolume(new BigDecimal((cartonManager.getVolumeCbm(serviceOrder.getShipNumber())).get(0).toString()));
    				}
    			}
    		}       
  	   	carton.setTotalPieces(Integer.parseInt((cartonManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));
        cartons = new ArrayList( serviceOrder.getCartons());
  	   	getSession().setAttribute("cartons", cartons);
        
    	}
    	containerNumberListCarton=cartonManager.getContainerNumber(serviceOrder.getShipNumber());
    	cartonTypeValue = cartonManager.findCartonTypeValue(sessionCorpID);
    	
    	// Vehicle List
    	vehicles = new ArrayList(serviceOrder.getVehicles());
    	containerNumberListVehicle=vehicleManager.getContainerNumber(serviceOrder.getShipNumber());
    	
    	// Routing List 
    	servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));
    	containerNumberListRouting=servicePartnerManager.getContainerNumber(serviceOrder.getShipNumber());
    	getSession().setAttribute("servicePartnerss", servicePartnerss);
    	//Custom List
    	customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
    	// Consignee edit Code
    	
		if (id != null) {
			 consigneeInstructionList = consigneeInstructionManager.getByShipNumber(serviceOrder.getShipNumber());
			if(!consigneeInstructionList .isEmpty()){
				consigneeInstruction = (ConsigneeInstruction)consigneeInstructionList.get(0);
				consigneeInstruction.getConsigneeAddress();
				consigneeInstruction.getNotification();
				consigneeInstruction.getSpecialInstructions();
				consigneeInstruction.getShipmentLocation();
			}else {
				consigneeInstruction = new ConsigneeInstruction();
     			consigneeInstruction.setCreatedOn(new Date());
				consigneeInstruction.setUpdatedOn(new Date());
				
				consigneeShiperList = consigneeInstructionManager.getShiperAddress(serviceOrder.getCorpID(),serviceOrder.getShipNumber());
				if((consigneeShiperList!=null) && (!(consigneeShiperList.isEmpty()))){
					String[] str1 = consigneeShiperList.get(0).toString().split("~");
					if(str1[0].equalsIgnoreCase("1")){
						str1[0]="";
					}
					else{
						str1[0]=str1[0]+"\n";
					}
					if(str1[1].equalsIgnoreCase("1")){
						str1[1]="";
					}
					else{
						str1[1]=str1[1]+" ";
					}
					if(str1[2].equalsIgnoreCase("1")){
						str1[2]="";
					}
					else{
						str1[2]=str1[2]+"\n";
					}
					if(str1[3].equalsIgnoreCase("1")){
						str1[3]="";
					}
					else{
						str1[3]=str1[3]+"\n";
					}
					if(str1[4].equalsIgnoreCase("1")){
						str1[4]="";
					}
					else{
						str1[4]=str1[4]+"\n";
					}
					if(str1[5].equalsIgnoreCase("1")){
						str1[5]="";
					}
					else{
						str1[5]=str1[5]+"\n";
					}
					if(str1[6].equalsIgnoreCase("1")){
						str1[6]="";
					}
					else{
						str1[6]=str1[6]+",";
					}
					if(str1[7].equalsIgnoreCase("1")){
						str1[7]="";
					}
					else{
						str1[7]=str1[7]+" ";
					}
					if(str1[8].equalsIgnoreCase("1")){
						str1[8]="";
					}
					else{
						str1[8]= str1[8]+"\n";
					}
					if(str1[9].equalsIgnoreCase("1")){
						str1[9]="";
					}
					else{
						str1[9]=str1[9]+"\n";
					}
					if(str1[10].equalsIgnoreCase("1")){
						str1[10]="";
					}
					else{
						str1[10]= "Phn :" +str1[10];
					}
					consigneeInstruction.setShipperAddress(str1[0]+"For :"+str1[1]+str1[2]+str1[3]+str1[4]+str1[5]+str1[6]+str1[7]+str1[8]+str1[9]+str1[10]);
				}	
			}
				
				
				
				if((trackingStatus.getOriginAgentCode() != null) && (!(trackingStatus.getOriginAgentCode().equalsIgnoreCase(""))))
					consigneeInstructionCode=trackingStatus.getOriginAgentCode();
				if((trackingStatus.getOriginSubAgentCode() != null) && (!(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(""))))	
					consigneeInstructionCode=trackingStatus.getOriginSubAgentCode(); 
				if(id == null){				
				consigneeShipmentLocationList=consigneeInstructionManager.findShipmentLocation(consigneeInstructionCode,serviceOrder.getCorpID());				
				if((consigneeShipmentLocationList!=null) && (!(consigneeShipmentLocationList.isEmpty()))){
				String[] str1 = consigneeShipmentLocationList.get(0).toString().split("~");
				if(str1[0].equalsIgnoreCase("1")){
					str1[0]="";
				}
				else{
					str1[0]=str1[0]+"\n";
				}
				if(str1[1].equalsIgnoreCase("1")){
					str1[1]="";
				}
				else{
					str1[1]=str1[1]+"\n";
				}
				if(str1[2].equalsIgnoreCase("1")){
					str1[2]="";
				}
				else{
					str1[2]=str1[2]+"\n";
				}
				if(str1[3].equalsIgnoreCase("1")){
					str1[3]="";
				}
				else{
					str1[3]=str1[3]+"\n";
				}
				if(str1[4].equalsIgnoreCase("1")){
					str1[4]="";
				}
				else{
					str1[4]=str1[4]+"\n";
				}
				if(str1[5].equalsIgnoreCase("1")){
					str1[5]="";
				}
				else{
					str1[5]=str1[5]+" ";
				}
				if(str1[6].equalsIgnoreCase("1")){
					str1[6]="";
				}
				else{
					str1[6]=str1[6]+" ";
				}
				if(str1[7].equalsIgnoreCase("1")){
					str1[7]="";
				}
				else{
					str1[7]=str1[7]+"\n";
				}
				if(str1[8].equalsIgnoreCase("1")){
					str1[8]="";
				}
				else{
					str1[8]="Phn : "+str1[8];
				}
				if(str1[9].equalsIgnoreCase("1")){
					str1[9]="";
				}
				else{
					str1[9]=str1[9]+"\n";
				}
				consigneeInstruction.setShipmentLocation(str1[0]+str1[1]+str1[2]+str1[3]+str1[4]+str1[5]+str1[6]+str1[9]+str1[7]+str1[8]);
				}
				}
				
			sACode=trackingStatus.getDestinationSubAgentCode();
			if((trackingStatus.getOriginAgentCode() != null) && (!(trackingStatus.getOriginAgentCode().equalsIgnoreCase(""))))
				osACode=trackingStatus.getOriginAgentCode();
			if((trackingStatus.getOriginSubAgentCode() != null) && (!(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(""))))	
				osACode=trackingStatus.getOriginSubAgentCode(); 
			if((trackingStatus.getBrokerCode()!= null) && (!(trackingStatus.getBrokerCode().equalsIgnoreCase(""))))	
				brokerCode=trackingStatus.getBrokerCode(); 
		}
		consigneeInstruction.setCorpID(serviceOrder.getCorpID());
		inlandAgentsList=new ArrayList(inlandAgentManager.findByInLandAgentList(serviceOrder.getShipNumber()));
		containerNumberListInlandAgent=inlandAgentManager.getContainerNumber(serviceOrder.getShipNumber());
		brokerDetailsList=brokerDetailsManager.getByShipNumber(serviceOrder.getShipNumber());
		if(!brokerDetailsList .isEmpty()){
			brokerDetails = (BrokerDetails)brokerDetailsList.get(0);
			
		}else {
			brokerDetails = new BrokerDetails();
			brokerDetails.setCreatedOn(new Date());
			brokerDetails.setUpdatedOn(new Date());
			if((brokerDetails.getVendorCode()==null || brokerDetails.getVendorCode().equals("")) && (trackingStatus.getBrokerCode()!=null) && (!trackingStatus.getBrokerCode().equals(""))){
				brokerDetails.setVendorCode(trackingStatus.getBrokerCode());
				brokerDetails.setVendorName(trackingStatus.getBrokerName());
			}
		}
		try{
		refExamPay = refMasterManager.findByParameter(sessionCorpID, "RefExamPay");
		brokerExamDetailsList=new ArrayList(brokerExamDetailsManager.findByBrokerExamDetailsList(serviceOrder.getShipNumber()));
		}
		catch(Exception e){
			e.printStackTrace();
		}
    	// Consignee End
    	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
        countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
    	}
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }    
    
   //  End of Method.
    private List consigneeInstructionList;
    private ConsigneeInstructionManager consigneeInstructionManager;
    private ConsigneeInstruction consigneeInstruction;
    private List consigneeShiperList;
    private String consigneeInstructionCode="";
    private List consigneeShipmentLocationList;
    private String sACode;
	private String osACode;
	private  Map<String, String> specific;
	private  Map<String, String> EQUIP_isactive;
	private  Map<String, String> specific_isactive;
//  Method for getting the values for all comboboxes in the dropdown list.
    
   	public String getComboList(String corpId){
   		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
   		EQUIP = refMasterManager.findByParameter(corpId, "EQUIP");
    	LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
    	 String parameters="'EQUIP','SPECIFIC'";
   		EQUIP_isactive  = new LinkedHashMap<String, String>();
   		specific_isactive=new LinkedHashMap<String, String>();
   	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
   	   for (RefMasterDTO  refObj : allParamValue) {
   	    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("EQUIP")){
   	    	    		 	EQUIP_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());}
   	    	    		 if(refObj.getParameter().trim().equalsIgnoreCase("SPECIFIC")){
   	    	    			specific_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());} 	    		 	
   	   }
   		weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs");
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");
      	specific = refMasterManager.findByParameter(corpId, "SPECIFIC");
      	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
   	
//  End of Method.
   	
   	
	@SkipValidation 
    public String containerNext(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List tempcontaiList=containerManager.goNextSOChild(sidNum,sessionCorpID,soIdNum);
		if(tempcontaiList!=null && (!(tempcontaiList.isEmpty())) && tempcontaiList.get(0)!=null){
		tempcontai=Long.parseLong(tempcontaiList.get(0).toString());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    @SkipValidation 
    public String containerPrev(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List tempcontaiList=containerManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum);
    	if(tempcontaiList!=null && (!(tempcontaiList.isEmpty())) && tempcontaiList.get(0)!=null){
    	tempcontai=Long.parseLong(tempcontaiList.get(0).toString());
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    } 
      
    @SkipValidation 
    public String containerSO(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	contaiSO=containerManager.goSOChild(sidNum,sessionCorpID,soIdNum);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
  
    @SkipValidation
	public String getContainerDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			trackingStatus=trackingStatusManager.get(id);
	    	miscellaneous = miscellaneousManager.get(id);
	    	billing = billingManager.get(id);
			EQUIP = refMasterManager.findByParameter(sessionCorpID, "EQUIP");
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							if(systemDefault.getWeightUnit().equals("Lbs")){
								UnitType=true;
							}else{
								UnitType=false;
							}
							if(systemDefault.getVolumeUnit().equals("Cft")){
								VolumeType=true;
							}else{
								VolumeType=false;
							}
						}
					}
			    	container = new Container();
			    	if(containerManager.getGross(serviceOrder.getShipNumber()).get(0)== null || containerManager.getNet(serviceOrder.getShipNumber()).get(0)== null ||containerManager.getVolume(serviceOrder.getShipNumber()).get(0) == null || containerManager.getPieces(serviceOrder.getShipNumber()).get(0)==null){
			    		containers = containerManager.containerList(serviceOrder.getId());
			            getSession().setAttribute("containers", containers);
			    	} 
			    	else{
			    		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			    		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			    			for (SystemDefault systemDefault : sysDefaultDetail) {
			    				if(systemDefault.getWeightUnit().equals("Lbs")){
			    					UnitType=true;
			    					container.setGrossWeightTotal(new BigDecimal((containerManager.getGross(serviceOrder.getShipNumber())).get(0).toString()));
			    					container.setTotalNetWeight(new BigDecimal((containerManager.getNet(serviceOrder.getShipNumber())).get(0).toString()));
			    				}else{
			    					UnitType=false;
			    					container.setGrossWeightTotal(new BigDecimal((containerManager.getGrossKilo(serviceOrder.getShipNumber())).get(0).toString()));
			    			    	container.setTotalNetWeight(new BigDecimal((containerManager.getNetKilo(serviceOrder.getShipNumber())).get(0).toString()));
			    				}
			    				if(systemDefault.getVolumeUnit().equals("Cft")){
			    					VolumeType=true;
			    					container.setTotalVolume(new BigDecimal((containerManager.getVolume(serviceOrder.getShipNumber())).get(0).toString()));
			    				}else{
			    					VolumeType=false;
			    					container.setTotalVolume(new BigDecimal((containerManager.getVolumeCbm(serviceOrder.getShipNumber())).get(0).toString()));
			    				}
			    			}
			    		}			    		
			        container.setTotalPieces(Integer.parseInt((containerManager.getPieces(serviceOrder.getShipNumber()).get(0).toString())));			    	
			    	containers = containerManager.containerList(serviceOrder.getId());
			        getSession().setAttribute("containers", containers);			        
			  }
            if(serviceOrder.getGrpID()!=null && !(serviceOrder.toString().equalsIgnoreCase(""))){            	
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getGrpID().toString(),serviceOrder.getCorpID());	
            }else if(serviceOrder.getControlFlag()!=null && serviceOrder.getControlFlag().equalsIgnoreCase("G")){
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getId().toString(),serviceOrder.getCorpID());	
            }else{
            	usedVolume="";
            }
		} catch (Exception e) {			
	    	 return CANCEL;
		}		
		return SUCCESS;
	} 
    
    private String fieldName;
    private String fieldValue;
    private String tableName;
    private String shipNumber;
    @SkipValidation
	public String updateContainerDetailsAjax(){
    		userName=getRequest().getRemoteUser();
			containerManager.updateContainerDetails(id,fieldName,fieldValue,tableName,sessionCorpID,shipNumber,userName);
			if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("Container")){
			container = containerManager.get(id);
			serviceOrder= container.getServiceOrder();
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
		        }
		        Long StartTime = System.currentTimeMillis();
				 /*if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
						String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
						if(agentShipNumber!=null && !agentShipNumber.equals(""))
							customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
			   		}*/
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	   
			       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	}

	                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
	                   		Iterator it=listByGrpId1.iterator();
	                   		while(it.hasNext()){
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			tempContainer.setSize(container.getSize());
	            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
	            	         tempContainer.setContainerNumber(container.getContainerNumber());
	            	         tempContainer.setSealNumber(container.getSealNumber());
	            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
	            	         tempContainer.setSealDate(container.getSealDate());
	            	         tempContainer.setContainerId(container.getId());
	            	         tempContainer=containerManager.save(tempContainer);	  
	            	         try{
	            	             if(tempOrder.getIsNetworkRecord()){
	            	             	List linkedShipNumber=findLinkedShipNumber(tempOrder);
	            	     			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder); 
	            	     			synchornizeOldContainer(serviceOrderRecords,tempContainer,false,tempOrder);
	            	            } 
	                    	}catch(Exception e){
	                    		e.printStackTrace();
	                    	}
	                   		}	                   		
	                   	}

					/*Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
		      
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
			}
		}else if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("Carton")){
			carton = cartonManager.get(id);
			serviceOrder=carton.getServiceOrder();
	        try{
	        if(serviceOrder.getIsNetworkRecord()){
	        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				synchornizeCarton(serviceOrderRecords,carton,false,serviceOrder);				
		   }
	        /*Long StartTime = System.currentTimeMillis();
			if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO") ){
				String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
				if(agentShipNumber!=null && !agentShipNumber.equals(""))
					customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
	   			
	   			}*/
	        if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
	  	   		}else{
	  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
	  	   		}
	         if(cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
	         	}else{
	         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
	         	}
	         
	         if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
	   	   		}else{
	   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
	   	   		}
	          if(cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){	
	          	} else{
	          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
	        }
			/*Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
	   
	       }catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
	       }
		}else if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("Vehicle")){
			vehicle = vehicleManager.get(id);
			serviceOrder=vehicle.getServiceOrder();
	        try{
	        if(serviceOrder.getIsNetworkRecord()){
	        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				synchornizeVehicle(serviceOrderRecords,vehicle,false,serviceOrder);				
	        }
	       /* Long StartTime = System.currentTimeMillis();
			if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
				String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
				if(agentShipNumber!=null && !agentShipNumber.equals(""))
					customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());	   			
	   			}
				
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
	   if(serviceOrder.getCommodity().equals("AUTO")){
	        if(!vehicleManager.getWeight(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
	        	vehicleManager.updateMisc(vehicle.getShipNumber(),vehicleManager.getWeight(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
	 	   }
	 	   if(!vehicleManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
	 		  vehicleManager.updateMiscVolume(vehicle.getShipNumber(), vehicleManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),vehicleManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
	 	   }
	   }
	        }catch(Exception ex){
				System.out.println("\n\n\n\n error------->"+ex);
				 ex.printStackTrace();
			}
		}else if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("ServicePartner")){
			servicePartner =servicePartnerManager.get(id);
			serviceOrder=servicePartner.getServiceOrder();
			trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
			try{
	            if(serviceOrder.getIsNetworkRecord()){
	            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
	    			synchornizeRouting(serviceOrderRecords,servicePartner,false,serviceOrder,trackingStatus);
	    		}
	            /*Long StartTime = System.currentTimeMillis();
	    		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
	    			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getSequenceNumber(),sessionCorpID);
	    			if(agentShipNumber!=null && !agentShipNumber.equals(""))
	    				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
	       			
	       			}*/
	            if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
	               	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){	               	
	               			listByGrpId1=servicePartnerManager.routingListByGrpId(servicePartner.getId());
	               		}	               	 
	               	}
	          	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
               		Iterator it=listByGrpId1.iterator();
               		while(it.hasNext()){
               			ServicePartner tempRouting=(ServicePartner)it.next();
               			ServiceOrder tempOrder =serviceOrderManager.get(tempRouting.getServiceOrderId());
            			TrackingStatus trackingStatus=trackingStatusManager.get(tempOrder.getId());
               			
               			tempRouting.setBlNumber(servicePartner.getBlNumber());
               			tempRouting.setBookNumber(servicePartner.getBookNumber());               			
               			tempRouting.setCntnrNumber(servicePartner.getCntnrNumber());                			
               			tempRouting.setTranshipped(servicePartner.getTranshipped());
               			
               			tempRouting.setUpdatedBy(servicePartner.getUpdatedBy());
               			tempRouting.setUpdatedOn(servicePartner.getUpdatedOn());
               			tempRouting=servicePartnerManager.save(tempRouting);
               			try{
               				if(tempOrder.getIsNetworkRecord()){
               	            	List linkedShipNumber=findLinkedShipNumber(tempOrder);
               	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
               	    			synchornizeRouting(serviceOrderRecords,tempRouting,false,tempOrder,trackingStatus);
               	    		} 
               			}catch(Exception e){
               				e.printStackTrace();
               			}
               			
               		}
               		
               	}
	    			
	    		/*Long timeTaken =  System.currentTimeMillis() - StartTime;
	    		logger.warn("\n\nTime taken for creating line in integration log info table : "+timeTaken+"\n\n");*/
	       
	            }catch(Exception ex){
	    			System.out.println("\n\n\n\n error------->"+ex);
	    			 ex.printStackTrace();
	    		}
			try{
				List al=new ArrayList();
				al.add("FOTDP");
				al.add("D/DP");
				al.add("Warehouse to Destination Port");
				al.add("OP/DP");
				al.add("D/P");
				al.add("W/P");	
					List temp=servicePartnerManager.findTranshipChild(serviceOrder.getShipNumber());
					if((temp!=null)&&(!temp.isEmpty())&&(temp.get(0)!=null)&&(!temp.get(0).toString().equalsIgnoreCase(""))){					
						Long spid=Long.parseLong(temp.get(0).toString());
						ServicePartner newService=servicePartnerManager.get(spid);
						if((!newService.getTranshipped())&&(serviceOrder.getServiceType()!=null)&&(!serviceOrder.getServiceType().equalsIgnoreCase(""))&&(al.contains(serviceOrder.getServiceType()))&&(trackingStatus.getDeliveryA()==null)&&(!serviceOrder.getIsNetworkRecord())){					
								if(newService.getAtArrival()!=null){
								trackingStatus.setDeliveryA(newService.getAtArrival());
								trackingStatus.setUpdatedBy(newService.getUpdatedBy());
								trackingStatus.setUpdatedOn(new Date());
								trackingStatus=trackingStatusManager.save(trackingStatus);
								//Bug 11839 - Possibility to modify Current status to Prior status
								//serviceOrder.setStatus("DLVR");
								//serviceOrder.setStatusNumber(60);
								//serviceOrder.setUpdatedBy(newService.getUpdatedBy());
								//serviceOrder.setUpdatedOn(new Date());
								//serviceOrder=serviceOrderManager.save(serviceOrder);
							}
					    }
				    }	        	  
			}catch(Exception e){
				 e.printStackTrace();
			}
			if((sessionCorpID.equals("VOER") || sessionCorpID.equals("VORU") || sessionCorpID.equals("VOCZ") || sessionCorpID.equals("VOAO")|| sessionCorpID.equals("BONG")) && serviceOrder.getServiceType()!=null &&(!serviceOrder.getServiceType().equalsIgnoreCase("")) && (serviceOrder.getServiceType().equals("D/DP")|| serviceOrder.getServiceType().equalsIgnoreCase("Free on Truck to Destination Port")|| serviceOrder.getServiceType().equalsIgnoreCase("Warehouse to Destination Port") || serviceOrder.getServiceType().equalsIgnoreCase("OP/DP"))){
				List maxAtArrival=servicePartnerManager.getMaxAtArrivalDate(serviceOrder.getId(),sessionCorpID);
				if(maxAtArrival!=null && !maxAtArrival.isEmpty() && maxAtArrival.get(0)!=null){
					ServicePartner  maxServicePartner = servicePartnerManager.get(Long.parseLong(maxAtArrival.get(0).toString()));
					trackingStatus.setServiceCompleteDate(maxServicePartner.getAtArrival());
					trackingStatus.setUpdatedBy(maxServicePartner.getUpdatedBy());
					trackingStatus.setUpdatedOn(new Date());
					trackingStatus=trackingStatusManager.save(trackingStatus);
					
					if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){
						if((serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
							linkedshipNumber.add(serviceOrder.getBookingAgentShipNumber());
						}
						if((trackingStatus.getBookingAgentExSO()!=null) && (!trackingStatus.getBookingAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
							linkedshipNumber.add(trackingStatus.getBookingAgentExSO());
						}
						if((trackingStatus.getNetworkAgentExSO()!=null) && (!trackingStatus.getNetworkAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
							linkedshipNumber.add(trackingStatus.getNetworkAgentExSO());
						}	
						if(linkedshipNumber!=null && !linkedshipNumber.isEmpty() && linkedshipNumber.size()>0 && trackingStatus.getServiceCompleteDate()!=null){
							trackingStatusManager.updateLinkedShipNumberforCompletedDate(linkedshipNumber,trackingStatus.getServiceCompleteDate(),sessionCorpID,getRequest().getRemoteUser());
						}
					}
						
				}
				
			}
			servicePartnerManager.updateTrack(servicePartner.getShipNumber(),servicePartner.getBlNumber(),servicePartner.getHblNumber(),userName);
		}
		return SUCCESS;
	}
    
    
    private String fieldNetWeight;
    private String fieldNetWeightVal;
    private String fieldGrossWeight;
    private String fieldGrossWeightVal;
    private String fieldEmptyConWeight;
    private String fieldEmptyConWeightVal;
    private String fieldNetWeightKilo;
    private String fieldNetWeightKiloVal;
    private String fieldGrossWeightKilo;
    private String fieldGrossWeightKiloVal;
    private String fieldemptyContWeightKilo;
    private String fieldemptyContWeightKiloVal;
    private String fieldVolumeCbm;
    private String fieldVolumeCbmVal;
    private String fieldDensity;
    private String fieldDensityVal;
    private String fieldDensityMetric;
    private String fieldDensityMetricVal;
    @SkipValidation
	public String updateContainerDetailsNetWtdAjax(){
    		userName=getRequest().getRemoteUser();
			containerManager.updateContainerDetailsAllFields(id,fieldNetWeight,fieldNetWeightVal,fieldGrossWeight,fieldGrossWeightVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldGrossWeightKilo,fieldGrossWeightKiloVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
			container = containerManager.get(id);
			serviceOrder= container.getServiceOrder();
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
		        }
		        /*Long StartTime = System.currentTimeMillis();
				 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
						String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
						if(agentShipNumber!=null && !agentShipNumber.equals(""))
							customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
			   		}*/
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null   || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	}

	                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
	                   		Iterator it=listByGrpId1.iterator();
	                   		while(it.hasNext()){
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			tempContainer.setSize(container.getSize());
	            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
	            	         tempContainer.setContainerNumber(container.getContainerNumber());
	            	         tempContainer.setSealNumber(container.getSealNumber());
	            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
	            	         tempContainer.setSealDate(container.getSealDate());
	            	         tempContainer.setContainerId(container.getId());
	            	         containerManager.save(tempContainer);	                   			
	                   		}	                   		
	                   	}
	            	
						
					/*Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
		      
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
			} 		
		return SUCCESS;
	}
    
    @SkipValidation
	public String updateContainerDetailsTareWtAjax(){
    		userName=getRequest().getRemoteUser();
			containerManager.updateContainerDetailsTareWt(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
			container = containerManager.get(id);
			serviceOrder= container.getServiceOrder();
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
		        }
		        /*Long StartTime = System.currentTimeMillis();
				 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
						String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
						if(agentShipNumber!=null && !agentShipNumber.equals(""))
							customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
			   		}*/
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	   
			       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	}

	                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
	                   		Iterator it=listByGrpId1.iterator();
	                   		while(it.hasNext()){
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			tempContainer.setSize(container.getSize());
	            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
	            	         tempContainer.setContainerNumber(container.getContainerNumber());
	            	         tempContainer.setSealNumber(container.getSealNumber());
	            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
	            	         tempContainer.setSealDate(container.getSealDate());
	            	         tempContainer.setContainerId(container.getId());
	            	         containerManager.save(tempContainer);	                   			
	                   		}	                   		
	                   	}	            	
						
					/*Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
		      
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
			} 		
		return SUCCESS;
	}
    private String fieldVolume;
    private String  fieldVolumeVal;
    @SkipValidation
	public String updateContainerDetailsDensityAjax(){
    		userName=getRequest().getRemoteUser();
			containerManager.updateContainerDetailsDensity(id,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
			container = containerManager.get(id);
			serviceOrder= container.getServiceOrder();
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
		        }
		        /*Long StartTime = System.currentTimeMillis();
				 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
						String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
						if(agentShipNumber!=null && !agentShipNumber.equals(""))
							customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
			   		}*/
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	}

	                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
	                   		Iterator it=listByGrpId1.iterator();
	                   		while(it.hasNext()){
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			tempContainer.setSize(container.getSize());
	            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
	            	         tempContainer.setContainerNumber(container.getContainerNumber());
	            	         tempContainer.setSealNumber(container.getSealNumber());
	            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
	            	         tempContainer.setSealDate(container.getSealDate());
	            	         tempContainer.setContainerId(container.getId());
	            	         containerManager.save(tempContainer);	                   			
	                   		}	                   		
	                   	}	            	
						
					/*Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
		      
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
			} 		
		return SUCCESS;
	}
    
	public String updateContainerDetailsNetWtKiloAjax(){
		userName=getRequest().getRemoteUser();
		containerManager.updateContainerDetailsNetWtKilo(id,fieldNetWeight,fieldNetWeightVal,fieldGrossWeight,fieldGrossWeightVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldGrossWeightKilo,fieldGrossWeightKiloVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
		container = containerManager.get(id);
		serviceOrder= container.getServiceOrder();
	   	try{
	        if(serviceOrder.getIsNetworkRecord()){
	        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
	        }
	        /*Long StartTime = System.currentTimeMillis();
			 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
					String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
					if(agentShipNumber!=null && !agentShipNumber.equals(""))
						customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
		   		}*/
		       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
		    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
		    	   } else {
		    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
		    	   }
		    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
		    		   
		    	   } else{
		    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		    	   }
		    	   }
		       
		       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
		    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
		    	   {
		    		  
		    	   } else {
		    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
		    	   }
		    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
		    		   
		    	   } else{
		    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
		    	   }
		    	   }
		       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
		           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
		           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
		           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
		           		}
		           	 
		           	}

                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
                   		Iterator it=listByGrpId1.iterator();
                   		while(it.hasNext()){
               			Container tempContainer=(Container)it.next();
               			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
               			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
               			tempContainer.setUpdatedOn(new Date());
               			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
               			tempContainer.setUnit1(tempMisc.getUnit1());
               			tempContainer.setUnit2(tempMisc.getUnit2());
               			tempContainer.setSize(container.getSize());
            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
            	         tempContainer.setContainerNumber(container.getContainerNumber());
            	         tempContainer.setSealNumber(container.getSealNumber());
            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
            	         tempContainer.setSealDate(container.getSealDate());
            	         tempContainer.setContainerId(container.getId());
            	         containerManager.save(tempContainer);	                   			
                   		}	                   		
                   	}            	
					
				/*Long timeTaken =  System.currentTimeMillis() - StartTime;
				logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
	      
	   	}catch(Exception ex){
			System.out.println("\n\n\n\n\n checkRemoveLinks exception");
			ex.printStackTrace();
		} 		
	return SUCCESS;
}
	
	@SkipValidation
	public String updateContainerDetailsTareWtKiloAjax(){
			userName=getRequest().getRemoteUser();
			containerManager.updateContainerTareWtKilo(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
			container = containerManager.get(id);
			serviceOrder= container.getServiceOrder();
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
		        }
		       /* Long StartTime = System.currentTimeMillis();
				 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
						String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
						if(agentShipNumber!=null && !agentShipNumber.equals(""))
							customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
			   		}*/
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	}

	                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
	                   		Iterator it=listByGrpId1.iterator();
	                   		while(it.hasNext()){
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			tempContainer.setSize(container.getSize());
	            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
	            	         tempContainer.setContainerNumber(container.getContainerNumber());
	            	         tempContainer.setSealNumber(container.getSealNumber());
	            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
	            	         tempContainer.setSealDate(container.getSealDate());
	            	         tempContainer.setContainerId(container.getId());
	            	         containerManager.save(tempContainer);	                   			
	                   		}	                   		
	                   	}
	            	
						
					/*Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
		      
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n\n checkRemoveLinks exception");
				ex.printStackTrace();
			} 		
		return SUCCESS;
	}
	
	 @SkipValidation
		public String updateContainerDetailsDensityMetricAjax(){
		 		userName=getRequest().getRemoteUser();
				containerManager.updateContainerDetailsDensityMetric(id,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID,userName);
				container = containerManager.get(id);
				serviceOrder= container.getServiceOrder();
			   	try{
			        if(serviceOrder.getIsNetworkRecord()){
			        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
						List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						 synchornizeOldContainer(serviceOrderRecords,container,false,serviceOrder);					
			        }
			       /* Long StartTime = System.currentTimeMillis();
					 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
							String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
							if(agentShipNumber!=null && !agentShipNumber.equals(""))
								customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());			   			
				   		}*/
				       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
				    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
				    	   } else {
				    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
				    	   }
				    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
				    		   
				    	   } else{
				    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
				    	   }
				    	   }
				       
				       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
				    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
				    	   {
				    		  
				    	   } else {
				    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
				    	   }
				    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
				    		   
				    	   } else{
				    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
				    	   }
				    	   }
				       	if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
				           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){			           	
				           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
				           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
				           		}
				           	 
				           	}

		                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
		                   		Iterator it=listByGrpId1.iterator();
		                   		while(it.hasNext()){
	                   			Container tempContainer=(Container)it.next();
	                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
	                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
	                   			tempContainer.setUpdatedOn(new Date());
	                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
	                   			tempContainer.setUnit1(tempMisc.getUnit1());
	                   			tempContainer.setUnit2(tempMisc.getUnit2());
	                   			tempContainer.setSize(container.getSize());
		            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
		            	         tempContainer.setContainerNumber(container.getContainerNumber());
		            	         tempContainer.setSealNumber(container.getSealNumber());
		            	         tempContainer.setCustomSeal(container.getCustomSeal());			            	      
		            	         tempContainer.setSealDate(container.getSealDate());
		            	         tempContainer.setContainerId(container.getId());
		            	         containerManager.save(tempContainer);	                   			
		                   		}	                   		
		                   	}		            	
							
						/*Long timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
			      
			   	}catch(Exception ex){
					System.out.println("\n\n\n\n\n checkRemoveLinks exception");
					ex.printStackTrace();
				} 		
			return SUCCESS;
		}
	 
    @SkipValidation
   	public String deleteContainerDetailsAjax(){
   		try {
   			container = containerManager.get(id);
   			//containerManager.remove(id); 
   			containerManager.updateStatus(id,containerStatus,updateRecords);
			serviceOrder= container.getServiceOrder();		   	
		        if(serviceOrder.getIsNetworkRecord()){
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					deleteContainer(serviceOrderRecords,container.getIdNumber(),containerStatus);
		        }
		        Long StartTime = System.currentTimeMillis();
			       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    		  
			    	   } else {
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
			    	   {
			    		  
			    	   } else {
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   
			    	   } else{
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       	try {
			       	   if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
			           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){	
			           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
			           			//listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
			           		}
			           	 
			           	} 
                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
                   		Iterator it=listByGrpId1.iterator();
                   		while(it.hasNext()){ 
                   			try {
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId()); 
                   		    if(tempOrder.getIsNetworkRecord()){
            		        	List linkedShipNumber=findLinkedShipNumber(tempOrder);
            					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
            					deleteContainer(serviceOrderRecords,tempContainer.getIdNumber(),containerStatus);
            		        } 
                   			}catch (Exception e) {			
       			   	    	 return CANCEL;
        			   		}	
                   	} 
                  	}
			       	}catch (Exception e) {			
			   	    	 return CANCEL;
			   		}	
   		} catch (Exception e) {			
   	    	 return CANCEL;
   		}		
   		return SUCCESS;
   	}
    
    
//  Method for editing and adding the Container record.
    List fieldsInfo=new ArrayList();
    private String usedVolume;
    Boolean alreadyExists=false;
    public String edit() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
    	String permKey = sessionCorpID +"-"+"component.field.SSCW.vgmCutoff";
    	weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs");
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");
        if (id != null) {   
        	container = containerManager.get(id);
        	serviceOrder = container.getServiceOrder();
        	
        	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
        	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
        	billing = billingManager.get(serviceOrder.getId());
        	customerFile = serviceOrder.getCustomerFile();
        	try {
      			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
      				 
      			 if(serviceOrder!=null){
      				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(serviceOrder.getCorpID()); 
      				 if(serviceOrder.getIsNetworkRecord() && (trackingStatus.getSoNetworkGroup() || chkCompanySurveyFlag)){
      						surveyTab= true;
      				}
      				}
      			 }
      		 } catch (Exception e) {
      			e.printStackTrace();
      		 }
        	if(container.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
        	if((!(containerManager.findMaximumChild(serviceOrder.getShipNumber()).isEmpty())) && containerManager.findMaximumChild(serviceOrder.getShipNumber()).get(0)!=null){
        	maxChild = containerManager.findMaximumChild(serviceOrder.getShipNumber()).get(0).toString();
        	}
        	if((!(containerManager.findMinimumChild(serviceOrder.getShipNumber()).isEmpty())) && containerManager.findMinimumChild(serviceOrder.getShipNumber()).get(0)!=null){
            minChild =  containerManager.findMinimumChild(serviceOrder.getShipNumber()).get(0).toString();
        	}
        	if((!(containerManager.findCountChild(serviceOrder.getShipNumber()).isEmpty())) && containerManager.findCountChild(serviceOrder.getShipNumber()).get(0)!=null){
            countChild =  containerManager.findCountChild(serviceOrder.getShipNumber()).get(0).toString();
        	}
            if(serviceOrder.getGrpID()!=null && !(serviceOrder.toString().equalsIgnoreCase(""))){
            	
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getGrpID().toString(),serviceOrder.getCorpID());	
            }
            else if(serviceOrder.getControlFlag()!=null && serviceOrder.getControlFlag().equalsIgnoreCase("G")){
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getId().toString(),serviceOrder.getCorpID());	
            }
            else{
            	usedVolume="";
            }
        	
        } else {  
        	container = new Container();
        	serviceOrder=serviceOrderManager.get(sid);
        	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        	miscellaneous = miscellaneousManager.get(sid);
        	trackingStatus=trackingStatusManager.get(sid);
        	billing = billingManager.get(sid);
        	customerFile = serviceOrder.getCustomerFile();
        	
            if(serviceOrder.getGrpID()!=null && !(serviceOrder.toString().equalsIgnoreCase(""))){
            	
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getGrpID().toString(),sessionCorpID);	
            }
            else if(serviceOrder.getControlFlag()!=null && serviceOrder.getControlFlag().equalsIgnoreCase("G")){
            	usedVolume=miscellaneousManager.findMasterOrderWeight(serviceOrder.getId().toString(),sessionCorpID);	
            }
            else{
            	usedVolume="";
            }
        	
        	container .setCreatedOn(new Date());
        	container .setUpdatedOn(new Date());
        	container.setCorpID(serviceOrder.getCorpID());
        	container.setUnit1(miscellaneous.getUnit1());
        	container.setUnit2(miscellaneous.getUnit2());
        	container.setGrossWeight(new BigDecimal(0));
        	container.setEmptyContWeight(new BigDecimal(0));
        	container.setVolume(new BigDecimal(0));
        	container.setNetWeight(new BigDecimal(0));
        	container.setGrossWeightKilo(new BigDecimal(0));
        	container.setEmptyContWeightKilo(new BigDecimal(0));
        	container.setVolumeCbm(new BigDecimal(0));
        	container.setNetWeightKilo(new BigDecimal(0));
        	container.setPieces(0);
        	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){
        /*		container.setSize(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		container.setGrossWeight(miscellaneous.get ==null ? "" : miscellaneous.getEquipment());
        		container.setEmptyContWeight(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		container.setNetWeight(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		container.setGrossWeightKilo(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		container.setEmptyContWeightKilo(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		container.setNetWeightKilo(miscellaneous.getEquipment() ==null ? "" : miscellaneous.getEquipment());
        		*/
        		fieldsInfo=containerManager.fieldsInfoById(serviceOrder.getId(), serviceOrder.getCorpID());
        		if(fieldsInfo!=null && !(fieldsInfo.isEmpty())){
        			alreadyExists=true;
        		}
        		else{
        			alreadyExists=false;
        		}
        	}
        	if(container.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
        	
        }
        shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        if(!customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).isEmpty()){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
        }
        getNotesForIconChange();
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
    
    public String updateDeleteStatus()
	{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	String key = "SS Container has been deleted successfully.";   
        saveMessage(getText(key));
        try{
        container=containerManager.get(id);
        serviceOrder=container.getServiceOrder();
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			deleteContainer(serviceOrderRecords,container.getIdNumber(),containerStatus);
      }
      }catch(Exception ex){
    	  System.out.println("\n\n\n\n error while removing container--->"+ex);
    	  ex.printStackTrace();
      } 
		containerManager.updateDeleteStatus(id);
		id=sid;
		containerList();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
    
    
//  End of Method.
    
//  Method getting the count of notes according to which the icon color changes in form
    
    private void deleteContainer(List<Object> serviceOrderRecords, String idNumber,String containerStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		containerManager.deleteNetworkContainer(serviceOrder.getId(),idNumber,containerStatus);
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	@SkipValidation 
    public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List m = notesManager.countForContainerNotes(container.getId());
    	if( m.isEmpty() ){
			countContainerNotes = "0";
		}else {
			countContainerNotes="";
			if((m).get(0)!=null){
			countContainerNotes = ((m).get(0)).toString() ;
			}
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }   
    
//  End of Method.
    
//  Method used to save and update the record.
    private String updateRecords;
    private List listByGrpId1=new ArrayList();
	private List<ServiceOrder> listByGrpId2=new ArrayList();
	private List listWithOutLine=new ArrayList();
	private List sumOfPieces=new ArrayList();
	private List listOfPieces=new ArrayList();
	int sumValue=0;
    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List listByGrpId=new ArrayList();
    	//getComboList(sessionCorpID);
    	weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs");
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");
        boolean isNew = (container.getId() == null);   
        container.setServiceOrder(serviceOrder);
        miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
        container.setUpdatedOn(new Date());
        //container.setCorpID(sessionCorpID);
        container.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	container.setCreatedOn(new Date());
        }
        List maxIdNumber = serviceOrderManager.findMaximumIdNumber(serviceOrder.getShipNumber());
   	    if ( maxIdNumber.get(0) == null ) {          
           	 idNumber = "01";
           }else {
           	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	            //System.out.println(autoId);
				if((autoId.toString()).length() == 1) {
	            	idNumber = "0"+(autoId.toString());
	            }else {
	            	idNumber=autoId.toString();
	        
	            }
           }
   	 if(isNew){
   		container.setIdNumber(idNumber);
     }
     else{
    	 container.setIdNumber(container.getIdNumber());
     }
   	 
   	 
   	 ServiceOrder soTemp=container.getServiceOrder();
   	 if(soTemp.getControlFlag()!=null && soTemp.getControlFlag().equalsIgnoreCase("G")){
   		 Long grpId=container.getServiceOrder().getId();
   		 List<ServiceOrder> childOrdersList=serviceOrderManager.findServiceOrderByGrpId(grpId.toString(), sessionCorpID);
   		 
   		if(childOrdersList!=null && !(childOrdersList.isEmpty())){
   			Iterator childIterator=childOrdersList.iterator();
   			while(childIterator.hasNext()){
   			  ServiceOrder temporaryOrder=(ServiceOrder)childIterator.next();
       		String shipNoo=temporaryOrder.getShipNumber();
       		sumOfPieces=cartonManager.getPieces(shipNoo);
       		if(sumOfPieces!=null && !(sumOfPieces.isEmpty()) && (sumOfPieces.get(0)!=null)){
       			sumValue=sumValue+Integer.parseInt(sumOfPieces.get(0).toString());
       		}
   				
   			}
   			container.setPieces(sumValue);
	           
     	  
     	}
   		 
   	 }
   	 
   	    
   	container= containerManager.save(container);
   	if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
   		container.setUgwIntId(container.getId().toString());
   	 	container= containerManager.save(container);
   	}
   		
   	serviceOrder= container.getServiceOrder();
   	try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			 if(isNew){
				 synchornizeContainer(serviceOrderRecords,container,isNew,serviceOrder);
				 
			 }else{
				 synchornizeOldContainer(serviceOrderRecords,container,isNew,serviceOrder);
				}
       }
        /*Long StartTime = System.currentTimeMillis();
		 if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
				String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
				if(agentShipNumber!=null && !agentShipNumber.equals(""))
					customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
	   			
	   			}
				
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
        List companyDetailsList=companyDivisionManager.findCompanyCodeByBookingAgent(serviceOrder.getBookingAgentCode(), sessionCorpID); 
        if(serviceOrder.getRegistrationNumber()!=null && !serviceOrder.getRegistrationNumber().equals("") && serviceOrder.getPackingMode()!=null && (serviceOrder.getPackingMode().equals("LOOSE") || serviceOrder.getPackingMode().equals("LVCO")) && serviceOrder.getJob().toString().trim().equalsIgnoreCase("INT") && serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("HOLL") 
	    		&& companyDetailsList!=null && !companyDetailsList.isEmpty() && companyDetailsList.get(0)!=null){
			Long StartTime = System.currentTimeMillis();
			customerFileAction.createIntegrationLogInfo(serviceOrder.getShipNumber(),"CREATE","CO");
			Long timeTaken =  System.currentTimeMillis() - StartTime;
    		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");
        }
      
   	}catch(Exception ex){
		System.out.println("\n\n\n\n\n checkRemoveLinks exception");
		ex.printStackTrace();
	}
      	shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        maxChild = containerManager.findMaximumChild(serviceOrder.getShipNumber()).get(0).toString();
        minChild =  containerManager.findMinimumChild(serviceOrder.getShipNumber()).get(0).toString();
        countChild =  containerManager.findCountChild(serviceOrder.getShipNumber()).get(0).toString();
        
   	    if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "container.added" : "container.updated";   
        saveMessage(getText(key)); 
        }
       if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){
    	   
    	   if(containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
    	   {
    		  
    	   }
    	   else
    	   {
    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
    	   }
    	   if(containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
    	   {
    		   
    	   }
    	   else{
    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
    	   }
    	   }
       
       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){
    	   
    	   if(containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
    	   {
    		  
    	   }
    	   else
    	   {
    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
    	   }
    	   if(containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00"))
    	   {
    		   
    	   }
    	   else{
    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
    	   }
    	   }
       	   
           if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){
           		if(isNew){
           			listByGrpId2=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);	
           		}
           		else{
           			listByGrpId1=containerManager.containerListOfMaster(container.getId());
           			listWithOutLine=containerManager.containerWithNoLine(sessionCorpID,serviceOrder.getId(),container.getId());
           		}
           	 
           	}
           	
       /*    	if(listByGrpId!=null && !(listByGrpId.isEmpty())){
           		Iterator it=listByGrpId.iterator();
           		while(it.hasNext()){
           			Long tempId=new Long(it.next().toString());
           			Container tempContainer=containerManager.get(tempId);
           			tempContainer.setContainerNumber(container.getContainerNumber());
           			tempContainer.setSealNumber(container.getSealNumber());
           			tempContainer.setCustomSeal(container.getCustomSeal());
           			tempContainer.setSealDate(container.getSealDate());
           			tempContainer.setSize(container.getSize());
           		    containerManager.save(tempContainer);
           		}
           		
           	}  */
           	
           	
          	if(isNew){
               	if(listByGrpId2!=null && !(listByGrpId2.isEmpty())){
               		Iterator it=listByGrpId2.iterator();
               		while(it.hasNext()){
               			ServiceOrder tempOrder =(ServiceOrder)it.next();
               			Miscellaneous tempMisc=miscellaneousManager.get(serviceOrder.getId());
               			Container tempContainer=new Container();
               			
               			tempContainer.setCreatedOn(new Date());
               			tempContainer.setUpdatedOn(new Date());
               			tempContainer.setCreatedBy(getRequest().getRemoteUser());
               			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
               			tempContainer.setCorpID(sessionCorpID);
               			tempContainer.setUnit1(tempMisc.getUnit1());
               			tempContainer.setUnit2(tempMisc.getUnit2());
               			tempContainer.setGrossWeight(new BigDecimal(0));
               			tempContainer.setEmptyContWeight(new BigDecimal(0));
        	            tempContainer.setVolume(new BigDecimal(0));
        	            tempContainer.setNetWeight(new BigDecimal(0));
        	            tempContainer.setGrossWeightKilo(new BigDecimal(0));
        	            tempContainer.setEmptyContWeightKilo(new BigDecimal(0));
        	            tempContainer.setVolumeCbm(new BigDecimal(0));
        	            tempContainer.setNetWeightKilo(new BigDecimal(0));
               			/*tempContainer.setGrossWeight(container.getGrossWeight());
               			tempContainer.setEmptyContWeight(container.getEmptyContWeight());
               			tempContainer.setVolume(container.getVolume());
               			tempContainer.setNetWeight(container.getNetWeight());
               			tempContainer.setGrossWeightKilo(container.getGrossWeightKilo());
               			tempContainer.setEmptyContWeightKilo(container.getEmptyContWeightKilo());
               			tempContainer.setVolumeCbm(container.getVolumeCbm());
               			tempContainer.setNetWeightKilo(container.getNetWeightKilo());*/
               			tempContainer.setSize(container.getSize());
               		//	tempContainer.setPieces(container.getPieces());
               			tempContainer.setShipNumber(tempOrder.getShipNumber());
               			tempContainer.setShip(tempOrder.getShip());
               			tempContainer.setServiceOrderId(tempOrder.getId());
               			tempContainer.setServiceOrder(tempOrder);
        	            
        	            List maxIdNumber1 = serviceOrderManager.findMaximumIdNumber(tempOrder.getShipNumber());
        	       	    if ( maxIdNumber1.get(0) == null ) {          
        	               	 idNumber = "01";
        	               }else {
        	            	   if((maxIdNumber1).get(0)!=null){
        	               	autoId = Long.parseLong((maxIdNumber1).get(0).toString()) + 1;
        	    	            //System.out.println(autoId);
        	    				if((autoId.toString()).length() == 1) {
        	    	            	idNumber = "0"+(autoId.toString());
        	    	            }else {
        	    	            	idNumber=autoId.toString();
        	    	        
        	    	            }
        	            	   }
        	               }
        	       	 tempContainer.setIdNumber(idNumber);
        	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
        	     //    tempContainer.setNumberOfContainer(container.getNumberOfContainer());
        	         tempContainer.setContainerNumber(container.getContainerNumber());
        	         tempContainer.setSealNumber(container.getSealNumber());
        	         tempContainer.setCustomSeal(container.getCustomSeal());
        	         /*tempContainer.setNetWeight(container.getNetWeight());
        	         tempContainer.setUseDsp(container.getUseDsp());
        	         tempContainer.setDensity(container.getDensity());
        	         tempContainer.setDensityMetric(container.getDensityMetric());
        	         tempContainer.setTotalLine(container.getTotalLine());
        	         tempContainer.setTopier(container.getTopier());
        	         tempContainer.setPickup(container.getPickup());
        	         tempContainer.setDeliver(container.getDeliver());
        	         tempContainer.setTripNumber(container.getTripNumber());
        	         tempContainer.setGrossWeightTotal(container.getGrossWeightTotal());
        	         tempContainer.setTotalNetWeight(container.getTotalNetWeight());
        	         tempContainer.setTotalVolume(container.getTotalVolume());
        	         tempContainer.setTotalPieces(container.getTotalPieces());*/
        	         tempContainer.setGrossWeight(new BigDecimal(0));
        	         tempContainer.setEmptyContWeight(new BigDecimal(0));
        	         tempContainer.setVolume(new BigDecimal(0));
        	         tempContainer.setNetWeight(new BigDecimal(0));
        	         tempContainer.setGrossWeightKilo(new BigDecimal(0));
        	         tempContainer.setEmptyContWeightKilo(new BigDecimal(0));
        	         tempContainer.setVolumeCbm(new BigDecimal(0));
        	         tempContainer.setNetWeightKilo(new BigDecimal(0));
        	         tempContainer.setSealDate(container.getSealDate());
        	         tempContainer.setContainerId(container.getId());
        	         containerManager.save(tempContainer); 
        	         try{
        	             if(tempOrder.getIsNetworkRecord()){
        	             	List linkedShipNumber=findLinkedShipNumber(tempOrder);
        	     			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder); 
        	     				 synchornizeContainer(serviceOrderRecords,tempContainer,isNew,tempOrder); 
        	            } 
                	}catch(Exception e){
                		e.printStackTrace();
                	}
                	}
               		
               	}
               	
               	}
               	else{
                  	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
                   		Iterator it=listByGrpId1.iterator();
                   		while(it.hasNext()){
                   		//	Long tempId=new Long(it.next().toString());
                   			Container tempContainer=(Container)it.next();
                   			Miscellaneous tempMisc=miscellaneousManager.get(tempContainer.getServiceOrderId());
                   			ServiceOrder tempOrder =serviceOrderManager.get(tempContainer.getServiceOrderId());
                   			tempContainer.setUpdatedOn(new Date());
                   			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
                   			tempContainer.setUnit1(tempMisc.getUnit1());
                   			tempContainer.setUnit2(tempMisc.getUnit2());
                   			/*tempContainer.setGrossWeight(container.getGrossWeight());
                   			tempContainer.setEmptyContWeight(container.getEmptyContWeight());
                   			tempContainer.setVolume(container.getVolume());
                   			tempContainer.setNetWeight(container.getNetWeight());
                   			tempContainer.setGrossWeightKilo(container.getGrossWeightKilo());
                   			tempContainer.setEmptyContWeightKilo(container.getEmptyContWeightKilo());
                   			tempContainer.setVolumeCbm(container.getVolumeCbm());
                   			tempContainer.setNetWeightKilo(container.getNetWeightKilo());*/
                   			tempContainer.setSize(container.getSize());
                   		//	tempContainer.setPieces(container.getPieces());
                   			
            	            
            	    /*        List maxIdNumber1 = serviceOrderManager.findMaximumIdNumber(tempOrder.getShipNumber());
            	       	    if ( maxIdNumber1.get(0) == null ) {          
            	               	 idNumber = "01";
            	               }else {
            	               	autoId = Long.parseLong((maxIdNumber1).get(0).toString()) + 1;
            	    	            //System.out.println(autoId);
            	    				if((autoId.toString()).length() == 1) {
            	    	            	idNumber = "0"+(autoId.toString());
            	    	            }else {
            	    	            	idNumber=autoId.toString();
            	    	        
            	    	            }
            	               }
            	       	 tempContainer.setIdNumber(idNumber);   */
            	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
            	     //    tempContainer.setNumberOfContainer(container.getNumberOfContainer());
            	         tempContainer.setContainerNumber(container.getContainerNumber());
            	         tempContainer.setSealNumber(container.getSealNumber());
            	         tempContainer.setCustomSeal(container.getCustomSeal());
            	        /* tempContainer.setNetWeight(container.getNetWeight());
            	         tempContainer.setUseDsp(container.getUseDsp());
            	         tempContainer.setDensity(container.getDensity());
            	         tempContainer.setDensityMetric(container.getDensityMetric());
            	         tempContainer.setTotalLine(container.getTotalLine());
            	         tempContainer.setTopier(container.getTopier());
            	         tempContainer.setPickup(container.getPickup());
            	         tempContainer.setDeliver(container.getDeliver());
            	         tempContainer.setTripNumber(container.getTripNumber());
            	         tempContainer.setGrossWeightTotal(container.getGrossWeightTotal());
            	         tempContainer.setTotalNetWeight(container.getTotalNetWeight());
            	         tempContainer.setTotalVolume(container.getTotalVolume());
            	         tempContainer.setTotalPieces(container.getTotalPieces());*/
            	         tempContainer.setSealDate(container.getSealDate());
            	         tempContainer.setContainerId(container.getId());
            	         tempContainer.setStatus(container.isStatus());
            	         containerManager.save(tempContainer);
            	         try{
            	             if(tempOrder.getIsNetworkRecord()){
            	             	List linkedShipNumber=findLinkedShipNumber(tempOrder);
            	     			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder); 
            	     			synchornizeOldContainer(serviceOrderRecords,tempContainer,false,tempOrder);
            	            } 
                    	}catch(Exception e){
                    		e.printStackTrace();
                    	}
                   			
                   		}
                   		
                   	}
               		
               	}
          	
         	if(!isNew){
            	if(listWithOutLine!=null && !(listWithOutLine.isEmpty())){
               		Iterator it=listWithOutLine.iterator();
               		while(it.hasNext()){
               			ServiceOrder tempOrder =serviceOrderManager.get(new Long((it.next().toString())));
               			Miscellaneous tempMisc=miscellaneousManager.get(tempOrder.getId());
               			Container tempContainer=new Container();
               			
               			tempContainer.setCreatedOn(new Date());
               			tempContainer.setUpdatedOn(new Date());
               			tempContainer.setCreatedBy(getRequest().getRemoteUser());
               			tempContainer.setUpdatedBy(getRequest().getRemoteUser());
               			tempContainer.setCorpID(sessionCorpID);
               			tempContainer.setUnit1(tempMisc.getUnit1());
               			tempContainer.setUnit2(tempMisc.getUnit2());
               			tempContainer.setGrossWeight(new BigDecimal(0));
               			tempContainer.setEmptyContWeight(new BigDecimal(0));
        	            tempContainer.setVolume(new BigDecimal(0));
        	            tempContainer.setNetWeight(new BigDecimal(0));
        	            tempContainer.setGrossWeightKilo(new BigDecimal(0));
        	            tempContainer.setEmptyContWeightKilo(new BigDecimal(0));
        	            tempContainer.setVolumeCbm(new BigDecimal(0));
        	            tempContainer.setNetWeightKilo(new BigDecimal(0));
               			
               			tempContainer.setSize(container.getSize());
               	
               			tempContainer.setShipNumber(tempOrder.getShipNumber());
               			tempContainer.setShip(tempOrder.getShip());
               			tempContainer.setServiceOrderId(tempOrder.getId());
               			tempContainer.setServiceOrder(tempOrder);
        	            
        	            List maxIdNumber1 = serviceOrderManager.findMaximumIdNumber(tempOrder.getShipNumber());
        	       	    if ( maxIdNumber1.get(0) == null ) {          
        	               	 idNumber = "01";
        	               }else {
        	               	autoId = Long.parseLong((maxIdNumber1).get(0).toString()) + 1;
        	    	            //System.out.println(autoId);
        	    				if((autoId.toString()).length() == 1) {
        	    	            	idNumber = "0"+(autoId.toString());
        	    	            }else {
        	    	            	idNumber=autoId.toString();
        	    	        
        	    	            }
        	               }
        	       	 tempContainer.setIdNumber(idNumber);
        	       	 tempContainer.setCntnrNumber(container.getCntnrNumber());
        	   
        	         tempContainer.setContainerNumber(container.getContainerNumber());
        	         tempContainer.setSealNumber(container.getSealNumber());
        	         tempContainer.setCustomSeal(container.getCustomSeal());
        	        
        	         tempContainer.setGrossWeight(new BigDecimal(0));
        	         tempContainer.setEmptyContWeight(new BigDecimal(0));
        	         tempContainer.setVolume(new BigDecimal(0));
        	         tempContainer.setNetWeight(new BigDecimal(0));
        	         tempContainer.setGrossWeightKilo(new BigDecimal(0));
        	         tempContainer.setEmptyContWeightKilo(new BigDecimal(0));
        	         tempContainer.setVolumeCbm(new BigDecimal(0));
        	         tempContainer.setNetWeightKilo(new BigDecimal(0));
        	         tempContainer.setSealDate(container.getSealDate());
        	         tempContainer.setContainerId(container.getId());
        	         containerManager.save(tempContainer);
        	         try{
        	             if(tempOrder.getIsNetworkRecord()){
        	             	List linkedShipNumber=findLinkedShipNumber(tempOrder);
        	     			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder); 
        	     				 synchornizeContainer(serviceOrderRecords,tempContainer,isNew,tempOrder); 
        	            } 
                	}catch(Exception e){
                		e.printStackTrace();
                	}
               			
               		}
               		
               	}
           		
           	}
           	
           	
           }
      
        if (!isNew) { 
         	getNotesForIconChange();
        	container.setGrossWeightTotal(new BigDecimal((containerManager.getGross(container.getShipNumber())).get(0).toString()));
        	container.setTotalNetWeight(new BigDecimal((containerManager.getNet(container.getShipNumber())).get(0).toString()));
        	container.setTotalVolume(new BigDecimal((containerManager.getVolume(container.getShipNumber())).get(0).toString()));
       //     container.setTotalPieces(Integer.parseInt((containerManager.getPieces(container.getShipNumber()).get(0).toString())));
        	
        	 listOfPieces=containerManager.getPieces(serviceOrder.getShipNumber());
             if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
            	 container.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
             }
             else{
            	 container.setTotalPieces(0);
             	
             }
             serviceOrder=serviceOrderManager.get(serviceOrder.getId());
           	containers = new ArrayList(serviceOrder.getContainers());
            getSession().setAttribute("containers", containers);
            hitFlag="1";
            serviceOrder=serviceOrderManager.get(serviceOrder.getId());
            return SUCCESS;   
        } else {   
        	IdMax = Long.parseLong(containerManager.findMaxId().get(0).toString());
            container = containerManager.get(IdMax);
        }
        serviceOrder=serviceOrderManager.get(serviceOrder.getId());
        /*container.setGrossWeightTotal(new BigDecimal((containerManager.getGross(container.getShipNumber())).get(0).toString()));
        container.setTotalNetWeight(new BigDecimal((containerManager.getNet(container.getShipNumber())).get(0).toString()));
        container.setTotalVolume(new BigDecimal((containerManager.getVolume(container.getShipNumber())).get(0).toString()));
      //  container.setTotalPieces(Integer.parseInt((containerManager.getPieces(container.getShipNumber()).get(0).toString())));
        
        listOfPieces=containerManager.getPieces(serviceOrder.getShipNumber());
        if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
       	 container.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
        }else{
       	 container.setTotalPieces(0);        	
        }*/
        containers = new ArrayList(serviceOrder.getContainers());
        getSession().setAttribute("containers", containers);
        hitFlag="1";
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        
        return SUCCESS;
    }
    
//  End of Method.
    
    private void synchornizeMiscellaneous(List<Object> miscellaneousRecords,Miscellaneous miscellaneous,TrackingStatus trackingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	String fieldType="";
		String uniqueFieldType=""; 
    	Iterator  it=miscellaneousRecords.iterator();
    	while(it.hasNext()){
    		Miscellaneous miscellaneousToRecods=(Miscellaneous)it.next();
    		if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){	
    		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(miscellaneousToRecods.getId());
    		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
    			uniqueFieldType=trackingStatusToRecods.getContractType();
    		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
    			fieldType="CMM/DMM";
    		}
    		List fieldToSync=customerFileManager.findFieldToSync("Miscellaneous",fieldType,uniqueFieldType);
    		fieldType="";
   		    uniqueFieldType=""; 
    		Iterator it1=fieldToSync.iterator();
    		while(it1.hasNext()){
    			String field=it1.next().toString();
    			String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
    			try{
    				//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
    				try{
    				beanUtilsBean.setProperty(miscellaneousToRecods,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
    				}catch(Exception ex){
						System.out.println("\n\n\n\n\n error while setting property--->"+ex);
						ex.printStackTrace();
					}
    				
    			}catch(Exception ex){
    				System.out.println("\n\n\n\n Exception while copying");
    				ex.printStackTrace();
    			}
    			
    		}
    		try{ 
    		if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
    		miscellaneousToRecods.setUpdatedBy(miscellaneous.getCorpID()+":"+getRequest().getRemoteUser());
    		miscellaneousToRecods.setUpdatedOn(new Date());
    		}} catch(Exception e){
    			e.printStackTrace();
    			
    		}
    		miscellaneousManager.save(miscellaneousToRecods);
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	private List<Object> findMiscellaneousRecords(List linkedShipNumber, ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		Miscellaneous miscellaneousRemote=miscellaneousManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    		recordList.add(miscellaneousRemote);
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return recordList;
	}
	private void synchornizeOldContainer(List<Object> serviceOrderRecords,Container container, boolean isNew,ServiceOrder serviceOrderold) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){		
    			Container containerTo= containerManager.getForOtherCorpid(containerManager.findRemoteContainer(container.getIdNumber(),serviceOrder.getId())); 
    			Long id=containerTo.getId();
    			String createdBy=containerTo.getCreatedBy();
    			Date createdOn=containerTo.getCreatedOn();
    			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(containerTo, container);
				
				containerTo.setId(id);
				containerTo.setCreatedBy(createdBy);
				containerTo.setCreatedOn(createdOn);
				containerTo.setCorpID(serviceOrder.getCorpID());
				containerTo.setShipNumber(serviceOrder.getShipNumber());
				containerTo.setSequenceNumber(serviceOrder.getSequenceNumber());
				containerTo.setServiceOrder(serviceOrder);
				containerTo.setServiceOrderId(serviceOrder.getId());
				containerTo.setUpdatedBy(container.getCorpID()+":"+getRequest().getRemoteUser()); 
				containerTo.setUpdatedOn(new Date());
				/*// added to fixed issue of merge functionality in 2 step linking
				try{
					 List maxIdNumber = serviceOrderManager.findMaximumIdNumber(serviceOrder.getShipNumber());
				   	    if ( maxIdNumber.get(0) == null ) {          
				           	 idNumber = "01";
				           }else {
				           	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
					            //System.out.println(autoId);
								if((autoId.toString()).length() == 1) {
					            	idNumber = "0"+(autoId.toString());
					            }else {
					            	idNumber=autoId.toString();
					        
					            }
				         }
				}catch(Exception e){
					System.out.println("Error in id number");
				}
				containerTo.setIdNumber(idNumber);*/
				try{
					containerTo.setGrossWeightTotal(new BigDecimal((containerManager.getGrossNetwork(containerTo.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
					containerTo.setTotalNetWeight(new BigDecimal((containerManager.getNetNetwork(containerTo.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
					containerTo.setTotalVolume(new BigDecimal((containerManager.getVolumeNetwork(containerTo.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
			        listOfPieces=containerManager.getPiecesNetwork(containerTo.getShipNumber(),serviceOrder.getCorpID());
			        if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
			        	containerTo.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
			        }
			        else{
			        	containerTo.setTotalPieces(0);
			        	
			        }
					}catch(Exception e){
						e.printStackTrace();	
					}
					containerTo = containerManager.save(containerTo);
				
				 if(containerTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 containerTo.setUgwIntId(containerTo.getId().toString());
					 containerTo= containerManager.save(containerTo);
				 }
				   	 if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
				    	   if(!containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
				    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
				    	   }
				    	   if(!containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
				    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
				    	   }
				    	   }
				       
				       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
				    	   if(!containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	
				    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
				    	   }
				    	   if(!containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {			    	  
				    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
				    	   }
				     }
				 
    			}	
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while syn old data"+ex);
    			ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
    			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    			recordList.add(serviceOrderRemote);
    		}
      	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return recordList;
	}
	private void synchornizeContainer(List<Object> serviceOrderRecords,Container container, boolean isNew,ServiceOrder serviceOrderold) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	
    		
			if(isNew){
				Container containerNew= new Container();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(containerNew, container); 
				containerNew.setId(null);
				containerNew.setCorpID(serviceOrder.getCorpID());
				containerNew.setShipNumber(serviceOrder.getShipNumber());
				containerNew.setSequenceNumber(serviceOrder.getSequenceNumber());
				containerNew.setServiceOrder(serviceOrder);
				containerNew.setServiceOrderId(serviceOrder.getId()); 
				containerNew.setCreatedBy("Networking");
				containerNew.setUpdatedBy("Networking");
				containerNew.setCreatedOn(new Date());
				containerNew.setUpdatedOn(new Date());	
				try{
					 List maxIdNumber = serviceOrderManager.findMaximumIdNumber(serviceOrder.getShipNumber());
				   	    if ( maxIdNumber.get(0) == null ) {          
				           	 idNumber = "01";
				           }else {
				           	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
					            //System.out.println(autoId);
								if((autoId.toString()).length() == 1) {
					            	idNumber = "0"+(autoId.toString());
					            }else {
					            	idNumber=autoId.toString();
					        
					            }
				         }
				containerNew.setIdNumber(idNumber);
				}catch(Exception e){
					System.out.println("Error in id number");
					e.printStackTrace();
				}
				try{
				containerNew.setGrossWeightTotal(new BigDecimal((containerManager.getGrossNetwork(containerNew.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
				containerNew.setTotalNetWeight(new BigDecimal((containerManager.getNetNetwork(containerNew.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
				containerNew.setTotalVolume(new BigDecimal((containerManager.getVolumeNetwork(containerNew.getShipNumber(),serviceOrder.getCorpID())).get(0).toString()));
		        listOfPieces=containerManager.getPiecesNetwork(containerNew.getShipNumber(),serviceOrder.getCorpID());
		        if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
		        	containerNew.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
		        }
		        else{
		        	containerNew.setTotalPieces(0);
		        	
		        }
				}catch(Exception e){
					e.printStackTrace();
				}
				containerNew=containerManager.save(containerNew);
				
				 if(containerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 containerNew.setUgwIntId(containerNew.getId().toString());
					 containerNew= containerManager.save(containerNew);
				 }
				 
				 if(cartonManager.getGross(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(!containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    		   containerManager.updateMisc(container.getShipNumber(),containerManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);   
			    	   }
			    	   if(!containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {
			    		   containerManager.updateMiscVolume(container.getShipNumber(), containerManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			    	   }
			       
			       	   if(cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0)==null  || cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	   
			    	   if(!containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){			    	
			    		   containerManager.updateMiscKilo(container.getShipNumber(),containerManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),containerManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);  
			    	   }
			    	   if(!containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")) {			    	  
			    		   containerManager.updateMiscVolumeCbm(container.getShipNumber(), containerManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
			    	   }
			     }
				 
			}
			
    		}
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing container"+ex);
    			ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
}
	
	private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return linkedShipNumberList;
	}
	
	
	public String forwardingList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   /* forwardingExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
	    String key = "Please enter your search criteria below";
	    saveMessage(getText(key));*/
	    return SUCCESS;
    }
    
    public String searchForwarding(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	containers = containerManager.searchForwarding(containerNo, bookingNo, sessionCorpID, blNumber, sailDate,lastName,vesselFilight,caed);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;
    }
    
    public String refreshWeights(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	refreshWeights = containerManager.refreshWeights(shipNum, packingMode, sessionCorpID);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    public String refreshContainer(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	userName=getRequest().getRemoteUser();
    	refreshContainer = containerManager.refreshContainer(shipNum, containerNos, sessionCorpID,userName);
    	//System.out.println("n\n\n\n\n\n"+refreshContainer);
    	//System.out.println("n\n\n\n\n\n containerManager.refreshContainer(shipNum, containerNos, sessionCorpID)"+containerManager.refreshContainer(shipNum, containerNos, sessionCorpID));
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    private void synchornizeCarton(List<Object> serviceOrderRecords,Carton carton, boolean isNew,ServiceOrder serviceOrderold) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){
    		try{
    				Carton cartonTo= cartonManager.getForOtherCorpid(cartonManager.findRemoteCarton(carton.getIdNumber(),serviceOrder.getId())); 
        			Long id=cartonTo.getId();
        			String createdBy=cartonTo.getCreatedBy();
        			Date createdOn=cartonTo.getCreatedOn();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(cartonTo, carton);
    				
    				cartonTo.setId(id);
    				cartonTo.setCreatedBy(createdBy);
    				cartonTo.setCreatedOn(createdOn);
    				cartonTo.setCorpID(serviceOrder.getCorpID());
    				cartonTo.setShipNumber(serviceOrder.getShipNumber());
    				cartonTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				cartonTo.setServiceOrder(serviceOrder);
    				cartonTo.setServiceOrderId(serviceOrder.getId());
    				cartonTo.setUpdatedBy(carton.getCorpID()+":"+getRequest().getRemoteUser());
    				cartonTo.setUpdatedOn(new Date()); 

    				try{
    					cartonTo.setTotalGrossWeight(new BigDecimal((cartonManager.getGrossNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    					cartonTo.setTotalNetWeight(new BigDecimal((cartonManager.getNetNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    					cartonTo.setTotalVolume(new BigDecimal((cartonManager.getVolumeNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    	            listOfPieces=cartonManager.getPiecesNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID());
    	            if(listOfPieces!=null && !listOfPieces.isEmpty() && (listOfPieces.get(0)!=null)){
    	            	cartonTo.setTotalPieces(Integer.parseInt(listOfPieces.get(0).toString()));
    	            }
    	            else{
    	            	cartonTo.setTotalPieces(0);
    	            	
    	            }
    	            cartonTo.setTotalTareWeight(new BigDecimal((cartonManager.getTareNetwork(cartonTo.getShipNumber(),cartonTo.getCorpID())).get(0).toString()));
    				}catch(Exception e){
    					e.printStackTrace();	
    				}
    				cartonTo = cartonManager.save(cartonTo);
    				 if(cartonTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					 cartonTo.setUgwIntId(cartonTo.getId().toString());
    					 cartonTo= cartonManager.save(cartonTo);
						}
    				 if(!cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){   
    		  	   			cartonManager.updateMisc(carton.getShipNumber(),cartonManager.getGross(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNet(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTare(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
    		  	   		}
    		         if(!cartonManager.getVolume(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		         	
    		         		cartonManager.updateMiscVolume(carton.getShipNumber(), cartonManager.getVolume(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
    		         	}
    		         
    		         if(!cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		   	   		
    		   	   			cartonManager.updateMiscKilo(carton.getShipNumber(),cartonManager.getGrossKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getNetKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getTareKilo(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getWeightUnit(serviceOrder.getShipNumber()).get(0).toString(),cartonManager.getVolumeUnit(serviceOrder.getShipNumber()).get(0).toString(),getRequest().getRemoteUser(),sessionCorpID);              
    		   	   		}
    		          if(!cartonManager.getVolumeCbm(carton.getShipNumber()).get(0).toString().equalsIgnoreCase("0.00")){    		          	
    		          		cartonManager.updateMiscVolumeCbm(carton.getShipNumber(), cartonManager.getVolumeCbm(serviceOrder.getShipNumber()).get(0).toString(),sessionCorpID,getRequest().getRemoteUser());
    		          }	
    			
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing carton"+ex);	
    			 ex.printStackTrace();
    		}
    	}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
	}
    
    private void synchornizeVehicle(List<Object> serviceOrderRecords,Vehicle vehicle, boolean isNew ,ServiceOrder serviceOrderold) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	

    				Vehicle vehicleTo= vehicleManager.getForOtherCorpid(vehicleManager.findRemoteVehicle(vehicle.getIdNumber(),serviceOrder.getId()));  
        			Long id=vehicleTo.getId();
        			String createdBy=vehicleTo.getCreatedBy();
        			Date createdOn=vehicleTo.getCreatedOn();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(vehicleTo, vehicle);
    				
    				vehicleTo.setId(id);
    				vehicleTo.setCreatedBy(createdBy);
    				vehicleTo.setCreatedOn(createdOn);
    				vehicleTo.setCorpID(serviceOrder.getCorpID());
    				vehicleTo.setShipNumber(serviceOrder.getShipNumber());
    				vehicleTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				vehicleTo.setServiceOrder(serviceOrder);
    				vehicleTo.setServiceOrderId(serviceOrder.getId());
    				vehicleTo.setUpdatedBy(vehicle.getCorpID()+":"+getRequest().getRemoteUser());
    				vehicleTo.setUpdatedOn(new Date()); 

    				vehicleTo =vehicleManager.save(vehicleTo);
    				 if(vehicleTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					 vehicleTo.setUgwIntId(vehicleTo.getId().toString());
    					 vehicleTo= vehicleManager.save(vehicleTo);
    				}
    				
    			}    		
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing vehicle"+ex);	
    			 ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
    	}
    private String userName;
    private void synchornizeRouting(List<Object> serviceOrderRecords,ServicePartner servicePartner, boolean isNew,ServiceOrder serviceOrderold,TrackingStatus trackingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		TrackingStatus trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	

    				ServicePartner servicePartnerTo= servicePartnerManager.getForOtherCorpid(servicePartnerManager.findRemoteServicePartner(servicePartner.getCarrierNumber(),serviceOrder.getId()));  
        			Long id=servicePartnerTo.getId();
        			String createdBy=servicePartnerTo.getCreatedBy();
        			Date createdOn=servicePartnerTo.getCreatedOn();
        			String carrierCode=servicePartnerTo.getCarrierCode();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(servicePartnerTo, servicePartner);
    				servicePartnerTo.setCarrierCode(carrierCode); 
    				servicePartnerTo.setId(id);
    				servicePartnerTo.setCreatedBy(createdBy);
    				servicePartnerTo.setCreatedOn(createdOn);
    				servicePartnerTo.setCorpID(serviceOrder.getCorpID());
    				servicePartnerTo.setShipNumber(serviceOrder.getShipNumber());
    				servicePartnerTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				servicePartnerTo.setServiceOrder(serviceOrder);
    				servicePartnerTo.setServiceOrderId(serviceOrder.getId());
    				servicePartnerTo.setUpdatedBy(servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				servicePartnerTo.setUpdatedOn(new Date()); 
    				servicePartnerTo = servicePartnerManager.save(servicePartnerTo);
    				
    				 if(servicePartnerTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					 servicePartnerTo.setUgwIntId(servicePartnerTo.getId().toString());
    					 servicePartnerTo= servicePartnerManager.save(servicePartnerTo);
						 }
    				 
    				userName=getRequest().getRemoteUser();
    				servicePartnerManager.updateTrack(servicePartnerTo.getShipNumber(),servicePartnerTo.getBlNumber(),servicePartnerTo.getHblNumber(),userName);
    	    	    servicePartnerManager.updateServiceOrder(servicePartnerTo.getCarrierDeparture(), servicePartnerTo.getCarrierArrival(), servicePartnerTo.getShipNumber(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    	    	    servicePartnerManager.updateServiceOrderDate(servicePartnerTo.getServiceOrderId(),serviceOrder.getCorpID(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser() );
    	    	    servicePartnerManager.updateServiceOrderActualDate(servicePartnerTo.getServiceOrderId(),serviceOrder.getCorpID(),serviceOrder.getCustomerFileId(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    	    	
    			
    			if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
    	        {
    				if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecod.getSoNetworkGroup()){     		     		  
    				   //serviceOrder.setStatus(serviceOrderold.getStatus());
    				   //serviceOrder.setStatusNumber(serviceOrderold.getStatusNumber());
    				   //serviceOrder.setStatusDate(serviceOrderold.getStatusDate());
    				   //serviceOrder.setUpdatedBy(servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				   //serviceOrder.setUpdatedOn(new Date());
    		     	   //serviceOrderManager.save(serviceOrder);
    					serviceOrderManager.updateStorageJob(serviceOrder.getId(),serviceOrderold.getStatus(),serviceOrderold.getStatusNumber(),serviceOrder.getCorpID(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				}    	    		
    			}
    			}
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing servicePartner"+ex);	
    			 ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

    
    public String getCountContainerNotes() {
		return countContainerNotes;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}
	public Carton getCarton() {
		return carton;
	}
	public void setCarton(Carton carton) {
		this.carton = carton;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
    public void setContainerManager(ContainerManager containerManager) {
        this.containerManager = containerManager;
    }
    public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    }
    public List getContainers() {   
        return containers;   
    }
    public String getValidateFormNav()
       {
        return validateFormNav;
        }
    public void setValidateFormNav(String validateFormNav) {
        this.validateFormNav = validateFormNav;
        }
    public String getGotoPageString() {
        return gotoPageString;
    }
    public void setGotoPageString(String gotoPageString) {
       this.gotoPageString = gotoPageString;
    }
    public  Map<String, String> getEQUIP() {
    	if(container !=null && container.getId()!=null && container.getSize()!=null && !container.getSize().equals(""))
		{
		if(!EQUIP.containsValue(container.getSize()))
		{EQUIP.put(container.getSize(), container.getSize());
		}
		}
    	
		return EQUIP;
	}
    public void setSid(Long sid) {   
	       this.sid = sid;   
	   }
    public void setId(Long id) {   
        this.id = id;   
    } 
    public Container getContainer() {   
        return container;   
    } 
    public void setContainer(Container container) {   
        this.container = container;   
    } 
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    } 
    public void setServiceOrder(ServiceOrder serviceOrder  ) {   
        this.serviceOrder = serviceOrder;   
    } 
    public  List getWeightunits() {
		return weightunits;
	}
    public  List getVolumeunits() {
		return volumeunits;
	}

public ExtendedPaginatedList getForwardingExt() {
	return forwardingExt;
}

public void setForwardingExt(ExtendedPaginatedList forwardingExt) {
	this.forwardingExt = forwardingExt;
}

public PaginateListFactory getPaginateListFactory() {
	return paginateListFactory;
}

public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
	this.paginateListFactory = paginateListFactory;
}

public List getForwardingList() {
	return forwardingList;
}

public void setForwardingList(List forwardingList) {
	this.forwardingList = forwardingList;
}

public String getBlNumber() {
	return blNumber;
}

public void setBlNumber(String blNumber) {
	this.blNumber = blNumber;
}

public String getBookingNo() {
	return bookingNo;
}

public void setBookingNo(String bookingNo) {
	this.bookingNo = bookingNo;
}

public String getContainerNo() {
	return containerNo;
}

public void setContainerNo(String containerNo) {
	this.containerNo = containerNo;
}

public Date getSailDate() {
	return sailDate;
}

public void setSailDate(Date sailDate) {
	this.sailDate = sailDate;
}

public List getRefreshWeights() {
	return refreshWeights;
}

public void setRefreshWeights(List refreshWeights) {
	this.refreshWeights = refreshWeights;
}

public String getPackingMode() {
	return packingMode;
}

public void setPackingMode(String packingMode) {
	this.packingMode = packingMode;
}

public String getShipNum() {
	return shipNum;
}

public void setShipNum(String shipNum) {
	this.shipNum = shipNum;
}

public int getRefreshContainer() {
	return refreshContainer;
}

public void setRefreshContainer(int refreshContainer) {
	this.refreshContainer = refreshContainer;
}
/**
 * @return the auditContainers
 */
public List getAuditContainers() {
	return auditContainers;
}
/**
 * @param auditContainers the auditContainers to set
 */
public void setAuditContainers(List auditContainers) {
	this.auditContainers = auditContainers;
}
/**
 * @return the hitFlag
 */
public String getHitFlag() {
	return hitFlag;
}
/**
 * @param hitFlag the hitFlag to set
 */
public void setHitFlag(String hitFlag) {
	this.hitFlag = hitFlag;
}
public Long getSoId() {
	return soId;
}
public void setSoId(Long soId) {
	this.soId = soId;
}
/**
 * @return the weightType
 */
public String getWeightType() {
	return weightType;
}
/**
 * @param weightType the weightType to set
 */
public void setWeightType(String weightType) {
	this.weightType = weightType;
}
public String getCountChild() {
	return countChild;
}
public void setCountChild(String countChild) {
	this.countChild = countChild;
}
public String getMaxChild() {
	return maxChild;
}
public void setMaxChild(String maxChild) {
	this.maxChild = maxChild;
}
public String getMinChild() {
	return minChild;
}
public void setMinChild(String minChild) {
	this.minChild = minChild;
}
public List getContaiSO() {
	return contaiSO;
}
public void setContaiSO(List contaiSO) {
	this.contaiSO = contaiSO;
}
public Long getSidNum() {
	return sidNum;
}
public void setSidNum(Long sidNum) {
	this.sidNum = sidNum;
}
public Long getSoIdNum() {
	return soIdNum;
}
public void setSoIdNum(Long soIdNum) {
	this.soIdNum = soIdNum;
}
public long getTempcontai() {
	return tempcontai;
}
public void setTempcontai(long tempcontai) {
	this.tempcontai = tempcontai;
}
public Boolean getUnitType() {
	return UnitType;
}
public void setUnitType(Boolean unitType) {
	UnitType = unitType;
}
public Boolean getVolumeType() {
	return VolumeType;
}
public void setVolumeType(Boolean volumeType) {
	VolumeType = volumeType;
}
public String getCountBondedGoods() {
	return countBondedGoods;
}
public void setCountBondedGoods(String countBondedGoods) {
	this.countBondedGoods = countBondedGoods;
}
public void setCustomManager(CustomManager customManager) {
	this.customManager = customManager;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public List getFieldsInfo() {
	return fieldsInfo;
}
public void setFieldsInfo(List fieldsInfo) {
	this.fieldsInfo = fieldsInfo;
}
public Boolean getAlreadyExists() {
	return alreadyExists;
}
public void setAlreadyExists(Boolean alreadyExists) {
	this.alreadyExists = alreadyExists;
}
public String getUpdateRecords() {
	return updateRecords;
}
public void setUpdateRecords(String updateRecords) {
	this.updateRecords = updateRecords;
}
public String getUsedVolume() {
	return usedVolume;
}
public void setUsedVolume(String usedVolume) {
	this.usedVolume = usedVolume;
}
public Billing getBilling() {
	return billing;
}
public void setBilling(Billing billing) {
	this.billing = billing;
}
public void setBillingManager(BillingManager billingManager) {
	this.billingManager = billingManager;
}
public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
	this.toDoRuleManager = toDoRuleManager;
}

public void setCustomerFileAction(CustomerFileAction customerFileAction) {
	this.customerFileAction = customerFileAction;
}
public void setIntegrationLogInfoManager(
		IntegrationLogInfoManager integrationLogInfoManager) {
	this.integrationLogInfoManager = integrationLogInfoManager;
}
public String getVesselFilight() {
	return vesselFilight;
}
public void setVesselFilight(String vesselFilight) {
	this.vesselFilight = vesselFilight;
}
public Company getCompany() {
	return company;
}
public CompanyManager getCompanyManager() {
	return companyManager;
}
public String getVoxmeIntergartionFlag() {
	return voxmeIntergartionFlag;
}
public void setCompany(Company company) {
	this.company = company;
}
public void setCompanyManager(CompanyManager companyManager) {
	this.companyManager = companyManager;
}
public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
	this.voxmeIntergartionFlag = voxmeIntergartionFlag;
}
public String getUsertype() {
	return usertype;
}
public void setUsertype(String usertype) {
	this.usertype = usertype;
}
public Boolean getSurveyTab() {
	return surveyTab;
}
public void setSurveyTab(Boolean surveyTab) {
	this.surveyTab = surveyTab;
}
public List getCartons() {
	return cartons;
}
public void setCartons(List cartons) {
	this.cartons = cartons;
}
public List getVehicles() {
	return vehicles;
}
public void setVehicles(List vehicles) {
	this.vehicles = vehicles;
}
public List getServicePartnerss() {
	return servicePartnerss;
}
public void setServicePartnerss(List servicePartnerss) {
	this.servicePartnerss = servicePartnerss;
}
public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
	this.servicePartnerManager = servicePartnerManager;
}
public List getConsigneeInstructionList() {
	return consigneeInstructionList;
}
public void setConsigneeInstructionList(List consigneeInstructionList) {
	this.consigneeInstructionList = consigneeInstructionList;
}
public ConsigneeInstruction getConsigneeInstruction() {
	return consigneeInstruction;
}
public void setConsigneeInstruction(ConsigneeInstruction consigneeInstruction) {
	this.consigneeInstruction = consigneeInstruction;
}
public List getConsigneeShiperList() {
	return consigneeShiperList;
}
public void setConsigneeShiperList(List consigneeShiperList) {
	this.consigneeShiperList = consigneeShiperList;
}
public String getConsigneeInstructionCode() {
	return consigneeInstructionCode;
}
public void setConsigneeInstructionCode(String consigneeInstructionCode) {
	this.consigneeInstructionCode = consigneeInstructionCode;
}
public List getConsigneeShipmentLocationList() {
	return consigneeShipmentLocationList;
}
public void setConsigneeShipmentLocationList(List consigneeShipmentLocationList) {
	this.consigneeShipmentLocationList = consigneeShipmentLocationList;
}
public String getsACode() {
	return sACode;
}
public void setsACode(String sACode) {
	this.sACode = sACode;
}
public String getOsACode() {
	return osACode;
}
public void setOsACode(String osACode) {
	this.osACode = osACode;
}
public void setConsigneeInstructionManager(
		ConsigneeInstructionManager consigneeInstructionManager) {
	this.consigneeInstructionManager = consigneeInstructionManager;
}
public Map<String, String> getSpecific() {
	if(serviceOrder !=null && serviceOrder.getId()!=null && serviceOrder.getShipNumber()!=null && consigneeInstruction.getConsignmentInstructions()!=null && !consigneeInstruction.getConsignmentInstructions().equals(""))
	{
	if(!specific.containsValue(consigneeInstruction.getConsignmentInstructions()) && !specific.containsKey(consigneeInstruction.getConsignmentInstructions()))
	{specific.put(consigneeInstruction.getConsignmentInstructions(), consigneeInstruction.getConsignmentInstructions());
	}
	}
	return specific;
}
public void setSpecific(Map<String, String> specific) {
	this.specific = specific;
}
public String getFieldName() {
	return fieldName;
}
public void setFieldName(String fieldName) {
	this.fieldName = fieldName;
}
public String getFieldValue() {
	return fieldValue;
}
public void setFieldValue(String fieldValue) {
	this.fieldValue = fieldValue;
}
public String getTableName() {
	return tableName;
}
public void setTableName(String tableName) {
	this.tableName = tableName;
}
public static List getContainerNumberListVehicle() {
	return containerNumberListVehicle;
}
public static void setContainerNumberListVehicle(List containerNumberListVehicle) {
	ContainerAction.containerNumberListVehicle = containerNumberListVehicle;
}
public void setVehicleManager(VehicleManager vehicleManager) {
	this.vehicleManager = vehicleManager;
}
public List getContainerNumberListCarton() {
	return containerNumberListCarton;
}
public void setContainerNumberListCarton(List containerNumberListCarton) {
	this.containerNumberListCarton = containerNumberListCarton;
}
public List getContainerNumberListRouting() {
	return containerNumberListRouting;
}
public void setContainerNumberListRouting(List containerNumberListRouting) {
	this.containerNumberListRouting = containerNumberListRouting;
}
public List getCartonTypeValue() {
	return cartonTypeValue;
}
public void setCartonTypeValue(List cartonTypeValue) {
	this.cartonTypeValue = cartonTypeValue;
}
public String getShipNumber() {
	return shipNumber;
}
public void setShipNumber(String shipNumber) {
	this.shipNumber = shipNumber;
}
public boolean isFutureDateFlag() {
	return futureDateFlag;
}
public void setFutureDateFlag(boolean futureDateFlag) {
	this.futureDateFlag = futureDateFlag;
}
public Vehicle getVehicle() {
	return vehicle;
}
public void setVehicle(Vehicle vehicle) {
	this.vehicle = vehicle;
}
public ServicePartner getServicePartner() {
	return servicePartner;
}
public void setServicePartner(ServicePartner servicePartner) {
	this.servicePartner = servicePartner;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getFieldNetWeight() {
	return fieldNetWeight;
}
public void setFieldNetWeight(String fieldNetWeight) {
	this.fieldNetWeight = fieldNetWeight;
}
public String getFieldNetWeightVal() {
	return fieldNetWeightVal;
}
public void setFieldNetWeightVal(String fieldNetWeightVal) {
	this.fieldNetWeightVal = fieldNetWeightVal;
}
public String getFieldGrossWeight() {
	return fieldGrossWeight;
}
public void setFieldGrossWeight(String fieldGrossWeight) {
	this.fieldGrossWeight = fieldGrossWeight;
}
public String getFieldGrossWeightVal() {
	return fieldGrossWeightVal;
}
public void setFieldGrossWeightVal(String fieldGrossWeightVal) {
	this.fieldGrossWeightVal = fieldGrossWeightVal;
}
public String getFieldEmptyConWeight() {
	return fieldEmptyConWeight;
}
public void setFieldEmptyConWeight(String fieldEmptyConWeight) {
	this.fieldEmptyConWeight = fieldEmptyConWeight;
}
public String getFieldEmptyConWeightVal() {
	return fieldEmptyConWeightVal;
}
public void setFieldEmptyConWeightVal(String fieldEmptyConWeightVal) {
	this.fieldEmptyConWeightVal = fieldEmptyConWeightVal;
}
public String getFieldNetWeightKilo() {
	return fieldNetWeightKilo;
}
public void setFieldNetWeightKilo(String fieldNetWeightKilo) {
	this.fieldNetWeightKilo = fieldNetWeightKilo;
}
public String getFieldNetWeightKiloVal() {
	return fieldNetWeightKiloVal;
}
public void setFieldNetWeightKiloVal(String fieldNetWeightKiloVal) {
	this.fieldNetWeightKiloVal = fieldNetWeightKiloVal;
}
public String getFieldGrossWeightKilo() {
	return fieldGrossWeightKilo;
}
public void setFieldGrossWeightKilo(String fieldGrossWeightKilo) {
	this.fieldGrossWeightKilo = fieldGrossWeightKilo;
}
public String getFieldGrossWeightKiloVal() {
	return fieldGrossWeightKiloVal;
}
public void setFieldGrossWeightKiloVal(String fieldGrossWeightKiloVal) {
	this.fieldGrossWeightKiloVal = fieldGrossWeightKiloVal;
}
public String getFieldemptyContWeightKilo() {
	return fieldemptyContWeightKilo;
}
public void setFieldemptyContWeightKilo(String fieldemptyContWeightKilo) {
	this.fieldemptyContWeightKilo = fieldemptyContWeightKilo;
}
public String getFieldemptyContWeightKiloVal() {
	return fieldemptyContWeightKiloVal;
}
public void setFieldemptyContWeightKiloVal(String fieldemptyContWeightKiloVal) {
	this.fieldemptyContWeightKiloVal = fieldemptyContWeightKiloVal;
}
public String getFieldVolumeCbm() {
	return fieldVolumeCbm;
}
public void setFieldVolumeCbm(String fieldVolumeCbm) {
	this.fieldVolumeCbm = fieldVolumeCbm;
}
public String getFieldVolumeCbmVal() {
	return fieldVolumeCbmVal;
}
public void setFieldVolumeCbmVal(String fieldVolumeCbmVal) {
	this.fieldVolumeCbmVal = fieldVolumeCbmVal;
}
public String getFieldDensity() {
	return fieldDensity;
}
public void setFieldDensity(String fieldDensity) {
	this.fieldDensity = fieldDensity;
}
public String getFieldDensityVal() {
	return fieldDensityVal;
}
public void setFieldDensityVal(String fieldDensityVal) {
	this.fieldDensityVal = fieldDensityVal;
}
public String getFieldDensityMetric() {
	return fieldDensityMetric;
}
public void setFieldDensityMetric(String fieldDensityMetric) {
	this.fieldDensityMetric = fieldDensityMetric;
}
public String getFieldDensityMetricVal() {
	return fieldDensityMetricVal;
}
public void setFieldDensityMetricVal(String fieldDensityMetricVal) {
	this.fieldDensityMetricVal = fieldDensityMetricVal;
}
public String getFieldVolume() {
	return fieldVolume;
}
public void setFieldVolume(String fieldVolume) {
	this.fieldVolume = fieldVolume;
}
public String getFieldVolumeVal() {
	return fieldVolumeVal;
}
public void setFieldVolumeVal(String fieldVolumeVal) {
	this.fieldVolumeVal = fieldVolumeVal;
}
public List getCustoms() {
	return customs;
}
public void setCustoms(List customs) {
	this.customs = customs;
}
public String getContainerStatus() {
	return containerStatus;
}
public void setContainerStatus(String containerStatus) {
	this.containerStatus = containerStatus;
}
public List getInlandAgentList() {
	return inlandAgentList;
}
public void setInlandAgentList(List inlandAgentList) {
	this.inlandAgentList = inlandAgentList;
}
public InlandAgentManager getInlandAgentManager() {
	return inlandAgentManager;
}
public void setInlandAgentManager(InlandAgentManager inlandAgentManager) {
	this.inlandAgentManager = inlandAgentManager;
}
public InlandAgent getInlandAgent() {
	return inlandAgent;
}
public void setInlandAgent(InlandAgent inlandAgent) {
	this.inlandAgent = inlandAgent;
}
public List getInlandAgentsList() {
	return inlandAgentsList;
}
public void setInlandAgentsList(List inlandAgentsList) {
	this.inlandAgentsList = inlandAgentsList;
}
public List getContainerNumberListInlandAgent() {
	return containerNumberListInlandAgent;
}
public void setContainerNumberListInlandAgent(
		List containerNumberListInlandAgent) {
	this.containerNumberListInlandAgent = containerNumberListInlandAgent;
}
public BrokerDetails getBrokerDetails() {
	return brokerDetails;
}
public void setBrokerDetails(BrokerDetails brokerDetails) {
	this.brokerDetails = brokerDetails;
}
public BrokerDetailsManager getBrokerDetailsManager() {
	return brokerDetailsManager;
}
public void setBrokerDetailsManager(BrokerDetailsManager brokerDetailsManager) {
	this.brokerDetailsManager = brokerDetailsManager;
}
public List getBrokerDetailsList() {
	return brokerDetailsList;
}
public void setBrokerDetailsList(List brokerDetailsList) {
	this.brokerDetailsList = brokerDetailsList;
}
public List getBrokerExamDetailsList() {
	return brokerExamDetailsList;
}
public void setBrokerExamDetailsList(List brokerExamDetailsList) {
	this.brokerExamDetailsList = brokerExamDetailsList;
}
public void setBrokerExamDetailsManager(
		BrokerExamDetailsManager brokerExamDetailsManager) {
	this.brokerExamDetailsManager = brokerExamDetailsManager;
}
public Map<String, String> getRefExamPay() {
	return refExamPay;
}
public void setRefExamPay(Map<String, String> refExamPay) {
	this.refExamPay = refExamPay;
}
public Set getLinkedshipNumber() {
	return linkedshipNumber;
}
public void setLinkedshipNumber(Set linkedshipNumber) {
	this.linkedshipNumber = linkedshipNumber;
}
public void setCompanyDivisionManager(CompanyDivisionManager companyDivisionManager) {
	this.companyDivisionManager = companyDivisionManager;
}
public String getMsgClicked() {
	return msgClicked;
}
public void setMsgClicked(String msgClicked) {
	this.msgClicked = msgClicked;
}
public String getCaed() {
	return caed;
}
public void setCaed(String caed) {
	this.caed = caed;
}
public Map<String, String> getEQUIP_isactive() {
	return EQUIP_isactive;
}
public void setEQUIP_isactive(Map<String, String> eQUIP_isactive) {
	EQUIP_isactive = eQUIP_isactive;
}
public Map<String, String> getSpecific_isactive() {
	return specific_isactive;
}
public void setSpecific_isactive(Map<String, String> specific_isactive) {
	this.specific_isactive = specific_isactive;
}
public String getBrokerCode() {
	return brokerCode;
}
public void setBrokerCode(String brokerCode) {
	this.brokerCode = brokerCode;
}
public String getOiJobList() {
	return oiJobList;
}
public void setOiJobList(String oiJobList) {
	this.oiJobList = oiJobList;
}
public String getDashBoardHideJobsList() {
	return dashBoardHideJobsList;
}
public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
	this.dashBoardHideJobsList = dashBoardHideJobsList;
}




}

//End of Class.   