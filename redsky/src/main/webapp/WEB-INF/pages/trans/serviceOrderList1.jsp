<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
function addServiceOrder(){
			var listSize = '${serviceOrdersSize}';
				if(listSize == 99){
					alert('Maximum limit of service order is 99 for a customer.');
				  return false;  
				}
				
			var url = "editServiceOrder.html";	 
			document.forms['serviceOrderListForm'].action= url;	 
 			document.forms['serviceOrderListForm'].submit();
			return true;
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>
<script>
function goToCustomerDetail(targetValue){
        document.forms['serviceOrderListForm'].elements['id'].value = targetValue;
        document.forms['serviceOrderListForm'].action = 'editTrackingStatus.html?from=list';
        document.forms['serviceOrderListForm'].submit();
}
</script>
<script type="text/javascript">
function requestedQuotes(targetElement,bookingAgentCode,billToCode,evt,soCompanyDivision){
	   var bookingAgent=bookingAgentCode;
	   var billToCode=billToCode;
	   if(bookingAgent==null||bookingAgent==undefined){  bookingAgent="";	  }
	   if(billToCode==null||billToCode==undefined){	  billToCode="";  }
	   var requestedAQP = evt.value;
	   var id = evt.id;
	   id=id.replace("quoteAccept",""); 
	   id=id=id.trim();
	   var targetElement=targetElement;
	   var closedCompanyDivisionVar=""; 
	   closedCompanyDivisionVar ="${closeCompanyDivisionList}" 
	   if(closedCompanyDivisionVar.indexOf('${customerFile.companyDivision}')>=0 || closedCompanyDivisionVar.indexOf(soCompanyDivision)>=0) {
	   if(requestedAQP=='A'){
		alert("Quote cannot be accepted as Company Division is closed.");
		document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
	 }else{
		 requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id); 
	 }
	   } 
	 if(closedCompanyDivisionVar.indexOf('${customerFile.companyDivision}')<0 && closedCompanyDivisionVar.indexOf(soCompanyDivision)<0) { 
	 if(requestedAQP=='A'){
		  if((bookingAgent=='')&&(billToCode=='' || billToCode!='')){
			  alert("Booking Agent code not valid");
			  document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
		  }else{
			  var billToCode1=billToCode;
		  		if(billToCode1==''){
		  			billToCode1="Invalid";
		  		}
			  	var bookCode=bookingAgent+"~"+billToCode1;
		    	var url="findBookCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
				http50.open("GET", url, true);
				http50.onreadystatechange = function(){handleHttpResponse50(targetElement,bookingAgent,billToCode,requestedAQP,id)};
				http50.send(null);
		  }
	    }else{
	        requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id); 
	    }
	 }
}
function requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id){
    var targetElement=targetElement;
    var requestedAQP =requestedAQP;
  		 if(requestedAQP=='M' ){
  		 	mergeScript(targetElement);
  		 }else{	
  			if(requestedAQP=='A'){
  				type='Accept';
 			}else if(requestedAQP=='R'){
  				type='Reject';
  			}else if(requestedAQP=='P' ){
    			 type='Pending';
    	    }else if(requestedAQP=='M' ){
      			 mergeScript(targetElement);
      			 var type='Merge';
      		}else if( requestedAQP==''){
       		     alert("You can not select blank value.");
                 document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
      		}
  			if(requestedAQP!=''){
  				var agree="";
  				var url='';
  				if(requestedAQP=='A'){
  					 agree = confirm("Do you want to change All SO status to "+type+"\n"+"Click ok to continue or cancel to apply on this record only.");
  					if(agree){ 	 				
  	 					document.getElementById('tempQuoteAccept'+id).value=requestedAQP;
  	   					soid=targetElement;
  	  				     <c:if test="${customerFile.controlFlag =='C'}">
  	     					url = "quoteAcceptanceForQuote.html?ajax=1&decorator=simple&popup=true&checkAccessQuotation=${checkAccessQuotation}&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id}+"&allIdOFSo=${allIdOFSo}+&updateTypeFlag="+"All+&listViewFlag=Y";
  		 				 </c:if>
  	  				  	 http52.open("GET", url, true);
  	   					 http52.onreadystatechange = handleHttpResponse52;
  	   				 	 http52.send(null);
  	  					}else{
	  	  					document.getElementById('tempQuoteAccept'+id).value=requestedAQP;
	  	   					  soid=targetElement;	  	    				    	    				 
	  	  				     <c:if test="${customerFile.controlFlag =='C'}">
	  	     					url = "quoteAcceptanceForQuote.html?ajax=1&decorator=simple&popup=true&checkAccessQuotation=${checkAccessQuotation}&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id}+"&listViewFlag=Y";
	  		 				 </c:if>
	  	  				  	 http52.open("GET", url, true);
	  	   					 http52.onreadystatechange = handleHttpResponse52;
	  	   				 	 http52.send(null);
  	      				}
  				}else{
  					 agree = confirm("Are you sure, You want to change status to "+type+"\n"+"Click ok to continue or cancel to reset.");  				
 				 if(agree){ 	 				
 					document.getElementById('tempQuoteAccept'+id).value=requestedAQP;
 					//showOrHideRejectReason();
   					  soid=targetElement;
    				  var url='';    			
  				     <c:if test="${customerFile.controlFlag =='C'}">
     					url = "quoteAcceptanceForQuote.html?ajax=1&decorator=simple&popup=true&checkAccessQuotation=${checkAccessQuotation}&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id}+"&listViewFlag=Y";
	 				 </c:if>
  				  	 http52.open("GET", url, true);
   					 http52.onreadystatechange = handleHttpResponse52;
   				 	 http52.send(null);
  					}else{
  						 document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
  						showOrHideRejectReason();
  						location.href=window.location;
      				}
  				}
 			}
   	   }	
}
function handleHttpResponse52(){
	if (http52.readyState == 4){
			var results = http52.responseText
			location.href=window.location;
			var url = window.location.href;
			if (url.indexOf('&listViewFlag=Y') > -1){
				}else{
					location.href = url +"&listViewFlag=Y";
				}			
            }
      }
function handleHttpResponse50(targetElement,bookingAgent,billToCode,requestedAQP,id){
	if (http50.readyState == 4) {
        var results = http50.responseText
        results = results.trim();
        if(results.length > 2) {
            var rec=results.split(",");
            var bookingAgent1=rec[0].split(":");
            var billToCode1=rec[1].split(":");
            if(bookingAgent1[1]=="Y"){
				if((billToCode=='')||(billToCode1[1]=="Y")){
	       		     requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id);       		        
				}else{
          			alert("Quote cannot be accepted as selected partners have not been approved yet");
             		document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
				}     		    	
            }else{
       			alert("Quote cannot be accepted as selected partners have not been approved yet");
       			document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;        
            }
        }
	}
}
function rejectedQuotesCust(targetElement, evt){
    var requestedAQP = evt.value;
   soid=targetElement;
   	 var url = "rejectedQuotesCust.html?ajax=1&decorator=simple&popup=true&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id};
	  	http555.open("GET", url, true);
    	http555.onreadystatechange = handleHttpResponseRej;
    	http555.send(null);   
   }
function handleHttpResponseRej(){
		if (http555.readyState == 4){
 			var results = http555.responseText
 		     }
}
var http50 = getHTTPObject();
var http52 = getHTTPObject();
var http555 = getHTTPObject();
function getHTTPObject(){
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
function mergeScript(targetElement){
	javascript:openWindow("mergeServiceOrdersForm.html?decorator=popup&popup=true&sequenceNumber=${customerFile.bookingAgentName}&bookingAgentName=${customerFile.bookingAgentName}&bookingAgentCode=${customerFile.bookingAgentCode}&quotesId="+targetElement,"height=200,width=500");
}
</script>
<script>
function showOrHideRejectReason(){
	var reject="F";
	<c:forEach items="${serviceOrders}" var="list1">
	var id='${list1.id}';
	var rejectTemp=document.getElementById('quoteAccept'+id).value;
	if(rejectTemp=='R'){
			reject="T";
	}
	</c:forEach>
	if(reject=="F"){
		$("#serviceOrderList tr").find('th:last, td:last').hide();
	}else{
		$("#serviceOrderList tr").find('th:last, td:last').show();
	}
	<c:forEach items="${serviceOrders}" var="list1">
	if(reject!='F'){
		var id='${list1.id}';
		var el=document.getElementById('tempStatusReason'+id);
		if(document.getElementById('quoteAccept'+id).value=='R'){
			el.style.display = 'block';
		}else{
			el.style.display = 'none';
		}
	}
	</c:forEach>
}

  </script>
</head>
<c:set var="buttons"> 
<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
<input type="button" class="cssbutton" style="width:55px; height:28px" onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" value="<fmt:message key="button.add"/>"/>   
</c:if> 
<c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>     
<input type="button" class="cssbutton" style="width:55px; height:28px" onclick="addServiceOrder();" value="<fmt:message key="button.add"/>"/>   
</c:if>
</c:set>   
<div id="Layer5" style="width:100%">	
	
		<div id="newmnav">
		  <ul>
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Service Orders<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  	</c:if>
		  	<%-- <c:if test="${salesPortalAccess=='false'}">
		    	<c:if test="${usertype!='ACCOUNT' && usertype!='AGENT'}">
		    	<!-- <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    	<li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    	<li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}&jobNumber=${customerFile.sequenceNumber}" ><span>Account Policy</span></a></li> 
		  		</c:if>
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&companyDivision=${customerFile.companyDivision}&jobType=${customerFile.job}&billToCode =${customerFile.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		  </c:if> --%>
		  		    <sec-auth:authComponent componentId="module.tab.customerFile.accountPolicyTab">
		    	<configByCorp:fieldVisibility componentId="component.tab.customerFile.AccountPolicy">		    	
		    			<c:if test="${ usertype!='AGENT'}"> <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}&cid=${customerFile.id}" ><span>Account Policy</span></a></li></c:if>
		    	</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.customerFile.reportTab">
		    <configByCorp:fieldVisibility componentId="component.tab.customerFile.Reports">
		    <c:if test="${customerFile.moveType=='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=quotation&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=quotation&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		   	<c:if test="${customerFile.moveType!='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=customerFile&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		    </configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    
		    <sec-auth:authComponent componentId="module.tab.customerFile.auditTab">
		   	    <c:if test="${not empty customerFile.id}"> 
		    <li>
              <a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
              <span>Audit</span></a></li>
		    </c:if>
		    <c:if test="${empty customerFile.id}">
		    	<li><a><span>Audit</span></a></li>
		    </c:if>
		    </sec-auth:authComponent>
		    <c:if test="${usertype=='USER'}">
			    <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
			  		<li><a href="findEmailSetupTemplateByModuleNameCF.html?cid=${customerFile.id}"><span>View Emails</span></a></li>
			  	</configByCorp:fieldVisibility>
		  	</c:if>
		  </ul>
		</div><div class="spn spnSF">&nbsp;</div>		 
<s:hidden name="noteFor" value="CustomerFile" />
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>	 
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/>  
<s:hidden name="allIdOFSo"/>
<s:hidden name="listViewFlag" value="<%=request.getParameter("listViewFlag")%>"/>
 <s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/> 
<s:form id="serviceOrderListForm" action="searchServiceOrders" method="post" > 
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:set var="idOfTasks" value="" scope="session"/>
<s:hidden name="cid" value="${customerFile.id}" />
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>  

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top: -14px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:780px">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssStyle="width:116px;" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true"  cssStyle="width:90px;" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" cssStyle="width: 116px;" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='quotationFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:if test="${quoteCustomerFilesatus!='Quote' && listViewFlag !='Y'}">
	<div id="otabs">
		<ul>
		  <li><a class="current"><span>Service Order List</span></a></li>
		</ul>
	</div>
	<div class="spnblk">&nbsp;</div> 
	<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="20" style="width:100%">   
		<%--<display:column sortable="true" sortProperty="shipNumber" titleKey="serviceOrder.shipNumber" style="width:75px"><a onclick="goToCustomerDetail(${serviceOrderList.id});" style="cursor:pointer"><c:out value="${serviceOrderList.shipNumber}" /></a></display:column>--%>
		   <s:textfield id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:textfield id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrderList.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
             <c:if test="${usertype!='ACCOUNT'}">
             <c:choose>
				<c:when test='${serviceOrderList.job=="RLO"}'>
            		<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl=='editTrackingStatus.html?from=list&id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="editDspDetails.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl!='editMiscellaneous.html?id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl=='editMiscellaneous.html?id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl==''}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				 <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
				</c:when>
				<c:otherwise>
				<c:choose>
					<c:when test='${serviceOrderList.job=="INT"}'>
						<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl=='editMiscellaneous.html?id=')}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
						<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editMiscellaneous.html?id=')}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
						<c:if test="${defaultSoTabUrl==''}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				 	<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
					</c:when>
					<c:otherwise>
					<c:choose>
						<c:when test='${serviceOrderList.corpID=="CWMS"}'>
							<c:if test='${serviceOrderList.job=="OFF"}'>
				 				<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               						<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 						<c:out value="${serviceOrderList.shipNumber}" /> 
                  					</a> 
                				</display:column>  
							</c:if>
							<c:if test='${defaultSoTabUrl!="" && serviceOrderList.job!="OFF"}'>
				 				<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               						<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 							<c:out value="${serviceOrderList.shipNumber}" />
                  					</a> 
                				</display:column>  
							</c:if>
							<c:if test='${defaultSoTabUrl==""}'>
				 					<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               							<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 							<c:out value="${serviceOrderList.shipNumber}" /> 
                  						</a> 
                					</display:column>  
							</c:if>
						</c:when>
							<c:otherwise>
								<c:if test='${defaultSoTabUrl!=""}'>
				 					<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               							<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 							<c:out value="${serviceOrderList.shipNumber}" />
                  						</a> 
                					</display:column>  
								</c:if>
								<c:if test='${defaultSoTabUrl==""}'>
				 					<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               							<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 							<c:out value="${serviceOrderList.shipNumber}" /> 
                  						</a> 
                					</display:column>  
								</c:if>
							</c:otherwise>
						</c:choose>	
					</c:otherwise>
				</c:choose>
				</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${usertype=='ACCOUNT'}">
			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <%-- <a href="findSummaryList.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a>  --%>
                  
                    <c:choose>
        <c:when test="${serviceOrderList.job=='OFF' && serviceOrderList.corpID=='CWMS'}">
         <a href="operationResourceFromAcPortal.html?id=${serviceOrderList.id}">
            <c:out value="${serviceOrderList.shipNumber}" />
        </a>
        </c:when>
        <c:otherwise>
        <a href="findSummaryList.html?id=${serviceOrderList.id}">
            <c:out value="${serviceOrderList.shipNumber}" />
        </a>
        </c:otherwise>
        </c:choose> 
                  
                </display:column>  
			</c:if> 
		<display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:50px;!width:100px;"/>  
		<display:column property="status" sortable="true" titleKey="serviceOrder.status" style="width:20px" />
		<display:column property="statusDate" sortable="true" titleKey="serviceOrder.statusDate" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
		<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
		<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
		<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/>   
		<display:column property="serviceType" sortable="true" title="Service" style="width:20px"/>  
		<display:column property="createdOn" sortable="true" titleKey="serviceOrder.createdOn" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  
		
		<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 		<display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
	    <display:setProperty name="paging.banner.items_name" value="orders"/>   
	    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
	    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
	    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
	</display:table>
	</c:if>
	<c:if test="${quoteCustomerFilesatus!='Quote' && listViewFlag =='Y'}">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Service Order List</span></a></li> 
		  </ul>
		</div>		
		<div class="spnblk" style="line-height:7px;">&nbsp;</div>
		<c:if test="${serviceOrders!=null && serviceOrders!='[]'}">
		<div align="right" style="margin:0px;margin-right:8px;font-size:11px;"><b>Note:</b> Quote status is saved automatically when changed.</div>
		</c:if>
<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="10" style="width:99%;!margin-top:-2px;margin-left:5px;">   
    <c:if test="${usertype!='ACCOUNT'}">
            <c:choose>
				<c:when test='${serviceOrderList.job=="RLO"}'>
            		<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl=='editTrackingStatus.html?from=list&id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="editDspDetails.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl!='editMiscellaneous.html?id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl=='editMiscellaneous.html?id=')}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
					<c:if test="${defaultSoTabUrl==''}">
				 		<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				 <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 				<c:out value="${serviceOrderList.shipNumber}" />
                  			</a> 
                		</display:column> 
					</c:if>
				</c:when>
				<c:otherwise>
				<c:choose>
					<c:when test='${serviceOrderList.job=="INT"}'>
						<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl=='editMiscellaneous.html?id=')}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
						<c:if test="${defaultSoTabUrl!='' && (defaultSoTabUrl!='editMiscellaneous.html?id=')}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
						<c:if test="${defaultSoTabUrl==''}">
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               				 	<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column> 
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test='${defaultSoTabUrl!=""}'>
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="${defaultSoTabUrl}${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" />
                  				</a> 
                			</display:column>  
						</c:if>
						<c:if test='${defaultSoTabUrl==""}'>
				 			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               					<a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 					<c:out value="${serviceOrderList.shipNumber}" /> 
                  				</a> 
                		</display:column>  
						</c:if>
					</c:otherwise>
				</c:choose>
				</c:otherwise>
				</c:choose>
			</c:if> 
			<c:if test="${usertype=='ACCOUNT'}">
			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="findSummaryList.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if>
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/> 
<display:column property="packingMode" sortable="true" titleKey="serviceOrder.packingMode" style="width:50px"/> 
<c:choose>
  <c:when test="${weightUnit == 'Lbs'}">
    <display:column property="estimateGrossWeight" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:when>
  <c:otherwise>
	 <display:column property="estimateGrossWeightKilo" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:otherwise>
 </c:choose>
<c:choose>
  <c:when test="${volumeUnit == 'Cft'}">
    <display:column property="estimateCubicFeet" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:when>
  <c:otherwise>
	 <display:column property="estimateCubicMtr" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:otherwise>
 </c:choose>

<display:column property="estimatedTotalExpense" sortable="true" title="Expense" style="width:50px"/>
<display:column property="estimatedTotalRevenue" sortable="true" title="Revenue" style="width:50px"/>
<display:column property="estimatedGrossMargin" sortable="true" title="Margin" style="width:20px"/> 

<display:column title="Quote Status" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" id="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this,'${serviceOrderList.companyDivision}');" class="list-menu"> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
</select> 
<s:hidden name="tempQuoteAccept${serviceOrderList.id}" id="tempQuoteAccept${serviceOrderList.id}" value="${serviceOrderList.quoteAccept}"/>
</display:column>
<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
	</c:if>
	<c:if test="${quoteCustomerFilesatus=='Quote' && listViewFlag  !='Y'}">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Quotes List</span></a></li> 
		  </ul>
		</div>		
		<div class="spnblk" style="line-height:7px;">&nbsp;</div>
		<c:if test="${serviceOrders!=null && serviceOrders!='[]'}">
		<div align="right" style="margin:0px;margin-right:8px;font-size:11px;"><b>Note:</b> Quote status is saved automatically when changed.</div>
		</c:if>
<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="10" style="width:99%;!margin-top:-2px;margin-left:5px;">   
    <c:if test="${usertype!='ACCOUNT'}">
            <c:if test="${serviceOrderList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="Quotes List" sortProperty="shipNumber">
               <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${serviceOrderList.job!='RLO'}">
           <display:column sortable="true" style="width:50px" title="Quotes List" sortProperty="shipNumber">
               <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if> 
			</c:if>
			<c:if test="${usertype=='ACCOUNT'}">
			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="findSummaryList.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if>
<display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:50px;!width:100px;"/>  
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/> 
<display:column property="packingMode" sortable="true" titleKey="serviceOrder.packingMode" style="width:50px"/> 
<c:choose>
  <c:when test="${weightUnit == 'Lbs'}">
    <display:column property="estimateGrossWeight" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:when>
  <c:otherwise>
	 <display:column property="estimateGrossWeightKilo" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:otherwise>
 </c:choose>
<c:choose>
  <c:when test="${volumeUnit == 'Cft'}">
    <display:column property="estimateCubicFeet" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:when>
  <c:otherwise>
	 <display:column property="estimateCubicMtr" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:otherwise>
 </c:choose>

<display:column property="estimatedTotalExpense" sortable="true" title="Expense" style="width:50px"/>
<display:column property="estimatedTotalRevenue" sortable="true" title="Revenue" style="width:50px"/>
<display:column property="estimatedGrossMargin" sortable="true" title="Margin" style="width:20px"/> 

<display:column title="Quote Status" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" id="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this,'${serviceOrderList.companyDivision}');" class="list-menu"> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
</select> 
<s:hidden name="tempQuoteAccept${serviceOrderList.id}" id="tempQuoteAccept${serviceOrderList.id}" value="${serviceOrderList.quoteAccept}"/>
</display:column>
<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
	</c:if>
		<c:if test="${quoteCustomerFilesatus=='Quote' && listViewFlag =='Y'}">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Quotes List</span></a></li> 
		  </ul>
		</div>		
		<div class="spnblk" style="line-height:7px;">&nbsp;</div>
		<c:if test="${serviceOrders!=null && serviceOrders!='[]'}">
		<div align="right" style="margin:0px;margin-right:8px;font-size:11px;"><b>Note:</b> Quote status is saved automatically when changed.</div>
		</c:if>
<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="10" style="width:99%;!margin-top:-2px;margin-left:5px;">   
    <c:if test="${usertype!='ACCOUNT'}">
            <c:if test="${serviceOrderList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="Quotes List" sortProperty="shipNumber">
               <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${serviceOrderList.job!='RLO'}">
           <display:column sortable="true" style="width:50px" title="Quotes List" sortProperty="shipNumber">
               <a href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if> 
			</c:if>
			<c:if test="${usertype=='ACCOUNT'}">
			<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="findSummaryList.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if>
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/> 
<display:column property="packingMode" sortable="true" titleKey="serviceOrder.packingMode" style="width:50px"/> 
<c:choose>
  <c:when test="${weightUnit == 'Lbs'}">
    <display:column property="estimateGrossWeight" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:when>
  <c:otherwise>
	 <display:column property="estimateGrossWeightKilo" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:otherwise>
 </c:choose>
<c:choose>
  <c:when test="${volumeUnit == 'Cft'}">
    <display:column property="estimateCubicFeet" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:when>
  <c:otherwise>
	 <display:column property="estimateCubicMtr" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:otherwise>
 </c:choose>

<display:column property="estimatedTotalExpense" sortable="true" title="Expense" style="width:50px"/>
<display:column property="estimatedTotalRevenue" sortable="true" title="Revenue" style="width:50px"/>
<display:column property="estimatedGrossMargin" sortable="true" title="Margin" style="width:20px"/> 

<display:column title="Quote Status" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" id="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this,'${serviceOrderList.companyDivision}');" class="list-menu"> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
</select> 
<s:hidden name="tempQuoteAccept${serviceOrderList.id}" id="tempQuoteAccept${serviceOrderList.id}" value="${serviceOrderList.quoteAccept}"/>
</display:column>
<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
	</c:if>
</div>

<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<s:hidden name="componentId" value="module.serviceOrder" /> 
<sec-auth:authComponent componentId="module.serviceOrder.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
<c:if test="${not empty customerFile.sequenceNumber}">
	<c:out value="${buttons}" escapeXml="false" />
</c:if>
</sec-auth:authComponent>
<c:if test="${empty customerFile.sequenceNumber}">
	<c:set var="isTrue" value="false" scope="application"/>
</c:if>
<s:hidden name="id"/>
</s:form>
<script type="text/javascript">
<c:if test="${serviceOrders!=null && serviceOrders!='[]' && quoteCustomerFilesatus=='Quote'}">
showOrHideRejectReason();
</c:if>
</script> 

<script type="text/javascript">
<c:choose>
	<c:when test="${(serviceOrdersSize==1 || serviceOrdersSize=='1') && defaultSoTabUrl!=''}">
			<c:if test="${serviceOrderList.job=='RLO'}">
				<c:if test="${defaultSoTabUrl=='editTrackingStatus.html?from=list&id='}">
					<c:redirect url="/editDspDetails.html?id=${serviceOrderList.id}" ></c:redirect>
				</c:if>
				<c:if test="${defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl!='editMiscellaneous.html?id='}">
					<c:redirect url="/${defaultSoTabUrl}${serviceOrderList.id}" ></c:redirect>
				</c:if>
				<c:if test="${defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl=='editMiscellaneous.html?id='}">
					<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrderList.id}" ></c:redirect>
				</c:if>
				<c:if test="${defaultSoTabUrl!='editTrackingStatus.html?from=list&id=' && defaultSoTabUrl=='editMiscellaneous.html?id='}">
					<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrderList.id}" ></c:redirect>
				</c:if>
			</c:if>
			<c:if test="${serviceOrderList.job=='INT'}">
				<c:if test="${defaultSoTabUrl=='editMiscellaneous.html?id='}">
					<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrderList.id}" ></c:redirect>
				</c:if>
				<c:if test="${defaultSoTabUrl!='editMiscellaneous.html?id='}">
					<c:redirect url="/${defaultSoTabUrl}${serviceOrderList.id}" ></c:redirect>
				</c:if>
			</c:if>
			<c:if test="${serviceOrderList.job!='INT' && serviceOrderList.job!='RLO'}">
				<c:if test="${defaultSoTabUrl=='editMiscellaneous.html?id='}">
					<c:redirect url="/${defaultSoTabUrl}${serviceOrderList.id}" ></c:redirect>
				</c:if>
				<c:if test="${defaultSoTabUrl!='editMiscellaneous.html?id='}">
					<c:redirect url="/${defaultSoTabUrl}${serviceOrderList.id}" ></c:redirect>
				</c:if>
			</c:if>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${(serviceOrdersSize==1 || serviceOrdersSize=='1') && defaultSoTabUrl==''}">
					<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrderList.id}" ></c:redirect>
				</c:when>
			</c:choose>
		</c:otherwise>
</c:choose>
</script> 