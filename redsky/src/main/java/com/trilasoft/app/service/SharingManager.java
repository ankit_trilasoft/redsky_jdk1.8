package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Sharing;


public interface SharingManager extends GenericManager<Sharing, Long>{
	
	public List findById(Long id);
	public List<Sharing> searchSharing(String code, String discription);
	public List findMaximumId();
}
