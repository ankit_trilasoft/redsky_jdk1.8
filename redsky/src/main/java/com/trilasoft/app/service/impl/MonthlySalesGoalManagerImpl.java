package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MonthlySalesGoalDao;
import com.trilasoft.app.model.MonthlySalesGoal;
import com.trilasoft.app.service.MonthlySalesGoalManager;


public class MonthlySalesGoalManagerImpl extends GenericManagerImpl<MonthlySalesGoal, Long> implements MonthlySalesGoalManager{
	
	MonthlySalesGoalDao monthlySalesGoalDao; 
	
	public MonthlySalesGoalManagerImpl(MonthlySalesGoalDao monthlySalesGoalDao) {   
        super(monthlySalesGoalDao);   
        this.monthlySalesGoalDao = monthlySalesGoalDao;   
    }
	
	public MonthlySalesGoal getMonthlySalesGoalDetails(String partnerCode,String sessionCorpID,String yearVal){
		return monthlySalesGoalDao.getMonthlySalesGoalDetails(partnerCode, sessionCorpID, yearVal);
	}
}
