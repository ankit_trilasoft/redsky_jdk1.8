<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@page import="java.util.Iterator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="heading" content="<fmt:message key='toDoRule.heading'/>"/> 
<title>ToDo Rule</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1; <fmt:message key='toDoRule.heading'/>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/comman.css'/>" />
  <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/saverules.css'/>" />
  <style>
 
  <%@ include file="/styles/comman.css"%> 
    <%@ include file="/styles/saveRules.css"%>
         </style>
        
         
         <style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		

		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 20.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		
	</style>
         
</head>
<body>
<s:textfield name="actionList" value="${actionList}"/>
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>ToDo Rule<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="toDoRuleLists.html"><span>Rule List</span></a></li>
		<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${toDoRule.id}&tableName=todorule&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
  </ul>
		</div><div class="spn">&nbsp;</div>
<div class="row ml-0 mr-0 to-dolist">    
                              <div class="col-md-12">                              
                                    <div class="card rules border-b">
                                             <div class="card-header clearfix">
                                                   <h6 class="float-left mt-1">Rules</h6>
                                                    <button type="button" class="float-right btn btn-sm" data-toggle="modal" data-target="#exampleModal">Condition Field</button>
                                                   <div class="btn-group float-right mr-2">
                                                    <button type="button" class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Action
                                                    </button>
                                                    <div class="dropdown-menu">
                                                      <a class="dropdown-item" href="#">Copy</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" onClick="return btntype(this);">Test Condition</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="#">Execute This Rule</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="#">Add New Rules</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="#">Copy Rule</a>
                                                      
                                                     
                                                    </div>
                                                  </div>
                                             </div>
                                             <div class="card-body row">
                                                
                                                     <div class="col-md-7">                                                       
                                                        <s:form id="toDoRule" name="toDoRule" action="saveRulesNew" method="post" validate="true">                                                            <div class="row">
                                                                  <div class="col-md-3">
                                                                      <label class="label-heading">Category</label>
                                                                  </div>

                                                                  <div class="col-md-6">
                                                                      <s:select cssClass="list-menu" id="entity" name="toDoRule.entitytablerequired" list="%{pentity}" headerKey="" headerValue="" cssStyle="width:100px" onchange="getFieldList(this);getEmailList(this);"/>
                                                                  </div>

                                                                  <div class="col-md-2">
                                                                      <label class="label-heading">Enable</label>
                                                                  </div>
                                                                  <c:set var="ischecked" value="false"/>
				                                                  <c:if test="${toDoRule.checkEnable}">
				                                                  <c:set var="ischecked" value="true"/>
				                                                  </c:if>
                                                                  <div class="col-md-1">
                                                              
                                                                      <s:checkbox key="toDoRule.checkEnable" value="${ischecked}" fieldValue="true" onclick="changeStatus();"/>                        </div>
                                                             </div>

                                                             <div class="row">
                                                              <div class="col-md-3">
                                                                <label class="label-heading">Control Date	</label>
                                                              </div>

                                                              <div class="col-md-3">
                                                             <s:select cssClass="list-menu"  id="" list="%{selectedDateField}"  cssStyle="width:100%;" headerKey="" headerValue="" name="toDoRule.testdate" onchange="changeStatus();"/> 
                                                          </div>

                                                          <div class="col-md-3 pl-0 pr-0">
                                                      
                                                            <label class="label-heading""> is <span>less than equal to +/- days</span> <s:textfield name="toDoRule.durationAddSub" required="true" maxlength="4" size="1"  onkeydown="return onlyNumsAllowed(event)" onkeyup="shiftCode(event)"  cssClass="input-text" readonly="false" cssStyle="width:20px" /> from today</label>
                                                        </div>

                                                            <div class="col-md-2">
                                                            
                                                                <label class="label-heading">Status</label>
                                                                 </div>
                                                                 <div class="col-md-1">
                                                                <c:if test="${toDoRule.status!='Error'}">
		  		                                                <td align="left"><s:textfield name="toDoRule.status" size="4"  cssClass="status green"  readonly="true"/></td>
		  		                                                </c:if>
		  		                                                <c:if test="${toDoRule.status=='Error'}">
		  		                                                <td align="left"><s:textfield name="toDoRule.status" size="4" cssClass="status green"  readonly="true"/></td>
		  		                                                </c:if>
		  		                                                </div>
                                                           
                                                           
                                                             
                                                         </div>
                                                               
                                                              

                                                               <div class="row">
                                                                     <div class="col-md-3">
                                                                           <label class="label-heading">Field to Validate #1</label>
                                                                       
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                      <s:textfield name="toDoRule.fieldToValidate1"  maxlength="75" size="72"  cssClass="input-text" readonly="false" onchange="getDescription(this)"/>
                                                                      
                                                                      </div>
                                                                    
                                                               </div>
                                                                
                                                               <div class="row">
                                                                  
                                                                  <div class="col-md-3">
                                                                     <label class="label-heading">Field to Validate #2</label>
                                                                 
                                                               </div>
                                                               <div class="col-md-6">
                                                                
                                                             <s:textfield name="toDoRule.fieldToValidate2"  maxlength="50" size="30"  cssClass="input-text" readonly="false"/> </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3"> 
                                                                      <label class="label-heading">Display Field	</label>
                                                                 
                                                                </div>

                                                                <div class="col-md-9">
                                                                        <s:textfield name="toDoRule.fieldDisplay"  maxlength="75" size="72"  cssClass="input-text" readonly="false"/>
                                                                </div>
                                                               
                                                          </div>
                                                           
                                                          <!-- <div class="row">
                                                              <div class="col-md-12">
                                                                    <button type="button" class="font-weight-semibold" data-toggle="modal" data-target="#exampleModal">Condition Field</button>
                                                              </div>
                                                              
                                                        </div> -->
                                                              
                                                        <!-- <div class="row mt-4">
                                                          <div class="col">
                                                             <button type="button " class="btn default blue-stripe">Copy</button>
                                                          </div>
                                                          <div class="col">
                                                              <button type="button "  class="btn default blue-stripe">Test Condition</button>
                                                           </div>
                                                           <div class="col">
                                                              <button type="button "  class="btn default blue-stripe">Execute This Rule</button>
                                                           </div>
                                                        </div> -->

                                                              
                                                         </s:form>

                                                        </div>

                                                        <div class="col-md-5">                                                       
                                                            
                                                                 
                                                              
                                                               
                                                                   
                                                                   <!-- <div class="row">
                                                              <div class="col-md-12">
                                                                    <button type="button" class="font-weight-semibold btn default" data-toggle="modal" data-target="#exampleModal">Condition Field</button>
                                                              </div>
                                                              
                                                        </div> -->
    
                                                                    
                                                                  
                                                                  
    
                                                                   <div class="row">
                                                                         
                                                                         <div class="col-md-12">
                                                                               <label for="exampleFormControlTextarea1" class="label-heading"> Validation Condition*</label>
                                                                               <s:textarea cssClass="form-control" name="toDoRule.expression"  rows="10" cols="90" readonly="false"/>
                                                                              
                                                                             </div>
                                                                              
                                                                   </div>
                                                            
    
                                                            </div>

                                                       
                                                       



                                                   
                                               
                                             </div>
                                    </div>
                              </div>

                              <div class="col-md-12">                              
                                    <!--<div class="card border-b">
                                             <div class="card-header">
                                                 <h6>Action</h6>
                                             </div>
                                             <div class="card-body">
                                                <div class="row">
                                                  <div class="col-md-6">    

                                               <div class="row">
                                                  <div class="col-md-3">
                                                        <label class="label-heading eclipse-text">Message to give the user*	</label>
                                                   
                                                  </div>

                                                  <div class="col-md-9">
                                                     <label class=" ml-1">Estimated Net Volume missing 5 days before begin load (#1)	</label>
                                               </div>
                                                 
                                            </div>

                                             

                                             <div class="row">
                                                <div class="col-md-3">
                                                    <label class="label-heading eclipse-text">Then,add the record to the list of the</label>
                                                </div>

                                                <div class="col-md-4">
                                                    <select id="inputState" class="form-control">
                                                      <option value="Auditor">Auditor</option>
                                                      <option value="Billing">Billing</option>
                                                      <option value="BrokerAgent">BrokerAgent</option>
                                                      <option value="Claim Handler">Claim Handler</option>
                                                      <option value="Claims">Claims</option>
                                                      <option value="Consultant">Consultant</option>
                                                      <option value="Coordinator" selected="selected">Coordinator</option>
                                                      <option value="DestinationAgent">DestinationAgent</option>
                                                      <option value="DestinationSubAgent">DestinationSubAgent</option>
                                                      <option value="Forwarder">Forwarder</option>
                                                      <option value="ForwarderAgent">ForwarderAgent</option>
                                                      <option value="MM Counselor">MM Counselor</option>
                                                      <option value="Ops Person">Ops Person</option>
                                                      <option value="OriginAgent">OriginAgent</option>
                                                      <option value="OriginSubAgent">OriginSubAgent</option>
                                                      <option value="Payable">Payable</option>
                                                      <option value="Pricing">Pricing</option>
                                                      <option value="Salesman">Salesman</option>
                                                      <option value="Warehouse Manager">Warehouse Manager</option>
                                                       
                                                  </select>
                                                </div>
                                           </div>

                                           <div class="row">
                                              <div class="col-md-3">
                                                  <label class="label-heading eclipse-text">Note Type	</label>
                                              </div>

                                              <div class="col-md-4">
                                                  <select id="inputState" class="form-control">
                                                    <option value="Account Line">Account Line</option>
                                                    <option value="Activity">ActivityManagement</option>
                                                    <option value="Claim">Claim</option>
                                                    <option value="Crew">Crew</option>
                                                    <option value="Customer File">Customer File</option>
                                                    <option value="Issue Resolution">Issue Resolution</option>
                                                    <option value="Partner">Partner</option>
                                                    <option value="Service Order">Service Order</option>
                                                    <option value="Work Tickets">Work Tickets</option>
                                                     
                                                </select>
                                              </div>

                                              <div class="col-md-2">
                                                  <label class="label-heading ">Note Sub Type	</label>
                                              </div>
  
                                              <div class="col-md-3">
                                                  <select id="inputState" class="form-control">
                                                    <option value="AccountContact">AccountContact</option>
                                                    <option value="Accounting Issue">Accounting Issue</option>
                                                    <option value="AccountProfile">AccountProfile</option>
                                                    <option value="ActivityManagement">ActivityManagement</option>
                                                    <option value="AgentNotes">AgentNotes</option>
                                                    <option value="Audit Remarks">Audit Remarks</option>
                                                    <option value="Billing">Billing</option>
                                                    <option value="BillingPrimary">BillingPrimary</option>
                                                    <option value="BillingPrivateParty">BillingPrivateParty</option>
                                                    <option value="BillingSecondary">BillingSecondary</option>
                                                    <option value="BillingValuation">BillingValuation</option>
                                                    <option value="BookingAgentNotes">BookingAgentNotes</option>
                                                    <option value="Both">Both</option>
                                                    <option value="BrokerAgent">Broker Agent</option>
                                                    <option value="Carrier">Carrier</option>
                                                    <option value="Carton">Carton</option>
                                                    <option value="Claim">Claim</option>
                                                    <option value="ClaimForm">ClaimForm</option>
                                                    <option value="Claims">Claims</option>
                                                    <option value="ClaimValuation">ClaimValuation</option>
                                                    <option value="Container">Container</option>
                                                    <option value="cportal">cportal</option>
                                                    <option value="CreditCard">CreditCard</option>
                                                    <option value="Crew">Crew</option>
                                                    <option value="Critical to Quality Note">Critical to Quality Note</option>
                                                    <option value="Customer">Customer</option>
                                                    <option value="CustomerFile">CustomerFile</option>
                                                    <option value="DestinAgentNotes">DestinAgentNotes</option>
                                                    <option value="Destination">Destination</option>
                                                    <option value="DestinationImport">DestinationImport</option>
                                                    <option value="DestinationStatus">DestinationStatus</option>
                                                    <option value="Domestic">Domestic</option>
                                                    <option value="DomService">DomService</option>
                                                    <option value="DsAirportPickUp">DsAirportPickUp</option>
                                                    <option value="DsAutomobileAssistan">DsAutomobileAssistance</option>
                                                    <option value="DSBenefit">DSBenefit</option>
                                                    <option value="Dscola">Dscola</option>
                                                    <option value="Dscrossculturaltrain">Dscrossculturaltraining</option>
                                                    <option value="DsEducationAdvice">DsEducationAdvice</option>
                                                    <option value="Dsexpensemanagement">Dsexpensemanagement</option>
                                                    <option value="DsFlightBookings">DsFlightBookings</option>
                                                    <option value="DsFurnitureRental">DsFurnitureRental</option>
                                                    <option value="Dshomepurchase">Dshomepurchase</option>
                                                    <option value="Dshomerental">DsHomeRental</option>
                                                    <option value="DsHotelBookings">DsHotelBookings</option>
                                                    <option value="DSInternationalInsurance">DSInternationalInsurance</option>
                                                    <option value="DsInternationalPension">DsInternationalPension</option>
                                                    <option value="Dslanguage">Dslanguage</option>
                                                    <option value="DsMoveMgmt">DsMoveMgmt</option>
                                                    <option value="DsOngoingSupport">DsOngoingSupport</option>
                                                    <option value="Dsorientation">Dsorientation</option>
                                                    <option value="Dsrepatriation">Dsrepatriation</option>
                                                    <option value="DsResidencePermit">DsResidencePermit</option>
                                                    <option value="Dsschooling">Dsschooling</option>
                                                    <option value="DsSettlingIn">DsSettlingIn</option>
                                                    <option value="DsSpousalAssistance">DsSpousalAssistance</option>
                                                    <option value="Dstaxes">Dstaxes</option>
                                                    <option value="DsTaxSupport">DsTaxSupport</option>
                                                    <option value="Dstempaccomodation">Dstempaccomodation</option>
                                                    <option value="DsTenancyManagement">DsTenancyManagement</option>
                                                    <option value="DsWorkPermit">DsWorkPermit</option>
                                                    <option value="EntitleDetail">EntitleDetail</option>
                                                    <option value="Entitlement">Entitlement</option>
                                                    <option value="EstimateDetail">EstimateDetail</option>
                                                    <option value="Feedback">Feedback</option>
                                                    <option value="ForwarderAgent">Forwarder Agent</option>
                                                    <option value="InterState">InterState</option>
                                                    <option value="Issue">Issue</option>
                                                    <option value="Issue &amp; Errors">Issue &amp; Errors</option>
                                                    <option value="JobTicket">JobTicket</option>
                                                    <option value="KPI">KPI</option>
                                                    <option value="Leads">Leads</option>
                                                    <option value="Loss">Loss</option>
                                                    <option value="LossTicket">LossTicket</option>
                                                    <option value="Margin">Margin</option>
                                                    <option value="Miscelleneous">Miscelleneous</option>
                                                    <option value="Nature">Nature</option>
                                                    <option value="NetworkAgentNotes">NetworkAgentNotes</option>
                                                    <option value="Operations">Operations</option>
                                                    <option value="Origin">Origin</option>
                                                    <option value="OriginAgentNotes">OriginAgentNotes</option>
                                                    <option value="OriginForwarding">OriginForwarding</option>
                                                    <option value="OriginPackout">OriginPackout</option>
                                                    <option value="PayableDetail">PayableDetail</option>
                                                    <option value="Preview">Preview</option>
                                                    <option value="QuoteMessage">QuoteMessage</option>
                                                    <option value="ReceivableDetail">ReceivableDetail</option>
                                                    <option value="Resolution">Resolution</option>
                                                    <option value="Scheduling">Scheduling</option>
                                                    <option value="ServiceOrder">ServiceOrder</option>
                                                    <option value="SitDestination">SitDestination</option>
                                                    <option value="SitOrigin">SitOrigin</option>
                                                    <option value="Spouse">Spouse</option>
                                                    <option value="Status">Status</option>
                                                    <option value="Storage">Storage</option>
                                                    <option value="SubDestinAgentNotes">SubDestinAgentNotes</option>
                                                    <option value="SubOriginAgentNotes">SubOriginAgentNotes</option>
                                                    <option value="Survey">Survey</option>
                                                    <option value="Ticket">Ticket</option>
                                                    <option value="TicketBilling">TicketBilling</option>
                                                    <option value="TicketDestination">TicketDestination</option>
                                                    <option value="TicketLocation">TicketLocation</option>
                                                    <option value="TicketOrigin">TicketOrigin</option>
                                                    <option value="TicketScheduling">TicketScheduling</option>
                                                    <option value="TicketStaff">TicketStaff</option>
                                                    <option value="TicketStorage">TicketStorage</option>
                                                    <option value="USGovernment">USGovernment</option>
                                                    <option value="Vehicle">Vehicle</option>
                                                    <option value="VipPerson">VipPerson</option>
                                                    <option value="VipReason">VipReason</option>
                                                    <option value="VisaImmigration">VisaImmigration</option>
                                                    <option value="WeightDetail">WeightDetail</option>
                                                
                                                     
                                                </select>
                                              </div>

                                             
                                         </div>

                                        


                                         

                                      </div>
                                      <div class="col-md-6">
                                          <div class="row">
                                            <div class="col-md-8">
                                                <label class="label-heading"> Agent TDR</label></div>
                                            <div class="col-md-4 text-right"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
                                          </div>

                                          <div class="row">
                                              <div class="col-md-3">
                                                  <label class="label-heading ">Email Notification</label>
                                              </div>

                                              <div class="col-md-9">
                                                  <select id="inputState" class="form-control">
                                                      

                                                      <option value="trackingstatus.destinationAgentEmail">trackingstatus.destinationAgentEmail</option>
                                                      <option value="trackingstatus.originAgentEmail">trackingstatus.originAgentEmail</option>
                                                      <option value="trackingstatus.subDestinationAgentEmail">trackingstatus.subDestinationAgentEmail</option>
                                                      <option value="trackingstatus.subOriginAgentEmail">trackingstatus.subOriginAgentEmail</option>
                                                     
                                                </select>
                                              </div>
                                         </div>

                                         <div class="row">
                                            <div class="col-md-4">
                                                  <label class="label-heading">Manual Email		</label>
                                             
                                            </div>

                                            <div class="col-md-8">
                                               <label class="label-heading">	</label>
                                         </div>
                                           
                                      </div>
                                    </div>



                                    </div>




                                                     


                                                   
                                                         
                                               
                                             </div>
                                    </div>-->

                                      <!--testing-->
                                   
                              <div class="row">                            
                                  <div class="accordion col-md-12" id="accordionExample">
                                  <div class="input_fields_wrap" id="sha">
                                    <div class="card-header " id="headingOne">
                                      <h2 class="mb-0">
                                        
                                      
                                        <i class="fa fa-trash float-right" aria-hidden="true"></i>
                                        <i class="fa fa-pencil-square-o float-right mr-3" data-toggle="modal" aria-hidden="true"></i>
                                      </h2>
                             
                                      <div class="row">
                                      <c:forEach var="myFileList" items="${actionList}">
                                      <div class="col-md-3">
                                          <button class="btn btn-link " type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                              Action 1
                                             </button>
                                      </div>
                              
                                      <div class="col-md-1">
                                          <label class="label-heading ">Message</label>
                                         
                                      </div>
                                      <div class="col-md-3">
                                         
                                          <label class=" ml-1">${myFileList.messagedisplayed}</label>
                                      </div>
                                      <div class="col-md-2">
                                          <label class="label-heading ">Then,add the record to the list of the </label>
                                         
                                      </div>
                                      <div class="col-md-1">
                                         
                                          <label>${myFileList.rolelist}</label>
                                      </div>

                                      <div class="col-md-1">
                                          <label class="label-heading"> Agent TDR</label>
                                         
                                      </div>
                                      <div class="col-md-1">
                                         <s:checkbox name="toDoRule.publishRule"   value="${myFileList.publishRule}" fieldValue="true" disabled="true" />
                                     
                                      </div>
                                
                                    </div></c:forEach>

                                    </div>  
                                
                                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                     <%--  <div class="card-body">
                                          <div class="row">
                                              <div class="col-md-6">    
                                            <!-- <div class="row">
                                                <div class="col-md-3">
                                                    <label class="label-heading">Control Date*	</label>
                                                </div>

                                                <div class="col-md-9">
                                                    <select id="inputState" class="form-control">
                                                        <option selected>billing.recSignedDisclaimerDate</option>
                                                        <option value="Claim">Claim</option>
                                                        <option value="CreditCard">CreditCard</option>
                                                        <option value="CustomerFile">CustomerFile</option>
                                                        <option value="DspDetails">DspDetails</option>
                                                        <option value="LeadCapture">LeadCapture</option>
                                                       
                                                  </select>
                                                </div>
                                           </div> -->

                                           <div class="row">
                                              <div class="col-md-3">
                                                    <label class="label-heading eclipse-text">Message to give the user*	</label>
                                               
                                              </div>

                                              <div class="col-md-9">
                                                 <label class=" ml-1">${toDoRule.messagedisplayed}</label>
                                           </div>
                                             
                                        </div>

                                         

                                         <div class="row">
                                            <div class="col-md-3">
                                                <label class="label-heading eclipse-text">Then,add the record to the list of the</label>
                                            </div>

                                            <div class="col-md-4">
                                                <label class=" ml-1">${toDoRule.rolelist}</label>
                                            </div>
                                       </div>

                                       <div class="row">
                                          <div class="col-md-3">
                                              <label class="label-heading eclipse-text">Note Type	</label>
                                          </div>

                                          <div class="col-md-4">

                                              <label class=" ml-1">${toDoRule.noteType}</label>
                                             
                                          </div>

                                          <div class="col-md-2">
                                              <label class="label-heading ">Note Sub Type	</label>
                                          </div>

                                          <div class="col-md-3">

                                              <label class=" ml-1">${toDoRule.noteSubType}</label>
                                             
                                          </div>

                                         
                                     </div>

                                    


                                     

                                  </div>
                                  <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-8">
                                            <label class="label-heading"> Agent TDR</label></div>
                                        <div class="col-md-4 text-right"><s:checkbox name="toDoRule.publishRule"   value="${toDoRule.publishRule}" fieldValue="true" disabled="true" /></div>
                                      </div>

                                      <div class="row">
                                          <div class="col-md-3">
                                              <label class="label-heading ">Email Notification</label>
                                          </div>

                                          <div class="col-md-9">
                                              <label class=" ml-1">${toDoRule.emailNotification}</label>
                                           
                                          </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-md-4">
                                              <label class="label-heading">Manual Email		</label>
                                         
                                        </div>

                                        <div class="col-md-8">
                                            <label class=" ml-1">${toDoRule.manualEmail}</label>
                                     </div>
                                       
                                  </div>
                                </div>



                                </div>
                                      </div> --%>
                                    </div>
                                  </div>
                                
                                 
                                
                                </div>
                              </div>

                                <!--end testing-->
                              </div>

                            
                              

                                 <div class="col-md-3 mt-4">
                                 <s:submit cssClass="btn btn-blue" type="button" method="save" key="button.save"  onclick="return btntype(this);"  />  
                                
                                 <button type="button" class="btn btn-blue ">Reset</button>
                                  	<div id="agentDetailDiv${toDoRule.id}" class="modal fade" ></div>
                                 <button type="button" class="btn btn-blue  add-actions" data-toggle="modal"  onclick="editAction('${toDoRule.id}');">Add Action <i class="fa fa-plus" aria-hidden="true"></i></button>
                              </div>
                             
                            
                           </div>
 
                                    
                                 </div>
                                 <div class="tab-pane fade" id="pills-rulelist" role="tabpanel" aria-labelledby="pills-rulelist-tab">2...</div>
                                 <div class="tab-pane fade" id="pills-audit" role="tabpanel" aria-labelledby="pills-audittab">3...</div>
                                
                                
                               </div>
                            
                              <!-- BEGIN Portlet PORTLET-->
                            
                           </div>
                        </div>
                     </div>


                  <!--test-->
                 
                  <!--end second row-->       
               
            </div>
         </div>
      </div>
      <!-- END Portlet PORTLET-->
      </div>
      </div>
      </div>
      <!-- END PAGE BASE CONTENT -->
      </div>
      </div>
      </div>
      <!-- END CONTAINER -->
      <!-- full width -->
     <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel">Please Select Condition</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">serviceorder.id</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">serviceorder.email</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">serviceorder.firstName</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">serviceorder.lastName</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">serviceorder.corpID</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck6">
              <label class="custom-control-label" for="customCheck6">serviceorder.createdBy</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck7">
              <label class="custom-control-label" for="customCheck7">serviceorder.createdOn</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck8">
              <label class="custom-control-label" for="customCheck8">serviceorder.updatedBy</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck9">
              <label class="custom-control-label" for="customCheck9">serviceorder.updatedOn</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.orderBy</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.billToCode</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.billToName</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.contract</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.sequenceNumber</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.destinationZip</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.estimator</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.firstName2</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.initial2</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.job</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.lastName2</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.mi</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck10">
              <label class="custom-control-label" for="customCheck10">serviceorder.orderPhone</label>
            </div>
            
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           
          </div>
        </div>
      </div>
    </div>
      <!-- long modals -->


      <!-- Modal -->

   
    

  <!-- long modals -->

      <!-- END FOOTER -->
      <!-- jQuery library -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
      <!-- Popper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script>
         $(document).ready(function(){
        	 alert()
           $('[data-toggle="tooltip"]').tooltip();   
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
             $('[data-toggle="popover"]').popover({
                 placement : 'top',
                 trigger : 'hover'
             });
         });
         
      </script>
      
        <script>
         $(document).ready(function(){
        	 
           $('[data-toggle="tooltip"]').tooltip();   
         });
      </script>

<script language="JavaScript">
	function btntype(targerelement){
		var category= document.forms['toDoRule'].elements['toDoRule.entitytablerequired'].value;
		var field1= document.forms['toDoRule'].elements['toDoRule.fieldToValidate1'].value;
		var expression= document.forms['toDoRule'].elements['toDoRule.expression'].value;
		var testDate= document.forms['toDoRule'].elements['toDoRule.testdate'].value;
		var message= document.forms['toDoRule'].elements['toDoRule.messagedisplayed'].value;
		var duration= document.forms['toDoRule'].elements['toDoRule.durationAddSub'].value;
		if(category!='' && field1!='' && expression!='' && testDate!='' && message!='' && duration!=''){
			document.forms['toDoRule'].elements['btn'].value=targerelement.value;
			saveRulesNew();
		}else{
			alert("Please fill all required field!");
			return false;
		}
	}
	function getFieldList(target){
		var tableName= target.value;
		var url="getTempFieldList.html?ajax=1&decorator=simple&popup=true&tableName=" + encodeURI(tableName);
		http2.open("GET", url, true);
    	http2.onreadystatechange = handleHttpResponseFieldList;
     	http2.send(null);
	}
	
	function handleHttpResponseFieldList()
        {            
         if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                // alert(results);
                res = results.split("@");
                
                targetElement = document.forms['toDoRule'].elements['toDoRule.tempField'];
				targetElement.length = res.length;
				
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){					
						document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].text = '';
						document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].value = '';
					}else{
					
					// contractVal = res[i].split("#");
					document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].text = res[i];
					document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].value = res[i];
					}
					
					
             }
           
        }
	}
	try{
		disableCheckBox();
		}
		catch(e){}
		 showOrHide(0);
		function showOrHide(value) {
		    if (value==0) {
		       if (document.layers)
		           document.layers["overlay11"].visibility='hide';
		        else
		           document.getElementById("overlay11").style.visibility='hidden';
		   }
		   else if (value==1) {
		      if (document.layers)
		          document.layers["overlay11"].visibility='show';
		       else
		          document.getElementById("overlay11").style.visibility='visible';
		   }
		}
		</script>
		<script type="text/javascript">
		/* try{
			var catagory = document.forms['toDoRule'].elements['toDoRule.entitytablerequired'];
			getEmailList(catagory);
		}
		catch(e){}
		 */
function getEmailList(target){
			var fieldToValiD = document.forms['toDoRule'].elements['toDoRule.fieldToValidate1'].value;
			var tenpField = fieldToValiD.split('.');
			var fieldToValiDate1 = tenpField[0];
			var tableName= target.value;
			var parameter = '';
	        if(tableName=='ServiceOrder'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='CustomerFile'){
	        	parameter = 'PENTITY_CUSTOMERFILE';
	        }else if(tableName=='QuotationFile'){
	        	parameter = 'PENTITY_QUOTATIONFILE';
	        }else if(tableName=='WorkTicket'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='AccountLine'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='AccountLineList'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='Billing'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='Claim'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='Miscellaneous'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='ServicePartner'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='Vehicle'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }else if(tableName=='TrackingStatus'){
	        	parameter = 'PENTITY_SERVICEORDER';
	        }		
			var url="getTempEmailList.html?ajax=1&decorator=simple&popup=true&param=" + encodeURI(parameter);
			http22.open("GET", url, true);
	    	http22.onreadystatechange = handleHttpResponseEmailList;
	     	http22.send(null);
		}
		function handleHttpResponseEmailList(){            
	     if (http22.readyState == 4){
	            var results = http22.responseText
	            results = results.trim(); 
	            var res = results.split("@");      
	            targetElement = document.forms['toDoRule'].elements['toDoRule.emailNotification'];
				targetElement.length = res.length;			
				for(i=0;i<res.length;i++){
				  if(res[i] == ''){
					document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].text = ''; 
					document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].value = '';
				  }else{
					document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].text = res[i];
					document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].value = res[i];
				  }				
	            } 
				document.forms['toDoRule'].elements['toDoRule.emailNotification'].value='${toDoRule.emailNotification}';       
	   		}
		}
		function getDescription(target){
			alert(target.value)
			var fieldToValidate= target.value;
			var url="getDescriptionValue.html?ajax=1&decorator=simple&popup=true&fieldToValidateValue=" + encodeURI(fieldToValidate);
			http2.open("GET", url, true);
	    	http2.onreadystatechange = handleHttpResponseDescription;
	     	http2.send(null);	
		}
			
		function handleHttpResponseDescription()
	        {           
	             if (http2.readyState == 4)
	             {
	              var results = http2.responseText
	               results = results.trim();
	                if(results.length >= 2){ 
	                res = results.split("@");
	                document.forms['toDoRule'].elements['toDoRule.fieldDisplay'].value=res[1];
	 				}
	 				else{
	 				
	 						document.forms['toDoRule'].elements['toDoRule.fieldDisplay'].value='';
	 				}
	           
	        }
		}
		
		function getSubType(target){
		if(target.value==''){
	    	document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[0].text = '';
			document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[0].value = '';
		}
	    if(target.value != ''){
	     	var url="getSubTypeList.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
	     	http2.open("GET", url, true);
	     	http2.onreadystatechange = handleHttpResponseSubType;
	     	http2.send(null);
	   	}
	}

	function handleHttpResponseSubType(){
	            if (http2.readyState == 4) { 
	              	var results = http2.responseText
	             	results = results.trim();
	              	var res = results.split("#");
	              	if(res.length == 2)  {
		                var targetElement = document.forms['toDoRule'].elements['toDoRule.noteSubType'];
						targetElement.length = (res.length-1);
	 					for(i=1;i<res.length;i++){ 	
	 						document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[0].text= res[i];
							document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[0].value = res[i];
						}
					} else {
						var targetElement = document.forms['toDoRule'].elements['toDoRule.noteSubType'];
						targetElement.length = res.length;
	 					for(i=0;i<res.length;i++){
	 						document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[i].text = res[i];
							document.forms['toDoRule'].elements['toDoRule.noteSubType'].options[i].value = res[i];
						}
					}
				}
	}
		
		String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g,"");
		}
		String.prototype.ltrim = function() {
		    return this.replace(/^\s+/,"");
		}
		String.prototype.rtrim = function() {
		    return this.replace(/\s+$/,"");
		}
		String.prototype.lntrim = function() {
		    return this.replace(/^\s+/,"","\n");
		}
		
		
		function getHTTPObject()
		{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
		}
	    var http2 = getHTTPObject();

	    function getHTTPObject22()
		{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsofrstruts.cmlt.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
		}
	    var http22 = getHTTPObject22();
	    var temp,ruleId;
	    function editAction(ruleId){
	    	var $j = jQuery.noConflict();
	    	
	    	$j.ajax({
	    		
		  		 url: 'editToDoRuleNew.html?ruleId='+ruleId+'&decorator=modal&popup=true',
		            success: function(data){
		            	$j("#agentDetailDiv"+ruleId).modal({show:true,backdrop: 'static',keyboard: false});
						$j("#agentDetailDiv"+ruleId).html(data);
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#agentDetailDiv"+ruleId).modal("hide");
	             }   
		});  
	      		
	      		
	    } 	
	    	
	  /*   
	    function editAction()
	    {
	    	alert("gjgjregevr")
	    	
	    			var toDoRuleId =${toDoRule.id};
	    			alert(toDoRuleId)
	    		 handleHttpResponse10(toDoRuleId);
	    				}

	    	    var temp;
	    function handleHttpResponse10(temp)
	    {     
	    	alert("temp"+temp)
	    	var $j = jQuery.noConflict();
	    	
	    	    $j('#mapModal').modal({show:true,backdrop: 'static',keyboard: false});
	    	 
	     	        				  
	     	        			
	     	   } */
	    function saveRulesNew(){ 
	    	var toDoRuleId =${toDoRule.id};
        	  var url="saveRulesNew.html?ajax=1&decorator=simple&popup=true&ruleId="+toDoRuleId;
        	 	   
        		 http779.open("GET", url, true);
        		 http779.onreadystatechange =function() {handleHttpResponsereason(ruleId);};
        		 http779.send(null);
        }  	 
         function handleHttpResponsereason(agentId) {

                
        		 
             
          
        	 }
         function getHTTPObject(){
      	    var xmlhttp;
      	    if(window.XMLHttpRequest)
      	    {
      	        xmlhttp = new XMLHttpRequest();
      	    }
      	    else if (window.ActiveXObject)
      	    {
      	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      	        if (!xmlhttp)
      	        {
      	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
      	        }
      	    return xmlhttp;
      	} }    
         var http779= getHTTPObject();
</script> 


   </body>
</html>