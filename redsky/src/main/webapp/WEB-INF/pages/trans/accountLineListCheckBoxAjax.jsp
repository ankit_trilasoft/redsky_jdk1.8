<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head>   
<title></title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:156px;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right"  style="width:30px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<s:hidden name="approvedPayingStatusDisableStatus" value=""/>
<c:set var="approvedPayingStatusDisableStatus" value="no"/>
<configByCorp:fieldVisibility componentId="component.accountLine.approvedPayingStatus.disableStatus">
	<c:set var="approvedPayingStatusDisableStatus" value="yes"/>
</configByCorp:fieldVisibility>
<c:set var="ignoreBilling" value="TRUE" />
<configByCorp:fieldVisibility componentId="component.field.Alternative.ignoreBilling">
		<c:set var="ignoreBilling" value="FALSE" />
	<sec-auth:authComponent componentId="module.tab.accountline.ignoreBilling">
		<c:set var="ignoreBilling" value="TRUE" />
	</sec-auth:authComponent>
</configByCorp:fieldVisibility>
<table class="ctrltable" style="" border="0" cellpadding="3" cellspacing="0">
			<thead>
			<tr>
				<th colspan="3"><img id="target" style="margin:0px 2px 0px 2px;vertical-align:middle;" width="15" src="${pageContext.request.contextPath}/images/ctrl-gray.png"  />&nbsp;Accounting Control</th>
			</tr>
			</thead>
			<tbody>
		
			<%-- <tr>				
				<td align="right" style="padding-left:5px;">
					<c:if test="${accountLine.status}">
						<s:checkbox name="accountLine.displayOnQuote" cssStyle="margin:0px;" onclick=""/>
					</c:if>
				
				</td>
				<td class="listwhitetextFC" style="padding: 8px 8px 7px 3px;"><b>Check&nbsp;All/&nbsp;Uncheck&nbsp;All</b></td>
			</tr> --%>
			
			
			<tr>
				
				<td align="right">
					<c:if test="${accountLine.status}">
						<s:checkbox name="accountLine.displayOnQuote" id="displayOnQuote" value="${accountLine.displayOnQuote}" cssStyle="margin:0px;" onclick="updateCheckBoxValue('displayOnQuote','accountline','${accountLine.id}',this)"/>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<input type="checkbox" name="accountLine.displayOnQuote" id="displayOnQuote" value="${accountLine.displayOnQuote}" style="margin:0px;" disabled="disabled"/>
					</c:if>
				</td>
				<td class="listwhitetext">Display On Quote</td>
			</tr>
			
			<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
			<tr>				
				<td align="right">
					<c:if test="${accountLine.status}">
						<s:checkbox name="accountLine.rollUpInInvoice" id="rollUpInInvoice" value="${accountLine.rollUpInInvoice}" cssStyle="margin:0px;" onclick="updateCheckBoxValue('rollUpInInvoice','accountline','${accountLine.id}',this)"/>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<input type="checkbox" name="accountLine.rollUpInInvoice" value="${accountLine.rollUpInInvoice}" style="margin:0px;" disabled="disabled"/>
					</c:if>
				</td>
				<td class="listwhitetext">Roll&nbsp;Up In Invoice</td>
			</tr>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
			<tr>				
				<td align="right">
					<c:if test="${accountLine.status}">
						<s:checkbox name="accountLine.additionalService" value="${accountLine.additionalService}" cssStyle="margin:0px;" onclick="updateCheckBoxValue('additionalService','accountline','${accountLine.id}',this)"/>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<input type="checkbox" name="accountLine.additionalService" value="${accountLine.additionalService}" style="margin:0px;" disabled="disabled"/>
					</c:if>
				</td>
				<td class="listwhitetext">Additional Services</td>
			</tr>
			</configByCorp:fieldVisibility>
			<tr>				
				<td align="right">
					<c:if test="${accountLine.status}">
						<c:choose>
							<c:when test="${ignoreBilling=='TRUE'}">
					        <c:set var="isVipFlag" value="false" />
							<c:if test="${accountLine.ignoreForBilling}">
									<c:set var="isVipFlag" value="true" />
								</c:if>
								<s:checkbox name="accountLine.ignoreForBilling" cssStyle="margin:0px;" value="${isVipFlag}" onclick="updateCheckBoxValue('ignoreForBilling','accountline','${accountLine.id}',this)"/>
							</c:when>
							<c:otherwise>
					        <c:set var="isVipFlag" value="false" />
							<c:if test="${accountLine.ignoreForBilling}">
									<c:set var="isVipFlag" value="true" />
								</c:if>
								<s:checkbox name="accountLine.ignoreForBilling" cssStyle="margin:0px;" value="${isVipFlag}" onclick="updateCheckBoxValue('ignoreForBilling','accountline','${accountLine.id}',this)"/>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<c:choose>
							<c:when test="${ignoreBilling=='TRUE'}">
					        <c:set var="isVipFlag" value="false" />
							<c:if test="${accountLine.ignoreForBilling}">
									<c:set var="isVipFlag" value="true" />
								</c:if>
								<input type="checkbox" name="accountLine.ignoreForBilling" value="${isVipFlag}" style="margin:0px;" disabled="disabled"/>
							</c:when>
							<c:otherwise>
					        <c:set var="isVipFlag" value="false" />
							<c:if test="${accountLine.ignoreForBilling}">
									<c:set var="isVipFlag" value="true" />
								</c:if>
								<input type="checkbox" name="accountLine.ignoreForBilling" value="${isVipFlag}" style="margin:0px;" disabled="disabled"/>
							</c:otherwise>
						</c:choose>
					</c:if>
				</td>
				<td class="listwhitetext">Ignore for Invoicing</td>
			</tr>
			<tr>
				
				<td align="right">
					<c:choose> 
				        <c:when test="${((!(trackingStatus.accNetworkGroup)) && (billingCMMContractType || billingDMMContractType) && (trackingStatus.soNetworkGroup) && ((accountLine.createdBy == 'Networking') || ( (fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 )  &&  not empty accountLine.networkSynchedId )) ) || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' || accountLine.createdBy == 'Networking' )) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLine.chargeCode == 'MGMTFEE') ||(accountLine.chargeCode == 'MGMTFEE') || (billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' )) }">
					        <c:if test="${accountLine.status}">
					        	<input type="checkbox" style="margin:0px;" checked="checked" disabled="disabled"/>
					        </c:if>
					        <c:if test="${accountLine.status==false}">
				    			<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" onclick="inactiveStatusCheck(${accountLine.id},this)" />
				 			</c:if>
				        </c:when>
				        <c:otherwise> 
				          <c:if test="${accountLine.status}">
					          <c:if test="${approvedPayingStatusDisableStatus!='yes'}">
						          <c:if test="${accountInterface!='Y'}">
							          <c:if test="${accountLine.actualRevenue!='0.00' || accountLine.actualExpense!='0.00'}">
							          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLine.id},this)" />
								      </c:if>
								      <c:if test="${accountLine.actualRevenue=='0.00' && accountLine.actualExpense=='0.00'}">
							          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" onclick="inactiveStatusCheck(${accountLine.id},this)" />
								      </c:if>
							      </c:if>
							      <c:if test="${accountInterface=='Y'}">
							          <c:if test="${ not empty accountLine.accruePayable || not empty accountLine.accrueRevenue || not empty accountLine.recAccDate || not empty accountLine.payAccDate || not empty accountLine.receivedInvoiceDate }">
							          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLine.id},this)" />
								      </c:if>
								      <c:if test="${empty accountLine.accruePayable && empty accountLine.accrueRevenue && empty accountLine.recAccDate && empty accountLine.payAccDate && empty accountLine.receivedInvoiceDate}">
							          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" onclick="inactiveStatusCheck(${accountLine.id},this)" />
								      </c:if>
							      </c:if>
						      </c:if>
					      <c:if test="${approvedPayingStatusDisableStatus=='yes'}">
						      <c:if test="${accountInterface!='Y'}">
						          <c:if test="${accountLine.actualRevenue!='0.00' || accountLine.actualExpense!='0.00'}">
						          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLine.id},this)" />
							      </c:if>
							      <c:if test="${accountLine.actualRevenue=='0.00' && accountLine.actualExpense=='0.00'}">
						          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" onclick="inactiveStatusCheck(${accountLine.id},this)" />
							      </c:if>
						      </c:if>
						      <c:if test="${accountInterface=='Y'}">
						          <c:if test="${ not empty accountLine.accruePayable || not empty accountLine.accrueRevenue || not empty accountLine.recAccDate || not empty accountLine.payAccDate || not empty accountLine.receivedInvoiceDate || accountLine.payingStatus=='A' }">
						          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLine.id},this)" />
							      </c:if>
							      <c:if test="${empty accountLine.accruePayable && empty accountLine.accrueRevenue && empty accountLine.recAccDate && empty accountLine.payAccDate && empty accountLine.receivedInvoiceDate && accountLine.payingStatus!='A'}">
						          	<input type="checkbox" id="${accountLine.id}"  style="margin:0px;" checked="checked" onclick="inactiveStatusCheck(${accountLine.id},this)" />
							      </c:if>
						      </c:if>
					      </c:if>
					     </c:if>
					     	<c:if test="${accountLine.status==false}">
				    			<input type="checkbox" id="${accountLine.id}" style="margin:0px;"  onclick="inactiveStatusCheck(${accountLine.id},this)" />
				 			</c:if>
						</c:otherwise>
					</c:choose>
				</td>
				<td class="listwhitetext">Active</td>
			</tr>
			<tr>				
				<td align="right">
					<c:if test="${accountLine.status}">
						<c:choose> 
						        <c:when test="${(trackingStatus.accNetworkGroup && billingCMMContractType) || serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD' }">
						        	<input type="checkbox"   style="margin:0px;" checked="checked" disabled="disabled"   />
						        </c:when> 
						        <c:otherwise>
							         <c:if test="${accountLine.recInvoiceNumber != '' && accountLine.recInvoiceNumber != null}">
							         	<input type="checkbox" style="margin:0px;" checked="checked" disabled="disabled"/>
							         </c:if>
						         <c:if test="${accountLine.recInvoiceNumber == '' || accountLine.recInvoiceNumber == null}">
							         <c:if test="${accountLine.selectiveInvoice}">
							           <input type="checkbox" style="margin:0px;"   checked="checked" onclick="updateCheckBoxValue('selectiveInvoice','accountline','${accountLine.id}',this)" />
							         </c:if>
							         <c:if test="${accountLine.selectiveInvoice==false}">
									   <input type="checkbox" style="margin:0px;" onclick="updateCheckBoxValue('selectiveInvoice','accountline','${accountLine.id}',this)" />  
								     </c:if>
							     </c:if>
							     </c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<c:choose> 
						        <c:when test="${(trackingStatus.accNetworkGroup && billingCMMContractType) || serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD' }">
						        	<input type="checkbox"   style="margin:0px;" checked="checked" disabled="disabled"   />
						        </c:when> 
						        <c:otherwise>
							         <c:if test="${accountLine.recInvoiceNumber != '' && accountLine.recInvoiceNumber != null}">
							         	<input type="checkbox" style="margin:0px;" checked="checked" disabled="disabled"/>
							         </c:if>
						         <c:if test="${accountLine.recInvoiceNumber == '' || accountLine.recInvoiceNumber == null}">
							         <c:if test="${accountLine.selectiveInvoice}">
							           <input type="checkbox"   style="margin:0px;" checked="checked" disabled="disabled" />
							         </c:if>
							         <c:if test="${accountLine.selectiveInvoice==false}">
									   <input type="checkbox" style="margin:0px;" disabled="disabled" />  
								     </c:if>
							     </c:if>
							     </c:otherwise>
						</c:choose>
					</c:if>
				</td>
				<td class="listwhitetext">Selective Invoice</td>
			</tr>
			<tr>				
				<td align="right">
					<c:if test="${accountLine.status}">
						<c:choose> 
					        <c:when test="${((!(trackingStatus.accNetworkGroup)) && (billingCMMContractType) && (trackingStatus.soNetworkGroup)) || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' || accountLine.createdBy == 'Networking' )) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLine.chargeCode == 'MGMTFEE') ||(accountLine.chargeCode == 'MGMTFEE') }">
						        <c:if test="${accountLine.activateAccPortal eq true || accountLine.activateAccPortal == null}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" id="activateAccPortal" checked="checked" value="${accountLine.activateAccPortal}" onclick="" disabled="disabled"/>
							    </c:if>
							    <c:if test="${accountLine.activateAccPortal eq false}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" value="${accountLine.activateAccPortal}" onclick="" disabled="disabled"/>
							    </c:if>
					        </c:when>
					        <c:otherwise>
						      	<c:if test="${accountLine.activateAccPortal eq true || accountLine.activateAccPortal == null}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" checked="checked" value="${accountLine.activateAccPortal}" onclick="updateCheckBoxValue('activateAccPortal','accountline','${accountLine.id}',this)"/>
							    </c:if>
							    <c:if test="${accountLine.activateAccPortal eq false}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" value="${accountLine.activateAccPortal}" onclick="updateCheckBoxValue('activateAccPortal','accountline','${accountLine.id}',this)" />
							    </c:if>
						    </c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${accountLine.status==false}">
						<c:choose> 
					        <c:when test="${((!(trackingStatus.accNetworkGroup)) && (billingCMMContractType) && (trackingStatus.soNetworkGroup)) || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' || accountLine.createdBy == 'Networking' )) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLine.chargeCode == 'MGMTFEE') ||(accountLine.chargeCode == 'MGMTFEE') }">
						        <c:if test="${accountLine.activateAccPortal eq true || accountLine.activateAccPortal == null}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" id="activateAccPortal" checked="checked" value="${accountLine.activateAccPortal}" onclick="" disabled="disabled"/>
							    </c:if>
							    <c:if test="${accountLine.activateAccPortal eq false}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" value="${accountLine.activateAccPortal}" onclick="" disabled="disabled"/>
							    </c:if>
					        </c:when>
					        <c:otherwise>
						      	<c:if test="${accountLine.activateAccPortal eq true || accountLine.activateAccPortal == null}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" checked="checked" value="${accountLine.activateAccPortal}" disabled="disabled"/>
							    </c:if>
							    <c:if test="${accountLine.activateAccPortal eq false}">
							      	<input type="checkbox" style="margin:0px;" name="accountLine.activateAccPortal" value="${accountLine.activateAccPortal}" disabled="disabled" />
							    </c:if>
						    </c:otherwise>
						</c:choose>
					</c:if>
				</td>
				<td class="listwhitetext">Account Portal</td>
			</tr>
	</tbody>
	
	
</table>
<script type="text/JavaScript"></script>