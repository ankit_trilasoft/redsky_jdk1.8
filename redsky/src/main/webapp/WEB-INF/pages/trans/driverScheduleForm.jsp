<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Driver Schedule</title>
<meta name="heading" content="Driver Schedule" />	



  <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script type="text/javascript">
    function searchDate(){
    	var fromDate =document.forms['workPlanForm'].elements['fromDate'].value;
    	var toDate =document.forms['workPlanForm'].elements['toDate'].value;
    	if(fromDate==''){
    		alert('Select Planning Period From Date to continue.....');
    		return false;
    	}else if(toDate==''){
    		alert('Select Planning Period To Date to continue.....');
    		return false;
    	}
    	
    }
    </script>
</head>
<s:form name="workPlanForm" id="workPlanForm" action="driverScheduleList" method="post" validate="true" >
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<c:set var="toDate" value="" ></c:set>
<c:set var="now" value="<%=new java.util.Date()%>" />
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer1" style="width:85%"> 
<div id="otabs">
			  <ul>
			   <li><a class="current"><span>Work Planning</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="3" border="0">

	  		<tr><td height="5px"></td></tr>
  			<tr> 
  		    <td align="right" width="130px" class="listwhitetext" >Planning Period From<font color="red" size="2">*</font></td>		  
	  	  	<td width="120px" > 
	  	  	<s:text id="nowFormattedValue" name="${FormDateValue}"><fmt:formatDate  pattern="dd-MMM-yy" value="${now}" /></s:text>	 
		  	<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{nowFormattedValue}" size="8" maxlength="11"  readonly="true"   onkeydown="return onlyDel(event,this);" /> 
		  	<img id="fromDate_trigger" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20 onclick="updateCheckDate()" />
		  	</td>
			<td align="right" class="listwhitetext"> To<font color="red" size="2">*</font></td>	
		  	<td width="160px" align="left" class="listwhitetext" >
		  	<s:text id="nowToDateValue" name="${FormDateValue}"><fmt:formatDate  pattern="dd-MMM-yy" value="${now}" /></s:text>
		  		<s:textfield cssClass="input-text" id="toDate" name="toDate" value="%{nowToDateValue}" size="8" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"  /> 
		  		<img id="toDate_trigger" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20  />
		  	</td>	 	
			</tr> 
			<tr><td height="5px"></td></tr>
	  		<tr>	
	  			<td width=""></td>	  						
				<td colspan="5"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="driverScheduleList" value="Submit" onclick="return searchDate()"/></td>
	    	</tr>
	    	<tr><td height="10px"></td></tr>	 				
	</table>       
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div> 
</s:form>
<script type="text/javascript">
var checkDate='';
function addDate(dateObject, numDays) {
	dateObject.setDate(dateObject.getDate() + numDays);
	return dateObject;
   }
   function updateCheckDate(){
	   checkDate='Y';
   }
function setEndDate(){
	if(checkDate=='Y'){
		checkDate='';
			var date2 = document.forms['workPlanForm'].elements['fromDate'].value;
			if(date2!=''){
			var mdate111= new Date();
			var mdate11 = date2.split("-");         
			var day = mdate11[0];
			var month = mdate11[1];
			var year = mdate11[2];
			if(month == 'Jan'){
			       month = "01";
			   	}
			if(month == 'Feb'){
			       month = "02";
			   	}
			if(month == 'Mar'){
			       month = "03";
			   	}
			if(month == 'Apr'){
			       month = "04";
			   	}
			if(month == 'May'){
			       month = "05";
			   	}
			if(month == 'Jun'){
			       month = "06";
			   	}
			if(month == 'Jul'){
			       month = "07";
			   	}
			if(month == 'Aug'){
			       month = "08";
			   	}
			if(month == 'Sep'){
			       month = "09";
			   	}
			if(month == 'Oct'){
			       month = "10";
			  	}
			if(month == 'Nov'){
			       month = "11";
			   	}
			if(month == 'Dec'){
			       month = "12";
			   	}
			 if(parseInt(year)>=00 && parseInt(year)<50)
		     {
		         year=2000+parseInt(year)+"";
		     }
		     
		     if(parseInt(year)>=50)
		     {
		         year=1900+parseInt(year)+"";
		     }
		    mdate11=year+"/"+month+"/"+day;
			mdate111= new Date(mdate11);
			var mydate = addDate(mdate111,7);
			var year1=mydate.getFullYear();
			var y=""+year1;
			if (year1 < 1000)
			year1+=1900;
			var month1=mydate.getMonth()+1;
			if(month1 == 1)month1="Jan";
			if(month1 == 2)month1="Feb";
			if(month1 == 3)month1="Mar";
			if(month1 == 4)month1="Apr";
			if(month1 == 5)month1="May";
			if(month1 == 6)month1="Jun";
			if(month1 == 7)month1="Jul";
			if(month1 == 8)month1="Aug";
			if(month1 == 9)month1="Sep";
			if(month1 == 10)month1="Oct";
			if(month1 == 11)month1="Nov";
			if(month1 == 12)month1="Dec";
		    var daym=mydate.getDate();
			if (daym<10)
			daym="0"+daym;
			var datam = daym+"-"+month1+"-"+y.substring(2,4);
			document.forms['workPlanForm'].elements['toDate'].value=datam;
			}
	}
}
</script>
<script type="text/javascript">
setOnSelectBasedMethods(["setEndDate()"]);
	setCalendarFunctionality();
	try{
   updateCheckDate()
   setEndDate();	
	}catch(e){
		
	}
</script>