package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DiscountDao;
import com.trilasoft.app.model.Discount;

public class DiscountDaoHibernate extends GenericDaoHibernate<Discount, Long> implements DiscountDao {
	public DiscountDaoHibernate() {
		super(Discount.class);
	}

	public List discountList(String contract, String chargeCode,String corpId) {
		return getHibernateTemplate().find("from Discount where contract='"+contract+"' and corpID='"+corpId+"' and chargeCode='"+chargeCode+"'");
	}
	public List discountMapList(String corpId){
		return getHibernateTemplate().find("from Discount where corpID='"+corpId+"'");
	}
}
