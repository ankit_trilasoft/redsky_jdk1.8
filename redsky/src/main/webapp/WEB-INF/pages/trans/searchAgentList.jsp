<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="agentRequest.title"/></title>   
    <meta name="heading" content="<fmt:message key="agentRequest.title"/>"/>   
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    
   <style> 
.btn-group-sm > .btn, .btn-sm {    
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12) !important;
}
.btn.focus, .btn:focus, .btn:hover {outline:none !important;}

.alert { margin-bottom: 0px;}

#modalAlert{
    padding: 10px!important;
    overflow-y: inherit!important;
    margin-top: 0px!important;
}
.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
   .key_so_dashboard { background: url("images/key_partner_list.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 23px; margin-left:22%; }
    #overlay190 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

.btn-outline {
    background-color: transparent;
    color: inherit;
    transition: all .5s;
}

.btn-primary {
color: #fff;
border-color: #308ee0;
}

.btn-primary.btn-outline {
    color: #308ee0;
}

.btn-success.btn-outline {
    color: #5cb85c;
}

.btn-info.btn-outline {
    color: #5bc0de;
}

.btn-warning.btn-outline {
    color: #f0ad4e;
}

.btn-danger.btn-outline {
    color: #d9534f;
}

.btn-primary.btn-outline:hover,
.btn-success.btn-outline:hover,
.btn-info.btn-outline:hover,
.btn-warning.btn-outline:hover,
.btn-danger.btn-outline:hover {
    color: #fff;
    </style>
 
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	
    $("#agentRequestLists tr").each(function() {
    	var MyRows = $('table#agentRequestLists').find('tbody').find('tr');
    	for (var i = 0; i < MyRows.length; i++) {
    	var MyIndexValue = $(MyRows[i]).find('td:eq(4)').html();
    	
    	var MyIndexValue1 = $(MyRows[i]).find("option:selected").text();
    	
    	  if ($.trim(MyIndexValue) == 'Rejected') {
    		  $(MyRows[i]).css('background-color', '#f2dede');
    	  }
    	if ($.trim(MyIndexValue) == 'Approved') {
  		  $(MyRows[i]).css('background-color', '#d9edf7');
  	      }
    	if ($.trim(MyIndexValue) == 'New') {
  		  $(MyRows[i]).css('background-color', '#dff0d8');
  	     }
    	if ($.trim(MyIndexValue) == 'Edit') {
  		  $(MyRows[i]).css('background-color', '#fcf8e3');
  	     }
    	  
    	  }
   });
}); 
function clear_fields(){
	     document.forms['agentListForm'].elements['agentRequest.aliasName'].value = '';
		document.forms['agentListForm'].elements['agentRequest.lastName'].value = '';
		document.forms['agentListForm'].elements['countryCodeSearch'].value = '';
		document.forms['agentListForm'].elements['stateSearch'].value = '';
		document.forms['agentListForm'].elements['countrySearch'].value = '';
		document.forms['agentListForm'].elements['agentRequest.status'].value = '';
		document.forms['agentListForm'].elements['corpid'].value = '';
		}
function activBtn(temp){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
    var len = temp.length;
    var val="" ;
    if(len == undefined){
		if(temp.checked){
			val= temp.value;
		}
    }
		
	  for(var i = 0; i < len; i++) {
		if(temp[i].checked) {
			val = temp[i].value;
		}
	}

	if(val=='AG'){
		document.forms['addPartner'].elements['chkAgentTrue'].value='Y';
	}else{
		document.forms['addPartner'].elements['chkAgentTrue'].value='';	
	}
}

function seachValidate(){
		
		var ag =  document.forms['agentListForm'].elements['agentRequest.isAgent'].checked;
		
		if ( ag ==false){
			alert('Please select atleast one agentRequest type.');
			return false;
		} 
		
} 

function findVanLineCode(partnerCode,position){
	var url="findVanLineCodeList.html?ajax=1&decorator=simple&popup=true&partnerVanLineCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);
}
</script>
<style>
span.pagelinks {display:block;margin-bottom:0px;!margin-bottom:2px;margin-top:0px;!margin-top:-17px;padding:2px 0px;text-align:right;width:99%;!width:98%;font-size:0.9em;
}
.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;}
.radiobtn {margin:0px 0px 2px 0px;!padding-bottom:5px;!margin-bottom:5px;}
form {margin-top:-10px;!margin-top:0px;}

div#main {margin:-5px 0 0;}
input[type="checkbox"] {vertical-align:middle;}
.br-n-ctnr {border:none !important;text-align:center !important;}
.br-n-rgt {border:none !important;text-align:right !important;}
</style>
</head>
<c:set var="buttons">     
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/agentRequestFinal.html"/>'" value="<fmt:message key="button.add"/>"/>         
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" onclick=""/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="agentListForm" action="searchAgentRequest.html" method="post" >  
<s:hidden name="agentStatus" />
<s:hidden name="counter" />
<s:hidden name="agentId" />

<div id="layer5" style="width:100%">
<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
		<li><a href="geoCodeMaps.html?partnerOption=notView" ><span>Geo Search</span></a></li>					  	
	  </ul>
</div>
<div class="spnlk" style="margin-top:-10px;">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-2px;"><span></span></div>
<div class="center-content">
<table class="table" border="" style="width:99%;">
	<thead>
		<tr> 
			<th>Last/Company Name</th>
			<th>Alias Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th>State Code</th>
			<th>CorpId</th>
			<th><fmt:message key="partner.status"/></th>
			<th></th>
		</tr>
	</thead>	
	<tbody>
		<tr>
			
			<td><s:textfield name="agentRequest.lastName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="agentRequest.aliasName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="7" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="12" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="corpid"  list="%{corpidList}"  cssStyle="width:75px" headerKey="" headerValue=""/></td>
			<td><s:select cssClass="list-menu" name="agentRequest.status" list="%{agentRequestStatus}" cssStyle="width:75px" headerKey="" headerValue=""/></td>
			<s:hidden  key="agentRequest.isAgent" value="true" />
			<td width="120px"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr> 
		
	</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>
</div>
</div>
</div>
<div id="overlay190">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing .......Please wait<br></font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
</s:form>
<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
	<ul>
		<li><a class="current"><span>Agent  Change Request</span></a></li>
	</ul>
</div><div class="spn">&nbsp;</div> 
<div id="KeyDiv" class="key_so_dashboard" style="float:left;width: 392px;margin-left:15px;">&nbsp;</div>
		
<s:set name="agentRequestList" value="agentRequestList" scope="request"/> 
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility> 
<display:table name="agentRequestList" class="table" requestURI="" id="agentRequestLists" export="false" defaultsort="1" pagesize="10" style="width:99%;margin-left:5px;">  		
   <display:column  titleKey="partner.name" sortProperty="lastName" sortable="true" style="width:150px" ><c:out value="${agentRequestLists.lastName}" />
    </display:column>		
    <display:column property="countryName"  titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName"  titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName"  titleKey="partner.billingCity" style="width:35px"/>
	
   <c:if test="${agentClassificationShow=='Y'}">
     <display:column  title="Agent Classification" sortable="true" sortProperty="agentClassification" style="width:45px;" >
    <c:if test="${agentRequestLists.isAgent == true}">
   		<c:out value="${agentRequestLists.agentClassification}" />
    </c:if>
	</display:column>
	</c:if>

  
    <display:column title="Status"  style="width:80px" >
    <c:if test="${sessionCorpID=='TSFT' && agentRequestLists.counter == '1'}">
   <c:out value="ReRequest" />
    </c:if>
    <c:if test="${sessionCorpID=='TSFT' && agentRequestLists.counter != '1'}">
	   	<c:out value="${agentRequestLists.status}" />
	  </c:if>
			</display:column>
			
			<display:column property="corpID"  title="CorpID" style="width:35px"/>  			
    <display:column title="Action" style="width:45px;font-size: 9px;" >
		<div id="agentDetailDiv${agentRequestLists.id}" class="modal fade" ></div>
   		<button type="button" style="width:6em;" class="btn btn-primary btn-xs" id="editProcess${agentRequestLists.id}" onclick="getAgentDetails('${agentRequestLists.status}','${agentRequestLists.id}','${agentRequestLists.createdBy}','${agentRequestLists.updatedBy}','${agentRequestLists.partnerId}','${agentRequestLists.corpID}')" >Processed</button>	
		<c:if test="${agentRequestLists.isAgent == true}">  
    	<button type="button" style="width:4em;" class="btn btn-primary btn-xs" id="edit${agentRequestLists.id}" onclick="window.open('agentRequestFinal.html?partnerId=${agentRequestLists.partnerId}&id=${agentRequestLists.id}&partnerType=AG')" ><i class='fa fa-pencil'></i>&nbsp;Edit</button>
			
    	</c:if>
	</display:column>
	
	    <display:column  style="width:2%;">
	    <c:if test="${agentRequestLists.status=='Rejected'}">
             <a><img src="images/viewAgent.png" onclick="findCustomInfo(this,'${agentRequestLists.id}');" alt="Agent Request Message" title="Agent Request Message" /></a></c:if> </display:column>
   
    <display:column  title="Partner Id" sortable="true" style="width:30px" ><c:out value="${agentRequestLists.partnerId}" />
    </display:column>
 
			  
    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>
</div>
<%-- <table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="padding:1px;width: 100%;">
	<tbody>
		<s:form id="addPartner" action="agentRequestFinal.html" method="post" name="addPartner">  
		<s:hidden name="chkAgentTrue" />
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">Add New Partner :
					<s:radio name="partnerType" list="%{actionType}" onclick="activBtn(this);"  />
				   	<s:submit name="addBtn" cssClass="cssbutton" cssStyle="width:125px; height:25px; margin-left:50px;" align="top" value=" Add Partner"/>   
				</td>
			</tr>	
		</s:form>
		
	</tbody>
</table> --%>

<script type="text/javascript">



function showMessage(){
	location.href  = 'searchAgentRequest.html?';
}
	
function getAgentDetails(temp,id,createdBy,updatedBy,partnerId, corpID){
	var $j = jQuery.noConflict();
	  	$j.ajax({
	  		 url: 'viewAgentDetail.html?id='+id+'&createdBy='+createdBy+'&updatedBy='+updatedBy+'&partnerId='+partnerId+'&temp='+temp+'&decorator=modal&popup=true',
	            success: function(data){
	            	$j("#agentDetailDiv"+id).modal({show:true,backdrop: 'static',keyboard: false});
					$j("#agentDetailDiv"+id).html(data);
            },
            error: function () {
                alert('Some error occurs');
                $j("#agentDetailDiv"+id).modal("hide");
             }   
	});  
} 	
	
function handleHttpResponse9(id,createdBy,corpID)
   {
        if (http2.readyState == 4)
        {
        	
           var results = http2.responseText
           results = results.trim();
          alert("Your Agent Has been Rejected");
          var agree = confirm("Do you want to Reject the Request Of Agent ?click OK to proceed or Cancel.");
  		if(agree)
  		{
  		window.open('editAgentRequestReason.html?agentRequestId='+id+'&decorator=popup&popup=true','forms','height=600,width=600,top=1, left=120, scrollbars=yes,resizable=yes');
  		} 
           window.location.reload();
         
          } 
   }
var http2 = getHTTPObject();
function getHTTPObject6(){
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}  
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay190"].visibility='hide';
        else
           document.getElementById("overlay190").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay190"].visibility='show';
       else
          document.getElementById("overlay190").style.visibility='visible';
   }
}
showOrHide(0);
function ajax_showTooltip1(e,t){if(!ajax_tooltipObj){ajax_tooltipObj=document.createElement("DIV");ajax_tooltipObj.style.position="absolute";ajax_tooltipObj.id="ajax_tooltipObj";document.body.appendChild(ajax_tooltipObj);var n=document.createElement("DIV");n.className="ajax_tooltip_arrow";n.id="ajax_tooltip_arrow";ajax_tooltipObj.appendChild(n);var r=document.createElement("DIV");r.className="ajax_tooltip_content";ajax_tooltipObj.appendChild(r);r.id="ajax_tooltip_content";if(ajax_tooltip_MSIE){ajax_tooltipObj_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObj_iframe.style.position="absolute";ajax_tooltipObj_iframe.border="0";ajax_tooltipObj_iframe.frameborder=0;ajax_tooltipObj_iframe.style.backgroundColor="#FFF";ajax_tooltipObj_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObj_iframe.style.left="0px";ajax_tooltipObj_iframe.style.top="0px"}}ajax_tooltipObj.style.display="block";ajax_loadContent("ajax_tooltip_content",e);if(ajax_tooltip_MSIE){ajax_tooltipObj_iframe.style.width=ajax_tooltipObj.clientWidth+"px";ajax_tooltipObj_iframe.style.height=ajax_tooltipObj.clientHeight+"px"}ajax_positionTooltip(t)}function ajax_positionTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajax_tooltip_content").offsetWidth+document.getElementById("ajax_tooltip_arrow").offsetWidth;ajax_tooltipObj.style.left=t-20+"px";ajax_tooltipObj.style.width=100+"%";ajax_tooltipObj.style.top=n-25+"px"}function ajaxTooltip_getTopPos(e){var t=e.offsetTop;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetTop}return t}function ajaxTooltip_getLeftPos(e){var t=e.offsetLeft;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetLeft}return t}function ajax_hideTooltip(){if(ajax_tooltipObj){ajax_tooltipObj.style.display="none"}if(ajax_tooltipObjBig){ajax_tooltipObjBig.style.display="none"}}function ajax_showBigTooltip(e,t){if(!ajax_tooltipObjBig){ajax_tooltipObjBig=document.createElement("DIV");ajax_tooltipObjBig.style.position="absolute";ajax_tooltipObjBig.id="ajax_big_tooltipObj";document.body.appendChild(ajax_tooltipObjBig);var n=document.createElement("DIV");n.className="ajax_big_tooltip_arrow";n.id="ajax_big_tooltip_arrow";ajax_tooltipObjBig.appendChild(n);var r=document.createElement("DIV");r.className="ajax_big_tooltip_content";ajax_tooltipObjBig.appendChild(r);r.id="ajax_big_tooltip_content";if(ajax_tooltipBig_MSIE){ajax_tooltipObjBig_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObjBig_iframe.style.position="absolute";ajax_tooltipObjBig_iframe.border="0";ajax_tooltipObjBig_iframe.frameborder=0;ajax_tooltipObjBig_iframe.style.backgroundColor="#FFF";ajax_tooltipObjBig_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObjBig_iframe.style.left="0px";ajax_tooltipObjBig_iframe.style.top="0px"}}ajax_tooltipObjBig.style.display="block";ajax_loadContent("ajax_big_tooltip_content",e);if(ajax_tooltipBig_MSIE){ajax_tooltipObjBig_iframe.style.width=ajax_tooltipObjBig.clientWidth+"px";ajax_tooltipObjBig_iframe.style.height=ajax_tooltipObjBig.clientHeight+"px"}ajax_positionBigTooltip(t)}function ajax_positionBigTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajax_big_tooltip_content").offsetWidth+document.getElementById("ajax_big_tooltip_arrow").offsetWidth;ajax_tooltipObjBig.style.left=t-20+"px";ajax_tooltipObjBig.style.top=n-25+"px"}function ajax_SoTooltip(e,t){if(!ajax_tooltipObjSo){ajax_tooltipObjSo=document.createElement("DIV");ajax_tooltipObjSo.style.position="absolute";ajax_tooltipObjSo.id="ajaxtooltipObj";document.body.appendChild(ajax_tooltipObjSo);var n=document.createElement("DIV");n.className="ajaxtooltip_arrow";n.id="ajaxtooltip_arrow";ajax_tooltipObjSo.appendChild(n);var r=document.createElement("DIV");r.className="ajaxtooltip_content";ajax_tooltipObjSo.appendChild(r);r.id="ajaxtooltip_content";if(ajax_tooltipSo_MSIE){ajax_tooltipObjSo_iframe=document.createElement('<IFRAME frameborder="0">');ajax_tooltipObjSo_iframe.style.position="absolute";ajax_tooltipObjSo_iframe.border="0";ajax_tooltipObjSo_iframe.frameborder=0;ajax_tooltipObjSo_iframe.style.backgroundColor="#FFF";ajax_tooltipObjSo_iframe.src="about:blank";r.appendChild(ajax_tooltipObj_iframe);ajax_tooltipObjSo_iframe.style.left="0px";ajax_tooltipObjSo_iframe.style.top="0px"}}ajax_tooltipObjSo.style.display="block";ajax_loadContent("ajaxtooltip_content",e);if(ajax_tooltipSo_MSIE){ajax_tooltipObjSo_iframe.style.width=ajax_tooltipObjSo.clientWidth+"px";ajax_tooltipObjSo_iframe.style.height=ajax_tooltipObjSo.clientHeight+"px"}ajax_positionSoTooltip(t)}function ajax_positionSoTooltip(e){var t=ajaxTooltip_getLeftPos(e)+e.offsetWidth;var n=ajaxTooltip_getTopPos(e);var r=document.getElementById("ajaxtooltip_content").offsetWidth+document.getElementById("ajaxtooltip_arrow").offsetWidth;ajax_tooltipObjSo.style.left=t-20+"px";ajax_tooltipObjSo.style.top=n-25+"px"}function ajaxTooltip_getTopPos(e){var t=e.offsetTop;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetTop}return t}function ajaxTooltip_getLeftPos(e){var t=e.offsetLeft;while((e=e.offsetParent)!=null){if(e.tagName!="HTML")t+=e.offsetLeft}return t}function hideTooltip(){if(ajax_tooltipObjSo){ajax_tooltipObjSo.style.display="none"}}var x_offset_tooltip=5;var y_offset_tooltip=0;var ajax_tooltipObj=false;var ajax_tooltipObj_iframe=false;var ajax_tooltip_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltip_MSIE=true;var ajax_tooltipObjBig=false;var ajax_tooltipObjBig_iframe=false;var ajax_tooltipBig_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltipBig_MSIE=true;var x_offset_tooltip=5;var y_offset_tooltip=0;var ajax_tooltipObjSo=false;var ajax_tooltipObjSo_iframe=false;var ajax_tooltipSo_MSIE=false;if(navigator.userAgent.indexOf("MSIE")>=0)ajax_tooltipSo_MSIE=true

function findCustomInfo(position,value,corp) { 
	 
	 var url="agentInfo.html?ajax=1&decorator=simple&popup=true&agentRequestId=" + encodeURI(value)
	  ajax_showTooltip1(url,position);
	  }
</script>