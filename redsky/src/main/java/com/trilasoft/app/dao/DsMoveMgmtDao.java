
package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsMoveMgmt;

public interface DsMoveMgmtDao extends GenericDao<DsMoveMgmt, Long>{
	
	List getListById(Long id);

}

