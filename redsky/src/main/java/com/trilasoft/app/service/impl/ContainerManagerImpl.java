/**
 * @Class Name  Container Manager Impl
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ContainerDao;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.service.ContainerManager;

public class ContainerManagerImpl extends GenericManagerImpl<Container, Long> implements ContainerManager { 
	ContainerDao containerDao; 
	public ContainerManagerImpl(ContainerDao containerDao) { 
        super(containerDao); 
        this.containerDao = containerDao; 
    } 
    public List<Container> findByLastName(String lastName) { 
        return containerDao.findByLastName(lastName); 
     } 
    public List findMaxId() {
		return containerDao.findMaxId();
	} 
    public List checkById(Long id) { 
        return containerDao.checkById(id); 
    }
    public List getGross(String shipNumber)
    {
    	return containerDao.getGross(shipNumber);
    }
    public List getTare(String shipNumber){
    	return containerDao.getTare(shipNumber);
    }
    public List getNet(String shipNumber){
    	return containerDao.getNet(shipNumber);
    }
    public List getPieces(String shipNumber){
    	return containerDao.getPieces(shipNumber);
    }
    public List getVolume(String shipNumber){
    	return containerDao.getVolume(shipNumber);
    }
    public List getWeights(String shipNumber){
    	return containerDao.getWeights(shipNumber);
    }
    public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight, String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID)
    {
    	return containerDao.updateMisc(shipNumber,actualGrossWeight,actualNetWeight,actualTareWeight,weightUnit,volumeUnit,updatedBy, sessionCorpID);
    }
	public List getVolumeUnit(String shipNumber) {
		return containerDao.getVolumeUnit(shipNumber);
	}
	public List getWeightUnit(String shipNumber) {
		return containerDao.getWeightUnit(shipNumber);
	}
	public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy){
		return containerDao.updateMiscVolume(shipNumber, actualCubicFeet,corpID,updatedBy);
	}
	public int updateDeleteStatus(Long ids){
		return containerDao.updateDeleteStatus(ids);
	}
	public List searchForwarding(String containerNo, String bookingNo, String corpid, String blNumber, Date sailDate,String lastName,String vesselFilight,String caed){
		return containerDao.searchForwarding(containerNo, bookingNo, corpid, blNumber, sailDate,lastName,vesselFilight,caed);
	}
	public List refreshWeights(String shipNum, String packingMode, String sessionCorpID){
		return containerDao.refreshWeights(shipNum, packingMode, sessionCorpID);
	}
	
	public int refreshContainer(String shipNum, String containerNos, String sessionCorpID,String userName){
		return containerDao.refreshContainer(shipNum, containerNos, sessionCorpID,userName);
	}
	public List containerList(Long serviceOrderId){
		return containerDao.containerList(serviceOrderId);
	}
	public List containerAuditList(Long serviceOrderId){
		return containerDao.containerAuditList(serviceOrderId);
	}
	
	public List getGrossKilo(String shipNumber)
    {
    	return containerDao.getGrossKilo(shipNumber);
    }
    public List getTareKilo(String shipNumber){
    	return containerDao.getTareKilo(shipNumber);
    }
    public List getNetKilo(String shipNumber){
    	return containerDao.getNetKilo(shipNumber);
    }
    public List getVolumeCbm(String shipNumber){
    	return containerDao.getVolumeCbm(shipNumber);
    }
    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
    	return containerDao.updateMiscKilo(shipNumber, actualGrossWeightKilo, actualNetWeightKilo, actualTareWeightKilo, weightUnit, volumeUnit,updatedBy, sessionCorpID);
    }
    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy){
    	return containerDao.updateMiscVolumeCbm(shipNumber, actualCubicMtr,corpID,updatedBy);
    }
    public List findMaximumChild(String shipNm){
    	return containerDao.findMaximumChild(shipNm);
    }
    public List findMinimumChild(String shipNm){
        	return containerDao.findMinimumChild(shipNm);
    }
    public List findCountChild(String shipNm){
            	return containerDao.findCountChild(shipNm);
    }
	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return containerDao.goNextSOChild(sidNum, sessionCorpID, soIdNum);
	}
	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return containerDao.goPrevSOChild(sidNum, sessionCorpID, soIdNum);
	}
	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return containerDao.goSOChild(sidNum, sessionCorpID, soIdNum);
	}
	public Long findRemoteContainer(String idNumber, Long serviceOrderID) {
		return containerDao.findRemoteContainer(idNumber, serviceOrderID);
	}
	public List findRemoteContainerList(String shipNumber) {
		return containerDao.findRemoteContainerList(shipNumber);
	}
	public void deleteNetworkContainer(Long serviceOrderID, String idNumber,String containerStatus) {
		containerDao.deleteNetworkContainer(serviceOrderID, idNumber,containerStatus);		
	}
	public List findByServiceOrderId(Long sofid, String sessionCorpID){
		return containerDao.findByServiceOrderId(sofid, sessionCorpID);
	}
	public List fieldsInfoById(Long grpId, String sessionCorpID){
		return containerDao.fieldsInfoById(grpId, sessionCorpID);
	}
	public List containerListByGrpId(Long grpId, Long contId){
		return containerDao.containerListByGrpId(grpId,contId);
	}
	public List containerListByIdAndgrpId(Long grpId){
		return containerDao.containerListByIdAndgrpId(grpId);
	}
	public List orderIdWithNoContainer(String newIdList, String sessionCorpId){
		return containerDao.orderIdWithNoContainer(newIdList, sessionCorpId);
	}
	public List containersForSizeUpdate(String draftOrdersId){
		return containerDao.containersForSizeUpdate(draftOrdersId);
	}
	 public List containerListOfMaster(Long containerId){
		 return containerDao.containerListOfMaster(containerId);
	 }
	 public List containerWithNoLine(String sessionCorpId,Long grpId,Long masterContainerId){
		 return containerDao.containerWithNoLine(sessionCorpId, grpId, masterContainerId);
	 }
	 public void updateContainerId(Long mastercontainerid, String oldIdList){
		 containerDao.updateContainerId(mastercontainerid, oldIdList);
	 }
	 public List getExistingContainerList(String sessionCorpId,String shipNumber,Long masterContainerId){
		 return containerDao.getExistingContainerList(sessionCorpId, shipNumber, masterContainerId);
	 }
	public List getGrossNetwork(String shipNumber, String corpID) {
		return containerDao.getGrossNetwork(shipNumber, corpID);
	}
	public List getNetNetwork(String shipNumber, String corpID) {
		return containerDao.getNetNetwork(shipNumber, corpID);
	}
	public List getPiecesNetwork(String shipNumber, String corpID) {
		return containerDao.getPiecesNetwork(shipNumber, corpID);
	}
	public List getVolumeNetwork(String shipNumber, String corpID) {
		return containerDao.getVolumeNetwork(shipNumber, corpID);
	}
	public List containerListOtherCorpId(Long sid){
		return containerDao.containerListOtherCorpId(sid);
	}
	public void updateContainerDetails(Long id,String fieldName,String fieldValue,String tableName, String sessionCorpID,String shipNumber, String userName){
		containerDao.updateContainerDetails(id,fieldName,fieldValue,tableName,sessionCorpID,shipNumber, userName);
	}
	public void updateContainerDetailsAllFields(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerDetailsAllFields(id,fieldNetWeight,fieldNetWeightVal,fieldGrossWeight,fieldGrossWeightVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldGrossWeightKilo,fieldGrossWeightKiloVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateContainerDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerDetailsTareWt(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateContainerDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerDetailsDensity(id,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateContainerDetailsNetWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerDetailsNetWtKilo(id,fieldNetWeight,fieldNetWeightVal,fieldGrossWeight,fieldGrossWeightVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldGrossWeightKilo,fieldGrossWeightKiloVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateContainerTareWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerTareWtKilo(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateContainerDetailsDensityMetric(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName){
		containerDao.updateContainerDetailsDensityMetric(id,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateStatus(Long id,String containerStatus, String updateRecords) {
		containerDao.updateStatus(id, containerStatus,updateRecords);
	}
	public String findContainerNumber(String corpId,String shipNumber){
		return containerDao.findContainerNumber(corpId,shipNumber);
	}
	public List findServicePartnerData(String corpId,String shipNumber){
		return containerDao.findServicePartnerData(corpId,shipNumber);
	}
	
	

}

//End of Class.   