// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `orderPhone` VARCHAR(255) ,MODIFY COLUMN `localHrPhone` VARCHAR(255) ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `receivableVat` VARCHAR(30) AFTER `maxupdatedon`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `payableVat` VARCHAR(30) AFTER `receivableVat`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN  `excludeFinancialExtract`  BIT(1) NULL DEFAULT b'0'  AFTER `vendorName` ;

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `flex7` VARCHAR(100) AFTER `status`,ADD COLUMN `flex8` VARCHAR(100) AFTER `flex7`,
 ADD COLUMN `label7` VARCHAR(100) AFTER `flex8`, ADD COLUMN `label8` VARCHAR(100) AFTER `label7`;

ALTER TABLE `redsky`.`company` ADD COLUMN `accountLineNonEditable` 
VARCHAR(100) DEFAULT '' ;

Insert into refmaster (corpid, code, description, fieldlength,parameter)
values ('TSFT', '', '', 100, 'accountLineNonEditableList'),
('TSFT', 'Invoicing', 'Invoicing', 100, 'accountLineNonEditableList'),
('TSFT', 'SentToFinance', 'Sent To Finance', 100, 
'accountLineNonEditableList');

INSERT INTO
parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','accountLineNonEditableList',100,'',now(),'amishra',now(),'amishra',1,1,'');

update refmaster set language ='en' where parameter='accountLineNonEditableList';

update company set accountLineNonEditable='SentToFinance';

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,bucket,createdon,createdby,updatedon,updatedby)values
('TSFT', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en','salesMan',now(),'subrat',now(),'subrat');

//#9801 as per dileep and mintu
update customerfile set quotationStatus='' where controlflag='G' and quotationStatus is null;




//origin and destination country update as per vaibhav after taking dump

select origincountry,origincountrycode from customerfile where origincountry ='Viet nam';

update customerfile set origincountry='Vietnam' where origincountry ='Viet nam';

select destinationcountry,destinationcountrycode from customerfile where destinationcountry='Viet nam';

update customerfile set destinationcountry='Vietnam' where destinationcountry ='Viet nam';


select origincountry,origincountrycode from serviceorder where origincountry ='Viet nam';

update serviceorder set origincountry='Vietnam' where origincountry ='Viet nam';

select destinationcountry,destinationcountrycode from serviceorder where destinationcountry='Viet nam';

update serviceorder set destinationcountry='Vietnam' where destinationcountry ='Viet nam';


select origincountry,origincountrycode from workticket where origincountry ='Viet nam';

update workticket set origincountry='Vietnam' where origincountry ='Viet nam';

select destinationcountry,destinationcountrycode from workticket where destinationcountry='Viet nam';

update workticket set destinationcountry='Vietnam' where destinationcountry ='Viet nam';


************************************************************************



select distinct concat("update customerfile set origincountrycode='",r.code,"' where (origincountrycode is null or origincountrycode='') and origincountry='",origincountry,"';")
from customerfile c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and (c.origincountrycode is null or c.origincountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update customerfile set origincountrycode='",r.code,"' where origincountry='",origincountry,"' and origincountrycode='",c.origincountrycode,"';")
from customerfile c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and c.origincountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update customerfile set origincountry='",r.description,"' where (origincountry is null or origincountry='') and origincountrycode='",c.origincountrycode,"';")
from customerfile c,refmaster r
where (origincountrycode <>'' or origincountrycode is not null)
and r.code=c.origincountrycode
and (c.origincountry is null or c.origincountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update customerfile set destinationcountrycode='",r.code,"' where (destinationcountrycode is null or destinationcountrycode='') and destinationcountry='",destinationcountry,"';")
from customerfile c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and (c.destinationcountrycode is null or c.destinationcountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update customerfile set destinationcountrycode='",r.code,"' where destinationcountry='",destinationcountry,"' and destinationcountrycode='",c.destinationcountrycode,"';")
from customerfile c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and c.destinationcountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update customerfile set destinationcountry='",r.description,"' where (destinationcountry is null or destinationcountry='') and destinationcountrycode='",c.destinationcountrycode,"';")
from customerfile c,refmaster r
where (destinationcountrycode <>'' or destinationcountrycode is not null)
and r.code=c.destinationcountrycode
and (c.destinationcountry is null or c.destinationcountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';




select distinct concat("update serviceorder set origincountrycode='",r.code,"' where (origincountrycode is null or origincountrycode='') and origincountry='",origincountry,"';")
from serviceorder c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and (c.origincountrycode is null or c.origincountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update serviceorder set origincountrycode='",r.code,"' where origincountry='",origincountry,"' and origincountrycode='",c.origincountrycode,"';")
from serviceorder c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and c.origincountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update serviceorder set origincountry='",r.description,"' where (origincountry is null or origincountry='') and origincountrycode='",c.origincountrycode,"';")
from serviceorder c,refmaster r
where (origincountrycode <>'' or origincountrycode is not null)
and r.code=c.origincountrycode
and (c.origincountry is null or c.origincountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update serviceorder set destinationcountrycode='",r.code,"' where (destinationcountrycode is null or destinationcountrycode='') and destinationcountry='",destinationcountry,"';")
from serviceorder c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and (c.destinationcountrycode is null or c.destinationcountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update serviceorder set destinationcountrycode='",r.code,"' where destinationcountry='",destinationcountry,"' and destinationcountrycode='",c.destinationcountrycode,"';")
from serviceorder c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and c.destinationcountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update serviceorder set destinationcountry='",r.description,"' where (destinationcountry is null or destinationcountry='') and destinationcountrycode='",c.destinationcountrycode,"';")
from serviceorder c,refmaster r
where (destinationcountrycode <>'' or destinationcountrycode is not null)
and r.code=c.destinationcountrycode
and (c.destinationcountry is null or c.destinationcountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';





select distinct concat("update workticket set origincountrycode='",r.code,"' where (origincountrycode is null or origincountrycode='') and origincountry='",origincountry,"';")
from workticket c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and (c.origincountrycode is null or c.origincountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update workticket set origincountrycode='",r.code,"' where origincountry='",origincountry,"' and origincountrycode='",c.origincountrycode,"';")
from workticket c,refmaster r
where (origincountry <>'' or origincountry is not null)
and r.description=c.origincountry
and c.origincountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update workticket set origincountry='",r.description,"' where (origincountry is null or origincountry='') and origincountrycode='",c.origincountrycode,"';")
from workticket c,refmaster r
where (origincountrycode <>'' or origincountrycode is not null)
and r.code=c.origincountrycode
and (c.origincountry is null or c.origincountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update workticket set destinationcountrycode='",r.code,"' where (destinationcountrycode is null or destinationcountrycode='') and destinationcountry='",destinationcountry,"';")
from workticket c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and (c.destinationcountrycode is null or c.destinationcountrycode ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update workticket set destinationcountrycode='",r.code,"' where destinationcountry='",destinationcountry,"' and destinationcountrycode='",c.destinationcountrycode,"';")
from workticket c,refmaster r
where (destinationcountry <>'' or destinationcountry is not null)
and r.description=c.destinationcountry
and c.destinationcountrycode<>r.code
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';

select distinct concat("update workticket set destinationcountry='",r.description,"' where (destinationcountry is null or destinationcountry='') and destinationcountrycode='",c.destinationcountrycode,"';")
from workticket c,refmaster r
where (destinationcountrycode <>'' or destinationcountrycode is not null)
and r.code=c.destinationcountrycode
and (c.destinationcountry is null or c.destinationcountry ='')
and r.parameter='COUNTRY' and r.corpid ='TSFT' and r.language='en';
//
