package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.VatIdentification;

public interface VatIdentificationManager extends GenericManager<VatIdentification,Long> {
public List findAllVat();
public List isCountryExisted(String Country ,String corpid);
public List searchAll(String vatCountryCode);
public String getIdVatCode(String vatCountryCode);
}
