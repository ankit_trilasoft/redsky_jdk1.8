
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetQuotePdf2Result" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getQuotePdf2Result"
})
@XmlRootElement(name = "GetQuotePdf2Response")
public class GetQuotePdf2Response {

    @XmlElement(name = "GetQuotePdf2Result")
    protected byte[] getQuotePdf2Result;

    /**
     * Gets the value of the getQuotePdf2Result property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGetQuotePdf2Result() {
        return getQuotePdf2Result;
    }

    /**
     * Sets the value of the getQuotePdf2Result property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGetQuotePdf2Result(byte[] value) {
        this.getQuotePdf2Result = ((byte[]) value);
    }

}
