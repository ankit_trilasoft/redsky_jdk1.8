package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DsLanguage;

public interface DsLanguageManager extends GenericManager<DsLanguage, Long>{
	List getListById(Long id);
}
