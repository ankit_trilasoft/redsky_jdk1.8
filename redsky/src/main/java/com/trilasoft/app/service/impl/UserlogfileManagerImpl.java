package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccountLineDao;
import com.trilasoft.app.dao.UserlogfileDao;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.UserlogfileManager;



public class UserlogfileManagerImpl extends GenericManagerImpl<Userlogfile, Long> implements UserlogfileManager {
	UserlogfileDao userlogfileDao;
	public UserlogfileManagerImpl(UserlogfileDao userlogfileDao) { 
        super(userlogfileDao); 
        this.userlogfileDao = userlogfileDao; 
    }
	public int updateUserLogOut(String sessionId) {
		
		return userlogfileDao.updateUserLogOut(sessionId);
	}
	public List search(String userName, String userType, Date login) {
		return userlogfileDao.search(userName, userType, login);
	}
	public List getexcludeIPsList(String CorpID) { 
		return userlogfileDao.getexcludeIPsList(CorpID);
	}
	public List getPermission(String CorpID, String userName) { 
		return userlogfileDao.getPermission(CorpID, userName);
	}
	public List getAssignedSecurity(String sessionCorpID, Long partnerId) {
		return userlogfileDao.getAssignedSecurity(sessionCorpID, partnerId);
	}
	public List searchuUserLogFile(String sessionCorpID, String userNameSearch, String userTypeSearch, String loginDateSearch, String excludeIPsList) {
		return userlogfileDao.searchuUserLogFile(sessionCorpID, userNameSearch, userTypeSearch, loginDateSearch, excludeIPsList);
	}
	public List findLogTime(String sessionCorpID, String userNamePermission, String loginDate) {
		return userlogfileDao.findLogTime(sessionCorpID, userNamePermission, loginDate);
	} 

}
