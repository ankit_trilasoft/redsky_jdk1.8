package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CompanyDivisionDao;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.service.CompanyDivisionManager;

public class CompanyDivisionManagerImpl  extends GenericManagerImpl<CompanyDivision, Long> implements CompanyDivisionManager { 
	CompanyDivisionDao companyDivisionDao; 
    public CompanyDivisionManagerImpl(CompanyDivisionDao companyDivisionDao) { 
        super(companyDivisionDao); 
        this.companyDivisionDao = companyDivisionDao; 
    }
	public List getInvCompanyCode(String corpID) {
		
		return companyDivisionDao.getInvCompanyCode(corpID);
	}
public List getForUGCNInvCompanyCode(String corpID) {
		
		return companyDivisionDao.getForUGCNInvCompanyCode(corpID);
	}
	public List getPayCompanyCode(String corpID) {
		
		return companyDivisionDao.getPayCompanyCode(corpID);
	}
public List getPayCompanyCodeForUGCN(String corpID) {
		
		return companyDivisionDao.getPayCompanyCodeForUGCN(corpID);
	}
	public List findCompanyCodeList(String corpID) {
		return companyDivisionDao.findCompanyCodeList(corpID);
	}
	public List findCompanyDivFlag(String corpID) {
		return companyDivisionDao.findCompanyDivFlag(corpID);
	}
	public Map<String, String> findDefaultStateList(String bucket2, String corpId){
		return companyDivisionDao.findDefaultStateList(bucket2, corpId);	
	}
	public List checkBookingAgentCode(String bookingAgentCode, String corpID) {
		return companyDivisionDao.checkBookingAgentCode(bookingAgentCode, corpID);
	}
	public List checkCompayCode(String compayCode, String corpID) {
		return companyDivisionDao.checkCompayCode(compayCode, corpID);
	}
	public String searchBaseCurrencyCompanyDivision(String compayCode ,String corpID){
		return companyDivisionDao.searchBaseCurrencyCompanyDivision(compayCode, corpID);
	}
	public List getBookingAgentCode(String companyDivision, String sessionCorpID) {
		return companyDivisionDao.getBookingAgentCode(companyDivision, sessionCorpID);
	}
	public List findCompanyCodeByBookingAgent(String bookingAgentCode,String corpID){
		return companyDivisionDao.findCompanyCodeByBookingAgent(bookingAgentCode, corpID);
	}
	public Map<String, String> getUTSICompanyDivision(String sessionCorpID) {
		return companyDivisionDao.getUTSICompanyDivision(sessionCorpID);
	}
	public Map<String, String> getCompanyDivisionCurrencyMap(String sessionCorpID) {
		return companyDivisionDao.getCompanyDivisionCurrencyMap(sessionCorpID);
	}
	public Map<String, String> getAgentCompanyDivisionCurrencyMap() {
		return companyDivisionDao.getAgentCompanyDivisionCurrencyMap();
	}
	public Map<String, String> getAgentBookingAgentCompanyDivisionCurrencyMap() {
		return companyDivisionDao.getAgentBookingAgentCompanyDivisionCurrencyMap();
	}
	public int findGlobalCompanyCode(String corpID){
		return companyDivisionDao.findGlobalCompanyCode(corpID);
	}
	public Map<String,String> getComDivCodePerCorpIdList(){
		return companyDivisionDao.getComDivCodePerCorpIdList();
	}
	public List findCompanyCodeListForVanLine(String sessionCorpID) {
		return companyDivisionDao.findCompanyCodeListForVanLine(sessionCorpID);
	}
	public List findCMMDMMAgentList() {
		return companyDivisionDao.findCMMDMMAgentList();
	}
}
 