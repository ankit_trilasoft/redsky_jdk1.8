package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;


@Entity
@Table(name="mssgrid")
public class MssGrid extends BaseObject {
	private Long id;
	private Long mssId;
	private String corpId;
	private String transportType;
	private String description;
	private double length;
	private double width;
	private double height;
	private Boolean ply=new Boolean(false);
	private Boolean intlIspmis=new Boolean(false);
	private Boolean appr=new Boolean(true);
	private Boolean cod=new Boolean(false);
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String inventoryFlag;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("mssId", mssId).append("corpId", corpId)
				.append("transportType", transportType)
				.append("description", description).append("length", length)
				.append("width", width).append("height", height)
				.append("ply", ply).append("intlIspmis", intlIspmis)
				.append("appr", appr).append("cod", cod)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("inventoryFlag", inventoryFlag)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MssGrid))
			return false;
		MssGrid castOther = (MssGrid) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(mssId, castOther.mssId)
				.append(corpId, castOther.corpId)
				.append(transportType, castOther.transportType)
				.append(description, castOther.description)
				.append(length, castOther.length)
				.append(width, castOther.width)
				.append(height, castOther.height).append(ply, castOther.ply)
				.append(intlIspmis, castOther.intlIspmis)
				.append(appr, castOther.appr).append(cod, castOther.cod)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(inventoryFlag, castOther.inventoryFlag)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(mssId).append(corpId)
				.append(transportType).append(description).append(length)
				.append(width).append(height).append(ply).append(intlIspmis)
				.append(appr).append(cod).append(createdOn).append(createdBy)
				.append(updatedOn).append(updatedBy).append(inventoryFlag)
				.toHashCode();
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMssId() {
		return mssId;
	}
	public void setMssId(Long mssId) {
		this.mssId = mssId;
	}
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	public String getTransportType() {
		return transportType;
	}
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Boolean getPly() {
		return ply;
	}
	public void setPly(Boolean ply) {
		this.ply = ply;
	}
	public Boolean getIntlIspmis() {
		return intlIspmis;
	}
	public void setIntlIspmis(Boolean intlIspmis) {
		this.intlIspmis = intlIspmis;
	}
	public Boolean getAppr() {
		return appr;
	}
	public void setAppr(Boolean appr) {
		this.appr = appr;
	}
	public Boolean getCod() {
		return cod;
	}
	public void setCod(Boolean cod) {
		this.cod = cod;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}	
	public String getInventoryFlag() {
		return inventoryFlag;
	}
	public void setInventoryFlag(String inventoryFlag) {
		this.inventoryFlag = inventoryFlag;
	}

}
