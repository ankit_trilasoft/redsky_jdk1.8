

delimiter $$

CREATE trigger redsky.trigger_add_history_company
BEFORE UPDATE on redsky.company
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'company.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;






   IF (OLD.address1 <> NEW.address1 ) THEN
      select CONCAT(fieldNameList,'company.address1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address1,'~') into newValueList;
  END IF;



   IF (OLD.address2 <> NEW.address2 ) THEN
      select CONCAT(fieldNameList,'company.address2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address2,'~') into newValueList;
  END IF;
  
  IF (OLD.generateInvoiceBy <> NEW.generateInvoiceBy ) THEN
      select CONCAT(fieldNameList,'company.generateInvoiceBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.generateInvoiceBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.generateInvoiceBy,'~') into newValueList;
  END IF;
  
  IF (OLD.integrationUser  <> NEW.integrationUser  ) THEN
      select CONCAT(fieldNameList,'company.integrationUser ~') into fieldNameList;
      select CONCAT(oldValueList,OLD.integrationUser ,'~') into oldValueList;
      select CONCAT(newValueList,NEW.integrationUser ,'~') into newValueList;
  END IF;
  
  IF (OLD.integrationPassword   <> NEW.integrationPassword   ) THEN
      select CONCAT(fieldNameList,'company.integrationPassword  ~') into fieldNameList;
      select CONCAT(oldValueList,OLD.integrationPassword  ,'~') into oldValueList;
      select CONCAT(newValueList,NEW.integrationPassword  ,'~') into newValueList;
  END IF;



   IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'company.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;



   IF (OLD.companyName <> NEW.companyName ) THEN
      select CONCAT(fieldNameList,'company.companyName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyName,'~') into newValueList;
  END IF;



   IF (OLD.contactNumber <> NEW.contactNumber ) THEN
      select CONCAT(fieldNameList,'company.contactNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactNumber,'~') into newValueList;
  END IF;



   IF (OLD.countryID <> NEW.countryID ) THEN
      select CONCAT(fieldNameList,'company.countryID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.countryID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.countryID,'~') into newValueList;
  END IF;



   IF (OLD.parentCorpId <> NEW.parentCorpId ) THEN
      select CONCAT(fieldNameList,'company.parentCorpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parentCorpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parentCorpId,'~') into newValueList;
  END IF;



   IF (OLD.postal <> NEW.postal ) THEN
      select CONCAT(fieldNameList,'company.postal~') into fieldNameList;
      select CONCAT(oldValueList,OLD.postal,'~') into oldValueList;
      select CONCAT(newValueList,NEW.postal,'~') into newValueList;
  END IF;



   IF (OLD.timeZone <> NEW.timeZone ) THEN
      select CONCAT(fieldNameList,'company.timeZone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.timeZone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.timeZone,'~') into newValueList;
  END IF;



   IF (OLD.autoSavePrompt <> NEW.autoSavePrompt ) THEN
      select CONCAT(fieldNameList,'company.autoSavePrompt~') into fieldNameList;
      select CONCAT(oldValueList,OLD.autoSavePrompt,'~') into oldValueList;
      select CONCAT(newValueList,NEW.autoSavePrompt,'~') into newValueList;
  END IF;



   IF (OLD.companyDivisionFlag <> NEW.companyDivisionFlag ) THEN
      select CONCAT(fieldNameList,'company.companyDivisionFlag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivisionFlag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivisionFlag,'~') into newValueList;
  END IF;



   IF (OLD.reportValidationFlag <> NEW.reportValidationFlag ) THEN
      select CONCAT(fieldNameList,'company.reportValidationFlag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportValidationFlag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportValidationFlag,'~') into newValueList;
  END IF;



   IF (OLD.lastInvoiceNumber <> NEW.lastInvoiceNumber ) THEN
      select CONCAT(fieldNameList,'company.lastInvoiceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastInvoiceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastInvoiceNumber,'~') into newValueList;
  END IF;



   IF (OLD.lastTicketNumber <> NEW.lastTicketNumber ) THEN
      select CONCAT(fieldNameList,'company.lastTicketNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastTicketNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastTicketNumber,'~') into newValueList;
  END IF;



   IF (OLD.lastClaimNumber <> NEW.lastClaimNumber ) THEN
      select CONCAT(fieldNameList,'company.lastClaimNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastClaimNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastClaimNumber,'~') into newValueList;
  END IF;



   IF (OLD.toDoRuleExecutionTime <> NEW.toDoRuleExecutionTime ) THEN
      select CONCAT(fieldNameList,'company.toDoRuleExecutionTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.toDoRuleExecutionTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.toDoRuleExecutionTime,'~') into newValueList;
  END IF;



   IF (OLD.multiCurrency <> NEW.multiCurrency ) THEN
      select CONCAT(fieldNameList,'company.multiCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.multiCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.multiCurrency,'~') into newValueList;
  END IF;



   IF (OLD.userActionOnExpiry <> NEW.userActionOnExpiry ) THEN
      select CONCAT(fieldNameList,'company.userActionOnExpiry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.userActionOnExpiry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.userActionOnExpiry,'~') into newValueList;
  END IF;



   IF (OLD.partnerActionOnExpiry <> NEW.partnerActionOnExpiry ) THEN
      select CONCAT(fieldNameList,'company.partnerActionOnExpiry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerActionOnExpiry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerActionOnExpiry,'~') into newValueList;
  END IF;



   IF (OLD.lastCreditInvoice <> NEW.lastCreditInvoice ) THEN
      select CONCAT(fieldNameList,'company.lastCreditInvoice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastCreditInvoice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastCreditInvoice,'~') into newValueList;
  END IF;



   IF (OLD.parentID <> NEW.parentID ) THEN
      select CONCAT(fieldNameList,'company.parentID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parentID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parentID,'~') into newValueList;
  END IF;



   IF (OLD.currencySign <> NEW.currencySign ) THEN
      select CONCAT(fieldNameList,'company.currencySign~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currencySign,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currencySign,'~') into newValueList;
  END IF;



   IF (OLD.localeCountry <> NEW.localeCountry ) THEN
      select CONCAT(fieldNameList,'company.localeCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.localeCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.localeCountry,'~') into newValueList;
  END IF;



   IF (OLD.localeLanguage <> NEW.localeLanguage ) THEN
      select CONCAT(fieldNameList,'company.localeLanguage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.localeLanguage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.localeLanguage,'~') into newValueList;
  END IF;



   IF (OLD.jobType1 <> NEW.jobType1 ) THEN
      select CONCAT(fieldNameList,'company.jobType1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType1,'~') into newValueList;
  END IF;



   IF (OLD.jobType2 <> NEW.jobType2 ) THEN
      select CONCAT(fieldNameList,'company.jobType2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType2,'~') into newValueList;
  END IF;



   IF (OLD.jobType3 <> NEW.jobType3 ) THEN
      select CONCAT(fieldNameList,'company.jobType3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType3,'~') into newValueList;
  END IF;



   IF (OLD.distanceFrom1 <> NEW.distanceFrom1 ) THEN
      select CONCAT(fieldNameList,'company.distanceFrom1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.distanceFrom1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.distanceFrom1,'~') into newValueList;
  END IF;



   IF (OLD.distanceFrom2 <> NEW.distanceFrom2 ) THEN
      select CONCAT(fieldNameList,'company.distanceFrom2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.distanceFrom2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.distanceFrom2,'~') into newValueList;
  END IF;



   IF (OLD.distanceFrom3 <> NEW.distanceFrom3 ) THEN
      select CONCAT(fieldNameList,'company.distanceFrom3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.distanceFrom3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.distanceFrom3,'~') into newValueList;
  END IF;



   IF (OLD.grpDefault <> NEW.grpDefault ) THEN
      select CONCAT(fieldNameList,'company.grpDefault~') into fieldNameList;
      select CONCAT(oldValueList,OLD.grpDefault,'~') into oldValueList;
      select CONCAT(newValueList,NEW.grpDefault,'~') into newValueList;
  END IF;



   IF (OLD.estimateVATFlag <> NEW.estimateVATFlag ) THEN
      select CONCAT(fieldNameList,'company.estimateVATFlag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimateVATFlag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimateVATFlag,'~') into newValueList;
  END IF;



   IF (OLD.customerFeedback <> NEW.customerFeedback ) THEN
      select CONCAT(fieldNameList,'company.customerFeedback~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerFeedback,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerFeedback,'~') into newValueList;
  END IF;



   IF (OLD.autoGenerateAccRef <> NEW.autoGenerateAccRef ) THEN
      select CONCAT(fieldNameList,'company.autoGenerateAccRef~') into fieldNameList;
      select CONCAT(oldValueList,OLD.autoGenerateAccRef,'~') into oldValueList;
      select CONCAT(newValueList,NEW.autoGenerateAccRef,'~') into newValueList;
  END IF;



   IF (OLD.lastTripNumber <> NEW.lastTripNumber ) THEN
      select CONCAT(fieldNameList,'company.lastTripNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastTripNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastTripNumber,'~') into newValueList;
  END IF;



   IF (OLD.surveyEmailSign <> NEW.surveyEmailSign ) THEN
      select CONCAT(fieldNameList,'company.surveyEmailSign~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyEmailSign,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyEmailSign,'~') into newValueList;
  END IF;



   IF (OLD.certificateAdditionalNumber <> NEW.certificateAdditionalNumber ) THEN
      select CONCAT(fieldNameList,'company.certificateAdditionalNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.certificateAdditionalNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.certificateAdditionalNumber,'~') into newValueList;
  END IF;



   IF (OLD.RSBillingInstructions <> NEW.RSBillingInstructions ) THEN
      select CONCAT(fieldNameList,'company.RSBillingInstructions~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RSBillingInstructions,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RSBillingInstructions,'~') into newValueList;
  END IF;



   IF (OLD.ftpDir <> NEW.ftpDir ) THEN
      select CONCAT(fieldNameList,'company.ftpDir~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ftpDir,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ftpDir,'~') into newValueList;
  END IF;



   IF (OLD.rskyBillGroup <> NEW.rskyBillGroup ) THEN
      select CONCAT(fieldNameList,'company.rskyBillGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rskyBillGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rskyBillGroup,'~') into newValueList;
  END IF;



   IF (OLD.email <> NEW.email ) THEN
      select CONCAT(fieldNameList,'company.email~') into fieldNameList;
      select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
      select CONCAT(newValueList,NEW.email,'~') into newValueList;
  END IF;








  IF ((OLD.lastRunDate <> NEW.lastRunDate) or (OLD.lastRunDate is null and NEW.lastRunDate is not null)
      or (OLD.lastRunDate is not null and NEW.lastRunDate is null)) THEN
       select CONCAT(fieldNameList,'company.lastRunDate~') into fieldNameList;
       IF(OLD.lastRunDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.lastRunDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.lastRunDate is not null) THEN
        	select CONCAT(oldValueList,OLD.lastRunDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.lastRunDate is not null) THEN
        	select CONCAT(newValueList,NEW.lastRunDate,'~') into newValueList;
      	END IF;
   END IF;



     IF ((OLD.goLiveDate <> NEW.goLiveDate) or (OLD.goLiveDate is null and NEW.goLiveDate is not null)
      or (OLD.goLiveDate is not null and NEW.goLiveDate is null)) THEN
       select CONCAT(fieldNameList,'company.goLiveDate~') into fieldNameList;
       IF(OLD.goLiveDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.goLiveDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.goLiveDate is not null) THEN
        	select CONCAT(oldValueList,OLD.goLiveDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.goLiveDate is not null) THEN
        	select CONCAT(newValueList,NEW.goLiveDate,'~') into newValueList;
      	END IF;
   END IF;



  IF (OLD.networkFlag <> NEW.networkFlag ) THEN
       select CONCAT(fieldNameList,'company.networkFlag~') into fieldNameList;
       IF(OLD.networkFlag = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.networkFlag = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.networkFlag = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.networkFlag = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.customerSurveyByEmail <> NEW.customerSurveyByEmail ) THEN
       select CONCAT(fieldNameList,'company.customerSurveyByEmail~') into fieldNameList;
       IF(OLD.customerSurveyByEmail = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.customerSurveyByEmail = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.customerSurveyByEmail = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.customerSurveyByEmail = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.customerSurveyOnCustomerPortal <> NEW.customerSurveyOnCustomerPortal ) THEN
       select CONCAT(fieldNameList,'company.customerSurveyOnCustomerPortal~') into fieldNameList;
       IF(OLD.customerSurveyOnCustomerPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.customerSurveyOnCustomerPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.customerSurveyOnCustomerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.customerSurveyOnCustomerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.restrictAccess <> NEW.restrictAccess ) THEN
       select CONCAT(fieldNameList,'company.restrictAccess~') into fieldNameList;
       IF(OLD.restrictAccess = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.restrictAccess = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.restrictAccess = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.restrictAccess = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.extractedFileAudit <> NEW.extractedFileAudit ) THEN
       select CONCAT(fieldNameList,'company.extractedFileAudit~') into fieldNameList;
       IF(OLD.extractedFileAudit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.extractedFileAudit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.extractedFileAudit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.extractedFileAudit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

     IF (OLD.flagDefaultVendorAccountLines <> NEW.flagDefaultVendorAccountLines ) THEN
       select CONCAT(fieldNameList,'company.flagDefaultVendorAccountLines~') into fieldNameList;
       IF(OLD.flagDefaultVendorAccountLines = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.flagDefaultVendorAccountLines = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.flagDefaultVendorAccountLines = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.flagDefaultVendorAccountLines = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.tracInternalUser <> NEW.tracInternalUser ) THEN
       select CONCAT(fieldNameList,'company.tracInternalUser~') into fieldNameList;
       IF(OLD.tracInternalUser = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.tracInternalUser = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.tracInternalUser = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.tracInternalUser = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.UTSI <> NEW.UTSI ) THEN
       select CONCAT(fieldNameList,'company.UTSI~') into fieldNameList;
       IF(OLD.UTSI = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.UTSI = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.UTSI = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.UTSI = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.voxmeIntegration <> NEW.voxmeIntegration ) THEN
       select CONCAT(fieldNameList,'company.voxmeIntegration~') into fieldNameList;
       IF(OLD.voxmeIntegration = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.voxmeIntegration = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.voxmeIntegration = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.voxmeIntegration = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.weeklyBilling <> NEW.weeklyBilling ) THEN
       select CONCAT(fieldNameList,'company.weeklyBilling~') into fieldNameList;
       IF(OLD.weeklyBilling = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.weeklyBilling = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.weeklyBilling = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.weeklyBilling = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.TransfereeInfopackage <> NEW.TransfereeInfopackage ) THEN
       select CONCAT(fieldNameList,'company.TransfereeInfopackage~') into fieldNameList;
       IF(OLD.TransfereeInfopackage = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TransfereeInfopackage = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TransfereeInfopackage = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TransfereeInfopackage = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.justSayYes <> NEW.justSayYes ) THEN
       select CONCAT(fieldNameList,'company.justSayYes~') into fieldNameList;
       IF(OLD.justSayYes = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.justSayYes = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.justSayYes = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.justSayYes = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.qualitySurvey <> NEW.qualitySurvey ) THEN
       select CONCAT(fieldNameList,'company.qualitySurvey~') into fieldNameList;
       IF(OLD.qualitySurvey = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.qualitySurvey = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.qualitySurvey = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.qualitySurvey = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.workticketQueue <> NEW.workticketQueue ) THEN
       select CONCAT(fieldNameList,'company.workticketQueue~') into fieldNameList;
       IF(OLD.workticketQueue = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.workticketQueue = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.workticketQueue = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.workticketQueue = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



  IF (OLD.useCostTransferred <> NEW.useCostTransferred ) THEN
       select CONCAT(fieldNameList,'company.useCostTransferred~') into fieldNameList;
       IF(OLD.useCostTransferred = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.useCostTransferred = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.useCostTransferred = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.useCostTransferred = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.creditInvoiceSequence <> NEW.creditInvoiceSequence ) THEN
       select CONCAT(fieldNameList,'company.creditInvoiceSequence~') into fieldNameList;
       IF(OLD.creditInvoiceSequence = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.creditInvoiceSequence = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.creditInvoiceSequence = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.creditInvoiceSequence = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.securityChecked <> NEW.securityChecked ) THEN
       select CONCAT(fieldNameList,'company.securityChecked~') into fieldNameList;
       IF(OLD.securityChecked = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.securityChecked = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.securityChecked = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.securityChecked = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.payablesXferWithApprovalOnly <> NEW.payablesXferWithApprovalOnly ) THEN
       select CONCAT(fieldNameList,'company.payablesXferWithApprovalOnly~') into fieldNameList;
       IF(OLD.payablesXferWithApprovalOnly = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.payablesXferWithApprovalOnly = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.payablesXferWithApprovalOnly = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.payablesXferWithApprovalOnly = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.vanlineEnabled <> NEW.vanlineEnabled ) THEN
       select CONCAT(fieldNameList,'company.vanlineEnabled~') into fieldNameList;
       IF(OLD.vanlineEnabled = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.vanlineEnabled = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.vanlineEnabled = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.vanlineEnabled = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.printInsuranceCertificate <> NEW.printInsuranceCertificate ) THEN
       select CONCAT(fieldNameList,'company.printInsuranceCertificate~') into fieldNameList;
       IF(OLD.printInsuranceCertificate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.printInsuranceCertificate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.printInsuranceCertificate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.printInsuranceCertificate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.postingDateStop <> NEW.postingDateStop ) THEN
       select CONCAT(fieldNameList,'company.postingDateStop~') into fieldNameList;
       IF(OLD.postingDateStop = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.postingDateStop = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.postingDateStop = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.postingDateStop = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



  IF (OLD.automaticLinkup <> NEW.automaticLinkup ) THEN
       select CONCAT(fieldNameList,'company.automaticLinkup~') into fieldNameList;
       IF(OLD.automaticLinkup = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.automaticLinkup = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.automaticLinkup = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.automaticLinkup = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.postingDateFlexibility <> NEW.postingDateFlexibility ) THEN
       select CONCAT(fieldNameList,'company.postingDateFlexibility~') into fieldNameList;
       IF(OLD.postingDateFlexibility = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.postingDateFlexibility = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.postingDateFlexibility = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.postingDateFlexibility = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.cmmdmmAgent <> NEW.cmmdmmAgent ) THEN
       select CONCAT(fieldNameList,'company.cmmdmmAgent~') into fieldNameList;
       IF(OLD.cmmdmmAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.cmmdmmAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.cmmdmmAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.cmmdmmAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.autoFxUpdater <> NEW.autoFxUpdater ) THEN
       select CONCAT(fieldNameList,'company.autoFxUpdater~') into fieldNameList;
       IF(OLD.autoFxUpdater = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.autoFxUpdater = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.autoFxUpdater = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.autoFxUpdater = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.rulesRunning <> NEW.rulesRunning ) THEN
       select CONCAT(fieldNameList,'company.rulesRunning~') into fieldNameList;
       IF(OLD.rulesRunning = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.rulesRunning = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.rulesRunning = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.rulesRunning = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF ((OLD.defaultBillingRate <> NEW.defaultBillingRate) or (OLD.defaultBillingRate is null and NEW.defaultBillingRate is not null)
      or (OLD.defaultBillingRate is not null and NEW.defaultBillingRate is null)) THEN
        select CONCAT(fieldNameList,'company.defaultBillingRate~') into fieldNameList;
        IF(OLD.defaultBillingRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.defaultBillingRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.defaultBillingRate is not null) then
            select CONCAT(oldValueList,OLD.defaultBillingRate,'~') into oldValueList;
        END IF;
        IF(NEW.defaultBillingRate is not null) then
            select CONCAT(newValueList,NEW.defaultBillingRate,'~') into newValueList;
        END IF;
   END IF;     
    


    

    IF ((OLD.rate2 <> NEW.rate2) or (OLD.rate2 is null and NEW.rate2 is not null)
      or (OLD.rate2 is not null and NEW.rate2 is null)) THEN
        select CONCAT(fieldNameList,'company.rate2~') into fieldNameList;
        IF(OLD.rate2 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rate2 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rate2 is not null) then
            select CONCAT(oldValueList,OLD.rate2,'~') into oldValueList;
        END IF;
        IF(NEW.rate2 is not null) then
            select CONCAT(newValueList,NEW.rate2,'~') into newValueList;
        END IF;
   END IF;     
    
 IF (OLD.userPasswordExpiryDuration <> NEW.userPasswordExpiryDuration ) THEN
      select CONCAT(fieldNameList,'company.userPasswordExpiryDuration~') into fieldNameList;
      select CONCAT(oldValueList,OLD.userPasswordExpiryDuration,'~') into oldValueList;
      select CONCAT(newValueList,NEW.userPasswordExpiryDuration,'~') into newValueList;
  END IF;
  IF (OLD.partnerPasswordExpiryDuration <> NEW.partnerPasswordExpiryDuration ) THEN
      select CONCAT(fieldNameList,'company.partnerPasswordExpiryDuration~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerPasswordExpiryDuration,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerPasswordExpiryDuration,'~') into newValueList;
  END IF;
  IF (OLD.RSBillingInstructions <> NEW.RSBillingInstructions ) THEN
      select CONCAT(fieldNameList,'company.RSBillingInstructions~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RSBillingInstructions,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RSBillingInstructions,'~') into newValueList;
  END IF;
  IF (OLD.restrictedMassage <> NEW.restrictedMassage ) THEN
      select CONCAT(fieldNameList,'company.restrictedMassage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.restrictedMassage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.restrictedMassage,'~') into newValueList;
  END IF;
  IF (OLD.scaling <> NEW.scaling ) THEN
      select CONCAT(fieldNameList,'company.scaling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.scaling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.scaling,'~') into newValueList;
  END IF;
  IF (OLD.clientID <> NEW.clientID ) THEN
      select CONCAT(fieldNameList,'company.clientID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.clientID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.clientID,'~') into newValueList;
  END IF;
  IF (OLD.ccEmail <> NEW.ccEmail ) THEN
      select CONCAT(fieldNameList,'company.ccEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ccEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ccEmail,'~') into newValueList;
  END IF;
  IF (OLD.mmClientID <> NEW.mmClientID ) THEN
      select CONCAT(fieldNameList,'company.mmClientID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mmClientID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mmClientID,'~') into newValueList;
  END IF;
  IF (OLD.numberOfPricePointUser <> NEW.numberOfPricePointUser ) THEN
      select CONCAT(fieldNameList,'company.numberOfPricePointUser~') into fieldNameList;
      select CONCAT(oldValueList,OLD.numberOfPricePointUser,'~') into oldValueList;
      select CONCAT(newValueList,NEW.numberOfPricePointUser,'~') into newValueList;
  END IF;
  IF (OLD.landingPageWelcomeMsg <> NEW.landingPageWelcomeMsg ) THEN
      select CONCAT(fieldNameList,'company.landingPageWelcomeMsg~') into fieldNameList;
      select CONCAT(oldValueList,OLD.landingPageWelcomeMsg,'~') into oldValueList;
      select CONCAT(newValueList,NEW.landingPageWelcomeMsg,'~') into newValueList;
  END IF;
  IF (OLD.privateCustomerTCode <> NEW.privateCustomerTCode ) THEN
      select CONCAT(fieldNameList,'company.privateCustomerTCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateCustomerTCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateCustomerTCode,'~') into newValueList;
  END IF;
  IF ((OLD.clientBillingDate <> NEW.clientBillingDate) or (OLD.clientBillingDate is null and NEW.clientBillingDate is not null)
      or (OLD.clientBillingDate is not null and NEW.clientBillingDate is null)) THEN

       select CONCAT(fieldNameList,'company.clientBillingDate~') into fieldNameList;
        IF(OLD.clientBillingDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.clientBillingDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.clientBillingDate is not null) THEN
        	select CONCAT(oldValueList,OLD.clientBillingDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.clientBillingDate is not null) THEN
        	select CONCAT(newValueList,NEW.clientBillingDate,'~') into newValueList;
      	END IF;

   END IF;
   IF ((OLD.priceStartDate <> NEW.priceStartDate) or (OLD.priceStartDate is null and NEW.priceStartDate is not null)
      or (OLD.priceStartDate is not null and NEW.priceStartDate is null)) THEN

       select CONCAT(fieldNameList,'company.priceStartDate~') into fieldNameList;
        IF(OLD.priceStartDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.priceStartDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.priceStartDate is not null) THEN
        	select CONCAT(oldValueList,OLD.priceStartDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.priceStartDate is not null) THEN
        	select CONCAT(newValueList,NEW.priceStartDate,'~') into newValueList;
      	END IF;

   END IF;
   IF ((OLD.priceEndDate <> NEW.priceEndDate) or (OLD.priceEndDate is null and NEW.priceEndDate is not null)
      or (OLD.priceEndDate is not null and NEW.priceEndDate is null)) THEN

       select CONCAT(fieldNameList,'company.priceEndDate~') into fieldNameList;
        IF(OLD.priceEndDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.priceEndDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.priceEndDate is not null) THEN
        	select CONCAT(oldValueList,OLD.priceEndDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.priceEndDate is not null) THEN
        	select CONCAT(newValueList,NEW.priceEndDate,'~') into newValueList;
      	END IF;

   END IF;
   IF ((OLD.mmStartDate <> NEW.mmStartDate) or (OLD.mmStartDate is null and NEW.mmStartDate is not null)
      or (OLD.mmStartDate is not null and NEW.mmStartDate is null)) THEN

       select CONCAT(fieldNameList,'company.mmStartDate~') into fieldNameList;
        IF(OLD.mmStartDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.mmStartDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.mmStartDate is not null) THEN
        	select CONCAT(oldValueList,OLD.mmStartDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.mmStartDate is not null) THEN
        	select CONCAT(newValueList,NEW.mmStartDate,'~') into newValueList;
      	END IF;

   END IF;
   IF ((OLD.mmEndDate <> NEW.mmEndDate) or (OLD.mmEndDate is null and NEW.mmEndDate is not null)
      or (OLD.mmEndDate is not null and NEW.mmEndDate is null)) THEN

       select CONCAT(fieldNameList,'company.mmEndDate~') into fieldNameList;
        IF(OLD.mmEndDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.mmEndDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.mmEndDate is not null) THEN
        	select CONCAT(oldValueList,OLD.mmEndDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.mmEndDate is not null) THEN
        	select CONCAT(newValueList,NEW.mmEndDate,'~') into newValueList;
      	END IF;

   END IF;
   IF (OLD.allowFutureActualDateEntry <> NEW.allowFutureActualDateEntry ) THEN
       select CONCAT(fieldNameList,'company.allowFutureActualDateEntry~') into fieldNameList;
       IF(OLD.allowFutureActualDateEntry = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.allowFutureActualDateEntry = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.allowFutureActualDateEntry = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.allowFutureActualDateEntry = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.enablePricePoint <> NEW.enablePricePoint ) THEN
       select CONCAT(fieldNameList,'company.enablePricePoint~') into fieldNameList;
       IF(OLD.enablePricePoint = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.enablePricePoint = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.enablePricePoint = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.enablePricePoint = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.cportalAccessCompanyDivisionLevel <> NEW.cportalAccessCompanyDivisionLevel ) THEN
       select CONCAT(fieldNameList,'company.cportalAccessCompanyDivisionLevel~') into fieldNameList;
       IF(OLD.cportalAccessCompanyDivisionLevel = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.cportalAccessCompanyDivisionLevel = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.cportalAccessCompanyDivisionLevel = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.cportalAccessCompanyDivisionLevel = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.chargeDiscountSetting <> NEW.chargeDiscountSetting ) THEN
       select CONCAT(fieldNameList,'company.chargeDiscountSetting~') into fieldNameList;
       IF(OLD.chargeDiscountSetting = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.chargeDiscountSetting = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.chargeDiscountSetting = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.chargeDiscountSetting = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.accessQuotationFromCustomerFile <> NEW.accessQuotationFromCustomerFile ) THEN
       select CONCAT(fieldNameList,'company.accessQuotationFromCustomerFile~') into fieldNameList;
       IF(OLD.accessQuotationFromCustomerFile = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accessQuotationFromCustomerFile = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accessQuotationFromCustomerFile = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accessQuotationFromCustomerFile = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.enableMobileMover <> NEW.enableMobileMover ) THEN
       select CONCAT(fieldNameList,'company.enableMobileMover~') into fieldNameList;
       IF(OLD.enableMobileMover = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.enableMobileMover = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.enableMobileMover = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.enableMobileMover = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.agentTDREmail <> NEW.agentTDREmail ) THEN
       select CONCAT(fieldNameList,'company.agentTDREmail~') into fieldNameList;
       IF(OLD.agentTDREmail = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.agentTDREmail = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.agentTDREmail = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.agentTDREmail = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.singleCompanyDivisionInvoicing <> NEW.singleCompanyDivisionInvoicing ) THEN
       select CONCAT(fieldNameList,'company.singleCompanyDivisionInvoicing~') into fieldNameList;
       IF(OLD.singleCompanyDivisionInvoicing = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.singleCompanyDivisionInvoicing = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.singleCompanyDivisionInvoicing = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.singleCompanyDivisionInvoicing = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.UGRNNetting <> NEW.UGRNNetting ) THEN
       select CONCAT(fieldNameList,'company.UGRNNetting~') into fieldNameList;
       IF(OLD.UGRNNetting = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.UGRNNetting = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.UGRNNetting = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.UGRNNetting = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF ((OLD.subscriptionAmt <> NEW.subscriptionAmt) or (OLD.subscriptionAmt is null and NEW.subscriptionAmt is not null)
      or (OLD.subscriptionAmt is not null and NEW.subscriptionAmt is null)) THEN
        select CONCAT(fieldNameList,'company.subscriptionAmt~') into fieldNameList;
        IF(OLD.subscriptionAmt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.subscriptionAmt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.subscriptionAmt is not null) then
            select CONCAT(oldValueList,OLD.subscriptionAmt,'~') into oldValueList;
        END IF;
        IF(NEW.subscriptionAmt is not null) then
            select CONCAT(newValueList,NEW.subscriptionAmt,'~') into newValueList;
        END IF;
   END IF; 
IF ((OLD.mmSubscriptionAmt <> NEW.mmSubscriptionAmt) or (OLD.mmSubscriptionAmt is null and NEW.mmSubscriptionAmt is not null)
      or (OLD.mmSubscriptionAmt is not null and NEW.mmSubscriptionAmt is null)) THEN
        select CONCAT(fieldNameList,'company.mmSubscriptionAmt~') into fieldNameList;
        IF(OLD.mmSubscriptionAmt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.mmSubscriptionAmt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.mmSubscriptionAmt is not null) then
            select CONCAT(oldValueList,OLD.mmSubscriptionAmt,'~') into oldValueList;
        END IF;
        IF(NEW.mmSubscriptionAmt is not null) then
            select CONCAT(newValueList,NEW.mmSubscriptionAmt,'~') into newValueList;
        END IF;
   END IF; 
   IF ((OLD.rsAmount <> NEW.rsAmount) or (OLD.rsAmount is null and NEW.rsAmount is not null)
      or (OLD.rsAmount is not null and NEW.rsAmount is null)) THEN
        select CONCAT(fieldNameList,'company.rsAmount~') into fieldNameList;
        IF(OLD.rsAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rsAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rsAmount is not null) then
            select CONCAT(oldValueList,OLD.rsAmount,'~') into oldValueList;
        END IF;
        IF(NEW.rsAmount is not null) then
            select CONCAT(newValueList,NEW.rsAmount,'~') into newValueList;
        END IF;
   END IF;
   IF ((OLD.minimumBillAmount <> NEW.minimumBillAmount) or (OLD.minimumBillAmount is null and NEW.minimumBillAmount is not null)
      or (OLD.minimumBillAmount is not null and NEW.minimumBillAmount is null)) THEN
        select CONCAT(fieldNameList,'company.minimumBillAmount~') into fieldNameList;
        IF(OLD.minimumBillAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.minimumBillAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.minimumBillAmount is not null) then
            select CONCAT(oldValueList,OLD.minimumBillAmount,'~') into oldValueList;
        END IF;
        IF(NEW.minimumBillAmount is not null) then
            select CONCAT(newValueList,NEW.minimumBillAmount,'~') into newValueList;
        END IF;
   END IF; 

   
   
   
   
   
   
   
   IF (OLD.accessRedSkyCportalapp <> NEW.accessRedSkyCportalapp ) THEN
       select CONCAT(fieldNameList,'company.accessRedSkyCportalapp~') into fieldNameList;
       IF(OLD.accessRedSkyCportalapp = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accessRedSkyCportalapp = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accessRedSkyCportalapp = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accessRedSkyCportalapp = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


 IF (OLD.quoteServices <> NEW.quoteServices ) THEN
      select CONCAT(fieldNameList,'company.quoteServices~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quoteServices,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quoteServices,'~') into newValueList;
  END IF;

 IF (OLD.surveyLinking <> NEW.surveyLinking ) THEN
       select CONCAT(fieldNameList,'company.surveyLinking~') into fieldNameList;
       IF(OLD.accessRedSkyCportalapp = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accessRedSkyCportalapp = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accessRedSkyCportalapp = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accessRedSkyCportalapp = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

 IF (OLD.storageBillingStatus <> NEW.storageBillingStatus ) THEN
       select CONCAT(fieldNameList,'company.storageBillingStatus~') into fieldNameList;
       IF(OLD.storageBillingStatus = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.storageBillingStatus = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.storageBillingStatus = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.storageBillingStatus = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'company.status~') into fieldNameList;
       IF(OLD.status = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.status = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.status = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.status = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

 IF (OLD.accountLineNonEditable <> NEW.accountLineNonEditable ) THEN
      select CONCAT(fieldNameList,'company.accountLineNonEditable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountLineNonEditable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountLineNonEditable,'~') into newValueList;
  END IF;


IF (OLD.subOADASoftValidation <> NEW.subOADASoftValidation ) THEN
       select CONCAT(fieldNameList,'company.subOADASoftValidation~') into fieldNameList;
       IF(OLD.subOADASoftValidation = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.subOADASoftValidation = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.subOADASoftValidation = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.subOADASoftValidation = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.oAdAWeightVolumeMandatoryValidation <> NEW.oAdAWeightVolumeMandatoryValidation ) THEN
       select CONCAT(fieldNameList,'company.oAdAWeightVolumeMandatoryValidation~') into fieldNameList;
       IF(OLD.oAdAWeightVolumeMandatoryValidation = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.oAdAWeightVolumeMandatoryValidation = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.oAdAWeightVolumeMandatoryValidation = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.oAdAWeightVolumeMandatoryValidation = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.historyFx <> NEW.historyFx ) THEN
       select CONCAT(fieldNameList,'company.historyFx~') into fieldNameList;
       IF(OLD.historyFx = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.historyFx = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.historyFx = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.historyFx = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.enableMoveForYou <> NEW.enableMoveForYou ) THEN
       select CONCAT(fieldNameList,'company.enableMoveForYou~') into fieldNameList;
       IF(OLD.enableMoveForYou = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.enableMoveForYou = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.enableMoveForYou = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.enableMoveForYou = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   IF (OLD.enableMSS <> NEW.enableMSS ) THEN
       select CONCAT(fieldNameList,'company.enableMSS~') into fieldNameList;
       IF(OLD.enableMSS = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.enableMSS = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.enableMSS = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.enableMSS = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.insurancePremiumTax <> NEW.insurancePremiumTax ) THEN
      select CONCAT(fieldNameList,'company.insurancePremiumTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insurancePremiumTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insurancePremiumTax,'~') into newValueList;
  END IF;

IF ((OLD.insurancePremiumTaxValue <> NEW.insurancePremiumTaxValue) or (OLD.insurancePremiumTaxValue is null and NEW.insurancePremiumTaxValue is not null)
      or (OLD.insurancePremiumTaxValue is not null and NEW.insurancePremiumTaxValue is null)) THEN
        select CONCAT(fieldNameList,'company.insurancePremiumTaxValue~') into fieldNameList;
        IF(OLD.insurancePremiumTaxValue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.insurancePremiumTaxValue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.insurancePremiumTaxValue is not null) then
            select CONCAT(oldValueList,OLD.insurancePremiumTaxValue,'~') into oldValueList;
        END IF;
        IF(NEW.insurancePremiumTaxValue is not null) then
            select CONCAT(newValueList,NEW.insurancePremiumTaxValue,'~') into newValueList;
        END IF;
   END IF;


    IF (OLD.vatInsurancePremiumTax <> NEW.vatInsurancePremiumTax ) THEN
      select CONCAT(fieldNameList,'company.vatInsurancePremiumTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vatInsurancePremiumTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vatInsurancePremiumTax,'~') into newValueList;
  END IF;


   IF (OLD.forceSoDashboard <> NEW.forceSoDashboard ) THEN
      select CONCAT(fieldNameList,'company.forceSoDashboard~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forceSoDashboard,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forceSoDashboard,'~') into newValueList;
  END IF;

   IF (OLD.forceDocCenter <> NEW.forceDocCenter ) THEN
      select CONCAT(fieldNameList,'company.forceDocCenter~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forceDocCenter,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forceDocCenter,'~') into newValueList;
  END IF;

     IF (OLD.displayAPortalRevision <> NEW.displayAPortalRevision ) THEN
      select CONCAT(fieldNameList,'company.displayAPortalRevision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayAPortalRevision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayAPortalRevision,'~') into newValueList;
  END IF;
  
  
  IF (OLD.agentInvoiceSeed <> NEW.agentInvoiceSeed ) THEN
       select CONCAT(fieldNameList,'company.agentInvoiceSeed~') into fieldNameList;
       IF(OLD.agentInvoiceSeed = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.agentInvoiceSeed = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.agentInvoiceSeed = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.agentInvoiceSeed = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
     IF (OLD.fullAudit <> NEW.fullAudit ) THEN
       select CONCAT(fieldNameList,'company.fullAudit~') into fieldNameList;
       IF(OLD.fullAudit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.fullAudit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.fullAudit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.fullAudit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
    IF (OLD.recInvoicePerSO <> NEW.recInvoicePerSO ) THEN
       select CONCAT(fieldNameList,'company.recInvoicePerSO~') into fieldNameList;
       IF(OLD.recInvoicePerSO = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.recInvoicePerSO = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.recInvoicePerSO = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.recInvoicePerSO = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.recInvoicePerSOJob <> NEW.recInvoicePerSOJob ) THEN
       select CONCAT(fieldNameList,'company.recInvoicePerSOJob~') into fieldNameList;
       IF(OLD.recInvoicePerSOJob = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.recInvoicePerSOJob = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.recInvoicePerSOJob = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.recInvoicePerSOJob = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


CALL add_tblHistory (OLD.id,"company", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$

delimiter;

