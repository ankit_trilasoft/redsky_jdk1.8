<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

 <style>				
div#content{padding:0px 0px; min-height:50px; margin-left:0px;}
#mydiv{margin-top:-45px; }
</style>
<style type="text/css">
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
#loader {
position:absolute; z-index:999; margin:0px auto -1px 250px;
}
.upper-case{
text-transform:capitalize;
}
.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style> 
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>  
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" /> 
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.min.js"></script>  	
<script>

//jQuery.noConflict();
//jQuery(document).ready(function() {
  //  jQuery("#ajax_tooltipObj").draggable();
  //  findService();
 // });
//jQuery(document).ready(function() {
 //   jQuery("#ajax_big_tooltipObj").draggable();
 // });
  </script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
window.addEventListener("load", function() {
	findService();
	setVipImage();
	showMmCounselorByJob();
	if(${isNetworkRecord}){
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper';
		
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].setAttribute('readonly','true');
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].setAttribute('readonly','true');
		var el = document.getElementById('serviceOrder.bookingAgentCode.img');
		el.onclick = false;
	    document.images['serviceOrder.bookingAgentCode.img'].src = 'images/navarrow.gif';
	}
	
},false);
    var map;
    var gdir;
    var geocoder = null;
    var addressMarker; 
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }
    function handleErrors(){
	   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
	     alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
	     alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
	   
	   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
	     alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);
	     
	   else if (gdir.getStatus().code == G_GEO_BAD_KEY)
	     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

	   else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
	     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
	    
	   else alert("An unknown error occurred."); 
	} 
	function onGDirectionsLoad() { 
		var distanceInMile=gdir.getDistance().html;
		distanceInMile=distanceInMile.split('&nbsp;');
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=distanceInMile[0].replace(',','');
	if(distanceInMile[1]=='mi'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
		} } 
  </script>
  <script type="text/JavaScript">
function setQuesAns(ans,qus){
	var ansopt=ans.options[ans.options.selectedIndex].value;
	document.removalRelocationServiceForm.quesansId.value=qus+"-"+ansopt; 
} 

</script> 
<script type="text/javascript"> 
function getBigPartnerAlert(code,position) {
	if(code != ''){
		var url="findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
		position = document.getElementById(position);
		ajax_showBigTooltip(url,position);
	} }  
function findCityStateNotPrimary(targetField, position){
if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	 }
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }
  function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     } 
     var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){  handleHttpResponseCityStateFlex(targetElement,targetField);};
     http99.send(null);
     } 
   function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                if(results.length>0){
                     if(results=='Y' ){
                     	findCityState(targetElement,targetField);
                     } } }  } 
 function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }
  function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
       if(targetField=='OZ'){
       var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
        if(document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
          if( document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             } }  }

function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = id;
	 } else if (targetField=='DZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = id;
	} }
 function handleHttpResponseCityState(targetField){
                  if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=res[2]; 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value=res[2];
	           	 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].focus();
	           	} } } else { }
             }
// End Of Method
function zipCode(){
        var originCountryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value; 
		var el = document.getElementById('zipCodeRequiredTrue');
		var el1 = document.getElementById('zipCodeRequiredFalse');
		if(originCountryCode == 'United States'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		}
}
</script>  
<script>
function navigationVal(){
	if (window.addEventListener)
		 window.addEventListener("load", function(){
			 var imgList = document.getElementsByTagName('img');
			 for(var i = 0 ; i < imgList.length ; i++){
				if(imgList[i].src.indexOf('images/nav') > 1){
					imgList[i].style.display = 'none';
				}
			 }
		 }, false)
}
function right(e) {
		if (navigator.appName == 'Netscape' && e.which == 1) {
		return false;
		}
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
			return false;
		}else 
			return true;
		}
 function changeCalOpenarvalue() { 
		  	document.forms['serviceOrderForm'].elements['calOpener'].value='open';
		  }
 function trap1(){
	 if(document.images){
		 var imgList = document.getElementsByTagName('img');
		 for(var i = 0 ; i < imgList.length ; i++){
			if(imgList[i].src.indexOf('images/navarrow.gif') > 0){
				if(imgList[i].id.indexOf('-trigger') > 0){
					imgList[i].removeAttribute("id");
				} }  } 	}
	 try{
			var fieldName = document.forms['serviceOrderForm'].elements['field'].value;
			var fieldName1 = document.forms['serviceOrderForm'].elements['field1'].value;
			var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
			if(fieldName!=''){
			document.forms['serviceOrderForm'].elements[fieldName].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
				animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init();
			}
			if(fieldName1!=''){
			document.forms['serviceOrderForm'].elements[fieldName1].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init()
			}
				
	}catch(e){}
		 } 	  
function trap(){
		  if(document.images)  {
		    	for(i=0;i<document.images.length;i++){
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="/images/open-popup.gif"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="/redsky/images/open-popup.gif"]').attr('src','images/navarrow.gif');		    		
		    		$('#serviceOrderForm').find('img[src="images/navarrows_03.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navarrows_04.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/navarrows_05.png"]').attr('src','images/navarrow.gif');		    		
		    		$('#serviceOrderForm').find('img[src="images/navdisable_03.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navdisable_04.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navdisable_05.png"]').attr('src','images/navarrow.gif');
		    		
		      } 
			} 
		}
   function getOriginCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
} 
function getDestinationCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}
function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}try{	
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].selected=true;
 					}catch (e){}
				}else{
                     
                 } }   }
function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='N';
				 	}try{
				 	document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].select();
				 	}catch (e){}		     	 
 				}else{ 
                 }  }
}   

        
function findCompanyDivisionByBookAg(loadParam) {
    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCheckedBy'].value="";
	var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
    http6.open("GET", url, true);
    http6.onreadystatechange = function() {handleHttpResponse4444(loadParam)};
    http6.send(null);
}
function handleHttpResponse4444(loadParamRes){
		    if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@"); 
                var companyDivision= document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value
                var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value; 
                if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){ 
                if(res.indexOf(companyDivision)!=-1){  
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
                    }
					if(res.length == 2){
						 	document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;												
						 }else{
							 document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[0].selected=true;
						 }
                }
                else{
                	try
                	{
                	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value='${serviceOrder.bookingAgentCode}';
                	}
                	catch(err){}
                }
                }
                else{
                	var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						if(res.length == 2){
							 	document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;												
							 }else{
								 document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[0].selected=true;
							 } } }
			if(loadParamRes!='onload')
			getJobList('unload');
			}	 }   
</script>
<script language="javascript" type="text/javascript">
 var r={
'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\=']/g,
'quotes':/['\''&'\"']/g,
'notnumbers':/[^\d]/g
};
function valid(targetElement,w){
targetElement.value = targetElement.value.replace(r[w],'');
}
</script>
<script language="javascript" type="text/javascript">
		function change() {
		       if( document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "BOAT" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "AUTO" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "HHG/A") {
		       	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = false ;
			  }else{
			  	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = true ;
			  } }
</script>
<SCRIPT LANGUAGE="JavaScript">
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function checkQuotation(){ 
	var bookCode=document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	 var  quotationStatus=document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value ;
	if(bookCode!=''&& quotationStatus=='AC'){
		var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
		http454.open("GET", url, true);
		http454.onreadystatechange = handleHttpResponse454;
		http454.send(null);	
	}
	}
	function handleHttpResponse454(){
		if (http454.readyState == 4) {
	          var results = http454.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){ 
	       			if(res[2] == 'Approved'){
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");
	           			document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';	
	           		}			
	      		}else{
	           		alert("Booking Agent code not valid");
	           		document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';
		   		}
	     
	  }
	} 
	 var http454 = getHTTPObject();
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    var http5 = getHTTPObject5();
    var http6 = getHTTPObject();
   var http9 = getHTTPObject9();
  var http33 = getHTTPObject33();
  var httpHVY = getHTTPObjectHVY();
  var http99 = getHTTPObject();
  var httpChargeDiscount = getHTTPObject();
  function getHTTPObjectHVY() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObject33() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
   function getHTTPObject9() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject5() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http99 = getHTTPObject9();
     function getHTTPObject9() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}  

</script>
<script type="text/javascript">
<sec-auth:authComponent componentId="module.script.form.partnerPortalScript">
navigationVal();
		 trap();
		 $('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');		
		$('#serviceOrder.general').removeAttr('disabled');
		<sec-auth:authScript tableList="serviceOrder,miscellaneous" formNameList="serviceOrderForm,serviceOrderForm" transIdList='${serviceOrder.shipNumber}'>
	    </sec-auth:authScript>
		trap1();

</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.agentScript">
navigationVal();
		 trap();
		 $('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');		
		$('#serviceOrder.general').removeAttr('disabled');
		<sec-auth:authScript tableList="serviceOrder,miscellaneous" formNameList="serviceOrderForm,serviceOrderForm" transIdList='${serviceOrder.shipNumber}'>
	    </sec-auth:authScript>
		trap1();

</sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
navigationVal();
trap();
$('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
$('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');
trap1();
</sec-auth:authComponent>
try{
	 var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	 if(job=='RLO'){		
		checkColumn();
	 }
	 <c:if test="${empty serviceOrder.id}">
  	     fillCommodity();
	 </c:if>
	 if(document.forms['serviceOrderForm'].elements['customerFileJob'].value != ''){
	 	document.forms['serviceOrderForm'].elements['tempJob'].value  =  document.forms['serviceOrderForm'].elements['customerFileJob'].value
	 }else{
		 document.forms['serviceOrderForm'].elements['customerFileJob'].value  =  document.forms['serviceOrderForm'].elements['tempJob'].value
	}
}
catch(e){}
</script> 
<script type="text/javascript"> 
 <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
		animatedcollapse.addDiv('resources', 'fade=0,persist=0,hide=1')
	</configByCorp:fieldVisibility>
</c:if> 
	animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weight', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('volume', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('customerServiceSurvey', 'fade=0,persist=1,hide=1')
	
animatedcollapse.addDiv('address', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('customer', 'fade=0,persist=1,hide=1') 
<c:if test="${quotesToValidate=='QTG'}">
	animatedcollapse.addDiv('estpricing', 'fade=0,persist=1,hide=1')
</c:if>
<c:if test="${redirectPricingButton=='yes'}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
	</c:if>
</c:if>
<c:if test="${redirectPricingButton!='yes'}"> 
<c:if test="${not empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=1,hide=1')
	</c:if>
</c:if>
<c:if test="${empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,hide=1')
	</c:if>
</c:if>
</c:if>
animatedcollapse.init();

<c:if test="${usertype!='PARTNER' && usertype!='AGENT'}">
try{
accountIdCheck();
} 
catch(e){}
<c:if test="${not empty serviceOrder.id}">
<c:if test="${hitFlag=='1'}">
 <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:if>
</c:if>
	//setTimeout("getJobList('onload')", 2000);
	try{
			var fieldName = document.forms['serviceOrderForm'].elements['field'].value;
			var fieldName1 = document.forms['serviceOrderForm'].elements['field1'].value;
			var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
			if(fieldName!=''){
			document.forms['serviceOrderForm'].elements[fieldName].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init();
			}
			if(fieldName1!=''){
			document.forms['serviceOrderForm'].elements[fieldName1].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){ 
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')	
			}
			animatedcollapse.init()
			}
			
	}catch(e){}
</c:if>	
//alert("wat");
try{
    var job = document.getElementsByName('serviceOrder.job')[0].value;
    var hidStatusEstimator1 = document.getElementById('EstimatorService1');
    var labellabelEstimator1 = document.getElementById('labelEstimator');
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var labelserviceVar = document.getElementById('labelservice');
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel');
    var weightserviceVar = document.getElementById('weightVolSection');
    var hidStatusCommoditText1=document.getElementById('hidStatusCommoditText');
    var hidStatusCommoditLabel1=document.getElementById('hidStatusCommoditLabel');
    var hidStatusSalesText11=document.getElementById('hidStatusSalesText');
    var hidStatusSalesLabel11=document.getElementById('hidStatusSalesLabel');
    var hidStatusSalesLabeTextl11=document.getElementById('hidStatusSalesText');
    var registrationHide1=document.getElementById('registrationHide');
    var registrationHide11=document.getElementById('registrationHideLabel');
    var carrierFildLabel1=document.getElementById('carrierFildLabel');
    var carrierFild1=document.getElementById('carrierFild');
   	if(job!=''){
   if(job=='RLO'){
	   hidStatusReg5.style.display = 'block';
	   hidStatusReg4.style.display = 'none';
	   hidStatusEstimator1.style.display = 'none';
	   labellabelEstimator1.style.display= 'none';
	   labelserviceVar.style.display = 'none';
	   modehidStatusReg.style.display = 'none';
	   modelabelserviceVar.style.display = 'none';
	   routinghidStatusReg.style.display = 'none';
	   routinglabelserviceVar.style.display = 'none';
	   packhidStatusReg.style.display = 'none';
	   packlabelserviceVar.style.display = 'none';
	   weightserviceVar.style.display = 'none';
	   hidStatusCommoditText1.style.display = 'none';
	   hidStatusCommoditLabel1.style.display = 'none';
	   hidStatusSalesText11.style.display = 'none';
	   <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.city">
	   hidStatusSalesLabel11.style.display = 'block';
	   hidStatusSalesLabeTextl11.style.display = 'block';
	   </configByCorp:fieldVisibility>
	   <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.notshowcity">
	   hidStatusSalesLabel11.style.display = 'none';
	   </configByCorp:fieldVisibility>
	   registrationHide1.style.display = 'block';
	   registrationHide11.style.display = 'block';
	   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
		   carrierFildLabel1.style.display = 'none';
	     carrierFild1.style.display = 'none';
	  </configByCorp:fieldVisibility>
	   <c:if test="${not empty serviceOrder.id}">
	   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=true;
	   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=true;	   
	   </c:if>
	      }else{
	   	   hidStatusReg4.style.display = 'block';
	   	   hidStatusEstimator1.style.display = 'block';
	   	   labellabelEstimator1.style.display= 'block';
		   hidStatusReg5.style.display = 'none';
		   labelserviceVar.style.display = 'block';
		   modehidStatusReg.style.display = 'block';
		   modelabelserviceVar.style.display = 'block';
		   routinghidStatusReg.style.display = 'block';
		   routinglabelserviceVar.style.display = 'block';
		   packhidStatusReg.style.display = 'block';
		   packlabelserviceVar.style.display = 'block';	
		   weightserviceVar.style.display = 'block';
		   hidStatusCommoditText1.style.display = 'block';
		   hidStatusCommoditLabel1.style.display = 'block';	  
		   hidStatusSalesText11.style.display = 'block';
		   hidStatusSalesLabel11.style.display = 'block'; 
		   registrationHide1.style.display = 'block'; 
		   registrationHide11.style.display = 'block';
		   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
			   carrierFildLabel1.style.display = 'block';
           carrierFild1.style.display = 'block';
           </configByCorp:fieldVisibility>
		   <c:if test="${not empty serviceOrder.id}">
		   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=false;
		   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=false;
		   </c:if>		   
    }
    } 
}catch(e){//alert(e);
}
</script>	
<script type="text/javascript"> 
	setOnSelectBasedMethods(["changeStatus(),hitBilling()"]);
	setCalendarFunctionality(); 
	function setVipImageReset(){
		if('${serviceOrder.vip}'.toString() == 'true' || '${customerFile.vip}'.toString() == 'true')
			document.getElementById('serviceOrder.vip').checked = true;	
		else
			document.getElementById('serviceOrder.vip').checked = false;	
		setVipImage();
	}
	function setVipImage(){ 
		var imgElement = document.getElementById('vipImage');
		if(document.getElementById('serviceOrder.vip').checked == true){
			imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
			imgElement.style.display = 'block';
		}
		else{
			if(('${serviceOrder.vip}' == 'true' && document.getElementById('serviceOrder.vip').checked == true) || ('${customerFile.vip}' == 'true' && document.getElementById('serviceOrder.vip').checked == true)){
				imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
				imgElement.style.display = 'block';
			}
			else
				imgElement.src = '';
				imgElement.style.display = 'none';
		}
	}
	setVipImage();
</script>
<script type="text/javascript">
function showMmCounselorByJob(){
	   var str="0";
	   <configByCorp:fieldVisibility componentId="component.field.Alternative.mmCounselor">
	   str="1";
	   </configByCorp:fieldVisibility>
			var jobValue=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			if((str=="1") && (jobValue=='VIS' || jobValue=='GST' || jobValue=='INT' || jobValue=='STO' || jobValue=='UVL')){
				document.getElementById('mmCounselor').style.display="block";	
				document.getElementById('mmCounselorLabel').style.display="block";	
			}else{
				document.getElementById('mmCounselor').style.display="none";
				document.getElementById('mmCounselorLabel').style.display="none";	
			}				
	}
try{
	showMmCounselorByJob();
	} 
	catch(e){}
setTimeout("setOriginCityCode()",500);
function setOriginCityCode(){
try{
	document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value ="${serviceOrder.originCityCode}";
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value ="${serviceOrder.destinationCityCode}";
}catch(e){}
}
function setFieldValue(id,result){
	document.forms['serviceOrderForm'].elements[id].value = result;
	//document.getElementById(id).value = result;
}
</script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}
</script>
<script type="text/javascript">
try{  	
		document.getElementById('tabindexFlag').focus();
	}catch(e){}
</script>
<script language="javascript" type="text/javascript">
	 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
	 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	 var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
  if((modeType=='Air'|| modeType=='Sea')&& jobType!='RLO'){
   document.getElementById('carrierFild').style.display="block";
   document.getElementById('carrierFildLabel').style.display="block";
  }else{
   document.getElementById('carrierFild').style.display="none";
   document.getElementById('carrierFildLabel').style.display="none";
  }
  </configByCorp:fieldVisibility>
</script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
 
 <jsp:include flush="true" page="serviceOrderFormJS.jsp"></jsp:include>  
 