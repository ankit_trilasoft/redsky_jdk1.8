<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />
</head>



<c:set var="buttons">

</c:set>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="0" align="right"><img width="0" height="20"
				src="<c:url value='/images/head-left.jpg'/>" alt="" /></td>
			<td width="100" class="content-tab"><a href="storages.html">Work
			On Ticket</a></td>
			<td width="0" align="left"><img width="0" height="20"
				src="<c:url value='/images/head-right.jpg'/>" alt="" /></td>

			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="90" class="content-tab"><a href="editStorage.html">Detail</a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="130" class="content-tab"><a href="locations.html">Add
			Item To Location</a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>

			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="160" class="detailActiveTabLabel content-tab"><a
				href="bookStorages.html">Access,Release,Move Items </a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
			<td width="4"></td>

		</tr>
	</tbody>
</table>
<c:out value="${buttons}" escapeXml="false" />

<s:set name="locations" value="locations" scope="request" />
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td id="searchLabelCenter" width="90">
			<div align="center" class="content-head">List</div>
			</td>
			<td width="7"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td></td>
		</tr>
	</tbody>
</table>
<display:table name="locations" class="table" requestURI=""
	id="locationList" export="true" pagesize="25">
	<display:column property="id" sortable="true" href="editLocation.html"
		paramId="id" paramProperty="id" titleKey="location.id" />
	<display:column property="capacity" sortable="true"
		titleKey="location.capacity" />
	<display:column property="cubicFeet" sortable="true"
		titleKey="location.cubicFeet" />
	<display:column property="occupied" sortable="true"
		titleKey="location.occupied" />
	<display:column property="type" sortable="true"
		titleKey="location.type" />
	<display:column property="location" sortable="true"
		titleKey="location.location" />


	<display:setProperty name="paging.banner.item_name" value="location" />
	<display:setProperty name="paging.banner.items_name" value="location" />
	<display:setProperty name="export.excel.filename"
		value="Location List.xls" />
	<display:setProperty name="export.csv.filename"
		value="Location List.csv" />
	<display:setProperty name="export.pdf.filename"
		value="Location List.pdf" />
</display:table>

<c:out value="${buttons}" escapeXml="false" />

<script type="text/javascript"> 
    highlightTableRows("searchlocationacList"); 
</script>
