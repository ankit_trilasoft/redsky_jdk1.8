package com.trilasoft.app.model;


 
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="glcoderategrid")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID") 


public class GlCodeRateGrid extends BaseObject{ 
	private Long id;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;	
	private Date updatedOn;
	private Long costElementId;
	private String gridRecGl;
	private String gridPayGl;
	private String gridJob;
	private String gridRouting;
	private String companyDivision;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("costElementId", costElementId)
				.append("gridRecGl", gridRecGl).append("gridPayGl", gridPayGl)
				.append("gridJob", gridJob).append("gridRouting", gridRouting)
				.append("companyDivision", companyDivision).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof GlCodeRateGrid))
			return false;
		GlCodeRateGrid castOther = (GlCodeRateGrid) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(costElementId, castOther.costElementId)
				.append(gridRecGl, castOther.gridRecGl)
				.append(gridPayGl, castOther.gridPayGl)
				.append(gridJob, castOther.gridJob)
				.append(gridRouting, castOther.gridRouting)
				.append(companyDivision, castOther.companyDivision).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(costElementId).append(gridRecGl)
				.append(gridPayGl).append(gridJob).append(gridRouting)
				.append(companyDivision).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCostElementId() {
		return costElementId;
	}
	public void setCostElementId(Long costElementId) {
		this.costElementId = costElementId;
	}
	@Column(length=20)
	public String getGridRecGl() {
		return gridRecGl;
	}
	public void setGridRecGl(String gridRecGl) {
		this.gridRecGl = gridRecGl;
	}
	@Column(length=20)
	public String getGridPayGl() {
		return gridPayGl;
	}
	public void setGridPayGl(String gridPayGl) {
		this.gridPayGl = gridPayGl;
	}
	@Column(length=3)
	public String getGridJob() {
		return gridJob;
	}
	public void setGridJob(String gridJob) {
		this.gridJob = gridJob;
	}
	@Column(length=5)
	public String getGridRouting() {
		return gridRouting;
	}
	public void setGridRouting(String gridRouting) {
		this.gridRouting = gridRouting;
	}
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
 
}