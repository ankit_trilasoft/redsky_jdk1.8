package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@NamedQueries({
	@NamedQuery(
			name = "findByParentId",
			query = "from PrintDailyPackage p where p.parentId= :parentId"
	)
})

@Entity
@Table(name = "printdailypackage")

public class PrintDailyPackage extends BaseObject {

	private Long id;
	
	private String corpId;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;
	
	private String formName;

	private String formDesc;

	private String jrxmlName;
	
	private String parameters;

	private String wtServiceType;

	private String mode;
	
	private Integer noOfCopies;
	
	private String onePerSOperDay;
	
	private String military;
	
	private Long parentId;
	
	private Integer printSeq;
	
	private String module;
	
	private Boolean status;
	private String job;
	private String billToCode;
	

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("formName", formName)
				.append("formDesc", formDesc)
				.append("jrxmlName", jrxmlName)
				.append("parameters", parameters)
				.append("wtServiceType", wtServiceType)
				.append("mode", mode)
				.append("noOfCopies", noOfCopies)
				.append("onePerSOperDay", onePerSOperDay)
				.append("military", military)
				.append("parentId",parentId)
				
				.append("corpId", corpId)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
		        .append("createdOn",createdOn)
		        .append("printSeq",printSeq)
		        .append("module",module)
		        .append("status",status)
		        .append("job",job)
		        .append("billToCode",billToCode)
		        .toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PrintDailyPackage))
			return false;
		PrintDailyPackage castOther = (PrintDailyPackage) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(formName, castOther.formName)
				.append(formDesc, castOther.formDesc)
				.append(jrxmlName, castOther.jrxmlName)
				.append(parameters, castOther.parameters)
				.append(corpId, castOther.corpId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(wtServiceType, castOther.wtServiceType)
				.append(mode, castOther.mode)
				.append(noOfCopies, castOther.noOfCopies)
				.append(onePerSOperDay, castOther.onePerSOperDay)
				.append(military, castOther.military)
				.append(parentId,castOther.parentId)
				.append(printSeq, castOther.printSeq)
				.append(module,castOther.module)
				.append(status, castOther.status)
				.append(job, castOther.job)
				.append(billToCode, castOther.billToCode)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(formName)
				.append(formDesc).append(jrxmlName).append(corpId)
				.append(updatedBy)
				.append(createdBy)
				.append(createdOn).append(updatedOn).append(parameters).append(wtServiceType)
				.append(mode).append(noOfCopies).append(onePerSOperDay).append(military).append(parentId).append(printSeq)
				.append(module).append(status).append(job).append(billToCode)
				.toHashCode();
	}

	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}
	
	@Column
	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}
	
	@Column
	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}

	@Column
	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	@Column
	public String getWtServiceType() {
		return wtServiceType;
	}

	public void setWtServiceType(String wtServiceType) {
		this.wtServiceType = wtServiceType;
	}

	@Column
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Column
	public Integer getNoOfCopies() {
		return noOfCopies;
	}

	public void setNoOfCopies(Integer noOfCopies) {
		this.noOfCopies = noOfCopies;
	}

	@Column
	public String getOnePerSOperDay() {
		return onePerSOperDay;
	}

	public void setOnePerSOperDay(String onePerSOperDay) {
		this.onePerSOperDay = onePerSOperDay;
	}

	@Column
	public String getMilitary() {
		return military;
	}

	public void setMilitary(String military) {
		this.military = military;
	}
	
	@Column
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Column
	public Integer getPrintSeq() {
		return printSeq;
	}

	public void setPrintSeq(Integer printSeq) {
		this.printSeq = printSeq;
	}

	@Column
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@Column
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}	

}
