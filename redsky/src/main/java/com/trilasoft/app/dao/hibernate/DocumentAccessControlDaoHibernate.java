package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DocumentAccessControlDao;
import com.trilasoft.app.model.DocumentAccessControl;

public class DocumentAccessControlDaoHibernate extends GenericDaoHibernate<DocumentAccessControl, Long> implements DocumentAccessControlDao {

	public DocumentAccessControlDaoHibernate() {
		super(DocumentAccessControl.class);
	}

	public List isExisted(String fileType, String sessionCorpID) {
		return getHibernateTemplate().find("from DocumentAccessControl where fileType='"+fileType +"' and corpID='"+sessionCorpID+"'");
	}
}
