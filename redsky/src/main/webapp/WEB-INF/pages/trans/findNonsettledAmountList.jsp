<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findAcctRefNumList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findAcctRefNumList.heading'/>"/> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>Driver&nbsp;Info</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>

<s:form id="findAcctRefNumList" action="findAcctRefNumList" method="post">  
<s:set name="getNonsettledAmountList" value="getNonsettledAmountList" scope="request"/>
<display:table name="getNonsettledAmountList" class="table" requestURI="" id="getNonsettledAmountList" style="width:200px" defaultsort="1" pagesize="100" >   
<display:column  headerClass="headerColorInv" title="Description" style="width:190px" >
${getNonsettledAmountList.description }
</display:column>
<display:column  headerClass="headerColorInv" title="Amount" style="width:20px" >
${getNonsettledAmountList.amount}
</display:column>
</display:table>
</s:form>
		  	