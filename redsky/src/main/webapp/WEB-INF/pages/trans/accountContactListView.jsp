<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="accountContactList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountContactList.heading'/>"/>  
    
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
} 
</style>
</head>
<s:form id="accountContactListForm" action="searchAccountContacts" method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<div id="Layer1" style="width:95%;">
<div id="newmnav">
				<ul>
				 <c:if test="${empty param.popup}" >
				    <li><a href="viewPartner.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
				    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Contact<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		<li><a href="partnerView.html"><span>Partner List</span></a></li>
				</c:if>
			  	</ul>
		</div>
		<div class="spn">&nbsp;</div>

<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" 
        onclick="location.href='<c:url value="/editNewAccountContact.html?pID=${partner.id}&partnerType=${partnerType}&fromPage=View"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set> 

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:4px;"><span></span></div>
   <div class="center-content">
				<table class="detailTabLabel" cellspacing="1" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="10px" ><s:label label="  "/></td>
							<td class="listwhitetext" >Account&nbsp;Name</td>
							<td><s:hidden name="partner.id" />
							    <s:textfield cssClass="input-textUpper" name="partner.lastName" required="true" size="35" readonly="true"/>
							</td>
							<td align="right" class="listwhitetext" width="50px" ><fmt:message key='partner.partnerCode' /></td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
							<td align="right" class="listwhitetext" width="50px" >Status</td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
						</tr>
						<tr><td height="20px"></td></tr>
					</tbody>
				</table>			
		</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>	
</div>
<s:set name="accountContacts" value="accountContacts" scope="request"/>  
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>List</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="Layer5" style="width:95%;">
<display:table name="accountContacts" class="table" requestURI="" id="accountContactList" export="true" defaultsort="1" pagesize="10" >   
	 <c:if test="${empty param.popup}" >
	<display:column property="jobType" sortable="true" titleKey="accountContact.jobType"
		href="editAccountContact.html?partnerType=${partnerType}&fromPage=View&pID=${partner.id}" paramId="id" paramProperty="id" />   
	</c:if>
	
	<c:if test="${param.popup}" >
	<display:column property="jobType" sortable="true" titleKey="accountContact.jobType"
		href="editAccountContact.html?partnerType=${partnerType}&pID=${partner.id}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" paramId="id" paramProperty="id" />   
	</c:if>
	<display:column property="jobTypeOwner" sortable="true" titleKey="accountContact.jobTypeOwner" />
	<display:column property="contractType" sortable="true" titleKey="accountContact.contractType" />
	<display:column sortable="true" titleKey="accountContact.contactName" >
		<c:out value="${accountContactList.contactName} ${accountContactList.contactLastName}" />
	</display:column>
	<display:column property="department" sortable="true" titleKey="accountContact.department" />
	<display:column headerClass="containeralign" style="text-align: right" property="primaryPhone" sortable="true" titleKey="accountContact.primaryPhone" />
	<c:if test="${accountContactList.status=='true'}">
	<display:column title="Active"> <img src="${pageContext.request.contextPath}/images/tick01.gif" /> </display:column>
	</c:if>
	<c:if test="${accountContactList.status!='true'}">
	<display:column title="Active"> <img src="${pageContext.request.contextPath}/images/cancel001.gif" /> </display:column>
	</c:if>
    <display:setProperty name="paging.banner.item_name" value="accountContact"/>   
    <display:setProperty name="paging.banner.items_name" value="accountContacts"/>   
  
    <display:setProperty name="export.excel.filename" value="AccountContacts List.xls"/>   
    <display:setProperty name="export.csv.filename" value="AccountContacts List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="AccountContacts List.pdf"/>   
</display:table>  
 </div>
<c:if test="${empty param.popup}" >
	<c:out value="${buttons}" escapeXml="false" />  
</c:if>
</div>
</s:form>
<script type="text/javascript">   
   highlightTableRows("accountContactList");        
</script>