// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`itemdata` ADD COLUMN `volumeReceived` DOUBLE ,
   ADD COLUMN `weightReceived` DOUBLE ;
   
   ALTER TABLE `redsky`.`company` ADD COLUMN `fullAudit` bit(1) DEFAULT b'0' ;
   
   ALTER TABLE `redsky`.`carton` MODIFY COLUMN `description` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
   
   ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `frequentlyUsedResources` bit(1) DEFAULT b'0';
   
   ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `displayPriority` INTEGER DEFAULT 0 ;
   
   ALTER TABLE `redsky`.`companydivision` ADD COLUMN `inactiveEffectiveDate` DATETIME;
   
   ALTER TABLE `redsky`.`carton` MODIFY COLUMN `description` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
   
   ALTER TABLE `redsky`.`vehicle` ADD COLUMN `valuationCurrency` VARCHAR(100) NULL  AFTER `engineNumber` , ADD COLUMN `type` VARCHAR(45) NULL  AFTER `valuationCurrency` ;