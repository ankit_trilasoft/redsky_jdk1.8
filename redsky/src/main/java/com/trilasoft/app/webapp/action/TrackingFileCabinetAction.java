package com.trilasoft.app.webapp.action;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException; 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List; 

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse; 

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder; 
import org.apache.struts2.interceptor.validation.SkipValidation; 
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction; 

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company; 
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MyFileManager;

public class TrackingFileCabinetAction  extends BaseAction implements Preparable{
	private Long sid;
	private String sessionCorpID;
	private String usertype;
	private Company company; 
	private User user; 
    private List corpIDList;  
    private Date openfromdate;
	private Date opentodate;
	private String companyCorpID;
	private CompanyManager companyManager;
    private ErrorLogManager errorLogManager;
    private MyFileManager myFileManager;
    
    
	public TrackingFileCabinetAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.usertype = user.getUserType();
	}
	
	public void prepare() throws Exception {
		company = companyManager.findByCorpID(sessionCorpID).get(0);
		corpIDList =companyManager.getcorpID();
	}
	
  	@SkipValidation
	public String trackingFile() {
		return SUCCESS;
	}

	@SkipValidation
	public void search()  throws IOException {
	   	StringBuilder output = new StringBuilder(); 
	   	String openFrom = "";				
		if (openfromdate!= null) {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			openFrom = new String(dateformatYYYYMMDD.format(openfromdate));

		}
		String closeFrom = "";
		if (opentodate != null) {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			closeFrom = new String(dateformatYYYYMMDD.format(opentodate));

		}
		List locationList = myFileManager.findFileLocation(openFrom, closeFrom,companyCorpID);
		File file1 = new File("NonExistsFile");
		String header=new String();
		HttpServletResponse response = getResponse();
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		Iterator iterator = locationList.iterator();
		header = " Location" + "\t" +" FileID" + "\t" +"Description "+ "\n";
		outputStream.write(header.getBytes());
		while(iterator.hasNext()){
			Object[] row = (Object[]) iterator.next();
			
			if (row[0] != null) {
				String location=row[0].toString();
				
				location = location.replace("\\","/");
				File  f = new File(location);
	
				if (f.exists()) {
					System.out.print("File Found"+f.getName());	
			
				}
				else { 
					outputStream.write((location + "\t").getBytes()); 
				
				if (row[1] != null) { 
					outputStream.write((row[1].toString() + "\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if (row[2] != null) { 
					outputStream.write((row[2].toString() + "\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				outputStream.write("\n".getBytes());
				}
				 
			}
				
				
			   
		}
		
	 
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	} 
	public Company getCompany() {
		return company;
	} 
	public void setCompany(Company company) {
		this.company = company;
	} 
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	} 
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	} 
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	} 
	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public List getCorpIDList() {
		return corpIDList;
	}

	public void setCorpIDList(List corpIDList) {
		this.corpIDList = corpIDList;
	}

	public Date getOpenfromdate() {
		return openfromdate;
	}

	public void setOpenfromdate(Date openfromdate) {
		this.openfromdate = openfromdate;
	}

	public Date getOpentodate() {
		return opentodate;
	}

	public void setOpentodate(Date opentodate) {
		this.opentodate = opentodate;
	}

	public String getCompanyCorpID() {
		return companyCorpID;
	}

	public void setCompanyCorpID(String companyCorpID) {
		this.companyCorpID = companyCorpID;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}



}
