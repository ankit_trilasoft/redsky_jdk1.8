<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Quote Details Screen</title>
<meta name="heading" content="Quote Details Screen" />
<script language="javascript" type="text/javascript">
function clear_fields()
{  		    	
	document.forms['quoteDetailsScreen'].elements['firstName'].value = "";
	document.forms['quoteDetailsScreen'].elements['lastName'].value ="";
	document.forms['quoteDetailsScreen'].elements['createdOn'].value = "";
	document.forms['quoteDetailsScreen'].elements['originCountry'].value ="";
	document.forms['quoteDetailsScreen'].elements['originCity'].value = "";
	document.forms['quoteDetailsScreen'].elements['destinationCountry'].value = "";
	document.forms['quoteDetailsScreen'].elements['destinationCity'].value ="";
}
function confirmSubmit(targetElement){
    
	var agree=confirm("Are you sure you want to remove this record?");
	var did = targetElement;
	if (agree){
		 location.href="deleteQuoteDetails.html?id="+did;
	}else{
		return false;
	}
}
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
margin-top:-16px;
!margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<c:set var="quotedetailsbuttons"/>
<s:form id="quoteDetailsScreen" name="quoteDetailsScreen" action="quoteDetailSearch.html" method='post'>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:100%">
	 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
 <div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="100%">
		<thead>
			<tr>
			    <th>First Name</th>
				<th>Last name</th>
				<th>Created On</th>
				<th>Origin Country</th>
				<th>Origin City</th>
				<th>Destination Country</th>
			    <th>Destination City</th>	
			</tr>
		</thead>
		<tbody>
			<tr>
			  <td align="left">
			     <s:textfield name="firstName" id="firstName" cssClass="input-text" size="12" tabindex="1"/>
			 </td> 	
			 <td align="left">
			     <s:textfield name="lastName" id="lastName" cssClass="input-text" size="12" tabindex="2"/>
			 </td>
			 <td align="left">
			  <s:textfield cssClass="input-text" id="createdOn" name="createdOn" onkeydown="return onlyDel(event,this)" size="7" maxlength="11" readonly="false" tabindex="3"/>
			  <img id="createdOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			 </td>
			 <td align="left">
			  <s:select name="originCountry" id="originCountry" cssClass="list-menu" cssStyle="width:162px" list="%{ocountry}" tabindex="4"/>
			 </td> 	
			  <td align="left">
			     <s:textfield name="originCity" id="originCity" cssClass="input-text" size="12" tabindex="5"/>
			 </td> 	
			  <td align="left">
			   <s:select name="destinationCountry" id="destinationCountry" cssClass="list-menu" cssStyle="width:162px" list="%{dcountry}" headerKey=""  headerValue="" tabindex="6"/>
			 </td> 	
			  <td align="left">
			     <s:textfield name="destinationCity" id="destinationCity" cssClass="input-text" size="12" tabindex="8"/>
			 </td> 	
		  </tr>
			<tr>
				<td colspan="5"></td>
				<td style="border-left: hidden; text-align:right;" colspan="2">
				<s:submit cssClass="cssbutton" method="quoteDetailSearch" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${quotedetailsbuttons}" escapeXml="false" />

	<div id="Layer5" style="width:100%;">
	<div id="otabs" style="margin-top: -10px;" >
		  <ul>
		    <li><a class="current"><span>Quote Details Screen</span></a></li>
		  </ul>
		</div>
	
	<div class="spnblk">&nbsp;</div>
	<s:set name="quoteDetailslist" value="quoteDetailsList" scope="request" />
	<display:table name="quoteDetailslist" class="table" requestURI="" id="quoteDetailsList" export="true" pagesize="10" style="margin-top:12px;!margin-top: -1px;width:100%;">
	<display:column property="quoteId" sortable="true" title="Quote Id" href="editQuoteDetails.html" paramId="quoteId" paramProperty="quoteId" />
	<display:column property="firstName" sortable="true" title="First Name" maxLength="15" />
	<display:column property="lastName" sortable="true" title="Last Name" maxLength="15" />
	<display:column property="createdOn" sortable="true" title="Created On" maxLength="15"  format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="originCountry" sortable="true" title="Origin Country" maxLength="15" />
	<display:column property="originCity" sortable="true" title="Origin City" maxLength="15" />
	<display:column property="destinationCountry" sortable="true" title="Destination Country" maxLength="15" />
	<display:column property="destinationCity" sortable="true" title="Destination City" maxLength="30" />
	
	
	<display:setProperty name="export.excel.filename" value="QuoteDetailsList.xls"/>
	<display:setProperty name="export.csv.filename" value="QuoteDetailsList.csv"/>
	</display:table>
	</div>
	</div>
</s:form>
<script type="text/javascript"> 
    highlightTableRows("quoteDetailsList"); 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>