<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%-- <%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_ADMIN")||userRole.equalsIgnoreCase("ROLE_STO_LOCATION")){
		userRole=role.getName();
		break;
	}
	
}
%> --%>
  
<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />
<script language="javascript" type="text/javascript">

function clear_fields()
{  		    	
		document.forms['locationacList'].elements['location.type'].value = "";
		document.forms['locationacList'].elements['location.warehouse'].value = "";
		   document.forms['locationacList'].elements['location.locationId'].value ="";
		   document.forms['locationacList'].elements['location.capacity'].value ="";
		   document.forms['locationacList'].elements['occupieType'].value = "";

}

function confirmSubmit(targetElement,targetElement1){
	var agree = false;
   if(targetElement1 == 'X'){
	   agree=confirm("Are you sure you want to remove this location? Any occupied location will not be deleted");
   }else{
	    agree=confirm("Are you sure you want to remove this location?");;
   }
	var did = targetElement;
	var occ=targetElement1;
	if (agree){
	    if(occ ==''){
		 location.href="locationdelete.html?from=main&id="+did;
		 
		}
	}else{
		return false;
	}
}
</script>

<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:3px;!margin-bottom:2px;margin-top:-17px;!margin-top:-15px;padding:2px 0px;text-align:right;width:100%;}
</style>
</head>
<c:set var="locationacbuttons"/>
<s:form cssClass="form_magn" id="locationacList" name="locationacList" action="locationacSearch" method="post">
<div id="Layer1" style="width:100%">	
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="98%">
		<thead>
			<tr>
			    <th><fmt:message key="location.warehouse" /></th>
			    <th><fmt:message key="location.locationId" /></th>
				<th><fmt:message key="location.type" /></th>
				<th><fmt:message key="location.capacity" /></th>
				<th><fmt:message key="location.occupied" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:select name="location.warehouse" cssClass="list-menu" cssStyle="width:180px" list="%{house}" headerKey=""  headerValue=""/></td>
			    <td><s:textfield name="location.locationId" required="true" cssStyle="width:180px" cssClass="input-text" /></td>
				<td><s:select name="location.type" cssClass="list-menu" cssStyle="width:180px" list="%{locTYPE}" headerKey=""  headerValue=""/></td>
				<td>
				<s:select cssClass="list-menu" name="location.capacity" cssStyle="width:180px" list="%{capacity}" headerKey=""  headerValue=""/>				
				</td>
				<td><s:select name="occupieType" list="{'All','Occupied','Empty'}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:180px"/></td>	
				</tr>
				<tr>
				<td colspan="4"></td>
				<td style="border-left: hidden;"><s:submit cssClass="cssbutton" method="search" cssStyle="width:60px; height:25px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${locationacbuttons}" escapeXml="false" />

	<div id="Layer5" style="width:100%;">
	<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Location List</span></a></li>
		    <li><a href="uploadXlsFilePage.html"><span>Location Maintenance</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
	<s:set name="locations" value="locations" scope="request" />
	<display:table name="locations" class="table" requestURI="" id="locationlibList" export="true" pagesize="50" style="margin-top: 1px;">
	<display:column property="locationId" sortable="true" titleKey="location.locationId"  href="editLocationac.html?from=main" paramId="id" paramProperty="id" style="width:80px"/>
	<display:column property="warehouse" sortable="true" titleKey="location.warehouse" style="width:50px"/>
	<display:column property="description" sortable="true" titleKey="location.type" style="width:50px"/>
	<display:column headerClass="containeralign"  property="capacity" sortable="true"	title="Holding Capacity" style="width:50px; text-align: right"/>
	<c:if test="${sessionCorpID=='SSCW' || sessionCorpID=='STAR'}">
	<display:column headerClass="containeralign"  sortable="true" title="Capacity Cft" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.cubicFeet}" /></div></display:column>
    </c:if>
	<display:column headerClass="containeralign" sortable="true" title="Capacity Cbm" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.cubicMeter}" /></div></display:column>
	<c:if test="${sessionCorpID=='SSCW' || sessionCorpID=='STAR'}">
	<display:column headerClass="containeralign"  sortable="true" title="Utilized Cft" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.utilizedVolCft}" /></div></display:column>
     </c:if>
	<display:column headerClass="containeralign" sortable="true" title="Utilized Cbm" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.utilizedVolCbm}" /></div></display:column>
		
	<c:if test="${locationlibList.occupied =='X' }">
		<display:column  titleKey="location.occupied" style="width:7%;"  sortable="true">
			<img src="${pageContext.request.contextPath}/images/tick01.gif"/>&nbsp;
			<img align="top" onclick="workTicketByLocation('${locationlibList.locationId}',this);"  src="${pageContext.request.contextPath}/images/plus-small.png"/>
		</display:column> 
	</c:if>
	<c:if test="${locationlibList.occupied !='X' }">
			<display:column  titleKey="location.occupied" style="width:7%;" sortable="true">&nbsp;&nbsp;</display:column> 
	</c:if>
		<sec-auth:authComponent componentId="module.locationLibraryList.delete">
		<display:column title="Remove" style="text-align:center; width:5%;">
				<a><img align="middle" onclick="confirmSubmit(${locationlibList.id},'${locationlibList.occupied}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
		</sec-auth:authComponent>
		
	<%-- <%if((userRole.indexOf("ROLE_ADMIN")>-1)||(userRole.indexOf("ROLE_STO_LOCATION")>-1)){ %>
		<display:column title="Remove" style="text-align:center; width:20px;">
				<a><img align="middle" onclick="confirmSubmit(${locationlibList.id},'${locationlibList.occupied}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
	<%}%> --%>
	<display:setProperty name="export.excel.filename" value="Location List.xls"/>
	<display:setProperty name="export.csv.filename" value="Location List.csv"/>
	</display:table>
	<c:set var="buttons"/>
	<input type="button" class="cssbuttonA" value="Add"  style="width:60px; height:25px" onclick="location.href='<c:url value="/saveLocationForm.html"/>'" /></td>
	  
	<c:out value="${buttons}" escapeXml="false" />
	</div>
	</div>
</s:form>
<script type="text/javascript"> 
    highlightTableRows("locationacList"); 
    function workTicketByLocation(locationId,position){
    	var url="workTicketByLocation.html?locationId="+locationId+"&decorator=simple&popup=true";
    	ajax_SoTooltip(url,position);	
    }
</script>
