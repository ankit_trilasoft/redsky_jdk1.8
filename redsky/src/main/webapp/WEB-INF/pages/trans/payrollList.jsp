<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title><fmt:message key="payrollList.title"/></title>   
    <meta name="heading" content="<fmt:message key='payrollList.heading'/>"/>   
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
	for(i=0;i<=3;i++){
		document.forms['payrollList'].elements[i].value = "";
		document.forms['payrollList'].elements['payroll.active'].checked = false;		
		document.forms['payrollList'].elements['union'].checked = false;
	}
}

</script>
<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:1px;
	margin-top:-18px;
	!margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head>   


<s:form id="payrollList" name="payrollList" action="searchPayroll"  validate="true">   
<div id="Layer1" style="width:100%">  

<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div> 
<div class="spnblk">&nbsp;</div>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editPayroll.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set> 

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="payroll.userName"/></th>
<th><fmt:message key="payroll.firstName"/></th>
<th><fmt:message key="payroll.lastName"/></th>
<th><fmt:message key="payroll.war"/></th>
<th><fmt:message key="payroll.active"/></th>
<th>Union</th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left"><s:textfield name="payroll.userName" required="true" cssClass="input-text" size="20" cssStyle="width:150px;"/></td>
		    <td width="" align="left"><s:textfield name="payroll.firstName" required="true" cssClass="input-text" size="20" cssStyle="width:150px;"/></td>
			<td width="" align="left"><s:textfield name="payroll.lastName" required="true" cssClass="input-text" size="20" cssStyle="width:150px;"/></td>
			<td width="" align="left" ><s:select name="payroll.warehouse" list="%{house}" cssClass="list-menu" cssStyle="width:150px;" headerKey="" headerValue=""/></td>
			<td align="right" width=""><s:checkbox name="payroll.active" fieldValue="true"/></td>
			<td align="right" width=""><s:checkbox name="union" fieldValue="true"/></td>		  
			<td align="center"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />  

		<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Personnel List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

 
<display:table name="payrolllist" class="table" requestURI="" id="PayrollssList" export="true" defaultsort="2" pagesize="10" style="width:100%;margin-top: 1px;" >   
   <display:column property="userName" href="payrollForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="payroll.userName" maxLength="20"/>
   <display:column property="firstName" sortable="true" titleKey="payroll.firstName" maxLength="20"/>
   <display:column property="lastName"  sortable="true" titleKey="payroll.lastName" maxLength="20"/>
   <display:column property="warehouse" sortable="true" titleKey="payroll.warehouse" maxLength="20"/>
   <display:column property="typeofWork" sortable="true" titleKey="payroll.typeofWork" maxLength="20"/>
   <display:column property="unionName" sortable="true" title="Union Date" format="{0,date,dd-MMM-yyyy}" style="width:70px"/>
   <display:column property="payhour" headerClass="containeralign" style="text-align: right" sortable="true" titleKey="payroll.payhour" maxLength="20"/>

    <c:if test="${PayrollssList.active=='true' }">
   <display:column titleKey="payroll.active" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
   </c:if>
   <c:if test="${PayrollssList.active!='true' }">
   <display:column titleKey="payroll.active"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
   </c:if>
    
   
    <display:setProperty name="paging.banner.item_name" value="payroll"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="Payroll List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Payroll List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Payroll List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>

</div>
</s:form>   
<script type="text/javascript"> 
try{
<c:if test="${chekcflag=='1'}">
   document.forms['payrollList'].elements['payroll.active'].checked = true;
</c:if>
}
catch(e){}
</script> 


 
