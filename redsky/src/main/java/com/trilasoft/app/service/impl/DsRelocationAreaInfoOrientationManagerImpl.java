package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DsRelocationAreaInfoOrientationDao;
import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;
import com.trilasoft.app.service.DsRelocationAreaInfoOrientationManager;



public class DsRelocationAreaInfoOrientationManagerImpl extends GenericManagerImpl<DsRelocationAreaInfoOrientation, Long> implements DsRelocationAreaInfoOrientationManager{
	DsRelocationAreaInfoOrientationDao dsRelocationAreaInfoOrientationDao;
	public DsRelocationAreaInfoOrientationManagerImpl(DsRelocationAreaInfoOrientationDao dsRelocationAreaInfoOrientationDao){
		super(dsRelocationAreaInfoOrientationDao);
		this.dsRelocationAreaInfoOrientationDao=dsRelocationAreaInfoOrientationDao;
	}
	public List getListById(Long id) {
		return dsRelocationAreaInfoOrientationDao.getListById(id);
	}
}
