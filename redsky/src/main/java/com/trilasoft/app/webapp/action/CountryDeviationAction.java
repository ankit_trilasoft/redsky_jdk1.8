package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CountryDeviationManager;
import com.trilasoft.app.service.RefMasterManager;

public class CountryDeviationAction extends BaseAction{

	private Long id;
	private String corpID;
	private CountryDeviationManager countryDeviationManager;
	private List countryDeviationList;
	private Charges charges;
	private ChargesManager chargesManager;
	private ContractManager contractManager;
	private String charge;
	private String contract;
	private Map<String,String> country;
	private RefMasterManager refMasterManager;
	private String checkData; 
	private CountryDeviation countryDeviation;
	private String delDeviationId;
	private String btn;
	private String deviationType;
	private Contract contracts;
	private String deviationCorpId;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CountryDeviationAction.class);

	
	public CountryDeviationAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.corpID = user.getCorpID();
	}
	
	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		country = refMasterManager.findByParameter(corpID, "COUNTRY");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;  
	}

	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(corpID);
		countryDeviationList=countryDeviationManager.findDeviation(charge,deviationType);
		charges=chargesManager.get(Long.parseLong(charge));
		List tempIdList = contractManager.countRow(charges.getContract(),corpID);
    	Long tempId = Long.parseLong(tempIdList.get(0).toString());
    	contracts = contractManager.get(tempId);
    	deviationCorpId = corpID;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	
@SkipValidation
	public String saveDeviation(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	Long maxId=null;
	if(!checkData.equalsIgnoreCase("")){
			String[] str = checkData.split(",");
					for(int i=0;i<str.length;i++){ 
				     String	idValue=str[i];
				     String countryCode="";
				     BigDecimal deviation= new BigDecimal(0.00);
						if(!((idValue.equals("new")))){
							countryDeviation=countryDeviationManager.get(Long.parseLong(idValue));
							countryCode=getRequest().getParameter("countryCode"+idValue); 
							countryDeviation.setCountryCode(countryCode); 
							deviation=new BigDecimal(getRequest().getParameter("deviation"+idValue));
							countryDeviation.setDeviation(deviation); 
							if(btn.equalsIgnoreCase("sell")){
								countryDeviation.setPurchase("N");
							}
							if(btn.equalsIgnoreCase("buy")){
								countryDeviation.setPurchase("Y");
							}
							countryDeviation.setUpdatedOn(new Date());
							countryDeviation.setUpdatedBy(getRequest().getRemoteUser());
							countryDeviationManager.save(countryDeviation);
						}
						else{
							countryDeviation=new CountryDeviation();
							countryDeviation.setCharge(charge);
							countryDeviation.setCorpID(corpID);
							countryDeviation.setContractName(contract);
							countryDeviation.setCreatedOn(new Date());
							countryDeviation.setCreatedBy(getRequest().getRemoteUser());
							countryDeviation.setUpdatedOn(new Date());
							countryDeviation.setUpdatedBy(getRequest().getRemoteUser()); 
							countryCode=getRequest().getParameter("newCountryCode"); 
							countryDeviation.setCountryCode(countryCode); 
							deviation=new BigDecimal(getRequest().getParameter("newDeviation")); 
							countryDeviation.setDeviation(deviation); 
							if(btn.equalsIgnoreCase("sell")){
								countryDeviation.setPurchase("N");
							}
							if(btn.equalsIgnoreCase("buy")){
								countryDeviation.setPurchase("Y");
							}
							countryDeviationManager.save(countryDeviation);
						}	
			}
	} 
		getComboList(corpID);
		countryDeviationList=countryDeviationManager.findDeviation(charge,deviationType);
		charges=chargesManager.get(Long.parseLong(charge));
		List tempIdList = contractManager.countRow(charges.getContract(),corpID);
    	Long tempId = Long.parseLong(tempIdList.get(0).toString());
    	deviationCorpId = corpID;
    	contracts = contractManager.get(tempId);
		checkData=""; 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;

	}
		
		
@SkipValidation
	public String addRowToDeviation(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		saveDeviation();
		getComboList(corpID);
		countryDeviationList=countryDeviationManager.findDeviation(charge,deviationType);
		charges=chargesManager.get(Long.parseLong(charge));
    	countryDeviation=new CountryDeviation();
    	countryDeviationList.add(countryDeviation); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
@SkipValidation
public String delDeviation(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	countryDeviationManager.delSelectedDevitaion(delDeviationId);
	getComboList(corpID);
	countryDeviationList=countryDeviationManager.findDeviation(charge,deviationType);
	charges=chargesManager.get(Long.parseLong(charge));
	List tempIdList = contractManager.countRow(charges.getContract(),corpID);
	Long tempId = Long.parseLong(tempIdList.get(0).toString());
	deviationCorpId = corpID;
	contracts = contractManager.get(tempId);
	checkData="";
	delDeviationId="";
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public void setCountryDeviationManager(CountryDeviationManager countryDeviationManager) {
		this.countryDeviationManager = countryDeviationManager;
	}
	public List getCountryDeviationList() {
		return countryDeviationList;
	}

	public void setCountryDeviationList(List countryDeviationList) {
		this.countryDeviationList = countryDeviationList;
	}
	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}
	public String getCharge() {
		return charge;
	}


	public void setCharge(String charge) {
		this.charge = charge;
	}
	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}
	
	public CountryDeviation getCountryDeviation() {
		return countryDeviation;
	}

	public void setCountryDeviation(CountryDeviation countryDeviation) {
		this.countryDeviation = countryDeviation;
	}
	public String getDelDeviationId() {
		return delDeviationId;
	}

	public void setDelDeviationId(String delDeviationId) {
		this.delDeviationId = delDeviationId;
	}
	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}
	public String getCheckData() {
		return checkData;
	}

	public void setCheckData(String checkData) {
		this.checkData = checkData;
	}

	public String getDeviationType() {
		return deviationType;
	}

	public void setDeviationType(String deviationType) {
		this.deviationType = deviationType;
	}

	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}

	public String getDeviationCorpId() {
		return deviationCorpId;
	}

	public void setDeviationCorpId(String deviationCorpId) {
		this.deviationCorpId = deviationCorpId;
	}

	public Contract getContracts() {
		return contracts;
	}

	public void setContracts(Contract contracts) {
		this.contracts = contracts;
	}

	

}
