<%@ include file="/common/taglibs.jsp"%><head>
    <title><fmt:message key="login.title"/></title>
    <meta name="heading" content="<fmt:message key='login.heading'/>"/>
    <meta name="menu" content="Login"/>    
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />   

<style>
 #privacy {	
	width:868px; 
	margin:0px auto;
	background-color:#FFF;
	height:90px;
	}
#privacyText {
	float: left;
    font: 12px/15px arial,verdana;
    margin-top: 33px;
    padding-left: 45px;
    text-align: left;
    width: 640px;	
/*	!width:672px; */
	}
	
#privacyText a { text-decoration: underline; color: #0428db}	
	
#privacyLogo {	
	width:162px; 	
	text-align:left;
	border:0px;
	padding-top:2px;
	float:right;
	}
   
#leftcolumn{
margin-top:40px !important;
}    
    
div#branding { 
  float: left;
  width: 100%;
  margin: 0;
  text-align: left;
  height:0px;
} 
  
#wrapper {
margin:0 auto;
width:950px;
}       
div#footer div#divider div {
 border-top:5px solid #EBF5FC;
 margin:1px 0 0;
 }
body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(../images/indexbg-final.gif); background-repeat:repeat-x;}
#logincontainer{ width:915px; height:376px;!height:370px; background-image:url(../images/loginbg-account.gif); background-repeat:no-repeat; margin:0px auto; margin-top:10px;}
#errormessage {width:300px; list-style:none;margin:0px auto;}

img.validationWarning, div.error img.icon, div.message img.icon, li.error img.icon {
 background:transparent none repeat scroll 0 0 !important;
 border:0 none !important;
 margin-left:3px;
 padding-bottom:1px;
 vertical-align:middle;
 width:14px;
 height:14px;
 }
 
 #wrapper{width:950px; margin:0px auto -1px auto;}

.clear{ clear:both;}



#indexlogo {
	width:800px;
	float:left;
	text-align:right;
	margin-right:0px;
	margin-top:10px;
	padding-top:0px;
	padding-right:40px;
	padding-bottom:0px;
	padding-left:0px;
	position:relative;	

}

#leftcolumn {
	width:480px;
	float:left;
	top: 150px;
	margin-top:60px;

}
#rightcolumn { width:400px; float:right; margin-top:20px;}

#indexfooter { width:925px; }

.textbox{
	background-color:#e7e7e7;
	border: 1px solid #b2b2b2;
	font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#616161;
	width:150px;
}

.btn{ cursor:auto;}

.content{ font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#444444;}
#content {padding-top:0px !important;}
.contentLabel{ color: #222222;
    font-family: Arial;
    font-size: 13px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 25px;
	!line-height: 15px;
    word-wrap: normal;}

.footertext{ font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#616161;}

a.otherlinks:link  {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#444444; text-decoration:none; }

a.otherlinks:hover  {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#444444; text-decoration:underline; }

a.otherlinks:visited {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#444444; text-decoration:none; }

.punchline{ font-family:"Arial"; font-size:16px; color:#616161; text-decoration:none;}

 
/*.cssbutton {
width: 55px;
height: 25px;
padding:0px;
font-family:'arial';
font-weight:normal;
color:#000;
font-size:12px;
background:url(/redsky/images/btn_bg.gif) repeat-x;
background-color:#e7e7e7;
border:1px solid #cccccc;
}
.cssbutton:hover {
color: black;
background:url(/redsky/images/btn_hoverbg.gif) repeat-x;
background-color: #c1c2bf;
}
*/



.cssbutton {
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #378de5) );
	background:-moz-linear-gradient( center top, #79bbff 5%, #378de5 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5');
	background-color:#79bbff;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #84bbf3;
	display:inline-block;
	color:#ffffff;
	font-family:Arial;
	font-size:13px;
	font-weight:bold;
	font-style:normal;
	height:30px;	
	width:68px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #528ecc;
}
.cssbutton:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff) );
	background:-moz-linear-gradient( center top, #378de5 5%, #79bbff 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff');
	color:#ffffff;
	background-color:#378de5;
}.cssbutton:active {
	position:relative;
	top:1px;
}

.BoxBorder {
padding:10px 10px 5px 30px; 
!padding-top:0px;
border: 1px solid rgb(186, 214, 239);
box-shadow: 0px 0px 5px 0px rgba(212,218,219,0.77);
-webkit-box-shadow: 0px 0px 5px 0px rgba(212,218,219,0.77);
-moz-box-shadow: 0px 0px 5px 0px rgba(212,218,219,0.77);

}

.textbox_username {
	background: url("../images/usersmall-black.png") no-repeat scroll 2% 50% #FFFFFF;
    border: 1px solid #12A6D7;
    color: #616161;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 13px;
    height: 27px;
    !height: 22px;
    width: 225px;
	padding-left: 22px;
	background-color: #FFFFFF;
	display:inline-block;
	!padding-top:5px;
	border-radius:2px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
}
.textbox_username:focus{
	border-color: rgba(38, 155, 201, 0.8);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(38, 155, 201, 0.6);
    outline: 0 none;
}
.textbox_password {
	background: url("../images/passsmall-black.png") no-repeat scroll 2% 50% #FFFFFF;
    border: 1px solid #12A6D7;
    color: #616161;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 13px;
    height: 27px;
    !height: 22px;
	!padding-top:5px;
    width: 225px;
	padding-left: 25px;
	background-color: #FFFFFF;
	display:inline-block;
	border-radius:2px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
}
.textbox_password:focus{
	border-color: rgba(38, 155, 201, 0.8);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(38, 155, 201, 0.6);
    outline: 0 none;
}
div.message{
	background: none repeat scroll 0 50% #F5DFDF !important;
    border: 1px solid #CE9E9E !important;
    border-radius: 0.3em 0.3em 0.3em 0.3em !important;
    color: #000000;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 0.9em;
    font-weight: normal;   
    padding: 5px;    
    text-align: center;
    vertical-align: bottom;
   /* width: 22.5% !important;*/
    width: 287px !important;
    margin:0px auto !important;
    margin-top:3px;
	z-index: 1000;

}

div.error, span.error, li.error {

color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:normal;
text-align:center;
vertical-align:bottom;
z-index: 1000;
font-size: 1em;
}

form li.error{  	
	background-color:  #F5DFDF !important;
    border: 1px solid #F5ACA6 !important;
    border-radius: 0.3em 0.3em 0.3em 0.3em;
    color: #000000;
    margin: 2px 0 !important;
    padding: 1.5px !important;
    width: 294px;
    text-align:center !important;
}

	
    </style>
</head> 
    
 <!--<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script> 
$(document).ready(function(){
	    $(".message").slideDown("slow");   
 
});

</script>
    
    --><script>
    function clear_fileds(){
       	document.forms['loginForm'].elements['j_username'].value = "";
    	document.forms['loginForm'].elements['j_password'].value = "";
    }
     
    </script>
    <script language="javascript" type="text/javascript">
 	 function forgotPassword()
 	{ 
 		document.forms['userOTPForm'].elements['OTPusername'].value = document.forms['loginForm'].elements['j_username'].value
 	 	location.href= "../findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true";
 		document.forms['userOTPForm'].submit();
 	} 
 	
 	function openPartnerRequest(){
 	var sessionCorpID=document.forms['loginForm'].elements['corpID'].value;
	location.href="partnerRequestAccess.html?decorator=popup&popup=true&bypass_sessionfilter=YES&sessionCorpID="+sessionCorpID;	
}
 </script>
 
 <script>
window.onload = function() {
	var scrollOne = new tScroll();
	scrollOne.init('Scroller');

}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.5});
}
</script>
<script language="javascript" type="text/javascript">
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
var today = new Date(); 
var expiry = new Date(today.getTime() + 505000); 
function setCookie(name, value) { 
	document.cookie=name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString(); 
}
function loginAccountPortalStatusCheck(){
		var chk=getCookie('error2');
		if(chk!='true'){
			var userNameStatus=getCookie('username');		
        	//location.href="updateSucFail.html?decorator=popup&popup=true&bypass_sessionfilter=YES&userNameStatus="+userNameStatus;
		}
}
 </script>

<body id="login"/ style="text-align:center">
<c:if test="${not empty messages}">
    <div class="message" id="successMessages">    
        <c:forEach var="msg" items="${messages}">
            <img src="<c:url value="/images/iconInformation.gif"/>"
                alt="<fmt:message key="icon.information"/>" class="icon" />
            <c:out value="${msg}" escapeXml="false"/><br />
        </c:forEach>
    </div>
    <c:remove var="messages" scope="session"/>
</c:if>
<%@page language="java" import="java.io.*"%>
<form method="post" id="loginForm" name="loginForm" onClick="setCookie('error2', 'null');" action="<c:url value='/j_security_check_account'/>"
    onsubmit="saveUsername(this); return validateForm(this),submit_form();">
<c:set var="corpID" value="<%=request.getParameter("corpID") %>" />
<s:hidden name="corpID" value="<%=request.getParameter("corpID") %>" />
    <div id="errormessage">
<c:if test="${param.error != null}">
    <li class="error">
        <img src="${ctx}/images/logerror.png" alt="<fmt:message key='icon.warning'/>" class="icon"/>
        <fmt:message key="errors.password.mismatch"/>
        <%--${sessionScope.ACEGI_SECURITY_LAST_EXCEPTION.message}--%>
    </li>
</c:if>
</div>
<div id="wrapper" style="margin:0px auto; text-align:center;">

<div id="logincontainer" style="margin-bottom:1px;" >

<div id="indexlogo"><img src="${pageContext.request.contextPath}/images/logo-redsky.gif" style="cursor: default;"/></div>
<div id="marquee"><%
     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/clients.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }
%></div>
<div id="leftcolumn">
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
    
      <td align="left"><img src="${pageContext.request.contextPath}/images/title-account.png"  style="cursor: default;"/></td>
    </tr>
     <tr>
     <td><%
     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/RSuserMessage.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }
%></td>
      <td align="left" class="content">&nbsp;</td>
    </tr>
  </table>
  <table border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:65px">
  <tr>
  <td style="height:35px;!height:143px;">&nbsp;</td>
  </tr>
  
  <tr>
  <td style="width:26px;">&nbsp;</td>
  <td style="!padding-left:24px;" align="left">
 <!--  <div id="02e8fab8-4533-4af0-a212-95518d622fbd" class="trust"> 
  <script type="text/javascript" src="http://privacy-policy.truste.com/privacy-seal/RedSky-Mobility-Solutions,-LLC/asc?rid=02e8fab8-4533-4af0-a212-95518d622fbd"></script><a href="//privacy.truste.com/privacy-seal/RedSky-Mobility-Solutions,-LLC/validation?rid=3470f34c-05bb-4946-96d8-a87d03b1cb29" title="Validate TRUSTe privacy certification" target="_blank"><img style="border: none" src="http://privacy-policy.truste.com/privacy-seal/RedSky-Mobility-Solutions,-LLC/seal?rid=0912fcba-6cfa-4b5b-991d-2afa52719b3c" alt="Validate TRUSTe privacy certification"/></a>
  </div> -->
  </td>
  </tr>
  </table>
</div>

<div id="rightcolumn" style="text-align:left;">
  <table border="0" cellpadding="0" cellspacing="0" width="310" class="BoxBorder">
  <tr>
      <td style="height:0px;!height:15px;"></td>     
    </tr>
    <tr>
      <td class="contentLabel"  colspan="4">Username</td>
	   </tr>
    <tr>
      <td colspan="4"><label>
        <input type="text" class="textbox_username" name="j_username" id="j_username" tabindex="1" />
      </label></td>
    </tr>
    <tr>
      <td style="height:10px;!height:0px;"></td>     
    </tr>
    <tr>
      <td class="contentLabel"  colspan="4">Password</td>
	   </tr>
    <tr>
      <td colspan="4"><input type="password" class="textbox_password" name="j_password" id="j_password" tabindex="2" />
    </tr>
    <tr>
      <td colspan="3" class="content" height="5"></td>
      </tr>
      <c:if test="${appConfig['rememberMeEnabled']}">
      <tr>
    <td colspan="3">
    <table border="0" cellpadding="2" cellspacing="0" style="margin:0px;">
	 <tr>
      <td style="height:1px;!height:0px;"></td>     
    </tr>
    <tr>  
      <td class="content" width=""><input type="checkbox" style="margin:0px;" class="checkbox" name="rememberMe" id="rememberMe" tabindex="3"/></td>
       <td class="content" align="left"><label for="rememberMe"><fmt:message key="login.rememberMe"/></label>
      </td>
    </tr>
    </table>
    </td>
    </tr>
    </c:if>
     <tr>
      <td class="content" style="height:10px;!height:0px;"></td>
    </tr>
    <tr>     
      <td colspan="2"><input type="submit" class="cssbutton" name="login" value="<fmt:message key='button.login'/>" tabindex="4" style="">&nbsp;<input type="button" class="cssbutton" name="Clear" value="Clear" onClick="clear_fileds();" style=""></td>
    </tr>    
   <tr>     
       <td colspan="2" style="height:15px;!height:0px;"></td>
    </tr>
    <tr>      
      <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin:0px 0px 2px 0px;">
         <tr>
        <td align="center"><img src="${pageContext.request.contextPath}/images/circle-arrow-right.png"  /></td>
        <td class="formtext" colspan="2" align="left" style="padding-left: 4px;color: #71b9c3;">
         <script language="javascript">

           document.write('<a href="javascript:forgotPassword()" class="otherlinks" >Forgot Password</a>');

                      </script> 
                    </td></tr>
                       <tr>     
       <td colspan="2" style="height:1px;!height:0px;"></td>
    	</tr> 
        <%-- <tr>
          <td align="center"><img src="${pageContext.request.contextPath}/images/circle-arrow-right.png"  /></td>
          <td align="left"><a href="../signup.html?target=upwd&bypass_sessionfilter=YES" class="otherlinks">Forgot username and Password</a></td>
        </tr> --%>  
      </table>
      </td>
    </tr>
  </table>
  
     </div>
     </div>
               
  <!-- Modified By Arbind at 16-Feb-2012 -->       
 
 <div id="privacy">
  <div style="float:left;">  
   
  <%-- <div id="privacyLogo">
  <img src="${pageContext.request.contextPath}/images/SafeHarboR.gif" width="160" height="84" border="0" usemap="#Map" />
<map name="Map" id="Map">
  <area shape="rect" coords="2,1,158,17" href="http://safeharbor.export.gov/list.aspx" target="_blank" />
  <area shape="rect" coords="3,18,158,83" href="http://www.export.gov/safeharbor" target="_blank" />
</map>
  </div> --%>
  <div id="privacyText"><!-- RedSky complies with the US-EU & US-Swiss <a href="http://www.export.gov/safeharbor" target="_blank"><font color="#0428db">Safe Harbor Frameworks</font></a> for Data Privacy. -->
   View our <a href="https://redskymobility.com/RedSky%20EU%20and%20Swiss-US%20Privacy%20Shield_20170522.pdf" target="_blank" style="margin-top: 15px !important; display: inline-block;">Privacy Policy</a>.
  </div>
  </div>
  </div>
 
  <!-- end 16-Feb-2012 -->     
    
     </div>
  
 </form>
 
 <script type="text/javascript">
<!--
function popup(url) 
{
 var width  = 850;
 var height = 800;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}
// -->
</script>

          
    
<script type="text/javascript">   
    document.forms['loginForm'].elements['j_username'].focus();  
    
</script> 
<c:if test="${param.error != null}">
<script language="javascript" type="text/javascript">
loginAccountPortalStatusCheck();
setCookie('error2', 'true');
</script>
</c:if>
 <s:form name="userOTPForm" action="../findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true" method="post" validate="true" >
<input type="hidden" class="textbox" id="OTPusername" name="OTPusername" tabindex="1"  value=""/>
<input type="hidden" class="textbox" id="bypass_sessionfilter" name="bypass_sessionfilter"   value="YES"/>
</s:form>  
<%@ include file="/scripts/login.js"%>