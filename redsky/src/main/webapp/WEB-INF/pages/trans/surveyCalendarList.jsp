<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Survey Calendar</title>
<meta name="heading" content="Survey Calendar" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<head >
<!-- Modified script 12-Dec-2013 --> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.css'/>" /> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.print.css'/>" media='print'/> 
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/cupertino/jquery-ui.css" />	
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/moment.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquerycustom.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/styles/fullcalendar/fullcalendarcustom.min.js"></script>
<!-- Modification closed here -->
<script type="text/javascript">
$(document).ready(function() {
	$('#calendar').fullCalendar({
		editable: true,
		eventLimit: 11,			
		defaultView: 'basicWeek',
		theme:true,
		header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,basicWeek,basicDay'
		},
	    eventSources: [
	       	        {
	    	            events: [ 
	    	                    
	    	                          
	    				<c:forEach var="entry" items="${surveyCalendarViewList}" varStatus="rowCounter"> 
	    				<c:if test="${rowCounter.count>1}">
	    				,
	    				</c:if>
	    	                {
		                    title  : "${entry.title}",
		                    start  : "${entry.start}T00:00:00",
		                    end    : "${entry.end}T23:58:00",
			                color  : "#C3CCF1"
	    	                }
	    	            </c:forEach>
	    	            
	    	            ]

	    	        }
	    ],
	    eventRender: function(event, element) {
	        element.find(element.find(".fc-title").html("<span style='color:#000;font-size:11px;line-height:14px;'>"+event.title+"</span>"));
	    }
		});
	
	var mycal = $('#calendar td.fc-today');
	if(mycal){
	 var position = $(mycal).position();
	 if(position){
	window.scrollTo(0, position.top);
	 }
	}
	$("#calendar .fc-button").click(function(){ 
		var mycal1 = $('#calendar td.fc-today');
		if(mycal1){
		 	var position1 = $(mycal1).position();
		 	if(position1){
			window.scrollTo(0, position1.top);
		 	}
		}
	})
	});
	</script>	
</head>
<style> <%@ include file="/common/calenderStyle.css"%> 

</style>
<style type="text/css">

.fc-day-grid-container { height : auto !important}
.fc-popover {width:385px !important; }
.fc-popover .fc-more-popover { margin-right:10px !important}

.fc-view-container {
	overflow-y: auto !important;
	overflow-x: hidden !important;
    height: 652px !important;
}
.fc-more-popover .fc-event-container {
    padding: 10px !important;
    padding-right: 2px !important;
    overflow-y: auto !important;
    height: 300px !important;
}


.fc-popover div.fc-content{text-align:left !important}

.fc-toolbar .fc-state-active, .fc-toolbar .ui-state-active {
    z-index: 1 !important;
}
.fc-time{ display:none !important}

.fc button {
    
    width: auto !important;
}

td.ui-widget-header table{ margin:0px !important; }

.fc-title{ white-space: normal !important;}

 #calendar {
  width: 900px;
/*  height: 350px; */
  height: auto;
 /* margin: 0 auto;
  margin-top: 75px; */
  margin:0px;
  text-align: center;
  font-size: 12px;
  font-family: verdana,arial,helvetica,sans-serif;
  font-color: 'blue';
  } 
  
  a{color: #2779aa}
  a:hover{color: #ff0000}
  a:active{color: #2779aa}
	h2 {
    background: #d7ebf9 url("images/ui-bg_glass_80_d7ebf9_1x400.png") repeat-x scroll 50% 50%;
    border: 1px solid #aed0ea;
    border-radius: 3px;
    color: #2779aa;
    font-size: 17px !important;
    padding: 3px;
}
	.subcontent-tab {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;font-weight: bold;
		color: #15428B;text-decoration: none;
		background:url(images/collapsebg.gif) #BCD2EF;
		padding:4px 3px 1px 5px; height:15px;
		width:598px; 
		border:1px solid #99BBE8; 
		border-right:none; 
		border-left:none
	} 
	a.dsphead{
		text-decoration:none;
		color:#000000;
	}
	.dspchar2{
		padding-left:0px;
	}
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
	.bgblue{
		background:url(images/blue_band.jpg); 
		height: 30px; 
		width:630px; 
		background-repeat: no-repeat;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-weight: bold;
		color: #25383c; 
		padding-left: 40px; 
	}
	.fc-state-highlight {
	background: #808080;
	}
	.fc-event-inner {
    text-align: left;
	}
	.fc-event {
   /* background-color: #CDDEE7 !important;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f0f9ff+0,cddee7+0,b9d6e5+100 */
background: #f0f9ff; 
background: -moz-linear-gradient(top,  #f0f9ff 0%, #dae6f2 47%, #b6d1ee 100%) !important; 
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f0f9ff), color-stop(47%,#dae6f2), color-stop(100%,#b6d1ee)) !important; 
background: -webkit-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important;
background: -o-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
background: -ms-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
background: linear-gradient(to bottom,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f0f9ff', endColorstr='#b6d1ee',GradientType=0 ) !important; 
border :1px solid #6a859a !important;    
    
    border: 1px solid #6BA5C1 !important;
    color: #000000;
    cursor: default;
    font-size: 0.85em;
    margin-top:1px !important;
    margin-bottom:1px !important;
}

.fc-grid .fc-day-content { background: #fff none repeat scroll 0 0;}

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
background: #edd8a9 !important; 
background: -moz-linear-gradient(top,  #edd8a9 0%, #f4ab44 48%, #ffd672 100%) !important; 
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#edd8a9), color-stop(48%,#f4ab44), color-stop(100%,#ffd672)) !important; 
background: -webkit-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: -o-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: -ms-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: linear-gradient(to bottom,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#edd8a9', endColorstr='#ffd672',GradientType=0 ) !important;
 border: 1px solid orange;
 color: #444;
 font-weight: normal;
}
form { margin-top:-10px;}
.scrollup{ position:fixed; right:20px; top:500px  }
</style>

<script type="text/javascript">
function refreshCalendar(){
	var estimated= document.forms['priceCalendarForm'].elements['estimated'].value;
	location.href = 'surveyCalendarList.html?estimated='+estimated+'&decorator=popup&popup=true';
}

</script>

</head>

<s:form name="priceCalendarForm" id="priceCalendarForm" action="" method="post" validate="true">

<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="type"/>
<div id="layer1" style="width:950px; margin: 0px auto;">

<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="2" border="0" style="margin:0px" width="100%">	
	

	<tr>		  		

	  		
			<td align="right" width="63px" class="listwhitetext" >Consultant</td>		  
	  	  	<td width="160px" > 
	  	  	 <s:select name="estimated" list="%{saleConsultant}" cssStyle="width:125px" cssClass="list-menu" headerKey="" headerValue="" />
		  		</td>
			<td colspan="2"> <input type="button" class="cssbutton" style="margin-right: 5px;width:116px;" value="Refresh Calendar" onclick="refreshCalendar();"/></td>	  		
	</tr>	 
	<tr><td height="10"></td></tr>
		 				
</table>   
<div id="calendar">
<div width="100%" style="border:0px">
<img id="scrollup" style="float:right" src="${pageContext.request.contextPath}/images/top.jpg" title="Go To Top" class="scrollup" />
</div>
</div>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('#calendar .scrollup').fadeIn(duration);
		} else {
			jQuery('#calendar .scrollup').fadeOut(duration);
		}
	});
	
    $('#calendar .scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 2000);        
        return false;
    });
});
</script>
</s:form>
