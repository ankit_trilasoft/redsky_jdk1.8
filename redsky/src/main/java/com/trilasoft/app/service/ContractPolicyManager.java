package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ContractPolicy;

public interface ContractPolicyManager extends GenericManager<ContractPolicy, Long> {   

	public List checkById(String code);

	public List getContractPolicy(String code, String sessionCorpID);
	public List findPolicyFileList(String code, String sessionCorpID);
	public List findPolicyFileNewList(String code, String sessionCorpID);
	public List getPolicyReference(String partnerCode, String corpID,Long parentId);
	public List findDocumentList(String code, String sectionName ,String language ,String sessionCorpID);
	public int checkSectionName(String sectionName ,String Language,String code, String sessionCorpID);
	public List getPolicyDocReference(String partnerCode, String corpID,Long parentId,String sectionName,String language);
	public int updatePolicyDocument(String dummsectionName,String dummlanguage,String code, String sectionName ,String language ,String sessionCorpID);
	public int deletedDocument(String documentSectionName,String documentLanguage,String partnerCode,String sessionCorpID);
	public List getContractPolicyRef(String partnerCode,String sessionCorpID);
	public List policyFilesList(String sessionCorpID);
	public int sectionNameCheck(String sectionName ,String Language, String sessionCorpID);
	public List documentFileList( String sectionName ,String language,String sessionCorpID);
	public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID);
	public void deleteChildDocument(Long id2, String sessionCorpID);
	public void deleteChildDocumentFromPolicy(Long id2, String sessionCorpID);
	public int checkSequenceNumber(Long sequenceNumber ,String Language,String code, String sessionCorpID);
	public int deletedPolicyContract(String partnerCode,String sessionCorpID);
	public String findByBookingLastName(String partnerCode, String corpid);
	}
