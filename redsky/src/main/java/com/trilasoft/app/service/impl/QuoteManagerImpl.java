package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import com.trilasoft.app.dao.QuoteDao;   
import com.trilasoft.app.model.CustomerInformation;
import com.trilasoft.app.model.Quote;   
import com.trilasoft.app.model.QuoteItem;
import com.trilasoft.app.model.QuoteRate;
import com.trilasoft.app.model.SecorQuote;
import com.trilasoft.app.service.QuoteManager;   
import org.appfuse.service.impl.GenericManagerImpl;   
  
public class QuoteManagerImpl extends GenericManagerImpl<Quote, Long> implements QuoteManager {   
    QuoteDao quoteDao;   
  
    public QuoteManagerImpl(QuoteDao quoteDao) {   
        super(quoteDao);   
        this.quoteDao = quoteDao;   
    }   
    
    public List<Quote> findBySequenceNumber(Long sequenceNumber){
    	return quoteDao.findBySequenceNumber(sequenceNumber);
    }

	public List searchQuoteDetailList(String firstName, String lastName,
			Date createdOn, String originCity, String originCountry,
			String destinationCity, String destinationCountry, String sessionCorpID) {
	return quoteDao.searchQuoteDetailList(firstName,lastName,createdOn,originCity,originCountry,destinationCity,destinationCountry,sessionCorpID);
	}

	public List<SecorQuote> findSecorQuoteByQuoteId(String sessionCorpID, Long quoteId) {
		return quoteDao.findSecorQuoteByQuoteId(sessionCorpID, quoteId);
	}

	public List<CustomerInformation> findCustomerInformationByQuoteId(String sessionCorpID, Long quoteId) {
		return quoteDao.findCustomerInformationByQuoteId(sessionCorpID, quoteId);
	}

	public List<QuoteItem> findQuoteItemByQuoteId(String sessionCorpID, Long quoteId) {
		return quoteDao.findQuoteItemByQuoteId(sessionCorpID, quoteId);
	}

	public List<QuoteRate> findQuoteRateByQuoteId(String sessionCorpID,	Long quoteId) {
		return quoteDao.findQuoteRateByQuoteId(sessionCorpID, quoteId);
	}

	
}