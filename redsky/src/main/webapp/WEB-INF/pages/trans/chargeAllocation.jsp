<!-- *File Name:subcontractorCharges.jsp 
 * Created By:Ashish Mishra 
 * Created Date:08-Aug-2008
 * Summary: This jsp is created for showing List Subcontractor Charges.
 -->
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="subcontractorChargesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='subcontractorChargesList.heading'/>"/> 
    <style type="text/css">

/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}
.table tfoot td {
border:1px solid #E0E0E0;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
</style> 

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>

</head>  
<s:form id="subcontractorChargesList" action="searchSubcontractorCharges" validate="true" method="post"> 
  <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
  <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
  <s:hidden name="formStatus" />
<c:out value="${searchresults}" escapeXml="false" />  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Subcontractor Charges List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<table class="" cellspacing="0" cellpadding="1" border="0" style="width:100%">
 <tbody>
   <tr>
	 <td>  
	  <s:set name="subcontractorChargesExt" value="subcontractorChargesExt" scope="request"/>
      <display:table name="subcontractorChargesExt" class="table" requestURI="" id="subcontractorChargesList1" export="false" defaultsort="1" pagesize="10" style="width:100%">   
      
      <display:column sortable="true" titleKey="subcontractorCharges.personIdList" >
	<a href="editChargeSubcontractor.html?decorator=popup&popup=true&id=${subcontractorChargesList1.id}"><c:out value="${subcontractorChargesList1.personId}" /></a>
	</display:column>
      <display:column property="personType" sortable="true" titleKey="subcontractorCharges.personTypeList"/>
      <display:column property="amount" sortable="true" titleKey="subcontractorCharges.amount"/>
      <display:column property="branch" sortable="true" titleKey="subcontractorCharges.branch"/>
      <display:column property="description" sortable="true" titleKey="subcontractorCharges.description"/>
      <display:column property="regNumber" sortable="true" titleKey="subcontractorCharges.regNumber"/>
      <display:column property="approved" title="Approved" format="{0,date,dd-MMM-yyyy}"/>
     <display:column property="accountSent" title="Sent Ac" format="{0,date,dd-MMM-yyyy}"/> 
      <display:setProperty name="paging.banner.item_name" value="container"/>  
      <display:setProperty name="paging.banner.items_name" value="container"/> 
     <display:setProperty name="export.excel.filename" value="Container List.xls"/>   
     <display:setProperty name="export.csv.filename" value="Container List.csv"/>   
     <display:setProperty name="export.pdf.filename" value="Container List.pdf"/>   
 
     <display:footer>
			<tr>
 	  	           <td align="right"><b><div align="right"><fmt:message key="subcontractorCharges.totalAmount"/></div></b></td>
 	  	           <td></td>
		           <td align="left"  width="70px"> 
		           <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" groupingUsed="true" value="${totalAmount}" /> 
                   </td>
                   <td colspan="5"></td>
		  	</tr>
	</display:footer><!--
--></display:table>
<!--<s:textfield name="vid" value="<%=request.getParameter("id") %>"/>
--><c:set var="vid"  value="<%=request.getParameter("id") %>"/>
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"   onclick="location.href='<c:url value="/editChargeSubcontractor.html?vid=${vid}&decorator=popup&popup=true"/>'" value="<fmt:message key="button.add"/>" /><!--  
    <input type="button" value="Add" class="cssbuttonA" style="width:55px; height:25px" onclick="newWindow('editsubcontractorCharges.html?regNum=${vanLine.regNum}&decorator=popup&popup=true','','width=750,height=400');" />  
  --></td>
  </tr>
  </tbody>
  </table>
  </div>
  </s:form> 
  
<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>  
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['subcontractorChargesList'].elements['subcontractorCharges.personId'].value = "";
	document.forms['subcontractorChargesList'].elements['subcontractorCharges.personType'].value = "";
	document.forms['subcontractorChargesList'].elements['subcontractorCharges.branch'].value = "";
	document.forms['subcontractorChargesList'].elements['subcontractorCharges.regNumber'].value = "";
	document.forms['subcontractorChargesList'].elements['subcontractorCharges.approved'].value = "";
}
</script>
<script>
function newWindow(mypage,myname,w,h,features) {
  var winl = (screen.width-w)/2;
  var wint = (screen.height-h)/2;
  if (winl < 0) winl = 0;
  if (wint < 0) wint = 0;
  var settings = 'height=' + h + ',';
  settings += 'width=' + w + ',';
  settings += 'top=' + wint + ',';
  settings += 'left=' + winl + ',';
  settings += features;
  win = window.open(mypage,myname,settings);
  win.window.focus();
}
</script>
<%-- Shifting Closed Here --%>  
 
<script type="text/javascript">
    highlightTableRows("subcontractorChargesList1");
</script>
  
     