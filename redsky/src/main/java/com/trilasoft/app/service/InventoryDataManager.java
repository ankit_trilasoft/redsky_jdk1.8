package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.WorkTicket;


public interface InventoryDataManager extends GenericManager<InventoryData, Long> {
	public List getListByType(String type,Long id);
	public List getSumOfListByType(String type,Long id,Long soid);
	public String updateEstimatedVolumeCft(int volumeAir,int volumeSea,int volumeRoad,int volumeStorage,Long cid);
	public List getInventoryListForSO(Long id,Long cid);
	public List getInventoryListForSOByType(Long id,Long cid,String type);
	public Boolean getCountForSameModeTypeSO(Long cid);
	public List getSOListbyCF(Long cid);
	public List getDistinctMode(Long cid);
	public int countByIndividualMode(Long cid,String mode);
	public void InventorySOMapping(Long cid,String modeSORelation,String modeWithOneSO ,String sessionCorpID, String userName);
	public void updateOtherModeList(Long cid,String otherModeItem);
	public int workTicketCount(Long id,String sessionCorpID);
	public void tranferInventoryMaterial(Long id,Long cid,String sessionCorpID);
	public List getWorkTicket(Long id,String sessionCorpID);
	public List getInventoryListByType(Long id,Long cid,String type);
	public List getWorkTicketList(Long id,String sessionCorpID);
	public void setOtherModeInventorySOid(Long cid,String otherModeItemList);
	public String getSOTransferStatus(String type,Long cid,String sessionCorpID);
	public List getDistinctArticleInventoryListByType(Long id,Long cid,String type);
	public void updateWeghtAndVol(Long cid, Long sid,String sessionCorpID );
	public String findServiceOrderId(String tempOrgId, String tempEmfId);
	public List getItemList(Long sid,String sessionCorpID);
	public List getVehicle(Long sid,String sessionCorpID);
	public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID);
	public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID);
	public AccessInfo getAccessinfo(Long cid, String sessionCorpID, Long sid);
	public WorkTicket getWorkTicketByTicket(String ticket, String sessionCorpID);
	public List<Long> getWorkTicketIdList(String shipno, String sessionCorpID);
	public InventoryData getLinkedInventoryData(Long invId, Long cId, Long sId);
	public void deleteInventoryData(Long sid, Long networkSynchedId);
	public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid, Long cid);
	public void updateLinkedVehicleData(String newField, String fieldValue, Long sid, String idNumber);
	public void removeLinkedInventories(Long id);
	public AccessInfo getAccessinfoByCid(Long id, String corpID);
	public InventoryData getSyncInventoryData(Long cid, String corpID,Long networkSynchedId);
	public AccessInfo getAccessinfoByNetworkSyncId(Long id, String corpID);
	public List getInventoryItemForMss(Long sid, Long cid, String sessionCorpID);
}
