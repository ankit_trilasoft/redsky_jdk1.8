package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.RefQuoteServicesDao;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.RefQuoteServices;

public class RefQuoteServicesDaoHibernate extends GenericDaoHibernate<RefQuoteServices,Long> implements RefQuoteServicesDao{

	public RefQuoteServicesDaoHibernate() {
		super(RefQuoteServices.class);
		
	}
	
	public List searchList(String job, String mode,String routing, String langauge, String sessionCorpID ,String description,String checkIncludeExclude,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceType,String contract){
		String a = sessionCorpID;
		String b = "%"+job + "%";
		String c = "%"+ mode + "%";
		String d = "%"+routing + "%";
		String e = "%"+ langauge + "%";
		String f = "%"+checkIncludeExclude + "%";
		String g = "%"+ description + "%";
		String h = "%"+ companyDivision + "%";
		String i = "%"+commodity + "%";
		String j = "%"+ packingMode + "%";
		String k = "%"+originCountry + "%";
		String l = "%"+ destinationCountry + "%";
		String m = "%"+ serviceType + "%";
		String n = "%"+ contract + "%";
			
		Criteria crit = this.getSession().createCriteria(RefQuoteServices.class);
		return crit.add(Restrictions.eq("corpID", a))
		           .add(Restrictions.ilike("job", b))
		           .add(Restrictions.ilike("mode", c))
		           .add(Restrictions.ilike("routing", d))
		           .add(Restrictions.ilike("langauge", e))
		           .add(Restrictions.ilike("checkIncludeExclude", f))
		           .add(Restrictions.ilike("description", g))
		           .add(Restrictions.ilike("companyDivision", h))
		           .add(Restrictions.ilike("commodity", i))
		           .add(Restrictions.ilike("packingMode", j))
		           .add(Restrictions.ilike("originCountry", k))
		           .add(Restrictions.ilike("destinationCountry", l))
		            .add(Restrictions.ilike("serviceType", m))
		            .add(Restrictions.ilike("contract", n))
		           .list();
	}
	public String findUniqueCode(String code, String sessionCorpID) {
		/*return getHibernateTemplate().find(" SELECT distinct code FROM refquoteservices ");*/
		String discodeList="";
		List list = this.getSession().createSQLQuery("SELECT count(code) FROM refquoteservices where code='"+code+"'and corpID='"+sessionCorpID+"' ").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			discodeList=list.get(0).toString();
		}
		return discodeList;
	
	}
	public void autoSaveRefQuotesSevisesAjax(String sessionCorpID,String fieldName,String fieldVal,Long serviceId){
		try{
			if(fieldVal==null){
				if(fieldName!=null && fieldName.equalsIgnoreCase("displayOrder")){
					getSession().createSQLQuery("update refquoteservices set displayOrder=null where id= "+serviceId+" and corpId='"+sessionCorpID+"'").executeUpdate();
				}else{
					getSession().createSQLQuery("update refquoteservices set "+fieldName+"='' where id= "+serviceId+" and corpId='"+sessionCorpID+"'").executeUpdate();
				}
			}else{
				getSession().createSQLQuery("update refquoteservices set "+fieldName+"='"+fieldVal+"' where id= "+serviceId+" and corpId='"+sessionCorpID+"'").executeUpdate();
			}
			}catch(Exception e){
		    	 e.printStackTrace();
			}
	}
	
	public Map<String, String> findForContract(String corpID) {
		Date endDate = null;
		String s = null;
		List list = new ArrayList();
		Map<String, String> contractMap = new LinkedHashMap<String, String>(); 
	    try{	
			list = getSession() .createSQLQuery("select contract,description from contract where corpID='" + corpID + "' and (ending is null OR DATE_FORMAT(ending,'%Y-%m-%d')>= DATE_FORMAT(now(),'%Y-%m-%d')) order by contract")
			.list();
		 
		    Iterator iterator = list.iterator();
		     while (iterator.hasNext()) {
		      String contract = "";
		      String description = "";
		      Object[] row = (Object[]) iterator.next();
		     // if (row[0] != null) {
		        if ((row[0] != null) && (!row[0].toString().equals(""))){
		           contract = row[0].toString();
		      }
		      //if (row[1] != null) {
		        if ((row[1] != null) && (!row[1].toString().equals(""))){
		        	description = row[1].toString();
		      }
		      contractMap.put(contract, contract);
		     } 
		}catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
	return contractMap;
	}
	
}
