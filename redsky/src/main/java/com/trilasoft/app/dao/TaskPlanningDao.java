package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.TaskPlanning;



public interface TaskPlanningDao extends GenericDao<TaskPlanning, Long>{
	
	public List<TaskPlanning> findTaskPlanningList(String corpId,Long id);

}