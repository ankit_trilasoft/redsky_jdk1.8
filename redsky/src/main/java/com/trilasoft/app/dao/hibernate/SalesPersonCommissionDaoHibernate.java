package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.trilasoft.app.dao.SalesPersonCommissionDao;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.SalesPersonCommission;

public class SalesPersonCommissionDaoHibernate extends GenericDaoHibernate<SalesPersonCommission, Long> implements SalesPersonCommissionDao{
	public SalesPersonCommissionDaoHibernate(){
		super(SalesPersonCommission.class);
	}
	public List getCommPersentAcRange(String corpId,String contractName,String chargeCode){
		return getHibernateTemplate().find("from SalesPersonCommission where contractName='"+contractName+"' and chargeCode='"+chargeCode+"' and corpId='"+corpId+"'");
	}
	public Map<Double,BigDecimal > getCommPersentAcMap(String corpId,String contractName,String chargeCode,String companyDiv){
		Map<Double,BigDecimal > commMap = new  HashMap<Double,BigDecimal>();
		String contracts="";
		List contractList = this.getSession().createSQLQuery("select contract from contract where companyDivision='"+companyDiv+"' and corpId='"+corpId+"' and internalCostContract is  true").list();
		if(contractList != null){
			contracts=contractList.toString().replace("[","'").replace("]","'").replaceAll(",","','");
		}
		String query="select r.id,r.quantity1,r.rate1"+
		 " from charges ch, contract c,rategrid r"+
		" where ch.contract=c.contract and r.contractName=c.contract and c.contract in ("+contracts+")"+
		" and r.corpId=ch.corpId and c.corpId = ch.corpId and ch.corpId='"+corpId+"'"+
		" and r.charge=ch.id"+
		 " and ch.bucket= '"+chargeCode+"'";
		
		String idList="";
		List list = this.getSession().createSQLQuery(query).list();
		Iterator it = list.iterator();
		while (it.hasNext()){
			Object []obj=(Object[]) it.next();
			if(idList.equalsIgnoreCase("")){
				idList=obj[0].toString();
			}else{
				idList=idList+","+obj[0].toString();
			}
		}
		List <RateGrid>al=new ArrayList();
		if(!idList.equals("")){
			al=getSession().createQuery("from RateGrid where id in ("+idList+")").list();
		}		
		BigDecimal quantity1 = new BigDecimal(new BigInteger("0"),2);
		BigDecimal rate1 = new BigDecimal(new BigInteger("0"),2);
		if((al!=null)&&(!al.isEmpty()))	{
    		for (RateGrid salesPersonCommission : al) {
    			if((salesPersonCommission.getRate1()!=null)&&(!salesPersonCommission.getRate1().equalsIgnoreCase(""))){
        			rate1=new BigDecimal(Double.parseDouble(salesPersonCommission.getRate1()));
        			}else{
        				rate1=new BigDecimal("0.00");
        			}
    			commMap.put(salesPersonCommission.getQuantity1(), rate1);
    		}
		}
		
		return commMap;
	}
}
