<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="StorageBilling"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="806"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT  'StorageBilling-R-SSCW.jrxml',
	concat(if(trim(serviceorder.prefix)="" || serviceorder.prefix is null,"",serviceorder.prefix),if(trim(serviceorder.firstName)="" || serviceorder.firstName is null,"",if(trim(serviceorder.prefix)="" || serviceorder.prefix is null,trim(serviceorder.firstName),concat(" ",trim(serviceorder.firstName)))),if(trim(serviceorder.lastName)="" || serviceorder.lastName is null,"",if((trim(serviceorder.prefix)="" || serviceorder.prefix is null) AND(trim(serviceorder.firstName)="" || serviceorder.firstName is null),trim(serviceorder.lastName),concat(" ",trim(serviceorder.lastName)))))AS Shipper,
	creditcard.`ccNumber` AS creditcard_ccNumber,
	serviceorder.`shipNumber` AS serviceorder_shipNumber,
	serviceorder.companydivision AS serviceorder_companydivision,
	billing.`billTo1Point` AS billing_billTo1Point,
    	if(billing.`isAutomatedStorageBillingProcessing` is true,'Yes','No') AS billing_isAutomatedStorageBillingProcessing,
	if(creditcard.`primaryFlag` is true,'Yes','No') AS creditcard_primaryFlag,
	if(billing.personBilling is null || billing.personBilling='',"ZZZZZ",billing.personBilling) AS billing_personBilling,
     	creditcard.`expireYear` AS creditcard_expireYear,
        creditcard.`expireMonth` AS creditcard_expireMonth,
	concat(creditcard.`expireYear`,'-',creditcard.`expireMonth`) AS creditcard_time,
        serviceorder.lastName AS serviceorder_lastname
FROM `creditcard` creditcard 
INNER JOIN `billing` billing ON creditcard.`shipNumber` = billing.`shipNumber` and billing.`corpID` = $P{Corporate ID}
INNER JOIN `serviceorder` serviceorder ON serviceorder.`id` = billing.`id` and serviceorder.`corpID` = $P{Corporate ID}
WHERE creditcard.`corpID` = $P{Corporate ID}
 AND billing.billTo1Point in ('AE','MC','VC')
 AND serviceorder.job ='STO'
 AND (billing.`isautomatedstoragebillingprocessing` is not true || creditcard.primaryFlag is not true)
 AND (billing.`isautomatedstoragebillingprocessing` is true AND serviceorder.shipNumber not in (select c.shipNumber from creditcard c where c.corpid=$P{Corporate ID} and c.shipNumber =creditcard.shipNumber and c.`primaryFlag` is true))
 AND billing.`storageout` is null
 AND billing.`billThrough` is not null
 AND serviceorder.status not in ('CLSD','CNCL')
 AND datediff(date_format(concat(expireyear,'-',left(expiremonth,2),'-01'),'%Y-%m-%d'),sysdate()) <= 70]]></queryString>

	<field name="StorageBilling-R-SSCW.jrxml" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="creditcard_ccNumber" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_companydivision" class="java.lang.String"/>
	<field name="billing_billTo1Point" class="java.lang.String"/>
	<field name="billing_isAutomatedStorageBillingProcessing" class="java.lang.String"/>
	<field name="creditcard_primaryFlag" class="java.lang.String"/>
	<field name="billing_personBilling" class="java.lang.String"/>
	<field name="creditcard_expireYear" class="java.lang.String"/>
	<field name="creditcard_expireMonth" class="java.lang.String"/>
	<field name="creditcard_time" class="java.lang.String"/>
	<field name="serviceorder_lastname" class="java.lang.String"/>

	<sortField name="billing_personBilling" />
	<sortField name="creditcard_time" />
	<sortField name="serviceorder_lastname" />


		<group  name="name" >
			<groupExpression><![CDATA[$F{billing_personBilling}]]></groupExpression>
			<groupHeader>
			<band height="19"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="89"
						height="18"
						forecolor="#0066CC"
						key="staticText-61"/>
					<box></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Billing Person]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="89"
						y="1"
						width="281"
						height="18"
						forecolor="#0066CC"
						key="textField-19"/>
					<box></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_personBilling}.equalsIgnoreCase("ZZZZZ") ? "" : $F{billing_personBilling}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="68"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="175"
						y="0"
						width="457"
						height="27"
						forecolor="#CC0066"
						key="staticText-53"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" pdfFontName="Times-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Automatic Storage Billing Report]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="51"
						width="160"
						height="17"
						key="staticText-55"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="160"
						y="51"
						width="123"
						height="17"
						key="staticText-56"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CC #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="283"
						y="51"
						width="87"
						height="17"
						key="staticText-59"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Service Order #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="370"
						y="51"
						width="98"
						height="17"
						key="staticText-59"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Company Division]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="547"
						y="51"
						width="91"
						height="17"
						key="staticText-62"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Payment method]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="638"
						y="51"
						width="101"
						height="17"
						key="staticText-63"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Automated Billing]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="739"
						y="51"
						width="67"
						height="17"
						key="staticText-64"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CC Primary Flag]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="468"
						y="51"
						width="40"
						height="17"
						key="staticText-65"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Month]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="468"
						y="40"
						width="76"
						height="17"
						key="staticText-66"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Expiration]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="512"
						y="51"
						width="32"
						height="17"
						key="staticText-67"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Year]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="160"
						height="18"
						forecolor="#000000"
						key="textField-7"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="160"
						y="1"
						width="123"
						height="18"
						forecolor="#000000"
						key="textField-8"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_ccNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="283"
						y="1"
						width="87"
						height="18"
						forecolor="#000000"
						key="textField-12"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="370"
						y="1"
						width="98"
						height="18"
						forecolor="#000000"
						key="textField-12"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companydivision}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="558"
						y="1"
						width="80"
						height="18"
						forecolor="#000000"
						key="textField-14"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billTo1Point}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="638"
						y="1"
						width="101"
						height="18"
						forecolor="#000000"
						key="textField-15"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_isAutomatedStorageBillingProcessing}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="739"
						y="1"
						width="67"
						height="18"
						forecolor="#000000"
						key="textField-16"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_primaryFlag}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="468"
						y="1"
						width="44"
						height="18"
						forecolor="#000000"
						key="textField-17"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_expireMonth}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="515"
						y="1"
						width="43"
						height="18"
						forecolor="#000000"
						key="textField-18"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_expireYear}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="290"
						y="3"
						width="226"
						height="14"
						key="staticText-54"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<text><![CDATA[StorageBilling.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="650"
						y="3"
						width="117"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="3"
						y="3"
						width="129"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="777"
						y="3"
						width="29"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="806"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
