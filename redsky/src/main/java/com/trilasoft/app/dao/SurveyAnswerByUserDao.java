package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SurveyAnswerByUser;

public interface SurveyAnswerByUserDao extends GenericDao<SurveyAnswerByUser, Long>{
	public List getRecordsByQuestionId(Long qId,Long sid,Long cid);
	public List getRecordsBySOId(Long sid);
	public List getRecordsByCId(Long cid);
}
