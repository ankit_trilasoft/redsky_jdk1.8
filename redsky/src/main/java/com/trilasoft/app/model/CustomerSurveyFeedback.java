package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="customersurveyfeedback")
public class CustomerSurveyFeedback  extends BaseObject{
	private Long id;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;	
	private Date updatedOn;
	private Long serviceOrderId;
	private String shipNumber;
	private Integer customerRating;
	private String feedBack;
	private String serviceName;
	private String serviceDetails;
	private Boolean serviceForDestEmail= false;
	private String vendorCode;
	private String vendorContact;
	
	/*
	 * Fields increased for post move survey
	 * 
	 */
	
	private Integer couselorRating;
	private String couselorFeedBack;
	private Integer oaRating;
	private String oaFeedBack;
	private Integer daRating;
	private String daFeedBack;
	private Integer recommendation;
	private String recommendationFeedBack;
	private String additionalFeedBack;
	private String additionalSecondFeedBack;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("serviceOrderId", serviceOrderId)
				.append("shipNumber", shipNumber)
				.append("customerRating", customerRating)
				.append("feedBack", feedBack)
				.append("serviceName", serviceName)
				.append("serviceDetails", serviceDetails)
				.append("serviceForDestEmail", serviceForDestEmail)
				.append("vendorCode", vendorCode)
				.append("vendorContact", vendorContact)
				.append("couselorRating", couselorRating)
				.append("couselorFeedBack", couselorFeedBack)
				.append("oaRating", oaRating).append("oaFeedBack", oaFeedBack)
				.append("daRating", daRating).append("daFeedBack", daFeedBack)
				.append("recommendation", recommendation)
				.append("recommendationFeedBack", recommendationFeedBack)
				.append("additionalFeedBack", additionalFeedBack)
				.append("additionalSecondFeedBack", additionalSecondFeedBack)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CustomerSurveyFeedback))
			return false;
		CustomerSurveyFeedback castOther = (CustomerSurveyFeedback) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(shipNumber, castOther.shipNumber)
				.append(customerRating, castOther.customerRating)
				.append(feedBack, castOther.feedBack)
				.append(serviceName, castOther.serviceName)
				.append(serviceDetails, castOther.serviceDetails)
				.append(serviceForDestEmail, castOther.serviceForDestEmail)
				.append(vendorCode, castOther.vendorCode)
				.append(vendorContact, castOther.vendorContact)
				.append(couselorRating, castOther.couselorRating)
				.append(couselorFeedBack, castOther.couselorFeedBack)
				.append(oaRating, castOther.oaRating)
				.append(oaFeedBack, castOther.oaFeedBack)
				.append(daRating, castOther.daRating)
				.append(daFeedBack, castOther.daFeedBack)
				.append(recommendation, castOther.recommendation)
				.append(recommendationFeedBack,
						castOther.recommendationFeedBack)
				.append(additionalFeedBack, castOther.additionalFeedBack)
				.append(additionalSecondFeedBack,
						castOther.additionalSecondFeedBack).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(serviceOrderId).append(shipNumber)
				.append(customerRating).append(feedBack).append(serviceName)
				.append(serviceDetails).append(serviceForDestEmail)
				.append(vendorCode).append(vendorContact)
				.append(couselorRating).append(couselorFeedBack)
				.append(oaRating).append(oaFeedBack).append(daRating)
				.append(daFeedBack).append(recommendation)
				.append(recommendationFeedBack).append(additionalFeedBack)
				.append(additionalSecondFeedBack).toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Integer getCustomerRating() {
		return customerRating;
	}
	public void setCustomerRating(Integer customerRating) {
		this.customerRating = customerRating;
	}
	@Column
	public String getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}
	@Column
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@Column
	public String getServiceDetails() {
		return serviceDetails;
	}
	public void setServiceDetails(String serviceDetails) {
		this.serviceDetails = serviceDetails;
	}
	@Column
	public Boolean getServiceForDestEmail() {
		return serviceForDestEmail;
	}
	public void setServiceForDestEmail(Boolean serviceForDestEmail) {
		this.serviceForDestEmail = serviceForDestEmail;
	}
	@Column
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column
	public String getVendorContact() {
		return vendorContact;
	}
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	public Integer getCouselorRating() {
		return couselorRating;
	}
	public void setCouselorRating(Integer couselorRating) {
		this.couselorRating = couselorRating;
	}
	public String getCouselorFeedBack() {
		return couselorFeedBack;
	}
	public void setCouselorFeedBack(String couselorFeedBack) {
		this.couselorFeedBack = couselorFeedBack;
	}
	public Integer getOaRating() {
		return oaRating;
	}
	public void setOaRating(Integer oaRating) {
		this.oaRating = oaRating;
	}
	public String getOaFeedBack() {
		return oaFeedBack;
	}
	public void setOaFeedBack(String oaFeedBack) {
		this.oaFeedBack = oaFeedBack;
	}
	public Integer getDaRating() {
		return daRating;
	}
	public void setDaRating(Integer daRating) {
		this.daRating = daRating;
	}
	public String getDaFeedBack() {
		return daFeedBack;
	}
	public void setDaFeedBack(String daFeedBack) {
		this.daFeedBack = daFeedBack;
	}
	public Integer getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(Integer recommendation) {
		this.recommendation = recommendation;
	}
	public String getRecommendationFeedBack() {
		return recommendationFeedBack;
	}
	public void setRecommendationFeedBack(String recommendationFeedBack) {
		this.recommendationFeedBack = recommendationFeedBack;
	}
	public String getAdditionalFeedBack() {
		return additionalFeedBack;
	}
	public void setAdditionalFeedBack(String additionalFeedBack) {
		this.additionalFeedBack = additionalFeedBack;
	}
	public String getAdditionalSecondFeedBack() {
		return additionalSecondFeedBack;
	}
	public void setAdditionalSecondFeedBack(String additionalSecondFeedBack) {
		this.additionalSecondFeedBack = additionalSecondFeedBack;
	}
}
