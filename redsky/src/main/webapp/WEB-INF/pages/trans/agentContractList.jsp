<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="agentList.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentList.heading'/>"/> 
  <style>
span.pagelinks {
    display: block;
    font-size: 0.9em;
    margin-bottom: 1px;
    margin-top: -5px;
    !margin-top: -23px;       
    text-align: right;
}
#overlay11 {
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
}
  </style>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</head>
<s:form name="contractAgent">
<c:set var="corpId" value="${sessionCorpID}" />
<s:hidden name="contractid" value="<%=request.getParameter("id")%>"/>
<c:set var="contractid" value="<%=request.getParameter("id")%>"/>
<s:hidden name="publishContractId" />
<s:hidden name="btnType" value="<%= request.getParameter("btnType")%>" />
<c:set var="btnType" value="<%= request.getParameter("btnType")%>" />
<div id="Layer1" style="width:75%;">	
<div id="newmnav">
		  <ul>
		    <li><a href="editContract.html?id=${contract.id}"><span>Contract Details</span></a></li>
		    <li><a href="charge23.html?id=${contract.id}"><span>Charge List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Agent List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  		</ul>
  		</div><div class="spn">&nbsp;</div>
  	<div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
    <table style="margin-bottom: 3px">
    <tr>
        <td align="right" class="listwhitetext">Contract</td>
  <td><s:textfield   value="${contract.contract}" required="true" readonly="true" size="30" cssClass="input-textUpper"/></td>
  </tr>
  </table>
  
  </div>
<div class="bottom-header" style="margin-top:30px;"><span></span></div>
</div>
</div>
  		
  		<div class="spnblk">&nbsp;</div>  	
  		<display:table name="agentList" class="table" requestURI="" id="agent" export="true" defaultsort="1" pagesize="5" >
  		<display:column property="accountCode" url="/agentContractAdd.html?contractid=${contractid}" paramId="id" paramProperty="id" title="Agent Code"/>
  		<display:column property="accountName" title="Agent Name"  />
  		<display:column title="Corp ID"  > 
  			<c:if test="${not empty agent.agentCorpID}">
  				<c:out value="${agent.agentCorpID}" />&nbsp;&nbsp;
  				<a href="#" onclick="findPublishAgents('${agent.agentCorpID}',this);" style="text-decoration:none;" >[+]</a> 
  			</c:if>
  		</display:column>
  		<display:column title="Contract Types">
  		<c:if test="${contract.contractType=='NAA'}"></c:if>
  		<c:if test="${contract.contractType!='NAA'}"> 
  			<c:out value="${contract.contractType}">
  		</c:out>
  		</c:if>
  		</display:column>
  		<display:column title="Publish Status">
  			<c:if test="${agent.published=='Y' }"> 
  				<c:if test="${contractEnding !='Y'}">Published</c:if>
  				<c:if test="${contractEnding =='Y'}">Published but outdated</c:if>
  			</c:if>
  			<c:if test="${agent.published!='Y' }"> Not published</c:if>
  		</display:column>
  		<display:column>
  		<c:set var="entry" value="${cmmDmmAgentList}"/>
  		<c:if test="${not empty agent.agentCorpID && agent.agentCorpID != 'UTSI' }" >
  			<c:if test="${contract.contractType=='CMM' || contract.contractType=='DMM'}">
  				<c:if test="${fn:contains(entry, agent.agentCorpID)}">
  					<input type="checkbox" name="agentCorpId" id="${agent.agentCorpID }" value="${agent.agentCorpID }"  onclick="showPublishButton()"/>
  				</c:if>
  			</c:if>
  			
  		</c:if>
  		<c:if test="${empty agent.agentCorpID || contract.contractType==null || contract.contractType=='' || contract.contractType=='NAA'|| agent.agentCorpID == 'UTSI' }" >
  			<input type="checkbox" name="agentCorpId" id="${agent.agentCorpID }" value="${agent.agentCorpID }" disabled="disabled"/>
  		</c:if>
  		
  		</display:column>
  		
  		<display:column title="Remove" style="width: 15px;">
  			<c:if test="${agent.published!='Y' }">
  			 	<img align="middle" title="" onclick="confirmSubmit('${agent.id}','${contractid}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
			</c:if>
		</display:column>
  	
  		<display:setProperty name="export.excel.filename" value="agentContract List.xls"/>   
        <display:setProperty name="export.csv.filename" value="agentContract List.csv"/>   
        <display:setProperty name="export.pdf.filename" value="agentContract List.pdf"/>
  		</display:table>	
  	
  	
  		<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/agentContractAdd.html?contractid=${contractid}"/>'"  
        value="Add"/>
        <input type="button" class="cssbuttonA" style="width:120px; height:25px"  
        onclick="publishSelectedContract()"  
        value="Publish Contract" name="publish" disabled="true"/>
</div>
<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>
<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>

<script language="javascript" type="text/javascript"">
function publishSelectedContract(){
	showOrHide(0);
   var chkObj = document.forms['contractAgent'].elements['agentCorpId'];
   var tempId='';
   var publishButtonClick=0;
   try{
		if(chkObj.length>1){
		   for(var i=0; i<chkObj.length; i++){
				if(chkObj[i].checked == true){
					if(tempId==''){
						tempId=chkObj[i].value;
					}else{
						tempId= tempId+','+chkObj[i].value ;
					}
				}
			}
	   }else{
			if(chkObj.checked == true){
				tempId = chkObj.value;
			}
       }
    }catch(e){}
	 document.forms['contractAgent'].elements['publishContractId'].value = tempId;
	 var url = "publishContract.html";
 	 document.forms['contractAgent'].action = url;
	 document.forms['contractAgent'].submit();
	 publishButtonClick++;
	 if(publishButtonClick>0){
		 document.forms['contractAgent'].elements['publish'].disabled = true;
	 }
	 //setTimeout(function(){displayProgressBar();}, 1000);
	 showOrHide(1);
}

function displayProgressBar(){
	var contractName = '${contract.contract}';
	 var agentCorpIdName = document.forms['contractAgent'].elements['publishContractId'].value;
	 agentCorpIdName = agentCorpIdName.split(",");
	 for(var i = 0; i < agentCorpIdName.length; i++) {
	 	window.open('publishContractProgressbar.html?contractName='+contractName+'&agentCorpIdName='+agentCorpIdName[i]+'&decorator=simple&popup=true','mywin'+agentCorpIdName[i],'location=1,status=1,scrollbars=1,width=610,height=150, top=300, left=300');
	 }
}
function showPublishButton(){
	var chk = document.forms['contractAgent'].elements['agentCorpId'] ;
	var count =0;
	try{
		if(chk.length>1){
			for(var i=0; i<chk.length; i++){
				if(chk[i].checked == true){
					count++;
				}
			}
		}else{
			if(chk.checked == true){
				count++;
			}
		}
		if(count>0 ){
			  document.forms['contractAgent'].elements['publish'].disabled = false;
		}else{
			document.forms['contractAgent'].elements['publish'].disabled = true;
		}
	}catch(e){}
}
function confirmSubmit(targetElement, targetElement2){
	var agree=confirm("Please confirm that you want to delete this agent ?");
	var id = targetElement;
	var contractid=targetElement2;
	if (agree){
        window.location="deleteContractAgent.html?delAccountId="+id+"&id="+contractid+"&btnType=del";
	}
}
  function show(){
    var redirect = document.forms['contractAgent'].elements['btnType'].value;
	if(redirect=='del'){
		location.href = "agentContractLists.html?id=${contract.id}";
	}
}
function findPublishAgents(partnerCorpId,position){
	  var url="findToolTipfindPublishAgents.html?ajax=1&decorator=simple&popup=true&agentCorpId=" + encodeURI(partnerCorpId);
	  ajax_showTooltip(url,position);
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
try{ 
	show();
	showOrHide(0);
}catch(e){}
</script>