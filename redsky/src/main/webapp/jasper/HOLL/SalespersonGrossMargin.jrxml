<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SalespersonGrossMargin" pageWidth="1728" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1728" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" isIgnorePagination="true" uuid="8c039a7c-017b-44df-a32a-8e3868300b70">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="940"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Date_From" class="java.util.Date"/>
	<parameter name="Date_To" class="java.util.Date"/>
	<parameter name="Sales Person" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT serviceorder.id as serviceorder_id,
serviceorder.shipNumber as 'SO_NUMBER',
serviceorder.registrationNumber as 'REGISTRATION_NUM_SO',
billing.contract as 'CONTRACT',
accountline.recInvoiceNumber as 'INVOICE #',
serviceorder.lastName as 'LAST_NAME',
serviceorder.salesMan as 'SALESMAN',
accountline.chargeCode as 'CHARGE_CODE',
accountline.description as 'CHARGE_CODE_DESCRIPTION',
round(accountline.actualRevenue,2) as 'DYNAMIC_REVENUE',
round(accountline.actualExpense,2) as 'DYNAMIC_EXPENSE',
(round(accountline.actualRevenue,2) - round(accountline.actualExpense,2)) as 'G_M',
round((round(accountline.actualRevenue,2) - round(accountline.actualExpense,2)) * (100/round(accountline.actualRevenue,2)),2) as 'G_M_%',
billing.billToName as 'BILL_To_NAME',
"AAA" as report_group
from serviceorder serviceorder
inner join billing billing on billing.id=serviceorder.id and billing.corpid=$P{Corporate ID}
left outer join accountline accountline on accountline.serviceorderid=serviceorder.id and accountline.corpid=$P{Corporate ID}
where serviceorder.corpid=$P{Corporate ID}
and serviceorder.status !='CNCL'
and ((date(accountline.paypostdate) BETWEEN $P{Date_From} AND $P{Date_To}) or (date(accountline.recPostDate) BETWEEN $P{Date_From} AND $P{Date_To}))
and serviceorder.salesMan LIKE $P{Sales Person}
order by serviceorder.id]]>
	</queryString>
	<field name="serviceorder_id" class="java.lang.Long"/>
	<field name="SO_NUMBER" class="java.lang.String"/>
	<field name="REGISTRATION_NUM_SO" class="java.lang.String"/>
	<field name="CONTRACT" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="INVOICE #" class="java.lang.String"/>
	<field name="LAST_NAME" class="java.lang.String"/>
	<field name="SALESMAN" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CHARGE_CODE" class="java.lang.String"/>
	<field name="CHARGE_CODE_DESCRIPTION" class="java.lang.String"/>
	<field name="DYNAMIC_REVENUE" class="java.math.BigDecimal"/>
	<field name="DYNAMIC_EXPENSE" class="java.math.BigDecimal"/>
	<field name="G_M" class="java.math.BigDecimal"/>
	<field name="G_M_%" class="java.math.BigDecimal"/>
	<field name="BILL_To_NAME" class="java.lang.String"/>
	<field name="report_group" class="java.lang.String"/>
	<variable name="TotalRevenuePerSo" class="java.math.BigDecimal" resetType="Group" resetGroup="shipNumber" calculation="Sum">
		<variableExpression><![CDATA[$F{DYNAMIC_REVENUE}]]></variableExpression>
	</variable>
	<variable name="TotalExpensesPerSo" class="java.math.BigDecimal" resetType="Group" resetGroup="shipNumber" calculation="Sum">
		<variableExpression><![CDATA[$F{DYNAMIC_EXPENSE}]]></variableExpression>
	</variable>
	<variable name="TotalGrossMarginPerSo" class="java.math.BigDecimal" resetType="Group" resetGroup="shipNumber" calculation="Sum">
		<variableExpression><![CDATA[$F{G_M}]]></variableExpression>
	</variable>
	<variable name="GrossMarginPercentage" class="java.math.BigDecimal" resetType="Group" resetGroup="shipNumber">
		<variableExpression><![CDATA[$V{TotalRevenuePerSo}.intValue()==0 ? new BigDecimal(0.00):
(
$V{TotalGrossMarginPerSo}.intValue()==0 ? new BigDecimal(0.00):
(($V{TotalGrossMarginPerSo}.multiply(new BigDecimal(100.00))).divide($V{TotalRevenuePerSo},4))
)]]></variableExpression>
	</variable>
	<variable name="TotalRevenueReport" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{DYNAMIC_REVENUE}]]></variableExpression>
	</variable>
	<variable name="TotalExpensesReport" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{DYNAMIC_EXPENSE}]]></variableExpression>
	</variable>
	<variable name="TotalGrossMarginReport" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{G_M}]]></variableExpression>
	</variable>
	<variable name="GrossMarginPercentageReport" class="java.math.BigDecimal" resetType="Group" resetGroup="shipNumber">
		<variableExpression><![CDATA[$V{TotalRevenueReport}.intValue()==0 ? new BigDecimal(0.00):
(
$V{TotalGrossMarginReport}.intValue()==0 ? new BigDecimal(0.00):
(($V{TotalGrossMarginReport}.multiply(new BigDecimal(100.00))).divide($V{TotalRevenueReport},4))
)]]></variableExpression>
	</variable>
	<group name="Third">
		<groupHeader>
			<band>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="0" width="102" height="0" backcolor="#6666FF" uuid="a9bc4c37-fd5b-4916-bb1b-518bd47a287c">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[""]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="0" uuid="f07c50fe-f33f-4bd6-b508-a9fc6c4bfdff">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="END"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[""]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="RG">
		<groupHeader>
			<band>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="0" width="102" height="0" backcolor="#6666FF" uuid="47a5de54-48a1-43a3-9355-687678d16f2b"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[""]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="1">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="1" uuid="f45da545-14c4-45c9-b6f5-5483c912f9c3">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="END"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[""]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="shipNumber">
		<groupExpression><![CDATA[$F{serviceorder_id}]]></groupExpression>
		<groupHeader>
			<band height="18" splitType="Stretch">
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-565" stretchType="RelativeToTallestObject" x="1307" y="0" width="120" height="18" uuid="c5c9e662-cb75-4027-bfc9-9698bd33c24b"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalExpensesPerSo}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" isBlankWhenNull="true">
					<reportElement key="textField-566" stretchType="RelativeToTallestObject" x="1187" y="0" width="120" height="18" uuid="76cc3bdf-9137-4dee-9c2c-911cc60fdc92"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalRevenuePerSo}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-567" stretchType="RelativeToTallestObject" x="1498" y="0" width="50" height="18" printWhenGroupChanges="shipNumber" uuid="1c933ebe-6a7b-4c47-abae-0be076188ae1"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{GrossMarginPercentage}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" isBlankWhenNull="true">
					<reportElement key="textField-568" stretchType="RelativeToTallestObject" x="1427" y="0" width="71" height="18" uuid="054fde56-7eee-4491-8585-824eaa33c938"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalGrossMarginPerSo}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="18" uuid="556b6972-b253-4d75-a1f5-d48744cce3ef">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SO_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-564" stretchType="RelativeToTallestObject" x="1548" y="0" width="180" height="18" uuid="20e636bd-4eaa-44a2-b892-339fb2ac4a5f"/>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{BILL_To_NAME}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="102" y="0" width="152" height="18" uuid="8f9bb099-07ea-47d3-a3f8-985ead632e66">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{REGISTRATION_NUM_SO}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="254" y="0" width="212" height="18" uuid="0d71edff-69f8-45f0-82ff-43846f90e6c9">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{CONTRACT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="531" y="0" width="216" height="18" uuid="9b986194-2701-46a6-86de-780495180349">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{LAST_NAME}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="747" y="0" width="120" height="18" uuid="cf94717c-4804-40ff-913b-2ae576b788ed">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SALESMAN}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch">
				<textField isBlankWhenNull="true">
					<reportElement key="textField-542" x="0" y="0" width="102" height="0" uuid="fde000cb-cef4-43f3-adb7-7a1622990e1d">
						<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="END"/>
					</reportElement>
					<box leftPadding="2">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<textFieldExpression><![CDATA[""]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="25">
			<staticText>
				<reportElement key="staticText-1" x="0" y="0" width="102" height="25" uuid="69b4d1e9-7908-4964-a3a4-86e9b896076c"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SO_NUMBER]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="102" y="0" width="152" height="25" uuid="79739835-4f21-4b44-b425-b6882b72253d"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[REGISTRATION_NUM_SO]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="466" y="0" width="65" height="25" uuid="281cae14-114c-464a-bfe5-3d53b0d3c20d"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[INVOICE #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="747" y="0" width="120" height="25" uuid="138fe963-18b1-40ea-b7f1-67bcd6e75e8e"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SALESMAN]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="867" y="0" width="120" height="25" uuid="832e2912-bfd4-4abc-b9d1-67bc36abc710"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[CHARGE_CODE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="987" y="0" width="200" height="25" uuid="26987869-2203-44c4-ba1c-74a4829469c2"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[CHARGE_CODE_DESCRIPTION]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="1307" y="0" width="120" height="25" uuid="484b7ef5-f43c-44fe-b297-128e6cf11024"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DYNAMIC_EXPENSE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="1187" y="0" width="120" height="25" uuid="f62b3e11-bca8-47d7-96bd-e67d8d562ae4"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DYNAMIC_REVENUE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="254" y="0" width="212" height="25" uuid="37660d0c-256d-428e-92ba-70e55a1dc76d"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[CONTRACT]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="531" y="0" width="216" height="25" uuid="99c629f1-2802-4712-a200-44647a461838"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[LAST_NAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" x="1498" y="0" width="50" height="25" uuid="0b92b722-f6b0-4440-8ac4-01b2dfe28823"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[G.M. %]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" x="1427" y="0" width="71" height="25" uuid="084c43d2-0f60-4464-8050-d3075407a114"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[G.M. $]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" x="1548" y="0" width="180" height="25" uuid="a4bde51d-ee1d-4560-a6df-3422c0173a26"/>
				<box topPadding="3" leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[BILL_To_NAME]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="14" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-550" stretchType="RelativeToTallestObject" x="102" y="0" width="152" height="14" uuid="d8bc635e-3bc6-453e-a5a6-cadb3eedc340">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{REGISTRATION_NUM_SO}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-552" stretchType="RelativeToTallestObject" x="466" y="0" width="65" height="14" uuid="a19895ce-f871-418b-8e17-c320bae21dff">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{INVOICE #}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-553" stretchType="RelativeToTallestObject" x="747" y="0" width="120" height="14" uuid="09c330dd-a180-4761-9328-b5d6c1ce82c6">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SALESMAN}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-554" stretchType="RelativeToTallestObject" x="867" y="0" width="120" height="14" uuid="92d89d3e-dda8-4f9e-966d-df22147298f4">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CHARGE_CODE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-555" stretchType="RelativeToTallestObject" x="987" y="0" width="200" height="14" uuid="14972b5c-bdff-42d5-a23f-745df53f6ebc">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CHARGE_CODE_DESCRIPTION}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-556" stretchType="RelativeToTallestObject" x="1307" y="0" width="120" height="14" uuid="8a5a42c9-e0e3-49e6-956d-46a55e048148">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DYNAMIC_EXPENSE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-559" stretchType="RelativeToTallestObject" x="1187" y="0" width="120" height="14" uuid="e1277786-250f-4c35-b021-1e7fbb89a7db">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DYNAMIC_REVENUE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-560" stretchType="RelativeToTallestObject" x="254" y="0" width="212" height="14" uuid="39059362-e2d2-4933-bb9e-c0d2fff48b0c">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CONTRACT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-561" stretchType="RelativeToTallestObject" x="531" y="0" width="216" height="14" uuid="5b23af8f-ffb3-4b01-b0d7-dc528868522b">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LAST_NAME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-562" stretchType="RelativeToTallestObject" x="1498" y="0" width="50" height="14" uuid="90ea75b8-6cdc-4d3d-8d81-715362519b13">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{G_M_%}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-563" stretchType="RelativeToTallestObject" x="1427" y="0" width="71" height="14" uuid="52ade5dc-48aa-40cd-9cdd-1dd95c0715d9">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{G_M}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-564" stretchType="RelativeToTallestObject" x="1548" y="0" width="180" height="14" uuid="93f5d643-fdd7-4052-bea7-37fedc6ac009">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BILL_To_NAME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-569" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="14" uuid="4e2c6565-ce6c-409f-8102-ded4e39b9529">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.2" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial" size="9" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SO_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="18">
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-565" stretchType="RelativeToTallestObject" mode="Opaque" x="1307" y="0" width="120" height="18" backcolor="#6666FF" uuid="a5b22a56-c398-4d43-b967-924e7e5c1ba3"/>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalExpensesReport}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-567" stretchType="RelativeToTallestObject" mode="Opaque" x="1498" y="0" width="50" height="18" printWhenGroupChanges="shipNumber" backcolor="#6666FF" uuid="2071fb68-4709-4a6a-89c4-e7c3cf490e33"/>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{GrossMarginPercentageReport}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" isBlankWhenNull="true">
				<reportElement key="textField-568" stretchType="RelativeToTallestObject" mode="Opaque" x="1427" y="0" width="71" height="18" backcolor="#6666FF" uuid="4bcb4eff-9120-4af4-aed6-a45c9067b92c"/>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalGrossMarginReport}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="shipNumber" isBlankWhenNull="true">
				<reportElement key="textField-566" stretchType="RelativeToTallestObject" mode="Opaque" x="1187" y="0" width="120" height="18" backcolor="#6666FF" uuid="23b45edb-c92d-4f63-83c2-5c527b40b535"/>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalRevenueReport}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-569" stretchType="RelativeToTallestObject" mode="Opaque" x="0" y="0" width="102" height="18" backcolor="#6666FF" uuid="9e1c680a-75ef-4a29-b0d8-2a7dc3c50979">
					<property name="net.sf.jasperreports.export.xls.row.outline.level.1" value="Body"/>
				</reportElement>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand Total"]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
