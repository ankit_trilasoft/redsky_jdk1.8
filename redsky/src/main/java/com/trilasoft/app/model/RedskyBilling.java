package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="redskybilling")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class RedskyBilling extends BaseObject   {
	private String corpID; 
	private Long id;
	private String companyDivision;
	private String shipNumber;
	private String lastName;
	private String firstName;
	private String job;
	private String billToName;
	private Date redskyBillDate;
	private String createdBy; 
	private Date createdOn; 
	private String updatedBy; 
	private Date updatedOn;
	private Date packingA;
	private Date loadA;
	private Date deliveryA;
	private BigDecimal billingRate;
	private String routing;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("companyDivision", companyDivision).append(
				"shipNumber", shipNumber).append("lastName", lastName).append(
				"firstName", firstName).append("job", job).append("billToName",
				billToName).append("redskyBillDate", redskyBillDate).append(
				"createdBy", createdBy).append("createdOn", createdOn).append(
				"updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("packingA",packingA)
				.append("loadA",loadA)
				.append("deliveryA",deliveryA)
				.append("billingRate",billingRate)
				.append("routing",routing)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RedskyBilling))
			return false;
		RedskyBilling castOther = (RedskyBilling) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id)
				.append(companyDivision, castOther.companyDivision).append(
						shipNumber, castOther.shipNumber).append(lastName,
						castOther.lastName).append(firstName,
						castOther.firstName).append(job, castOther.job).append(
						billToName, castOther.billToName).append(
						redskyBillDate, castOther.redskyBillDate).append(
						createdBy, castOther.createdBy).append(createdOn,
						castOther.createdOn).append(updatedBy,
						castOther.updatedBy)
						.append(updatedOn,castOther.updatedOn)
						.append(packingA,castOther.packingA)
						.append(loadA,castOther.loadA)
						.append(deliveryA,castOther.deliveryA)
							.append(billingRate,castOther.billingRate)
							.append(routing, castOther.routing)
						.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				companyDivision).append(shipNumber).append(lastName).append(
				firstName).append(job).append(billToName)
				.append(redskyBillDate).append(createdBy).append(createdOn)
				.append(updatedBy).append(updatedOn)
				.append(packingA)
				.append(loadA)
				.append(deliveryA)
				.append(billingRate)
				.append(routing)
				.toHashCode();
	}
	
	@Column(length = 255)
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	
	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length = 80)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length = 3)
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	
	@Column(length = 80)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column
	public Date getRedskyBillDate() {
		return redskyBillDate;
	}
	public void setRedskyBillDate(Date redskyBillDate) {
		this.redskyBillDate = redskyBillDate;
	}
	
	@Column(length = 15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getPackingA() {
		return packingA;
	}
	public void setPackingA(Date packingA) {
		this.packingA = packingA;
	}
	@Column
	public Date getLoadA() {
		return loadA;
	}
	public void setLoadA(Date loadA) {
		this.loadA = loadA;
	}
	@Column
	public Date getDeliveryA() {
		return deliveryA;
	}
	public void setDeliveryA(Date deliveryA) {
		this.deliveryA = deliveryA;
	}
	
	@Column
	public BigDecimal getBillingRate() {
		return billingRate;
	}
	public void setBillingRate(BigDecimal billingRate) {
		this.billingRate = billingRate;
	}
	@Column
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}


}
