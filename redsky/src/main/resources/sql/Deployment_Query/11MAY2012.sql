ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `bookingAgentExSO` VARCHAR(25) DEFAULT NULL AFTER `agentNetworkGroup`;
ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `serviceCompleteDate` DATETIME DEFAULT NULL; 
ALTER TABLE `redsky`.`resourcegrid` MODIFY COLUMN `resourceLimit` DECIMAL(19,2);

ALTER TABLE `redsky`.`standardaddresses` MODIFY COLUMN `countryCode` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`subcontractorcharges` MODIFY COLUMN `companyDivision` VARCHAR(10)   DEFAULT NULL;

ALTER TABLE `redsky`.`refzipgeocodemap` MODIFY COLUMN `country` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '';
ALTER TABLE `redsky`.`billing` MODIFY COLUMN `insuranceOption` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `insuranceOptionCodeWithDesc` VARCHAR(150) DEFAULT NULL;

ALTER TABLE `redsky`.`contract` ADD COLUMN `dmmInsurancePolicy` bit(1) DEFAULT 'b'1'';
update contract set dmmInsurancePolicy =true ;

SELECT max(columnsequencenumber) FROM extractcolumnmgmt where reportname='dynamicFinancialSummary';
 
insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('SSCW', 'adminsscw', now(), 'adminsscw', now(), 'dynamicFinancialSummary', 'trackingstatus',"date_format(t.serviceCompleteDate,'%d-%b-%y') as SERVICE_COMPLETE_DATE", 'SERVICE_COMPLETE_DATE', 172);

SELECT max(columnsequencenumber) FROM extractcolumnmgmt where reportname='dynamicFinancialDetails';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('SSCW', 'adminsscw', now(), 'adminsscw', now(), 'dynamicFinancialDetails', 'billing',"b.insuranceValueActual as INSURANCE_VALUE_ACTUAL", 'INSURANCE_VALUE_ACTUAL', 178);

SELECT max(columnsequencenumber) FROM extractcolumnmgmt where reportname='shipmentAnalysis';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('SSCW', 'adminsscw', now(), 'adminsscw', now(), 'shipmentAnalysis', 'trackingstatus',"date_format(t.serviceCompleteDate,'%d-%b-%y') as SERVICE_COMPLETE_DATE", 'SERVICE_COMPLETE_DATE', 1749);

ALTER TABLE `redsky`.`mss` ADD COLUMN `companyDivision` VARCHAR(10) DEFAULT NULL AFTER `location`;

update todorule set expression=replace(expression,'trackingstatus.deliveryA is null','(trackingstatus.deliveryA is null or trackingstatus.serviceCompleteDate is null)')
where expression like '%trackingstatus.deliveryA is null%';
 

