package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RuleActions;

public interface RuleActionsDao extends GenericDao<RuleActions, Long>{
    
	public List getFilterName(String sessionCorpID ,Long userId);
	public List getActionList(Long ruleId) ;
	public <RuleActions> List getToDoRuleByRuleNumber(Long ruleId, String corpId);
	public void updateToDoRole(String messageDisplayed,String roleList, Long id,String corpId);
	public void updateStatus(String activestatus,Long ruleId,Long id);
}

