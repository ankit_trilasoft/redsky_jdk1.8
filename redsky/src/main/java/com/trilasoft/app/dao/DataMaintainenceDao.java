package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataMaintainence;

public interface DataMaintainenceDao extends GenericDao<DataMaintainence, Long>{
	public List executeQuery(String query );
	public String bulkUpdate(String query);

}
