package com.trilasoft.app.model;

public class PricingDetailsValue {
	 private Object rate;
	 private Object type;
	 private Object basis;
	 private Object agentId;
	public Object getRate() {
		return rate;
	}
	public void setRate(Object rate) {
		this.rate = rate;
	}
	public Object getType() {
		return type;
	}
	public void setType(Object type) {
		this.type = type;
	}
	public Object getBasis() {
		return basis;
	}
	public void setBasis(Object basis) {
		this.basis = basis;
	}
	public Object getAgentId() {
		return agentId;
	}
	public void setAgentId(Object agentId) {
		this.agentId = agentId;
	}
}
