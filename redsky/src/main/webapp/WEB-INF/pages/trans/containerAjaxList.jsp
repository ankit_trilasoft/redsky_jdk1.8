<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.net.URLEncoder"%>
<head>   
    <title><fmt:message key="containerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='containerList.heading'/>"/> 
    <style type="text/css">
/* collapse */

span.pagelinks 
{display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:-1px;
margin-top:-31px;
!margin-top:-28px;
padding:2px 0px;
width:100%;
}

@media screen and (-webkit-min-device-pixel-ratio:0) {
.table td, .table th, .tableHeaderTable td{ height:5px }
}
} 
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
 .hidden {
 display: none;
} 
</style> 
<script>
 <sec-auth:authComponent componentId="module.script.form.agentScript">
	window.onload = function() { 
	 trap();
		<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm1" transIdList='${serviceOrder.shipNumber}'> 
		</sec-auth:authScript> 
		}
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript"> 
	window.onload = function() {  
		//trap(); 
	}
</sec-auth:authComponent> 
 function trap() { 
		  if(document.images)   { 
		    	for(i=0;i<document.images.length;i++)
		      { 
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}    }    }   }
		  
 function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="0";	        
	        return false;
	        }	       
	    }	 
	    return true;
	}
 
</script>

</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" id="noteFor" value="ServiceOrder"></s:hidden>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 
  <s:hidden name="shipNumber" id ="fileID" value="%{serviceOrder.shipNumber}" />
  <s:hidden name="regNumber" id ="fileID" value="%{serviceOrder.registrationNumber}" />
  <s:hidden name="packingMode" id ="fileID" value="%{serviceOrder.packingMode}" />
  <c:set var="shipNumber" value="${serviceOrder.shipNumber}" scope="session"/>
  <c:set var="packingMode" value="${serviceOrder.packingMode}" scope="session"/>
  <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
  <c:set var="buttons"> 
     <input type="button" class="cssbutton1" onclick="window.open('editContainerAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=550,width=900,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Add" style="width:60px; height:25px;margin-bottom:5px;" /> 
          
     <input id="refresh" type="button" class="cssbuttonA" style="width:110px; height:25px;margin-bottom:5px;" 
	      onclick="refreshWeights('click');" value="<fmt:message key="button.refreshWeights"/>"/>  
</c:set> 
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="mode" value="%{serviceOrder.mode}" />

<s:set name="containers" value="containers" scope="request"/>

<display:table name="containers" class="table" requestURI="" id="containerList" export="false" defaultsort="1" style="width:100%;margin:2px 0 10px;">   
 <display:column sortProperty="idNumber" titleKey="container.idNumber" style="width:20px">
 <sec-auth:authComponent componentId="module.link.container.id"> 
 	<a href="javascript:openWindow('editContainerAjax.html?id=${containerList.id}&decorator=popup&popup=true',900,550)" style="cursor:pointer"> 
 </sec-auth:authComponent>
 <c:out value="${containerList.idNumber}" /></a></display:column>  
 <display:column  titleKey="container.containerNumber" style="width:70px">
 	<input type="text" class="input-text" style="text-align:left;width:150px" maxlength="20" id="containerNumber${containerList.id}" name="container.containerNumber" onchange="updateContainerContainerNumber('${containerList.id}','containerNumber',this,'Container','${containerList.shipNumber}','${containerList.containerId}');"  value="${containerList.containerNumber}"/>
 </display:column>
 <display:column  titleKey="container.sealNumber" style="width:70px">
 	<input type="text" class="input-textUpper" style="text-align:left;width:120px" name="container.sealNumber" readonly="true" maxlength="14" onchange="updateContainerDetails('${containerList.id}','sealNumber',this,'Container');" value="${containerList.sealNumber}"/>
 </display:column>
 
<display:column   titleKey="container.size" style="width:10px;text-align:left">
	 <select name ="container.size" style="width:90px" onchange="updateContainerSizeDetails('${containerList.id}','size',this,'Container');" class="list-menu">
					<option value="<c:out value='' />">
						<c:out value=""></c:out>
						</option> 	
					<c:set target="${EQUIP}" property="${containerList.size }" value="${containerList.size }"/>
			      <c:forEach var="chrms" items="${EQUIP}" varStatus="loopStatus">
			           <c:choose>
		                       <c:when test="${chrms.key == containerList.size}">
		                        <c:set var="selectedInd" value=" selected"></c:set>
		                        </c:when>
		                       	<c:otherwise>
		                 		<c:set var="selectedInd" value=""></c:set>
		                  		</c:otherwise>
	                            </c:choose>
			      	<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                    <c:out value="${chrms.value}"></c:out>
			                    </option>
				</c:forEach>		      
		</select>
 </display:column>
 
 <c:if test="${UnitType==true}">
  <display:column headerClass="containeralign"   title="Gross Weight" style="width:80px;text-align:right;padding-right: 0.3em;">
  	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.grossWeight" id="congrossWeight${containerList.id}" onchange="onlyFloat(this);calcNetWeightCon('${containerList.id}','${containerList.unit1}','${containerList.unit2}')" value="<c:out value="${containerList.grossWeight}"/>" 
 </display:column>
   <display:column headerClass="hidden"   title="Gross Wt Kilo" style="width:50px" class="hidden">
  	<input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="container.grossWeightKilo" id="congrossWeightKilo${containerList.id}" value="<c:out value="${containerList.grossWeightKilo}"/>" 
 </display:column>
 <display:column headerClass="containeralign"  titleKey="container.emptyContWeight" style="width:70px;text-align:right;padding-right: 0.3em;"> 
  <input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.emptyContWeight" id="conemptyContWeight${containerList.id}" onchange="onlyFloat(this);calcNetWeightCon('${containerList.id}','${containerList.unit1}','${containerList.unit2}')" value="<c:out value="${containerList.emptyContWeight}"/>"
  </display:column>
   <display:column headerClass="hidden"  title="Tare Wt Kilo" style="width:50px" class="hidden">
	<input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="container.emptyContWeightKilo" id="conemptyContWeightKilo${containerList.id}" value="<c:out value="${containerList.emptyContWeightKilo}"/>"
</display:column>
 <display:column headerClass="containeralign"  titleKey="container.netWeight" style="width:70px;text-align:right;padding-right: 0.3em;">
 	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.netWeight" id="connetWeight${containerList.id}" onchange="onlyFloat(this);calculateTareCon('${containerList.id}','${containerList.unit1}','${containerList.unit2}');" value="<c:out value="${containerList.netWeight}"/>" 
 </display:column>
  <display:column headerClass="hidden"  title="Net Wt Kilo" style="width:50px" class="hidden" >
	 <input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="container.netWeightKilo" id="connetWeightKilo${containerList.id}" value="<c:out value="${containerList.netWeightKilo}"/>" 
 </display:column>
  </c:if>
 <c:if test="${UnitType==false}">
  <display:column headerClass="containeralign"   title="Gross Weight" style="width:80px;text-align:right;padding-right: 0.3em;">
  	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.grossWeightKilo" id="congrossWeightKilo${containerList.id}" onchange="onlyFloat(this);calcNetWeightKiloCon('${containerList.id}','${containerList.unit1}','${containerList.unit2}')" value="<c:out value="${containerList.grossWeightKilo}"/>" 
 </display:column>
   <display:column headerClass="hidden"   title="Gross Wt" style="width:80px" class="hidden">
  	<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.grossWeight" id="congrossWeight${containerList.id}" value="<c:out value="${containerList.grossWeight}"/>" 
 </display:column>
 <display:column headerClass="containeralign"  titleKey="container.emptyContWeight" style="width:70px;text-align:right;padding-right: 0.3em;">
	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.emptyContWeightKilo" id="conemptyContWeightKilo${containerList.id}" onchange="onlyFloat(this);calcNetWeightKiloCon('${containerList.id}','${containerList.unit1}','${containerList.unit2}')" value="<c:out value="${containerList.emptyContWeightKilo}"/>"
</display:column>
 <display:column headerClass="hidden"  title="Tare Wt" style="width:70px" class="hidden"> 
  <input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.emptyContWeight" id="conemptyContWeight${containerList.id}" value="<c:out value="${containerList.emptyContWeight}"/>"
  </display:column>
 <display:column headerClass="containeralign"  titleKey="container.netWeight" style="width:70px;text-align:right;padding-right: 0.3em;">
	 <input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.netWeightKilo" id="connetWeightKilo${containerList.id}" onchange="onlyFloat(this);calcTareKiloCon(${containerList.id});" value="<c:out value="${containerList.netWeightKilo}"/>" 
 </display:column>
  <display:column headerClass="hidden"  title="Net Wt" style="width:70px" class="hidden">
 	<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.netWeight" id="connetWeight${containerList.id}" value="<c:out value="${containerList.netWeight}"/>" 
 </display:column>
  </c:if>
  <c:if test="${VolumeType==true}">
 <display:column  headerClass="containeralign"  titleKey="container.volume" style="width:70px;text-align:right;padding-right: 0.3em;">
 	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.volume" id="convolume${containerList.id}" onchange="onlyFloat(this);calculateVolumeDensity('${containerList.id}','${containerList.unit1}','${containerList.unit2}');" value="<c:out value="${containerList.volume}"/>"
</display:column>
 <display:column  headerClass="hidden"  title="Volumn Cbm" style="width:40px" class="hidden">
	<input type="hidden" class="input-text" style="text-align:right;width:40px" maxlength="10" name="container.volumeCbm" id="convolumeCbm${containerList.id}" onchange="onlyFloat(this);" value="<c:out value="${containerList.volumeCbm}"/>"                  
</display:column>
<display:column headerClass="containeralign"  titleKey="container.density" style="width:70px;text-align:right;padding-right: 0.3em;">
	<input type="text" class="input-textUpper" style="text-align:right;width:100px" maxlength="5" readonly="true" name="container.density" id="condensity${containerList.id}" onchange="onlyFloat(this);" value="<c:out value="${containerList.density}"/>"
</display:column>
 <display:column headerClass="hidden"  title="Density Metric" style="width:30px" class="hidden">
 	<input type="hidden" class="input-textUpper" style="text-align:right;width:30px" maxlength="5" readonly="true" name="container.densityMetric" id="condensityMetric${containerList.id}" onchange="onlyFloat(this);" value="<c:out value="${containerList.densityMetric}"/>"
 </display:column>
 </c:if>
 <c:if test="${VolumeType==false}">
 <display:column  headerClass="containeralign"  titleKey="container.volume" style="width:70px;text-align:right;padding-right: 0.3em;">
	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.volumeCbm" id="convolumeCbm${containerList.id}" onchange="onlyFloat(this);calculateDensityMetric('${containerList.id}','${containerList.unit1}','${containerList.unit2}');" value="<c:out value="${containerList.volumeCbm}"/>"                  
</display:column>
 <display:column  headerClass="hidden"  titleKey="container.volume" style="width:70px" class="hidden">
 	<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="container.volume" id="convolume${containerList.id}" onchange="" value="<c:out value="${containerList.volume}"/>"
</display:column>
 <display:column headerClass="containeralign"  titleKey="container.density" style="width:70px;text-align:right;padding-right: 0.3em;">
 	<input type="text" class="input-textUpper" style="text-align:right;width:100px" maxlength="5" readonly="true" name="container.densityMetric" id="condensityMetric${containerList.id}" onchange="" value="<c:out value="${containerList.densityMetric}"/>"
 </display:column>
 <display:column headerClass="hidden"  titleKey="container.density" style="width:70px" class="hidden">
	<input type="hidden" class="input-textUpper" style="text-align:right;width:100px" maxlength="5" readonly="true" name="container.density" id="condensity${containerList.id}" onchange="" value="<c:out value="${containerList.density}"/>"
</display:column>
 </c:if>
 
 <display:column  titleKey="container.pieces" headerClass="containeralign" style="width:10px;text-align:right;padding-right: 0.3em;">
	<input type="text" class="input-text" style="text-align:right;width:70px" maxlength="4" name="container.pieces" onchange="isNumeric(this);updateContainerDetails('${containerList.id}','pieces',this,'Container');" value="<c:out value="${containerList.pieces}"/>"
</display:column>
<sec-auth:authComponent componentId="module.tab.container.auditTab">
   <display:column title="Audit" style="width:25px; text-align: center;">
   	<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${containerList.id}&tableName=container&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
   </display:column>
  </sec-auth:authComponent>
  
<display:column title="Status" style="width:45px; text-align: center;">
  <c:choose>
   <c:when test="${containerList.containerId != null && (serviceOrder.grpStatus=='Draft' || serviceOrder.grpStatus=='Finalized')}">
      <c:if test="${containerList.status}">
          <input type="checkbox"  checked="checked" onclick="alert('Deactivation is not allowed as this service order is a part of a groupage order.');" disabled="disabled"/>
        </c:if>
       <c:if test="${containerList.status==false}">
		<input type="checkbox"   onclick="alert('Deactivation is not allowed as this service order is a part of a groupage order.');" disabled="disabled"/>
       </c:if>    
   </c:when>
   <c:otherwise>
  <c:if test="${containerList.status}">
        <input type="checkbox" name="status${containerList.id}" id="status${containerList.id}"  checked="checked" onclick="deleteContainerDetails(${containerList.id},this);" />
        </c:if>
        <c:if test="${containerList.status==false}">
		<input type="checkbox" name="status${containerList.id}" id="status${containerList.id}" onclick="deleteContainerDetails(${containerList.id},this);" />
   </c:if>
   </c:otherwise>
   </c:choose>     </display:column>
    
    <display:setProperty name="paging.banner.item_name" value="container"/>  
    <display:setProperty name="paging.banner.items_name" value="container"/>
  
    <display:setProperty name="export.excel.filename" value="Container List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Container List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Container List.pdf"/>   
<display:footer >
            <tr > 
 	  	          <td align="right"  colspan="4"><b><div align="right"><fmt:message key="container.total"/></div></b></td>
		          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.grossWeightTotal}" /></div></td>
                  <td></td>
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.totalNetWeight}" /></div></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.totalVolume}" /></div></td>
		          <td></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${container.totalPieces}" /></div></td> 
		  	      <td></td> 
		  	      <td></td>
		  	    
           </tr>
</display:footer>
</display:table>  
	<s:hidden name="id"></s:hidden>	 
	<sec-auth:authComponent componentId="module.button.container.addButton"> 
   <c:out value="${buttons}" escapeXml="false" /> 
     </sec-auth:authComponent>
     </div>
     <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
    <c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
     <c:set var="tableName" value="serviceorder" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if> 
<s:hidden name="serviceOrder.grpID"/>  
<s:hidden name="serviceOrder.grpStatus"/>

<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script> 

<script> 
		  function right(e) {  
		if (navigator.appName == 'Netscape' && e.which == 1) { 
		return false;
		} 
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) { 
		return false;
		} 
		else return true;
		}
function goToCustomerDetail(targetValue){
        document.forms['serviceForm1'].elements['id'].value = targetValue;
        document.forms['serviceForm1'].action = 'editContainer.html?from=list';
        document.forms['serviceForm1'].submit();
} 
function refreshContainer() {
	showOrHide(1);
	 var shipNum= '${serviceOrder.shipNumber}';
	 url="refreshContainer.html?ajax=1&decorator=simple&popup=true&containerNos="+encodeURI(containerNos)+"&shipNum="+encodeURI(shipNum);
	 http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse5;
     http5.send(null);
}
function handleHttpResponse5()    {
    if (http5.readyState == 4) {
         var results = http5.responseText
         results = results.trim(); 
         document.getElementById('refresh').disabled = false;
         getContainerDetails();
         showOrHide(0);
 }
}
function refreshWeights1() {
		var packingMode= '${serviceOrder.packingMode}';
		if(packingMode !='LVCO'){ 
			 } else {
			 refreshWeights("onload");
			 }
} 
function refreshWeights(check) {

	 var shipNum= '${serviceOrder.shipNumber}';
	 var packingMode= '${serviceOrder.packingMode}';

		if((packingMode !='LVCO') && (check != 'onload' || check == 'click')){
		 alert("No Weights to be refreshed");
			 } else {
	 var url="refreshWeights.html?ajax=1&decorator=simple&popup=true&shipNum="+encodeURI(shipNum)+"&packingMode="+encodeURI(packingMode); 
	 http66.open("GET", url, true);
     http66.onreadystatechange = function() {handleHttpResponse4444(check)};
     http66.send(null);
}
}	
function handleHttpResponse4444(check) {
           if (http66.readyState == 4){
                var results = http66.responseText
                results = results.trim();
                var  res1 = results.replace("[",'');
                var res2 = res1.replace("]",'');
                var res = res2.split(","); 
                if(res.length >=1 && res !=''){
                	document.getElementById('refresh').disabled = false;
					if(check != 'onload' || check == 'click'){					
                	var agree=confirm("Are you sure you want to overwrite container info?");
					if (agree){
					for(i=0;i<res.length;i++){
 			 		 containerNos = res[i];
 					 refreshContainer(containerNos);
 					}
					}else{ 
					return false;
					}
					}
                    } else { 
                 document.getElementById('refresh').disabled = true;
			}  } }

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	var sid = document.forms['serviceForm1'].elements['serviceOrder.id'].value;	
	     location.href = "updateDeleteStatus.html?id="+encodeURI(targetElement)+"&sid="+sid;
     }else{
		return false;
	}
}  

var http66 = getHTTPObject();
var http5 = getHTTPObject();

function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function updateContainerSizeDetails(id,fieldName,target,tableName){
	var fieldValue ="";		
	fieldValue = target.value;	
	if(fieldValue==undefined){
		fieldValue = target;
	}	
	if((fieldName=='size') && (fieldValue=='')){
		var blankFieldname = '';
			blankFieldname = 'Size';			
		alert("Please fill value for "+" "+blankFieldname);
		getContainerDetails();
	}else if((fieldName=='size') && (fieldValue!='')){
		 var url="findNewVolume.html?ajax=1&decorator=simple&popup=true&containerSize="+encodeURI(fieldValue);
		   httpNewVolume.open("GET",url,true);
		   httpNewVolume.onreadystatechange= function(){httpResponseNewVolume(id,fieldName,fieldValue,tableName)};
		   httpNewVolume.send(null);		
	}	
}

var http007 = getHTTPObject();

function httpResponseNewVolume(id,fieldName,fieldValue,tableName){
	if(httpNewVolume.readyState==4){
     var result=httpNewVolume.responseText
     var usedVol='${usedVolume}';	     
     result=result.trim();	    
	     if(usedVol!=''){
	     	if(result!='' && result!='0'){
	       		var total=parseFloat(result)-parseFloat(usedVol);
	       		total=parseFloat(total).toFixed(2);
			       if(total<0){
				       alert("You cannot change the container size as used volume exceeds avaliable volume.");
				       	getContainerDetails();
			       }
	      		}else{
		       			alert("You cannot change the container size as used volume exceeds avaliable volume.");
		       			getContainerDetails();
	      		}
	      		}else{
	      			var url='';
	    			if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
	    				var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
	    				if(agree){
	      					var url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&updateRecords="+updateRecords;
	    				}else{
	   					 	url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
	    				}
	    			}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (contId!='0' && contId!='')){
						alert("Update is not allowed as this service order is a part of a groupage order.");
						getContainerDetails();
					}else{
						 url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
					}
	      		  http007.open("GET", url, true); 
	      		  http007.onreadystatechange = handleHttpResponse007;
	      		  http007.send(null);
	      	}	     
	   	}
	}
 function handleHttpResponse007(){
		if (http007.readyState == 4){		
	            var results = http007.responseText
	            results = results.trim();           
		}
}
var httpNewVolume = getHTTPObject();
var soGrpId = '${serviceOrder.grpID}';
var soControlFlag = '${serviceOrder.controlFlag}';
var soGrpStatus = '${serviceOrder.grpStatus}';
var updateRecords = 'updateAll';
function updateContainerContainerNumber(id,fieldName,target,tableName,shipNumber,contId){
	showOrHide(1);
	var url ='';
	var fieldValue ="";		
	fieldValue = target.value;		
	if(fieldValue==undefined){
		fieldValue = target;
	}
	var containerValidationVal = validateSSContainer(id);
	if(containerValidationVal){
		if((fieldName=='containerNumber') && (fieldValue=='')){
			var blankFieldname = '';		
				blankFieldname='SS Container #';			
			alert("Please fill value for "+" "+blankFieldname);
			getContainerDetails();
		}else{
			if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
				var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
				if(agree){
					 url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&shipNumber="+shipNumber+"&updateRecords="+updateRecords;
				}else{
					 url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&shipNumber="+shipNumber;
				}
			}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (contId!='0' && contId!='')){
					alert("Update is not allowed as this service order is a part of a groupage order.");
					getContainerDetails();
			}else{
					 url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&shipNumber="+shipNumber;
			}
		  http120.open("GET", url, true); 
		  http120.onreadystatechange = handleHttpResponse120;
		  http120.send(null);
		}
	}else{
		getContainerDetails();
	}
}

function handleHttpResponse120(){
	if (http120.readyState == 4){		
            var results = http120.responseText
            getContainerDetails();
            getCartonDetails();
            getVehicleDetails();
            getRoutingDetails();
            showOrHide(0);
	}
}
var http120 = getHTTPObject();

function updateContainerDetails(id,fieldName,target,tableName){
	showOrHide(1);
	var fieldValue ="";		
	fieldValue = target.value;		
	if(fieldValue==undefined){
		fieldValue = target;
	}
		var url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+encodeURIComponent(fieldValue)+"&tableName="+tableName;
			  http20.open("GET", url, true); 
			  http20.onreadystatechange = handleHttpResponse20;
			  http20.send(null);
		
}
function handleHttpResponse20(){
	if (http20.readyState == 4){		
            var results = http20.responseText
            results = results.trim();            
            getContainerDetails();            
            showOrHide(0);                      
	}
}
var http20 = getHTTPObject();

function refreshWindow(){	
	window.location.reload(true);
}
function calcNetWeightCon(id,unit1,unit2){
	   var Q1 = eval(document.getElementById('congrossWeight'+id).value);
	   var Q2 = eval(document.getElementById('conemptyContWeight'+id).value);
	   var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getContainerDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateContainerDetails115(id,Q1,Q2,unit1,unit2)
		}else if((Q1>Q2) ||Q2 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getContainerDetails();
		}
		 
	}
function calculateTareCon(id,unit1,unit2){	
	   var Q1 = eval(document.getElementById('congrossWeight'+id).value);
	   var Q2 = eval(document.getElementById('connetWeight'+id).value);
	   var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Net weight");
			getContainerDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateContainerDetails333(id,Q1,Q2,unit1,unit2);			
		}else if((Q1>Q2) ||Q2 == undefined){
			alert("Gross weight should be greater than Net weight");
			getContainerDetails();
		}
		 
	}
var fieldVolume = 'volume';
function calculateVolumeDensity(id,unit1,unit2){
	showOrHide(1);
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolume'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	if(weightUnitKgs=='Kgs') {
	     netWeight=netWeight*2.2046;
	     grossWeight=grossWeight*2.2046;
	  }
	   if(volumeUnitCbm=='Cbm'){
	     volume=volume*35.3147;
	   }
	   
	   var E11=volume*0.0283;
	   var E22=Math.round(E11*10000)/10000;
	   var fieldVolumeCbmVal = document.getElementById('convolumeCbm'+id).value = E22;
	  var fieldVolumeVal = document.getElementById('convolume'+id).value;
	   var fieldDensityVal = 0;
	   var fieldDensityMetricVal = 0;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{ 
			densityMetric=Math.round((grossWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}
	
	var url = "updateContainerDetailsDensityAjax.html?ajax=1&id="+id+"&fieldVolume="+fieldVolume+"&fieldVolumeVal="+fieldVolumeVal+"&fieldVolumeCbm="+fieldVolumeCbm+"&fieldVolumeCbmVal="+fieldVolumeCbmVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
	
		httpDensity.open("GET", url, true); 
		httpDensity.onreadystatechange = function(){handleHttpResponseDensity(id)};
		httpDensity.send(null);	
}
function handleHttpResponseDensity(id){
	if (httpDensity.readyState == 4){		
            var results = httpDensity.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var httpDensity = getHTTPObject();

function calcNetWeightKiloCon(id,unit1,unit2){
	var Q1 = eval(document.getElementById('congrossWeightKilo'+id).value);
	var Q2 = eval(document.getElementById('conemptyContWeightKilo'+id).value);
	var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getContainerDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateContainerNetWtKilo11(id,Q1,Q2,unit1,unit2);
		}else if((Q1>Q2) ||Q2 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getContainerDetails();
		}	   
}
function calcTareKiloCon(id){
	var Q1 = eval(document.getElementById('congrossWeightKilo'+id).value);
	var Q2 = eval(document.getElementById('connetWeightKilo'+id).value);
	var E1='';
	if((Q2>Q1) || Q1 == undefined){
		alert("Gross weight should be greater than Net weight");
		getContainerDetails();
	}else if(Q1 != undefined && Q2 != undefined){		
		updateContainerTareWtKilo(id,Q1,Q2,unit1,unit2);
	}else if((Q1>Q2) || Q2 == undefined){
		alert("Gross weight should be greater than Net weight");
		getContainerDetails();
	}
}
function calculateDensityMetric(id,unit1,unit2){
	showOrHide(1);
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolumeCbm'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	
	var E1=volume*35.3147;
	   var E2=Math.round(E1*10000)/10000;
	   fieldVolumeVal = document.getElementById('convolume'+id).value = E2;
	   var fieldVolumeCbmVal = document.getElementById('convolumeCbm'+id).value;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((grossWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}
	
	var url = "updateContainerDetailsDensityMetricAjax.html?ajax=1&id="+id+"&fieldVolume="+fieldVolume+"&fieldVolumeVal="+fieldVolumeVal+"&fieldVolumeCbm="+fieldVolumeCbm+"&fieldVolumeCbmVal="+fieldVolumeCbmVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
				
	  httpDensityMetricContainer.open("GET", url, true); 
	  httpDensityMetricContainer.onreadystatechange = function(){handleHttpResponseDensityMetricContainer(id)};
	  httpDensityMetricContainer.send(null);
}

function handleHttpResponseDensityMetricContainer(id){
	if (httpDensityMetricContainer.readyState == 4){		
            var results = httpDensityMetricContainer.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var httpDensityMetricContainer = getHTTPObject();

var tableName = 'Container';
var fieldNetWeight = 'netWeight';
var fieldGrossWeight = 'grossWeight';
var fieldEmptyConWeight ='emptyContWeight';
var fieldNetWeightKilo ='netWeightKilo';
var fieldGrossWeightKilo = 'grossWeightKilo';
var fieldemptyContWeightKilo = 'emptyContWeightKilo';
var fieldVolumeCbm = 'volumeCbm';
var fieldDensity = 'density';
var fieldDensityMetric = 'densityMetric';

function updateContainerDetails115(id,Q1,Q2,unit1,unit2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	
	var fieldNetWeightVal = document.getElementById('connetWeight'+id).value=E2;
	
	var fieldGrossWeightVal = Q1;
	
	var fieldEmptyConWeightVal = Q2;
	var E3 = E1*0.4536;
	var E4=Math.round(E3*10000)/10000;
	
	var fieldNetWeightKiloVal = document.getElementById('connetWeightKilo'+id).value=E4;
	var Q3 = Q1*0.4536;
	var Q4=Math.round(Q3*10000)/10000;
	
	var fieldGrossWeightKiloVal =document.getElementById('congrossWeightKilo'+id).value=Q4;
	var Q5 = Q2*0.4536;
	var Q6=Math.round(Q5*10000)/10000;
	
	var fieldemptyContWeightKiloVal =document.getElementById('conemptyContWeightKilo'+id).value=Q6;
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolume'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	if(weightUnitKgs=='Kgs') {
	     netWeight=netWeight*2.2046;
	     grossWeight=grossWeight*2.2046;
	  }
	   if(volumeUnitCbm=='Cbm'){
	     volume=volume*35.3147;
	   }
	   
	   var E11=volume*0.0283;
	   var E22=Math.round(E11*10000)/10000;
	   var fieldVolumeCbmVal = document.getElementById('convolumeCbm'+id).value = E22;
	  
	   var fieldDensityVal = 0;
	   var fieldDensityMetricVal = 0;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{ 
			densityMetric=Math.round((grossWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}
	
	var url = "updateContainerDetailsNetWtdAjax.html?ajax=1&id="+id+"&fieldNetWeight="+fieldNetWeight+"&fieldNetWeightVal="+fieldNetWeightVal+"&fieldGrossWeight="+fieldGrossWeight+"&fieldGrossWeightVal="+fieldGrossWeightVal+"&fieldEmptyConWeight="+fieldEmptyConWeight+"&fieldEmptyConWeightVal="+fieldEmptyConWeightVal
			+"&fieldNetWeightKilo="+fieldNetWeightKilo+"&fieldNetWeightKiloVal="+fieldNetWeightKiloVal+"&fieldGrossWeightKilo="+fieldGrossWeightKilo+"&fieldGrossWeightKiloVal="+fieldGrossWeightKiloVal+"&fieldemptyContWeightKilo="+fieldemptyContWeightKilo+"&fieldemptyContWeightKiloVal="+fieldemptyContWeightKiloVal
			+"&fieldVolumeCbm="+fieldVolumeCbm+"&fieldVolumeCbmVal="+fieldVolumeCbmVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
		http2005.open("POST", url, true); 
	  	http2005.onreadystatechange = function(){handleHttpResponse2005(id)};
	  	http2005.send(null);	 
	}
function handleHttpResponse2005(id){
	if (http2005.readyState == 4){		
            var results = http2005.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var http2005 = getHTTPObject();

function updateContainerDetails333(id,Q1,Q2,unit1,unit2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	var fieldEmptyConWeightVal = document.getElementById('conemptyContWeight'+id).value=E2;
	var fieldNetWeightVal = Q2;
	var E3 = E1*0.4536;
	var E4=Math.round(E3*10000)/10000;
	var fieldemptyContWeightKiloVal = document.getElementById('conemptyContWeightKilo'+id).value=E4;
	var E5 = Q2*0.4536;
	var E6=Math.round(E5*10000)/10000;
	var fieldNetWeightKiloVal = document.getElementById('connetWeightKilo'+id).value=E6;
	
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolume'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	if(weightUnitKgs=='Kgs') {
	     netWeight=netWeight*2.2046;
	     grossWeight=grossWeight*2.2046;
	  }
	   if(volumeUnitCbm=='Cbm'){
	     volume=volume*35.3147;
	   }
	   
	   var E11=volume*0.0283;
	   var E22=Math.round(E11*10000)/10000;
	   var fieldVolumeCbmVal = document.getElementById('convolumeCbm'+id).value = E22;
	  
	   var fieldDensityVal = 0;
	   var fieldDensityMetricVal = 0;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{ 
			densityMetric=Math.round((grossWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||volume==undefined||volume==0||volume==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/volume)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||fieldVolumeCbmVal==undefined||fieldVolumeCbmVal==0||fieldVolumeCbmVal==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/fieldVolumeCbmVal)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}

	var url = "updateContainerDetailsTareWtAjax.html?ajax=1&id="+id+"&fieldNetWeight="+fieldNetWeight+"&fieldNetWeightVal="+fieldNetWeightVal+"&fieldNetWeightKilo="+fieldNetWeightKilo+"&fieldNetWeightKiloVal="+fieldNetWeightKiloVal
				+"&fieldEmptyConWeight="+fieldEmptyConWeight+"&fieldEmptyConWeightVal="+fieldEmptyConWeightVal+"&fieldemptyContWeightKilo="+fieldemptyContWeightKilo+"&fieldemptyContWeightKiloVal="+fieldemptyContWeightKiloVal
				+"&fieldVolumeCbm="+fieldVolumeCbm+"&fieldVolumeCbmVal="+fieldVolumeCbmVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
	  http20333.open("GET", url, true); 
	  http20333.onreadystatechange = function(){handleHttpResponse20333(id)};
	  http20333.send(null);	 
}
function handleHttpResponse20333(id,fieldName){
	if (http20333.readyState == 4){		
            var results = http20333.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var http20333 = getHTTPObject();

function updateContainerNetWtKilo11(id,Q1,Q2,unit1,unit2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	fieldNetWeightKiloVal = document.getElementById('connetWeightKilo'+id).value=E2;
	var E3 = E1*2.2046;
	var E4=Math.round(E3*10000)/10000;
	fieldNetWeightVal = document.getElementById('connetWeight'+id).value=E4;
	var Q3 = Q1*2.2046;
	var Q4=Math.round(Q3*10000)/10000;
	fieldGrossWeightVal = document.getElementById('congrossWeight'+id).value=Q4;
	var Q5 = Q2*2.2046;
	var Q6=Math.round(Q5*10000)/10000;
	fieldEmptyConWeightVal = document.getElementById('conemptyContWeight'+id).value=Q6;
	
	fieldGrossWeightKiloVal = Q1;
	fieldemptyContWeightKiloVal = document.getElementById('conemptyContWeightKilo'+id).value=Q2;
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolumeCbm'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	
	   var E1=volume*35.3147;
	   var E2=Math.round(E1*10000)/10000;
	   fieldVolumeVal = document.getElementById('convolume'+id).value = E2;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((grossWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}
	
	
	var url = "updateContainerDetailsNetWtKiloAjax.html?ajax=1&id="+id+"&fieldNetWeight="+fieldNetWeight+"&fieldNetWeightVal="+fieldNetWeightVal+"&fieldNetWeightKilo="+fieldNetWeightKilo+"&fieldNetWeightKiloVal="+fieldNetWeightKiloVal
			+"&fieldGrossWeight="+fieldGrossWeight+"&fieldGrossWeightVal="+fieldGrossWeightVal+"&fieldGrossWeightKilo="+fieldGrossWeightKilo+"&fieldGrossWeightKiloVal="+fieldGrossWeightKiloVal+"&fieldEmptyConWeight="+fieldEmptyConWeight
			+"&fieldEmptyConWeightVal="+fieldEmptyConWeightVal+"&fieldemptyContWeightKilo="+fieldemptyContWeightKilo+"&fieldemptyContWeightKiloVal="+fieldemptyContWeightKiloVal
			+"&fieldVolume="+fieldVolume+"&fieldVolumeVal="+fieldVolumeVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
	  httpNetWtKilo11.open("GET", url, true); 
	  httpNetWtKilo11.onreadystatechange = function(){handleHttpResponseNetWtKilo11(id)};
	  httpNetWtKilo11.send(null);	 
}
function handleHttpResponseNetWtKilo11(id){
	if (httpNetWtKilo11.readyState == 4){		
            var results = httpNetWtKilo11.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var httpNetWtKilo11 = getHTTPObject();

function updateContainerTareWtKilo(id,Q1,Q2,unit1,unit2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	fieldemptyContWeightKiloVal = document.getElementById('conemptyContWeightKilo'+id).value=E2;
	var E3 = E1*2.2046;
	var E4=Math.round(E3*10000)/10000;		
	fieldEmptyConWeightVal = document.getElementById('conemptyContWeight'+id).value=E4;
	var E5 = Q2*2.2046;
	var E6=Math.round(E5*10000)/10000;
	fieldNetWeightVal = document.getElementById('connetWeight'+id).value=E6;

	fieldNetWeightKiloVal = document.getElementById('connetWeightKilo'+id).value=Q2;
	
	var weightUnitKgs = unit1.value;
	var volumeUnitCbm = unit2.value;
	var density=0;
	var densityMetric=0;
	var E2=0;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('congrossWeight'+id).value);
	var netWeight = eval(document.getElementById('connetWeight'+id).value);
	var volume = eval(document.getElementById('convolumeCbm'+id).value);
	var netWeightKilo= eval (document.getElementById('connetWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('congrossWeightKilo'+id).value);
	
	
	   var E1=volume*35.3147;
	   var E2=Math.round(E1*10000)/10000;
	   fieldVolumeVal = document.getElementById('convolume'+id).value = E2;
	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			var density=Math.round((grossWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((grossWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}else{
		if(netWeight==undefined||netWeight==0 ||netWeight==0.00 ||grossWeight==undefined||grossWeight==0 ||grossWeight==0.00||fieldVolumeVal==undefined||fieldVolumeVal==0||fieldVolumeVal==0.00){
			fieldDensityVal = document.getElementById('condensity'+id).value=0;
		}else{
			density=Math.round((netWeight/fieldVolumeVal)*1000)/1000;
			fieldDensityVal = document.getElementById('condensity'+id).value=density;
		}
		if(netWeightKilo==undefined||netWeightKilo==0||netWeightKilo==0.00||grossWeightKilo==undefined||grossWeightKilo==0||grossWeightKilo==0.00||volume==undefined||volume==0||volume==0.00){ 
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=0;
		}else{
			densityMetric=Math.round((netWeightKilo/volume)*1000)/1000;
			fieldDensityMetricVal = document.getElementById('condensityMetric'+id).value=densityMetric;
		}
	}
	
	var url = "updateContainerDetailsTareWtKiloAjax.html?ajax=1&id="+id+"&fieldNetWeight="+fieldNetWeight+"&fieldNetWeightVal="+fieldNetWeightVal+"&fieldNetWeightKilo="+fieldNetWeightKilo+"&fieldNetWeightKiloVal="+fieldNetWeightKiloVal
				+"&fieldEmptyConWeight="+fieldEmptyConWeight+"&fieldEmptyConWeightVal="+fieldEmptyConWeightVal+"&fieldemptyContWeightKilo="+fieldemptyContWeightKilo+"&fieldemptyContWeightKiloVal="+fieldemptyContWeightKiloVal
				+"&fieldVolume="+fieldVolume+"&fieldVolumeVal="+fieldVolumeVal+"&fieldDensity="+fieldDensity+"&fieldDensityVal="+fieldDensityVal+"&fieldDensityMetric="+fieldDensityMetric+"&fieldDensityMetricVal="+fieldDensityMetricVal+"&tableName="+tableName;
	 httpTareWtKilo.open("GET", url, true); 
	  httpTareWtKilo.onreadystatechange = function(){handleHttpResponseTareWtKilo(id)};
	  httpTareWtKilo.send(null);	 
}
function handleHttpResponseTareWtKilo(id){
	if (httpTareWtKilo.readyState == 4){		
            var results = httpTareWtKilo.responseText
            results = results.trim();
            getContainerDetails();
            showOrHide(0);
	}
}
var httpTareWtKilo = getHTTPObject();

function deleteContainerDetails(targetElement,targetElementStatus){
	var massage="";
	if(targetElementStatus.checked==false){
		massage="Are you sure you wish to deactivate this row?"
	}else{
		massage="Are you sure you wish to activate this row?"
	}
	   var agree=confirm(massage);
		if (agree){
			showOrHide(1);
			var containerStatus=false;
			if(targetElementStatus.checked==false){
				containerStatus=false;	
			}else{
				containerStatus=true;
			}
			var url =''; 
	   	if(soControlFlag=='G'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
				 url = "deleteContainerDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&containerStatus="+containerStatus+"&updateRecords="+updateRecords;
			}else{
				 url = "deleteContainerDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&containerStatus="+containerStatus;
			}
		}else{
			url = "deleteContainerDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&containerStatus="+containerStatus;
		}
	 	  http1222.open("GET", url, true); 
	 	  http1222.onreadystatechange = handleHttpResponse1222;
	 	  http1222.send(null);
		}else{
			if(targetElementStatus.checked==false){
			document.getElementById('status'+targetElement).checked=true;
			}else{
				document.getElementById('status'+targetElement).checked=false;
			}
		}
 }
 function handleHttpResponse1222(){
 	if (http1222.readyState == 4){
             var results = http1222.responseText
             getContainerDetails();
             showOrHide(0);
             //window.location.reload(true);
 	}
 }
 var http1222 = getHTTPObject();

//Soft validation for SS container #
 function isNumericCheck(elem, helperMsg){ 
 if(elem.trim().length !=6){
 var  agree = confirm(helperMsg);
 if(agree){
 return true;
 }else{
 return false;
 }
 } else{
 	var numericExpression = /^[0-9]+$/;
 	if(elem.match(numericExpression)){
 		return true;
 	}else{
 		var  agree = confirm(helperMsg);
 		if(agree){
 		return true;
 		}else{
 		return false;
 		}
 	}
 }
 }
 function isNumeric1(elem, helperMsg){
	 if(elem.trim().length !=1){
	 var  agree = confirm(helperMsg);
	 if(agree){
	 return true;
	 }else{
	 return false;
	 }
	 } else{
	 	var numericExpression = /^[0-9]+$/;
	 	if(elem.match(numericExpression)){
	 		return true;
	 	}else{
	 		var  agree = confirm(helperMsg);
	 		if(agree){
	 		return true;
	 		}else{
	 		return false;
	 		}
	 	}
	 }
	 }

	 function isAlphabet(elem, helperMsg){
	 if(elem.trim().length !=4){
	 var  agree = confirm(helperMsg);
	 if(agree){
	 return true;
	 }else{
	 return false;
	 }
	 } else{
	 	var alphaExp = /^[a-zA-Z]+$/;
	 	if(elem.match(alphaExp)){
	 		return true;
	 	}else{
	 		var  agree = confirm(helperMsg);
	 		if(agree){
	 		return true;
	 		}else{
	 		return false;
	 		}
	 	}
	 }
	 }
 function validateSSContainer(id){
	 var containerNumber=document.getElementById('containerNumber'+id).value;
	 //alert(containerNumber);
	 if(containerNumber.trim().length !=13){
	 var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	 if(agree){
	 	return true;
	 }else{
	 	return false;
	 }
	 } else {
	 var check="";
	 var checkdash="";
	 var first4 =containerNumber.substring(0,4)
	 if(check==""){
	  var returnCheck = isAlphabet(first4,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	  if(!returnCheck){
	 check="Ok";
	 }
	 } 
	 var firstdash = containerNumber.substring(4,5)
	 if(firstdash!='-'){
	 var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	 if(agree){
	 return true;
	 }else{
	 return false;
	 }
	 checkdash="Ok"
	 }
	
	 var next6 = containerNumber.substring(5,11) 
	 if(check == "" && checkdash==""){ 
	 var returnCheck =isNumericCheck(next6,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	 if(!returnCheck){
	 check="Ok";
	 }
	 }
	 var seconddash = containerNumber.substring(11,12)
	 if(checkdash==""){
	 if(seconddash!='-'){
	 var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	 if(agree){
	 return true;
	 }else{
	 return false;
	 }
	 }
	 }
	 var last1 = containerNumber.substring(12,13)
	 if(check=="" && checkdash==""){
	 var returnCheck =isNumeric1(last1,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
	 if(!returnCheck){
	 check="Ok";
	 }
	 }
	 	//if(containerNumber)
	 }
 return true;
 }
</script>
 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>   
