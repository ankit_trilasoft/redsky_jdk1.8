package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="dspdetails")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID") 


public class DspDetails extends BaseObject{
	private Long id;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;	
	private Date updatedOn;
	private Long serviceOrderId;
	private String shipNumber;
	
	private String CAR_vendorCode;
	private String CAR_vendorName;
	private String CAR_vendorContact;
	private String CAR_vendorEmail;
	private Date CAR_serviceStartDate;
	private Date CAR_serviceEndDate;
	private String CAR_vendorCodeEXSO;
	private Date CAR_notificationDate;
	private Date CAR_pickUpDate;
	private Date CAR_registrationDate;
	private Date CAR_deliveryDate;	
	
	private String COL_vendorCode;
	private String COL_vendorName;
	private String COL_vendorContact;
	private String COL_vendorEmail;
	private Date COL_serviceStartDate;
	private Date COL_serviceEndDate;
	private Date COL_notificationDate;
	private String COL_vendorCodeEXSO;
	private String COL_estimatedAllowanceEuro;
	
	private String TRG_vendorCode;
	private String TRG_vendorName;
	private String TRG_vendorContact;
	private String TRG_vendorEmail;
	private Date TRG_serviceStartDate;
	private Date TRG_serviceEndDate;	
	private String TRG_employeeDaysAuthorized;
	private String TRG_spouseDaysAuthorized;
	private String TRG_employeeTime;
	private String TRG_spouseTime;
	private String TRG_vendorCodeEXSO;

	private String HOM_vendorCode;
	private String HOM_vendorName;
	private String HOM_vendorContact;
	private String HOM_vendorEmail;
	private Date HOM_serviceStartDate;
	private Date HOM_serviceEndDate;
	private String HOM_viewStatus;
	private Date HOM_moveInDate;
	private String HOM_vendorCodeEXSO;
	
	private String HOM_agentCode;
	private String HOM_agentName;
	private Date HOM_initialContactDate;  
	private String HOM_agentPhone;
	private Date HOM_houseHuntingTripArrival;
	private String HOM_agentEmail;
	private Date HOM_houseHuntingTripDeparture;
	private String HOM_brokerage;
	private Date HOM_offerDate;
	private Date HOM_closingDate;
	
	private String RNT_vendorCode;
	private String RNT_vendorName;
	private String RNT_vendorContact;
	private String RNT_vendorEmail;
	private Date RNT_serviceStartDate;
	private Date RNT_serviceEndDate;	
	private Date RNT_leaseStartDate;
	private Date RNT_leaseExpireDate;
	private String RNT_monthlyRentalAllowance;
	private String RNT_negotiatedRent;
	private String RNT_utilitiesIncluded;
	private String RNT_securityDeposit;
	private Date RNT_reminderDate;
	private Date RNT_checkInDate;
	private String RNT_depositPaidBy;
	private String RNT_comment;
	private String RNT_paidCurrency;
	private String RNT_vendorCodeEXSO;
	private String RNT_homeSearchType;
	private Date RNT_rsfReceived;

	private Date RNT_moveInspection;
	private String LAN_vendorCode;
	private String LAN_vendorName;
	private String LAN_vendorContact;
	private String LAN_vendorEmail;
	private Date LAN_serviceStartDate;
	private Date LAN_serviceEndDate;
	private Date LAN_notificationDate;	
	private String LAN_employeeDaysAuthorized;
	private String LAN_spouseDaysAuthorized;
	private String LAN_childrenDaysAuthorized;
	private String LAN_employeeTime;
	private String LAN_spouseTime;
	private String LAN_childrenTime;
	private String LAN_vendorCodeEXSO;
	
	private String MMG_vendorCode;
	private String MMG_vendorName;
	private String MMG_vendorContact;
	private String MMG_vendorEmail;
	private Date MMG_serviceStartDate;
	private Date MMG_serviceEndDate;
	private String MMG_vendorCodeEXSO;
	private Date MMG_vendorInitiation;
	
	private String ONG_vendorCode;
	private String ONG_vendorName;
	private String ONG_vendorContact;
	private String ONG_vendorEmail;
	private Date ONG_serviceStartDate;
	private Date ONG_serviceEndDate;

	private String ONG_dateType1;
	private String ONG_dateType2;
	private String ONG_dateType3;
	private Date ONG_date1;
	private Date ONG_date2;
	private Date ONG_date3;
	private String ONG_comment;
	private String ONG_vendorCodeEXSO;

	private String PRV_vendorCode;
	private String PRV_vendorName;
	private String PRV_vendorContact;
	private String PRV_vendorEmail;
	private Date PRV_serviceStartDate;
	private Date PRV_serviceEndDate;	
	private Date PRV_travelPlannerNotification;
	private Date PRV_travelDeparture;
	private Date PRV_travelArrival;
	private String PRV_hotelDetails;
	private Date PRV_providerNotificationAndServicesAuthorized;
	private Date PRV_meetGgreetAtAirport;
	private String PRV_areaOrientationDays;
	private String PRV_flightNumber;
	private String PRV_flightArrivalTime;
	private String PRV_vendorCodeEXSO;
	
	private String AIO_airlineFlight;
	private String AIO_contactInformation;
	private Date AIO_flightArrival;
	private Date AIO_flightDeparture;
	private Date AIO_initialDateOfServiceRequested;
	private Date AIO_prearrivalDiscussionOfProviderAndFamily;
	private Date AIO_providerConfirmationReceived;
	private Date AIO_providerNotificationSent;
	private String AIO_serviceAuthorized;
	private String AIO_vendorCode;
	private String AIO_vendorName;
	private String AIO_vendorContact;
	private String AIO_vendorEmail;
	private Date AIO_serviceStartDate;
	private Date AIO_serviceEndDate;
	private String AIO_vendorCodeEXSO;

	private String EXP_vendorCode;
	private String EXP_vendorName;
	private String EXP_vendorContact;
	private String EXP_vendorEmail;
	private Date EXP_serviceStartDate;
	private Date EXP_serviceEndDate;
	private String EXP_paidStatus;
	private Date EXP_providerNotification;
	private String EXP_vendorCodeEXSO;

	private String RPT_vendorCode;
	private String RPT_vendorName;
	private String RPT_vendorContact;
	private String RPT_vendorEmail;
	private Date RPT_serviceStartDate;
	private Date RPT_serviceEndDate;
	private Date RPT_landlordPaidNotificationDate;
	private Date RPT_professionalCleaningDate;
	private String RPT_comment;
	private Date RPT_utilitiesShutOff;
	private Date RPT_moveOutDate;
	private Date RPT_negotiatedDepositReturnedDate;
	private String RPT_damageCost;
	private String RPT_securityDeposit;
	private String RPT_depositReturnedTo;
	private String RPT_vendorCodeEXSO;
		
	private String SCH_schoolSelected;
	private String SCH_schoolName;
	private String SCH_contactPerson;
	private String SCH_website;
	private Date SCH_serviceStartDate;
	private Date SCH_serviceEndDate;
	private Date SCH_prearrivalDiscussionBetweenProviderAndFamily;
	private Date SCH_admissionDate;
	private String SCH_city;
	private int SCH_noOfChildren;
	private String SCH_vendorCodeEXSO;

	
	private String TAX_vendorCode;
	private String TAX_vendorName;
	private String TAX_vendorContact;
	private String TAX_vendorEmail;
	private Date TAX_serviceStartDate;
	private Date TAX_serviceEndDate;
	private Date TAX_notificationdate;
	private String TAX_vendorCodeEXSO;

	private String TAC_vendorCode;
	private String TAC_vendorName;
	private String TAC_vendorContact;
	private String TAC_vendorEmail;
	private Date TAC_serviceStartDate;
	private Date TAC_serviceEndDate;
	private String TAC_monthsWeeksDaysAuthorized;
	private String TAC_monthlyRentalAllowance;
	private Date TAC_leaseStartDate;
	private String TAC_securityDeposit;
	private Date TAC_leaseExpireDate;
	private Date TAC_expiryReminderPriorToExpiry;	
	private String TAC_negotiatedRent;
	private String TAC_utilitiesIncluded;
	private String TAC_depositPaidBy;
	private String TAC_comment;
	private String TAC_vendorCodeEXSO;


	private String TEN_vendorCode;
	private String TEN_vendorName;
	private String TEN_vendorContact;
	private String TEN_vendorEmail;
	private Date TEN_serviceStartDate;
	private Date TEN_serviceEndDate;
	private String TEN_vendorCodeEXSO;
	private String TEN_landlordName;
	private String TEN_landlordContactNumber;
	private String TEN_landlordEmailAddress;
	private String TEN_propertyAddress;
	private String TEN_accountHolder;
	private String TEN_bankName;
	private String TEN_bankAddress;
	private String TEN_bankAccountNumber;
	private String TEN_bankIbanNumber;
	private String TEN_abaNumber;
	private String TEN_accountType;
	private Date TEN_leaseStartDate;
	private Date TEN_leaseExpireDate;
	private Date TEN_expiryReminderPriorToExpiry;
	private String TEN_monthlyRentalAllowance;
	private String TEN_securityDeposit;
	private String TEN_leaseCurrency;
	private String TEN_leaseSignee;
	
	private String TEN_propertyName;
	private String TEN_city;
	private String TEN_zipCode;
	private String TEN_addressLine1;
	private String TEN_addressLine2;
	private String TEN_state;
	private String TEN_country;
	
	private String TEN_leasedBy;
	private BigDecimal TEN_rentAmount=new BigDecimal(0.00);
	private String TEN_rentCurrency;
	private BigDecimal TEN_rentAllowance=new BigDecimal(0.00);
	private String TEN_allowanceCurrency;
	private String TEN_utilitiesIncluded;
	private String TEN_rentPaidTo;
	private Date TEN_rentalIncreaseDate;
	private String TEN_rentalComment;
	private String TEN_followUpNeeded;
	private String TEN_termOfNotice;
	private Date TEN_checkInMoveIn;
	private Date TEN_preCheckOut;
	private Date TEN_checkOutMoveOut;
	
	private String TEN_swiftCode;
	private String TEN_paymentDescription;
	
    private String TEN_exceptionComments;
    
    private BigDecimal TEN_depositAmount=new BigDecimal(0.00);
    private String TEN_depositReturned;
    private String TEN_depositCurrency;
    private BigDecimal TEN_depositReturnedAmount=new BigDecimal(0.00);
    private String TEN_depositPaidBy;
    private String TEN_depositReturnedCurrency;
    private String TEN_depositComment;
    private String TEN_refundableTo;
    
	private String VIS_vendorCode;
	private String VIS_vendorName;
	private String VIS_vendorContact;
	private String VIS_vendorEmail;
	private Date VIS_serviceStartDate;
	private Date VIS_serviceEndDate;
	private String VIS_workPermitVisaHolderName;
	private Date VIS_providerNotificationDate;
	private Date VIS_visaExpiryDate;
	private Date VIS_workPermitExpiry;
	private Date VIS_expiryReminder3MosPriorToExpiry;
	private Date VIS_visaStartDate;
	private String VIS_visaExtensionNeeded;
	private String VIS_assignees;
	private String VIS_employers;
	private String VIS_convenant;
	private String VIS_residencePermitHolderNameRP; 
	private Date VIS_providerNotificationDateRP;
	private Date VIS_visaExpiryDateRP;
	private Date VIS_expiryReminder3MosPriorToExpiryRP;
	private Date VIS_residenceExpiryRP;
	private Date VIS_questionnaireSentDate;
	private String VIS_primaryEmail;
	private Boolean VIS_authorizationLetter;
	private Boolean VIS_copyResidence;
	private String VIS_vendorCodeEXSO;
	private Date VIS_applicationDateWork;
	private Date VIS_permitStartDateWork;
	private Date VIS_applicationDateResidence;
	private Date VIS_permitStartDateResidence;
	
	private String WOP_vendorCode;
	private String WOP_vendorName;
	private String WOP_vendorContact;
	private String WOP_vendorEmail;
	private Date WOP_serviceStartDate;
	private Date WOP_serviceEndDate;
	private String WOP_workPermitHolderName;
	private Date WOP_providerNotificationDate;
	private Date WOP_permitStartDate;
	private Date WOP_permitExpiryDate;
	private Date WOP_reminder3MosPriorToExpiry;
	private Date WOP_questionnaireSentDate;
	private Boolean WOP_authorizationLetter;
	private Boolean WOP_copyResidence;
	private String WOP_vendorCodeEXSO;
	
	private String REP_vendorCode;
	private String REP_vendorName;
	private String REP_vendorContact;
	private String REP_vendorEmail;
	private Date REP_serviceStartDate;
	private Date REP_serviceEndDate;
	private String REP_workPermitHolderName;
	private Date REP_providerNotificationDate;
	private Date REP_permitStartDate;
	private Date REP_permitExpiryDate;
	private Date REP_reminder3MosPriorToExpiry;
	private Date REP_questionnaireSentDate;
	private Boolean REP_authorizationLetter;
	private Boolean REP_copyResidence;
	private String REP_vendorCodeEXSO;
	
	private String SET_vendorCode;
	private String SET_vendorName;
	private String SET_vendorContact;
	private String SET_vendorEmail;
	private Date SET_serviceStartDate;
	private Date SET_serviceEndDate;
	private Date SET_parkingPermit;
	private Date SET_bankAccount;
	private Date SET_cityHall;
	private Date SET_internetConn;
	private Date SET_driverLicense;
	private String SET_comment;
	private String SET_vendorCodeEXSO;
	
	
	private String CAR_comment;
	private String COL_comment;
	private String TRG_comment;
	private String HOM_comment;
	private String LAN_comment;
	private String MMG_comment;
	private String PRV_comment;
	private String AIO_comment;
	private String EXP_comment;
	private String SCH_comment;
	private String TAX_comment;
	private String TEN_comment;
	private String VIS_comment;
	private String WOP_comment;
	private String REP_comment;
	private Date serviceCompleteDate;
	
	private Boolean CAR_displyOtherVendorCode; 
	private Boolean COL_displyOtherVendorCode;
	private Boolean TRG_displyOtherVendorCode;
	private Boolean HOM_displyOtherVendorCode;
	private Boolean RNT_displyOtherVendorCode;
	private Boolean SET_displyOtherVendorCode;
	private Boolean LAN_displyOtherVendorCode;
	private Boolean MMG_displyOtherVendorCode;
	private Boolean ONG_displyOtherVendorCode;
	private Boolean PRV_displyOtherVendorCode;
	private Boolean AIO_displyOtherVendorCode;
	private Boolean EXP_displyOtherVendorCode;
	private Boolean RPT_displyOtherVendorCode;
	private Boolean SCH_displyOtherSchoolSelected;
	private Boolean TAX_displyOtherVendorCode;
	private Boolean TAC_displyOtherVendorCode;
	private Boolean TEN_displyOtherVendorCode;
	private Boolean VIS_displyOtherVendorCode;
	private Boolean WOP_displyOtherVendorCode;
	private Boolean REP_displyOtherVendorCode;
	
	private String RLS_vendorCode;
	private String RLS_vendorName;
	private Date RLS_serviceStartDate;
	private String RLS_vendorCodeEXSO;
	private String RLS_vendorContact;
	private Date RLS_serviceEndDate;
	private String RLS_vendorEmail;
	private Boolean RLS_displyOtherVendorCode;
	private String CAT_vendorCode;
	private String CAT_vendorName;
	private Date CAT_serviceStartDate;
	private String CAT_vendorCodeEXSO;
	private String CAT_vendorContact;
	private Date CAT_serviceEndDate;
	private String CAT_vendorEmail;
	private Boolean CAT_displyOtherVendorCode;	

	private String CLS_vendorCode;
	private String CLS_vendorName;
	private Date CLS_serviceStartDate;
	private String CLS_vendorCodeEXSO;
	private String CLS_vendorContact;
	private Date CLS_serviceEndDate;
	private String CLS_vendorEmail;
	private Boolean CLS_displyOtherVendorCode;

	private String CHS_vendorCode;
	private String CHS_vendorName;
	private Date CHS_serviceStartDate;
	private String CHS_vendorCodeEXSO;
	private String CHS_vendorContact;
	private Date CHS_serviceEndDate;
	private String CHS_vendorEmail;
	private Boolean CHS_displyOtherVendorCode;
	private String DPS_vendorCode;
	private String DPS_vendorName;
	private Date DPS_serviceStartDate;
	private String DPS_vendorCodeEXSO;
	private String DPS_vendorContact;
	private Date DPS_serviceEndDate;
	private String DPS_vendorEmail;
	private Boolean DPS_displyOtherVendorCode;
	private String HSM_vendorCode;
	private String HSM_vendorName;
	private Date HSM_serviceStartDate;
	private String HSM_vendorCodeEXSO;
	private String HSM_vendorContact;
	private Date HSM_serviceEndDate;
	private String HSM_vendorEmail;
	private Boolean HSM_displyOtherVendorCode;	
	private String PDT_vendorCode;
	private String PDT_vendorName;
	private Date PDT_serviceStartDate;
	private String PDT_vendorCodeEXSO;
	private String PDT_vendorContact;
	private Date PDT_serviceEndDate;
	private String PDT_vendorEmail;
	private Boolean PDT_displyOtherVendorCode;	
	private String RCP_vendorCode;
	private String RCP_vendorName;
	private Date RCP_serviceStartDate;
	private String RCP_vendorCodeEXSO;
	private String RCP_vendorContact;
	private Date RCP_serviceEndDate;
	private String RCP_vendorEmail;
	private Boolean RCP_displyOtherVendorCode;	

	private String SPA_vendorCode;
	private String SPA_vendorName;
	private Date SPA_serviceStartDate;
	private String SPA_vendorCodeEXSO;
	private String SPA_vendorContact;
	private Date SPA_serviceEndDate;
	private String SPA_vendorEmail;
	private Boolean SPA_displyOtherVendorCode;	
	private String TCS_vendorCode;
	private String TCS_vendorName;
	private Date TCS_serviceStartDate;
	private String TCS_vendorCodeEXSO;
	private String TCS_vendorContact;
	private Date TCS_serviceEndDate;
	private String TCS_vendorEmail;
	private Boolean TCS_displyOtherVendorCode;	
	private String mailServiceType;
	private String RNT_monthlyRentalAllowanceCurrency;
	private String RNT_securityDepositCurrency;

	private String MTS_vendorCode;
	private String MTS_vendorName;
	private Date MTS_serviceStartDate;
	private String MTS_vendorCodeEXSO;
	private String MTS_vendorContact;
	private Date MTS_serviceEndDate;
	private String MTS_vendorEmail;
	private String MTS_paymentResponsibility;
	private Boolean MTS_displyOtherVendorCode;
	
	private String RLS_paymentResponsibility;
	private String CAT_paymentResponsibility;
	private String CLS_paymentResponsibility;
	private String CHS_paymentResponsibility;
	private String DPS_paymentResponsibility;
	private String HSM_paymentResponsibility;
	private String PDT_paymentResponsibility;
	private String RCP_paymentResponsibility;
	private String SPA_paymentResponsibility;
	private String TCS_paymentResponsibility;
	
	private String DPS_serviceType;   
	private Date DPS_completionDate;
	private String DPS_comment;
	private String HSM_brokerCode;
	private String HSM_brokerName;
	private String HSM_brokerContact;
	private String HSM_brokerEmail;
	private String HSM_status;
	private String HSM_comment;
	private Date HSM_marketingPlanNBMAReceived;  
	private String PDT_serviceType;
	private BigDecimal PDT_budget=new BigDecimal(0.00);   
	private BigDecimal RCP_estimateCost=new BigDecimal(0.00);     
	private BigDecimal RCP_actualCost=new BigDecimal(0.00);    
	private String MTS_lender;
	private Date MTS_initialContact;   
	private String MTS_status;
	private BigDecimal MTS_mortgageAmount=new BigDecimal(0.00);      
	private BigDecimal MTS_mortgageRate=new BigDecimal(0.00);     
	private String MTS_mortgageTerm;
	private String COL_estimatedTaxAllowance;
	private String COL_estimatedHousingAllowance;
	private String COL_estimatedTransportationAllowance;
	private String RNT_leaseSignee;
	private String RNT_leaseExtensionNeeded;
	private Date SET_governmentID;
	private Date SET_heathCare;
	private Date SET_utilities;
	private Date SET_automobileRegistration;
	private Date SET_dayofServiceAuthorized;
	private Date SET_initialDateofServiceRequested;
	private Date SET_providerNotificationSent;
	private Date SET_providerConfirmationReceived;
	private String TAX_allowance;
	private String VIS_immigrationStatus;
	private Date VIS_arrivalDate;
	private String VIS_portofEntry;

	private String DSS_vendorCode;
	private String DSS_vendorName;
	private Date DSS_serviceStartDate;
	private String DSS_vendorCodeEXSO;
	private String DSS_vendorContact;
	private Date DSS_serviceEndDate;
	private String DSS_vendorEmail;
	private String DSS_paymentResponsibility;
	private Boolean DSS_displyOtherVendorCode;	
	private Date DSS_initialContactDate;
	private String PRV_paymentResponsibility; 
	private String VIS_paymentResponsibility;
	private String TAX_paymentResponsibility;
	private String RNT_paymentResponsibility; 
	private String AIO_paymentResponsibility; 
	private String TRG_paymentResponsibility; 
	private String LAN_paymentResponsibility; 
	private String RPT_paymentResponsibility; 
	private String COL_paymentResponsibility; 
	private String TAC_paymentResponsibility; 
	private String HOM_paymentResponsibility; 
	private String SCH_paymentResponsibility; 
	private String EXP_paymentResponsibility; 
	private String ONG_paymentResponsibility; 
	private String TEN_paymentResponsibility; 
	private String MMG_paymentResponsibility; 
	private String CAR_paymentResponsibility; 
	private String SET_paymentResponsibility; 
	private String WOP_paymentResponsibility; 
	private String REP_paymentResponsibility; 
	private String HSM_agentCode;
	private String HSM_agentName;
	private String HSM_brokerage;  
	private String HSM_agentPhone;
	private String HSM_agentEmail;
	private BigDecimal HSM_estimatedHSRReferral=new BigDecimal(0.00);
	private BigDecimal HSM_actualHSRReferral=new BigDecimal(0.00); 
	private String reminderServices;
	private String CAT_comment;
	private String CLS_comment;
	private String CHS_comment;
	private String PDT_comment;
	private String RCP_comment;
	private String SPA_comment;
	private String TCS_comment;
	private String MTS_comment;
	private String DSS_comment;
	private BigDecimal HOM_estimatedHSRReferral=new BigDecimal(0.00);
	private BigDecimal HOM_actualHSRReferral=new BigDecimal(0.00);
	private Date HSM_offerDate;
	private Date HSM_closingDate;
	private String TAC_timeAuthorized;
	private String MMG_originAgentCode;
	private String MMG_originAgentName;
	private String MMG_originAgentPhone;
	private String MMG_originAgentEmail;
	private String MMG_destinationAgentCode;
	private String MMG_destinationAgentName;
	private String MMG_destinationAgentPhone;
	private String MMG_destinationAgentEmail;
	
	private Date PRV_emailSent;
	private Date TAX_emailSent;
	private Date AIO_emailSent;
	private Date TRG_emailSent;
	private Date LAN_emailSent;
	private Date RPT_emailSent;
	private Date COL_emailSent;
	private Date TAC_emailSent;
	private Date HOM_emailSent;
	private Date SCH_emailSent;
	private Date EXP_emailSent;
	private Date ONG_emailSent;
	private Date TEN_emailSent;
	private Date MMG_orginEmailSent;
	private Date MMG_destinationEmailSent;
	private Date CAR_emailSent;
	private Date SET_emailSent;
	private Date CAT_emailSent;
	private Date CLS_emailSent;
	private Date CHS_emailSent;
	private Date DPS_emailSent;
	private Date HSM_emailSent;
	private Date PDT_emailSent;
	private Date RCP_emailSent;
	private Date SPA_emailSent;
	private Date TCS_emailSent;
	private Date MTS_emailSent;
	private Date DSS_emailSent;
	private Date VIS_emailSent;
	private Date RNT_emailSent;	
	private Date NET_emailSent;
	private Date WOP_emailSent;	
	private Date REP_emailSent;	
	
	private String resendEmailServiceName;
	private Date HOB_startDate;	
	private Date HOB_endDate;
	private String HOB_hotelName;
	private String HOB_city; 
	private Date FLB_arrivalDate;	
	private Date FLB_departureDate;
	private Date FLB_additionalBookingReminderDate;
	private Date FLB_arrivalDate1;	
	private Date FLB_departureDate1;
	private Date FLB_additionalBookingReminderDate1;
	private Boolean FLB_addOn;
	private String HOB_vendorCode;
	private String HOB_vendorName;
	private String HOB_vendorContact;
	private String HOB_vendorEmail;
	private Date HOB_serviceStartDate;
	private Date HOB_serviceEndDate;
	private String HOB_vendorCodeEXSO;
	private String FLB_vendorCode;
	private String FLB_vendorName;
	private String FLB_vendorContact;
	private String FLB_vendorEmail;
	private Date FLB_serviceStartDate;
	private Date FLB_serviceEndDate;
	private String FLB_vendorCodeEXSO;
	private Date FLB_emailSent;
	private Date HOB_emailSent;
	private Boolean HOB_displyOtherVendorCode;
	private Boolean FLB_displyOtherVendorCode;
	
	private String FRL_vendorCode;
	private String FRL_vendorName;
	private Date FRL_serviceStartDate;
	private String FRL_vendorCodeEXSO;
	private String FRL_vendorContact;
	private Date FRL_serviceEndDate;
	private Date FRL_emailSent;
	private String FRL_vendorEmail;
	private Boolean FRL_displyOtherVendorCode;
	private String FRL_paymentResponsibility;
	private String FRL_rentalRate;     
	private String FRL_deposit;
	private String FRL_rentalCurrency;
	private String FRL_depositCurrency;
	private String FRL_month;
	private Date FRL_rentalStartDate;
	private Date FRL_rentalEndDate;
	private String FRL_terminationNotice;
	private String FRL_comment;
	
	private String APU_vendorCode;
	private String APU_vendorName;
	private Date APU_serviceStartDate;
	private String APU_vendorCodeEXSO;
	private String APU_vendorContact;
	private Date APU_serviceEndDate;
	private Date APU_emailSent;
	private String APU_vendorEmail;
	private Boolean APU_displyOtherVendorCode;
	private String APU_paymentResponsibility;
	private String APU_flightDetails;
	private String APU_driverName;
	private String APU_driverPhoneNumber;
	private Date APU_arrivalDate;
	private String APU_carMake;
	private String APU_carModel;
	private String APU_carColor;
	private String WOP_leaseExNeeded;
	private String REP_leaseExNeeded;
	
	private Boolean TEN_Gas_Water;
	private Boolean TEN_Gas;
	private Boolean TEN_TV_Internet_Phone;
	private Boolean TEN_mobilePhone;
	private Boolean TEN_furnitureRental;
	private Boolean TEN_cleaningServices;
	private Boolean TEN_parkingPermit;
	private Boolean TEN_communityTax;
	private Boolean TEN_insurance;
	private Boolean TEN_gardenMaintenance;
	private Boolean TEN_Electricity;
	private Boolean TEN_Miscellaneous;

	private String TEN_toBePaidBy;
	private BigDecimal TEN_assigneeContributionAmount= new BigDecimal(0);
	private Boolean TEN_directDebit;
	private String TEN_description;
	private String TEN_IBAN_BankAccountNumber;
	private String TEN_BIC_SWIFT;
	private String TEN_assigneeContributionCurrency;
	
	private Boolean TEN_Utility_Gas_Water;
	private Boolean TEN_Utility_Gas;
	private Boolean TEN_Utility_TV_Internet_Phone;
	private Boolean TEN_Utility_mobilePhone;
	private Boolean TEN_Utility_furnitureRental;
	private Boolean TEN_Utility_cleaningServices;
	private Boolean TEN_Utility_parkingPermit;
	private Boolean TEN_Utility_communityTax;
	private Boolean TEN_Utility_insurance;
	private Boolean TEN_Utility_gardenMaintenance;
	private BigDecimal TEN_contributionAmount= new BigDecimal(0);
	private Boolean TEN_Utility_Electricity;
	private Boolean TEN_Utility_Miscellaneous;
	private Boolean TEN_Gas_Electric;
	private Boolean TEN_Utility_Gas_Electric;
	
	private String TEN_exceptionAddedValue;
	private String TEN_housingRentVatDesc;
	private Date TEN_billThroughDate;
	private String TEN_housingRentVatPercent;
	
	private String INS_vendorCode;
	private String INS_vendorName;
	private String INS_vendorCodeEXSO;
	private String INS_vendorContact;
	private String INS_vendorEmail;
	private String INS_comment;
	private Date INS_vendorInitiation;
	private Date INS_serviceStartDate;
	private Date INS_serviceEndDate;
	
	private String INP_vendorCode;
	private String INP_vendorName;
	private String INP_vendorCodeEXSO;
	private String INP_vendorContact;
	private String INP_vendorEmail;
	private String INP_comment;
	private Date INP_vendorInitiation;
	private Date INP_serviceStartDate;
	private Date INP_serviceEndDate;
	
	private String EDA_vendorCode;
	private String EDA_vendorName;
	private String EDA_vendorCodeEXSO;
	private String EDA_vendorContact;
	private String EDA_vendorEmail;
	private String EDA_comment;
	private Date EDA_vendorInitiation;
	private Date EDA_serviceStartDate;
	private Date EDA_serviceEndDate;
	
	private Date SPA_vendorInitiation;
	
	private String TAS_vendorCode;
	private String TAS_vendorName;
	private String TAS_vendorCodeEXSO;
	private String TAS_vendorContact;
	private String TAS_vendorEmail;
	private String TAS_comment;
	private Date TAS_vendorInitiation;
	private Date TAS_serviceStartDate;
	private Date TAS_serviceEndDate;
	private Integer totalDspDays;




	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("serviceOrderId", serviceOrderId)
				.append("shipNumber", shipNumber)
				.append("CAR_vendorCode", CAR_vendorCode)
				.append("CAR_vendorName", CAR_vendorName)
				.append("CAR_vendorContact", CAR_vendorContact)
				.append("CAR_vendorEmail", CAR_vendorEmail)
				.append("CAR_serviceStartDate", CAR_serviceStartDate)
				.append("CAR_serviceEndDate", CAR_serviceEndDate)
				.append("CAR_vendorCodeEXSO", CAR_vendorCodeEXSO)
				.append("CAR_notificationDate", CAR_notificationDate)
				.append("CAR_pickUpDate", CAR_pickUpDate)
				.append("CAR_registrationDate", CAR_registrationDate)
				.append("CAR_deliveryDate", CAR_deliveryDate)
				.append("COL_vendorCode", COL_vendorCode)
				.append("COL_vendorName", COL_vendorName)
				.append("COL_vendorContact", COL_vendorContact)
				.append("COL_vendorEmail", COL_vendorEmail)
				.append("COL_serviceStartDate", COL_serviceStartDate)
				.append("COL_serviceEndDate", COL_serviceEndDate)
				.append("COL_notificationDate", COL_notificationDate)
				.append("COL_vendorCodeEXSO", COL_vendorCodeEXSO)
				.append("COL_estimatedAllowanceEuro",
						COL_estimatedAllowanceEuro)
				.append("TRG_vendorCode", TRG_vendorCode)
				.append("TRG_vendorName", TRG_vendorName)
				.append("TRG_vendorContact", TRG_vendorContact)
				.append("TRG_vendorEmail", TRG_vendorEmail)
				.append("TRG_serviceStartDate", TRG_serviceStartDate)
				.append("TRG_serviceEndDate", TRG_serviceEndDate)
				.append("TRG_employeeDaysAuthorized",
						TRG_employeeDaysAuthorized)
				.append("TRG_spouseDaysAuthorized", TRG_spouseDaysAuthorized)
				.append("TRG_employeeTime", TRG_employeeTime)
				.append("TRG_spouseTime", TRG_spouseTime)
				.append("TRG_vendorCodeEXSO", TRG_vendorCodeEXSO)
				.append("HOM_vendorCode", HOM_vendorCode)
				.append("HOM_vendorName", HOM_vendorName)
				.append("HOM_vendorContact", HOM_vendorContact)
				.append("HOM_vendorEmail", HOM_vendorEmail)
				.append("HOM_serviceStartDate", HOM_serviceStartDate)
				.append("HOM_serviceEndDate", HOM_serviceEndDate)
				.append("HOM_viewStatus", HOM_viewStatus)
				.append("HOM_moveInDate", HOM_moveInDate)
				.append("HOM_vendorCodeEXSO", HOM_vendorCodeEXSO)
				.append("HOM_agentCode", HOM_agentCode)
				.append("HOM_agentName", HOM_agentName)
				.append("HOM_initialContactDate", HOM_initialContactDate)
				.append("HOM_agentPhone", HOM_agentPhone)
				.append("HOM_houseHuntingTripArrival",
						HOM_houseHuntingTripArrival)
				.append("HOM_agentEmail", HOM_agentEmail)
				.append("HOM_houseHuntingTripDeparture",
						HOM_houseHuntingTripDeparture)
				.append("HOM_brokerage", HOM_brokerage)
				.append("HOM_offerDate", HOM_offerDate)
				.append("HOM_closingDate", HOM_closingDate)
				.append("RNT_vendorCode", RNT_vendorCode)
				.append("RNT_vendorName", RNT_vendorName)
				.append("RNT_vendorContact", RNT_vendorContact)
				.append("RNT_vendorEmail", RNT_vendorEmail)
				.append("RNT_serviceStartDate", RNT_serviceStartDate)
				.append("RNT_serviceEndDate", RNT_serviceEndDate)
				.append("RNT_leaseStartDate", RNT_leaseStartDate)
				.append("RNT_leaseExpireDate", RNT_leaseExpireDate)
				.append("RNT_monthlyRentalAllowance",
						RNT_monthlyRentalAllowance)
				.append("RNT_negotiatedRent", RNT_negotiatedRent)
				.append("RNT_utilitiesIncluded", RNT_utilitiesIncluded)
				.append("RNT_securityDeposit", RNT_securityDeposit)
				.append("RNT_reminderDate", RNT_reminderDate)
				.append("RNT_checkInDate", RNT_checkInDate)
				.append("RNT_depositPaidBy", RNT_depositPaidBy)
				.append("RNT_comment", RNT_comment)
				.append("RNT_paidCurrency", RNT_paidCurrency)
				.append("RNT_vendorCodeEXSO", RNT_vendorCodeEXSO)
				.append("RNT_homeSearchType", RNT_homeSearchType)
				.append("LAN_vendorCode", LAN_vendorCode)
				.append("LAN_vendorName", LAN_vendorName)
				.append("LAN_vendorContact", LAN_vendorContact)
				.append("LAN_vendorEmail", LAN_vendorEmail)
				.append("LAN_serviceStartDate", LAN_serviceStartDate)
				.append("LAN_serviceEndDate", LAN_serviceEndDate)
				.append("LAN_notificationDate", LAN_notificationDate)
				.append("LAN_employeeDaysAuthorized",
						LAN_employeeDaysAuthorized)
				.append("LAN_spouseDaysAuthorized", LAN_spouseDaysAuthorized)
				.append("LAN_childrenDaysAuthorized",
						LAN_childrenDaysAuthorized)
				.append("LAN_employeeTime", LAN_employeeTime)
				.append("LAN_spouseTime", LAN_spouseTime)
				.append("LAN_childrenTime", LAN_childrenTime)
				.append("LAN_vendorCodeEXSO", LAN_vendorCodeEXSO)
				.append("MMG_vendorCode", MMG_vendorCode)
				.append("MMG_vendorName", MMG_vendorName)
				.append("MMG_vendorContact", MMG_vendorContact)
				.append("MMG_vendorEmail", MMG_vendorEmail)
				.append("MMG_serviceStartDate", MMG_serviceStartDate)
				.append("MMG_serviceEndDate", MMG_serviceEndDate)
				.append("MMG_vendorCodeEXSO", MMG_vendorCodeEXSO)
				.append("MMG_vendorInitiation", MMG_vendorInitiation)
				.append("ONG_vendorCode", ONG_vendorCode)
				.append("ONG_vendorName", ONG_vendorName)
				.append("ONG_vendorContact", ONG_vendorContact)
				.append("ONG_vendorEmail", ONG_vendorEmail)
				.append("ONG_serviceStartDate", ONG_serviceStartDate)
				.append("ONG_serviceEndDate", ONG_serviceEndDate)
				.append("ONG_dateType1", ONG_dateType1)
				.append("ONG_dateType2", ONG_dateType2)
				.append("ONG_dateType3", ONG_dateType3)
				.append("ONG_date1", ONG_date1)
				.append("ONG_date2", ONG_date2)
				.append("ONG_date3", ONG_date3)
				.append("ONG_comment", ONG_comment)
				.append("ONG_vendorCodeEXSO", ONG_vendorCodeEXSO)
				.append("PRV_vendorCode", PRV_vendorCode)
				.append("PRV_vendorName", PRV_vendorName)
				.append("PRV_vendorContact", PRV_vendorContact)
				.append("PRV_vendorEmail", PRV_vendorEmail)
				.append("PRV_serviceStartDate", PRV_serviceStartDate)
				.append("PRV_serviceEndDate", PRV_serviceEndDate)
				.append("PRV_travelPlannerNotification",
						PRV_travelPlannerNotification)
				.append("PRV_travelDeparture", PRV_travelDeparture)
				.append("PRV_travelArrival", PRV_travelArrival)
				.append("PRV_hotelDetails", PRV_hotelDetails)
				.append("PRV_providerNotificationAndServicesAuthorized",
						PRV_providerNotificationAndServicesAuthorized)
				.append("PRV_meetGgreetAtAirport", PRV_meetGgreetAtAirport)
				.append("PRV_areaOrientationDays", PRV_areaOrientationDays)
				.append("PRV_flightNumber", PRV_flightNumber)
				.append("PRV_flightArrivalTime", PRV_flightArrivalTime)
				.append("PRV_vendorCodeEXSO", PRV_vendorCodeEXSO)
				.append("AIO_airlineFlight", AIO_airlineFlight)
				.append("AIO_contactInformation", AIO_contactInformation)
				.append("AIO_flightArrival", AIO_flightArrival)
				.append("AIO_flightDeparture", AIO_flightDeparture)
				.append("AIO_initialDateOfServiceRequested",
						AIO_initialDateOfServiceRequested)
				.append("AIO_prearrivalDiscussionOfProviderAndFamily",
						AIO_prearrivalDiscussionOfProviderAndFamily)
				.append("AIO_providerConfirmationReceived",
						AIO_providerConfirmationReceived)
				.append("AIO_providerNotificationSent",
						AIO_providerNotificationSent)
				.append("AIO_serviceAuthorized", AIO_serviceAuthorized)
				.append("AIO_vendorCode", AIO_vendorCode)
				.append("AIO_vendorName", AIO_vendorName)
				.append("AIO_vendorContact", AIO_vendorContact)
				.append("AIO_vendorEmail", AIO_vendorEmail)
				.append("AIO_serviceStartDate", AIO_serviceStartDate)
				.append("AIO_serviceEndDate", AIO_serviceEndDate)
				.append("AIO_vendorCodeEXSO", AIO_vendorCodeEXSO)
				.append("EXP_vendorCode", EXP_vendorCode)
				.append("EXP_vendorName", EXP_vendorName)
				.append("EXP_vendorContact", EXP_vendorContact)
				.append("EXP_vendorEmail", EXP_vendorEmail)
				.append("EXP_serviceStartDate", EXP_serviceStartDate)
				.append("EXP_serviceEndDate", EXP_serviceEndDate)
				.append("EXP_paidStatus", EXP_paidStatus)
				.append("EXP_providerNotification", EXP_providerNotification)
				.append("EXP_vendorCodeEXSO", EXP_vendorCodeEXSO)
				.append("RPT_vendorCode", RPT_vendorCode)
				.append("RPT_vendorName", RPT_vendorName)
				.append("RPT_vendorContact", RPT_vendorContact)
				.append("RPT_vendorEmail", RPT_vendorEmail)
				.append("RPT_serviceStartDate", RPT_serviceStartDate)
				.append("RPT_serviceEndDate", RPT_serviceEndDate)
				.append("RPT_landlordPaidNotificationDate",
						RPT_landlordPaidNotificationDate)
				.append("RPT_professionalCleaningDate",
						RPT_professionalCleaningDate)
				.append("RPT_comment", RPT_comment)
				.append("RPT_utilitiesShutOff", RPT_utilitiesShutOff)
				.append("RPT_moveOutDate", RPT_moveOutDate)
				.append("RPT_negotiatedDepositReturnedDate",
						RPT_negotiatedDepositReturnedDate)
				.append("RPT_damageCost", RPT_damageCost)
				.append("RPT_securityDeposit", RPT_securityDeposit)
				.append("RPT_depositReturnedTo", RPT_depositReturnedTo)
				.append("RPT_vendorCodeEXSO", RPT_vendorCodeEXSO)
				.append("SCH_schoolSelected", SCH_schoolSelected)
				.append("SCH_schoolName", SCH_schoolName)
				.append("SCH_contactPerson", SCH_contactPerson)
				.append("SCH_website", SCH_website)
				.append("SCH_serviceStartDate", SCH_serviceStartDate)
				.append("SCH_serviceEndDate", SCH_serviceEndDate)
				.append("SCH_prearrivalDiscussionBetweenProviderAndFamily",
						SCH_prearrivalDiscussionBetweenProviderAndFamily)
				.append("SCH_admissionDate", SCH_admissionDate)
				.append("SCH_city", SCH_city)
				.append("SCH_noOfChildren", SCH_noOfChildren)
				.append("SCH_vendorCodeEXSO", SCH_vendorCodeEXSO)
				.append("TAX_vendorCode", TAX_vendorCode)
				.append("TAX_vendorName", TAX_vendorName)
				.append("TAX_vendorContact", TAX_vendorContact)
				.append("TAX_vendorEmail", TAX_vendorEmail)
				.append("TAX_serviceStartDate", TAX_serviceStartDate)
				.append("TAX_serviceEndDate", TAX_serviceEndDate)
				.append("TAX_notificationdate", TAX_notificationdate)
				.append("TAX_vendorCodeEXSO", TAX_vendorCodeEXSO)
				.append("TAC_vendorCode", TAC_vendorCode)
				.append("TAC_vendorName", TAC_vendorName)
				.append("TAC_vendorContact", TAC_vendorContact)
				.append("TAC_vendorEmail", TAC_vendorEmail)
				.append("TAC_serviceStartDate", TAC_serviceStartDate)
				.append("TAC_serviceEndDate", TAC_serviceEndDate)
				.append("TAC_monthsWeeksDaysAuthorized",
						TAC_monthsWeeksDaysAuthorized)
				.append("TAC_monthlyRentalAllowance",
						TAC_monthlyRentalAllowance)
				.append("TAC_leaseStartDate", TAC_leaseStartDate)
				.append("TAC_securityDeposit", TAC_securityDeposit)
				.append("TAC_leaseExpireDate", TAC_leaseExpireDate)
				.append("TAC_expiryReminderPriorToExpiry",
						TAC_expiryReminderPriorToExpiry)
				.append("TAC_negotiatedRent", TAC_negotiatedRent)
				.append("TAC_utilitiesIncluded", TAC_utilitiesIncluded)
				.append("TAC_depositPaidBy", TAC_depositPaidBy)
				.append("TAC_comment", TAC_comment)
				.append("TAC_vendorCodeEXSO", TAC_vendorCodeEXSO)
				.append("TEN_vendorCode", TEN_vendorCode)
				.append("TEN_vendorName", TEN_vendorName)
				.append("TEN_vendorContact", TEN_vendorContact)
				.append("TEN_vendorEmail", TEN_vendorEmail)
				.append("TEN_serviceStartDate", TEN_serviceStartDate)
				.append("TEN_serviceEndDate", TEN_serviceEndDate)
				.append("TEN_vendorCodeEXSO", TEN_vendorCodeEXSO)
				.append("TEN_landlordName", TEN_landlordName)
				.append("TEN_landlordContactNumber", TEN_landlordContactNumber)
				.append("TEN_landlordEmailAddress", TEN_landlordEmailAddress)
				.append("TEN_propertyAddress", TEN_propertyAddress)
				.append("TEN_accountHolder", TEN_accountHolder)
				.append("TEN_bankName", TEN_bankName)
				.append("TEN_bankAddress", TEN_bankAddress)
				.append("TEN_bankAccountNumber", TEN_bankAccountNumber)
				.append("TEN_bankIbanNumber", TEN_bankIbanNumber)
				.append("TEN_abaNumber", TEN_abaNumber)
				.append("TEN_accountType", TEN_accountType)
				.append("TEN_leaseStartDate", TEN_leaseStartDate)
				.append("TEN_leaseExpireDate", TEN_leaseExpireDate)
				.append("TEN_expiryReminderPriorToExpiry",
						TEN_expiryReminderPriorToExpiry)
				.append("TEN_monthlyRentalAllowance",
						TEN_monthlyRentalAllowance)
				.append("TEN_securityDeposit", TEN_securityDeposit)
				.append("TEN_leaseCurrency", TEN_leaseCurrency)
				.append("TEN_leaseSignee", TEN_leaseSignee)
				.append("VIS_vendorCode", VIS_vendorCode)
				.append("VIS_vendorName", VIS_vendorName)
				.append("VIS_vendorContact", VIS_vendorContact)
				.append("VIS_vendorEmail", VIS_vendorEmail)
				.append("VIS_serviceStartDate", VIS_serviceStartDate)
				.append("VIS_serviceEndDate", VIS_serviceEndDate)
				.append("VIS_workPermitVisaHolderName",
						VIS_workPermitVisaHolderName)
				.append("VIS_providerNotificationDate",
						VIS_providerNotificationDate)
				.append("VIS_visaExpiryDate", VIS_visaExpiryDate)
				.append("VIS_workPermitExpiry", VIS_workPermitExpiry)
				.append("VIS_expiryReminder3MosPriorToExpiry",
						VIS_expiryReminder3MosPriorToExpiry)
				.append("VIS_visaStartDate", VIS_visaStartDate)
				.append("VIS_visaExtensionNeeded", VIS_visaExtensionNeeded)
				.append("VIS_assignees", VIS_assignees)
				.append("VIS_employers", VIS_employers)
				.append("VIS_convenant", VIS_convenant)
				.append("VIS_residencePermitHolderNameRP",
						VIS_residencePermitHolderNameRP)
				.append("VIS_providerNotificationDateRP",
						VIS_providerNotificationDateRP)
				.append("VIS_visaExpiryDateRP", VIS_visaExpiryDateRP)
				.append("VIS_expiryReminder3MosPriorToExpiryRP",
						VIS_expiryReminder3MosPriorToExpiryRP)
				.append("VIS_residenceExpiryRP", VIS_residenceExpiryRP)
				.append("VIS_questionnaireSentDate", VIS_questionnaireSentDate)
				.append("VIS_primaryEmail", VIS_primaryEmail)
				.append("VIS_authorizationLetter", VIS_authorizationLetter)
				.append("VIS_copyResidence", VIS_copyResidence)
				.append("VIS_vendorCodeEXSO", VIS_vendorCodeEXSO)
				.append("VIS_applicationDateWork", VIS_applicationDateWork)
				.append("VIS_permitStartDateWork", VIS_permitStartDateWork)
				.append("VIS_applicationDateResidence",
						VIS_applicationDateResidence)
				.append("VIS_permitStartDateResidence",
						VIS_permitStartDateResidence)
				.append("WOP_vendorCode", WOP_vendorCode)
				.append("WOP_vendorName", WOP_vendorName)
				.append("WOP_vendorContact", WOP_vendorContact)
				.append("WOP_vendorEmail", WOP_vendorEmail)
				.append("WOP_serviceStartDate", WOP_serviceStartDate)
				.append("WOP_serviceEndDate", WOP_serviceEndDate)
				.append("WOP_workPermitHolderName", WOP_workPermitHolderName)
				.append("WOP_providerNotificationDate",
						WOP_providerNotificationDate)
				.append("WOP_permitStartDate", WOP_permitStartDate)
				.append("WOP_permitExpiryDate", WOP_permitExpiryDate)
				.append("WOP_reminder3MosPriorToExpiry",
						WOP_reminder3MosPriorToExpiry)
				.append("WOP_questionnaireSentDate", WOP_questionnaireSentDate)
				.append("WOP_authorizationLetter", WOP_authorizationLetter)
				.append("WOP_copyResidence", WOP_copyResidence)
				.append("WOP_vendorCodeEXSO", WOP_vendorCodeEXSO)
				.append("REP_vendorCode", REP_vendorCode)
				.append("REP_vendorName", REP_vendorName)
				.append("REP_vendorContact", REP_vendorContact)
				.append("REP_vendorEmail", REP_vendorEmail)
				.append("REP_serviceStartDate", REP_serviceStartDate)
				.append("REP_serviceEndDate", REP_serviceEndDate)
				.append("REP_workPermitHolderName", REP_workPermitHolderName)
				.append("REP_providerNotificationDate",
						REP_providerNotificationDate)
				.append("REP_permitStartDate", REP_permitStartDate)
				.append("REP_permitExpiryDate", REP_permitExpiryDate)
				.append("REP_reminder3MosPriorToExpiry",
						REP_reminder3MosPriorToExpiry)
				.append("REP_questionnaireSentDate", REP_questionnaireSentDate)
				.append("REP_authorizationLetter", REP_authorizationLetter)
				.append("REP_copyResidence", REP_copyResidence)
				.append("REP_vendorCodeEXSO", REP_vendorCodeEXSO)
				.append("SET_vendorCode", SET_vendorCode)
				.append("SET_vendorName", SET_vendorName)
				.append("SET_vendorContact", SET_vendorContact)
				.append("SET_vendorEmail", SET_vendorEmail)
				.append("SET_serviceStartDate", SET_serviceStartDate)
				.append("SET_serviceEndDate", SET_serviceEndDate)
				.append("SET_parkingPermit", SET_parkingPermit)
				.append("SET_bankAccount", SET_bankAccount)
				.append("SET_cityHall", SET_cityHall)
				.append("SET_internetConn", SET_internetConn)
				.append("SET_driverLicense", SET_driverLicense)
				.append("SET_comment", SET_comment)
				.append("SET_vendorCodeEXSO", SET_vendorCodeEXSO)
				.append("CAR_comment", CAR_comment)
				.append("COL_comment", COL_comment)
				.append("TRG_comment", TRG_comment)
				.append("HOM_comment", HOM_comment)
				.append("LAN_comment", LAN_comment)
				.append("MMG_comment", MMG_comment)
				.append("PRV_comment", PRV_comment)
				.append("AIO_comment", AIO_comment)
				.append("EXP_comment", EXP_comment)
				.append("SCH_comment", SCH_comment)
				.append("TAX_comment", TAX_comment)
				.append("TEN_comment", TEN_comment)
				.append("VIS_comment", VIS_comment)
				.append("WOP_comment", WOP_comment)
				.append("REP_comment", REP_comment)
				.append("serviceCompleteDate", serviceCompleteDate)
				.append("CAR_displyOtherVendorCode", CAR_displyOtherVendorCode)
				.append("COL_displyOtherVendorCode", COL_displyOtherVendorCode)
				.append("TRG_displyOtherVendorCode", TRG_displyOtherVendorCode)
				.append("HOM_displyOtherVendorCode", HOM_displyOtherVendorCode)
				.append("RNT_displyOtherVendorCode", RNT_displyOtherVendorCode)
				.append("SET_displyOtherVendorCode", SET_displyOtherVendorCode)
				.append("LAN_displyOtherVendorCode", LAN_displyOtherVendorCode)
				.append("MMG_displyOtherVendorCode", MMG_displyOtherVendorCode)
				.append("ONG_displyOtherVendorCode", ONG_displyOtherVendorCode)
				.append("PRV_displyOtherVendorCode", PRV_displyOtherVendorCode)
				.append("AIO_displyOtherVendorCode", AIO_displyOtherVendorCode)
				.append("EXP_displyOtherVendorCode", EXP_displyOtherVendorCode)
				.append("RPT_displyOtherVendorCode", RPT_displyOtherVendorCode)
				.append("SCH_displyOtherSchoolSelected",
						SCH_displyOtherSchoolSelected)
				.append("TAX_displyOtherVendorCode", TAX_displyOtherVendorCode)
				.append("TAC_displyOtherVendorCode", TAC_displyOtherVendorCode)
				.append("TEN_displyOtherVendorCode", TEN_displyOtherVendorCode)
				.append("VIS_displyOtherVendorCode", VIS_displyOtherVendorCode)
				.append("WOP_displyOtherVendorCode", WOP_displyOtherVendorCode)
				.append("REP_displyOtherVendorCode", REP_displyOtherVendorCode)
				.append("RLS_vendorCode", RLS_vendorCode)
				.append("RLS_vendorName", RLS_vendorName)
				.append("RLS_serviceStartDate", RLS_serviceStartDate)
				.append("RLS_vendorCodeEXSO", RLS_vendorCodeEXSO)
				.append("RLS_vendorContact", RLS_vendorContact)
				.append("RLS_serviceEndDate", RLS_serviceEndDate)
				.append("RLS_vendorEmail", RLS_vendorEmail)
				.append("RLS_displyOtherVendorCode", RLS_displyOtherVendorCode)
				.append("CAT_vendorCode", CAT_vendorCode)
				.append("CAT_vendorName", CAT_vendorName)
				.append("CAT_serviceStartDate", CAT_serviceStartDate)
				.append("CAT_vendorCodeEXSO", CAT_vendorCodeEXSO)
				.append("CAT_vendorContact", CAT_vendorContact)
				.append("CAT_serviceEndDate", CAT_serviceEndDate)
				.append("CAT_vendorEmail", CAT_vendorEmail)
				.append("CAT_displyOtherVendorCode", CAT_displyOtherVendorCode)
				.append("CLS_vendorCode", CLS_vendorCode)
				.append("CLS_vendorName", CLS_vendorName)
				.append("CLS_serviceStartDate", CLS_serviceStartDate)
				.append("CLS_vendorCodeEXSO", CLS_vendorCodeEXSO)
				.append("CLS_vendorContact", CLS_vendorContact)
				.append("CLS_serviceEndDate", CLS_serviceEndDate)
				.append("CLS_vendorEmail", CLS_vendorEmail)
				.append("CLS_displyOtherVendorCode", CLS_displyOtherVendorCode)
				.append("CHS_vendorCode", CHS_vendorCode)
				.append("CHS_vendorName", CHS_vendorName)
				.append("CHS_serviceStartDate", CHS_serviceStartDate)
				.append("CHS_vendorCodeEXSO", CHS_vendorCodeEXSO)
				.append("CHS_vendorContact", CHS_vendorContact)
				.append("CHS_serviceEndDate", CHS_serviceEndDate)
				.append("CHS_vendorEmail", CHS_vendorEmail)
				.append("CHS_displyOtherVendorCode", CHS_displyOtherVendorCode)
				.append("DPS_vendorCode", DPS_vendorCode)
				.append("DPS_vendorName", DPS_vendorName)
				.append("DPS_serviceStartDate", DPS_serviceStartDate)
				.append("DPS_vendorCodeEXSO", DPS_vendorCodeEXSO)
				.append("DPS_vendorContact", DPS_vendorContact)
				.append("DPS_serviceEndDate", DPS_serviceEndDate)
				.append("DPS_vendorEmail", DPS_vendorEmail)
				.append("DPS_displyOtherVendorCode", DPS_displyOtherVendorCode)
				.append("HSM_vendorCode", HSM_vendorCode)
				.append("HSM_vendorName", HSM_vendorName)
				.append("HSM_serviceStartDate", HSM_serviceStartDate)
				.append("HSM_vendorCodeEXSO", HSM_vendorCodeEXSO)
				.append("HSM_vendorContact", HSM_vendorContact)
				.append("HSM_serviceEndDate", HSM_serviceEndDate)
				.append("HSM_vendorEmail", HSM_vendorEmail)
				.append("HSM_displyOtherVendorCode", HSM_displyOtherVendorCode)
				.append("PDT_vendorCode", PDT_vendorCode)
				.append("PDT_vendorName", PDT_vendorName)
				.append("PDT_serviceStartDate", PDT_serviceStartDate)
				.append("PDT_vendorCodeEXSO", PDT_vendorCodeEXSO)
				.append("PDT_vendorContact", PDT_vendorContact)
				.append("PDT_serviceEndDate", PDT_serviceEndDate)
				.append("PDT_vendorEmail", PDT_vendorEmail)
				.append("PDT_displyOtherVendorCode", PDT_displyOtherVendorCode)
				.append("RCP_vendorCode", RCP_vendorCode)
				.append("RCP_vendorName", RCP_vendorName)
				.append("RCP_serviceStartDate", RCP_serviceStartDate)
				.append("RCP_vendorCodeEXSO", RCP_vendorCodeEXSO)
				.append("RCP_vendorContact", RCP_vendorContact)
				.append("RCP_serviceEndDate", RCP_serviceEndDate)
				.append("RCP_vendorEmail", RCP_vendorEmail)
				.append("RCP_displyOtherVendorCode", RCP_displyOtherVendorCode)
				.append("SPA_vendorCode", SPA_vendorCode)
				.append("SPA_vendorName", SPA_vendorName)
				.append("SPA_serviceStartDate", SPA_serviceStartDate)
				.append("SPA_vendorCodeEXSO", SPA_vendorCodeEXSO)
				.append("SPA_vendorContact", SPA_vendorContact)
				.append("SPA_serviceEndDate", SPA_serviceEndDate)
				.append("SPA_vendorEmail", SPA_vendorEmail)
				.append("SPA_displyOtherVendorCode", SPA_displyOtherVendorCode).append("SPA_vendorInitiation", SPA_vendorInitiation)
				.append("TCS_vendorCode", TCS_vendorCode)
				.append("TCS_vendorName", TCS_vendorName)
				.append("TCS_serviceStartDate", TCS_serviceStartDate)
				.append("TCS_vendorCodeEXSO", TCS_vendorCodeEXSO)
				.append("TCS_vendorContact", TCS_vendorContact)
				.append("TCS_serviceEndDate", TCS_serviceEndDate)
				.append("TCS_vendorEmail", TCS_vendorEmail)
				.append("TCS_displyOtherVendorCode", TCS_displyOtherVendorCode)
				.append("mailServiceType", mailServiceType)
				.append("RNT_monthlyRentalAllowanceCurrency",
						RNT_monthlyRentalAllowanceCurrency)
				.append("RNT_securityDepositCurrency",
						RNT_securityDepositCurrency)
				.append("MTS_vendorCode", MTS_vendorCode)
				.append("MTS_vendorName", MTS_vendorName)
				.append("MTS_serviceStartDate", MTS_serviceStartDate)
				.append("MTS_vendorCodeEXSO", MTS_vendorCodeEXSO)
				.append("MTS_vendorContact", MTS_vendorContact)
				.append("MTS_serviceEndDate", MTS_serviceEndDate)
				.append("MTS_vendorEmail", MTS_vendorEmail)
				.append("MTS_paymentResponsibility", MTS_paymentResponsibility)
				.append("MTS_displyOtherVendorCode", MTS_displyOtherVendorCode)
				.append("RLS_paymentResponsibility", RLS_paymentResponsibility)
				.append("CAT_paymentResponsibility", CAT_paymentResponsibility)
				.append("CLS_paymentResponsibility", CLS_paymentResponsibility)
				.append("CHS_paymentResponsibility", CHS_paymentResponsibility)
				.append("DPS_paymentResponsibility", DPS_paymentResponsibility)
				.append("HSM_paymentResponsibility", HSM_paymentResponsibility)
				.append("PDT_paymentResponsibility", PDT_paymentResponsibility)
				.append("RCP_paymentResponsibility", RCP_paymentResponsibility)
				.append("SPA_paymentResponsibility", SPA_paymentResponsibility)
				.append("TCS_paymentResponsibility", TCS_paymentResponsibility)
				.append("DPS_serviceType", DPS_serviceType)
				.append("DPS_completionDate", DPS_completionDate)
				.append("DPS_comment", DPS_comment)
				.append("HSM_brokerCode", HSM_brokerCode)
				.append("HSM_brokerName", HSM_brokerName)
				.append("HSM_brokerContact", HSM_brokerContact)
				.append("HSM_brokerEmail", HSM_brokerEmail)
				.append("HSM_status", HSM_status)
				.append("HSM_comment", HSM_comment)
				.append("HSM_marketingPlanNBMAReceived",
						HSM_marketingPlanNBMAReceived)
				.append("PDT_serviceType", PDT_serviceType)
				.append("PDT_budget", PDT_budget)
				.append("RCP_estimateCost", RCP_estimateCost)
				.append("RCP_actualCost", RCP_actualCost)
				.append("MTS_lender", MTS_lender)
				.append("MTS_initialContact", MTS_initialContact)
				.append("MTS_status", MTS_status)
				.append("MTS_mortgageAmount", MTS_mortgageAmount)
				.append("MTS_mortgageRate", MTS_mortgageRate)
				.append("MTS_mortgageTerm", MTS_mortgageTerm)
				.append("COL_estimatedTaxAllowance", COL_estimatedTaxAllowance)
				.append("COL_estimatedHousingAllowance",
						COL_estimatedHousingAllowance)
				.append("COL_estimatedTransportationAllowance",
						COL_estimatedTransportationAllowance)
				.append("RNT_leaseSignee", RNT_leaseSignee)
				.append("RNT_leaseExtensionNeeded", RNT_leaseExtensionNeeded)
				.append("SET_governmentID", SET_governmentID)
				.append("SET_heathCare", SET_heathCare)
				.append("SET_utilities", SET_utilities)
				.append("SET_automobileRegistration",
						SET_automobileRegistration)
				.append("SET_dayofServiceAuthorized",
						SET_dayofServiceAuthorized)
				.append("SET_initialDateofServiceRequested",
						SET_initialDateofServiceRequested)
				.append("SET_providerNotificationSent",
						SET_providerNotificationSent)
				.append("SET_providerConfirmationReceived",
						SET_providerConfirmationReceived)
				.append("TAX_allowance", TAX_allowance)
				.append("VIS_immigrationStatus", VIS_immigrationStatus)
				.append("VIS_arrivalDate", VIS_arrivalDate)
				.append("VIS_portofEntry", VIS_portofEntry)
				.append("DSS_vendorCode", DSS_vendorCode)
				.append("DSS_vendorName", DSS_vendorName)
				.append("DSS_serviceStartDate", DSS_serviceStartDate)
				.append("DSS_vendorCodeEXSO", DSS_vendorCodeEXSO)
				.append("DSS_vendorContact", DSS_vendorContact)
				.append("DSS_serviceEndDate", DSS_serviceEndDate)
				.append("DSS_vendorEmail", DSS_vendorEmail)
				.append("DSS_paymentResponsibility", DSS_paymentResponsibility)
				.append("DSS_displyOtherVendorCode", DSS_displyOtherVendorCode)
				.append("DSS_initialContactDate", DSS_initialContactDate)
				.append("PRV_paymentResponsibility", PRV_paymentResponsibility)
				.append("VIS_paymentResponsibility", VIS_paymentResponsibility)
				.append("TAX_paymentResponsibility", TAX_paymentResponsibility)
				.append("RNT_paymentResponsibility", RNT_paymentResponsibility)
				.append("AIO_paymentResponsibility", AIO_paymentResponsibility)
				.append("TRG_paymentResponsibility", TRG_paymentResponsibility)
				.append("LAN_paymentResponsibility", LAN_paymentResponsibility)
				.append("RPT_paymentResponsibility", RPT_paymentResponsibility)
				.append("COL_paymentResponsibility", COL_paymentResponsibility)
				.append("TAC_paymentResponsibility", TAC_paymentResponsibility)
				.append("HOM_paymentResponsibility", HOM_paymentResponsibility)
				.append("SCH_paymentResponsibility", SCH_paymentResponsibility)
				.append("EXP_paymentResponsibility", EXP_paymentResponsibility)
				.append("ONG_paymentResponsibility", ONG_paymentResponsibility)
				.append("TEN_paymentResponsibility", TEN_paymentResponsibility)
				.append("MMG_paymentResponsibility", MMG_paymentResponsibility)
				.append("CAR_paymentResponsibility", CAR_paymentResponsibility)
				.append("SET_paymentResponsibility", SET_paymentResponsibility)
				.append("WOP_paymentResponsibility", WOP_paymentResponsibility)
				.append("REP_paymentResponsibility", REP_paymentResponsibility)
				.append("HSM_agentCode", HSM_agentCode)
				.append("HSM_agentName", HSM_agentName)
				.append("HSM_brokerage", HSM_brokerage)
				.append("HSM_agentPhone", HSM_agentPhone)
				.append("HSM_agentEmail", HSM_agentEmail)
				.append("HSM_estimatedHSRReferral", HSM_estimatedHSRReferral)
				.append("HSM_actualHSRReferral", HSM_actualHSRReferral)
				.append("reminderServices", reminderServices)
				.append("CAT_comment", CAT_comment)
				.append("CLS_comment", CLS_comment)
				.append("CHS_comment", CHS_comment)
				.append("PDT_comment", PDT_comment)
				.append("RCP_comment", RCP_comment)
				.append("SPA_comment", SPA_comment)
				.append("TCS_comment", TCS_comment)
				.append("MTS_comment", MTS_comment)
				.append("DSS_comment", DSS_comment)
				.append("HOM_estimatedHSRReferral", HOM_estimatedHSRReferral)
				.append("HOM_actualHSRReferral", HOM_actualHSRReferral)
				.append("HSM_offerDate", HSM_offerDate)
				.append("HSM_closingDate", HSM_closingDate)
				.append("TAC_timeAuthorized", TAC_timeAuthorized)
				.append("MMG_originAgentCode", MMG_originAgentCode)
				.append("MMG_originAgentName", MMG_originAgentName)
				.append("MMG_originAgentPhone", MMG_originAgentPhone)
				.append("MMG_originAgentEmail", MMG_originAgentEmail)
				.append("MMG_destinationAgentCode", MMG_destinationAgentCode)
				.append("MMG_destinationAgentName", MMG_destinationAgentName)
				.append("MMG_destinationAgentPhone", MMG_destinationAgentPhone)
				.append("MMG_destinationAgentEmail", MMG_destinationAgentEmail)
				.append("PRV_emailSent", PRV_emailSent)
				.append("TAX_emailSent", TAX_emailSent)
				.append("AIO_emailSent", AIO_emailSent)
				.append("TRG_emailSent", TRG_emailSent)
				.append("LAN_emailSent", LAN_emailSent)
				.append("RPT_emailSent", RPT_emailSent)
				.append("COL_emailSent", COL_emailSent)
				.append("TAC_emailSent", TAC_emailSent)
				.append("HOM_emailSent", HOM_emailSent)
				.append("SCH_emailSent", SCH_emailSent)
				.append("EXP_emailSent", EXP_emailSent)
				.append("ONG_emailSent", ONG_emailSent)
				.append("TEN_emailSent", TEN_emailSent)
				.append("MMG_orginEmailSent", MMG_orginEmailSent)
				.append("MMG_destinationEmailSent", MMG_destinationEmailSent)
				.append("CAR_emailSent", CAR_emailSent)
				.append("SET_emailSent", SET_emailSent)
				.append("CAT_emailSent", CAT_emailSent)
				.append("CLS_emailSent", CLS_emailSent)
				.append("CHS_emailSent", CHS_emailSent)
				.append("DPS_emailSent", DPS_emailSent)
				.append("HSM_emailSent", HSM_emailSent)
				.append("PDT_emailSent", PDT_emailSent)
				.append("RCP_emailSent", RCP_emailSent)
				.append("SPA_emailSent", SPA_emailSent)
				.append("TCS_emailSent", TCS_emailSent)
				.append("MTS_emailSent", MTS_emailSent)
				.append("DSS_emailSent", DSS_emailSent)
				.append("VIS_emailSent", VIS_emailSent)
				.append("RNT_emailSent", RNT_emailSent)
				.append("NET_emailSent", NET_emailSent)
				.append("WOP_emailSent", WOP_emailSent)
				.append("REP_emailSent", REP_emailSent)
				.append("resendEmailServiceName", resendEmailServiceName)
				.append("HOB_startDate", HOB_startDate)
				.append("HOB_endDate", HOB_endDate)
				.append("HOB_hotelName", HOB_hotelName)
				.append("HOB_city", HOB_city)
				.append("FLB_arrivalDate", FLB_arrivalDate)
				.append("FLB_departureDate", FLB_departureDate)
				.append("FLB_additionalBookingReminderDate",
						FLB_additionalBookingReminderDate)
				.append("FLB_arrivalDate1", FLB_arrivalDate1)
				.append("FLB_departureDate1", FLB_departureDate1)
				.append("FLB_additionalBookingReminderDate1",
						FLB_additionalBookingReminderDate1)
				.append("FLB_addOn", FLB_addOn)
				.append("HOB_vendorCode", HOB_vendorCode)
				.append("HOB_vendorName", HOB_vendorName)
				.append("HOB_vendorContact", HOB_vendorContact)
				.append("HOB_vendorEmail", HOB_vendorEmail)
				.append("HOB_serviceStartDate", HOB_serviceStartDate)
				.append("HOB_serviceEndDate", HOB_serviceEndDate)
				.append("HOB_vendorCodeEXSO", HOB_vendorCodeEXSO)
				.append("FLB_vendorCode", FLB_vendorCode)
				.append("FLB_vendorName", FLB_vendorName)
				.append("FLB_vendorContact", FLB_vendorContact)
				.append("FLB_vendorEmail", FLB_vendorEmail)
				.append("FLB_serviceStartDate", FLB_serviceStartDate)
				.append("FLB_serviceEndDate", FLB_serviceEndDate)
				.append("FLB_vendorCodeEXSO", FLB_vendorCodeEXSO)
				.append("FLB_emailSent", FLB_emailSent)
				.append("HOB_emailSent", HOB_emailSent)
				.append("HOB_displyOtherVendorCode", HOB_displyOtherVendorCode)
				.append("FLB_displyOtherVendorCode", FLB_displyOtherVendorCode)
				.append("FRL_vendorCode", FRL_vendorCode)
				.append("FRL_vendorName", FRL_vendorName)
				.append("FRL_serviceStartDate", FRL_serviceStartDate)
				.append("FRL_vendorCodeEXSO", FRL_vendorCodeEXSO)
				.append("FRL_vendorContact", FRL_vendorContact)
				.append("FRL_serviceEndDate", FRL_serviceEndDate)
				.append("FRL_emailSent", FRL_emailSent)
				.append("FRL_vendorEmail", FRL_vendorEmail)
				.append("FRL_displyOtherVendorCode", FRL_displyOtherVendorCode)
				.append("FRL_paymentResponsibility", FRL_paymentResponsibility)
				.append("FRL_rentalRate", FRL_rentalRate)
				.append("FRL_deposit", FRL_deposit)
				.append("FRL_rentalCurrency", FRL_rentalCurrency)
				.append("FRL_depositCurrency", FRL_depositCurrency)
				.append("FRL_month", FRL_month)
				.append("FRL_rentalStartDate", FRL_rentalStartDate)
				.append("FRL_rentalEndDate", FRL_rentalEndDate)
				.append("FRL_terminationNotice", FRL_terminationNotice)
				.append("FRL_comment", FRL_comment)
				.append("APU_vendorCode", APU_vendorCode)
				.append("APU_vendorName", APU_vendorName)
				.append("APU_serviceStartDate", APU_serviceStartDate)
				.append("APU_vendorCodeEXSO", APU_vendorCodeEXSO)
				.append("APU_vendorContact", APU_vendorContact)
				.append("APU_serviceEndDate", APU_serviceEndDate)
				.append("APU_emailSent", APU_emailSent)
				.append("APU_vendorEmail", APU_vendorEmail)
				.append("APU_displyOtherVendorCode", APU_displyOtherVendorCode)
				.append("APU_paymentResponsibility", APU_paymentResponsibility)
				.append("APU_flightDetails", APU_flightDetails)
				.append("APU_driverName", APU_driverName)
				.append("APU_driverPhoneNumber", APU_driverPhoneNumber)
				.append("APU_arrivalDate", APU_arrivalDate)
				.append("APU_carMake", APU_carMake)
				.append("APU_carModel", APU_carModel)
				.append("APU_carColor", APU_carColor)
				.append("WOP_leaseExNeeded", WOP_leaseExNeeded)
				.append("REP_leaseExNeeded", REP_leaseExNeeded)
				.append("TEN_propertyName", TEN_propertyName)
				.append("TEN_city", TEN_city)
				.append("TEN_zipCode", TEN_zipCode)
				.append("TEN_addressLine1", TEN_addressLine1)
				.append("TEN_addressLine2", TEN_addressLine2)
				.append("TEN_state", TEN_state)
				.append("TEN_country", TEN_country)
				.append("TEN_leasedBy", TEN_leasedBy)
				.append("TEN_rentAmount", TEN_rentAmount)
				.append("TEN_rentCurrency", TEN_rentCurrency)
				.append("TEN_rentAllowance", TEN_rentAllowance)
				.append("TEN_allowanceCurrency", TEN_allowanceCurrency)
				.append("TEN_utilitiesIncluded", TEN_utilitiesIncluded)
				.append("TEN_rentPaidTo", TEN_rentPaidTo)
				.append("TEN_rentalIncreaseDate", TEN_rentalIncreaseDate)
				.append("TEN_rentalComment", TEN_rentalComment)
				.append("TEN_followUpNeeded", TEN_followUpNeeded)
				.append("TEN_termOfNotice", TEN_termOfNotice)
				.append("TEN_checkInMoveIn", TEN_checkInMoveIn)
				.append("TEN_preCheckOut", TEN_preCheckOut)
				.append("TEN_checkOutMoveOut", TEN_checkOutMoveOut)
				.append("TEN_swiftCode", TEN_swiftCode)
				.append("TEN_paymentDescription", TEN_paymentDescription)
				.append("TEN_exceptionComments", TEN_exceptionComments)
				.append("TEN_depositAmount", TEN_depositAmount)
				.append("TEN_depositReturned", TEN_depositReturned)
				.append("TEN_depositCurrency", TEN_depositCurrency)
				.append("TEN_depositReturnedAmount", TEN_depositReturnedAmount)
				.append("TEN_depositPaidBy", TEN_depositPaidBy)
				.append("TEN_depositReturnedCurrency", TEN_depositReturnedCurrency)
				.append("TEN_depositComment", TEN_depositComment)
				.append("TEN_refundableTo", TEN_refundableTo)
				.append("TEN_Gas_Water", TEN_Gas_Water)
				.append("TEN_TV_Internet_Phone", TEN_TV_Internet_Phone)
				.append("TEN_furnitureRental", TEN_furnitureRental)
				.append("TEN_cleaningServices", TEN_cleaningServices)
				.append("TEN_parkingPermit", TEN_parkingPermit)
				.append("TEN_communityTax", TEN_communityTax)
				.append("TEN_insurance", TEN_insurance)
				.append("TEN_gardenMaintenance", TEN_gardenMaintenance)
				.append("TEN_toBePaidBy", TEN_toBePaidBy)
				.append("TEN_assigneeContributionAmount", TEN_assigneeContributionAmount)
				.append("TEN_directDebit", TEN_directDebit)
				.append("TEN_description", TEN_description)
				.append("TEN_IBAN_BankAccountNumber", TEN_IBAN_BankAccountNumber)
				.append("TEN_BIC_SWIFT", TEN_BIC_SWIFT)
				.append("TEN_assigneeContributionCurrency", TEN_assigneeContributionCurrency)
				.append("TEN_mobilePhone", TEN_mobilePhone)
				.append("TEN_Utility_Gas_Water", TEN_Utility_Gas_Water)
				.append("TEN_Utility_TV_Internet_Phone", TEN_Utility_TV_Internet_Phone)
				.append("TEN_Utility_mobilePhone", TEN_Utility_mobilePhone)
				.append("TEN_Utility_furnitureRental", TEN_Utility_furnitureRental)
				.append("TEN_Utility_cleaningServices", TEN_Utility_cleaningServices)
				.append("TEN_Utility_parkingPermit", TEN_Utility_parkingPermit)
				.append("TEN_Utility_communityTax", TEN_Utility_communityTax)
				.append("TEN_Utility_insurance", TEN_Utility_insurance)
				.append("TEN_Utility_gardenMaintenance", TEN_Utility_gardenMaintenance)
				.append("TEN_contributionAmount", TEN_contributionAmount)
				.append("TEN_Gas", TEN_Gas)
				.append("TEN_Electricity", TEN_Electricity)
				.append("TEN_Miscellaneous", TEN_Miscellaneous)
				.append("TEN_Utility_Gas", TEN_Utility_Gas)
				.append("TEN_Utility_Electricity", TEN_Utility_Electricity)
				.append("TEN_Utility_Miscellaneous", TEN_Utility_Miscellaneous)
				.append("TEN_exceptionAddedValue", TEN_exceptionAddedValue)
				.append("TEN_Gas_Electric", TEN_Gas_Electric)
				.append("TEN_Utility_Gas_Electric", TEN_Utility_Gas_Electric)
				.append("TEN_housingRentVatDesc", TEN_housingRentVatDesc)
				.append("TEN_billThroughDate", TEN_billThroughDate)
				.append("TEN_housingRentVatPercent", TEN_housingRentVatPercent)
				.append("INS_vendorCode", INS_vendorCode).append("INS_vendorName", INS_vendorName).append("INS_vendorCodeEXSO", INS_vendorCodeEXSO)
				.append("INS_vendorContact", INS_vendorContact).append("INS_vendorEmail", INS_vendorEmail).append("INS_comment", INS_comment)
				.append("INS_vendorInitiation", INS_vendorInitiation).append("INS_serviceStartDate", INS_serviceStartDate).append("INS_serviceEndDate", INS_serviceEndDate)
				.append("INP_vendorCode", INP_vendorCode).append("INP_vendorName", INP_vendorName).append("INP_vendorCodeEXSO", INP_vendorCodeEXSO)
				.append("INP_vendorContact", INP_vendorContact).append("INP_vendorEmail", INP_vendorEmail).append("INP_comment", INP_comment)
				.append("INP_vendorInitiation", INP_vendorInitiation).append("INP_serviceStartDate", INP_serviceStartDate).append("INP_serviceEndDate", INP_serviceEndDate)
				.append("EDA_vendorCode", EDA_vendorCode).append("EDA_vendorName", EDA_vendorName).append("EDA_vendorCodeEXSO", EDA_vendorCodeEXSO)
				.append("EDA_vendorContact", EDA_vendorContact).append("EDA_vendorEmail", EDA_vendorEmail).append("EDA_comment", EDA_comment)
				.append("EDA_vendorInitiation", EDA_vendorInitiation).append("EDA_serviceStartDate", EDA_serviceStartDate).append("EDA_serviceEndDate", EDA_serviceEndDate)
				.append("TAS_vendorCode", TAS_vendorCode).append("TAS_vendorName", TAS_vendorName).append("TAS_vendorCodeEXSO", TAS_vendorCodeEXSO)
				.append("TAS_vendorContact", TAS_vendorContact).append("TAS_vendorEmail", TAS_vendorEmail).append("TAS_comment", TAS_comment)
				.append("TAS_vendorInitiation", TAS_vendorInitiation).append("TAS_serviceStartDate", TAS_serviceStartDate).append("TAS_serviceEndDate", TAS_serviceEndDate)
				.append("totalDspDays", totalDspDays)
				.append("RNT_rsfReceived", RNT_rsfReceived)
				.append("RNT_moveInspection", RNT_moveInspection)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DspDetails))
			return false;
		DspDetails castOther = (DspDetails) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(shipNumber, castOther.shipNumber)
				.append(CAR_vendorCode, castOther.CAR_vendorCode)
				.append(CAR_vendorName, castOther.CAR_vendorName)
				.append(CAR_vendorContact, castOther.CAR_vendorContact)
				.append(CAR_vendorEmail, castOther.CAR_vendorEmail)
				.append(CAR_serviceStartDate, castOther.CAR_serviceStartDate)
				.append(CAR_serviceEndDate, castOther.CAR_serviceEndDate)
				.append(CAR_vendorCodeEXSO, castOther.CAR_vendorCodeEXSO)
				.append(CAR_notificationDate, castOther.CAR_notificationDate)
				.append(CAR_pickUpDate, castOther.CAR_pickUpDate)
				.append(CAR_registrationDate, castOther.CAR_registrationDate)
				.append(CAR_deliveryDate, castOther.CAR_deliveryDate)
				.append(COL_vendorCode, castOther.COL_vendorCode)
				.append(COL_vendorName, castOther.COL_vendorName)
				.append(COL_vendorContact, castOther.COL_vendorContact)
				.append(COL_vendorEmail, castOther.COL_vendorEmail)
				.append(COL_serviceStartDate, castOther.COL_serviceStartDate)
				.append(COL_serviceEndDate, castOther.COL_serviceEndDate)
				.append(COL_notificationDate, castOther.COL_notificationDate)
				.append(COL_vendorCodeEXSO, castOther.COL_vendorCodeEXSO)
				.append(COL_estimatedAllowanceEuro,
						castOther.COL_estimatedAllowanceEuro)
				.append(TRG_vendorCode, castOther.TRG_vendorCode)
				.append(TRG_vendorName, castOther.TRG_vendorName)
				.append(TRG_vendorContact, castOther.TRG_vendorContact)
				.append(TRG_vendorEmail, castOther.TRG_vendorEmail)
				.append(TRG_serviceStartDate, castOther.TRG_serviceStartDate)
				.append(TRG_serviceEndDate, castOther.TRG_serviceEndDate)
				.append(TRG_employeeDaysAuthorized,
						castOther.TRG_employeeDaysAuthorized)
				.append(TRG_spouseDaysAuthorized,
						castOther.TRG_spouseDaysAuthorized)
				.append(TRG_employeeTime, castOther.TRG_employeeTime)
				.append(TRG_spouseTime, castOther.TRG_spouseTime)
				.append(TRG_vendorCodeEXSO, castOther.TRG_vendorCodeEXSO)
				.append(HOM_vendorCode, castOther.HOM_vendorCode)
				.append(HOM_vendorName, castOther.HOM_vendorName)
				.append(HOM_vendorContact, castOther.HOM_vendorContact)
				.append(HOM_vendorEmail, castOther.HOM_vendorEmail)
				.append(HOM_serviceStartDate, castOther.HOM_serviceStartDate)
				.append(HOM_serviceEndDate, castOther.HOM_serviceEndDate)
				.append(HOM_viewStatus, castOther.HOM_viewStatus)
				.append(HOM_moveInDate, castOther.HOM_moveInDate)
				.append(HOM_vendorCodeEXSO, castOther.HOM_vendorCodeEXSO)
				.append(HOM_agentCode, castOther.HOM_agentCode)
				.append(HOM_agentName, castOther.HOM_agentName)
				.append(HOM_initialContactDate,
						castOther.HOM_initialContactDate)
				.append(HOM_agentPhone, castOther.HOM_agentPhone)
				.append(HOM_houseHuntingTripArrival,
						castOther.HOM_houseHuntingTripArrival)
				.append(HOM_agentEmail, castOther.HOM_agentEmail)
				.append(HOM_houseHuntingTripDeparture,
						castOther.HOM_houseHuntingTripDeparture)
				.append(HOM_brokerage, castOther.HOM_brokerage)
				.append(HOM_offerDate, castOther.HOM_offerDate)
				.append(HOM_closingDate, castOther.HOM_closingDate)
				.append(RNT_vendorCode, castOther.RNT_vendorCode)
				.append(RNT_vendorName, castOther.RNT_vendorName)
				.append(RNT_vendorContact, castOther.RNT_vendorContact)
				.append(RNT_vendorEmail, castOther.RNT_vendorEmail)
				.append(RNT_serviceStartDate, castOther.RNT_serviceStartDate)
				.append(RNT_serviceEndDate, castOther.RNT_serviceEndDate)
				.append(RNT_leaseStartDate, castOther.RNT_leaseStartDate)
				.append(RNT_leaseExpireDate, castOther.RNT_leaseExpireDate)
				.append(RNT_monthlyRentalAllowance,
						castOther.RNT_monthlyRentalAllowance)
				.append(RNT_negotiatedRent, castOther.RNT_negotiatedRent)
				.append(RNT_utilitiesIncluded, castOther.RNT_utilitiesIncluded)
				.append(RNT_securityDeposit, castOther.RNT_securityDeposit)
				.append(RNT_reminderDate, castOther.RNT_reminderDate)
				.append(RNT_checkInDate, castOther.RNT_checkInDate)
				.append(RNT_depositPaidBy, castOther.RNT_depositPaidBy)
				.append(RNT_comment, castOther.RNT_comment)
				.append(RNT_paidCurrency, castOther.RNT_paidCurrency)
				.append(RNT_vendorCodeEXSO, castOther.RNT_vendorCodeEXSO)
				.append(RNT_homeSearchType, castOther.RNT_homeSearchType)
				.append(LAN_vendorCode, castOther.LAN_vendorCode)
				.append(LAN_vendorName, castOther.LAN_vendorName)
				.append(LAN_vendorContact, castOther.LAN_vendorContact)
				.append(LAN_vendorEmail, castOther.LAN_vendorEmail)
				.append(LAN_serviceStartDate, castOther.LAN_serviceStartDate)
				.append(LAN_serviceEndDate, castOther.LAN_serviceEndDate)
				.append(LAN_notificationDate, castOther.LAN_notificationDate)
				.append(LAN_employeeDaysAuthorized,
						castOther.LAN_employeeDaysAuthorized)
				.append(LAN_spouseDaysAuthorized,
						castOther.LAN_spouseDaysAuthorized)
				.append(LAN_childrenDaysAuthorized,
						castOther.LAN_childrenDaysAuthorized)
				.append(LAN_employeeTime, castOther.LAN_employeeTime)
				.append(LAN_spouseTime, castOther.LAN_spouseTime)
				.append(LAN_childrenTime, castOther.LAN_childrenTime)
				.append(LAN_vendorCodeEXSO, castOther.LAN_vendorCodeEXSO)
				.append(MMG_vendorCode, castOther.MMG_vendorCode)
				.append(MMG_vendorName, castOther.MMG_vendorName)
				.append(MMG_vendorContact, castOther.MMG_vendorContact)
				.append(MMG_vendorEmail, castOther.MMG_vendorEmail)
				.append(MMG_serviceStartDate, castOther.MMG_serviceStartDate)
				.append(MMG_serviceEndDate, castOther.MMG_serviceEndDate)
				.append(MMG_vendorCodeEXSO, castOther.MMG_vendorCodeEXSO)
				.append(MMG_vendorInitiation, castOther.MMG_vendorInitiation)
				.append(ONG_vendorCode, castOther.ONG_vendorCode)
				.append(ONG_vendorName, castOther.ONG_vendorName)
				.append(ONG_vendorContact, castOther.ONG_vendorContact)
				.append(ONG_vendorEmail, castOther.ONG_vendorEmail)
				.append(ONG_serviceStartDate, castOther.ONG_serviceStartDate)
				.append(ONG_serviceEndDate, castOther.ONG_serviceEndDate)
				.append(ONG_dateType1, castOther.ONG_dateType1)
				.append(ONG_dateType2, castOther.ONG_dateType2)
				.append(ONG_dateType3, castOther.ONG_dateType3)
				.append(ONG_date1, castOther.ONG_date1)
				.append(ONG_date2, castOther.ONG_date2)
				.append(ONG_date3, castOther.ONG_date3)
				.append(ONG_comment, castOther.ONG_comment)
				.append(ONG_vendorCodeEXSO, castOther.ONG_vendorCodeEXSO)
				.append(PRV_vendorCode, castOther.PRV_vendorCode)
				.append(PRV_vendorName, castOther.PRV_vendorName)
				.append(PRV_vendorContact, castOther.PRV_vendorContact)
				.append(PRV_vendorEmail, castOther.PRV_vendorEmail)
				.append(PRV_serviceStartDate, castOther.PRV_serviceStartDate)
				.append(PRV_serviceEndDate, castOther.PRV_serviceEndDate)
				.append(PRV_travelPlannerNotification,
						castOther.PRV_travelPlannerNotification)
				.append(PRV_travelDeparture, castOther.PRV_travelDeparture)
				.append(PRV_travelArrival, castOther.PRV_travelArrival)
				.append(PRV_hotelDetails, castOther.PRV_hotelDetails)
				.append(PRV_providerNotificationAndServicesAuthorized,
						castOther.PRV_providerNotificationAndServicesAuthorized)
				.append(PRV_meetGgreetAtAirport,
						castOther.PRV_meetGgreetAtAirport)
				.append(PRV_areaOrientationDays,
						castOther.PRV_areaOrientationDays)
				.append(PRV_flightNumber, castOther.PRV_flightNumber)
				.append(PRV_flightArrivalTime, castOther.PRV_flightArrivalTime)
				.append(PRV_vendorCodeEXSO, castOther.PRV_vendorCodeEXSO)
				.append(AIO_airlineFlight, castOther.AIO_airlineFlight)
				.append(AIO_contactInformation,
						castOther.AIO_contactInformation)
				.append(AIO_flightArrival, castOther.AIO_flightArrival)
				.append(AIO_flightDeparture, castOther.AIO_flightDeparture)
				.append(AIO_initialDateOfServiceRequested,
						castOther.AIO_initialDateOfServiceRequested)
				.append(AIO_prearrivalDiscussionOfProviderAndFamily,
						castOther.AIO_prearrivalDiscussionOfProviderAndFamily)
				.append(AIO_providerConfirmationReceived,
						castOther.AIO_providerConfirmationReceived)
				.append(AIO_providerNotificationSent,
						castOther.AIO_providerNotificationSent)
				.append(AIO_serviceAuthorized, castOther.AIO_serviceAuthorized)
				.append(AIO_vendorCode, castOther.AIO_vendorCode)
				.append(AIO_vendorName, castOther.AIO_vendorName)
				.append(AIO_vendorContact, castOther.AIO_vendorContact)
				.append(AIO_vendorEmail, castOther.AIO_vendorEmail)
				.append(AIO_serviceStartDate, castOther.AIO_serviceStartDate)
				.append(AIO_serviceEndDate, castOther.AIO_serviceEndDate)
				.append(AIO_vendorCodeEXSO, castOther.AIO_vendorCodeEXSO)
				.append(EXP_vendorCode, castOther.EXP_vendorCode)
				.append(EXP_vendorName, castOther.EXP_vendorName)
				.append(EXP_vendorContact, castOther.EXP_vendorContact)
				.append(EXP_vendorEmail, castOther.EXP_vendorEmail)
				.append(EXP_serviceStartDate, castOther.EXP_serviceStartDate)
				.append(EXP_serviceEndDate, castOther.EXP_serviceEndDate)
				.append(EXP_paidStatus, castOther.EXP_paidStatus)
				.append(EXP_providerNotification,
						castOther.EXP_providerNotification)
				.append(EXP_vendorCodeEXSO, castOther.EXP_vendorCodeEXSO)
				.append(RPT_vendorCode, castOther.RPT_vendorCode)
				.append(RPT_vendorName, castOther.RPT_vendorName)
				.append(RPT_vendorContact, castOther.RPT_vendorContact)
				.append(RPT_vendorEmail, castOther.RPT_vendorEmail)
				.append(RPT_serviceStartDate, castOther.RPT_serviceStartDate)
				.append(RPT_serviceEndDate, castOther.RPT_serviceEndDate)
				.append(RPT_landlordPaidNotificationDate,
						castOther.RPT_landlordPaidNotificationDate)
				.append(RPT_professionalCleaningDate,
						castOther.RPT_professionalCleaningDate)
				.append(RPT_comment, castOther.RPT_comment)
				.append(RPT_utilitiesShutOff, castOther.RPT_utilitiesShutOff)
				.append(RPT_moveOutDate, castOther.RPT_moveOutDate)
				.append(RPT_negotiatedDepositReturnedDate,
						castOther.RPT_negotiatedDepositReturnedDate)
				.append(RPT_damageCost, castOther.RPT_damageCost)
				.append(RPT_securityDeposit, castOther.RPT_securityDeposit)
				.append(RPT_depositReturnedTo, castOther.RPT_depositReturnedTo)
				.append(RPT_vendorCodeEXSO, castOther.RPT_vendorCodeEXSO)
				.append(SCH_schoolSelected, castOther.SCH_schoolSelected)
				.append(SCH_schoolName, castOther.SCH_schoolName)
				.append(SCH_contactPerson, castOther.SCH_contactPerson)
				.append(SCH_website, castOther.SCH_website)
				.append(SCH_serviceStartDate, castOther.SCH_serviceStartDate)
				.append(SCH_serviceEndDate, castOther.SCH_serviceEndDate)
				.append(SCH_prearrivalDiscussionBetweenProviderAndFamily,
						castOther.SCH_prearrivalDiscussionBetweenProviderAndFamily)
				.append(SCH_admissionDate, castOther.SCH_admissionDate)
				.append(SCH_city, castOther.SCH_city)
				.append(SCH_noOfChildren, castOther.SCH_noOfChildren)
				.append(SCH_vendorCodeEXSO, castOther.SCH_vendorCodeEXSO)
				.append(TAX_vendorCode, castOther.TAX_vendorCode)
				.append(TAX_vendorName, castOther.TAX_vendorName)
				.append(TAX_vendorContact, castOther.TAX_vendorContact)
				.append(TAX_vendorEmail, castOther.TAX_vendorEmail)
				.append(TAX_serviceStartDate, castOther.TAX_serviceStartDate)
				.append(TAX_serviceEndDate, castOther.TAX_serviceEndDate)
				.append(TAX_notificationdate, castOther.TAX_notificationdate)
				.append(TAX_vendorCodeEXSO, castOther.TAX_vendorCodeEXSO)
				.append(TAC_vendorCode, castOther.TAC_vendorCode)
				.append(TAC_vendorName, castOther.TAC_vendorName)
				.append(TAC_vendorContact, castOther.TAC_vendorContact)
				.append(TAC_vendorEmail, castOther.TAC_vendorEmail)
				.append(TAC_serviceStartDate, castOther.TAC_serviceStartDate)
				.append(TAC_serviceEndDate, castOther.TAC_serviceEndDate)
				.append(TAC_monthsWeeksDaysAuthorized,
						castOther.TAC_monthsWeeksDaysAuthorized)
				.append(TAC_monthlyRentalAllowance,
						castOther.TAC_monthlyRentalAllowance)
				.append(TAC_leaseStartDate, castOther.TAC_leaseStartDate)
				.append(TAC_securityDeposit, castOther.TAC_securityDeposit)
				.append(TAC_leaseExpireDate, castOther.TAC_leaseExpireDate)
				.append(TAC_expiryReminderPriorToExpiry,
						castOther.TAC_expiryReminderPriorToExpiry)
				.append(TAC_negotiatedRent, castOther.TAC_negotiatedRent)
				.append(TAC_utilitiesIncluded, castOther.TAC_utilitiesIncluded)
				.append(TAC_depositPaidBy, castOther.TAC_depositPaidBy)
				.append(TAC_comment, castOther.TAC_comment)
				.append(TAC_vendorCodeEXSO, castOther.TAC_vendorCodeEXSO)
				.append(TEN_vendorCode, castOther.TEN_vendorCode)
				.append(TEN_vendorName, castOther.TEN_vendorName)
				.append(TEN_vendorContact, castOther.TEN_vendorContact)
				.append(TEN_vendorEmail, castOther.TEN_vendorEmail)
				.append(TEN_serviceStartDate, castOther.TEN_serviceStartDate)
				.append(TEN_serviceEndDate, castOther.TEN_serviceEndDate)
				.append(TEN_vendorCodeEXSO, castOther.TEN_vendorCodeEXSO)
				.append(TEN_landlordName, castOther.TEN_landlordName)
				.append(TEN_landlordContactNumber,
						castOther.TEN_landlordContactNumber)
				.append(TEN_landlordEmailAddress,
						castOther.TEN_landlordEmailAddress)
				.append(TEN_propertyAddress, castOther.TEN_propertyAddress)
				.append(TEN_accountHolder, castOther.TEN_accountHolder)
				.append(TEN_bankName, castOther.TEN_bankName)
				.append(TEN_bankAddress, castOther.TEN_bankAddress)
				.append(TEN_bankAccountNumber, castOther.TEN_bankAccountNumber)
				.append(TEN_bankIbanNumber, castOther.TEN_bankIbanNumber)
				.append(TEN_abaNumber, castOther.TEN_abaNumber)
				.append(TEN_accountType, castOther.TEN_accountType)
				.append(TEN_leaseStartDate, castOther.TEN_leaseStartDate)
				.append(TEN_leaseExpireDate, castOther.TEN_leaseExpireDate)
				.append(TEN_expiryReminderPriorToExpiry,
						castOther.TEN_expiryReminderPriorToExpiry)
				.append(TEN_monthlyRentalAllowance,
						castOther.TEN_monthlyRentalAllowance)
				.append(TEN_securityDeposit, castOther.TEN_securityDeposit)
				.append(TEN_leaseCurrency, castOther.TEN_leaseCurrency)
				.append(TEN_leaseSignee, castOther.TEN_leaseSignee)
				.append(VIS_vendorCode, castOther.VIS_vendorCode)
				.append(VIS_vendorName, castOther.VIS_vendorName)
				.append(VIS_vendorContact, castOther.VIS_vendorContact)
				.append(VIS_vendorEmail, castOther.VIS_vendorEmail)
				.append(VIS_serviceStartDate, castOther.VIS_serviceStartDate)
				.append(VIS_serviceEndDate, castOther.VIS_serviceEndDate)
				.append(VIS_workPermitVisaHolderName,
						castOther.VIS_workPermitVisaHolderName)
				.append(VIS_providerNotificationDate,
						castOther.VIS_providerNotificationDate)
				.append(VIS_visaExpiryDate, castOther.VIS_visaExpiryDate)
				.append(VIS_workPermitExpiry, castOther.VIS_workPermitExpiry)
				.append(VIS_expiryReminder3MosPriorToExpiry,
						castOther.VIS_expiryReminder3MosPriorToExpiry)
				.append(VIS_visaStartDate, castOther.VIS_visaStartDate)
				.append(VIS_visaExtensionNeeded,
						castOther.VIS_visaExtensionNeeded)
				.append(VIS_assignees, castOther.VIS_assignees)
				.append(VIS_employers, castOther.VIS_employers)
				.append(VIS_convenant, castOther.VIS_convenant)
				.append(VIS_residencePermitHolderNameRP,
						castOther.VIS_residencePermitHolderNameRP)
				.append(VIS_providerNotificationDateRP,
						castOther.VIS_providerNotificationDateRP)
				.append(VIS_visaExpiryDateRP, castOther.VIS_visaExpiryDateRP)
				.append(VIS_expiryReminder3MosPriorToExpiryRP,
						castOther.VIS_expiryReminder3MosPriorToExpiryRP)
				.append(VIS_residenceExpiryRP, castOther.VIS_residenceExpiryRP)
				.append(VIS_questionnaireSentDate,
						castOther.VIS_questionnaireSentDate)
				.append(VIS_primaryEmail, castOther.VIS_primaryEmail)
				.append(VIS_authorizationLetter,
						castOther.VIS_authorizationLetter)
				.append(VIS_copyResidence, castOther.VIS_copyResidence)
				.append(VIS_vendorCodeEXSO, castOther.VIS_vendorCodeEXSO)
				.append(VIS_applicationDateWork,
						castOther.VIS_applicationDateWork)
				.append(VIS_permitStartDateWork,
						castOther.VIS_permitStartDateWork)
				.append(VIS_applicationDateResidence,
						castOther.VIS_applicationDateResidence)
				.append(VIS_permitStartDateResidence,
						castOther.VIS_permitStartDateResidence)
				.append(WOP_vendorCode, castOther.WOP_vendorCode)
				.append(WOP_vendorName, castOther.WOP_vendorName)
				.append(WOP_vendorContact, castOther.WOP_vendorContact)
				.append(WOP_vendorEmail, castOther.WOP_vendorEmail)
				.append(WOP_serviceStartDate, castOther.WOP_serviceStartDate)
				.append(WOP_serviceEndDate, castOther.WOP_serviceEndDate)
				.append(WOP_workPermitHolderName,
						castOther.WOP_workPermitHolderName)
				.append(WOP_providerNotificationDate,
						castOther.WOP_providerNotificationDate)
				.append(WOP_permitStartDate, castOther.WOP_permitStartDate)
				.append(WOP_permitExpiryDate, castOther.WOP_permitExpiryDate)
				.append(WOP_reminder3MosPriorToExpiry,
						castOther.WOP_reminder3MosPriorToExpiry)
				.append(WOP_questionnaireSentDate,
						castOther.WOP_questionnaireSentDate)
				.append(WOP_authorizationLetter,
						castOther.WOP_authorizationLetter)
				.append(WOP_copyResidence, castOther.WOP_copyResidence)
				.append(WOP_vendorCodeEXSO, castOther.WOP_vendorCodeEXSO)
				.append(REP_vendorCode, castOther.REP_vendorCode)
				.append(REP_vendorName, castOther.REP_vendorName)
				.append(REP_vendorContact, castOther.REP_vendorContact)
				.append(REP_vendorEmail, castOther.REP_vendorEmail)
				.append(REP_serviceStartDate, castOther.REP_serviceStartDate)
				.append(REP_serviceEndDate, castOther.REP_serviceEndDate)
				.append(REP_workPermitHolderName,
						castOther.REP_workPermitHolderName)
				.append(REP_providerNotificationDate,
						castOther.REP_providerNotificationDate)
				.append(REP_permitStartDate, castOther.REP_permitStartDate)
				.append(REP_permitExpiryDate, castOther.REP_permitExpiryDate)
				.append(REP_reminder3MosPriorToExpiry,
						castOther.REP_reminder3MosPriorToExpiry)
				.append(REP_questionnaireSentDate,
						castOther.REP_questionnaireSentDate)
				.append(REP_authorizationLetter,
						castOther.REP_authorizationLetter)
				.append(REP_copyResidence, castOther.REP_copyResidence)
				.append(REP_vendorCodeEXSO, castOther.REP_vendorCodeEXSO)
				.append(SET_vendorCode, castOther.SET_vendorCode)
				.append(SET_vendorName, castOther.SET_vendorName)
				.append(SET_vendorContact, castOther.SET_vendorContact)
				.append(SET_vendorEmail, castOther.SET_vendorEmail)
				.append(SET_serviceStartDate, castOther.SET_serviceStartDate)
				.append(SET_serviceEndDate, castOther.SET_serviceEndDate)
				.append(SET_parkingPermit, castOther.SET_parkingPermit)
				.append(SET_bankAccount, castOther.SET_bankAccount)
				.append(SET_cityHall, castOther.SET_cityHall)
				.append(SET_internetConn, castOther.SET_internetConn)
				.append(SET_driverLicense, castOther.SET_driverLicense)
				.append(SET_comment, castOther.SET_comment)
				.append(SET_vendorCodeEXSO, castOther.SET_vendorCodeEXSO)
				.append(CAR_comment, castOther.CAR_comment)
				.append(COL_comment, castOther.COL_comment)
				.append(TRG_comment, castOther.TRG_comment)
				.append(HOM_comment, castOther.HOM_comment)
				.append(LAN_comment, castOther.LAN_comment)
				.append(MMG_comment, castOther.MMG_comment)
				.append(PRV_comment, castOther.PRV_comment)
				.append(AIO_comment, castOther.AIO_comment)
				.append(EXP_comment, castOther.EXP_comment)
				.append(SCH_comment, castOther.SCH_comment)
				.append(TAX_comment, castOther.TAX_comment)
				.append(TEN_comment, castOther.TEN_comment)
				.append(VIS_comment, castOther.VIS_comment)
				.append(WOP_comment, castOther.WOP_comment)
				.append(REP_comment, castOther.REP_comment)
				.append(serviceCompleteDate, castOther.serviceCompleteDate)
				.append(CAR_displyOtherVendorCode,
						castOther.CAR_displyOtherVendorCode)
				.append(COL_displyOtherVendorCode,
						castOther.COL_displyOtherVendorCode)
				.append(TRG_displyOtherVendorCode,
						castOther.TRG_displyOtherVendorCode)
				.append(HOM_displyOtherVendorCode,
						castOther.HOM_displyOtherVendorCode)
				.append(RNT_displyOtherVendorCode,
						castOther.RNT_displyOtherVendorCode)
				.append(SET_displyOtherVendorCode,
						castOther.SET_displyOtherVendorCode)
				.append(LAN_displyOtherVendorCode,
						castOther.LAN_displyOtherVendorCode)
				.append(MMG_displyOtherVendorCode,
						castOther.MMG_displyOtherVendorCode)
				.append(ONG_displyOtherVendorCode,
						castOther.ONG_displyOtherVendorCode)
				.append(PRV_displyOtherVendorCode,
						castOther.PRV_displyOtherVendorCode)
				.append(AIO_displyOtherVendorCode,
						castOther.AIO_displyOtherVendorCode)
				.append(EXP_displyOtherVendorCode,
						castOther.EXP_displyOtherVendorCode)
				.append(RPT_displyOtherVendorCode,
						castOther.RPT_displyOtherVendorCode)
				.append(SCH_displyOtherSchoolSelected,
						castOther.SCH_displyOtherSchoolSelected)
				.append(TAX_displyOtherVendorCode,
						castOther.TAX_displyOtherVendorCode)
				.append(TAC_displyOtherVendorCode,
						castOther.TAC_displyOtherVendorCode)
				.append(TEN_displyOtherVendorCode,
						castOther.TEN_displyOtherVendorCode)
				.append(VIS_displyOtherVendorCode,
						castOther.VIS_displyOtherVendorCode)
				.append(WOP_displyOtherVendorCode,
						castOther.WOP_displyOtherVendorCode)
				.append(REP_displyOtherVendorCode,
						castOther.REP_displyOtherVendorCode)
				.append(RLS_vendorCode, castOther.RLS_vendorCode)
				.append(RLS_vendorName, castOther.RLS_vendorName)
				.append(RLS_serviceStartDate, castOther.RLS_serviceStartDate)
				.append(RLS_vendorCodeEXSO, castOther.RLS_vendorCodeEXSO)
				.append(RLS_vendorContact, castOther.RLS_vendorContact)
				.append(RLS_serviceEndDate, castOther.RLS_serviceEndDate)
				.append(RLS_vendorEmail, castOther.RLS_vendorEmail)
				.append(RLS_displyOtherVendorCode,
						castOther.RLS_displyOtherVendorCode)
				.append(CAT_vendorCode, castOther.CAT_vendorCode)
				.append(CAT_vendorName, castOther.CAT_vendorName)
				.append(CAT_serviceStartDate, castOther.CAT_serviceStartDate)
				.append(CAT_vendorCodeEXSO, castOther.CAT_vendorCodeEXSO)
				.append(CAT_vendorContact, castOther.CAT_vendorContact)
				.append(CAT_serviceEndDate, castOther.CAT_serviceEndDate)
				.append(CAT_vendorEmail, castOther.CAT_vendorEmail)
				.append(CAT_displyOtherVendorCode,
						castOther.CAT_displyOtherVendorCode)
				.append(CLS_vendorCode, castOther.CLS_vendorCode)
				.append(CLS_vendorName, castOther.CLS_vendorName)
				.append(CLS_serviceStartDate, castOther.CLS_serviceStartDate)
				.append(CLS_vendorCodeEXSO, castOther.CLS_vendorCodeEXSO)
				.append(CLS_vendorContact, castOther.CLS_vendorContact)
				.append(CLS_serviceEndDate, castOther.CLS_serviceEndDate)
				.append(CLS_vendorEmail, castOther.CLS_vendorEmail)
				.append(CLS_displyOtherVendorCode,
						castOther.CLS_displyOtherVendorCode)
				.append(CHS_vendorCode, castOther.CHS_vendorCode)
				.append(CHS_vendorName, castOther.CHS_vendorName)
				.append(CHS_serviceStartDate, castOther.CHS_serviceStartDate)
				.append(CHS_vendorCodeEXSO, castOther.CHS_vendorCodeEXSO)
				.append(CHS_vendorContact, castOther.CHS_vendorContact)
				.append(CHS_serviceEndDate, castOther.CHS_serviceEndDate)
				.append(CHS_vendorEmail, castOther.CHS_vendorEmail)
				.append(CHS_displyOtherVendorCode,
						castOther.CHS_displyOtherVendorCode)
				.append(DPS_vendorCode, castOther.DPS_vendorCode)
				.append(DPS_vendorName, castOther.DPS_vendorName)
				.append(DPS_serviceStartDate, castOther.DPS_serviceStartDate)
				.append(DPS_vendorCodeEXSO, castOther.DPS_vendorCodeEXSO)
				.append(DPS_vendorContact, castOther.DPS_vendorContact)
				.append(DPS_serviceEndDate, castOther.DPS_serviceEndDate)
				.append(DPS_vendorEmail, castOther.DPS_vendorEmail)
				.append(DPS_displyOtherVendorCode,
						castOther.DPS_displyOtherVendorCode)
				.append(HSM_vendorCode, castOther.HSM_vendorCode)
				.append(HSM_vendorName, castOther.HSM_vendorName)
				.append(HSM_serviceStartDate, castOther.HSM_serviceStartDate)
				.append(HSM_vendorCodeEXSO, castOther.HSM_vendorCodeEXSO)
				.append(HSM_vendorContact, castOther.HSM_vendorContact)
				.append(HSM_serviceEndDate, castOther.HSM_serviceEndDate)
				.append(HSM_vendorEmail, castOther.HSM_vendorEmail)
				.append(HSM_displyOtherVendorCode,
						castOther.HSM_displyOtherVendorCode)
				.append(PDT_vendorCode, castOther.PDT_vendorCode)
				.append(PDT_vendorName, castOther.PDT_vendorName)
				.append(PDT_serviceStartDate, castOther.PDT_serviceStartDate)
				.append(PDT_vendorCodeEXSO, castOther.PDT_vendorCodeEXSO)
				.append(PDT_vendorContact, castOther.PDT_vendorContact)
				.append(PDT_serviceEndDate, castOther.PDT_serviceEndDate)
				.append(PDT_vendorEmail, castOther.PDT_vendorEmail)
				.append(PDT_displyOtherVendorCode,
						castOther.PDT_displyOtherVendorCode)
				.append(RCP_vendorCode, castOther.RCP_vendorCode)
				.append(RCP_vendorName, castOther.RCP_vendorName)
				.append(RCP_serviceStartDate, castOther.RCP_serviceStartDate)
				.append(RCP_vendorCodeEXSO, castOther.RCP_vendorCodeEXSO)
				.append(RCP_vendorContact, castOther.RCP_vendorContact)
				.append(RCP_serviceEndDate, castOther.RCP_serviceEndDate)
				.append(RCP_vendorEmail, castOther.RCP_vendorEmail)
				.append(RCP_displyOtherVendorCode,
						castOther.RCP_displyOtherVendorCode)
				.append(SPA_vendorCode, castOther.SPA_vendorCode)
				.append(SPA_vendorName, castOther.SPA_vendorName)
				.append(SPA_serviceStartDate, castOther.SPA_serviceStartDate)
				.append(SPA_vendorCodeEXSO, castOther.SPA_vendorCodeEXSO)
				.append(SPA_vendorContact, castOther.SPA_vendorContact)
				.append(SPA_serviceEndDate, castOther.SPA_serviceEndDate)
				.append(SPA_vendorEmail, castOther.SPA_vendorEmail).append(SPA_vendorInitiation, castOther.SPA_vendorInitiation)
				.append(SPA_displyOtherVendorCode,
						castOther.SPA_displyOtherVendorCode)
				.append(TCS_vendorCode, castOther.TCS_vendorCode)
				.append(TCS_vendorName, castOther.TCS_vendorName)
				.append(TCS_serviceStartDate, castOther.TCS_serviceStartDate)
				.append(TCS_vendorCodeEXSO, castOther.TCS_vendorCodeEXSO)
				.append(TCS_vendorContact, castOther.TCS_vendorContact)
				.append(TCS_serviceEndDate, castOther.TCS_serviceEndDate)
				.append(TCS_vendorEmail, castOther.TCS_vendorEmail)
				.append(TCS_displyOtherVendorCode,
						castOther.TCS_displyOtherVendorCode)
				.append(mailServiceType, castOther.mailServiceType)
				.append(RNT_monthlyRentalAllowanceCurrency,
						castOther.RNT_monthlyRentalAllowanceCurrency)
				.append(RNT_securityDepositCurrency,
						castOther.RNT_securityDepositCurrency)
				.append(MTS_vendorCode, castOther.MTS_vendorCode)
				.append(MTS_vendorName, castOther.MTS_vendorName)
				.append(MTS_serviceStartDate, castOther.MTS_serviceStartDate)
				.append(MTS_vendorCodeEXSO, castOther.MTS_vendorCodeEXSO)
				.append(MTS_vendorContact, castOther.MTS_vendorContact)
				.append(MTS_serviceEndDate, castOther.MTS_serviceEndDate)
				.append(MTS_vendorEmail, castOther.MTS_vendorEmail)
				.append(MTS_paymentResponsibility,
						castOther.MTS_paymentResponsibility)
				.append(MTS_displyOtherVendorCode,
						castOther.MTS_displyOtherVendorCode)
				.append(RLS_paymentResponsibility,
						castOther.RLS_paymentResponsibility)
				.append(CAT_paymentResponsibility,
						castOther.CAT_paymentResponsibility)
				.append(CLS_paymentResponsibility,
						castOther.CLS_paymentResponsibility)
				.append(CHS_paymentResponsibility,
						castOther.CHS_paymentResponsibility)
				.append(DPS_paymentResponsibility,
						castOther.DPS_paymentResponsibility)
				.append(HSM_paymentResponsibility,
						castOther.HSM_paymentResponsibility)
				.append(PDT_paymentResponsibility,
						castOther.PDT_paymentResponsibility)
				.append(RCP_paymentResponsibility,
						castOther.RCP_paymentResponsibility)
				.append(SPA_paymentResponsibility,
						castOther.SPA_paymentResponsibility)
				.append(TCS_paymentResponsibility,
						castOther.TCS_paymentResponsibility)
				.append(DPS_serviceType, castOther.DPS_serviceType)
				.append(DPS_completionDate, castOther.DPS_completionDate)
				.append(DPS_comment, castOther.DPS_comment)
				.append(HSM_brokerCode, castOther.HSM_brokerCode)
				.append(HSM_brokerName, castOther.HSM_brokerName)
				.append(HSM_brokerContact, castOther.HSM_brokerContact)
				.append(HSM_brokerEmail, castOther.HSM_brokerEmail)
				.append(HSM_status, castOther.HSM_status)
				.append(HSM_comment, castOther.HSM_comment)
				.append(HSM_marketingPlanNBMAReceived,
						castOther.HSM_marketingPlanNBMAReceived)
				.append(PDT_serviceType, castOther.PDT_serviceType)
				.append(PDT_budget, castOther.PDT_budget)
				.append(RCP_estimateCost, castOther.RCP_estimateCost)
				.append(RCP_actualCost, castOther.RCP_actualCost)
				.append(MTS_lender, castOther.MTS_lender)
				.append(MTS_initialContact, castOther.MTS_initialContact)
				.append(MTS_status, castOther.MTS_status)
				.append(MTS_mortgageAmount, castOther.MTS_mortgageAmount)
				.append(MTS_mortgageRate, castOther.MTS_mortgageRate)
				.append(MTS_mortgageTerm, castOther.MTS_mortgageTerm)
				.append(COL_estimatedTaxAllowance,
						castOther.COL_estimatedTaxAllowance)
				.append(COL_estimatedHousingAllowance,
						castOther.COL_estimatedHousingAllowance)
				.append(COL_estimatedTransportationAllowance,
						castOther.COL_estimatedTransportationAllowance)
				.append(RNT_leaseSignee, castOther.RNT_leaseSignee)
				.append(RNT_leaseExtensionNeeded,
						castOther.RNT_leaseExtensionNeeded)
				.append(SET_governmentID, castOther.SET_governmentID)
				.append(SET_heathCare, castOther.SET_heathCare)
				.append(SET_utilities, castOther.SET_utilities)
				.append(SET_automobileRegistration,
						castOther.SET_automobileRegistration)
				.append(SET_dayofServiceAuthorized,
						castOther.SET_dayofServiceAuthorized)
				.append(SET_initialDateofServiceRequested,
						castOther.SET_initialDateofServiceRequested)
				.append(SET_providerNotificationSent,
						castOther.SET_providerNotificationSent)
				.append(SET_providerConfirmationReceived,
						castOther.SET_providerConfirmationReceived)
				.append(TAX_allowance, castOther.TAX_allowance)
				.append(VIS_immigrationStatus, castOther.VIS_immigrationStatus)
				.append(VIS_arrivalDate, castOther.VIS_arrivalDate)
				.append(VIS_portofEntry, castOther.VIS_portofEntry)
				.append(DSS_vendorCode, castOther.DSS_vendorCode)
				.append(DSS_vendorName, castOther.DSS_vendorName)
				.append(DSS_serviceStartDate, castOther.DSS_serviceStartDate)
				.append(DSS_vendorCodeEXSO, castOther.DSS_vendorCodeEXSO)
				.append(DSS_vendorContact, castOther.DSS_vendorContact)
				.append(DSS_serviceEndDate, castOther.DSS_serviceEndDate)
				.append(DSS_vendorEmail, castOther.DSS_vendorEmail)
				.append(DSS_paymentResponsibility,
						castOther.DSS_paymentResponsibility)
				.append(DSS_displyOtherVendorCode,
						castOther.DSS_displyOtherVendorCode)
				.append(DSS_initialContactDate,
						castOther.DSS_initialContactDate)
				.append(PRV_paymentResponsibility,
						castOther.PRV_paymentResponsibility)
				.append(VIS_paymentResponsibility,
						castOther.VIS_paymentResponsibility)
				.append(TAX_paymentResponsibility,
						castOther.TAX_paymentResponsibility)
				.append(RNT_paymentResponsibility,
						castOther.RNT_paymentResponsibility)
				.append(AIO_paymentResponsibility,
						castOther.AIO_paymentResponsibility)
				.append(TRG_paymentResponsibility,
						castOther.TRG_paymentResponsibility)
				.append(LAN_paymentResponsibility,
						castOther.LAN_paymentResponsibility)
				.append(RPT_paymentResponsibility,
						castOther.RPT_paymentResponsibility)
				.append(COL_paymentResponsibility,
						castOther.COL_paymentResponsibility)
				.append(TAC_paymentResponsibility,
						castOther.TAC_paymentResponsibility)
				.append(HOM_paymentResponsibility,
						castOther.HOM_paymentResponsibility)
				.append(SCH_paymentResponsibility,
						castOther.SCH_paymentResponsibility)
				.append(EXP_paymentResponsibility,
						castOther.EXP_paymentResponsibility)
				.append(ONG_paymentResponsibility,
						castOther.ONG_paymentResponsibility)
				.append(TEN_paymentResponsibility,
						castOther.TEN_paymentResponsibility)
				.append(MMG_paymentResponsibility,
						castOther.MMG_paymentResponsibility)
				.append(CAR_paymentResponsibility,
						castOther.CAR_paymentResponsibility)
				.append(SET_paymentResponsibility,
						castOther.SET_paymentResponsibility)
				.append(WOP_paymentResponsibility,
						castOther.WOP_paymentResponsibility)
				.append(REP_paymentResponsibility,
						castOther.REP_paymentResponsibility)
				.append(HSM_agentCode, castOther.HSM_agentCode)
				.append(HSM_agentName, castOther.HSM_agentName)
				.append(HSM_brokerage, castOther.HSM_brokerage)
				.append(HSM_agentPhone, castOther.HSM_agentPhone)
				.append(HSM_agentEmail, castOther.HSM_agentEmail)
				.append(HSM_estimatedHSRReferral,
						castOther.HSM_estimatedHSRReferral)
				.append(HSM_actualHSRReferral, castOther.HSM_actualHSRReferral)
				.append(reminderServices, castOther.reminderServices)
				.append(CAT_comment, castOther.CAT_comment)
				.append(CLS_comment, castOther.CLS_comment)
				.append(CHS_comment, castOther.CHS_comment)
				.append(PDT_comment, castOther.PDT_comment)
				.append(RCP_comment, castOther.RCP_comment)
				.append(SPA_comment, castOther.SPA_comment)
				.append(TCS_comment, castOther.TCS_comment)
				.append(MTS_comment, castOther.MTS_comment)
				.append(DSS_comment, castOther.DSS_comment)
				.append(HOM_estimatedHSRReferral,
						castOther.HOM_estimatedHSRReferral)
				.append(HOM_actualHSRReferral, castOther.HOM_actualHSRReferral)
				.append(HSM_offerDate, castOther.HSM_offerDate)
				.append(HSM_closingDate, castOther.HSM_closingDate)
				.append(TAC_timeAuthorized, castOther.TAC_timeAuthorized)
				.append(MMG_originAgentCode, castOther.MMG_originAgentCode)
				.append(MMG_originAgentName, castOther.MMG_originAgentName)
				.append(MMG_originAgentPhone, castOther.MMG_originAgentPhone)
				.append(MMG_originAgentEmail, castOther.MMG_originAgentEmail)
				.append(MMG_destinationAgentCode,
						castOther.MMG_destinationAgentCode)
				.append(MMG_destinationAgentName,
						castOther.MMG_destinationAgentName)
				.append(MMG_destinationAgentPhone,
						castOther.MMG_destinationAgentPhone)
				.append(MMG_destinationAgentEmail,
						castOther.MMG_destinationAgentEmail)
				.append(PRV_emailSent, castOther.PRV_emailSent)
				.append(TAX_emailSent, castOther.TAX_emailSent)
				.append(AIO_emailSent, castOther.AIO_emailSent)
				.append(TRG_emailSent, castOther.TRG_emailSent)
				.append(LAN_emailSent, castOther.LAN_emailSent)
				.append(RPT_emailSent, castOther.RPT_emailSent)
				.append(COL_emailSent, castOther.COL_emailSent)
				.append(TAC_emailSent, castOther.TAC_emailSent)
				.append(HOM_emailSent, castOther.HOM_emailSent)
				.append(SCH_emailSent, castOther.SCH_emailSent)
				.append(EXP_emailSent, castOther.EXP_emailSent)
				.append(ONG_emailSent, castOther.ONG_emailSent)
				.append(TEN_emailSent, castOther.TEN_emailSent)
				.append(MMG_orginEmailSent, castOther.MMG_orginEmailSent)
				.append(MMG_destinationEmailSent,
						castOther.MMG_destinationEmailSent)
				.append(CAR_emailSent, castOther.CAR_emailSent)
				.append(SET_emailSent, castOther.SET_emailSent)
				.append(CAT_emailSent, castOther.CAT_emailSent)
				.append(CLS_emailSent, castOther.CLS_emailSent)
				.append(CHS_emailSent, castOther.CHS_emailSent)
				.append(DPS_emailSent, castOther.DPS_emailSent)
				.append(HSM_emailSent, castOther.HSM_emailSent)
				.append(PDT_emailSent, castOther.PDT_emailSent)
				.append(RCP_emailSent, castOther.RCP_emailSent)
				.append(SPA_emailSent, castOther.SPA_emailSent)
				.append(TCS_emailSent, castOther.TCS_emailSent)
				.append(MTS_emailSent, castOther.MTS_emailSent)
				.append(DSS_emailSent, castOther.DSS_emailSent)
				.append(VIS_emailSent, castOther.VIS_emailSent)
				.append(RNT_emailSent, castOther.RNT_emailSent)
				.append(NET_emailSent, castOther.NET_emailSent)
				.append(WOP_emailSent, castOther.WOP_emailSent)
				.append(REP_emailSent, castOther.REP_emailSent)
				.append(resendEmailServiceName,
						castOther.resendEmailServiceName)
				.append(HOB_startDate, castOther.HOB_startDate)
				.append(HOB_endDate, castOther.HOB_endDate)
				.append(HOB_hotelName, castOther.HOB_hotelName)
				.append(HOB_city, castOther.HOB_city)
				.append(FLB_arrivalDate, castOther.FLB_arrivalDate)
				.append(FLB_departureDate, castOther.FLB_departureDate)
				.append(FLB_additionalBookingReminderDate,
						castOther.FLB_additionalBookingReminderDate)
				.append(FLB_arrivalDate1, castOther.FLB_arrivalDate1)
				.append(FLB_departureDate1, castOther.FLB_departureDate1)
				.append(FLB_additionalBookingReminderDate1,
						castOther.FLB_additionalBookingReminderDate1)
				.append(FLB_addOn, castOther.FLB_addOn)
				.append(HOB_vendorCode, castOther.HOB_vendorCode)
				.append(HOB_vendorName, castOther.HOB_vendorName)
				.append(HOB_vendorContact, castOther.HOB_vendorContact)
				.append(HOB_vendorEmail, castOther.HOB_vendorEmail)
				.append(HOB_serviceStartDate, castOther.HOB_serviceStartDate)
				.append(HOB_serviceEndDate, castOther.HOB_serviceEndDate)
				.append(HOB_vendorCodeEXSO, castOther.HOB_vendorCodeEXSO)
				.append(FLB_vendorCode, castOther.FLB_vendorCode)
				.append(FLB_vendorName, castOther.FLB_vendorName)
				.append(FLB_vendorContact, castOther.FLB_vendorContact)
				.append(FLB_vendorEmail, castOther.FLB_vendorEmail)
				.append(FLB_serviceStartDate, castOther.FLB_serviceStartDate)
				.append(FLB_serviceEndDate, castOther.FLB_serviceEndDate)
				.append(FLB_vendorCodeEXSO, castOther.FLB_vendorCodeEXSO)
				.append(FLB_emailSent, castOther.FLB_emailSent)
				.append(HOB_emailSent, castOther.HOB_emailSent)
				.append(HOB_displyOtherVendorCode,
						castOther.HOB_displyOtherVendorCode)
				.append(FLB_displyOtherVendorCode,
						castOther.FLB_displyOtherVendorCode)
				.append(FRL_vendorCode, castOther.FRL_vendorCode)
				.append(FRL_vendorName, castOther.FRL_vendorName)
				.append(FRL_serviceStartDate, castOther.FRL_serviceStartDate)
				.append(FRL_vendorCodeEXSO, castOther.FRL_vendorCodeEXSO)
				.append(FRL_vendorContact, castOther.FRL_vendorContact)
				.append(FRL_serviceEndDate, castOther.FRL_serviceEndDate)
				.append(FRL_emailSent, castOther.FRL_emailSent)
				.append(FRL_vendorEmail, castOther.FRL_vendorEmail)
				.append(FRL_displyOtherVendorCode,
						castOther.FRL_displyOtherVendorCode)
				.append(FRL_paymentResponsibility,
						castOther.FRL_paymentResponsibility)
				.append(FRL_rentalRate, castOther.FRL_rentalRate)
				.append(FRL_deposit, castOther.FRL_deposit)
				.append(FRL_rentalCurrency, castOther.FRL_rentalCurrency)
				.append(FRL_depositCurrency, castOther.FRL_depositCurrency)
				.append(FRL_month, castOther.FRL_month)
				.append(FRL_rentalStartDate, castOther.FRL_rentalStartDate)
				.append(FRL_rentalEndDate, castOther.FRL_rentalEndDate)
				.append(FRL_terminationNotice, castOther.FRL_terminationNotice)
				.append(FRL_comment, castOther.FRL_comment)
				.append(APU_vendorCode, castOther.APU_vendorCode)
				.append(APU_vendorName, castOther.APU_vendorName)
				.append(APU_serviceStartDate, castOther.APU_serviceStartDate)
				.append(APU_vendorCodeEXSO, castOther.APU_vendorCodeEXSO)
				.append(APU_vendorContact, castOther.APU_vendorContact)
				.append(APU_serviceEndDate, castOther.APU_serviceEndDate)
				.append(APU_emailSent, castOther.APU_emailSent)
				.append(APU_vendorEmail, castOther.APU_vendorEmail)
				.append(APU_displyOtherVendorCode,
						castOther.APU_displyOtherVendorCode)
				.append(APU_paymentResponsibility,
						castOther.APU_paymentResponsibility)
				.append(APU_flightDetails, castOther.APU_flightDetails)
				.append(APU_driverName, castOther.APU_driverName)
				.append(APU_driverPhoneNumber, castOther.APU_driverPhoneNumber)
				.append(APU_arrivalDate, castOther.APU_arrivalDate)
				.append(APU_carMake, castOther.APU_carMake)
				.append(APU_carModel, castOther.APU_carModel)
				.append(APU_carColor, castOther.APU_carColor)
				.append(WOP_leaseExNeeded, castOther.WOP_leaseExNeeded)
				.append(REP_leaseExNeeded, castOther.REP_leaseExNeeded)
				.append(TEN_propertyName, castOther.TEN_propertyName)
				.append(TEN_city, castOther.TEN_city)
				.append(TEN_zipCode, castOther.TEN_zipCode)
				.append(TEN_addressLine1, castOther.TEN_addressLine1)
				.append(TEN_addressLine2, castOther.TEN_addressLine2)
				.append(TEN_state, castOther.TEN_state)
				.append(TEN_country, castOther.TEN_country)
				.append(TEN_leasedBy, castOther.TEN_leasedBy)
				.append(TEN_rentAmount, castOther.TEN_rentAmount)
				.append(TEN_rentCurrency, castOther.TEN_rentCurrency)
				.append(TEN_rentAllowance, castOther.TEN_rentAllowance)
				.append(TEN_allowanceCurrency, castOther.TEN_allowanceCurrency)
				.append(TEN_utilitiesIncluded, castOther.TEN_utilitiesIncluded)
				.append(TEN_rentPaidTo, castOther.TEN_rentPaidTo)
				.append(TEN_rentalIncreaseDate, castOther.TEN_rentalIncreaseDate)
				.append(TEN_rentalComment, castOther.TEN_rentalComment)
				.append(TEN_followUpNeeded, castOther.TEN_followUpNeeded)
				.append(TEN_termOfNotice, castOther.TEN_termOfNotice)
				.append(TEN_checkInMoveIn, castOther.TEN_checkInMoveIn)
				.append(TEN_preCheckOut, castOther.TEN_preCheckOut)
				.append(TEN_checkOutMoveOut, castOther.TEN_checkOutMoveOut)
				.append(TEN_swiftCode, castOther.TEN_swiftCode)
				.append(TEN_paymentDescription, castOther.TEN_paymentDescription)
				.append(TEN_exceptionComments, castOther.TEN_exceptionComments)
				.append(TEN_depositAmount, castOther.TEN_depositAmount)
				.append(TEN_depositReturned, castOther.TEN_depositReturned)
				.append(TEN_depositCurrency, castOther.TEN_depositCurrency)
				.append(TEN_depositReturnedAmount, castOther.TEN_depositReturnedAmount)
				.append(TEN_depositPaidBy, castOther.TEN_depositPaidBy)
				.append(TEN_depositReturnedCurrency, castOther.TEN_depositReturnedCurrency)
				.append(TEN_depositComment, castOther.TEN_depositComment)
				.append(TEN_refundableTo, castOther.TEN_refundableTo)
				
				.append(TEN_Gas_Water, castOther.TEN_Gas_Water)
				.append(TEN_TV_Internet_Phone, castOther.TEN_TV_Internet_Phone)
				.append(TEN_mobilePhone, castOther.TEN_mobilePhone)
				.append(TEN_furnitureRental, castOther.TEN_furnitureRental)
				.append(TEN_cleaningServices, castOther.TEN_cleaningServices)
				.append(TEN_parkingPermit, castOther.TEN_parkingPermit)
				.append(TEN_communityTax, castOther.TEN_communityTax)
				.append(TEN_insurance, castOther.TEN_insurance)
				.append(TEN_gardenMaintenance, castOther.TEN_gardenMaintenance)
				.append(TEN_toBePaidBy, castOther.TEN_toBePaidBy)
				.append(TEN_assigneeContributionAmount, castOther.TEN_assigneeContributionAmount)
				.append(TEN_directDebit, castOther.TEN_directDebit)
				.append(TEN_description, castOther.TEN_description)
				.append(TEN_IBAN_BankAccountNumber, castOther.TEN_IBAN_BankAccountNumber)
				.append(TEN_BIC_SWIFT, castOther.TEN_BIC_SWIFT)
				.append(TEN_assigneeContributionCurrency, castOther.TEN_assigneeContributionCurrency)
				.append("TEN_Utility_Gas_Water", castOther.TEN_Utility_Gas_Water)
				.append("TEN_Utility_TV_Internet_Phone", castOther.TEN_Utility_TV_Internet_Phone)
				.append("TEN_Utility_mobilePhone", castOther.TEN_Utility_mobilePhone)
				.append("TEN_Utility_furnitureRental", castOther.TEN_Utility_furnitureRental)
				.append("TEN_Utility_cleaningServices", castOther.TEN_Utility_cleaningServices)
				.append("TEN_Utility_parkingPermit", castOther.TEN_Utility_parkingPermit)
				.append("TEN_Utility_communityTax", castOther.TEN_Utility_communityTax)
				.append("TEN_Utility_insurance", castOther.TEN_Utility_insurance)
				.append("TEN_Utility_gardenMaintenance", castOther.TEN_Utility_gardenMaintenance)
				.append("TEN_contributionAmount", castOther.TEN_contributionAmount)
				.append("TEN_Gas", castOther.TEN_Gas)
				.append("TEN_Electricity", castOther.TEN_Electricity)
				.append("TEN_Miscellaneous", castOther.TEN_Miscellaneous)
				.append("TEN_Utility_Gas", castOther.TEN_Utility_Gas)
				.append("TEN_Utility_Electricity", castOther.TEN_Utility_Electricity)
				.append("TEN_Utility_Miscellaneous", castOther.TEN_Utility_Miscellaneous)
				.append("TEN_exceptionAddedValue", castOther.TEN_exceptionAddedValue)
				.append("TEN_Gas_Electric", castOther.TEN_Gas_Electric)
				.append("TEN_Utility_Gas_Electric", castOther.TEN_Utility_Gas_Electric)
				.append("TEN_housingRentVatDesc", castOther.TEN_housingRentVatDesc)
				.append("TEN_billThroughDate", castOther.TEN_billThroughDate)
				.append("TEN_housingRentVatPercent", castOther.TEN_housingRentVatPercent)
				.append("INS_vendorCode", castOther.INS_vendorCode).append("INS_vendorName", castOther.INS_vendorName).append("INS_vendorCodeEXSO", castOther.INS_vendorCodeEXSO)
				.append("INS_vendorContact", castOther.INS_vendorContact).append("INS_vendorEmail", castOther.INS_vendorEmail).append("INS_comment", castOther.INS_comment)
				.append("INS_vendorInitiation", castOther.INS_vendorInitiation).append("INS_serviceStartDate", castOther.INS_serviceStartDate).append("INS_serviceEndDate", castOther.INS_serviceEndDate)
				.append("INP_vendorCode", castOther.INP_vendorCode).append("INP_vendorName", castOther.INP_vendorName).append("INP_vendorCodeEXSO", castOther.INP_vendorCodeEXSO)
				.append("INP_vendorContact", castOther.INP_vendorContact).append("INP_vendorEmail", castOther.INP_vendorEmail).append("INP_comment", castOther.INP_comment)
				.append("INP_vendorInitiation", castOther.INP_vendorInitiation).append("INP_serviceStartDate", castOther.INP_serviceStartDate).append("INP_serviceEndDate", castOther.INP_serviceEndDate)
				.append("EDA_vendorCode", castOther.EDA_vendorCode).append("EDA_vendorName", castOther.EDA_vendorName).append("EDA_vendorCodeEXSO", castOther.EDA_vendorCodeEXSO)
				.append("EDA_vendorContact", castOther.EDA_vendorContact).append("EDA_vendorEmail", castOther.EDA_vendorEmail).append("EDA_comment", castOther.EDA_comment)
				.append("EDA_vendorInitiation", castOther.EDA_vendorInitiation).append("EDA_serviceStartDate", castOther.EDA_serviceStartDate).append("EDA_serviceEndDate", castOther.EDA_serviceEndDate)
				.append("TAS_vendorCode", castOther.TAS_vendorCode).append("TAS_vendorName", castOther.TAS_vendorName).append("TAS_vendorCodeEXSO", castOther.TAS_vendorCodeEXSO)
				.append("TAS_vendorContact", castOther.TAS_vendorContact).append("TAS_vendorEmail", castOther.TAS_vendorEmail).append("TAS_comment", castOther.TAS_comment)
				.append("TAS_vendorInitiation", castOther.TAS_vendorInitiation).append("TAS_serviceStartDate", castOther.TAS_serviceStartDate).append("TAS_serviceEndDate", castOther.TAS_serviceEndDate)
				.append("totalDspDays", castOther.totalDspDays)
				.append("RNT_rsfReceived", castOther.RNT_rsfReceived)
				.append("RNT_moveInspection", castOther.RNT_moveInspection)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(serviceOrderId).append(shipNumber)
				.append(CAR_vendorCode).append(CAR_vendorName)
				.append(CAR_vendorContact).append(CAR_vendorEmail)
				.append(CAR_serviceStartDate).append(CAR_serviceEndDate)
				.append(CAR_vendorCodeEXSO).append(CAR_notificationDate)
				.append(CAR_pickUpDate).append(CAR_registrationDate)
				.append(CAR_deliveryDate).append(COL_vendorCode)
				.append(COL_vendorName).append(COL_vendorContact)
				.append(COL_vendorEmail).append(COL_serviceStartDate)
				.append(COL_serviceEndDate).append(COL_notificationDate)
				.append(COL_vendorCodeEXSO).append(COL_estimatedAllowanceEuro)
				.append(TRG_vendorCode).append(TRG_vendorName)
				.append(TRG_vendorContact).append(TRG_vendorEmail)
				.append(TRG_serviceStartDate).append(TRG_serviceEndDate)
				.append(TRG_employeeDaysAuthorized)
				.append(TRG_spouseDaysAuthorized).append(TRG_employeeTime)
				.append(TRG_spouseTime).append(TRG_vendorCodeEXSO)
				.append(HOM_vendorCode).append(HOM_vendorName)
				.append(HOM_vendorContact).append(HOM_vendorEmail)
				.append(HOM_serviceStartDate).append(HOM_serviceEndDate)
				.append(HOM_viewStatus).append(HOM_moveInDate)
				.append(HOM_vendorCodeEXSO).append(HOM_agentCode)
				.append(HOM_agentName).append(HOM_initialContactDate)
				.append(HOM_agentPhone).append(HOM_houseHuntingTripArrival)
				.append(HOM_agentEmail).append(HOM_houseHuntingTripDeparture)
				.append(HOM_brokerage).append(HOM_offerDate)
				.append(HOM_closingDate).append(RNT_vendorCode)
				.append(RNT_vendorName).append(RNT_vendorContact)
				.append(RNT_vendorEmail).append(RNT_serviceStartDate)
				.append(RNT_serviceEndDate).append(RNT_leaseStartDate)
				.append(RNT_leaseExpireDate).append(RNT_monthlyRentalAllowance)
				.append(RNT_negotiatedRent).append(RNT_utilitiesIncluded)
				.append(RNT_securityDeposit).append(RNT_reminderDate)
				.append(RNT_checkInDate).append(RNT_depositPaidBy)
				.append(RNT_comment).append(RNT_paidCurrency)
				.append(RNT_vendorCodeEXSO).append(RNT_homeSearchType)
				.append(LAN_vendorCode).append(LAN_vendorName)
				.append(LAN_vendorContact).append(LAN_vendorEmail)
				.append(LAN_serviceStartDate).append(LAN_serviceEndDate)
				.append(LAN_notificationDate)
				.append(LAN_employeeDaysAuthorized)
				.append(LAN_spouseDaysAuthorized)
				.append(LAN_childrenDaysAuthorized).append(LAN_employeeTime)
				.append(LAN_spouseTime).append(LAN_childrenTime)
				.append(LAN_vendorCodeEXSO).append(MMG_vendorCode)
				.append(MMG_vendorName).append(MMG_vendorContact)
				.append(MMG_vendorEmail).append(MMG_serviceStartDate)
				.append(MMG_serviceEndDate).append(MMG_vendorCodeEXSO).append(MMG_vendorInitiation)
				.append(ONG_vendorCode).append(ONG_vendorName)
				.append(ONG_vendorContact).append(ONG_vendorEmail)
				.append(ONG_serviceStartDate).append(ONG_serviceEndDate)
				.append(ONG_dateType1).append(ONG_dateType2)
				.append(ONG_dateType3).append(ONG_date1).append(ONG_date2)
				.append(ONG_date3).append(ONG_comment)
				.append(ONG_vendorCodeEXSO).append(PRV_vendorCode)
				.append(PRV_vendorName).append(PRV_vendorContact)
				.append(PRV_vendorEmail).append(PRV_serviceStartDate)
				.append(PRV_serviceEndDate)
				.append(PRV_travelPlannerNotification)
				.append(PRV_travelDeparture).append(PRV_travelArrival)
				.append(PRV_hotelDetails)
				.append(PRV_providerNotificationAndServicesAuthorized)
				.append(PRV_meetGgreetAtAirport)
				.append(PRV_areaOrientationDays).append(PRV_flightNumber)
				.append(PRV_flightArrivalTime).append(PRV_vendorCodeEXSO)
				.append(AIO_airlineFlight).append(AIO_contactInformation)
				.append(AIO_flightArrival).append(AIO_flightDeparture)
				.append(AIO_initialDateOfServiceRequested)
				.append(AIO_prearrivalDiscussionOfProviderAndFamily)
				.append(AIO_providerConfirmationReceived)
				.append(AIO_providerNotificationSent)
				.append(AIO_serviceAuthorized).append(AIO_vendorCode)
				.append(AIO_vendorName).append(AIO_vendorContact)
				.append(AIO_vendorEmail).append(AIO_serviceStartDate)
				.append(AIO_serviceEndDate).append(AIO_vendorCodeEXSO)
				.append(EXP_vendorCode).append(EXP_vendorName)
				.append(EXP_vendorContact).append(EXP_vendorEmail)
				.append(EXP_serviceStartDate).append(EXP_serviceEndDate)
				.append(EXP_paidStatus).append(EXP_providerNotification)
				.append(EXP_vendorCodeEXSO).append(RPT_vendorCode)
				.append(RPT_vendorName).append(RPT_vendorContact)
				.append(RPT_vendorEmail).append(RPT_serviceStartDate)
				.append(RPT_serviceEndDate)
				.append(RPT_landlordPaidNotificationDate)
				.append(RPT_professionalCleaningDate).append(RPT_comment)
				.append(RPT_utilitiesShutOff).append(RPT_moveOutDate)
				.append(RPT_negotiatedDepositReturnedDate)
				.append(RPT_damageCost).append(RPT_securityDeposit)
				.append(RPT_depositReturnedTo).append(RPT_vendorCodeEXSO)
				.append(SCH_schoolSelected).append(SCH_schoolName)
				.append(SCH_contactPerson).append(SCH_website)
				.append(SCH_serviceStartDate).append(SCH_serviceEndDate)
				.append(SCH_prearrivalDiscussionBetweenProviderAndFamily)
				.append(SCH_admissionDate).append(SCH_city)
				.append(SCH_noOfChildren).append(SCH_vendorCodeEXSO)
				.append(TAX_vendorCode).append(TAX_vendorName)
				.append(TAX_vendorContact).append(TAX_vendorEmail)
				.append(TAX_serviceStartDate).append(TAX_serviceEndDate)
				.append(TAX_notificationdate).append(TAX_vendorCodeEXSO)
				.append(TAC_vendorCode).append(TAC_vendorName)
				.append(TAC_vendorContact).append(TAC_vendorEmail)
				.append(TAC_serviceStartDate).append(TAC_serviceEndDate)
				.append(TAC_monthsWeeksDaysAuthorized)
				.append(TAC_monthlyRentalAllowance).append(TAC_leaseStartDate)
				.append(TAC_securityDeposit).append(TAC_leaseExpireDate)
				.append(TAC_expiryReminderPriorToExpiry)
				.append(TAC_negotiatedRent).append(TAC_utilitiesIncluded)
				.append(TAC_depositPaidBy).append(TAC_comment)
				.append(TAC_vendorCodeEXSO).append(TEN_vendorCode)
				.append(TEN_vendorName).append(TEN_vendorContact)
				.append(TEN_vendorEmail).append(TEN_serviceStartDate)
				.append(TEN_serviceEndDate).append(TEN_vendorCodeEXSO)
				.append(TEN_landlordName).append(TEN_landlordContactNumber)
				.append(TEN_landlordEmailAddress).append(TEN_propertyAddress)
				.append(TEN_accountHolder).append(TEN_bankName)
				.append(TEN_bankAddress).append(TEN_bankAccountNumber)
				.append(TEN_bankIbanNumber).append(TEN_abaNumber)
				.append(TEN_accountType).append(TEN_leaseStartDate)
				.append(TEN_leaseExpireDate)
				.append(TEN_expiryReminderPriorToExpiry)
				.append(TEN_monthlyRentalAllowance).append(TEN_securityDeposit)
				.append(TEN_leaseCurrency).append(TEN_leaseSignee)
				.append(VIS_vendorCode).append(VIS_vendorName)
				.append(VIS_vendorContact).append(VIS_vendorEmail)
				.append(VIS_serviceStartDate).append(VIS_serviceEndDate)
				.append(VIS_workPermitVisaHolderName)
				.append(VIS_providerNotificationDate)
				.append(VIS_visaExpiryDate).append(VIS_workPermitExpiry)
				.append(VIS_expiryReminder3MosPriorToExpiry)
				.append(VIS_visaStartDate).append(VIS_visaExtensionNeeded)
				.append(VIS_assignees).append(VIS_employers)
				.append(VIS_convenant).append(VIS_residencePermitHolderNameRP)
				.append(VIS_providerNotificationDateRP)
				.append(VIS_visaExpiryDateRP)
				.append(VIS_expiryReminder3MosPriorToExpiryRP)
				.append(VIS_residenceExpiryRP)
				.append(VIS_questionnaireSentDate).append(VIS_primaryEmail)
				.append(VIS_authorizationLetter).append(VIS_copyResidence)
				.append(VIS_vendorCodeEXSO).append(VIS_applicationDateWork)
				.append(VIS_permitStartDateWork)
				.append(VIS_applicationDateResidence)
				.append(VIS_permitStartDateResidence).append(WOP_vendorCode)
				.append(WOP_vendorName).append(WOP_vendorContact)
				.append(WOP_vendorEmail).append(WOP_serviceStartDate)
				.append(WOP_serviceEndDate).append(WOP_workPermitHolderName)
				.append(WOP_providerNotificationDate)
				.append(WOP_permitStartDate).append(WOP_permitExpiryDate)
				.append(WOP_reminder3MosPriorToExpiry)
				.append(WOP_questionnaireSentDate)
				.append(WOP_authorizationLetter).append(WOP_copyResidence)
				.append(WOP_vendorCodeEXSO).append(REP_vendorCode)
				.append(REP_vendorName).append(REP_vendorContact)
				.append(REP_vendorEmail).append(REP_serviceStartDate)
				.append(REP_serviceEndDate).append(REP_workPermitHolderName)
				.append(REP_providerNotificationDate)
				.append(REP_permitStartDate).append(REP_permitExpiryDate)
				.append(REP_reminder3MosPriorToExpiry)
				.append(REP_questionnaireSentDate)
				.append(REP_authorizationLetter).append(REP_copyResidence)
				.append(REP_vendorCodeEXSO).append(SET_vendorCode)
				.append(SET_vendorName).append(SET_vendorContact)
				.append(SET_vendorEmail).append(SET_serviceStartDate)
				.append(SET_serviceEndDate).append(SET_parkingPermit)
				.append(SET_bankAccount).append(SET_cityHall)
				.append(SET_internetConn).append(SET_driverLicense)
				.append(SET_comment).append(SET_vendorCodeEXSO)
				.append(CAR_comment).append(COL_comment).append(TRG_comment)
				.append(HOM_comment).append(LAN_comment).append(MMG_comment)
				.append(PRV_comment).append(AIO_comment).append(EXP_comment)
				.append(SCH_comment).append(TAX_comment).append(TEN_comment)
				.append(VIS_comment).append(WOP_comment).append(REP_comment)
				.append(serviceCompleteDate).append(CAR_displyOtherVendorCode)
				.append(COL_displyOtherVendorCode)
				.append(TRG_displyOtherVendorCode)
				.append(HOM_displyOtherVendorCode)
				.append(RNT_displyOtherVendorCode)
				.append(SET_displyOtherVendorCode)
				.append(LAN_displyOtherVendorCode)
				.append(MMG_displyOtherVendorCode)
				.append(ONG_displyOtherVendorCode)
				.append(PRV_displyOtherVendorCode)
				.append(AIO_displyOtherVendorCode)
				.append(EXP_displyOtherVendorCode)
				.append(RPT_displyOtherVendorCode)
				.append(SCH_displyOtherSchoolSelected)
				.append(TAX_displyOtherVendorCode)
				.append(TAC_displyOtherVendorCode)
				.append(TEN_displyOtherVendorCode)
				.append(VIS_displyOtherVendorCode)
				.append(WOP_displyOtherVendorCode)
				.append(REP_displyOtherVendorCode).append(RLS_vendorCode)
				.append(RLS_vendorName).append(RLS_serviceStartDate)
				.append(RLS_vendorCodeEXSO).append(RLS_vendorContact)
				.append(RLS_serviceEndDate).append(RLS_vendorEmail)
				.append(RLS_displyOtherVendorCode).append(CAT_vendorCode)
				.append(CAT_vendorName).append(CAT_serviceStartDate)
				.append(CAT_vendorCodeEXSO).append(CAT_vendorContact)
				.append(CAT_serviceEndDate).append(CAT_vendorEmail)
				.append(CAT_displyOtherVendorCode).append(CLS_vendorCode)
				.append(CLS_vendorName).append(CLS_serviceStartDate)
				.append(CLS_vendorCodeEXSO).append(CLS_vendorContact)
				.append(CLS_serviceEndDate).append(CLS_vendorEmail)
				.append(CLS_displyOtherVendorCode).append(CHS_vendorCode)
				.append(CHS_vendorName).append(CHS_serviceStartDate)
				.append(CHS_vendorCodeEXSO).append(CHS_vendorContact)
				.append(CHS_serviceEndDate).append(CHS_vendorEmail)
				.append(CHS_displyOtherVendorCode).append(DPS_vendorCode)
				.append(DPS_vendorName).append(DPS_serviceStartDate)
				.append(DPS_vendorCodeEXSO).append(DPS_vendorContact)
				.append(DPS_serviceEndDate).append(DPS_vendorEmail)
				.append(DPS_displyOtherVendorCode).append(HSM_vendorCode)
				.append(HSM_vendorName).append(HSM_serviceStartDate)
				.append(HSM_vendorCodeEXSO).append(HSM_vendorContact)
				.append(HSM_serviceEndDate).append(HSM_vendorEmail)
				.append(HSM_displyOtherVendorCode).append(PDT_vendorCode)
				.append(PDT_vendorName).append(PDT_serviceStartDate)
				.append(PDT_vendorCodeEXSO).append(PDT_vendorContact)
				.append(PDT_serviceEndDate).append(PDT_vendorEmail)
				.append(PDT_displyOtherVendorCode).append(RCP_vendorCode)
				.append(RCP_vendorName).append(RCP_serviceStartDate)
				.append(RCP_vendorCodeEXSO).append(RCP_vendorContact)
				.append(RCP_serviceEndDate).append(RCP_vendorEmail)
				.append(RCP_displyOtherVendorCode).append(SPA_vendorCode)
				.append(SPA_vendorName).append(SPA_serviceStartDate)
				.append(SPA_vendorCodeEXSO).append(SPA_vendorContact)
				.append(SPA_serviceEndDate).append(SPA_vendorEmail)
				.append(SPA_displyOtherVendorCode).append(TCS_vendorCode)
				.append(TCS_vendorName).append(TCS_serviceStartDate)
				.append(TCS_vendorCodeEXSO).append(TCS_vendorContact)
				.append(TCS_serviceEndDate).append(TCS_vendorEmail)
				.append(TCS_displyOtherVendorCode).append(mailServiceType)
				.append(RNT_monthlyRentalAllowanceCurrency)
				.append(RNT_securityDepositCurrency).append(MTS_vendorCode)
				.append(MTS_vendorName).append(MTS_serviceStartDate)
				.append(MTS_vendorCodeEXSO).append(MTS_vendorContact)
				.append(MTS_serviceEndDate).append(MTS_vendorEmail)
				.append(MTS_paymentResponsibility).append(SPA_vendorInitiation)
				.append(MTS_displyOtherVendorCode)
				.append(RLS_paymentResponsibility)
				.append(CAT_paymentResponsibility)
				.append(CLS_paymentResponsibility)
				.append(CHS_paymentResponsibility)
				.append(DPS_paymentResponsibility)
				.append(HSM_paymentResponsibility)
				.append(PDT_paymentResponsibility)
				.append(RCP_paymentResponsibility)
				.append(SPA_paymentResponsibility)
				.append(TCS_paymentResponsibility).append(DPS_serviceType)
				.append(DPS_completionDate).append(DPS_comment)
				.append(HSM_brokerCode).append(HSM_brokerName)
				.append(HSM_brokerContact).append(HSM_brokerEmail)
				.append(HSM_status).append(HSM_comment)
				.append(HSM_marketingPlanNBMAReceived).append(PDT_serviceType)
				.append(PDT_budget).append(RCP_estimateCost)
				.append(RCP_actualCost).append(MTS_lender)
				.append(MTS_initialContact).append(MTS_status)
				.append(MTS_mortgageAmount).append(MTS_mortgageRate)
				.append(MTS_mortgageTerm).append(COL_estimatedTaxAllowance)
				.append(COL_estimatedHousingAllowance)
				.append(COL_estimatedTransportationAllowance)
				.append(RNT_leaseSignee).append(RNT_leaseExtensionNeeded)
				.append(SET_governmentID).append(SET_heathCare)
				.append(SET_utilities).append(SET_automobileRegistration)
				.append(SET_dayofServiceAuthorized)
				.append(SET_initialDateofServiceRequested)
				.append(SET_providerNotificationSent)
				.append(SET_providerConfirmationReceived).append(TAX_allowance)
				.append(VIS_immigrationStatus).append(VIS_arrivalDate)
				.append(VIS_portofEntry).append(DSS_vendorCode)
				.append(DSS_vendorName).append(DSS_serviceStartDate)
				.append(DSS_vendorCodeEXSO).append(DSS_vendorContact)
				.append(DSS_serviceEndDate).append(DSS_vendorEmail)
				.append(DSS_paymentResponsibility)
				.append(DSS_displyOtherVendorCode)
				.append(DSS_initialContactDate)
				.append(PRV_paymentResponsibility)
				.append(VIS_paymentResponsibility)
				.append(TAX_paymentResponsibility)
				.append(RNT_paymentResponsibility)
				.append(AIO_paymentResponsibility)
				.append(TRG_paymentResponsibility)
				.append(LAN_paymentResponsibility)
				.append(RPT_paymentResponsibility)
				.append(COL_paymentResponsibility)
				.append(TAC_paymentResponsibility)
				.append(HOM_paymentResponsibility)
				.append(SCH_paymentResponsibility)
				.append(EXP_paymentResponsibility)
				.append(ONG_paymentResponsibility)
				.append(TEN_paymentResponsibility)
				.append(MMG_paymentResponsibility)
				.append(CAR_paymentResponsibility)
				.append(SET_paymentResponsibility)
				.append(WOP_paymentResponsibility)
				.append(REP_paymentResponsibility).append(HSM_agentCode)
				.append(HSM_agentName).append(HSM_brokerage)
				.append(HSM_agentPhone).append(HSM_agentEmail)
				.append(HSM_estimatedHSRReferral).append(HSM_actualHSRReferral)
				.append(reminderServices).append(CAT_comment)
				.append(CLS_comment).append(CHS_comment).append(PDT_comment)
				.append(RCP_comment).append(SPA_comment).append(TCS_comment)
				.append(MTS_comment).append(DSS_comment)
				.append(HOM_estimatedHSRReferral).append(HOM_actualHSRReferral)
				.append(HSM_offerDate).append(HSM_closingDate)
				.append(TAC_timeAuthorized).append(MMG_originAgentCode)
				.append(MMG_originAgentName).append(MMG_originAgentPhone)
				.append(MMG_originAgentEmail).append(MMG_destinationAgentCode)
				.append(MMG_destinationAgentName)
				.append(MMG_destinationAgentPhone)
				.append(MMG_destinationAgentEmail).append(PRV_emailSent)
				.append(TAX_emailSent).append(AIO_emailSent)
				.append(TRG_emailSent).append(LAN_emailSent)
				.append(RPT_emailSent).append(COL_emailSent)
				.append(TAC_emailSent).append(HOM_emailSent)
				.append(SCH_emailSent).append(EXP_emailSent)
				.append(ONG_emailSent).append(TEN_emailSent)
				.append(MMG_orginEmailSent).append(MMG_destinationEmailSent)
				.append(CAR_emailSent).append(SET_emailSent)
				.append(CAT_emailSent).append(CLS_emailSent)
				.append(CHS_emailSent).append(DPS_emailSent)
				.append(HSM_emailSent).append(PDT_emailSent)
				.append(RCP_emailSent).append(SPA_emailSent)
				.append(TCS_emailSent).append(MTS_emailSent)
				.append(DSS_emailSent).append(VIS_emailSent)
				.append(RNT_emailSent).append(NET_emailSent)
				.append(WOP_emailSent).append(REP_emailSent)
				.append(resendEmailServiceName).append(HOB_startDate)
				.append(HOB_endDate).append(HOB_hotelName).append(HOB_city)
				.append(FLB_arrivalDate).append(FLB_departureDate)
				.append(FLB_additionalBookingReminderDate)
				.append(FLB_arrivalDate1).append(FLB_departureDate1)
				.append(FLB_additionalBookingReminderDate1).append(FLB_addOn)
				.append(HOB_vendorCode).append(HOB_vendorName)
				.append(HOB_vendorContact).append(HOB_vendorEmail)
				.append(HOB_serviceStartDate).append(HOB_serviceEndDate)
				.append(HOB_vendorCodeEXSO).append(FLB_vendorCode)
				.append(FLB_vendorName).append(FLB_vendorContact)
				.append(FLB_vendorEmail).append(FLB_serviceStartDate)
				.append(FLB_serviceEndDate).append(FLB_vendorCodeEXSO)
				.append(FLB_emailSent).append(HOB_emailSent)
				.append(HOB_displyOtherVendorCode)
				.append(FLB_displyOtherVendorCode).append(FRL_vendorCode)
				.append(FRL_vendorName).append(FRL_serviceStartDate)
				.append(FRL_vendorCodeEXSO).append(FRL_vendorContact)
				.append(FRL_serviceEndDate).append(FRL_emailSent)
				.append(FRL_vendorEmail).append(FRL_displyOtherVendorCode)
				.append(FRL_paymentResponsibility).append(FRL_rentalRate)
				.append(FRL_deposit).append(FRL_rentalCurrency)
				.append(FRL_depositCurrency).append(FRL_month)
				.append(FRL_rentalStartDate).append(FRL_rentalEndDate)
				.append(FRL_terminationNotice).append(FRL_comment)
				.append(APU_vendorCode).append(APU_vendorName)
				.append(APU_serviceStartDate).append(APU_vendorCodeEXSO)
				.append(APU_vendorContact).append(APU_serviceEndDate)
				.append(APU_emailSent).append(APU_vendorEmail)
				.append(APU_displyOtherVendorCode)
				.append(APU_paymentResponsibility).append(APU_flightDetails)
				.append(APU_driverName).append(APU_driverPhoneNumber)
				.append(APU_arrivalDate).append(APU_carMake)
				.append(APU_carModel).append(APU_carColor)
				.append(WOP_leaseExNeeded)
				.append(REP_leaseExNeeded)
				.append(TEN_propertyName)
				.append(TEN_city)
				.append(TEN_zipCode)
				.append(TEN_addressLine1)
				.append(TEN_addressLine2)
				.append(TEN_state)
				.append(TEN_country)
				.append(TEN_leasedBy)
				.append(TEN_rentAmount)
				.append(TEN_rentCurrency)
				.append(TEN_rentAllowance)
				.append(TEN_allowanceCurrency)
				.append(TEN_utilitiesIncluded)
				.append(TEN_rentPaidTo)
				.append(TEN_rentalIncreaseDate)
				.append(TEN_rentalComment)
				.append(TEN_followUpNeeded)
				.append(TEN_termOfNotice)
				.append(TEN_checkInMoveIn)
				.append(TEN_preCheckOut)
				.append(TEN_checkOutMoveOut)
				.append(TEN_swiftCode)
				.append(TEN_paymentDescription)
				.append(TEN_exceptionComments)
				.append(TEN_depositAmount).append(TEN_depositReturned).append(TEN_depositCurrency).append(TEN_depositReturnedAmount)
				.append(TEN_depositPaidBy).append(TEN_depositReturnedCurrency).append(TEN_depositComment).append(TEN_refundableTo)
				
				.append(TEN_Gas_Water).append(TEN_TV_Internet_Phone)
				.append(TEN_mobilePhone).append(TEN_furnitureRental).append(TEN_cleaningServices)
				.append(TEN_parkingPermit).append(TEN_communityTax)
				.append(TEN_insurance).append(TEN_gardenMaintenance)
				.append(TEN_toBePaidBy).append(TEN_assigneeContributionAmount)
				.append(TEN_directDebit).append(TEN_description)
				.append(TEN_IBAN_BankAccountNumber).append(TEN_BIC_SWIFT).append(TEN_assigneeContributionCurrency)
				.append(TEN_Utility_Gas_Water).append(TEN_Utility_TV_Internet_Phone).append(TEN_Utility_mobilePhone)
				.append(TEN_Utility_furnitureRental).append(TEN_Utility_cleaningServices).append(TEN_Utility_parkingPermit).append(TEN_Utility_communityTax)
				.append(TEN_Utility_insurance).append(TEN_Utility_gardenMaintenance).append(TEN_contributionAmount)
				.append(TEN_Gas).append(TEN_Electricity).append(TEN_Miscellaneous)
				.append(TEN_Utility_Gas).append(TEN_Utility_Electricity).append(TEN_Utility_Miscellaneous)
				.append(TEN_exceptionAddedValue).append(TEN_Gas_Electric).append(TEN_Utility_Gas_Electric)
				.append(TEN_housingRentVatDesc).append(TEN_billThroughDate).append(TEN_housingRentVatPercent)
				.append(INS_vendorCode).append(INS_vendorName).append(INS_vendorCodeEXSO)
				.append(INS_vendorContact).append(INS_vendorEmail).append(INS_comment)
				.append(INS_vendorInitiation).append(INS_serviceStartDate).append(INS_serviceEndDate)
				.append(INP_vendorCode).append(INP_vendorName).append(INP_vendorCodeEXSO)
				.append(INP_vendorContact).append(INP_vendorEmail).append(INP_comment)
				.append(INP_vendorInitiation).append(INP_serviceStartDate).append(INP_serviceEndDate)
				.append(EDA_vendorCode).append(EDA_vendorName).append(EDA_vendorCodeEXSO)
				.append(EDA_vendorContact).append(EDA_vendorEmail).append(EDA_comment)
				.append(EDA_vendorInitiation).append(EDA_serviceStartDate).append(EDA_serviceEndDate)
				.append(TAS_vendorCode).append(TAS_vendorName).append(TAS_vendorCodeEXSO)
				.append(TAS_vendorContact).append(TAS_vendorEmail).append(TAS_comment)
				.append(TAS_vendorInitiation).append(TAS_serviceStartDate).append(TAS_serviceEndDate)
			     .append(totalDspDays)
				.append(RNT_rsfReceived)
		        .append(RNT_moveInspection)
				.toHashCode();
	}
	@Id 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public String getCAR_vendorCode() {
		return CAR_vendorCode;
	}
	public void setCAR_vendorCode(String cAR_vendorCode) {
		CAR_vendorCode = cAR_vendorCode;
	}
	public String getCAR_vendorName() {
		return CAR_vendorName;
	}
	public void setCAR_vendorName(String cAR_vendorName) {
		CAR_vendorName = cAR_vendorName;
	}
	public String getCAR_vendorContact() {
		return CAR_vendorContact;
	}
	public void setCAR_vendorContact(String cAR_vendorContact) {
		CAR_vendorContact = cAR_vendorContact;
	}
	public String getCAR_vendorEmail() {
		return CAR_vendorEmail;
	}
	public void setCAR_vendorEmail(String cAR_vendorEmail) {
		CAR_vendorEmail = cAR_vendorEmail;
	}
	public Date getCAR_serviceStartDate() {
		return CAR_serviceStartDate;
	}
	public void setCAR_serviceStartDate(Date cAR_serviceStartDate) {
		CAR_serviceStartDate = cAR_serviceStartDate;
	}
	public Date getCAR_serviceEndDate() {
		return CAR_serviceEndDate;
	}
	public void setCAR_serviceEndDate(Date cAR_serviceEndDate) {
		CAR_serviceEndDate = cAR_serviceEndDate;
	}
	public String getCOL_vendorCode() {
		return COL_vendorCode;
	}
	public void setCOL_vendorCode(String cOL_vendorCode) {
		COL_vendorCode = cOL_vendorCode;
	}
	public String getCOL_vendorName() {
		return COL_vendorName;
	}
	public void setCOL_vendorName(String cOL_vendorName) {
		COL_vendorName = cOL_vendorName;
	}
	public String getCOL_vendorContact() {
		return COL_vendorContact;
	}
	public void setCOL_vendorContact(String cOL_vendorContact) {
		COL_vendorContact = cOL_vendorContact;
	}
	public String getCOL_vendorEmail() {
		return COL_vendorEmail;
	}
	public void setCOL_vendorEmail(String cOL_vendorEmail) {
		COL_vendorEmail = cOL_vendorEmail;
	}
	public Date getCOL_serviceStartDate() {
		return COL_serviceStartDate;
	}
	public void setCOL_serviceStartDate(Date cOL_serviceStartDate) {
		COL_serviceStartDate = cOL_serviceStartDate;
	}
	public Date getCOL_serviceEndDate() {
		return COL_serviceEndDate;
	}
	public void setCOL_serviceEndDate(Date cOL_serviceEndDate) {
		COL_serviceEndDate = cOL_serviceEndDate;
	}
	public Date getCOL_notificationDate() {
		return COL_notificationDate;
	}
	public void setCOL_notificationDate(Date cOL_notificationDate) {
		COL_notificationDate = cOL_notificationDate;
	}
	public String getTRG_vendorCode() {
		return TRG_vendorCode;
	}
	public void setTRG_vendorCode(String tRG_vendorCode) {
		TRG_vendorCode = tRG_vendorCode;
	}
	public String getTRG_vendorName() {
		return TRG_vendorName;
	}
	public void setTRG_vendorName(String tRG_vendorName) {
		TRG_vendorName = tRG_vendorName;
	}
	public String getTRG_vendorContact() {
		return TRG_vendorContact;
	}
	public void setTRG_vendorContact(String tRG_vendorContact) {
		TRG_vendorContact = tRG_vendorContact;
	}
	public String getTRG_vendorEmail() {
		return TRG_vendorEmail;
	}
	public void setTRG_vendorEmail(String tRG_vendorEmail) {
		TRG_vendorEmail = tRG_vendorEmail;
	}
	public Date getTRG_serviceStartDate() {
		return TRG_serviceStartDate;
	}
	public void setTRG_serviceStartDate(Date tRG_serviceStartDate) {
		TRG_serviceStartDate = tRG_serviceStartDate;
	}
	public Date getTRG_serviceEndDate() {
		return TRG_serviceEndDate;
	}
	public void setTRG_serviceEndDate(Date tRG_serviceEndDate) {
		TRG_serviceEndDate = tRG_serviceEndDate;
	}

	public String getTRG_employeeDaysAuthorized() {
		return TRG_employeeDaysAuthorized;
	}
	public void setTRG_employeeDaysAuthorized(String tRG_employeeDaysAuthorized) {
		TRG_employeeDaysAuthorized = tRG_employeeDaysAuthorized;
	}
	public String getTRG_spouseDaysAuthorized() {
		return TRG_spouseDaysAuthorized;
	}
	public void setTRG_spouseDaysAuthorized(String tRG_spouseDaysAuthorized) {
		TRG_spouseDaysAuthorized = tRG_spouseDaysAuthorized;
	}
	public String getHOM_vendorCode() {
		return HOM_vendorCode;
	}
	public void setHOM_vendorCode(String hOM_vendorCode) {
		HOM_vendorCode = hOM_vendorCode;
	}
	public String getHOM_vendorName() {
		return HOM_vendorName;
	}
	public void setHOM_vendorName(String hOM_vendorName) {
		HOM_vendorName = hOM_vendorName;
	}
	public String getHOM_vendorContact() {
		return HOM_vendorContact;
	}
	public void setHOM_vendorContact(String hOM_vendorContact) {
		HOM_vendorContact = hOM_vendorContact;
	}
	public String getHOM_vendorEmail() {
		return HOM_vendorEmail;
	}
	public void setHOM_vendorEmail(String hOM_vendorEmail) {
		HOM_vendorEmail = hOM_vendorEmail;
	}
	public Date getHOM_serviceStartDate() {
		return HOM_serviceStartDate;
	}
	public void setHOM_serviceStartDate(Date hOM_serviceStartDate) {
		HOM_serviceStartDate = hOM_serviceStartDate;
	}
	public Date getHOM_serviceEndDate() {
		return HOM_serviceEndDate;
	}
	public void setHOM_serviceEndDate(Date hOM_serviceEndDate) {
		HOM_serviceEndDate = hOM_serviceEndDate;
	}
	public String getHOM_viewStatus() {
		return HOM_viewStatus;
	}
	public void setHOM_viewStatus(String hOM_viewStatus) {
		HOM_viewStatus = hOM_viewStatus;
	}
	public Date getHOM_moveInDate() {
		return HOM_moveInDate;
	}
	public void setHOM_moveInDate(Date hOM_moveInDate) {
		HOM_moveInDate = hOM_moveInDate;
	}
	public String getRNT_vendorCode() {
		return RNT_vendorCode;
	}
	public void setRNT_vendorCode(String rNT_vendorCode) {
		RNT_vendorCode = rNT_vendorCode;
	}
	public String getRNT_vendorName() {
		return RNT_vendorName;
	}
	public void setRNT_vendorName(String rNT_vendorName) {
		RNT_vendorName = rNT_vendorName;
	}
	public String getRNT_vendorContact() {
		return RNT_vendorContact;
	}
	public void setRNT_vendorContact(String rNT_vendorContact) {
		RNT_vendorContact = rNT_vendorContact;
	}
	public String getRNT_vendorEmail() {
		return RNT_vendorEmail;
	}
	public void setRNT_vendorEmail(String rNT_vendorEmail) {
		RNT_vendorEmail = rNT_vendorEmail;
	}
	public Date getRNT_serviceStartDate() {
		return RNT_serviceStartDate;
	}
	public void setRNT_serviceStartDate(Date rNT_serviceStartDate) {
		RNT_serviceStartDate = rNT_serviceStartDate;
	}
	public Date getRNT_serviceEndDate() {
		return RNT_serviceEndDate;
	}
	public void setRNT_serviceEndDate(Date rNT_serviceEndDate) {
		RNT_serviceEndDate = rNT_serviceEndDate;
	}
	public Date getRNT_leaseStartDate() {
		return RNT_leaseStartDate;
	}
	public void setRNT_leaseStartDate(Date rNT_leaseStartDate) {
		RNT_leaseStartDate = rNT_leaseStartDate;
	}
	public Date getRNT_leaseExpireDate() {
		return RNT_leaseExpireDate;
	}
	public void setRNT_leaseExpireDate(Date rNT_leaseExpireDate) {
		RNT_leaseExpireDate = rNT_leaseExpireDate;
	}
	public String getRNT_monthlyRentalAllowance() {
		return RNT_monthlyRentalAllowance;
	}
	public void setRNT_monthlyRentalAllowance(String rNT_monthlyRentalAllowance) {
		RNT_monthlyRentalAllowance = rNT_monthlyRentalAllowance;
	}

	public String getRNT_securityDeposit() {
		return RNT_securityDeposit;
	}
	public void setRNT_securityDeposit(String rNT_securityDeposit) {
		RNT_securityDeposit = rNT_securityDeposit;
	}
	public Date getRNT_reminderDate() {
		return RNT_reminderDate;
	}
	public void setRNT_reminderDate(Date rNT_reminderDate) {
		RNT_reminderDate = rNT_reminderDate;
	}

	public String getLAN_vendorCode() {
		return LAN_vendorCode;
	}
	public void setLAN_vendorCode(String lAN_vendorCode) {
		LAN_vendorCode = lAN_vendorCode;
	}
	public String getLAN_vendorName() {
		return LAN_vendorName;
	}
	public void setLAN_vendorName(String lAN_vendorName) {
		LAN_vendorName = lAN_vendorName;
	}
	public String getLAN_vendorContact() {
		return LAN_vendorContact;
	}
	public void setLAN_vendorContact(String lAN_vendorContact) {
		LAN_vendorContact = lAN_vendorContact;
	}
	public String getLAN_vendorEmail() {
		return LAN_vendorEmail;
	}
	public void setLAN_vendorEmail(String lAN_vendorEmail) {
		LAN_vendorEmail = lAN_vendorEmail;
	}
	public Date getLAN_serviceStartDate() {
		return LAN_serviceStartDate;
	}
	public void setLAN_serviceStartDate(Date lAN_serviceStartDate) {
		LAN_serviceStartDate = lAN_serviceStartDate;
	}
	public Date getLAN_serviceEndDate() {
		return LAN_serviceEndDate;
	}
	public void setLAN_serviceEndDate(Date lAN_serviceEndDate) {
		LAN_serviceEndDate = lAN_serviceEndDate;
	}
	public Date getLAN_notificationDate() {
		return LAN_notificationDate;
	}
	public void setLAN_notificationDate(Date lAN_notificationDate) {
		LAN_notificationDate = lAN_notificationDate;
	}
	public String getLAN_employeeDaysAuthorized() {
		return LAN_employeeDaysAuthorized;
	}
	public void setLAN_employeeDaysAuthorized(String lAN_employeeDaysAuthorized) {
		LAN_employeeDaysAuthorized = lAN_employeeDaysAuthorized;
	}
	public String getLAN_spouseDaysAuthorized() {
		return LAN_spouseDaysAuthorized;
	}
	public void setLAN_spouseDaysAuthorized(String lAN_spouseDaysAuthorized) {
		LAN_spouseDaysAuthorized = lAN_spouseDaysAuthorized;
	}
	public String getMMG_vendorCode() {
		return MMG_vendorCode;
	}
	public void setMMG_vendorCode(String mMG_vendorCode) {
		MMG_vendorCode = mMG_vendorCode;
	}
	public String getMMG_vendorName() {
		return MMG_vendorName;
	}
	public void setMMG_vendorName(String mMG_vendorName) {
		MMG_vendorName = mMG_vendorName;
	}
	public String getMMG_vendorContact() {
		return MMG_vendorContact;
	}
	public void setMMG_vendorContact(String mMG_vendorContact) {
		MMG_vendorContact = mMG_vendorContact;
	}
	public String getMMG_vendorEmail() {
		return MMG_vendorEmail;
	}
	public void setMMG_vendorEmail(String mMG_vendorEmail) {
		MMG_vendorEmail = mMG_vendorEmail;
	}
	public Date getMMG_serviceStartDate() {
		return MMG_serviceStartDate;
	}
	public void setMMG_serviceStartDate(Date mMG_serviceStartDate) {
		MMG_serviceStartDate = mMG_serviceStartDate;
	}
	public Date getMMG_serviceEndDate() {
		return MMG_serviceEndDate;
	}
	public void setMMG_serviceEndDate(Date mMG_serviceEndDate) {
		MMG_serviceEndDate = mMG_serviceEndDate;
	}
	public String getONG_vendorCode() {
		return ONG_vendorCode;
	}
	public void setONG_vendorCode(String oNG_vendorCode) {
		ONG_vendorCode = oNG_vendorCode;
	}
	public String getONG_vendorName() {
		return ONG_vendorName;
	}
	public void setONG_vendorName(String oNG_vendorName) {
		ONG_vendorName = oNG_vendorName;
	}
	public String getONG_vendorContact() {
		return ONG_vendorContact;
	}
	public void setONG_vendorContact(String oNG_vendorContact) {
		ONG_vendorContact = oNG_vendorContact;
	}
	public String getONG_vendorEmail() {
		return ONG_vendorEmail;
	}
	public void setONG_vendorEmail(String oNG_vendorEmail) {
		ONG_vendorEmail = oNG_vendorEmail;
	}
	public Date getONG_serviceStartDate() {
		return ONG_serviceStartDate;
	}
	public void setONG_serviceStartDate(Date oNG_serviceStartDate) {
		ONG_serviceStartDate = oNG_serviceStartDate;
	}
	public Date getONG_serviceEndDate() {
		return ONG_serviceEndDate;
	}
	public void setONG_serviceEndDate(Date oNG_serviceEndDate) {
		ONG_serviceEndDate = oNG_serviceEndDate;
	}
	public String getPRV_vendorCode() {
		return PRV_vendorCode;
	}
	public void setPRV_vendorCode(String pRV_vendorCode) {
		PRV_vendorCode = pRV_vendorCode;
	}
	public String getPRV_vendorName() {
		return PRV_vendorName;
	}
	public void setPRV_vendorName(String pRV_vendorName) {
		PRV_vendorName = pRV_vendorName;
	}
	public String getPRV_vendorContact() {
		return PRV_vendorContact;
	}
	public void setPRV_vendorContact(String pRV_vendorContact) {
		PRV_vendorContact = pRV_vendorContact;
	}
	public String getPRV_vendorEmail() {
		return PRV_vendorEmail;
	}
	public void setPRV_vendorEmail(String pRV_vendorEmail) {
		PRV_vendorEmail = pRV_vendorEmail;
	}
	public Date getPRV_serviceStartDate() {
		return PRV_serviceStartDate;
	}
	public void setPRV_serviceStartDate(Date pRV_serviceStartDate) {
		PRV_serviceStartDate = pRV_serviceStartDate;
	}
	public Date getPRV_serviceEndDate() {
		return PRV_serviceEndDate;
	}
	public void setPRV_serviceEndDate(Date pRV_serviceEndDate) {
		PRV_serviceEndDate = pRV_serviceEndDate;
	}
	public Date getPRV_travelPlannerNotification() {
		return PRV_travelPlannerNotification;
	}
	public void setPRV_travelPlannerNotification(Date pRV_travelPlannerNotification) {
		PRV_travelPlannerNotification = pRV_travelPlannerNotification;
	}
	public Date getPRV_travelDeparture() {
		return PRV_travelDeparture;
	}
	public void setPRV_travelDeparture(Date pRV_travelDeparture) {
		PRV_travelDeparture = pRV_travelDeparture;
	}
	public Date getPRV_travelArrival() {
		return PRV_travelArrival;
	}
	public void setPRV_travelArrival(Date pRV_travelArrival) {
		PRV_travelArrival = pRV_travelArrival;
	}
	public String getPRV_hotelDetails() {
		return PRV_hotelDetails;
	}
	public void setPRV_hotelDetails(String pRV_hotelDetails) {
		PRV_hotelDetails = pRV_hotelDetails;
	}
	public Date getPRV_providerNotificationAndServicesAuthorized() {
		return PRV_providerNotificationAndServicesAuthorized;
	}
	public void setPRV_providerNotificationAndServicesAuthorized(
			Date pRV_providerNotificationAndServicesAuthorized) {
		PRV_providerNotificationAndServicesAuthorized = pRV_providerNotificationAndServicesAuthorized;
	}
	public Date getPRV_meetGgreetAtAirport() {
		return PRV_meetGgreetAtAirport;
	}
	public void setPRV_meetGgreetAtAirport(Date pRV_meetGgreetAtAirport) {
		PRV_meetGgreetAtAirport = pRV_meetGgreetAtAirport;
	}
	public String getPRV_areaOrientationDays() {
		return PRV_areaOrientationDays;
	}
	public void setPRV_areaOrientationDays(String pRV_areaOrientationDays) {
		PRV_areaOrientationDays = pRV_areaOrientationDays;
	}
	public String getPRV_flightNumber() {
		return PRV_flightNumber;
	}
	public void setPRV_flightNumber(String pRV_flightNumber) {
		PRV_flightNumber = pRV_flightNumber;
	}
	public String getPRV_flightArrivalTime() {
		return PRV_flightArrivalTime;
	}
	public void setPRV_flightArrivalTime(String pRV_flightArrivalTime) {
		PRV_flightArrivalTime = pRV_flightArrivalTime;
	}
	public String getAIO_airlineFlight() {
		return AIO_airlineFlight;
	}
	public void setAIO_airlineFlight(String aIO_airlineFlight) {
		AIO_airlineFlight = aIO_airlineFlight;
	}
	public String getAIO_contactInformation() {
		return AIO_contactInformation;
	}
	public void setAIO_contactInformation(String aIO_contactInformation) {
		AIO_contactInformation = aIO_contactInformation;
	}
	public Date getAIO_flightArrival() {
		return AIO_flightArrival;
	}
	public void setAIO_flightArrival(Date aIO_flightArrival) {
		AIO_flightArrival = aIO_flightArrival;
	}
	public Date getAIO_flightDeparture() {
		return AIO_flightDeparture;
	}
	public void setAIO_flightDeparture(Date aIO_flightDeparture) {
		AIO_flightDeparture = aIO_flightDeparture;
	}
	public Date getAIO_initialDateOfServiceRequested() {
		return AIO_initialDateOfServiceRequested;
	}
	public void setAIO_initialDateOfServiceRequested(
			Date aIO_initialDateOfServiceRequested) {
		AIO_initialDateOfServiceRequested = aIO_initialDateOfServiceRequested;
	}
	public Date getAIO_prearrivalDiscussionOfProviderAndFamily() {
		return AIO_prearrivalDiscussionOfProviderAndFamily;
	}
	public void setAIO_prearrivalDiscussionOfProviderAndFamily(
			Date aIO_prearrivalDiscussionOfProviderAndFamily) {
		AIO_prearrivalDiscussionOfProviderAndFamily = aIO_prearrivalDiscussionOfProviderAndFamily;
	}
	public Date getAIO_providerConfirmationReceived() {
		return AIO_providerConfirmationReceived;
	}
	public void setAIO_providerConfirmationReceived(
			Date aIO_providerConfirmationReceived) {
		AIO_providerConfirmationReceived = aIO_providerConfirmationReceived;
	}
	public Date getAIO_providerNotificationSent() {
		return AIO_providerNotificationSent;
	}
	public void setAIO_providerNotificationSent(Date aIO_providerNotificationSent) {
		AIO_providerNotificationSent = aIO_providerNotificationSent;
	}
	public String getAIO_serviceAuthorized() {
		return AIO_serviceAuthorized;
	}
	public void setAIO_serviceAuthorized(String aIO_serviceAuthorized) {
		AIO_serviceAuthorized = aIO_serviceAuthorized;
	}
	public String getAIO_vendorCode() {
		return AIO_vendorCode;
	}
	public void setAIO_vendorCode(String aIO_vendorCode) {
		AIO_vendorCode = aIO_vendorCode;
	}
	public String getAIO_vendorName() {
		return AIO_vendorName;
	}
	public void setAIO_vendorName(String aIO_vendorName) {
		AIO_vendorName = aIO_vendorName;
	}
	public String getAIO_vendorContact() {
		return AIO_vendorContact;
	}
	public void setAIO_vendorContact(String aIO_vendorContact) {
		AIO_vendorContact = aIO_vendorContact;
	}
	public String getAIO_vendorEmail() {
		return AIO_vendorEmail;
	}
	public void setAIO_vendorEmail(String aIO_vendorEmail) {
		AIO_vendorEmail = aIO_vendorEmail;
	}
	public Date getAIO_serviceStartDate() {
		return AIO_serviceStartDate;
	}
	public void setAIO_serviceStartDate(Date aIO_serviceStartDate) {
		AIO_serviceStartDate = aIO_serviceStartDate;
	}
	public Date getAIO_serviceEndDate() {
		return AIO_serviceEndDate;
	}
	public void setAIO_serviceEndDate(Date aIO_serviceEndDate) {
		AIO_serviceEndDate = aIO_serviceEndDate;
	}
	public String getEXP_vendorCode() {
		return EXP_vendorCode;
	}
	public void setEXP_vendorCode(String eXP_vendorCode) {
		EXP_vendorCode = eXP_vendorCode;
	}
	public String getEXP_vendorName() {
		return EXP_vendorName;
	}
	public void setEXP_vendorName(String eXP_vendorName) {
		EXP_vendorName = eXP_vendorName;
	}
	public String getEXP_vendorContact() {
		return EXP_vendorContact;
	}
	public void setEXP_vendorContact(String eXP_vendorContact) {
		EXP_vendorContact = eXP_vendorContact;
	}
	public String getEXP_vendorEmail() {
		return EXP_vendorEmail;
	}
	public void setEXP_vendorEmail(String eXP_vendorEmail) {
		EXP_vendorEmail = eXP_vendorEmail;
	}
	public Date getEXP_serviceStartDate() {
		return EXP_serviceStartDate;
	}
	public void setEXP_serviceStartDate(Date eXP_serviceStartDate) {
		EXP_serviceStartDate = eXP_serviceStartDate;
	}
	public Date getEXP_serviceEndDate() {
		return EXP_serviceEndDate;
	}
	public void setEXP_serviceEndDate(Date eXP_serviceEndDate) {
		EXP_serviceEndDate = eXP_serviceEndDate;
	}
	public String getEXP_paidStatus() {
		return EXP_paidStatus;
	}
	public void setEXP_paidStatus(String eXP_paidStatus) {
		EXP_paidStatus = eXP_paidStatus;
	}
	public Date getEXP_providerNotification() {
		return EXP_providerNotification;
	}
	public void setEXP_providerNotification(Date eXP_providerNotification) {
		EXP_providerNotification = eXP_providerNotification;
	}
	public String getRPT_vendorCode() {
		return RPT_vendorCode;
	}
	public void setRPT_vendorCode(String rPT_vendorCode) {
		RPT_vendorCode = rPT_vendorCode;
	}
	public String getRPT_vendorName() {
		return RPT_vendorName;
	}
	public void setRPT_vendorName(String rPT_vendorName) {
		RPT_vendorName = rPT_vendorName;
	}
	public String getRPT_vendorContact() {
		return RPT_vendorContact;
	}
	public void setRPT_vendorContact(String rPT_vendorContact) {
		RPT_vendorContact = rPT_vendorContact;
	}
	public String getRPT_vendorEmail() {
		return RPT_vendorEmail;
	}
	public void setRPT_vendorEmail(String rPT_vendorEmail) {
		RPT_vendorEmail = rPT_vendorEmail;
	}
	public Date getRPT_serviceStartDate() {
		return RPT_serviceStartDate;
	}
	public void setRPT_serviceStartDate(Date rPT_serviceStartDate) {
		RPT_serviceStartDate = rPT_serviceStartDate;
	}
	public Date getRPT_serviceEndDate() {
		return RPT_serviceEndDate;
	}
	public void setRPT_serviceEndDate(Date rPT_serviceEndDate) {
		RPT_serviceEndDate = rPT_serviceEndDate;
	}

	public Date getRPT_utilitiesShutOff() {
		return RPT_utilitiesShutOff;
	}
	public void setRPT_utilitiesShutOff(Date rPT_utilitiesShutOff) {
		RPT_utilitiesShutOff = rPT_utilitiesShutOff;
	}
	public Date getRPT_moveOutDate() {
		return RPT_moveOutDate;
	}
	public void setRPT_moveOutDate(Date rPT_moveOutDate) {
		RPT_moveOutDate = rPT_moveOutDate;
	}

	public Date getSCH_serviceStartDate() {
		return SCH_serviceStartDate;
	}
	public void setSCH_serviceStartDate(Date sCH_serviceStartDate) {
		SCH_serviceStartDate = sCH_serviceStartDate;
	}
	public Date getSCH_serviceEndDate() {
		return SCH_serviceEndDate;
	}
	public void setSCH_serviceEndDate(Date sCH_serviceEndDate) {
		SCH_serviceEndDate = sCH_serviceEndDate;
	}
	public Date getSCH_prearrivalDiscussionBetweenProviderAndFamily() {
		return SCH_prearrivalDiscussionBetweenProviderAndFamily;
	}
	public void setSCH_prearrivalDiscussionBetweenProviderAndFamily(
			Date sCH_prearrivalDiscussionBetweenProviderAndFamily) {
		SCH_prearrivalDiscussionBetweenProviderAndFamily = sCH_prearrivalDiscussionBetweenProviderAndFamily;
	}

	public String getTAX_vendorCode() {
		return TAX_vendorCode;
	}
	public void setTAX_vendorCode(String tAX_vendorCode) {
		TAX_vendorCode = tAX_vendorCode;
	}
	public String getTAX_vendorName() {
		return TAX_vendorName;
	}
	public void setTAX_vendorName(String tAX_vendorName) {
		TAX_vendorName = tAX_vendorName;
	}
	public String getTAX_vendorContact() {
		return TAX_vendorContact;
	}
	public void setTAX_vendorContact(String tAX_vendorContact) {
		TAX_vendorContact = tAX_vendorContact;
	}
	public String getTAX_vendorEmail() {
		return TAX_vendorEmail;
	}
	public void setTAX_vendorEmail(String tAX_vendorEmail) {
		TAX_vendorEmail = tAX_vendorEmail;
	}
	public Date getTAX_serviceStartDate() {
		return TAX_serviceStartDate;
	}
	public void setTAX_serviceStartDate(Date tAX_serviceStartDate) {
		TAX_serviceStartDate = tAX_serviceStartDate;
	}
	public Date getTAX_serviceEndDate() {
		return TAX_serviceEndDate;
	}
	public void setTAX_serviceEndDate(Date tAX_serviceEndDate) {
		TAX_serviceEndDate = tAX_serviceEndDate;
	}
	public Date getTAX_notificationdate() {
		return TAX_notificationdate;
	}
	public void setTAX_notificationdate(Date tAX_notificationdate) {
		TAX_notificationdate = tAX_notificationdate;
	}
	public String getTAC_vendorCode() {
		return TAC_vendorCode;
	}
	public void setTAC_vendorCode(String tAC_vendorCode) {
		TAC_vendorCode = tAC_vendorCode;
	}
	public String getTAC_vendorName() {
		return TAC_vendorName;
	}
	public void setTAC_vendorName(String tAC_vendorName) {
		TAC_vendorName = tAC_vendorName;
	}
	public String getTAC_vendorContact() {
		return TAC_vendorContact;
	}
	public void setTAC_vendorContact(String tAC_vendorContact) {
		TAC_vendorContact = tAC_vendorContact;
	}
	public String getTAC_vendorEmail() {
		return TAC_vendorEmail;
	}
	public void setTAC_vendorEmail(String tAC_vendorEmail) {
		TAC_vendorEmail = tAC_vendorEmail;
	}
	public Date getTAC_serviceStartDate() {
		return TAC_serviceStartDate;
	}
	public void setTAC_serviceStartDate(Date tAC_serviceStartDate) {
		TAC_serviceStartDate = tAC_serviceStartDate;
	}
	public Date getTAC_serviceEndDate() {
		return TAC_serviceEndDate;
	}
	public void setTAC_serviceEndDate(Date tAC_serviceEndDate) {
		TAC_serviceEndDate = tAC_serviceEndDate;
	}

	public String getTEN_vendorCode() {
		return TEN_vendorCode;
	}
	public void setTEN_vendorCode(String tEN_vendorCode) {
		TEN_vendorCode = tEN_vendorCode;
	}
	public String getTEN_vendorName() {
		return TEN_vendorName;
	}
	public void setTEN_vendorName(String tEN_vendorName) {
		TEN_vendorName = tEN_vendorName;
	}
	public String getTEN_vendorContact() {
		return TEN_vendorContact;
	}
	public void setTEN_vendorContact(String tEN_vendorContact) {
		TEN_vendorContact = tEN_vendorContact;
	}
	public String getTEN_vendorEmail() {
		return TEN_vendorEmail;
	}
	public void setTEN_vendorEmail(String tEN_vendorEmail) {
		TEN_vendorEmail = tEN_vendorEmail;
	}
	public Date getTEN_serviceStartDate() {
		return TEN_serviceStartDate;
	}
	public void setTEN_serviceStartDate(Date tEN_serviceStartDate) {
		TEN_serviceStartDate = tEN_serviceStartDate;
	}
	public Date getTEN_serviceEndDate() {
		return TEN_serviceEndDate;
	}
	public void setTEN_serviceEndDate(Date tEN_serviceEndDate) {
		TEN_serviceEndDate = tEN_serviceEndDate;
	}
	public String getVIS_vendorCode() {
		return VIS_vendorCode;
	}
	public void setVIS_vendorCode(String vIS_vendorCode) {
		VIS_vendorCode = vIS_vendorCode;
	}
	public String getVIS_vendorName() {
		return VIS_vendorName;
	}
	public void setVIS_vendorName(String vIS_vendorName) {
		VIS_vendorName = vIS_vendorName;
	}
	public String getVIS_vendorContact() {
		return VIS_vendorContact;
	}
	public void setVIS_vendorContact(String vIS_vendorContact) {
		VIS_vendorContact = vIS_vendorContact;
	}
	public String getVIS_vendorEmail() {
		return VIS_vendorEmail;
	}
	public void setVIS_vendorEmail(String vIS_vendorEmail) {
		VIS_vendorEmail = vIS_vendorEmail;
	}
	public Date getVIS_serviceStartDate() {
		return VIS_serviceStartDate;
	}
	public void setVIS_serviceStartDate(Date vIS_serviceStartDate) {
		VIS_serviceStartDate = vIS_serviceStartDate;
	}
	public Date getVIS_serviceEndDate() {
		return VIS_serviceEndDate;
	}
	public void setVIS_serviceEndDate(Date vIS_serviceEndDate) {
		VIS_serviceEndDate = vIS_serviceEndDate;
	}
	public String getVIS_workPermitVisaHolderName() {
		return VIS_workPermitVisaHolderName;
	}
	public void setVIS_workPermitVisaHolderName(String vIS_workPermitVisaHolderName) {
		VIS_workPermitVisaHolderName = vIS_workPermitVisaHolderName;
	}
	public Date getVIS_providerNotificationDate() {
		return VIS_providerNotificationDate;
	}
	public void setVIS_providerNotificationDate(Date vIS_providerNotificationDate) {
		VIS_providerNotificationDate = vIS_providerNotificationDate;
	}
	public Date getVIS_visaExpiryDate() {
		return VIS_visaExpiryDate;
	}
	public void setVIS_visaExpiryDate(Date vIS_visaExpiryDate) {
		VIS_visaExpiryDate = vIS_visaExpiryDate;
	}
	public Date getVIS_workPermitExpiry() {
		return VIS_workPermitExpiry;
	}
	public void setVIS_workPermitExpiry(Date vIS_workPermitExpiry) {
		VIS_workPermitExpiry = vIS_workPermitExpiry;
	}
	public Date getVIS_expiryReminder3MosPriorToExpiry() {
		return VIS_expiryReminder3MosPriorToExpiry;
	}
	public void setVIS_expiryReminder3MosPriorToExpiry(
			Date vIS_expiryReminder3MosPriorToExpiry) {
		VIS_expiryReminder3MosPriorToExpiry = vIS_expiryReminder3MosPriorToExpiry;
	}
	@Column(length = 15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column( length=5000)
	public String getRNT_comment() {
		return RNT_comment;
	}
	public void setRNT_comment(String rNT_comment) {
		RNT_comment = rNT_comment;
	}
	public String getLAN_childrenDaysAuthorized() {
		return LAN_childrenDaysAuthorized;
	}
	public void setLAN_childrenDaysAuthorized(String lAN_childrenDaysAuthorized) {
		LAN_childrenDaysAuthorized = lAN_childrenDaysAuthorized;
	}
	public String getTAC_monthlyRentalAllowance() {
		return TAC_monthlyRentalAllowance;
	}
	public void setTAC_monthlyRentalAllowance(String tAC_monthlyRentalAllowance) {
		TAC_monthlyRentalAllowance = tAC_monthlyRentalAllowance;
	}
	public Date getTAC_leaseStartDate() {
		return TAC_leaseStartDate;
	}
	public void setTAC_leaseStartDate(Date tAC_leaseStartDate) {
		TAC_leaseStartDate = tAC_leaseStartDate;
	}
	public String getTAC_securityDeposit() {
		return TAC_securityDeposit;
	}
	public void setTAC_securityDeposit(String tAC_securityDeposit) {
		TAC_securityDeposit = tAC_securityDeposit;
	}
	public Date getTAC_leaseExpireDate() {
		return TAC_leaseExpireDate;
	}
	public void setTAC_leaseExpireDate(Date tAC_leaseExpireDate) {
		TAC_leaseExpireDate = tAC_leaseExpireDate;
	}

	@Column( length=5000)
	public String getTAC_comment() {
		return TAC_comment;
	}
	public void setTAC_comment(String tAC_comment) {
		TAC_comment = tAC_comment;
	}
	public String getONG_dateType1() {
		return ONG_dateType1;
	}
	public void setONG_dateType1(String oNG_dateType1) {
		ONG_dateType1 = oNG_dateType1;
	}
	public String getONG_dateType2() {
		return ONG_dateType2;
	}
	public void setONG_dateType2(String oNG_dateType2) {
		ONG_dateType2 = oNG_dateType2;
	}
	public String getONG_dateType3() {
		return ONG_dateType3;
	}
	public void setONG_dateType3(String oNG_dateType3) {
		ONG_dateType3 = oNG_dateType3;
	}
	public Date getONG_date1() {
		return ONG_date1;
	}
	public void setONG_date1(Date oNG_date1) {
		ONG_date1 = oNG_date1;
	}
	public Date getONG_date2() {
		return ONG_date2;
	}
	public void setONG_date2(Date oNG_date2) {
		ONG_date2 = oNG_date2;
	}
	public Date getONG_date3() {
		return ONG_date3;
	}
	public void setONG_date3(Date oNG_date3) {
		ONG_date3 = oNG_date3;
	}
	@Column( length=5000)
	public String getONG_comment() {
		return ONG_comment;
	}
	public void setONG_comment(String oNG_comment) {
		ONG_comment = oNG_comment;
	}
	public String getSET_vendorCode() {
		return SET_vendorCode;
	}
	public void setSET_vendorCode(String sET_vendorCode) {
		SET_vendorCode = sET_vendorCode;
	}
	public String getSET_vendorName() {
		return SET_vendorName;
	}
	public void setSET_vendorName(String sET_vendorName) {
		SET_vendorName = sET_vendorName;
	}
	public String getSET_vendorContact() {
		return SET_vendorContact;
	}
	public void setSET_vendorContact(String sET_vendorContact) {
		SET_vendorContact = sET_vendorContact;
	}
	public String getSET_vendorEmail() {
		return SET_vendorEmail;
	}
	public void setSET_vendorEmail(String sET_vendorEmail) {
		SET_vendorEmail = sET_vendorEmail;
	}
	public Date getSET_serviceStartDate() {
		return SET_serviceStartDate;
	}
	public void setSET_serviceStartDate(Date sET_serviceStartDate) {
		SET_serviceStartDate = sET_serviceStartDate;
	}
	public Date getSET_serviceEndDate() {
		return SET_serviceEndDate;
	}
	public void setSET_serviceEndDate(Date sET_serviceEndDate) {
		SET_serviceEndDate = sET_serviceEndDate;
	}
	public Date getSET_parkingPermit() {
		return SET_parkingPermit;
	}
	public void setSET_parkingPermit(Date sET_parkingPermit) {
		SET_parkingPermit = sET_parkingPermit;
	}
	public Date getSET_bankAccount() {
		return SET_bankAccount;
	}
	public void setSET_bankAccount(Date sET_bankAccount) {
		SET_bankAccount = sET_bankAccount;
	}
	public Date getSET_cityHall() {
		return SET_cityHall;
	}
	public void setSET_cityHall(Date sET_cityHall) {
		SET_cityHall = sET_cityHall;
	}
	public Date getSET_internetConn() {
		return SET_internetConn;
	}
	public void setSET_internetConn(Date sET_internetConn) {
		SET_internetConn = sET_internetConn;
	}
	public Date getSET_driverLicense() {
		return SET_driverLicense;
	}
	public void setSET_driverLicense(Date sET_driverLicense) {
		SET_driverLicense = sET_driverLicense;
	}
	@Column( length=5000)
	public String getSET_comment() {
		return SET_comment;
	}
	public void setSET_comment(String sET_comment) {
		SET_comment = sET_comment;
	}
	public String getRPT_damageCost() {
		return RPT_damageCost;
	}
	public void setRPT_damageCost(String rPT_damageCost) {
		RPT_damageCost = rPT_damageCost;
	}
	public String getRPT_securityDeposit() {
		return RPT_securityDeposit;
	}
	public void setRPT_securityDeposit(String rPT_securityDeposit) {
		RPT_securityDeposit = rPT_securityDeposit;
	}

	public String getSCH_city() {
		return SCH_city;
	}
	public void setSCH_city(String sCH_city) {
		SCH_city = sCH_city;
	}
	public int getSCH_noOfChildren() {
		return SCH_noOfChildren;
	}
	public void setSCH_noOfChildren(int sCH_noOfChildren) {
		SCH_noOfChildren = sCH_noOfChildren;
	}
	public String getRNT_negotiatedRent() {
		return RNT_negotiatedRent;
	}
	public void setRNT_negotiatedRent(String rNT_negotiatedRent) {
		RNT_negotiatedRent = rNT_negotiatedRent;
	}
	public String getRNT_utilitiesIncluded() {
		return RNT_utilitiesIncluded;
	}
	public void setRNT_utilitiesIncluded(String rNT_utilitiesIncluded) {
		RNT_utilitiesIncluded = rNT_utilitiesIncluded;
	}
	public String getRNT_depositPaidBy() {
		return RNT_depositPaidBy;
	}
	public void setRNT_depositPaidBy(String rNT_depositPaidBy) {
		RNT_depositPaidBy = rNT_depositPaidBy;
	}
	public String getTAC_monthsWeeksDaysAuthorized() {
		return TAC_monthsWeeksDaysAuthorized;
	}
	public void setTAC_monthsWeeksDaysAuthorized(
			String tAC_monthsWeeksDaysAuthorized) {
		TAC_monthsWeeksDaysAuthorized = tAC_monthsWeeksDaysAuthorized;
	}
	public Date getTAC_expiryReminderPriorToExpiry() {
		return TAC_expiryReminderPriorToExpiry;
	}
	public void setTAC_expiryReminderPriorToExpiry(
			Date tAC_expiryReminderPriorToExpiry) {
		TAC_expiryReminderPriorToExpiry = tAC_expiryReminderPriorToExpiry;
	}
	public String getTAC_negotiatedRent() {
		return TAC_negotiatedRent;
	}
	public void setTAC_negotiatedRent(String tAC_negotiatedRent) {
		TAC_negotiatedRent = tAC_negotiatedRent;
	}
	public String getTAC_utilitiesIncluded() {
		return TAC_utilitiesIncluded;
	}
	public void setTAC_utilitiesIncluded(String tAC_utilitiesIncluded) {
		TAC_utilitiesIncluded = tAC_utilitiesIncluded;
	}
	public String getTAC_depositPaidBy() {
		return TAC_depositPaidBy;
	}
	public void setTAC_depositPaidBy(String tAC_depositPaidBy) {
		TAC_depositPaidBy = tAC_depositPaidBy;
	}
	public Date getRPT_landlordPaidNotificationDate() {
		return RPT_landlordPaidNotificationDate;
	}
	public void setRPT_landlordPaidNotificationDate(
			Date rPT_landlordPaidNotificationDate) {
		RPT_landlordPaidNotificationDate = rPT_landlordPaidNotificationDate;
	}
	public Date getRPT_professionalCleaningDate() {
		return RPT_professionalCleaningDate;
	}
	public void setRPT_professionalCleaningDate(Date rPT_professionalCleaningDate) {
		RPT_professionalCleaningDate = rPT_professionalCleaningDate;
	}
	public String getRPT_comment() {
		return RPT_comment;
	}
	public void setRPT_comment(String rPT_comment) {
		RPT_comment = rPT_comment;
	}
	public Date getRPT_negotiatedDepositReturnedDate() {
		return RPT_negotiatedDepositReturnedDate;
	}
	public void setRPT_negotiatedDepositReturnedDate(
			Date rPT_negotiatedDepositReturnedDate) {
		RPT_negotiatedDepositReturnedDate = rPT_negotiatedDepositReturnedDate;
	}
	public String getRPT_depositReturnedTo() {
		return RPT_depositReturnedTo;
	}
	public void setRPT_depositReturnedTo(String rPT_depositReturnedTo) {
		RPT_depositReturnedTo = rPT_depositReturnedTo;
	}
	public String getSCH_schoolSelected() {
		return SCH_schoolSelected;
	}
	public void setSCH_schoolSelected(String sCH_schoolSelected) {
		SCH_schoolSelected = sCH_schoolSelected;
	}
	public String getSCH_schoolName() {
		return SCH_schoolName;
	}
	public void setSCH_schoolName(String sCH_schoolName) {
		SCH_schoolName = sCH_schoolName;
	}
	public String getSCH_contactPerson() {
		return SCH_contactPerson;
	}
	public void setSCH_contactPerson(String sCH_contactPerson) {
		SCH_contactPerson = sCH_contactPerson;
	}
	public String getSCH_website() {
		return SCH_website;
	}
	public void setSCH_website(String sCH_website) {
		SCH_website = sCH_website;
	}
	public Date getSCH_admissionDate() {
		return SCH_admissionDate;
	}
	public void setSCH_admissionDate(Date sCH_admissionDate) {
		SCH_admissionDate = sCH_admissionDate;
	}
	@Column(length=100)
	public String getVIS_assignees() {
		return VIS_assignees;
	}
	public void setVIS_assignees(String vIS_assignees) {
		VIS_assignees = vIS_assignees;
	}
	@Column(length=100)
	public String getVIS_employers() {
		return VIS_employers;
	}
	public void setVIS_employers(String vIS_employers) {
		VIS_employers = vIS_employers;
	}
	@Column(length=100)
	public String getVIS_convenant() {
		return VIS_convenant;
	}
	public void setVIS_convenant(String vIS_convenant) {
		VIS_convenant = vIS_convenant;
	}
	@Column
	public Date getVIS_visaExpiryDateRP() {
		return VIS_visaExpiryDateRP;
	}
	public void setVIS_visaExpiryDateRP(Date vIS_visaExpiryDateRP) {
		VIS_visaExpiryDateRP = vIS_visaExpiryDateRP;
	}
	@Column
	public Date getVIS_expiryReminder3MosPriorToExpiryRP() {
		return VIS_expiryReminder3MosPriorToExpiryRP;
	}
	public void setVIS_expiryReminder3MosPriorToExpiryRP(
			Date vIS_expiryReminder3MosPriorToExpiryRP) {
		VIS_expiryReminder3MosPriorToExpiryRP = vIS_expiryReminder3MosPriorToExpiryRP;
	}
	@Column
	public Date getVIS_residenceExpiryRP() {
		return VIS_residenceExpiryRP;
	}
	public void setVIS_residenceExpiryRP(Date vIS_residenceExpiryRP) {
		VIS_residenceExpiryRP = vIS_residenceExpiryRP;
	}
	@Column
	public Date getVIS_questionnaireSentDate() {
		return VIS_questionnaireSentDate;
	}
	public void setVIS_questionnaireSentDate(Date vIS_questionnaireSentDate) {
		VIS_questionnaireSentDate = vIS_questionnaireSentDate;
	}
	@Column(length=65)
	public String getVIS_primaryEmail() {
		return VIS_primaryEmail;
	}
	public void setVIS_primaryEmail(String vIS_primaryEmail) {
		VIS_primaryEmail = vIS_primaryEmail;
	}
	@Column
	public Boolean getVIS_authorizationLetter() {
		return VIS_authorizationLetter;
	}
	public void setVIS_authorizationLetter(Boolean vIS_authorizationLetter) {
		VIS_authorizationLetter = vIS_authorizationLetter;
	}
	@Column
	public Boolean getVIS_copyResidence() {
		return VIS_copyResidence;
	}
	public void setVIS_copyResidence(Boolean vIS_copyResidence) {
		VIS_copyResidence = vIS_copyResidence;
	}
	@Column(length=100)
	public String getVIS_residencePermitHolderNameRP() {
		return VIS_residencePermitHolderNameRP;
	}
	public void setVIS_residencePermitHolderNameRP(
			String vIS_residencePermitHolderNameRP) {
		VIS_residencePermitHolderNameRP = vIS_residencePermitHolderNameRP;
	}
	@Column
	public Date getVIS_providerNotificationDateRP() {
		return VIS_providerNotificationDateRP;
	}
	public void setVIS_providerNotificationDateRP(
			Date vIS_providerNotificationDateRP) {
		VIS_providerNotificationDateRP = vIS_providerNotificationDateRP;
	}
	@Column(length=10)
	public String getTRG_employeeTime() {
		return TRG_employeeTime;
	}
	public void setTRG_employeeTime(String tRG_employeeTime) {
		TRG_employeeTime = tRG_employeeTime;
	}
	@Column(length=10)
	public String getTRG_spouseTime() {
		return TRG_spouseTime;
	}
	public void setTRG_spouseTime(String tRG_spouseTime) {
		TRG_spouseTime = tRG_spouseTime;
	}
	@Column(length=3)
	public String getRNT_paidCurrency() {
		return RNT_paidCurrency;
	}
	public void setRNT_paidCurrency(String rNT_paidCurrency) {
		RNT_paidCurrency = rNT_paidCurrency;
	}
	@Column(length=10)
	public String getLAN_employeeTime() {
		return LAN_employeeTime;
	}
	public void setLAN_employeeTime(String lAN_employeeTime) {
		LAN_employeeTime = lAN_employeeTime;
	}
	@Column(length=10)
	public String getLAN_spouseTime() {
		return LAN_spouseTime;
	}
	public void setLAN_spouseTime(String lAN_spouseTime) {
		LAN_spouseTime = lAN_spouseTime;
	}
	@Column(length=10)
	public String getLAN_childrenTime() {
		return LAN_childrenTime;
	}
	public void setLAN_childrenTime(String lAN_childrenTime) {
		LAN_childrenTime = lAN_childrenTime;
	}
	
	@Column(length=25)
	public String getCAR_vendorCodeEXSO() {
		return CAR_vendorCodeEXSO;
	}
	public void setCAR_vendorCodeEXSO(String cAR_vendorCodeEXSO) {
		CAR_vendorCodeEXSO = cAR_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getCOL_vendorCodeEXSO() {
		return COL_vendorCodeEXSO;
	}
	public void setCOL_vendorCodeEXSO(String cOL_vendorCodeEXSO) {
		COL_vendorCodeEXSO = cOL_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getTRG_vendorCodeEXSO() {
		return TRG_vendorCodeEXSO;
	}
	public void setTRG_vendorCodeEXSO(String tRG_vendorCodeEXSO) {
		TRG_vendorCodeEXSO = tRG_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getHOM_vendorCodeEXSO() {
		return HOM_vendorCodeEXSO;
	}
	public void setHOM_vendorCodeEXSO(String hOM_vendorCodeEXSO) {
		HOM_vendorCodeEXSO = hOM_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getRNT_vendorCodeEXSO() {
		return RNT_vendorCodeEXSO;
	}
	public void setRNT_vendorCodeEXSO(String rNT_vendorCodeEXSO) {
		RNT_vendorCodeEXSO = rNT_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getLAN_vendorCodeEXSO() {
		return LAN_vendorCodeEXSO;
	}
	public void setLAN_vendorCodeEXSO(String lAN_vendorCodeEXSO) {
		LAN_vendorCodeEXSO = lAN_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getMMG_vendorCodeEXSO() {
		return MMG_vendorCodeEXSO;
	}
	public void setMMG_vendorCodeEXSO(String mMG_vendorCodeEXSO) {
		MMG_vendorCodeEXSO = mMG_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getONG_vendorCodeEXSO() {
		return ONG_vendorCodeEXSO;
	}
	public void setONG_vendorCodeEXSO(String oNG_vendorCodeEXSO) {
		ONG_vendorCodeEXSO = oNG_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getPRV_vendorCodeEXSO() {
		return PRV_vendorCodeEXSO;
	}
	public void setPRV_vendorCodeEXSO(String pRV_vendorCodeEXSO) {
		PRV_vendorCodeEXSO = pRV_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getAIO_vendorCodeEXSO() {
		return AIO_vendorCodeEXSO;
	}
	public void setAIO_vendorCodeEXSO(String aIO_vendorCodeEXSO) {
		AIO_vendorCodeEXSO = aIO_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getEXP_vendorCodeEXSO() {
		return EXP_vendorCodeEXSO;
	}
	public void setEXP_vendorCodeEXSO(String eXP_vendorCodeEXSO) {
		EXP_vendorCodeEXSO = eXP_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getRPT_vendorCodeEXSO() {
		return RPT_vendorCodeEXSO;
	}
	public void setRPT_vendorCodeEXSO(String rPT_vendorCodeEXSO) {
		RPT_vendorCodeEXSO = rPT_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getSCH_vendorCodeEXSO() {
		return SCH_vendorCodeEXSO;
	}
	public void setSCH_vendorCodeEXSO(String sCH_vendorCodeEXSO) {
		SCH_vendorCodeEXSO = sCH_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getTAX_vendorCodeEXSO() {
		return TAX_vendorCodeEXSO;
	}
	public void setTAX_vendorCodeEXSO(String tAX_vendorCodeEXSO) {
		TAX_vendorCodeEXSO = tAX_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getTAC_vendorCodeEXSO() {
		return TAC_vendorCodeEXSO;
	}
	public void setTAC_vendorCodeEXSO(String tAC_vendorCodeEXSO) {
		TAC_vendorCodeEXSO = tAC_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getTEN_vendorCodeEXSO() {
		return TEN_vendorCodeEXSO;
	}
	public void setTEN_vendorCodeEXSO(String tEN_vendorCodeEXSO) {
		TEN_vendorCodeEXSO = tEN_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getVIS_vendorCodeEXSO() {
		return VIS_vendorCodeEXSO;
	}
	public void setVIS_vendorCodeEXSO(String vIS_vendorCodeEXSO) {
		VIS_vendorCodeEXSO = vIS_vendorCodeEXSO;
	}
	
	@Column(length=25)
	public String getSET_vendorCodeEXSO() {
		return SET_vendorCodeEXSO;
	}
	public void setSET_vendorCodeEXSO(String sET_vendorCodeEXSO) {
		SET_vendorCodeEXSO = sET_vendorCodeEXSO;
	}
	@Column
	public Date getCAR_notificationDate() {
		return CAR_notificationDate;
	}
	public void setCAR_notificationDate(Date cAR_notificationDate) {
		CAR_notificationDate = cAR_notificationDate;
	}
	@Column
	public Date getCAR_pickUpDate() {
		return CAR_pickUpDate;
	}
	public void setCAR_pickUpDate(Date cAR_pickUpDate) {
		CAR_pickUpDate = cAR_pickUpDate;
	}
	@Column
	public Date getCAR_registrationDate() {
		return CAR_registrationDate;
	}
	public void setCAR_registrationDate(Date cAR_registrationDate) {
		CAR_registrationDate = cAR_registrationDate;
	}
	@Column
	public Date getCAR_deliveryDate() {
		return CAR_deliveryDate;
	}
	public void setCAR_deliveryDate(Date cAR_deliveryDate) {
		CAR_deliveryDate = cAR_deliveryDate;
	}
	public String getCOL_estimatedAllowanceEuro() {
		return COL_estimatedAllowanceEuro;
	}
	public void setCOL_estimatedAllowanceEuro(String cOL_estimatedAllowanceEuro) {
		COL_estimatedAllowanceEuro = cOL_estimatedAllowanceEuro;
	}
	@Column
	public Date getVIS_applicationDateWork() {
		return VIS_applicationDateWork;
	}
	public void setVIS_applicationDateWork(Date vIS_applicationDateWork) {
		VIS_applicationDateWork = vIS_applicationDateWork;
	}
	@Column
	public Date getVIS_permitStartDateWork() {
		return VIS_permitStartDateWork;
	}
	public void setVIS_permitStartDateWork(Date vIS_permitStartDateWork) {
		VIS_permitStartDateWork = vIS_permitStartDateWork;
	}
	@Column
	public Date getVIS_applicationDateResidence() {
		return VIS_applicationDateResidence;
	}
	public void setVIS_applicationDateResidence(Date vIS_applicationDateResidence) {
		VIS_applicationDateResidence = vIS_applicationDateResidence;
	}
	@Column
	public Date getVIS_permitStartDateResidence() {
		return VIS_permitStartDateResidence;
	}
	public void setVIS_permitStartDateResidence(Date vIS_permitStartDateResidence) {
		VIS_permitStartDateResidence = vIS_permitStartDateResidence;
	}
	@Column( length=5000)
	public String getCAR_comment() {
		return CAR_comment;
	}
	public void setCAR_comment(String cAR_comment) {
		CAR_comment = cAR_comment;
	}
	@Column( length=5000)
	public String getCOL_comment() {
		return COL_comment;
	}
	public void setCOL_comment(String cOL_comment) {
		COL_comment = cOL_comment;
	}
	@Column( length=5000)
	public String getTRG_comment() {
		return TRG_comment;
	}
	public void setTRG_comment(String tRG_comment) {
		TRG_comment = tRG_comment;
	}
	@Column( length=5000)
	public String getHOM_comment() {
		return HOM_comment;
	}
	
	public void setHOM_comment(String hOM_comment) {
		HOM_comment = hOM_comment;
	}
	@Column( length=5000)
	public String getLAN_comment() {
		return LAN_comment;
	}
	public void setLAN_comment(String lAN_comment) {
		LAN_comment = lAN_comment;
	}
	@Column( length=5000)
	public String getMMG_comment() {
		return MMG_comment;
	}
	public void setMMG_comment(String mMG_comment) {
		MMG_comment = mMG_comment;
	}
	@Column( length=5000)
	public String getPRV_comment() {
		return PRV_comment;
	}
	public void setPRV_comment(String pRV_comment) {
		PRV_comment = pRV_comment;
	}
	@Column( length=5000)
	public String getAIO_comment() {
		return AIO_comment;
	}
	public void setAIO_comment(String aIO_comment) {
		AIO_comment = aIO_comment;
	}
	@Column( length=5000)
	public String getEXP_comment() {
		return EXP_comment;
	}
	public void setEXP_comment(String eXP_comment) {
		EXP_comment = eXP_comment;
	}
	@Column( length=5000)
	public String getSCH_comment() {
		return SCH_comment;
	}
	public void setSCH_comment(String sCH_comment) {
		SCH_comment = sCH_comment;
	}
	@Column( length=5000)
	public String getTAX_comment() {
		return TAX_comment;
	}
	public void setTAX_comment(String tAX_comment) {
		TAX_comment = tAX_comment;
	}
	@Column( length=5000)
	public String getTEN_comment() {
		return TEN_comment;
	}
	public void setTEN_comment(String tEN_comment) {
		TEN_comment = tEN_comment;
	}
	@Column( length=5000)
	public String getVIS_comment() {
		return VIS_comment;
	}
	public void setVIS_comment(String vIS_comment) {
		VIS_comment = vIS_comment;
	}
	@Column
	public Date getServiceCompleteDate() {
		return serviceCompleteDate;
	}
	public void setServiceCompleteDate(Date serviceCompleteDate) {
		this.serviceCompleteDate = serviceCompleteDate;
	}
	@Column
	public Boolean getCAR_displyOtherVendorCode() {
		return CAR_displyOtherVendorCode;
	}
	public void setCAR_displyOtherVendorCode(Boolean cAR_displyOtherVendorCode) {
		CAR_displyOtherVendorCode = cAR_displyOtherVendorCode;
	}
	@Column
	public Boolean getCOL_displyOtherVendorCode() {
		return COL_displyOtherVendorCode;
	}
	public void setCOL_displyOtherVendorCode(Boolean cOL_displyOtherVendorCode) {
		COL_displyOtherVendorCode = cOL_displyOtherVendorCode;
	}
	@Column
	public Boolean getTRG_displyOtherVendorCode() {
		return TRG_displyOtherVendorCode;
	}
	public void setTRG_displyOtherVendorCode(Boolean tRG_displyOtherVendorCode) {
		TRG_displyOtherVendorCode = tRG_displyOtherVendorCode;
	}
	@Column
	public Boolean getHOM_displyOtherVendorCode() {
		return HOM_displyOtherVendorCode;
	}
	public void setHOM_displyOtherVendorCode(Boolean hOM_displyOtherVendorCode) {
		HOM_displyOtherVendorCode = hOM_displyOtherVendorCode;
	}
	@Column
	public Boolean getRNT_displyOtherVendorCode() {
		return RNT_displyOtherVendorCode;
	}
	public void setRNT_displyOtherVendorCode(Boolean rNT_displyOtherVendorCode) {
		RNT_displyOtherVendorCode = rNT_displyOtherVendorCode;
	}
	@Column
	public Boolean getSET_displyOtherVendorCode() {
		return SET_displyOtherVendorCode;
	}
	public void setSET_displyOtherVendorCode(Boolean sET_displyOtherVendorCode) {
		SET_displyOtherVendorCode = sET_displyOtherVendorCode;
	}
	@Column
	public Boolean getLAN_displyOtherVendorCode() {
		return LAN_displyOtherVendorCode;
	}
	public void setLAN_displyOtherVendorCode(Boolean lAN_displyOtherVendorCode) {
		LAN_displyOtherVendorCode = lAN_displyOtherVendorCode;
	}
	@Column
	public Boolean getMMG_displyOtherVendorCode() {
		return MMG_displyOtherVendorCode;
	}
	public void setMMG_displyOtherVendorCode(Boolean mMG_displyOtherVendorCode) {
		MMG_displyOtherVendorCode = mMG_displyOtherVendorCode;
	}
	@Column
	public Boolean getONG_displyOtherVendorCode() {
		return ONG_displyOtherVendorCode;
	}
	public void setONG_displyOtherVendorCode(Boolean oNG_displyOtherVendorCode) {
		ONG_displyOtherVendorCode = oNG_displyOtherVendorCode;
	}
	@Column
	public Boolean getPRV_displyOtherVendorCode() {
		return PRV_displyOtherVendorCode;
	}
	public void setPRV_displyOtherVendorCode(Boolean pRV_displyOtherVendorCode) {
		PRV_displyOtherVendorCode = pRV_displyOtherVendorCode;
	}
	@Column
	public Boolean getAIO_displyOtherVendorCode() {
		return AIO_displyOtherVendorCode;
	}
	public void setAIO_displyOtherVendorCode(Boolean aIO_displyOtherVendorCode) {
		AIO_displyOtherVendorCode = aIO_displyOtherVendorCode;
	}
	@Column
	public Boolean getEXP_displyOtherVendorCode() {
		return EXP_displyOtherVendorCode;
	}
	public void setEXP_displyOtherVendorCode(Boolean eXP_displyOtherVendorCode) {
		EXP_displyOtherVendorCode = eXP_displyOtherVendorCode;
	}
	@Column
	public Boolean getRPT_displyOtherVendorCode() {
		return RPT_displyOtherVendorCode;
	}
	public void setRPT_displyOtherVendorCode(Boolean rPT_displyOtherVendorCode) {
		RPT_displyOtherVendorCode = rPT_displyOtherVendorCode;
	}
	@Column
	public Boolean getSCH_displyOtherSchoolSelected() {
		return SCH_displyOtherSchoolSelected;
	}
	public void setSCH_displyOtherSchoolSelected(
			Boolean sCH_displyOtherSchoolSelected) {
		SCH_displyOtherSchoolSelected = sCH_displyOtherSchoolSelected;
	}
	@Column
	public Boolean getTAX_displyOtherVendorCode() {
		return TAX_displyOtherVendorCode;
	}
	public void setTAX_displyOtherVendorCode(Boolean tAX_displyOtherVendorCode) {
		TAX_displyOtherVendorCode = tAX_displyOtherVendorCode;
	}
	@Column
	public Boolean getTAC_displyOtherVendorCode() {
		return TAC_displyOtherVendorCode;
	}
	public void setTAC_displyOtherVendorCode(Boolean tAC_displyOtherVendorCode) {
		TAC_displyOtherVendorCode = tAC_displyOtherVendorCode;
	}
	@Column
	public Boolean getTEN_displyOtherVendorCode() {
		return TEN_displyOtherVendorCode;
	}
	public void setTEN_displyOtherVendorCode(Boolean tEN_displyOtherVendorCode) {
		TEN_displyOtherVendorCode = tEN_displyOtherVendorCode;
	}
	@Column
	public Boolean getVIS_displyOtherVendorCode() {
		return VIS_displyOtherVendorCode;
	}
	public void setVIS_displyOtherVendorCode(Boolean vIS_displyOtherVendorCode) {
		VIS_displyOtherVendorCode = vIS_displyOtherVendorCode;
	}
	@Column
	public String getMailServiceType() {
		return mailServiceType;
	}
	public void setMailServiceType(String mailServiceType) {
		this.mailServiceType = mailServiceType;
	}
	@Column
	public Date getVIS_visaStartDate() {
		return VIS_visaStartDate;
	}
	public void setVIS_visaStartDate(Date vIS_visaStartDate) {
		VIS_visaStartDate = vIS_visaStartDate;
	}
	@Column
	public String getWOP_vendorCode() {
		return WOP_vendorCode;
	}
	@Column
	public String getWOP_vendorName() {
		return WOP_vendorName;
	}
	@Column
	public String getWOP_vendorContact() {
		return WOP_vendorContact;
	}
	@Column
	public String getWOP_vendorEmail() {
		return WOP_vendorEmail;
	}
	@Column
	public Date getWOP_serviceStartDate() {
		return WOP_serviceStartDate;
	}
	@Column
	public Date getWOP_serviceEndDate() {
		return WOP_serviceEndDate;
	}
	@Column
	public String getWOP_workPermitHolderName() {
		return WOP_workPermitHolderName;
	}
	@Column
	public Date getWOP_providerNotificationDate() {
		return WOP_providerNotificationDate;
	}
	@Column
	public Date getWOP_permitStartDate() {
		return WOP_permitStartDate;
	}
	@Column
	public Date getWOP_permitExpiryDate() {
		return WOP_permitExpiryDate;
	}
	@Column
	public Date getWOP_reminder3MosPriorToExpiry() {
		return WOP_reminder3MosPriorToExpiry;
	}
	@Column
	public Date getWOP_questionnaireSentDate() {
		return WOP_questionnaireSentDate;
	}
	@Column
	public Boolean getWOP_authorizationLetter() {
		return WOP_authorizationLetter;
	}
	@Column
	public Boolean getWOP_copyResidence() {
		return WOP_copyResidence;
	}
	@Column
	public String getWOP_vendorCodeEXSO() {
		return WOP_vendorCodeEXSO;
	}
	@Column
	public String getWOP_comment() {
		return WOP_comment;
	}
	@Column
	public String getREP_comment() {
		return REP_comment;
	}
	@Column
	public Boolean getWOP_displyOtherVendorCode() {
		return WOP_displyOtherVendorCode;
	}
	@Column
	public Boolean getREP_displyOtherVendorCode() {
		return REP_displyOtherVendorCode;
	}
	public void setWOP_vendorCode(String wOP_vendorCode) {
		WOP_vendorCode = wOP_vendorCode;
	}
	public void setWOP_vendorName(String wOP_vendorName) {
		WOP_vendorName = wOP_vendorName;
	}
	public void setWOP_vendorContact(String wOP_vendorContact) {
		WOP_vendorContact = wOP_vendorContact;
	}
	public void setWOP_vendorEmail(String wOP_vendorEmail) {
		WOP_vendorEmail = wOP_vendorEmail;
	}
	public void setWOP_serviceStartDate(Date wOP_serviceStartDate) {
		WOP_serviceStartDate = wOP_serviceStartDate;
	}
	public void setWOP_serviceEndDate(Date wOP_serviceEndDate) {
		WOP_serviceEndDate = wOP_serviceEndDate;
	}
	public void setWOP_workPermitHolderName(String wOP_workPermitHolderName) {
		WOP_workPermitHolderName = wOP_workPermitHolderName;
	}
	public void setWOP_providerNotificationDate(Date wOP_providerNotificationDate) {
		WOP_providerNotificationDate = wOP_providerNotificationDate;
	}
	public void setWOP_permitStartDate(Date wOP_permitStartDate) {
		WOP_permitStartDate = wOP_permitStartDate;
	}
	public void setWOP_permitExpiryDate(Date wOP_permitExpiryDate) {
		WOP_permitExpiryDate = wOP_permitExpiryDate;
	}
	public void setWOP_reminder3MosPriorToExpiry(Date wOP_reminder3MosPriorToExpiry) {
		WOP_reminder3MosPriorToExpiry = wOP_reminder3MosPriorToExpiry;
	}
	public void setWOP_questionnaireSentDate(Date wOP_questionnaireSentDate) {
		WOP_questionnaireSentDate = wOP_questionnaireSentDate;
	}
	public void setWOP_authorizationLetter(Boolean wOP_authorizationLetter) {
		WOP_authorizationLetter = wOP_authorizationLetter;
	}
	public void setWOP_copyResidence(Boolean wOP_copyResidence) {
		WOP_copyResidence = wOP_copyResidence;
	}
	public void setWOP_vendorCodeEXSO(String wOP_vendorCodeEXSO) {
		WOP_vendorCodeEXSO = wOP_vendorCodeEXSO;
	}
	public void setWOP_comment(String wOP_comment) {
		WOP_comment = wOP_comment;
	}
	public void setREP_comment(String rEP_comment) {
		REP_comment = rEP_comment;
	}
	public void setWOP_displyOtherVendorCode(Boolean wOP_displyOtherVendorCode) {
		WOP_displyOtherVendorCode = wOP_displyOtherVendorCode;
	}
	public void setREP_displyOtherVendorCode(Boolean rEP_displyOtherVendorCode) {
		REP_displyOtherVendorCode = rEP_displyOtherVendorCode;
	}
	@Column
	public String getREP_vendorCode() {
		return REP_vendorCode;
	}
	@Column
	public String getREP_vendorName() {
		return REP_vendorName;
	}
	@Column
	public String getREP_vendorContact() {
		return REP_vendorContact;
	}
	@Column
	public String getREP_vendorEmail() {
		return REP_vendorEmail;
	}
	@Column
	public Date getREP_serviceStartDate() {
		return REP_serviceStartDate;
	}
	@Column
	public Date getREP_serviceEndDate() {
		return REP_serviceEndDate;
	}
	@Column
	public String getREP_workPermitHolderName() {
		return REP_workPermitHolderName;
	}
	@Column
	public Date getREP_providerNotificationDate() {
		return REP_providerNotificationDate;
	}
	@Column
	public Date getREP_permitStartDate() {
		return REP_permitStartDate;
	}
	@Column
	public Date getREP_reminder3MosPriorToExpiry() {
		return REP_reminder3MosPriorToExpiry;
	}
	@Column
	public Date getREP_questionnaireSentDate() {
		return REP_questionnaireSentDate;
	}
	@Column
	public Boolean getREP_authorizationLetter() {
		return REP_authorizationLetter;
	}
	@Column
	public Boolean getREP_copyResidence() {
		return REP_copyResidence;
	}
	@Column
	public String getREP_vendorCodeEXSO() {
		return REP_vendorCodeEXSO;
	}
	public void setREP_vendorCode(String rEP_vendorCode) {
		REP_vendorCode = rEP_vendorCode;
	}
	public void setREP_vendorName(String rEP_vendorName) {
		REP_vendorName = rEP_vendorName;
	}
	public void setREP_vendorContact(String rEP_vendorContact) {
		REP_vendorContact = rEP_vendorContact;
	}
	public void setREP_vendorEmail(String rEP_vendorEmail) {
		REP_vendorEmail = rEP_vendorEmail;
	}
	public void setREP_serviceStartDate(Date rEP_serviceStartDate) {
		REP_serviceStartDate = rEP_serviceStartDate;
	}
	public void setREP_serviceEndDate(Date rEP_serviceEndDate) {
		REP_serviceEndDate = rEP_serviceEndDate;
	}
	public void setREP_workPermitHolderName(String rEP_workPermitHolderName) {
		REP_workPermitHolderName = rEP_workPermitHolderName;
	}
	public void setREP_providerNotificationDate(Date rEP_providerNotificationDate) {
		REP_providerNotificationDate = rEP_providerNotificationDate;
	}
	public void setREP_permitStartDate(Date rEP_permitStartDate) {
		REP_permitStartDate = rEP_permitStartDate;
	}
	public void setREP_reminder3MosPriorToExpiry(Date rEP_reminder3MosPriorToExpiry) {
		REP_reminder3MosPriorToExpiry = rEP_reminder3MosPriorToExpiry;
	}
	public void setREP_questionnaireSentDate(Date rEP_questionnaireSentDate) {
		REP_questionnaireSentDate = rEP_questionnaireSentDate;
	}
	public void setREP_authorizationLetter(Boolean rEP_authorizationLetter) {
		REP_authorizationLetter = rEP_authorizationLetter;
	}
	public void setREP_copyResidence(Boolean rEP_copyResidence) {
		REP_copyResidence = rEP_copyResidence;
	}
	public void setREP_vendorCodeEXSO(String rEP_vendorCodeEXSO) {
		REP_vendorCodeEXSO = rEP_vendorCodeEXSO;
	}
	@Column
	public Date getREP_permitExpiryDate() {
		return REP_permitExpiryDate;
	}
	public void setREP_permitExpiryDate(Date rEP_permitExpiryDate) {
		REP_permitExpiryDate = rEP_permitExpiryDate;
	}
	@Column
	public String getRLS_vendorCode() {
		return RLS_vendorCode;
	}
	public void setRLS_vendorCode(String rLS_vendorCode) {
		RLS_vendorCode = rLS_vendorCode;
	}
	@Column
	public String getRLS_vendorName() {
		return RLS_vendorName;
	}
	public void setRLS_vendorName(String rLS_vendorName) {
		RLS_vendorName = rLS_vendorName;
	}
	@Column
	public Date getRLS_serviceStartDate() {
		return RLS_serviceStartDate;
	}
	public void setRLS_serviceStartDate(Date rLS_serviceStartDate) {
		RLS_serviceStartDate = rLS_serviceStartDate;
	}
	@Column
	public String getRLS_vendorCodeEXSO() {
		return RLS_vendorCodeEXSO;
	}
	public void setRLS_vendorCodeEXSO(String rLS_vendorCodeEXSO) {
		RLS_vendorCodeEXSO = rLS_vendorCodeEXSO;
	}
	@Column
	public String getRLS_vendorContact() {
		return RLS_vendorContact;
	}
	public void setRLS_vendorContact(String rLS_vendorContact) {
		RLS_vendorContact = rLS_vendorContact;
	}
	@Column
	public Date getRLS_serviceEndDate() {
		return RLS_serviceEndDate;
	}
	public void setRLS_serviceEndDate(Date rLS_serviceEndDate) {
		RLS_serviceEndDate = rLS_serviceEndDate;
	}
	@Column
	public String getRLS_vendorEmail() {
		return RLS_vendorEmail;
	}
	public void setRLS_vendorEmail(String rLS_vendorEmail) {
		RLS_vendorEmail = rLS_vendorEmail;
	}
	@Column
	public Boolean getRLS_displyOtherVendorCode() {
		return RLS_displyOtherVendorCode;
	}
	public void setRLS_displyOtherVendorCode(Boolean rLS_displyOtherVendorCode) {
		RLS_displyOtherVendorCode = rLS_displyOtherVendorCode;
	}
	@Column
	public String getCAT_vendorCode() {
		return CAT_vendorCode;
	}
	public void setCAT_vendorCode(String cAT_vendorCode) {
		CAT_vendorCode = cAT_vendorCode;
	}
	@Column
	public String getCAT_vendorName() {
		return CAT_vendorName;
	}
	public void setCAT_vendorName(String cAT_vendorName) {
		CAT_vendorName = cAT_vendorName;
	}
	@Column
	public Date getCAT_serviceStartDate() {
		return CAT_serviceStartDate;
	}
	public void setCAT_serviceStartDate(Date cAT_serviceStartDate) {
		CAT_serviceStartDate = cAT_serviceStartDate;
	}
	@Column
	public String getCAT_vendorCodeEXSO() {
		return CAT_vendorCodeEXSO;
	}
	public void setCAT_vendorCodeEXSO(String cAT_vendorCodeEXSO) {
		CAT_vendorCodeEXSO = cAT_vendorCodeEXSO;
	}
	@Column
	public String getCAT_vendorContact() {
		return CAT_vendorContact;
	}
	public void setCAT_vendorContact(String cAT_vendorContact) {
		CAT_vendorContact = cAT_vendorContact;
	}
	@Column
	public Date getCAT_serviceEndDate() {
		return CAT_serviceEndDate;
	}
	public void setCAT_serviceEndDate(Date cAT_serviceEndDate) {
		CAT_serviceEndDate = cAT_serviceEndDate;
	}
	@Column
	public String getCAT_vendorEmail() {
		return CAT_vendorEmail;
	}
	public void setCAT_vendorEmail(String cAT_vendorEmail) {
		CAT_vendorEmail = cAT_vendorEmail;
	}
	@Column
	public Boolean getCAT_displyOtherVendorCode() {
		return CAT_displyOtherVendorCode;
	}
	public void setCAT_displyOtherVendorCode(Boolean cAT_displyOtherVendorCode) {
		CAT_displyOtherVendorCode = cAT_displyOtherVendorCode;
	}
	@Column
	public String getCLS_vendorCode() {
		return CLS_vendorCode;
	}
	public void setCLS_vendorCode(String cLS_vendorCode) {
		CLS_vendorCode = cLS_vendorCode;
	}
	@Column
	public String getCLS_vendorName() {
		return CLS_vendorName;
	}
	public void setCLS_vendorName(String cLS_vendorName) {
		CLS_vendorName = cLS_vendorName;
	}
	@Column
	public Date getCLS_serviceStartDate() {
		return CLS_serviceStartDate;
	}
	public void setCLS_serviceStartDate(Date cLS_serviceStartDate) {
		CLS_serviceStartDate = cLS_serviceStartDate;
	}
	@Column
	public String getCLS_vendorCodeEXSO() {
		return CLS_vendorCodeEXSO;
	}
	public void setCLS_vendorCodeEXSO(String cLS_vendorCodeEXSO) {
		CLS_vendorCodeEXSO = cLS_vendorCodeEXSO;
	}
	@Column
	public String getCLS_vendorContact() {
		return CLS_vendorContact;
	}
	public void setCLS_vendorContact(String cLS_vendorContact) {
		CLS_vendorContact = cLS_vendorContact;
	}
	@Column
	public Date getCLS_serviceEndDate() {
		return CLS_serviceEndDate;
	}
	public void setCLS_serviceEndDate(Date cLS_serviceEndDate) {
		CLS_serviceEndDate = cLS_serviceEndDate;
	}
	@Column
	public String getCLS_vendorEmail() {
		return CLS_vendorEmail;
	}
	public void setCLS_vendorEmail(String cLS_vendorEmail) {
		CLS_vendorEmail = cLS_vendorEmail;
	}
	@Column
	public Boolean getCLS_displyOtherVendorCode() {
		return CLS_displyOtherVendorCode;
	}
	public void setCLS_displyOtherVendorCode(Boolean cLS_displyOtherVendorCode) {
		CLS_displyOtherVendorCode = cLS_displyOtherVendorCode;
	}
	@Column
	public String getCHS_vendorCode() {
		return CHS_vendorCode;
	}
	public void setCHS_vendorCode(String cHS_vendorCode) {
		CHS_vendorCode = cHS_vendorCode;
	}
	@Column
	public String getCHS_vendorName() {
		return CHS_vendorName;
	}
	public void setCHS_vendorName(String cHS_vendorName) {
		CHS_vendorName = cHS_vendorName;
	}
	@Column
	public Date getCHS_serviceStartDate() {
		return CHS_serviceStartDate;
	}
	public void setCHS_serviceStartDate(Date cHS_serviceStartDate) {
		CHS_serviceStartDate = cHS_serviceStartDate;
	}
	@Column
	public String getCHS_vendorCodeEXSO() {
		return CHS_vendorCodeEXSO;
	}
	public void setCHS_vendorCodeEXSO(String cHS_vendorCodeEXSO) {
		CHS_vendorCodeEXSO = cHS_vendorCodeEXSO;
	}
	@Column
	public String getCHS_vendorContact() {
		return CHS_vendorContact;
	}
	public void setCHS_vendorContact(String cHS_vendorContact) {
		CHS_vendorContact = cHS_vendorContact;
	}
	@Column
	public Date getCHS_serviceEndDate() {
		return CHS_serviceEndDate;
	}
	public void setCHS_serviceEndDate(Date cHS_serviceEndDate) {
		CHS_serviceEndDate = cHS_serviceEndDate;
	}
	@Column
	public String getCHS_vendorEmail() {
		return CHS_vendorEmail;
	}
	public void setCHS_vendorEmail(String cHS_vendorEmail) {
		CHS_vendorEmail = cHS_vendorEmail;
	}
	@Column
	public Boolean getCHS_displyOtherVendorCode() {
		return CHS_displyOtherVendorCode;
	}
	public void setCHS_displyOtherVendorCode(Boolean cHS_displyOtherVendorCode) {
		CHS_displyOtherVendorCode = cHS_displyOtherVendorCode;
	}
	@Column
	public String getDPS_vendorCode() {
		return DPS_vendorCode;
	}
	public void setDPS_vendorCode(String dPS_vendorCode) {
		DPS_vendorCode = dPS_vendorCode;
	}
	@Column
	public String getDPS_vendorName() {
		return DPS_vendorName;
	}
	public void setDPS_vendorName(String dPS_vendorName) {
		DPS_vendorName = dPS_vendorName;
	}
	@Column
	public Date getDPS_serviceStartDate() {
		return DPS_serviceStartDate;
	}
	public void setDPS_serviceStartDate(Date dPS_serviceStartDate) {
		DPS_serviceStartDate = dPS_serviceStartDate;
	}
	@Column
	public String getDPS_vendorCodeEXSO() {
		return DPS_vendorCodeEXSO;
	}
	public void setDPS_vendorCodeEXSO(String dPS_vendorCodeEXSO) {
		DPS_vendorCodeEXSO = dPS_vendorCodeEXSO;
	}
	@Column
	public String getDPS_vendorContact() {
		return DPS_vendorContact;
	}
	public void setDPS_vendorContact(String dPS_vendorContact) {
		DPS_vendorContact = dPS_vendorContact;
	}
	@Column
	public Date getDPS_serviceEndDate() {
		return DPS_serviceEndDate;
	}
	public void setDPS_serviceEndDate(Date dPS_serviceEndDate) {
		DPS_serviceEndDate = dPS_serviceEndDate;
	}
	@Column
	public String getDPS_vendorEmail() {
		return DPS_vendorEmail;
	}
	public void setDPS_vendorEmail(String dPS_vendorEmail) {
		DPS_vendorEmail = dPS_vendorEmail;
	}
	@Column
	public Boolean getDPS_displyOtherVendorCode() {
		return DPS_displyOtherVendorCode;
	}
	public void setDPS_displyOtherVendorCode(Boolean dPS_displyOtherVendorCode) {
		DPS_displyOtherVendorCode = dPS_displyOtherVendorCode;
	}
	@Column
	public String getHSM_vendorCode() {
		return HSM_vendorCode;
	}
	public void setHSM_vendorCode(String hSM_vendorCode) {
		HSM_vendorCode = hSM_vendorCode;
	}
	@Column
	public String getHSM_vendorName() {
		return HSM_vendorName;
	}
	public void setHSM_vendorName(String hSM_vendorName) {
		HSM_vendorName = hSM_vendorName;
	}
	@Column
	public Date getHSM_serviceStartDate() {
		return HSM_serviceStartDate;
	}
	public void setHSM_serviceStartDate(Date hSM_serviceStartDate) {
		HSM_serviceStartDate = hSM_serviceStartDate;
	}
	@Column
	public String getHSM_vendorCodeEXSO() {
		return HSM_vendorCodeEXSO;
	}
	public void setHSM_vendorCodeEXSO(String hSM_vendorCodeEXSO) {
		HSM_vendorCodeEXSO = hSM_vendorCodeEXSO;
	}
	@Column
	public String getHSM_vendorContact() {
		return HSM_vendorContact;
	}
	public void setHSM_vendorContact(String hSM_vendorContact) {
		HSM_vendorContact = hSM_vendorContact;
	}
	@Column
	public Date getHSM_serviceEndDate() {
		return HSM_serviceEndDate;
	}
	public void setHSM_serviceEndDate(Date hSM_serviceEndDate) {
		HSM_serviceEndDate = hSM_serviceEndDate;
	}
	@Column
	public String getHSM_vendorEmail() {
		return HSM_vendorEmail;
	}
	public void setHSM_vendorEmail(String hSM_vendorEmail) {
		HSM_vendorEmail = hSM_vendorEmail;
	}
	@Column
	public Boolean getHSM_displyOtherVendorCode() {
		return HSM_displyOtherVendorCode;
	}
	public void setHSM_displyOtherVendorCode(Boolean hSM_displyOtherVendorCode) {
		HSM_displyOtherVendorCode = hSM_displyOtherVendorCode;
	}
	@Column
	public String getPDT_vendorCode() {
		return PDT_vendorCode;
	}
	public void setPDT_vendorCode(String pDT_vendorCode) {
		PDT_vendorCode = pDT_vendorCode;
	}
	@Column
	public String getPDT_vendorName() {
		return PDT_vendorName;
	}
	public void setPDT_vendorName(String pDT_vendorName) {
		PDT_vendorName = pDT_vendorName;
	}
	@Column
	public Date getPDT_serviceStartDate() {
		return PDT_serviceStartDate;
	}
	public void setPDT_serviceStartDate(Date pDT_serviceStartDate) {
		PDT_serviceStartDate = pDT_serviceStartDate;
	}
	@Column
	public String getPDT_vendorCodeEXSO() {
		return PDT_vendorCodeEXSO;
	}
	public void setPDT_vendorCodeEXSO(String pDT_vendorCodeEXSO) {
		PDT_vendorCodeEXSO = pDT_vendorCodeEXSO;
	}
	@Column
	public String getPDT_vendorContact() {
		return PDT_vendorContact;
	}
	public void setPDT_vendorContact(String pDT_vendorContact) {
		PDT_vendorContact = pDT_vendorContact;
	}
	@Column
	public Date getPDT_serviceEndDate() {
		return PDT_serviceEndDate;
	}
	public void setPDT_serviceEndDate(Date pDT_serviceEndDate) {
		PDT_serviceEndDate = pDT_serviceEndDate;
	}
	@Column
	public String getPDT_vendorEmail() {
		return PDT_vendorEmail;
	}
	public void setPDT_vendorEmail(String pDT_vendorEmail) {
		PDT_vendorEmail = pDT_vendorEmail;
	}
	@Column
	public Boolean getPDT_displyOtherVendorCode() {
		return PDT_displyOtherVendorCode;
	}
	public void setPDT_displyOtherVendorCode(Boolean pDT_displyOtherVendorCode) {
		PDT_displyOtherVendorCode = pDT_displyOtherVendorCode;
	}
	@Column
	public String getRCP_vendorCode() {
		return RCP_vendorCode;
	}
	public void setRCP_vendorCode(String rCP_vendorCode) {
		RCP_vendorCode = rCP_vendorCode;
	}
	@Column
	public String getRCP_vendorName() {
		return RCP_vendorName;
	}
	public void setRCP_vendorName(String rCP_vendorName) {
		RCP_vendorName = rCP_vendorName;
	}
	@Column
	public Date getRCP_serviceStartDate() {
		return RCP_serviceStartDate;
	}
	public void setRCP_serviceStartDate(Date rCP_serviceStartDate) {
		RCP_serviceStartDate = rCP_serviceStartDate;
	}
	@Column
	public String getRCP_vendorCodeEXSO() {
		return RCP_vendorCodeEXSO;
	}
	public void setRCP_vendorCodeEXSO(String rCP_vendorCodeEXSO) {
		RCP_vendorCodeEXSO = rCP_vendorCodeEXSO;
	}
	@Column
	public String getRCP_vendorContact() {
		return RCP_vendorContact;
	}
	public void setRCP_vendorContact(String rCP_vendorContact) {
		RCP_vendorContact = rCP_vendorContact;
	}
	@Column
	public Date getRCP_serviceEndDate() {
		return RCP_serviceEndDate;
	}
	public void setRCP_serviceEndDate(Date rCP_serviceEndDate) {
		RCP_serviceEndDate = rCP_serviceEndDate;
	}
	@Column
	public String getRCP_vendorEmail() {
		return RCP_vendorEmail;
	}
	public void setRCP_vendorEmail(String rCP_vendorEmail) {
		RCP_vendorEmail = rCP_vendorEmail;
	}
	@Column
	public Boolean getRCP_displyOtherVendorCode() {
		return RCP_displyOtherVendorCode;
	}
	public void setRCP_displyOtherVendorCode(Boolean rCP_displyOtherVendorCode) {
		RCP_displyOtherVendorCode = rCP_displyOtherVendorCode;
	}
	@Column
	public String getSPA_vendorCode() {
		return SPA_vendorCode;
	}
	public void setSPA_vendorCode(String sPA_vendorCode) {
		SPA_vendorCode = sPA_vendorCode;
	}
	@Column
	public String getSPA_vendorName() {
		return SPA_vendorName;
	}
	public void setSPA_vendorName(String sPA_vendorName) {
		SPA_vendorName = sPA_vendorName;
	}
	@Column
	public Date getSPA_serviceStartDate() {
		return SPA_serviceStartDate;
	}
	public void setSPA_serviceStartDate(Date sPA_serviceStartDate) {
		SPA_serviceStartDate = sPA_serviceStartDate;
	}
	@Column
	public String getSPA_vendorCodeEXSO() {
		return SPA_vendorCodeEXSO;
	}
	public void setSPA_vendorCodeEXSO(String sPA_vendorCodeEXSO) {
		SPA_vendorCodeEXSO = sPA_vendorCodeEXSO;
	}
	@Column
	public String getSPA_vendorContact() {
		return SPA_vendorContact;
	}
	public void setSPA_vendorContact(String sPA_vendorContact) {
		SPA_vendorContact = sPA_vendorContact;
	}
	@Column
	public Date getSPA_serviceEndDate() {
		return SPA_serviceEndDate;
	}
	public void setSPA_serviceEndDate(Date sPA_serviceEndDate) {
		SPA_serviceEndDate = sPA_serviceEndDate;
	}
	@Column
	public String getSPA_vendorEmail() {
		return SPA_vendorEmail;
	}
	public void setSPA_vendorEmail(String sPA_vendorEmail) {
		SPA_vendorEmail = sPA_vendorEmail;
	}
	@Column
	public Boolean getSPA_displyOtherVendorCode() {
		return SPA_displyOtherVendorCode;
	}
	public void setSPA_displyOtherVendorCode(Boolean sPA_displyOtherVendorCode) {
		SPA_displyOtherVendorCode = sPA_displyOtherVendorCode;
	}
	@Column
	public String getTCS_vendorCode() {
		return TCS_vendorCode;
	}
	public void setTCS_vendorCode(String tCS_vendorCode) {
		TCS_vendorCode = tCS_vendorCode;
	}
	@Column
	public String getTCS_vendorName() {
		return TCS_vendorName;
	}
	public void setTCS_vendorName(String tCS_vendorName) {
		TCS_vendorName = tCS_vendorName;
	}
	@Column
	public Date getTCS_serviceStartDate() {
		return TCS_serviceStartDate;
	}
	public void setTCS_serviceStartDate(Date tCS_serviceStartDate) {
		TCS_serviceStartDate = tCS_serviceStartDate;
	}
	@Column
	public String getTCS_vendorCodeEXSO() {
		return TCS_vendorCodeEXSO;
	}
	public void setTCS_vendorCodeEXSO(String tCS_vendorCodeEXSO) {
		TCS_vendorCodeEXSO = tCS_vendorCodeEXSO;
	}
	@Column
	public String getTCS_vendorContact() {
		return TCS_vendorContact;
	}
	public void setTCS_vendorContact(String tCS_vendorContact) {
		TCS_vendorContact = tCS_vendorContact;
	}
	@Column
	public Date getTCS_serviceEndDate() {
		return TCS_serviceEndDate;
	}
	public void setTCS_serviceEndDate(Date tCS_serviceEndDate) {
		TCS_serviceEndDate = tCS_serviceEndDate;
	}
	@Column
	public String getTCS_vendorEmail() {
		return TCS_vendorEmail;
	}
	public void setTCS_vendorEmail(String tCS_vendorEmail) {
		TCS_vendorEmail = tCS_vendorEmail;
	}
	@Column
	public Boolean getTCS_displyOtherVendorCode() {
		return TCS_displyOtherVendorCode;
	}
	public void setTCS_displyOtherVendorCode(Boolean tCS_displyOtherVendorCode) {
		TCS_displyOtherVendorCode = tCS_displyOtherVendorCode;
	}
	@Column
	public String getRNT_monthlyRentalAllowanceCurrency() {
		return RNT_monthlyRentalAllowanceCurrency;
	}
	public void setRNT_monthlyRentalAllowanceCurrency(
			String rNT_monthlyRentalAllowanceCurrency) {
		RNT_monthlyRentalAllowanceCurrency = rNT_monthlyRentalAllowanceCurrency;
	}
	@Column
	public String getRNT_securityDepositCurrency() {
		return RNT_securityDepositCurrency;
	}
	public void setRNT_securityDepositCurrency(String rNT_securityDepositCurrency) {
		RNT_securityDepositCurrency = rNT_securityDepositCurrency;
	}
	@Column
	public Date getRNT_checkInDate() {
		return RNT_checkInDate;
	}
	public void setRNT_checkInDate(Date rNT_checkInDate) {
		RNT_checkInDate = rNT_checkInDate;
	}
	@Column
	public String getMTS_vendorCode() {
		return MTS_vendorCode;
	}
	public void setMTS_vendorCode(String mTS_vendorCode) {
		MTS_vendorCode = mTS_vendorCode;
	}
	@Column
	public String getMTS_vendorName() {
		return MTS_vendorName;
	}
	public void setMTS_vendorName(String mTS_vendorName) {
		MTS_vendorName = mTS_vendorName;
	}
	@Column
	public Date getMTS_serviceStartDate() {
		return MTS_serviceStartDate;
	}
	public void setMTS_serviceStartDate(Date mTS_serviceStartDate) {
		MTS_serviceStartDate = mTS_serviceStartDate;
	}
	@Column
	public String getMTS_vendorCodeEXSO() {
		return MTS_vendorCodeEXSO;
	}
	public void setMTS_vendorCodeEXSO(String mTS_vendorCodeEXSO) {
		MTS_vendorCodeEXSO = mTS_vendorCodeEXSO;
	}
	@Column
	public String getMTS_vendorContact() {
		return MTS_vendorContact;
	}
	public void setMTS_vendorContact(String mTS_vendorContact) {
		MTS_vendorContact = mTS_vendorContact;
	}
	@Column
	public Date getMTS_serviceEndDate() {
		return MTS_serviceEndDate;
	}
	public void setMTS_serviceEndDate(Date mTS_serviceEndDate) {
		MTS_serviceEndDate = mTS_serviceEndDate;
	}
	@Column
	public String getMTS_vendorEmail() {
		return MTS_vendorEmail;
	}
	public void setMTS_vendorEmail(String mTS_vendorEmail) {
		MTS_vendorEmail = mTS_vendorEmail;
	}
	@Column
	public String getMTS_paymentResponsibility() {
		return MTS_paymentResponsibility;
	}
	public void setMTS_paymentResponsibility(String mTS_paymentResponsibility) {
		MTS_paymentResponsibility = mTS_paymentResponsibility;
	}
	@Column
	public Boolean getMTS_displyOtherVendorCode() {
		return MTS_displyOtherVendorCode;
	}
	public void setMTS_displyOtherVendorCode(Boolean mTS_displyOtherVendorCode) {
		MTS_displyOtherVendorCode = mTS_displyOtherVendorCode;
	}
	@Column
	public String getRLS_paymentResponsibility() {
		return RLS_paymentResponsibility;
	}
	public void setRLS_paymentResponsibility(String rLS_paymentResponsibility) {
		RLS_paymentResponsibility = rLS_paymentResponsibility;
	}
	@Column
	public String getCAT_paymentResponsibility() {
		return CAT_paymentResponsibility;
	}
	public void setCAT_paymentResponsibility(String cAT_paymentResponsibility) {
		CAT_paymentResponsibility = cAT_paymentResponsibility;
	}
	@Column
	public String getCLS_paymentResponsibility() {
		return CLS_paymentResponsibility;
	}
	public void setCLS_paymentResponsibility(String cLS_paymentResponsibility) {
		CLS_paymentResponsibility = cLS_paymentResponsibility;
	}
	@Column
	public String getCHS_paymentResponsibility() {
		return CHS_paymentResponsibility;
	}
	public void setCHS_paymentResponsibility(String cHS_paymentResponsibility) {
		CHS_paymentResponsibility = cHS_paymentResponsibility;
	}
	@Column
	public String getDPS_paymentResponsibility() {
		return DPS_paymentResponsibility;
	}
	public void setDPS_paymentResponsibility(String dPS_paymentResponsibility) {
		DPS_paymentResponsibility = dPS_paymentResponsibility;
	}
	@Column
	public String getHSM_paymentResponsibility() {
		return HSM_paymentResponsibility;
	}
	public void setHSM_paymentResponsibility(String hSM_paymentResponsibility) {
		HSM_paymentResponsibility = hSM_paymentResponsibility;
	}
	@Column
	public String getPDT_paymentResponsibility() {
		return PDT_paymentResponsibility;
	}
	public void setPDT_paymentResponsibility(String pDT_paymentResponsibility) {
		PDT_paymentResponsibility = pDT_paymentResponsibility;
	}
	@Column
	public String getRCP_paymentResponsibility() {
		return RCP_paymentResponsibility;
	}
	public void setRCP_paymentResponsibility(String rCP_paymentResponsibility) {
		RCP_paymentResponsibility = rCP_paymentResponsibility;
	}
	@Column
	public String getSPA_paymentResponsibility() {
		return SPA_paymentResponsibility;
	}
	public void setSPA_paymentResponsibility(String sPA_paymentResponsibility) {
		SPA_paymentResponsibility = sPA_paymentResponsibility;
	}
	@Column
	public String getTCS_paymentResponsibility() {
		return TCS_paymentResponsibility;
	}
	public void setTCS_paymentResponsibility(String tCS_paymentResponsibility) {
		TCS_paymentResponsibility = tCS_paymentResponsibility;
	}
	@Column
	public String getDPS_serviceType() {
		return DPS_serviceType;
	}
	public void setDPS_serviceType(String dPS_serviceType) {
		DPS_serviceType = dPS_serviceType;
	}
	@Column
	public Date getDPS_completionDate() {
		return DPS_completionDate;
	}
	public void setDPS_completionDate(Date dPS_completionDate) {
		DPS_completionDate = dPS_completionDate;
	}
	@Column
	public String getDPS_comment() {
		return DPS_comment;
	}
	public void setDPS_comment(String dPS_comment) {
		DPS_comment = dPS_comment;
	}
	@Column
	public String getHSM_brokerCode() {
		return HSM_brokerCode;
	}
	public void setHSM_brokerCode(String hSM_brokerCode) {
		HSM_brokerCode = hSM_brokerCode;
	}
	@Column
	public String getHSM_brokerName() {
		return HSM_brokerName;
	}
	public void setHSM_brokerName(String hSM_brokerName) {
		HSM_brokerName = hSM_brokerName;
	}
	@Column
	public String getHSM_brokerContact() {
		return HSM_brokerContact;
	}
	public void setHSM_brokerContact(String hSM_brokerContact) {
		HSM_brokerContact = hSM_brokerContact;
	}
	@Column
	public String getHSM_brokerEmail() {
		return HSM_brokerEmail;
	}
	public void setHSM_brokerEmail(String hSM_brokerEmail) {
		HSM_brokerEmail = hSM_brokerEmail;
	}
	@Column
	public String getHSM_status() {
		return HSM_status;
	}
	public void setHSM_status(String hSM_status) {
		HSM_status = hSM_status;
	}
	@Column
	public String getHSM_comment() {
		return HSM_comment;
	}
	public void setHSM_comment(String hSM_comment) {
		HSM_comment = hSM_comment;
	}
	@Column
	public Date getHSM_marketingPlanNBMAReceived() {
		return HSM_marketingPlanNBMAReceived;
	}
	public void setHSM_marketingPlanNBMAReceived(Date hSM_marketingPlanNBMAReceived) {
		HSM_marketingPlanNBMAReceived = hSM_marketingPlanNBMAReceived;
	}
	@Column
	public String getPDT_serviceType() {
		return PDT_serviceType;
	}
	public void setPDT_serviceType(String pDT_serviceType) {
		PDT_serviceType = pDT_serviceType;
	}
	@Column
	public BigDecimal getPDT_budget() {
		return PDT_budget;
	}
	public void setPDT_budget(BigDecimal pDT_budget) {
		PDT_budget = pDT_budget;
	}
	@Column
	public BigDecimal getRCP_estimateCost() {
		return RCP_estimateCost;
	}
	public void setRCP_estimateCost(BigDecimal rCP_estimateCost) {
		RCP_estimateCost = rCP_estimateCost;
	}
	@Column
	public BigDecimal getRCP_actualCost() {
		return RCP_actualCost;
	}
	public void setRCP_actualCost(BigDecimal rCP_actualCost) {
		RCP_actualCost = rCP_actualCost;
	}
	@Column
	public String getMTS_lender() {
		return MTS_lender;
	}
	public void setMTS_lender(String mTS_lender) {
		MTS_lender = mTS_lender;
	}
	@Column
	public Date getMTS_initialContact() {
		return MTS_initialContact;
	}
	public void setMTS_initialContact(Date mTS_initialContact) {
		MTS_initialContact = mTS_initialContact;
	}
	@Column
	public String getMTS_status() {
		return MTS_status;
	}
	public void setMTS_status(String mTS_status) {
		MTS_status = mTS_status;
	}
	@Column
	public BigDecimal getMTS_mortgageAmount() {
		return MTS_mortgageAmount;
	}
	public void setMTS_mortgageAmount(BigDecimal mTS_mortgageAmount) {
		MTS_mortgageAmount = mTS_mortgageAmount;
	}
	@Column
	public BigDecimal getMTS_mortgageRate() {
		return MTS_mortgageRate;
	}
	public void setMTS_mortgageRate(BigDecimal mTS_mortgageRate) {
		MTS_mortgageRate = mTS_mortgageRate;
	}
	@Column
	public String getMTS_mortgageTerm() {
		return MTS_mortgageTerm;
	}
	public void setMTS_mortgageTerm(String mTS_mortgageTerm) {
		MTS_mortgageTerm = mTS_mortgageTerm;
	}
	@Column
	public String getCOL_estimatedTaxAllowance() {
		return COL_estimatedTaxAllowance;
	}
	public void setCOL_estimatedTaxAllowance(String cOL_estimatedTaxAllowance) {
		COL_estimatedTaxAllowance = cOL_estimatedTaxAllowance;
	}
	@Column
	public String getCOL_estimatedHousingAllowance() {
		return COL_estimatedHousingAllowance;
	}
	public void setCOL_estimatedHousingAllowance(
			String cOL_estimatedHousingAllowance) {
		COL_estimatedHousingAllowance = cOL_estimatedHousingAllowance;
	}
	@Column
	public String getCOL_estimatedTransportationAllowance() {
		return COL_estimatedTransportationAllowance;
	}
	public void setCOL_estimatedTransportationAllowance(
			String cOL_estimatedTransportationAllowance) {
		COL_estimatedTransportationAllowance = cOL_estimatedTransportationAllowance;
	}
	@Column
	public String getRNT_leaseSignee() {
		return RNT_leaseSignee;
	}
	public void setRNT_leaseSignee(String rNT_leaseSignee) {
		RNT_leaseSignee = rNT_leaseSignee;
	}
	@Column
	public Date getSET_governmentID() {
		return SET_governmentID;
	}

	public void setSET_governmentID(Date sET_governmentID) {
		SET_governmentID = sET_governmentID;
	}
	@Column
	public Date getSET_heathCare() {
		return SET_heathCare;
	}

	public void setSET_heathCare(Date sET_heathCare) {
		SET_heathCare = sET_heathCare;
	}
	@Column
	public Date getSET_utilities() {
		return SET_utilities;
	}

	public void setSET_utilities(Date sET_utilities) {
		SET_utilities = sET_utilities;
	}
	@Column
	public Date getSET_automobileRegistration() {
		return SET_automobileRegistration;
	}

	public void setSET_automobileRegistration(Date sET_automobileRegistration) {
		SET_automobileRegistration = sET_automobileRegistration;
	}
	@Column
	public Date getSET_dayofServiceAuthorized() {
		return SET_dayofServiceAuthorized;
	}

	public void setSET_dayofServiceAuthorized(Date sET_dayofServiceAuthorized) {
		SET_dayofServiceAuthorized = sET_dayofServiceAuthorized;
	}
	@Column
	public Date getSET_initialDateofServiceRequested() {
		return SET_initialDateofServiceRequested;
	}

	public void setSET_initialDateofServiceRequested(
			Date sET_initialDateofServiceRequested) {
		SET_initialDateofServiceRequested = sET_initialDateofServiceRequested;
	}
	@Column
	public Date getSET_providerNotificationSent() {
		return SET_providerNotificationSent;
	}

	public void setSET_providerNotificationSent(Date sET_providerNotificationSent) {
		SET_providerNotificationSent = sET_providerNotificationSent;
	}
	@Column
	public Date getSET_providerConfirmationReceived() {
		return SET_providerConfirmationReceived;
	}

	public void setSET_providerConfirmationReceived(
			Date sET_providerConfirmationReceived) {
		SET_providerConfirmationReceived = sET_providerConfirmationReceived;
	}
	@Column
	public String getTAX_allowance() {
		return TAX_allowance;
	}

	public void setTAX_allowance(String tAX_allowance) {
		TAX_allowance = tAX_allowance;
	}
	@Column
	public String getVIS_immigrationStatus() {
		return VIS_immigrationStatus;
	}

	public void setVIS_immigrationStatus(String vIS_immigrationStatus) {
		VIS_immigrationStatus = vIS_immigrationStatus;
	}
	@Column
	public Date getVIS_arrivalDate() {
		return VIS_arrivalDate;
	}

	public void setVIS_arrivalDate(Date vIS_arrivalDate) {
		VIS_arrivalDate = vIS_arrivalDate;
	}
	@Column
	public String getVIS_portofEntry() {
		return VIS_portofEntry;
	}

	public void setVIS_portofEntry(String vIS_portofEntry) {
		VIS_portofEntry = vIS_portofEntry;
	}
	@Column
	public String getTEN_landlordName() {
		return TEN_landlordName;
	}
	public void setTEN_landlordName(String tEN_landlordName) {
		TEN_landlordName = tEN_landlordName;
	}
	@Column
	public String getTEN_landlordContactNumber() {
		return TEN_landlordContactNumber;
	}
	public void setTEN_landlordContactNumber(String tEN_landlordContactNumber) {
		TEN_landlordContactNumber = tEN_landlordContactNumber;
	}
	@Column
	public String getTEN_landlordEmailAddress() {
		return TEN_landlordEmailAddress;
	}
	public void setTEN_landlordEmailAddress(String tEN_landlordEmailAddress) {
		TEN_landlordEmailAddress = tEN_landlordEmailAddress;
	}
	@Column
	public String getTEN_propertyAddress() {
		return TEN_propertyAddress;
	}
	public void setTEN_propertyAddress(String tEN_propertyAddress) {
		TEN_propertyAddress = tEN_propertyAddress;
	}
	@Column
	public String getTEN_accountHolder() {
		return TEN_accountHolder;
	}
	public void setTEN_accountHolder(String tEN_accountHolder) {
		TEN_accountHolder = tEN_accountHolder;
	}
	@Column
	public String getTEN_bankName() {
		return TEN_bankName;
	}
	public void setTEN_bankName(String tEN_bankName) {
		TEN_bankName = tEN_bankName;
	}
	@Column
	public String getTEN_bankAddress() {
		return TEN_bankAddress;
	}
	public void setTEN_bankAddress(String tEN_bankAddress) {
		TEN_bankAddress = tEN_bankAddress;
	}
	@Column
	public String getTEN_bankAccountNumber() {
		return TEN_bankAccountNumber;
	}
	public void setTEN_bankAccountNumber(String tEN_bankAccountNumber) {
		TEN_bankAccountNumber = tEN_bankAccountNumber;
	}
	@Column
	public String getTEN_bankIbanNumber() {
		return TEN_bankIbanNumber;
	}
	public void setTEN_bankIbanNumber(String tEN_bankIbanNumber) {
		TEN_bankIbanNumber = tEN_bankIbanNumber;
	}
	@Column
	public String getTEN_abaNumber() {
		return TEN_abaNumber;
	}
	public void setTEN_abaNumber(String tEN_abaNumber) {
		TEN_abaNumber = tEN_abaNumber;
	}
	@Column
	public String getTEN_accountType() {
		return TEN_accountType;
	}
	public void setTEN_accountType(String tEN_accountType) {
		TEN_accountType = tEN_accountType;
	}
	@Column
	public Date getTEN_leaseStartDate() {
		return TEN_leaseStartDate;
	}
	public void setTEN_leaseStartDate(Date tEN_leaseStartDate) {
		TEN_leaseStartDate = tEN_leaseStartDate;
	}
	@Column
	public Date getTEN_leaseExpireDate() {
		return TEN_leaseExpireDate;
	}
	public void setTEN_leaseExpireDate(Date tEN_leaseExpireDate) {
		TEN_leaseExpireDate = tEN_leaseExpireDate;
	}
	@Column
	public Date getTEN_expiryReminderPriorToExpiry() {
		return TEN_expiryReminderPriorToExpiry;
	}
	public void setTEN_expiryReminderPriorToExpiry(
			Date tEN_expiryReminderPriorToExpiry) {
		TEN_expiryReminderPriorToExpiry = tEN_expiryReminderPriorToExpiry;
	}
	@Column
	public String getTEN_monthlyRentalAllowance() {
		return TEN_monthlyRentalAllowance;
	}
	public void setTEN_monthlyRentalAllowance(String tEN_monthlyRentalAllowance) {
		TEN_monthlyRentalAllowance = tEN_monthlyRentalAllowance;
	}
	@Column
	public String getTEN_securityDeposit() {
		return TEN_securityDeposit;
	}
	public void setTEN_securityDeposit(String tEN_securityDeposit) {
		TEN_securityDeposit = tEN_securityDeposit;
	}
	@Column
	public String getTEN_leaseCurrency() {
		return TEN_leaseCurrency;
	}
	public void setTEN_leaseCurrency(String tEN_leaseCurrency) {
		TEN_leaseCurrency = tEN_leaseCurrency;
	}
	@Column
	public String getTEN_leaseSignee() {
		return TEN_leaseSignee;
	}
	public void setTEN_leaseSignee(String tEN_leaseSignee) {
		TEN_leaseSignee = tEN_leaseSignee;
	}
	@Column
	public String getHOM_agentCode() {
		return HOM_agentCode;
	}
	public void setHOM_agentCode(String hOM_agentCode) {
		HOM_agentCode = hOM_agentCode;
	}
	@Column
	public String getHOM_agentName() {
		return HOM_agentName;
	}
	public void setHOM_agentName(String hOM_agentName) {
		HOM_agentName = hOM_agentName;
	}
	@Column
	public Date getHOM_initialContactDate() {
		return HOM_initialContactDate;
	}
	public void setHOM_initialContactDate(Date hOM_initialContactDate) {
		HOM_initialContactDate = hOM_initialContactDate;
	}
	@Column
	public String getHOM_agentPhone() {
		return HOM_agentPhone;
	}
	public void setHOM_agentPhone(String hOM_agentPhone) {
		HOM_agentPhone = hOM_agentPhone;
	}
	@Column
	public Date getHOM_houseHuntingTripArrival() {
		return HOM_houseHuntingTripArrival;
	}
	public void setHOM_houseHuntingTripArrival(Date hOM_houseHuntingTripArrival) {
		HOM_houseHuntingTripArrival = hOM_houseHuntingTripArrival;
	}
	@Column
	public String getHOM_agentEmail() {
		return HOM_agentEmail;
	}
	public void setHOM_agentEmail(String hOM_agentEmail) {
		HOM_agentEmail = hOM_agentEmail;
	}
	@Column
	public Date getHOM_houseHuntingTripDeparture() {
		return HOM_houseHuntingTripDeparture;
	}
	public void setHOM_houseHuntingTripDeparture(Date hOM_houseHuntingTripDeparture) {
		HOM_houseHuntingTripDeparture = hOM_houseHuntingTripDeparture;
	}
	@Column
	public String getHOM_brokerage() {
		return HOM_brokerage;
	}
	public void setHOM_brokerage(String hOM_brokerage) {
		HOM_brokerage = hOM_brokerage;
	}
	@Column
	public Date getHOM_offerDate() {
		return HOM_offerDate;
	}
	public void setHOM_offerDate(Date hOM_offerDate) {
		HOM_offerDate = hOM_offerDate;
	}
	@Column
	public Date getHOM_closingDate() {
		return HOM_closingDate;
	}
	public void setHOM_closingDate(Date hOM_closingDate) {
		HOM_closingDate = hOM_closingDate;
	}
	@Column
	public String getDSS_vendorCode() {
		return DSS_vendorCode;
	}
	public void setDSS_vendorCode(String dSS_vendorCode) {
		DSS_vendorCode = dSS_vendorCode;
	}
	@Column
	public String getDSS_vendorName() {
		return DSS_vendorName;
	}
	public void setDSS_vendorName(String dSS_vendorName) {
		DSS_vendorName = dSS_vendorName;
	}
	@Column
	public Date getDSS_serviceStartDate() {
		return DSS_serviceStartDate;
	}
	public void setDSS_serviceStartDate(Date dSS_serviceStartDate) {
		DSS_serviceStartDate = dSS_serviceStartDate;
	}
	@Column
	public String getDSS_vendorCodeEXSO() {
		return DSS_vendorCodeEXSO;
	}
	public void setDSS_vendorCodeEXSO(String dSS_vendorCodeEXSO) {
		DSS_vendorCodeEXSO = dSS_vendorCodeEXSO;
	}
	@Column
	public String getDSS_vendorContact() {
		return DSS_vendorContact;
	}
	public void setDSS_vendorContact(String dSS_vendorContact) {
		DSS_vendorContact = dSS_vendorContact;
	}
	@Column
	public Date getDSS_serviceEndDate() {
		return DSS_serviceEndDate;
	}
	public void setDSS_serviceEndDate(Date dSS_serviceEndDate) {
		DSS_serviceEndDate = dSS_serviceEndDate;
	}
	@Column
	public String getDSS_vendorEmail() {
		return DSS_vendorEmail;
	}
	public void setDSS_vendorEmail(String dSS_vendorEmail) {
		DSS_vendorEmail = dSS_vendorEmail;
	}
	@Column
	public String getDSS_paymentResponsibility() {
		return DSS_paymentResponsibility;
	}
	public void setDSS_paymentResponsibility(String dSS_paymentResponsibility) {
		DSS_paymentResponsibility = dSS_paymentResponsibility;
	}
	@Column
	public Boolean getDSS_displyOtherVendorCode() {
		return DSS_displyOtherVendorCode;
	}
	public void setDSS_displyOtherVendorCode(Boolean dSS_displyOtherVendorCode) {
		DSS_displyOtherVendorCode = dSS_displyOtherVendorCode;
	}
	@Column
	public Date getDSS_initialContactDate() {
		return DSS_initialContactDate;
	}
	public void setDSS_initialContactDate(Date dSS_initialContactDate) {
		DSS_initialContactDate = dSS_initialContactDate;
	}
	@Column
	public String getPRV_paymentResponsibility() {
		return PRV_paymentResponsibility;
	}
	public void setPRV_paymentResponsibility(String pRV_paymentResponsibility) {
		PRV_paymentResponsibility = pRV_paymentResponsibility;
	}
	@Column
	public String getVIS_paymentResponsibility() {
		return VIS_paymentResponsibility;
	}
	public void setVIS_paymentResponsibility(String vIS_paymentResponsibility) {
		VIS_paymentResponsibility = vIS_paymentResponsibility;
	}
	@Column
	public String getTAX_paymentResponsibility() {
		return TAX_paymentResponsibility;
	}
	public void setTAX_paymentResponsibility(String tAX_paymentResponsibility) {
		TAX_paymentResponsibility = tAX_paymentResponsibility;
	}
	@Column
	public String getRNT_paymentResponsibility() {
		return RNT_paymentResponsibility;
	}
	public void setRNT_paymentResponsibility(String rNT_paymentResponsibility) {
		RNT_paymentResponsibility = rNT_paymentResponsibility;
	}
	@Column
	public String getAIO_paymentResponsibility() {
		return AIO_paymentResponsibility;
	}
	public void setAIO_paymentResponsibility(String aIO_paymentResponsibility) {
		AIO_paymentResponsibility = aIO_paymentResponsibility;
	}
	@Column
	public String getTRG_paymentResponsibility() {
		return TRG_paymentResponsibility;
	}
	public void setTRG_paymentResponsibility(String tRG_paymentResponsibility) {
		TRG_paymentResponsibility = tRG_paymentResponsibility;
	}
	@Column
	public String getLAN_paymentResponsibility() {
		return LAN_paymentResponsibility;
	}
	public void setLAN_paymentResponsibility(String lAN_paymentResponsibility) {
		LAN_paymentResponsibility = lAN_paymentResponsibility;
	}
	@Column
	public String getRPT_paymentResponsibility() {
		return RPT_paymentResponsibility;
	}
	public void setRPT_paymentResponsibility(String rPT_paymentResponsibility) {
		RPT_paymentResponsibility = rPT_paymentResponsibility;
	}
	@Column
	public String getCOL_paymentResponsibility() {
		return COL_paymentResponsibility;
	}
	public void setCOL_paymentResponsibility(String cOL_paymentResponsibility) {
		COL_paymentResponsibility = cOL_paymentResponsibility;
	}
	@Column
	public String getTAC_paymentResponsibility() {
		return TAC_paymentResponsibility;
	}
	public void setTAC_paymentResponsibility(String tAC_paymentResponsibility) {
		TAC_paymentResponsibility = tAC_paymentResponsibility;
	}
	@Column
	public String getHOM_paymentResponsibility() {
		return HOM_paymentResponsibility;
	}
	public void setHOM_paymentResponsibility(String hOM_paymentResponsibility) {
		HOM_paymentResponsibility = hOM_paymentResponsibility;
	}
	@Column
	public String getSCH_paymentResponsibility() {
		return SCH_paymentResponsibility;
	}
	public void setSCH_paymentResponsibility(String sCH_paymentResponsibility) {
		SCH_paymentResponsibility = sCH_paymentResponsibility;
	}
	@Column
	public String getEXP_paymentResponsibility() {
		return EXP_paymentResponsibility;
	}
	public void setEXP_paymentResponsibility(String eXP_paymentResponsibility) {
		EXP_paymentResponsibility = eXP_paymentResponsibility;
	}
	@Column
	public String getONG_paymentResponsibility() {
		return ONG_paymentResponsibility;
	}
	public void setONG_paymentResponsibility(String oNG_paymentResponsibility) {
		ONG_paymentResponsibility = oNG_paymentResponsibility;
	}
	@Column
	public String getTEN_paymentResponsibility() {
		return TEN_paymentResponsibility;
	}
	public void setTEN_paymentResponsibility(String tEN_paymentResponsibility) {
		TEN_paymentResponsibility = tEN_paymentResponsibility;
	}
	@Column
	public String getMMG_paymentResponsibility() {
		return MMG_paymentResponsibility;
	}
	public void setMMG_paymentResponsibility(String mMG_paymentResponsibility) {
		MMG_paymentResponsibility = mMG_paymentResponsibility;
	}
	@Column
	public String getCAR_paymentResponsibility() {
		return CAR_paymentResponsibility;
	}
	public void setCAR_paymentResponsibility(String cAR_paymentResponsibility) {
		CAR_paymentResponsibility = cAR_paymentResponsibility;
	}
	@Column
	public String getSET_paymentResponsibility() {
		return SET_paymentResponsibility;
	}
	public void setSET_paymentResponsibility(String sET_paymentResponsibility) {
		SET_paymentResponsibility = sET_paymentResponsibility;
	}
	@Column
	public String getWOP_paymentResponsibility() {
		return WOP_paymentResponsibility;
	}
	public void setWOP_paymentResponsibility(String wOP_paymentResponsibility) {
		WOP_paymentResponsibility = wOP_paymentResponsibility;
	}
	@Column
	public String getREP_paymentResponsibility() {
		return REP_paymentResponsibility;
	}
	public void setREP_paymentResponsibility(String rEP_paymentResponsibility) {
		REP_paymentResponsibility = rEP_paymentResponsibility;
	}
	@Column
	public String getHSM_agentCode() {
		return HSM_agentCode;
	}
	public void setHSM_agentCode(String hSM_agentCode) {
		HSM_agentCode = hSM_agentCode;
	}
	@Column
	public String getHSM_agentName() {
		return HSM_agentName;
	}
	public void setHSM_agentName(String hSM_agentName) {
		HSM_agentName = hSM_agentName;
	}
	@Column
	public String getHSM_brokerage() {
		return HSM_brokerage;
	}
	public void setHSM_brokerage(String hSM_brokerage) {
		HSM_brokerage = hSM_brokerage;
	}
	@Column
	public String getHSM_agentPhone() {
		return HSM_agentPhone;
	}
	public void setHSM_agentPhone(String hSM_agentPhone) {
		HSM_agentPhone = hSM_agentPhone;
	}
	@Column
	public String getHSM_agentEmail() {
		return HSM_agentEmail;
	}
	public void setHSM_agentEmail(String hSM_agentEmail) {
		HSM_agentEmail = hSM_agentEmail;
	}
	@Column
	public BigDecimal getHSM_estimatedHSRReferral() {
		return HSM_estimatedHSRReferral;
	}
	public void setHSM_estimatedHSRReferral(BigDecimal hSM_estimatedHSRReferral) {
		HSM_estimatedHSRReferral = hSM_estimatedHSRReferral;
	}
	@Column
	public BigDecimal getHSM_actualHSRReferral() {
		return HSM_actualHSRReferral;
	}
	public void setHSM_actualHSRReferral(BigDecimal hSM_actualHSRReferral) {
		HSM_actualHSRReferral = hSM_actualHSRReferral;
	}
	@Column
	public String getReminderServices() {
		return reminderServices;
	}
	public void setReminderServices(String reminderServices) {
		this.reminderServices = reminderServices;
	}
	@Column
	public String getCAT_comment() {
		return CAT_comment;
	}
	public void setCAT_comment(String cAT_comment) {
		CAT_comment = cAT_comment;
	}
	@Column
	public String getCLS_comment() {
		return CLS_comment;
	}
	public void setCLS_comment(String cLS_comment) {
		CLS_comment = cLS_comment;
	}
	@Column
	public String getCHS_comment() {
		return CHS_comment;
	}
	public void setCHS_comment(String cHS_comment) {
		CHS_comment = cHS_comment;
	}
	@Column
	public String getPDT_comment() {
		return PDT_comment;
	}
	public void setPDT_comment(String pDT_comment) {
		PDT_comment = pDT_comment;
	}
	@Column
	public String getRCP_comment() {
		return RCP_comment;
	}
	public void setRCP_comment(String rCP_comment) {
		RCP_comment = rCP_comment;
	}
	@Column
	public String getSPA_comment() {
		return SPA_comment;
	}
	public void setSPA_comment(String sPA_comment) {
		SPA_comment = sPA_comment;
	}
	@Column
	public String getTCS_comment() {
		return TCS_comment;
	}
	public void setTCS_comment(String tCS_comment) {
		TCS_comment = tCS_comment;
	}
	@Column
	public String getMTS_comment() {
		return MTS_comment;
	}
	public void setMTS_comment(String mTS_comment) {
		MTS_comment = mTS_comment;
	}
	@Column
	public String getDSS_comment() {
		return DSS_comment;
	}
	public void setDSS_comment(String dSS_comment) {
		DSS_comment = dSS_comment;
	}
	@Column
	public BigDecimal getHOM_estimatedHSRReferral() {
		return HOM_estimatedHSRReferral;
	}
	public void setHOM_estimatedHSRReferral(BigDecimal hOM_estimatedHSRReferral) {
		HOM_estimatedHSRReferral = hOM_estimatedHSRReferral;
	}
	@Column
	public BigDecimal getHOM_actualHSRReferral() {
		return HOM_actualHSRReferral;
	}
	public void setHOM_actualHSRReferral(BigDecimal hOM_actualHSRReferral) {
		HOM_actualHSRReferral = hOM_actualHSRReferral;
	}
	@Column
	public Date getHSM_offerDate() {
		return HSM_offerDate;
	}
	public void setHSM_offerDate(Date hSM_offerDate) {
		HSM_offerDate = hSM_offerDate;
	}
	@Column
	public Date getHSM_closingDate() {
		return HSM_closingDate;
	}
	public void setHSM_closingDate(Date hSM_closingDate) {
		HSM_closingDate = hSM_closingDate;
	}
	@Column
	public String getTAC_timeAuthorized() {
		return TAC_timeAuthorized;
	}
	public void setTAC_timeAuthorized(String tAC_timeAuthorized) {
		TAC_timeAuthorized = tAC_timeAuthorized;
	}
	@Column
	public String getMMG_originAgentCode() {
		return MMG_originAgentCode;
	}
	public void setMMG_originAgentCode(String mMG_originAgentCode) {
		MMG_originAgentCode = mMG_originAgentCode;
	}
	@Column
	public String getMMG_originAgentName() {
		return MMG_originAgentName;
	}
	public void setMMG_originAgentName(String mMG_originAgentName) {
		MMG_originAgentName = mMG_originAgentName;
	}
	@Column
	public String getMMG_originAgentPhone() {
		return MMG_originAgentPhone;
	}
	public void setMMG_originAgentPhone(String mMG_originAgentPhone) {
		MMG_originAgentPhone = mMG_originAgentPhone;
	}
	@Column
	public String getMMG_originAgentEmail() {
		return MMG_originAgentEmail;
	}
	public void setMMG_originAgentEmail(String mMG_originAgentEmail) {
		MMG_originAgentEmail = mMG_originAgentEmail;
	}
	@Column
	public String getMMG_destinationAgentCode() {
		return MMG_destinationAgentCode;
	}
	public void setMMG_destinationAgentCode(String mMG_destinationAgentCode) {
		MMG_destinationAgentCode = mMG_destinationAgentCode;
	}
	@Column
	public String getMMG_destinationAgentName() {
		return MMG_destinationAgentName;
	}
	public void setMMG_destinationAgentName(String mMG_destinationAgentName) {
		MMG_destinationAgentName = mMG_destinationAgentName;
	}
	@Column
	public String getMMG_destinationAgentPhone() {
		return MMG_destinationAgentPhone;
	}
	public void setMMG_destinationAgentPhone(String mMG_destinationAgentPhone) {
		MMG_destinationAgentPhone = mMG_destinationAgentPhone;
	}
	@Column
	public String getMMG_destinationAgentEmail() {
		return MMG_destinationAgentEmail;
	}
	public void setMMG_destinationAgentEmail(String mMG_destinationAgentEmail) {
		MMG_destinationAgentEmail = mMG_destinationAgentEmail;
	}
	@Column
	public Date getPRV_emailSent() {
		return PRV_emailSent;
	}
	public void setPRV_emailSent(Date pRV_emailSent) {
		PRV_emailSent = pRV_emailSent;
	}
	@Column
	public Date getTAX_emailSent() {
		return TAX_emailSent;
	}
	public void setTAX_emailSent(Date tAX_emailSent) {
		TAX_emailSent = tAX_emailSent;
	}
	@Column
	public Date getAIO_emailSent() {
		return AIO_emailSent;
	}
	public void setAIO_emailSent(Date aIO_emailSent) {
		AIO_emailSent = aIO_emailSent;
	}
	@Column
	public Date getTRG_emailSent() {
		return TRG_emailSent;
	}
	public void setTRG_emailSent(Date tRG_emailSent) {
		TRG_emailSent = tRG_emailSent;
	}
	@Column
	public Date getLAN_emailSent() {
		return LAN_emailSent;
	}
	public void setLAN_emailSent(Date lAN_emailSent) {
		LAN_emailSent = lAN_emailSent;
	}
	@Column
	public Date getRPT_emailSent() {
		return RPT_emailSent;
	}
	public void setRPT_emailSent(Date rPT_emailSent) {
		RPT_emailSent = rPT_emailSent;
	}
	@Column
	public Date getCOL_emailSent() {
		return COL_emailSent;
	}
	public void setCOL_emailSent(Date cOL_emailSent) {
		COL_emailSent = cOL_emailSent;
	}
	@Column
	public Date getTAC_emailSent() {
		return TAC_emailSent;
	}
	public void setTAC_emailSent(Date tAC_emailSent) {
		TAC_emailSent = tAC_emailSent;
	}
	@Column
	public Date getHOM_emailSent() {
		return HOM_emailSent;
	}
	public void setHOM_emailSent(Date hOM_emailSent) {
		HOM_emailSent = hOM_emailSent;
	}
	@Column
	public Date getSCH_emailSent() {
		return SCH_emailSent;
	}
	public void setSCH_emailSent(Date sCH_emailSent) {
		SCH_emailSent = sCH_emailSent;
	}
	@Column
	public Date getEXP_emailSent() {
		return EXP_emailSent;
	}
	public void setEXP_emailSent(Date eXP_emailSent) {
		EXP_emailSent = eXP_emailSent;
	}
	@Column
	public Date getONG_emailSent() {
		return ONG_emailSent;
	}
	public void setONG_emailSent(Date oNG_emailSent) {
		ONG_emailSent = oNG_emailSent;
	}
	@Column
	public Date getTEN_emailSent() {
		return TEN_emailSent;
	}
	public void setTEN_emailSent(Date tEN_emailSent) {
		TEN_emailSent = tEN_emailSent;
	}
	@Column
	public Date getMMG_orginEmailSent() {
		return MMG_orginEmailSent;
	}
	public void setMMG_orginEmailSent(Date mMG_orginEmailSent) {
		MMG_orginEmailSent = mMG_orginEmailSent;
	}
	@Column
	public Date getMMG_destinationEmailSent() {
		return MMG_destinationEmailSent;
	}
	public void setMMG_destinationEmailSent(Date mMG_destinationEmailSent) {
		MMG_destinationEmailSent = mMG_destinationEmailSent;
	}
	@Column
	public Date getCAR_emailSent() {
		return CAR_emailSent;
	}
	public void setCAR_emailSent(Date cAR_emailSent) {
		CAR_emailSent = cAR_emailSent;
	}
	@Column
	public Date getSET_emailSent() {
		return SET_emailSent;
	}
	public void setSET_emailSent(Date sET_emailSent) {
		SET_emailSent = sET_emailSent;
	}
	@Column
	public Date getCAT_emailSent() {
		return CAT_emailSent;
	}
	public void setCAT_emailSent(Date cAT_emailSent) {
		CAT_emailSent = cAT_emailSent;
	}
	@Column
	public Date getCLS_emailSent() {
		return CLS_emailSent;
	}
	public void setCLS_emailSent(Date cLS_emailSent) {
		CLS_emailSent = cLS_emailSent;
	}
	@Column
	public Date getCHS_emailSent() {
		return CHS_emailSent;
	}
	public void setCHS_emailSent(Date cHS_emailSent) {
		CHS_emailSent = cHS_emailSent;
	}
	@Column
	public Date getDPS_emailSent() {
		return DPS_emailSent;
	}
	public void setDPS_emailSent(Date dPS_emailSent) {
		DPS_emailSent = dPS_emailSent;
	}
	@Column
	public Date getHSM_emailSent() {
		return HSM_emailSent;
	}
	public void setHSM_emailSent(Date hSM_emailSent) {
		HSM_emailSent = hSM_emailSent;
	}
	@Column
	public Date getPDT_emailSent() {
		return PDT_emailSent;
	}
	public void setPDT_emailSent(Date pDT_emailSent) {
		PDT_emailSent = pDT_emailSent;
	}
	@Column
	public Date getRCP_emailSent() {
		return RCP_emailSent;
	}
	public void setRCP_emailSent(Date rCP_emailSent) {
		RCP_emailSent = rCP_emailSent;
	}
	@Column
	public Date getSPA_emailSent() {
		return SPA_emailSent;
	}
	public void setSPA_emailSent(Date sPA_emailSent) {
		SPA_emailSent = sPA_emailSent;
	}
	@Column
	public Date getTCS_emailSent() {
		return TCS_emailSent;
	}
	public void setTCS_emailSent(Date tCS_emailSent) {
		TCS_emailSent = tCS_emailSent;
	}
	@Column
	public Date getMTS_emailSent() {
		return MTS_emailSent;
	}
	public void setMTS_emailSent(Date mTS_emailSent) {
		MTS_emailSent = mTS_emailSent;
	}
	@Column
	public Date getDSS_emailSent() {
		return DSS_emailSent;
	}
	public void setDSS_emailSent(Date dSS_emailSent) {
		DSS_emailSent = dSS_emailSent;
	}
	@Column
	public Date getVIS_emailSent() {
		return VIS_emailSent;
	}
	public void setVIS_emailSent(Date vIS_emailSent) {
		VIS_emailSent = vIS_emailSent;
	}
	@Column
	public Date getRNT_emailSent() {
		return RNT_emailSent;
	}
	public void setRNT_emailSent(Date rNT_emailSent) {
		RNT_emailSent = rNT_emailSent;
	}
	@Column
	public Date getNET_emailSent() {
		return NET_emailSent;
	}
	public void setNET_emailSent(Date nET_emailSent) {
		NET_emailSent = nET_emailSent;
	}
	@Column
	public Date getWOP_emailSent() {
		return WOP_emailSent;
	}
	public void setWOP_emailSent(Date wOP_emailSent) {
		WOP_emailSent = wOP_emailSent;
	}
	@Column
	public Date getREP_emailSent() {
		return REP_emailSent;
	}
	public void setREP_emailSent(Date rEP_emailSent) {
		REP_emailSent = rEP_emailSent;
	}
	@Column
	public String getResendEmailServiceName() {
		return resendEmailServiceName;
	}
	public void setResendEmailServiceName(String resendEmailServiceName) {
		this.resendEmailServiceName = resendEmailServiceName;
	}
	@Column
	public Date getHOB_serviceStartDate() {
		return HOB_serviceStartDate;
	}
	public void setHOB_serviceStartDate(Date hOB_serviceStartDate) {
		HOB_serviceStartDate = hOB_serviceStartDate;
	}
	@Column
	public Date getHOB_serviceEndDate() {
		return HOB_serviceEndDate;
	}
	public void setHOB_serviceEndDate(Date hOB_serviceEndDate) {
		HOB_serviceEndDate = hOB_serviceEndDate;
	}
	@Column
	public String getHOB_hotelName() {
		return HOB_hotelName;
	}
	public void setHOB_hotelName(String hOB_hotelName) {
		HOB_hotelName = hOB_hotelName;
	}
	@Column
	public String getHOB_city() {
		return HOB_city;
	}
	public void setHOB_city(String hOB_city) {
		HOB_city = hOB_city;
	}
	@Column
	public Date getFLB_arrivalDate() {
		return FLB_arrivalDate;
	}
	public void setFLB_arrivalDate(Date fLB_arrivalDate) {
		FLB_arrivalDate = fLB_arrivalDate;
	}
	@Column
	public Date getFLB_departureDate() {
		return FLB_departureDate;
	}
	public void setFLB_departureDate(Date fLB_departureDate) {
		FLB_departureDate = fLB_departureDate;
	}
	@Column
	public Date getFLB_additionalBookingReminderDate() {
		return FLB_additionalBookingReminderDate;
	}
	public void setFLB_additionalBookingReminderDate(
			Date fLB_additionalBookingReminderDate) {
		FLB_additionalBookingReminderDate = fLB_additionalBookingReminderDate;
	}
	@Column
	public Date getFLB_arrivalDate1() {
		return FLB_arrivalDate1;
	}
	public void setFLB_arrivalDate1(Date fLB_arrivalDate1) {
		FLB_arrivalDate1 = fLB_arrivalDate1;
	}
	@Column
	public Date getFLB_departureDate1() {
		return FLB_departureDate1;
	}
	public void setFLB_departureDate1(Date fLB_departureDate1) {
		FLB_departureDate1 = fLB_departureDate1;
	}
	@Column
	public Date getFLB_additionalBookingReminderDate1() {
		return FLB_additionalBookingReminderDate1;
	}
	public void setFLB_additionalBookingReminderDate1(
			Date fLB_additionalBookingReminderDate1) {
		FLB_additionalBookingReminderDate1 = fLB_additionalBookingReminderDate1;
	}
	@Column
	public Boolean getFLB_addOn() {
		return FLB_addOn;
	}
	public void setFLB_addOn(Boolean fLB_addOn) {
		FLB_addOn = fLB_addOn;
	}
	@Column
	public Date getHOB_startDate() {
		return HOB_startDate;
	}
	public void setHOB_startDate(Date hOB_startDate) {
		HOB_startDate = hOB_startDate;
	}
	@Column
	public Date getHOB_endDate() {
		return HOB_endDate;
	}
	public void setHOB_endDate(Date hOB_endDate) {
		HOB_endDate = hOB_endDate;
	}
	@Column
	public String getHOB_vendorCode() {
		return HOB_vendorCode;
	}
	public void setHOB_vendorCode(String hOB_vendorCode) {
		HOB_vendorCode = hOB_vendorCode;
	}
	@Column
	public String getHOB_vendorName() {
		return HOB_vendorName;
	}
	public void setHOB_vendorName(String hOB_vendorName) {
		HOB_vendorName = hOB_vendorName;
	}
	@Column
	public String getHOB_vendorContact() {
		return HOB_vendorContact;
	}
	public void setHOB_vendorContact(String hOB_vendorContact) {
		HOB_vendorContact = hOB_vendorContact;
	}
	@Column
	public String getHOB_vendorEmail() {
		return HOB_vendorEmail;
	}
	public void setHOB_vendorEmail(String hOB_vendorEmail) {
		HOB_vendorEmail = hOB_vendorEmail;
	}
	@Column
	public String getHOB_vendorCodeEXSO() {
		return HOB_vendorCodeEXSO;
	}
	public void setHOB_vendorCodeEXSO(String hOB_vendorCodeEXSO) {
		HOB_vendorCodeEXSO = hOB_vendorCodeEXSO;
	}
	@Column
	public String getFLB_vendorCode() {
		return FLB_vendorCode;
	}
	public void setFLB_vendorCode(String fLB_vendorCode) {
		FLB_vendorCode = fLB_vendorCode;
	}
	@Column
	public String getFLB_vendorName() {
		return FLB_vendorName;
	}
	public void setFLB_vendorName(String fLB_vendorName) {
		FLB_vendorName = fLB_vendorName;
	}
	@Column
	public String getFLB_vendorContact() {
		return FLB_vendorContact;
	}
	public void setFLB_vendorContact(String fLB_vendorContact) {
		FLB_vendorContact = fLB_vendorContact;
	}
	@Column
	public String getFLB_vendorEmail() {
		return FLB_vendorEmail;
	}
	public void setFLB_vendorEmail(String fLB_vendorEmail) {
		FLB_vendorEmail = fLB_vendorEmail;
	}
	@Column
	public Date getFLB_serviceStartDate() {
		return FLB_serviceStartDate;
	}
	public void setFLB_serviceStartDate(Date fLB_serviceStartDate) {
		FLB_serviceStartDate = fLB_serviceStartDate;
	}
	@Column
	public Date getFLB_serviceEndDate() {
		return FLB_serviceEndDate;
	}
	public void setFLB_serviceEndDate(Date fLB_serviceEndDate) {
		FLB_serviceEndDate = fLB_serviceEndDate;
	}
	@Column
	public String getFLB_vendorCodeEXSO() {
		return FLB_vendorCodeEXSO;
	}
	public void setFLB_vendorCodeEXSO(String fLB_vendorCodeEXSO) {
		FLB_vendorCodeEXSO = fLB_vendorCodeEXSO;
	}
	@Column
	public Date getFLB_emailSent() {
		return FLB_emailSent;
	}
	public void setFLB_emailSent(Date fLB_emailSent) {
		FLB_emailSent = fLB_emailSent;
	}
	@Column
	public Date getHOB_emailSent() {
		return HOB_emailSent;
	}
	public void setHOB_emailSent(Date hOB_emailSent) {
		HOB_emailSent = hOB_emailSent;
	}
	@Column
	public Boolean getHOB_displyOtherVendorCode() {
		return HOB_displyOtherVendorCode;
	}
	public void setHOB_displyOtherVendorCode(Boolean hOB_displyOtherVendorCode) {
		HOB_displyOtherVendorCode = hOB_displyOtherVendorCode;
	}
	@Column
	public Boolean getFLB_displyOtherVendorCode() {
		return FLB_displyOtherVendorCode;
	}
	public void setFLB_displyOtherVendorCode(Boolean fLB_displyOtherVendorCode) {
		FLB_displyOtherVendorCode = fLB_displyOtherVendorCode;
	}
	@Column
	public String getVIS_visaExtensionNeeded() {
		return VIS_visaExtensionNeeded;
	}
	public void setVIS_visaExtensionNeeded(String vIS_visaExtensionNeeded) {
		VIS_visaExtensionNeeded = vIS_visaExtensionNeeded;
	}
	@Column
	public String getRNT_leaseExtensionNeeded() {
		return RNT_leaseExtensionNeeded;
	}
	public void setRNT_leaseExtensionNeeded(String rNT_leaseExtensionNeeded) {
		RNT_leaseExtensionNeeded = rNT_leaseExtensionNeeded;
	}
	@Column(length=25)
	public String getRNT_homeSearchType() {
		return RNT_homeSearchType;
	}
	public void setRNT_homeSearchType(String rNT_homeSearchType) {
		RNT_homeSearchType = rNT_homeSearchType;
	}
	@Column
	public String getFRL_vendorCode() {
		return FRL_vendorCode;
	}
	public void setFRL_vendorCode(String fRL_vendorCode) {
		FRL_vendorCode = fRL_vendorCode;
	}
	@Column
	public String getFRL_vendorName() {
		return FRL_vendorName;
	}
	public void setFRL_vendorName(String fRL_vendorName) {
		FRL_vendorName = fRL_vendorName;
	}
	@Column
	public Date getFRL_serviceStartDate() {
		return FRL_serviceStartDate;
	}
	public void setFRL_serviceStartDate(Date fRL_serviceStartDate) {
		FRL_serviceStartDate = fRL_serviceStartDate;
	}
	@Column
	public String getFRL_vendorCodeEXSO() {
		return FRL_vendorCodeEXSO;
	}
	public void setFRL_vendorCodeEXSO(String fRL_vendorCodeEXSO) {
		FRL_vendorCodeEXSO = fRL_vendorCodeEXSO;
	}
	@Column
	public String getFRL_vendorContact() {
		return FRL_vendorContact;
	}
	public void setFRL_vendorContact(String fRL_vendorContact) {
		FRL_vendorContact = fRL_vendorContact;
	}
	@Column
	public Date getFRL_serviceEndDate() {
		return FRL_serviceEndDate;
	}
	public void setFRL_serviceEndDate(Date fRL_serviceEndDate) {
		FRL_serviceEndDate = fRL_serviceEndDate;
	}
	@Column
	public Date getFRL_emailSent() {
		return FRL_emailSent;
	}
	public void setFRL_emailSent(Date fRL_emailSent) {
		FRL_emailSent = fRL_emailSent;
	}
	@Column
	public String getFRL_vendorEmail() {
		return FRL_vendorEmail;
	}
	public void setFRL_vendorEmail(String fRL_vendorEmail) {
		FRL_vendorEmail = fRL_vendorEmail;
	}
	@Column
	public Boolean getFRL_displyOtherVendorCode() {
		return FRL_displyOtherVendorCode;
	}
	public void setFRL_displyOtherVendorCode(Boolean fRL_displyOtherVendorCode) {
		FRL_displyOtherVendorCode = fRL_displyOtherVendorCode;
	}
	@Column
	public String getFRL_paymentResponsibility() {
		return FRL_paymentResponsibility;
	}
	public void setFRL_paymentResponsibility(String fRL_paymentResponsibility) {
		FRL_paymentResponsibility = fRL_paymentResponsibility;
	}
	@Column
	public String getFRL_rentalRate() {
		return FRL_rentalRate;
	}
	public void setFRL_rentalRate(String fRL_rentalRate) {
		FRL_rentalRate = fRL_rentalRate;
	}
	@Column
	public String getFRL_deposit() {
		return FRL_deposit;
	}
	public void setFRL_deposit(String fRL_deposit) {
		FRL_deposit = fRL_deposit;
	}
	@Column
	public String getFRL_rentalCurrency() {
		return FRL_rentalCurrency;
	}
	public void setFRL_rentalCurrency(String fRL_rentalCurrency) {
		FRL_rentalCurrency = fRL_rentalCurrency;
	}
	@Column
	public String getFRL_depositCurrency() {
		return FRL_depositCurrency;
	}
	public void setFRL_depositCurrency(String fRL_depositCurrency) {
		FRL_depositCurrency = fRL_depositCurrency;
	}
	@Column
	public String getFRL_month() {
		return FRL_month;
	}
	public void setFRL_month(String fRL_month) {
		FRL_month = fRL_month;
	}
	@Column
	public Date getFRL_rentalStartDate() {
		return FRL_rentalStartDate;
	}
	public void setFRL_rentalStartDate(Date fRL_rentalStartDate) {
		FRL_rentalStartDate = fRL_rentalStartDate;
	}
	@Column
	public Date getFRL_rentalEndDate() {
		return FRL_rentalEndDate;
	}
	public void setFRL_rentalEndDate(Date fRL_rentalEndDate) {
		FRL_rentalEndDate = fRL_rentalEndDate;
	}
	@Column
	public String getFRL_terminationNotice() {
		return FRL_terminationNotice;
	}
	public void setFRL_terminationNotice(String fRL_terminationNotice) {
		FRL_terminationNotice = fRL_terminationNotice;
	}
	@Column
	public String getFRL_comment() {
		return FRL_comment;
	}
	public void setFRL_comment(String fRL_comment) {
		FRL_comment = fRL_comment;
	}
	@Column
	public String getAPU_vendorCode() {
		return APU_vendorCode;
	}
	public void setAPU_vendorCode(String aPU_vendorCode) {
		APU_vendorCode = aPU_vendorCode;
	}
	@Column
	public String getAPU_vendorName() {
		return APU_vendorName;
	}
	public void setAPU_vendorName(String aPU_vendorName) {
		APU_vendorName = aPU_vendorName;
	}
	@Column
	public Date getAPU_serviceStartDate() {
		return APU_serviceStartDate;
	}
	public void setAPU_serviceStartDate(Date aPU_serviceStartDate) {
		APU_serviceStartDate = aPU_serviceStartDate;
	}
	@Column
	public String getAPU_vendorCodeEXSO() {
		return APU_vendorCodeEXSO;
	}
	public void setAPU_vendorCodeEXSO(String aPU_vendorCodeEXSO) {
		APU_vendorCodeEXSO = aPU_vendorCodeEXSO;
	}
	@Column
	public String getAPU_vendorContact() {
		return APU_vendorContact;
	}
	public void setAPU_vendorContact(String aPU_vendorContact) {
		APU_vendorContact = aPU_vendorContact;
	}
	@Column
	public Date getAPU_serviceEndDate() {
		return APU_serviceEndDate;
	}
	public void setAPU_serviceEndDate(Date aPU_serviceEndDate) {
		APU_serviceEndDate = aPU_serviceEndDate;
	}
	@Column
	public Date getAPU_emailSent() {
		return APU_emailSent;
	}
	public void setAPU_emailSent(Date aPU_emailSent) {
		APU_emailSent = aPU_emailSent;
	}
	@Column
	public String getAPU_vendorEmail() {
		return APU_vendorEmail;
	}
	public void setAPU_vendorEmail(String aPU_vendorEmail) {
		APU_vendorEmail = aPU_vendorEmail;
	}
	@Column
	public Boolean getAPU_displyOtherVendorCode() {
		return APU_displyOtherVendorCode;
	}
	public void setAPU_displyOtherVendorCode(Boolean aPU_displyOtherVendorCode) {
		APU_displyOtherVendorCode = aPU_displyOtherVendorCode;
	}
	@Column
	public String getAPU_paymentResponsibility() {
		return APU_paymentResponsibility;
	}
	public void setAPU_paymentResponsibility(String aPU_paymentResponsibility) {
		APU_paymentResponsibility = aPU_paymentResponsibility;
	}
	@Column
	public String getAPU_flightDetails() {
		return APU_flightDetails;
	}
	public void setAPU_flightDetails(String aPU_flightDetails) {
		APU_flightDetails = aPU_flightDetails;
	}
	@Column
	public String getAPU_driverName() {
		return APU_driverName;
	}
	public void setAPU_driverName(String aPU_driverName) {
		APU_driverName = aPU_driverName;
	}
	@Column
	public String getAPU_driverPhoneNumber() {
		return APU_driverPhoneNumber;
	}
	public void setAPU_driverPhoneNumber(String aPU_driverPhoneNumber) {
		APU_driverPhoneNumber = aPU_driverPhoneNumber;
	}
	@Column
	public Date getAPU_arrivalDate() {
		return APU_arrivalDate;
	}
	public void setAPU_arrivalDate(Date aPU_arrivalDate) {
		APU_arrivalDate = aPU_arrivalDate;
	}
	@Column
	public String getAPU_carMake() {
		return APU_carMake;
	}
	public void setAPU_carMake(String aPU_carMake) {
		APU_carMake = aPU_carMake;
	}
	@Column
	public String getAPU_carModel() {
		return APU_carModel;
	}
	public void setAPU_carModel(String aPU_carModel) {
		APU_carModel = aPU_carModel;
	}
	@Column
	public String getAPU_carColor() {
		return APU_carColor;
	}
	public void setAPU_carColor(String aPU_carColor) {
		APU_carColor = aPU_carColor;
	}
	@Column
	public String getWOP_leaseExNeeded() {
		return WOP_leaseExNeeded;
	}
	public void setWOP_leaseExNeeded(String wOP_leaseExNeeded) {
		WOP_leaseExNeeded = wOP_leaseExNeeded;
	}
	@Column
	public String getREP_leaseExNeeded() {
		return REP_leaseExNeeded;
	}
	public void setREP_leaseExNeeded(String rEP_leaseExNeeded) {
		REP_leaseExNeeded = rEP_leaseExNeeded;
	}
	@Column
	public String getTEN_propertyName() {
		return TEN_propertyName;
	}
	public void setTEN_propertyName(String tEN_propertyName) {
		TEN_propertyName = tEN_propertyName;
	}
	@Column
	public String getTEN_city() {
		return TEN_city;
	}
	public void setTEN_city(String tEN_city) {
		TEN_city = tEN_city;
	}
	@Column
	public String getTEN_zipCode() {
		return TEN_zipCode;
	}
	public void setTEN_zipCode(String tEN_zipCode) {
		TEN_zipCode = tEN_zipCode;
	}
	@Column
	public String getTEN_addressLine1() {
		return TEN_addressLine1;
	}
	public void setTEN_addressLine1(String tEN_addressLine1) {
		TEN_addressLine1 = tEN_addressLine1;
	}
	@Column
	public String getTEN_addressLine2() {
		return TEN_addressLine2;
	}
	public void setTEN_addressLine2(String tEN_addressLine2) {
		TEN_addressLine2 = tEN_addressLine2;
	}
	@Column
	public String getTEN_state() {
		return TEN_state;
	}
	public void setTEN_state(String tEN_state) {
		TEN_state = tEN_state;
	}
	@Column
	public String getTEN_country() {
		return TEN_country;
	}
	public void setTEN_country(String tEN_country) {
		TEN_country = tEN_country;
	}
	@Column
	public String getTEN_leasedBy() {
		return TEN_leasedBy;
	}
	public void setTEN_leasedBy(String tEN_leasedBy) {
		TEN_leasedBy = tEN_leasedBy;
	}
	@Column
	public BigDecimal getTEN_rentAmount() {
		return TEN_rentAmount;
	}
	public void setTEN_rentAmount(BigDecimal tEN_rentAmount) {
		TEN_rentAmount = tEN_rentAmount;
	}
	@Column
	public String getTEN_rentCurrency() {
		return TEN_rentCurrency;
	}
	public void setTEN_rentCurrency(String tEN_rentCurrency) {
		TEN_rentCurrency = tEN_rentCurrency;
	}
	@Column
	public BigDecimal getTEN_rentAllowance() {
		return TEN_rentAllowance;
	}
	public void setTEN_rentAllowance(BigDecimal tEN_rentAllowance) {
		TEN_rentAllowance = tEN_rentAllowance;
	}
	@Column
	public String getTEN_allowanceCurrency() {
		return TEN_allowanceCurrency;
	}
	public void setTEN_allowanceCurrency(String tEN_allowanceCurrency) {
		TEN_allowanceCurrency = tEN_allowanceCurrency;
	}
	@Column
	public String getTEN_utilitiesIncluded() {
		return TEN_utilitiesIncluded;
	}
	public void setTEN_utilitiesIncluded(String tEN_utilitiesIncluded) {
		TEN_utilitiesIncluded = tEN_utilitiesIncluded;
	}
	@Column
	public String getTEN_rentPaidTo() {
		return TEN_rentPaidTo;
	}
	public void setTEN_rentPaidTo(String tEN_rentPaidTo) {
		TEN_rentPaidTo = tEN_rentPaidTo;
	}
	@Column
	public Date getTEN_rentalIncreaseDate() {
		return TEN_rentalIncreaseDate;
	}
	public void setTEN_rentalIncreaseDate(Date tEN_rentalIncreaseDate) {
		TEN_rentalIncreaseDate = tEN_rentalIncreaseDate;
	}
	@Column
	public String getTEN_rentalComment() {
		return TEN_rentalComment;
	}
	public void setTEN_rentalComment(String tEN_rentalComment) {
		TEN_rentalComment = tEN_rentalComment;
	}
	@Column
	public String getTEN_followUpNeeded() {
		return TEN_followUpNeeded;
	}
	public void setTEN_followUpNeeded(String tEN_followUpNeeded) {
		TEN_followUpNeeded = tEN_followUpNeeded;
	}
	@Column
	public String getTEN_termOfNotice() {
		return TEN_termOfNotice;
	}
	public void setTEN_termOfNotice(String tEN_termOfNotice) {
		TEN_termOfNotice = tEN_termOfNotice;
	}
	@Column
	public Date getTEN_checkInMoveIn() {
		return TEN_checkInMoveIn;
	}
	public void setTEN_checkInMoveIn(Date tEN_checkInMoveIn) {
		TEN_checkInMoveIn = tEN_checkInMoveIn;
	}
	@Column
	public Date getTEN_preCheckOut() {
		return TEN_preCheckOut;
	}
	public void setTEN_preCheckOut(Date tEN_preCheckOut) {
		TEN_preCheckOut = tEN_preCheckOut;
	}
	@Column
	public Date getTEN_checkOutMoveOut() {
		return TEN_checkOutMoveOut;
	}
	public void setTEN_checkOutMoveOut(Date tEN_checkOutMoveOut) {
		TEN_checkOutMoveOut = tEN_checkOutMoveOut;
	}
	@Column
	public String getTEN_swiftCode() {
		return TEN_swiftCode;
	}
	public void setTEN_swiftCode(String tEN_swiftCode) {
		TEN_swiftCode = tEN_swiftCode;
	}
	@Column
	public String getTEN_paymentDescription() {
		return TEN_paymentDescription;
	}
	public void setTEN_paymentDescription(String tEN_paymentDescription) {
		TEN_paymentDescription = tEN_paymentDescription;
	}
	@Column
	public String getTEN_exceptionComments() {
		return TEN_exceptionComments;
	}
	public void setTEN_exceptionComments(String tEN_exceptionComments) {
		TEN_exceptionComments = tEN_exceptionComments;
	}
	@Column
	public BigDecimal getTEN_depositAmount() {
		return TEN_depositAmount;
	}
	public void setTEN_depositAmount(BigDecimal tEN_depositAmount) {
		TEN_depositAmount = tEN_depositAmount;
	}
	@Column
	public String getTEN_depositReturned() {
		return TEN_depositReturned;
	}
	public void setTEN_depositReturned(String tEN_depositReturned) {
		TEN_depositReturned = tEN_depositReturned;
	}
	@Column
	public String getTEN_depositCurrency() {
		return TEN_depositCurrency;
	}
	public void setTEN_depositCurrency(String tEN_depositCurrency) {
		TEN_depositCurrency = tEN_depositCurrency;
	}
	@Column
	public BigDecimal getTEN_depositReturnedAmount() {
		return TEN_depositReturnedAmount;
	}
	public void setTEN_depositReturnedAmount(BigDecimal tEN_depositReturnedAmount) {
		TEN_depositReturnedAmount = tEN_depositReturnedAmount;
	}
	@Column
	public String getTEN_depositPaidBy() {
		return TEN_depositPaidBy;
	}
	public void setTEN_depositPaidBy(String tEN_depositPaidBy) {
		TEN_depositPaidBy = tEN_depositPaidBy;
	}
	@Column
	public String getTEN_depositReturnedCurrency() {
		return TEN_depositReturnedCurrency;
	}
	public void setTEN_depositReturnedCurrency(String tEN_depositReturnedCurrency) {
		TEN_depositReturnedCurrency = tEN_depositReturnedCurrency;
	}
	@Column
	public String getTEN_depositComment() {
		return TEN_depositComment;
	}
	public void setTEN_depositComment(String tEN_depositComment) {
		TEN_depositComment = tEN_depositComment;
	}
	@Column
	public String getTEN_refundableTo() {
		return TEN_refundableTo;
	}
	public void setTEN_refundableTo(String tEN_refundableTo) {
		TEN_refundableTo = tEN_refundableTo;
	}
	@Column
	public Boolean getTEN_Gas_Water() {
		return TEN_Gas_Water;
	}
	public void setTEN_Gas_Water(Boolean tEN_Gas_Water) {
		TEN_Gas_Water = tEN_Gas_Water;
	}
	
	@Column
	public Boolean getTEN_mobilePhone() {
		return TEN_mobilePhone;
	}
	public void setTEN_mobilePhone(Boolean tEN_mobilePhone) {
		TEN_mobilePhone = tEN_mobilePhone;
	}
	@Column
	public Boolean getTEN_furnitureRental() {
		return TEN_furnitureRental;
	}
	public void setTEN_furnitureRental(Boolean tEN_furnitureRental) {
		TEN_furnitureRental = tEN_furnitureRental;
	}
	@Column
	public Boolean getTEN_cleaningServices() {
		return TEN_cleaningServices;
	}
	public void setTEN_cleaningServices(Boolean tEN_cleaningServices) {
		TEN_cleaningServices = tEN_cleaningServices;
	}
	@Column
	public Boolean getTEN_parkingPermit() {
		return TEN_parkingPermit;
	}
	public void setTEN_parkingPermit(Boolean tEN_parkingPermit) {
		TEN_parkingPermit = tEN_parkingPermit;
	}
	@Column
	public Boolean getTEN_communityTax() {
		return TEN_communityTax;
	}
	public void setTEN_communityTax(Boolean tEN_communityTax) {
		TEN_communityTax = tEN_communityTax;
	}
	@Column
	public Boolean getTEN_insurance() {
		return TEN_insurance;
	}
	public void setTEN_insurance(Boolean tEN_insurance) {
		TEN_insurance = tEN_insurance;
	}
	@Column
	public Boolean getTEN_gardenMaintenance() {
		return TEN_gardenMaintenance;
	}
	public void setTEN_gardenMaintenance(Boolean tEN_gardenMaintenance) {
		TEN_gardenMaintenance = tEN_gardenMaintenance;
	}
	@Column
	public String getTEN_toBePaidBy() {
		return TEN_toBePaidBy;
	}
	public void setTEN_toBePaidBy(String tEN_toBePaidBy) {
		TEN_toBePaidBy = tEN_toBePaidBy;
	}	
	@Column
	public String getTEN_description() {
		return TEN_description;
	}
	public void setTEN_description(String tEN_description) {
		TEN_description = tEN_description;
	}
	@Column
	public String getTEN_IBAN_BankAccountNumber() {
		return TEN_IBAN_BankAccountNumber;
	}
	public void setTEN_IBAN_BankAccountNumber(String tEN_IBAN_BankAccountNumber) {
		TEN_IBAN_BankAccountNumber = tEN_IBAN_BankAccountNumber;
	}
	@Column
	public String getTEN_BIC_SWIFT() {
		return TEN_BIC_SWIFT;
	}
	public void setTEN_BIC_SWIFT(String tEN_BIC_SWIFT) {
		TEN_BIC_SWIFT = tEN_BIC_SWIFT;
	}
	@Column
	public Boolean getTEN_TV_Internet_Phone() {
		return TEN_TV_Internet_Phone;
	}
	public void setTEN_TV_Internet_Phone(Boolean tEN_TV_Internet_Phone) {
		TEN_TV_Internet_Phone = tEN_TV_Internet_Phone;
	}
	@Column
	public String getTEN_assigneeContributionCurrency() {
		return TEN_assigneeContributionCurrency;
	}
	public void setTEN_assigneeContributionCurrency(
			String tEN_assigneeContributionCurrency) {
		TEN_assigneeContributionCurrency = tEN_assigneeContributionCurrency;
	}
	public BigDecimal getTEN_assigneeContributionAmount() {
		return TEN_assigneeContributionAmount;
	}
	public void setTEN_assigneeContributionAmount(
			BigDecimal tEN_assigneeContributionAmount) {
		TEN_assigneeContributionAmount = tEN_assigneeContributionAmount;
	}
	public Boolean getTEN_directDebit() {
		return TEN_directDebit;
	}
	public void setTEN_directDebit(Boolean tEN_directDebit) {
		TEN_directDebit = tEN_directDebit;
	}
	public Boolean getTEN_Utility_Gas_Water() {
		return TEN_Utility_Gas_Water;
	}
	public void setTEN_Utility_Gas_Water(Boolean tEN_Utility_Gas_Water) {
		TEN_Utility_Gas_Water = tEN_Utility_Gas_Water;
	}
	
	public Boolean getTEN_Utility_TV_Internet_Phone() {
		return TEN_Utility_TV_Internet_Phone;
	}
	public void setTEN_Utility_TV_Internet_Phone(
			Boolean tEN_Utility_TV_Internet_Phone) {
		TEN_Utility_TV_Internet_Phone = tEN_Utility_TV_Internet_Phone;
	}
	public Boolean getTEN_Utility_mobilePhone() {
		return TEN_Utility_mobilePhone;
	}
	public void setTEN_Utility_mobilePhone(Boolean tEN_Utility_mobilePhone) {
		TEN_Utility_mobilePhone = tEN_Utility_mobilePhone;
	}
	public Boolean getTEN_Utility_furnitureRental() {
		return TEN_Utility_furnitureRental;
	}
	public void setTEN_Utility_furnitureRental(Boolean tEN_Utility_furnitureRental) {
		TEN_Utility_furnitureRental = tEN_Utility_furnitureRental;
	}
	public Boolean getTEN_Utility_cleaningServices() {
		return TEN_Utility_cleaningServices;
	}
	public void setTEN_Utility_cleaningServices(Boolean tEN_Utility_cleaningServices) {
		TEN_Utility_cleaningServices = tEN_Utility_cleaningServices;
	}
	public Boolean getTEN_Utility_parkingPermit() {
		return TEN_Utility_parkingPermit;
	}
	public void setTEN_Utility_parkingPermit(Boolean tEN_Utility_parkingPermit) {
		TEN_Utility_parkingPermit = tEN_Utility_parkingPermit;
	}
	public Boolean getTEN_Utility_communityTax() {
		return TEN_Utility_communityTax;
	}
	public void setTEN_Utility_communityTax(Boolean tEN_Utility_communityTax) {
		TEN_Utility_communityTax = tEN_Utility_communityTax;
	}
	public Boolean getTEN_Utility_insurance() {
		return TEN_Utility_insurance;
	}
	public void setTEN_Utility_insurance(Boolean tEN_Utility_insurance) {
		TEN_Utility_insurance = tEN_Utility_insurance;
	}
	public Boolean getTEN_Utility_gardenMaintenance() {
		return TEN_Utility_gardenMaintenance;
	}
	public void setTEN_Utility_gardenMaintenance(
			Boolean tEN_Utility_gardenMaintenance) {
		TEN_Utility_gardenMaintenance = tEN_Utility_gardenMaintenance;
	}
	public BigDecimal getTEN_contributionAmount() {
		return TEN_contributionAmount;
	}
	public void setTEN_contributionAmount(BigDecimal tEN_contributionAmount) {
		TEN_contributionAmount = tEN_contributionAmount;
	}
	public Boolean getTEN_Gas() {
		return TEN_Gas;
	}
	public void setTEN_Gas(Boolean tEN_Gas) {
		TEN_Gas = tEN_Gas;
	}
	public Boolean getTEN_Electricity() {
		return TEN_Electricity;
	}
	public void setTEN_Electricity(Boolean tEN_Electricity) {
		TEN_Electricity = tEN_Electricity;
	}
	public Boolean getTEN_Miscellaneous() {
		return TEN_Miscellaneous;
	}
	public void setTEN_Miscellaneous(Boolean tEN_Miscellaneous) {
		TEN_Miscellaneous = tEN_Miscellaneous;
	}
	public Boolean getTEN_Utility_Gas() {
		return TEN_Utility_Gas;
	}
	public void setTEN_Utility_Gas(Boolean tEN_Utility_Gas) {
		TEN_Utility_Gas = tEN_Utility_Gas;
	}
	public Boolean getTEN_Utility_Electricity() {
		return TEN_Utility_Electricity;
	}
	public void setTEN_Utility_Electricity(Boolean tEN_Utility_Electricity) {
		TEN_Utility_Electricity = tEN_Utility_Electricity;
	}
	public Boolean getTEN_Utility_Miscellaneous() {
		return TEN_Utility_Miscellaneous;
	}
	public void setTEN_Utility_Miscellaneous(Boolean tEN_Utility_Miscellaneous) {
		TEN_Utility_Miscellaneous = tEN_Utility_Miscellaneous;
	}
	public String getTEN_exceptionAddedValue() {
		return TEN_exceptionAddedValue;
	}
	public void setTEN_exceptionAddedValue(String tEN_exceptionAddedValue) {
		TEN_exceptionAddedValue = tEN_exceptionAddedValue;
	}
	public Boolean getTEN_Gas_Electric() {
		return TEN_Gas_Electric;
	}
	public void setTEN_Gas_Electric(Boolean tEN_Gas_Electric) {
		TEN_Gas_Electric = tEN_Gas_Electric;
	}
	public Boolean getTEN_Utility_Gas_Electric() {
		return TEN_Utility_Gas_Electric;
	}
	public void setTEN_Utility_Gas_Electric(Boolean tEN_Utility_Gas_Electric) {
		TEN_Utility_Gas_Electric = tEN_Utility_Gas_Electric;
	}
	public String getTEN_housingRentVatDesc() {
		return TEN_housingRentVatDesc;
	}
	public void setTEN_housingRentVatDesc(String tEN_housingRentVatDesc) {
		TEN_housingRentVatDesc = tEN_housingRentVatDesc;
	}
	public Date getTEN_billThroughDate() {
		return TEN_billThroughDate;
	}
	public void setTEN_billThroughDate(Date tEN_billThroughDate) {
		TEN_billThroughDate = tEN_billThroughDate;
	}
	public String getTEN_housingRentVatPercent() {
		return TEN_housingRentVatPercent;
	}
	public void setTEN_housingRentVatPercent(String tEN_housingRentVatPercent) {
		TEN_housingRentVatPercent = tEN_housingRentVatPercent;
	}
	public String getINS_vendorCode() {
		return INS_vendorCode;
	}
	public void setINS_vendorCode(String iNS_vendorCode) {
		INS_vendorCode = iNS_vendorCode;
	}
	public String getINS_vendorName() {
		return INS_vendorName;
	}
	public void setINS_vendorName(String iNS_vendorName) {
		INS_vendorName = iNS_vendorName;
	}
	public String getINS_vendorCodeEXSO() {
		return INS_vendorCodeEXSO;
	}
	public void setINS_vendorCodeEXSO(String iNS_vendorCodeEXSO) {
		INS_vendorCodeEXSO = iNS_vendorCodeEXSO;
	}
	public String getINS_vendorContact() {
		return INS_vendorContact;
	}
	public void setINS_vendorContact(String iNS_vendorContact) {
		INS_vendorContact = iNS_vendorContact;
	}
	public String getINS_vendorEmail() {
		return INS_vendorEmail;
	}
	public void setINS_vendorEmail(String iNS_vendorEmail) {
		INS_vendorEmail = iNS_vendorEmail;
	}
	public String getINS_comment() {
		return INS_comment;
	}
	public void setINS_comment(String iNS_comment) {
		INS_comment = iNS_comment;
	}
	public Date getINS_vendorInitiation() {
		return INS_vendorInitiation;
	}
	public void setINS_vendorInitiation(Date iNS_vendorInitiation) {
		INS_vendorInitiation = iNS_vendorInitiation;
	}
	public Date getINS_serviceStartDate() {
		return INS_serviceStartDate;
	}
	public void setINS_serviceStartDate(Date iNS_serviceStartDate) {
		INS_serviceStartDate = iNS_serviceStartDate;
	}
	public Date getINS_serviceEndDate() {
		return INS_serviceEndDate;
	}
	public void setINS_serviceEndDate(Date iNS_serviceEndDate) {
		INS_serviceEndDate = iNS_serviceEndDate;
	}
	public String getINP_vendorCode() {
		return INP_vendorCode;
	}
	public void setINP_vendorCode(String iNP_vendorCode) {
		INP_vendorCode = iNP_vendorCode;
	}
	public String getINP_vendorName() {
		return INP_vendorName;
	}
	public void setINP_vendorName(String iNP_vendorName) {
		INP_vendorName = iNP_vendorName;
	}
	public String getINP_vendorCodeEXSO() {
		return INP_vendorCodeEXSO;
	}
	public void setINP_vendorCodeEXSO(String iNP_vendorCodeEXSO) {
		INP_vendorCodeEXSO = iNP_vendorCodeEXSO;
	}
	public String getINP_vendorContact() {
		return INP_vendorContact;
	}
	public void setINP_vendorContact(String iNP_vendorContact) {
		INP_vendorContact = iNP_vendorContact;
	}
	public String getINP_vendorEmail() {
		return INP_vendorEmail;
	}
	public void setINP_vendorEmail(String iNP_vendorEmail) {
		INP_vendorEmail = iNP_vendorEmail;
	}
	public String getINP_comment() {
		return INP_comment;
	}
	public void setINP_comment(String iNP_comment) {
		INP_comment = iNP_comment;
	}
	public Date getINP_vendorInitiation() {
		return INP_vendorInitiation;
	}
	public void setINP_vendorInitiation(Date iNP_vendorInitiation) {
		INP_vendorInitiation = iNP_vendorInitiation;
	}
	public Date getINP_serviceStartDate() {
		return INP_serviceStartDate;
	}
	public void setINP_serviceStartDate(Date iNP_serviceStartDate) {
		INP_serviceStartDate = iNP_serviceStartDate;
	}
	public Date getINP_serviceEndDate() {
		return INP_serviceEndDate;
	}
	public void setINP_serviceEndDate(Date iNP_serviceEndDate) {
		INP_serviceEndDate = iNP_serviceEndDate;
	}
	public String getEDA_vendorCode() {
		return EDA_vendorCode;
	}
	public void setEDA_vendorCode(String eDA_vendorCode) {
		EDA_vendorCode = eDA_vendorCode;
	}
	public String getEDA_vendorName() {
		return EDA_vendorName;
	}
	public void setEDA_vendorName(String eDA_vendorName) {
		EDA_vendorName = eDA_vendorName;
	}
	public String getEDA_vendorCodeEXSO() {
		return EDA_vendorCodeEXSO;
	}
	public void setEDA_vendorCodeEXSO(String eDA_vendorCodeEXSO) {
		EDA_vendorCodeEXSO = eDA_vendorCodeEXSO;
	}
	public String getEDA_vendorContact() {
		return EDA_vendorContact;
	}
	public void setEDA_vendorContact(String eDA_vendorContact) {
		EDA_vendorContact = eDA_vendorContact;
	}
	public String getEDA_vendorEmail() {
		return EDA_vendorEmail;
	}
	public void setEDA_vendorEmail(String eDA_vendorEmail) {
		EDA_vendorEmail = eDA_vendorEmail;
	}
	public String getEDA_comment() {
		return EDA_comment;
	}
	public void setEDA_comment(String eDA_comment) {
		EDA_comment = eDA_comment;
	}
	public Date getEDA_vendorInitiation() {
		return EDA_vendorInitiation;
	}
	public void setEDA_vendorInitiation(Date eDA_vendorInitiation) {
		EDA_vendorInitiation = eDA_vendorInitiation;
	}
	public Date getEDA_serviceStartDate() {
		return EDA_serviceStartDate;
	}
	public void setEDA_serviceStartDate(Date eDA_serviceStartDate) {
		EDA_serviceStartDate = eDA_serviceStartDate;
	}
	public Date getEDA_serviceEndDate() {
		return EDA_serviceEndDate;
	}
	public void setEDA_serviceEndDate(Date eDA_serviceEndDate) {
		EDA_serviceEndDate = eDA_serviceEndDate;
	}
	public Date getSPA_vendorInitiation() {
		return SPA_vendorInitiation;
	}
	public void setSPA_vendorInitiation(Date sPA_vendorInitiation) {
		SPA_vendorInitiation = sPA_vendorInitiation;
	}
	public String getTAS_vendorCode() {
		return TAS_vendorCode;
	}
	public void setTAS_vendorCode(String tAS_vendorCode) {
		TAS_vendorCode = tAS_vendorCode;
	}
	public String getTAS_vendorName() {
		return TAS_vendorName;
	}
	public void setTAS_vendorName(String tAS_vendorName) {
		TAS_vendorName = tAS_vendorName;
	}
	public String getTAS_vendorCodeEXSO() {
		return TAS_vendorCodeEXSO;
	}
	public void setTAS_vendorCodeEXSO(String tAS_vendorCodeEXSO) {
		TAS_vendorCodeEXSO = tAS_vendorCodeEXSO;
	}
	public String getTAS_vendorContact() {
		return TAS_vendorContact;
	}
	public void setTAS_vendorContact(String tAS_vendorContact) {
		TAS_vendorContact = tAS_vendorContact;
	}
	public String getTAS_vendorEmail() {
		return TAS_vendorEmail;
	}
	public void setTAS_vendorEmail(String tAS_vendorEmail) {
		TAS_vendorEmail = tAS_vendorEmail;
	}
	public String getTAS_comment() {
		return TAS_comment;
	}
	public void setTAS_comment(String tAS_comment) {
		TAS_comment = tAS_comment;
	}
	public Date getTAS_vendorInitiation() {
		return TAS_vendorInitiation;
	}
	public void setTAS_vendorInitiation(Date tAS_vendorInitiation) {
		TAS_vendorInitiation = tAS_vendorInitiation;
	}
	public Date getTAS_serviceStartDate() {
		return TAS_serviceStartDate;
	}
	public void setTAS_serviceStartDate(Date tAS_serviceStartDate) {
		TAS_serviceStartDate = tAS_serviceStartDate;
	}
	public Date getTAS_serviceEndDate() {
		return TAS_serviceEndDate;
	}
	public void setTAS_serviceEndDate(Date tAS_serviceEndDate) {
		TAS_serviceEndDate = tAS_serviceEndDate;
	}
	public Date getMMG_vendorInitiation() {
		return MMG_vendorInitiation;
	}
	public void setMMG_vendorInitiation(Date mMG_vendorInitiation) {
		MMG_vendorInitiation = mMG_vendorInitiation;
	}

	public Date getRNT_rsfReceived() {
		return RNT_rsfReceived;
	}
	public void setRNT_rsfReceived(Date rNT_rsfReceived) {
		RNT_rsfReceived = rNT_rsfReceived;
	}
	public Date getRNT_moveInspection() {
		return RNT_moveInspection;
	}
	public void setRNT_moveInspection(Date rNT_moveInspection) {
		RNT_moveInspection = rNT_moveInspection;
	}
	public Integer getTotalDspDays() {
		return totalDspDays;
	}
	public void setTotalDspDays(Integer totalDspDays) {
		this.totalDspDays = totalDspDays;
	}

}