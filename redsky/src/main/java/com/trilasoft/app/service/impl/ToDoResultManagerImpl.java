package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ToDoResultDao;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.service.ToDoResultManager;



public class ToDoResultManagerImpl extends GenericManagerImpl<ToDoResult, Long> implements ToDoResultManager {

	ToDoResultDao toDoResultDao;
	
	public ToDoResultManagerImpl(ToDoResultDao toDoResultDao) {   
        super(toDoResultDao);   
        this.toDoResultDao = toDoResultDao;   
    } 
    
    public List findMaximumId(){
     	return toDoResultDao.findMaximumId(); 
     }

	public void updateToDoResult(String sessionCorpID) {
		toDoResultDao.updateToDoResult(sessionCorpID);
	}
	//13674 - Create dashboard
	public List getTaskList(String filenumber,String sessionCorpID,Long id) {
		return toDoResultDao.getTaskList(filenumber,sessionCorpID, id);
	}
   //End  dashboard
}
