package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="standardaddresses")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class StandardAddresses extends BaseObject implements ListLinkData {
	
	private Long id;
	private String corpID;
	private String job; 
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String addressLine1;
	private String addressLine2;	
	private String addressLine3;
	private String state;		
	private String countryCode;	
	private String zip;			
	private String dayPhone;
	private String homePhone;
	private String faxPhone;
	private String city;
	private String mobPhone;
	private String residenceType;
	private String residenceMgmtName;
	private String residenceDeposit1;
	private String residencePurpose1;
	private String residenceDeposit2;
	private String residencePurpose2;
	private String residencePayType;
	private String residenceNotes;
	private BigDecimal depositAmount1 = new BigDecimal(0);
	private BigDecimal depositAmount2 = new BigDecimal(0);
	private String email;
	private String contactPersonEmail;
	private String contactPerson;
	private String contactPersonPhone;
	
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof StandardAddresses))
			return false;
		StandardAddresses castOther = (StandardAddresses) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(job, castOther.job).append(createdBy,
				castOther.createdBy).append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(addressLine1,
						castOther.addressLine1).append(addressLine2,
						castOther.addressLine2).append(addressLine3,
						castOther.addressLine3).append(state, castOther.state)
				.append(countryCode, castOther.countryCode).append(zip,
						castOther.zip).append(dayPhone, castOther.dayPhone)
				.append(homePhone, castOther.homePhone).append(faxPhone,
						castOther.faxPhone).append(city, castOther.city)
				.append(residenceMgmtName, castOther.residenceMgmtName).append(residenceDeposit1, castOther.residenceDeposit1)
				.append(residencePurpose1, castOther.residencePurpose1).append(residenceDeposit2, castOther.residenceDeposit2)
				.append(residencePurpose2, castOther.residencePurpose2).append(residencePayType, castOther.residencePayType)
				.append(residenceNotes, castOther.residenceNotes).append(residenceType, castOther.residenceType)
				.append(depositAmount1, castOther.depositAmount1).append(depositAmount2, castOther.depositAmount2)
				.append(email, castOther.email).append(contactPersonEmail, castOther.contactPersonEmail)
				.append(contactPerson, castOther.contactPerson).append(contactPersonPhone, castOther.contactPersonPhone)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(job)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(addressLine1).append(addressLine2)
				.append(addressLine3).append(state).append(countryCode).append(
						zip).append(dayPhone).append(homePhone)
				.append(faxPhone).append(city)
				.append(residenceType).append(residenceMgmtName).append(residenceDeposit1)
				.append(residencePurpose1).append(residenceDeposit2).append(residencePurpose2)
				.append(residencePayType).append(residenceNotes)
				.append(depositAmount1).append(depositAmount2)
				.append(email).append(contactPersonEmail)
				.append(contactPerson).append(contactPersonPhone)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("job", job).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("addressLine1",
						addressLine1).append("addressLine2", addressLine2)
				.append("addressLine3", addressLine3).append("state", state)
				.append("countryCode", countryCode).append("zip", zip).append(
						"dayPhone", dayPhone).append("homePhone", homePhone)
				.append("faxPhone", faxPhone).append("city", city)
				.append("residenceType", residenceType).append("residenceMgmtName", residenceMgmtName)
				.append("residenceDeposit1", residenceDeposit1).append("residencePurpose1", residencePurpose1)
				.append("residenceDeposit2", residenceDeposit2).append("residencePurpose2", residencePurpose2)
				.append("residencePayType", residencePayType).append("residenceNotes", residenceNotes)
				.append("depositAmount1", depositAmount1).append("depositAmount2", depositAmount2)
				.append("email", email).append("contactPersonEmail", contactPersonEmail)
				.append("contactPerson", contactPerson).append("contactPersonPhone", contactPersonPhone)
				.toString();
	}

	@Column(length=50)
	public String getAddressLine1() {
		return addressLine1;
	}
	
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	@Column(length=50)
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	@Column(length=50)
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	@Column(length=30)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=200)
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDayPhone() {
		return dayPhone;
	}
	public void setDayPhone(String dayPhone) {
		this.dayPhone = dayPhone;
	}
	@Column(length=20)
	public String getFaxPhone() {
		return faxPhone;
	}
	public void setFaxPhone(String faxPhone) {
		this.faxPhone = faxPhone;
	}
	@Column
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=3)
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	@Column
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=10)
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	@Transient
	public String getListCode() {
		String addressLineA ="";
		if(addressLine1==null){
			return addressLineA;
		}
		else return this.addressLine1;
	}

	@Transient
	public String getListDescription() {
		String addressLineB ="";
		if(addressLine2==null){
			return addressLineB;
		}
		else return addressLine2;
		
	}

	@Transient
	public String getListSecondDescription() {
		
		String addressLineC ="";
		if(addressLine3==null){
			return addressLineC;
		}
		else return addressLine3;
	}

	@Transient
	public String getListThirdDescription() {
		String ConCode ="";
		if(countryCode==null){
			return ConCode;
		}
		else return countryCode;
	}

	@Transient
	public String getListFourthDescription() {
		String stateCode ="";
		if(state==null){
			return stateCode;
		}
		else return state;
	}

	@Transient
	public String getListFifthDescription() {
		
		String zipT ="";
		if(zip==null){
			return zipT;
		}
		else return zip;
	}

	@Transient
	public String getListSixthDescription() {
		
		String dayPhNo ="";
		if(dayPhone==null){
			return dayPhNo;
		}
		return dayPhone;

	}
	@Transient
	public String getListSeventhDescription() {
		String homePhNo ="";
		if(homePhone==null){
			return homePhNo;
		}
		return homePhone;
	}
	@Transient
	public String getListEigthDescription() {
		String faxPhNo ="";
		if(faxPhone==null){
			return faxPhNo;
		}
		return faxPhone;
	}
	@Transient
	public String getListNinthDescription() {
		String city1="";
       if(this.city==null) 
		return city1;
       else return this.city;
	}
	@Column(length=20)
	public String getMobPhone() {
		return mobPhone;
	}

	public void setMobPhone(String mobPhone) {
		this.mobPhone = mobPhone;
	}
	@Transient
	public String getListTenthDescription() {
		String mobPhNo ="";
		if(mobPhone==null){
			return mobPhNo;
		}
		return mobPhone;
	}

	public String getResidenceType() {
		return residenceType;
	}

	public void setResidenceType(String residenceType) {
		this.residenceType = residenceType;
	}

	public String getResidenceMgmtName() {
		return residenceMgmtName;
	}

	public void setResidenceMgmtName(String residenceMgmtName) {
		this.residenceMgmtName = residenceMgmtName;
	}

	public String getResidenceDeposit1() {
		return residenceDeposit1;
	}

	public void setResidenceDeposit1(String residenceDeposit1) {
		this.residenceDeposit1 = residenceDeposit1;
	}

	public String getResidencePurpose1() {
		return residencePurpose1;
	}

	public void setResidencePurpose1(String residencePurpose1) {
		this.residencePurpose1 = residencePurpose1;
	}

	public String getResidenceDeposit2() {
		return residenceDeposit2;
	}

	public void setResidenceDeposit2(String residenceDeposit2) {
		this.residenceDeposit2 = residenceDeposit2;
	}

	public String getResidencePurpose2() {
		return residencePurpose2;
	}

	public void setResidencePurpose2(String residencePurpose2) {
		this.residencePurpose2 = residencePurpose2;
	}

	public String getResidencePayType() {
		return residencePayType;
	}

	public void setResidencePayType(String residencePayType) {
		this.residencePayType = residencePayType;
	}

	public String getResidenceNotes() {
		return residenceNotes;
	}

	public void setResidenceNotes(String residenceNotes) {
		this.residenceNotes = residenceNotes;
	}

	public BigDecimal getDepositAmount1() {
		return depositAmount1;
	}

	public void setDepositAmount1(BigDecimal depositAmount1) {
		this.depositAmount1 = depositAmount1;
	}

	public BigDecimal getDepositAmount2() {
		return depositAmount2;
	}

	public void setDepositAmount2(BigDecimal depositAmount2) {
		this.depositAmount2 = depositAmount2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonPhone() {
		return contactPersonPhone;
	}

	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}
	
}