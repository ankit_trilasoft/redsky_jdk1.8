package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ProfileInfoDao;
import com.trilasoft.app.model.ProfileInfo;
import com.trilasoft.app.service.ProfileInfoManager;

public class ProfileInfoManagerImpl extends GenericManagerImpl<ProfileInfo, Long> implements ProfileInfoManager {   
	ProfileInfoDao profileInfoDao;   
	  
    public ProfileInfoManagerImpl(ProfileInfoDao profileInfoDao) {   
        super(profileInfoDao);   
        this.profileInfoDao = profileInfoDao;   
    }
    
    public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode) {
		return profileInfoDao.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode);
	}

}
