package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TaskPlanning;


public interface TaskPlanningManager extends GenericManager<TaskPlanning, Long>{
	public List<TaskPlanning> findTaskPlanningList(String corpId,Long id);
	
}
