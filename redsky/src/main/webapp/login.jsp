<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="login.title"/></title>
    <meta name="heading" content="<fmt:message key='login.heading'/>"/>
    <meta name="menu" content="Login"/>    
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
    <link rel="shortcut icon" href="<c:url value="/images/favicon2.ico"/>" type="image/x-icon" />
   <style>
	body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}   
	
	#leftcolumn{ margin-top:60px !important;}
	    
	div#branding {float: left; width: 100%; margin: 0;text-align: left;height:0px;} 
	
	#wrapper {margin:0 auto; width:950px;}  
	     
	div#footer div#divider div {border-top:5px solid #EBF5FC; margin:1px 0 0; }
	
	#logincontainer{ width:915px; height:376px;!height:370px; background-image:url(images/loginbg-final.gif); background-repeat:no-repeat; margin:0px auto; margin-top:10px;}
	
	div.messageSussess {background: none repeat scroll 0 0 rgb(179, 218, 244) !important; border: 1px solid rgb(0, 0, 0) !important;border-radius: 0.3em !important;color: rgb(0, 0, 0);font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;font-weight: normal;margin: 0 auto !important;margin-top:3px;padding: 5px;text-align: center; vertical-align: bottom;width: 400px !important;z-index: 1000;}
	
	#errormessage {width:300px; list-style:none;margin:0px auto;}

	img.validationWarning, div.error img.icon, div.message img.icon, li.error img.icon {
	 background:transparent none repeat scroll 0 0 !important; border:0 none !important; margin-left:3px; padding-bottom:1px;
	 vertical-align:middle; width:14px; height:14px;}
 
	.cssbutton {width: 55px; height: 25px; padding:0px; font-family:'arial'; font-weight:normal; color:#000;font-size:12px;
	 background:url(/redsky/images/btn_bg.gif) repeat-x; background-color:#e7e7e7;border:1px solid #cccccc; }	
	.cssbutton:hover {color: black; background:url(/redsky/images/btn_hoverbg.gif) repeat-x;background-color: #c1c2bf;}
	
	.textbox_username {background-color:#E7E7E7;border:1px solid #B2B2B2; color:#616161;font-family:Arial,Helvetica,sans-serif;
     font-size:12px; width:135px;background: #E7E7E7 url('images/user.png') no-repeat; background-position: 1 1;padding-left: 19px; }
	.textbox_password {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
	background: #E7E7E7 url('images/icon_password.png') no-repeat;background-position: 1 1;padding-left: 19px;}

	div.message{background: none repeat scroll 0 50% #F5DFDF !important; border: 1px solid #CE9E9E !important;
    border-radius: 0.3em 0.3em 0.3em 0.3em !important;color: #000000;font-family: Arial,Helvetica,sans-serif;
    font-size: 1em;font-weight: normal;padding: 5px;text-align: center;vertical-align: bottom; /* width: 22.5% !important;*/
    width: 287px !important; margin:0px auto !important; margin-top:3px; z-index: 1000; display:none;}

	div.error, span.error, li.error {color:#000000;font-family:Arial,Helvetica,sans-serif; font-weight:normal;text-align:center;
	vertical-align:bottom; z-index: 1000;font-size: 1em;}

	form li.error{ background-color:  #F5DFDF !important; border: 1px solid #F5ACA6 !important;border-radius: 0.3em 0.3em 0.3em 0.3em;
    color: #000000; margin: 2px 0 !important;padding: 1.5px !important;width: 294px;text-align:center !important; }

	#inlogo {margin-right: 0;margin-top: -1px;padding: 0;text-align: right;width:87px;height:89px;float:right;background-image: url("images/IN-Icon.gif");
    background-repeat: no-repeat; }
 </style>
  </head>  
	<script>
    function clear_fileds(){
       	document.forms['loginForm'].elements['j_username'].value = "";
    	document.forms['loginForm'].elements['j_password'].value = "";
    }
    
    </script>
    <script language="javascript" type="text/javascript">
 	function forgotPassword()
 	{
 	document.forms['userOTPForm'].elements['OTPusername'].value = document.forms['loginForm'].elements['j_username'].value
 	location.href= "findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true";
	document.forms['userOTPForm'].submit();
 		
 	}
 	
 	function openPartnerRequest(){
 	var sessionCorpID=document.forms['loginForm'].elements['corpID'].value;
	location.href="partnerRequestAccess.html?decorator=popup&popup=true&bypass_sessionfilter=YES&sessionCorpID="+sessionCorpID;	
}
 </script>
 
 <script>
window.onload = function() {
	var scrollOne = new tScroll();
	scrollOne.init('Scroller');

}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.5});
}
</script>
<script language="javascript" type="text/javascript">
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
var today = new Date(); 
var expiry = new Date(today.getTime() + 505000); 
function setCookie(name, value) { 
	document.cookie=name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString(); 
}
function loginStatusCheck(){
	var soDiv = document.getElementById("successMessages");
	soDiv.style.display = 'none';
	var userNameStatus=getCookie('username');
	//var currentPassword = getCookie('currentPassword');
	var url = "updateSucFail.html?ajax=1&decorator=simple&popup=true&bypass_sessionfilter=YES&userNameStatus="+encodeURIComponent(userNameStatus);   
	http.open("GET", url, true);
	http.onreadystatechange = function(){  handleHttpResponseUserAuthenticate(userNameStatus);};
	http.send(null);
}
function handleHttpResponseUserAuthenticate(userNameStatus){
 if (http.readyState == 4){
   var results = http.responseText
  
   results = results.trim();
   if(results.length>0){
	  
	   var soDiv = document.getElementById("successMessages");
	   soDiv.style.display = 'block';
	   soDiv.innerHTML = results;
         } }  } 
var http = getHTTPObject();
function getHTTPObject() {
var xmlhttp;
if(window.XMLHttpRequest) {
xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject) {
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
if (!xmlhttp) {
    xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
}
}
return xmlhttp;
}
 </script>

<body id="login"/ style="text-align:center">

 

<%@page language="java" import="java.io.*"%>
<form method="post" id="loginForm" name="loginForm" action="<c:url value='/j_security_check'/>"
    onsubmit="saveUsername(this); return validateForm(this),submit_form();">
<c:set var="corpID" value="<%=request.getParameter("corpID") %>" />
<s:hidden name="corpID" value="<%=request.getParameter("corpID") %>" />
<c:if test="${param.error != null}">
   <div class="message" id="successMessages"></div>
  </c:if>
  <c:if test="${globalMsg != null && globalMsg!=''}">   
   <div class="messageSussess" id="successMessages"><img class="icon" style="vertical-align:top;" alt="Information" src="${pageContext.request.contextPath}/images/iconInformation.gif">&nbsp;${globalMsg}</div>
   
<%
    application.setAttribute("globalMsg", "");
%>
   
  </c:if>
  <div id="errormessage">
  <c:if test="${restrictLoginMsg != null && restrictLoginMsg!=''}">   
   <li class="error" style ="font-size:11px"><img class="icon" style="vertical-align:top;" alt="<fmt:message key='icon.warning'/>" src="${ctx}/images/logerror.png">&nbsp;${restrictLoginMsg}</li>
		<%
		    application.setAttribute("restrictLoginMsg", "");
		%>
  </c:if>
  
    
<c:if test="${param.error != null}">
    <li class="error">
        <img src="${ctx}/images/logerror.png" alt="<fmt:message key='icon.warning'/>" class="icon"/>
        <fmt:message key="errors.password.mismatch"/>
        <%--${sessionScope.ACEGI_SECURITY_LAST_EXCEPTION.message}--%>
    </li>
</c:if>
<c:if test="${param.companySystemDefaultError != null}">
    <li class="error">
        <img src="${ctx}/images/logerror.png" alt="<fmt:message key='icon.warning'/>" class="icon"/>
        <fmt:message key="errors.companySystemDefault.mismatch"/>
        <%--${sessionScope.ACEGI_SECURITY_LAST_EXCEPTION.message}--%>
    </li>
</c:if>
</div>
<div id="wrapper" style="margin:0px auto; text-align:center;">

<div id="logincontainer" style="margin-bottom:1px;" >
<div style="margin: 0px; padding: 0px; width: 892px; float: left; position: relative; clear: both;">
<div id="indexlogo"><img src="${pageContext.request.contextPath}/images/title.gif"  style="cursor: default;"/></div>
<div id="inlogo">
<div id="inTiny"><a href="https://www.linkedin.com/company/redsky-new-features?trk=biz-brand-tree-co-name" target="_blank"><img src="${pageContext.request.contextPath}/images/in16.gif" style="cursor: inherit;border:none;"/></a></div>
</div>
</div>
<div id="marquee"><%
     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/clients.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }
%></div>
<div id="leftcolumn">
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">  
  <td style="!padding-left:24px;" align="left" ></td>
    <tr>    
      <%-- <td align="left"><img src="${pageContext.request.contextPath}/images/title.gif"  style="cursor: default;"/> --%></td>
    </tr>
     <tr>
     <td><%
     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/RSuserMessage.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }
%></td>
      <td align="left" class="content">&nbsp;</td>
    </tr>
  </table>
  <table border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:65px">
  <tr>
  <td style="">&nbsp;</td>
  </tr>
  
  <tr>
  <td style="width:26px;">&nbsp;</td>
  <td style="!padding-left:24px;" align="left">
  <!--<a href="//privacy.truste.com/privacy-seal/RedSky-Mobility-Solutions,-LLC/validation?rid=3470f34c-05bb-4946-96d8-a87d03b1cb29" title="Validate TRUSTe privacy certification" target="_blank">
  <img src="${pageContext.request.contextPath}/images/TRUSTeSeal.png" border="0" alt="Validate TRUSTe privacy certification" />
  </a>
  
  -->
  
  </td>
  </tr>
  </table>
</div>

<div id="rightcolumn" style="text-align:left;">
  <table width="285" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="60" class="content">Username</td>
      <td colspan="2"><label>
        <input type="text" class="textbox_username" name="j_username" id="j_username" tabindex="1" />
      </label></td>
    </tr>
    <tr>
      <td height="15px">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content">Password</td>
      <td colspan="2"><input type="password" class="textbox_password" name="j_password" id="j_password" tabindex="2" />
    </tr>
    <tr>
      <td colspan="3" class="content" height="5"></td>
      </tr>
      <c:if test="${appConfig['rememberMeEnabled']}">
    <tr>
      <td class="content">&nbsp;</td>
      <td width="22" class="content"><input type="checkbox" class="checkbox" name="rememberMe" id="rememberMe" tabindex="3"/></td>
      <td width="170" class="content" style="padding-top:0px; !padding-top:9px;"><label for="rememberMe"><fmt:message key="login.rememberMe"/></label></td>
    
    </tr>
    </c:if> 
    <tr>
      <td class="content" height="20px">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2"><input type="submit" class="cssbutton" name="login" value="<fmt:message key='button.login'/>" tabindex="4" style="">&nbsp;<input type="button" class="cssbutton" name="Clear" value="Clear" onclick="clear_fileds();" style=""></td>
    </tr>    
   <tr>     
       <td colspan="2" style="height:20px;!height:0px;"></td>
    </tr>
    <tr>     
       <td colspan="2" style="height:20px;!height:0px;"></td>
    </tr>
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
         <tr>
        <td align="center"><img src="${pageContext.request.contextPath}/images/helpicon.gif" width="26" height="23" /></td>
        <td class="formtext" colspan="2" align="left" style="padding-left: 4px;color: #71b9c3;">
          <script language="javascript">

           document.write('<a href="javascript:forgotPassword()" class="otherlinks" >Forgot Password</a>');

                      </script>
                     
                     
                      
                      </td></tr>  
        <%-- <tr>
          <td align="center"><img src="${pageContext.request.contextPath}/images/helpicon.gif" width="26" height="23" /></td>
          <td align="left"><a href="signup.html?target=upwd&bypass_sessionfilter=YES" class="otherlinks">Forgot username and Password</a></td>
        </tr> --%>
        <tr>
          <td align="center"><img src="${pageContext.request.contextPath}/images/helpicon.gif" width="26" height="23" /></td>
          <td align="left"><a href="partnerRequestAccess.html?bypass_sessionfilter=YES&sessionCorpID=TSFT&decorator=popup&popup=true" class="otherlinks">Request Agent Portal Access</a></td>
        </tr>
        <!--<tr>
        <td align="center"><img src="${pageContext.request.contextPath}/images/helpicon.gif" width="26" height="23" /></td>
      <td align="left" class="content" style="font-size:11px;"><a href="http://redskymobility.com/redskyforums/phpBB3/ucp.php?mode=login" target="_blank"><font color="#fe000c">Red</font><font color="#1f1a17">Sky Forums</font></a></td>
    </tr>
        <tr>
        <td align="center"><img src="${pageContext.request.contextPath}/images/helpicon.gif" width="26" height="23" /></td>
        <td align="left"><a href="javascript:alert('Your screen resolution is '+screen.width+'x'+screen.height);" class="otherlinks">Click for your screen resolution</a>
        </td>
        </tr>-->
      </table></td>
    </tr>
    <tr><!--
    <c:if test="${not empty corpID}">
    <td colspan="3">
    	<input type="button" class="cssbutton1" style="width: 150px" value="Request Partner Access" onclick="openPartnerRequest();"/>
    </td>
    </c:if>
    --></tr>
  </table>
  
     </div>
     </div>   
  <!-- Modified By Arbind at 16-Feb-2012 -->   
      
 
 <%@ include file="newclient.jsp" %>
  <!-- end 16-Feb-2012 --> 
     </div>
 </form>
 <script type="text/javascript">
<!--
function popup(url) 
{
 var width  = 850;
 var height = 800;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}
// -->
</script>
<script type="text/javascript">   
    document.forms['loginForm'].elements['j_username'].focus();  
    
</script> 
<c:if test="${param.error != null}">
<script language="javascript" type="text/javascript">

</script>
</c:if>
<s:form name="userOTPForm" action="findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true" method="post" validate="true" >
<input type="hidden" class="textbox" id="OTPusername" name="OTPusername" tabindex="1"  value=""/>
<input type="hidden" class="textbox" id="bypass_sessionfilter" name="bypass_sessionfilter"   value="YES"/>
</s:form>    
<%@ include file="/scripts/login.js"%>