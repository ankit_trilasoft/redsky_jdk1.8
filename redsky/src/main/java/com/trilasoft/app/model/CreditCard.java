package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="creditcard")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class CreditCard extends BaseObject  {

	private Long id;
	private String corpID;
	private String shipNumber;
    private Date ccApproved;
    private BigDecimal ccApprovedBillAmount;
    private String ccAuthorizationBillNumber;     
    private String ccExpires;
    private String ccName;
    private String ccNumber;
    private String ccNumber1;
    private String ccType;
    private Date ccVlTransDate;
    
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String securityCodeNo;
	private String expireYear;
	private String expireMonth;
	private Boolean primaryFlag;
	private Date authorizationDate;
    private Date expiryDate;	
	
	
	private Billing billing;
//	For Mapping 
	//private Billing billing;
    
 
	
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("shipNumber", shipNumber).append("ccApproved",
				ccApproved)
				.append("ccApprovedBillAmount", ccApprovedBillAmount).append(
						"ccAuthorizationBillNumber", ccAuthorizationBillNumber)
				.append("ccExpires", ccExpires).append("ccName", ccName)
				.append("ccNumber", ccNumber).append("ccNumber1", ccNumber1)
				.append("ccType", ccType)
				.append("ccVlTransDate", ccVlTransDate).append("createdBy",
						createdBy).append("updatedBy", updatedBy).append(
						"createdOn", createdOn).append("updatedOn", updatedOn)
				.append("securityCodeNo", securityCodeNo).append("expireYear",
						expireYear).append("expireMonth", expireMonth).append(
						"primaryFlag", primaryFlag)
						.append("authorizationDate", authorizationDate)
						.append("expiryDate", expiryDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CreditCard))
			return false;
		CreditCard castOther = (CreditCard) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(shipNumber, castOther.shipNumber)
				.append(ccApproved, castOther.ccApproved).append(
						ccApprovedBillAmount, castOther.ccApprovedBillAmount)
				.append(ccAuthorizationBillNumber,
						castOther.ccAuthorizationBillNumber).append(ccExpires,
						castOther.ccExpires).append(ccName, castOther.ccName)
				.append(ccNumber, castOther.ccNumber).append(ccNumber1,
						castOther.ccNumber1).append(ccType, castOther.ccType)
				.append(ccVlTransDate, castOther.ccVlTransDate).append(
						createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(securityCodeNo,
						castOther.securityCodeNo).append(expireYear,
						castOther.expireYear).append(expireMonth,
						castOther.expireMonth).append(primaryFlag,
						castOther.primaryFlag).append(authorizationDate,
						castOther.authorizationDate)
						.append(expiryDate,castOther.expiryDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				shipNumber).append(ccApproved).append(ccApprovedBillAmount)
				.append(ccAuthorizationBillNumber).append(ccExpires).append(
						ccName).append(ccNumber).append(ccNumber1).append(
						ccType).append(ccVlTransDate).append(createdBy).append(
						updatedBy).append(createdOn).append(updatedOn).append(
						securityCodeNo).append(expireYear).append(expireMonth)
				.append(primaryFlag).append(authorizationDate).append(expiryDate).toHashCode();
	}
	//@Column(length=9, insertable=false, updatable=false)
	
	@ManyToOne
	@JoinColumn(name="shipNumber", nullable=false, updatable=false,
			referencedColumnName="shipNumber")
			public Billing getBilling() {
			        return billing;
			    }
			    
				 public void setBilling(Billing billing) {
						this.billing = billing;
				}  
    
	
	@Column(length=15, insertable=false, updatable=false)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	@Column
	public Date getCcApproved() {
		return ccApproved;
	}
	public void setCcApproved(Date ccApproved) {
		this.ccApproved = ccApproved;
	}
	
	@Column(length=11)
	public BigDecimal getCcApprovedBillAmount() {
		return ccApprovedBillAmount;
	}
	public void setCcApprovedBillAmount(BigDecimal ccApprovedBillAmount) {
		this.ccApprovedBillAmount = ccApprovedBillAmount;
	}
	
	@Column(length=20)
	public String getCcAuthorizationBillNumber() {
		return ccAuthorizationBillNumber;
	}
	public void setCcAuthorizationBillNumber(String ccAuthorizationBillNumber) {
		this.ccAuthorizationBillNumber = ccAuthorizationBillNumber;
	}
	
	
	
	@Column(length=45)
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	
	@Column(length=20)
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	
	@Column(length=10)
	public String getCcType() {
		return ccType;
	}
	public void setCcType(String ccType) {
		this.ccType = ccType;
	}
	
	@Column
	public Date getCcVlTransDate() {
		return ccVlTransDate;
	}
	public void setCcVlTransDate(Date ccVlTransDate) {
		this.ccVlTransDate = ccVlTransDate;
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
    
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=25)
	public String getSecurityCodeNo() {
		return securityCodeNo;
	}
	public void setSecurityCodeNo(String securityCodeNo) {
		this.securityCodeNo = securityCodeNo;
	}
	@Column(length=5)
	public String getCcExpires() {
		return ccExpires;
	}
	public void setCcExpires(String ccExpires) {
		this.ccExpires = ccExpires;
	}
	@Column(length=4)
	public String getExpireYear() {
		return expireYear;
	}
	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}
	@Column(length=4)
	public String getExpireMonth() {
		return expireMonth;
	}
	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}
	@Column
	public Boolean getPrimaryFlag() {
		return primaryFlag;
	}
	public void setPrimaryFlag(Boolean primaryFlag) {
		this.primaryFlag = primaryFlag;
	}
	@Column(length=20)
	public String getCcNumber1() {
		return ccNumber1;
	}
	public void setCcNumber1(String ccNumber1) {
		this.ccNumber1 = ccNumber1;
	}
	@Column
	public Date getAuthorizationDate() {
		return authorizationDate;
	}
	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}
	@Column
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	
	
	

	
}
