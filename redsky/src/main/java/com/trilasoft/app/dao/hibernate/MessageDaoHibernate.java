package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.MessageDao;
import com.trilasoft.app.model.MyMessage;

public class MessageDaoHibernate extends GenericDaoHibernate<MyMessage, Long> implements MessageDao {   
	private List a;  
    public MessageDaoHibernate() {   
        super(MyMessage.class);   
    }   
  
    public List<MyMessage> findByToUser(String toUser) {   
        return getHibernateTemplate().find("from MyMessage where toUser=?", toUser);   
    }   
    
    public List<MyMessage> findByFromUser(String fromUser) {  
    	String statuss = "Sent";
        return getHibernateTemplate().find("from MyMessage where toUser='"+ fromUser+"' AND status not in('" +statuss+"')");   
    }   

    public List<MyMessage> findByUnreadMessages(String fromUser) {   
    	String status = "UnRead" ;
        return getHibernateTemplate().find("Select count(*) from MyMessage where status='" + status + "' AND toUser=?",fromUser);   
    }
    
    public List<MyMessage> findByOutbox(String status, String loggedInUser){
    	//System.out.println("Select count(*) from MyMessage where status='" + status + "' AND fromUser not in('" +loggedInUser+ "')");
    	//a = getHibernateTemplate().find("from MyMessage where status='" + status + "' AND fromUser not in('" +loggedInUser+"')");
    	a = getHibernateTemplate().find("from MyMessage where status='" + status + "' AND fromUser=?", loggedInUser);
    	//System.out.println(a);
    	return a;
    }
    
    public int updateFwdStatus(Long noteId){
    	return getHibernateTemplate().bulkUpdate("update Notes set forwardStatus='READ' where id=?", noteId);
    }
   /* 
    public int remindMessage() {
    	//return getHibernateTemplate().bulkUpdate("update Receivable set invoiceNumber="+newInvoiceNumber+",invoiceDate=Now() where shipNumber="+shipNumber+" and invoiceNumber='' and amount <> 0");
    	return getHibernateTemplate().bulkUpdate("Insert into MyMessage ('Message') VALUES ('Hurrey') ");
    }*/
}  
