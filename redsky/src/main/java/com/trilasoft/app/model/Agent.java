package com.trilasoft.app.model;

import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="agent")
public class Agent extends BaseObject{
	private String corpID;
	private Long id;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String bookingCode;
	private String bookingCoord;
	private String originAgentCode;
	private String originAgentCoord;
	private String destinationAgentCode;
	private String destinationAgentCoord;
	private String freightForworderCode;
	private String freightForworderCoord;
	private String bookingEmail;
	private String originAgentEmail;
	private String destinationAgentEmail;
	private String freightForworderEmail;
	private String tranship;
	private String thrdCode;
	private String thrdRef;
	private String thrdEmail;
	private String thrdCoord;
	private String setoffCode;
	private String pickupCode;
	private String deliveryCode;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	private String bookingCity;
	private String pickupDesc;
	private String bookingCountry;
	private String bookingAgentFirstName;
	private String bookingAgentLastName;
	
	private String originAgentFirstName;
	private String originAgentLastName;
	
	private String originAgentCity;
	private String setoffDesc;
	private String originAgentCountry;
	
	private String destinationAgentFirstName;
	private String destinationAgentLastName;
	
	
	
	private String destinationAgentCity;
	private String deliveryDesc;
	private String destinationAgentCountry;
	
	
	
	private String freightForworderCity;
	private String freightForworderState;
	private String freightForworderCountry;
	
	
	private String freightForworderFirstName;
	private String freightForworderLastName;
	
	private ServiceOrder serviceOrder;
	
	private Set<ServicePartner> servicePartners;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("sequenceNumber", sequenceNumber).append("ship",
				ship).append("shipNumber", shipNumber).append("bookingCode",
				bookingCode).append("bookingCoord", bookingCoord).append(
				"originAgentCode", originAgentCode).append("originAgentCoord",
				originAgentCoord).append("destinationAgentCode",
				destinationAgentCode).append("destinationAgentCoord",
				destinationAgentCoord).append("freightForworderCode",
				freightForworderCode).append("freightForworderCoord",
				freightForworderCoord).append("bookingEmail", bookingEmail)
				.append("originAgentEmail", originAgentEmail).append(
						"destinationAgentEmail", destinationAgentEmail).append(
						"freightForworderEmail", freightForworderEmail).append(
						"tranship", tranship).append("thrdCode", thrdCode)
				.append("thrdRef", thrdRef).append("thrdEmail", thrdEmail)
				.append("thrdCoord", thrdCoord)
				.append("setoffCode", setoffCode).append("pickupCode",
						pickupCode).append("deliveryCode", deliveryCode)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("bookingCity", bookingCity).append("pickupDesc",
						pickupDesc).append("bookingCountry", bookingCountry)
				.append("bookingAgentFirstName", bookingAgentFirstName).append(
						"bookingAgentLastName", bookingAgentLastName).append(
						"originAgentFirstName", originAgentFirstName).append(
						"originAgentLastName", originAgentLastName).append(
						"originAgentCity", originAgentCity).append(
						"setoffDesc", setoffDesc).append(
						"originAgentCountry", originAgentCountry).append(
						"destinationAgentFirstName", destinationAgentFirstName)
				.append("destinationAgentLastName", destinationAgentLastName)
				.append("destinationAgentCity", destinationAgentCity).append(
						"deliveryDesc", deliveryDesc).append(
						"destinationAgentCountry", destinationAgentCountry)
				.append("freightForworderCity", freightForworderCity).append(
						"freightForworderState", freightForworderState).append(
						"freightForworderCountry", freightForworderCountry)
				.append("freightForworderFirstName", freightForworderFirstName)
				.append("freightForworderLastName", freightForworderLastName)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Agent))
			return false;
		Agent castOther = (Agent) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship).append(shipNumber,
						castOther.shipNumber).append(bookingCode,
						castOther.bookingCode).append(bookingCoord,
						castOther.bookingCoord).append(originAgentCode,
						castOther.originAgentCode).append(originAgentCoord,
						castOther.originAgentCoord).append(
						destinationAgentCode, castOther.destinationAgentCode)
				.append(destinationAgentCoord, castOther.destinationAgentCoord)
				.append(freightForworderCode, castOther.freightForworderCode)
				.append(freightForworderCoord, castOther.freightForworderCoord)
				.append(bookingEmail, castOther.bookingEmail).append(
						originAgentEmail, castOther.originAgentEmail).append(
						destinationAgentEmail, castOther.destinationAgentEmail)
				.append(freightForworderEmail, castOther.freightForworderEmail)
				.append(tranship, castOther.tranship).append(thrdCode,
						castOther.thrdCode).append(thrdRef, castOther.thrdRef)
				.append(thrdEmail, castOther.thrdEmail).append(thrdCoord,
						castOther.thrdCoord).append(setoffCode,
						castOther.setoffCode).append(pickupCode,
						castOther.pickupCode).append(deliveryCode,
						castOther.deliveryCode).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(bookingCity,
						castOther.bookingCity).append(pickupDesc,
						castOther.pickupDesc).append(bookingCountry,
						castOther.bookingCountry).append(bookingAgentFirstName,
						castOther.bookingAgentFirstName).append(
						bookingAgentLastName, castOther.bookingAgentLastName)
				.append(originAgentFirstName, castOther.originAgentFirstName)
				.append(originAgentLastName, castOther.originAgentLastName)
				.append(originAgentCity, castOther.originAgentCity).append(
						setoffDesc, castOther.setoffDesc).append(
						originAgentCountry, castOther.originAgentCountry)
				.append(destinationAgentFirstName,
						castOther.destinationAgentFirstName).append(
						destinationAgentLastName,
						castOther.destinationAgentLastName).append(
						destinationAgentCity, castOther.destinationAgentCity)
				.append(deliveryDesc, castOther.deliveryDesc)
				.append(destinationAgentCountry,
						castOther.destinationAgentCountry).append(
						freightForworderCity, castOther.freightForworderCity)
				.append(freightForworderState, castOther.freightForworderState)
				.append(freightForworderCountry,
						castOther.freightForworderCountry).append(
						freightForworderFirstName,
						castOther.freightForworderFirstName).append(
						freightForworderLastName,
						castOther.freightForworderLastName).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				sequenceNumber).append(ship).append(shipNumber).append(
				bookingCode).append(bookingCoord).append(originAgentCode)
				.append(originAgentCoord).append(destinationAgentCode).append(
						destinationAgentCoord).append(freightForworderCode)
				.append(freightForworderCoord).append(bookingEmail).append(
						originAgentEmail).append(destinationAgentEmail).append(
						freightForworderEmail).append(tranship)
				.append(thrdCode).append(thrdRef).append(thrdEmail).append(
						thrdCoord).append(setoffCode).append(pickupCode)
				.append(deliveryCode).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(bookingCity)
				.append(pickupDesc).append(bookingCountry).append(
						bookingAgentFirstName).append(bookingAgentLastName)
				.append(originAgentFirstName).append(originAgentLastName)
				.append(originAgentCity).append(setoffDesc).append(
						originAgentCountry).append(destinationAgentFirstName)
				.append(destinationAgentLastName).append(destinationAgentCity)
				.append(deliveryDesc).append(destinationAgentCountry)
				.append(freightForworderCity).append(freightForworderState)
				.append(freightForworderCountry).append(
						freightForworderFirstName).append(
						freightForworderLastName).toHashCode();
	}
/*	
	@OneToOne
    @JoinColumn(name="shipNumber",
    referencedColumnName="shipNumber", insertable=false, updatable=false)
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }

	public void setServiceOrder(ServiceOrder serviceOrder) {
			this.serviceOrder = serviceOrder;
	 } 
	*/
/*	@OneToMany(mappedBy="agent", cascade = CascadeType.ALL, fetch = FetchType.EAGER )

	public Set<ServicePartner> getServicePartners() {
		return servicePartners;
	 }
	public void setServicePartners(Set<ServicePartner> servicePartners){
		this.servicePartners = servicePartners;
	 }
	*/
	
	
	@Column(length=9)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=10)
	public String getBookingCode() {
		return bookingCode;
	}
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	@Column(length=35)
	public String getBookingCoord() {
		return bookingCoord;
	}
	public void setBookingCoord(String bookingCoord) {
		this.bookingCoord = bookingCoord;
	}
	@Column(length=65)
	public String getBookingEmail() {
		return bookingEmail;
	}
	public void setBookingEmail(String bookingEmail) {
		this.bookingEmail = bookingEmail;
	}
	@Column(length=10)
	public String getDestinationAgentCode() {
		return destinationAgentCode;
	}
	public void setDestinationAgentCode(String destinationAgentCode) {
		this.destinationAgentCode = destinationAgentCode;
	}
	@Column(length=35)
	public String getDestinationAgentCoord() {
		return destinationAgentCoord;
	}
	public void setDestinationAgentCoord(String destinationAgentCoord) {
		this.destinationAgentCoord = destinationAgentCoord;
	}
	@Column(length=65)
	public String getDestinationAgentEmail() {
		return destinationAgentEmail;
	}
	public void setDestinationAgentEmail(String destinationAgentEmail) {
		this.destinationAgentEmail = destinationAgentEmail;
	}
	@Column(length=10)
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	@Column(length=10)
	public String getPickupCode() {
		return pickupCode;
	}
	public void setPickupCode(String pickupCode) {
		this.pickupCode = pickupCode;
	}
	@Column(length=10)
	public String getSetoffCode() {
		return setoffCode;
	}
	public void setSetoffCode(String setoffCode) {
		this.setoffCode = setoffCode;
	}
	@Column(length=10)
	public String getFreightForworderCode() {
		return freightForworderCode;
	}
	public void setFreightForworderCode(String freightForworderCode) {
		this.freightForworderCode = freightForworderCode;
	}
	@Column(length=35)
	public String getFreightForworderCoord() {
		return freightForworderCoord;
	}
	public void setFreightForworderCoord(String freightForworderCoord) {
		this.freightForworderCoord = freightForworderCoord;
	}
	@Column(length=65)
	public String getFreightForworderEmail() {
		return freightForworderEmail;
	}
	public void setFreightForworderEmail(String freightForworderEmail) {
		this.freightForworderEmail = freightForworderEmail;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=10)
	public String getOriginAgentCode() {
		return originAgentCode;
	}
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}
	@Column(length=35)
	public String getOriginAgentCoord() {
		return originAgentCoord;
	}
	public void setOriginAgentCoord(String originAgentCoord) {
		this.originAgentCoord = originAgentCoord;
	}
	@Column(length=65)
	public String getOriginAgentEmail() {
		return originAgentEmail;
	}
	public void setOriginAgentEmail(String originAgentEmail) {
		this.originAgentEmail = originAgentEmail;
	}
	
	@Column(length=7)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
		
	@Column(length=10)
	public String getThrdCode() {
		return thrdCode;
	}
	public void setThrdCode(String thrdCode) {
		this.thrdCode = thrdCode;
	}
	@Column(length=65)
	public String getThrdEmail() {
		return thrdEmail;
	}
	public void setThrdEmail(String thrdEmail) {
		this.thrdEmail = thrdEmail;
	}
	@Column(length=5)
	public String getThrdRef() {
		return thrdRef;
	}
	public void setThrdRef(String thrdRef) {
		this.thrdRef = thrdRef;
	}
	@Column(length=9)
	public String getTranship() {
		return tranship;
	}
	public void setTranship(String tranship) {
		this.tranship = tranship;
	}@Column(length=35)
	public String getThrdCoord() {
		return thrdCoord;
	}
	public void setThrdCoord(String thrdCoord) {
		this.thrdCoord = thrdCoord;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
	
	
	@Column(length=50)
	public String getBookingAgentFirstName() {
		return bookingAgentFirstName;
	}
	public void setBookingAgentFirstName(String bookingAgentFirstName) {
		this.bookingAgentFirstName = bookingAgentFirstName;
	}
	
	@Column(length=50)
	public String getBookingAgentLastName() {
		return bookingAgentLastName;
	}
	public void setBookingAgentLastName(String bookingAgentLastName) {
		this.bookingAgentLastName = bookingAgentLastName;
	}
	
	@Column(length=50)
	public String getBookingCity() {
		return bookingCity;
	}
	public void setBookingCity(String bookingCity) {
		this.bookingCity = bookingCity;
	}
	
	
	@Column(length=50)
	public String getBookingCountry() {
		return bookingCountry;
	}
	public void setBookingCountry(String bookingCountry) {
		this.bookingCountry = bookingCountry;
	}
	
	@Column(length=50)
	public String getPickupDesc() {
		return pickupDesc;
	}
	public void setPickupDesc(String pickupDesc) {
		this.pickupDesc = pickupDesc;
	}
	
	@Column(length=50)
	public String getDestinationAgentCity() {
		return destinationAgentCity;
	}
	public void setDestinationAgentCity(String destinationAgentCity) {
		this.destinationAgentCity = destinationAgentCity;
	}
	
	@Column(length=50)
	public String getDestinationAgentCountry() {
		return destinationAgentCountry;
	}
	public void setDestinationAgentCountry(String destinationAgentCountry) {
		this.destinationAgentCountry = destinationAgentCountry;
	}
	@Column(length=50)
	public String getDestinationAgentFirstName() {
		return destinationAgentFirstName;
	}
	public void setDestinationAgentFirstName(String destinationAgentFirstName) {
		this.destinationAgentFirstName = destinationAgentFirstName;
	}
	@Column(length=50)
	public String getDestinationAgentLastName() {
		return destinationAgentLastName;
	}
	public void setDestinationAgentLastName(String destinationAgentLastName) {
		this.destinationAgentLastName = destinationAgentLastName;
	}
	@Column(length=50)
	public String getDeliveryDesc() {
		return deliveryDesc;
	}
	public void setDeliveryDesc(String deliveryDesc) {
		this.deliveryDesc = deliveryDesc;
	}
	@Column(length=50)
	public String getFreightForworderCity() {
		return freightForworderCity;
	}
	public void setFreightForworderCity(String freightForworderCity) {
		this.freightForworderCity = freightForworderCity;
	}
	@Column(length=50)
	public String getFreightForworderCountry() {
		return freightForworderCountry;
	}
	public void setFreightForworderCountry(String freightForworderCountry) {
		this.freightForworderCountry = freightForworderCountry;
	}
	@Column(length=50)
	public String getFreightForworderFirstName() {
		return freightForworderFirstName;
	}
	public void setFreightForworderFirstName(String freightForworderFirstName) {
		this.freightForworderFirstName = freightForworderFirstName;
	}
	@Column(length=50)
	public String getFreightForworderLastName() {
		return freightForworderLastName;
	}
	public void setFreightForworderLastName(String freightForworderLastName) {
		this.freightForworderLastName = freightForworderLastName;
	}
	@Column(length=50)
	public String getFreightForworderState() {
		return freightForworderState;
	}
	public void setFreightForworderState(String freightForworderState) {
		this.freightForworderState = freightForworderState;
	}
	@Column(length=50)
	public String getOriginAgentCity() {
		return originAgentCity;
	}
	public void setOriginAgentCity(String originAgentCity) {
		this.originAgentCity = originAgentCity;
	}
	@Column(length=50)
	public String getOriginAgentCountry() {
		return originAgentCountry;
	}
	public void setOriginAgentCountry(String originAgentCountry) {
		this.originAgentCountry = originAgentCountry;
	}
	@Column(length=50)
	public String getOriginAgentFirstName() {
		return originAgentFirstName;
	}
	public void setOriginAgentFirstName(String originAgentFirstName) {
		this.originAgentFirstName = originAgentFirstName;
	}
	@Column(length=50)
	public String getOriginAgentLastName() {
		return originAgentLastName;
	}
	public void setOriginAgentLastName(String originAgentLastName) {
		this.originAgentLastName = originAgentLastName;
	}
	
	@Column(length=50)
	public String getSetoffDesc() {
		return setoffDesc;
	}
	public void setSetoffDesc(String setoffDesc) {
		this.setoffDesc = setoffDesc;
	}
	
	
}
