/**
 * @Class Name  Vehicle Dao Hibernate
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.VehicleDao;
import com.trilasoft.app.model.Vehicle;

public class VehicleDaoHibernate extends GenericDaoHibernate<Vehicle, Long> implements VehicleDao
{ 
	public VehicleDaoHibernate() 
    {   
        super(Vehicle.class);   
    }
    public List<Vehicle> findByLastName(String lastName) {   
        return getHibernateTemplate().find("from Vehicle where lastName=?", lastName);   
    }
    public List findMaxId() {
		return getHibernateTemplate().find("select max(id) from Vehicle");
	}
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from Vehicle where id=?",id);
    }
    public List getContainerNumber(String shipNumber) {
		return getHibernateTemplate().find("select containerNumber from Container where status = true and  shipNumber=? and containerNumber is not null",shipNumber);
	}
    public List findMaximumIdNumber(String shipNum) {
    	List list=  this.getSession().createSQLQuery("select max(idNumber) from vehicle where shipNumber='"+shipNum+"' ").list();
    	return list;
    }
    public int updateDeleteStatus(Long ids){
    	return getHibernateTemplate().bulkUpdate("delete Vehicle where id='"+ids+"'");
    }
    public List goSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("from  Vehicle where  status = true and    serviceOrderId='"+serviceOrderId+"' and id !='"+id+"' and corpID='"+corpID+"'");
	}
	public List goNextSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  Vehicle where  status = true and    serviceOrderId='"+serviceOrderId+"' and id >'"+id+"' and corpID='"+corpID+"' order by 1 ");
	}
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  Vehicle where  status = true and    serviceOrderId='"+serviceOrderId+"' and id <'"+id+"' and corpID='"+corpID+"' order by 1 desc");
	}
	 public List findMaximumChild(String shipNm) {
	    	return getHibernateTemplate().find("select max(id) from Vehicle where    shipNumber=?", shipNm);
	    }
	    public List findMinimumChild(String shipNm){
	    	return getHibernateTemplate().find("select min(id) from Vehicle where    shipNumber=?", shipNm);
	    }
	    public List findCountChild(String shipNm){
	    	return getHibernateTemplate().find("select count(*) from Vehicle where   shipNumber=?", shipNm);
	    }
		public List vehicleList(Long serviceOrderId) {
			return getHibernateTemplate().find("from Vehicle where serviceOrderId='"+serviceOrderId+"'");
		} 
		public List vehicleListOtherCorpId(Long sid){
			return getSession().createSQLQuery("select id from vehicle where serviceOrderId='"+sid+"'").list();
		}
		public Long findRemoteVehicle(String idNumber, Long serviceOrderId) {
			return Long.parseLong(this.getSession().createSQLQuery("select id from vehicle where idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderId+"").list().get(0).toString());
		}
		public void deleteNetworkVehicle(Long serviceOrderId, String idNumber, String vehicleStatus) {
			this.getSession().createSQLQuery("update vehicle set status="+vehicleStatus+" where idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderId+"").executeUpdate();			
		}
		public List findVehicleBySid(Long sofid, String sessionCorpID){
			return this.getSession().createSQLQuery("select id from vehicle where serviceOrderId='"+sofid+"'and corpID='"+sessionCorpID+"' ").list();
		}
		public void updateStatus(Long id, String vehicleStatus,String updatedBy) {
			getHibernateTemplate().bulkUpdate("update Vehicle set status="+vehicleStatus+",updatedBy='"+updatedBy+"',updatedOn = now() where id='"+id+"'");
			
		}
		
		/*
         * Bug 15084 - Changes on the EDI Report for ONLY customers using EDI Reporting
         */
		 public List getWeight(String shipNumber) {
		    	List L1 = new ArrayList();
		    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(weight, 0)) is null , 0.00 , sum(coalesce(weight, 0)) )  from vehicle where status = true and   shipNumber='"+shipNumber+"'").list();
		    	return L1;
		    }

		public List getVolume(String shipNumber){
		    	List L1 = new ArrayList();
		    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(volume, 0)) is null , 0.00 , sum(coalesce(volume, 0)) )  from vehicle where status = true and  shipNumber='"+shipNumber+"'").list();
		    	return L1;
		    }
		
		public List getVolumeUnit(String shipNumber)
		{
		    List L1 = new ArrayList();
	    	L1 = this.getSession().createSQLQuery("select if( coalesce(unit2,' ') is null , '' , coalesce(unit2,' ') ) from vehicle where status = true and   shipNumber='"+shipNumber+"'").list();
	    	return L1;
		}
		public List getWeightUnit(String shipNumber) 
		{
			List L1 = new ArrayList();
	    	L1 = this.getSession().createSQLQuery("select if( coalesce(unit1,' ') is null , '', coalesce(unit1,' ') )  from vehicle where status = true and   shipNumber='"+shipNumber+"'").list();
	    	return L1;
		}
		
		public int updateMisc(String shipNumber,String weight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
			BigDecimal weightmultiplyerkg=new BigDecimal(0.4536);
			BigDecimal weightmultiplyerlbs=new BigDecimal(2.2046);
			BigDecimal weightInKg=new BigDecimal(0);
			if(weightUnit.equals("Lbs"))
			{
				weightInKg = new BigDecimal(weight).multiply(weightmultiplyerkg);
			}else{
				weightInKg = new BigDecimal(weight).multiply(weightmultiplyerlbs);
			}
	    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualGrossWeight='" +weight+ "',actualNetWeight='"+weight+"',actualGrossWeightKilo='"+weightInKg+"',actualNetWeightKilo='"+weightInKg+"',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
		   	return 1;
	    }
		
		public int updateMiscVolume(String shipNumber,String volume,String weightUnit,String corpID,String updatedBy){
			BigDecimal volumeMultiplyercbm=new BigDecimal(0.0283);
			BigDecimal volumeMultiplyercft=new BigDecimal(35.3357);
			BigDecimal volumeInCbm=new BigDecimal(0);
			if(weightUnit.equals("Lbs"))
			{
				volumeInCbm = new BigDecimal(volume).multiply(volumeMultiplyercbm);
			}else{
				volumeInCbm = new BigDecimal(volume).multiply(volumeMultiplyercft);
			}
			return getHibernateTemplate().bulkUpdate("update Miscellaneous set actualCubicFeet='" +volume+ "',actualCubicMtr='" +volumeInCbm+ "',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
		}
		    /*
		     * End
		     */
}

//End of Class.