// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company` ADD COLUMN `displayAPortalRevision` BOOLEAN DEFAULT true AFTER `fullAudit`;
