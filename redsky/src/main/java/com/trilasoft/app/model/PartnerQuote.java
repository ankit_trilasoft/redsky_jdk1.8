package com.trilasoft.app.model;
import org.appfuse.model.BaseObject;   
import java.util.*;
import java.lang.Boolean;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;   
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;

@Entity
@Table(name="partnerquote")
public class PartnerQuote extends BaseObject implements Comparable<PartnerQuote>, ListLinkData{

	private Long id;
	private String cso;
	
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	
	
	
	private String corpID;
	
	
	private String quote;
	private String quoteType;
	private String vendorCode;
	private String vendorName;
	private String vendorMail;
	private String quoteStatus;
	private Date sentDate;
	private Date reminderDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	//@@@@@@@@@@@@@  For Partner Portal By Rajesh @@@@@@@@@@@@@@@@@@@@@@@@@@@@
	private String destinationCity;
	private String originCity;
	
	//@@@@@@@@@@@@@  For Partner Portal By Rajesh @@@@@@@@@@@@@@@@@@@@@@@@@@@@	
	
//	@@@@@@@@@@@@@  added Portal By Vijay @@@@@@@@@@@@@@@@@@@@@@@@@@@@
	private String requestedSO;
	private String portOfEntry;
	private String totalPrice;
	
	//@@@@@@@@@@@@@  added By Vijay @@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
	private CustomerFile customerFile;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("cso", cso)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber).append("corpID", corpID)
				.append("quote", quote).append("quoteType", quoteType).append(
						"vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorMail", vendorMail).append(
						"quoteStatus", quoteStatus)
				.append("sentDate", sentDate).append("reminderDate",
						reminderDate).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("destinationCity",
						destinationCity).append("originCity", originCity)
				.append("requestedSO", requestedSO).append("portOfEntry",
						portOfEntry).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerQuote))
			return false;
		PartnerQuote castOther = (PartnerQuote) other;
		return new EqualsBuilder().append(id, castOther.id).append(cso,
				castOther.cso).append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship).append(shipNumber,
						castOther.shipNumber).append(corpID, castOther.corpID)
				.append(quote, castOther.quote).append(quoteType,
						castOther.quoteType).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorMail,
						castOther.vendorMail).append(quoteStatus,
						castOther.quoteStatus).append(sentDate,
						castOther.sentDate).append(reminderDate,
						castOther.reminderDate).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(destinationCity,
						castOther.destinationCity).append(originCity,
						castOther.originCity).append(requestedSO,
						castOther.requestedSO).append(portOfEntry,
						castOther.portOfEntry).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(cso).append(
				sequenceNumber).append(ship).append(shipNumber).append(corpID)
				.append(quote).append(quoteType).append(vendorCode).append(
						vendorName).append(vendorMail).append(quoteStatus)
				.append(sentDate).append(reminderDate).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn).append(
						destinationCity).append(originCity).append(requestedSO)
				.append(portOfEntry).toHashCode();
	}
	public int compareTo(final PartnerQuote other) {
		return new CompareToBuilder().append(id, other.id).append(cso,
				other.cso).append(sequenceNumber, other.sequenceNumber).append(
				ship, other.ship).append(shipNumber, other.shipNumber).append(
				corpID, other.corpID).append(quote, other.quote).append(
				quoteType, other.quoteType)
				.append(vendorCode, other.vendorCode).append(vendorName,
						other.vendorName).append(vendorMail, other.vendorMail)
				.append(quoteStatus, other.quoteStatus).append(sentDate,
						other.sentDate)
				.append(reminderDate, other.reminderDate).append(createdBy,
						other.createdBy).append(updatedBy, other.updatedBy)
				.append(createdOn, other.createdOn).append(updatedOn,
						other.updatedOn).toComparison();
	}
	@ManyToOne
	@JoinColumn(name="sequenceNumber", nullable=false, updatable=false,
	referencedColumnName="sequenceNumber")
	public CustomerFile getCustomerFile() {
	        return customerFile;
	    }
	    
		 public void setCustomerFile(CustomerFile customerFile) {
				this.customerFile = customerFile;
		}  

	@Column(length=25)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=25)
	public String getCso() {
		return cso;
	}
	public void setCso(String cso) {
		this.cso = cso;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	//@Index(name = "idNum")
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=25)
	public String getQuote() {
		return quote;
	}
	public void setQuote(String quote) {
		this.quote = quote;
	}
	@Column(length=25)
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	@Column(length=25)
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	@Column
	public Date getReminderDate() {
		return reminderDate;
	}
	public void setReminderDate(Date reminderDate) {
		this.reminderDate = reminderDate;
	}
	@Column
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	@Column(length=15, insertable = false, updatable = false )
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/*
	@Column(length=254)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	*/
	@Column(length=25)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column(length=50)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column(length=50)
	public String getVendorMail() {
		return vendorMail;
	}
	public void setVendorMail(String vendorMail) {
		this.vendorMail = vendorMail;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column( length=30 )
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	
	@Column( length=30 )
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	@Column( length=30 )
	public String getPortOfEntry() {
		return portOfEntry;
	}
	public void setPortOfEntry(String portOfEntry) {
		this.portOfEntry = portOfEntry;
	}
	@Column
	public String getRequestedSO() {
		return requestedSO;
	}
	public void setRequestedSO(String requestedSO) {
		this.requestedSO = requestedSO;
	}
	//@@@@@@@@@@@@ by Raj   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	// For look up Return values-By Madhu
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @Transient
    public String getListCode() {        
        return vendorCode;  //return the code that needs to be set on the parent page
    }

    @Transient
    public String getListDescription() {
        return this.vendorName; //return the description that needs to be set on the parent page
    } 

	@Transient
	public String getListSecondDescription() {
		return null;
	}
	
	@Transient
	public String getListThirdDescription() {
		return null;
	}
	
	@Transient
	public String getListFourthDescription() {
		return null;
	}
	
	@Transient
	public String getListFifthDescription() {
		return null;
	}
	
	@Transient
	public String getListSixthDescription() {
		return null;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
	
}
