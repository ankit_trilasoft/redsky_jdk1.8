// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `targetRevenueRecognition` DATETIME NULL  ; 


Alter table customerfile modify approvedBy varchar(82) ;
Alter table billing modify claimHandler varchar(82) ;
Alter table claim modify claimPerson  varchar(82) ; 


Alter table billing modify approvedBy  varchar(82);

Alter table workticket modify coordinator  varchar(82);
Alter table systemdefault modify claimContactPerson  varchar(82),modify networkCoordinator  varchar(82);
Alter table companydivision modify vanlineCoordinator  varchar(82);
Alter table app_user modify supervisor  varchar(82); 

ALTER TABLE `redsky`.`billing` ADD COLUMN `financialsComplete` DATETIME NULL   ;

ALTER TABLE `redsky`.`clipboard` ADD COLUMN  workDate datetime DEFAULT NULL ;


ALTER TABLE `redsky`.`customerfile` ADD COLUMN originCompanyCode varchar(8),ADD COLUMN destinationCompanyCode varchar(8),ADD COLUMN assignmentEndReason varchar(20)   Default NULL,ADD COLUMN currentEmploymentCode  varchar(8)   Default NULL,ADD COLUMN currentEmploymentName varchar(255) default Null ; 
CREATE TABLE `agentRequestReason` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(15) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `agentRequestId` bigint(20) NOT NULL,
  `comment` text,
  `reason` text,
  `counter` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agentRequestId` (`agentRequestId`)
);

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN companyLogo varchar(20000) Default NULL ;


CREATE TABLE `agentRequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(255) DEFAULT NULL,
  `corpID` varchar(15) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `billingAddress1` varchar(100) DEFAULT NULL,
  `billingAddress2` varchar(100) DEFAULT NULL,
  `billingAddress3` varchar(255) DEFAULT NULL,
  `billingAddress4` varchar(255) DEFAULT NULL,
  `billingCity` varchar(50) DEFAULT NULL,
  `billingCountry` varchar(45) DEFAULT NULL,
  `billingEmail` varchar(70) DEFAULT NULL,
  `billingFax` varchar(20) DEFAULT NULL,
  `billingPhone` varchar(100) DEFAULT NULL,
  `billingState` varchar(3) DEFAULT NULL,
  `billingZip` varchar(50) DEFAULT NULL,
  `isAgent` bit(1) DEFAULT NULL,
  `mailingAddress1` varchar(100) DEFAULT NULL,
  `mailingAddress2` varchar(100) DEFAULT NULL,
  `mailingAddress3` varchar(255) DEFAULT NULL,
  `mailingAddress4` varchar(255) DEFAULT NULL,
  `mailingCity` varchar(50) DEFAULT NULL,
  `mailingCountry` varchar(45) DEFAULT NULL,
  `mailingCountryCode` varchar(3) DEFAULT NULL,
  `mailingEmail` varchar(65) DEFAULT NULL,
  `mailingFax` varchar(20) DEFAULT NULL,
  `mailingPhone` varchar(100) DEFAULT NULL,
  `mailingState` varchar(3) DEFAULT NULL,
  `mailingTelex` varchar(20) DEFAULT NULL,
  `mailingZip` varchar(50) DEFAULT NULL,
  `terminalAddress1` varchar(100) DEFAULT NULL,
  `terminalAddress2` varchar(100) DEFAULT NULL,
  `terminalAddress3` varchar(255) DEFAULT NULL,
  `terminalAddress4` varchar(255) DEFAULT NULL,
  `terminalCity` varchar(50) DEFAULT NULL,
  `terminalCountry` varchar(45) DEFAULT NULL,
  `terminalCountryCode` varchar(3) DEFAULT NULL,
  `terminalEmail` varchar(70) DEFAULT NULL,
  `terminalFax` varchar(20) DEFAULT NULL,
  `terminalPhone` varchar(100) DEFAULT NULL,
  `terminalState` varchar(3) DEFAULT NULL,
  `terminalZip` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `agentParent` varchar(8) DEFAULT NULL,
  `companyProfile` mediumtext,
  `url` varchar(100) DEFAULT NULL,
  `latitude` decimal(15,10) DEFAULT NULL,
  `longitude` decimal(15,10) DEFAULT NULL,
  `yearEstablished` varchar(5) DEFAULT NULL,
  `companyFacilities` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `companyCapabilities` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `companyDestiantionProfile` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `fidiNumber` varchar(10) DEFAULT NULL,
  `OMNINumber` varchar(10) DEFAULT NULL,
  `IAMNumber` varchar(10) DEFAULT NULL,
  `AMSANumber` varchar(10) DEFAULT NULL,
  `WERCNumber` varchar(10) DEFAULT NULL,
  `facilitySizeSQFT` varchar(10) DEFAULT NULL,
  `vanLineAffiliation` varchar(255) DEFAULT NULL,
  `serviceLines` varchar(255) DEFAULT NULL,
  `facilitySizeSQMT` varchar(10) DEFAULT NULL,
  `agentParentName` varchar(255) DEFAULT NULL,
  `doNotMerge` bit(1) DEFAULT NULL,
  `utsNumber` varchar(1) DEFAULT NULL,
  `viewChild` bit(1) DEFAULT NULL,
  `partnerPortalActive` bit(1) DEFAULT NULL,
  `UTSmovingCompanyType` varchar(50) DEFAULT NULL,
  `billingCurrency` varchar(4) DEFAULT NULL,
  `bankcode` varchar(45) DEFAULT NULL,
  `bankAccountNumber` varchar(40) DEFAULT NULL,
  `vatNumber` varchar(25) DEFAULT NULL,
  `ugwwNetworkGroup` bit(1) DEFAULT b'0',
  `agentGroup` varchar(15) DEFAULT NULL,
  `sentToAccounting` bit(1) DEFAULT b'0',
  `mergeInto` varchar(8) DEFAULT NULL,
  `doNotSendEmailtoAgentUser` bit(1) DEFAULT b'0',
  `PAIMA` varchar(1) DEFAULT NULL,
  `LACMA` varchar(1) DEFAULT NULL,
  `eurovanNetwork` varchar(1) DEFAULT NULL,
  `aliasName` varchar(255) DEFAULT NULL,
  `location1` varchar(255) DEFAULT NULL,
  `location2` varchar(255) DEFAULT NULL,
  `location3` varchar(255) DEFAULT NULL,
  `location4` varchar(255) DEFAULT NULL,
  `billingCountryCode` varchar(3) DEFAULT NULL,
  `isPrivateParty` bit(1) DEFAULT NULL,
  `typeOfVendor` varchar(300) DEFAULT NULL,
  `partnerType` varchar(5) DEFAULT NULL,
  `partnerId` bigint(20) DEFAULT NULL,
  `partnerCode` varchar(8) DEFAULT NULL,
  `counter` varchar(100) DEFAULT NULL,
  `companyLogo` varchar(20000) DEFAULT NULL,
  PRIMARY KEY (`id`)
);