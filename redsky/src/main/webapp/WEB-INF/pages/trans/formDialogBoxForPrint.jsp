<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>

<head>   
<title>Print Daily Package</title>   
<meta name="heading" content="Print Daily Package"/>   

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:1px;
margin-top:-21px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->
	
<style type="text/css">
	/* collapse */

img {
cursor:pointer;
}

#Layer1 {
width:540px;
}
		
</style>
	
</head>  

<s:form id="printForm" name="printForm"  action='' method="post" enctype="multipart/form-data" >
	<s:hidden name="whouse" id="whouse" />
	
	<div id="Layer1">
	<table cellspacing="0" cellpadding="0" border="0" style="padding-bottom:0px">
		
			<tr>
				<td><img src="<c:url value='/images/form_ico.gif'/>"/></td>
				<td>
					<table cellspacing="0" cellpadding="0">
					<tr><td>&nbsp;</td></tr> 
					<tr style="height: 5px;"><td></td></tr> 
					<tr><td><b><font style="color: #5D5D5D;">Work Ticket info by Date and WareHouse</font></b></td></tr> 
					</table>
				</td>
			</tr>	
		
	</table>
	
	</div>
	<div id="newmnav" style="margin-bottom:0px;!margin-bottom:-13px;">
		  <ul>
		    <li id="newmnav1"><a class="current"><span>Print</span></a></li>
		  </ul>
		  <ul>
		    <li><a onclick="targetPage('SetUp');"><span>SetUp</span></a></li>
		  </ul>
		  <ul>
		    <li><a onclick="targetPage('Waste');"><span>Waste Basket</span></a></li>
		  </ul>
	</div>
	<div class="spn" style="width:510px;line-height:0.2em;">&nbsp;</div>
	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px;padding-bottom:0px">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Select Parameters and Output format for Report</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>	
		    					<tr><td style="height:10px;"></td></tr> 
								<tr><td><b>&nbsp;&nbsp;Parameter(s):</b></td></tr>
								<tr><td style="height:10px;"></td></tr> 
								
								<tr style="height: 10px;">
								<td align="right">Warehouse<font color="RED">*</font>:</td>
								<td><s:select id="wareHouse" name="wareHouse" list="%{house}" value="${whouse}"  headerKey="" headerValue="" cssStyle="width:150px" cssClass="list-menu" /></td>
								</tr> 
								<tr><td style="height:5px;"></td></tr> 
								<tr style="height: 20px;">
								<td align="right">Date<font color="RED">*</font>:</td>
								<td>
					 			<s:textfield name="printDate" cssClass="input-text" id="printDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
					 			<img id="printDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
								</td>
								</tr> 
								  <tr><td style="height:15px;"></td></tr> 
								  <tr>
								  <td></td>
								  <td>
									<input type="button" class="cssbuttonA" style="width:100px; height:25px"  
								        onclick="return validatefields('PDF');" value="Print Package"/> 
								</td></tr> 
								<tr><td style="height:25px;"></td></tr> 
						           
				             </tbody>
				           </table>
						</td>	
		  			</tr>
		  		</tbody>
			</table>
            
<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Fetch WorkTicket In Progress...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>

</s:form>

<script language="javascript" type="text/javascript">
var http = getHTTPObject();

	function validatefields(val){
		var wareHouse = document.getElementById('wareHouse').value;
		var printDate = document.getElementById('printDate').value;
		if(wareHouse == '' || printDate == ''){
			alert('Please Enter Mandatory Field.');
			return false;
		}
		document.getElementById('whouse').value = wareHouse;
		showHide("block");
		var url = "getAllTicketByWHAjax.html?ajax=1&decorator=simple&popup=true&&warehouse="+wareHouse+"&printDate="+printDate;
		http.open("GET", url, true);
		http.onreadystatechange = function(){ handleHttpResponse(printDate,wareHouse);};
		http.send(null);
	}
	function handleHttpResponse(date,wh){
		if (http.readyState == 4){
			showHide("none");
			var results = http.responseText;
		    results = results.trim();
		    if(results == '0'){
		    	alert('There is nothing to print for the selected date and WH.');
		    	return false;
		    }
		    var agree=confirm('There is '+results+' WorkTicket(s) to print for the selected date and WH. Are you sure to Print ? \n  Please note, preparing package to print will take 3-5 minutes, so please be patient.');
			if (agree){
				document.forms['printForm'].action ='viewFormPrintReport.html?ajax=1&decorator=simple&popup=true';
				document.forms['printForm'].submit();
			 }else{
				return false;
			}
		}
	}
	function showHide(action){
		document.getElementById("loader").style.display = action;
	}
</script>


 <script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script> 
<script type="text/javascript">
autoPopulate_Date(this);
function autoPopulate_Date(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['printForm'].elements['printDate'].value=datam;
	}
	function targetPage(target){
		var url = '';
		if(target == 'SetUp'){
			url = 'printSetUp.html?tabId=setup';
		}else if(target == 'Waste'){
			url = 'printWasteBasket.html?tabId=waste';
		}
		
		location.href = url;
	}
	</script> 