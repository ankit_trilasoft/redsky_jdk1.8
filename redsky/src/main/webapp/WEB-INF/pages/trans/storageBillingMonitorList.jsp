<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Storage Billing Monitor List</title>   
    <meta name="heading" content="Storage Billing Monitor List"/>      
<style>
.tab{border:1px solid #74B3DC;}
.table th.containeralignH{text-align:right;padding-right:8px;}
span.pagelinks {display:block;font-size:0.90em;margin-bottom:2px;!margin-bottom:2px;margin-top:-16px;!margin-top:-18px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
</style>
</head>
<s:form id="storageBillingMonitorList" action="" method="post" validate="true">
<div id="newmnav">
	  <ul>
 		<li><a href="networkAgentStorageBilling.html" ><span>Storage Billing</span></a></li>
 	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Storage Billing Monitor</span></a></li>	  									  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="Layer1" style="width: 100%;">

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>				
<display:table name="storageBillingMonitorDataList" class="table" id="storageBillingMonitorDataList" requestURI="" pagesize="25" style="width:100%;">
<display:column sortable="true" title="Storage Type"  maxLength="100" style="width:210px">
	<c:choose>
		<c:when test="${storageBillingMonitorDataList.billingType=='1'}">
			<c:out value="Storage Billing" />
		</c:when>
		<c:when test="${storageBillingMonitorDataList.billingType=='2'}">
			<c:out value="SIT Billing" />
		</c:when>
		<c:when test="${storageBillingMonitorDataList.billingType=='3'}">
			<c:out value="TPS" />
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</display:column>
<display:column sortable="true" title="Bill Through From" style="width:100px" sortProperty="billThroughFrom">
<c:set var="billThroughFrom" value="${fn:substring(storageBillingMonitorDataList.billThroughFrom,0,10)}" />
<c:out value="${billThroughFrom}"></c:out>
</display:column>
<display:column sortable="true" title="Bill Through To" style="width:100px" sortProperty="billThroughTo">
<c:set var="billThroughTo" value="${fn:substring(storageBillingMonitorDataList.billThroughTo,0,10)}" />
<c:out value="${billThroughTo}"></c:out>
</display:column>
<display:column property="storageBillingGroup" sortable="true" title="Storage Billing Group"  maxLength="100" style="width:210px"/>
<display:column title="Include/Exclude"  maxLength="100" style="width:210px">
		<c:if test="${storageBillingMonitorDataList.incExcGroup=='2'}">
				<c:out value="Excluded"/>
		</c:if>
		<c:if test="${storageBillingMonitorDataList.incExcGroup!='2'}">			
				<c:out value="Included"/>			
		</c:if>
</display:column>
<display:column property="contractType" sortable="true" title="Contract Type"  maxLength="100" style="width:210px"/>
<display:column property="status" sortable="true" title="Status"  maxLength="100" style="width:210px"/>
<display:column property="totalCount" sortable="true" title="Total Count" headerClass="containeralignH" maxLength="100" style="width:210px;text-align:right;"/>
<display:column property="presentCount" sortable="true" title="Current Count"  headerClass="containeralignH" maxLength="100" style="width:210px;text-align:right;"/>
<display:column  title="Invoice" headerClass="centeralign" style="width:70px;text-align:center;">
	<c:choose> 
			<c:when test="${storageBillingMonitorDataList.invoice==true || storageBillingMonitorDataList.invoice=='true'}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
			</c:when>
			<c:otherwise>
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
			</c:otherwise>			
	</c:choose>	
</display:column>
</display:table>
			</td>
		</tr>
		<tr><td>
		<input type="button"  class="cssbutton1"  value="Refresh Storage Billing Monitor List" name="refresh"  style="width:230px;" onclick="location.reload();" />
		</td>
		</tr>
	</tbody>
</table> 
</div>
</div>
</s:form>