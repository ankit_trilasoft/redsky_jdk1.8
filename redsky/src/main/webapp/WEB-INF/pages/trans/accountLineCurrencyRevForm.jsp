<%@ include file="/common/taglibs.jsp"%>
<title>Pricing Currency Form</title>
<meta name="heading" content="Pricing Currency Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 
</script>


<s:form id="accountLineCurrencyRecForm" name="accountLineCurrencyRecForm" action="" onsubmit="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="tempAccList"/>
 <s:hidden name="accountLine.revisionExpense"  /> 
 <s:hidden name="accountLine.revisionQuantity"  />  
 <s:hidden name="accountLine.basis"  />
 <s:hidden name="accountLine.revisionDiscount"/> 
  <s:hidden name="accountLine.revisionRate"/> 
 <s:hidden name="aid" value="${accountLine.id}" />  
<s:hidden name="formStatus"  />
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Buy&nbsp;Rate&nbsp;Currency&nbsp;Data</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    
<table border="0" style="margin:0px;padding:0px;">
										<c:if test="${contractType}">																											 
										 <tr>
										 <td colspan="9">															 											
										 <table class="detailTabLabel" style="margin:0px;">
										 <tr>															
										 <td width="1px"></td>
										 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
										 <td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.revisionPayableContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.revisionPayableContractExchangeRate');calculateRecCurrency('accountLine.revisionPayableContractRate');" headerValue="" tabindex="12"  /></td>
										 <td align="right" width="" class="listwhitetext">Value&nbsp;Date</td>

										 <c:if test="${not empty accountLine.revisionPayableContractValueDate}"> 
										  <s:text id="accountLineFormattedRevisionPayableContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionPayableContractValueDate"/></s:text>
										  <td ><s:textfield id="revisionPayableContractValueDate" name="accountLine.revisionPayableContractValueDate" value="%{accountLineFormattedRevisionPayableContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
										  <img id="revisionPayableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										 </td>
										 </c:if>
										     <c:if test="${empty accountLine.revisionPayableContractValueDate}">
										   <td><s:textfield id="revisionPayableContractValueDate" name="accountLine.revisionPayableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/>
										   <img id="revisionPayableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										   </td>
										     </c:if>
                                            <td  align="right" class="listwhitetext" width="55px">Ex.&nbsp;Rate</td>
										 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPayableContractExchangeRate"  size="8" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.revisionPayableContractRate');" /></td>   			
                                            <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPayableContractRate" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.revisionPayableContractRate');" /></td>	 
                                            <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionPayableContractRateAmmount" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true"  onblur=""/></td>	 
										 </tr></table>															
										 </td>															
										 </tr>
										</c:if> 
										
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">

<tr>

													 <c:choose>
													 <c:when test="${contractType}">
													 <td align="right" class="listwhitetext" width="102">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
													 </c:when>
													 <c:otherwise>
													 <td align="right" class="listwhitetext">Currency</td>
													 </c:otherwise>
													 </c:choose>

<td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.revisionCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.revisionExchangeRate');calculateRecCurrency('accountLine.revisionPayableContractRate');" headerValue="" tabindex="18"  /></td>
<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
<c:if test="${not empty accountLine.revisionValueDate}"> 
<s:text id="accountLineRevisionValueDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionValueDate"/></s:text>
<td width="110px" ><s:textfield id="revisionValueDate" name="accountLine.revisionValueDate" value="%{accountLineRevisionValueDateFormattedValue}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="revisionValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<c:if test="${empty accountLine.revisionValueDate}">
<td width="110px"><s:textfield id="revisionValueDate" name="accountLine.revisionValueDate" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="revisionValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<td  align="right" class="listwhitetext">Ex.&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionExchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.revisionPayableContractRate');"/></td>
<td align="right" class="listwhitetext" width="80px">Buy&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionLocalRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.revisionLocalRate');" onblur=""/></td>	 
<td align="right" class="listwhitetext" width="86px">Curr&nbsp;Amount</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionLocalAmount" size="8" maxlength="12" readonly="true"/></td>	 
</tr> 
</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="calculateRecRate();"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<script type="text/javascript">
</script>
<script type="text/javascript">
<c:if test="${(((!discountUserFlag.pricingActual && !discountUserFlag.pricingRevision) &&  (accountLine.revisionExpense!='0.00' || accountLine.revisionRevenueAmount!='0.00' || accountLine.actualRevenue!='0.00' || accountLine.actualExpense!='0.00' ||accountLine.distributionAmount!='0.00' || accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || ((discountUserFlag.pricingActual || discountUserFlag.pricingRevision) &&  (accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking'))}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">
function calculateRecRate(){ 
	var aid=document.forms['accountLineCurrencyRecForm'].elements['aid'].value;
	window.opener.document.getElementById('revisionExpense'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExpense'].value;
	window.opener.document.getElementById('revisionRate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value;
	window.opener.document.getElementById('revisionQuantity'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value;
    <c:if test="${contractType}">
    window.opener.document.getElementById('revisionPayableContractCurrency'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractCurrency'].value;
    window.opener.document.getElementById('revisionPayableContractExchangeRate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractExchangeRate'].value;
    window.opener.document.getElementById('revisionPayableContractRate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRate'].value;
    window.opener.document.getElementById('revisionPayableContractRateAmmount'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRateAmmount'].value;
    window.opener.document.getElementById('revisionPayableContractValueDate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value;
    </c:if>  
    window.opener.document.getElementById('revisionCurrency'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionCurrency'].value;
    window.opener.document.getElementById('revisionExchangeRate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value;
    window.opener.document.getElementById('revisionLocalRate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalRate'].value;
    window.opener.document.getElementById('revisionLocalAmount'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value;
    window.opener.document.getElementById('revisionValueDate'+aid).value=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value;
    if(document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value=='2'){
    window.opener.calculateRevisionBlock(aid);
    window.opener.changeStatus();
    }
		window.close();
}


function changeStatus() {
	   document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value = '2'; 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)||(keyCode==189)|| (keyCode==110); 
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function updateExchangeRate(currency,field){
	if(field=='accountLine.revisionPayableContractExchangeRate'){
		
		document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value);
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
		var revisionCurrency = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionCurrency'].value;
        if(revisionCurrency==currency.value){
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractExchangeRate'].value;
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value;
        } 
	}
	if(field=='accountLine.revisionExchangeRate'){
		
		document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value);
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value=currentDateGlobal(); 
		<c:if test="${contractType}">
		var revisionPayableContractCurrency = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractCurrency'].value;
        if(revisionPayableContractCurrency==currency.value){
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractExchangeRate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value ;
        	document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value ;
        } 
        </c:if>
	}
}
function currentDateGlobal(){
	 var mydate=new Date();
    var daym;
    var year=mydate.getFullYear()
    var y=""+year;
    if (year < 1000)
    year+=1900
    var day=mydate.getDay()
    var month=mydate.getMonth()+1
    if(month == 1)month="Jan";
    if(month == 2)month="Feb";
    if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}

function calculateRecCurrency(target){
	 var basis=document.forms['accountLineCurrencyRecForm'].elements['accountLine.basis'].value;
	 var baseRateVal=1;
	var revisionQuantity=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value;	
    var revisionDiscount=0.00;
    var revisionRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value;
    try{
    	revisionDiscount=document.forms['serviceOrderForm'].elements['accountLine.revisionDiscount'].value;
    }catch(e){}
	 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		 revisionQuantity = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value=1;
        } 
    if( basis=="cwt" || basis=="%age"){
      	 baseRateVal=100;
    }else if(basis=="per 1000"){
      	 baseRateVal=1000;
    } else {
      	 baseRateVal=1;  	
	 }		
<c:if test="${contractType}">

if(target=='accountLine.revisionPayableContractRate'){
	var revisionPayableContractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractCurrency'].value;		
	var revisionPayableContractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractExchangeRate'].value;
	var revisionPayableContractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRate'].value;
	var revisionPayableContractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRateAmmount'].value;
	var revisionPayableContractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value;
	if(revisionPayableContractCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
		revisionPayableContractRateAmmount=(revisionQuantity*revisionPayableContractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRateAmmount'].value=revisionPayableContractRateAmmount;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value=revisionPayableContractRate/revisionPayableContractExchangeRate;
	}
	   var revisionCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionCurrency'].value;		
	   var revisionExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value;
	   var revisionLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalRate'].value;
	   var revisionLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value;
	   var revisionValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value;
	   revisionRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value;
	   	if(revisionCurrency!=''){
	   		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
	   		revisionLocalRate=revisionRate*revisionExchangeRate;
	   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalRate'].value=revisionLocalRate;
	   		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmount;   	 		
	}
}else if(target=='accountLine.revisionLocalRate'){
	   var revisionCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionCurrency'].value;		
	   var revisionExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value;
	   var revisionLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalRate'].value;
	   var revisionLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value;
	   var revisionValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value;
	   
	   	if(revisionCurrency!=''){
	   		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
	   		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmount; 
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value=revisionLocalRate/revisionExchangeRate; 		
	}	
	
	var revisionPayableContractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractCurrency'].value;		
	var revisionPayableContractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractExchangeRate'].value;
	var revisionPayableContractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRate'].value;
	var revisionPayableContractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRateAmmount'].value;
	var revisionPayableContractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value;
	revisionRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value;
	if(revisionPayableContractCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
		revisionPayableContractRate=revisionRate*revisionPayableContractExchangeRate;
   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRate'].value=revisionPayableContractRate;
   		revisionPayableContractRateAmmount=(revisionQuantity*revisionPayableContractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionPayableContractRateAmmount'].value=revisionPayableContractRateAmmount;
	}
}else{

}
	
</c:if>
<c:if test="${!contractType}">
	var revisionCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionCurrency'].value;		
	var revisionExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionExchangeRate'].value;
	var revisionLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalRate'].value;
	var revisionLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value;
	var revisionValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value;
	if(revisionCurrency!=''){
		//document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmount;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRate'].value=revisionLocalRate/revisionExchangeRate; 		
	}
	</c:if>
}
</script>
