package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="dsvisaimmigration")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DsVisaImmigration extends BaseObject{
	private Long id;
	private Long serviceOrderId;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String vendorCode;
	private String vendorName;
	private String vendorContact;
	private String vendorEmail;
	private Date serviceStartDate;
	private Date serviceEndDate;
	private String workPermitVisaHolderName;
	private Date providerNotificationDate;
	private Date visaExpiryDate  ;
	private Date workPermitExpiry   ;
	private Date expiryReminder3MosPriorToExpiry;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorContact", vendorContact)
				.append("vendorEmail", vendorEmail).append("serviceStartDate",
						serviceStartDate).append("serviceEndDate",
						serviceEndDate).append("workPermitVisaHolderName",
						workPermitVisaHolderName).append(
						"providerNotificationDate", providerNotificationDate)
				.append("visaExpiryDate", visaExpiryDate).append(
						"workPermitExpiry", workPermitExpiry).append(
						"expiryReminder3MosPriorToExpiry",
						expiryReminder3MosPriorToExpiry).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsVisaImmigration))
			return false;
		DsVisaImmigration castOther = (DsVisaImmigration) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				serviceOrderId, castOther.serviceOrderId).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorContact,
						castOther.vendorContact).append(vendorEmail,
						castOther.vendorEmail).append(serviceStartDate,
						castOther.serviceStartDate).append(serviceEndDate,
						castOther.serviceEndDate).append(
						workPermitVisaHolderName,
						castOther.workPermitVisaHolderName).append(
						providerNotificationDate,
						castOther.providerNotificationDate).append(
						visaExpiryDate, castOther.visaExpiryDate).append(
						workPermitExpiry, castOther.workPermitExpiry).append(
						expiryReminder3MosPriorToExpiry,
						castOther.expiryReminder3MosPriorToExpiry).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId).append(
				corpID).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(vendorCode).append(vendorName)
				.append(vendorContact).append(vendorEmail).append(
						serviceStartDate).append(serviceEndDate).append(
						workPermitVisaHolderName).append(
						providerNotificationDate).append(visaExpiryDate)
				.append(workPermitExpiry).append(
						expiryReminder3MosPriorToExpiry).toHashCode();
	}
	
	@Column(length=30)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column
	public Date getExpiryReminder3MosPriorToExpiry() {
		return expiryReminder3MosPriorToExpiry;
	}
	public void setExpiryReminder3MosPriorToExpiry(
			Date expiryReminder3MosPriorToExpiry) {
		this.expiryReminder3MosPriorToExpiry = expiryReminder3MosPriorToExpiry;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public Date getProviderNotificationDate() {
		return providerNotificationDate;
	}
	public void setProviderNotificationDate(Date providerNotificationDate) {
		this.providerNotificationDate = providerNotificationDate;
	}
	
	@Column
	public Date getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	@Column(length=225)
	public String getVendorContact() {
		return vendorContact;
	}
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	
	@Column(length=65)
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	
	@Column(length=225)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	@Column
	public Date getVisaExpiryDate() {
		return visaExpiryDate;
	}
	public void setVisaExpiryDate(Date visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}
	
	@Column
	public Date getWorkPermitExpiry() {
		return workPermitExpiry;
	}
	public void setWorkPermitExpiry(Date workPermitExpiry) {
		this.workPermitExpiry = workPermitExpiry;
	}
	
	@Column(length=100)
	public String getWorkPermitVisaHolderName() {
		return workPermitVisaHolderName;
	}
	public void setWorkPermitVisaHolderName(String workPermitVisaHolderName) {
		this.workPermitVisaHolderName = workPermitVisaHolderName;
	}
}
