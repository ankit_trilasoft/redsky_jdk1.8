<!-- 
	Created By: - Kunal Kumar Sharma
	Date: - 23-January-2012
	Comment: - To Join this data to *serviceOrderForm.jsp* page put it just before </head> tag 
			   This page is created due to the exceeding Byte limit (65536) of the _jspService method 
-->


<script type="text/javascript">
window.onload = function() {
	if(${isNetworkRecord}){
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper';
		
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].setAttribute('readonly','readonly');
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].setAttribute('readonly','readonly');
		var el = document.getElementById('serviceOrder.bookingAgentCode.img');
		el.onclick = false;
	    document.images['serviceOrder.bookingAgentCode.img'].src = 'images/navarrow.gif';
	}
}


    var map;
    var gdir;
    var geocoder = null;
    var addressMarker;

    function initialize() {
    
    var terminalCity=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
	var terminalZip=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	var terminalState=document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
	
	var fromAddress = terminalCity+','+terminalState+' '+terminalZip;
	
	var destCity=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	var destZip=document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	var destState=document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	
	var toAddress = destCity+','+destState+' '+destZip;
	
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        gdir = new GDirections(map, document.getElementById("directions"));
		
        GEvent.addListener(gdir, "load", onGDirectionsLoad);
        GEvent.addListener(gdir, "error", handleErrors);

        setDirections(fromAddress, toAddress, "en_US");
      }
    }
    
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }

    function handleErrors(){
	   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
	     alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
	     alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
	   
	   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
	     alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);

	//   else if (gdir.getStatus().code == G_UNAVAILABLE_ADDRESS)  <--- Doc bug... this is either not defined, or Doc is wrong
	//     alert("The geocode for the given address or the route for the given directions query cannot be returned due to legal or contractual reasons.\n Error code: " + gdir.getStatus().code);
	     
	   else if (gdir.getStatus().code == G_GEO_BAD_KEY)
	     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

	   else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
	     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
	    
	   else alert("An unknown error occurred.");
	   
	}

	function onGDirectionsLoad()
	{ 
		var distanceInMile=gdir.getDistance().html;
		distanceInMile=distanceInMile.split('&nbsp;');
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=distanceInMile[0].replace(',','');
	if(distanceInMile[1]=='mi'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
		}
		//document.getElementById("getDistance").innerHTML = gdir.getDistance().html; 
	} 
  </script>
  
<script type="text/javascript">
     
function getBigPartnerAlert(code,position) {
	if(code != ''){
		var url="findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
		position = document.getElementById(position);
		ajax_showBigTooltip(url,position);
	}  			
} 

// Method For City And State On the Basis of ZipCode
function findCityStateNotPrimary(targetField, position){

if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	 }
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }
 
 
  function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     
     var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){  handleHttpResponseCityStateFlex(targetElement,targetField);};
    
     http99.send(null);
     }
  
   function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                if(results.length>0)
                    {
                     if(results=='Y' ){
                     findCityState(targetElement,targetField);
                     }
                   if(results=='N'){                 
                    }
                    } 
                    else{
                    
                    } 
                                               
                   }
                   }
     
 function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }
     
     
     
  function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
       if(targetField=='OZ'){
       var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
        if(document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
          if( document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
      
        }  
      }

function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = id;
	 } else if (targetField=='DZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = id;
	} }

 function handleHttpResponseCityState(targetField){
                  if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=res[2];
	           	 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value=res[2];
	           	 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].focus();
	           	} } } else { }
             }
// End Of Method
function zipCode(){
        var originCountryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value; 
		var el = document.getElementById('zipCodeRequiredTrue');
		var el1 = document.getElementById('zipCodeRequiredFalse');
		if(originCountryCode == 'United States'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		}
}
</script>  
  
<script>
 <sec-auth:authComponent componentId="module.script.form.agentScript">
 window.onload = function() { 


		trap();
		var elementsLen=document.forms['serviceOrderForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['serviceOrderForm'].elements[i].type=='text')
					{
						document.forms['serviceOrderForm'].elements[i].readOnly =true;
						document.forms['serviceOrderForm'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['serviceOrderForm'].elements[i].disabled=true;
					}
					
					
			}
			if(document.forms['serviceOrderForm'].elements['saveButton'])
			{
				document.forms['serviceOrderForm'].elements['saveButton'].disabled=false;
			}
		
			document.forms['serviceOrderForm'].elements['serviceOrder.general'].disabled=false;
		
		
		<sec-auth:authScript tableList="serviceOrder" formNameList="serviceOrderForm" transIdList='${serviceOrder.shipNumber}'>
	
		</sec-auth:authScript>
	}
</sec-auth:authComponent>

<sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 

		
		trap();
		var elementsLen=document.forms['serviceOrderForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['serviceOrderForm'].elements[i].type=='text')
					{
						document.forms['serviceOrderForm'].elements[i].readOnly =true;
						document.forms['serviceOrderForm'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['serviceOrderForm'].elements[i].disabled=true;
					}
					
						
			}
			
	}
</sec-auth:authComponent>


function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}
 function changeCalOpenarvalue()
		  {
		  	
		  	document.forms['serviceOrderForm'].elements['calOpener'].value='open';
		  }
		  
function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		        	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
						
						if(document.images[i].src.indexOf('notes_empty1.jpg')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
						if(document.images[i].src.indexOf('notes_open1.jpg')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{ 
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
						document.forms['serviceOrderForm'].elements['serviceOrder.general'].disabled=false;
						
		      }
		    }
		  }
   function getOriginCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}

function getDestinationCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}
function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}	
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].selected=true;
				}else{
                     
                 }
             }
        }
function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='N';
				 	}
				 	document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].select();		     	 
 				}else{
                     
                 }
             }
}   
         
        
// A function for auto save funtionality.

function ContainerAutoSave(clickType){
	var check=false;
	 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
		 var check = true;
	 }else{
		 var check =saveValidation('autoTab');
	 }
 if(check){ 	 
	 	progressBarAutoSave('1');
		var elementsLen=document.forms['serviceOrderForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
		{
			
					document.forms['serviceOrderForm'].elements[i].disabled=false;
		}
        
        var id1 = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    	var jobNumber = document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    	var idc = document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	if (document.forms['serviceOrderForm'].elements['formStatus'].value == '1'){
	if ('${autoSavePrompt}' == 'No'){
		
			var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                noSaveAction = 'customerServiceOrders.html?id='+idc;
              }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                noSaveAction = 'accountLineList.html?sid='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
        noSaveAction = 'pricingList.html?sid='+id1;
        }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
                noSaveAction = 'containers.html?id='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
                noSaveAction = 'editBilling.html?id='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                noSaveAction = 'editMiscellaneous.html?id='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){
                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO')
                { 
                	noSaveAction = 'editDspDetails.html?id='+id1; 
                  relo="yes";
                }else{
                	noSaveAction =  'editTrackingStatus.html?id='+id1;
                    } 
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                noSaveAction = 'customerWorkTickets.html?id='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
                noSaveAction = 'claims.html?id='+id1;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
                noSaveAction = 'editCustomerFile.html?id='+idc;
                }
          processAutoSave(document.forms['serviceOrderForm'], 'saveServiceOrder!saveOnTabChange.html', noSaveAction);
	}
else{
     if(!(clickType == 'save')){
      var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='serviceOrderDetail.heading'/>");
         if(agree){
           document.forms['serviceOrderForm'].action = 'saveServiceOrder!saveOnTabChange.html';
           document.forms['serviceOrderForm'].submit();
       }else{
           if(id1 != ''){
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               location.href = 'accountLineList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
               location.href = 'containers.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               location.href = 'editMiscellaneous.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){

                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO')
                { 
                	location.href = 'editDspDetails.html?id='+id1; 
                  relo="yes";
                }else{
                	location.href =  'editTrackingStatus.html?id='+id1;
                    } 
                
                }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               location.href = 'customerWorkTickets.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               location.href = 'claims.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
               location.href = 'editCustomerFile.html?id='+idc;
               }
       }
       }
   }else{
   if(id1 != ''){
       if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               location.href = 'accountLineList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
               location.href = 'containers.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
			   }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               location.href = 'editMiscellaneous.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){
                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO')
                { 
                	location.href = 'editDspDetails.html?id='+id1; 
                  relo="yes";
                }else{
                	location.href =  'editTrackingStatus.html?id='+id1;
                    }                 
                }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               location.href = 'customerWorkTickets.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               location.href = 'claims.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
               location.href = 'editCustomerFile.html?id='+idc;
               }
   }
   }
   }
  }
  else{	  
  if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               location.href = 'accountLineList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
               location.href = 'containers.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
				}
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               location.href = 'editMiscellaneous.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){               
                   var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                   if(rel=='RLO')
                   { 
                     location.href = 'editDspDetails.html?id='+id1; 
                     relo="yes";
                   }else{
                	   location.href =  'editTrackingStatus.html?id='+id1;
                       } 
                }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               location.href = 'customerWorkTickets.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               location.href = 'claims.html?id='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
               location.href = 'editCustomerFile.html?id='+idc;
               }
               }
}
	 
}
function changeStatus1()
{
	for (var i=0; i < document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'].length; i++)
   	{
   if (document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'][i].checked)
      {
      		var rad_val = document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'][i].value;
	      if(rad_val=='No')
	      {
	      	document.getElementById('COA').style.display = 'none';
	      	document.getElementById('OAE').style.display = 'none';
	      	document.getElementById('DAE').style.display = 'none';
			document.getElementById('Comments').style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.feedBack'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].style.display = 'none';
	     	document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value=''
			document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value=''
			document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].checked=false
			
	      }
		   else  if(rad_val=='Yes')
		   {
		   		document.getElementById('COA').style.display = '';
	      		document.getElementById('OAE').style.display = '';
	      		document.getElementById('DAE').style.display = '';
		   		document.getElementById('Comments').style.display = '';
		   		document.forms['serviceOrderForm'].elements['serviceOrder.feedBack'].style.display = '';
		   		document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].style.display = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].style.display = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].style.display = '';
		   }
   		}
}
}

function changeStatus(){
    
   document.forms['serviceOrderForm'].elements['formStatus'].value='1';
}

function validate(targetElement){
if (document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].value=='Y'){
document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value='1'
document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value='1'


}
else{
   	if (document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].value=='N'){
document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value='-1'
document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value='-1'
    }
    }
}


// End of function.

function myFunction(){
 var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
 	 if(bookCode != '' || bookCode != null){
 	    	checkBookingAgent();  	
		   	setTimeout(function(){findBookingAgentName();}, 500);  		
	 }	
	}    
 function checkBookingAgent()
{
	var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
    var url="bookingAgent.html?ajax=1&decorator=simple&popup=true&companyDivision=" + encodeURI(companyDivision);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
     setTimeout(function(){findBookingAgentName();},1000);
}
function handleHttpResponse222()
        { 
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
                 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value= results;                   
                 }
             }
        }
        
function getCompanyDivisionByJob() {
	var custJobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	var url="findCompanyDivisionByJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse3333;
    http6.send(null);
    setTimeout(function(){checkBookingAgent();},400); 
}
	
function handleHttpResponse3333()
        {
		    if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@");
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++)
 					{
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;
					}
			}	
        } 
        
function findCompanyDivisionByBookAg(loadParam) {
    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCheckedBy'].value="";
	var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
    http6.open("GET", url, true);
    http6.onreadystatechange = function() {handleHttpResponse4444(loadParam)};
    http6.send(null);
}
	
function handleHttpResponse4444(loadParamRes){
		    if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@"); 
                var companyDivision= document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value
                var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value; 
                if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){ 
                if(res.indexOf(companyDivision)!=-1){  
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
                    }
                    document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value='${serviceOrder.companyDivision}';
                }
                else{
                	try
                	{
                	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value='${serviceOrder.bookingAgentCode}';
                	//document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value=encodeURI("${serviceOrder.bookingAgentName}");
                	}
                	catch(err){}
                }
                }
                else{
                	var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						if(res.length-1 == 1){
							document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;
						} else {
						///document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=false;
						}
					}
			}
			if(loadParamRes!='onload')
			getJobList('unload');
			}	
        }   
          

</script>
<script language="javascript" type="text/javascript">
 var r={
'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\=']/g,
'quotes':/['\''&'\"']/g,
'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
targetElement.value = targetElement.value.replace(r[w],'');
}
</script>
<script language="javascript" type="text/javascript">
		function change() {
		       if( document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "BOAT" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "AUTO" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "HHG/A") {
		       	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = false ;
			  }else{
			  	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = true ;
			  }
		}
</script>
<SCRIPT LANGUAGE="JavaScript">
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    var http5 = getHTTPObject5();
    var http6 = getHTTPObject();
   var http9 = getHTTPObject9();
  var http33 = getHTTPObject33();
  var httpHVY = getHTTPObjectHVY();
  var http99 = getHTTPObject();
  function getHTTPObjectHVY()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
   function getHTTPObject9()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject5()
    {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http99 = getHTTPObject9();
     function getHTTPObject9()
    {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
</script>



<SCRIPT LANGUAGE="JavaScript">

// A function for populating the commodity on the basis of different job type.

function getCommodity(){
    var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var url;    
    if(jobType=='STO'|| jobType=='STF'|| jobType=='STL' || jobType=='TPS' ){
      url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODITS');
     }
     else
     {
       url="getCommodity.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(jobType)+"&commodityParameter=" + encodeURI('COMMODIT');      
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse100;
     http2.send(null);
}

function handleHttpResponse100()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.replace("{",'');
                res = res.replace("}",'');
                res = res.split(",");
                targetElementValue=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'];
				targetElement.length = res.length;
				
				var loc=0;
				var flag=0;
				var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 				for(i=0;i<res.length;i++)
					{
 					
					  if(res[i] == ''){
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text = '';
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value = '';
					}
					else
					{
						 
					   stateVal = res[i].replace("=",'+');
					   stateVal=stateVal.split("+");
					  // if((stateVal[0].trim())==targetElementValue){
				//	   loc=i;
					//   flag=1;
					//   }
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text=stateVal[1];
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value=stateVal[0].trim();
					   if(job == "")
					   {
					   	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					  	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					   }
					}
             
             }
         //    if(flag==1){
               //  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[loc].selected=true;
			   //  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[loc].selected=true;
          //   }
           document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value=targetElementValue;
       }
       
   }      

//  End Of function.

// A function for populating the booking agent code.

function findBookingAgentName(){
    var bookingAgentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
    	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = "";
    	
    }
    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value = "";
    if(bookingAgentCode != ''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } 
}

function handleHttpResponse4(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
	           			document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = res[1];
                   		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
                	}else{
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
	           		}  
                }else{
                     alert("Booking Agent code not valid");
                     document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
               }
        }
} 
        
// End Of function.

 function getState(targetElement) {
   var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpHVY.open("GET", url, true);
     httpHVY.onreadystatechange = handleHttpResponse7;
     httpHVY.send(null);
	}
	
function getDestinationState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse8;
     http3.send(null);
	}
	
//  End of function.
    
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    

function findWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
     
}
}

function handleHttpResponse3()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value = '${miscellaneous.actualGrossWeight}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 	calcNetWeight3();
                 }
 				}
                 
             }
        }     
        
//  End of function.
    
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    

function findNetWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     }
}

function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 }
 				}
                 
             }
        } 
            
//  End of function.
    
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    


function findTareWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
}
}

function handleHttpResponse6()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
               
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value = '${miscellaneous.actualTareWeight}';
                 	calcNetWeight3();	
                 }
 				}
                 
             }
        }     

//  End of function.
    
//  function used to put the alert to indicate that volume is available in Conatainer/Carton section. 
    


function findVolumeWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
}
}

function handleHttpResponse5()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 }
 				}
                 
             }
        }     

//  End of function.


//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    

function findWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse33;
     http4.send(null);
     
}
}

function handleHttpResponse33()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value = '${miscellaneous.actualGrossWeightKilo}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 	calcNetWeightKgs3();
                 }
 				}
                 
             }
        }     
        
//  End of function.
    
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    

function findNetWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse22;
     http4.send(null);
     }
}

function handleHttpResponse22()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 }
 				}
                 
             }
        } 
            
//  End of function.
    
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
    


function findTareWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse66;
     http4.send(null);
}
}

function handleHttpResponse66()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
               
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value = '${miscellaneous.actualTareWeightKilo}';
                 	calcNetWeightKgs3();	
                 }
 				}
                 
             }
        }     

//  End of function.
    
//  function used to put the alert to indicate that volume is available in Conatainer/Carton section. 
    


function findVolumeWeightCbm(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightCbmList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse55;
     http4.send(null);
}
}

function handleHttpResponse55()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 }
 				}
                 
             }
        }     

//  End of function.







String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}




function handleHttpResponse7()
        {
             if (httpHVY.readyState == 4)
             {
                var results = httpHVY.responseText
                results = results.trim();                
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ document.getElementById("originState").value == '${serviceOrder.originState}';
					 }								
					
             
        }  
        
function handleHttpResponse8()
        {
             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value == '${serviceOrder.destinationState}';
					   }
             }
        
        

                      
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();


function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject4();



</script>

<script language="javascript" type="text/javascript">
function statusCheck()
{
	var status = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value; 
}
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	var tim1=document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value; 
	
	if(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value == "null" ){
		document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value="";
		}
		if(document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value="";
	} 
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value="";
	}
	 var f = document.getElementById('serviceOrderForm'); 
		f.setAttribute("autocomplete", "off");
	var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
		var el = document.getElementById('hid');
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.visibility = 'visible';
		}else{
		el.style.visibility = 'collapse';
		}
		var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		var el = document.getElementById('hidDM');
		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
		el.style.display = 'block';
		el.style.visibility = 'visible';		
		}else{
		el.style.display = 'collapse';
		}
}

function initLoader() { 
  var script = document.createElement("script");    
  script.src = "http://maps.google.com/jsapi?autoload=%7B%22modules%22%3A%5B%7B%22name%22%3A%22maps&amp;key="+googlekey+"&callback=loadMaps"; 
  script.type = "text/javascript"; 
  document.getElementsByTagName("head")[0].appendChild(script); 
  setTimeout('initialize()',3000);
}
function waitForMapToLoad(){
}
function loadMaps() { 
  google.load("maps", "2", {"callback" : waitForMapToLoad}); 
}


function validAutoBoat()
{
      var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
		var el = document.getElementById('hid');
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.display = 'block';
		el.style.visibility = 'visible';
		}else{
		el.style.display = 'none';
		}
		}
function validateDistanceUnit()
{
      var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		var el = document.getElementById('hidDM');
		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
		el.style.display = 'block';
		el.style.visibility = 'visible';		
		}else{
		el.style.display = 'none';
		}
		}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_statusDate(targetElement) { 
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['serviceOrder.statusDate'].value=datam;
		
	}
	
	function copyCompanyToDestination(targetElement){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = targetElement.value;
	}	
</script>
<script type="text/javascript">
function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
}

//  End of function.
    
//  function used to calculate the entitle net weight. 
    
function calcNetWeight1(){
var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value);
var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value);

var E1='';
	if(Q1<Q2)
	{
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q2 != undefined)
			{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=Q6;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
	}
	else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the estimate net weight. 
    

function calcNetWeight2(){
var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value);
var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value);

var E1='';
	if(Q1<Q2)
	{
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q2 != undefined)
			{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=Q6;
	}
	else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the actual net weight. 
    


function calcNetWeight3(){
			
		var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value);
     	var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value);
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=Q4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=Q6;
			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the rwgh net weight. 
    


function calcNetWeight4(){
	
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value);
		var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value);
		
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{	E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=Q6;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the chargeable net weight. 
    

function calcNetWeight5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value);
		var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value);
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				 var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=Q4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=Q6;

			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}
	}


//  End of function.
function calcGrossVolume1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=0;
			}
			
	}


//  End of function.    

function calcGrossVolume2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=0;
			}
			
	}


//  End of function. 
function calcGrossVolume3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=0;
			}
			
	}


//  End of function. 
function calcGrossVolume4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=0;
			}
			
	}


//  End of function. 
function calcGrossVolume5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=0;
			}
	}


//  End of function. 

function calcNetVolume1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=0;
			}
	}


//  End of function. 

function calcNetVolume2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=0;
			}
	}


//  End of function. 
function calcNetVolume3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=0;
			}
	}


//  End of function. 
function calcNetVolume4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=0;
			}
	}


//  End of function. 
function calcNetVolume5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=0;
			}
	}


//  End of function. 


function calcNetVolumeMtr1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=0;
			}
	}


//  End of function. 

function calcNetVolumeMtr2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcNetVolumeMtr3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcNetVolumeMtr4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcNetVolumeMtr5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=0;
			}
	}


//  End of function. 

function calcGrossVolumeMtr1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=0;
			}
	}


//  End of function. 

function calcGrossVolumeMtr2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcGrossVolumeMtr3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcGrossVolumeMtr4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=0;
			}
	}


//  End of function. 
function calcGrossVolumeMtr5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value=E3;
			}
			else{
			 document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value =0;
			}
			
	}


//  End of function. 

function calcNetWeightKgs1(){
var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value);
var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value);

var E1='';
	if(Q1<Q2)
	{
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q2 != undefined)
			{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
		        var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=Q6;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
	}
	else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
			}
}

//  End of function.
   

function calcNetWeightKgs2(){
var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value);
var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value);

var E1='';
	if(Q1<Q2)
	{
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q2 != undefined)
			{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
		        var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=Q6;
	}
	else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the actual net weight. 
    


function calcNetWeightKgs3(){
			
		var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value);
     	var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value);
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=Q4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=Q6;
			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the rwgh net weight. 
    


function calcNetWeightKgs4(){
	
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value);
		var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value);
		
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{	E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=Q6;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
			}
}

//  End of function.
    
//  function used to calculate the chargeable net weight. 
    

function calcNetWeightKgs5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value);
		var Q2 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value);
		var E1='';
			if(Q1<Q2)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			}
			else if(Q1 != undefined && Q2 != undefined)
			{	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				 var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=Q4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=Q6;

			}
			else if(Q1 == undefined && Q2 == undefined)
			{
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
			}
			else if(Q2 == undefined)
			{
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
			}
			else if(Q1 == undefined)
			{
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			}
	}


//  End of function.

function calcNetWeightLbs1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
			}
	}

function calcNetWeightLbs2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
			}
	}
	
	function calcNetWeightLbs3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
			}
	}
	
	
	function calcNetWeightLbs4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
			}
	}
	
	function calcNetWeightLbs5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
			}
	}

//  End of function.

function calcNetWeightOnlyKgs1(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
			}
	}

function calcNetWeightOnlyKgs2(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
			}
	}
	
	function calcNetWeightOnlyKgs3(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
			}
	}
	
	
	function calcNetWeightOnlyKgs4(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
			}
	}
	
	function calcNetWeightOnlyKgs5(){
        var Q1 = eval(document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value);
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
			}
	}



</script>

<script type="text/javascript">

// function for validations.

  var digits = "0123456789";
  var phoneNumberDelimiters = "()- ";
  var validWorldPhoneChars = phoneNumberDelimiters + "+";
  var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
    
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
 
 // End of function.
 
</script>

<script language="JavaScript">
// function for validations.

	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
// End of function.

function changeOriginTicketAdd() {
        var agree = confirm("Do you want to update address in corresponding Tickets? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('changeOriginTicketAdd.html?id=${serviceOrder.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
		}
function changeDestinTicketAdd() {
	    var agree = confirm("Do you want to update address in corresponding Tickets? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('changeDestinTicketAdd.html?id=${serviceOrder.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
}
function disabledOriginTicketBtn()
		{
			document.forms['serviceOrderForm'].changeOriginTicket.disabled=true;
		}
function disabledDestinTicketBtn()
		{
			document.forms['serviceOrderForm'].changeDestinTicket.disabled=true;
		}	
function disabledOriginTicketBtnReset()
		{	 <c:if test="${serviceOrder.job !='RLO'}">
			document.forms['serviceOrderForm'].changeOriginTicket.disabled=false;	 </c:if>
		}
function disabledDestinTicketBtnReset()
		{	<c:if test="${serviceOrder.job !='RLO'}">
			document.forms['serviceOrderForm'].changeDestinTicket.disabled=false;	</c:if>
		}	
</script>

<script type="text/javascript">
	function autoPopulate_serviceOrder_originCountry(targetElement) {
		var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value=originCountryCode.substring(0,originCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+1,originCountryCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_serviceOrder_destinationCountry(targetElement) {
		var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value=destinationCountryCode.substring(0,destinationCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+1,destinationCountryCode.length);
	}
</script>
<script>
	function wrongValue(field, event) {
		var key, keyChar;
		if(window.event) {
			key = window.event.keyCode;
		}else if(event) {
			key = event.which;
		}
		else
			return true;
		
		keyChar = String.fromCharCode(key);
		
		if((key==null) || (key==0) || (key==8) ||(key== 9) || (key==13) || (key==27)) {
			window.status="";
			return true;
		}
		else {
		if((("01234567890").indexOf(keyChar)>-1)) {
		
			window.status="";
			return true;
		}else {
		
			window.status="It accepts only numeric value";
			alert("It accepts only numeric value");
			return false;
		}
		}
	}
</script>


<script type="text/javascript">
	function openOriginLocation() {
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		var address = document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+country+','+city+','+zip+','+address);
	}
	
	function openDestinationLocation() {
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
		var address = document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+country+','+city+','+zip+','+address);
	}
</script>

<script type="text/javascript"> 
		function forwardToMyMessage1(){
			 
		    if(${serviceOrder.job!='RLO'})
		    {
     		document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimateCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimateGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimatedNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;		
			}
			
			 
			} 
		function forwardToMyMessage2(){
			var elementsLen=document.forms['serviceOrderForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
			{
				
						document.forms['serviceOrderForm'].elements[i].disabled=false;
			} 
			var check =saveValidation('saveButton');
			if(check){
		    if(${serviceOrder.job!='RLO'})
		    {
     		document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimateCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimateGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;		
			document.forms['serviceOrderForm'].elements['serviceOrder.estimatedNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;		
			}
			
			return true;
			}else{
             return false;
				}
		}	
function notExists(){
	alert("The Order has not been saved yet, please save Order to continue");
}
function resetAfterSubmit(){
	var goCnt = '-'+document.forms['serviceOrderForm'].elements['submitCnt'].value * 1;
	history.go(goCnt);
}
	</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script type="text/javascript">
	function forGST(targetElement) {
		document.forms['serviceOrderForm'].elements['serviceOrder.job'].value=targetElement.options[targetElement.selectedIndex].value;
		if(targetElement.value == 'GST' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].disabled = false;
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].value="";
			document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].disabled = true;
		}
	}
</script>



<script type="text/javascript">
	function autoPopulate_customerFile_originCountry(targetElement) {
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States')
		{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India')
		{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada')
		{
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
			if(oriCountry == 'United States'){
			document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
			}
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = '';
			if(oriCountry == 'Netherlands'|| oriCountry == 'Austria' ||oriCountry == 'Belgium' ||oriCountry == 'Bulgaria' ||oriCountry == 'Croatia' ||oriCountry == 'Cyprus' ||oriCountry == 'Czech Republic' ||
			oriCountry == 'Denmark' ||oriCountry == 'Estonia' ||oriCountry == 'Finland' ||oriCountry == 'France' ||oriCountry == 'Germany' ||oriCountry == 'Greece' ||oriCountry == 'Hungary' ||
			oriCountry == 'Iceland' ||oriCountry == 'Ireland' ||oriCountry == 'Italy' ||oriCountry == 'Latvia' ||oriCountry == 'Lithuania' ||oriCountry == 'Luxembourg' ||oriCountry == 'Malta' ||
			oriCountry == 'Poland' ||oriCountry == 'Portugal' ||oriCountry == 'Romania' ||oriCountry == 'Slovakia' ||oriCountry == 'Slovenia' ||oriCountry == 'Spain' ||oriCountry == 'Sweden' ||
			oriCountry == 'Turkey' ||oriCountry == 'United Kingdom' ){
				document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
			}
			autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCountry(targetElement) {
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
			if(dCountry == 'United States'){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
			}
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
			if(dCountry== 'Netherlands'|| dCountry== 'Austria' ||dCountry== 'Belgium' ||dCountry== 'Bulgaria' ||dCountry== 'Croatia' ||dCountry== 'Cyprus' ||dCountry== 'Czech Republic' ||
			dCountry== 'Denmark' ||dCountry== 'Estonia' ||dCountry== 'Finland' ||dCountry== 'France' ||dCountry== 'Germany' ||dCountry== 'Greece' ||dCountry== 'Hungary' ||
			dCountry== 'Iceland' ||dCountry== 'Ireland' ||dCountry== 'Italy' ||dCountry== 'Latvia' ||dCountry== 'Lithuania' ||dCountry== 'Luxembourg' ||dCountry== 'Malta' ||
			dCountry== 'Poland' ||dCountry== 'Portugal' ||dCountry== 'Romania' ||dCountry== 'Slovakia' ||dCountry== 'Slovenia' ||dCountry== 'Spain' ||dCountry== 'Sweden' ||
			dCountry== 'Turkey' ||dCountry== 'United Kingdom' ){
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
			}
			autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	}
	
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCityCode(targetElement) {
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		}
			}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_originCityCode(targetElement) {
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		}
		}
</script>
<script type="text/javascript">
	function autoFilter_PartnerType(targetElement) {
		var partnerType=targetElement.options[targetElement.selectedIndex].value;
	
		var el = document.getElementById('hid');
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.visibility = 'visible';
		}else{
		el.style.visibility = 'collapse';
		}
	
	}

// function for booking agent code for job.	
	
function openBookingAgentPopWindow(){
	<configByCorp:fieldVisibility componentId="component.field.vanline">
		var job =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
	    if(job=='DOM'||job=='UVL'||job=='MVL'){
	    	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
				var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
				var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
				javascript:openWindow('destinationPartnersVanline.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=serviceOrder.bookingAgentVanlineCode&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode',1000,550);
			}
			else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
				var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
				javascript:openWindow('bookingAgentCompanyDivisionVanline.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=serviceOrder.bookingAgentVanlineCode&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode',1000,550);
			}
	    }else{
			if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
				var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
				var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
				javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
			}
			else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
				var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
				javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
			}
		}
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
				var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
				var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
				javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
		else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
			var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
			javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
	</configByCorp:fieldVisibility>			
}
function openBookingAgentBookingAgentSetPopWindow(){
	var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
	var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
	javascript:openWindow('bookingAgentPopupWithBookingAgentSet.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
	} 

// End of function

// function for status reason for job through status logic.

function displayStatusReason(targetElement)
  {
	  var statusValue=targetElement.value;
      var serviceOrderStatus1 = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value;
      if(serviceOrderStatus1 == 'NEW' && (statusValue == '2CLS' || statusValue == 'CLSD'))
       {
       		alert("You can not change status NEW to Closed or Ready To Closed");
       		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value;
       }
	  if(statusValue=='CNCL')
	   {
	     document.getElementById("reasonHid").style.display="block";
	     document.getElementById("reasonHid1").style.display="block";
	   }
	  else 
	  {
	    document.getElementById("reasonHid").style.display="none";
	    document.getElementById("reasonHid1").style.display="none";
	  }
	  
	  if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.display="block";
	         document.getElementById("reasonHid3").style.position="absolute";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.display="none";
         	
      }	
  }
  
  // End of function
  
  
</script>
<script type="text/javascript">

// function for status reason for job.	
function findTicketStatus()
{ 
 var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value;
      var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 
     if(serviceOrderStatus=='CNCL'&& oldStatus!='CNCL'){
     var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     var url="findTicketStatus.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(serviceOrderId);
     http99.open("GET", url, true);
     http99.onreadystatechange = handleHttpResponse9999;
     http99.send(null);
     }else {
     cheackStatusReason();
     }
}

function handleHttpResponse9999()
        {
		 var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 	
             if (http99.readyState == 4)
             {
                var results = http99.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results >0)
                {	
                	alert("You can't change current status to cancel as tickets are still open");
		            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus; 
		            cheackStatusReason();
                }
                else { 
                cheackStatusReason();
                }
               }
        }	
	
	
	
function cheackStatusReason()
{
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
      var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 
     if(serviceOrderStatus=='CNCL'&& oldStatus!='CNCL'){
     var agree = confirm("Are you sure you want to cancel this Service Order?");
     if(agree){ 
     document.getElementById("reasonHid").style.display="block";
	 document.getElementById("reasonHid1").style.display="block";
	 autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	 }else{document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
         	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';}
     }
      else{
      if(serviceOrderStatus=='CNCL'){
      document.getElementById("reasonHid").style.display="block";
	  document.getElementById("reasonHid1").style.display="block";
      }else {
            document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
         	}
         	}
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.display="block";
	         document.getElementById("reasonHid3").style.position="absolute";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.display="none";
         	document.forms['serviceOrderForm'].elements['serviceOrder.nextCheckOn'].value="";
         	
      }
}

function cheackStatusReasonLoad(){
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
        if(serviceOrderStatus=='CNCL'){
	         document.getElementById("reasonHid").style.display="block";
	         document.getElementById("reasonHid1").style.display="block";
       	}else{ 
         	document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
      }
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.display="block";
	         document.getElementById("reasonHid3").style.position="absolute";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.display="none";
         	
      }
}

// End of function
function cheackStatusReasonReset(){
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['oldStatus'].value; 
        if(serviceOrderStatus=='CNCL'){
	         document.getElementById("reasonHid").style.display="block";
	         document.getElementById("reasonHid1").style.display="block";
       	}else{ 
         	document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
      }
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.display="block";
	         document.getElementById("reasonHid3").style.position="absolute";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.display="none"; 
      }
}
</script>
<script type="text/javascript">

// function for checking the limit of service order.

   function addServiceOrder(){
   			   var listSize='${shipSize}';
   			   if(listSize == 99){
					alert('Maximum limit of service order is 99 for a customer.');
				  return false;  
				}
			var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
			var url = "editServiceOrder.html?cid="+${customerFile.id};	 
			document.forms['serviceOrderForm'].action= url;	 
 			document.forms['serviceOrderForm'].submit();
			return true;
}
   

 function addCopyServiceOrder(){
   			 var listSize='${shipSize}';
   			 if(listSize == 99){
				alert('Maximum limit of service order is 99 for a customer.');
			  return false;  
				}
		    var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
		    var url = "addAndCopyServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
			document.forms['serviceOrderForm'].action= url;	 
 		document.forms['serviceOrderForm'].submit();
		return true;
     } 

   function addCopyForwardingServiceOrder(){
	   var listSize='${shipSize}';
	   if(listSize == 99){
			alert('Maximum limit of service order is 99 for a customer.');
		    return false;  
		}
	    var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	    var url = "addAndCopyServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}&forwarding=fwd"; 
		document.forms['serviceOrderForm'].action= url;	 
	    document.forms['serviceOrderForm'].submit();
	return true;
  } 
// End of function
</script>
<script type="text/javascript">
function checkJobType(targetElement){  
  if(targetElement.value=='DOM'||targetElement.value=='UVL'||targetElement.value=='MVL'){
   		document.getElementById("hidRegNum").style.display="block";
   		document.getElementById("hidVanLine").style.display="block";
  }else{
      	document.getElementById("hidRegNum").style.display="none";
      	document.getElementById("hidVanLine").style.display="none";
  }
}

function checkJobTypeOnLoad(){     
    var job='${serviceOrder.job}';
    if(job=='DOM'||job=='UVL'||job=='MVL'){
      	document.getElementById("hidRegNum").style.display="block"; 
      	document.getElementById("hidVanLine").style.display="block";
    }else{
     	document.getElementById("hidRegNum").style.display="none"; 
     	document.getElementById("hidVanLine").style.display="none";
    }
}
 
 function checkJobTypeOnReset(){ 
    var job =document.forms['serviceOrderForm'].elements['jobtypeSO'].value; 
    if(job=='DOM'||job=='UVL'||job=='MVL'){
      	document.getElementById("hidRegNum").style.display="block"; 
      	document.getElementById("hidVanLine").style.display="block";
    }else{
     	document.getElementById("hidRegNum").style.display="none"; 
     	document.getElementById("hidVanLine").style.display="none";
    }
} 
// Function for getting booking agent code through magnifying glass.
  
  function findJobSeq()
  { 
     
     var registrationNumber= document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value
     var bookingAgentCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value
     
     registrationNumber =registrationNumber.trim();
     bookingAgentCode =bookingAgentCode.trim();
     if(registrationNumber==''&& bookingAgentCode=='')
     {
        alert("Please select Booking Agent Code");
     }
     if(registrationNumber==''&& bookingAgentCode!='')
     { 
     var url="getJobSeqNumber.html?ajax=1&decorator=simple&popup=true&bookingAgentCode="+encodeURI(bookingAgentCode);
     http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponse14; 
     http5.send(null); 
     }
  } 
  function handleHttpResponse14()
        {
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();  
               var res = results.split("@");  
               if(results.length >= 2){ 
               if(res[2]=='')
               {
                  res[2]='00001';
               } 
               var mydate=new Date();
		       var daym;
		       var year1 = mydate.getFullYear();
		       var str = ""+year1;
		       var yearLastChar=str.substring( str.length-1, str.length);
                if(res[1]!='')
                {
                  document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value=res[1]+'-'+ res[2]+'-'+yearLastChar;
                  document.forms['serviceOrderForm'].elements['vanlineseqNum'].value=res[2];
                  document.forms['serviceOrderForm'].elements['vanlineCodeForRef'].value=res[1]; 
                }
             }
             }
     }
     
  function validateRegistrationNumber(){
     var regNum= document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value;
     var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 	 regNum =regNum.trim();
     job =job.trim();
     if(job=='UVL' || job=='MVL')
     {
     regNum =regNum.trim();
     var url="validateRegistrationNumber.html?ajax=1&decorator=simple&popup=true&regNum="+encodeURI(regNum);
     http6.open("GET", url, true); 
     http6.onreadystatechange = handleHttpResponse74; 
     http6.send(null); 
  }
  }
  //By Vivek for A/C Ref#
  
  function validateRegistraionNumberAC(x){
     var  regNum= document.getElementById(x).value;
      var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 	  regNum =regNum.trim();
      job =job.trim();
      if(job=='UVL' || job=='MVL')
      {
      
         if((regNum == null)||(regNum =="")){ }
         else{
         var checkFirst =regNum.indexOf("-");
         var checkLast  =regNum.lastIndexOf("-");
              if(checkFirst == 5 &&  checkLast == 11 && regNum.length == 13){
         
              }else{
                     
              alert('Registration Number format is incorrect');
             }         }   } }
    function handleHttpResponse74() {
             if (http6.readyState == 4)
             {
               var results = http6.responseText
               results = results.trim(); 
               if(results > 0){ 
                alert("Reg # is already assigned Please write another");  
                  document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value='';
                } else {
              }
     } 
     }
     function checkStatus()
     {
     	var status=document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
     	var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value;
     	var oldStatusNumber =document.forms['serviceOrderForm'].elements['oldStatusNumber'].value;
     	var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
     	if((jobType!='STO')&& (jobType!='TPS')&& (jobType!='STF')&&(jobType!='STL')){
     	if(status=='CNCL' || status=='HOLD' || status=='REOP' || status=='2CLS'|| status=='CLSD' || status=='TCNL'){
     	if(status=='CLSD' || status=='2CLS'){
     	if(oldStatus=='2CLS'||oldStatus=='CNCL'||oldStatus=='DWNLD'){
     	var agree = confirm("Are you sure you want to close this Service Order?");
         if(agree){
         autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
          if(status=='2CLS'){
          document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          else if(status=='CLSD'){
          document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
        }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	}
     	else if(oldStatus!='CNCL' && oldStatus!='DWNLD'){
     	<c:if test="${not empty serviceOrder.id}">
     	if(document.forms['serviceOrderForm'].elements['billing.billCompleteA'].value!='A' && document.forms['serviceOrderForm'].elements['billing.billComplete'].value=='' && status =='CLSD'){ 
     		alert('Job cannot be closed as billing has not been completed');
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	} else 
     	{
     	  if(document.forms['serviceOrderForm'].elements['claimCount'].value!='0'){
     	   alert("There are open claim against the Service Order.") 
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}'; 
     	} else {
     	if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value==''){
     	var agree = confirm("The service order has not been delivered, do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	
     	} else {
     	
     	if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value!=''){ 
     	var date1 = document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value;	 
        var mySplitResult = date1.split("-");
        var day = mySplitResult[0];
        var month = mySplitResult[1];
        var year = mySplitResult[2]; 
        if(month == 'Jan')
        {
         month = "01";
        }
        else if(month == 'Feb')
        {
          month = "02";
        }
        else if(month == 'Mar')
        {
         month = "03"
        }
        else if(month == 'Apr')
        {
         month = "04"
        }
        else if(month == 'May')
        {
          month = "05"
        }
        else if(month == 'Jun')
        {
          month = "06"
        }
        else if(month == 'Jul')
        {
          month = "07"
        }
        else if(month == 'Aug')
        {
         month = "08"
        }
        else if(month == 'Sep')
        {
         month = "09"
        }
        else if(month == 'Oct')
        {
         month = "10"
        }
        else if(month == 'Nov')
        {
         month = "11"
        }
        else if(month == 'Dec')
        {
         month = "12";
        }
        year="20"+year; 
        var finalDate = month+"-"+day+"-"+year;  
        date1 = finalDate.split("-"); 
        var deliveryADate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);  
        var sysDate = new Date();  
        var daysApart = Math.round((deliveryADate-sysDate)/86400000); 
     	 if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value!='' && daysApart>0){
     	 var agree = confirm("It's been "+daysApart+ "days after delivery,do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	
     	}     	}     	}     	}     	}
     	</c:if>
     	<c:if test="${empty serviceOrder.id}">
     	var agree = confirm("This is new service order, do you  want to cloase this service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          }  
     	</c:if>
     	} 
     	}
     	if(status=='HOLD') {
        if(eval(oldStatusNumber)<10){
        document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
        }
     	if(!(eval(oldStatusNumber)<10)){
     	alert("You can't change current status to HOLD");
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	}
     	}
     	if(status=='REOP'){
     	if(oldStatus=='HOLD'||oldStatus=='CNCL'||oldStatus=='2CLS'){
     	<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
     	alert("Cannot change current status to Reopen as the Customer File is not active.");
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	</c:if>
     	<c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
     	</c:if>
     	} 
     	else if (oldStatus!='HOLD' && oldStatus!='CNCL' && oldStatus!='2CLS'){
     		if(oldStatus=='CLSD'){
     		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status;
     		}else{
     	  alert("You can't change current status to Reopen");
     	  document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	}
     	}
     	}
     	}
     	else { 
     	if(status =='NEW' && oldStatus == 'DWNLD'){ 
     	autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status;
     	document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=1;
     	} else {
     		if(oldStatus=='CLSD'){
     			alert("You can select only Reopen status");
     		}else{
     	alert("You can select only Cancelled, Hold, Reopen, Ready To Close and Closed status");
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
        document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
     	}
        }     	}     	}
     	else if((jobType=='STO')|| (jobType=='TPS')|| (jobType=='STF')||(jobType=='STL')){
     	if(status !='CNCL'){
     	 autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
     	}
     	}
       findTicketStatus();
     } 
     
  function  changebAgentCheckedBy() 
  {
  document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCheckedBy'].value="";
  }
    function sendEmail(target)
   {
   		var originEmail = target;
   		var subject = 'S/O# ${serviceOrder.shipNumber}';
   		subject = subject+" ${serviceOrder.firstName}";
   		subject = subject+" ${serviceOrder.lastName}";   		
		var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		
		}
		function copyToEmail1(target)
		{
			if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value=='')
			{
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value=target;
			}
		
		}
		
		function copyToEmail2(target)
		{
			if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value=='')
			{
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value=target;
			}
		
		}
// End of Function 
    function chkSelect()
	{
		if (checkFloat('serviceOrderForm','miscellaneous.entitleGrossWeight','Invalid data in entitleGrossWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateGrossWeight','Invalid data in estimateGrossWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualGrossWeight','Invalid data in actualGrossWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableGrossWeight','Invalid data in chargeableGrossWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghGross','Invalid data in grossreweight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleTareWeight','Invalid data in entitleTareWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateTareWeight','Invalid data in estimateTareWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualTareWeight','Invalid data in actualTareWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableTareWeight','Invalid data in chargeableTareWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghTare','Invalid data in reweighttare') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleNetWeight','Invalid data in entitleNetWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimatedNetWeight','Invalid data in estimatedNetWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualNetWeight','Invalid data in actualNetWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableNetWeight','Invalid data in chargeableNetWeight') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghNet','Invalid data in rwghNet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleCubicFeet','Invalid data in entitleCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateCubicFeet','Invalid data in estimateCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualCubicFeet','Invalid data in actualCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableCubicFeet','Invalid data in chargeableCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghCubicFeet','Invalid data in rwghCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netEntitleCubicFeet','Invalid data in netEntitleCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netEstimateCubicFeet','Invalid data in netEstimateCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netActualCubicFeet','Invalid data in netActualCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableNetCubicFeet','Invalid data in chargeableNetCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghNetCubicFeet','Invalid data in rwghNetCubicFeet') == false)
           {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].focus();
              return false
           }
	} 
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editServiceOrderUpdate.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrl(id)
	{
		location.href = "editServiceOrderUpdate.html?id="+id;
	}
	 
	 
	function checkTransferDate(){
	var companyDivision='${serviceOrder.companyDivision}';
	if(companyDivision!= ""){
	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
	  alert("You can not change company division as accounting data has been transferred to Solomon");
               document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value='${serviceOrder.companyDivision}';
	}
	}
	}
	function onLoadcheckTransferDate(){
	 var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	 var url="checkTransferDate.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttponLoadTransferDate; 
     http5.send(null);  
	}
	 function  handleHttponLoadTransferDate(){ 
	   if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim(); 
               if(results.length>2){  
               document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].readOnly=true;
               document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
               document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper';
               document.forms['serviceOrderForm'].elements['transferDateFlag'].value="yes"; 
               document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].readOnly = true;
     		   document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].className = 'input-textUpper';
               //document.getElementById("serviceOrder.bookingAgentCode.img").style.display="none";
               
               }
             }
	 }
function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Lbs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Kgs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cbm';
  }
} 
 function openStandardAddressesPopWindow(){
 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
	javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.originMobile&fld_ninthDescription=serviceOrder.originCity&fld_eigthDescription=serviceOrder.originFax&fld_seventhDescription=serviceOrder.originHomePhone&fld_sixthDescription=serviceOrder.originDayPhone&fld_fifthDescription=serviceOrder.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=serviceOrder.originCountry&fld_secondDescription=serviceOrder.originAddressLine3&fld_description=serviceOrder.originAddressLine2&fld_code=serviceOrder.originAddressLine1');
document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
function openStandardAddressesDestinationPopWindow(){
 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.destinationMobile&fld_ninthDescription=serviceOrder.destinationCity&fld_eigthDescription=serviceOrder.destinationFax&fld_seventhDescription=serviceOrder.destinationHomePhone&fld_sixthDescription=serviceOrder.destinationDayPhone&fld_fifthDescription=serviceOrder.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=serviceOrder.destinationCountry&fld_secondDescription=serviceOrder.destinationAddressLine3&fld_description=serviceOrder.destinationAddressLine2&fld_code=serviceOrder.destinationAddressLine1');
document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
 
 function enableState(){
var id=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
var AddSection = document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
if(id!=''){
document.forms['serviceOrderForm'].changeOriginTicket.disabled=true;
}
	var originCountry = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value
getOriginCountryCode();
	var countryCode = "";
	if(originCountry == 'United States'){
		countryCode = "USA";
	}
	if(originCountry == 'India'){
		countryCode = "IND";
	}
	if(originCountry == 'Canada'){
		countryCode = "CAN";
	}
	
if(countryCode == 'IND' || countryCode == 'CAN' || countryCode == 'USA'){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =false;
		
		}else{
			
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value="";
		}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);

}
if(AddSection=='destinAddSection'){
if(id!=''){
document.forms['serviceOrderForm'].changeDestinTicket.disabled=true;
}
	var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value
	getDestinationCountryCode();
	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
if(countryCode == 'IND' || countryCode == 'CAN' || countryCode == 'USA'){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =false;
		
		}else{
			
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value="";
		}
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);
}

}


function handleHttpResponse11(){
		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
                var stdAddDestinState=document.forms['serviceOrderForm'].elements['stdAddDestinState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = stdAddDestinState;
					if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		}
        
        }
}  
function handleHttpResponse12(){
		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
                var stdAddOriginState=document.forms['serviceOrderForm'].elements['stdAddOriginState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = stdAddOriginState;
        if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		}
        }
}  

function getVanlineCode(targetField, position) {
	var partnerCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	<configByCorp:fieldVisibility componentId="component.field.vanline">
		if(partnerCode != ''){
			var job =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
    		if(job=='DOM'||job=='UVL'||job=='MVL'){
				var partnerCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
			 	var url="findVanLineList.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode) +"&agentType="+ encodeURI(targetField);
			  	position = document.getElementById('navigations');
			  	ajax_showTooltip(url,position);
			}  	
		}
	</configByCorp:fieldVisibility>	
	setTimeout("getBigPartnerAlert(position,'noteNavigations')",2000);
}

function goToUrlVanline(id, targetField){
	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value = id;
}

function getVanlineAgent(targetField){
	var vanlineCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value;
	if(vanlineCode != ''){
		var url="findVanLineAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vanlineCode);
    	http9.open("GET", url, true);
    	http9.onreadystatechange = function() {handleHttpResponseVanlineAgent(targetField);};
    	http9.send(null);
	}
}

function handleHttpResponseVanlineAgent(targetField){
	if (http9.readyState == 4){
		var results = http9.responseText
        results = results.trim();

        if(results.length>1){
	        res = results.split("@");
	
	        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'];
	        
	        targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res.length == 2){
					stateVal = res[i].split("~");
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = '';
				}else{
					stateVal = res[i].split("~");
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value = stateVal[0];
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
				}
			}
        }else{
        	alert("Invalid vanline code");
        	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value='';
       	}	
    }
} 
var http50 = getHTTPObject50();
	function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http51 = getHTTPObject51();
	function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http77 = getHTTPObject77();
function getHTTPObject77()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getNewCoordAndSale(){
	 document.getElementById("coordinator").value='';   
	    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	    var soNetworkGrp =false;
	    var accNetworkGrp = false;
	    var checkUTSI="${company.UTSI}";
	    <c:if test="${not empty serviceOrder.id}">
	        soNetworkGrp= ${trackingStatus.soNetworkGroup};
	        accNetworkGrp= ${trackingStatus.accNetworkGroup};
	    </c:if>
	    var checkSoFlag = true;
	    var bookAgCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	   if(job!=''){
	     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&soNetworkGrp="+encodeURI(soNetworkGrp)+"&accNetworkGrp="+encodeURI(accNetworkGrp)+"&checkSoFlag="+encodeURI(checkSoFlag)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkUTSI="+encodeURI(checkUTSI);
	     	http50.open("GET", url, true);
	     	http50.onreadystatechange = handleHttpResponse50;
	     	http50.send(null);
	    }   
}
function handleHttpResponse50(){
		
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = stateVal[0];
					
					//if (document.getElementById("coordinator").options[i].value == '${customerFile.coordinator}'){
					//   document.getElementById("coordinator").options[i].defaultSelected = true;
					//}
					}
					}
					document.getElementById("coordinator").value = '${serviceOrder.coordinator}';
					//getSale();
					getConsultant();
        
        }

}

function getConsultant(){
    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
   	if(job!=''){
     	var url = "findSale.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }
    
}
function handleHttpResponse52(){
		
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("salesMan").value = '${serviceOrder.salesMan}';
        
        } 
  }
  
  function fillCheckJob(temp){ 
  document.forms['serviceOrderForm'].elements['jobtypeCheck'].value=temp.value;
  } 
  
  function findService(){
	  try{     var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var hidStatusEstimator1 = document.getElementById('EstimatorService1');
    var labellabelEstimator1 = document.getElementById('labelEstimator');
    var labelserviceVar = document.getElementById('labelservice'); 
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel');  
    var weightserviceVar = document.getElementById('weightVolSection');
    var hidStatusCommoditText1=document.getElementById('hidStatusCommoditText');
    var hidStatusCommoditLabel1=document.getElementById('hidStatusCommoditLabel');    
   	if(job!=''){   		if(job=='RLO'){
   	       hidStatusEstimator1.style.display = 'none';
	       labellabelEstimator1.style.display= 'none';
   		   hidStatusReg5.style.display = 'block';
   		   hidStatusReg4.style.display = 'none';
   		   labelserviceVar.style.display = 'none';
   		   modehidStatusReg.style.display = 'none';
   		   modelabelserviceVar.style.display = 'none';
   		   routinghidStatusReg.style.display = 'none';
   		   routinglabelserviceVar.style.display = 'none';
   		   packhidStatusReg.style.display = 'none';
   		   packlabelserviceVar.style.display = 'none';
   		   weightserviceVar.style.display = 'none';
   		   hidStatusCommoditText1.style.display = 'none';
   		   hidStatusCommoditLabel1.style.display = 'none';
   		   <c:if test="${not empty serviceOrder.id}">
   		   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=true;
   		   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=true;
   		   </c:if>   		      		   
   		      }else{ 
   		       hidStatusEstimator1.style.display = 'block';
   		   	   labellabelEstimator1.style.display= 'block';
   		       hidStatusReg4.style.display = 'block';
   			   hidStatusReg5.style.display = 'none';
   			   labelserviceVar.style.display = 'block';
   			   modehidStatusReg.style.display = 'block';
   			   modelabelserviceVar.style.display = 'block';
   			   routinghidStatusReg.style.display = 'block';
   			   routinglabelserviceVar.style.display = 'block';
   			   packhidStatusReg.style.display = 'block';
   			   packlabelserviceVar.style.display = 'block';	
   	  		   weightserviceVar.style.display = 'block';
   	 	       hidStatusCommoditText1.style.display = 'block';
   		   	   hidStatusCommoditLabel1.style.display = 'block';	 
   			   <c:if test="${not empty serviceOrder.id}">
   			   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=false;
   			   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=false;
   			   </c:if>		   	        	    }    }
	  }catch(e){}
}

function handleHttpResponse54(){
if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();  
                res = results.split("@"); 
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = '';
					}else{
					stateVal = res[i].split("#"); 
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = stateVal[0];
					}
					}
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value = '';
					
        
        } 
  }
  
 var httpState = getHTTPObjectState();
 var httpDState = getHTTPObjectDState();
   function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
  function getHTTPObjectDState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
function newFunctionForCountryState(){
var destinationAbc ='${serviceOrder.destinationCountryCode}';
var originAbc ='${serviceOrder.originCountryCode}';
if(originAbc == "USA" || originAbc == "IND" || originAbc == "CAN"){
document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =false;
getOriginStateReset(originAbc);
}
else{
document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
}
if(destinationAbc == "USA" || destinationAbc == "IND" || destinationAbc == "CAN"){
document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =false;
getDestinationStateReset(destinationAbc);
}
else{
document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
}
}
function getDestinationStateReset(destinationAbc){
	var countryCode = destinationAbc;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpDState.open("GET", url, true);
     httpDState.onreadystatechange = handleHttpResponse6666666666;
     httpDState.send(null);
}
function getOriginStateReset(originAbc) {
	 var countryCode = originAbc;
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse555555;
     httpState.send(null);
}
function handleHttpResponse6666666666(){
		if (httpDState.readyState == 4){
                var results = httpDState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = '${serviceOrder.destinationState}';
        }
}          
   function handleHttpResponse555555(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = '${serviceOrder.originState}';
             }
        } 
        
        
function showPartnerAlert(display, code, position){
     if(code == ''){ 	
     	document.getElementById(position).style.display="none";
     	ajax_hideTooltip();
     }else if(code != ''){
     	getBAPartner(display,code,position);
     }      
}
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     httpPA.open("GET", url, true);
     httpPA.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     httpPA.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (httpPA.readyState == 4){
		var results = httpPA.responseText
        results = results.trim();
        
     if(results.indexOf("Nothing found to display")>1){  
     exit();
      	}else{
      	      if(results.length > 555){
        	document.getElementById(position).style.display="block";
           	if(display != 'onload'){
      		getPartnerAlert(code,position);
          	}
      	}else{
      	  document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	}
      	}
	}
}

var httpPA = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  


var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
  
function getJobList(loadParam){
	var comDiv = "";
	if(document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value != null){
		comDiv = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = function() {handleHttpResponseJob(loadParam)};
   	httpJob.send(null);
}
function handleHttpResponseJob(loadParamRes){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.job'];
        targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = stateVal[1];
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = stateVal[0];
			}
		}
		document.getElementById("jobService").value = '${serviceOrder.job}';
		if(loadParamRes!='onload'){
	getCommodity();
	}
	}
	
}     

function toCheckWeightnVolumeRadio1()
{
     var weightTabSub1  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
     var weightTabSub2  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
	if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio'].checked == true)
	{  if(weightTabSub1%2==0){
        animatedcollapse.toggle('weightnvolumesub1');
        weightTabSub1 =weightTabSub1+1;
        document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
        }    
    }
	else if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio2'].checked ==true)
	{ if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumesub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }	
	}
}
function toCheckWeightnVolumeRadio(){
 var weightTab      =  parseInt(document.forms['serviceOrderForm'].elements['weightTab'].value);
 var weightTabSub1  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
 var weightTabSub2  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
 if(weightTab%2==1){
 
 if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio'].checked==true){
   animatedcollapse.toggle('weightnvolumesub1');
   weightTabSub1 =weightTabSub1+1;
   document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
  }
if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio2'].checked==true){
  animatedcollapse.toggle('weightnvolumesub2');
    weightTabSub2 =weightTabSub2+1;
   document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
  }
  weightTab =weightTab+1;
  document.forms['serviceOrderForm'].elements['weightTab'].value=String(weightTab);
  }else{
       if(weightTabSub1%2==0){
       animatedcollapse.toggle('weightnvolumesub1');
       weightTabSub1 =weightTabSub1+1;
       document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
       }
       if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumesub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }
        weightTab =weightTab+1;
      document.forms['serviceOrderForm'].elements['weightTab'].value=String(weightTab);
     }
  
}
</script>
<script type="text/javascript" >

function toIncrementWeightTabSub1(){ 
 var weightTabSub1=  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
      weightTabSub1 =weightTabSub1+1;
      document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
 }
function  toIncrementWeightTabSub2(){
         var weightTabSub2=  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
          weightTabSub2 =weightTabSub2+1;
         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2); 
}
function toCheckCountryFields(){
	 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL'){
    if(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value=='' || document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value=='')
    {
     if(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value==''){
      alert("Origin country is required field");
      return false;
      }
     if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value==''){
       alert("Destination country is required field");
       return false;
    }
    }
	else{
    submit_form();
    }
	 }else{
		 submit_form();
	 }
}
function openPricing(){
form_submitted = false;
var checkValidation =  submit_form(); 
 if(checkValidation == true){
document.forms['serviceOrderForm'].action = 'saveServiceOrderPricing.html?pricingButton=yes&sid=${serviceOrder.id}';
var check=false;
if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
	 var check = true;
}else{
	var check =saveValidation('saveButton');
}
if(check){
document.forms['serviceOrderForm'].submit();
setTimeout("animatedcollapse.toggle('pricing')", 5000);
}
;
}
}
function checkAccountLineNumber(temp) { 
       var accountLineNumber=document.forms['serviceOrderForm'].elements[temp].value; 
       var accountLineNumber1=document.forms['serviceOrderForm'].elements[temp].value;
       accountLineNumber=accountLineNumber.trim();
       accountLineNumber1=accountLineNumber1.trim();
       if(accountLineNumber!=""){
       if(accountLineNumber>999)
       {
         alert("Manually you can not Enter Line # greater than 999");
         document.forms['serviceOrderForm'].elements[temp].value=0;
       }
       else if(accountLineNumber1.length<2)
       { 
       	document.forms['serviceOrderForm'].elements[temp].value='00'+document.forms['serviceOrderForm'].elements[temp].value;
       }
       else if(accountLineNumber1.length==2)
       { 
       	var aa = document.forms['serviceOrderForm'].elements[temp].value;
       	document.forms['serviceOrderForm'].elements[temp].value='0'+aa;
       }
       }
     }  
function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	}
function fillDiscription(discription,chargeCode,category,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate)
{ 
	var category = document.forms['serviceOrderForm'].elements[category].value;
	var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
	var chargeCode = document.forms['serviceOrderForm'].elements[chargeCode].value;
	var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value;
	var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	var sid='${serviceOrder.id}';   
		var url = "findQuotationDiscription.html?contract="+quotationContract+"&chargeCode="+chargeCode+"&decorator=simple&popup=true&soId="+sid+"&companyDivision="+companyDivision;
     	http50.open("GET", url, true);
     	http50.onreadystatechange =  function(){fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate);};
     	http50.send(null);
		
}
function fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate){ 
   if (http50.readyState == 4)
       {
           var result= http50.responseText
           result = result.trim();   
           result = result.replace('[','');
           result=result.replace(']','');
           var res = result.split("#"); 
           var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value; 
           if(result.indexOf('NoDiscription')<0) {

           if(res[5]==''){   
        	   document.forms['serviceOrderForm'].elements[discription].value = res[0]; 
	           }else{
	        	   document.forms['serviceOrderForm'].elements[discription].value = res[5]; 
	           }
            
           document.forms['serviceOrderForm'].elements[recGl].value = res[1];     
           document.forms['serviceOrderForm'].elements[payGl].value = res[2]; 
           document.forms['serviceOrderForm'].elements[contractCurrency].value = res[3];        
           }
        }
   <c:if test="${contractType}">
   		var contCurrency = document.forms['serviceOrderForm'].elements[contractCurrency].value
    	var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(contCurrency);
	    http77.open("GET", url, true);
	    http77.onreadystatechange = function(){handleHttpResponseContractRate(contractExchangeRate,contractValueDate);};
	    http77.send(null);
	</c:if>
}

function handleHttpResponseContractRate(contractExchangeRate,contractValueDate) { 
    if (http77.readyState == 4) {
       var results = http77.responseText
       results = results.trim(); 
       results = results.replace('[','');
       results=results.replace(']',''); 
       if(results.length>1) {
         document.forms['serviceOrderForm'].elements[contractExchangeRate].value=results 
         } else {
         document.forms['serviceOrderForm'].elements[contractExchangeRate].value=1;
         }
         var mydate=new Date();
         var daym;
         var year=mydate.getFullYear()
         var y=""+year;
         if (year < 1000)
         year+=1900
         var day=mydate.getDay()
         var month=mydate.getMonth()+1
         if(month == 1)month="Jan";
         if(month == 2)month="Feb";
         if(month == 3)month="Mar";
		  if(month == 4)month="Apr";
		  if(month == 5)month="May";
		  if(month == 6)month="Jun";
		  if(month == 7)month="Jul";
		  if(month == 8)month="Aug";
		  if(month == 9)month="Sep";
		  if(month == 10)month="Oct";
		  if(month == 11)month="Nov";
		  if(month == 12)month="Dec";
		  var daym=mydate.getDate()
		  if (daym<10)
		  daym="0"+daym
		  var datam = daym+"-"+month+"-"+y.substring(2,4); 
		  document.forms['serviceOrderForm'].elements[contractValueDate].value=datam;  
 }   } 

 function checkChargeCode(chargeCode,category)
{
    var val = document.forms['serviceOrderForm'].elements[category].value;
	var charge= document.forms['serviceOrderForm'].elements[chargeCode].value;
	var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
	var myRegxp = /([a-zA-Z0-9_-]+)$/;
	if(myRegxp.test(charge)==false){
	   alert( "Please enter a valid charge code." );
       document.forms['serviceOrderForm'].elements[chargeCode].value='';
	
	}
	else{
	if(val=='Internal Cost'){
	 quotationContract="Internal Cost Management";
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(quotationContract)+"&chargeCode="+encodeURIComponent(charge); 
	 http5.open("GET", url, true);
     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category);} ;
     http5.send(null);
	}
	else{
	if(quotationContract=='' || quotationContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	document.forms['serviceOrderForm'].elements[chargeCode].value = "";
	}
	else{
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+charge; 
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){handleHttpResponseChargeCode(chargeCode,category);} ;
     http5.send(null);
	}
	}
	}
}
 function handleHttpResponseChargeCode(chargeCode,category)
 {
      if (http5.readyState == 4)
      {
         var results = http5.responseText
         results = results.trim(); 
         var res = results.split("#");  
         if(results.length>4)
         {
         
          }
          else
          {
             alert("Charge code does not exist according the pricing contract in Pricing Detail, Please select another" );
             document.forms['serviceOrderForm'].elements[chargeCode].value = "";  
		   }
      }
 }
function checkVendorName(vendorCode,estimateVendorName){
    var vendorId = document.forms['serviceOrderForm'].elements[vendorCode].value;
	if(vendorId == ''){
		document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
	}
	if(vendorId != ''){
		var myRegxp = /([a-zA-Z0-9_-]+)$/;
		if(myRegxp.test(vendorId)==false){
			alert("Vendor code not valid" );
            document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
		 	 document.forms['serviceOrderForm'].elements[vendorCode].value="";
		}else{
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http5.open("GET", url, true);
     http5.onreadystatechange =function(){handleHttpResponsevendorCode(vendorCode,estimateVendorName);} ;
     http5.send(null);
    } }
}

function handleHttpResponsevendorCode(vendorCode,estimateVendorName){
		if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                var res = results.split("#"); 
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           			document.forms['serviceOrderForm'].elements[estimateVendorName].value = res[1];
		           			document.forms['serviceOrderForm'].elements[vendorCode].select();
		           		}else{
		           			alert("Vendor code is not approved" ); 
						    document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
						    document.forms['serviceOrderForm'].elements[vendorCode].value="";
						    document.forms['serviceOrderForm'].elements[vendorCode].select();
		           		}
                }else{
                     alert("Vendor code not valid" );
                     document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
				 	 document.forms['serviceOrderForm'].elements[vendorCode].value="";
				 	 document.forms['serviceOrderForm'].elements[vendorCode].select();
			   }
     }
}
   function winOpenForActgCode(vendorCode,vendorName,category)
  {
		
    var sid='${serviceOrder.id}';
 	var val = document.forms['serviceOrderForm'].elements[category].value;
 	//alert(val); 
 	var finalVal = "OK";
 	var indexCheck = val.indexOf('Origin'); 
    openWindow('findVendorCode.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription');
 	document.forms['serviceOrderForm'].elements[vendorCode].select(); 
 }
 function changeBasis(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots){  
  var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
  var basisValue = document.forms['serviceOrderForm'].elements[basis].value; 
  var oldBasis="old"+basis;
  var oldQuantity="old"+quantity;
  var oldQuantityValue = document.forms['serviceOrderForm'].elements[oldQuantity].value;
  var oldBasisValue = document.forms['serviceOrderForm'].elements[oldBasis].value;  
  if((document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="AIR")|| (document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="Air")){
  if(basisValue=='cwt'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
  }else if(basisValue=='kg'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
  }
  else if(basisValue=='cft'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
  }
  else if(basisValue=='cbm'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
  }
  else if(basisValue=='each'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  else if(basisValue=='value'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  else if(basisValue=='flat'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  }
  else{
  if(basisValue=='cwt'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
  }else if(basisValue=='kg'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
  }
  else if(basisValue=='cft'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
  }
  else if(basisValue=='cbm'){
  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
  }
  else if(basisValue=='each'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  else if(basisValue=='value'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  else if(basisValue=='flat'){
  document.forms['serviceOrderForm'].elements[quantity].value=1;
  }
  }
  
calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots);
calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots);
} 
 function calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots){
    var expenseValue=0;
    var expenseRound=0;
    var oldExpense=0;
    var oldExpenseSum=0;
    var balanceExpense=0;
    oldExpense=eval(document.forms['serviceOrderForm'].elements[expense].value);
    <c:if test="${not empty serviceOrder.id}">
    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
    </c:if>
    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
    checkFloatNew('serviceOrderForm',quantity,'Nothing');    
    var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
    checkFloatNew('serviceOrderForm',buyRate,'Nothing');    
    var buyRateValue = document.forms['serviceOrderForm'].elements[buyRate].value;
    if(basisValue=="cwt" || basisValue== "%age"){
    expenseValue=(quantityValue*buyRateValue)/100 ;
    }
    else if(basisValue=="per 1000"){
    expenseValue=(quantityValue*buyRateValue)/1000 ;
    }else{
    expenseValue=(quantityValue*buyRateValue);
    }
    expenseRound=Math.round(expenseValue*10000)/10000;
    balanceExpense=expenseRound-oldExpense;
    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
    balanceExpense=Math.round(balanceExpense*10000)/10000; 
    oldExpenseSum=oldExpenseSum+balanceExpense;
    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
    var expenseRoundNew=""+expenseRound;
    if(expenseRoundNew.indexOf(".") == -1){
    expenseRoundNew=expenseRoundNew+".00";
    } 
    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
    expenseRoundNew=expenseRoundNew+"0";
    }
    document.forms['serviceOrderForm'].elements[expense].value=expenseRoundNew;
    <c:if test="${not empty serviceOrder.id}">
    var newExpenseSum=""+oldExpenseSum;
    if(newExpenseSum.indexOf(".") == -1){
    newExpenseSum=newExpenseSum+".00";
    } 
    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
    newExpenseSum=newExpenseSum+"0";
    } 
    document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
    </c:if>
    if(expenseRoundNew>0){
    document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
    }
    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
    }
 function checkFloatNew(formName,fieldName,message)
 {   var i;
 	var s = document.forms[formName].elements[fieldName].value;
 	var count = 0;
 	if(s.trim()=='.')
 	{
 		document.forms[formName].elements[fieldName].value='0.00';
 	}else{
     for (i = 0; i < s.length; i++)
     {   
         var c = s.charAt(i);
         
         if(c == '.')
         {
         	count = count+1
         }
         if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
         {
 	        document.forms[formName].elements[fieldName].value='0.00';
         }
     }    } 
 }
 function calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots){
    var revenueValue=0;
    var revenueRound=0;
    var oldRevenue=0;
    var oldRevenueSum=0;
    var balanceRevenue=0;
    oldRevenue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
    <c:if test="${not empty serviceOrder.id}">
    oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
    </c:if>
    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
    checkFloatNew('serviceOrderForm',quantity,'Nothing');
    var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
    checkFloatNew('serviceOrderForm',sellRate,'Nothing');
    var sellRateValue = document.forms['serviceOrderForm'].elements[sellRate].value;
    if(basisValue=="cwt" || basisValue== "%age"){
    revenueValue=(quantityValue*sellRateValue)/100 ;
    }
    else if(basisValue=="per 1000"){
    revenueValue=(quantityValue*sellRateValue)/1000 ;
    }else{
    revenueValue=(quantityValue*sellRateValue);
    } 
    revenueRound=Math.round(revenueValue*10000)/10000;
    balanceRevenue=revenueRound-oldRevenue;
    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
    oldRevenueSum=oldRevenueSum+balanceRevenue; 
    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
    var revenueRoundNew=""+revenueRound;
    if(revenueRoundNew.indexOf(".") == -1){
    revenueRoundNew=revenueRoundNew+".00";
    } 
    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
    revenueRoundNew=revenueRoundNew+"0";
    }
    document.forms['serviceOrderForm'].elements[revenue].value=revenueRoundNew;
    <c:if test="${not empty serviceOrder.id}">
    var newRevenueSum=""+oldRevenueSum;
    if(newRevenueSum.indexOf(".") == -1){
    newRevenueSum=newRevenueSum+".00";
    } 
    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
    newRevenueSum=newRevenueSum+"0";
    } 
    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
    </c:if>
    if(revenueRoundNew>0){
    document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
    }
    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
    }
  function onlyRateAllowed(evt)
	{
	
	  var keyCode = evt.which ? evt.which : evt.keyCode; 
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
	}
  function enterNotAllow(evt){
  	var keyCode = evt.which ? evt.which : evt.keyCode;
  	if(keyCode==16)
  	{
      	return false;
  	}
  	else
  	{
      	return true;
  	}
  }
	function checkFloat1(temp)
  { 
    var check='';  
    var i;
    var dotcheck=0;
	var s = temp.value;
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name;  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if(c == '-')
        {
        	countArth = countArth+1
        }
        if((i!=0)&&(c=='-'))
       	{
       	  alert("Invalid data." ); 
          document.forms['serviceOrderForm'].elements[fieldName].select();
          document.forms['serviceOrderForm'].elements[fieldName].value='';
       	  return false;
       	}
        	
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1'))) 
        {
        	check='Invalid'; 
        }
    }
    if(dotcheck>15){
    check='tolong';
    }
    if(check=='Invalid'){ 
    alert("Invalid data." ); 
    document.forms['serviceOrderForm'].elements[fieldName].select();
    document.forms['serviceOrderForm'].elements[fieldName].value='';
    return false;
    } else if(check=='tolong'){ 
    alert("Data Too Long." ); 
     document.forms['serviceOrderForm'].elements[fieldName].select();
    document.forms['serviceOrderForm'].elements[fieldName].value='';
    return false;
    } else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['serviceOrderForm'].elements[fieldName].value=value;
    return true;
    }
    }
     function getAccountlineField(aid,basis,quantity){
  accountLineBasis=document.forms['serviceOrderForm'].elements[basis].value;
  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
  openWindow('getAccountlineField.html?aid='+aid+'&accountLineBasis='+encodeURI(accountLineBasis)+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&decorator=popup&popup=true',920,250);
  
  }
  function changeEstimatePassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots){
       var estimate=0; 
       var estimateround=0;
       var estimateRevenue=0;
       var estimateRevenueRound=0;
       var sellRateValue=0;
       var sellRateround=0;
       var	estimatepasspercentage =0;
       var quantityValue = 0;
       var oldRevenue=0;
       var oldRevenueSum=0;
       var balanceRevenue=0; 
       var buyRateValue=0;  
	   oldRevenue=eval(document.forms['serviceOrderForm'].elements[revenue].value); 
	    <c:if test="${not empty serviceOrder.id}">
        oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
       </c:if> 
       quantityValue = eval(document.forms['serviceOrderForm'].elements[quantity].value); 
       buyRateValue = eval(document.forms['serviceOrderForm'].elements[buyRate].value); 
       estimatepasspercentage= document.forms['serviceOrderForm'].elements[estimatePassPercentage].value;
       estimate = (quantityValue*buyRateValue);
       
   	   if(quantityValue>0 && estimate>0){
   	   if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age"){
       		estimate = estimate/100; 	  	
       	} 
       	if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000"){
       		estimate = estimate/1000; 	  	
       	}
       	estimateRevenue=((estimate)*estimatepasspercentage/100)
       	estimateRevenueRound=Math.round(estimateRevenue*10000)/10000; 
  	    document.forms['serviceOrderForm'].elements[revenue].value=estimateRevenueRound;   
	    if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age"){
       	  sellRateValue=(estimateRevenue/quantityValue)*100;
       	  sellRateround=Math.round(sellRateValue*10000)/10000;
       	  
       	  document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
       	}
	    else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000"){
       	  sellRateValue=(estimateRevenue/quantityValue)*1000;
       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
       	  document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
       	} else if(document.forms['serviceOrderForm'].elements[basis].value!="cwt" || document.forms['serviceOrderForm'].elements[basis].value!="%age"||document.forms['serviceOrderForm'].elements[basis].value!="per 1000")
        {
          sellRateValue=(estimateRevenue/quantityValue);
       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
          document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
	    }
	    balanceRevenue=estimateRevenueRound-oldRevenue;
        oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
        balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
        oldRevenueSum=oldRevenueSum+balanceRevenue;  
        <c:if test="${not empty serviceOrder.id}">
        var newRevenueSum=""+oldRevenueSum;
        if(newRevenueSum.indexOf(".") == -1){
        newRevenueSum=newRevenueSum+".00";
        } 
        if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
        newRevenueSum=newRevenueSum+"0";
        } 
        document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
        </c:if>
       if(estimateRevenueRound>0){
        document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
       }
	 calculateGrossMargin();
//calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots);
//calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots);

}
else{
document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=0;
}
} 	
 	function onlyNumsAllowed1(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
function discriptionView(discription){ 
 document.getElementById(discription).style.height="50px"; 
 } 
  
 function discriptionView1(discription){ 
 document.getElementById(discription).style.height="15px"; 
 SetCursorToTextEnd(discription);
 }
 function SetCursorToTextEnd(textControlID) 
   { 
	var text = document.getElementById(textControlID); 
	text.scrollTop = 1;
   }
function inactiveStatusCheck(rowId,targetElement) 
	   { 
	   if(targetElement.checked==false)
	     {
	      var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
	      if(userCheckStatus == '')
	      {
		  	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = rowId;
	      }
	      else
	      {
	       var userCheckStatus=	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
	      document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
	      }
	    }
	   if(targetElement.checked)
	    {
	     var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
	     var userCheckStatus=document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
	     document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
	     } 
	     accountIdCheck();
	    }
 function accountIdCheck(){
    var accountIdCheck = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
    accountIdCheck=accountIdCheck.trim(); 
    if(accountIdCheck=='')
    {
    document.forms['serviceOrderForm'].elements['Inactivate'].disabled=true;
    }
    else if(accountIdCheck==',')
    {
    document.forms['serviceOrderForm'].elements['Inactivate'].disabled=true;
    }
    else if(accountIdCheck!='')
    {
      document.forms['serviceOrderForm'].elements['Inactivate'].disabled=false;
    }
    }	    
	      
   function findDefaultLine()
{
     try{ 
	document.forms['serviceOrderForm'].elements['jobtypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	document.forms['serviceOrderForm'].elements['routingSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
	document.forms['serviceOrderForm'].elements['commoditySO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
	document.forms['serviceOrderForm'].elements['serviceTypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
	document.forms['serviceOrderForm'].elements['companyDivisionSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	document.forms['serviceOrderForm'].elements['modeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	document.forms['serviceOrderForm'].elements['packingModeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
	} 
catch(e){}
	var customerFileContract=document.forms['serviceOrderForm'].elements['billing.contract'].value;
	if(customerFileContract=='')
	 { 
	 		alert("Please select  Pricing Contract from billing Detail form"); 
	 }
	else
	{
	  document.forms['serviceOrderForm'].action = 'addServiceOrderPricingDefaultLine.html?pricingButton=yes&sid=${serviceOrder.id}';
		var check=false;
		 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
			 var check = true;
		 }else{
			var check =saveValidation('saveButton');
		 }
 	  if(check){
      document.forms['serviceOrderForm'].submit();
      }
	}
	

}
function defaultLineMassage()
{
alert("Default pricing template have already been added.");

}

 function addPricingLine() {
    document.forms['serviceOrderForm'].action ='addServiceOrderPricingLine.html?pricingButton=yes&sid=${serviceOrder.id}';
	var check=false;
	 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
		 var check = true;
	 }else{
		var check =saveValidation('saveButton');
	 }
 	if(check){
    document.forms['serviceOrderForm'].submit();
    } 
    }
function savePricingLine() {
    document.forms['serviceOrderForm'].action ='savePricingLineSO.html?pricingButton=yes&sid=${serviceOrder.id}';
	var check=false;
	 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
		 var check = true;
	 }else{
		var check =saveValidation('saveButton');
	 }
    if(check){
    document.forms['serviceOrderForm'].submit();
    } 
    }    
  function updateAccInactive() {
	 var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	 document.forms['serviceOrderForm'].action ='deletePricingLineSO.html?pricingButton=yes&sid=${serviceOrder.id}';
     document.forms['serviceOrderForm'].submit(); 
	    } 	
    function saveValidation(type){
    	document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].disabled=false;
  if((document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')&&(type=='saveButton')){
	   alert("Last name is required field");
	   document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].disabled=true;
	   showOrHideAutoSave('0');
	   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Origin country is required field. ");
   showOrHideAutoSave('0');
   return false;
 }else if((document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Company Division is required field.");
   showOrHideAutoSave('0');
   return false;
 }else if((document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Destination country is required field.");
   showOrHideAutoSave('0');
   return false;
 }else if((document.forms['serviceOrderForm'].elements['serviceOrder.job'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Job Type is required field.");
   showOrHideAutoSave('0');
   return false;
 }else if((document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Coordinator is required field.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.job'].value!='RLO')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Commodity is required field.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value=="United States")&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("State is a required field in Origin.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Origin city is required field.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value=="United States")&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Origin Postal Code is required field.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value=="United States")&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("State is a required field in Destination.");
   showOrHideAutoSave('0');
   return false;
  }else if((document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value=='')&&(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL')){
   alert("Destination city is required field.");
   showOrHideAutoSave('0');
   return false;
  }else{	
   return true;
   }
 }
 
  function calculateestimatePassPercentage(revenue,expense,estimatePassPercentage){
    var revenueValue=0;
    var expenseValue=0;
    var estimatePassPercentageValue=0; 
    checkFloatNew('serviceOrderForm',estimatePassPercentage,'Nothing'); 
    revenueValue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
    expenseValue=eval(document.forms['serviceOrderForm'].elements[expense].value); 
    estimatePassPercentageValue=(revenueValue/expenseValue)*100; 
    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
    
    if(document.forms['serviceOrderForm'].elements[expense].value == '' || document.forms['serviceOrderForm'].elements[expense].value == 0||document.forms['serviceOrderForm'].elements[expense].value == 0.00){
  	   document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
  	   }else{ 
  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
  	   } 
    calculateGrossMargin();
    } 
       
     function calculateEstimateRate(){
    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value
    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value
    var accEstimateRate=document.forms['serviceOrderForm'].elements['accEstimateRate'].value 
    var accEstimateExpense=document.forms['serviceOrderForm'].elements['accEstimateExpense'].value 
    var expenseValue=0;
    var expenseRound=0;
    var oldExpense=0;
    var oldExpenseSum=0;
    var balanceExpense=0;
    oldExpense=eval(document.forms['serviceOrderForm'].elements['estimateExpense'+accountlineId].value);
    <c:if test="${not empty serviceOrder.id}">
    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
    </c:if> 
    expenseValue=accEstimateExpense;
    expenseRound=Math.round(expenseValue*10000)/10000;
    balanceExpense=expenseRound-oldExpense;
    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
    balanceExpense=Math.round(balanceExpense*10000)/10000; 
    oldExpenseSum=oldExpenseSum+balanceExpense;
    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
    var expenseRoundNew=""+expenseRound;
    if(expenseRoundNew.indexOf(".") == -1){
    expenseRoundNew=expenseRoundNew+".00";
    } 
    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
    expenseRoundNew=expenseRoundNew+"0";
    } 
    document.forms['serviceOrderForm'].elements['estimateExpense'+accountlineId].value=expenseRoundNew;
    document.forms['serviceOrderForm'].elements['estimateRate'+accountlineId].value=accEstimateRate;
    estimateQuantity=document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value;
    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
     document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
     }
    <c:if test="${not empty serviceOrder.id}">
    var newExpenseSum=""+oldExpenseSum;
    if(newExpenseSum.indexOf(".") == -1){
    newExpenseSum=newExpenseSum+".00";
    } 
    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
    newExpenseSum=newExpenseSum+"0";
    } 
    document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
    </c:if>
    if(expenseRoundNew>0){
    document.forms['serviceOrderForm'].elements['displayOnQuote'+accountlineId].checked = true;
    }
    var revenue='estimateRevenueAmount'+accountlineId;
    var expense='estimateExpense'+accountlineId;
    var estimatePassPercentage='estimatePassPercentage'+accountlineId;
    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
    } 
     function calculateGrossMargin(){
    <c:if test="${not empty serviceOrder.id}">
    var revenueValue=0;
    var expenseValue=0;
    var grossMarginValue=0; 
    var grossMarginPercentageValue=0; 
    revenueValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
    expenseValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
    grossMarginValue=revenueValue-expenseValue;
    grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
    document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
    if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
  	    document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
  	   }else{
  	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
  	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
  	   	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
  	   }
  	</c:if>    
    }
  function chk(chargeCode,category)
{
	var val = document.forms['serviceOrderForm'].elements[category].value;
	var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value; 
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription'; 
	if(val=='Internal Cost'){
		quotationContract="Internal Cost Management";
		javascript:openWindow('chargess.html?contract='+quotationContract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
	    document.forms['serviceOrderForm'].elements[chargeCode].select();
	}
	else {
	if(quotationContract=='' || quotationContract==' ')
	{
		alert("There is no pricing contract in billing: Please select.");
	}
	else{
		javascript:openWindow('chargess.html?contract='+quotationContract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&categoryType='+val);
	}
	
}
document.forms['serviceOrderForm'].elements[chargeCode].focus();
}  	

  function checkBillingComplete() {  
      if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){ 
          var agree = confirm("The billing has been completed, do you still want to add lines?");
           if(agree)
             {
                 addPricingLine();
             }else { 
             }
      }else{
          addPricingLine();      
      }
  } 
	function numbersonly(myfield, e)
	{
		var key;
		var keychar;

		var val = myfield.value;
				if(val.indexOf(".")<0)
				{
				count=0;
				}
	    
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
	
			keychar = String.fromCharCode(key);
	
		if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
		   return true;
		else if ((("0123456789").indexOf(keychar) > -1))
		   return true;
		else if ((count<1)&&(keychar == "."))
		{
			
		   count++;
		   return true;
		   
		}
		else if ((count>0)&&(keychar == "."))
		{
		   return false;
		}
		
		else
		   return false;
	}	
    function changeVatAmt(estVatDec,estVatPer,revenue,estAmt)
    {
        	 <c:forEach var="entry" items="${estVatPersentList}">
	            if(document.forms['serviceOrderForm'].elements[estVatDec].value=='${entry.key}') {
            		document.forms['serviceOrderForm'].elements[estVatPer].value='${entry.value}'; 
		            calculateVatAmt(estVatPer,revenue,estAmt);
            	}
        	</c:forEach>
          		if(document.forms['serviceOrderForm'].elements[estVatDec].value==''){
          			document.forms['serviceOrderForm'].elements[estVatPer].value=0;
          			calculateVatAmt(estVatPer,revenue,estAmt);
          		}          
    }  
	function calculateVatAmt(estVatPer,revenue,estAmt){		 
		var estVatAmt=0;
		if(document.forms['serviceOrderForm'].elements[estVatPer].value!=''){
		var estVatPercent=eval(document.forms['serviceOrderForm'].elements[estVatPer].value);
		var estRevenueForeign= eval(document.forms['serviceOrderForm'].elements[revenue].value);
		estVatAmt=(estRevenueForeign*estVatPercent)/100;
		estVatAmt=Math.round(estVatAmt*10000)/10000; 
		document.forms['serviceOrderForm'].elements[estAmt].value=estVatAmt;
		}	    
	} 
	function onlyNumsAllowedPersent(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	} 

	function fillCommodity()
	{
		/*if(document.forms['serviceOrderForm'].elements['serviceOrder.job'].value=='RLO')
		{
			document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value='RELSE';		
		}*/
	}
	function getOnGroup(label)
	{
		if(label=='dropDown')
		{
			if(document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value=='GRP')
			{
			document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked=true;
			}
		}
		if(label=='checkBox')
		{
			if(document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked==true)
			{
			document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value='GRP';
			}
		}
	}    
	function selectColumn(targetElement) 
	   {  
	     var rowId=targetElement.value;  
	     if(targetElement.checked)
	       {
	        var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
	        if(userCheckStatus == '')
	        {
	  	  	document.forms['serviceOrderForm'].elements['reloServiceType'].value = rowId;
	        }
	        else
	        {
	         var userCheckStatus=	document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus + '#' + rowId;
	        document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
	        }
	      }
	     if(targetElement.checked==false)
	      {
	       var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
	       var userCheckStatus=document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( rowId , '' );
	       document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
	       } 
	    }
	function checkColumn(){
		var userCheckStatus ="";
		userCheckStatus = '${serviceOrder.serviceType}';		  
		if(userCheckStatus!="")
		{
		var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			document.forms['serviceOrderForm'].elements['reloServiceType'].value="";
			var column = userCheckStatus.split(",");  
			for(i = 0; i<column.length ; i++)
			{    
			     var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;  
		    	 var userCheckStatus=	document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus + '#' +column[i];
		        	if(column[i]!="")
		        	{
			        	if(job=='RLO'){
		     				document.getElementById(column[i]).checked=true;
		        		}
		        	}
			} 
		}
		if(userCheckStatus==""){	}
	}
	
</script>