DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`add_tblHistory` $$
CREATE PROCEDURE `add_tblHistory`(
IN pId BIGINT(20),
IN pTableName VARCHAR(100),
IN pFieldNameList LONGTEXT,
IN pOldValueList LONGTEXT,
IN pNewValueList LONGTEXT,
IN pUserName VARCHAR(255),
IN pCorpID VARCHAR(25),
IN pDated TIMESTAMP
)
BEGIN
DECLARE pFieldName LONGTEXT;
DECLARE remainingFields LONGTEXT;
DECLARE pOldValue LONGTEXT;
DECLARE remainingOldValues LONGTEXT;
DECLARE pNewValue LONGTEXT;
DECLARE remainingNewValues LONGTEXT;
DECLARE pUserId LONGTEXT;
DECLARE pAuditable int(2);
DECLARE pDesc VARCHAR(1500);
DECLARE palertValue VARCHAR(25);
DECLARE palertRole VARCHAR(90);
DECLARE palertUser VARCHAR(30);
DECLARE palertFileNumber VARCHAR(15);
DECLARE palertShipperName VARCHAR(160);
DECLARE sid VARCHAR(160);
DECLARE spid VARCHAR(160);
DECLARE ppalertRole VARCHAR(160);
DECLARE remainingalert VARCHAR(160);
DECLARE lfullAudit VARCHAR(3);

SET palertUser = "";
SET palertFileNumber = "";
SET palertShipperName = "";
SET palertValue = 'All Updates';
SET pAuditable = 0;
SET pDesc = "";
SET lfullAudit = "";

SELECT SUBSTRING_INDEX(pFieldNameList,'~',1) INTO pFieldName;
SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;
SELECT SUBSTRING_INDEX(pOldValueList,'~',1) into pOldValue;
SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;
SELECT SUBSTRING_INDEX(pNewValueList,'~',1) into pNewValue;
SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;


WHILE pFieldName <> '' DO
    begin
      DECLARE CONTINUE HANDLER FOR 1329
      SET pAuditable = 0;
      SET palertValue = '';
      SET ppalertRole = '';



    SELECT id,description,alertValue,concat(if(alertRole is null,'',alertrole),',') into pAuditable,pDesc,palertValue,ppalertRole
    FROM auditSetup where tablename = pTableName
    AND fieldname = pFieldName and auditable='true' and corpID = pCorpID  limit 0,1;


 if(pTableName='servicepartner') then

    select serviceorderid into spid from servicepartner where id=PId;

    end if;

 if(pTableName='container') then

    select serviceorderid into spid from container where id=PId;

    end if;

 if(pTableName='carton') then

    select serviceorderid into spid from carton where id=PId;

    end if;

 if(pTableName='vehicle') then

    select serviceorderid into spid from vehicle where id=PId;

    end if;

 if(pTableName='dspdetails') then

    select serviceorderid into spid from dspdetails where id=PId;

    end if;

    if(pTableName='accountline') then

    select serviceorderid into sid from accountline where id=PId;

    end if;


    end;


WHILE ppalertRole <> '' DO

      SET palertRole = '';
      SET remainingalert = '';

  SELECT SUBSTRING_INDEX(ppalertRole,',',1) INTO palertRole;

  SELECT MID(ppalertRole,LOCATE(',',ppalertRole)+1,CHAR_LENGTH(ppalertRole)) into remainingalert;

  set ppalertRole=remainingalert;


    IF (pAuditable != 0) THEN

    IF(palertValue = 'All Updates') THEN

      IF(palertRole = 'Coordinator') THEN

          IF(pTableName='customerfile') THEN

              SELECT coordinator,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

          END IF;

          IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

              SELECT coordinator,shipnumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder where id= pId;

          END IF;

          IF(pTableName='accountline') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a where s.id= sid and a.id=PId;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT s.coordinator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;


      END IF;

      IF(palertRole = 'Auditor') THEN

          IF(pTableName='customerfile') THEN

                SELECT auditor,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

          END IF;

          IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

          END IF;

          IF(pTableName='accountline') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a,billing b where s.id= sid and a.id=PId and b.id=sid;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


          IF(pTableName='vehicle') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


          IF(pTableName='dspdetails') THEN

              SELECT b.auditor,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;




      END IF;

      IF(palertRole = 'Billing') THEN

          IF(pTableName='customerfile') THEN

                SELECT personBilling,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

          END IF;

          IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

          END IF;


          IF(pTableName='accountline') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a,billing b where s.id= sid and a.id=PId and b.id=sid;

          END IF;

         IF(pTableName='servicepartner') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT b.personBilling,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


      END IF;

      IF(palertRole = 'Consultant') THEN

          IF(pTableName='customerfile') THEN

                SELECT estimator,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

          END IF;

          IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                SELECT estimator,shipnumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder where id= pId;

          END IF;


         IF(pTableName='accountline') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a where s.id= sid and a.id=PId;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT s.estimator,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;


      END IF;

      IF(palertRole = 'Payable') THEN

          IF(pTableName='customerfile') THEN

                SELECT personPayable,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

          END IF;

          IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

          END IF;

          IF(pTableName='accountline') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a,billing b where s.id= sid and a.id=PId and b.id=sid;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


          IF(pTableName='carton') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT b.personPayable,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

       END IF;

       IF(palertRole = 'Pricing') THEN

           IF(pTableName='customerfile') THEN

                 SELECT personPricing,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

           END IF;

           IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                 SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

           END IF;

           IF(pTableName='accountline') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a,billing b where s.id= sid and a.id=PId and b.id=sid;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT b.personPricing,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;



       END IF;

       IF(palertRole = 'Salesman') THEN

            IF(pTableName='customerfile') THEN

                 SELECT salesMan,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

            END IF;

            IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

            END IF;

            IF(pTableName='accountline') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a where s.id= sid and a.id=PId;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='container') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT salesMan,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a where s.id= spid and a.serviceorderid=spid and a.id=PId;

          END IF;


       END IF;

    IF(palertRole = 'Claim Handler') THEN

           IF(pTableName='customerfile') THEN

                 SELECT personPricing,sequencenumber,concat(firstname,' ',lastname) into palertUser,palertFileNumber,palertShipperName from customerfile where id= pId;

           END IF;

           IF(pTableName='serviceorder' or pTableName='billing' or pTableName='miscellaneous' or pTableName='trackingstatus') THEN

                 SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from billing b,serviceorder s where b.id= pId and s.id=pid;

           END IF;

           IF(pTableName='accountline') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,accountline a,billing b where s.id= sid and a.id=PId and b.id=sid;

          END IF;

          IF(pTableName='servicepartner') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,servicepartner a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


          IF(pTableName='container') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,container a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='carton') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,carton a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='vehicle') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,vehicle a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;

          IF(pTableName='dspdetails') THEN

              SELECT b.claimHandler,s.shipnumber,concat(s.firstname,' ',s.lastname) into palertUser,palertFileNumber,palertShipperName from serviceorder s ,dspdetails a,billing b where s.id= spid and a.serviceorderid=spid and b.id=spid and a.id=PId;

          END IF;


       END IF;

    END IF ;


      INSERT INTO `history` (`xtranId`, `user`, `tableName`, `fieldName`, `oldValue`, `newValue`, corpID, description,dated,alertRole,alertUser,isViewed,alertFileNumber,alertShipperName)
      VALUES (pId, pUserName, pTableName, pFieldName, pOldValue, pNewValue, pCorpID, pDesc,now(),palertRole,palertUser,false,palertFileNumber,palertShipperName);
        End IF ;

END WHILE;
	SELECT if(fullaudit is true,'YES','NO') into lfullAudit
	FROM company where corpID = pCorpID;
	
	if(lfullAudit='YES') then
		IF (pAuditable = 0) THEN
			SELECT description into pDesc
		    FROM datacatalog where tableName = pTableName
		    AND fieldname = pFieldName and corpID = 'TSFT'  limit 0,1;

		INSERT INTO `history` (`xtranId`, `user`, `tableName`, `fieldName`, `oldValue`, `newValue`, corpID, description,dated,alertRole,alertUser,isViewed,alertFileNumber,alertShipperName)
		 VALUES (pId, pUserName, pTableName, pFieldName, pOldValue, pNewValue, pCorpID, pDesc,now(),'','',false,palertFileNumber,palertShipperName);
		End IF ;
	End IF ;

    SELECT SUBSTRING_INDEX(remainingFields,'~',1) INTO pFieldName;
    SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
    SET pFieldNameList = remainingFields;
    SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;
    SELECT SUBSTRING_INDEX(remainingOldValues,'~',1) into pOldValue;
    SET pOldValueList = remainingOldValues;
    SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;
    SELECT SUBSTRING_INDEX(remainingNewValues,'~',1) into pNewValue;
    SET pNewValueList = remainingNewValues;
    SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;
   SET pAuditable = 0;
   SET palertValue ='';
   set palertUser = '';
   set palertRole = '';
   SET palertFileNumber = '';
   SET palertShipperName = '';

END WHILE;


END$$




