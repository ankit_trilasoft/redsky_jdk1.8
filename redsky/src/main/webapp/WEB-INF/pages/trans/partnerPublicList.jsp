<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="partnerPublicList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerPublicList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
	     document.forms['partnerListForm'].elements['partnerPublic.aliasName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.lastName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.firstName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.partnerCode'].value = '';
		document.forms['partnerListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerListForm'].elements['stateSearch'].value = '';
		document.forms['partnerListForm'].elements['countrySearch'].value = '';
		document.forms['partnerListForm'].elements['vanlineCodeSearch'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.status'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked = false;
		document.forms['partnerListForm'].elements['isIgnoreInactive'].checked = false;
		document.forms['partnerListForm'].elements['partnerPrivate.extReference'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.fidiNumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.OMNINumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.AMSANumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.WERCNumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.IAMNumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.utsNumber'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.eurovanNetwork'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.PAIMA'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.LACMA'].value = '';
}
function activBtn(temp){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
    var len = temp.length;
    var val="" ;
    if(len == undefined){
		if(temp.checked){
			val= temp.value;
		}
    }
		
	  for(var i = 0; i < len; i++) {
		if(temp[i].checked) {
			val = temp[i].value;
		}
	}

	if(val=='AG'){
		document.forms['addPartner'].elements['chkAgentTrue'].value='Y';
	}else{
		document.forms['addPartner'].elements['chkAgentTrue'].value='';	
	}
}

function seachValidate(){
		var pp =  document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked;
		var ac =  document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked;
		var ag =  document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked;
		var vd =  document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked;
		var cr =  document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked;
		var oo =  document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one partnerPublic type.');
			return false;
		} 
		
}


function findVanLineCode(partnerCode,position){
	var url="findVanLineCodeList.html?ajax=1&decorator=simple&popup=true&partnerVanLineCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);
}
</script>
<style>
span.pagelinks {display:block;margin-bottom:0px;!margin-bottom:2px;margin-top:-10px;!margin-top:-17px;padding:2px 0px;text-align:right;width:99%;!width:98%;font-size:0.85em;
}
.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;}
.radiobtn {margin:0px 0px 2px 0px;!padding-bottom:5px;!margin-bottom:5px;}
form {margin-top:-10px;!margin-top:0px;}

div#main {margin:-5px 0 0;}
input[type="checkbox"] {vertical-align:middle;}
.br-n-ctnr {border:none !important;text-align:center !important;}
.br-n-rgt {border:none !important;text-align:right !important;}
</style>
</head>
<c:set var="buttons">     
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editPartnerPublic.html"/>'" value="<fmt:message key="button.add"/>"/>         
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action="searchPartnerPublic" method="post" >  

<div id="layer5" style="width:100%">
<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
		<li><a href="geoCodeMaps.html?partnerOption=notView" ><span>Geo Search</span></a></li>					  	
	  </ul>
</div>
<div class="spnblk" style="margin-top:-10px;">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-2px;"><span></span></div>
<div class="center-content">
<table class="table" border="0" style="width:100%;">
	<thead>
		<tr>
			<th style="width: 120px;"><fmt:message key="partner.partnerCode"/></th>
			<th style="width: 120px;">Vanline Code</th>
			<th style="width: 120px;">External Ref.</th>
			<th style="width: 120px;"><fmt:message key="partner.firstName"/></th>
			<th style="width: 120px;">Last/Company Name</th>
			<th style="width: 120px;">Alias Name</th>
			<th style="width: 120px;">Country Code</th>
			<th style="width: 120px;">Country Name</th>
			<th style="width: 120px;">State Code</th>
			<th style="width: 120px;"><fmt:message key="partner.status"/></th>
			<th style="width: 120px;">Vendor Type</th>
		</tr>
	</thead> 	
	<tbody>
		<tr>
			<td><s:textfield name="partnerPublic.partnerCode" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="vanlineCodeSearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="partnerPrivate.extReference" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="partnerPublic.firstName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.lastName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.aliasName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="14" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:106px" headerKey="" headerValue=""/></td>
			<td><s:select cssClass="list-menu" name="partnerPublic.typeOfVendor" list="%{typeOfVendor}" cssStyle="width:106px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
		
		
		 <td class="listwhitetext br-n-ctnr1">FIDI&nbsp;<s:select cssClass="list-menu" name="partnerPublic.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
			<td class="listwhitetext br-n-rgt1">OMNI&nbsp;<s:select cssClass="list-menu" name="partnerPublic.OMNINumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
			<td class="listwhitetext br-n-rgt1">AMSA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-rgt1">WERC&nbsp;<s:select cssClass="list-menu" name="partnerPublic.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-rgt1">IAM&nbsp;<s:select cssClass="list-menu" name="partnerPublic.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-rgt1">Harmony&nbsp;<s:select cssClass="list-menu" name="partnerPublic.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-ctnr1">Eurovan&nbsp;Network&nbsp;<s:select cssClass="list-menu" name="partnerPublic.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-ctnr1">PAIMA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-ctnr1">LACMA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td colspan="2"></td>		
		
		
		
		<!-- <td colspan="11">
		<table style="margin:0px; padding:0px;width:100%;" cellpadding="0" cellspacing="0" border="0" >
		 <tbody>
			<tr>
		    <td class="listwhitetext br-n-ctnr1">FIDI&nbsp;<s:select cssClass="list-menu" name="partnerPublic.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
			<td class="listwhitetext br-n-rgt1">OMNI&nbsp;<s:select cssClass="list-menu" name="partnerPublic.OMNINumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
			<td class="listwhitetext br-n-rgt1">AMSA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-rgt1">WERC&nbsp;<s:select cssClass="list-menu" name="partnerPublic.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-rgt1">IAM&nbsp;<s:select cssClass="list-menu" name="partnerPublic.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-rgt1">Harmony&nbsp;<s:select cssClass="list-menu" name="partnerPublic.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
			<td class="listwhitetext br-n-ctnr1">Eurovan&nbsp;Network&nbsp;<s:select cssClass="list-menu" name="partnerPublic.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-ctnr1">PAIMA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
			<td class="listwhitetext br-n-ctnr1">LACMA&nbsp;<s:select cssClass="list-menu" name="partnerPublic.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>			
		</tr>
		</tbody>
		</table>
		</td>  -->
		</tr>
		<tr>
		
		
		<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isPrivateParty}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Private party<s:checkbox key="partnerPublic.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAccount}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Accounts<s:checkbox key="partnerPublic.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAgent}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Agents<s:checkbox key="partnerPublic.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
														
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isVendor}">
							<c:set var="ischecked" value="true"/>
							</c:if>					
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Vendors<s:checkbox key="partnerPublic.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
															
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isCarrier}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Carriers<s:checkbox key="partnerPublic.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
									
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isOwnerOp}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Owner Ops<s:checkbox key="partnerPublic.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
						
							<c:set var="ischecked" value="false"/>
							<c:if test="${isIgnoreInactive}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="110px" colspan="4" style="border:none;  border-right:1px solid #e0e0e0;">Ignore Inactive<s:checkbox key="isIgnoreInactive" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
							
							<td width="130px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
							
							
							
							
			<!--  <td colspan="11">
				<table style="margin:0px; padding:0px;width:100%;" cellpadding="0" cellspacing="0" >
					<tbody>
						<tr>
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isPrivateParty}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Private party<s:checkbox key="partnerPublic.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAccount}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Accounts<s:checkbox key="partnerPublic.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAgent}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Agents<s:checkbox key="partnerPublic.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
														
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isVendor}">
							<c:set var="ischecked" value="true"/>
							</c:if>					
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Vendors<s:checkbox key="partnerPublic.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
															
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isCarrier}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Carriers<s:checkbox key="partnerPublic.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
									
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isOwnerOp}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none; border-right:1px solid #e0e0e0;">Owner Ops<s:checkbox key="partnerPublic.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
						
							<c:set var="ischecked" value="false"/>
							<c:if test="${isIgnoreInactive}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="110px" style="border:none; border-right:1px solid #e0e0e0;">Ignore Inactive<s:checkbox key="isIgnoreInactive" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
							
							<td width="130px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
						</tr>
					</tbody>	
				</table>
			</td>-->
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>
</div>
</div>
</div>
</s:form>
<div id="layer1" style="width:100%">
<div style="float:left;margin-bottom:1px;" id="newmnav">
		  <ul>	
		  <li style="background:#FFF" id="newmnav1"><a class="current"><span>Partner List</span></a></li>
		  <li> <a href="agentList.html"><span>Agent Change Request</span></a></li>
		  		 
		
		  </ul>
		</div>
<div class="spnblk">&nbsp;</div> 


<s:set name="partnerPublicList" value="partnerPublicList" scope="request"/> 
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility> 
<display:table name="partnerPublicList" class="table" requestURI="" id="partnerPublicLists" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;">  		
	<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerPublicLists.firstName} ${partnerPublicLists.lastName}" /></display:column>		
	<display:column property="aliasName" sortable="true" title="Alias Name" style="width:65px"/>
	<display:column property="extReference" sortable="true" titleKey="partner.extReference" style="width:65px"/>
	<display:column title="PParty" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isPrivateParty == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=PP"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAccount == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAgent == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isCarrier == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isVendor == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Owner&nbsp;Ops" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isOwnerOp == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>			     
	
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    <display:column title="VL&nbsp;Code" style="width:90px"><!--
    <c:out value="${partnerPublicLists.vanLineCode}" />
    <c:if test="${partnerPublicLists.vanLineCode!=''}">
    <img id="target" onclick ="findVanLineCode('${partnerPublicLists.partnerCode}',this);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    </c:if>
    -->
    <!--<img id="target" onclick ="findVanLineCode('${partnerPublicLists.partnerCode}',this);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    --><a onclick ="findVanLineCode('${partnerPublicLists.partnerCode}',this);">View&nbsp;List</a>
    </display:column>
    <c:if test="${agentClassificationShow=='Y'}">
     <display:column  title="Agent&nbsp;Classification" sortable="true" sortProperty="agentClassification" style="width:45px;" >
    <c:if test="${partnerPublicLists.isAgent == true}">
   		<c:out value="${partnerPublicLists.agentClassification}" />
    </c:if>
	</display:column>
	</c:if>
    <display:column property="agentGroup" sortable="true" title="Group" style="width:100px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:100px"/>
    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>
</div>
<table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="padding:1px;width: 100%;">
	<tbody>
		<s:form id="addPartner" action="" method="post" name="addPartner">  
		<s:hidden name="chkAgentTrue" />
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;padding-top: 3px;">Add New Partner :
					<s:radio name="partnerType" list="%{actionType}" onclick="activBtn(this);" cssStyle="vertical-align: sub;" />
				   	<s:submit name="addBtn" cssClass="cssbutton" cssStyle="width:125px; height:25px; margin-left:50px;" align="top" value=" Add Partner" onclick='submit_addPartner();'/>   
				</td>
			</tr>	
		</s:form>
	</tbody>
</table>
<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
try{
    document.forms['addPartner'].elements['addBtn'].disabled = true;	
    function submit_addPartner()
	{
	verify_radio("addPartner","partnerType");
	}
	
	function verify_radio(frmName, rbGroupName)
    {
	var radios = document[frmName].elements[rbGroupName];
			for (var i=0; i <radios.length; i++) {
			if (radios[i].checked) {
				<c:if test="${loginCorpId !='TSFT'}">
				if(radios[i].value=='AG')
					{
					  document.forms['addPartner'].action ='agentRequestFinal.html';
					document.forms['addPartner'].submit();
					
					}
				else{
					
					 document.forms['addPartner'].action ='editPartnerPublic.html';
						document.forms['addPartner'].submit();
				}
				</c:if>
				<c:if test="${loginCorpId =='TSFT'}">
				document.forms['addPartner'].action ='editPartnerPublic.html';
				document.forms['addPartner'].submit();
				</c:if>
			   return true;
			  }
			 }
			 return false;
			}
}
catch(e){}
</script>