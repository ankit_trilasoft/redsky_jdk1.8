<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Address Details</title>
    <meta name="heading" content="Address Details:"/>
   <style type="text/css">  
 .upper-case{
  text-transform:capitalize;
   }
  
</style>
</head>

<script type="text/JavaScript">

function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}

function titleCase(element){
    var txt=element.value; var spl=txt.split(" "); 
    var upstring=""; for(var i=0;i<spl.length;i++){ 
    try{ 
    upstring+=spl[i].charAt(0).toUpperCase(); 
    }catch(err){} 
    upstring+=spl[i].substring(1, spl[i].length); 
    upstring+=" ";   
    } 
    element.value=upstring.substring(0,upstring.length-1); 
     
}
</script> 
<s:form id="adAddressesDetailsForm" action="saveAdAddressesDetails.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">


<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="adAddressesDetails.id" />
<s:hidden name="id" value="${adAddressesDetails.id}"/>
<s:hidden name="adAddressesDetails.customerFileId" />  
<s:hidden name="adAddressesDetails.networkId" />  

<div id="layer1" style="margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Addresses<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:100%" onkeydown="">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0"  style="margin-left:30px">
<tbody>
<tr>
<td align="left" width="175px" class="listwhitetext" valign="bottom">Description<font color="red" size="2">*</font></td> 
<td style="width:30px"></td>
<td align="left" width="150px" class="listwhitetext" valign="bottom">Address Type<font color="red" size="2">*</font></td>
</tr>
<tr>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="adAddressesDetails.description" maxlength="20"  required="true" cssClass="input-text" size="25" cssStyle="width:174px;" tabindex="1" /></td>
<td></td>
<td  align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${addressType_isactive}" fieldValue="${adAddressesDetails.addressType}" 
			attribute="class=list-menu name=adAddressesDetails.addressType style=width:178px headerKey='' headerValue='' tabindex=2"/>							
</td> 
 </tr>
 <tr> 
 <td align="left" class="listwhitetext" valign="bottom">Address1</td>
 <td></td>
 <td align="left" class="listwhitetext" valign="bottom">Address2</td> 
 </tr>
 <tr> 
 <td align="left" class="listwhitetext" valign="top"><s:textfield name="adAddressesDetails.address1" maxlength="50"  required="true" cssClass="input-text upper-case" onblur="titleCase(this)" size="25" cssStyle="width:174px;" tabindex="3"  /></td> 
<td></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="adAddressesDetails.address2" maxlength="50"  required="true" cssClass="input-text upper-case" onblur="titleCase(this)" size="25" cssStyle="width:174px;" tabindex="4" /></td> 
</tr> 
 <td align="left" class="listwhitetext" valign="bottom">Address3</td> 
 <td></td>
 <td align="left" class="listwhitetext" valign="bottom">Country</td>
 </tr>
 <tr> 
 <td align="left" class="listwhitetext" valign="top"><s:textfield name="adAddressesDetails.address3" maxlength="50"  required="true" cssClass="input-text upper-case" onblur="titleCase(this)" size="25" cssStyle="width:174px;" tabindex="5"  /></td>
 <td></td> 
<td align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${ocountry_isactive}" fieldValue="${adAddressesDetails.country}" 
			attribute="class=list-menu name=adAddressesDetails.country onchange=getState();enableStateListOrigin(); style=width:178px  headerKey='' headerValue='' tabindex=6"/>							
</td>
</tr>
<tr>
<td align="left" class="listwhitetext" valign="bottom">State</td>
 <td></td>
<td align="left" class="listwhitetext" valign="bottom">City</td>
</tr>
<tr>
<td align="left" valign="top"><s:select cssClass="list-menu" id="state" name="adAddressesDetails.state" list="%{ostates}" cssStyle="width:178px"  headerKey="" headerValue="" tabindex="7" /></td>					
 <td></td>
 <td><s:textfield name="adAddressesDetails.city" maxlength="100"  required="true" cssClass="input-text upper-case" onblur="titleCase(this)" size="25" cssStyle="width:174px;" tabindex="8" /></td>
</tr>
<tr>    
  <td align="left" class="listwhitetext" valign="bottom">Phone</td>
  <td></td>
  <td align="left" class="listwhitetext" valign="bottom">Postal Code</td>
 </tr>
 <tr> 
 <td><s:textfield name="adAddressesDetails.phone" maxlength="20"  required="true" cssClass="input-text" size="25" cssStyle="width:174px;" tabindex="9" /></td>
 <td></td>
 <td align="left"><s:textfield name="adAddressesDetails.zipCode" maxlength="10"  required="true" cssClass="input-text" size="25" cssStyle="width:174px;" tabindex="10"  /></td>
<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
						                        <td>
                                                <li><a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="" style="cursor: pointer;">&nbsp;<u><strong><font size="1"><spam >Look-Up Canadian PostalCode</spam></font></strong></u></a>
                                                </li></td>
                                                </configByCorp:fieldVisibility>
</tr>

<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
<table width="900px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${adAddressesDetails.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="adAddressesDetails.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${adAddressesDetails.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty adAddressesDetails.id}">
								<s:hidden name="adAddressesDetails.createdBy"/>
								<td ><s:label name="createdBy" value="%{adAddressesDetails.createdBy}"/></td>
							</c:if>
							<c:if test="${empty adAddressesDetails.id}">
								<s:hidden name="adAddressesDetails.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${adAddressesDetails.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="adAddressesDetails.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${adAddressesDetails.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty adAddressesDetails.id}">
							<s:hidden name="adAddressesDetails.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{adAddressesDetails.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty adAddressesDetails.id}">
							<s:hidden name="adAddressesDetails.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table><tr><td>   
   <c:set var="isOrderInitiation" value="<%=request.getParameter("isOrderInitiation") %>"/>
        <c:choose>
	<c:when test="${(usertype=='ACCOUNT' || usertype=='AGENT') and not isOrderInitiation}">	
	</c:when>
	<c:otherwise>
		<s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" onclick="reset_originCountry();"/>
	</c:otherwise>
   </c:choose>
	</td></tr></table>
</div>
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<script type="text/javascript">
function iehack(){
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ 
		window.onload = function(){
			getState();
		}
		var ieversion=new Number(RegExp.$1) 
			var elementList = document.getElementsByTagName('*');
			 for(var i in elementList){
				 if(elementList[i].type == 'text')
					{
						 elementList[i].readOnly =true;
						 elementList[i].className = 'input-textUpper';
					}
				 if(elementList[i].type == 'checkbox')
					{
					 elementList[i].disabled =true;
					}
				 if(elementList[i].type == 'select-one')
					{
						 elementList[i].disabled =true;
						 elementList[i].style.className = 'list-menu';
					}
			}
	  }
}

function trap() 
{ 
if(document.images)
  {
    var totalImages = document.images.length;
    	for (var i=0;i<totalImages;i++)
			{
				if(document.images[i].src.indexOf('calender.png')>0)
				{
					
					var el = document.getElementById(document.images[i].id);  
					document.images[i].src = 'images/navarrow.gif'; 
					if((el.getAttribute("id")).indexOf('trigger')>0){
						el.removeAttribute("id");
					}
				}
				if(document.images[i].src.indexOf('open-popup.gif')>0)
				{ 
					
						var el = document.getElementById(document.images[i].id);
						//alert(el)
						try{
						el.onclick = false;
						}catch(e){}
					    document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('notes_empty1.jpg')>0)
				{
						var el = document.getElementById(document.images[i].id);
						//alert(el)
						try{
						el.onclick = false;
						}catch(e){}
				        document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('notes_open1.jpg')>0)
				{
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick =false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
				
									
				if(document.images[i].src.indexOf('images/nav')>0)
				{
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick = false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
			
			}
			
			var elementsLen=document.forms['adAddressesDetailsForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['adAddressesDetailsForm'].elements[i].type=='text')
					{
						document.forms['adAddressesDetailsForm'].elements[i].readOnly =true;
						document.forms['adAddressesDetailsForm'].elements[i].className = 'input-textUpper';						
					}
					else
					{
					 document.forms['adAddressesDetailsForm'].elements[i].disabled=true;
					}	
			}  
		  }
  	document.forms['adAddressesDetailsForm'].setAttribute('bypassDirtyCheck',false);
}
function findActiveLength(type,fieldValue,result){
	var len=0;
		var res = result.split("@");
		for(i=0;i<res.length;i++){
		var	stateVal = res[i].split("#");
			if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==fieldValue)){
				len++;
			}
		}
	return len;
}
function getState() { 
	var country = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.country'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">	
	if(country=="${entry.value}"){
		countryCode="${entry.key}";		
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(country); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}	
function handleHttpResponse5(){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       var selectVal='${adAddressesDetails.state}';
       var len=findActiveLength('',selectVal,results);
       var res = results.split("@");
       targetElement = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'];
       targetElement.length = len+1;
       document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[0].text = '';
       document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[0].value = '';
		var j=1;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){}else{
				stateVal = res[i].split("#");
				if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){			
					document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[j].text = stateVal[1];
					document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[j].value = stateVal[0]; 
					j++;
				}
			}
			}
			document.getElementById("state").value = '${adAddressesDetails.state}'; 
			
					 
    }
}  
var httpState = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function resetState() { 
	document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.country'].value='${adAddressesDetails.country}';
	var country = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.country'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">	
	if(country=="${entry.value}"){
		countryCode="${entry.key}";		
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(country); 
	 http14.open("GET", url, true);
     http14.onreadystatechange = handleHttpResponse144;
     http14.send(null);
}
function handleHttpResponse144(){
	if (http14.readyState == 4){
        var results = http14.responseText
        results = results.trim();
        var selectVal='${adAddressesDetails.state}';
        var len=findActiveLength('',selectVal,results);        
        var res = results.split("@");
        targetElement = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'];
        targetElement.length = len+1;
        document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[0].text = '';
        document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[0].value = '';
 		var j=1;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){}else{
				stateVal = res[i].split("#");	
				if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){	
					document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[j].text = stateVal[1];
					document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].options[j].value = stateVal[0];
				}
			}
		}	
		document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].value='${adAddressesDetails.state}';
	}
}
var http14 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

</script>
<script>
function pick() {
//  		parent.window.opener.document.location.reload();
  		try {
  			window.opener.clickOnRequest();
  		}catch(e){
  			window.opener.document.forms[0].submit();
  		}
  		 
  		window.close();
	}
	
function validation(){
if(document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.description'].value==''){
   alert("Description is a required field ")
   return false;
 }else if(document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.addressType'].value==''){
   alert("Address Type is a required field ")
   return false;
 } 
var detail = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.description'].value;
var add1 = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.address1'].value;
var add2 = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.address2'].value;
var add3 = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.address3'].value;
var city = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.city'].value;
var pin = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.zipCode'].value;
var phone = document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.phone'].value;

for(var i=0 ; i<detail.length ; i++){
	if(detail.charAt(i)=='~' || detail.charAt(i)=='>' || detail.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Description.');
		return false;
	}
}

for(var i=0 ; i<add1.length ; i++){
	if(add1.charAt(i)=='~' || add1.charAt(i)=='>' || add1.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Address1.');
		return false;
	}
}

for(var i=0 ; i<add2.length ; i++){
	if(add2.charAt(i)=='~' || add2.charAt(i)=='>' || add2.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Address2.');
		return false;
	}
}

for(var i=0 ; i<add3.length ; i++){
	if(add3.charAt(i)=='~' || add3.charAt(i)=='>' || add3.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Address3.');
		return false;
	}
}

for(var i=0 ; i<city.length ; i++){
	if(city.charAt(i)=='~' || city.charAt(i)=='>' || city.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in City.');
		return false;
	}
}
for(var i=0 ; i<phone.length ; i++){
	if(phone.charAt(i)=='~' || phone.charAt(i)=='>' || phone.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Phone.');
		return false;
	}
}
for(var i=0 ; i<pin.length ; i++){
	if(pin.charAt(i)=='~' || pin.charAt(i)=='>' || pin.charAt(i)=='<'){
		alert('Special charecters ~,< or > not allow in Postal Code.');
		return false;
}
}
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.country'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].disabled = false;
	  }else{
		  document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].disabled = true;
	  }	        
}
function reset_originCountry() {   
	var dCountry ='${adAddressesDetails.country}';
	var enbState = '${enbState}';
	var index = (enbState.indexOf(dCountry)> -1);
	if(index != ''){
			document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].disabled = false;
			resetState(dCountry);	
		}else{
			document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.state'].disabled = true;			
		}
	}
</script> 
<%-- Shifting Closed Here --%>

<script type="text/javascript">  
	try{ 
		<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">	
		var isOrderInitiation = <%= request.getParameter("isOrderInitiation") %>
		  if(!isOrderInitiation){
			    iehack();
				trap() ;
		  }
		</c:if>
		getState(); 
  
  	  }
	 catch(e){}
try{
if(document.forms['adAddressesDetailsForm'].elements['btntype'].value=='yes'){
 pick();
	}
	}
 catch(e){} 
 try{
	 var enbState = '${enbState}';
	 var mailCon=document.forms['adAddressesDetailsForm'].elements['adAddressesDetails.country'].value;
	 if(mailCon==""){
			document.getElementById('state').disabled = true;
			document.getElementById('state').value ="";
	 }else{
	  		if(enbState.indexOf(mailCon)> -1){
				document.getElementById('state').disabled = false; 
				document.getElementById('state').value='${adAddressesDetails.state}';
			}else{
				document.getElementById('state').disabled = true;
				document.getElementById('state').value ="";
			} 
		}
	 <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
	  iehack();
	  trap() ;
	 </sec-auth:authComponent>
	 }catch(e){}	 
</script>