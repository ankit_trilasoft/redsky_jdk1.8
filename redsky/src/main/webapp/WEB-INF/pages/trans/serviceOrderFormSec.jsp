<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %> 
<style>
.textareaUpper {
    background-color: #E3F1FE; border: 1px solid #ABADB3; color: #000000; font-family: arial,verdana;font-size: 12px;   
    text-decoration: none;
}
.table th.sorted {
    background-color: #BCD2EF; color: #15428B;
}
div.message {
    font-size:12px;
}
.table th.RemoveBorder 
{
border-left: hidden;
!border-left-style:none;
}

.table th.FXRight {
text-align:right;
padding-right:10px;
}
.table th.RemoveBorderRight 
{
!border-right-style:none;
}
a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
</style>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="javascript" type="text/javascript">		
		<%@ include file="/common/calenderStyle.css"%>
</script>
<script type="text/JavaScript">
function resetSurveyResult(){ 
	<c:forEach var="setQusList" items="${flex1SurveyList}" varStatus="rowCounter">
	var aa1='${setQusList}';
	var flag='0';
	var ansid;
	<c:forEach var="setQusAns" items="${quesAnsListResult}" varStatus="rowCounter">
	var aa='${setQusAns.key}';
	<c:set var="ansIdANDTxnId" value="${fn:split(setQusAns.value, '*')}" />	
	if(aa==aa1){ 
		flag='1';
		ansid='${ansIdANDTxnId[0]}'; 
	}
	</c:forEach>
	if(flag=='1'){ 
		parent.frames['iFrame'].document.getElementById(aa1).value=ansid; 
	}
 else{  
		parent.frames['iFrame'].document.getElementById(aa1).selectedIndex='0';
	} 
	</c:forEach> 
} 
</script>
<script type="text/JavaScript">
function setQuesAns(ans,qus,ee){ 
	if(ee==null ||ee=='' || ee==' '){
		ee=" ";
	}
	 var ansopt=ans.options[ans.options.selectedIndex].value;
		var temp=document.serviceOrderForm.quesansIds.value;		
		if(temp!=null && temp!=" " && temp!=""){
			document.serviceOrderForm.quesansIds.value=temp+"~"+(qus+"-"+ansopt+"-"+ee);
			}
			else{
				document.serviceOrderForm.quesansIds.value=qus+"-"+ansopt+"-"+ee;
			}  } 
			
function calcPreferedDays(){
	<c:if test="${serviceOrder.corpID=='STVF'}">
	<c:if test="${not empty trackingStatus.beginLoad}">
	var date = '${trackingStatus.beginLoad}';
	var days = document.forms['serviceOrderForm'].elements['serviceOrder.transitDays'].value;
if(days!=''){
	var datebegin = new Date(date); // Now
	datebegin.setDate(datebegin.getDate() + parseInt(days)); // Set now + 30 days as the new date
	
	var dd = datebegin.getDate();
	var mm = datebegin.getMonth() + 1;
	var y = datebegin.getFullYear();
	var someFormattedDate = dd + '-'+ mm + '-'+ y;
	
	var mySplitResult = someFormattedDate.split("-");
	   var day = mySplitResult[0];
	   var month = mySplitResult[1];
	   var year = mySplitResult[2].substring(2, 4);
	  if(month == '1') {month = "Jan";
	   }  else if(month == '2'){month = "Feb";
	   } else if(month == '3') {month = "Mar"
	   } else if(month == '4') {month = "Apr"
	   } else if(month == '5') {month = "May"
	   } else if(month == '6') {month = "Jun"
	   } else if(month == '7') {month = "Jul"
	   } else if(month == '8') {month = "Aug"
	   } else if(month == '9') {month = "Sep"
	   } else if(month == '10') {month = "Oct"
	   } else if(month == '11') {month = "Nov"
	   } else if(month == '12') {month = "Dec";
	   }
	   var finalDate = day+"-"+month+"-"+year;
	   document.forms['serviceOrderForm'].elements['serviceOrder.grpDeliveryDate'].value=finalDate;
}
	   </c:if>
	   </c:if>
}			
			
</script> 
<script type="text/JavaScript">
window.onload = function() { 
<c:if test="${template == 'YES'}">
	window.scrollTo(100,500);
</c:if>
<c:if test="${not empty day}">
	window.scrollTo(100,500);
</c:if> 
} 
</script> 
<script type="text/javascript">
    var location1;
	var location2;	
	var address1;
	var address2;
	var latlng;
	var geocoder;
	var map;	
	var distance;
	var gDir;
	var inactiveCheck='';
	var GOOGLE_MAP_KEY = googlekey;
	function initLoader() {
		
		 var script = document.createElement("script");
	     script.type = "text/javascript";
		script.src = "https://maps.googleapis.com/maps/api/js?key="+GOOGLE_MAP_KEY ;
	     document.body.appendChild(script);
	     setTimeout('',6000);
	}
	
function calculateDistance() {
        var origAddress1=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		var origAddress2=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value;
		var origAddress3=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value;
		var origCity=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		var origZip=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		var origState=document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		var dd = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].selectedIndex;
		var origCountry = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[dd].text;
      	var address1 = origAddress1+" "+origAddress2+" "+origAddress3+","+origCity+" , "+origZip+","+origState+","+origCountry;
		var destAddress1=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
		var destAddress2=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value;
		var destAddress3=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value;				
		var destCity=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		var destZip=document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
		var destState=document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		var dd = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].selectedIndex;
		var destCountry = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[dd].text;
      	var address2 = destAddress1+" "+destAddress2+" "+destAddress3+","+destCity+" , "+destZip+","+destState+","+destCountry; 
        var geocoder = new google.maps.Geocoder;
      
      	geocoder.geocode({'address': address1}, function(results, status) {
      		
      		 var lat = results[0].geometry.location.lat(),
                                 lng = results[0].geometry.location.lng(),
                                 location1 = {lat: lat, lng: lng};
         
           geocoder.geocode({'address': address2}, function(results, status) {
        		 var lat = results[0].geometry.location.lat(),
                                   lng = results[0].geometry.location.lng(),
                                   location2 ={lat: lat, lng: lng};
      
         var bounds = new google.maps.LatLngBounds;
         var markersArray = [];
         var origin1 = location1;
        
         var destinationB = location2;

         var destinationIcon = 'https://chart.googleapis.com/chart?' +
             'chst=d_map_pin_letter&chld=D|FF0000|000000';
         var originIcon = 'https://chart.googleapis.com/chart?' +
             'chst=d_map_pin_letter&chld=O|FFFF00|000000';
        var kmmiles=document.getElementById('serviceOrderForm_serviceOrder_distanceInKmMiles').value;
   
         if(kmmiles=="Mile")
        	 {
        	
         var service = new google.maps.DistanceMatrixService;
         service.getDistanceMatrix({
           origins: [origin1],
           destinations: [destinationB],
           travelMode: 'DRIVING',
           unitSystem: google.maps.UnitSystem.IMPERIAL,
           avoidHighways: false,
           avoidTolls: false
         }, function(response, status) {
           if (status !== 'OK') {
             alert('Error was: ' + status);
           } else {
             var originList = response.originAddresses;
             var destinationList = response.destinationAddresses;
            
             for (var i = 0; i < originList.length; i++) {
               var results = response.rows[i].elements;
               
               for (var j = 0; j < results.length; j++) {
            	var soDitance = parseFloat(results[j].distance.text.slice(0,-2).replace(',','').replace(',','').replace(',','').trim()); 
                document.getElementById('serviceOrderForm_serviceOrder_distance').value=soDitance;
                document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
        		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
        		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
        		checkUnitValidation();
               }
             }
           }
         });
        	 }
         if(kmmiles=="KM" || kmmiles=="" )
        	 {
        	
        	 var service = new google.maps.DistanceMatrixService;
             service.getDistanceMatrix({
               origins: [origin1],
               destinations: [destinationB],
               travelMode: 'DRIVING',
               unitSystem: google.maps.UnitSystem.METRIC,
               avoidHighways: false,
               avoidTolls: false
             }, function(response, status) {
               if (status !== 'OK') {
                 alert('Error was: ' + status);
               } else {
                 var originList = response.originAddresses;
                 var destinationList = response.destinationAddresses;
                
                 for (var i = 0; i < originList.length; i++) {
                   var results = response.rows[i].elements;
                   
                   for (var j = 0; j < results.length; j++) {
                   console.log(results[j].distance.text)
                  
                   var soDitance = parseFloat(results[j].distance.text.slice(0,-2).replace(',','').replace(',','').replace(',','').trim()); 
                   document.getElementById('serviceOrderForm_serviceOrder_distance').value=soDitance;
                   document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].text = 'KM';
           		   document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].value = 'KM';
           		   document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].selected=true;	
           		checkUnitValidation();
                   }
                 }
               }
             });
        	 
        	 
        	 }
         
         });});
}
	</script> 
<script type="text/JavaScript"> 
function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}
</script>  
	<script type="text/javascript"> 
	<configByCorp:fieldVisibility componentId="component.field.State.NotManadotry">
	<c:set var="stateShowOrHide" value="Y" />
    </configByCorp:fieldVisibility>
	function ostateMandatory(){
	    var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	    var reqTrue=document.getElementById("originStateRequiredTrue");
	    var reqFalse=document.getElementById("originStateRequiredFalse");
	    if(originCountry=='United States'){
	    reqTrue.style.display='block';
	    reqFalse.style.display='none';
	    }else if(originCountry == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
		    reqTrue.style.display = 'none';		
		    reqFalse.style.display = 'block';
			</c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
			 reqTrue.style.display='block';
			    reqFalse.style.display='none';
			</c:if>
		
		}else{
	    reqFalse.style.display='block';
	    reqTrue.style.display='none';
	    } }	
	function dstateMandatory(){
	    var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	    var reqTrue=document.getElementById("destinationStateRequiredTrue");
	    var reqFalse=document.getElementById("destinationStateRequiredFalse");
	    if(destinationCountry=='United States'){
	    reqTrue.style.display='block';
	    reqFalse.style.display='none';
	    }else if(destinationCountry == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
		    reqTrue.style.display = 'none';		
		    reqFalse.style.display = 'block';
			</c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
			 reqTrue.style.display='block';
			    reqFalse.style.display='none';
			</c:if>
		
		} else{
	    reqFalse.style.display='block';
	    reqTrue.style.display='none';
	    }
	}
	 function findRevisedEstimateQuantitys(chargeCode, category, estimateExpense, basis, estimateQuantity, estimateRate, estimateSellRate, estimateRevenueAmount, estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
		var charge = document.forms['serviceOrderForm'].elements[chargeCode].value;
		var billingContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
		var shipNumber=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
		var rev=document.forms['serviceOrderForm'].elements[category].value;
		var comptetive = document.forms['serviceOrderForm'].elements['inComptetive'].value;
		if(charge!=''){
		if(rev == "Internal Cost"){
		    if(charge=="VANPAY" ){
		     	getVanPayCharges("Estimate");
		    }else if(charge=="DOMCOMM" ){
		     	getDomCommCharges("Estimate");
		    }else if(charge=="SALESCM" && comptetive=="Y") { 
			    var totalExpense = eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalExpense'].value);
			    var totalRevenue =eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalRevenue'].value); 
			    var commisionRate =eval(document.forms['serviceOrderForm'].elements['salesCommisionRate'].value); 
			    var grossMargin = eval(document.forms['serviceOrderForm'].elements['grossMarginThreshold'].value);
			    var commision=((totalRevenue*commisionRate)/100);
			    var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
			    calGrossMargin=calGrossMargin*100;
			    var calGrossMargin=Math.round(calGrossMargin);
			    if(calGrossMargin>=grossMargin)  {
			    	document.forms['serviceOrderForm'].elements[estimateExpense].value=Math.round(commision*10000)/10000; 
			    	document.forms['serviceOrderForm'].elements[basis].value='each';
			    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
				  <c:if test="${contractType}">
				  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
				  </c:if>			    	
			    } else { 
				    commision=((.8*totalRevenue)-totalExpense); 
				    commision=Math.round(commision*10000)/10000;
				    if(commision<0) { 
				    	document.forms['serviceOrderForm'].elements[estimateExpense].value=0; 
				    	document.forms['serviceOrderForm'].elements[basis].value='each';
				    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
						  <c:if test="${contractType}">
						  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
						  </c:if>			    					    	
				    } else {
				    	document.forms['serviceOrderForm'].elements[estimateExpense].value=Math.round(commision*10000)/10000;
				    	document.forms['serviceOrderForm'].elements[basis].value='each';
				    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
						  <c:if test="${contractType}">
						  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
						  </c:if>			    					    	
				    }   }  
		    }else if(charge=="SALESCM" && comptetive!="Y") {
		    var agree =confirm("This job is not competitive; do you still want to calculate commission?");
		    if(agree) {
		    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
		    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
		    http88.open("GET", url, true);
		    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate,basis,aid);};
		    http88.send(null);
		    } else {
		    return false;
		    }
		    } else {
		    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
		    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
		    http88.open("GET", url, true);
		    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis,aid);};
		    http88.send(null);
		    }
		} else{	
		var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
		    http88.open("GET", url, true);
		    http88.onreadystatechange = function(){ handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);};
		    http88.send(null);
		}
	  }else{
	    alert("Please select chargeCode!");
	 }   }
		  	function handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis,aid) { 
		          if (http88.readyState == 4)  {
		                  var results = http88.responseText
		                  results = results.trim(); 
		                  var res = results.split("#");
		                  if(res[3]=="AskUser1"){
		                  var reply = prompt("Please Enter Quantity", "");
		                  if(reply)   {
		                  if((reply>0 || reply<9)|| reply=='.') {
		               	    estimateQuantity.value = Math.round(reply*100)/100;
		               	}  else  {
		               		alert("Please Enter legitimate Quantity");
		               	} }
		               	 else{
		               		document.forms['serviceOrderForm'].elements[estimateQuantity].value = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		  				  <c:if test="${contractType}">
						  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
						  </c:if> 	
		               }  }
		               else{
		               if(res[1] == undefined){
		            	   document.forms['serviceOrderForm'].elements[estimateQuantity].value ="0.00";
			  				  <c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
							  </c:if>			    			            	   
		               }else{
		            	   document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100 ;
			  				  <c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= Math.round(res[1]*100)/100 ;
							  </c:if> 
		               }  }
		               if(res[5]=="AskUser"){
		               var reply = prompt("Please Enter the value of Rate", "");
		               if(reply)  {
		               if((reply>0 || reply<9)|| reply=='.')  {
		            	   document.forms['serviceOrderForm'].elements[estimateRate].value =Math.round(reply*10000)/10000 ;
		               	} else {
		               		alert("Please Enter legitimate Rate");
		               	} 	}
		               	 else{
		               		document.forms['serviceOrderForm'].elements[estimateRate].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
		               } 
		               }else{
		               if(res[4] == undefined){
		            	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
		               }else{
		            	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[4]*10000)/10000 ;
		               }  }
		               if(res[5]=="BuildFormula")
		               {
		               if(res[6] == undefined){
		            	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
		               }else{
		            	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[6]*10000)/10000;
		               }  }
		              var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		              var Q2 = document.forms['serviceOrderForm'].elements[estimateRate].value;
		              if(res[5]=="BuildFormula" && res[6] != undefined){
		            	  Q2 =res[6];
		              }
		              var E1=Q1*Q2;
		              var E2="";
		              E2=Math.round(E1*10000)/10000;
		              document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
		              var type = res[8];
		                var typeConversion=res[10];
		                if(type == 'Division')  {
		                	 if(typeConversion==0)  {
		                	   E1=0*1;
		                	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E1;
		                	 } else  {	
		                		E1=(Q1*Q2)/typeConversion;
		                		E2=Math.round(E1*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
		                	 }  }
		                if(type == ' ' || type == '')  {
		                		E1=(Q1*Q2);
		                		E2=Math.round(E1*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
		                }
		                if(type == 'Multiply')  {
		                		E1=(Q1*Q2*typeConversion);
		                		E2=Math.round(E1*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
		                } 
		                basis.value=res[9];

		                document.forms['serviceOrderForm'].elements[oldEstimateSellDeviation].value=0;
		                document.forms['serviceOrderForm'].elements[estimateDeviation].value=0;
		                document.forms['serviceOrderForm'].elements[oldEstimateDeviation].value=0;
		                document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=0; 
		            }   }
		  	function handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) { 
		  	     if (http88.readyState == 4)  {
		  	        var results = http88.responseText;
		  	        results = results.trim(); 
		  	      <c:if test="${!contractType}">
		  	        var res = results.split("#");
		  	        var multquantity=res[14];
		            if(res[17] == undefined){             
		         	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
		            }else{                  
		         	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[17]*10000)/10000 ;
		            }
		  	        if(res[3]=="AskUser1"){ 
		  	           var reply = prompt("Please Enter Quantity", "");
		  	             if(reply) {
			  	             if((reply>0 || reply<9)|| reply=='.')  {
			  	            	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(reply*100)/100;
				  				  <c:if test="${contractType}">
								  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= Math.round(reply*100)/100;
								  </c:if>			    			            	   			  	            	
			  	             }else {
			  	                alert("Please Enter legitimate Quantity");
			  	             } 
		  	             }else{
		  	             	document.forms['serviceOrderForm'].elements[estimateQuantity].value = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
			  				  <c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
							  </c:if>			    			            	   			  	            			  	             	
		  	             }
		           	}else{             
			            if(res[1] == undefined){
			         	 	document.forms['serviceOrderForm'].elements[estimateQuantity].value ="0.00";
			  				  <c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value="0.00";
							  </c:if>			    			            	   			  	            	
			         	 	
			            }else{
			         	 	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100;
			  				  <c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= Math.round(res[1]*100)/100;
							  </c:if> 
			            }   }
		  	       	if(res[5]=="AskUser"){ 
		               var reply = prompt("Please Enter the value of Rate", "");
		               if(reply)  {
			               if((reply>0 || reply<9)|| reply=='.') {
			            	 	document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(reply*10000)/10000;
			               } else {
			               		alert("Please Enter legitimate Rate");
			               	} 
			            }else{
		               		document.forms['serviceOrderForm'].elements[estimateRate].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
		               	} 
		  	       	}else{
		               if(res[4] == undefined){
		            	 document.forms['serviceOrderForm'].elements[estimateSellRate].value ="0.00";
		               }else{
		            	 document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(res[4]*10000)/10000 ;
		               }  
		  	      	 }
		  	        if(res[5]=="BuildFormula"){
		  	          if(res[6] == undefined){
		  	           		document.forms['serviceOrderForm'].elements[estimateSellRate].value ="0.00";
		  	           }else{
		  	            	document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(res[6]*10000)/10000 ;
		  	           }  
			         }
		            var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		  		  <c:if test="${contractType}">
		  				Q1=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
				  </c:if>
				            
		            var Q2 = document.forms['serviceOrderForm'].elements[estimateSellRate].value;
		            if(res[5]=="BuildFormula" && res[6] != undefined ){
		            	Q2 =res[6]
		            }
		            var oldRevenue = eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value);
		            var oldExpense=eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);
		            var E1=Q1*Q2;
		            var E2="";
		            E2=Math.round(E1*10000)/10000;
		            document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
		            var type = res[8];
		              var typeConversion=res[10];   
		              if(type == 'Division')   {
		              	 if(typeConversion==0) 	 {
		              	   E1=0*1;
		              	 	document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E1;
		              	 } else {	
		              		E1=(Q1*Q2)/typeConversion;
		              		E2=Math.round(E1*10000)/10000;
		              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
		              	 } }
		              if(type == ' ' || type == '') {
		              		E1=(Q1*Q2);
		              		E2=Math.round(E1*10000)/10000;
		              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
		              }
		              if(type == 'Multiply')  {
		              		E1=(Q1*Q2*typeConversion);
		              		E2=Math.round(E1*10000)/10000;
		              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
		              } 
		              var Q11 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		              var Q22 = document.forms['serviceOrderForm'].elements[estimateRate].value;
		              var E11=Q11*Q22;
		              var E22="";
		              E22=Math.round(E11*10000)/10000;
		              document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
		                if(type == 'Division')  {
		                	 if(typeConversion==0)  {
		                	   E11=0*1;
		                	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E11;
		                	 } else  {	
		                		E11=(Q11*Q22)/typeConversion;
		                		E22=Math.round(E11*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
		                	 }  }
		                if(type == ' ' || type == '')  {
		                		E11=(Q11*Q22);
		                		E22=Math.round(E11*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
		                }
		                if(type == 'Multiply')  {
		                		E11=(Q11*Q22*typeConversion);
		                		E22=Math.round(E11*10000)/10000;
		                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
		                }  
		  	         document.forms['serviceOrderForm'].elements[basis].value=res[9];
		             var chargedeviation=res[15];
		             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
		            	 var buyDependSellAccount=res[11];
			             var estimateRevenueAmt=0;
			             var finalEstimateRevenueAmount=0; 
			             var finalEstimateExpenceAmount=0;
			             var estimateSellRates=0; 
			             var sellDeviation=0;
			             var estimatepasspercentages=0;
			             var estimatepasspercentageRound=0;
			             var buyDeviation=0;
			             var oldEstDeviation =  document.forms['serviceOrderForm'].elements[estimateDeviation].value;
			             var oldEstSellDeviation = document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
			             sellDeviation=res[12];
			             buyDeviation=res[13]; 
		            	 document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=sellDeviation;
				         document.forms['serviceOrderForm'].elements[estimateDeviation].value=buyDeviation; 
		            	 document.forms['serviceOrderForm'].elements[deviation].value=chargedeviation; 
			             document.forms['serviceOrderForm'].elements['buyDependSell'].value=res[11]; 
			             estimateRevenueAmt=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
			             if(sellDeviation==0 && sellDeviation==undefined){
			            	 sellDeviation = document.forms['serviceOrderForm'].elements[oldEstSellDeviation].value;
				         }

			             finalEstimateRevenueAmount=Math.round((estimateRevenueAmt*(sellDeviation/100))*10000)/10000; 
			             document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=finalEstimateRevenueAmount;
			             if(buyDeviation==0 && buyDeviation==undefined){
			            	 buyDeviation = document.forms['serviceOrderForm'].elements[oldEstDeviation].value;
				         } 
			             if(buyDependSellAccount=="Y"){
			              	finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*10000)/10000;
			             }else{
			              	finalEstimateExpenceAmount= Math.round((estimateRevenueAmt*(buyDeviation/100))*10000)/10000;
			             }
			             document.forms['serviceOrderForm'].elements[estimateExpense].value=finalEstimateExpenceAmount;
			             estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
			 	         estimatepasspercentageRound=Math.round(estimatepasspercentages);
			 	         if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
			 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
			 	         }else{
			 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
			 	         } 
			 	        var estimateQuantity1 =0;
			 		    var estimateDeviation1=100;
			 		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
			 		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
			 		    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);  
			 		    var estimateRate1=0;
			 		    var estimateRateRound1=0; 
			 		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
			 	         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
			 	         } 
			 	        if( basis1=="cwt" || basis1=="%age"){
			 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
			 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
			 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;	  	
			 	       	}  else if(basis1=="per 1000"){
			 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
			 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
			 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		  	
			 	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
			 	          estimateRate1=(estimateExpense1/estimateQuantity1); 
			 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000; 
			 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		  	
			 		    } 
			 		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
			 		    if(estimateDeviation1!='' && estimateDeviation1>0){
				 		    estimateRate1=estimateRateRound1*100/estimateDeviation1;
				 		    estimateRateRound1=Math.round(estimateRate1*10000)/10000;
				 		    document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		
			 		    } 
		             }else {
			             document.forms['serviceOrderForm'].elements[estimateDeviation].value=0.00;
			             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=0.00;
			             var bayRate=0;
			             bayRate = res[17];
			             if(bayRate!='' && bayRate!=0 && bayRate!=undefined){
			               var estimateQuantityPay=0;
			               estimateQuantityPay= document.forms['serviceOrderForm'].elements[estimateQuantity].value;
			               Q1=estimateQuantityPay;
			               Q2=bayRate; 
			               if(type == 'Division')   {
			               	 if(typeConversion==0) 	 {
			               	   E1=0*1;
			               	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E1;
			               	 } else {	
			               		E1=(Q1*Q2)/typeConversion;
			               		E2=Math.round(E1*10000)/10000;
			               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
			               	 } }
			               if(type == ' ' || type == '') {
			               		E1=(Q1*Q2);
			               		E2=Math.round(E1*10000)/10000;
			               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
			               }
			               if(type == 'Multiply')  {
			               		E1=(Q1*Q2*typeConversion);
			               		E2=Math.round(E1*10000)/10000;
			               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
			               }
			               document.forms['serviceOrderForm'].elements[estimateRate].value = Q2;
			               var estimatepasspercentages=0;
			               var estimatepasspercentageRound=0;
			               var finalEstimateRevenueAmount=0; 
			               var finalEstimateExpenceAmount=0;
			               finalEstimateRevenueAmount=  document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
			               finalEstimateExpenceAmount=document.forms['serviceOrderForm'].elements[estimateExpense].value;
			               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
			  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
			  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
			  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
			  	           }else{
			  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
			  	           } 
			             }else{	           	   
			               var quantity =0;
			               var rate =0;
			               var estimatepasspercentages=0;
			               var estimatepasspercentageRound=0;
			               var finalEstimateRevenueAmount=0; 
			               var finalEstimateExpenceAmount=0;
			               quantity= document.forms['serviceOrderForm'].elements[estimateQuantity].value;	               
			               rate=document.forms['serviceOrderForm'].elements[estimateRate].value;	               
			               finalEstimateExpenceAmount = (quantity*rate); 
			               if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age")  {
			      	          finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
			               }
			               if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
			      	        	finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
			               }
			               finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*10000)/10000;
			               finalEstimateRevenueAmount=  document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
			               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
			  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
		       
			  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
			  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
			  	           }else{
			  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
			  	           }  }  }           
					try{
		     	    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
		    	    checkFloatNew('serviceOrderForm',estimateQuantity,'Nothing');
		    	    var quantityValue = document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		 	        var buyRate1=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
			        var estCurrency1=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var estimateExpenseQ1=0;
							Q1=buyRate1;
							Q2=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=E1;
				            var estLocalAmt1=0;
				            estimateExpenseQ1=document.forms['serviceOrderForm'].elements[estimateExpense].value	
				            estLocalAmt1=estimateExpenseQ1*Q2; 
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
					} 
			        buyRate1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
			        estCurrency1=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							var estimateRevenueAmountQ1=0;
							Q1=buyRate1;
							Q2=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*10000)/10000;
				            document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=E1;
				            var estLocalAmt1=0;
				            estimateRevenueAmountQ1=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value
				            estLocalAmt1=estimateRevenueAmountQ1*Q2; 
				            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
				            document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
					}	
				     var type01 = res[8];
				     var typeConversion01=res[10]; 
					}catch(e){} 
			        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
			        try{
			        var estVatP='vatPers'+aid;
			        var revenueP='estimateRevenueAmount'+aid;
			        var estVatA='vatAmt'+aid; 
			        calculateVatAmt(estVatP,revenueP,estVatA);
			        }catch(e){}
			        </c:if>            
		                //--------------------Start--estimateRevenueAmount-------------------------//
			               var revenueValue=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
			  	           var revenueRound=0;
			  	           var oldRevenueSum=0;
			  	           var balanceRevenue=0;
			  	           <c:if test="${not empty serviceOrder.id}">
			  	           	oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
			  	           </c:if> 	  	          
			  	           revenueRound=Math.round(revenueValue*10000)/10000;
			  	           balanceRevenue=revenueRound-oldRevenue;
			  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
			  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
			  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			  	           var revenueRoundNew=""+revenueRound;
			  	           if(revenueRoundNew.indexOf(".") == -1){
			  	           revenueRoundNew=revenueRoundNew+".00";
			  	           } 
			  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
			  	           revenueRoundNew=revenueRoundNew+"0";
			  	           }
			  	           document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=revenueRoundNew;
			  	           <c:if test="${not empty serviceOrder.id}">
			  	  	           var newRevenueSum=""+oldRevenueSum;
			  	  	           if(newRevenueSum.indexOf(".") == -1){
			  	  	           	newRevenueSum=newRevenueSum+".00";
			  	  	           } 
			  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			  	  	           	newRevenueSum=newRevenueSum+"0";
			  	  	           } 
			  	  	           document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
			  	           </c:if>
			  	           if(revenueRoundNew>0){
			  	           	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
			  	           }
			  	       //--------------------End--estimateRevenueAmount-------------------------//
			  	       
		               //-------------------Start---estimateExpense-------------------------//                
			              var expenseValue=document.forms['serviceOrderForm'].elements[estimateExpense].value;
			              var expenseRound=0;
			              var oldExpenseSum=0;
			              var balanceExpense=0;
			              <c:if test="${not empty serviceOrder.id}">
			              	oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
			              </c:if>
			              expenseRound=Math.round(expenseValue*10000)/10000;
			              balanceExpense=expenseRound-oldExpense;
			              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
			              balanceExpense=Math.round(balanceExpense*10000)/10000; 
			              oldExpenseSum=oldExpenseSum+balanceExpense;
			              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
			              var expenseRoundNew=""+expenseRound;
			              if(expenseRoundNew.indexOf(".") == -1){
			              	expenseRoundNew=expenseRoundNew+".00";
			              } 
			              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
			              	expenseRoundNew=expenseRoundNew+"0";
			              }
			              document.forms['serviceOrderForm'].elements[estimateExpense].value=expenseRoundNew;
			              <c:if test="${not empty serviceOrder.id}">
				              var newExpenseSum=""+oldExpenseSum;
				              if(newExpenseSum.indexOf(".") == -1){
				              	newExpenseSum=newExpenseSum+".00";
				              } 
				              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
				              	newExpenseSum=newExpenseSum+"0";
				              } 
				              document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
			              </c:if>
			              if(expenseRoundNew>0){
			              	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
			              }
			              //-------------------End---estimateExpense-------------------------//
			              //calculateGrossMargin();
			             <c:if test="${not empty serviceOrder.id}">
			              var revenueValue=0;
			              var expenseValue=0;
			              var grossMarginValue=0; 
			              var grossMarginPercentageValue=0; 
			              revenueValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
			              expenseValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
			              grossMarginValue=revenueValue-expenseValue;
			              grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
			              document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
			              if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
			            	    document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
			            	   }else{
			            	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
			            	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
			            	   	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
			            	   }
			            </c:if>
			            </c:if>      
			            <c:if test="${contractType}">
				 	       var res = results.split("#");  
				 	       document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value=res[18];
		 	               document.forms['serviceOrderForm'].elements['estimateContractValueDateNew'+aid].value=currentDateRateOnActualization();
		 	      		   document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(res[18]);
		 	      		   document.forms['serviceOrderForm'].elements['estSellValueDateNew'+aid].value=currentDateRateOnActualization();
		 	      		   document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value);
			 	           document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value=res[19];
			 	           document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value=currentDateRateOnActualization();
			 	      	   document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(res[19]);
			 	      	   document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value=currentDateRateOnActualization();
			 	      	   document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value);
			               var multquantity=res[14]
			              if(res[3]=="AskUser1"){ 
			               var reply = prompt("Please Enter Quantity", "");
			               if(reply) {
			               if((reply>0 || reply<9)|| reply=='.')  {
			            	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(reply*100)/100;
			            	<c:if test="${contractType}">
							  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= Math.round(reply*100)/100;
							  </c:if>	
			            	} else {
			            		alert("Please Enter legitimate Quantity");
			            	} }
			            	else{
			            		document.forms['serviceOrderForm'].elements[estimateQuantity].value =document.forms['serviceOrderForm'].elements[estimateQuantity].value ;
			            		<c:if test="${contractType}">
								  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
								  </c:if>	
			            	}
			            }else{             
			            if(res[1] == undefined){
			            document.forms['serviceOrderForm'].elements[estimateQuantity].value ="";
			            <c:if test="${contractType}">
						  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value="";
						  </c:if>
			            }else{
			            document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100;
			            <c:if test="${contractType}">
						  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value= Math.round(res[1]*100)/100;
						  </c:if>
			            }  }
			            if(res[5]=="AskUser"){ 
			            var reply = prompt("Please Enter the value of Rate", "");
			            if(reply)  {
			            if((reply>0 || reply<9)|| reply=='.') {
			            	document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value = Math.round(reply*10000)/10000;
			            	} else {
			            		alert("Please Enter legitimate Rate");
			            	} }
			            	else{
			            	document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
			            	} 
			            }else{
			            if(res[4] == undefined){
			            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value ="";
			            }else{
			            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value =res[4] ;
			            }  }
			            if(res[5]=="BuildFormula")   {
			            if(res[6] == undefined){
			            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value ="";
			            }else{
			             document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value =res[6] ;
			            }  }
			           var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
			           var Q2 = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
			           if(res[5]=="BuildFormula" && res[6] != undefined){
			        	   Q2 = res[6];
			           }
			            var oldRevenue = eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value);
			            var oldExpense=eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);		           
			           var E1=Q1*Q2;
			           var E2="";
			           E2=Math.round(E1*10000)/10000;
			           document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
			           var type = res[8];
			             var typeConversion=res[10];   
			             if(type == 'Division')   {
			             	 if(typeConversion==0) 	 {
			             	   E1=0*1;
			             	   document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E1;
			             	 } else {	
			             		E1=(Q1*Q2)/typeConversion;
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
			             	 } }
			             if(type == ' ' || type == '' ) {
			             		E1=(Q1*Q2);
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
			             }
			             if(type == 'Multiply')  {
			             		E1=(Q1*Q2*typeConversion);
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
			             } 
			             document.forms['serviceOrderForm'].elements[basis].value=res[9];
			             var chargedeviation=res[15];
			             document.forms['serviceOrderForm'].elements[deviation].value=chargedeviation;
			             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
			             document.forms['serviceOrderForm'].elements['buyDependSellNew'+aid].value=res[11];
			             var buyDependSellAccount=res[11];
			             var estimateRevenueAmount1=0;
			             var finalEstimateRevenueAmount=0; 
			             var finalEstimateExpenceAmount=0;
			             var finalEstimateContractRateAmmount=0;
			             var estimateContractRateAmmount1=0;
			             var estimatePayableContractRateAmmount1=0;
			             var estimateSellRate1=0; 
			             var sellDeviation1=0;
			             var  estimatepasspercentage=0
			             var estimatepasspercentageRound=0;
			             var buyDeviation1=0;
			             sellDeviation1=res[12]
			             buyDeviation1=res[13] 
			             var estimateContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value
			             estimateContractRateAmmount1 =document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value
			             estimateRevenueAmount1=estimateContractRateAmmount1/estimateContractExchangeRate1;
			             estimateRevenueAmount1=Math.round(estimateRevenueAmount1*10000)/10000;
			             estimateRevenueAmount1=document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;  
			             finalEstimateRevenueAmount=Math.round((estimateRevenueAmount1*(sellDeviation1/100))*10000)/10000;
			             finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount1*(sellDeviation1/100))*10000)/10000;
			             document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value=finalEstimateContractRateAmmount; 
			             if(buyDependSellAccount=="Y"){
			              finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation1/100))*10000)/10000;
			             }else{
			              finalEstimateExpenceAmount= Math.round((estimateRevenueAmount1*(buyDeviation1/100))*10000)/10000;
			             }
			             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=sellDeviation1;
			             document.forms['serviceOrderForm'].elements['oldEstimateSellDeviationNew'+aid].value=sellDeviation1;
			             document.forms['serviceOrderForm'].elements[estimateDeviation].value=buyDeviation1;
			             document.forms['serviceOrderForm'].elements['oldEstimateDeviationNew'+aid].value=buyDeviation1;
				    	 var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
				 	    estimatePayableContractRateAmmount1=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
			             estimatePayableContractRateAmmount1=Math.round(estimatePayableContractRateAmmount1*10000)/10000;
			             document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estimatePayableContractRateAmmount1;
			             calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
			             }else {
			             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=0;
			             document.forms['serviceOrderForm'].elements['oldEstimateSellDeviationNew'+aid].value=0;
			             document.forms['serviceOrderForm'].elements[estimateDeviation].value=0;
			            document.forms['serviceOrderForm'].elements['oldEstimateDeviationNew'+aid].value=0;
			             var bayRate=0
			             bayRate = res[17] ;
			             if(bayRate!=0){
			             var estimateQuantityPay=0;
			             estimateQuantityPay= document.forms['serviceOrderForm'].elements[estimateQuantity].value;
			             Q1=estimateQuantityPay;
			             Q2=bayRate; 
			             if(type == 'Division')   {
			             	 if(typeConversion==0) 	 {
			             	   E1=0*1;
			             	   document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E1;
			             	 } else {	
			             		E1=(Q1*Q2)/typeConversion;
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
			             	 } }
			             if(type == ' ' || type == '') {
			             		E1=(Q1*Q2);
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
			             }
			             if(type == 'Multiply')  {
			             		E1=(Q1*Q2*typeConversion);
			             		E2=Math.round(E1*10000)/10000;
			             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
			             }
			               document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value =Math.round(Q2*10000)/10000; 
			             } } 
			             var estimatePayableContractRatecal= document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value
			             var estimatePayableContractRateAmmountcal= document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value
			             var  estimatePayableContractCurrencycal=document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value;			        
						 if(estimatePayableContractCurrencycal.trim()!=""){
						 var estimatePayableContractExchangeRatecal=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
			             var estimateRatecal=estimatePayableContractRatecal/estimatePayableContractExchangeRatecal;
			             estimateRatecal=Math.round(estimateRatecal*10000)/10000;
			             var estimateExpensecal=estimatePayableContractRateAmmountcal/estimatePayableContractExchangeRatecal;
			             estimateExpensecal=Math.round(estimateExpensecal*10000)/10000;
			             document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRatecal; 
			             document.forms['serviceOrderForm'].elements[estimateExpense].value=estimateExpensecal; 
					     } 
						 var estimateRatecal= document.forms['serviceOrderForm'].elements[estimateRate].value;
			             var estimateExpensecal= document.forms['serviceOrderForm'].elements[estimateExpense].value;
					     var estCurrencycal=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
						 if(estCurrencycal.trim()!=""){
						 var estExchangeRatecal = document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
						 var estLocalRatecal=estimateRatecal*estExchangeRatecal;
						 estLocalRatecal=Math.round(estLocalRatecal*10000)/10000;
						 var estLocalAmountcal=estimateExpensecal*estExchangeRatecal;
						 estLocalAmountcal=Math.round(estLocalAmountcal*10000)/10000;
						 document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estLocalRatecal;
			             document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmountcal;
						 }  
						 var estimateContractRatecal= document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value
			             var estimateContractRateAmmountcal= document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value
			             var  estimateContractCurrencycal=document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value;
			             if(estimateContractCurrencycal.trim()!=""){
			             var estimateContractExchangeRatecal=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
			             var estimateSellRatecal=estimateContractRatecal/estimateContractExchangeRatecal;
			             estimateSellRatecal=Math.round(estimateSellRatecal*10000)/10000;
			             var estimateRevenueAmountcal=estimateContractRateAmmountcal/estimateContractExchangeRatecal;
			             estimateRevenueAmountcal=Math.round(estimateRevenueAmountcal*10000)/10000;
			             document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRatecal;
			             document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=estimateRevenueAmountcal;
			             }  
			             var estimateSellRatecal= document.forms['serviceOrderForm'].elements[estimateSellRate].value;
			             var estimateRevenueAmountcal= document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
					     var estSellCurrencycal=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;
						 if(estSellCurrencycal.trim()!=""){
							 var estSellExchangeRatecal = document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
							 var estSellLocalRatecal=estimateSellRatecal*estSellExchangeRatecal;
							 estSellLocalRatecal=Math.round(estSellLocalRatecal*10000)/10000;
							 var estSellLocalAmountcal=estimateRevenueAmountcal*estSellExchangeRatecal;
							 estSellLocalAmountcal=Math.round(estSellLocalAmountcal*10000)/10000;
							 document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=estSellLocalRatecal;
				             document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estSellLocalAmountcal;
						 } 
			             var estimateRevenueAmountpasspercentages= document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value
						 var estimateExpensepasspercentages= document.forms['serviceOrderForm'].elements[estimateExpense].value
			             var estimatepasspercentagesTest = (estimateRevenueAmountpasspercentages/estimateExpensepasspercentages)*100;
			 	         var estimatepasspercentageRoundTest=Math.round(estimatepasspercentagesTest);
			 	         if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
			 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
			 	         }else{
			 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRoundTest;
			 	         } 
			  	         var type01 = res[8];
			             var typeConversion01=res[10];  
			             //#7298 - Copy esimates not calculating preset calculations in charges start 
				         //#7298 - Copy esimates not calculating preset calculations in charges End 
						//--------------------Start--estimateRevenueAmount-------------------------//
			               var revenueValue=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
			  	           var revenueRound=0;
			  	           var oldRevenueSum=0;
			  	           var balanceRevenue=0;
			  	           <c:if test="${not empty serviceOrder.id}">
			  	           	oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
			  	           </c:if> 	  	          
			  	           revenueRound=Math.round(revenueValue*10000)/10000;
			  	           balanceRevenue=revenueRound-oldRevenue;
			  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
			  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
			  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			  	           var revenueRoundNew=""+revenueRound;
			  	           if(revenueRoundNew.indexOf(".") == -1){
			  	           revenueRoundNew=revenueRoundNew+".00";
			  	           } 
			  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
			  	           revenueRoundNew=revenueRoundNew+"0";
			  	           }
			  	           document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=revenueRoundNew;
				                 
					        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
					        try{
					        var estVatP='vatPers'+aid;
					        var revenueP='estimateRevenueAmount'+aid;
					        var estVatA='vatAmt'+aid;
					        calculateVatAmt(estVatP,revenueP,estVatA);
					        }catch(e){}
					        </c:if>  
			  	           <c:if test="${not empty serviceOrder.id}">
			  	  	           var newRevenueSum=""+oldRevenueSum;
			  	  	           if(newRevenueSum.indexOf(".") == -1){
			  	  	           	newRevenueSum=newRevenueSum+".00";
			  	  	           } 
			  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			  	  	           	newRevenueSum=newRevenueSum+"0";
			  	  	           } 
			  	  	           document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
			  	           </c:if>
			  	           if(revenueRoundNew>0){
			  	           	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
			  	           }
			  	       //--------------------End--estimateRevenueAmount-------------------------// 
		               //-------------------Start---estimateExpense-------------------------//                
			              var expenseValue=document.forms['serviceOrderForm'].elements[estimateExpense].value;
			              var expenseRound=0;
			              var oldExpenseSum=0;
			              var balanceExpense=0;
			              <c:if test="${not empty serviceOrder.id}">
			              	oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
			              </c:if>
			              expenseRound=Math.round(expenseValue*10000)/10000;
			              balanceExpense=expenseRound-oldExpense;
			              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
			              balanceExpense=Math.round(balanceExpense*10000)/10000; 
			              oldExpenseSum=oldExpenseSum+balanceExpense;
			              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
			              var expenseRoundNew=""+expenseRound;
			              if(expenseRoundNew.indexOf(".") == -1){
			              	expenseRoundNew=expenseRoundNew+".00";
			              } 
			              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
			              	expenseRoundNew=expenseRoundNew+"0";
			              }
			              document.forms['serviceOrderForm'].elements[estimateExpense].value=expenseRoundNew;
			              <c:if test="${not empty serviceOrder.id}">
				              var newExpenseSum=""+oldExpenseSum;
				              if(newExpenseSum.indexOf(".") == -1){
				              	newExpenseSum=newExpenseSum+".00";
				              } 
				              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
				              	newExpenseSum=newExpenseSum+"0";
				              } 
				              document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
			              </c:if>
			              if(expenseRoundNew>0){
			              	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
			              }
			              //-------------------End---estimateExpense-------------------------//
			              //calculateGrossMargin();
			             <c:if test="${not empty serviceOrder.id}">
			              var revenueValue=0;
			              var expenseValue=0;
			              var grossMarginValue=0; 
			              var grossMarginPercentageValue=0; 
			              revenueValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
			              expenseValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
			              grossMarginValue=revenueValue-expenseValue;
			              grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
			              document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
			              if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
			            	    document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
			            	   }else{
			            	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
			            	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
			            	   	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
			            	   }
			            </c:if>  
			            </c:if> 
		  	        }   }
		  	 function getHTTPObject88() {
		    	    var xmlhttp;
		    	    if(window.XMLHttpRequest) {
		    	        xmlhttp = new XMLHttpRequest();
		    	    }
		    	    else if (window.ActiveXObject)  {
		    	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		    	        if (!xmlhttp)   {
		    	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		    	        }  }
		    	    return xmlhttp;
		    	   }
		    	 var http88 = getHTTPObject88(); 
		    	 var httpTemplate = getHTTPObject88();
</script>
<script type="text/javascript"> 
      function getAddress(targetElement,temp){
         	 var address=targetElement.value;
        	 address=address.trim();
        	 var cid='${customerFile.id}';
        	 if(address!='')
        	 {
            	if(address=='Origin Address')
            	{
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
            	} else if(address=='Destination Address'){
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
            	}else{
                	var tempAddress = address.split('^');
                	if(tempAddress[1]=='add'){
                       var url="getAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&descType="+ encodeURI(tempAddress[0]);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
                	}
                	if(tempAddress[1]=='std'){
                       var url="getStandardAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&stdAddId="+ encodeURI(tempAddress[0]);
					   httpst.open("GET",url,true);
               		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
               		   httpst.send(null);
                   }  }  }  }
      function handleHttpResponseState(temp,address){
  		if (httpst.readyState == 4){
      			var results = httpst.responseText                  
                  results = results.trim();   
                  var res = results.split("~");                                                                   
                 	if(address=='Origin Address')  	{      
                     	document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2];
						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value = res[4];
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[5];
						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[6];
						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[7];
						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[8];
						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = res[9];
						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = res[10];
						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = res[11];
						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = res[12];
						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[5];
						getOriginCountryCode();
						getADDState('OA');
						ostateMandatory();
						zipCode();
						autoPopulate_customerFile_originCityCode(this);
  					} 
                 	else if(address=='Destination Address') 	{
                     	document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value = res[4];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[5];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[6];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[7];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[8];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = res[9];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = res[10];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = res[11];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = res[12];
						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[5];
						getDestinationCountryCode();
						getADDState('DA');
						autoPopulate_customerFile_destinationCityCode(this);
					}
                 	else 
                	{
                 		var tempAddress = address.split('^');
                 		if(tempAddress[1]=='add'){
                 	if(temp=='origin'){ 
                           	if(res!=''){  
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2];
	                 			//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = '';
	                     			    						   
	                 			targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'];            				
	             				for(i=0;i<targetElement.length;i++) { 	
	                				if(res[3] == targetElement.options[i].value) { 
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].text = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].selected=true;
	         					}
	            				}  									
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[4];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[5];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[6];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[7];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
	    						getOriginCountryCode();
	    						getADDState('OA');
	    						ostateMandatory();
	    						zipCode();
	                     	}else{
	                     		document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value ='';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = '';
	    						//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=''
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
		                     }
                           	autoPopulate_customerFile_originCityCode(this);
    					}
                 		if(temp=='dest'){
                 			if(res!=''){
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
	    						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = '';
								targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'];            				
	             				for(i=0;i<targetElement.length;i++) {
	                				if(res[3] == targetElement.options[i].value) { 
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].text = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].selected=true;
	         					}
	            				} 
	             				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[4];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[5];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[6];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[7];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
	    						getDestinationCountryCode();
	    						getADDState('DA');
	    					}else{
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = '';
	    						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
		    				}
                 			autoPopulate_customerFile_destinationCityCode(this);
                 		} }
                 	if(tempAddress[1]=='std'){
                     	if(temp=='origin'){                     		
                 			document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2]; 
                 			targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'];            				
             				for(i=0;i<targetElement.length;i++) { 
                				if(res[3] == targetElement.options[i].value) {	 
                 			document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].text = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].value = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].selected=true;
         					}
            				}  										
    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[4];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[5];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[6];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[7];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = res[8];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = res[9];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value =res[10];
    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
    						getOriginCountryCode();
    						getADDState('OA');
    						ostateMandatory();
    						zipCode();
    						autoPopulate_customerFile_originCityCode(this);
    					}
                 		if(temp=='dest'){
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
							targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'];            				
             				for(i=0;i<targetElement.length;i++) {
                				if(res[3] == targetElement.options[i].value) { 
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].text = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].value = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].selected=true;
         					}
            				} 
             				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[4];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[5];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[6];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[7];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = res[8];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = res[9];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = res[10];
    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
    						getDestinationCountryCode();
    						getADDState('DA');
    						autoPopulate_customerFile_destinationCityCode(this);
    					}  } }	   } }      		      
	var httpst = getHTTPObjectst();
	function getHTTPObjectst() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   } 
    return xmlhttp;
} 
</script>
<script language="javascript">
function getHTTPObject21() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var httpADDDetail = getHTTPObject21(); 
function getADDState(from) {
var country = "";
var getStateList = "";
var countryCode = "";
if(from == 'OA') {
	country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	getStateList = handleHttpResponseAddOrg;
}
if(from == 'DA') {
	country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	getStateList = handleHttpResponseAddDest;
}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2="+encodeURI(countryCode);
	httpADDDetail.open("GET", url, true);
	httpADDDetail.onreadystatechange = getStateList;
	httpADDDetail.send(null);
} 
function handleHttpResponseAddOrg() {
	 if (httpADDDetail.readyState == 4) {
    	var tempState = document.forms['serviceOrderForm'].elements['addAddressState'].value;
    	var results = httpADDDetail.responseText;
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
       	targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
			
			if (tempState=='' && document.getElementById("originState").value == '${serviceOrder.originState}' ){
				document.getElementById("originState").options[i].selectedIndex = true;
			}
			if(tempState!='' && document.getElementById("originState").options[i].value == tempState ){
				document.getElementById("originState").options[i].selected = true;
			} } } }   }  
function handleHttpResponseAddDest() {
	if (httpADDDetail.readyState == 4)  {
    	var tempState = document.forms['serviceOrderForm'].elements['addAddressState'].value;
    	var results = httpADDDetail.responseText;
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
       targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
			if (tempState=='' && document.getElementById("destinationState").value == '${serviceOrder.destinationState}'){				
			   document.getElementById("destinationState").options[i].selectedIndex = true;
			}
			if (document.getElementById("destinationState").options[i].value == tempState && tempState!='' ){
				   document.getElementById("destinationState").options[i].selected = true;
				} } } } } 
function selectAll(str){ 
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('displayOnQuote'+id[i].trim()).checked = true;
		  }else{
			  document.getElementById('displayOnQuote'+id[i].trim()).checked = false;
		  }  } }
function selectAdd(str){ 
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('additionalService'+id[i].trim()).checked = true;
		  }else{
			  document.getElementById('additionalService'+id[i].trim()).checked = false;
		  }
	  }
	}
function findClosedCompanyDivisionForCopy(companyCode){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
			    	    var url = "addWithPricingServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
			    		document.forms['serviceOrderForm'].action= url;	 
			    		document.forms['serviceOrderForm'].submit();
			    		return true;    
			       }else{
			    	  alert('You can not copy this serviceOrder, As Company Division is closed.') 
			    	  return false;
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
function addWithPricingServiceOrder(){
	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
	<c:set var="closedCompanyDivision" value="Y" />
    </configByCorp:fieldVisibility>
	var check =saveValidation('Add');
	var listSize='${shipSize}';
	   	   if(!check){
		   return false; 
	   }else if(listSize == 99){
		alert('Maximum limit of service order is 99 for a customer.');
	  return false;  
		} 
	   	<c:if test="${closedCompanyDivision=='Y'}">
	   	if('${serviceOrder.companyDivision}'!=''){
	   	findClosedCompanyDivisionForCopy('${serviceOrder.companyDivision}'); 
	   	return false; 
	   	}else{
	   		var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	   	    var url = "addWithPricingServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
	   		document.forms['serviceOrderForm'].action= url;	 
	   		document.forms['serviceOrderForm'].submit();
	   		return true;	
	   	}
		</c:if>  
	<c:if test="${closedCompanyDivision!='Y'}">	
    var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
    var url = "addWithPricingServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
	document.forms['serviceOrderForm'].action= url;	 
	document.forms['serviceOrderForm'].submit();
	return true;
	</c:if>  
}


function CopyOrderWithResources(){
 
	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
    <c:set var="closedCompanyDivision" value="Y" />
    </configByCorp:fieldVisibility>
    var check =saveValidation('Add');
    var listSize='${shipSize}';
           if(!check){
           return false; 
       }else if(listSize == 99){
        alert('Maximum limit of service order is 99 for a customer.');
      return false;  
        } 
        <c:if test="${closedCompanyDivision=='Y'}">
        if('${serviceOrder.companyDivision}'!=''){
        findClosedCompanyDivisionForCopy('${serviceOrder.companyDivision}'); 
        return false; 
        }else{
            var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
            var url = "addWithPricingServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
            document.forms['serviceOrderForm'].action= url;  
            document.forms['serviceOrderForm'].submit();
            return true;    
        }
        </c:if>  
    <c:if test="${closedCompanyDivision!='Y'}"> 
	
    var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
    var url = "addWithResourcesServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
    document.forms['serviceOrderForm'].action= url;  
    document.forms['serviceOrderForm'].submit();
    return true;
    </c:if>  
}



function checkUnitValidation(){
	var e4 = document.getElementById('hid4');
	if(document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value!=''){
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value =(document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value).trim();
		e4.style.visibility = 'visible';	
	}else{
		e4.style.visibility = 'collapse';
	}
	
}
function calculateRevenueNew(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	   var revenueValue=0;
	    var revenueRound=0;
	    var oldRevenue=0;
	    var oldRevenueSum=0;
	    var balanceRevenue=0;
	    oldRevenue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
	    <c:if test="${not empty serviceOrder.id}">
	    oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	    </c:if>
	    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
	    checkFloatNew('serviceOrderForm',quantity,'Nothing');
	    var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
	    checkFloatNew('serviceOrderForm',sellRate,'Nothing');
	    var sellRateValue = document.forms['serviceOrderForm'].elements[sellRate].value;
	    if(basisValue=="cwt" || basisValue== "%age"){
	    revenueValue=(quantityValue*sellRateValue)/100 ;
	    }
	    else if(basisValue=="per 1000"){
	    revenueValue=(quantityValue*sellRateValue)/1000 ;
	    }else{
	    revenueValue=(quantityValue*sellRateValue);
	    } 
	    revenueRound=Math.round(revenueValue*10000)/10000;
	    balanceRevenue=revenueRound-oldRevenue;
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	    oldRevenueSum=oldRevenueSum+balanceRevenue; 
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    var revenueRoundNew=""+revenueRound;
	    if(revenueRoundNew.indexOf(".") == -1){
	    revenueRoundNew=revenueRoundNew+".00";
	    } 
	    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
	    revenueRoundNew=revenueRoundNew+"0";
	    }
	    document.forms['serviceOrderForm'].elements[revenue].value=revenueRoundNew;
	    try{
	        var buyRate1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
	        var estCurrency1=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					Q1=buyRate1;
					Q2=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		            var E1=Q1*Q2;
		            E1=Math.round(E1*10000)/10000;
		            document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=E1;
		            var estLocalAmt1=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt1=(quantityValue*E1)/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt1=(quantityValue*E1)/1000 ;
		                }else{
		                	estLocalAmt1=(quantityValue*E1);
		                }	 
		            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
		            document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
			}
			<c:if test="${contractType}">
				try{
					var estimateContractExchangeRate=0;
					estimateContractExchangeRate=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value; 
					var QbuyRate1=0;
					QbuyRate1=buyRate1;
					document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value=QbuyRate1*estimateContractExchangeRate
					var Q11=0;
					var Q21=0; 
					Q11=document.forms['serviceOrderForm'].elements[quantity].value;
					Q21=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		            var E11=Q11*Q21;
		            var estLocalAmt11=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt11=E11/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt11=E11/1000 ;
		                }else{
		                	estLocalAmt11=E11;
		                }	 
		            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
		            document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
				}catch(e){}     
			</c:if>
	    }catch(e){}
	    <c:if test="${not empty serviceOrder.id}">
	    var newRevenueSum=""+oldRevenueSum;
	    if(newRevenueSum.indexOf(".") == -1){
	    newRevenueSum=newRevenueSum+".00";
	    } 
	    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	    newRevenueSum=newRevenueSum+"0";
	    } 
	    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	    </c:if>
	    if(revenueRoundNew>0){
	    document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
	    }
	    try{
	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	    calculateVatAmt(estVatPer,revenue,estAmt);
	    </c:if>
	    }catch(e){} 	  
	   calculateGrossMargin();
	 }   
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode != 46 && charCode > 31 
     && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

</script>
<script type="text/javascript">
var r1={
		 'quotes':/['\"'&'\\']/g,
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />	
		<!--    This section copy from serviceorderform    -->
	
  <%-- <c:if test="${serviceOrder.job == 'OFF'}">
	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
  <tr>
	<td height="10" width="100%" align="left" style="margin:0px">
		<div onClick="javascript:animatedcollapse.toggle('resources');loadResources();" style="margin:0px">
		     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Resources</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
	     </div>
	     <div id="resources" class="switchgroup1">
				<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;margin-left:25px;" >
					<tr>
					  	<td>
				 		  	<input type="button" class="cssbutton1" onclick="getDayTemplate();" value="Resource Template" style="width:150px; height:25px; margin:5px 0px " tabindex=""/>
				 		</td>
				 		<td align="left" width="5px"></td>
				 		<td id="createPriceButton">
							<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px;  margin:5px 0px;" tabindex=""/>
							<input type="button" class="cssbutton1" onclick="transferResourceToWorkTicket();" value="Transfer Resource to WorkTicket" style="width:200px; height:25px;  margin:5px 0px;" tabindex=""/>
						</td>
					</tr>
			 	</table>
			 	<div id="resourcMapAjax">
			 	
			 	</div>
		</div>
</td>
</tr>
</configByCorp:fieldVisibility>
</c:if> --%>
				 <div id="weightVolSection">
					<tr id="weightVolMain">					 
					   <td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					   <c:if test="${serviceOrder.job !='RLO'}"> 
					   <div  onClick="javascript:animatedcollapse.toggle('weightnvolume'),toCheckWeightnVolumeRadio();" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left">
						</td>
						<td class="headtab_center" >&nbsp;Weights & Volume
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>						
                      </c:if>
					  <div id="weightnvolume" >
					  <table class="detailTabLabel" border="0" style="margin-left:-11px">
					  <tr>
					  <td align="right" class="listwhitetext"><a href="#" style="cursor:help; margin-left:10px;" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.equipment',this);return false" >
					  Planned&nbsp;Equipment</a></td>
					<td> 
					<configByCorp:customDropDown 	listType="map" list="${EQUIP_isactive}" fieldValue="${miscellaneous.equipment}" 
										attribute="id='serviceOrderForm_miscellaneous_equipment' class=list-menu name='miscellaneous.equipment'  
										style='width:251px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
					<%-- <s:select cssClass="list-menu" name="miscellaneous.equipment" list="%{EQUIP}" cssStyle="width:81px" headerKey="" headerValue=" " onchange="changeStatus();" tabindex="" /> --%>
					</td>
					<td width="70px" align="right" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.transitDays',this);return false">Transit&nbsp;Days</a></td>
					<td width="66px"><s:textfield name="serviceOrder.transitDays" cssClass="input-text" id="serviceOrder.transitDays" maxlength="25" cssStyle="width:60px;" tabindex="" onchange="calcPreferedDays();"/></td>
					<td width="70px" align="right" class="listwhitetext">Packing Days</td>
					<td width="66px"><s:textfield name="serviceOrder.packingDays" cssClass="input-text"  id="serviceOrder.packingDays" cssStyle="width:60px;" maxlength="25" tabindex=""/></td>
					
					<%-- <td align="right">Groupage</td>
                     <td width="40px"><s:checkbox name="serviceOrder.grpPref" tabindex="31" onchange="" /></td> --%>
                      <td align="right">Preferred&nbsp;Delivery</td>                                        
                          <c:if test="${not empty serviceOrder.grpDeliveryDate}">
							<s:text id="serviceOrderGrpDeliveryDate" name="${FormDateValue}"><s:param name="value" value="serviceOrder.grpDeliveryDate"/></s:text>
							<td style="width:60px;"><s:textfield id="grpDeliveryDate" cssClass="input-text" name="serviceOrder.grpDeliveryDate" value="%{serviceOrderGrpDeliveryDate}" cssStyle="width:65px" maxlength="11" onfocus="changeStatus();" readonly="true" onkeydown="onlyDel(event,this)" tabindex=""/></td><td><img id="grpDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.grpDeliveryDate}">
							<td style="width:60px;"><s:textfield id="grpDeliveryDate" cssClass="input-text" name="serviceOrder.grpDeliveryDate" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="onlyDel(event,this)" tabindex=""/></td><td><img id="grpDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								            <td class="listwhitetext" align="left" colspan="0" style="padding-left: 10px;">Excess Weight Billing
								<configByCorp:customDropDown 	listType="map" list="${excessWeightBillingList}" fieldValue="${miscellaneous.excessWeightBilling}"      attribute="id='serviceOrderForm_miscellaneous_excessWeightBilling' class=list-menu name='miscellaneous.excessWeightBilling'  style='width:250px;height:19px;' onchange='' tabindex='' headerKey='' headerValue='' "/>
						    </td>
                            </tr>
							<tr><td colspan="5">
							<div id="hid">
							<table class="detailTabLabel" border="0">
							<tbody>
								<tr>
									<td></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td></tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='labels.autoboat' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleNumberAuto" size="6" maxlength="2" required="true"  tabindex="" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateAuto" size="6" maxlength="2" required="true"  tabindex="" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAuto" size="6" maxlength="2" required="true"  tabindex="" /></td>
									<td></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.vehicleweight' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleAutoWeight" onchange="onlyFloat(this);"  size="6" maxlength="10"  tabindex="" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimatedAutoWeight" onchange="onlyFloat(this);" size="6" maxlength="10"  tabindex="" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAutoWeight" onchange="onlyFloat(this);" size="6" maxlength="10"  tabindex="" /></td>
									<td></td>
								</tr>
							</tbody>
						</table>
						</div></td>
							</tr>			
						</table>
					  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;"><tr><td>
					   <div  onClick="javascript:animatedcollapse.toggle('weightnvolumesub1'),toIncrementWeightTabSub1();" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left">
						</td>
						<td class="headtab_center" >&nbsp;(Pounds & Cft)
						<c:if test="${weightType == 'lbscft'}">			
						   <INPUT type="radio" name="weightType" id="weightnVolumeRadio" checked="checked"    value="lbscft" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();" tabindex="">
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						 <INPUT type="radio" name="weightType" id="weightnVolumeRadio"  value="lbscft" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();" tabindex="">
						</c:if>
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>						
					  <div id="weightnvolumesub1" >
						<table class="detailTabLabel" border="0" width="" style="margin-left:-5px">
							<tbody>
							<tr><td height="5px"></td></tr>
								<tr>
									<td></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.chargeableWeight' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.rewgh/fnl$' /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message
										key='labels.grossweight' /></td>
									<td align="left" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleGrossWeight" size="6"
										maxlength="10" required="true"
										onchange="calcNetWeight1();" tabindex="62" /></td>
										<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimateGrossWeight" size="6"
											maxlength="10" required="true"											
											onchange="calcNetWeight2();checkPricePointDetails('lbscft');" tabindex="70"/></td>									
										<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.actualGrossWeight" size="6"
											maxlength="10" required="true"										
											onchange="findWeight(),calcNetWeight3();" tabindex="78" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableGrossWeight" size="6"
										maxlength="10" required="true"									
										onchange="calcNetWeight5();" tabindex="86" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghGross"
										size="6" required="true" maxlength="10"										
										onchange="calcNetWeight4();" tabindex="92" /></td>									
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.tareweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleTareWeight" size="6" maxlength="8"
										required="true" 
										onchange="calcNetWeight1();" tabindex="63" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.estimateTareWeight" size="6" maxlength="8"
										required="true" 
										onchange="calcNetWeight2();" tabindex="71" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.actualTareWeight" size="6" maxlength="8"
										required="true" 
										onchange="findTareWeight(),calcNetWeight3();" tabindex="79" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableTareWeight" size="6"
										maxlength="8" required="true"										
										onchange="calcNetWeight5();" tabindex="87" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghTare"
										size="6" required="true" maxlength="8"										
										onchange="calcNetWeight4();" tabindex="95" /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width=""><fmt:message
										key='labels.netweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleNetWeight" size="6" maxlength="10"
										required="true"
										onchange="calcNetWeightLbs1();" tabindex="64" /></td>									
										<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimatedNetWeight" size="6"
											maxlength="10" required="true"
											onchange="calcNetWeightLbs2();" tabindex="72" /></td>									
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualNetWeight" size="6" maxlength="10" required="true" 
											onchange="findNetWeight();calcNetWeightLbs3();" tabindex="80" /></td>
									<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetWeight" onchange="calcNetWeightLbs4();" size="6" maxlength="10" required="true"  tabindex="88" /></td>
									<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNet" size="6" onchange="calcNetWeightLbs5();" maxlength="10" required="true"  tabindex="96" /></td>
									<td width="20px"></td>
									<td width="380px"></td>
									<c:if test="${empty serviceOrder.id}">
										<td><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>									
									</c:if>
									<c:if test="${not empty serviceOrder.id}">
										<c:choose>
											<c:when
												test="${countWeightDetailNotes == '0' || countWeightDetailNotes == '' || countWeightDetailNotes == null}">
												<td><img id="countWeightDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:when>
											<c:otherwise>
												<td><img id="countWeightDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:otherwise>
										</c:choose>
									</c:if>
								</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" width="100%">
							<tbody>
								<tr>
									<td align="left" class="vertlinedata"></td>								</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" style="margin-left:-5px">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.volume1' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicFeet" onchange="calcGrossVolume1();" size="6" maxlength="10" required="true"  tabindex="65" /></td>
									
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicFeet" size="6" onchange="calcGrossVolume2();checkPricePointDetails('lbscft');" maxlength="10" required="true"  tabindex="73" /></td>
								
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicFeet" size="6" maxlength="10" onchange="findVolumeWeight(),calcGrossVolume3();" required="true"  tabindex="81" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicFeet" onchange="calcGrossVolume4();" size="6" maxlength="10" required="true"  tabindex="89" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right"name="miscellaneous.rwghCubicFeet" onchange="calcGrossVolume5();" size="6" maxlength="10" required="true"  tabindex="97" /></td>

								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='labels.volume2' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicFeet" onchange="calcNetVolume1();" size="6" maxlength="10" required="true"  tabindex="66" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicFeet" onchange="calcNetVolume2();" size="6" maxlength="10" required="true"  tabindex="74" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicFeet" onchange="calcNetVolume3();" size="6" maxlength="10" required="true"  tabindex="82" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicFeet" onchange="calcNetVolume4();" size="6" maxlength="10" required="true"  tabindex="90" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicFeet" onchange="calcNetVolume5();" size="6" maxlength="10" required="true" 
										tabindex="98" /></td>									
								</tr>
								<tr>
									<td align="left" height="5px"></td>
								</tr>
							</tbody>
						</table>
						</div>
						</td>
						</tr>
						</table>				
						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;">
						<tr>
						<td >
						<div  onClick="javascript:animatedcollapse.toggle('weightnvolumesub2'),toIncrementWeightTabSub2();" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left">
						</td>
						<td class="headtab_center" >&nbsp;(Metric Units)<c:if test="${weightType == 'kgscbm'}">
                         <INPUT type="radio" name="weightType"   value="kgscbm" id="weightnVolumeRadio2" checked="checked"    onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();" tabindex="">
                           </c:if>
						<c:if test="${weightType != 'kgscbm'}">
                           <INPUT type="radio" name="weightType"    value="kgscbm" id="weightnVolumeRadio2"  onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();" tabindex="">
                         </c:if>
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>						
						<div id="weightnvolumesub2">
						<table class="detailTabLabel" border="0" style="margin-left:-5px">
							<tbody>
							<tr><td height="5px"></td></tr>
								<tr>
									<td></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.chargeableWeight' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.rewgh/fnl$' /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message
										key='labels.grossweight' /></td>
									<td align="left" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleGrossWeightKilo" size="6"
										maxlength="10" required="true"
										 onchange="onlyFloat(this);calcNetWeightKgs1();" tabindex="99" /></td>
									
										<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimateGrossWeightKilo" size="6"
											maxlength="10" required="true"											
											onchange="onlyFloat(this);calcNetWeightKgs2();checkPricePointDetails('kgscbm');" tabindex="107" /></td>
										<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.actualGrossWeightKilo" size="6"
											maxlength="10" required="true"										
											onchange="onlyFloat(this);findWeightKilo();calcNetWeightKgs3();" tabindex="115" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableGrossWeightKilo" size="6"
										maxlength="10" required="true"
										onchange="onlyFloat(this);calcNetWeightKgs5();" tabindex="123"  /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghGrossKilo"
										size="6" required="true" maxlength="10"
										onchange="onlyFloat(this);calcNetWeightKgs4();" tabindex="131" /></td>
									</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.tareweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleTareWeightKilo" size="6" maxlength="8"
										required="true" 
										 onchange="onlyFloat(this);calcNetWeightKgs1();" tabindex="100" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.estimateTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this);calcNetWeightKgs2();" tabindex="108" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.actualTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this);findTareWeightKilo();calcNetWeightKgs3();" tabindex="115" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableTareWeightKilo" size="6"
										maxlength="8" required="true"
										
										onchange="onlyFloat(this);calcNetWeightKgs5();" tabindex="123" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghTareKilo"
										size="6" required="true" maxlength="8"
										
										onchange="onlyFloat(this);calcNetWeightKgs4();" tabindex="131" /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width=""><fmt:message
										key='labels.netweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleNetWeightKilo" size="6" maxlength="10"
										required="true"
										onchange="onlyFloat(this);calcNetWeightOnlyKgs1();" tabindex="101" /></td>
									
										<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimatedNetWeightKilo" size="6"
											maxlength="10" required="true"
											 onchange="onlyFloat(this);calcNetWeightOnlyKgs2();" tabindex="109" /></td>
											<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualNetWeightKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs3();findNetWeightKilo();" size="6" maxlength="10" required="true" 
											 tabindex="117" /></td>								
									<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetWeightKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs4();" size="6" maxlength="10" required="true"  tabindex="125" /></td>
									<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs5();" size="6" maxlength="10" required="true"  tabindex="133" /></td>
									</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" width="100%">
							<tbody>
								<tr>
									<td align="left" class="vertlinedata"></td>	</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" style="margin-left:-5px">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.volume1' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr1();" size="6" maxlength="10" required="true"  tabindex="102" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr2();checkPricePointDetails('kgscbm');" size="6" maxlength="10" required="true"  tabindex="110" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicMtr" onchange="onlyFloat(this); findVolumeWeightCbm();calcGrossVolumeMtr3();" size="6" maxlength="10" required="true"  tabindex="118" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr4();" size="6" maxlength="10" required="true"  tabindex="125" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right"name="miscellaneous.rwghCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr5();" size="6" maxlength="10" required="true"  tabindex="133" /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='labels.volume2' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr1();" size="6" maxlength="10" required="true"  tabindex="103" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr2();" size="6" maxlength="10" required="true"  tabindex="111" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr3();" size="6" maxlength="10" required="true"  tabindex="119" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr4();" size="6" maxlength="10" required="true"  tabindex="127" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr5();" size="6" maxlength="10" required="true" 
										tabindex="135" /></td>
									</tr>
								<tr>
									<td align="left" height="5px"></td>
								</tr>
							</tbody>
						</table>						
						</div>
						</td>
						</tr></table>						
					    </div>
						</td>
						</tr>
						</div>
	<!--	End of this Section-->		
<tr>
						<td height="10" width="100%" align="left">
						<div  onClick="javascript:animatedcollapse.toggle('address')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Address Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>					
					   <div id="address" >
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
							<tbody>
							<tr><td>
											<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr class="subcontenttabChild">
								<td  width="215px"><b>&nbsp;Origin</b></td>
								</tr>
								</table>
								</td>
								</tr>
								<tr>
									<td>
									<table class="detailTabLabel" border="0" width="863px" cellpadding="0" cellspacing="0" style="margin-left:-5px">
										<tbody>
											<tr>
									<td align="left" height="6px"></td></tr>
								
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originAddress1' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext-scope"><fmt:message key='serviceOrder.originCompany' /></td>
											<td rowspan="6" valign="bottom" width="120">
											 <sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
											<c:if test="${not empty serviceOrder.id}">
											<fieldset style="width:95px;!width:107px;margin:0px;padding:12px 7px;!padding:7px 3px 4px 3px;">
																<legend>Update Address</legend>
																	<table style="margin:0px; padding: 0px;"><tr>														
   																	<td width="0"></td>
													                <td colspan=""><input type="button" name="changeOriginTicket" value="In All Tickets" style="height:23px;!width:90px;" class="cssbuttonB" onclick="changeOriginTicketAdd();" tabindex="" /></td>
													               </tr>													              
													               </table>
													               </fieldset>
													               </c:if>	
													               <c:if test="${empty serviceOrder.id}">
											<fieldset style="width:95px;!width:107px;margin:0px;padding:12px 7px;!padding:7px 3px 4px 3px;">
																<legend>Update Address</legend>
																	<table style="margin:0px; padding: 0px;"><tr>														
   																	<td width="0"></td>
													                <td colspan=""><input type="button" name="changeOriginTicket" value="In All Tickets" style="height:23px;!width:90px;" class="cssbuttonB" onclick="changeOriginTicketAdd();" disabled="disabled" tabindex=""/></td>
													                </tr>													              
													               </table>
													               </fieldset>
													               </c:if>	
													               </sec-auth:authComponent>
											</td>
											<td align="left" class="listwhitetext" width="133">
												<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >Address Book</sec-auth:authComponent>
											</td>
											</tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												
												<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text upper-case" name="serviceOrder.originAddressLine1" cssStyle="width:188px;" maxlength="100" tabindex="137" onblur="titleCase(this)" onchange="isUpdaterFlag();" onkeydown="disabledOriginTicketBtn();"/>
												<img align="top" class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td></td>
												<td align="left" class="listwhitetext"><s:hidden name="serviceOrder.originCompany"/></td>
												<td>
													<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
														<s:select cssClass="list-menu" cssStyle="width:110px;" list="%{originAddress}" headerKey="" headerValue="" onchange="disabledOriginTicketBtn();getAddress(this,'origin');" name="addOriginAdd" id="addOriginAdd" tabindex=""/>
													</sec-auth:authComponent>
												</td>
											</tr>
											<tr>
									<td align="left" height="2px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext" colspan="2" style="padding-bottom:3px"><s:textfield cssClass="input-text upper-case" name="serviceOrder.originAddressLine2" cssStyle="width:188px;" maxlength="100" tabindex="138" onblur="titleCase(this)" onchange="isUpdaterFlag();" onkeydown="disabledOriginTicketBtn();"/><font color="gray">Address2</font></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td align="left" class="listwhitetext" style="padding-top:5px"><c:if test="${serviceOrder.job !='RLO'}">Port Of Loading</c:if></td>
											</tr>											
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text upper-case" name="serviceOrder.originAddressLine3" cssStyle="width:188px;" maxlength="100" tabindex="139" onblur="titleCase(this)" onchange="isUpdaterFlag();" onkeydown="disabledOriginTicketBtn();"/><font color="gray">Address3</font></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td align="left" class="listwhitetext"><c:if test="${serviceOrder.job !='RLO'}"><s:textfield cssClass="input-text" name="serviceOrder.portOfLading" cssStyle="width:235px;" maxlength="45" tabindex=""  /></c:if></td>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>
									<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originCountry' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
												<td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:8px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originCity' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
												<td align="left" id="zipCodeRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originZip' /><font color="red" size="2">*</font></td>
												</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:25px"></td>
												<td>
												<c:if test="${not empty serviceOrder.id}">
												<configByCorp:customDropDown 	listType="map" list="${ocountry_isactive}" fieldValue="${serviceOrder.originCountry}" attribute="id='ocountry' class='list-menu' name='serviceOrder.originCountry'  style='width:190px'  headerKey='' headerValue='' onkeydown='disabledOriginTicketBtn();' onchange='ostateMandatory(),zipCode(),disabledOriginTicketBtn(),getOriginCountryCode(this),autoPopulate_customerFile_originCountry(this),getState(this),changeStatus(),enableStateListOrigin(),isUpdaterFlag();' tabindex='140' "/>
		    									</c:if>
												<c:if test="${empty serviceOrder.id}">
												<configByCorp:customDropDown 	listType="map" list="${ocountry_isactive}" fieldValue="${serviceOrder.originCountry}" attribute="id='ocountry' class='list-menu' name='serviceOrder.originCountry'  style=width:190px  headerKey='' headerValue='' onkeydown='disabledOriginTicketBtn();' onchange='callInclusionExclusionData(),ostateMandatory(),zipCode(),disabledOriginTicketBtn(),getOriginCountryCode(this),autoPopulate_customerFile_originCountry(this),getState(this),changeStatus(),enableStateListOrigin(),getIncExcDocDetails();' tabindex='140' "/>
												</c:if>
												</td>
												<td width="25px"><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();" /></td>
												<td><configByCorp:customDropDown 	listType="map" list="${ostates_isactive}" fieldValue="${serviceOrder.originState}" attribute="id=originState class=list-menu name=serviceOrder.originState  style=width:234px  headerKey='' headerValue='' onkeydown='disabledOriginTicketBtn();' onchange='disabledOriginTicketBtn();autoPopulate_customerFile_originCityCode(this),changeStatus();isUpdaterFlag();' headerKey='' headerValue='' tabindex='141' "/>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text upper-case" name="serviceOrder.originCity" id="originCity" cssStyle="width:141px" maxlength="30" onkeyup="autoCompleterAjaxCallOriCity();" onkeydown="disabledOriginTicketBtn();" onkeypress="" onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this);" onchange="isUpdaterFlag();" tabindex="142"/></td>
												<td align="left" class="listwhitetext" style="width:10px"><div id="zipCodeList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findCityStateNotPrimary('OZ', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originZip" cssStyle="width:107px" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ');disabledOriginTicketBtn();isUpdaterFlag();" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="143" /></td>
                                                <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
						                        <td class="listwhitetext">
                                                <a id="myUniqueLinkId" onclick="callPostalCode();" >&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></font></a>
                                                </td>
                                                </configByCorp:fieldVisibility>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>								
								<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhone' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originHomePhone' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originMobile' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originFax' /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originDayPhone" cssStyle="width:127px;" maxlength="20"  tabindex="" onkeydown="disabledOriginTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originDayExtn" cssStyle="width:49px;" maxlength="10"	 tabindex="" onkeydown="disabledOriginTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:22px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originHomePhone" cssStyle="width:135px;" maxlength="20"  tabindex="" onkeydown="disabledOriginTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originMobile" cssStyle="width:135px;" maxlength="25"  tabindex="" onkeydown="disabledOriginTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originFax" cssStyle="width:112px;" maxlength="20"  tabindex="" onkeydown="disabledOriginTicketBtn();" /></td>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.email' /></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
													<td width=""></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.email2' /></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td align="right" class="listwhitetext" style="width:1px"></td>
												<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
												</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" cssStyle="width:189px" name="serviceOrder.email" size="22" maxlength="65" tabindex="144" onchange="copyToEmail1(document.forms['serviceOrderForm'].elements['serviceOrder.email'].value);isUpdaterFlag();"/></td>
												<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.email}"/></td>
												<td width="0px"></td>
												<td><s:textfield cssClass="input-text" cssStyle="width:200px;" name="serviceOrder.email2" size="28" maxlength="65" tabindex="145" /></td>
												<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.email2}"/></td>
												<td align="right" class="listwhitetext" style="width:1px"></td>
												<td><s:textfield cssClass="input-text" cssStyle="width:184px" name="serviceOrder.originPreferredContactTime" size="20" maxlength="30" tabindex="146" /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td>
									<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" ></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<c:if test="${serviceOrder.job !='RLO'}"> 
												<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.originLoadSite',this);return false" >
												<fmt:message key='serviceOrder.originLoadSite' /></a></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												</c:if>
											    <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">	
												<td align="left" class="listwhitetext" colspan="3"><c:if test="${serviceOrder.job !='RLO'}"> <a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.originMilitary',this);return false" >
												<fmt:message key='serviceOrder.originMilitary' /></a></c:if></td>
   												</configByCorp:fieldVisibility>												
												<td align="right" class="listwhitetext" style="width:5px"></td>
												</tr>
												<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<c:if test="${serviceOrder.job !='RLO'}"> 
												<td>
												<configByCorp:customDropDown 	listType="map" list="${loadsite_isactive}" fieldValue="${serviceOrder.originLoadSite}" 
										        attribute="id='serviceOrderForm_serviceOrder_originLoadSite' class=list-menu name='serviceOrder.originLoadSite'  
										        style='width:191px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
												<%-- <s:select cssClass="list-menu" name="serviceOrder.originLoadSite" list="%{loadsite}" cssStyle="width:194px" onchange="changeStatus();" tabindex="147" /> --%>
												</td>
												<td align="right" class="listwhitetext" style="width:22px"></td>
												</c:if>
												<td colspan="3">
												 <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">	
												<c:if test="${serviceOrder.job !='RLO'}"> <configByCorp:customDropDown 	listType="map" list="${military_isactive}" fieldValue="${serviceOrder.originMilitary}"	attribute="id=serviceOrderForm_serviceOrder_originMilitary class=list-menu name=serviceOrder.originMilitary style=width:112px  headerKey='' headerValue='' onchange='changeStatus();' tabindex='148' "/>
		    									</c:if>
												</configByCorp:fieldVisibility>													
													</td>
													<td></td>
												</tr>
												<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
												<tr>
												<td align="right" class="listwhitetext" style="width:92px" ></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactName' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext">Contact Last Name</td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactWork' /></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactPhone' /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactName" cssStyle="width:190px" maxlength="35" tabindex="149" onkeydown="disabledOriginTicketBtn();" /></td>
												<td align="right" class="listwhitetext" style="width:22px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactLastName" cssStyle="width:110px" maxlength="35" tabindex="150" onkeydown="disabledOriginTicketBtn();" /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactWork" cssStyle="width:100px" maxlength="20"  tabindex="151" onkeydown="disabledOriginTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactNameExt" cssStyle="width:35px" maxlength="4"	 tabindex="152" /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactEmail" cssStyle="width:123px" maxlength="65" tabindex="153"  /></td>
												<td align="right" class="listwhitetext" style="width:135px"></td>
													<c:if test="${empty serviceOrder.id}">
													<td align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
												</c:if>
												<c:if test="${not empty serviceOrder.id}">
													<c:choose>
														<c:when
															test="${countOriginDetailNotes == '0' || countOriginDetailNotes == '' || countOriginDetailNotes == null}">
															<td align="right"><img id="countOriginDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:when>
														<c:otherwise>
															<td align="right"><img id="countOriginDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</tr>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
										</tbody>
									</table>									
								    <tr>
									<td width="100%">
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px; padding: 0px;">
										<tbody>
											<tr>
												<td align="left" class="vertlinedata"></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>								
								<tr>
									<td valign="top" align="left" class="listwhitetext">
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>
                                            <tr><td>
											<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr class="subcontenttabChild">										
												<td colspan=""><b>&nbsp;Destination</b></td>											
											</tr>
											</table>
											</td>
											</tr>
											<tr>
											<td>
											<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="863px" style="margin-left:-5px">
											<tr><td height="6px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationAddress1' /></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td></td>
												<td align="left" class="listwhitetext-scope"><fmt:message	key='serviceOrder.destinationCompany' /></td>
											    <td rowspan="6" valign="bottom" width="120">
											    <sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
											<c:if test="${not empty serviceOrder.id}">
											<fieldset style="width:95px;!width:107px;margin:0px;padding:12px 7px;!padding:7px 3px 4px 3px;">
																<legend>Update Address</legend>
																	<table style="margin:0px; padding: 0px;"><tr>															
   																	<td width="0"></td>
													                <td colspan=""><input type="button" name="changeDestinTicket" style="height:23px;!width:90px;" value="In All Tickets" class="cssbuttonB" onclick="changeDestinTicketAdd();" tabindex="155" /></td>
													                </tr>													              
													               </table>
													               </fieldset>
													               </c:if>
													               <c:if test="${empty serviceOrder.id}">
											<fieldset style="width:95px;!width:107px;margin:0px;padding:12px 7px;!padding:7px 3px 4px 3px; ">
																<legend>Update Address</legend>
																	<table style="margin:0px; padding: 0px;"><tr>															
   																	<td width="0"></td>
													                <td colspan=""><input type="button" name="changeDestinTicket" style="height:23px;!width:90px;" value="In All Tickets" class="cssbuttonB" onclick="changeDestinTicketAdd();" disabled="disabled" tabindex="156"/></td>
													               </tr>													              
													               </table>
													               </fieldset>
													               </c:if>
													               </sec-auth:authComponent>
											</td>
												<td align="left" class="listwhitetext" width="133">
												<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >Address Book</sec-auth:authComponent></td>
											</tr>											
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine1" cssStyle="width:188px;" maxlength="100" tabindex="157" onchange="isUpdaterFlag();" onblur="titleCase(this)" onkeydown="disabledDestinTicketBtn();"/>
												<img align="top" class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td></td>
												<td align="left" class="listwhitetext"><s:hidden cssClass="input-text" name="serviceOrder.destinationCompany"/></td>
												<td align="left">
													<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
														<s:select cssClass="list-menu" cssStyle="width:110px;"  list="%{destinationAddress}" onchange="getAddress(this,'dest');disabledDestinTicketBtn();" headerKey="" headerValue=""  name="addDestAdd" id="addDestAdd" tabindex=""/>
													</sec-auth:authComponent>
												</td>											
											</tr>
											<tr><td height="2px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext" colspan="1" style="padding-bottom:3px"><s:textfield	cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine2" cssStyle="width:188px;" maxlength="100" tabindex="158" onblur="titleCase(this)" onchange="isUpdaterFlag();" onkeydown="disabledDestinTicketBtn();"/><font color="gray">Address2</font></td>
												<td></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td align="left" class="listwhitetext" style="padding-top: 5px"><c:if test="${serviceOrder.job !='RLO'}">Port&nbsp;Of&nbsp;Entry</c:if></td>
											 </tr>											
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td colspan=""><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine3" cssStyle="width:188px;" maxlength="100" tabindex="159" onblur="titleCase(this)" onchange="isUpdaterFlag();" onkeydown="disabledDestinTicketBtn();"/><font color="gray">Address3</font></td>
												<td></td>
												<td align="left" class="listwhitetext" style=""></td>
												<td align="left" class="listwhitetext"> <c:if test="${serviceOrder.job !='RLO'}"> <s:textfield cssClass="input-text" name="serviceOrder.portOfEntry" size="38" cssStyle="width:235px;" maxlength="45" tabindex="160" /></c:if></td>
											</tr>
											</table>
											</td>
											</tr>										
										</tbody>
									</table>									
									</td>
									</tr>									
									<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationCountry' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
												<td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationCity' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
												</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td>
												<c:if test="${not empty serviceOrder.id}">
												<configByCorp:customDropDown 	listType="map" list="${dcountry_isactive}" fieldValue="${serviceOrder.destinationCountry}"	attribute="id=dcountry class=list-menu name=serviceOrder.destinationCountry style=width:190px  headerKey='' headerValue='' onkeydown='disabledDestinTicketBtn();' onchange='dstateMandatory();disabledDestinTicketBtn(),getDestinationCountryCode(this),autoPopulate_customerFile_destinationCountry(this),changeStatus(),getDestinationState(this),enableStateListDestin(),isUpdaterFlag();' tabindex='161' "/>
		    									</c:if>
												<c:if test="${empty serviceOrder.id}">
												<configByCorp:customDropDown 	listType="map" list="${dcountry_isactive}" fieldValue="${serviceOrder.destinationCountry}"	attribute="id=dcountry class=list-menu name=serviceOrder.destinationCountry style=width:190px  headerKey='' headerValue='' onkeydown='disabledDestinTicketBtn();' onchange='callInclusionExclusionData(),dstateMandatory(),disabledDestinTicketBtn(),getDestinationCountryCode(this),autoPopulate_customerFile_destinationCountry(this),changeStatus(),getDestinationState(this),enableStateListDestin(),getIncExcDocDetails();' tabindex='161' "/>
		    					                </c:if>
												</td>
												<td width="26px"><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();" /></td>
												<td><configByCorp:customDropDown 	listType="map" list="${dstates_isactive}" fieldValue="${serviceOrder.destinationState}"	attribute="id=destinationState class=list-menu name=serviceOrder.destinationState style=width:234px  headerKey='' headerValue='' onkeydown='disabledDestinTicketBtn();' onchange='disabledDestinTicketBtn();autoPopulate_customerFile_destinationCityCode(this),changeStatus();isUpdaterFlag();' tabindex='162' "/></td>
												<td align="left" class="listwhitetext" style="width:8px"></td>
												<td><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationCity" id="destinationCity" cssStyle="width:141px"  maxlength="30" onkeyup="autoCompleterAjaxCallDestCity();" onkeydown="disabledDestinTicketBtn();" onchange="isUpdaterFlag();" onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this);" tabindex="163"/></td>
												<td align="left"><div id="zipDestCodeList" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="findCityStateNotPrimary('DZ', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></div></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationZip" cssStyle="width:107px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" onchange="findCityStateOfZipCode(this,'DZ');disabledDestinTicketBtn();isUpdaterFlag();" tabindex="164" /></td>
							<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
							<td class="listwhitetext">
<a id="myUniqueLinkId" onclick="callPostalCode();">&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></font></a>
</td>  
</configByCorp:fieldVisibility>


											</tr>
										</tbody>
									</table>
								</tr>
								<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-left:-5px">
										<tbody>
										
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhone' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationHomePhone' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationMobile' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationFax' /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationDayPhone" cssStyle="width:127px" size="22" maxlength="20"  tabindex="165" onkeydown="disabledDestinTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left"><s:textfield cssClass="input-text" name="serviceOrder.destinationDayExtn" cssStyle="width:49px"  size="10" maxlength="10"  tabindex="166" onkeydown="disabledDestinTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:22px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationHomePhone" cssStyle="width:135px" size="19" maxlength="20"  tabindex="167" onkeydown="disabledDestinTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationMobile" cssStyle="width:135px" size="17" maxlength="25"  tabindex="168" onkeydown="disabledDestinTicketBtn();"/></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationFax" cssStyle="width:112px" size="18" maxlength="20"  tabindex="169"  onkeydown="disabledDestinTicketBtn();"/></td>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-left:-5px">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.email' /></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td width=""></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationEmail2' /></td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td align="right" class="listwhitetext" style="width:1px"></td>
												<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
												</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail" cssStyle="width:190px" size="22" maxlength="65" tabindex="170" onchange="isUpdaterFlag();" /></td>
												<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.destinationEmail}"/></td>
												<td width="0px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail2" cssStyle="width:200px" maxlength="65" tabindex="171" /></td>
												<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.destinationEmail2}"/></td>
												<td align="right" class="listwhitetext" style="width:1px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destPreferredContactTime" cssStyle="width:184px" size="20" maxlength="30" tabindex="172"  /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>								
								<tr>
								<td>
								<table class="detailTabLabel" cellspacing="0" cellpadding="0"  border="0" style="margin-left:-5px">
									<tbody>
										<tr>
											<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
										</tr>
										<tr>
											<td align="right" class="listwhitetext" style="width:92px"></td>
											  <c:if test="${serviceOrder.job !='RLO'}"> 
											<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.destinationLoadSite',this);return false" >
												<fmt:message key='serviceOrder.destinationLoadSite' /></a></td>
											<td align="left" class="listwhitetext" style="width:5px"></td>
											</c:if>											
											 <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">	
											<c:if test="${serviceOrder.job !='RLO'}"><td align="left" class="listwhitetext"> <a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.destinationMilitary',this);return false" >
												<fmt:message key='serviceOrder.destinationMilitary' /></a></td>
												<td align="left" class="listwhitetext" style="width:10px"></td></c:if>
												</configByCorp:fieldVisibility>	
											<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.consignee',this);return false" >
												<fmt:message key='serviceOrder.consignee' /></a></td>
										</tr>
										<tr>
											<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
											 <c:if test="${serviceOrder.job !='RLO'}"> 
											<td>
											<configByCorp:customDropDown 	listType="map" list="${loadsite_isactive}" fieldValue="${serviceOrder.destinationLoadSite}" 
										        attribute="id='serviceOrderForm_serviceOrder_destinationLoadSite' class=list-menu name='serviceOrder.destinationLoadSite'  
										        style='width:192px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
											<%-- <s:select cssClass="list-menu" name="serviceOrder.destinationLoadSite" list="%{loadsite}" cssStyle="width:194px" onchange="changeStatus();" tabindex="173" /> --%>
											</td>
											<td align="right" class="listwhitetext" style="width:22px"></td>
											</c:if>
											 <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">	
											<c:if test="${serviceOrder.job !='RLO'}"><td><configByCorp:customDropDown 	listType="map" list="${military_isactive}" fieldValue="${serviceOrder.destinationMilitary}"	attribute="id=serviceOrderForm_serviceOrder_destinationMilitary class=list-menu name=serviceOrder.destinationMilitary style=width:111px  headerKey='' headerValue='' onchange='changeStatus();' tabindex='174' "/></td>
										   <td align="right" class="listwhitetext" style="width:10px"></td></c:if>
											</configByCorp:fieldVisibility>
											<td><s:textfield cssClass="input-text" cssStyle="width:288px" name="serviceOrder.consignee" size="22" maxlength="30" tabindex="175" /></td>
											<td align="right" class="listwhitetext" style="width:62px"></td>
										</tr>
									
									</tbody>
								</table>
								</td>
								</tr>										
						<tr>						
						<td>
						<div id="hidDM">
						<div id="map" style="width:0px; height:0px; display:none"></div>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
							<tbody>
	                            <center><div id="map_canvas" style="width:0%; height:0%"></div></center>							
							<tr>
							<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
							</tr>
								<tr>
								<td align="right" class="listwhitetext" style="width:87px"></td>
								<td align="right" class="listwhitetext" style="width:60px"></td>
										<td align="left" class="listwhitetext" style="width:10px"></td>										
										<td align="left" class="listwhitetext" style="width:10px">Distance&nbsp;(Est.)</td>
										<td align="left" class="listwhitetext" style="width:10px"></td>	
										<td align="left" class="listwhitetext" >Unit<div id="hid4" style="margin: 0px;float:right;width:83px;"><font color="red" size="2">*</font></div></td>									
										</tr>
								<tr>
								<td align="right" class="listwhitetext"></td>
										<td  align="left" class="listwhitetext">
										<input type="button" name="Calc Distance" value="Calc Distance" class="cssbuttonB" onclick="calculateDistance();" tabindex="176" /></td>
										<td align="right" class="listwhitetext"></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:91px;" id="serviceOrderForm_serviceOrder_distance"
										name="serviceOrder.distance"  onblur="checkUnitValidation();"
										maxlength="10" required="true" tabindex=""/></td>
										<td align="left" class="listwhitetext" style="width:22px"></td>	
                                       <td align="left" class="listwhitetext"><s:select cssClass="list-menu"  cssStyle="width:114px" id="serviceOrderForm_serviceOrder_distanceInKmMiles" name="serviceOrder.distanceInKmMiles" list="{'KM','Mile'}"  headerKey="" headerValue="" tabindex="" onchange="calculateDistance();"/></td>									
								</tr>								
							</tbody>
						</table>
						</div>						
						</td>						
						</tr>
								<tr>
								<td>
								<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-left:-5px" >
									<tbody>
										<tr>
											<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
										</tr>
										<tr>
											<td align="right" class="listwhitetext" style="width:92px"></td>
											<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.contactName' /></td>
											<td align="left" class="listwhitetext" style="width:10px"></td>
											<td align="left" class="listwhitetext">Contact Last Name</td>
											<td align="left" class="listwhitetext" style="width:10px"></td>
											<td align="left" class="listwhitetext">
											<fmt:message key='serviceOrder.contactPhone' /></td>
											
											<td align="left" class="listwhitetext" style="width:10px"></td>
											<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
											<td align="left" class="listwhitetext">
											<fmt:message key='serviceOrder.contactFax' /></td>
										</tr>										
										<tr>
											<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.contactName" cssStyle="width:190px"  size="22" maxlength="35"  onkeydown="disabledDestinTicketBtn();" tabindex="" /></td>
											<td align="right" class="listwhitetext" style="width:22px"></td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.contactLastName" cssStyle="width:110px"  size="22" maxlength="35"  onkeydown="disabledDestinTicketBtn();" tabindex="" /></td>
											<td align="right" class="listwhitetext" style="width:10px"></td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.contactPhone" cssStyle="width:100px"  size="18" maxlength="20"  tabindex="" onkeydown="disabledDestinTicketBtn();"/></td>
											<td align="right" class="listwhitetext" style="width:5px"></td>
											<td align="left"><s:textfield cssClass="input-text" name="serviceOrder.contactNameExt" cssStyle="width:35px"  size="3" maxlength="4"  tabindex="" /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
											<td><s:textfield cssStyle="width:123px" cssClass="input-text" name="serviceOrder.destinationContactEmail" size="22" maxlength="65" tabindex="" /></td>
											<td align="right" class="listwhitetext" style="width:133px"></td>
											
											<c:if test="${empty serviceOrder.id}">
												<td align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
											</c:if>
											<c:if test="${not empty serviceOrder.id}">
												<c:choose>
													<c:when
														test="${countDestinationDetailNotes == '0' || countDestinationDetailNotes == '' || countDestinationDetailNotes == null}">
														<td align="right"><img id="countDestinationDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);" /><a
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
													</c:when>
													<c:otherwise>
														<td align="right"><img id="countDestinationDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);" /><a
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
													</c:otherwise>
												</c:choose>
											</c:if>
										</tr>
							</tbody>
						</table>
						</div>
						</div>
						</td>
						<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td>
					</tr>					
											<tr>
												<td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td>
											</tr>
										</tbody>
									</table>
									</div>
									</td>
								</tr>
			<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
			<%--  Pricing code start   --%>	
				<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
				<tr>
				<td width="100%" align="left" style="margin:0px">
					<div id="pricingShow" tabindex="136">
					<div onClick="javascript:animatedcollapse.toggle('pricing');pricingClick();loadSoPricing();" style="margin:0px">
						<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
							<tr>
								<td class="headtab_left"></td>
								<td NOWRAP class="headtab_center">&nbsp;Pricing </td>
								<td width="28" valign="top" class="headtab_bg"></td>
								<td class="headtab_bg_center">&nbsp;</td>
								<td class="headtab_right"></td>
							</tr>
						</table>
					</div>  				
			<div id="pricing" class="switchgroup1">
			   <div id="pricingSoAjax">	
			   			 
			  </div>	
		  </div>
	</div>
				</td>
			</tr>
		<%-- 			<tr>
							<td  align="left" class="listwhitetext" >
							<div id="pricingShow">
							<c:if test="${not empty serviceOrder.id}">
							<div  onClick="javascript:animatedcollapse.toggle('pricing')" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left">
							</td>
							<td NOWRAP class="headtab_center">&nbsp;Pricing 
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
							</td>
							<td class="headtab_right">
							</td>
							</tr>
						</table>
							</div>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<div  onClick = "openPricing();" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left">
							</td>
							<td NOWRAP class="headtab_center">&nbsp;Pricing  
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
							</td>
							<td class="headtab_right">
							</td>
							</tr>
						</table>
							</div>
							</c:if> 
							<div  id="pricing" >
							<div id="para1" >
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"><tr><td>
		                     <display:table name="accountLineList"  class="table" requestURI="" id="accountLineList" style="width:1550px;margin-top:2px;margin-left:0px;margin-right:0px;" defaultsort="2"  >
		                     <display:column title="#" style="width:10px;"><c:out value="${accountLineList_rowNum}"/></display:column>   
							 <c:if test="${accountLineList!='[]'}"> 
							 <c:choose>
	                         <c:when test="${((accountLineList.revisionExpense!='0.00' || accountLineList.revisionRevenueAmount!='0.00' || accountLineList.actualRevenue!='0.00' || accountLineList.actualExpense!='0.00' ||accountLineList.distributionAmount!='0.00')||(trackingStatus.soNetworkGroup==true))}">
	                            <display:column sortProperty="accountLineNumber"  sortable="true" titleKey="accountLine.accountLineNumber"  style="width:10px; font-size:12px;" > 
								<input type="text" style="text-align:right" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-textUpper"  readonly="true" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" >  
								<input type="text" style="width:70px" name="category${accountLineList.id}" value="${accountLineList.category }"  maxlength="3" class="input-textUpper"  readonly="true" />
								</display:column>
								<display:column title="Charge &nbsp;Code" style="width:150px" >
								<input type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="20"  maxlength="25" class="input-textUpper" readonly="true"/>
								<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="vatDecr${accountLineList.id}" value="${accountLineList.estVatDescr}"/>								
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>								
								</c:if>	
								<c:if test="${contractType}">
								<input type="hidden"  name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden"  name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden"  name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden"  name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>

								<input type="hidden"  name="revisionPayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>

								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>

								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />
								<input type="hidden"  name="revisionCurrencyNew${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDateNew${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
															
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
								<input type="text" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-textUpper" readonly="true" />								
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<input type="text" name="estimateVendorName${accountLineList.id}"  value="${accountLineList.estimateVendorName }" size="15" class="input-textUpper" readonly="true"  /> 
								</display:column>
								<display:column   title="Basis"  style="width:80px"> 
								<input type="text" name ="basis${accountLineList.id}"  value="${accountLineList.basis}" style="width:50px" class="input-textUpper" readonly="true"> 
								<input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/>
								</display:column>
								<display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-textUpper" readonly="true"/>
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column> 
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-textUpper" readonly="true"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estLocalAmount>0  }"> 
			                           <img  class="openpopup" style="float:right;" width="14" height="14"  onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>								
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-textUpper" readonly="true"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellLocalAmount>0  }"> 
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-textUpper" readonly="true"/>
								</display:column> 
								<display:column title="Revenue" style="width:20px" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Desc"  style="width:50px">
								<input type="text" name ="vatDecr${accountLineList.id}"  value="${accountLineList.estVatDescr}" style="width:75px" class="input-textUpper" readonly="true"> 
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								</display:column>
								<display:column   title="VAT Amt"  style="width:50px">
								<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px" class="input-textUpper" readonly="true"> 
								</display:column>
								</c:if>																   
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"  style="overflow:auto;" class="input-textUpper" readonly="true"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<display:column style="width:85px;padding:0px;text-align:center;" 
								title="Display&nbsp;On&nbsp;Quote <input type='checkbox' style='margin-left:42px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>All" media="html" > 
                                  <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                  <input type="checkbox"  name="Quote"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                <input type="checkbox"  name="Quote"   disabled="disabled" /> 
                                 </c:if>
                                 </display:column>
                                 <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"   style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.additionalService}"  /> 
                                  <input type="checkbox"  name="additionalService"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.additionalService}"  /> 
                                 <input type="checkbox"  name="additionalService"    disabled="disabled" /> 
                                 </c:if>
                                 </display:column>
								</configByCorp:fieldVisibility>
	                            <display:column style="width:75px;padding:0px;text-align:center;" title="<font style='padding-left:25px;'>Act </font> <br><input type='checkbox' style='margin-left:29px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>All"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="hidden"  name="statusCheck${accountLineList.id}" value="${accountLineList.status}"/>
				                 <input type="checkbox"  name="statusCheckHidden"  checked="checked" disabled="disabled" /> 
				                 </c:if>
				               </display:column>	                                 
	                         </c:when>
	                         <c:otherwise> 
								<display:column sortProperty="accountLineNumber"  sortable="true" titleKey="accountLine.accountLineNumber"  style="width:10px; font-size:12px;" > 
								<input type="text" style="text-align:right" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text" onkeydown="return onlyNumberAllowed(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" > 
								<select name ="category${accountLineList.id}" style="width:70px" onchange="" class="list-menu"> 
								<c:forEach var="chrms" items="${category}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.category}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								 </display:column>
								<display:column title="Charge &nbsp;Code" style="width:150px" >
								<input type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="20" maxlength="25" class="input-text" onblur="fillDiscription('quoteDescription${accountLineList.id}','chargeCode${accountLineList.id}','category${accountLineList.id}','recGl${accountLineList.id}','payGl${accountLineList.id}','contractCurrency${accountLineList.id}','contractExchangeRate${accountLineList.id}','contractValueDate${accountLineList.id}','payableContractCurrency${accountLineList.id}','payableContractExchangeRate${accountLineList.id}','payableContractValueDate${accountLineList.id}','accChargeCodeTemp${accountLineList.id}')" onchange="checkChargeCode('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}')" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')"/>								
								<img class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
								<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="vatDecr${accountLineList.id}" value="${accountLineList.estVatDescr}"/>								
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>								
								</c:if>		
								<c:if test="${contractType}">
								<input type="hidden"  name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden"  name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden"  name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden"  name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>

								<input type="hidden"  name="revisionPayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>

								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>

								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />


								<input type="hidden"  name="revisionCurrencyNew${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDateNew${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
														
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
								<input type="text" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-text" onchange="checkVendorName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');fillCurrencyByChargeCode('${accountLineList.id}');"/>
								<img  class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<input type="text" name="estimateVendorName${accountLineList.id}"  value="${accountLineList.estimateVendorName }" size="15" class="input-text" /> 
								</display:column>
								<display:column   title="Basis"  style="width:80px;!width:95px;"> 
								<select name ="basis${accountLineList.id}" style="width:50px" class="list-menu" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')"> 
								<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.value == accountLineList.basis}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								<input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/>			
								<input type="hidden" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
								<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
								<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" /> 		               		               
							    </display:column>
								<display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estLocalAmount>0  }">			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 								
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellLocalAmount>0  }">			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text" onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"  onkeydown="return onlyNumsAllowedPersent(event)" />
								</display:column> 
								<display:column title="Revenue" style="width:20px" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Desc"  style="width:50px"> 
								<select name ="vatDecr${accountLineList.id}" style="width:75px" class="list-menu" onchange="changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}');">																 								
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.estVatDescr}">
	                                  <c:set var="selectedInd" value="selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								</display:column>
								<display:column   title="VAT Amt"  style="width:50px">
								<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px" class="input-textUpper" readonly="true"> 
								</display:column>
								</c:if>
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"  style="overflow:auto;" class="input-text" onselect="discriptionView('quoteDescription${accountLineList.id}')" onblur="discriptionView1('quoteDescription${accountLineList.id}')" value ="${accountLineList.quoteDescription }"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<display:column style="width:85px;padding:0px;text-align:center;" 
								title="Display&nbsp;On&nbsp;Quote <input type='checkbox' style='margin-left:42px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>All" media="html" > 
                                 <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="checkbox"  style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
                                   <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"    style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="checkbox"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="checkbox"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
								</configByCorp:fieldVisibility>
	                            <display:column style="width:75px;padding:0px;text-align:center;"  title="<font style='padding-left:25px;'>Act </font><br><input type='checkbox' style='margin-left:29px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>All"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
				                 </c:if>
				               </display:column>				              				               
	                         </c:otherwise>
                             </c:choose>
                             </c:if>
		                     <c:if test="${accountLineList=='[]'}">
		                     <display:column sortProperty="accountLineNumber"  titleKey="accountLine.accountLineNumber"  style="width:10px; font-size:12px;" > 
								<input type="text" style="text-align:right" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text" onkeydown="return onlyNumberAllowed(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" > 
								<select name ="category${accountLineList.id}" style="width:70px" onchange="" class="list-menu"> 
								<c:forEach var="chrms" items="${category}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.category}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								 </display:column>
								<display:column title="Charge &nbsp;Code" style="width:150px" >
								<input type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="20" maxlength="25" class="input-text" onblur="fillDiscription('quoteDescription${accountLineList.id}','chargeCode${accountLineList.id}','category${accountLineList.id}','recGl${accountLineList.id}','payGl${accountLineList.id}','contractCurrency${accountLineList.id}','contractExchangeRate${accountLineList.id}','contractValueDate${accountLineList.id}','payableContractCurrency${accountLineList.id}','payableContractExchangeRate${accountLineList.id}','payableContractValueDate${accountLineList.id}','accChargeCodeTemp${accountLineList.id}')" onchange="checkChargeCode('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}')" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')"/>
								<img class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
								<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="vatDecr${accountLineList.id}" value="${accountLineList.estVatDescr}"/>								
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>								
								</c:if>	
								<c:if test="${contractType}">
								<input type="hidden"  name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden"  name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden"  name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden"  name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>

								<input type="hidden"  name="revisionPayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>

								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>

								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />


								<input type="hidden"  name="revisionCurrencyNew${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDateNew${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
															
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
								<input type="text" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-text" onchange="checkVendorName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');fillCurrencyByChargeCode('${accountLineList.id}');"/>								
								<img  class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<input type="text" name="estimateVendorName${accountLineList.id}"  value="${accountLineList.estimateVendorName }" size="15" class="input-text" /> 
								</display:column>
								<display:column   title="Basis"  style="width:150px"> 
								<select name ="basis${accountLineList.id}" style="width:50px" class="list-menu" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')">
								<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.value == accountLineList.basis}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								<input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/>			
								<input type="hidden" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
								<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
								<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" /> 		               		               
								</display:column>
                                <display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estLocalAmount>0  }">			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>								
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellLocalAmount>0  }">			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>								
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text" onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')" onkeydown="return onlyNumsAllowedPersent(event)" />
								</display:column> 
								<display:column title="Revenue" style="width:20px" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Desc"  style="width:50px"> 
								<select name ="vatDecr${accountLineList.id}" style="width:75px" class="list-menu" onchange="changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}');">															 								 
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.estVatDescr}">
	                                  <c:set var="selectedInd" value="selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach>                                
								</select>
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								</display:column>
								<display:column   title="VAT Amt"  style="width:50px">
								<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px" class="input-textUpper" readonly="true"> 
								</display:column>
								</c:if>								 
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"   style="overflow:auto;" class="input-text" onselect="discriptionView('quoteDescription${accountLineList.id}')" onblur="discriptionView1('quoteDescription${accountLineList.id}')" value ="${accountLineList.quoteDescription }"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<display:column style="width:85px;padding:0px;text-align:center;" 
								title="Display&nbsp;On&nbsp;Quote <input type='checkbox' style='margin-left:42px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>All" media="html" > 
                                  <c:if test="${accountLineList.displayOnQuote}"> 
                                 <input type="checkbox"   name="displayOnQuote${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" id="displayOnQuote${accountLineList.id}"/> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="checkbox"   name="displayOnQuote${accountLineList.id}" value="${accountLineList.status}" id="displayOnQuote${accountLineList.id}"/> 
                                 </c:if>
                                 </display:column>
                                 <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"    style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="checkbox"  name="additionalService${accountLineList.id}"  checked="checked" id="additionalService${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="checkbox"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
								</configByCorp:fieldVisibility>
	                            <display:column style="width:75px;padding:0px;text-align:center;"  title="<font style='padding-left:25px;'>Act </font> <br><input type='checkbox' style='margin-left:29px; vertical-align:middle;' id='selectallActive' checked='checked' name='selectallActive' onclick='selectAllActive(this);'/>All"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="checkbox"  name="statusCheck${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
				                 </c:if>
				               </display:column>
		                     </c:if> 
                               <display:footer>
                                 <tr> 
				 	  	          <td align="right" colspan="11"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
			  	                    <c:if test="${multiCurrency=='Y'}">
			  	                    <td align="left" ></td>
			  	                    </c:if>				 	  	          
						          <td align="left" width="40px">
						          <input type="text" style="text-align:right" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper"/>
						          </td>
						          <td align="left"> 
						  	      </td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="estimatedTotalRevenue${serviceOrder.id}" value="${serviceOrder.estimatedTotalRevenue}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      </td> 
		  	                    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>
		  	                    <td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <td colspan="2" align="right"><input type="button" class="cssbuttonA" style="width:60px; height:25px; margin-top:-4px;"  name="Inactivate"  value="Inactivate" onclick="updateAccInactive();"/></td>
		  	                    
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin</div></b></td>
		  	                     		  	                    <c:if test="${multiCurrency=='Y'}">
		  	                    <td align="left" ></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						          </td>
						          </td>
						  	      <td align="left"> 
						          <td align="left" width="70px" >
						  	      <input type="text" style="text-align:right" name="estimatedGrossMargin${serviceOrder.id}" value="${serviceOrder.estimatedGrossMargin}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      </td>
						  	       <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if> 
		  	                    <td colspan="3"></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
		  	                     		  	                    <c:if test="${multiCurrency=='Y'}">
		  	                    <td align="left" ></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						          </td>
						          </td>
						  	      <td align="left"> 
						          <td align="left" width="40px">
						  	      <input type="text" style="text-align:right" name="estimatedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.estimatedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      
						  	      </td> 
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>
		  	                    <td colspan="3"></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    </tr>		  	                 
                             </display:footer> 
                          </display:table> 
						</td></tr></table></div>
						<table>
						<tr>
						<td>
						<c:if test="${not empty serviceOrder.id}">
						
							<c:if test="${trackingStatus.soNetworkGroup==null || trackingStatus.soNetworkGroup==false}">
								<input type="button" class="cssbuttonA" style="width:65px; height:25px" name="pricingAdd" value="Add Line"  onclick="checkBillingComplete();" /> 
								<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="savePricingLine();"/>
							</c:if>
							<c:if test="${trackingStatus.soNetworkGroup==true}">
								<input type="button" class="cssbuttonA" style="width:65px; height:25px" name="pricingAdd" value="Add Line"  disabled="true" /> 
								<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" disabled="true"/>
							</c:if>
						</c:if>
						<c:if test="${empty serviceOrder.id}">
						<input type="button" class="cssbuttonA" style="width:65px; height:25px" name="pricingAdd" value="Add Line" disabled="true"/> 
						<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" disabled="true"/>
						</c:if>
						<c:if test="${serviceOrder.defaultAccountLineStatus=='false'}">
						<c:if test="${not empty serviceOrder.id}">
		 				 <c:if test="${trackingStatus.soNetworkGroup==null || trackingStatus.soNetworkGroup==false}">
                           <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="findDefaultLine();" 	value="Add Default Template" />
                         </c:if>
                         <c:if test="${trackingStatus.soNetworkGroup==true}">
                           <input type="button" class="cssbuttonA" style="width:140px; height:25px" disabled="true"	value="Add Default Template" />
                         </c:if>
                         </c:if>
                         <c:if test="${serviceOrder.job == 'OFF'}">
                         	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
                         	<c:if test="${trackingStatus.soNetworkGroup==null || trackingStatus.soNetworkGroup==false}">
                        	 	<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px;  margin:5px 0px;" tabindex="83"/>
							</c:if>
							<c:if test="${trackingStatus.soNetworkGroup==true}">
								<input type="button" class="cssbutton1" disabled="true" value="Create Pricing" style="width:120px; height:25px;  margin:5px 0px;" tabindex="83"/>
							</c:if>                        	 	
                         	</configByCorp:fieldVisibility>
                         </c:if>
						<c:if test="${empty serviceOrder.id}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" disabled="true" 	value="Add Default Template" />
                         </c:if>
	                   </c:if>
	                   <c:if test="${serviceOrder.defaultAccountLineStatus=='true'}">
                        <c:if test="${not empty serviceOrder.id}">
                        <c:if test="${trackingStatus.soNetworkGroup==null || trackingStatus.soNetworkGroup==false}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="defaultLineMassage()" value="Add Default Template" />
                         </c:if>
                         <c:if test="${trackingStatus.soNetworkGroup==true}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" disabled="true" value="Add Default Template" />
                         </c:if>
                         </c:if>
                        <c:if test="${empty serviceOrder.id}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" disabled="true" value="Add Default Template" />
                         </c:if>
	                   </c:if>
	                   
						</td>
						</tr></table> 
		                    </div></div></td></tr> --%> 						 
<%--  Pricing code end   --%> 

<tr >
	<td width="100%" align="left" style="margin:0px">
	<div id="estimatePricingShow">
	<div onClick="javascript:animatedcollapse.toggle('estpricing')" style="margin:0px">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">

<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Estimate&nbsp;Pricing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>

</table>
     </div>
		<div id="estpricing" class="switchgroup1">
		
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  <tr>
 		  <td colspan="5">
 		  <div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;text-align:center;font-size: 1em;font-weight: bold;">
<b><div align="right">Total Estimate Amount:&nbsp;$${miscellaneous.estimatedRevenue}</div></b>
</td>
</tr>
</table>
</div>
</td>
</tr>
<tr><td align="left" colspan="10">
<div>
<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="530">
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">TRANSPORTATION</td>
</tr>
<tr>
<td>
<s:set name="underLyingHaulList" value="underLyingHaulList" scope="request"/>
<c:if test="${underLyingHaulList!='[]'}"> 
	<display:table name="underLyingHaulList" class="table" requestURI="" id="underLyingHaulList"  defaultsort="1" defaultorder="descending" pagesize="10">
    <display:column property="description"  title="Description" />
   	<display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.rate}" /></div></display:column>
   	<display:column  title="Discount">
  	 <c:if test="${underLyingHaulList.discount!=''}">
   	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.discount}" />%</div>
             </c:if>
             </display:column>
   	<display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.charge}" /></div></display:column>
   	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${transportSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${transportSum}" /></div></c:if></td>             
      </tr>
	</display:footer>
	</display:table>
</c:if>
<c:if test="${underLyingHaulList=='[]'}"> 
   <display:table name="underLyingHaulList" class="table" requestURI="" id="underLyingHaulList"  defaultsort="1" defaultorder="descending" pagesize="10">
   <display:column  title="Description" />
   <display:column title="Rate"/>
   <display:column  title="Discount"/>
   <display:column title="Charge" />
   <display:footer>
	 <tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${transportSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${transportSum}" /></div></c:if></td>             
     </tr>
   </display:footer>
	</display:table>
</c:if>
	</td>
	</tr>
	</table>
	</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
<thead>
<tr>
<th colspan="3" class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">PACKING</th>
</tr>
</thead>
<tr>
<td>
<div style="float:left;">
<div style="float:left;">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">

<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;text-align:right;padding-right:8%;">
<tr>
<td style="padding-left:32%;">Container</td>
<td style="padding-left:28%;">Packing</td>
<td style="padding-left:15%;">Unpacking</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<s:set name="packingList" value="packingList" scope="request"/>
<c:if test="${packingList!='[]'}"> 
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
	<display:column property="mainDescr"  title="Description" />
	<display:column title="Qty" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctRate}" /></div></display:column>
             <c:if test="${packingList.ctDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.ctDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">   
	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkRate}" /></div></display:column>
   <c:if test="${packingList.pkDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.pkDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">
      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upQty}" /></div></display:column>
   <display:column  title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upRate}" /></div></display:column>
             <c:if test="${packingList.upDisc==''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" /></div></display:column>
        </c:if>
        <c:if test="${packingList.upDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" />%</div></display:column>
        </c:if>     
   <display:column title="Charge"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upCharge}" /></div></display:column>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>	
	</display:table>
</c:if>
<c:if test="${packingList=='[]'}"> 
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
	<display:column title="Description" />
	<display:column title="Qty" />
   	<display:column title="Rate" />
   	<display:column title="Discount" />
   	<display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;" />   
	<display:column title="Qty" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;"/>
	<display:column title="Qty"/>
   <display:column  title="Rate"/>
   <display:column title="Discount"/>   
   <display:column title="Charge"/>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>	
	</display:table>
</c:if>
	</td>
	</tr>
	</table>
	</div>	
	</div>
	</td>	
	</tr>
	</table>	
	<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="530" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">OTHER</td>
</tr>
<tr>
<td>
<s:set name="otherMiscellaneousList" value="otherMiscellaneousList" scope="request"/>
<c:if test="${otherMiscellaneousList!='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.qty}" /></div></display:column>
    <display:column property="rateType"  title="Rate Type" />
   <display:column property="description"  title="Description" />
   <display:column title="Rate" >
   <c:if test="${otherMiscellaneousList.qty!=''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.rate}" /></div></c:if>
             <c:if test="${otherMiscellaneousList.qty==''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></c:if>
             </display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.discount}" />%</div></display:column>
   <display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></display:column>   
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>
	</display:table>
</c:if>
<c:if test="${otherMiscellaneousList=='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  defaultsort="1" defaultorder="descending" pagesize="10">
    <display:column title="Qty"/>
    <display:column title="Rate Type" />
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" style="width:10%;" />
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>
    
	</display:table>
</c:if>
	</td>
	</tr>
	</table>	
	</div>	
		<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"  width="530" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">VALUATION</td>
</tr>
<tr>
<td>
<s:set name="valuationList" value="valuationList" scope="request"/>
<c:if test="${valuationList!='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.qty}" /></div></display:column>
   <display:column property="description"  title="Description" />
   <display:column title="Rate" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.rate}" /></div></display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.discount}" />%</div></display:column>
   <display:column title="Charge" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.charge}" /></div></display:column>   
	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
<c:if test="${valuationList=='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
    <display:column title="Qty"/>
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" />	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
	</td>
	</tr>      
    
	</table>	
	</div>	
</div>
</td></tr></table></div></div></td></tr> 
			</sec-auth:authComponent></sec-auth:authComponent>
			<!-- &nbsp;Inclusions and Exclusions  START-->
		<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices!='N/A'}">
				<tr>
				<td width="100%" align="left" style="margin:0px">
					<div onClick="javascript:divtoggle('incexc');" style="margin:0px">
						<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
							<tr>
								<td class="headtab_left"></td>
								<td NOWRAP class="headtab_center">&nbsp;Inclusions and Exclusions</td>
								<td width="28" valign="top" class="headtab_bg"></td>
								<td class="headtab_bg_center">&nbsp;</td>
								<td class="headtab_right"></td>
							</tr>
							
							
						</table>
					</div> 
					<table cellpadding="0" cellspacing="0"  border="0" width="" style="margin: 0px" id="langS">
					<tr>
					<td class="listwhitetext" align="left" colspan="0">Language
							<c:if test="${not empty serviceOrder.id}">
						    	<configByCorp:customDropDown 	listType="map" list="${languageList_isactive}" fieldValue="${serviceOrder.incExcServiceTypeLanguage}"      attribute="id='serviceOrderForm_serviceOrder_incExcServiceTypeLanguage' class=list-menu name='serviceOrder.incExcServiceTypeLanguage'  style='width:75px;height:19px;' onchange='' tabindex='' "/>
						    </c:if>
						    <c:if test="${empty serviceOrder.id}">
						   		<configByCorp:customDropDown 	listType="map" list="${languageList_isactive}" fieldValue="${serviceOrder.incExcServiceTypeLanguage}"      attribute="id='serviceOrderForm_serviceOrder_incExcServiceTypeLanguage' class=list-menu name='serviceOrder.incExcServiceTypeLanguage'  style='width:75px;height:19px;' onchange='callInclusionExclusionData();' tabindex='' "/>
						    </c:if>
				    </td>
					<td><input type="button" id="resetServices" name="resetServices" onclick="getConfirmationMessage();" class="cssbuttonA" style="width: 100px; height: 20px; margin: 2px;" value="Reset Services" tabindex="" /></td>
							</tr>
					</table>
					<div style="text-align: center; display: none; left: 25%; margin-top: 5%;" id="loader"><img src="${pageContext.request.contextPath}/images/ajax-loader.gif" /></div>
					<div id="incexc">
 
					</div> 
				</td>
			</tr>
			</c:if>	
			<!-- &nbsp;Inclusions and Exclusions  END-->
	<configByCorp:fieldVisibility componentId="component.field.refQuote.document">
		<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices!='N/A'}">		
		<tr>
		<td height="10" width="100%" align="left" style="margin:0px">
		<div onClick="javascript:divtoggleDoc('inclusionExclusionDocument');" style="margin:0px">
		     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
		<tr>
		<td class="headtab_left"></td>
		<td NOWRAP class="headtab_center">&nbsp;Documents&nbsp;Required</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;</td>
		<td class="headtab_right"></td>
		</tr>
		</table>
		</div>
		     <table cellpadding="3" cellspacing="0"  border="0" width="" style="margin: 0px" id="docs">
					<tr>
					<td class="listwhitetext" align="left" colspan="0">Language
							<c:if test="${not empty serviceOrder.id}">
						    <configByCorp:customDropDown 	listType="map" list="${languageList_isactive}" fieldValue="${serviceOrder.incExcDocTypeLanguage}"      attribute="id='serviceOrderForm_serviceOrder_incExcDocTypeLanguage' class=list-menu name='incExcDocTypeLanguage'  style='width:75px;height:19px;' headerKey='' headerValue='' onchange='' tabindex='' "/>
						   
						    </c:if>
						    <c:if test="${empty serviceOrder.id}">
						    <configByCorp:customDropDown 	listType="map" list="${languageList_isactive}" fieldValue="${serviceOrder.incExcDocTypeLanguage}"      attribute="id='serviceOrderForm_serviceOrder_incExcDocTypeLanguage' class=list-menu name='incExcDocTypeLanguage'  style='width:75px;height:19px;' headerKey='' headerValue='' onchange='getIncExcDocDetails();' tabindex='' "/>
						    
						    </c:if>
				    </td>
					<td><input type="button" id="resetServices" name="resetServices" onclick="getConfirmationMessage1();" class="cssbuttonA" style="width: 120px; height: 20px; margin: 2px;" value="Reset Documents" tabindex="" /></td>
							</tr>
					</table>
			<div id="inclusionExclusionDocListAjax">		
			
			</div>
		</td></tr>		
		</c:if>
	</configByCorp:fieldVisibility>		
	<c:if test="${usertype=='ACCOUNT' || usertype=='USER' || usertype=='AGENT' || usertype=='PARTNER'}">
<configByCorp:fieldVisibility componentId="component.serviceOrder.section.customerFeedback.view">	
<tr>
<td height="10" align="left" class="listwhitetext" colspan="2" width="100%" >
<div  onClick="javascript:animatedcollapse.toggle('customer')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Customer Feedback
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
 <div id="customer" >
  <c:if test="${customerFeedbackFlag=='Just Say Yes'}">	
  <div  style="margin: 0px"  >          
  <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>				
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;height:13px;cursor:pointer;">Just Say Yes</td></tr>
<tr>
<td align="left" class="vertlinedata">
</td>
</tr>

</table >
</div>
 
									<c:if test="${usertype=='USER'}">
									<sec-auth:authComponent componentId="module.serviceOrder.section.customerFeedback.edit">
									<%
	String sectionCustomerFeedback="true";
	int permission  = (Integer)request.getAttribute("module.serviceOrder.section.customerFeedback.edit" + "Permission");
 	if (permission > 2 ){
 		sectionCustomerFeedback = "false";
 		System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionCustomerFeedback);
 	}
 	System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionCustomerFeedback);
  %>
  <%-- <c:set var="sectionCustomerFeedback"  value='<%= sectionCustomerFeedback %>'/> --%>
  								<div>
									<table class="detailTabLabel" cellspacing="0" cellpadding="1" border="0" >
										<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />								
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:100px; height:5px;"></td>
											</tr>
											<tr>
												<tr>
												<td  id="COA" align="right" valign="middle" class="listwhitetext"
													style="width:180px"> Client Overall Response:&nbsp;</td>
												<td><s:select cssClass="list-menu" name="serviceOrder.clientOverallResponse" list="%{customerFeedBack}" headerKey="" headerValue="" onchange="changeStatus();return validate('this');" disabled="<%= sectionCustomerFeedback%>" tabindex="" /></td>
															
												<td align="right" style="width:150px" class="listwhitetext"><fmt:message key='serviceOrder.emailSent'/></td>
												<c:if test="${not empty serviceOrder.emailSent}">
												<s:text id="serviceOrderEmailSentFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.emailSent"/></s:text>
												<td align="left"><s:textfield cssClass="input-text" id="emailSent" name="serviceOrder.emailSent" value="%{serviceOrderEmailSentFormattedValue}" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" />
												<img id="emailSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty serviceOrder.emailSent}">
												<td align="left"><s:textfield cssClass="input-text" id="emailSent" name="serviceOrder.emailSent" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" />
												<img id="emailSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>	
												
												<td align="right" style="width:150px" class="listwhitetext">Email Recvd:&nbsp;</td>
												<c:if test="${not empty serviceOrder.emailRecvd}">
												<s:text id="serviceOrderEmailRecvdFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.emailRecvd"/></s:text>
												<td align="left"><s:textfield cssClass="input-text" id="emailRecvd" name="serviceOrder.emailRecvd" value="%{serviceOrderEmailRecvdFormattedValue}" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" />
												<img id="emailRecvd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty serviceOrder.emailRecvd}">
												<td align="left"><s:textfield cssClass="input-text" id="emailRecvd" name="serviceOrder.emailRecvd" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)"  />
												<img id="emailRecvd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
											    <configByCorp:fieldVisibility componentId="component.tab.dspDetails.HomeFinding">
	                                            <c:if test="${serviceOrder.job == 'RLO'}">
												<td align="right" style="width:150px" class="listwhitetext">Rpt of hours Received:&nbsp;</td>
													<c:if test="${not empty serviceOrder.reptHourReceived}">
												 <s:text id="reptHourReceivedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.reptHourReceived"/></s:text>
												<td ><s:textfield cssClass="input-text" id="reptHourReceived" name="serviceOrder.reptHourReceived" required="true" size="7" value="%{reptHourReceivedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
												<img id="reptHourReceived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											   </td>
											   </c:if>
											   		<c:if test="${empty serviceOrder.reptHourReceived}">
											   	<td ><s:textfield cssClass="input-text" id="reptHourReceived" name="serviceOrder.reptHourReceived" required="true" size="7" cssStyle="width:65px;"  onkeydown="return onlyDel(event,this)"/>
						                       <img id="reptHourReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/> </td>
											   		</c:if>
											   		</c:if>
											   		</configByCorp:fieldVisibility>
								</tr>
								</tr>
								<tr><td style="height:10px;"></td></tr>
								<tr>
								<td align="right" id="OAE" valign="middle" class="listwhitetext" style="width:180px"> Origin Agent Evaluation:&nbsp;</td>
								<%--<td>
								 <configByCorp:customDropDown 	listType="map" list="${oaEvaluation_isactive}" fieldValue="${serviceOrder.OAEvaluation}" disabled='${sectionCustomerFeedback}'  attribute="id=serviceOrderForm_serviceOrder_mode class=list-menu name=serviceOrder.OAEvaluation  style=width:145px  headerKey='' headerValue='' onchange='changeStatus()'  tabindex='' "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.OAEvaluation" list="%{oaEvaluation}" headerKey="" headerValue="" tabindex="" onchange="changeStatus()" disabled="<%= sectionCustomerFeedback%>"/></td>
								
								<td id="Comments" align="right" valign="middle" class="listwhitetext" >Comments:&nbsp;</td>
								<td rowspan="9" colspan="5" style="vertical-align:top;"><s:textarea  rows="5" cols="55" id="serviceOrder.feedBack" name="serviceOrder.feedBack"  tabindex=""  cssClass="textarea" disabled="<%= sectionCustomerFeedback%>"/>	
								</td>
								</tr>	
								<tr><td style="height:10px;"></td></tr>							
								<tr>
								<td align="right" id="DAE" valign="middle" class="listwhitetext"
													style="width:180px"> Destination Agent Evaluation:&nbsp;</td>
								<%-- <td><configByCorp:customDropDown 	listType="map" list="${daEvaluation_isactive}" fieldValue="${serviceOrder.DAEvaluation}"   attribute="id=serviceOrderForm_serviceOrder_mode class=list-menu name=serviceOrder.DAEvaluation   headerKey='' headerValue='' onchange='changeStatus()' disabled='<%= sectionCustomerFeedback%>' tabindex='' "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.DAEvaluation" list="%{daEvaluation}" headerKey="" tabindex=""  headerValue="" onchange="changeStatus()" disabled="<%= sectionCustomerFeedback%>"/></td>
								</tr>
								<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.surveyCoordEvaluation"> 
								<tr><td style="height:10px;"></td></tr>
								<tr>
								
								<td align="right" id="DAE" valign="middle" class="listwhitetext"
													style="width:180px">Surveyor Evaluation:&nbsp;</td>
								<%-- <td><configByCorp:customDropDown 	listType="map" list="${daEvaluation_isactive}" fieldValue="${serviceOrder.surveyorEvaluation}"  attribute="id=serviceOrderForm_serviceOrder_mode class=list-menu name=serviceOrder.surveyorEvaluation   headerKey='' headerValue='' onchange='changeStatus()' disabled='<%= sectionCustomerFeedback%>' tabindex='' "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.surveyorEvaluation" list="%{daEvaluation}" headerKey="" tabindex=""  headerValue="" onchange="changeStatus()" disabled="<%= sectionCustomerFeedback%>"/></td>

								</tr>
								<tr><td style="height:10px;"></td></tr>
								<tr>
								<td align="right" id="DAE" valign="middle" class="listwhitetext"
													style="width:180px">Coordinator Evaluation:&nbsp;</td>
								<%-- <td><configByCorp:customDropDown 	listType="map" list="${daEvaluation_isactive}" fieldValue="${serviceOrder.coordinatorEvaluation}"  attribute="id=serviceOrderForm_serviceOrder_mode class=list-menu name=serviceOrder.coordinatorEvaluation   headerKey='' headerValue='' onchange='changeStatus()' disabled='<%= sectionCustomerFeedback%>' tabindex='' "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.coordinatorEvaluation" list="%{daEvaluation}" headerKey="" tabindex=""  headerValue="" onchange="changeStatus()" disabled="<%= sectionCustomerFeedback%>"/></td>
								
								</tr>
								</configByCorp:fieldVisibility>
							</tbody>
						</table>
						</div>
						 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.customerServiceSurvey"> 
						 <div  onClick="javascript:animatedcollapse.toggle('customerServiceSurvey')" style="margin: 0px"  >
 <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>				
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;height:13px;cursor:pointer;">Customer Service Survey</td></tr>
<tr>
<td align="left" class="vertlinedata">
</td>
</tr>
</table>
</div>
						<div id="customerServiceSurvey"	>
<iframe name="iFrame" src ="customerServiceSurvey.html?decorator=asml&popup=true&submitCheck=false&id=${serviceOrder.id}" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
</div>
<s:hidden name="customerServiceSurveyQuesFlag" />
 </configByCorp:fieldVisibility> 
						</sec-auth:authComponent>
						</c:if>	
						<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT' || usertype=='PARTNER'}">
						<!--  <div  onClick="javascript:animatedcollapse.toggle('justsay1')" style="margin: 0px"  >          
						  <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
						<tr>
						<td align="left" class="vertlinedata"></td>
						</tr>				
						<tr><td align="left" class="subcontenttabChild" style="font-weight:800;height:13px;cursor:pointer;">Just Say Yes</td></tr>
						<tr>
						<td align="left" class="vertlinedata">
						</td>
						</tr>
						
						</table >
						</div> -->
									<div >
									<table class="detailTabLabel" cellspacing="0" cellpadding="1" border="0" >
										<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
								
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:100px; height:5px;"></td>
											</tr>

											<tr>
												<tr>
												<td  id="COA" align="right" valign="middle" class="listwhitetext"
													style="width:180px"> Client Overall Response:&nbsp;</td>
												<td><s:select cssClass="list-menu" name="serviceOrder.clientOverallResponse" list="%{customerFeedBack}" headerKey="" headerValue=""  disabled="true" tabindex="" /></td>
															
												<td align="right" style="width:150px" class="listwhitetext"><fmt:message key='serviceOrder.emailSent'/></td>
												<c:if test="${not empty serviceOrder.emailSent}">
												<s:text id="serviceOrderEmailSentFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.emailSent"/></s:text>
												<td align="left"><s:textfield cssClass="input-textUpper" id="emailSent" name="serviceOrder.emailSent" value="%{serviceOrderEmailSentFormattedValue}" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" />
												<img id="emailSent" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty serviceOrder.emailSent}">
												<td align="left"><s:textfield cssClass="input-textUpper" id="emailSent" name="serviceOrder.emailSent" required="true" cssStyle="width:65px;vertical-align:top;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" />
												<img id="emailSent" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>	
								</tr>
								</tr>
								<tr>
								<td align="right" id="OAE" valign="middle" class="listwhitetext" style="width:180px"> Origin Agent Evaluation:&nbsp;</td>
								<%-- <td>
								<configByCorp:customDropDown 	listType="map" list="${oaEvaluation_isactive}" fieldValue="${serviceOrder.OAEvaluation}"   attribute="id=serviceOrderForm_serviceOrder_OAEvaluation class=list-menu name=serviceOrder.OAEvaluation  style=width:145px  headerKey='' headerValue='' onchange='changeStatus()' disabled='true'  "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.OAEvaluation" list="%{oaEvaluation}" headerKey="" headerValue="" tabindex="" onchange="changeStatus()" disabled="true"/></td>
								
								
								<td id="Comments" align="right" valign="middle" class="listwhitetext" >Comments:&nbsp;</td>
								<td rowspan="5" colspan="5"><s:textarea  rows="5" cols="55" id="serviceOrder.feedBack" name="serviceOrder.feedBack"  tabindex=""  cssClass="textareaUpper" disabled="true"/>	
								</td>
								</tr>								
								<tr>
								<td align="right" id="DAE" valign="middle" class="listwhitetext"
													style="width:180px"> Destination Agent Evaluation:&nbsp;</td>
								<%-- <td><configByCorp:customDropDown 	listType="map" list="${daEvaluation_isactive}" fieldValue="${serviceOrder.DAEvaluation}"    attribute="id=serviceOrderForm_serviceOrder_DAEvaluation class=list-menu name=serviceOrder.DAEvaluation  style=width:145px  headerKey='' headerValue='' onchange='changeStatus()' disabled='true'  "/></td>
								 --%>
								<td><s:select cssClass="list-menu" name="serviceOrder.DAEvaluation" list="%{daEvaluation}" headerKey="" tabindex=""  headerValue="" onchange="changeStatus()" disabled="true"/></td>
								
								</tr>
							</tbody>
						</table>
						</div>
						 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.customerServiceSurvey"> 
						 <div  onClick="javascript:animatedcollapse.toggle('customerServiceSurvey')" style="margin: 0px"  >
 <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>				
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;height:13px;cursor:pointer;">Customer Service Survey</td></tr>
<tr>
<td align="left" class="vertlinedata">
</td>
</tr>
</table>
</div>	
						<div id="customerServiceSurvey"	>
<iframe name="iFrame" src ="customerServiceSurvey.html?decorator=asml&popup=true&submitCheck=false&id=${serviceOrder.id}" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
</div>
<s:hidden name="customerServiceSurveyQuesFlag" />
 </configByCorp:fieldVisibility> 
						</c:if>
						
					<%-- 	  <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.customerServiceSurvey"> 					
 <div  onClick="javascript:animatedcollapse.toggle('customerServiceSurvey')" style="margin: 0px"  >
 <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>				
<tr><td align="left" class="subcontenttabChild" style="font-weight:800;height:13px;cursor:pointer;">Customer Service Survey</td></tr>
<tr>
<td align="left" class="vertlinedata">
</td>
</tr>
</table>
</div> 

<div id="customerServiceSurvey"	>
<iframe src ="customerServiceSurvey.html?decorator=asml&popup=true&submitCheck=false&id=${serviceOrder.id}" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
</div>	
 </configByCorp:fieldVisibility> --%> 	 
                
													                 
					                 </c:if>
					                
					                 <c:if test="${customerFeedbackFlag1=='Quality Survey'}">
					                 	<c:if test="${usertype=='ACCOUNT' || usertype=='USER' || usertype=='AGENT'}">					 
 <table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;height:20px;">Quality Survey</td></tr>
</table>
				<table class="detailTabLabel" border="0">
					<tr>
						<td class="listwhitetext" align="right" width="90">Sent Date</td>
					    <c:if test="${not empty serviceOrder.sentDate}">
							 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.sentDate"/></s:text>
							 <td width="70"><s:textfield id="sentDate" cssClass="input-textUpper" name="serviceOrder.sentDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onselect="changeStatus();" tabindex=""/></td><td></td>
						</c:if>
					    <c:if test="${empty serviceOrder.sentDate}">
						<td width="70"><s:textfield id="sentDate" cssClass="input-textUpper" name="serviceOrder.sentDate" cssStyle="width:65px" maxlength="11" readonly="true" onselect="changeStatus();" tabindex="" /></td><td></td>
						</c:if>
						
						<td class="listwhitetext" style="width:80px;" align="right">Return Date</td>
					    <c:if test="${not empty serviceOrder.returnDate}">
							 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.returnDate"/></s:text>
							 <td width="70"><s:textfield id="returnDate" cssClass="input-textUpper" name="serviceOrder.returnDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onselect="changeStatus();" tabindex=""/></td><td></td>
						</c:if>
					    <c:if test="${empty serviceOrder.returnDate}">
						<td width="70"><s:textfield id="returnDate" cssClass="input-textUpper" name="serviceOrder.returnDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onselect="changeStatus();" tabindex=""/></td><td></td>
						</c:if>
						<td class="listwhitetext" align="right" style="width:105px;" >No of emails sent</td>
						<td>
						<s:textfield id="serviceOrder.noOfEmailsSent" cssClass="input-textUpper" name="serviceOrder.noOfEmailsSent" cssStyle="width:65px" readonly="true" tabindex=""/>
						</td>						
						<td class="listwhitetext" style="width:169px;" align="right">Quality Survey Executed By</td>
						<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
						<td align="left" width="87"><s:select cssClass="list-menu" key="serviceOrder.qualitySurveyExecutedBy" list="%{qualitySurveyBy}" headerKey="" headerValue="" cssStyle="width:67px; margin-left:1px;" disabled="true" tabindex=""/></td>
						</c:if>
						<c:if test="${usertype=='USER'}">
						<c:choose>
						<c:when test="${trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup }">
						<td align="left" width="87"><s:select cssClass="list-menu" key="serviceOrder.qualitySurveyExecutedBy" list="%{qualitySurveyBy}" headerKey="" headerValue="" cssStyle="width:67px; margin-left:1px;" disabled="true" tabindex=""/></td>
						</c:when>
						<c:otherwise>
						<td align="left" width="87"><s:select cssClass="list-menu" key="serviceOrder.qualitySurveyExecutedBy" list="%{qualitySurveyBy}" headerKey="" headerValue="" cssStyle="width:67px; margin-left:1px;" onchange="changeStatus();" tabindex=""/></td>
						</c:otherwise>
						</c:choose>
						</c:if> 
						<td class="listwhitetext" align="right" style="width:10px;">NPS&nbsp;Score</td>
			            <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.npsScore" readonly="true" size="2" cssStyle="width:65px;" maxlength="2" onchange="changeStatus();" tabindex=""/></td>
							
						<c:if test="${ratingOption!=''}">
						<td align="left"><b>Rating Scale:</b> </td><td><c:out value="${ratingOption}"/></td>
						</c:if>
					</tr>
					</table>											
<table class="detailTabLabel">
		<tr>
			<td class="listwhitetext" align="right" style="width:90px;">Pre Move Survey</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.preMoveSurvey" readonly="true" cssStyle="width:65px;" size="2" maxlength="2" onchange="changeStatus();" tabindex=""/></td>
			<td class="listwhitetext" align="right" style="width:85px;">Origin Services</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.originServices" readonly="true" cssStyle="width:65px;"size="2" maxlength="2" onchange="changeStatus();" tabindex=""/></td>
			<td class="listwhitetext" align="right" style="width:110px;">Destination Services</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.destinationServices" readonly="true" size="2" cssStyle="width:65px;" maxlength="2" onchange="changeStatus();" tabindex=""/></td>
			<td class="listwhitetext" align="right" style="width:70px;">Coordinator</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.surveyCoordinator" readonly="true" size="2" maxlength="2" cssStyle="width:64px;" onchange="changeStatus();" tabindex=""/></td>
			<td class="listwhitetext" align="right" style="width:70px;">Over All</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.overAll" readonly="true" size="2" maxlength="2" cssStyle="width:65px;" onchange="changeStatus();" tabindex=""/></td>
			<td class="listwhitetext" align="right" style="width:111px;">Recommendation</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="serviceOrder.recommendation" readonly="true" size="2" cssStyle="width:144px;" maxlength="2" onchange="changeStatus();" tabindex=""/></td>
			
			</tr>
			<c:if test="${not empty trackingStatus.deliveryA}">
				<tr>
					<td class="listwhitetext" align="right" style="width:90px;">Coord&nbsp;Eval.&nbsp;OA<configByCorp:fieldVisibility componentId="component.QualitySurvey.CoordinatorEval.validation"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
					<td align="left" class="listwhitetext" ><s:select cssClass="list-menu" key="serviceOrder.oaCoordinatorEval" list="%{coordinatorSortingMap}" headerKey="" headerValue="" cssStyle="width:50px"   tabindex=""/></td>
					<td class="listwhitetext" align="right">OA&nbsp;Code</td>
					<td><s:textfield cssClass="input-text" key="trackingStatus.originAgentCode"  cssStyle="width:50px;"  readonly="true"/></td>
					<td class="listwhitetext" align="right">OA&nbsp;Name</td>
					<td><s:textfield cssStyle="width:145px"   cssClass="input-text"   key="trackingStatus.originAgent"   readonly="true" /></td>
					<td class="listwhitetext" align="right" style="width:111px;">Coord&nbsp;Eval.&nbsp;DA<configByCorp:fieldVisibility componentId="component.QualitySurvey.CoordinatorEval.validation"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
					<td align="left" class="listwhitetext" ><s:select cssClass="list-menu" key="serviceOrder.daCoordinatorEval" list="%{coordinatorSortingMap}" headerKey="" headerValue="" cssStyle="width:67px"  tabindex=""/></td>
					<td class="listwhitetext" align="right">DA&nbsp;Code</td>
					<td><s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode"  cssStyle="width:65px;"  readonly="true" /></td>
					<td class="listwhitetext" align="right">DA&nbsp;Name</td>
					<td><s:textfield cssStyle="width:145px"   cssClass="input-text" key="trackingStatus.destinationAgent"   readonly="true"/></td>
					
				</tr>
			</c:if>
			<c:if test="${empty trackingStatus.deliveryA}">
				<tr>
					<td class="listwhitetext" align="right" style="width:90px;">Coord&nbsp;Eval.&nbsp;OA</td>
					<td align="left" class="listwhitetext" ><s:select cssClass="list-menu" key="serviceOrder.oaCoordinatorEval" list="%{coordinatorSortingMap}" headerKey="" headerValue="" cssStyle="width:67px"   tabindex=""/></td>
					<td class="listwhitetext" align="right">OA&nbsp;Code</td>
					<td><s:textfield cssClass="input-text" key="trackingStatus.originAgentCode"  cssStyle="width:65px;" readonly="true" /></td>
					<td class="listwhitetext" align="right">OA&nbsp;Name</td>
					<td><s:textfield cssStyle="width:145px"   cssClass="input-text"   key="trackingStatus.originAgent"  readonly="true" /></td>
					<td class="listwhitetext" align="right" style="width:90px;">Coord&nbsp;Eval.&nbsp;DA</td>
					<td align="left" class="listwhitetext" ><s:select cssClass="list-menu" key="serviceOrder.daCoordinatorEval" list="%{coordinatorSortingMap}" headerKey="" headerValue="" cssStyle="width:67px"  tabindex=""/></td>
					<td class="listwhitetext" align="right">DA&nbsp;Code</td>
					<td><s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode"  cssStyle="width:65px;" readonly="true"/></td>
					<td class="listwhitetext" align="right" width="60">DA&nbsp;Name</td>
					<td><s:textfield cssStyle="width:145px"   cssClass="input-text"   key="trackingStatus.destinationAgent" readonly="true" /></td>
					
				</tr>
			</c:if>	
		</table>											
		<table>
		<tr>
		<td class="listwhitetext" style="width:92px;" align="right"><b>Remarks:</b></td>
		<td class="listwhitetext" style="text-align:center;"><b>General</b></td>
		<td class="listwhitetext" style="text-align:center;"><b>Transferee</b></td>
		<c:if test="${usertype=='USER'}">
		<td class="listwhitetext" style="text-align:center;"><b>Internal</b></td>
		</c:if>										
		<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
		<td class="listwhitetext" style="text-align:center;"></td>
		</c:if>	
		</tr>
		<tr>
		<td></td>
		<td colspan="">  
						<c:if test="${usertype=='USER'}">
						<c:choose>
						<c:when test="${trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup }">
						<s:textarea name="serviceOrder.general" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  rows="3" onchange="changeStatus()" tabindex=""/>
						</c:when>
						<c:otherwise>
						<s:textarea name="serviceOrder.general" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  rows="3" onchange="changeStatus()" tabindex=""/>
						</c:otherwise>
						</c:choose>
						</c:if>			
		<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
		<s:textarea name="serviceOrder.general" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  rows="3" onchange="changeStatus()" tabindex=""/>
		</c:if>
		 </td>
		 <c:if test="${serviceOrder.corpID!='PUTT'}">
		<td colspan="">
		<s:textarea name="serviceOrder.transferee" cols="39" cssClass="textareaUpper" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  readonly="true" rows="3" onchange="changeStatus()" tabindex=""/>
		</td>
		</c:if>
		<c:if test="${serviceOrder.corpID=='PUTT'}">
		<td colspan="">
		<s:textarea name="serviceOrder.transferee" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" rows="3" onchange="changeStatus()" tabindex=""/>
		</td>
		</c:if>
		<td colspan="">
						<c:if test="${usertype=='USER'}">
						<c:choose>
						<c:when test="${trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup }">
						<s:textarea name="serviceOrder.internal" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  disabled="false" rows="3" onchange="changeStatus()" tabindex=""/>
						</c:when>
						<c:otherwise>
						<s:textarea name="serviceOrder.internal" cols="39" cssClass="textarea" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)"  disabled="false" rows="3" onchange="changeStatus()" tabindex=""/>
						</c:otherwise>
						</c:choose>
						</c:if>			
		<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
		<s:hidden name="serviceOrder.internal" />
		</c:if>
		</td>											
		</tr>
		</table>
		</c:if>		
</c:if>
</div>
</td>
</tr>
</configByCorp:fieldVisibility>
</c:if>	
<c:if test="${serviceOrder.job=='INT'}">
	<configByCorp:fieldVisibility componentId="component.serviceOrder.section.postMoveSurvey.view">
		<jsp:include flush="true" page="postMoveServiceSurvey.jsp"></jsp:include>
	</configByCorp:fieldVisibility>
</c:if>

<c:if test="${genericSurveyOnSoFlag}">
	<tr>
		<td height="10" width="100%" align="left" style="margin:0px">
		<div onClick="javascript:animatedcollapse.toggle('customerSurveyDiv');" style="margin:0px">
		<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
		<tr>
		<td class="headtab_left"></td>
		<td NOWRAP class="headtab_center">&nbsp;Customer&nbsp;Survey</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;</td>
		<td class="headtab_right"></td>
		</tr>
		</table>
		</div>
			<div id="customerSurveyDiv">
				<c:if test="${surveyUserList!='{}' && surveyUserList!=null}">
					<div style="overflow-y:auto;height:400px;margin-left:25px;">
				
					<c:forEach var="individualItem" items="${surveyUserList}" >

						<c:forEach var="surveyQuestionListValue" items="${individualItem.key}" >
				
							<c:if test="${(surveyQuestionListValue.surveyQuestionCategoryType!=null && surveyQuestionListValue.surveyQuestionCategoryType!='') && (prevCategory!=surveyQuestionListValue.surveyQuestionCategoryType)}">
								<div class="cshead"><b class="listwhite" style="color:#3292cd;font-size:13px;">${surveyQuestionListValue.surveyQuestionCategoryType}</b></div>
							</c:if>
		
							<c:if test="${surveyQuestionListValue.surveyQuestionQuestion!=null && surveyQuestionListValue.surveyQuestionQuestion!=''}">
								<div class="qhead"><b class="listwhite" style="color:#15428b;font-size:12px;">Q:&nbsp;${surveyQuestionListValue.surveyQuestionQuestion}</b></div>
							</c:if>
									
								<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='SingleAnswerDropdown'}">
									<select name="surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}" id="surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}" onchange="getSurveyFieldId('drp${surveyQuestionListValue.surveyQuestionId}',this,'dropDown')" style="margin-top:5px;margin-bottom:5px;width:258px" class="list-menu" ></select>
										<script type="text/javascript">
										try{
											var results = '${individualItem.value}';
							                results = results.trim();
							                var res = results.replace("{",'');
							                res = res.replace("}",'');
							                res = res.split(",");
							                var targetElement = document.getElementById('surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}');
											targetElement.length = res.length;
							 				for(i=0;i<res.length;i++){
							 					var idValue = res[i];
							 					idValue = idValue.split("=");
							 					document.getElementById('surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}').options[i].text =idValue[1].trim();
							 					document.getElementById('surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}').options[i].value =idValue[0].trim();
											}
							 				var opt = document.createElement('option');    
						 					var surveyList = document.getElementById('surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}');
						 					opt.selected = 'selected';
						 					surveyList.insertBefore(opt, surveyList[0]);
						 					document.getElementById('surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}').value='${surveyQuestionListValue.surveyByUserAnswerId}';
										}catch(e){
											//alert(e)
										}
						 				</script>
								</c:if>
								
								<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='SingleAnswerRadio'}">
									<c:forEach var="surveyAnswerListValue" items="${individualItem.value}" >
										<c:if test="${surveyQuestionListValue.surveyByUserAnswerId==surveyAnswerListValue.key}">
   											<input type="radio" style="vertical-align:middle;margin:1px 0px 5px;" checked="checked" name="surveyByUserRadio${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getSurveyFieldId('rad${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','radio')"/>
											<span style="color:#15428b;font-size:12px;margin-right:8px;">&nbsp;${surveyAnswerListValue.value}</span>
										</c:if>
										<c:if test="${surveyQuestionListValue.surveyByUserAnswerId!=surveyAnswerListValue.key}">
										    <input type="radio" style="vertical-align:middle;margin:1px 0px 5px;" name="surveyByUserRadio${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getSurveyFieldId('rad${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','radio')"/>
											<span style="color:#15428b;font-size:12px;margin-right:8px;">&nbsp;${surveyAnswerListValue.value}</span>
										</c:if>
									</c:forEach>
								</c:if>
								
								<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='MultipleCheckbox'}">
									<c:forEach var="surveyAnswerListValue" items="${individualItem.value}" >
		                 						<c:set var="checkOpt" value="false" />
									           	<c:set var="keyVal" value="${surveyAnswerListValue.key}" />
		                   						<c:forTokens items="${surveyQuestionListValue.surveyByUserAnswerId}" delims="#" var="kc">
											    <c:if test="${kc == keyVal && !checkOpt}">
											    	<c:set var="checkOpt" value="true" />
											    </c:if>
												</c:forTokens>
			                 				
												<c:if test="${checkOpt}">
													<input type="checkbox" style="vertical-align:middle;margin:1px 0px 5px;" checked name="surveyByUserCheckbox${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getSurveyFieldId('chk${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','checkBox')"/>
													<span style="color:#15428b;font-size:12px;margin-right:8px;">&nbsp;${surveyAnswerListValue.value}</span>
												</c:if>
												<c:if test="${!checkOpt}">
												    <input type="checkbox" style="vertical-align:middle;margin:1px 0px 5px;" name="surveyByUserCheckbox${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getSurveyFieldId('chk${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','checkBox')"/>
													<span style="color:#15428b;font-size:12px;margin-right:8px;">&nbsp;${surveyAnswerListValue.value}</span>
												</c:if>
									</c:forEach>
								</c:if>
								
								<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='Text'}">
									<s:textarea cssStyle="margin-top:5px;margin-bottom:5px;" rows="4" cols="40" cssClass="textarea" name="surveyByUserTextarea${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyQuestionListValue.surveyByUserAnswerId}" onchange="getSurveyFieldId('txt${surveyQuestionListValue.surveyQuestionId}',this,'text')"/>
								</c:if>
								<c:set var="prevCategory" value="${surveyQuestionListValue.surveyQuestionCategoryType}" />
						</c:forEach>
					</c:forEach>
				</div>
					<input type="button" class="cssbutton1" onclick="saveSurveyByUserDetails();" value="Save" style="width:60px;margin:15px 23px;"/>
			</c:if>
			<c:if test="${surveyUserList=='{}' || surveyUserList==null}">
			    <div class="listwhitetext" style="margin:5px 23px;font-size:11px;"><b>No&nbsp;Set&nbsp;Up&nbsp;Found&nbsp;For&nbsp;Survey</b></div>
			</c:if>
			</div>
		</td>
	</tr>		
</c:if>
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:40px;!margin-top:50px;"><span></span></div>
</div>
</div> 
</td>
</tr>
<tr>
<td>
			<table class="detailTabLabel" style="width:80%;">
									<tbody>
										<tr>
											<td align="left" class="listwhitetext" width="30px"></td>
											<td colspan="5"></td>
										</tr>
										<tr>
											<td align="left" class="listwhitetext" width="30px"></td>
											<td colspan="5"></td>
										</tr>
										<tr>
											<fmt:formatDate var="serviceCreatedOnFormattedValue" value="${serviceOrder.createdOn}"	pattern="${displayDateTimeEditFormat}" />
											<td align="right" class="listwhitetext" style="width:50px"><b>
											<fmt:message key='serviceOrder.createdOn' /></b></td>
											<s:hidden name="serviceOrder.createdOn" value="${serviceCreatedOnFormattedValue}" />
											<td>
											<fmt:formatDate	value="${serviceOrder.createdOn}" pattern="${displayDateTimeFormat}" /> 
											<td align="right" class="listwhitetext" style="width:70px"><b>
											<fmt:message key='serviceOrder.createdBy' /></b></td>
											<c:if test="${not empty serviceOrder.id}">
												<s:hidden name="serviceOrder.createdBy" />
												<td><s:label name="createdBy" value="%{serviceOrder.createdBy}" /></td>
											</c:if>
											<c:if test="${empty serviceOrder.id}">
												<s:hidden name="serviceOrder.createdBy" value="${pageContext.request.remoteUser}" />
												<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
											</c:if>
											<td align="right" class="listwhitetext" style="width:75px">
											<b><fmt:message	key='serviceOrder.updatedOn' /></b></td>
											<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${serviceOrder.updatedOn}" pattern="${displayDateTimeEditFormat}" />
											<s:hidden name="serviceOrder.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
											<td>
											<fmt:formatDate	value="${serviceOrder.updatedOn}" pattern="${displayDateTimeFormat}" />
											<td align="right" class="listwhitetext" style="width:75px"><b>
											<fmt:message key='serviceOrder.updatedBy' /></b></td>
											<c:if test="${not empty serviceOrder.id}">
												<s:hidden name="serviceOrder.updatedBy" />
												<td style="width:85px;"><s:label name="updatedBy" value="%{serviceOrder.updatedBy}" /></td>
											</c:if>
											<c:if test="${empty serviceOrder.id}">
												<s:hidden name="serviceOrder.updatedBy" value="${pageContext.request.remoteUser}" />
												<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
											</c:if>
										</tr>
									</tbody>
								</table>
</td>
  </tr>
  </table>
	</div>
		<s:hidden name="componentId" value="module.serviceOrder" /> 
		<sec-auth:authComponent componentId="module.button.serviceOrder.saveButton" >
		     <%-- <s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" id="saveButton" method="save" key="button.save" onmouseover="return chkSelect();" onclick="validatePricing('Save');return forwardToMyMessage2();" tabindex="" /> --%>
			 <c:if test="${serviceOrder.corpID!='STVF'}">
			<input type="button" id="pricingBtnId" name="pricingBtnId" onmouseover="return chkSelect();" onclick="validatePricing('Save');" class="cssbuttonA" style="width:55px; height:25px" value="Save" tabindex="" />
			 </c:if>
			 <c:if test="${serviceOrder.corpID=='STVF'}">
			<input type="button" id="pricingBtnId" name="pricingBtnId" onmouseover="completeTimeString();chkSelect();" onclick="IsValidTime('Save');" class="cssbuttonA" style="width:55px; height:25px" value="Save" tabindex="" />
			</c:if>
			<div id="submitBtn" style="display:none;float:left;margin-right:4px;">
          <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
                <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 				
                	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
						<s:submit id="submitButtonIdOFFMOVE" name="submitButtonIdOFFMOVE" cssClass="cssbuttonA" onclick="return forwardToMyMessage2();" cssStyle="width:55px; height:25px" method="save" key="button.save" tabindex="" /> 
					</configByCorp:fieldVisibility>
				</c:if>
			
				<s:submit id="submitButtonId" name="submitButtonId"  method="save" key="button.save"  onclick="return forwardToMyMessage2();" cssClass="cssbuttonA" cssStyle="width:55px; height:25px;" tabindex=""/> 
 			</div>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.button.serviceOrder.AddButton" >
		<c:if test="${not empty serviceOrder.id}">
		   <c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
            <input type="button" class="cssbutton1" value="Add" style="width:55px; height:25px" onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
           </c:if> 
            <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
			 <input type="button" class="cssbutton1" value="Add" style="width:55px; height:25px" onclick="addServiceOrder();" tabindex="" />
		   </c:if>
		</c:if>
		<c:if test="${not empty serviceOrder.id}">			
			<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onmousemove="myDate();" onclick="this.form.reset();populateInclusionExclusionData('noDefault','1','T');checkColumn();cheackStatusReasonReset();disabledOriginTicketBtnReset();disabledDestinTicketBtnReset();newFunctionForCountryState(); checkJobTypeOnReset();setVipImageReset();resetSurveyResult();putDefault();packModeMandatoryUGWW();"  tabindex="" />
		</c:if>
		<c:if test="${empty serviceOrder.id}">
            <input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onmousemove="myDate();" onclick="this.form.reset();populateInclusionExclusionData('noDefault','1','T');checkColumn();cheackStatusReasonReset();newFunctionForCountryState(); checkJobTypeOnReset(); setVipImageReset();resetSurveyResult();putDefault();packModeMandatoryUGWW();"  tabindex="" />			
		</c:if>
		
		<c:if test="${serviceOrder.controlFlag !='G'}">
		<c:if test="${not empty serviceOrder.id}">
		<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
            <input type="button" class="cssbutton1" value="Copy without Pricing" style="width:140px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
        </c:if> 
        <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
			<input type="button" class="cssbutton1" value="Copy without Pricing" style="width:140px; height:25px" onclick="addCopyServiceOrder();" tabindex="" />
		</c:if>
		</c:if>	
		<c:if test="${not empty serviceOrder.id}">
		<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
            <input type="button" class="cssbutton1" value="Copy with Pricing" style="width:140px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
        </c:if> 
         <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>
			<input type="button" class="cssbutton1" value="Copy with Pricing" style="width:130px; height:25px" onclick="addWithPricingServiceOrder();" tabindex=""/>
		</c:if>
		</c:if>
		
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
		<c:if test="${not empty serviceOrder.id}">
        <c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
            <input type="button" class="cssbutton1" value="Copy Order with Resources" style="width:165px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
        </c:if> 
         <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'>
            <input type="button" class="cssbutton1" value="Copy Order with Resources" style="width:165px; height:25px" onclick="CopyOrderWithResources();" tabindex=""/>
        </c:if>
        </c:if>
		</sec-auth:authComponent>
		</c:if>
		
		<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.AddwithCopyForwarding">
		<c:if test="${not empty serviceOrder.id}">
		<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
            <input type="button" class="cssbutton1" value="Add with Copy Forwarding" style="width:170px; height:25px"   onclick="javascript:alert('Cannot add a new Service Order as the Customer File is not active.')" tabindex="" />   
        </c:if> 
        <c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
			<input type="button" class="cssbutton1" value="Add with Copy Forwarding" style="width:170px; height:25px" onclick="addCopyForwardingServiceOrder();" tabindex="" />
		</c:if>
		</c:if>
		</configByCorp:fieldVisibility>	
		</c:if>					
		</sec-auth:authComponent>
				<s:hidden name="serviceOrder.gbl"  />
			    <s:hidden name="customerFile.sequenceNumber" />
			    <s:hidden name="serviceOrder.sequenceNumber" /> 
			    <s:hidden name="serviceOrder.statusNumber"/>
			    <s:hidden name="serviceOrder.contract" /> 
			   <s:hidden name="serviceOrder.ship" />
			    <s:hidden name="miscellaneous.id" /> 
			   <s:hidden name="miscellaneous.shipNumber" /><s:hidden name="miscellaneous.g11" /> 
			   <s:hidden name="serviceOrder.billToCode" /> 
			   <s:hidden name="serviceOrder.billToName" /> 
			   <s:hidden name="serviceOrder.orderBy" value="%{customerFile.orderBy}" /> 
			   <s:hidden name="serviceOrder.orderPhone" value="%{customerFile.orderPhone}" />
			    <s:hidden name="serviceOrder.payType" value="%{customerFile.billPayMethod}" /> 
				<s:hidden name="serviceOrder.actualNetWeight" value="%{miscellaneous.actualNetWeight}" /> 
				<s:hidden name="serviceOrder.actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" /> 
				<s:hidden name="serviceOrder.actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" /> 
				<s:hidden name="serviceOrder.estimatedNetWeight" value="%{miscellaneous.estimatedNetWeight}" /> 
				<s:hidden name="serviceOrder.estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" />
				 <s:hidden name="miscellaneous.corpID" /> 
				<s:hidden name="miscellaneous.sequenceNumber" /> 
				<s:hidden name="miscellaneous.ship" /> 
				<s:hidden name="firstDescription" />
				<s:hidden name="secondDescription" />
			    <s:hidden name="thirdDescription" /> 
			    <s:hidden name="fourthDescription" /> 
				<s:hidden name="fifthDescription" />
				 <s:hidden name="sixthDescription" />
				 <s:hidden name="seventhDescription" /> 
				 <s:hidden name="eigthDescription" />
	             <s:hidden name="ninthDescription" />
	             <s:hidden name="tenthDescription" />
	             <s:hidden name="miscellaneous.listLotNumber" /> 
				<s:hidden name="miscellaneous.bookerSelfPacking" /> 
				<s:hidden name="miscellaneous.bookerOwnAuthority" /> 
				<s:hidden name="miscellaneous.packAuthorize" /> 
				<s:hidden name="miscellaneous.unPack" />
				 <s:hidden name="miscellaneous.packingBulky" />
				 <s:hidden name="miscellaneous.codeHauling" /> 
				<s:hidden name="miscellaneous.tarif" /> 
				<s:hidden name="miscellaneous.section" />
			   <s:hidden name="miscellaneous.applicationNumber3rdParty" />
				 <s:hidden	name="miscellaneous.needWaiveEstimate" /> 
				<s:hidden name="miscellaneous.needWaiveSurvey" /> 
				<s:hidden name="miscellaneous.needWaivePeakRate" />
				 <s:hidden name="miscellaneous.needWaiveSRA" />
				 <s:hidden name="miscellaneous.vanLineOrderNumber" />
				 <s:hidden name="miscellaneous.vanLineNationalCode" /> 
				<s:hidden name="miscellaneous.originCountyCode" /> 
				<s:hidden name="miscellaneous.originStateCode" />
				 <s:hidden name="miscellaneous.originCounty" />
				 <s:hidden name="miscellaneous.originState" /> 
				<s:hidden name="miscellaneous.destinationCountyCode" /> 
				<s:hidden name="miscellaneous.destinationStateCode" />
				 <s:hidden name="miscellaneous.destinationCounty" />
				 <s:hidden name="miscellaneous.destinationState" />
				 <s:hidden name="miscellaneous.extraStopYN" /> 
				<s:hidden name="miscellaneous.mile" />
			    <s:hidden name="miscellaneous.ratePSTG" /> 
				<s:hidden name="miscellaneous.estimatedRevenue" />
				<s:hidden name="miscellaneous.warehouse" /> 
				<s:hidden name="miscellaneous.tag" />
				 <s:hidden name="miscellaneous.actualHouseHoldGood" /> 
				 <s:hidden	name="miscellaneous.actualRevenueDollor" />
				 <s:hidden	name="miscellaneous.agentPerformNonperform" />
				 <s:hidden	name="miscellaneous.destination24HR" /> 
				 <s:hidden	name="miscellaneous.origin24Hr" />
				 <s:hidden	name="miscellaneous.vanLinePickupNumber" />
			<c:if test="${not empty miscellaneous.atlasDown}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.atlasDown" /></s:text>
			 <s:hidden  name="miscellaneous.atlasDown" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.atlasDown}">
		 <s:hidden   name="miscellaneous.atlasDown"/> 
	 </c:if>
	 		<c:if test="${not empty miscellaneous.shipmentRegistrationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.shipmentRegistrationDate" /></s:text>
			 <s:hidden  name="miscellaneous.shipmentRegistrationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.shipmentRegistrationDate}">
		 <s:hidden   name="miscellaneous.shipmentRegistrationDate"/> 
	 </c:if>
				 <s:hidden name="miscellaneous.actualBoat" />
				 <s:hidden name="miscellaneous.trailerVolumeWeight" /> 
				<s:hidden name="miscellaneous.gate" /> 
				<s:hidden name="miscellaneous.trailer" />
			    <s:hidden name="miscellaneous.pendingNumber" /> 
				<s:hidden name="miscellaneous.vanLineCODAmount" /> 
				<s:hidden name="miscellaneous.weightTicket" /> 
				<s:hidden name="miscellaneous.domesticInstruction" />
				<s:hidden name="miscellaneous.paperWork" />
				 <c:if test="${not empty miscellaneous.recivedPO}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPO" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPO" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPO}">
		 <s:hidden   name="miscellaneous.recivedPO"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedHVI}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedHVI" /></s:text>
			 <s:hidden  name="miscellaneous.recivedHVI" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedHVI}">
		 <s:hidden   name="miscellaneous.recivedHVI"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.confirmOriginAgent}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.confirmOriginAgent" /></s:text>
			 <s:hidden  name="miscellaneous.confirmOriginAgent" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.confirmOriginAgent}">
		 <s:hidden   name="miscellaneous.confirmOriginAgent"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.dd1840}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.dd1840" /></s:text>
			 <s:hidden  name="miscellaneous.dd1840" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.dd1840}">
		 <s:hidden   name="miscellaneous.dd1840"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.quoteOn}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.quoteOn" /></s:text>
			 <s:hidden  name="miscellaneous.quoteOn" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.quoteOn}">
		 <s:hidden   name="miscellaneous.quoteOn"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedPayment}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPayment" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPayment" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPayment}">
		 <s:hidden   name="miscellaneous.recivedPayment"/> 
	 </c:if>	
	<c:if test="${not empty miscellaneous.printGbl}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.printGbl" /></s:text>
			 <s:hidden  name="miscellaneous.printGbl" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.printGbl}">
		 <s:hidden   name="miscellaneous.printGbl"/> 
	 </c:if>
	 <c:if test="${not empty miscellaneous.actualPickUp}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.actualPickUp" /></s:text>
			 <s:hidden  name="miscellaneous.actualPickUp" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.actualPickUp}">
		 <s:hidden   name="miscellaneous.actualPickUp"/> 
	 </c:if>			
				<s:hidden name="miscellaneous.shortFuse" />
				
				<s:hidden name="miscellaneous.peakRate" /> 
				<s:hidden name="miscellaneous.createdBy" />
				<s:hidden name="miscellaneous.updatedBy" /> 
				<s:hidden name="miscellaneous.preApprovalRequest1" />
	            <s:hidden name="miscellaneous.preApprovalRequest2" />
	            <s:hidden name="miscellaneous.preApprovalRequest3" />
				<s:hidden name="miscellaneous.dp3" />
				<c:if test="${not empty miscellaneous.originDocsToBase}">
		 <s:text id="miscellaneousoriginDocsToBase" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.originDocsToBase" /></s:text>
			 <s:hidden  name="miscellaneous.originDocsToBase" value="%{miscellaneousoriginDocsToBase}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.originDocsToBase}">
		 <s:hidden name="miscellaneous.originDocsToBase"/> 
	 </c:if>
			
			<c:if test="${not empty miscellaneous.millitarySurveyDate}">
		 <s:text id="miscellaneoumillitarySurveyDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.millitarySurveyDate" /></s:text>
			 <s:hidden  name="miscellaneous.millitarySurveyDate" value="%{miscellaneoumillitarySurveyDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.millitarySurveyDate}">
		 <s:hidden name="miscellaneous.millitarySurveyDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty miscellaneous.millitarySurveyResults}">
		 <s:text id="miscellaneousmillitarySurveyResults" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.millitarySurveyResults" /></s:text>
			 <s:hidden  name="miscellaneous.millitarySurveyResults" value="%{miscellaneousmillitarySurveyResults}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.millitarySurveyResults}">
		 <s:hidden name="miscellaneous.millitarySurveyResults"/> 
	 </c:if>
	 
	 <c:if test="${not empty miscellaneous.millitaryDeliveryDate}">
		 <s:text id="miscellaneousmillitaryDeliveryDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.millitaryDeliveryDate" /></s:text>
			 <s:hidden  name="miscellaneous.millitaryDeliveryDate" value="%{miscellaneousmillitaryDeliveryDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.millitaryDeliveryDate}">
		 <s:hidden name="miscellaneous.millitaryDeliveryDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty miscellaneous.millitarySitDestinationDate}">
		 <s:text id="miscellaneousmillitarySitDestinationDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.millitarySitDestinationDate" /></s:text>
			 <s:hidden  name="miscellaneous.millitarySitDestinationDate" value="%{miscellaneousmillitarySitDestinationDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.millitarySitDestinationDate}">
		 <s:hidden name="miscellaneous.millitarySitDestinationDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty miscellaneous.millitaryActualWeightDate}">
		 <s:text id="miscellaneousmillitaryActualWeightDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.millitaryActualWeightDate" /></s:text>
			 <s:hidden  name="miscellaneous.millitaryActualWeightDate" value="%{miscellaneousmillitaryActualWeightDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.millitaryActualWeightDate}">
		 <s:hidden name="miscellaneous.millitaryActualWeightDate"/> 
	 </c:if>
	 <c:if test="${not empty miscellaneous.preApprovalDate1}">
		 <s:text id="miscellaneouspreApprovalDate1" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.preApprovalDate1" /></s:text>
			 <s:hidden  name="miscellaneous.preApprovalDate1" value="%{miscellaneouspreApprovalDate1}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.preApprovalDate1}">
		 <s:hidden name="miscellaneous.preApprovalDate1"/> 
	 </c:if>
	 
	 <c:if test="${not empty miscellaneous.preApprovalDate2}">
		 <s:text id="miscellaneouspreApprovalDate2" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.preApprovalDate2" /></s:text>
			 <s:hidden  name="miscellaneous.preApprovalDate2" value="%{miscellaneouspreApprovalDate2}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.preApprovalDate2}">
		 <s:hidden name="miscellaneous.preApprovalDate2"/> 
	 </c:if>
	 	<c:if test="${not empty miscellaneous.preApprovalDate3}">
		 <s:text id="miscellaneouspreApprovalDate3" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.preApprovalDate3" /></s:text>
			 <s:hidden  name="miscellaneous.preApprovalDate3" value="%{miscellaneouspreApprovalDate3}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.preApprovalDate3}">
		 <s:hidden name="miscellaneous.preApprovalDate3"/> 
	 </c:if>
			<c:if test="${not empty miscellaneous.updatedOn}">
				<fmt:formatDate var="updatedOnFormattedValue" value="${miscellaneous.updatedOn}" pattern="${displayDateTimeFormat}"/>
				<s:hidden name="miscellaneous.updatedOn" value="${updatedOnFormattedValue}"/>
				</c:if>
				<s:hidden name="miscellaneous.selfHaul"/>
				<s:hidden name="customerFile.id" />
			    <s:hidden name="serviceOrder.corpID" /> 
			     <s:hidden name="corpIDform" value="${serviceOrder.corpID}" /> 
			    <s:hidden name="serviceOrder.revisedTotalExpense" /> 
				<s:hidden name="serviceOrder.revisedTotalRevenue" /> 
				<s:hidden name="serviceOrder.revisedGrossMargin" />
				<s:hidden name="serviceOrder.revisedGrossMarginPercentage" /> 
				<s:hidden name="serviceOrder.entitledTotalAmount" /> 
				<s:hidden name="serviceOrder.estimatedTotalExpense" /> 
				<s:hidden name="serviceOrder.estimatedTotalRevenue" /> 
				<s:hidden name="serviceOrder.estimatedGrossMargin" /> 
				<s:hidden name="serviceOrder.distributedTotalAmount" />  
				<s:hidden name="serviceOrder.estimatedGrossMarginPercentage"/> 
				<s:hidden name="serviceOrder.actualExpense"/>
			    <s:hidden name="serviceOrder.actualRevenue"/> 
				<s:hidden name="serviceOrder.actualGrossMargin"/> 
				<s:hidden name="serviceOrder.actualGrossMarginPercentage"/>
				<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
				
				<s:hidden name="serviceOrder.projectedActualExpense" />
				<s:hidden name="serviceOrder.projectedActualRevenue" />
				
				<s:hidden name="serviceOrder.projectedGrossMargin" />
				<s:hidden name="serviceOrder.actualAuto"/>
				<s:hidden name="serviceOrder.actualBoat"/>				
				<s:hidden name="serviceOrder.controlFlag" />				
				<s:hidden name="miscellaneous.haulingAgentCode"/>
				<s:hidden name="miscellaneous.haulingAgentVanlineCode"/> 
				<s:hidden name="miscellaneous.haulerName"/> 
				<s:hidden name="miscellaneous.driverId"/> 
				<s:hidden name="miscellaneous.driverName"/> 
				<s:hidden name="miscellaneous.driverCell"/> 
				<s:hidden name="miscellaneous.carrier"/> 
				<s:hidden name="miscellaneous.carrierName"/> 
				<s:hidden name="miscellaneous.fromNonTempStorage"/> 
				<s:hidden name="miscellaneous.weaponsIncluded"/> 
				<s:hidden  name="miscellaneous.packingDriverId" />
				<s:hidden  name="miscellaneous.deliveryDriverId" />
				<s:hidden  name="miscellaneous.loadingDriverId" />
				<s:hidden  name="miscellaneous.sitDestinationDriverId" />
				<s:hidden name="miscellaneous.originScore"/> 
				<s:hidden name="serviceOrder.inlandCode"/> 
				<s:hidden name="miscellaneous.loadCount"/> 
                <s:hidden name="miscellaneous.loadSignature"/> 
                <s:hidden name="miscellaneous.deliveryCount"/> 
                <s:hidden name="miscellaneous.deliverySignature"/> 
                <s:hidden name="miscellaneous.tractorAgentVanLineCode"/> 
                <s:hidden name="miscellaneous.tractorAgentName"/> 
                <s:hidden name="miscellaneous.vanAgentVanLineCode"/>
                <s:hidden name="miscellaneous.vanAgentName"/>
                <s:hidden name="miscellaneous.vanID"/>
                <s:hidden name="miscellaneous.vanName"/>
					<c:if test="${not empty miscellaneous.transDocument}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.transDocument" /></s:text>
			 <s:hidden  name="miscellaneous.transDocument" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.transDocument}">
		 <s:hidden   name="miscellaneous.transDocument"/> 
	 </c:if>
				<s:hidden name="serviceOrder.entitleNumberAuto"/>
				<s:hidden name="serviceOrder.entitleBoatYN"/>
				<s:hidden name="serviceOrder.estimateAuto"/>
				<s:hidden name="serviceOrder.estimateBoat"/>
				<s:hidden name="serviceOrder.lastExtra"/>
				<s:hidden name="commodityParameter"/>
				<s:hidden name="SNumber" value="%{serviceOrder.shipNumber}"/>
                <c:set var="SNumber" value="${serviceOrder.shipNumber}" />
        <%
        String value=(String)pageContext.getAttribute("SNumber") ;
        Cookie cookie = new Cookie("TempnotesId",value);
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        %>      <c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
                 <c:set var="tableName" value="serviceorder" scope="session"/>
			    <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
	     
<script type="text/javascript">
function progressBarAutoSave(tar){
	showOrHideAutoSave(tar);
	}

	function showOrHideAutoSave(value) {
	    if (value==0) {
	        if (document.layers)
	           document.layers["overlay"].visibility='hide';
	        else
	           document.getElementById("overlay").style.visibility='hidden';
	   }
	   else if (value==1) {
	       if (document.layers)
	          document.layers["overlay"].visibility='show';
	       else
	          document.getElementById("overlay").style.visibility='visible';
	   }
	}
var pricingClicked=false;
function pricingClick(){
	pricingClicked=true;
}
var isLoadPricing='';
function loadSoPricing(){
		if(('${serviceOrder.id}' == '' || '${serviceOrder.id}' == null) && (pricingClicked==true)){
			alert('Please save the Order first before accessing the Pricing section.');
			return false;
		}
		if(isLoadPricing ==''){
			progressBarAutoSave('0');
			findSoAllPricing();
			isLoadPricing = 'YES';
			progressBarAutoSave('1');
		}
	}

function validatePricing(str){
   	var obj = document.getElementById('accInnerTable');
   	  if(obj == null || obj == undefined){
   		 saveForm(str);
   		  return true;
   	  }else{
   		acoountlineinvoice(str);
   		//findAllPricingLineId(str);
   		  //return false;
   	  }
   }
function saveForm(str){
	showOrHide(1);
	document.getElementById("pricingBtnId").style.display="none";
	document.getElementById("submitBtn").style.display="block";
	
	<c:if test="${not empty serviceOrder.id}">
	var obj = document.getElementById('accInnerTable');
	  if(obj != null && obj != undefined){
		  try{
	
			document.getElementById('pricingFooterValue').value = pricingFooterValue;
			alert("D"+document.getElementById('pricingFooterValue').value)
		  }catch(e){
			  
		  }
	  }
     </c:if> 	    
	
	
	var id = document.getElementById('submitButtonIdOFFMOVE');
	if(id == null || id == undefined){
		document.getElementById('submitButtonId').click();
	}else{
		document.getElementById('submitButtonIdOFFMOVE').click();
	}
}
function saveAuto(clickType){
	var obj = document.getElementById('accInnerTable');
	if(obj != null || obj != undefined){
		
		//validatePricing('TabChange');
		findAllPricingLineId('TabChange',clickType);
	}else{
		ContainerAutoSave(clickType);
	}
}
try{
	<c:if test="${quotesToValidate!='QTG'}"> 
	document.getElementById("pricingShow").style.display="block"; 
	document.getElementById("estimatePricingShow").style.display="none";  
	</c:if>
	<c:if test="${quotesToValidate=='QTG'}"> 
	document.getElementById("pricingShow").style.display="none";  
	document.getElementById("estimatePricingShow").style.display="block";  
	</c:if>
	}
	catch(e){}
<c:if test="${usertype!='PARTNER' && usertype!='AGENT'}">  
try{
onLoadcheckTransferDate();
document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].select(); 
autoPopulate_customerFile_destinationCountry(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry']);
 	}
catch(e){
}

try{
 	if('${stateshitFlag}'!= "1"){
 	getDestinationState(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry']);
 	getState(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry']);
 	}
 }
 catch(e){}
 try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	 if(oriCon=="")	 {		
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
	 }else{
		 if(enbState.indexOf(oriCon)> -1){
				document.getElementById('originState').disabled = false; 
				document.getElementById('originState').value='${serviceOrder.originState}';
			}else{
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";
			} 
		}
	 }
catch(e){}
try{	
	 var enbState1 = '${enbState}';	
	 var desCon=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	 if(desCon=="")	 {		
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
	 }else{
	  		if(enbState1.indexOf(desCon)> -1){
				document.getElementById('destinationState').disabled = false;		
				document.getElementById('destinationState').value='${serviceOrder.destinationState}';
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";
			}
	 	}
	}
catch(e){} 
 </c:if>
</script>
<script type="text/javascript"> 
<c:if test="${usertype!='PARTNER' && usertype!='AGENT'}"> 		
	try{
	//autoPopulate_customerFile_originCountry(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry']);
	<c:if test="${empty serviceOrder.id}">
	autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	</c:if>
  	//findCompanyDivisionByBookAg('onload');
  	}catch(e){}
  </c:if>	
 </script>
 <script type="text/javascript"> 
 try{
zipCode();
ostateMandatory();
dstateMandatory();
}
catch(e){} 
 <c:if test="${usertype!='PARTNER' && usertype!='AGENT'}"> 		
   try{
    validAutoBoat();
    validateDistanceUnit();
	var f = document.getElementById('serviceOrderForm'); 
	f.setAttribute("autocomplete", "off"); 
}
catch(e)
{
}
<%-- try{
if('${stateshitFlag}'!= 1){
getNewCoordAndSale();}}
catch(e){} --%>
try{
	checkUnitValidation();
}
catch(e){}

</c:if>
<c:if test="${usertype!='PARTNER'}"> 
validateDistanceUnit();
</c:if>
</script>
<script type="text/javascript"> 
try{
	initLoader();
	}catch(e){}
<%--
	function addResource(day){
		var catTempID = "catTempID"+day;
		var catTempVal = document.getElementById(catTempID).value;
		var id = '${serviceOrder.id}';
		if(id.length == 0){
			alert('Please Save Service Order first before adding Resource Template.');
		}else if(catTempVal.trim() == ''){
			alert("Select any Category.");
			document.getElementById(catTempID).focus();
			//document.getElementById(catTempID).select();
		}else{
			var categoryType='';
			if(catTempVal=='C'){categoryType='Crew'+day}else if(catTempVal=='M'){categoryType='Material'+day}else if(catTempVal=='E'){categoryType='Equipment'+day}
			var url = "addRowToResourceTemplateGridSO.html?tempId="+id+"&shipNumber=${serviceOrder.shipNumber}&day="+day+"&type="+catTempVal+"&categoryType="+categoryType;
			document.forms['serviceOrderForm'].action =url;
			document.forms['serviceOrderForm'].submit();
		}
	}
--%>
function transferResourceToWorkTicket(){
	window.open('transferResourceToWorkTicket.html?decorator=popup&popup=true&shipNumber=${serviceOrder.shipNumber}','accountProfileForm','height=500,width=925,top=0, scrollbars=yes,resizable=yes').focus();
}
</script>
<script type="text/javascript">
function confirmDeleteResource(targetElement,day,categoryType,ticketTransferStatus){
	var agree;
	if(ticketTransferStatus == 'true' || ticketTransferStatus == true ){
		agree=confirm("This action will delete resource from workTicket.\n Are you sure you wish to remove this row?");
	}else{
		agree=confirm("Are you sure you wish to remove this row?");
	}
	if (agree){
		showOrHide(1);
		catagoryName=categoryType+day;
		dayId=day;
		var url = 'deleteQuotaionResourceAjax.html?ajax=1&serviceOrderId=${serviceOrder.id}&id='+encodeURI(targetElement)+'&day='+day+"&categoryType="+categoryType+day;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}else{
		return false;
	}
}
<%--
function confirmSubmit(targetElement,status,day,ticketTransferStatus,categoryType){
	var agree;
	if(ticketTransferStatus == 'true' || ticketTransferStatus == true ){
		agree=confirm("This action will delete resource from workTicket.\n Are you sure you wish to remove this row?");
	}else{
		agree=confirm("Are you sure you wish to remove this row?");
	}
	if (agree){
		var url = 'deleteResource.html?serviceOrderId=${serviceOrder.id}&id='+encodeURI(targetElement)+'&day='+day+"&categoryType="+categoryType+day;
		location.href = url;
	}else{
		return false;
	}
}
--%>
</script>
<script type="text/javascript">
function createPrice(){
	var cfContract = document.forms['serviceOrderForm'].elements['cfContract'].value;
	var id='${serviceOrder.id}';
	var SOcontract = cfContract;
	var contractType='';
	var contractName='';
	if(cfContract.length > 0){
		contractType = cfContract;
		contractName= 'CustomerFile Contract';
	}else{
		alert('Enter Customerfile Contract');
	}
	
	if(contractType.length > 0){
		var conf = 'Continue with '+contractName+': '+contractType;
		if(confirm(conf)){
			var url="createPriceSO.html?tempId="+encodeURI(id)+"&serviceOrderId=${serviceOrder.id}&contractName=" + contractType+"&shipNumber=${serviceOrder.shipNumber}";
			document.forms['serviceOrderForm'].action =url;
			document.forms['serviceOrderForm'].submit();
		}
	}
}
</script>
<script type="text/javascript">
var http2 = getHTTPObject(); 
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}

function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
            window.location.reload();
	} }
</script>
<script type="text/javascript">
function findResource(type,descriptId){
	if(type.length > 0){
		document.getElementById(descriptId).value='';
	}else{
	}
}
function checkChar(descript,descriptId,categoryId,divId) {
	 	AutoCompleterAjaxCall(descriptId,categoryId,divId);
}

function AutoCompleterAjaxCall(descriptId,categoryId,divId){
	var descriptVal = document.getElementById(descriptId).value;
	var type = document.getElementById(categoryId).value;	
	document.getElementById('serviceOrderForm').setAttribute("AutoComplete","off");
	 document.forms['serviceOrderForm'].elements['tempId1'].value=descriptId;
	 document.forms['serviceOrderForm'].elements['tempId2'].value=divId;
	 new Ajax.Request('/redsky/resourceAutocomplete.html?ajax=1&tempCategory='+type+'&tempresource='+encodeURIComponent(descriptVal)+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      var mydiv = document.getElementById(divId);
                  document.getElementById(divId).style.display = "block";                
                  mydiv.innerHTML = response;
                  mydiv.show(); 
			    },
			    onFailure: function(){ alert('Something went wrong...') }
			  });
}
function checkValue(result){
	var targetID=document.forms['serviceOrderForm'].elements['tempId1'].value;
	var targetID2=document.forms['serviceOrderForm'].elements['tempId2'].value;
	document.getElementById(targetID).value=result.innerHTML;
	document.getElementById(targetID2).style.display = "none";
}
var http17 = getHTTPObject();
var http16 = getHTTPObject();

function handleHttpResponse16(){
	if (http16.readyState == 4){
		showOrHide(0);
            var results = http16.responseText
            results = results.trim();
            if(results == '[]'){
            	document.getElementById(resoure_ID).value='';
            	resoure_ID='';
            }
	} 
}
function handleHttpResponse17(){
	if (http17.readyState == 4){
		showOrHide(0);
            var results = http17.responseText
            results = results.trim();
            if(results == '[]'){
            	document.getElementById(resoure_ID).value='';
            	resoure_ID='';
            }} }
var resoure_ID='';
function handleHttpResponse18(){
	if (http17.readyState == 4){
		showOrHide(0);
            var results = http17.responseText
            results = results.trim();
            if(results == '[]'){
            	alert('Resource already exist for this day');
            	document.getElementById(resoure_ID).value='';
            	resoure_ID='';
            }else if(results == ''){
            	document.getElementById(resoure_ID).value='';
            	alert('Resource already exist for this day');
            }} }
function setFlag(){
	autoSave='';
}
function checkSPChar(id) {
//	 valid(document.forms['entitlementForm'].elements['entitlement.eOption'],'special');
	 var chArr = new Array("%","&","|","\\","#",":","~","\"");
	 var comments = document.getElementById('comments'+id).value;
     for ( var i = 0; i < chArr.length; i++ ) {
	 	comments = comments.split(chArr[i]).join(''); 
	 }
	 document.getElementById('comments'+id).value = comments;
	 return true;
 }
 
var resId,resDay;
var httpHub = getHTTPObject();
function validateHubLimit(id,day,fieldName,workTicketID){
	resId=id;resDay=day;
	var category = document.getElementById('category'+id).value;
	var descript = document.getElementById('descript'+id).value;
	var cost = document.getElementById('cost'+id).value;
	if(workTicketID != null && workTicketID != ''){
		showOrHide(1);
		var tickets = workTicketID+'-'+category+'-'+descript+'-'+cost;
		var url = "checkHubLimitForResourceSOAjax.html?ajax=1&decorator=simple&popup=true&tickets="+tickets;
		httpHub.open("GET", url, true);
		httpHub.onreadystatechange = function(){ handleHttpHubResponse(workTicketID);};
		httpHub.send(null);
	}else{
		autoSaveAjax(id,day,fieldName,'','');
	}
}

function handleHttpHubResponse(workTicketID){
    if(httpHub.readyState == 4){
      var results = httpHub.responseText;
      results = results.trim();
     	if(results == 'Y'){
     		showOrHide(0);
          	var agree = confirm("Exceeding the limit & will be sent in Dispatch Queue for Approval");
          	if(agree){
          		autoSaveAjax(resId,resDay,'','PENDING',workTicketID);
          	}
     	 }else{
     		autoSaveAjax(resId,resDay,'','',workTicketID);
     	 }
      }
}

var autoSave='';
function autoSaveAjax(id,day,fieldName,ticketStatus,workTicketID){
	if(autoSave == '' || fieldName != 'RESOURCE'){
		autoSave='YES';
		var category = document.getElementById('category'+id).value;
		var descript = document.getElementById('descript'+id).value;
		var cost = document.getElementById('cost'+id).value;
		var hour = document.getElementById('hour'+id).value;
		var comments = document.getElementById('comments'+id).value;
			
		var begin = convertDateFormat('beginDt'+day);
		var end = convertDateFormat('endDt'+day);
		var dturl='';
		if(begin.length > 0 && end.length > 0 ){
			dturl = "&beginDt="+begin+"&endDt="+end;
		}
		if(category.length > 0 && descript.length > 0 && cost.length > 0 && hour.length > 0 ){
			if(checkNumberResource(cost,'cost'+id) && checkNumberResource(hour,'hour'+id)){
				resoure_ID='descript'+id;
				showOrHide(1);
				var url = "autoSaveResouceTemplateAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}&day="+day+"&category="+category+"&resource="+encodeURIComponent(descript)+"&cost="+cost+"&comments="+encodeURIComponent(comments)+"&tempId="+id+"&estHour="+hour+"&btnType=addrow&workTicketStatus="+ticketStatus+"&workTicketID="+workTicketID+""+dturl;
				http17.open("GET", url, true);
			    http17.onreadystatechange = handleHttpResponse18;
			    http17.send(null);
			}
		}
	}
}
function autoSaveAjax1(id,day,fieldName){
	if(autoSave==''){
		document.getElementById('descript'+id).value='';
		return;
	}
		var category = document.getElementById('category'+id).value;
		var descript = document.getElementById('descript'+id).value;
		var cost = document.getElementById('cost'+id).value;
		var comments = document.getElementById('comments'+id).value;
	
		var begin = convertDateFormat('beginDt'+day);
		var end = convertDateFormat('endDt'+day);
		var dturl='';
		if(begin.length > 0 && end.length > 0 ){
			dturl = "&beginDt="+begin+"&endDt="+end+"&day="+day;
		}
		if(category.length > 0 && descript.length > 0 && cost.length > 0 ){
			if(checkNumberResource(cost,'cost'+id)){
				resoure_ID='descript'+id;
				showOrHide(1);
				var url = "autoSaveResouceTemplateAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}&day="+day+"&category="+category+"&resource="+descript+"&cost="+cost+"&comments="+comments+"&tempId="+id+"&btnType=addrow"+dturl;
				http16.open("GET", url, true);
			    http16.onreadystatechange = handleHttpResponse16;
			    http16.send(null);
			}
		}
}
function checkNumberResource(cost,costId){
	 if(parseInt(cost) == cost){
		 return true;
	 }else if(/^\d+\.\d+$/.test(cost)){
		  return true;
	 }else{
		 alert('Enter valid Number');
		 document.getElementById(costId).value='';
		 return false;
	 }
	return true;
}
</script>
<script type="text/javascript">
<%--
function createPrice123(){
	var SOcontract = '${customerFile.contract}';
	var id='${serviceOrder.id}';
	if(SOcontract.length > 0){
		var conf = 'Continue with CustomerFile Contract: '+SOcontract;
		if(confirm(conf)){
			var url="createPrice.html?ajax=1&tempId="+encodeURI(id)+"&serviceOrderId=${serviceOrder.id}&contractName=" + SOcontract+"&shipNumber=${serviceOrder.shipNumber}";
			document.forms['serviceOrderForm'].action =url;
			document.forms['serviceOrderForm'].submit();
		}
	}else{
		alert('CustomerFile Contract not Found');
	}}
--%>
</script>
<script type="text/javascript">
var httpTemplate = getHTTPObject();
function getDayTemplate(){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource Template.');
	}else {
		showOrHide(1);
		var url ="resouceTemplateQuotation.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}
}
function handleHttpResponseForTemplate(){
	if (httpTemplate.readyState == 4){
        var results= httpTemplate.responseText;
        findAllResource();
	}
}
function findAllResource(){
	var resourceDiv = document.getElementById("resourcMapAjax");
    showOrHide(1);
	var url="findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
	httpResource.open("POST", url, true);
	httpResource.onreadystatechange = handleHttpResponseForResources;
	httpResource.send(null);
}
function addResource(day){
	var catTempID = "catTempID"+day;
	var catTempVal = document.getElementById(catTempID).value;
	var id = '${serviceOrder.id}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource.');
	}else if(catTempVal.trim() == ''){
		alert("Select any Category for Day "+day);
		document.getElementById(catTempID).focus();
	}else{
		showOrHide(1);
		var categoryType='';
		if(catTempVal=='C'){categoryType='Crew'+day}else if(catTempVal=='M'){categoryType='Material'+day}else if(catTempVal=='E'){categoryType='Equipment'+day}else{categoryType='Vehicle'+day}
		catagoryName=categoryType;
		dayId=day;
		var url = "addRowToResourceTemplateGridAjax.html?ajax=1&tempId="+id+"&shipNumber=${serviceOrder.shipNumber}&day="+day+"&type="+catTempVal+"&categoryType="+categoryType;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	}
}
<%--
function getDayTemplate1(){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	if(id.length == 0){
		alert('Please Save Service Order first before adding Resource Template.');
	}else {
		var url ="resouceTemplateSO.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
	    document.forms['serviceOrderForm'].action =url;
		document.forms['serviceOrderForm'].submit();
	}}
--%>
function dateAutoSave(dt,day){
	var beginDt="";
	var endDt
	if(document.getElementById('beginDt'+day)!=null){
	 beginDt = document.getElementById('beginDt'+day).value;
	}
	if(document.getElementById('endDt'+day)!=null){
	 endDt = document.getElementById('endDt'+day).value;
	}
	var daysDiff = getDateCompare(beginDt, endDt);
	if(daysDiff<0) {
		   alert("From date cannot be greater than To date");
		   document.getElementById('endDt'+day).value='';
		   return false;
	}else{
		var begin = convertDateFormat('beginDt'+day);
		var end = convertDateFormat('endDt'+day);
		if(begin.length > 0 && end.length > 0 ){
			if(true){
				var url = "autoSaveDateAjax.html?ajax=1&decorator=simple&popup=true&shipNum=${serviceOrder.shipNumber}&beginDt="+begin+"&endDt="+end+"&day="+day+"&btnType=addrow";
				http17.open("GET", url, true);
			    http17.onreadystatechange = handleHttpResponse17;
			    http17.send(null);
			}
		}
	}
}
/* function convertDateFormat(fieldName,day){
	var varDate = document.getElementById(fieldName).value;
	if(varDate.length > 0){
		var mySplitResult = varDate.split("-");
	   	var day = mySplitResult[0];
	   	var month = mySplitResult[1];
	   	var year = '20'+mySplitResult[2];
	 	if(month == 'Jan'){ month = "01";
	   	}else if(month == 'Feb'){ month = "02";
	   	}else if(month == 'Mar'){ month = "03"
	   	}else if(month == 'Apr'){ month = "04"
	   	}else if(month == 'May'){ month = "05"
	   	}else if(month == 'Jun'){ month = "06"
	   	}else if(month == 'Jul'){ month = "07"
	   	}else if(month == 'Aug'){ month = "08"
	   	}else if(month == 'Sep'){ month = "09"
	   	}else if(month == 'Oct'){ month = "10"
	   	}else if(month == 'Nov'){ month = "11"
	   	}else if(month == 'Dec'){ month = "12";
	   	}
		varDate = year+'-'+month+'-'+day+' 00:00:00';
	   	return varDate;
	}else {
		return '';
	}} */
</script>
<script type="text/javascript">
var varName = '';
var dayNum='';
//setTimeout("setDateInitialization()",2000);
function setDateInitialization(){
		var imagelist = document.getElementsByTagName('img');
		for(var i = 0; i < imagelist.length; i++){
			if(imagelist[i].id.indexOf('Dt') > -1){
				new Calendar({
				    inputField: imagelist[i].id.split('-')[0] ,
				    dateFormat: "%d-%b-%y",
				    trigger: imagelist[i].id,
				    bottomBar: true,
				    animation:true,
				    onSelect: function() {
				    	this.hide();
				    	dateAutoSave('',dayNum);
					}
				});
			}
		}
}
function checkBillingCompleteStatus(elementName,day){
		dayNum = day;
		if(elementName.indexOf('-') > 0){
			varName = elementName.split('-')[0];
		}else{
			varName = elementName;
		}
		return varName;
}
	function hitBilling(){
			dateAutoSave('',dayNum);
			varName='';
			dayNum='';
	}
</script>   
<script type="text/javascript">
try{
	document.getElementById('serviceOrderForm').setAttribute("AutoComplete","off");
}catch(e){
}
</script>

<script type="text/JavaScript">
function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 

function showSuccessMsg(){
	document.getElementById("successMsg").style.display='block';
	if(document.getElementById("successMessages")!= null){
		document.getElementById("successMessages").style.display='none';
	}
	window.location.hash = '#header';
}
</script>
<script type="text/JavaScript">
<c:if test="${not empty dayRowFocus}">
try{
	$('descript${dayRowFocus}').focus();
	$$('descript${dayRowFocus}').scrollTo();
}catch(e){}
</c:if>
</script>
<script type="text/JavaScript">
try{
	var disableChk = '${disableChk}';
	 if(disableChk == 'N') {		
		document.getElementById('weightnVolumeRadio').disabled = true;	
		document.getElementById('weightnVolumeRadio2').disabled = true;	  		
	 }
}catch(e){}
</script>

<script type="text/javascript">
var catagoryName='';
var dayId='';
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
    	if(e.style.display == 'block')
    	   e.style.display = 'none';
    	else
    	   e.style.display = 'block';
    }
}
function setAnimatedColFocus(id){
		toggle_visibility("DayTable"+id);
		<c:if test="${serviceOrder.controlFlag != 'Q'}">
			//document.getElementById('date'+id).style.display='block';
		</c:if>
}
function setAnimatedColCatFocus(id,day){
		toggle_visibility(id);
}
</script>
<script type="text/JavaScript">
var httpResourceMap = getHTTPObject();
function getAllResourcMap(){
	var url="findAllResourceDateAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
	httpResourceMap.open("POST", url, true);
	httpResourceMap.onreadystatechange = handleHttpResponseForResourcesMap;
	httpResourceMap.send(null);
}
	
	function handleHttpResponseForResourcesMap(){
		if (httpResourceMap.readyState == 4){
            var results= httpResourceMap.responseText;
            dateStr=results.trim();
            setTimeout("setResourceDate()", 1000);
	 	}
	}
	function date_convert1(date){
		// date format should be 2012-09-03 00:00:00
		var intVal = date.split(' ')[0];
	   	var intval1 = intVal.split('-');
	   	
		var month=intval1[1]
		if(month == 01)month="Jan";
		if(month == 02)month="Feb";
		if(month == 03)month="Mar";
		if(month == 04)month="Apr";
		if(month == 05)month="May";
		if(month == 06)month="Jun";
		if(month == 07)month="Jul";
		if(month == 08)month="Aug";
		if(month == 09)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
	   	
		var datam = intval1[2]+"-"+month+"-"+intval1[0].substring(2,4);
		return datam;
	}
	var dateStr='';
	function setResourceDate(){
		if(dateStr.length > 0){
		   var mySplitResult = dateStr.split("_");
           for(var i = 0; i < mySplitResult.length; i++){
	           	var split1 = mySplitResult[i];
	           	var dayNdate = split1.split("*@");
	           	var bDate=dayNdate[1].trim();
	           	var eDate=dayNdate[2].trim();
	           	var bDateId='beginDt'+dayNdate[0].trim();
	           	var eDateId='endDt'+dayNdate[0].trim();
	           	
	           	document.getElementById(bDateId).value=date_convert1(bDate);
	           	document.getElementById(eDateId).value=date_convert1(eDate);
           }
		}
	}
	function volumeDisplay(){
		var weightVolSectionId = document.getElementById('weightVolMain');
		var fieldVal="0";
		<configByCorp:fieldVisibility componentId="component.field.weightVolume.Visibility">
		fieldVal="1";
		</configByCorp:fieldVisibility>
		if(weightVolSectionId != null && weightVolSectionId != undefined){
			if(fieldVal=="0"){
				document.getElementById('weightVolMain').style.display = 'none';
			}
		}
	}
</script>
<script type="text/JavaScript">
var httpResource = getHTTPObject();
<%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
	setTimeout("volumeDisplay()", 1500);
//	setTimeout("findAllResource()", 2000);
<%--
	var url="findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
		httpResource.open("POST", url, true);
		httpResource.onreadystatechange = handleHttpResponseForResources;
		httpResource.send(null);
--%>
	</configByCorp:fieldVisibility>
</c:if>
	
	function handleHttpResponseForResources(){
		if (httpResource.readyState == 3){
			volumeDisplay();
		}else if (httpResource.readyState == 4){
			showOrHide(0);
            var results= httpResource.responseText; 
	 	    var resourceDiv = document.getElementById("resourcMapAjax");
	 	    resourceDiv.innerHTML = results ;
	 	   	if(catagoryName !='' && dayId !=''){
	 	   		toggle_visibility("DayTable"+dayId);
	 			toggle_visibility("cat_"+catagoryName);
				catagoryName='';
	 			dayId='';
	 	   	}
	 	   setDateInitialization();
	 	   getAllResourcMap();
	 	}
	}
	
	var isLoadRes='';
	function loadResources(){
		if(isLoadRes ==''){
			findAllResource();
			isLoadRes='YES';
		}
	}
</script>
<script type="text/JavaScript">
function invoiceDetailsMethod(aid){
	  var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
	  var vendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
	  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
	  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
	  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
	  var payingStatus=document.forms['serviceOrderForm'].elements['payingStatus'+aid].value;
	  var receivedDate=document.forms['serviceOrderForm'].elements['receivedDate'+aid].value; 
	  var invoiceNumber=document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value;
	  var invoiceDate=document.forms['serviceOrderForm'].elements['invoiceDate'+aid].value;   
	  var valueDate=document.forms['serviceOrderForm'].elements['valueDate'+aid].value;
	  var type='PAY';
	  var acref="${accountInterface}";
	  acref=acref.trim();
	  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("Vendor Code Missing. Please select");
	  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		  alert("Charge code needs to be selected");
	  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
		  alert("Pay GL missing. Please select another charge code.");
	  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
		  alert("Receivable GL missing. Please select another charge code.");
	  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	  }else{		   
	  openWindow('invoiceDetailsForm.html?ajax=1&aid='+aid+'&payingStatus='+payingStatus+'&receivedDate='+receivedDate+'&invoiceNumber='+invoiceNumber+'&invoiceDate='+invoiceDate+'&vendorCode='+vendorCode+'&valueDate='+valueDate+'&decorator=popup&popup=true',920,350);
	  }
}

function getAccountlineFieldPay(aid,basis,quantity,chargeCode,contract,vendorCode){
	  var basis=document.forms['serviceOrderForm'].elements[basis].value;
	  var recQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  var chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  var contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  var vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value; 
	  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
	  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
	  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
	  var type='PAY';
	  var acref="${accountInterface}";
	  acref=acref.trim();
	  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("Vendor Code Missing. Please select");
	  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		  alert("Charge code needs to be selected");
	  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
		  alert("Pay GL missing. Please select another charge code.");
	  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
		  alert("Receivable GL missing. Please select another charge code.");
	  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	  }else{		   
	  var strValue="";
	  strValue=basis+"~"+recQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	  var country=document.forms['serviceOrderForm'].elements['country'+aid].value; 
	  var valueDate=document.forms['serviceOrderForm'].elements['valueDate'+aid].value;
	  var exchangeRate=document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value;
	  var localAmount=document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
	  strValue=strValue+"~"+country+"~"+valueDate+"~"+exchangeRate+"~"+localAmount;
	  var actualExpense=document.forms['serviceOrderForm'].elements['actualExpense'+aid].value;
	  var actualDiscount=0.00;
	  <c:if test="${chargeDiscountFlag}">
	  actualDiscount=document.forms['serviceOrderForm'].elements['actualDiscount'+aid].value;
	  </c:if>
	  strValue=strValue+"~"+actualExpense+"~"+actualDiscount;
	  <c:if test="${contractType}">
	  var payableContractCurrency=document.forms['serviceOrderForm'].elements['payableContractCurrency'+aid].value; 
	  var payableContractValueDate=document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value;
	  var payableContractExchangeRate=document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value;
	  var payableContractRateAmmount=document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value;
	  strValue=strValue+"~"+payableContractCurrency+"~"+payableContractValueDate+"~"+payableContractExchangeRate+"~"+payableContractRateAmmount;
 	 </c:if>	  
	  openWindow('getAccountlineFieldPay.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
	  }
}
function getAccountlineFieldRec(aid,basis,quantity,chargeCode,contract,vendorCode){
	  var basis=document.forms['serviceOrderForm'].elements[basis].value;
	  var recQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  var chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  var contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  var vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value; 
	  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
	  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value; 
	  var type='REC';  
	  var acref="${accountInterface}";
	  acref=acref.trim();
	  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
	  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("Vendor Code Missing. Please select");
	  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		  alert("Charge code needs to be selected");
	  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
		  alert("Pay GL missing. Please select another charge code.");
	  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
		  alert("Receivable GL missing. Please select another charge code.");
	  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	  }else{		   
	  var strValue="";
	  strValue=basis+"~"+recQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	  var recRateCurrency=document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value; 
	  var racValueDate=document.forms['serviceOrderForm'].elements['racValueDate'+aid].value;
	  var recCurrencyRate=document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value;
	  var recRateExchange=document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value;
	  var actualRevenueForeign=document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
	  strValue=strValue+"~"+recRateCurrency+"~"+racValueDate+"~"+recCurrencyRate+"~"+recRateExchange+"~"+actualRevenueForeign;
	  var actualRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value;
	  var actualDiscount=0.00;
	  <c:if test="${chargeDiscountFlag}">
	  actualDiscount=document.forms['serviceOrderForm'].elements['actualDiscount'+aid].value;
	  </c:if>
	  var recRate=document.forms['serviceOrderForm'].elements['recRate'+aid].value;
	  strValue=strValue+"~"+actualRevenue+"~"+actualDiscount+"~"+recRate;
	  <c:if test="${contractType}">
	  var contractCurrency=document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value; 
	  var contractValueDate=document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value;
	  var contractRate=document.forms['serviceOrderForm'].elements['contractRate'+aid].value;
	  var contractExchangeRate=document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value;
	  var contractRateAmmount=document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value;
	  strValue=strValue+"~"+contractCurrency+"~"+contractValueDate+"~"+contractRate+"~"+contractExchangeRate+"~"+contractRateAmmount;
 	 </c:if>	  
	  openWindow('getAccountlineFieldRec.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
	  }
}
function getAccountlineFieldRev(aid,basis,quantity,chargeCode,contract,vendorCode){
	  var basis=document.forms['serviceOrderForm'].elements[basis].value;
	  var revisionQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  var chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  var contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  var vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;  
	  var strValue="";
	  strValue=basis+"~"+revisionQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	  var revisionCurrency=document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value; 
	  var revisionValueDate=document.forms['serviceOrderForm'].elements['revisionValueDate'+aid].value;
	  var revisionLocalRate=document.forms['serviceOrderForm'].elements['revisionLocalRate'+aid].value;
	  var revisionExchangeRate=document.forms['serviceOrderForm'].elements['revisionExchangeRate'+aid].value;
	  var revisionLocalAmount=document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value;
	  strValue=strValue+"~"+revisionCurrency+"~"+revisionValueDate+"~"+revisionLocalRate+"~"+revisionExchangeRate+"~"+revisionLocalAmount;
	  var revisionExpense=document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value;
	  var revisionDiscount=0.00;
	  <c:if test="${chargeDiscountFlag}">
	  revisionDiscount=document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value;
	  </c:if>
	  var revisionRate=document.forms['serviceOrderForm'].elements['revisionRate'+aid].value;
	  strValue=strValue+"~"+revisionExpense+"~"+revisionDiscount+"~"+revisionRate;
	  <c:if test="${contractType}">
	  var revisionPayableContractCurrency=document.forms['serviceOrderForm'].elements['revisionPayableContractCurrency'+aid].value; 
	  var revisionPayableContractValueDate=document.forms['serviceOrderForm'].elements['revisionPayableContractValueDate'+aid].value;
	  var revisionPayableContractRate=document.forms['serviceOrderForm'].elements['revisionPayableContractRate'+aid].value;
	  var revisionPayableContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionPayableContractExchangeRate'+aid].value;
	  var revisionPayableContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionPayableContractRateAmmount'+aid].value;
	  strValue=strValue+"~"+revisionPayableContractCurrency+"~"+revisionPayableContractValueDate+"~"+revisionPayableContractRate+"~"+revisionPayableContractExchangeRate+"~"+revisionPayableContractRateAmmount;
 	 </c:if>	  
	  openWindow('getAccountlineFieldRev.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);	
}
function getAccountlineFieldRevNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	  var basis=document.forms['serviceOrderForm'].elements[basis].value;
	  var revisionQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  <c:if test="${contractType}">
	  revisionQuantity=document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value;
	  </c:if>
	  var chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  var contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  var vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;  
	  var strValue="";
	  strValue=basis+"~"+revisionQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	  var revisionSellCurrency=document.forms['serviceOrderForm'].elements['revisionSellCurrency'+aid].value; 
	  var revisionSellValueDate=document.forms['serviceOrderForm'].elements['revisionSellValueDate'+aid].value;
	  var revisionSellLocalRate=document.forms['serviceOrderForm'].elements['revisionSellLocalRate'+aid].value;
	  var revisionSellExchangeRate=document.forms['serviceOrderForm'].elements['revisionSellExchangeRate'+aid].value;
	  var revisionSellLocalAmount=document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value;
	  strValue=strValue+"~"+revisionSellCurrency+"~"+revisionSellValueDate+"~"+revisionSellLocalRate+"~"+revisionSellExchangeRate+"~"+revisionSellLocalAmount;
	  var revisionRevenueAmount=document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value;
	  var revisionDiscount=0.00;
	  <c:if test="${chargeDiscountFlag}">
	  revisionDiscount=document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value;
	  </c:if>
	  var revisionSellRate=document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value;
	  strValue=strValue+"~"+revisionRevenueAmount+"~"+revisionDiscount+"~"+revisionSellRate;
	  <c:if test="${contractType}">
	  var revisionContractCurrency=document.forms['serviceOrderForm'].elements['revisionContractCurrency'+aid].value; 
	  var revisionContractValueDate=document.forms['serviceOrderForm'].elements['revisionContractValueDate'+aid].value;
	  var revisionContractRate=document.forms['serviceOrderForm'].elements['revisionContractRate'+aid].value;
	  var revisionContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionContractExchangeRate'+aid].value;
	  var revisionContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionContractRateAmmount'+aid].value;
	  strValue=strValue+"~"+revisionContractCurrency+"~"+revisionContractValueDate+"~"+revisionContractRate+"~"+revisionContractExchangeRate+"~"+revisionContractRateAmmount;
 	 </c:if>	  
	  openWindow('getAccountlineFieldRevNew.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);	
}
//Pricing Method start//
function getAccountlineFieldNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	  accountLineBasis=document.forms['serviceOrderForm'].elements[basis].value;
	  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  <c:if test="${contractType}">
	  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
	  </c:if>	  
	  chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;  
	  <c:if test="${contractType}">
		  var estimateContractCurrencyTemp=document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value; 
		  var estimateContractValueDateTemp=document.forms['serviceOrderForm'].elements['estimateContractValueDateNew'+aid].value;
		  var estimateContractExchangeRateTemp=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
		  var estimateContractRateTemp=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		  var estimateContractRateAmmountTemp=document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;
	  </c:if>	  
		  var estSellCurrencyTemp=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value; 
		  var estSellValueDateTemp=document.forms['serviceOrderForm'].elements['estSellValueDateNew'+aid].value;
		  var estSellExchangeRateTemp=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		  var estSellLocalRateTemp=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
		  var estSellLocalAmountTemp=document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value;

		  var estimateQuantityTemp=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
		  var estimateSellRateTemp=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
		  var estimateRevenueAmountTemp=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value;
		  var basisTemp=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		  var estimateSellQuantity=1;
		  <c:if test="${contractType}">
		  estimateSellQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
		  </c:if>
		  
	  <c:if test="${contractType}">
		  openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estimateContractCurrencyTemp='+estimateContractCurrencyTemp+'&estimateContractValueDateTemp='+estimateContractValueDateTemp+'&estimateContractExchangeRateTemp='+estimateContractExchangeRateTemp+'&estimateContractRateTemp='+estimateContractRateTemp+'&estimateContractRateAmmountTemp='+estimateContractRateAmmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  <c:if test="${!contractType}">
		  openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  }


 function calculateEstimateSellRate(){

	    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value;
	    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value;
        <c:if test="${contractType}">
        accEstimateQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+accountlineId].value;
        </c:if>
	    var accEstimateSellRate=document.forms['serviceOrderForm'].elements['accEstimateSellRate'].value ;
	    var accEstimateRevenueAmount=document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value ;
	    var expenseValue=0;
	    var expenseRound=0;
	    var oldExpense=0;
	    var oldExpenseSum=0;
	    var balanceExpense=0;
	    oldExpense=eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accountlineId].value);
	    <c:if test="${not empty serviceOrder.id}">
	    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	    </c:if> 
	    expenseValue=accEstimateRevenueAmount;
	    expenseRound=Math.round(expenseValue*10000)/10000;
	    balanceExpense=expenseRound-oldExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    balanceExpense=Math.round(balanceExpense*10000)/10000; 
	    oldExpenseSum=oldExpenseSum+balanceExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    var expenseRoundNew=""+expenseRound;
	    if(expenseRoundNew.indexOf(".") == -1){
	    expenseRoundNew=expenseRoundNew+".00";
	    } 
	    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
	    expenseRoundNew=expenseRoundNew+"0";
	    } 
	    document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accountlineId].value=expenseRoundNew;
	    document.forms['serviceOrderForm'].elements['estimateSellRate'+accountlineId].value=accEstimateSellRate;
	    estimateQuantity=document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value;
        <c:if test="${contractType}">
        estimateSellQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+accountlineId].value;
        </c:if>
	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
	     document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
	     }
	    <c:if test="${contractType}">
	    if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {   
		     document.forms['serviceOrderForm'].elements['estimateSellQuantity'+accountlineId].value=accEstimateQuantity;
		     }	     
	     </c:if>
	    <c:if test="${not empty serviceOrder.id}">
	    var newExpenseSum=""+oldExpenseSum;
	    if(newExpenseSum.indexOf(".") == -1){
	    newExpenseSum=newExpenseSum+".00";
	    } 
	    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
	    newExpenseSum=newExpenseSum+"0";
	    } 
	    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newExpenseSum;
	    </c:if>
	    if(expenseRoundNew>0){
	    document.forms['serviceOrderForm'].elements['displayOnQuote'+accountlineId].checked = true;
	    }
	    var revenue='estimateRevenueAmount'+accountlineId;
	    var expense='estimateExpense'+accountlineId;
	    var estimatePassPercentage='estimatePassPercentage'+accountlineId;
	    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
	    	  
 }


	  	 function calculateAccountingACToBillingCurrency(accLineList,currency,extRate,valueDate){
			if(accLineList.trim()!=""){
		  	 var str=accLineList.split(",");
		  	 for(i=0;i<str.length;i++){
		  		var oldRevenue=0;
			    var oldRevenueSum=0;
			    var balanceRevenue=0;
				var aid=str[i];
				oldRevenue=eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value); 
		  		document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value=currency;
		  		document.forms['serviceOrderForm'].elements['estSellValueDateNew'+aid].value=valueDate;
		  		document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value=extRate;
		  		<c:if test="${contractType}">
		  		var extSellRateBelow=0;
		  		var extEstimateContractRateEXAbove=0;
		  		var extEstimateContractRateAbove=0;
		  		var estSellLocalRate1=0;
		  		var roundValue=0;
		  		var estimateQuantity1=0;
		  		extSellRateBelow=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		  		extEstimateContractRateEXAbove=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
		  		extEstimateContractRateAbove=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		  		try{
				  	if((extEstimateContractRateAbove!='')&&(extEstimateContractRateAbove!='0.00')&&(extEstimateContractRateAbove!='0')&&(extEstimateContractRateAbove!='0.0')){
				  		estSellLocalRate1=(extSellRateBelow*extEstimateContractRateAbove)/extEstimateContractRateEXAbove; 
				        roundValue=Math.round(estSellLocalRate1*10000)/10000;
					    document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue;
				    }else{
		  		        var recRateExcha1=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
		  		        var recRat1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value
		  		        var recCurrencyR1=recRat1*recRateExcha1;
		  		        var roundValue4=Math.round(recCurrencyR1*10000)/10000;
		  		        document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue4 ; 
				    }
		  		}catch(e){
			  		}
		  		
		  		try{
		  			estSellLocalRate1=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
			  		estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
					  <c:if test="${contractType}">
					  estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
					  </c:if>
			  	  	var basis1 =document.forms['serviceOrderForm'].elements['basis'+aid].value; 
			        var estimateSellLocalAmountRound=0;
			        
	    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	   	    	estimateQuantity1 =document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value=1;
						  <c:if test="${contractType}">
						  estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
						  </c:if>
	    	            } 
	    	        if(basis1=="cwt" || basis1=="%age"){
	    	        	estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
	    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;	  	
	    	          	}  else if(basis1=="per 1000"){
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
	    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000; 
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
	    	     	    }
	    	     	    var estimateDeviation1="0";
	    	     	    try{
	    	           estimateDeviation1=document.forms['serviceOrderForm'].elements['estimateSellDeviation'+aid].value;
	    	           }catch(e){}
	    	           if(estimateDeviation1!='' && estimateDeviation1>0){
	    	          	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
	    	          	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*10000)/10000;
	    	             document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = estimateSellLocalAmountRound;	 
	    	         } 
		  		}catch(e){
		  			document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = '0.00';
				  		}
		  		</c:if>
		  		<c:if test="${!contractType}">
		  		var estSellLocalRate1=0;
						extSellRateBelow=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
	  				estSellLocalRate1=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
	  			var estimateQuantity1=0;
		    	   var roundValue=0; 
		    	   try{
		    	  	 if(extSellRateBelow==0) {
		    	         document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value=0*1;
		    	     } else{
		    	       var amount1=estSellLocalRate1/extSellRateBelow; 
		    	       roundValue=Math.round(amount1*10000)/10000;
		    	       document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value=roundValue ; 
		    	     }}catch(e){
		    	    	 document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value='0.00' ;
			    	     }
		    	     var estSelRate=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
		    	     estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
		    	     var basis1 =document.forms['serviceOrderForm'].elements['basis'+aid].value;
			    	   try{
				    	      var amount1=estSelRate*estimateQuantity1;
				    	        if(basis1=="cwt" || basis1=="%age"){
				    	        	amount1=(amount1)/100;
				    	          	}  else if(basis1=="per 1000"){
				    	          		amount1=(amount1)/1000;
				    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
				    	          		amount1=amount1;
				    	     	    }
				    	       roundValue=Math.round(amount1*10000)/10000;
				    	       document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
				    	   }catch(e){
				    	    	 document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value='0.00' ;
					    	     }
				    	try{
					    	  var estimateRevenue=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value;
					    	  var estimate=document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value;
					    	  if(estimate==null || estimate== undefined || estimate.trim()=='' || parseInt(estimate.trim())==0){
					    		  document.forms['serviceOrderForm'].elements['estimatePassPercentage'+aid].value='0';
					    	  }else{
					    		  var estimatepasspercentage1 = (estimateRevenue/estimate)*100;
					    		  roundValue=Math.round(estimatepasspercentage1);
					    		  document.forms['serviceOrderForm'].elements['estimatePassPercentage'+aid].value=roundValue;
					    	  }
				    	}catch(e){
				    		document.forms['serviceOrderForm'].elements['estimatePassPercentage'+aid].value='0';
				    		 	}
		  		</c:if>

		  		
			    <c:if test="${not empty serviceOrder.id}">
			    oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue${serviceOrder.id}'].value);
			    
			    
			    var revenueValue=0;
			    revenueValue=eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value); 
                var revenueRound=0;
			    revenueRound=Math.round(revenueValue*10000)/10000;
			    balanceRevenue=revenueRound-oldRevenue;
			    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
			    oldRevenueSum=oldRevenueSum+balanceRevenue; 
			    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
		         
			    var newRevenueSum=""+oldRevenueSum; 
			    if(newRevenueSum.indexOf(".") == -1){
			    newRevenueSum=newRevenueSum+".00";
			    } 
			    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			    newRevenueSum=newRevenueSum+"0";
			    }  
			    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	              var revenueValuesum=0;
	              var expenseValuesum=0;
	              var grossMarginValuesum=0; 
	              var grossMarginPercentageValuesum=0; 
	              revenueValuesum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	              expenseValuesum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
	              grossMarginValuesum=revenueValuesum-expenseValuesum;
	              grossMarginValuesum=Math.round(grossMarginValuesum*10000)/10000; 
	              document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValuesum; 
	              if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0 || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
	            	  document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
	            	   }else{
	            		   grossMarginPercentageValuesum=(grossMarginValuesum*100)/revenueValuesum; 
	            		   grossMarginPercentageValuesum=Math.round(grossMarginPercentageValuesum*10000)/10000; 
	            		   document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValuesum;
	            	   }
	            </c:if>  
			    try{
			    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
			    <c:if test="${!contractType}">
			    calculateVatAmt('vatPers'+aid,'estimateRevenueAmount'+aid,'vatAmt'+aid);
			    </c:if>
			    <c:if test="${contractType}">
			    calculateVatAmt('vatPers'+aid,'estSellLocalAmountNew'+aid,'vatAmt'+aid);
			    </c:if>
			    </c:if>
			    }catch(e){}
		  	}
			}
	  	 } 

function calculateVatAmtEstTemp(vatPer,vatAmt,aid){
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
	  if(vatExclude==null || vatExclude==false || vatExclude=='false'){		 
		  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
		
		var estVatAmt=0;
		if(document.forms['serviceOrderForm'].elements[vatPer]!=undefined && document.forms['serviceOrderForm'].elements[vatPer].value!=''){
		var estVatPercent=eval(document.forms['serviceOrderForm'].elements[vatPer].value);
		var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value);
		<c:if test="${contractType}">
		 estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstSellLocalAmount'].value);
		</c:if>
		estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
		estVatAmt=Math.round(estVatAmt*10000)/10000; 
		document.forms['serviceOrderForm'].elements[vatAmt].value=estVatAmt;
		}
		
	  }else{		  
	 	  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";	  
		  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;		  
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
		  </c:if>
	  }
	  </c:if>
}
function calculateVatAmtEstExpTemp(vatPer,vatAmt,aid){
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
	  if(vatExclude==null || vatExclude==false || vatExclude=='false'){		 
		  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
		
		var estVatAmt=0;
		if(document.forms['serviceOrderForm'].elements[vatPer]!=undefined && document.forms['serviceOrderForm'].elements[vatPer].value!=''){
		var estVatPercent=eval(document.forms['serviceOrderForm'].elements[vatPer].value);
		var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstimateExpense'].value);
		<c:if test="${contractType}">
		 estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value);
		</c:if>
		estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
		
		estVatAmt=Math.round(estVatAmt*10000)/10000; 
		document.forms['serviceOrderForm'].elements[vatAmt].value=estVatAmt;
		}
		
	  }else{
		  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";	  	  		  
		  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;	
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
		  </c:if>
	  }
	  </c:if>
}

function calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
	  		  var estimateQuantity1 =0;
	  		    var estimateDeviation1=100;
	  		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	  		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	  		    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value); 
	  	        var estimateRate1=0;
	  		    var estimateRateRound=0; 
	  		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	  	       estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	  	       } 
	  	      if( basis1=="cwt" || basis1=="%age"){
	  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;	  	
	  	     	}  else if(basis1=="per 1000"){
	  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		  	
	  	     	} else  {
	  	        estimateRate1=(estimateExpense1/estimateQuantity1); 
	  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
	  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		  	
	  		    }
	  		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
	  		    if(estimateDeviation1!='' && estimateDeviation1>0){
	  		    estimateRate1=estimateRateRound*100/estimateDeviation1;
	  		    estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  		  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		
	  		    } 
	  	 }

	  	function  calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
	  		
	 	   <c:if test="${contractType}">
	 	        var country1 =document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value; 
	 	        if(country1=='') {
	 	          alert("Please select Contract Currency ");
	 	          document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value=0;
	 	          document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value=1;
	 	        } else if(country1!=''){
	 	        var estimateContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value
	 	        var estimateContractRate1=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value
	 	        estimateContractRate1=Math.round(estimateContractRate1*10000)/10000;
	 	        var estimateSellRate1=estimateContractRate1/estimateContractExchangeRate1;
	 	        var roundValue=Math.round(estimateSellRate1*10000)/10000;
	 	        document.forms['serviceOrderForm'].elements[estimateSellRate].value=roundValue ; 
	 	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	 	        calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	 	        }   
	 	   </c:if>
	 	   }

	  	 function  calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
	  	   <c:if test="${contractType}">
	  	        var country1 =document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
	  	        if(country1=='') {
	  	          alert("Please select Contract Currency ");
	  	          document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=0;
	  	          document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=1;
	  	        } else if(country1!=''){
	  	        var estimatePayableContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
	  	        var estimatePayableContractRate1=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value
	  	        estimatePayableContractRate1=Math.round(estimatePayableContractRate1*10000)/10000;
	  	        var estimateRate1=estimatePayableContractRate1/estimatePayableContractExchangeRate1;
	  	        var roundValue=Math.round(estimateRate1*10000)/10000;
	  	        document.forms['serviceOrderForm'].elements[estimateRate].value=roundValue ;  
	  	        calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  	        //calRecCurrencyRate();
	  	        calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  	        }   
	  	   </c:if>
	  	   } 
	  	function calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	  		<c:if test="${multiCurrency=='Y'}">
	  		          var country1 =document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value; 
	  		          if(country1=='') {
	  		          alert("Please select billing currency "); 
	  		          document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=0; 
	  		        } else if(country1!=''){  
	  		        var recRateExchange1=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
	  		        var recRate1=document.forms['serviceOrderForm'].elements[estimateSellRate].value
	  		        var recCurrencyRate1=recRate1*recRateExchange1;
	  		        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
	  		        document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue ; 
	  		        calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		        } 
	  	     </c:if>		        
	  		  }

	  	function calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {

	 	   <c:if test="${contractType}">
	 	   var estimate=0;
	 	   var estimaternd=0;
	 	   var receivableSellDeviation=0;
	 	       var quantity = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
	 	       var rate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
	 	   	   if(quantity<999999999999&& rate<999999999999) {
	 	  	 	estimate = (quantity*rate);
	 	  	 	estimaternd=Math.round(estimate*10000)/10000;
	 	  	 	} else { 
	 	  	 	 estimate=0;
	 	  	 	 estimaternd=Math.round(estimate*10000)/10000;
	 	  	 	} 
	 	         if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age") {
	 	      	    estimate = estimate/100; 
	 	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	 	      	    document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd; 
	 	      	      }
	 	          else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
	 	      	    estimate = estimate/1000; 
	 	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	 	      	    document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd; 
	 	      	    } else{
	 	            document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd;
	 	              } 
	 	           estimateDeviation=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
	 	             if(estimateDeviation!='' && estimateDeviation>0){
	 	               estimaternd=estimaternd*estimateDeviation/100;
	 	               estimaternd=Math.round(estimaternd*10000)/10000;
	 	               document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd;	 
	 	           }
	 	             calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)  
	 	   </c:if>
	 	  		}

	    function calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	    	   var estimateQuantity1 =0;
	    	   var estLocalRate1 =0;
	    	   	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	    	   	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	    	   	    estSellLocalRate1 =document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;  
	    	   	    var estimateSellLocalAmount1=0;
	    	   	    var estimateSellLocalAmountRound=0; 
	    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	            estimateQuantity =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	    	            } 
	    	           if( basis1=="cwt" || basis1=="%age"){
	    	        	   estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
	    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;	  	
	    	          	}  else if(basis1=="per 1000"){
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
	    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000; 
	    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
	    	   	    }
	    	           estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
	  	             if(estimateDeviation1!='' && estimateDeviation1>0){
	  	            	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
	  	            	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*10000)/10000;
	  	               document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = estimateSellLocalAmountRound;	 
	  	           }  
	  	    	   var contractRate=0;
	 			  <c:if test="${contractType}">
	         contractRate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
	         </c:if>
	       if(contractRate==0){
	    	           calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	       }
	    	   }	

	function calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
		  	 
	 	   var estSellLocalAmount1= document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value ;
	 	   var estSellExchangeRate1= document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value*1 ;
	 	   var roundValue=0; 
	 	  	 if(estSellExchangeRate1 ==0) {
	 	         document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=0*1;
	 	     } else if(estSellExchangeRate1 >0) {
	 	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
	 	       roundValue=Math.round(amount1*10000)/10000;
	 	       document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=roundValue ; 
	 	     }
	 	     var estimatepasspercentageRound=0;
	 	     var estimatepasspercentage1 = 0;
	 	     var estimateExpense1 = document.forms['serviceOrderForm'].elements[estimateExpense].value; 
	 	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
	 	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	 	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
	 	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	 	  	   }else{
	 	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	 	  	   }  
	 } 	 

function  calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	    	   var contractRate=0;
				  <c:if test="${contractType}">
	        contractRate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
	        </c:if>
	      if(contractRate==0){
	    	   var estSellLocalAmount1= document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value ;
	    	   var estSellExchangeRate1= document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value*1 ;
	    	   var roundValue=0; 
	    	  	 if(estSellExchangeRate1 ==0) {
	    	         document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=0*1;
	    	     } else if(estSellExchangeRate1 >0) {
	    	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
	    	       roundValue=Math.round(amount1*10000)/10000;
	    	       document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=roundValue ; 
	    	     }
	    	     var estimatepasspercentageRound=0;
	    	     var estimatepasspercentage1 = 0;
	    	     var estimateExpense1 = document.forms['serviceOrderForm'].elements[estimateExpense].value; 
	    	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
	    	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	    	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
	    	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	    	  	   }else{
	    	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	    	  	   } 
	    	  	 calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
	    	  	calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	            }else{
	       	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
	             }
	  	  	
	           } 

 function calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	      	    var estimateQuantity1 =0;
	      	    var estimateDeviation1=100;
	      	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	      	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	      	    var estimateRevenueAmount1 =eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value); 
	      	    var estimateSellRate1=0;
	      	    var estimateSellRateRound=0; 
	      	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	               estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	               } 
	              if( basis1=="cwt" || basis1=="%age"){
	           	   estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
	           	   estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;	  	
	             	}  else if(basis1=="per 1000"){
	             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
	             		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
	             	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
	             		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000; 
	             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
	      	    }
	              estimateSellDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value 
	      	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
	      	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation1;
	      	    	estimateSellRateRound=Math.round(estimateSellRateRound*10000)/10000;
	      	    document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		
	      	    }  }	  


function calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	    	    var estimateQuantity1 =0;
	    	    var estimateRevenueAmount1 =0;
	    		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	    		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	    		    estimateRevenueAmount1 =document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value; 
	    		    var estimateSellRate1=0;
	    		    var estimateSellRateRound=0; 
	    		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	    	         } 
	    	        if( basis1=="cwt" || basis1=="%age"){
	    	        	estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
	    	        	estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;	  	
	    	       	}  else if(basis1=="per 1000"){
	    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
	    	       		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
	    	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
	    	       		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000; 
	    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
	    		    }
	    	        estimateSellDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value 
	    	   	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
	    	   	    	estimateSellRate1=estimateSellRateRound*100/estimateSellDeviation1;
	    	   	    	estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	   	    document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=estimateSellRateRound;		
	    	   	    }  
	    	 }	

	 function calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	  	   
	          var country1 =document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value; 
	          if(country1=='') {
	          alert("Please select currency "); 
	          document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=0; 
	        } else if(country1!=''){  
	        var recRateExchange1=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value
	        var recRate1=document.forms['serviceOrderForm'].elements[estimateRate].value
	        var recCurrencyRate1=recRate1*recRateExchange1;
	        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
	        document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=roundValue ; 
	         calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	        } 
	  }		  

function calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) { 
	  	   <c:if test="${contractType}">
	  	   var estimate1=0;
	  	   var estimaternd=0;
	  	   var receivableSellDeviation1=0;
	  	       var quantity1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
	  	       var rate1 = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	  	   	   if(quantity1<999999999999&& rate1<999999999999) {
	  	  	 	estimate1 = (quantity1*rate1);
	  	  	 	estimaternd=Math.round(estimate1*10000)/10000;
	  	  	 	} else { 
	  	  	 	 estimate1=0;
	  	  	 	 estimaternd=Math.round(estimate1*10000)/10000;
	  	  	 	} 
	  	         if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age") {
	  	      	    estimate1 = estimate1/100; 
	  	      	    estimaternd=Math.round(estimate1*10000)/10000;	 
	  	      	    document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd; 
	  	      	      }
	  	          else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
	  	      	    estimate1 = estimate1/1000; 
	  	      	    estimaternd=Math.round(estimate1*10000)/10000;	 
	  	      	    document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd; 
	  	      	    } else{
	  	            document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd;
	  	              } 
	  	         estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value;
	  	             if(estimateDeviation1!='' && estimateDeviation1>0){
	  	               estimaternd=estimaternd*estimateDeviation1/100;
	  	               estimaternd=Math.round(estimaternd*10000)/10000;
	  	               document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd;	 
	  	           }
	  	             calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);   
	  	   </c:if>
	  	  		}	

function calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	  		var estimateQuantity1 =0;
	  		var estLocalRate1 =0;
	  			    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	  			    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	  			    estLocalRate1 =document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;  
	  			    var estimateLocalAmount1=0;
	  			    var estimateLocalAmountRound=0; 
	  			    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	  		         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	  		         } 
	  		        if( basis1=="cwt" || basis1=="%age"){
	  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/100;
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000;
	  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;	  	
	  		       	}  else if(basis1=="per 1000"){
	  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/1000;
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000;
	  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;		  	
	  		       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	  		          estimateLocalAmount1=(estLocalRate1*estimateQuantity1); 
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000; 
	  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;		  	
	  			    }
	  		        estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value;
	  		        if(estimateDeviation1!='' && estimateDeviation1>0){
	  		        	estimateLocalAmountRound=estimateLocalAmountRound*estimateDeviation1/100;
	  		        	estimateLocalAmountRound=Math.round(estimateLocalAmountRound*10000)/10000;
	  		          document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value = estimateLocalAmountRound;	 
	  		      }
	  		        var contractRate=0;
	  		  	  <c:if test="${contractType}">
	  		        contractRate = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	  		        </c:if>  
	  		        if(contractRate==0){   
	  			    calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		        }
	  		}	

function calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	  	  var contractRate=0;
	  	  <c:if test="${contractType}">
	        contractRate = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	        </c:if> 
	        if(contractRate==0){  
	     var estLocalAmount1= document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value ;
	     var estExchangeRate1= document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value*1 ;
	     var roundValue=0; 
	    	 if(estExchangeRate1 ==0) {
	           document.forms['serviceOrderForm'].elements[estimateExpense].value=0*1;
	       } else if(estExchangeRate1 >0) {
	         var amount1=estLocalAmount1/estExchangeRate1; 
	         roundValue=Math.round(amount1*10000)/10000;
	         document.forms['serviceOrderForm'].elements[estimateExpense].value=roundValue ; 
	       }
	       var estimatepasspercentageRound=0;
	       var estimatepasspercentage1 = 0;
	       var estimateRevenue1 = document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
	       estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
	    	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	    	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
	    	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	    	   }else{
	    	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	    	   } 
	       calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	       calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	        }else{
	      	  calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);    
	        }
	    } 

function calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
		    var estimateQuantity1 =0;
		    var estimateDeviation1=100;
		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements[estimateExpense].value); 
		    var estimateRate1=0;
		    var estimateRateRound=0; 
		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	         } 
	        if( basis1=="cwt" || basis1=="%age"){
	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	       	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;	  	
	       	}  else if(basis1=="per 1000"){
	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	       	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		  	
	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	          estimateRate1=(estimateExpense1/estimateQuantity1); 
	       	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		  	
		    }
		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
		    if(estimateDeviation1!='' && estimateDeviation1>0){
		    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
		    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
		    document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		
		    }  }

function calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
	        var estimateQuantity1 =0;
	        var estimateExpense1 =0;
	    	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	    	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	    	    estimateExpense1 =document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value; 
	    	    var estimateRate1=0;
	    	    var estimateRateRound=0; 
	    	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	             estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	             } 
	            if( basis1=="cwt" || basis1=="%age"){
	           	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	           	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;	  	
	           	}  else if(basis1=="per 1000"){
	           	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	           	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		  	
	           	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	              estimateRate1=(estimateExpense1/estimateQuantity1); 
	           	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
	           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		  	
	    	    }  
	            estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
	    	    if(estimateDeviation1!='' && estimateDeviation1>0){
	    	    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
	    	    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
	    	    document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		
	    	    }
	     }

function calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){ 
	  	   var estLocalAmount1= document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value ;
	  	   var estExchangeRate1= document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value*1 ;
	  	   var roundValue=0; 
	  	  	 if(estExchangeRate1 ==0) {
	  	         document.forms['serviceOrderForm'].elements[estimateExpense].value=0*1;
	  	     } else if(estExchangeRate1 >0) {
	  	       var amount1=estLocalAmount1/estExchangeRate1; 
	  	       roundValue=Math.round(amount1*10000)/10000;
	  	       document.forms['serviceOrderForm'].elements[estimateExpense].value=roundValue ; 
	  	     }
	  	     var estimatepasspercentageRound=0;
	  	     var estimatepasspercentage1 = 0;
	  	     var estimateRevenue1 = document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
	  	     estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
	  	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	  	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
	  	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	  	  	   }else{
	  	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	  	  	   }  
	  		 }
function BillingCurrencyFromCharge() {
	var aid=accountLineIdScript;  
   <configByCorp:fieldVisibility componentId="component.accountLine.BillingCurrencyFromCharge">
  	try{
  	if(document.forms['serviceOrderForm'].elements['tenthDescription'].value !='' && document.forms['serviceOrderForm'].elements['tenthDescription'].value!=undefined && document.forms['serviceOrderForm'].elements['tenthDescription'].value!="undefined"){
		document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value=document.forms['serviceOrderForm'].elements['tenthDescription'].value;
		document.forms['serviceOrderForm'].elements['revisionSellCurrency'+aid].value=document.forms['serviceOrderForm'].elements['tenthDescription'].value;
		document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value=document.forms['serviceOrderForm'].elements['tenthDescription'].value;
	}
  	 }catch(e){}
  	try{
	if(document.forms['serviceOrderForm'].elements['fourthDescription'].value !='' && document.forms['serviceOrderForm'].elements['fourthDescription'].value!=undefined &&  document.forms['serviceOrderForm'].elements['fourthDescription'].value!="undefined"){
		document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value=document.forms['serviceOrderForm'].elements['fourthDescription'].value;
		document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value=document.forms['serviceOrderForm'].elements['fourthDescription'].value;
		document.forms['serviceOrderForm'].elements['country'+aid].value=document.forms['serviceOrderForm'].elements['fourthDescription'].value;
	}
  	}catch(e){}
	</configByCorp:fieldVisibility>
}
	function checkChargesDataFromSO() {
	  	var aid=accountLineIdScript;  
	  	<c:if test="${contractType}"> 
  		var contractCurrency=document.forms['serviceOrderForm'].elements['contractCurrencyNew'+aid].value;		  		
  		if(contractCurrency.trim()!="")
  		{ 
  			document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value=contractCurrency.trim();
  			document.forms['serviceOrderForm'].elements['revisionContractCurrency'+aid].value=contractCurrency.trim();
  			document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value=contractCurrency.trim();
	  		 var rec='1';
   			<c:forEach var="entry" items="${currencyExchangeRate}">
   				if('${entry.key}'==contractCurrency.trim()){
					rec='${entry.value}';
   				}
			</c:forEach>
			document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value=rec;
  			document.forms['serviceOrderForm'].elements['revisionContractExchangeRate'+aid].value=rec;
  			document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value=rec;
			var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4);
			  document.forms['serviceOrderForm'].elements['estimateContractValueDateNew'+aid].value=datam;
	  		  document.forms['serviceOrderForm'].elements['revisionContractValueDate'+aid].value=datam;
	  		  document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value=datam;
  		}
  		var payableContractCurrency=document.forms['serviceOrderForm'].elements['payableContractCurrencyNew'+aid].value;
  		if(payableContractCurrency.trim()!="")
  		{ 
  			document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value=payableContractCurrency.trim();
  			document.forms['serviceOrderForm'].elements['revisionPayableContractCurrency'+aid].value=payableContractCurrency.trim();
  			document.forms['serviceOrderForm'].elements['payableContractCurrency'+aid].value=payableContractCurrency.trim();
	  		 var rec='1';
   			<c:forEach var="entry" items="${currencyExchangeRate}">
   				if('${entry.key}'==payableContractCurrency.trim()){
					rec='${entry.value}';
   				}
			</c:forEach>
  			document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=rec;
  			document.forms['serviceOrderForm'].elements['revisionPayableContractExchangeRate'+aid].value=rec;
  			document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value=rec;
			
			var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4);
	  		  document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value=datam;
	  		  document.forms['serviceOrderForm'].elements['revisionPayableContractValueDate'+aid].value=datam;
	  		  document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value=datam;
  		}
		  calculateEstimateByChargeCodeBlock(aid,'BAS');
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  calculateRevisionByChargeCodeBlock(aid,'BAS');
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">		  
		  calculateActualByChargeCodeBlock(aid,'','BAS');
		  </c:if>  		  		
  		</c:if>		
  	}

  	function fillCurrencyByChargeCode(aid) {
	  	if(aid=='temp'){
	  		aid=accountLineIdScript;
	  		checkAccountInterface('vendorCode'+aid,'estimateVendorName'+aid,'actgCode'+aid);
	  	}
	  	 fillPayVatByVendorCode(aid);			  		  	
  		<c:if test="${contractType}"> 
  	    var vendorCode = document.forms['serviceOrderForm'].elements['vendorCode'+aid].value;  
  	    var url="fillCurrencyByChargeCode.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorCode;  
  	     http5513.open("GET", url, true); 
  	     http5513.onreadystatechange = function(){ handleHttpResponse4113(aid);};
  	     http5513.send(null); 
  	     </c:if> 
  	} 
  	function handleHttpResponse4113(aid) { 
  	    if (http5513.readyState == 4)  {
  			       var results = http5513.responseText
  			       results = results.trim(); 
  			       if(results!=""){
  		                var res = results.split("~");
  		            if(res[0].trim()!=''){
  		            	
  		  		    document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value=res[0];                   
                    document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value=res[1];
		    		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">          
                    document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value=res[0];
                    document.forms['serviceOrderForm'].elements['revisionExchangeRate'+aid].value=res[1];
                    </c:if>
                   <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
                    document.forms['serviceOrderForm'].elements['country'+aid].value=res[0]; 
                    document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value=res[1];
                    </c:if>
                      var mydate=new Date();
  	  		          var daym;
  	  		          var year=mydate.getFullYear()
  	  		          var y=""+year;
  	  		          if (year < 1000)
  	  		          year+=1900
  	  		          var day=mydate.getDay()
  	  		          var month=mydate.getMonth()+1
  	  		          if(month == 1)month="Jan";
  	  		          if(month == 2)month="Feb";
  	  		          if(month == 3)month="Mar";
  	  				  if(month == 4)month="Apr";
  	  				  if(month == 5)month="May";
  	  				  if(month == 6)month="Jun";
  	  				  if(month == 7)month="Jul";
  	  				  if(month == 8)month="Aug";
  	  				  if(month == 9)month="Sep";
  	  				  if(month == 10)month="Oct";
  	  				  if(month == 11)month="Nov";
  	  				  if(month == 12)month="Dec";
  	  				  var daym=mydate.getDate()
  	  				  if (daym<10)
  	  				  daym="0"+daym
  	  				  var datam = daym+"-"+month+"-"+y.substring(2,4);
  		                document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value=datam;
  		              <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">         
  		                document.forms['serviceOrderForm'].elements['revisionValueDate'+aid].value=datam;
  		                </c:if>
  		              <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
  		                document.forms['serviceOrderForm'].elements['valueDate'+aid].value=datam;
  		                </c:if>
  		            }
  		          <c:if test="${contractType}">

  		        try{
  		        var buyRate1=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
  		        var estCurrency1=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
  		        var basisValue=document.forms['serviceOrderForm'].elements['basis'+aid].value;
    	  		var quantityValue1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
	  		    if(quantityValue1=='' ||quantityValue1=='0' ||quantityValue1=='0.0' ||quantityValue1=='0.00') {   
	  		      document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value=1;
	  		      }
	  		  try{
    	  		var estimateSellQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
	  		    if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {   
	  		      document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
	  		      }
	  		 }catch(e){}	  		      
  		       var quantityValue=0;
  		     	quantityValue=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
  				if(estCurrency1.trim()!=""){

	  		          try{
		  			        var val1=0.00;
		  			        val1=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
		  			        var val2=0.00;
		  			        val2=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		  			        var val3=0.00;
		  			        val3=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
							if((val2!='')&&(val2!='0.00')&&(val2!='0.0')&&(val2!='0')){
			  			    var recCurrencyRate1=(val1*val2)/val3;
		  			        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
		  			        document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=roundValue;
							}else{
	  						var Q1=0;
	  						var Q2=0;
	  						Q1=buyRate1;
	  						Q2=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
	  			            var E1=Q1*Q2;
	  			            E1=Math.round(E1*10000)/10000;
	  			            document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=E1;
							}			  			          			        
		  		          }catch(e){ 
		  	  		          }
  			            var E1=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
  			            var estLocalAmt1=0;
  			            if(basisValue=="cwt" || basisValue== "%age"){
  			            	estLocalAmt1=(quantityValue*E1)/100 ;
  			                }
  			                else if(basisValue=="per 1000"){
  			                	estLocalAmt1=(quantityValue*E1)/1000 ;
  			                }else{
  			                	estLocalAmt1=(quantityValue*E1);
  			                }	 
  			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
  			            document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
  				}
  		      }catch(e){}

  		          </c:if>
  			       }
  	        } 
  	    
  	    }	  	
	   var http5513 = getHTTPObject88();
	  
	   var idList='';
	
	   function findSoAllPricing(){
		   		var accUpdateAjax=document.forms['serviceOrderForm'].elements['accUpdateAjax'].value;
		   		document.forms['serviceOrderForm'].elements['accUpdateAjax'].value="";
		   		if('${serviceOrder.id}'!= null && '${serviceOrder.id}'!= ''){
				new Ajax.Request('findAllSoPricingAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&accUpdateAjax='+accUpdateAjax,
						  {
						    method:'get',
						    onSuccess: function(transport){
						      var response = transport.responseText || "no response text";
						    //alert(response);
						      var container = document.getElementById("pricingSoAjax");
						      pricingValue='';
						      findAllPricingAjax(); 
						      container.innerHTML = response;	
						      $("#accountLineList").find( "th:contains('Quantity')" ).css("border-left","medium solid #003366"); 
						      $("#accountLineList").find( "th:contains('Description')" ).css("border-left","medium solid #003366"); 
						      $("#accountLineList").find( "th:contains('Rec')" ).css("border-left","1px solid #3dafcb"); 
						       showOrHide(0);
						      container.show();
						     
						    },
						    
						    onLoading: function()
						       {
							       if(document.getElementById("loading")!=null){
						    	var loading = document.getElementById("loading").style.display = "block";
						    	
						    	 loading.innerHTML;
							       }
						    },
						    
						    onFailure: function(){ 
							    //alert('Something went wrong...')
							     }
					  });
		   		}
			
			}		
		function findAllPricingAjax(){
			new Ajax.Request('findAllPricingLineAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}',
					  {
					    method:'get',
					    onSuccess: function(transport){
					      var response = transport.responseText || "";
					      response = response.trim();
					      idList = response;
					        if(idList.trim() == ''){
					        	document.getElementById("estimatedTotalExpense${serviceOrder.id}").value = 0.0;
					        	document.getElementById("estimatedTotalRevenue${serviceOrder.id}").value = 0.0;
					        	document.getElementById("estimatedGrossMargin${serviceOrder.id}").value = 0.0;
					        	document.getElementById("estimatedGrossMarginPercentage${serviceOrder.id}").value = 0.0;
								<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								setFieldValue('actualTotalRevenue${serviceOrder.id}',0.0);
								setFieldValue('actualTotalExpense${serviceOrder.id}',0.0);
								setFieldValue('actualGrossMargin${serviceOrder.id}',0.0);
								setFieldValue('actualGrossMarginPercentage${serviceOrder.id}',0.0);
					        	</c:if>
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								setFieldValue('revisedTotalRevenue${serviceOrder.id}',0.0);
								setFieldValue('revisedTotalExpense${serviceOrder.id}',0.0);
								setFieldValue('revisedGrossMargin${serviceOrder.id}',0.0);
								setFieldValue('revisedGrossMarginPercentage${serviceOrder.id}',0.0);
					        	</c:if>					        	
					        	return;
					        }vatDescrEnable();			disableRevenue('');					        
					        var id = idList.split(",");
					        var statusFlag='';
							  for (i=0;i<id.length;i++){
								  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
									  document.getElementById('Inactivate').disabled = false;
									  statusFlag='YES';
									  return true;
								  }
							  }
							  if(statusFlag != 'YES'){
								  document.getElementById('Inactivate').disabled = true;
							  }		
															  					  
					      //showOrHide(0);
					    },
					    
					    onFailure: function(){ 
						    //alert('Something went wrong...')
						     }
					  });
		}
		  var pricingValue='';
		function findAllPricingLineId(str,clickType){
			showOrHide(1);
			if(str == 'Inactive'){
				  inactiveCheck=str;
				  }
			  if(str == 'AddLine' && idList.length == 0){
	        	  autoSavePricingAjax(getHTTPObject(),str);
	        	  return false;
	          }else if(str == 'AddTemplate' && idList.length == 0){
	        	  autoSavePricingAjax(getHTTPObject(),str);
	        	  return false;
	          }else if(str == 'Inactive' && idList.length == 0){
	        	  alert('No Line is there.');
	        	  return false;
	          }else if(idList.length == 0){
	        	  if(str == 'Save'){
	        		  try{
	        			  <c:if test="${not empty serviceOrder.id}">
	        			  var totalExp=document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
		    			  var totalRev = document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
		    			  var grossMargin =document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
		    			  var grossMarginPercent =document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent;
		    			  </c:if> 
	        		  }catch(e){}
	        		  saveForm(str);
	        	  }
	        	 
	          
	        	  if(str == 'TabChange'){
	        		  try{
		        		  <c:if test="${not empty serviceOrder.id}">
	        			  var totalExp=document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
		    			  var totalRev = document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
		    			  var grossMargin =document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
		    			  var grossMarginPercent =document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
		    			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent; 
		    			  </c:if>
	        		  }catch(e){}
	        		  ContainerAutoSave(clickType);
	        	  }
	        	  return false; 
	          }
			  var id = idList.split(",");
			
	          var delimeter1 = "-@-";
	          var check =saveValidation(str);
	          if(check){
	        	 /*  for (i=0;i<id.length;i++){
	        		if(pricingValue == ''){
			    			pricingValue  =  getPricingValue(id[i].trim());
			    		}else{
			    			pricingValue  =  pricingValue +delimeter1+ getPricingValue(id[i].trim());
			    		}
	             } */
	             /*  Vendor Name not populated a/c to code So applying delay 2 sec  */
	             closeAllAutoSavePricingDiv(idList);
	        	  setTimeout(function(){
							        	  populatePricingValue(idList,delimeter1);
							        	  if(pricingValue != ''){
											  autoSavePricingAjax(getHTTPObject(),str,clickType);
									      }
	        	  					   }, 2000);
		     }
			  
		  }
		 function closeAllAutoSavePricingDiv(idList){
			 var id = idList.split(",");
       	  		for (i=0;i<id.length;i++){
					document.getElementById('estimateVendorNameDivId'+id[i]).style.display = "none";	
					if(document.getElementById('vendorCode'+id[i]).value==''){
						document.getElementById('estimateVendorName'+id[i]).value="";	
					}
				}
		 }
		  function populatePricingValue(idList,delimeter1){
			  var id = idList.split(",");
        	  for (i=0;i<id.length;i++){
	        		if(pricingValue == ''){
			    			pricingValue  =  getPricingValue(id[i].trim());
			    		}else{
			    			pricingValue  =  pricingValue +delimeter1+ getPricingValue(id[i].trim());
			    		}
	             }			  
		  }
		  var pricingFooterValue ='';
		  function autoSavePricingAjax(httpObj,str,clickType){
			  showOrHide(1);
			  var check =saveValidation(str);
			  if(check){
			  showOrHide(1);
			  var url1 = '';
			  var addURL ='';
			  var inActiveLine ='';
			  var divisionFlag='';
			  var setDescriptionChargeCode='';
			  var setDefaultDescriptionChargeCode='';
			  var soFlag ='';
			  var rollUpInvoiceFlag='';
			  var oldRecRateCurrency='';
			  try{
				  if(document.forms['serviceOrderForm'].elements['setDescriptionChargeCode']!=undefined){
				  setDescriptionChargeCode = document.forms['serviceOrderForm'].elements['setDescriptionChargeCode'].value;
				  }
				  }catch(e){}
				  try{
					  if(document.forms['serviceOrderForm'].elements['oldRecRateCurrency']!=undefined){
						  oldRecRateCurrency = document.forms['serviceOrderForm'].elements['oldRecRateCurrency'].value;
					  }
					  }catch(e){}				  
			  try{
				  if(document.forms['serviceOrderForm'].elements['setDefaultDescriptionChargeCode']!=undefined){
				  setDefaultDescriptionChargeCode = document.forms['serviceOrderForm'].elements['setDefaultDescriptionChargeCode'].value;
				  }
			  }catch(e){}
				var actualDiscountFlag ="FALSE";
				try{
					<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					actualDiscountFlag="TRUE";
				   </c:if>
				 }catch(e){}
					var revisionDiscountFlag ="FALSE";
					try{
						<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						revisionDiscountFlag="TRUE";
					   </c:if>
					 }catch(e){}				 			  
			  if(str == 'AddLine'){
				 // addURL = "&addPriceLine=YES";
				  
				  try{
					divisionFlag = document.forms['serviceOrderForm'].elements['divisionFlag'].value;
				  }catch(e){}
			  }else if(str == 'AddTemplate'){
				 // addURL = "&addPriceLine=AddTemplate";				  
				  try{
					  rollUpInvoiceFlag = document.forms['serviceOrderForm'].elements['rollUpInvoiceFlag'].value;
				  }catch(e){}
				  try{
						divisionFlag = document.forms['serviceOrderForm'].elements['divisionFlag'].value;
					  }catch(e){}
				  var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
					var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
					var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
					var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
					var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
					var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
					var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
					var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
					while(reloServiceType.indexOf("#")>-1){
						reloServiceType=reloServiceType.replace('#',',');
					}
					url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO'+packingModeSO+'&commoditySO'+commoditySO;
					url1 = url1+'&serviceTypeSO'+serviceTypeSO+'&companyDivisionSO'+companyDivisionSO+'&reloServiceType'+reloServiceType;
			  }else if(str == 'Inactive'){
				  var id = idList.split(",");
				 
					  for (i=0;i<id.length;i++){
						  try{
						  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
							  if(inActiveLine == ''){
								  inActiveLine = id[i].trim();
							  }else{
								  inActiveLine = inActiveLine+','+id[i].trim();
							  }
						  }  
						  }catch(e){}
					
				  } 
				
				  if(inActiveLine == ''){
					  alert('Please uncheck any line.');
					  showOrHide(0);
					  return false;
				  }
			  }
			  <c:if test="${not empty serviceOrder.id}">
			  var totalExp=document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
			  var totalRev = document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
			  var grossMargin =document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
			  var grossMarginPercent =document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['serviceOrderForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent;
			  pricingFooterValue = totalExp+"-,-"+totalRev+"-,-"+grossMargin+"-,-"+grossMarginPercent;
			  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  var actualTotalRevenue=document.forms['serviceOrderForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.actualRevenue'].value=actualTotalRevenue;
			  var actualTotalExpense = document.forms['serviceOrderForm'].elements['actualTotalExpense'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.actualExpense'].value=actualTotalExpense;
			  var actualGrossMargin =document.forms['serviceOrderForm'].elements['actualGrossMargin'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossMargin'].value=actualGrossMargin;
			  var actualGrossMarginPercentage =document.forms['serviceOrderForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossMarginPercentage'].value=actualGrossMarginPercentage;
			  var projectedActualRevenue =document.forms['serviceOrderForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.projectedActualRevenue'].value=projectedActualRevenue;
		      var projectedActualExpense =document.forms['serviceOrderForm'].elements['projectedActualExpense'+${serviceOrder.id}].value; 
			  document.forms['serviceOrderForm'].elements['serviceOrder.projectedActualExpense'].value=projectedActualExpense;
			  pricingFooterValue=pricingFooterValue+"-,-"+actualTotalExpense+"-,-"+actualTotalRevenue+"-,-"+actualGrossMargin+"-,-"+actualGrossMarginPercentage+"-,-"+projectedActualRevenue+"-,-"+projectedActualExpense;
              </c:if>
			  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  var revisedTotalRevenue=document.forms['serviceOrderForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.revisedTotalRevenue'].value=revisedTotalRevenue;
			  var revisedTotalExpense = document.forms['serviceOrderForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.revisedTotalExpense'].value=revisedTotalExpense;
			  var revisedGrossMargin =document.forms['serviceOrderForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value;
			  document.forms['serviceOrderForm'].elements['serviceOrder.revisedGrossMargin'].value=revisedGrossMargin;
			  var revisedGrossMarginPercentage =document.forms['serviceOrderForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['serviceOrderForm'].elements['serviceOrder.revisedGrossMarginPercentage'].value=revisedGrossMarginPercentage;
			  pricingFooterValue=pricingFooterValue+"-,-"+revisedTotalExpense+"-,-"+revisedTotalRevenue+"-,-"+revisedGrossMargin+"-,-"+revisedGrossMarginPercentage;
			  </c:if>
              </c:if> 
               soFlag=document.forms['serviceOrderForm'].elements['isSOExtract'].value;       
              document.forms['serviceOrderForm'].elements['isSOExtract'].value="";      			  
              var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}
			  $.ajax({
				  type: "POST",
				  url: "autoSOSavePricingAjax.html?ajax=1&decorator=simple&popup=true",
				  data: { sid: '${serviceOrder.id}', id: '${serviceOrder.id}',pricingValue: pricingValue, delimeter1: '-@-',delimeter2: '~',delimeter3: '-,-',
				    	pricingFooterValue: pricingFooterValue,addPriceLine: str,
				    	jobtypeSO: jobtypeSO,routingSO:routingSO,modeSO: modeSO,packingModeSO:packingModeSO,commoditySO:commoditySO,
				    	serviceTypeSO:serviceTypeSO,reloServiceType:reloServiceType,companyDivisionSO:companyDivisionSO,inActiveLine:inActiveLine,divisionFlag:divisionFlag,actualDiscountFlag:actualDiscountFlag,revisionDiscountFlag:revisionDiscountFlag,setDescriptionChargeCode:setDescriptionChargeCode,oldRecRateCurrency:oldRecRateCurrency,setDefaultDescriptionChargeCode:setDefaultDescriptionChargeCode,isSOExtract:soFlag,rollUpInvoiceFlag:rollUpInvoiceFlag
				        },
				 success: function (data, textStatus, jqXHR) {
					 var resultValue = data;
					 var valueArr = resultValue.split("~");
					 var fieldName = valueArr[0];
					 var fieldValue = valueArr[1];
					 var lineNo = valueArr[2];
					 if(fieldName.trim()=='cmmDmmContract'){
						 alert(fieldValue+" Charge code is not acceptable for # "+lineNo);
					 }
					 if(str == 'AddTemplate'){
						 document.forms['serviceOrderForm'].elements['defaultTemplate'].value = 'YES';
		              }
					 document.forms['serviceOrderForm'].elements['oldRecRateCurrency'].value = '';
				    		showOrHide(0);
				    		//alert('ok');
				    		if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'|| str==''){
				    			findSoAllPricing();
				              }
							  if(str == 'Save'){
								 saveForm('Save');
				              }
							  if(str == 'TabChange'){
								  ContainerAutoSave(clickType);
				              }
							
						},
						
			    error: function (data, textStatus, jqXHR) {
					    	showOrHide(0);
					    	//alert('Something went wrong...'); 
					    	
					    	if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'){
					    		findSoAllPricing();
				              }
							  if(str == 'Save'){
								 // saveForm('Save');
				              }
							  if(str == 'TabChange'){
								  ContainerAutoSave(clickType);
				              }
							
						}
				});
			  }
			  inactiveCheck="";	
		  }
		  function getPricingValue(id){
			 // alert(id);
			  var categoryType =document.forms['serviceOrderForm'].elements['category'+id].value; // 0
			  var chargeCode = document.forms['serviceOrderForm'].elements['chargeCode'+id].value; // 1
			  var vendorCode = document.forms['serviceOrderForm'].elements['vendorCode'+id].value; // 2
			  var vendorName = document.forms['serviceOrderForm'].elements['estimateVendorName'+id].value; // 3
			  try{
					//var basis = document.forms['serviceOrderForm'].elements['basis'+id].value; //4
					var basis = document.getElementById('basis'+id).value;
					//alert(basis);
			  		basis = basis.replace("%","Percentage");
			  }catch(e){
				 // alert("basis error."+e)
				  basis="";
			  }
			  var rollUpInInvoiceCheck = false;
			  try{
				 rollUpInInvoiceCheck = document.forms['serviceOrderForm'].elements['rollUpInInvoice'+id].checked; // 14
				  }catch(e){					  }			  
			  var estimateQuantity = document.forms['serviceOrderForm'].elements['estimateQuantity'+id].value;
			  var estimateRate =document.forms['serviceOrderForm'].elements['estimateRate'+id].value;
			  var estimateSellRate =document.forms['serviceOrderForm'].elements['estimateSellRate'+id].value;
			  var estimateExpense =document.forms['serviceOrderForm'].elements['estimateExpense'+id].value;
			  var estimatePassPercentage =document.forms['serviceOrderForm'].elements['estimatePassPercentage'+id].value;
			  var estimateRevenueAmount =document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+id].value;
			  var quoteDescription =document.forms['serviceOrderForm'].elements['quoteDescription'+id].value;
			  
			  var displayOnQuote =document.forms['serviceOrderForm'].elements['displayOnQuote'+id].checked;
			  var additionalService =document.forms['serviceOrderForm'].elements['additionalService'+id].checked;
			  try{
			  var statusCheck = document.forms['serviceOrderForm'].elements['statusCheck'+id].checked; // 14
			  }catch(e){}
			  var companyDivision = document.forms['serviceOrderForm'].elements['companyDivision'+id].value; 
			 // alert(additionalService);
			  
			  var division = validateFieldValue('division'+id); //15
			  var vatDecr = validateFieldValue('vatDecr'+id);
			  var vatPers = validateFieldValue('vatPers'+id);
			  var vatAmt = validateFieldValue('vatAmt'+id);
			  var deviation = validateFieldValue('deviation'+id);
			  var estimateDeviation = validateFieldValue('estimateDeviation'+id);
			  var estimateSellDeviation = validateFieldValue('estimateSellDeviation'+id);  //21
			  
			  var delimeter2 = "~";
			  var delimeter = "-,-";
			  
			  var values = id+"~"+categoryType+ delimeter +chargeCode+ delimeter +vendorCode+ delimeter +vendorName;
			  values = values+ delimeter +basis+ delimeter +estimateQuantity+ delimeter +estimateRate+ delimeter +estimateSellRate+ delimeter +estimateExpense;
			  values = values+ delimeter +estimatePassPercentage+ delimeter +estimateRevenueAmount+ delimeter +quoteDescription;
			  values = values+ delimeter +displayOnQuote+ delimeter +additionalService+ delimeter +statusCheck;
			  values = values+ delimeter +division+ delimeter +vatDecr+ delimeter +vatPers+ delimeter +vatAmt+ delimeter +deviation+ delimeter +estimateDeviation;
			  values = values+ delimeter +estimateSellDeviation; // 0 - 21
			  
			  var pricingHiddenValue = getPricingHiddenValue(id);  // 22 - 37
			  var pricingHiddenValue1 = getPricingHiddenValue1(id); // 38 - 59
			  
			  values = values + delimeter + pricingHiddenValue;
			  values = values + delimeter + pricingHiddenValue1;
			  
			  values = values + delimeter + validateFieldValue('accountLineNumber'+id); //60
			  values = values + delimeter + companyDivision;//61
			  values = values + delimeter + validateFieldValue('pricePointAgent'+id);//62
			  values = values + delimeter + validateFieldValue('pricePointMarket'+id);//63
			  values = values + delimeter + validateFieldValue('pricePointTariff'+id);//64
			  values = values + delimeter + validateFieldValue('estimateDiscount'+id);//65
				 
			  values = values + delimeter + validateFieldValue('actualRevenue'+id);//66
			  values = values + delimeter + validateFieldValue('recRate'+id);//67
			  values = values + delimeter + validateFieldValue('recQuantity'+id);//68
			  values = values + delimeter + validateFieldValue('actualExpense'+id);//69
			  values = values + delimeter + validateFieldValue('actualDiscount'+id);//70
			  
			  values = values + delimeter + validateFieldValue('payableContractCurrency'+id);//71
			  values = values + delimeter + validateFieldValue('payableContractExchangeRate'+id);//72
			  values = values + delimeter + validateFieldValue('payableContractRateAmmount'+id);//73
			  values = values + delimeter + validateFieldValue('payableContractValueDate'+id);//74
			  values = values + delimeter + validateFieldValue('country'+id);//75
			  values = values + delimeter + validateFieldValue('exchangeRate'+id);//76
			  values = values + delimeter + validateFieldValue('localAmount'+id);//77
			  values = values + delimeter + validateFieldValue('valueDate'+id);//78
			  
			  values = values + delimeter + validateFieldValue('contractCurrency'+id);//79
			  values = values + delimeter + validateFieldValue('contractExchangeRate'+id);//80
			  values = values + delimeter + validateFieldValue('contractRate'+id);//81
			  values = values + delimeter + validateFieldValue('contractValueDate'+id);//82
			  values = values + delimeter + validateFieldValue('contractRateAmmount'+id);//83
			  values = values + delimeter + validateFieldValue('recRateCurrency'+id);//84
			  values = values + delimeter + validateFieldValue('recRateExchange'+id);//85
			  values = values + delimeter + validateFieldValue('recCurrencyRate'+id);//86
			  values = values + delimeter + validateFieldValue('actualRevenueForeign'+id);//87
			  values = values + delimeter + validateFieldValue('racValueDate'+id);//88

			  values = values + delimeter + validateFieldValue('revisionPayableContractCurrency'+id);//89
			  values = values + delimeter + validateFieldValue('revisionPayableContractValueDate'+id);//90
			  values = values + delimeter + validateFieldValue('revisionPayableContractExchangeRate'+id);//91
			  values = values + delimeter + validateFieldValue('revisionPayableContractRate'+id);//92
			  values = values + delimeter + validateFieldValue('revisionPayableContractRateAmmount'+id);//93
			  values = values + delimeter + validateFieldValue('revisionContractCurrency'+id);//94
			  values = values + delimeter + validateFieldValue('revisionContractValueDate'+id);//95
			  values = values + delimeter + validateFieldValue('revisionContractExchangeRate'+id);//96
			  values = values + delimeter + validateFieldValue('revisionContractRate'+id);//97
			  values = values + delimeter + validateFieldValue('revisionContractRateAmmount'+id);//98
			  values = values + delimeter + validateFieldValue('revisionCurrency'+id);//99
			  values = values + delimeter + validateFieldValue('revisionValueDate'+id);//100
			  values = values + delimeter + validateFieldValue('revisionExchangeRate'+id);//101
			  values = values + delimeter + validateFieldValue('revisionLocalRate'+id);//102
			  values = values + delimeter + validateFieldValue('revisionLocalAmount'+id);//103
			  values = values + delimeter + validateFieldValue('revisionSellCurrency'+id);//104
			  values = values + delimeter + validateFieldValue('revisionSellValueDate'+id);//105
			  values = values + delimeter + validateFieldValue('revisionSellExchangeRate'+id);//106
			  values = values + delimeter + validateFieldValue('revisionSellLocalRate'+id);//107
			  values = values + delimeter + validateFieldValue('revisionSellLocalAmount'+id);//108

			  values = values + delimeter + validateFieldValue('revisionQuantity'+id);//109
			  values = values + delimeter + validateFieldValue('revisionDiscount'+id);//110
			  values = values + delimeter + validateFieldValue('revisionRate'+id);//111
			  values = values + delimeter + validateFieldValue('revisionSellRate'+id);//112
			  values = values + delimeter + validateFieldValue('revisionExpense'+id);//113
			  values = values + delimeter + validateFieldValue('revisionRevenueAmount'+id);//114
			  values = values + delimeter + validateFieldValue('revisionPassPercentage'+id);//115
			  values = values + delimeter + validateFieldValue('revisionVatDescr'+id);//116
			  values = values + delimeter + validateFieldValue('revisionVatPercent'+id);//117
			  values = values + delimeter + validateFieldValue('revisionVatAmt'+id);//118
			  values = values + delimeter + validateFieldValue('recVatDescr'+id);//119
			  values = values + delimeter + validateFieldValue('recVatPercent'+id);//120
			  values = values + delimeter + validateFieldValue('recVatAmt'+id);//121
			  values = values + delimeter + validateFieldValue('payVatDescr'+id);//122
			  values = values + delimeter + validateFieldValue('payVatPercent'+id);//123
			  values = values + delimeter + validateFieldValue('payVatAmt'+id);//124

			  values = values + delimeter + validateFieldValue('payingStatus'+id);//125
			  values = values + delimeter + validateFieldValue('receivedDate'+id);//126
			  values = values + delimeter + validateFieldValue('invoiceNumber'+id);//127
			  values = values + delimeter + validateFieldValue('invoiceDate'+id);//128
			  values = values + delimeter + validateFieldValue('estimateSellQuantity'+id);//129
			  values = values + delimeter + validateFieldValue('revisionSellQuantity'+id);//130
			  values = values + delimeter + validateFieldValue('accountLineCostElement'+id);//131
			  values = values + delimeter + validateFieldValue('accountLineScostElementDescription'+id);//132
			  values = values + delimeter + validateFieldValue('estExpVatAmt'+id);//133
			  values = values + delimeter + validateFieldValue('revisionExpVatAmt'+id);//134
			  values = values + delimeter + rollUpInInvoiceCheck; //135
			  values = values + delimeter + validateFieldValue('recVatGl'+id);//136
			  values = values + delimeter + validateFieldValue('payVatGl'+id);//137
			  values = values + delimeter + validateFieldValue('qstRecVatGl'+id);//138
			  values = values + delimeter + validateFieldValue('qstPayVatGl'+id);//139
			  values = values + delimeter + validateFieldValue('qstRecVatAmt'+id);//140
			  values = values + delimeter + validateFieldValue('qstPayVatAmt'+id);//141
			  values = values + delimeter + validateFieldValue('recInvoiceNumber'+id);//142
			  return values;
		  }
		  function validateFieldValue(fieldId){
			
			  var result1 =" ";
			  try {
				  var result =document.forms['serviceOrderForm'].elements[fieldId];
				  
					 if(result == null || result ==  undefined){
						 return result1;
					 }else{
						 return result.value;
					 }
			  }catch(e) {
				  return result1;
			  }
		  }
		  function getPricingHiddenValue(id){
			  var estCurrencyNew = validateFieldValue('estCurrencyNew'+id); //22
			  var estSellCurrencyNew = validateFieldValue('estSellCurrencyNew'+id);
			  
			  var estValueDateNew = validateFieldValue('estValueDateNew'+id); //24
			  var estSellValueDateNew = validateFieldValue('estSellValueDateNew'+id); //25
			  
			  //alert(estValueDateNew);
			  //alert(estSellValueDateNew);
			  
			  var estExchangeRateNew = validateFieldValue('estExchangeRateNew'+id);
			  var estSellExchangeRateNew = validateFieldValue('estSellExchangeRateNew'+id);
			  var estLocalRateNew = validateFieldValue('estLocalRateNew'+id);  //28
			  var estSellLocalRateNew = validateFieldValue('estSellLocalRateNew'+id); //29
			  
			  var estLocalAmountNew = validateFieldValue('estLocalAmountNew'+id); //30
			  var estSellLocalAmountNew = validateFieldValue('estSellLocalAmountNew'+id);
			  var revisionCurrencyNew = validateFieldValue('revisionCurrency'+id);
			  var countryNew = validateFieldValue('countryNew'+id);
			  
			  var revisionValueDateNew = validateFieldValue('revisionValueDate'+id); // 34
			  var valueDateNew = validateFieldValue('valueDateNew'+id);
			  var revisionExchangeRateNew = validateFieldValue('revisionExchangeRate'+id);
			  var exchangeRateNew = validateFieldValue('exchangeRateNew'+id); //37
			  
			  var delimeter = "-,-";
			  
			  var values = estCurrencyNew +delimeter+ estSellCurrencyNew +delimeter+ estValueDateNew +delimeter+ estSellValueDateNew;
			  values = values +delimeter+ estExchangeRateNew +delimeter+ estSellExchangeRateNew +delimeter+ estLocalRateNew +delimeter+ estSellLocalRateNew;
			  values = values +delimeter+ estLocalAmountNew +delimeter+ estSellLocalAmountNew +delimeter+ revisionCurrencyNew +delimeter+ countryNew;
			  values = values +delimeter+ revisionValueDateNew +delimeter+ valueDateNew +delimeter+ revisionExchangeRateNew +delimeter+ exchangeRateNew;
			  
			  return values;
		  }
		  function getPricingHiddenValue1(id){
			  var delimeter = "-,-";
			  var values = validateFieldValue('estimatePayableContractCurrencyNew'+id); // 38
			  values = values +delimeter+ validateFieldValue('estimateContractCurrencyNew'+id); //39
			  values = values +delimeter+ validateFieldValue('estimatePayableContractValueDateNew'+id);
			  
			  values = values +delimeter+ validateFieldValue('estimateContractValueDateNew'+id); //41
			  values = values +delimeter+ validateFieldValue('estimatePayableContractExchangeRateNew'+id); //42
			  
			  values = values +delimeter+ validateFieldValue('estimateContractExchangeRateNew'+id);
			  values = values +delimeter+ validateFieldValue('estimatePayableContractRateNew'+id);
			  values = values +delimeter+ validateFieldValue('estimateContractRateNew'+id);
			  values = values +delimeter+ validateFieldValue('estimatePayableContractRateAmmountNew'+id);
			  
			  values = values +delimeter+ validateFieldValue('estimateContractRateAmmountNew'+id); // 47
			  values = values +delimeter+ validateFieldValue('revisionContractCurrency'+id);
			  values = values +delimeter+ validateFieldValue('revisionPayableContractCurrency'+id);
			  values = values +delimeter+ validateFieldValue('contractCurrencyNew'+id); // 50
			  
			  values = values +delimeter+ validateFieldValue('payableContractCurrencyNew'+id);
			  values = values +delimeter+ validateFieldValue('revisionContractValueDate'+id);
			  values = values +delimeter+ validateFieldValue('revisionPayableContractValueDate'+id);
			  values = values +delimeter+ validateFieldValue('contractValueDateNew'+id);
			  values = values +delimeter+ validateFieldValue('payableContractValueDateNew'+id); // 55
			  values = values +delimeter+ validateFieldValue('revisionContractExchangeRate'+id);
			  values = values +delimeter+ validateFieldValue('revisionPayableContractExchangeRate'+id);
			  values = values +delimeter+ validateFieldValue('contractExchangeRate'+id);
			  values = values +delimeter+ validateFieldValue('payableContractExchangeRate'+id); // 59
			  
			  
			  return values;
		  }
		
			function addDefaultSOPricingLineAjax(str){
				var check =saveValidation("AddDefault");
				  if(check){
				var customerFileContract=document.forms['serviceOrderForm'].elements['billing.contract'].value;
				if(customerFileContract==''){
				 	alert("Please select Pricing Contract from billing");
				 	return false;
				}
				if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){ 
			          var agree = confirm("The billing has been completed, do you still want to add lines?");
			           if(agree)
			             {
			        	   var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
							var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
							var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
							var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
							var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
							var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
							var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
							 var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
								while(reloServiceType.indexOf("#")>-1){
									reloServiceType=reloServiceType.replace('#',',');
								}

							var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO;
							url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO+'&reloServiceType='+reloServiceType;
							//alert(url1)
							//var url1 = getTemplateParam();
							showOrHide(1);
							
							var url="findSoDefaultPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}"+url1;
							httpTemplate.open("POST", url, true);
							httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
							httpTemplate.send(null);
			             }else{}
				}else{
				var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
				var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
				var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
				var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
				var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
				var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
				var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
				var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}
				var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO;
				url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO+'&reloServiceType='+reloServiceType;
				//alert(url1)
				//var url1 = getTemplateParam();
				showOrHide(1);
				
				var url="findSoDefaultPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}"+url1;
				httpTemplate.open("POST", url, true);
				httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
				httpTemplate.send(null);
				}
				  }
			}
			function handleHttpResponseForAddTemplate(){
				if (httpTemplate.readyState == 4){
					showOrHide(0);
					 var results = httpTemplate.responseText;
				     results = results.trim();
				     if(results == 'NO'){
				    	 alert('No Default Template Found');
				    	 return false;
				     }else{
				    	findAllPricingLineId('AddTemplate');
				     }
			 	}
			}
//Pricing Method End//
 function selectPricingMarketPlace(category,id){
	try{
	 var  hidMarketPlace='hidMarketPlace'+id;
	 var  hidMarketPlace11='hidMarketPlace11'+id;
      var val = document.getElementById(category).value;
      var pricingFlag='${pricePointFlag}';
      var e2 = document.getElementById(hidMarketPlace);
      var e3 = document.getElementById(hidMarketPlace11);
       if( pricingFlag=='true' && (val=='Origin' || val=='Destin')){
    	   e2.style.display='block'; 
    	   e3.style.display='none';
       }else{
    	   e2.style.display='none';
    	   e3.style.display='none';
    	   }
	}catch(e){}
       disableRevenue(id); 
       <c:if test="${!(networkAgent)}">
       var category = document.getElementById(category).value; 
       if(category=='Origin' && document.getElementById('vendorCode'+id).value==''){
    	   document.getElementById('vendorCode'+id).value='${trackingStatus.originAgentCode}'
    	   <c:if test="${accountInterface!='Y'}">
    	   checkVendorName('vendorCode'+id,'estimateVendorName'+id);
	       </c:if>
	       <c:if test="${accountInterface=='Y'}">
	       vendorCodeForActgCode('vendorCode'+id,'estimateVendorName'+id,'actgCode'+id);
	       </c:if>
    	} 
    	if(category=='Destin' && document.getElementById('vendorCode'+id).value==''){
    		document.getElementById('vendorCode'+id).value='${trackingStatus.destinationAgentCode}'
    	    <c:if test="${accountInterface!='Y'}">
		 	  checkVendorName('vendorCode'+id,'estimateVendorName'+id);
	       </c:if>
	       <c:if test="${accountInterface=='Y'}">
	          vendorCodeForActgCode('vendorCode'+id,'estimateVendorName'+id,'actgCode'+id);
	       </c:if>
    	}
    	</c:if>
       		  }
	  function marketPlaceCode(vendorCode,category,aid,vendorName,estimateRate,contractType){					  
		  var val = document.getElementById(category).value;
		  var mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		  var packingMode=document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
		  var jobType=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		  var weight="";
		  var volume="";
		  var storageUnit='${weightType}';
		  //var storageUnit='lbscft';
		  if(storageUnit=='lbscft'){
			//weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		    //volume=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
			  weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
			  volume=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  }else if(storageUnit=='kgscbm'){
			//weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
			//volume=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
			  weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
			   volume=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  }
		  var soid='<%=request.getParameter("id") %>';
		  var countryCode="";
		  var packService="";
		  if(val=='Origin'){
			  countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value
		  }else if(val=='Destin'){
			  countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value
		  }
		  var pricePointexpired='${pricePointexpired}';		 
		  if(pricePointexpired=='true' || pricePointexpired==true){	
			  if(jobType=='STO'){
					if(weight!='' && volume!=''){
					  window.open('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&aid='+aid+'&soid='+soid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');
					 }else{
						alert("Please Enter the Following Data - Estimated Weight and Volume..."); 
					 }
					  }else{
		  if(mode!='' && weight!='' && volume!='' ){
			  if(mode=='Air'){	
		    openWindow('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&soid='+soid+'&aid='+aid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100'); 
			  }else if(mode=='Sea'){
				  if(packingMode=='RORO' || packingMode=='GRP' || packingMode=='BKBLK' || packingMode=='LCL' || packingMode=='LVAN' || packingMode=='T/W' || packingMode=='LOOSE' || packingMode=='LVCO' || packingMode=='LVCNT' || packingMode=='LVCNTR'){
					  packService="LCL";
					  }
				  if(packService!=''){
					  openWindow('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&soid='+soid+'&contractType='+contractType+'&aid='+aid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');	
				  }else{
				alert("The Selected Pack Mode is not Supported by Price Point!");
				  }				  
			  }else{
			   alert("The Selected Mode is not supported by Price Point!");
			  }	
		  }else{
			 alert("Please Enter the Following Data - Mode, Estimated Weight, Estimated Volume and Packing Mode ...");
		    }
					  }
		  }else{
			  alert("Your PricePoint subscription has expired. \n Please send email to support@redskymobility.com for renewal of the subscription.");  
		  }		  
		  }
</script>
<script type="text/javascript">
function checkPricePointDetails(targetunit){
	var pricePointFlag='${pricePointFlag}';	
	//alert("target "+targetunit)
	 var storageUnit='${weightType}';
	// alert("storageUnit "+storageUnit)
	 var shipnumber='${serviceOrder.shipNumber}';
	 if(pricePointFlag=='true' || pricePointFlag==true){	
	 if(targetunit==storageUnit){
		/// alert(shipnumber)
		 $.get("updatePricPointCheckAjax.html?ajax=1&decorator=simple&popup=true", 
					{shipNumber:shipnumber},
				function(data){	
						//alert(data)			
						data=data.replace('[','');
						data=data.replace(']','');
						//alert(data)	
				if(data.trim()!=''){
					alert("PricePoint quotes  subject to change due to modified weight / volume. \nPlease review and change if required.");
				}
				}); 
	 }
	 }
}
</script>
<script type="text/JavaScript">
try{
	<sec-auth:authComponent componentId="module.tab.serviceorder.weightvol">  
	 volumeDisplay();
    </sec-auth:authComponent>
}catch(e){
}
function getHTTPObjectSO23()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getConfirmationMessage(){
	var agree = confirm("Click 'OK' to reset the service list or click 'Cancel' to continue using the current list.");
    if(agree){
      populateInclusionExclusionData('noDefault','1','T');
  }
}
var httpMergeSO23 = getHTTPObjectSO23();
var httpMergeSO24 = getHTTPObjectSO23();
function callInclusionExclusionData(){
	populateInclusionExclusionData('default','1','F')
}

function populateInclusionExclusionData(target,val,flag){
	var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
	 var copyWithoutPricingFlag45="${copyWithoutPricingFlag}";
	<c:if test="${empty serviceOrder.id}">
	 if((copyWithoutPricingFlag45 != undefined)&&(copyWithoutPricingFlag45 != null)&&(copyWithoutPricingFlag45 != '')&&(incExcype.trim()=='Separate section')){
	 }else{
		target="noDefault";
	 }
	</c:if>

	  try{
	            var incExcServiceTypeLanguageValue = '${serviceOrder.incExcServiceTypeLanguage}';
	            var soincExcServiceTypeLanguage = document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceTypeLanguage'].value;
	            if(incExcServiceTypeLanguageValue == soincExcServiceTypeLanguage){
	                flag = 'F';
	            	target ="default" 
	            	value = '0';
	            }
	        }catch(e){}
	
	
	if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){ 
		 	document.getElementById("loader").style.display = "block";
		 	var job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		 	var mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		 	var routing=document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
		 	var language=document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceTypeLanguage'].value;
		 	var companyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
		 	var commodity=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
		 	var packingMode=document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
		 	var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
		 	var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
		 	var serviceType=document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
		 	if((job==null)||(job==undefined)||(job.trim()=="")){
		 		job="";
		 	}
		 	if((mode==null)||(mode==undefined)||(mode.trim()=="")){
		 		mode="";
		 	}
		 	if((routing==null)||(routing==undefined)||(routing.trim()=="")){
		 		routing="";
		 	}
		 	if((companyDivision==null)||(companyDivision==undefined)||(companyDivision.trim()=="")){
		 		companyDivision="";
		 	}
		 	if((commodity==null)||(commodity==undefined)||(commodity.trim()=="")){
		 		commodity="";
		 	}
		 	if((packingMode==null)||(packingMode==undefined)||(packingMode.trim()=="")){
		 		packingMode="";
		 	}
		 	if((originCountry==null)||(originCountry==undefined)||(originCountry.trim()=="")){
		 		originCountry="";
		 	}
		 	if((destinationCountry==null)||(destinationCountry==undefined)||(destinationCountry.trim()=="")){
		 		destinationCountry="";
		 	}
		 	if((serviceType==null)||(serviceType==undefined)||(serviceType.trim()=="")){
		 		serviceType="";
		 	}
		    var id="${serviceOrder.id}";
		    if((language==null)||(language==undefined)||(language.trim()=="")){
		     if((target=='default')&&(id==null)){
		      language="en";
		     }else{
		      language="";
		     }  
		    }
		    var copyWithoutPricingFlag="${copyWithoutPricingFlag}";
		      if((copyWithoutPricingFlag == undefined)||(copyWithoutPricingFlag == null)){
		       copyWithoutPricingFlag="";
		      }
		    	 var winLoc =encodeURI(window.location);
		    	 var index=winLoc.indexOf("addAndCopyServiceOrder.html");
		    	 var copyWithoutPricingFlag1="FALSE";
		       	  if(index>-1){
		       		copyWithoutPricingFlag1="TRUE";
		       	  }
		    var url="inclusionExlusionlist.html?ajax=1&decorator=simple&popup=true&jobIncExc="+job+"&modeIncExc="+mode+"&routingIncExc="+routing+"&id="+id+"&languageIncExc="+language+"&companyDivisionIncExc="+companyDivision+"&commodityIncExc="+commodity+"&packingModeIncExc="+packingMode+"&originCountryIncExc="+encodeURIComponent(originCountry)+"&destinationCountryIncExc="+encodeURIComponent(destinationCountry)+"&checkDefaultIncExc="+target+"&copyWithoutPricingFlag="+encodeURIComponent(copyWithoutPricingFlag)+"&copyWithoutPricingFlag1="+encodeURIComponent(copyWithoutPricingFlag1)+"&userQuoteServices="+encodeURIComponent(incExcype)+"&incExcResetCheck="+encodeURIComponent(flag)+"&serviceIncExc="+encodeURIComponent(serviceType);		    

		    httpMergeSO23.open("GET", url, true);
		 	httpMergeSO23.onreadystatechange =  function(){ handleHttpResponseMergeSoList23(target,incExcype,val);};
		 	httpMergeSO23.send(null);	
	}
}
function handleHttpResponseMergeSoList23(target,incExcype,val){
	
	if (httpMergeSO23.readyState == 4){
	    var results=httpMergeSO23.responseText; 
	    var soDiv = document.getElementById("incexc"); 
	    if(val=='0'){  
	    soDiv.style.display = 'none';
			document.getElementById("langS").style.display="none";
	    }
	    document.getElementById("loader").style.display = "none";
	    soDiv.innerHTML = results;
	    <c:if test="${empty serviceOrder.id}">
	    if(target=='default'){
	    	 var winLoc =encodeURI(window.location);
	    	 var index=winLoc.indexOf("addAndCopyServiceOrder.html");
	       	  if(index<0){
	       		//document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceTypeLanguage'].value='en';  
	       	  }	        
	    }else{
	    	if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){ 
	    		pickCheckBoxData1(target);
	    	}else{
	    		pickCheckBoxDataSec1(target);
	    	}
	    }
	    </c:if>
	    <c:if test="${not empty serviceOrder.id}">
	    if(target=='default'){
	    }else{
	    	if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){ 
	    		pickCheckBoxData1(target);
	    	}else{
	    		pickCheckBoxDataSec1(target);
	    	}
	    }
	    </c:if>	    
	    }
}

function pickCheckBoxData1(target){  
	if(target!='default'){
	var payingStatusListServer="";
    if(document.getElementsByName("checkVC")!=undefined){
    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("checkVC")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("checkVC").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("checkVC").id+":"+document.getElementsByName("checkVC").value;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
	}

}
function pickCheckBoxDataSec1(target){  
	if(target!='default'){
	var payingStatusListServer="";
    if(document.getElementsByName("checkVC")!=undefined){
    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("checkVC")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("checkVC").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("checkVC").id;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
	}

}
function checkAllInxExc(target,type){
	try{
		if(target.checked==true){
		    if(document.getElementsByName("checkVC")!=undefined){
		    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
		 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
			 	      if(document.getElementsByName("checkVC")[i].value==type){
		 	    	 document.getElementsByName("checkVC")[i].checked=true;	
			 	      }        	           
	 		       }	
	 	     	}else{
	 	     		if(document.getElementsByName("checkVC").value==type){
	 	    		document.getElementsByName("checkVC").checked==true;
	 	     		}		      
	 	         }	        
	 	    }
		}else{
		    if(document.getElementsByName("checkVC")!=undefined){
		    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
		 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
		 	    	 if(document.getElementsByName("checkVC")[i].value==type){
		 	    	 document.getElementsByName("checkVC")[i].checked=false;	  
		 	    	 }      	           
	 		       }	
	 	     	}else{
	 	     		if(document.getElementsByName("checkVC").value==type){
	 	    		document.getElementsByName("checkVC").checked==false;
	 	     		}		      
	 	         }	        
	 	    }			
		}
	}catch(e){}
	pickCheckBoxData(type);
}
function checkAllINCEXCFlag(type){
	  var checkAllF='T';
		    if(document.getElementsByName("checkVC")!=undefined){
		    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
		 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
			 	      if(document.getElementsByName("checkVC")[i].value==type && document.getElementsByName("checkVC")[i].checked==false && checkAllF=='T'){
			 	    	 checkAllF='F';
			 	      }        	           
	 		       }	
	 	     	}else{
	 	     		if(document.getElementsByName("checkVC").value==type && document.getElementsByName("checkVC").checked==false && checkAllF=='T'){
	 	     			checkAllF='F';
	 	     		}		      
	 	         }	        
	 	    }

	  if(checkAllF=='T'){
		  document.getElementById(type+'All').checked=true;
	  }else{
		  document.getElementById(type+'All').checked=false;
	  }	 
		var incCount=0;
		var excCount=0;
	    if(document.getElementsByName("checkVC")!=undefined){
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
		 	      if(document.getElementsByName("checkVC")[i].value=='INC'){
		 	    	 incCount++;
		 	      }else{
		 	    	 excCount++;
		 	      }        	           
 		       }	
 	     	}else{
 	     		if(document.getElementsByName("checkVC").value=='INC'){
 	     			incCount++;
 	     		}else{
		 	    	excCount++;
		 	    }		      
 	        }	        
 	    }
	  if(incCount<1){
		  document.getElementById('INCAll').checked=false;
	  } 
	  if(excCount<1){
		  document.getElementById('EXCAll').checked=false;
	  } 	  
}

function pickCheckBoxData(type){
	try{
	var payingStatusListServer='';
    if(document.getElementsByName("checkVC")!=undefined){
    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("checkVC")[i].checked==true){
 	                  		payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
 	                	}
 	                }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("checkVC").checked==true){
 	           payingStatusListServer=document.getElementsByName("checkVC").id+":"+document.getElementsByName("checkVC").value;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
    checkAllINCEXCFlag(type)
    autoSaveIncusionExclusion();
	}catch(e){}
}
function pickCheckBoxDataSec(){
	try{
	var payingStatusListServer="";
    if(document.getElementsByName("checkVC")!=undefined){
    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("checkVC")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("checkVC").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("checkVC").id;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
    autoSaveIncusionExclusion();
	}catch(e){}
}
function autoSaveIncusionExclusion(){
	 <c:if test="${not empty serviceOrder.id}">
		var incExcServiceType=document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value;
		document.forms['serviceOrderForm'].elements['servIncExcDefault'].value=incExcServiceType;
		var sid="${serviceOrder.id}";
		var url="autoSaveIncusionExclusionAjax.html?ajax=1&decorator=simple&popup=true&incExcServiceType="+encodeURIComponent(incExcServiceType)+"&sid="+sid;		    
	 	httpMergeSO24.open("GET", url, true);
	 	httpMergeSO24.onreadystatechange = handleHttpResponseMergeSoList24;
	 	httpMergeSO24.send(null);
	 </c:if>
}
function handleHttpResponseMergeSoList24(){
	if (httpMergeSO24.readyState == 4){
	    var results=httpMergeSO24.responseText; 
	}
}
function divtoggle(divid)
{
	if(togle=='0'){
		togle='1';
	document.getElementById(divid).style.display= "block";
	document.getElementById("langS").style.display="block";
	checkAllINCEXCFlag('INC');
	checkAllINCEXCFlag('EXC');		
	}else{
		togle='0';
		document.getElementById(divid).style.display= "none";
		document.getElementById("langS").style.display="none";
		
	}

}
var togle="0";
populateInclusionExclusionData('default','0','F');
function checkNotesForRevnue(aid,url){
	var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
	if(chargeCode==''){
		alert("Charge code needs to be selected");
}else{
	openWindow('notess.html?id='+aid+'&notesId='+aid+'&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);
}
}
function updateContractChangeCharge(newchargeCode,chargeCodeTemp,id,accContract){
	var chargeCodeValidationVal=document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value;
	if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
	var chargeCode = document.forms['serviceOrderForm'].elements[newchargeCode].value;
	var billingContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
	var chargeCodetemp= document.forms['serviceOrderForm'].elements[chargeCodeTemp].value;
    var sid='${serviceOrder.id}';
    var accountlineContract=accContract;
    var shipNumber='${serviceOrder.shipNumber}';
     if(sid!='' && sid!=null && chargeCode!=''){
    	if((chargeCode.trim()!=chargeCodetemp.trim()) && (billingContract.trim()!=accountlineContract.trim())){
    		//showOrHide(1);
    		url = "updateContractChangeChargeAjax.html?ajax=1&id="+id+"&sid="+sid+"&billingContract="+billingContract+"&shipNumber="+shipNumber;
    		http2.open("GET", url, true); 
    		http2.onreadystatechange = handleHttpResponse233;
    		http2.send(null);	 
    }
    }}
}

function handleHttpResponse233(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            showOrHide(0);
	}
}
function getIncExcDocDetails(){
	<configByCorp:fieldVisibility componentId="component.field.refQuote.document">
	var id="${serviceOrder.id}";
	var winLoc =encodeURI(window.location);
	var index=winLoc.indexOf("addAndCopyServiceOrder.html");
  	if(index>-1){
  		id = document.forms['serviceOrderForm'].elements['sofid'].value
  	}	
	var job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 	var mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
 	var routing=document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
 	var language=document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocTypeLanguage'].value;
 	var companyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
 	var commodity=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
 	var packingMode=document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
 	var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
 	var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var url="findIncExcDocListAjax.html?ajax=1&decorator=simple&popup=true&jobIncExc="+job+"&modeIncExc="+mode+"&routingIncExc="+routing+"&id="+id+"&languageIncExc="+language+"&companyDivisionIncExc="+companyDivision+"&commodityIncExc="+commodity+"&packingModeIncExc="+packingMode+"&originCountryIncExc="+encodeURIComponent(originCountry)+"&destinationCountryIncExc="+encodeURIComponent(destinationCountry)+"&docListResetCheck="+encodeURIComponent(docListReset);
	httpIncExcDoc.open("POST", url, true);
	httpIncExcDoc.onreadystatechange = function(){handleHttpResponseIncExcDoc()};
	httpIncExcDoc.send(null);
	</configByCorp:fieldVisibility>
}
function handleHttpResponseIncExcDoc(){
	if (httpIncExcDoc.readyState == 4){
		//showOrHide(0);
        var results= httpIncExcDoc.responseText;
        if(divClick=='1'){
        	document.getElementById("docs").style.display="block";
 	    var incExcDocDetailsDiv = document.getElementById("inclusionExclusionDocListAjax");
 	   		incExcDocDetailsDiv.innerHTML = results;
        }
 	   	<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
 	   		if(docListReset=='R'){
 	   			pickCheckBoxDocData1();
 	   		}else{
 	   			<c:if test="${empty serviceOrder.id}">
 	   				pickCheckBoxDocData1();
				</c:if>
 	   		}
 	   	</c:if>
 	   	<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Single section'}">
 	   		if(docListReset=='R'){
 	  			pickCheckBoxDocDataSec1();
 	   		}else{
 	   			<c:if test="${empty serviceOrder.id}">
 	   				pickCheckBoxDocDataSec1();
  				</c:if>
 	   		}
	   	</c:if>
 	}
}
<configByCorp:fieldVisibility componentId="component.field.refQuote.document">
	document.getElementById("docs").style.display="none";
	<c:if test="${empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocTypeLanguage'].value ='en';
	</c:if>
</configByCorp:fieldVisibility>
function divtoggleDoc(divid){
	if(toggle=='0'){
		toggle='1';
		document.getElementById("docs").style.display="block";
		document.getElementById("inclusionExclusionDocListAjax").style.display="block";
		<c:if test="${empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocTypeLanguage'].value ='en';
		</c:if>
		divClick="1";
	getIncExcDocDetails();		
	}else{
		toggle='0';
		document.getElementById("docs").style.display="none";
		document.getElementById("inclusionExclusionDocListAjax").style.display="none";
		
	}
}
var toggle="0";
var divClick="0";
var docListReset="";
var httpIncExcDoc = getHTTPObject();
var http5222 = getHTTPObject();
var http52222 = getHTTPObject();
function getConfirmationMessage1(){
	var agree = confirm("Click 'OK' to reset the service list or click 'Cancel' to continue using the current list.");
    if(agree){
    	docListReset="R";
    	getIncExcDocDetails();
  }
}
function pickCheckBoxDocData(){
	try{
	var payingStatusListServer='';
    if(document.getElementsByName("docChecked")!=undefined){
    	if((document.getElementsByName("docChecked").size!=0)&&(document.getElementsByName("docChecked").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("docChecked").length; i++){
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("docChecked")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("docChecked")[i].id+":"+document.getElementsByName("docChecked")[i].value;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("docChecked")[i].checked==true){
 	                  		payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("docChecked")[i].id+":"+document.getElementsByName("docChecked")[i].value;
 	                	}
 	                }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("docChecked").checked==true){
 	           payingStatusListServer=document.getElementsByName("docChecked").id+":"+document.getElementsByName("docChecked").value;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocServiceType'].value =payingStatusListServer;
    autoSaveIncusionExclusionDocument();
	}catch(e){}
}
function pickCheckBoxDocDataSec(){
	try{
	var payingStatusListServer="";
    if(document.getElementsByName("docChecked")!=undefined){
    	if((document.getElementsByName("docChecked").size!=0)&&(document.getElementsByName("docChecked").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("docChecked").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("docChecked")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("docChecked")[i].id;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("docChecked")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("docChecked")[i].id;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("docChecked").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("docChecked").id;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocServiceType'].value =payingStatusListServer;
    autoSaveIncusionExclusionDocument();
	}catch(e){}
}
function autoSaveIncusionExclusionDocument(){
	 <c:if test="${not empty serviceOrder.id}">
		var incExcServiceType=document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocServiceType'].value;
		//document.forms['serviceOrderForm'].elements['servIncExcDefault'].value=incExcServiceType;
		var sid="${serviceOrder.id}";
		var url="autoSaveIncusionExclusionDocAjax.html?ajax=1&decorator=simple&popup=true&incExcServiceType="+encodeURIComponent(incExcServiceType)+"&id="+sid;
	 	httpMergeSO241.open("GET", url, true);
	 	httpMergeSO241.onreadystatechange = handleHttpResponseMergeSoList241;
	 	httpMergeSO241.send(null);
	 </c:if>
}
function handleHttpResponseMergeSoList241(){
	if (httpMergeSO241.readyState == 4){
	    var results=httpMergeSO241.responseText; 
	}
}
var httpMergeSO241 = getHTTPObjectSO23();

function pickCheckBoxDocData1(){
	var payingStatusListServer="";
    if(document.getElementsByName("docChecked")!=undefined){
    	if((document.getElementsByName("docChecked").size!=0)&&(document.getElementsByName("docChecked").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("docChecked").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("docChecked")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("docChecked")[i].id+":"+document.getElementsByName("docChecked")[i].value;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("docChecked")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("docChecked")[i].id+":"+document.getElementsByName("docChecked")[i].value;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("docChecked").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("docChecked").id+":"+document.getElementsByName("docChecked").value;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocServiceType'].value =payingStatusListServer;
}
function pickCheckBoxDocDataSec1(){
	var payingStatusListServer="";
    if(document.getElementsByName("docChecked")!=undefined){
    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("docChecked").length!=undefined)){
 	      for (i=0; i<document.getElementsByName("docChecked").length; i++){	        	           
 	             if(payingStatusListServer==''){
 	 	             if(document.getElementsByName("docChecked")[i].checked==true){
 	                   payingStatusListServer=document.getElementsByName("docChecked")[i].id;
 	 	             }
 	                 }else{
 	                	if(document.getElementsByName("docChecked")[i].checked==true){
 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("docChecked")[i].id;
 	                	}
 	                   }
 	       }	
 	     }else{	      
 	    	if(document.getElementsByName("docChecked").checked==true){ 
 	           payingStatusListServer=document.getElementsByName("docChecked").id;
 	    	}
 	         }	        
 	     }
    document.forms['serviceOrderForm'].elements['serviceOrder.incExcDocServiceType'].value =payingStatusListServer;
}
 var q;
 var check;
function validateTabs(q) {
	
	$("#accountLineList").html();
	check=false;
    if($('#pricing').is(':visible'))
    {
  
     $("#accountLineList").find('.cCode').each(function( index, value  ) {
	 var val3=$(value).closest("tr").find(".lineNumber").val();
   	 var val4=$(value).closest("tr").find(".cCode").val();
   	
     	check=false;
		  if(val4=='')
			  {
			     alert("Please select Charge Code for Pricing line # '"+val3+"' and Kindly save the page");
			     check =true;
				 return false;
			  }
		  
		   
	});
	if(check==false)
		{
	 if(q=='notQuote')
		  {
		  	  window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
	  }
	  if(q=='quote')
		  {
		 
		 window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=Quote&reportSubModule=Quote&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
		  }
	  if(q=='viewEmail')
		  {
		
		  location.href='findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}';
	  }
	  if(q=='document')
	  {
		 
		 location.href='accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}';
	  }
	  if(q=='audit')
	  {
		 
		 window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
		  }
	}}
    if(!$('#pricing').is(':visible'))
    	{
	
  if(q=='notQuote')
	  {
	   window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
  }
  if(q=='quote')
	  {
	   window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&routingfilter=${serviceOrder.routing}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=Quote&reportSubModule=Quote&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
	  }
  
  if(q=='viewEmail')
	  {
			  location.href='findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}';
	  }
  if(q=='document')
  {
	 
	  location.href='accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}';
  }
  if(q=='audit')
  {
	 window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes');
	  }
}
	}
var httpAccInv = getHTTPObject082();

function getHTTPObject082()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function acoountlineinvoice(StrAcc)
{
	 
    var shipNumber = document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
         var url="findAccountlineInvoiceAjax.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(shipNumber);
         httpAccInv.open("POST", url, true);
         httpAccInv.onreadystatechange = function(){handleHttpAccountlineInvoiceAjax(StrAcc)};
         httpAccInv.send(null);
	
}
function handleHttpAccountlineInvoiceAjax(StrAcc){
	if (httpAccInv.readyState == 4){
	//showOrHide(0);
	var results= httpAccInv.responseText;
	results = results.trim();
	results = results.replace('[','');
	results=results.replace(']','');
	results = results.trim();
	var resu = results.split(",");
	var validationAccInvoice=true;
	for(var g=0;g<resu.length;g++){
	resu[g]=resu[g].trim();
	var tempRecInvoiceNumber=validateFieldValue('recInvoiceNumber'+resu[g])
	if( tempRecInvoiceNumber=='' ){
		validationAccInvoice=false;
	alert("Please fill the data again as Invoice is already generated by other user."); break;
	return false;
	}else{ 
	}
	}
	if(validationAccInvoice){
	findAllPricingLineId(StrAcc);
	}
	}
	}
var httppay12VatBilling = gethttppayVatBilling();
function gethttppayVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}

var httprecVatBilling = gethttprecVatBilling();
var httpPayVatBilling = gethttprecVatBilling();
function gethttprecVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}

function findPayVatList(aid,ven){
    <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	   var companyDivision =document.forms['serviceOrderForm'].elements['companyDivision'+aid].value 
	   var partnerCode =document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
	   var payVatDescr=  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value
	   var url="payVatAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&companyDivision="+encodeURI(companyDivision)+"&payVatDescr="+encodeURI(payVatDescr);
	     httppay12VatBilling.open("GET", url, true);
		 httppay12VatBilling.onreadystatechange=function(){ handleHttpResponsePayVatBilling(aid,ven);};
		 httppay12VatBilling.send(null);
		 </configByCorp:fieldVisibility > 
		 
}
function handleHttpResponsePayVatBilling(aid,ven) { 
	 if (httppay12VatBilling.readyState == 4)
    {
      var results = httppay12VatBilling.responseText
      results = results.trim();
      results = results.replace('[','');
      results=results.replace(']',''); 
      res = results.split("@");
      var accpayVatDescr= document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value;
      targetElement = document.forms['serviceOrderForm'].elements['payVatDescr'+aid];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['payVatDescr'+aid].options[i].text = '';
					document.forms['serviceOrderForm'].elements['payVatDescr'+aid].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['payVatDescr'+aid].options[i].value = stateVal[0];
					document.forms['serviceOrderForm'].elements['payVatDescr'+aid].options[i].text = stateVal[1];
					}
					
					}
				document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value = accpayVatDescr; 
				if(ven=="Vcc"){
				findDefaultVatFromPartnerForVendor(aid);
				}
				
		
					
    }
}
function findDefaultVatFromPartnerForVendor(aid){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	<c:if test="${systemDefaultVatCalculation=='true'}">

	var bCode = document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = function(){ handleHttpStorageVat92222(aid);}; ; 
	    http5222.send(null);	     
     }</c:if> </configByCorp:fieldVisibility > }
function handleHttpStorageVat92222(aid){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	     
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
					 	document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value = res[8];
			    	}  }
	        }else{
	       	 var payVatDescr =document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex2PayVatCheck(aid,res[12],res[8]); 
                      if(chkVatBillingGroup){ 
                   	  if(payVatDescr != res[8]){
                  		 var agree = confirm("Updating Vendor code will also change the Payable VAT code. Do you wish to continue?");
        					if(agree){
        						 document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value = res[8];
        						 calculateVatSectionAll('EXPENSE',aid,'BAS');
        					}
                       }
                      } 
                   
		        	 }
		          
	        	
	        }
		    	}    	} }
function flex2PayVatCheck(aid,defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var companyDivision =document.forms['serviceOrderForm'].elements['companyDivision'+aid].value 
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex2PayCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach>
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  } 
function findrecVATDesc(aid,billToCode,ven){
<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	  var companyDivision =document.forms['serviceOrderForm'].elements['companyDivision'+aid].value  
	   var recVatDescr=  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value
	   var url="recVatAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(billToCode)+"&companyDivision="+encodeURI(companyDivision)+"&recVatDescr="+encodeURI(recVatDescr);
		  httprecVatBilling.open("GET", url, true);
		 httprecVatBilling.onreadystatechange=function(){ handleHttpResponseRecVatBilling(aid,billToCode,ven)};
		 httprecVatBilling.send(null);
		 </configByCorp:fieldVisibility > 
		 
}
function handleHttpResponseRecVatBilling(aid,billToCode,ven) { 
	 if (httprecVatBilling.readyState == 4)
{
var results = httprecVatBilling.responseText
results = results.trim();
results = results.replace('[','');
results=results.replace(']',''); 
res = results.split("@");

var accpayVatDescr= document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value;
targetElement = document.forms['serviceOrderForm'].elements['recVatDescr'+aid];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['recVatDescr'+aid].options[i].text = '';
					document.forms['serviceOrderForm'].elements['recVatDescr'+aid].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['recVatDescr'+aid].options[i].value = stateVal[0];
					document.forms['serviceOrderForm'].elements['recVatDescr'+aid].options[i].text = stateVal[1];
					}
					
					}
				document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value = accpayVatDescr; 
				if(ven=="Vcc"){
					findDefaultVatFromPartnerForBill(aid,billToCode);
					}	
}
}
 
function findDefaultVatFromPartnerForBill(aid,bCode){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	<c:if test="${systemDefaultVatCalculation=='true'}">  
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http52222.open("GET", url, true); 
	    http52222.onreadystatechange =  function(){ handleHttpRecVatDescr92222(aid,bCode);};  
	    http52222.send(null);	     
     }</c:if> </configByCorp:fieldVisibility > }
function handleHttpRecVatDescr92222(aid,bCode){
    if (http52222.readyState == 4){    	
    	var results = http52222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value==''){
					 	document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value = res[8];
			    	}  }
	        }else{ 
	       	 var recVatDescr = document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex1RecVatCheck(aid,res[12],res[8]); 
                      if(chkVatBillingGroup){
                    	  if(recVatDescr != res[8]){
                       		 var agree = confirm("Updating Billtocode code will also change the Receivable VAT code. Do you wish to continue? ");
             					if(agree){
             						document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value = res[8];
             						 calculateVatSectionAll('REVENUE',aid,'BAS');
             					}
                            }
                    	  	 
                      } 
                       
		        	 }
		        
	        	
	        }
		    	}    	} }	
function flex1RecVatCheck(aid,defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> 
	var companyDivision =  document.forms['serviceOrderForm'].elements['companyDivision'+aid].value 
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex1RecCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach> 
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  }
</script>