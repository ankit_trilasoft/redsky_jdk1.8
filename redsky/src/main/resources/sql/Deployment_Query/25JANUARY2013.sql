ALTER TABLE `redsky`.`companydivision` ADD COLUMN `beneficiaryName`  VARCHAR(100) DEFAULT NULL AFTER `swiftcode`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `download` BIT(1) DEFAULT b'1';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `rating` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`drivercommissionplan` DROP COLUMN `distributionAmount`,DROP COLUMN `grossRevenue`;

ALTER TABLE `redsky`.`drivercommissionplan` ADD COLUMN `amountBasis` VARCHAR(45) DEFAULT NULL AFTER `percent`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `invoiceAutoUpload` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `distribution` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`accountline` ADD COLUMN `invoiceType` VARCHAR(25) DEFAULT b'0'

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `insuranceRate` DECIMAL(19,5) DEFAULT NULL, MODIFY COLUMN `insuranceBuyRate` DECIMAL(19,5) DEFAULT '0.000';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `driverCrewType` VARCHAR(100) DEFAULT NULL AFTER `distribution`;

 ALTER TABLE `redsky`.`accessinfo` MODIFY COLUMN `originResidenceType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originResidenceSize` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originCarryDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originCarrydetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originStairDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originStairdetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originfloor` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originElevatorDetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originElevatorType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originParkingType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originParkinglotsize` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originParkingSpots` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originDistanceToParking` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originShuttleDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `originAdditionalStopDetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationResidenceType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationResidenceSize` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationCarryDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationCarrydetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationStairDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationStairdetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationfloor` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationElevatorDetails` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationElevatorType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationParkingType` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationParkinglotsize` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationParkingSpots` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationDistanceToParking` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationShuttleDistance` VARCHAR(150) DEFAULT NULL,
 MODIFY COLUMN `destinationAdditionalStopDetails` VARCHAR(150) DEFAULT NULL;
 
 update refmaster set code=description,fieldlength='100' where parameter in('ROOMTYPE','SERVICE', 'RESIDENCETYPE','ELEVATORTYPE','PARKINGTYPE','LANGUAGE')
and corpid in ('CWMS','SSCW','VOAO','VOCZ','VOER','VORU','BOUR','ICMG','STAR','UGCN','UGHK','UGSG');
 
ALTER TABLE `redsky`.`billing` ADD COLUMN `vendorCodeVatCode1` VARCHAR(30) AFTER `vendorCodeVatCode`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `driverCommissionSetUp` BIT(1) DEFAULT b'0' AFTER `operationMgmtBy`;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `billToAuthorization` VARCHAR(35)  DEFAULT NULL;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `billTo2Authority` VARCHAR(35)  DEFAULT NULL;

DROP TABLE IF EXISTS `redsky`.`print_daily_package`;

DROP TABLE IF EXISTS `redsky`.`printdailypackage`;
CREATE TABLE  `redsky`.`printdailypackage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpId` varchar(10) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `formName` varchar(200) DEFAULT NULL,
  `formDesc` varchar(500) DEFAULT NULL,
  `jrxmlName` varchar(200) DEFAULT NULL,
  `parameters` varchar(300) DEFAULT NULL,
  `wtServiceType` varchar(100) DEFAULT NULL,
  `mode` varchar(45) DEFAULT NULL,
  `noOfCopies` int(11) DEFAULT NULL,
  `onePerSOperDay` varchar(45) DEFAULT NULL,
  `military` varchar(45) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `printSeq` int(11) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

update networkdatafields set type='CMM' where modelname='Billing' and  FieldName='billingCurrency' and type='CMM/DMM';

update networkdatafields set type='CMM' where modelname='Billing' and  FieldName='storageVatDescr' and type='CMM/DMM';

update networkdatafields set type='CMM' where modelname='Billing' and  FieldName='storageVatPercent' and type='CMM/DMM';

update networkdatafields set type='CMM' where modelname='Billing' and  FieldName='insuranceVatDescr' and type='CMM/DMM';

update networkdatafields set type='CMM' where modelname='Billing' and  FieldName='insuranceVatPercent' and type='CMM/DMM';

ALTER TABLE `redsky`.`drivercommissionplan` MODIFY COLUMN `charge`  VARCHAR(225)  DEFAULT NULL;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','storageDateRangeFrom' ,'Create','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','storageDateRangeFrom' ,'Update','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','storageDateRangeTo' ,'Create','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','storageDateRangeTo' ,'Update','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','storageDays' ,'Create','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','storageDays' ,'Update','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','onHand' ,'Create','amohanty',now(),'amohanty',now(),false,'','','DMM'),
      ('AccountLine','onHand' ,'Update','amohanty',now(),'amohanty',now(),false,'','','DMM'); 


ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `contract` VARCHAR(65);

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `contract` VARCHAR(65);

CREATE TABLE `redsky`.`userguides` (
  `id` BIGINT(20) DEFAULT NULL AUTO_INCREMENT,
  `corpid` VARCHAR(15) DEFAULT NULL,
  `createdBy` VARCHAR(82) DEFAULT NULL,
  `createdOn` DATETIME DEFAULT NULL,
  `updatedBy` VARCHAR(82) DEFAULT NULL,
  `updatedOn` DATETIME DEFAULT NULL,
  `module` VARCHAR(80) DEFAULT NULL,
  `documentName` VARCHAR(255) DEFAULT NULL,
  `corpidChecks` VARCHAR(150) DEFAULT NULL,
  `location` VARCHAR(450) DEFAULT NULL,
  `displayOrder` INTEGER DEFAULT NULL,
  `visible` TINYINT(1) DEFAULT NULL,
  `fileName` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;
 

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'ALL/ General', 'ALL/ General', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Sales', 'Sales', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Portals', 'Portals', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Accounting and Finance', 'Accounting and Finance', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Operations', 'Operations', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Admin', 'Admin', 40, 'Training_Doc','en',now(),'kkumar',now(),'kkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','Training_Doc',20,true,now(),'kkumar',now(),'kkumar',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'All', 'ALL', 40, 'CorpID_Checks','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'CMM/DMM Agent', 'CMM/DMM Agent', 40, 'CorpID_Checks','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Vanline', 'Vanline', 40, 'CorpID_Checks','en',now(),'kkumar',now(),'kkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','CorpID_Checks',20,true,now(),'kkumar',now(),'kkumar',1,1,'');


 



