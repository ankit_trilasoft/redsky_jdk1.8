<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title><fmt:message key="executeRules.title"/></title> 
<meta name="heading" content="<fmt:message key='executeRules.heading'/>"/> 
</head> 

<s:form id="executeRules" name="executeRules" action="" method="post" validate="true">   
<div id="Layer1" style="width:90%">
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table cellspacing="2" cellpadding="4" border="0" width="100%" style="text-align:center;">
<tbody>
	<tr>
      <td class="listwhitetext" style="font-size:12px;line-height:25px;"><b>To-Do Rules have been executed successfully.</b></td>
     </tr>
     <tr> 
      <td class="listwhitetext" style="font-size:12px;line-height:25px;"><b>Number of Rules Executed are ${countNumberOfRules}.</b></td>
     </tr>
     <tr> 
      <td class="listwhitetext" style="font-size:12px;line-height:25px;"><b>Rule(s) which are failed to execute - ${errorRuleNumberList}.</b></td>
     </tr>
     <tr> 
      <td class="listwhitetext" style="font-size:12px;line-height:25px;"><b>Number of Result Entered in Activity Management are ${countNumberOfResult}.</b></td>
     </tr>
     </tbody></table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>           
</s:form>