package com.trilasoft.app.model;

import com.opensymphony.xwork2.ActionSupport;
import java.util.*;

public class DateTag extends ActionSupport {
  private Date currentDate;
  public String execute() throws Exception{
    setCurrentDate(new Date());
    return SUCCESS;
  }
  public void setCurrentDate(Date date){
    this.currentDate = date;
  }
  public Date getCurrentDate(){
    return currentDate;
  }
} 
