<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>
<title>Survey Details</title>
<STYLE type=text/css>
#bigimageid {font-size: 0.75em;font-family:Arial, Helvetica, sans-serif;position: absolute;visibility: hidden;left: 0px;top: 0px;width: 400px;height: 0px;z-index: 100; }
.subcontenttabChild {margin:0px;padding:5px 0 0 10px;height:20px;}
input[type=checkbox] {margin:0px;}
#tt {position:absolute; display:block; background:url(images/tt_left.gif) top left no-repeat; }
#tttop {display:block; height:5px; margin-left:5px; background:url(images/tt_top.gif) top right no-repeat; overflow:hidden}
#ttcont {display:block; padding:2px 12px 3px 7px; margin-left:5px; background:#dcf4fe; color:#000}
#ttbot {display:block; height:5px; margin-left:5px; background:url(images/tt_bottom.gif) top right no-repeat; overflow:hidden}
</STYLE>
<meta name="heading"
	content="Survey Details" />
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript">
	animatedcollapse.addDiv('accessinfo', 'fade=1,hide=1,show=0')
	 animatedcollapse.addDiv('inventory', 'fade=1,hide=0,show=1')
	animatedcollapse.init()
	</script>
	<script type="text/javascript">
	function add(){
	var SoMode='';
	var tempMode="${serviceOrder.mode}";
	var tempType="${serviceOrder.job}";
	if(tempMode=='' && tempType=='STO'){
		SoMode='STORAGE'
	}else{
		SoMode=tempMode.toUpperCase();
	}
	
	window.location ="editSurveyDetails.html?cid=${customerFile.id}&sid=${serviceOrder.id}&SOflag=SO&Quote=${Quote}&SoMode="+SoMode;
	}
	function saveAccessInfo(){
		  document.forms['accessInfoForm'].submit();
		}
	
	</script>
	<script language="javascript" type="text/javascript">	
	try{
	<c:if test="${message=='popup'}">
	window.open('materialsTransferPopUp.html?cid=${customerFile.id}&id=${serviceOrder.id}&decorator=popup&popup=true&Quote=${Quote}','forms','height=300,width=350,top=1, left=200, scrollbars=yes,resizable=yes');
	</c:if>
	}
	catch(e){} 		
/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var tooltip=function(){
 var id = 'tt';
 var top = 3;
 var left = 3;
 var maxw = 300;
 var speed = 10;
 var timer = 20;
 var endalpha = 95;
 var alpha = 0;
 var tt,t,c,b,h;
 var ie = document.all ? true : false;
 return{
  show:function(v,w){
   if(tt == null){
    tt = document.createElement('div');
    tt.setAttribute('id',id);
    t = document.createElement('div');
    t.setAttribute('id',id + 'top');
    c = document.createElement('div');
    c.setAttribute('id',id + 'cont');
    b = document.createElement('div');
    b.setAttribute('id',id + 'bot');
    tt.appendChild(t);
    tt.appendChild(c);
    tt.appendChild(b);
    document.body.appendChild(tt);
    tt.style.opacity = 0;
    tt.style.filter = 'alpha(opacity=0)';
    document.onmousemove = this.pos;
   }
   tt.style.display = 'block';
   c.innerHTML = v;
   tt.style.width = w ? w + 'px' : 'auto';
   if(!w && ie){
    t.style.display = 'none';
    b.style.display = 'none';
    tt.style.width = tt.offsetWidth;
    t.style.display = 'block';
    b.style.display = 'block';
   }
  if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
  h = parseInt(tt.offsetHeight) + top;
  clearInterval(tt.timer);
  tt.timer = setInterval(function(){tooltip.fade(1)},timer);
  },
  pos:function(e){
   var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
   var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
   tt.style.top = (u - h) + 'px';
   tt.style.left = (l + left) + 'px';
  },
  fade:function(d){
   var a = alpha;
   if((a != endalpha && d == 1) || (a != 0 && d == -1)){
    var i = speed;
   if(endalpha - a < speed && d == 1){
    i = endalpha - a;
   }else if(alpha < speed && d == -1){
     i = a;
   }
   alpha = a + (i * d);
   tt.style.opacity = alpha * .01;
   tt.style.filter = 'alpha(opacity=' + alpha + ')';
  }else{
    clearInterval(tt.timer);
     if(d == -1){tt.style.display = 'none'}
  }
 },
 hide:function(){
  clearInterval(tt.timer);
   tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
  }
 };
}();

function imageList(cid,id,position){
	var url="toolTipImageList.html?cid="+cid+"&id="+id+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}
function confirmSubmit(targetElement, targetElement2){
	var agree=confirm("Please confirm that you want to delete this survey permanently?");
	var cid = targetElement;
	var id=targetElement2;
	if (agree){
        window.location="deleteItem.html?SOflag=SO&cid="+cid+"&id="+id+"&sid=${serviceOrder.id}&Quote=${Quote}";
	}
}
var http5 = getHTTPObject();
function goPrev() {
	progressBarAutoSave('1');
	var soIdNum ='${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
    function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'inventoryDataList.html?cid=${serviceOrder.customerFileId}&id='+results;
             }
       }
   
   function goNext() {
	progressBarAutoSave('1');
	var soIdNum ='${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'inventoryDataList.html?cid=${serviceOrder.customerFileId}&id='+results;
             }
       }  
function findCustomerOtherSO(position) {
 var sid='${serviceOrder.customerFileId}';
 var soIdNum='${serviceOrder.id}';
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }
   function goToUrl(id)
	{
		location.href = "editServiceOrderUpdate.html?id="+id;
	}
function transfermaterials(){
     window.location="transferMaterials.html?id=${serviceOrder.id}&cid=${serviceOrder.customerFileId}&Quote=${Quote}";
 }

function linkToImages(id,type){
    if(id!=''){
         window.open('accessInfoImageList.html?id=${accessInfo.id}&cid=${customerFile.id}&accessInfoType='+type,'forms','height=650,width=950,top=1, left=50, scrollbars=yes,resizable=yes');
     }else{
        alert("No Data is uploaded from Voxme");
      }
}
</script>	
</head>
<div id="Layer5" style="width:100%">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<c:if test="${Quote=='y'}">	 
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
</c:if>	
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/>  
<s:hidden name="ppType" id ="ppType" value="" />
 <c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
  <c:set var="tableName" value="customerfile" scope="session"/>
<s:hidden name="SOflag" value="${SOflag}"/>
 <c:set var="quantitysum" value="0" />
  <c:set var="volumesum" value="0.00" />
  <c:set var="totalsum" value="0.00" />
<c:set var="ppType" value=""/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
 <c:if test="${empty serviceOrder.id}">
	 <c:set var="isTrue" value="false" scope="request" />
 </c:if> 
 <c:if test="${not empty serviceOrder.id}">
	 <c:set var="isTrue" value="true" scope="request" />
 </c:if>
<s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <s:hidden name="gotoPageString" id="gotoPageString" value="" />
   <s:hidden name="formStatus" value=""/>
	<c:if test="${Quote!='y'}">
		<div id="newmnav">
		  <ul>
		  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
   <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
   <c:set var="relocationServicesKey" value="" />
   <c:set var="relocationServicesValue" value="" /> 
   <c:forEach var="entry" items="${relocationServices}">
         <c:if test="${relocationServicesKey==''}">
         <c:if test="${entry.key==serviceOrder.serviceType}">
         <c:set var="relocationServicesKey" value="${entry.key}" />
         <c:set var="relocationServicesValue" value="${entry.value}" /> 
         </c:if>
         </c:if> 
     </c:forEach>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		    <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		       <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
		       </c:if>
		    <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
		    <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>Quotes</span></a></li>
		    </c:if>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
           <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
           </sec-auth:authComponent>
           <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		<c:choose>
			<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			   <li><a  onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			</c:when> --%>
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			<c:otherwise> 
		       <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </c:if>
	  </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
	  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		<c:choose> 
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			<c:otherwise> 
		       <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </c:if>
	  </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	  	<c:if test="${serviceOrder.job !='RLO'}"> 
     		<c:if test="${forwardingTabVal!='Y'}"> 
				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			</c:if>
			<c:if test="${forwardingTabVal=='Y'}">
				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			</c:if>
    	</c:if>
    </c:if>
    <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS' || billing.billToCode =='P4071'}">
    <c:if test="${serviceOrder.job !='RLO'}"> 
    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </c:if> 
    <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
     <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </sec-auth:authComponent>
   <c:if test="${serviceOrder.job =='RLO'}">  
   <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
   </c:if>
   <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}"> 
    <c:if test="${ usertype!='AGENT'}">
   <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
 
	    <c:if test="${serviceOrder.job =='RLO'}"> 
		<li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li> 
	   </c:if>
	    </sec-auth:authComponent>	
	      <configByCorp:fieldVisibility componentId="component.standard.claimTab"> 	
	  <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	  <c:if test="${serviceOrder.job =='RLO'}"> 	
		 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	  </c:if> 
	  </sec-auth:authComponent>
	  </configByCorp:fieldVisibility>
  <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab"> 
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  </c:if>
 
  </c:if>
  </c:if>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
  <c:if test="${ usertype=='AGENT'}">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
 </c:if>
 </c:if>
 </configByCorp:fieldVisibility> 
  <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey Data<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</sec-auth:authComponent>
	   <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
	   <c:if test="${ usertype=='AGENT'}">
	    <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
           </c:if>
        <c:if test="${ usertype!='AGENT'}"><li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li></c:if>
        <%-- <c:if test="${ usertype!='AGENT'}"><li><a href="pricingWizardSummary.html?from=Accounting&shipnumber=${serviceOrder.id}" ><span>Pricing</span></a></li> </c:if> --%>
		</ul>					
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left" valign="top">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>	
		<div class="spn">&nbsp;</div> 
			<div style="padding-bottom:0px;"></div>		
	<div id="content" align="center" >
     <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:100%">
		<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
			<tbody>
			<tr><td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td>
		  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   cssStyle="width:110px;"  cssClass="input-textUpper" readonly="true"/>
		  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" cssStyle="width:90px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" cssStyle="width:70px;" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  cssStyle="width:50px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" cssStyle="width:50px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  cssStyle="width:50px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.routing"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" cssStyle="width:50px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext"><fmt:message key='customerFile.status'/></td>
            <td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" cssStyle="width:50px;" readonly="true"/> </td>
		  	</tr>
		  	<tr>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td>
		  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  cssStyle="width:110px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext"><fmt:message key="billing.registrationNo"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  cssStyle="width:90px;" readonly="true"/></td>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  cssStyle="width:70px;" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" cssStyle="width:50px;" readonly="true"/></td>
			 <c:if test="${serviceOrder.job!='RLO'}">
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" cssStyle="width:50px;" readonly="true"/></td>
		  	</c:if>
		  	<td align="right" class="listwhitetext">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td>
		  	<td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" cssStyle="width:264px;" readonly="true"/></td>
		  	</tr>
		 
			<tr>
	            <td align="right" class="listwhitetext" width="60px">Bkg. Agent&nbsp;</td>
	            <td><s:textfield cssClass="input-textUpper" name="serviceOrder.bookingAgentCode" cssStyle="width:110px;" maxlength="8" readonly="true"/></td>
				<td align="left" class="listwhitetext" >Division</td>
				<td><s:textfield cssClass="input-textUpper" name="serviceOrder.companyDivision" cssStyle="width:90px;" maxlength="3" readonly="true"/></td>
				
				<td align="left" class="listwhitetext" colspan="11">		
				Revised
				<c:if test="${serviceOrder.revised == true}">
				    <input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)" checked/>
				    </c:if>
					<c:if test="${serviceOrder.revised == false || serviceOrder.revised == null}">
					<input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)"/>
					</c:if>
				&nbsp;Contract&nbsp;<s:textfield cssClass="input-textUpper" name="billing.contract" cssStyle="width:166px;" readonly="true" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instruction&nbsp;<s:textfield cssClass="input-textUpper" name="billing.billingInstruction" cssStyle="width:265px;"  readonly="true" /></td>		     		        
			</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>	
	</div>

<div class="bottom-header"><span style="margin-left:50px; padding-top:5px">
  <c:if test="${billing.cod}">
  <div style="position:absolute; width:95%; text-align:center; font-size:12px;padding-top:1px; font-family:'Arial'; font-weight:bold; color:#000 "> COD </div>
                            <img id="cod1" src="${pageContext.request.contextPath}/images/cod.png" width="95%" />
      </c:if>
                              
                           
</span></div>
</div>
</div> </c:if>

<div style="clear:both;"></div>
     <!--   <div onClick="javascript:animatedcollapse.toggle('accessinfo')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Access Info</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div>  -->   
        <%--  <div id="accessinfo">
        <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Origin</b></font></div>
        <s:form method="post" action="saveAccessInfoForSo.html?&Quote=${Quote}&id=${serviceOrder.id}&cid=${cid}"  name="accessInfoForm">
        <s:hidden name="countdown"  value="480"/>     
         <s:hidden name="accessInfo.serviceOrderId" value="${id}"/> 
         <s:hidden name="id" value="${serviceOrder.id}"/> 
         <s:hidden name="cid" value="${cid}"/>        
         <s:hidden name="accessInfo.corpID" value="${serviceOrder.corpID}"/>   
           <s:hidden name="aid" value="${accessInfo.id}"/> 
        <s:hidden name="accessInfo.id" value="${accessInfo.id}"/> 
         
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
           <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originResidenceType"  value="%{accessInfo.originResidenceType}" list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right" height="21px" >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.originResidenceSize"  size="30" /></td>	  	      
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>
	  	  <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.originLongCarry" value="${accessInfo.originLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" key="accessInfo.originCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="60px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.originStairCarry" value="${accessInfo.originStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" key="accessInfo.originStairDistance"/></td>
	  	          
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','origin')"/></td>
 	        
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right" >Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" colspan="2" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originNeedCrane" value="${accessInfo.originNeedCrane}" fieldValue="true"/></td>
	  	 		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.originComment" cssClass="input-text" cssStyle="width:210px;height:70px;" onkeydown="limitText(this,this.form.countdown,480);" 
                     onkeyup="limitText(this,this.form.countdown,480);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	 
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originElevator" value="${accessInfo.originElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.originExternalElevator" value="${accessInfo.originExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originElevatorType"  list="%{elevatorType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originReserveParking" value="${accessInfo.originReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originParkingType"  list="%{parkingType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originDistanceToParking"/></td>
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr> 
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="110px;">Shuttle Required</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.originShuttleRequired" value="${accessInfo.originShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originShuttleDistance"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.originAdditionalStopRequired" value="${accessInfo.originAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originAdditionalStopDetails"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   </table>
      
        <table width="100%" cellspacing="0" cellpadding="0" class="detailTabLabel" style="margin:0px;padding:0px;">
			<tbody>
			<tr></tr><tr><td align="center" class="vertlinedata" colspan="10"></td></tr>
			<tr><td>
			<div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Destination</b></font></div>
			</td></tr>
			
			<tr><td class="listwhitetext"></td></tr>
			</tbody>
		</table>
       
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
            <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationResidenceType"  list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue=""/></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right" height="21px" >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.destinationResidenceSize" size="30" /></td>
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>	
	  	    <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.destinationLongCarry" value="${accessInfo.destinationLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" key="accessInfo.destinationCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="60px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.destinationStairCarry" value="${accessInfo.destinationStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" key="accessInfo.destinationStairDistance"/></td>
	  	        	  	        	  		     		  
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','dest')"/></td>
	  	       
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right">Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" colspan="2" ><s:checkbox name="accessInfo.destinationNeedCrane" value="${accessInfo.destinationNeedCrane}" fieldValue="true"/></td>
	  	  		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.destinationComment" cssClass="input-text" cssStyle="width:210px;height:70px;" onkeydown="limitText(this,this.form.countdown,480);" 
                     onkeyup="limitText(this,this.form.countdown,480);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationElevator" value="${accessInfo.destinationElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.destinationExternalElevator" value="${accessInfo.destinationExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationElevatorType"  list="%{elevatorType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationReserveParking" value="${accessInfo.destinationReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking&nbsp;Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationParkingType"  list="%{parkingType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationDistanceToParking"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	   <tr>
	  	   <td>
	  	     <div style="float:left;">
	  	     <table>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right" width="110px;">Shuttle Required</td>
	  			  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.destinationShuttleRequired" value="${accessInfo.destinationShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationShuttleDistance"/></td>
	  			
	  	   </tr>
	  	 	<tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  						
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.destinationAdditionalStopRequired" value="${accessInfo.destinationAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"  ><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationAdditionalStopDetails"/></td>
	  	   </tr>
	  	   </table>
	  	   </div>
	  	   <div style="float:right;">
	  	   <c:if test="${inventoryPath.path!=null && inventoryPath.path!=''}">
	  	    <img id="userImage" src="UserImage?location=${inventoryPath.path}" alt="" width="150" height="150"  style="border:thin solid #219DD1;" />
	  	    </c:if> 
	  	   </div>
	  	   </td>
	  	   </tr>	  	  
	  	   </table>	  	   
	  	 </s:form>
        </div> --%>
        <!-- <div onClick="javascript:animatedcollapse.toggle('inventory')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Survey Data</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div>  -->

<c:if test="${Quote=='y'}">
<div id="newmnav">
		  <ul>		  
		    <li ><a href="QuotationFileForm.html?from=list&id=<%=request.getParameter("cid") %>"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a href="editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>		    
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey Details</span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=quotation&reportSubModule=quotation&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
            <li><a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>             
		</ul>
		</div><div class="spn">&nbsp;</div> 
			<div style="padding-bottom:0px;"></div>		
	<div id="content" align="center" >
  <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:100%">
		<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="5" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" onfocus="onLoad();"/></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" cssStyle="width:150px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.originCountryCode" required="true" cssStyle="width:60px;" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" cssStyle="width:150px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" colspan="3"></td>						
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
	</div>	
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>


</div>
  <div onClick="javascript:animatedcollapse.toggle('accessinfo')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Access Info</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div> 
         <div id="accessinfo">
        <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Origin</b></font></div>
        <s:form method="post" action="saveAccessInfoForSo.html?&Quote=${Quote}&id=${serviceOrder.id}&cid=${cid}"  name="accessInfoForm">
        <s:hidden name="countdown"  value="480"/>     
         <s:hidden name="accessInfo.serviceOrderId" value="${id}"/> 
         <%-- <s:hidden name="id" value="${serviceOrder.id}"/> 
         <s:hidden name="cid" value="${cid}"/>  --%>      
         <s:hidden name="sid" value="${serviceOrder.id}"/> 
         <s:hidden name="accessInfo.corpID" value="${serviceOrder.corpID}"/>   
           <s:hidden name="aid" value="${accessInfo.id}"/> 
        <s:hidden name="accessInfo.id" value="${accessInfo.id}"/> 
       <s:hidden name="accessInfo.networkSynchedId" />
         
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
           <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:100px;"  ><s:select name="accessInfo.originResidenceType"  value="%{accessInfo.originResidenceType}" list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right"  width="37px" >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.originResidenceSize"  cssStyle="width:100px;" /></td>	  	      
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>
	  	  <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   
	  	   <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.originLongCarry" value="${accessInfo.originLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.originCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="240px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.originStairCarry" value="${accessInfo.originStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.originStairDistance"/></td>
	  	          
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','origin')"/></td>
 	        
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right" >Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="90">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" colspan="2" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originNeedCrane" value="${accessInfo.originNeedCrane}" fieldValue="true"/></td>
	  	 		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.originComment" cssClass="input-text" cssStyle="width:240px;height:70px;" onkeydown="limitText(this,this.form.countdown,1950);" 
                     onkeyup="limitText(this,this.form.countdown,1950);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	 
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originElevator" value="${accessInfo.originElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.originExternalElevator" value="${accessInfo.originExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originElevatorType"  list="%{elevatorType}"  cssStyle="width:155px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originReserveParking" value="${accessInfo.originReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originParkingType"  list="%{parkingType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="145px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originDistanceToParking"/></td>
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr> 
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="110px;">Shuttle Required</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.originShuttleRequired" value="${accessInfo.originShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originShuttleDistance"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.originAdditionalStopRequired" value="${accessInfo.originAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originAdditionalStopDetails"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   </table>
      
        <table width="100%" cellspacing="0" cellpadding="0" class="detailTabLabel" style="margin:0px;padding:0px;">
			<tbody>
			<tr></tr><tr><td align="center" class="vertlinedata" colspan="10"></td></tr>
			<tr><td>
			<div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Destination</b></font></div>
			</td></tr>
			
			<tr><td class="listwhitetext"></td></tr>
			</tbody>
		</table>
       
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
            <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:100px;"  ><s:select name="accessInfo.destinationResidenceType"  list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue=""/></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right" width="37px"  >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.destinationResidenceSize" cssStyle="width:100px;" /></td>
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>	
	  	    <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.destinationLongCarry" value="${accessInfo.destinationLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.destinationCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="240px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.destinationStairCarry" value="${accessInfo.destinationStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.destinationStairDistance"/></td>
	  	        	  	        	  		     		  
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','dest')"/></td>
	  	       
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right">Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="90px">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" colspan="2" ><s:checkbox name="accessInfo.destinationNeedCrane" value="${accessInfo.destinationNeedCrane}" fieldValue="true"/></td>
	  	  		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.destinationComment" cssClass="input-text" cssStyle="width:240px;height:70px;" onkeydown="limitText(this,this.form.countdown,1950);" 
                     onkeyup="limitText(this,this.form.countdown,1950);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationElevator" value="${accessInfo.destinationElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.destinationExternalElevator" value="${accessInfo.destinationExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationElevatorType"  list="%{elevatorType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationReserveParking" value="${accessInfo.destinationReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking&nbsp;Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationParkingType"  list="%{parkingType}"  cssStyle="width:155px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="145px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationDistanceToParking"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	   <tr>
	  	   <td>
	  	     <div style="float:left;">
	  	     <table>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right" width="110px;">Shuttle Required</td>
	  			  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.destinationShuttleRequired" value="${accessInfo.destinationShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationShuttleDistance"/></td>
	  			
	  	   </tr>
	  	 	<tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  						
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.destinationAdditionalStopRequired" value="${accessInfo.destinationAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"  ><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationAdditionalStopDetails"/></td>
	  	   </tr>
	  	   </table>
	  	   </div>
	  	   <div style="float:right;">
	  	   <c:if test="${inventoryPath.path!=null && inventoryPath.path!=''}">
	  	    <img id="userImage" src="UserImage?location=${inventoryPath.path}" alt="" width="150" height="150"  style="border:thin solid #219DD1;" />
	  	    </c:if> 
	  	   </div>
	  	   </td>
	  	   </tr>	  	  
	  	   </table>	  	   
	  	 </s:form>
        </div>
        
        
        
        
        
<div onClick="javascript:animatedcollapse.toggle('inventory')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Survey Data</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div> 
<div style="clear:both;"></div>
	<div style="width=100%; ">
	
		<div style="width:100%; float:left;" id="inventory">
	<!-- <div id="otabs" style="margin-top: -5px; margin-bottom:0px;"> -->
	<div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  
		    <ul>
		    <li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}&Quote=${Quote}" class="current"><span>Survey Data</span></a></li>
		    <li><a class="current"><span>Materials</span></a></li>
		   </ul>
		</div>
       <display:table name="articlesList" class="table" requestURI="" id="articlesList"  pagesize="25" style="width:100%;" > 
       <display:column property="atricle" title="Article" style="width:120px;" sortable="true" href="editSurveyDetails.html?cid=${customerFile.id}&SOflag=SO&Quote=${Quote}" paramId="id" paramProperty="id" />
           <display:column property="mode" style="width:50px;" sortable="true"/>         
           <display:column property="room" style="width:200px;"/>
           <c:if test="${invListflag ne null && invListflag=='1'}">
             <c:set var="quantitysum" value="${articlesList.quantity+quantitysum }" /> 
             </c:if> 
             <display:column property="quantity" style="width:30px;text-align:right;"/>
            <c:if test="${invListflag ne null && invListflag=='1'}">
               <c:set var="volumesum" value="${articlesList.totalWeight+volumesum }" /> 
               </c:if>
           <display:column property="cft" title="Volume" style="width:30px;text-align:right;"/>
            <c:if test="${invListflag ne null && invListflag=='1'}">
           <c:set var="totalsum" value="${articlesList.total+totalsum }" /> 
           </c:if>
            <display:column property="total" style="width:120px;text-align:right;" title="Total Volume"/>
            <display:column property="weight" style="width:30px;text-align:right;" title="Weight"/>
             <display:column property="totalWeight"  style="width:120px;text-align:right;" title="Total Weight"/>
            <display:column title="Valuation"  style="width:80px;text-align:right;">
            <c:if test="${articlesList.valuation!='' && articlesList.valuation!=null && articlesList.valuation!='0'}">
            <img  src="${pageContext.request.contextPath}/images/dollar-black.gif" style="vertical-align:top;" />${articlesList.valuation}
              </c:if>
              </display:column>
          
            <display:column title="NTBS"  style="width:30px;text-align:right;">
                   <c:if test="${articlesList.ntbsFlag==true}">         
                   <input type="checkbox"  checked="checked" disabled="disabled"  readonly="readonly"/>
                   </c:if>
                    <c:if test="${articlesList.ntbsFlag==false||articlesList.ntbsFlag==null}">         
                   <input type="checkbox" id="${findSummryViewDocList.id}"  disabled="disabled"  readonly="readonly"/>
                   </c:if>
             </display:column>
            <display:column property="comment" style="width:150px;"  maxLength="80">
           <!--  <c:if test="${articlesList.comment!=''&& articlesList.comment!= null}" >
             <img src="${pageContext.request.contextPath}/images/commentplus.png" alt="" width="20" height="20" onmouseover="tooltip.show('${articlesList.comment} ', 200);" onmouseout="tooltip.hide();"  />
            </c:if>
           
            <c:if test="${articlesList.comment=='' || articlesList.comment == null}" >
             <img src="${pageContext.request.contextPath}/images/comment.png" alt="" width="20" height="20"  />
            </c:if>
            -->
             </display:column>
            <display:column  title="Images" style="width:50px;text-align:center;" >
            <c:if test="${articlesList.imagesAvailablityFlag!=null && articlesList.imagesAvailablityFlag!=''}" >
            <img id="userImage" src="${pageContext.request.contextPath}/images/camera24.png" alt="" width="24" height="24" onclick="imageList('${customerFile.id}','${articlesList.id}',this)"/>
           </c:if>
           </display:column>
            <display:column title="Remove" style="width: 15px;">
		 	   <c:if test="${usertype== 'AGENT' || ( not empty articlesList.id && not empty articlesList.networkSynchedId && articlesList.id != articlesList.networkSynchedId)}">
		 	 	<img  align="middle" title="" onclick="" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recyle-disable.png"/>
		 	 </c:if> 
		 	  <c:if test="${usertype!='AGENT' && (empty articlesList.networkSynchedId || articlesList.id == articlesList.networkSynchedId)}"> 
		 	 	<a>
		 	   		<img  align="middle" title="" onclick="confirmSubmit('${customerFile.id}','${articlesList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
	          	</a>
	          </c:if>
	        </display:column>
	          <display:footer>
	          <tr>                     
                 <td colspan="3" align="left"  class="tdheight-footer"></td>                      
                  <td align="right">${quantitysum}</td>
                   <td align="right"></td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalsum}"  /></td>
                  <td align="right"> </td>
                     <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum}"  /> </td><td></td><td></td><td></td><td></td><td></td>
               </tr>
               <tr>                     
                 <td colspan="2" align="left"  class="tdheight-footer"></td>
                 <td align="right">Survey&nbsp;Data&nbsp;Total:</td>                      
                  <td align="right">${quatSum}</td>
                   <td align="right"></td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totSum}"  /></td>
                   <td align="right"> </td>
                     <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volSum}"  /> </td><td></td><td></td><td></td><td></td><td></td>
               </tr>
                <tr>                     
                 <td colspan="2" align="left"  class="tdheight-footer"></td>
                 <td align="right">Grand&nbsp;Total:</td>                      
                  <td align="right">${quantitysum+quatSum}</td>
                   <td align="right"> </td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalsum+totSum}"  /></td>
                   <td align="right"> </td>
                      <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum+volSum}"  /> </td><td></td><td></td><td></td><td></td><td></td>
               </tr>
	         </display:footer>
       </display:table>
       </div>     	
</div>
</div>
<input id="addInv" type="button"  class="cssbutton" style="width:55px; height:25px;margin-left:25px;"  value="Add" onclick="add();"  />	
 <input id="saveAcc" type="button"  Class="cssbutton" Style="width:130px; height:25px;margin-left:20px;"   value="Save Access Info" onclick="saveAccessInfo();"/> 	
 <c:if test="${materialTransferFlag=='Y'}">
 <c:if test="${Quote!='y'}">
   <input type="button" id="transMaterial" class="cssbutton" style="width:140px; height:25px;margin-left:25px;"  value="Transfer Materials" onclick="transfermaterials();"  />		
 </c:if>
</c:if>
<script language="javascript" type="text/javascript">
try{
	<c:if test="${ usertype== 'AGENT' || ( not empty accessInfo.id && not empty accessInfo.networkSynchedId && accessInfo.id != accessInfo.networkSynchedId ) }">
	var elementsLen=document.forms['accessInfoForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
	if(document.forms['accessInfoForm'].elements[i].type=='text'){
				document.forms['accessInfoForm'].elements[i].readOnly =true;
				document.forms['accessInfoForm'].elements[i].className = 'input-textUpper';
		}else{
				document.forms['accessInfoForm'].elements[i].disabled=true;
		}
	}	
	document.getElementById('addInv').disabled=true;	
	document.getElementById('saveAcc').disabled = true;
	document.getElementById('transMaterial').disabled = true;
</c:if>
}catch(e){
	
}
<c:if test="${hitflag=='1'}">
 <c:redirect url="/inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}&Quote=${Quote}"/>
</c:if>
</script>