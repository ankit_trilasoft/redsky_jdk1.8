package com.trilasoft.qtg.wsclient.driver;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.trilasoft.qtg.wsclient.request.model.ESOpportunity;
import com.trilasoft.qtg.wsclient.request.model.OpportunityData;
import com.trilasoft.qtg.wsclient.response.model.CreateOpportunityReturn;

public class DriverClass {
	
	public static void main(String []a) throws JAXBException {
		
		OpportunityData opportunityData = new OpportunityData();
		StringWriter writer = new StringWriter();
		
		ESOpportunity esOpportunity = new ESOpportunity();
		esOpportunity.setCompanyName("abc");
		esOpportunity.setCustomerNameFirst("john");
		esOpportunity.setCustomerNameLast("doe");
		esOpportunity.setSourceType("xx");
		esOpportunity.setAgentAssignmentCode("cc");
		opportunityData.setOpportunity(esOpportunity);
		
		//create request
		final JAXBContext context =  JAXBContext.newInstance(com.trilasoft.qtg.wsclient.request.model.OpportunityData.class);
		context.createMarshaller().marshal(opportunityData, writer);
		
		System.out.println("xml request: "+writer.toString());
		
		
		//read response
		String response = "";
		StringReader stringReader = new StringReader(response);
		final CreateOpportunityReturn createOpportunityReturn = (CreateOpportunityReturn) context.createUnmarshaller().unmarshal(stringReader);
		
	}

}
