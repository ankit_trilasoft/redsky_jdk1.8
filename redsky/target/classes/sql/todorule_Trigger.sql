


delimiter $$

CREATE trigger redsky.trigger_add_history_todorule
BEFORE UPDATE on redsky.todorule
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE NewCorpid LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET NewCorpid = " ";

   IF (OLD.url <> NEW.url ) THEN
      select CONCAT(fieldNameList,'todorule.url~') into fieldNameList;
      select CONCAT(oldValueList,OLD.url,'~') into oldValueList;
      select CONCAT(newValueList,NEW.url,'~') into newValueList;
  END IF;
   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'todorule.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF;
   IF (OLD.expression <> NEW.expression ) THEN
      select CONCAT(fieldNameList,'todorule.expression~') into fieldNameList;
      select CONCAT(oldValueList,OLD.expression,'~') into oldValueList;
      select CONCAT(newValueList,NEW.expression,'~') into newValueList;
  END IF;
   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'todorule.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;
   
   IF (OLD.durationAddSub <> NEW.durationAddSub ) THEN
      select CONCAT(fieldNameList,'todorule.durationAddSub~') into fieldNameList;
      select CONCAT(oldValueList,OLD.durationAddSub,'~') into oldValueList;
      select CONCAT(newValueList,NEW.durationAddSub,'~') into newValueList;
  END IF;
   IF (OLD.entitytablerequired <> NEW.entitytablerequired ) THEN
      select CONCAT(fieldNameList,'todorule.entitytablerequired~') into fieldNameList;
      select CONCAT(oldValueList,OLD.entitytablerequired,'~') into oldValueList;
      select CONCAT(newValueList,NEW.entitytablerequired,'~') into newValueList;
  END IF;
   IF (OLD.fieldToValidate1 <> NEW.fieldToValidate1 ) THEN
      select CONCAT(fieldNameList,'todorule.fieldToValidate1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldToValidate1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldToValidate1,'~') into newValueList;
  END IF;
   IF (OLD.fieldToValidate2 <> NEW.fieldToValidate2 ) THEN
      select CONCAT(fieldNameList,'todorule.fieldToValidate2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldToValidate2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldToValidate2,'~') into newValueList;
  END IF;
   IF (OLD.fieldToValidate3 <> NEW.fieldToValidate3 ) THEN
      select CONCAT(fieldNameList,'todorule.fieldToValidate3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldToValidate3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldToValidate3,'~') into newValueList;
  END IF;
   IF (OLD.messagedisplayed <> NEW.messagedisplayed ) THEN
      select CONCAT(fieldNameList,'todorule.messagedisplayed~') into fieldNameList;
      select CONCAT(oldValueList,OLD.messagedisplayed,'~') into oldValueList;
      select CONCAT(newValueList,NEW.messagedisplayed,'~') into newValueList;
  END IF;
   IF (OLD.rolelist <> NEW.rolelist ) THEN
      select CONCAT(fieldNameList,'todorule.rolelist~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rolelist,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rolelist,'~') into newValueList;
  END IF;
   IF (OLD.testdate <> NEW.testdate ) THEN
      select CONCAT(fieldNameList,'todorule.testdate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.testdate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.testdate,'~') into newValueList;
  END IF;
   IF (OLD.emailAddress <> NEW.emailAddress ) THEN
      select CONCAT(fieldNameList,'todorule.emailAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailAddress,'~') into newValueList;
  END IF;
   IF (OLD.ruleType <> NEW.ruleType ) THEN
      select CONCAT(fieldNameList,'todorule.ruleType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ruleType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ruleType,'~') into newValueList;
  END IF;
   IF (OLD.fieldDisplay <> NEW.fieldDisplay ) THEN
      select CONCAT(fieldNameList,'todorule.fieldDisplay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldDisplay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldDisplay,'~') into newValueList;
  END IF;
  
   IF (OLD.noteType <> NEW.noteType ) THEN
      select CONCAT(fieldNameList,'todorule.noteType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.noteType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.noteType,'~') into newValueList;
  END IF;
  
   IF (OLD.noteSubType <> NEW.noteSubType ) THEN
      select CONCAT(fieldNameList,'todorule.noteSubType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.noteSubType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.noteSubType,'~') into newValueList;
  END IF;
  
   IF (OLD.remarks <> NEW.remarks ) THEN
      select CONCAT(fieldNameList,'todorule.remarks~') into fieldNameList;
      select CONCAT(oldValueList,OLD.remarks,'~') into oldValueList;
      select CONCAT(newValueList,NEW.remarks,'~') into newValueList;
  END IF;
  
   IF (OLD.emailNotification <> NEW.emailNotification ) THEN
      select CONCAT(fieldNameList,'todorule.emailNotification~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailNotification,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailNotification,'~') into newValueList;
  END IF;
  
   IF (OLD.manualEmail <> NEW.manualEmail ) THEN
      select CONCAT(fieldNameList,'todorule.manualEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.manualEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.manualEmail,'~') into newValueList;
  END IF;

   IF ((OLD.systemDate <> NEW.systemDate) or (OLD.systemDate is null and NEW.systemDate is not null)
      or (OLD.systemDate is not null and NEW.systemDate is null)) THEN
       select CONCAT(fieldNameList,'todorule.systemDate~') into fieldNameList;
       IF(OLD.systemDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.systemDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.systemDate is not null) THEN
        	select CONCAT(oldValueList,OLD.systemDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.systemDate is not null) THEN
        	select CONCAT(newValueList,NEW.systemDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF ((OLD.valueDate <> NEW.valueDate) or (OLD.valueDate is null and NEW.valueDate is not null)
      or (OLD.valueDate is not null and NEW.valueDate is null)) THEN
       select CONCAT(fieldNameList,'todorule.valueDate~') into fieldNameList;
       IF(OLD.valueDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.valueDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.valueDate is not null) THEN
        	select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.valueDate is not null) THEN
        	select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      	END IF;
   END IF;
   IF ((OLD.emailSentDateUpdate <> NEW.emailSentDateUpdate) or (OLD.emailSentDateUpdate is null and NEW.emailSentDateUpdate is not null)
      or (OLD.emailSentDateUpdate is not null and NEW.emailSentDateUpdate is null)) THEN
       select CONCAT(fieldNameList,'todorule.emailSentDateUpdate~') into fieldNameList;
       IF(OLD.emailSentDateUpdate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.emailSentDateUpdate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.emailSentDateUpdate is not null) THEN
        	select CONCAT(oldValueList,OLD.emailSentDateUpdate,'~') into oldValueList;
     	END IF;
      	IF(NEW.emailSentDateUpdate is not null) THEN
        	select CONCAT(newValueList,NEW.emailSentDateUpdate,'~') into newValueList;
      	END IF;
   END IF;
 
   IF ((OLD.emailOn <> NEW.emailOn) or (OLD.emailOn is null and NEW.emailOn is not null)
      or (OLD.emailOn is not null and NEW.emailOn is null)) THEN
       select CONCAT(fieldNameList,'todorule.emailOn~') into fieldNameList;
       IF(OLD.emailOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.emailOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.emailOn is not null) THEN
        	select CONCAT(oldValueList,OLD.emailOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.emailOn is not null) THEN
        	select CONCAT(newValueList,NEW.emailOn,'~') into newValueList;
      	END IF;
   END IF;
 IF (OLD.checkEnable <> NEW.checkEnable ) THEN
       select CONCAT(fieldNameList,'todorule.checkEnable~') into fieldNameList;
       IF(OLD.checkEnable = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.checkEnable = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.checkEnable = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.checkEnable = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  IF (OLD.publishRule <> NEW.publishRule ) THEN
       select CONCAT(fieldNameList,'todorule.publishRule~') into fieldNameList;
       IF(OLD.publishRule = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.publishRule = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.publishRule = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.publishRule = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.docType <> NEW.docType ) THEN
      select CONCAT(fieldNameList,'todorule.docType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.docType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.docType,'~') into newValueList;
  END IF;

  
  
  IF (OLD.ruleNumber <> NEW.ruleNumber ) THEN
      select CONCAT(fieldNameList,'todorule.ruleNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ruleNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ruleNumber,'~') into newValueList;
  END IF;
  
  
  select corpid into NewCorpid from app_user where username=NEW.updatedby;
  
 CALL add_tblHistory (OLD.id,"todorule", fieldNameList, oldValueList, newValueList, NEW.updatedby, NewCorpid, now());

END $$


delimiter;