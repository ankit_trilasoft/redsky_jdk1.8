package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.model.UserDevice;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.UserDeviceDao;
import com.trilasoft.app.service.UserDeviceManager;

public class UserDeviceManagerImpl extends GenericManagerImpl<UserDevice, Long> implements UserDeviceManager{
	UserDeviceDao userDeviceDao;
	public UserDeviceManagerImpl(UserDeviceDao userDeviceDao) {
		super(userDeviceDao);
		this.userDeviceDao = userDeviceDao;
	}
	public List findRecordByLoginUser(String loginUser, String deviceIdVal){
		return userDeviceDao.findRecordByLoginUser(loginUser, deviceIdVal);
	}
}
