package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "contract")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class Contract extends BaseObject implements ListLinkData {

	private String corpID;
	private Long id;
	private String billingInstructionCode;
	private Date begin;
	private String BillingInstruction;
	private String contract;
	private String description;

	private String details;

	private double discount;

	private String entitled;
	private String group1;
	private String insoptCode;

	private String insuranceHas;
	private String insThrough;

	private String invnum;

	private String jobType;

	private double insvaluEntitled;

	private double minimumPay;

	private double otherDiscount;
	private double packDisc;
	private String section;
	private double sitDiscount;
	private String tariff;

	private String timeFrom;

	private String timeTo;
	private double trnsDisc;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;

	private String updatedBy;
	private Date ending;

	private Boolean domesticSalesCommission;
	private Boolean displayonAgentTariff;
	
	private String companyDivision;
	private String payMethod;
	
	private Boolean internalCostContract;
	private String contractType;
	private String owner;
	private String contractCurrency;
	private Long origContract;
	private String published;
	private String payableContractCurrency;
	private Boolean dmmInsurancePolicy = new Boolean(true);
	private String recClearing;
	private String payClearing;
	private Boolean fXRateOnActualizationDate;
	private String clientPostingGroup;
	private String accountCode;
	private String accountName;
	private String storageBillingGroup;
	private String storageEmail;
	private String emailPrintOption;
	private String creditTerms;
	private String customerFeedback;
	private Boolean noTransfereeEvaluation;
	private Boolean oneRequestPerCF;
	private String ratingScale;
	private Boolean lumpSum;
	private Boolean detailedList;
	private Boolean noInsurance;
	private BigDecimal lumpSumAmount= new BigDecimal(0);
	private String lumpPerUnit;
	private String minReplacementvalue;
	private String networkPartnerCode;
	private String networkPartnerName;
	private String source;
	private String vendorCode;
	private String vendorName;
	private String insuranceOption;
	private String defaultVat;
	private String billToAuthorization;
	private String billingMoment;
	private String creditTermsCmmAgent;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id)
				.append("billingInstructionCode", billingInstructionCode)
				.append("begin", begin)
				.append("BillingInstruction", BillingInstruction)
				.append("contract", contract)
				.append("description", description).append("details", details)
				.append("discount", discount).append("entitled", entitled)
				.append("group1", group1).append("insoptCode", insoptCode)
				.append("insuranceHas", insuranceHas)
				.append("insThrough", insThrough).append("invnum", invnum)
				.append("jobType", jobType)
				.append("insvaluEntitled", insvaluEntitled)
				.append("minimumPay", minimumPay)
				.append("otherDiscount", otherDiscount)
				.append("packDisc", packDisc).append("section", section)
				.append("sitDiscount", sitDiscount).append("tariff", tariff)
				.append("timeFrom", timeFrom).append("timeTo", timeTo)
				.append("trnsDisc", trnsDisc).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).append("ending", ending)
				.append("domesticSalesCommission", domesticSalesCommission)
				.append("displayonAgentTariff", displayonAgentTariff)
				.append("companyDivision", companyDivision)
				.append("payMethod", payMethod)
				.append("internalCostContract", internalCostContract)
				.append("contractType", contractType).append("owner", owner)
				.append("contractCurrency", contractCurrency)
				.append("origContract", origContract)
				.append("published", published)
				.append("payableContractCurrency", payableContractCurrency) 
				.append("dmmInsurancePolicy", dmmInsurancePolicy) 
				.append("recClearing", recClearing) 
				.append("payClearing", payClearing).append("fXRateOnActualizationDate",fXRateOnActualizationDate) 
				.append("clientPostingGroup",clientPostingGroup)
				.append("accountCode",accountCode)
				.append("storageBillingGroup",storageBillingGroup)
				.append("storageEmail",storageEmail).append("emailPrintOption",emailPrintOption)
				.append("creditTerms",creditTerms).append("customerFeedback",customerFeedback)
				.append("noTransfereeEvaluation",noTransfereeEvaluation).append("oneRequestPerCF",oneRequestPerCF)
				.append("ratingScale",ratingScale).append("lumpSum",lumpSum)
				.append("detailedList",detailedList).append("noInsurance",noInsurance)
				.append("lumpSumAmount",lumpSumAmount).append("lumpPerUnit",lumpPerUnit)
				.append("minReplacementvalue",minReplacementvalue).append("networkPartnerCode",networkPartnerCode)
				.append("networkPartnerName",networkPartnerName).append("source",source)
				.append("vendorCode",vendorCode).append("vendorName",vendorName)
				.append("insuranceOption",insuranceOption).append("defaultVat",defaultVat)
				.append("billToAuthorization",billToAuthorization)
				.append("billingMoment",billingMoment).append("creditTermsCmmAgent",creditTermsCmmAgent)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Contract))
			return false;
		Contract castOther = (Contract) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(billingInstructionCode,
						castOther.billingInstructionCode)
				.append(begin, castOther.begin)
				.append(BillingInstruction, castOther.BillingInstruction)
				.append(contract, castOther.contract)
				.append(description, castOther.description)
				.append(details, castOther.details)
				.append(discount, castOther.discount)
				.append(entitled, castOther.entitled)
				.append(group1, castOther.group1)
				.append(insoptCode, castOther.insoptCode)
				.append(insuranceHas, castOther.insuranceHas)
				.append(insThrough, castOther.insThrough)
				.append(invnum, castOther.invnum)
				.append(jobType, castOther.jobType)
				.append(insvaluEntitled, castOther.insvaluEntitled)
				.append(minimumPay, castOther.minimumPay)
				.append(otherDiscount, castOther.otherDiscount)
				.append(packDisc, castOther.packDisc)
				.append(section, castOther.section)
				.append(sitDiscount, castOther.sitDiscount)
				.append(tariff, castOther.tariff)
				.append(timeFrom, castOther.timeFrom)
				.append(timeTo, castOther.timeTo)
				.append(trnsDisc, castOther.trnsDisc)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(ending, castOther.ending)
				.append(domesticSalesCommission,
						castOther.domesticSalesCommission)
				.append(displayonAgentTariff, castOther.displayonAgentTariff)
				.append(companyDivision, castOther.companyDivision)
				.append(payMethod, castOther.payMethod)
				.append(internalCostContract, castOther.internalCostContract)
				.append(contractType, castOther.contractType)
				.append(owner, castOther.owner)
				.append(contractCurrency, castOther.contractCurrency)
				.append(origContract, castOther.origContract)
				.append(published, castOther.published)
				.append(payableContractCurrency, castOther.payableContractCurrency)
				.append(dmmInsurancePolicy, castOther.dmmInsurancePolicy)
				.append(recClearing, castOther.recClearing)
				.append(payClearing, castOther.payClearing)
				.append(fXRateOnActualizationDate, castOther.fXRateOnActualizationDate)
				.append(clientPostingGroup, castOther.clientPostingGroup)
				.append(accountCode, castOther.accountCode)
				.append(accountName, castOther.accountName)
				.append(storageBillingGroup, castOther.storageBillingGroup)
				.append(storageEmail, castOther.storageEmail).append(emailPrintOption, castOther.emailPrintOption)
				.append(creditTerms, castOther.creditTerms).append(customerFeedback, castOther.customerFeedback)
				.append(noTransfereeEvaluation, castOther.noTransfereeEvaluation).append(oneRequestPerCF, castOther.oneRequestPerCF)
				.append(ratingScale, castOther.ratingScale).append(lumpSum, castOther.lumpSum)
				.append(detailedList, castOther.detailedList).append(noInsurance, castOther.noInsurance)
				.append(lumpSumAmount, castOther.lumpSumAmount).append(lumpPerUnit, castOther.lumpPerUnit)
				.append(minReplacementvalue, castOther.minReplacementvalue).append(networkPartnerCode, castOther.networkPartnerCode)
				.append(networkPartnerName, castOther.networkPartnerName).append(source, castOther.source)
				.append(vendorCode, castOther.vendorCode).append(vendorName, castOther.vendorName)
				.append(insuranceOption, castOther.insuranceOption).append(defaultVat, castOther.defaultVat)
				.append(billToAuthorization, castOther.billToAuthorization)
				.append(billingMoment, castOther.billingMoment).append(creditTermsCmmAgent, castOther.creditTermsCmmAgent)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(billingInstructionCode).append(begin)
				.append(BillingInstruction).append(contract)
				.append(description).append(details).append(discount)
				.append(entitled).append(group1).append(insoptCode)
				.append(insuranceHas).append(insThrough).append(invnum)
				.append(jobType).append(insvaluEntitled).append(minimumPay)
				.append(otherDiscount).append(packDisc).append(section)
				.append(sitDiscount).append(tariff).append(timeFrom)
				.append(timeTo).append(trnsDisc).append(createdBy)
				.append(createdOn).append(updatedOn).append(updatedBy)
				.append(ending).append(domesticSalesCommission)
				.append(displayonAgentTariff).append(companyDivision)
				.append(payMethod).append(internalCostContract)
				.append(contractType).append(owner).append(contractCurrency)
				.append(origContract).append(published).append(payableContractCurrency)
				.append(dmmInsurancePolicy)
				.append(recClearing).append(payClearing).append(fXRateOnActualizationDate)
				.append(clientPostingGroup).append(accountCode)
				.append(accountName).append(storageBillingGroup)
				.append(storageEmail).append(emailPrintOption).append(creditTerms)
				.append(customerFeedback).append(noTransfereeEvaluation).append(oneRequestPerCF)
				.append(ratingScale).append(lumpSum).append(detailedList).append(noInsurance)
				.append(lumpSumAmount).append(lumpPerUnit).append(minReplacementvalue).append(networkPartnerCode)
				.append(networkPartnerName).append(source).append(vendorCode).append(vendorName)
				.append(insuranceOption).append(defaultVat).append(billToAuthorization)
				.append(billingMoment).append(creditTermsCmmAgent)
				.toHashCode();
	}

	@Column(length = 65)
	public Date getEnding() {
		return ending;
	}

	public void setEnding(Date ending) {
		this.ending = ending;
	}

	@Column(length = 65)
	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	@Column(length = 65)
	public String getBillingInstruction() {
		return BillingInstruction;
	}

	public void setBillingInstruction(String billingInstruction) {
		BillingInstruction = billingInstruction;
	}

	@Column(length = 65)
	public String getBillingInstructionCode() {
		return billingInstructionCode;
	}

	public void setBillingInstructionCode(String billingInstructionCode) {
		this.billingInstructionCode = billingInstructionCode;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 65)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length = 2000)
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Column(length = 65)
	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	@Column(length = 254)
	public String getEntitled() {
		return entitled;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}


	@Column(length = 65)
	public String getGroup1() {
		return group1;
	}

	public void setGroup1(String group1) {
		this.group1 = group1;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 65)
	public String getInsoptCode() {
		return insoptCode;
	}

	public void setInsoptCode(String insoptCode) {
		this.insoptCode = insoptCode;
	}

	@Column(length = 65)
	public String getInsThrough() {
		return insThrough;
	}

	public void setInsThrough(String insThrough) {
		this.insThrough = insThrough;
	}

	@Column(length = 65)
	public String getInsuranceHas() {
		return insuranceHas;
	}

	public void setInsuranceHas(String insuranceHas) {
		this.insuranceHas = insuranceHas;
	}

	@Column(length = 65)
	public double getInsvaluEntitled() {
		return insvaluEntitled;
	}

	public void setInsvaluEntitled(double insvaluEntitled) {
		this.insvaluEntitled = insvaluEntitled;
	}

	@Column(length = 1)
	public String getInvnum() {
		return invnum;
	}

	@Column(length = 65)
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	@Column(length = 65)
	public double getMinimumPay() {
		return minimumPay;
	}

	public void setMinimumPay(double minimumPay) {
		this.minimumPay = minimumPay;
	}

	@Column(length = 65)
	public double getOtherDiscount() {
		return otherDiscount;
	}

	public void setOtherDiscount(double otherDiscount) {
		this.otherDiscount = otherDiscount;
	}

	@Column(length = 65)
	public double getPackDisc() {
		return packDisc;
	}

	public void setPackDisc(double packDisc) {
		this.packDisc = packDisc;
	}

	@Column(length = 65)
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(length = 65)
	public double getSitDiscount() {
		return sitDiscount;
	}

	public void setSitDiscount(double sitDiscount) {
		this.sitDiscount = sitDiscount;
	}

	@Column(length = 65)
	public String getTariff() {
		return tariff;
	}

	public void setTariff(String tariff) {
		this.tariff = tariff;
	}

	@Column(length = 65)
	public String getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(String timeFrom) {
		this.timeFrom = timeFrom;
	}

	@Column(length = 65)
	public String getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(String timeTo) {
		this.timeTo = timeTo;
	}

	@Column(length = 65)
	public double getTrnsDisc() {
		return trnsDisc;
	}

	public void setTrnsDisc(double trnsDisc) {
		this.trnsDisc = trnsDisc;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 65)
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 65)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 100)
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public void setInvnum(String invnum) {
		this.invnum = invnum;
	}

	@Column
	public Boolean getDomesticSalesCommission() {
		return domesticSalesCommission;
	}

	public void setDomesticSalesCommission(Boolean domesticSalesCommission) {
		this.domesticSalesCommission = domesticSalesCommission;
	}
	@Column
	public Boolean getDisplayonAgentTariff() {
		return displayonAgentTariff;
	}

	public void setDisplayonAgentTariff(Boolean displayonAgentTariff) {
		this.displayonAgentTariff = displayonAgentTariff;
	}
    
	@Column(length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	@Column(length=3)
	public String getPayMethod() {
		return payMethod;
	}

	@Column   
	public Boolean getInternalCostContract() {
		return internalCostContract;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public void setInternalCostContract(Boolean internalCostContract) {
		this.internalCostContract = internalCostContract;
	}
	@Transient
	public String getListCode() {		
		return contract;
	}
	@Transient
	public String getListDescription() {
		// TODO Auto-generated method stub
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListEigthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListFifthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListFourthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListNinthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListSecondDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListSeventhDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListSixthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListTenthDescription() {
		String contactValue="";
		return contactValue;
	}
	@Transient
	public String getListThirdDescription() {
		String contactValue="";
		return contactValue;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	@Column 
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	@Column
	public String getContractCurrency() {
		return contractCurrency;
	}

	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	
	@Column
	public Long getOrigContract() {
		return origContract;
	}

	public void setOrigContract(Long origContract) {
		this.origContract = origContract;
	}
	
	@Column
	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}
	@Column
	public String getPayableContractCurrency() {
		return payableContractCurrency;
	}

	public void setPayableContractCurrency(String payableContractCurrency) {
		this.payableContractCurrency = payableContractCurrency;
	}
	@Column
	public Boolean getDmmInsurancePolicy() {
		return dmmInsurancePolicy;
	}

	public void setDmmInsurancePolicy(Boolean dmmInsurancePolicy) {
		this.dmmInsurancePolicy = dmmInsurancePolicy;
	}
	@Column
	public String getRecClearing() {
		return recClearing;
	}

	public void setRecClearing(String recClearing) {
		this.recClearing = recClearing;
	}
	@Column
	public String getPayClearing() {
		return payClearing;
	}

	public void setPayClearing(String payClearing) {
		this.payClearing = payClearing;
	}
	@Column
	public Boolean getfXRateOnActualizationDate() {
		return fXRateOnActualizationDate;
	}

	public void setfXRateOnActualizationDate(Boolean fXRateOnActualizationDate) {
		this.fXRateOnActualizationDate = fXRateOnActualizationDate;
	}
	@Column
	public String getClientPostingGroup() {
		return clientPostingGroup;
	}

	public void setClientPostingGroup(String clientPostingGroup) {
		this.clientPostingGroup = clientPostingGroup;
	}
	@Column
	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	@Column
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getStorageBillingGroup() {
		return storageBillingGroup;
	}

	public void setStorageBillingGroup(String storageBillingGroup) {
		this.storageBillingGroup = storageBillingGroup;
	}

	public String getStorageEmail() {
		return storageEmail;
	}

	public void setStorageEmail(String storageEmail) {
		this.storageEmail = storageEmail;
	}

	public String getEmailPrintOption() {
		return emailPrintOption;
	}

	public void setEmailPrintOption(String emailPrintOption) {
		this.emailPrintOption = emailPrintOption;
	}

	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getCustomerFeedback() {
		return customerFeedback;
	}

	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}

	public Boolean getNoTransfereeEvaluation() {
		return noTransfereeEvaluation;
	}

	public void setNoTransfereeEvaluation(Boolean noTransfereeEvaluation) {
		this.noTransfereeEvaluation = noTransfereeEvaluation;
	}

	public Boolean getOneRequestPerCF() {
		return oneRequestPerCF;
	}

	public void setOneRequestPerCF(Boolean oneRequestPerCF) {
		this.oneRequestPerCF = oneRequestPerCF;
	}

	public String getRatingScale() {
		return ratingScale;
	}

	public void setRatingScale(String ratingScale) {
		this.ratingScale = ratingScale;
	}

	public Boolean getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(Boolean lumpSum) {
		this.lumpSum = lumpSum;
	}

	public Boolean getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(Boolean detailedList) {
		this.detailedList = detailedList;
	}

	public Boolean getNoInsurance() {
		return noInsurance;
	}

	public void setNoInsurance(Boolean noInsurance) {
		this.noInsurance = noInsurance;
	}

	public BigDecimal getLumpSumAmount() {
		return lumpSumAmount;
	}

	public void setLumpSumAmount(BigDecimal lumpSumAmount) {
		this.lumpSumAmount = lumpSumAmount;
	}

	public String getLumpPerUnit() {
		return lumpPerUnit;
	}

	public void setLumpPerUnit(String lumpPerUnit) {
		this.lumpPerUnit = lumpPerUnit;
	}

	public String getMinReplacementvalue() {
		return minReplacementvalue;
	}

	public void setMinReplacementvalue(String minReplacementvalue) {
		this.minReplacementvalue = minReplacementvalue;
	}

	public String getNetworkPartnerCode() {
		return networkPartnerCode;
	}

	public void setNetworkPartnerCode(String networkPartnerCode) {
		this.networkPartnerCode = networkPartnerCode;
	}

	public String getNetworkPartnerName() {
		return networkPartnerName;
	}

	public void setNetworkPartnerName(String networkPartnerName) {
		this.networkPartnerName = networkPartnerName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getInsuranceOption() {
		return insuranceOption;
	}

	public void setInsuranceOption(String insuranceOption) {
		this.insuranceOption = insuranceOption;
	}

	public String getDefaultVat() {
		return defaultVat;
	}

	public void setDefaultVat(String defaultVat) {
		this.defaultVat = defaultVat;
	}

	public String getBillToAuthorization() {
		return billToAuthorization;
	}

	public void setBillToAuthorization(String billToAuthorization) {
		this.billToAuthorization = billToAuthorization;
	}

	public String getBillingMoment() {
		return billingMoment;
	}

	public void setBillingMoment(String billingMoment) {
		this.billingMoment = billingMoment;
	}

	public String getCreditTermsCmmAgent() {
		return creditTermsCmmAgent;
	}

	public void setCreditTermsCmmAgent(String creditTermsCmmAgent) {
		this.creditTermsCmmAgent = creditTermsCmmAgent;
	}
	

	
}