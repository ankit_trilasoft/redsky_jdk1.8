package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SurveyResponseDao;
import com.trilasoft.app.model.SurveyResponse;
import com.trilasoft.app.service.SurveyResponseManager;

public class SurveyResponseManagerImpl extends GenericManagerImpl<SurveyResponse, Long> implements SurveyResponseManager{
	SurveyResponseDao surveyResponseDao; 
	public SurveyResponseManagerImpl(SurveyResponseDao surveyResponseDao) {
		super(surveyResponseDao);
		this.surveyResponseDao=surveyResponseDao;
	}

}
