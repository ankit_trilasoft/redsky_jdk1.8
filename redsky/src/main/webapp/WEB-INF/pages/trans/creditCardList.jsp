<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_ACCOUNTING")|| userRole.equalsIgnoreCase("ROLE_ADMIN") || userRole.equalsIgnoreCase("ROLE_FINANCE")){
		userRole=role.getName();
		break;
	}
	
}
%>

<script>
function showMessage(){
	alert("Billing not created.");
}
</script>
  
<head>   
    <title><fmt:message key="creditCardList.title"/></title>   
    <meta name="heading" content="<fmt:message key='creditCardList.heading'/>"/>   
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
<c:set var="buttons">  

    <c:if test="${billingFlag==1}">
	     <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editCosting.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
    <c:if test="${billingFlag==0}">
	     <input type="button" class="cssbuttonA" style="width:55px; height:25px" 
	      onclick="showMessage();" value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editCreditCard.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>
    <!--      
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
        -->
</c:set>   

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />      
<s:form id="searchForm" method="post" validate="true"> 
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <div id="Layer3" style="width:100%;">
  <div id="newmnav">
  
  <ul>
  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
  <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
  <configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
	</configByCorp:fieldVisibility>
  <c:set var="relocationServicesKey" value="" />
  <c:set var="relocationServicesValue" value="" /> 
  <c:forEach var="entry" items="${relocationServices}">
         <c:if test="${relocationServicesKey==''}">
         <c:if test="${entry.key==serviceOrder.serviceType}">
         <c:set var="relocationServicesKey" value="${entry.key}" />
         <c:set var="relocationServicesValue" value="${entry.value}" /> 
         </c:if>
         </c:if> 
     </c:forEach>
 <%--  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li> --%>
      <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
      </c:if>
      <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
             <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
              </c:if>
  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Billing</span></a></li>
  <%-- <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
  <c:choose>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
    </c:otherwise>
  </c:choose> 
  </sec-auth:authComponent> --%>
  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
              <c:choose>
                <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
                  <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>  
               </c:when> --%>
               <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                  <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li> 
               </c:when>
               <c:otherwise>
               <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
               <!-- <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li> -->
              </c:otherwise>
             </c:choose>
             </sec-auth:authComponent> 
             <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
             <c:choose> 
               <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                  <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li> 
               </c:when>
               <c:otherwise> 
               
               <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
               <!-- <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.newAccounting');return autoSaveFunc('none');"><span>Accounting</span></a></li> -->
              </c:otherwise>
             </c:choose>    
             </sec-auth:authComponent>
             <%--  <c:if test="${serviceOrder.job =='OFF'}">     
             <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
             <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.OI');return autoSaveFunc('none');"><span>O&I</span></a></li>
             </sec-auth:authComponent>
             </c:if> --%>
            <%--  <c:if test="${serviceOrder.job !='RLO'}">
             <c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}">             
                <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.forwarding');return autoSaveFunc('none');"><span>Forwarding3</span></a></li>
                </c:if>          
             </c:if> --%>
             </c:if>
  
  <%-- <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
    </c:otherwise>
  </c:choose>
  </sec-auth:authComponent> --%>
    		 
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
  <c:if test="${serviceOrder.job !='RLO'}">  
    <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">
  		<c:if test="${forwardingTabVal!='Y'}"> 
	   		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  </c:if>
	  <c:if test="${forwardingTabVal=='Y'}">
	  		<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  </c:if>
  </c:if>
  </c:if>
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <c:if test="${serviceOrder.job !='RLO'}"> 
   <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </c:if>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </sec-auth:authComponent>
  </c:if>
  <c:if test="${serviceOrder.job =='RLO'}">  
<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
 </c:if>
  <c:if test="${serviceOrder.job !='RLO'}"> 
    <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1  && serviceOrder.corpID=='CWMS')}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  </c:if>
   <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
  <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
  </c:if>
 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
</ul>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>


<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
   
  
<div id="newmnav">   
 <ul>
		  <li class="current"><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing Info</span></a></li>
		  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Credit Card</span></a></li>
		 </ul>
</div><div class="spn">&nbsp;</div>

 
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
	<tr><td>
      <display:table name="creditCards" class="table" requestURI="" id="creditCardList" defaultsort="4" export="true" >   
	  <c:set var="mask" value="${creditCardList.ccNumber}"></c:set>
	  <sec-auth:authComponent componentId="module.creditCard.section.creditCardNumberCOORD.edit">
	    <%;
	        String permitionCreditCardNumber="true";
	        int permissionCreditCard  = (Integer)request.getAttribute("module.creditCard.section.creditCardNumberCOORD.edit" + "Permission");
 	        if (permissionCreditCard > 2 ){
 	        	permitionCreditCardNumber = "false"; 
 	       }
 	    %>
				<%if(permitionCreditCardNumber.equalsIgnoreCase("false")){ %>
				
					 <display:column property="ccName" sortable="true" titleKey="billing.ccName" style="width:120px" url="/editCreditCard.html?sid=${serviceOrder.id}" paramId="id" paramProperty="id"/>
 				<%}else{%>
 			 			 <display:column property="ccName" sortable="true" titleKey="billing.ccName" style="width:120px" />
				<%}%>
</sec-auth:authComponent>
	   
    	
<sec-auth:authComponent componentId="module.creditCard.section.creditCardNumberAdmin.edit">
	    <%;
	        String permitionCreditCardAdmin="true";
	        int permissionadmin  = (Integer)request.getAttribute("module.creditCard.section.creditCardNumberAdmin.edit" + "Permission");
 	        if (permissionadmin > 2 ){
 	        	permitionCreditCardAdmin = "false"; 
 	       } 
  %>
				<%if(permitionCreditCardAdmin.equalsIgnoreCase("false")){ %>
				<display:column property="ccNumber"  sortable="true" titleKey="billing.ccNumber" style="width:150px"/>
			<%}else{%>
 			 			<display:column property="ccNumber1"  sortable="true" titleKey="billing.ccNumber" style="width:150px"/> 	    	   		
<%}%>	
</sec-auth:authComponent>	
 
		<display:column sortable="true" titleKey="billing.ccExpires" format="{0,date,MMM-yyyy}" style="width:75px">
		<c:out value="${creditCardList.expireMonth }"></c:out><c:out value="-"></c:out>
		<c:out value="${creditCardList.expireYear }"></c:out></display:column>
    	<display:column property="ccAuthorizationBillNumber" sortable="true" titleKey="billing.ccAuthorizationBillNumber" style="width:60px"/>
		<display:column property="ccType" sortable="true" titleKey="billing.ccType" style="width:30px"/>
		<display:column property="ccApprovedBillAmount" sortable="true" title="Approved&nbsp;Amount" />
		<display:column property="ccApproved" sortable="true" titleKey="billing.ccApproved" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="ccVlTransDate" sortable="true" titleKey="billing.ccVlTransDate" format="{0,date,dd-MMM-yyyy}"/>
		<display:column  title="Primary">
		<c:if test="${creditCardList.primaryFlag==true }">
		<img src="${pageContext.request.contextPath}/images/tick01.gif"  /> 
		</c:if>
		<c:if test="${creditCardList.primaryFlag!=true }">
		<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
		</c:if>
		</display:column>
    <display:setProperty name="export.excel.filename" value="CreditCard List.xls"/>   
    <display:setProperty name="export.csv.filename" value="CreditCard List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="CreditCard List.pdf"/>   
</display:table>   
  </td></tr></tbody>
  </table>
  </div>
  <sec-auth:authComponent componentId="module.creditCard.section.creditCardNumberCOORD.edit">
	    <%;
	        String permitionCreditCardNumber="true";
	        int permissionCreditCard  = (Integer)request.getAttribute("module.creditCard.section.creditCardNumberCOORD.edit" + "Permission");
 	        if (permissionCreditCard > 2 ){
 	        	permitionCreditCardNumber = "false"; 
 	       }
 	    %>
				<%if(permitionCreditCardNumber.equalsIgnoreCase("false")){ %>
				
					<c:out value="${buttons}" escapeXml="false" />   
 				<%}%>
</sec-auth:authComponent>

 
<%--
<c:set var="mask" value="${creditCardList.ccNumber}"></c:set>
	  
	  <display:column value="${mask}"></display:column>
${mask}
<display:column value="${fn:replace(mask, fn:substring(mask, 0,9),'XXXX-XXX-'}"></display:column>
${fn:replace("mask","ab","*@")}
<authz:authorize ifAllGranted="ROLE_ADMIN">
 <tr>
   

 </tr>
</authz:authorize>
<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
      <a href="skype:rajesh.trilasoft?call">
      <img src="http://mystatus.skype.com/smallicon/rajesh%2Etrilasoft" style="border: medium none ;" alt="My status" height="16" width="16"></a> 
 
--%>
</div>

</s:form>  

<script type="text/javascript">   
    highlightTableRows("creditCardList");   
</script>  
