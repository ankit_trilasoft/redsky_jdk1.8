package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="authorizationno")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class AuthorizationNo extends BaseObject implements ListLinkData{
	private String corpID;
	private Long id;
	private Long serviceOrderId;
	private String authorizationSequenceNumber;
	private String shipNumber;
	private String authorizationNumber;
	private String commodity;
	private String invoiceNumber;
	private Date invoiceDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Long workTicketNumber;
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AuthorizationNo))
			return false;
		AuthorizationNo castOther = (AuthorizationNo) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(serviceOrderId, castOther.serviceOrderId)
				.append(authorizationSequenceNumber,
						castOther.authorizationSequenceNumber).append(
						shipNumber, castOther.shipNumber).append(
						authorizationNumber, castOther.authorizationNumber)
				.append(commodity, castOther.commodity).append(invoiceNumber,
						castOther.invoiceNumber).append(invoiceDate,
						castOther.invoiceDate).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(workTicketNumber,
						castOther.workTicketNumber)
						.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				serviceOrderId).append(authorizationSequenceNumber).append(
				shipNumber).append(authorizationNumber).append(commodity)
				.append(invoiceNumber).append(invoiceDate).append(createdBy)
				.append(updatedBy).append(createdOn)
				.append(updatedOn)
				.append(workTicketNumber)
				.toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("serviceOrderId", serviceOrderId).append(
				"authorizationSequenceNumber", authorizationSequenceNumber)
				.append("shipNumber", shipNumber).append("authorizationNumber",
						authorizationNumber).append("commodity", commodity)
				.append("invoiceNumber", invoiceNumber).append("invoiceDate",
						invoiceDate).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("workTicketNumber", workTicketNumber)
				.toString();
	}
	/*@ManyToOne
	@JoinColumn(name="authorizationNumber", nullable=false, updatable=false, referencedColumnName="billToAuthority")
	public Billing getBilling() {
	        return billing;
	    }
	 public void setBilling(Billing billing) {
			this.billing = billing;
	} 
	 
	@Column(length = 27, insertable=false, updatable=false)*/
	@Column(length = 27)
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}
	@Column(length = 2)
	public String getAuthorizationSequenceNumber() {
		return authorizationSequenceNumber;
	}
	public void setAuthorizationSequenceNumber(String authorizationSequenceNumber) {
		this.authorizationSequenceNumber = authorizationSequenceNumber;
	}
	@Column(length = 15)
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	
	
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	@Column(length = 65)
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Column(length = 15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	} 
	@Transient
	public String getListSecondDescription() {
		return null;
	} 
	
	@Transient
	public String getListThirdDescription() {
		return null;
	}
	
	@Transient
	public String getListFourthDescription() {
		return null;
	}
	
	@Transient
	public String getListFifthDescription() {
		return null;
	}
	
	@Transient
	public String getListSixthDescription() {
		return null;
	}
	@Transient
	public String getListCode() {
		// TODO Auto-generated method stub
		return authorizationNumber;
	}
	@Transient
	public String getListDescription() {
		
		return authorizationNumber;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column
	public Long getWorkTicketNumber() {
		return workTicketNumber;
	}
	public void setWorkTicketNumber(Long workTicketNumber) {
		this.workTicketNumber = workTicketNumber;
	}
}
