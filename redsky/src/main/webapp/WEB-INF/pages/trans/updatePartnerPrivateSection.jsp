<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Update Additional Info</title>   
    <meta name="heading" content="Update Additional Info"/> 
<style>
 

 </style>
<script type="text/javascript">
 function updateChildFromParent()
{
	 var pagePrsListType="";
	 var pageCPFListType="";
	 var pageFaqInfo="";
	 try{
	 	 var controlInfo = document.getElementById("controlInfo");
	 	 var defaultRoles = document.getElementById("defaultRoles");
	 	 var paymentMethod = document.getElementById("paymentMethod");
	 	 var billingInstruction = document.getElementById("billingInstruction");
	 	 var insurance = document.getElementById("insurance");	
	 	 var creditTerms = document.getElementById("creditTerms");	
	 	 var qualityMeasurement = document.getElementById("qualityMeasurement");	
	 	 var entitlement = document.getElementById("entitlement");
	 	 var accountAssignmentType = document.getElementById("accountAssignmentType");	
	 	 var insuranceSet=document.getElementById("insuranceSet");
	 	 var storageBillingGroup = document.getElementById("storageBillingGroup");
	 	 var defaultRLORoles = document.getElementById("defaultRLORoles");
	 	var contract = document.getElementById("contract");
	 	var networkPartnerCode = document.getElementById("networkPartnerCode");
	 	var source = document.getElementById("source");
	 	var vendorCode = document.getElementById("vendorCode");
	 	var insuranceHas = document.getElementById("insuranceHas");
	 	var insuranceOption = document.getElementById("insuranceOption");
	 	var defaultVat = document.getElementById("defaultVat");
	 	var billToAuthorization = document.getElementById("billToAuthorization");
	 	var rddBased = document.getElementById("rddBased");
	 	var rddDays = document.getElementById("rddDays");
	 	var storageEmailType = document.getElementById("storageEmailType");
	 	var billingCycle = document.getElementById("billingCycle");
	 	var policyInfo = document.getElementById("policyInfo");
	 	var faqInfo = document.getElementById("faqInfo");
	 	var servicesInfo = document.getElementById("servicesInfo");
	 	 }catch(e){}	
	 	 var checkedOption="";
	 	 if(controlInfo.checked==true) {
	 		checkedOption=checkedOption+"controlInfo";
	 	 }
	 	 if(defaultRoles.checked==true) {
		 		checkedOption=checkedOption+"defaultRoles";
	 	 }
	 	 if(paymentMethod.checked==true) {
		 		checkedOption=checkedOption+"paymentMethod";
	 	 }
	 	 if(billingInstruction.checked==true) {
		 		checkedOption=checkedOption+"billingInstruction";
	 	 }
	 	 if(insurance.checked==true) {
		 		checkedOption=checkedOption+"insurance";
	 	 }
	 	 if(creditTerms.checked==true) {
		 		checkedOption=checkedOption+"creditTerms";
	 	 }
		 try{
	 	 if(qualityMeasurement.checked==true) {
		 		checkedOption=checkedOption+"qualityMeasurement";
	 	 }
	 	 }catch(e){}	
	 	 if(entitlement.checked==true) {
		 		checkedOption=checkedOption+"entitlement";
		 }
	 	if(accountAssignmentType.checked==true) {
	 		checkedOption=checkedOption+"accountAssignmentType";
 	 	}
	 	try{
	 	if(insuranceSet.checked==true){
	 		checkedOption=checkedOption+"InsuranceSetUp";
	 	}
	 	if(storageBillingGroup.checked==true){
	 		checkedOption=checkedOption+"storageBillingGroup";
	 	}
	 	
	 	}catch(e){
	 		
	 	}
	 	 try{
		 	 if(defaultRLORoles.checked==true) {
			 	checkedOption=checkedOption+"defaultRLORoles";
		 	 }
		 	 }catch(e){}
		 if(contract.checked==true) {
		 		checkedOption=checkedOption+"contract";
	 	 }
		 if(networkPartnerCode.checked==true) {
		 		checkedOption=checkedOption+"networkPartnerCode";
	 	 }
		 if(source.checked==true) {
		 		checkedOption=checkedOption+"source";
	 	 }
		 if(vendorCode.checked==true) {
		 		checkedOption=checkedOption+"vendorCode";
	 	 }
		 if(insuranceHas.checked==true) {
		 		checkedOption=checkedOption+"insuranceHas";
	 	 }
		 if(insuranceOption.checked==true) {
		 		checkedOption=checkedOption+"insuranceOption";
	 	 }
		 if(defaultVat.checked==true) {
		 		checkedOption=checkedOption+"defaultVat";
	 	 }
		 if(billToAuthorization.checked==true) {
		 		checkedOption=checkedOption+"billToAuthorization";
	 	 }
		 if(rddBased.checked==true) {
		 		checkedOption=checkedOption+"rddBased";
	 	 }
		 if(rddDays.checked==true) {
		 		checkedOption=checkedOption+"rddDays";
	 	 }
	 	 if(storageEmailType.checked==true) {
	 		checkedOption=checkedOption+"storageEmailType";
 	 	 }
		 if(billingCycle.checked==true) {
		 		checkedOption=checkedOption+"billingCycle";
	 	 	 }
		 if(policyInfo.checked==true) {
		 		checkedOption=checkedOption+"policyInfo";
	 	 	 }
		 if(faqInfo.checked==true) {
		 		checkedOption=checkedOption+"faqInfo";
	 	 	 }
		 if(servicesInfo.checked==true) {
		 		checkedOption=checkedOption+"servicesInfo";
	 	 	 }
		 if(servicesInfo.checked==true){
			 pagePrsListType='PRS';
			 
		 }
		 if(policyInfo.checked==true){
			 pageCPFListType='CPF';
			
		 }
		 if(faqInfo.checked==true){
			 pageFaqInfo='FAQ';
			
		 }
	 	 if(checkedOption.trim()!="") {
		 	 //Modified for ticket number 6477
			  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&checkedOption="+checkedOption+"&pagePrsListType="+pagePrsListType+"&pageCPFListType="+pageCPFListType+"&pageFaqInfo="+pageFaqInfo+"&id=${partnerPrivate.id}&pageListType=AIS&partnerCode=${partnerPrivate.partnerCode}";			
			  http10.open("GET", url, true); 
			  http10.onreadystatechange = handleHttpResponse; 
		      http10.send(null); 
	 	 }else{
	 		 alert("Please Select One Option.");
	 	 }
}
function handleHttpResponse(){
    if (http10.readyState == 4){
    	var results = http10.responseText
	        results = results.trim();
				if(results!="")	{
					alert(results);	
					self.close();
				} 
    	} 
}

var http10 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function closeWindow()
{
	self.close();
}


	var checkflag = "false";
	function check(field) {
		 if (checkflag == "false") {
	    for (i = 0; i < field.length; i++) {
	      field[i].checked = true;
	    }
	    checkflag = "true";
	     document.getElementById('CheckAll').style.display = 'none';
	     document.getElementById('UncheckAll').style.display = 'block';
	    return "Uncheck All";
	  } else {
	    for (i = 0; i < field.length; i++) {
	      field[i].checked = false;
	    }
	    checkflag = "false";
	     document.getElementById('UncheckAll').style.display = "none";
	    document.getElementById('CheckAll').style.display = "block";
	    return "Check All";
	  }}
</script>
 </head>
<s:form name="updateChildForm" action="" method="post" validate="true">
<c:set var="buttons">   
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="updateChildFromParent();" value="Update"/>
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="closeWindow();" value="Cancel"/>
</c:set> 

<table style="margin-top:-10px;" class="table">

<tr><td class="listwhitetext" width="10px"><input type="checkbox"  value="Check All" onclick="this.value=check(this.form)"/></td><td><div id="CheckAll" class="listwhitetext">
Check All
</div>
<div id="UncheckAll" style="display:none" class="listwhitetext" >
Uncheck All
</div></td></tr>
<tr><td class="subcontenttabChild" colspan="5">Update The Following Sections In Child Accounts.</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="controlInfo" name="controlInfo"/></td><td  align="left" class="listwhitetext" >Control Info</td>
<td class="listwhitetext" width=""><s:checkbox id="defaultRoles" name="defaultRoles"/></td><td  align="left" class="listwhitetext" width="150" >Default Roles</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="paymentMethod" name="paymentMethod"/></td><td  align="left" class="listwhitetext" >Payment Method</td>
<td class="listwhitetext" width="10px"><s:checkbox id="billingInstruction" name="billingInstruction"/></td><td  align="left" class="listwhitetext" >Billing Instruction</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insurance" name="insurance"/></td><td  align="left" class="listwhitetext" >Insurance</td>
<td class="listwhitetext" width="10px"><s:checkbox id="creditTerms" name="creditTerms"/></td><td  align="left" class="listwhitetext" >Credit Terms</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="entitlement" name="entitlement"/></td><td  align="left" class="listwhitetext" >Entitlement</td>
<td class="listwhitetext" width="10px"><s:checkbox id="accountAssignmentType" name="accountAssignmentType"/></td><td  align="left" class="listwhitetext" >Assignment&nbsp;Type</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insuranceSet" name="insuranceSet"/></td><td  align="left" class="listwhitetext" >Insurance Setup</td>
<td class="listwhitetext" width="10px"><s:checkbox id="storageBillingGroup" name="storageBillingGroup"/></td><td  align="left" class="listwhitetext" >Storage Billing Group</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultRLORoles" name="defaultRLORoles"/></td><td  align="left" class="listwhitetext" >Default&nbsp;Roles&nbsp;for&nbsp;Relocation</td>
<td class="listwhitetext" width="10px"><s:checkbox id="contract" name="contract"/></td><td  align="left" class="listwhitetext" >Default Contract</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="networkPartnerCode" name="networkPartnerCode"/></td><td  align="left" class="listwhitetext" >Default Network Code</td>
<td class="listwhitetext" width="10px"><s:checkbox id="vendorCode" name="vendorCode"/></td><td  align="left" class="listwhitetext" >Default Vendor Code</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="source" name="source"/></td><td  align="left" class="listwhitetext" >Default Source</td>
<td class="listwhitetext" width="10px"><s:checkbox id="insuranceHas" name="insuranceHas"/></td><td  align="left" class="listwhitetext" >Default Has Valuation</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insuranceOption" name="insuranceOption"/></td><td  align="left" class="listwhitetext" >Default Insurance Option</td>
<td class="listwhitetext" width="10px"><s:checkbox id="defaultVat" name="defaultVat"/></td><td  align="left" class="listwhitetext" >Default Vat Desc</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="rddBased" name="rddBased"/></td><td  align="left" class="listwhitetext" >Default RDD Based</td>
<td class="listwhitetext" width="10px"><s:checkbox id="rddDays" name="rddDays"/></td><td  align="left" class="listwhitetext" >Default # Of Days</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="billToAuthorization" name="billToAuthorization"/></td><td  align="left" class="listwhitetext" >Default Account PO#/SAP Number</td>
<td class="listwhitetext" width="10px"><s:checkbox id="storageEmailType" name="storageEmailType"/></td><td  align="left" class="listwhitetext" >Storage Email Type</td>
</tr>
<tr>
<configByCorp:fieldVisibility componentId="component.field.partner.QualityMeasurementUTSI">
<c:if test="${company.qualitySurvey==true}">  
<tr><td class="listwhitetext" width="10px"><s:checkbox id="qualityMeasurement" name="qualityMeasurement"/></td><td class="listwhitetext"  align="left" >Quality Measurements</td>
</c:if>
</configByCorp:fieldVisibility>
<td class="listwhitetext" width="10px"><s:checkbox id="billingCycle" name="billingCycle"/></td><td  align="left" class="listwhitetext" >Billing Cycle</td>

</tr>
<tr><td class="subcontenttabChild" colspan="4">Update The Policy Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="policyInfo" name="policyInfo" /></td><td  align="left" class="listwhitetext" >Policy</td></tr>

<tr><td class="subcontenttabChild" colspan="4">Update The FAQ Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="faqInfo" name="faqInfo"/></td><td  align="left" class="listwhitetext" >FAQ</td></tr>

<tr><td class="subcontenttabChild" colspan="4">Update The Services Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="servicesInfo" name="servicesInfo"/></td><td  align="left" class="listwhitetext" >Services</td></tr>
</table>
<tr><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr>  
</s:form>
 <script type="text/javascript">
try{ 
//alert('${partnerPrivate.excludeFromParentUpdate}');
}
catch(e){}
</script>