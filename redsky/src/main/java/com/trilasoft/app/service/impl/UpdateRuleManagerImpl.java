package com.trilasoft.app.service.impl;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.UpdateRuleDao;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.UpdateRule;
import com.trilasoft.app.model.UpdateRuleResult;
import com.trilasoft.app.service.UpdateRuleManager;
import com.trilasoft.app.service.UpdateRuleResultManager;
import com.trilasoft.app.service.UserGuidesManager;

public class UpdateRuleManagerImpl  extends GenericManagerImpl<UpdateRule, Long> implements UpdateRuleManager
{
	private List updaterule;
	UpdateRuleDao updateRuleDao;
	
	public UpdateRuleManagerImpl(UpdateRuleDao updateRuleDao) 
	{
		super(updateRuleDao);
		this.updateRuleDao=updateRuleDao;
		// TODO Auto-generated constructor stub
	}
	public List getByCorpID(String sessionCorpID )
	{
		return updateRuleDao.getByCorpID( sessionCorpID );
	} 
	
	public List<DataCatalog> selectfield(String entity,String corpID) {
		return updateRuleDao.selectfield(entity,corpID);
	}
	public List<DataCatalog> selectDatefield(String corpID) {
		return updateRuleDao.selectDatefield(corpID);
	}
	public void changestatus(String status, Long id) {
		updateRuleDao.changestatus(status,id);
	}
	public List searchById(Long id,String  fieldToUpdate,String  validationContion,String  ruleStatus) {
		//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return updateRuleDao.searchById(id, fieldToUpdate, validationContion, ruleStatus);
		
	}
	public String executeTestRule(UpdateRule updateRule, String sessionCorpID){
		String message = "";
		List<Object[]> updateRuleResults = null;
			try{
				updateRuleResults = executeRule(updateRule, null, sessionCorpID);
				if(updateRuleResults!=null && !updateRuleResults.isEmpty() && updateRuleResults.get(0)!=null){
				 Object[] tdr = updateRuleResults.get(0);
				 return null;
				}
			}catch(Exception ex){
				message = ex.getMessage();
				return message;
			}
		return null;	
	}
	private List executeRule(UpdateRule updateRule, Long recordID, String sessionCorpID) {
		String ruleExpression ="";
		try{
			ruleExpression= buildRuleExpression(updateRule, recordID, sessionCorpID);
		}catch(Exception e){
			e.printStackTrace();
		}
		return updateRuleDao.executeRule(ruleExpression);
	}
	
	public String buildRuleExpression(UpdateRule updateRule, Long recordID, String sessionCorpID){
		String baseExpression = updateRule.getValidationContion();
		String entity = updateRule.getTableName();
		if (entity.equalsIgnoreCase("TrackingStatus")) {
			entity = "ServiceOrder";		
		}else if (entity.equalsIgnoreCase("LeadCapture")) {
			entity = "CustomerFile";
		}
		
		String ruleExpression =  UpdateRuleExpressionBuilder.buildExpression(entity, baseExpression, recordID, sessionCorpID);
			String tempRuleExpression ="";
				tempRuleExpression = "Select "+updateRule.getUpdateFieldValue() + " " + " , " + getJoinid(entity) +" " +","+ // 0,1
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + // 2
				ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + //3
				ToDoRuleExpressionBuilder.getBilling(entity)+"," + //4
				ToDoRuleExpressionBuilder.getPayable(entity)+"," + //5
				ToDoRuleExpressionBuilder.getPricing(entity)+"," + //6
				ToDoRuleExpressionBuilder.getConsultant(entity)+"," + //7
				ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + //8
				ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + //9
				ToDoRuleExpressionBuilder.getFileNumber(entity)+","+ //10
				ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ //11
				ToDoRuleExpressionBuilder.getBillToName(entity); //12
				ruleExpression  =tempRuleExpression+ ruleExpression;	
		
		System.out.println("\n\n\n ruleID-->>"+updateRule.getId());
		System.out.println("\n\n\n Expression-->>"+ruleExpression);
		return ruleExpression;			
	}
	public static String getJoinid(String entity) {
		String joinid = entity;		
		if (entity.equals("ServiceOrder")){
			joinid = " serviceorder.id, serviceorder.id";
		}else if (entity.equals("CustomerFile")){
			joinid = " customerfile.id, customerfile.id";
		}else if (entity.equals("QuotationFile")){
			joinid = " customerfile.id, customerfile.id";
		}else if (entity.equals("ServicePartner")){
			joinid = " servicepartner.id, servicepartner.id";
		}else if (entity.equals("Vehicle")){
			joinid = " vehicle.id, vehicle.id";
		}else if (entity.equals("AccountLine")){
			joinid = " accountline.id, serviceorder.id as id1";
		}else if (entity.equals("WorkTicket")){
			joinid = " workticket.id, workticket.id as id1";
		}else if (entity.equals("AccountLineList")){
			joinid = " serviceorder.id, serviceorder.id";
		}else if (entity.equals("Billing")){
			joinid = " billing.id, billing.id";
		}else if (entity.equals("Miscellaneous")){
			joinid = " miscellaneous.id, miscellaneous.id";
		}else if (entity.equals("DspDetails")){
			joinid = " dspdetails.id, serviceorder.id";
		}else if (entity.equals("Claim")){
			joinid = " claim.id, claim.id";
		}
		else if (entity.equals("CreditCard")){
			joinid = " billing.id, billing.id";
		}
		return joinid;
	}
	private List toDoRules;
	private UpdateRuleResultManager updateRuleResultManager;
	public synchronized List executeThisRule(UpdateRule updateRule, String sessionCorpID, String userName){

		/*Map<String, Long> todoResultTempMap = new HashMap<String, Long>();
		List<UpdateRuleResult> byRuleToDoResult = (List<UpdateRuleResult>) updateRuleDao.getToDoResultByRuleNumber(updateRule.getRuleNumber().toString());
		for (UpdateRuleResult toDoResult : byRuleToDoResult) {
			String key = toDoResult.getFileNumber() + "-"
					+ toDoResult.getRuleNumber() + "-"
					+ toDoResult.getTodoRuleId() + "-" + toDoResult.getCorpID();
			todoResultTempMap.put(key, toDoResult.getId());
		}*/

		//toDoRuleDao.deleteThisResults(todoRule.getId(),sessionCorpID,todoRule.getRolelist());
		//List<ToDoRule> toDoRules = toDoRuleDao.getThisRule(todoRule.getId());
		//List<Object[]> todoResults = executeRule(toDoRules.get(0), null, sessionCorpID);
		List<Object[]> todoResults = executeRule(updateRule, null, sessionCorpID);
		if(todoResults!=null){
			toDoRules=new ArrayList();	
		try{
			for (Object[] toDoRuleResult: todoResults){	
			try{	
			Object id = toDoRuleResult[1];
			Object id1=toDoRuleResult[2];
			UpdateRuleResult toDoResult = new UpdateRuleResult();
			    toDoResult.setCorpID(sessionCorpID);
			    toDoResult.setTableName(updateRule.getTableName());
			    toDoResult.setFieldName(updateRule.getFieldToUpdate());
			    toDoResult.setRuleName(updateRule.getRuleName());
			    toDoResult.setValueOfField(toDoRuleResult[0].toString());
			    List l = updateRuleDao.findParterNameList(toDoRuleResult[0].toString(),sessionCorpID);
			    if(l!=null && !l.isEmpty()){
			    	toDoResult.setUpdatefieldValue(l.get(0).toString());
			    }
			    toDoResult.setOrderId(Long.parseLong(id.toString()));
			    toDoResult.setShipNumber(toDoRuleResult[12].toString());
			    toDoResult.setRuleNumber(updateRule.getRuleNumber());
			    toDoResult.setUpdatedRuleId(updateRule.getId());
			    toDoResult.setUpdatedBy(userName);
			    toDoResult.setUpdatedOn(new Date());
			    toDoResult.setCreatedBy(userName);
			    toDoResult.setCreatedOn(new Date());
			    toDoResult = updateRuleResultManager.save(toDoResult);			
		  }catch(Exception ex){	
				System.out.println("\n\n\n\n\n exception--->"+ex);	
				toDoRules=null;
		}
		}	
		}catch(Exception ex){	
			System.out.println("\n\n\n\n\n exception--->"+ex);
			toDoRules=null;
		}
		}else{
			toDoRules=null;	
		}
		return toDoRules;			
	
	}
	public List getToDoRules() {
		return toDoRules;
	}
	public void setToDoRules(List toDoRules) {
		this.toDoRules = toDoRules;
	}
	public <UpdateRuleResult> List getToDoResultByRuleNumber(String ruleNumber){
		return updateRuleDao.getToDoResultByRuleNumber(ruleNumber);
	}
	public void setUpdateRuleResultManager(
			UpdateRuleResultManager updateRuleResultManager) {
		this.updateRuleResultManager = updateRuleResultManager;
	}
	public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId){
		updateRuleDao.deleteUpdateByRuleNumber(ruleNumber, corpId);
	}
	public List findNumberOfResultEntered(String sessionCorpID, Long id){
		return updateRuleDao.findNumberOfResultEntered(sessionCorpID, id);
	}
	public List findMaximum(String sessionCorpID){
		return updateRuleDao.findMaximum(sessionCorpID);
		}
	public void updateResults(Long ruleNumber){
		updateRuleDao.updateResults(ruleNumber);
	}
	}

