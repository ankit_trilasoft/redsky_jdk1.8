// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company` ADD COLUMN `forceSoDashboard` BOOLEAN DEFAULT 0 AFTER `vatInsurancePremiumTax`,
 ADD COLUMN `forceDocCenter` BOOLEAN DEFAULT 0 AFTER `forceSoDashboard`;
 
ALTER TABLE `redsky`.`trackingstatus`
ADD COLUMN `originGivenCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `tmss` ,
ADD COLUMN `originGivenName` VARCHAR(255) NULL DEFAULT NULL  AFTER `originGivenCode` ,
ADD COLUMN `originReceivedCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `originGivenName` ,
ADD COLUMN `originReceivedName` VARCHAR(255) NULL DEFAULT NULL  AFTER `originReceivedCode` ,
ADD COLUMN `destinationGivenCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `originReceivedName` ,
ADD COLUMN `destinationGivenName` VARCHAR(255) NULL DEFAULT NULL  AFTER `destinationGivenCode` ,
ADD COLUMN `destinationReceivedCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `destinationGivenName` ,
ADD COLUMN `destinationReceivedName` VARCHAR(255) NULL DEFAULT NULL  AFTER `destinationReceivedCode`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `storageVatExclude` bit(1) DEFAULT b'0' AFTER `fXRateOnActualizationDate`,
 ADD COLUMN `insuranceVatExclude` bit(1) DEFAULT b'0' AFTER `storageVatExclude`;
 
 ALTER TABLE `redsky`.`serviceorder`  add column isUpdater bit(1);
 
 insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('Billing','storageVatExclude' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','storageVatExclude' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatExclude' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatExclude' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');
      
      

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `reciprocityjobtype` VARCHAR(100) DEFAULT '';

ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `estmatedSalesTax` DECIMAL(19,2),ADD COLUMN `revisionSalesTax` DECIMAL(19,2);

ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `printOnForm` BIT(1) NULL DEFAULT b'0';

CREATE TABLE `storagebillingmonitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpId` varchar(15) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `billingType` varchar(2) DEFAULT NULL,
  `billThroughFrom` datetime DEFAULT NULL,
  `billThroughTo` datetime DEFAULT NULL,
  `bookerCode` varchar(15) DEFAULT NULL,
  `storageBillingGroup` varchar(500) DEFAULT NULL,
  `contractType` varchar(5) DEFAULT NULL,
  `incExcGroup` varchar(2) DEFAULT NULL,
  `invoiceDate` datetime DEFAULT NULL,
  `postingDate` datetime DEFAULT NULL,
  `invoice` bit(1) DEFAULT b'0',
  `billingFlag` bit(1) DEFAULT b'0',
  `totalCount` bigint(5) DEFAULT NULL,
  `presentCount` bigint(5) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

// Already run on production

delete from networkdatafields where modelName='TrackingStatus' and fieldName in ('requestGBL','recivedGBL','gbl','daShipmentNum','gsaiffFundingFlag','mailEvaluation','rating','tmss','recivedEvaluation','paymentRecived','missedRDDNotification','reason','SITNotify','mailClaim','requiredDeliveryDays','requiredDeliveryDate');
delete from networkdatafields where modelName='Miscellaneous' and fieldName in ('millitaryShipment','fromNonTempStorage','dp3','shortFuse','fromNonTempStorage','weaponsIncluded','originDocsToBase','dd1840','originScore','millitarySurveyDate','millitarySurveyResults','preApprovalDate1','preApprovalRequest1','preApprovalDate2','preApprovalRequest2','preApprovalDate3','preApprovalRequest3','printGbl','actualPickUp','millitaryActualWeightDate','millitarySitDestinationDate','millitaryDeliveryDate');
delete from networkdatafields where modelName='ServiceOrder' and fieldName in ('shipmentType','originMilitary');