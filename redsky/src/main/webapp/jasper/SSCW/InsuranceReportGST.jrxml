<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="InsuranceReportGST"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1578"
		 pageHeight="595"
		 columnWidth="1548"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Begin Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End Date" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[SELECT "InsuranceReportGST-R-SSCW-XLS.jrxml",
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`companyDivision` AS serviceorder_companyDivision,
     serviceorder.`bookingAgentName` AS serviceorder_bookingAgentName,
     serviceorder.`originCity` AS serviceorder_originCity,
     serviceorder.`originState` AS serviceorder_originState,
     serviceorder.`originCountryCode` AS serviceorder_originCountryCode,
     serviceorder.`destinationCity` AS serviceorder_destinationCity,
     serviceorder.`destinationState` AS serviceorder_destinationState,
     serviceorder.`destinationCountryCode` AS serviceorder_destinationCountryCode,
     serviceorder.`job` AS serviceorder_job,
     serviceorder.`routing` AS serviceorder_routing,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`commodity` AS serviceorder_commodity,
     serviceorder.`coordinator` AS serviceorder_coordinator,
     trackingstatus.`destinationAgent` AS trackingstatus_destinationAgent,
     trackingstatus.`originAgent` AS trackingstatus_originAgent,
     trackingstatus.`packA` AS trackingstatus_packA,
     trackingstatus.`loadA` AS trackingstatus_loadA,
     billing.`billtoname` AS billing_billtoname,
     billing.`insuranceValueActual` AS billing_insuranceValueActual,
     billing.`vendorCode` AS billing_vendorCode,
     billing.`insuranceHas` AS billing_insuranceHas,
     billing.`vendorName` AS billing_vendorName,
     if(serviceorder.`commodity`='AUTO',0.00,if(serviceorder.`mode`='Air',miscellaneous.actualGrossWeight,miscellaneous.actualNetWeight)) AS DYNWeight
FROM
     `serviceorder` serviceorder inner join `trackingstatus` trackingstatus on trackingstatus.id=serviceorder.id and trackingstatus.corpid=$P{Corporate ID} 
     inner join `billing` billing on billing.id=serviceorder.id and billing.corpid=$P{Corporate ID}
     inner join `miscellaneous` miscellaneous on miscellaneous.id=serviceorder.id and miscellaneous.corpid=$P{Corporate ID}
where serviceorder.corpid=$P{Corporate ID} 
and billing.billtocode in ('P5150','T16684','T81129','T81169')
and serviceorder.`companyDivision`='SSC'
and DATE_FORMAT(trackingstatus.`loadA`,'%Y-%m-%d')>=$P{Begin Date} AND DATE_FORMAT(trackingstatus.`loadA`,'%Y-%m-%d')<=$P{End Date}]]></queryString>

	<field name="InsuranceReportGST-R-SSCW-XLS.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_bookingAgentName" class="java.lang.String"/>
	<field name="serviceorder_originCity" class="java.lang.String"/>
	<field name="serviceorder_originState" class="java.lang.String"/>
	<field name="serviceorder_originCountryCode" class="java.lang.String"/>
	<field name="serviceorder_destinationCity" class="java.lang.String"/>
	<field name="serviceorder_destinationState" class="java.lang.String"/>
	<field name="serviceorder_destinationCountryCode" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_commodity" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="trackingstatus_destinationAgent" class="java.lang.String"/>
	<field name="trackingstatus_originAgent" class="java.lang.String"/>
	<field name="trackingstatus_packA" class="java.sql.Timestamp"/>
	<field name="trackingstatus_loadA" class="java.sql.Timestamp"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="billing_insuranceValueActual" class="java.math.BigDecimal"/>
	<field name="billing_vendorCode" class="java.lang.String"/>
	<field name="billing_insuranceHas" class="java.lang.String"/>
	<field name="billing_vendorName" class="java.lang.String"/>
	<field name="DYNWeight" class="java.math.BigDecimal"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="56"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="1548"
						height="26"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font size="18"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["GST Insurance Report"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="415"
						y="26"
						width="100"
						height="30"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["OCITY"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="515"
						y="26"
						width="25"
						height="30"
						key="textField-4"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["OSTATE"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="540"
						y="26"
						width="30"
						height="30"
						key="textField-5"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["OCNTRYCD"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="570"
						y="26"
						width="100"
						height="30"
						key="textField-6"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["OA_NAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="670"
						y="26"
						width="100"
						height="30"
						key="textField-7"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DCITY"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="770"
						y="26"
						width="25"
						height="30"
						key="textField-8"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DSTATE"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="795"
						y="26"
						width="30"
						height="30"
						key="textField-9"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DCNTRYCD"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="825"
						y="26"
						width="100"
						height="30"
						key="textField-10"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DA_NAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="925"
						y="26"
						width="40"
						height="30"
						key="textField-11"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["JOB"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="965"
						y="26"
						width="40"
						height="30"
						key="textField-12"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["ROUTING"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1005"
						y="26"
						width="40"
						height="30"
						key="textField-13"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["MODE"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1045"
						y="26"
						width="31"
						height="30"
						key="textField-14"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["COMMODITY"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1276"
						y="26"
						width="70"
						height="30"
						key="textField-15"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["COORD"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1076"
						y="26"
						width="30"
						height="30"
						key="textField-16"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DYNWEIGHT"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1106"
						y="26"
						width="50"
						height="30"
						key="textField-17"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["INSURANCE_VALUE"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1156"
						y="26"
						width="60"
						height="30"
						key="textField-18"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["PACKING_A"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1216"
						y="26"
						width="60"
						height="30"
						key="textField-19"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["LOADING_A"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1346"
						y="26"
						width="84"
						height="30"
						key="textField-20"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["BILLTONAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1467"
						y="26"
						width="81"
						height="30"
						key="textField-21"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["VENDOR_NAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1430"
						y="26"
						width="37"
						height="30"
						key="textField-22"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["VALUATION"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="26"
						width="100"
						height="30"
						key="textField-23"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["SO_NUMBER"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="100"
						y="26"
						width="90"
						height="30"
						key="textField-24"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["LAST_NAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="190"
						y="26"
						width="90"
						height="30"
						key="textField-25"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["FIRST_NAME"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="280"
						y="26"
						width="135"
						height="30"
						key="textField-27"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["BOOKING_AGENT_NAME"]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="40"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="100"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="100"
						y="0"
						width="90"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_lastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="190"
						y="0"
						width="90"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_firstName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="280"
						y="0"
						width="135"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_bookingAgentName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="415"
						y="0"
						width="100"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_originCity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="515"
						y="0"
						width="25"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_originState}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="540"
						y="0"
						width="30"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_originCountryCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="570"
						y="0"
						width="100"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_originAgent}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="670"
						y="0"
						width="100"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_destinationCity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="770"
						y="0"
						width="25"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_destinationState}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="795"
						y="0"
						width="30"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_destinationCountryCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="825"
						y="0"
						width="100"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_destinationAgent}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="925"
						y="0"
						width="40"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="965"
						y="0"
						width="40"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_routing}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1005"
						y="0"
						width="40"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1045"
						y="0"
						width="31"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_commodity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1276"
						y="0"
						width="70"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1076"
						y="0"
						width="30"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{DYNWeight}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1106"
						y="0"
						width="50"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{billing_insuranceValueActual}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1156"
						y="0"
						width="60"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{trackingstatus_packA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1216"
						y="0"
						width="60"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{trackingstatus_loadA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1346"
						y="0"
						width="84"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billtoname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1467"
						y="0"
						width="81"
						height="40"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_vendorName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1430"
						y="0"
						width="37"
						height="40"
						key="textField-2"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_insuranceHas}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
