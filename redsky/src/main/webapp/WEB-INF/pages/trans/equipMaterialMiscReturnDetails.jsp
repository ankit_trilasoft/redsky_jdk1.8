<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Miscellaneous Assignment</title> 
<meta name="heading" content="Resource Limit"/>
<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
</style>
<script language="JavaScript">
function closePopup(){
	//alert("hiii");
	document.getElementById('saveBox').style.display="none";
	document.getElementById('searchBox111').style.display="block";
}
	function assignResources(){
		var category = document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.category'].value;
		if(category==''){
			alert('Please select the category.');
			return false;
		}
		window.open("assignEquipMaterial.html?category="+category+"&flagType=All&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=500, height=500,top=0, left=0,menubar=no")
	}
	
	
	function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==36); 
	} 
	
	function validate(){
		
		//alert("hiookloml");
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.category'].value==''){
			alert('Please select the category.');
			return false;
		}
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.resource'].value==''){
			alert('Please select resource.');
			return false;
		}
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.resourceLimit'].value==''){
			alert('Please enter the default resource resourceLimit.');
			return false;
		}
		
	}

	var count=0;
	var shiftCount=0;
	function onlyDoubleValue(evt,targetElement){
	  var keyCode = evt.keyCode || evt.which;
	  targetVal=targetElement.value;
	  if(keyCode==16){
	  shiftCount++;
	  return true;
	  }
	  if(targetVal.indexOf('.')>-1){
	   
	  }
	  else{
	  count=0;
	  }
	  if(((keyCode==110)||((keyCode==190)))  && (count==0) && (targetVal!='')){
	   count++;
	   return true;
	  }
	  else if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
	    return true;
	  }
	  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
	  return true;
	  }
	  else{
	   return false;
	  }
	}

	function shiftValueUpdate(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==16){
	shiftCount=0;
	}
	}
	function onlyNumberValue(evt){
		  var keyCode = evt.keyCode || evt.which;
		  if(keyCode==16){
		  shiftCount++;
		  return true;
		  }	  
		  if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
		    return true;
		  }
		  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
		  return true;
		  }
		  else{
		   return false;
		  }
		}

	function dotNotAllowed(target,fieldName){
		var dotValue=target.value;
		if(dotValue=='.' && dotValue.length==1 ){
			alert("Please enter valid Weight or Volume.");
			document.forms['operationsResourceLimits'].elements[fieldName].value="";
			document.forms['operationsResourceLimits'].elements[fieldName].value.focus();
			return false;
		    }
	     }
	function pickOldValue()
	{
	 document.forms['operationsResourceLimits'].elements['dummsectionName'].value='${operationsResourceLimits.resource}';
	}
</script>
</head>

<s:form id="operationsResourceLimits" name="operationsResourceLimits" action="saveMiscellaneousRequestReturnList.html" method="post" validate="true">
<%-- <s:hidden name="equipMaterialsLimits.qty"/> --%>
<s:hidden name="hub" value="<%=request.getParameter("hub")%>" />
<s:hidden name="operationsResourceLimits.hubId" value="${hub}"/>
<s:hidden name="dummsectionName" />
<s:hidden name="ship" value="MS" />
<s:hidden name="id"/>
<s:hidden name="isActQtyChng"/>
<s:hidden name="isRetQtyChng"/>
<s:hidden name="resourceId" />
<s:hidden name="masterRetQty" value="0"/>
<div id="Layer1" style="width:100%"> 
<div id="newmnav">
		  <ul>
		  		<%-- <li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
		  		<li><a href="editHubLimitOps.html?hubID=${hub}"><span>Operational&nbsp;/&nbsp;WH Management</span></a></li> --%>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Resource Limit<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<%-- <li><a href="resourceControlList.html?hub=${hub}"><span>Resource Limit List</span></a></li> --%>
		  </ul>
</div><div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!margin-bottom:3px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2" >
					  <tbody>  	
					  	<tr>
					  		<td align="left" height="3px"></td>
					  	</tr>
					  	<%-- <tr>
							<td align="left" width="15px"></td>
					  		<td align="right" width="100px">Hub&nbsp;/&nbsp;WH Selected :</td><td><strong>
					  		 <c:forEach var="entry" items="${distinctHubDescription}">
									<c:if test="${hub==entry.key}">
									<c:out value="${entry.value}" />
									</c:if>
									</c:forEach>
					  		 </strong></td>
						</tr> --%>
						<tr>
							<td align="left" width="15px">
						    <td align="right" width="">Category<font color="red" size="2">*</font></td>
						    <td width="250px"><s:textfield cssClass="input-text" name="category"  cssStyle="width:200px"   /></td>
                    		<%-- <td align="right" width="15px">Description<font color="red" size="2">*</font></td>
				    		<td style="width:223px;!width:230px;" ><s:select cssClass="list-menu" name="resources" list="%{material}" cssStyle="width:200px" headerKey="" headerValue="" /><s:textfield name="equipMaterialsLimits.resource"  cssClass="input-text" cssStyle="width:200px;" readonly="false" onkeydown="return onlyDel(event,this);"/>  --%>
				    		<%-- <img align="left" class="openpopup" width="17" height="20" style="float:right;!float:none;!vertical-align:bottom;" onclick="assignResources();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /> --%></td>	
					  	    <td align="right" width="">Warehouse<font color="red" size="2">*</font></td>
						    <td width="250px"><s:textfield cssClass="input-text" name="branch"  cssStyle="width:200px"  /></td>
					        <td align="right" width="">Division<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield cssClass="input-text" name="division"  cssStyle="width:150px"  /></td>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px"></td>
					  		<td align="right" width="15px">Description<font color="red" size="2">*</font></td>
				    		<td style="width:223px;!width:230px;" >
				    		<s:textfield cssClass="input-text" name="resource" cssStyle="width:200px"  >
				    		</s:textfield></td>					  		
					  		 <td align="right" width="">Actual&nbsp;Quantity</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="actQty" cssStyle="text-align:right;width:120px;" onchange="setActQtyFalg();"  /></td>
						     <td align="right" width="">Return&nbsp;Quantity</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="retQty" cssStyle="text-align:right;width:120px;"  onchange="setRetQtyFalg();"  /></td>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" rowspan="2">Comment</td>
					  		<td width="235px"><s:textarea name="comment"   cssClass="textarea" cssStyle="width:198px;" /></td>
					  		<td align="right" valign="top">Reffered By</td>						    
						    <td width="250px" valign="top"><s:textfield cssClass="input-text" name="reffBy" cssStyle="width:120px;"   /></td>
					  	</tr>
					  <%-- 	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" width="">Division<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:select cssClass="list-menu" name="equipMaterialsLimits.division" list="{'Domestic','Starline','OMD'}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
					  		<td align="right" width="">Unit&nbsp;Cost</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.unitCost" cssStyle="text-align:right;width:120px;"  /></td>
					  	</tr> --%>
					 </tbody>
				</table>
			 </td>
		    </tr>		    		  	
		 </tbody>
	</table>
	<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
			<td align="left" width="77"></td>
				<td align="left"><s:submit id="scan" cssClass="cssbutton1" type="button" key="button.save" onclick="return goForValidate();"/></td>
				
		       	<td align="right"><input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="resetData();" tabindex="137" /><%-- <s:reset cssClass="cssbutton1" type="button" key="Reset"/> --%></td>
		       	
		       	<td align="right"><input type="button" class="cssbutton1" type="button" value="Cancel" onclick="closePopup();"/></td>
			</tr>
			<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		</tbody>
	</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
	
	<%-- <table border="0" width="750px;">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
				<td valign="top">
				
				</td>
				<td style="width:120px">
				<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${operationsResourceLimits.createdOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="operationsResourceLimits.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${operationsResourceLimits.createdOn}" pattern="${displayDateTimeFormat}"/>
				</td>		
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
				<c:if test="${not empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.createdBy"/>
					<td style="width:85px"><s:label name="createdBy" value="%{operationsResourceLimits.createdBy}"/></td>
				</c:if>
				<c:if test="${empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.createdBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${operationsResourceLimits.updatedOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="operationsResourceLimits.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
				<td style="width:120px"><fmt:formatDate value="${operationsResourceLimits.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
				<c:if test="${not empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{operationsResourceLimits.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
		</tbody>
	</table> --%>

	<%-- <table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="left"><s:submit id="scan" cssClass="cssbutton1" type="button" key="button.save" onclick="return validate();"/></td>
				<c:if test="${hub!='0'}">
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
		       	</c:if>
			</tr>		  	
		</tbody>
	</table> --%>
</div>

</s:form>
<c:if test="${hitFlag == '1'}" >
	<c:redirect url="/equipMaterialControlList.html?hub=${hub}"  />
</c:if>
<script type="text/javascript"> 
function setActQtyFalg(){
	//alert("Act qyt chng");
	document.forms['operationsResourceLimits'].elements['isActQtyChng'].value=true;
	
}
function setRetQtyFalg(){
	//alert("Return qyt chng");
	document.forms['operationsResourceLimits'].elements['isRetQtyChng'].value=true
	
}
var f = document.getElementById('operationsResourceLimits'); 
f.setAttribute("autocomplete", "off");
pickOldValue();
 </script>
 <script type="text/javascript">
 var http51111 = getHTTPObject();
 function getHTTPObject()
 {
 var xmlhttp;
 if(window.XMLHttpRequest)
 {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)
 {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)
     {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }
 }
 return xmlhttp;
 }
 
 function findDescription(div){
	
	var cat=document.forms['operationsResourceLimits'].elements['category'];
	var bran=document.forms['operationsResourceLimits'].elements['branch'];
	 var division=div.options[div.options.selectedIndex].value;
	 var categ=cat.options[cat.options.selectedIndex].value;
	 var branch=bran.options[bran.options.selectedIndex].value;	
	 //alert(categ);
	 //alert(branch);
	 //alert(division);
	 var url="findInvDescription.html?ajax=1&decorator=simple&popup=true&branch="+branch+"&division="+division+"&category="+categ;
	   http51111.open("GET", url, true); 
	   http51111.onreadystatechange = handleDesc; 
	   http51111.send(null); 
 }
 function handleDesc(){
	 var des=document.forms['operationsResourceLimits'].elements['resource'];
	 if (http51111.readyState == 4){	 
	         var results = http51111.responseText
	         results = results.trim();	         
	         var res1=results.split("#");
	         //res1=res1.trim();
	         var k=0;
	         //alert(res1.length);
	         des.options.length=res1.length;
	         for(var i=0;i<res1.length;i++){
	        	 var iddes=res1[i];
	        	 //alert(iddes);
	        	 if(iddes!=null && (iddes!="")){
	        		 var iddess=iddes.split("~");
	        		 var id=iddess[0];
	        		 var desc=iddess[1];
	        		//alert(id);
	        		 //alert(desc);
	        		 //alert("finish")
	        		 des.options[k].value=id;
	        		 //alert("finish1")
	        		 des.options[k].text=desc;
	        		 
	        		 k=k+1;
	        	 }
	         }
	        // alert(results);
	 }
 }
	var hub = '${hub}';
	 if(hub=='0'){
		window.onload = function()
		{
			//Disable fields
			 	var inputs = document.getElementsByTagName("input");
			    for (var i = 0; i < inputs.length; i++) {
			    		inputs[i].disabled = true;														    	
			    }			    
			    var selects = document.getElementsByTagName("select");
			    for (var i = 0; i < selects.length; i++) {
			    	selects[i].disabled = true;
			    }
			    document.getElementById('openpopup4.img').style.display="none";
		      	document.getElementById("scan").disabled = true;
			}
	}
	 findDescription(document.forms['operationsResourceLimits'].elements['division']);
	// document.forms['operationsResourceLimits'].elements['division'].value=resource;
	</script>
	<script type="text/javascript">
	function resetData(){
		//document.forms['gridForm'].elements['equipMaterialsLimits.id'].value=tmp;
		//alert("hiiiiiiiiiiiiiiiiiiii");
    	/* document.forms['gridForm'].elements['equipMaterialsLimits.category'].value='${equipMaterialsLimits.category}';
    	document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value='${equipMaterialsLimits.branch}';
    	document.forms['gridForm'].elements['equipMaterialsLimits.division'].value='${equipMaterialsLimits.division}';
    	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value='${equipMaterialsLimits.resource}';            	
    	document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value='${equipMaterialsLimits.minResourceLimit}';  
    	document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value='${equipMaterialsLimits.maxResourceLimit}';
    	document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value='${equipMaterialsLimits.qty}';
		document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value='${equipMaterialsCost.salestCost}';    	
	    document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value='${equipMaterialsCost.ownerOpCost}';
	    document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value='${equipMaterialsCosts.unitCost}'; */
	   //alert(document.forms['operationsResourceLimits'].elements['id'].value);
	    if(document.forms['operationsResourceLimits'].elements['id'].value!=null && document.forms['operationsResourceLimits'].elements['id'].value!=""){
	    getEdit(document.forms['operationsResourceLimits'].elements['id'].value);
	    }else{
	    	document.forms['operationsResourceLimits'].elements['id'].value="";
	    	document.forms['operationsResourceLimits'].elements['category'].value="";
	    	document.forms['operationsResourceLimits'].elements['branch'].value="";
	    	document.forms['operationsResourceLimits'].elements['division'].value="";
	    	document.forms['operationsResourceLimits'].elements['comment'].value="";
	    	document.forms['operationsResourceLimits'].elements['reffBy'].value='';
	    	document.forms['operationsResourceLimits'].elements['actQty'].value='';
	    	document.forms['operationsResourceLimits'].elements['retQty'].value=''; 
	    	//document.forms['operationsResourceLimits'].elements['resource'].value='';
	    	document.forms['operationsResourceLimits'].elements['resource'].remove(document.forms['operationsResourceLimits'].elements['resource'].selectedIndex);
	    }
	}
	function goForValidate(){
		if(document.forms['operationsResourceLimits'].elements['category'].value==''){
			alert("Category is Required");
			return false;
		}
		if(document.forms['operationsResourceLimits'].elements['branch'].value==''){
			alert("Warehouse is Required");
					return false;
		}
		if(document.forms['operationsResourceLimits'].elements['division'].value==''){
			alert("Division is Required");
					return false;
		}
		if(document.forms['operationsResourceLimits'].elements['resource'].value==''){
			alert("Description is Required");
					return false;
		}
		/**if(document.forms['operationsResourceLimits'].elements['comment'].value==''){
			alert("Comment  is Required");
					return false;
		}
		
		if(document.forms['operationsResourceLimits'].elements['reffBy'].value==''){
			alert("Reffered By is Required");
					return false;
		}*/
		var matchQty=document.forms['operationsResourceLimits'].elements['masterRetQty'].value;
		//alert(matchQty);
		var retqty=document.forms['operationsResourceLimits'].elements['retQty'].value;
		//alert(retQty);
		 var diff=0;   		
   		if(parseInt(retqty)>parseInt(matchQty)){
   			diff=retqty-matchQty;
   		 }	else{
   			diff=matchQty-retqty;
   		 }	
  		 //alert(diff);
		if(document.forms['operationsResourceLimits'].elements['actQty'].value!=null && document.forms['operationsResourceLimits'].elements['actQty'].value!='' && document.forms['operationsResourceLimits'].elements['retQty'].value!=null && document.forms['operationsResourceLimits'].elements['retQty'].value!=''){
			if((parseInt(document.forms['operationsResourceLimits'].elements['actQty'].value))<parseInt(retqty)){
				alert("Return Quantity should not Exceed Actual Quantity Assigned");
				return false;
			}
		}
		
	}	
	</script>