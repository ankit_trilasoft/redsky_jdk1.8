package com.trilasoft.app.dao.hibernate;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.PayrollAllocationDao;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.Sharing;


public class PayrollAllocationDaoHibernate extends GenericDaoHibernate<PayrollAllocation, Long> implements PayrollAllocationDao{

	public PayrollAllocationDaoHibernate() {   
        super(PayrollAllocation.class);   
    }
	
	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from PayrollAllocation");
    }
	
	public List findById(Long id) {
    	return getHibernateTemplate().find("select id from PayrollAllocation where id=?",id);
    }
	
	public List<PayrollAllocation> searchpayrollAllocation(String code,String job,String service,String calculation) {
		return getHibernateTemplate().find("from PayrollAllocation where code like '" + code.replaceAll("'", "''") + "%' AND job like '"+job+"%' AND service like '"+service+"%' AND calculation like '"+calculation.replaceAll("'", "''")+"%'");
	}
	
	
	public Map<String, String> findRecalc(String corpId) {
		List<Sharing> list;
		list =  getHibernateTemplate().find("from Sharing where corpId = '" +corpId+ "'order by code asc");
		
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		for(Sharing sharing : list){
			parameterMap.put(sharing.getCode() , sharing.getCode()+" : "+ sharing.getDescription());
		}

		return parameterMap;
	}  

}
