<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Merge <c:out value="${mergeSO.shipNumber }" /> To :</title>   
    <meta name="heading" content="merge"/>   
 <script language="javascript">
 function getHTTPObject() {
	  var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	      
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
 }
 var httpMergeSO = getHTTPObject()
 function showSO(target){
	 var soDiv = document.getElementById("soList");
	 var seqNumber = target.value;
	 if(seqNumber!=''){
		 soDiv.style.display = 'none';
		 	document.getElementById("loader").style.display = "block";
		 	var url="mergeServiceOrderList.html?ajax=1&decorator=simple&popup=true&seqNumber="+seqNumber+"&quotesId=${quotesId}";
		 	httpMergeSO.open("GET", url, true);
		 	httpMergeSO.onreadystatechange =  handleHttpResponseMergeSoList;
			httpMergeSO.send(null);	
	 	}else{
		 	soDiv.style.display = 'none';
	 	}
}
 function handleHttpResponseMergeSoList(){
	if (httpMergeSO.readyState == 4){
	    var results= httpMergeSO.responseText; 
	    var soDiv = document.getElementById("soList");      
	    soDiv.style.display = 'block';
	    document.getElementById("loader").style.display = "none";
	    soDiv.innerHTML = results;
	    
	    }
}
 function mergeSO(){
		var quoteID = document.forms['mergeForm'].elements['quotesId'].value;
		var seviceOrderID = document.forms['mergeForm'].elements['mergedSoId'].value;
		location.href ="mergeQFtoSo.html?quotesId="+quoteID+"&serviceOrderID="+seviceOrderID+"&decorator=popup&popup=true";
	}
 
 function setSOIdvalue(target){
	 document.forms['mergeForm'].elements['mergedSoId'].value = target;
	 document.forms['mergeForm'].elements['Merge'].disabled = false;
 }
 
 function closeMyWindow(){
	var count='${hitFlag}';
	if(count=="Y"){
		parent.window.opener.document.location.reload();
		window.close();
	}
	
}
 
</script>
</head>
<form name="mergeForm" id="mergeForm" >
<s:hidden name="quotesId" value="<%=request.getParameter("quotesId") %>" />
<c:set var="quotesId" value="<%=request.getParameter("quotesId") %>" />
<c:set var="hitFlag" value="${hitFlag}" />
<s:hidden name="bookingAgentCode" />
<s:hidden name="mergedSoId" />
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
   
   
<table style="width:80%;">
<tr>
<td class="listwhitebox">Q.Booking Agent Code </td><td class="listwhitebox" style="text-align: left;">Customer File</td>
</tr>
<tr>
<td><s:textfield name="CFBookingAgentCode" value="<%=request.getParameter("bookingAgentCode") %>" size="10"  cssClass ="input-textUpper" readonly="true" /> </td>
<td style="text-align: left;"><s:select cssClass="list-menu" name="cfSeqNumber" list="%{mergeSOList}" headerKey="" headerValue="" cssStyle="width:105px" onchange="showSO(this);" /></td>

</tr>
<tr>
<td class="listwhitebox" colspan="5">Booking Agent Name</td>
</tr>
<tr>
<td colspan="5" style="text-align: left;"> <s:textfield name="CFBookingAgentName" value="<%=request.getParameter("bookingAgentName") %>" cssStyle="width:88%" cssClass ="input-textUpper" readonly="true" /></td>
</tr>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>


<div style="text-align:center; display:none" id="loader"><img src="/images/ajax-loader.gif" /></div>
<div id="soList">

</div>

</form>
<script>
try{
	document.getElementById("soList").style.display = 'none';
	closeMyWindow();
	
}catch(e){}
</script>