package com.trilasoft.app.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table ( name="surveyresponsedtl") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class SurveyResponseDtl extends BaseObject{
	
	private Long id;
	private Long surveyResponseId;
	private Long questionId;
	private Long answerId;
	private String answer;	
	private Long answerVersion;
	private Date submittedDate;	
	private String corpId;	
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("surveyResponseId", surveyResponseId)
				.append("questionId", questionId).append("answerId", answerId)
				.append("answer", answer)
				.append("answerVersion", answerVersion)
				.append("submittedDate", submittedDate)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SurveyResponseDtl))
			return false;
		SurveyResponseDtl castOther = (SurveyResponseDtl) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(surveyResponseId, castOther.surveyResponseId)
				.append(questionId, castOther.questionId)
				.append(answerId, castOther.answerId)
				.append(answer, castOther.answer)
				.append(answerVersion, castOther.answerVersion)
				.append(submittedDate, castOther.submittedDate)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(surveyResponseId)
				.append(questionId).append(answerId).append(answer)
				.append(answerVersion).append(submittedDate).append(corpId)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)		
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSurveyResponseId() {
		return surveyResponseId;
	}
	public void setSurveyResponseId(Long surveyResponseId) {
		this.surveyResponseId = surveyResponseId;
	}
	public Long getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}
	public Long getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}
	@Column(length=200)
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Long getAnswerVersion() {
		return answerVersion;
	}
	public void setAnswerVersion(Long answerVersion) {
		this.answerVersion = answerVersion;
	}
	@Column
	public Date getSubmittedDate() {
		return submittedDate;
	}
	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
