package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsRepatriationDao;
import com.trilasoft.app.model.DsRepatriation;

public class DsRepatriationDaoHibernate extends GenericDaoHibernate<DsRepatriation, Long> implements DsRepatriationDao{

	public DsRepatriationDaoHibernate() {
		super(DsRepatriation.class);
			}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsRepatriation where id = "+id+" ");
	}
}
