<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.appfuse.model.Role"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_SALE")){
		userRole=role.getName();
		break;
	}
	
}
%>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerViewListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerViewListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerViewListForm'].elements['stateSearch'].value = '';
		document.forms['partnerViewListForm'].elements['countrySearch'].value = '';
		document.forms['partnerViewListForm'].elements['vanlineCodeSearch'].value = '';
		document.forms['partnerViewListForm'].elements['partner.status'].value = '';
		document.forms['partnerViewListForm'].elements['partner.isPrivateParty'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isAccount'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isAgent'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isVendor'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isCarrier'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.isOwnerOp'].checked = false;
		document.forms['partnerViewListForm'].elements['isIgnoreInactive'].checked = false;
		document.forms['partnerViewListForm'].elements['partner.extReference'].value = '';
		document.forms['partnerListForm'].elements['partner.fidiNumber'].value = '';
		document.forms['partnerListForm'].elements['partner.OMNINumber'].value = '';
		document.forms['partnerListForm'].elements['partner.AMSANumber'].value = '';
		document.forms['partnerListForm'].elements['partner.WERCNumber'].value = '';
		document.forms['partnerListForm'].elements['partner.IAMNumber'].value = '';
		document.forms['partnerListForm'].elements['partner.utsNumber'].value = '';
		document.forms['partnerListForm'].elements['partner.eurovanNetwork'].value = '';
		document.forms['partnerListForm'].elements['partner.PAIMA'].value = '';
		document.forms['partnerListForm'].elements['partner.LACMA'].value = '';
}

function activBtn(){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
	var len = temp.length;
    var val="" ;
    if(len == undefined){
		if(temp.checked){
			val= temp.value;
		}
    }
		
	  for(var i = 0; i < len; i++) {
		if(temp[i].checked) {
			val = temp[i].value;
		}
	}
	if(val=='AG'){
		document.forms['addPartner'].elements['chkAgentTrue'].value='Y';
	}
}

function seachValidate(){
		var pp =  document.forms['partnerViewListForm'].elements['partner.isPrivateParty'].checked;
		var ac =  document.forms['partnerViewListForm'].elements['partner.isAccount'].checked;
		var ag =  document.forms['partnerViewListForm'].elements['partner.isAgent'].checked;
		var vd =  document.forms['partnerViewListForm'].elements['partner.isVendor'].checked;
		var cr =  document.forms['partnerViewListForm'].elements['partner.isCarrier'].checked;
		var oo =  document.forms['partnerViewListForm'].elements['partner.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one partner type.');
			return false;
		} 
}
function findVanLineCode(partnerCode,position){
	var url="findVanLineViewList.html?ajax=1&decorator=simple&popup=true&partnerVanLineCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);
}
function setRedSky(){
	document.forms['partnerViewListForm'].elements['partnerType'].value = "RSKY";
	document.forms['partnerViewListForm'].action = 'searchPartnerRS.html?search=YES';
	document.forms['partnerViewListForm'].submit();
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:0px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
font-size:.85em;
}
form {
margin-top:-45px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;
}

input[type="checkbox"]
{vertical-align:middle;}

span.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;position:absolute;}
.br-n-ctnr {border:none !important;text-align:center !important;}
.br-n-rgt {border:none !important;text-align:right !important;}

/*jitendra*/
.table td{border: 1px solid #e0e0e0 !important;}

/*end jitendra*/
</style>
</head>
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="popupListAdmin" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerViewListForm" action='${empty param.popup?"searchPartnerView.html":"searchPartnerView.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>
<div id="layer1" style="width:100%">
<div class="spnblk" style="height:40px;">&nbsp;</div> 	
<div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
			<li><a href="geoCodeMaps.html?partnerOption=view" ><span>Geo Search</span></a></li>
			<li><a onclick="setRedSky();"><span><font color="#fe000c">Red</font>Sky</span></a></li>				  	
	  </ul>
</div>		
<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:12px;!margin-top:-12px; "><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:100%;">
	<thead>
		<tr>
			<th style="width:120px;"><fmt:message key="partner.partnerCode"/></th>
			<th style="width:120px;">Vanline Code</th>
			<th style="width:120px;">External Ref.</th>
			<th style="width:120px;"><fmt:message key="partner.firstName"/></th>
			<th style="width:120px;">Alias Name</th>
			<th style="width:120px;">Last/Company Name</th>
			<th style="width:120px;">Country Code</th>
			<th style="width:120px;">Country Name</th>
			<th style="width:120px;"><fmt:message key="partner.billingState"/></th>
			<th style="width:120px;"><fmt:message key="partner.status"/></th>
			<th style="width:120px;">Vendor Type</th>

			<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="vanlineCodeSearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="partner.extReference" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="partner.firstName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="partner.aliasName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="14" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="14" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:105px" headerKey="" headerValue=""/></td>
		   <td><s:select cssClass="list-menu" name="partner.typeOfVendor" list="%{typeOfVendor}" cssStyle="width:105px" headerKey="" headerValue=""/></td>
		</tr>
		
		
		<tr>
		   <td class="listwhitetext br-n-ctnr1">FIDI&nbsp;<s:select cssClass="list-menu" name="partner.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
							<td class="listwhitetext br-n-ctnr1" style="width: 130px;">OMNI Number&nbsp;<s:select cssClass="list-menu" name="partner.OMNINumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
							<td class="listwhitetext br-n-ctnr1">AMSA&nbsp;<s:select cssClass="list-menu" name="partner.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-rgt1">WERC&nbsp;<s:select cssClass="list-menu" name="partner.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-rgt1">IAM&nbsp;<s:select cssClass="list-menu" name="partner.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td class="listwhitetext br-n-rgt1">Harmony&nbsp;<s:select cssClass="list-menu" name="partner.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-ctnr1">Eurovan&nbsp;Network&nbsp;<s:select cssClass="list-menu" name="partner.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td class="listwhitetext br-n-ctnr1">PAIMA&nbsp;<s:select cssClass="list-menu" name="partner.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td colspan="3" class="listwhitetext br-n-ctnr1"style="border-right:2px solid #74B3DC !important;" >LACMA&nbsp;<s:select cssClass="list-menu" name="partner.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
		</tr>
		<%-- <tr>
		<td colspan="11">
				<table style="margin:0px; padding:0px;width:100%;" cellpadding="0" cellspacing="0" border="0" >
					<tbody>
					<tr>
						    <td class="listwhitetext br-n-ctnr">FIDI&nbsp;<s:select cssClass="list-menu" name="partner.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
							<td class="listwhitetext br-n-ctnr">OMNI Number&nbsp;<s:select cssClass="list-menu" name="partner.OMNINumber" list="{'Y','N'}" headerKey="" headerValue="" /></td> 
							<td class="listwhitetext br-n-ctnr">AMSA&nbsp;<s:select cssClass="list-menu" name="partner.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-rgt">WERC&nbsp;<s:select cssClass="list-menu" name="partner.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-rgt">IAM&nbsp;<s:select cssClass="list-menu" name="partner.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td class="listwhitetext br-n-rgt">Harmony&nbsp;<s:select cssClass="list-menu" name="partner.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
							<td class="listwhitetext br-n-ctnr">Eurovan&nbsp;Network&nbsp;<s:select cssClass="list-menu" name="partner.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td class="listwhitetext br-n-ctnr">PAIMA&nbsp;<s:select cssClass="list-menu" name="partner.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							<td class="listwhitetext br-n-ctnr">LACMA&nbsp;<s:select cssClass="list-menu" name="partner.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
							
						</tr>
					</tbody></table></td>
		</tr> --%>
		
		<tr>
		
		  <c:set var="ischecked" value="false"/>
							<c:if test="${partner.isPrivateParty}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isAccount}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Accounts<s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isAgent}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Agents<s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
														
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isVendor}">
							<c:set var="ischecked" value="true"/>
							</c:if>					
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Vendors<s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
															
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isCarrier}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Carriers<s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
									
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isOwnerOp}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Owner Ops<s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
						
							<c:set var="ischecked" value="false"/>
							<c:if test="${isIgnoreInactive}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" colspan="4" class="listwhitetext" width="110px" style="border:hidden;">Ignore Inactive<s:checkbox key="isIgnoreInactive" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
							
							<td width="130px" style="border:hidden;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
		
	<%-- 	<tr>
			<td colspan="11" >
		<table cellspacing="0" cellpadding="0" border="0" style="margin:0px; padding:0px;width:100%;">
					<tbody>
						<tr>
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isPrivateParty}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isAccount}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Accounts<s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isAgent}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:hidden;">Agents<s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
														
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isVendor}">
							<c:set var="ischecked" value="true"/>
							</c:if>					
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Vendors<s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
															
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isCarrier}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Carriers<s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
									
							<c:set var="ischecked" value="false"/>
							<c:if test="${partner.isOwnerOp}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:hidden;">Owner Ops<s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
						
							<c:set var="ischecked" value="false"/>
							<c:if test="${isIgnoreInactive}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="110px" style="border:hidden;">Ignore Inactive<s:checkbox key="isIgnoreInactive" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
							
							<td width="130px" style="border:hidden;text-align:center;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr> --%>
	</tbody>
</table>
 </div>
<div class="bottom-header" style="margin-top:35px;!margin-top:40px;"><span></span></div>
</div>
</div> 
</div>
</s:form>
<div id="layer2" style="width:100%">

<div  style="float:left;margin-bottom:1px;" id="newmnav">
		<ul> 
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Partner List</span></a></li>	
		<li> <a href="agentList.html?paramView=View"><span>Agent Request List</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partners" value="partners" scope="request"/> 
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="false" defaultsort="2" pagesize="10" style="width:100%;!margin-top:0px;margin-left:5px; " decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>	
	
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
	<display:column  sortable="true" style="width:390px" title="Alias Name" ><c:out value="${partnerList.aliasName}" /></display:column>
	<display:column  sortable="true" style="width:150px" title="External Ref." ><c:out value="${partnerList.externalRef}" /></display:column>
	
	<%if(userRole.equalsIgnoreCase("ROLE_SALE")){ %>
	 		    <display:column title="P.Party" style="width:45px;font-size: 9px;" >
		 		    <c:if test="${partnerList.isPrivateParty == true}">
		 		    		<a href="editPartnerPublic.html?id=${partnerList.id}&partnerType=PP&paramView=View"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
		 		    </c:if>
				</display:column>
				
				<display:column title="Account" style="width:45px;font-size: 9px;" >
					<c:if test="${partnerList.isAccount == true && partnerList.status != 'Approved' && partnerList.stage == 'Prospect'}">
		 		    		<a href="editPartnerPublic.html?id=${partnerList.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
		 		    </c:if>
		 		    <c:if test="${partnerList.isAccount == true && (partnerList.status == 'Approved' || partnerList.stage != 'Prospect')}">
		 		    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
		 		    </c:if>
				</display:column>
				
				<display:column title="Agent" style="width:45px;font-size: 9px;" >
					<c:if test="${partnerList.isAgent == true}">  
			    		<a href="findPartnerProfileList.html?from=view&code=${partnerList.partnerCode}&partnerType=AG&id=${partnerList.id}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			    	</c:if>
				</display:column>
				<display:column title="Carrier" style="width:45px;font-size: 9px;" >
					<c:if test="${partnerList.isCarrier == true}">  
			    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			    	</c:if>
				</display:column>
				<display:column title="Vendor" style="width:45px;font-size: 9px;" >
					<c:if test="${partnerList.isVendor == true}">  
			    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			    	</c:if>
				</display:column>
				<display:column title="Owner Ops" style="width:45px;font-size: 9px;" >
					<c:if test="${partnerList.isOwnerOp == true}">  
			    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			    	</c:if>
				</display:column>
	<%}else{%>
		<display:column title="P.Party" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isPrivateParty == true}">  
				<a href="viewPartner.html?id=${partnerList.id}&partnerType=PP"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
			</c:if>
		</display:column>
		<display:column title="Account" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isAccount == true}">  
	    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Agent" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isAgent == true}">  
	    		<a href="findPartnerProfileList.html?from=view&code=${partnerList.partnerCode}&partnerType=AG&id=${partnerList.id}"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Carrier" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isCarrier == true}">  
	    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Vendor" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isVendor == true}">  
	    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
		<display:column title="Owner&nbsp;Ops" style="width:45px;font-size: 9px;" >
			<c:if test="${partnerList.isOwnerOp == true}">  
	    		<a href="viewPartner.html?id=${partnerList.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/view.gif"/>View</a>
	    	</c:if>
		</display:column>
	<%}%>
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>	
    <display:column title="VL&nbsp;Code" style="width:90px">
    <a onclick ="findVanLineCode('${partnerList.partnerCode}',this);">View&nbsp;List</a>
    </display:column>
    <c:if test="${agentClassificationShow=='Y'}">
     <display:column  title="Agent&nbsp;Classification" sortable="true" sortProperty="agentClassification" style="width:45px;" >
    <c:if test="${partnerList.isAgent == true}">
   		<c:out value="${partnerList.agentClassification}" />
    </c:if>
	</display:column>
	</c:if> 
    <display:column property="agentGroup" sortable="true" title="Group" style="width:100px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
 	
    <c:if test="${param.popup}">
    	<display:column style="width:105px;cursor:pointer;"><A onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?id=${partnerList.id}&partnerType=${partnerType}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:if>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="padding:1px;width: 100%;margin-left: 5px;">
	<tbody>
		<s:form id="addPartner" action="" method="post" name="addPartner">  
		<s:hidden name="chkAgentTrue" />
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px; padding-top:3px;">Add New Partner :
					<s:radio name="partnerType" list="%{actionTypeView}" onclick="activBtn(this);"  cssStyle="vertical-align: sub;" />
				   	<s:submit name="addBtn" cssClass="cssbutton" cssStyle="width:85px; height:25px; margin-left:50px;" align="top" value=" Add Partner" onclick='submit_addPartner();'/>   
				</td>
			</tr>	
		</s:form>
	</tbody>
</table>
<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
try{
    document.forms['addPartner'].elements['addBtn'].disabled = true;	
    function submit_addPartner()
	{
	verify_radio("addPartner","partnerType");
	}
	
	function verify_radio(frmName, rbGroupName)
    {
	var radios = document[frmName].elements[rbGroupName];
			for (var i=0; i <radios.length; i++) {
			if (radios[i].checked) {
				
				if(radios[i].value=='AG')
					{
					  document.forms['addPartner'].action ='agentRequestFinal.html?paramView=View';
					document.forms['addPartner'].submit();
					
					}
				else{
					
					 document.forms['addPartner'].action ='editPartnerPublic.html?paramView=View';
						document.forms['addPartner'].submit();
				}
				
			   return true;
			  }
			 }
			 return false;
			}
}
catch(e){}
</script>