<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Data&nbsp;Item</title>
<meta name="heading" content="Data&nbsp;Form" />
<style>
</style>
</head>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<body>
	<script language="javascript" type="text/javascript">
function callWeightAndVolume()
{
	var itemList = document.getElementById('itemDesc');
	fillWeightAndVolume(itemList);
}
		function fillWeightAndVolume(target) {
			if(target.value==null || target.value==''){
				document.getElementById('weight').value='';
				document.getElementById('volume').value='';
				document.getElementById('itemUomid').value='';
				}else{
			$
					.ajax({
						type : "POST",
						url : "weightAndVolumeForDataList.html?ajax=1&decorator=simple&popup=true",
						data : {
							itemDescription : target.value
						},
						success : function(data, textStatus, jqXHR) {
							if (data.trim() != null && data.trim() != '') {
								if (data.length > 0) {
									if(data.split("~")[0].trim() !=null && data.split("~")[0].trim() !=''){
									document.getElementById('weight').value = parseFloat(data
											.split("~")[0].trim()).toFixed(2);
									}else{document.getElementById('weight').value=0.00}
									if(data.split("~")[1].trim()!=null && data.split("~")[1].trim() !=''){
									document.getElementById('volume').value = parseFloat(data
											.split("~")[1].trim()).toFixed(2);
									}else{
										document.getElementById('volume').value=0.00;
									}
									document.getElementById('itemUomid').value= data
									.split("~")[2].trim();
								}
							}

						},
						error : function(data, textStatus, jqXHR) {
						}
					});
				}}

		 function closeWIN() {
			window.close(); 
		}
 
		function onlyNumberAllowed(target) {
			var patterns = /[^0-9]/g;
			if (target.value.match(patterns) != null) {
				alert("Please Enter Only Numbers");
				document.getElementById(target.id).value = '';
				return false;
			}
		}

		function onlyFloatAllowed(target) {
			var letters = /[^0-9]/g;
			var decimal = /^[-+]?[0-9]+\.[0-9]+$/;
			if ((target.value.match(letters) != null)
					&& (target.value.match(decimal) == null)) {
				alert("Please Enter Valid Value")
				document.getElementById(target.id).value = '';
				return false;
			}
			/* var aa = target.value.split(".")[1].length;
			alert(aa); */
		}
		
	function checkValidation()
	{
		document.forms['dataListForm'].elements['itemData.itemDesc'].disabled = false;
		document.forms['dataListForm'].elements['itemData.warehouse'].disabled = false; 
		var itemDescription = document.getElementById("itemDesc").value;
		var quantityExpected = document.getElementById("qtyExpected").value
		if(itemDescription==null || itemDescription ==''){
			alert("Item List is Mandatory Field");
			return false;
		}else if(quantityExpected==null || quantityExpected =='')
			{
			alert("Quantity Expected is Mandatory Field");
			return false;
			}
	}
	
	function limitText() {
		limitField=document.getElementById("notes");
		limitNum=480;
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0,limitNum);
		}
		}
	function setUnitOfWeightAndVolume(){	
	var unitWeightAndVolume = '${unitFromSystemDefault}';
	if(unitWeightAndVolume=='kgs'){
		document.getElementById("unitOfWeight").value="KG";
		document.getElementById("unitOfVolume").value="CBM";
	}else{
		document.getElementById("unitOfWeight").value="Pound";
        document.getElementById("unitOfVolume").value="CFT";
	}
	}
	
	function checkQtyReceived(target){
  	  var qtyExpected = document.getElementById('qtyExpected').value; 
      var qtyShipped = document.getElementById('qtyReceived').value; 
      var diff = qtyExpected - qtyShipped;
      if(parseInt(diff) < 1){
  	alert("Quantity Received less than Quantity Expected For Partial HandOut"); 
  	document.getElementById('qtyReceived').value=target;
  }
	}
  
  function checkVolumeReceived(target)
  {
      var volumeExpected = document.getElementById('volume').value;
      var volumeReceived = document.getElementById('volumeReceived').value;
     
      var diff = volumeExpected - volumeReceived; 
      if(parseFloat(diff) < 1){
      	alert("Volume Received always less than Volume Expected For Partial HandOut")
          document.getElementById('volumeReceived').value=target;
      }
  }
  
  function checkWeightReceived(target){
  	var weightExpected = document.getElementById('weight').value;
      var weightReceived = document.getElementById('weightReceived').value;
      var diff = weightExpected - weightReceived
       if(parseFloat(diff) < 1){
      	 alert("Weight Received always less than Weight Expected For Partial HandOut")
         document.getElementById('weightReceived').value=target;
     }
  }
	
	</script>
	<s:form id="dataListForm" name="dataListForm"
		action="saveWorkTicketDataList" onsubmit="return checkValidation();" method="post" validate="true">
		<s:hidden name="sid"></s:hidden>
		<s:hidden name="workTicketId"></s:hidden>
		<s:hidden name="flag"></s:hidden>
		<s:hidden name="shipNumber"></s:hidden>
		<s:hidden name="itemData.shipNumber"></s:hidden>
		<s:hidden name="itemData.id" />
		<s:hidden name="itemData.workTicketId" />
		<s:hidden name="itemData.serviceOrderId" />
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
        <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
        <s:hidden name="ticket"></s:hidden>
        <s:hidden name="itemData.ticket"/>
         <s:hidden name="itemData.itemQtyExpected"/>
         <s:hidden name="itemData.itemQtyReceived"/>
         <s:hidden name ="itemData.quantityShipped"/>
         <s:hidden name="flagForHO"/>
         <s:hidden name ="itemData.hoTicket"/>
         <c:if test="${not empty itemData.quantityReceived}">
         <c:set var="coraxValue" value="true"/>
         </c:if>
		
		<table class="" cellspacing="0" cellpadding="0" border="0"
			style="width: 100%; padding-top: 10px;">
			<tbody>
				<tr>
					<td width="100%" height="10" align="left">
						<div id="otabs">
							<ul>
								<li><a class="current"><span>Item Details</span></a></li>
							</ul>
						</div>

						<div class="spn">&nbsp;</div>
						<div align="center" id="content">
						<div id="liquid-round">
								<div style="margin-top: -10px;" class="top">
									<span></span>
								</div>
								<div class="center-content">
									<table cellspacing="0" cellpadding="2" border="0"
										class="detailTabLabel">
										<tbody>
											<tr>
												<td height="5px"></td>
											</tr>
											<tr>
											<td width="85px" align="right" class="listwhitetext">Line</td>
											<td class="listwhitetext"><s:textfield
                                                        cssClass="input-textUpper myclass" id="line" cssStyle="text-align: right;width:60px;"
                                                        maxLength="10" name="itemData.line" readonly="true"></s:textfield></td>
											</tr>
											
											<tr>
												<td width="85px" align="right" class="listwhitetext">Item
													List<font size="2" color="red">*</font>
												</td>


												<%-- <td class="listwhitetext"><s:select
														name="itemData.itemDesc" id="itemDesc" list="%{sortedItemList}"
														cssClass="list-menu myclass" cssStyle="width: 191px;" disabled="${coraxValue}" headerKey="" headerValue=""
														onchange="fillWeightAndVolume(this)">
													</s:select></td> --%>
													
													     <c:choose>
                                                <c:when test="${flagForHO eq 'HO' }">
                                                <td class="listwhitetext"><s:select
                                                        name="itemData.itemDesc" id="itemDesc" list="%{sortedItemList}" 
                                                        cssClass="list-menu myclass" cssStyle="width: 191px;" disabled="true" headerKey="" headerValue=""
                                                        onchange="fillWeightAndVolume(this)">
                                                    </s:select></td>
                                                </c:when>
                                                <c:otherwise>
                                                <td class="listwhitetext"><s:select
                                                        name="itemData.itemDesc" id="itemDesc" list="%{sortedItemList}"
                                                        cssClass="list-menu myclass" cssStyle="width: 191px;" disabled="${coraxValue}" headerKey="" headerValue=""
                                                        onchange="fillWeightAndVolume(this)">
                                                    </s:select></td>
                                                </c:otherwise></c:choose> 

										<td width="65px" align="right" class="listwhitetext">Pack&nbsp;Id</td>
                                             <td class="listwhitetext"><s:textfield
                                                        cssClass="input-textUpper myclass" id="line" cssStyle="text-align: right;width:64px;"
                                                        maxLength="40" name="itemData.packId" readonly="true"></s:textfield></td>
                                                        
                                                <td width="65px" align="right" class="listwhitetext">Item&nbsp;UomID&nbsp;</td>
                                                <td class="listwhitetext"><s:textfield
                                                        cssClass="input-textUpper myclass" id="itemUomid" cssStyle="text-align: right;width:64px;"
                                                        maxLength="10" name="itemData.itemUomid" readonly="true"
                                                        onchange="onlyNumberAllowed(this);"></s:textfield></td>     				
											</tr>

											<tr>
												<td height="1px"></td>
											</tr>

											<tr>
											
											<c:choose>
                                            <c:when test="${flagForHO eq 'HO' }">
                                              <td width="110px" align="right" class="listwhitetext">Warehouse</td>
                                                <td align="left" colspan="1"><s:select
                                                        name="itemData.warehouse" list="%{house}" id="warehouse"
                                                        cssClass="list-menu myclass" disabled="true" cssStyle="width: 192px;"></s:select>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                            <td width="110px" align="right" class="listwhitetext">Warehouse</td>
                                                <td align="left" colspan="1"><s:select
                                                        name="itemData.warehouse" list="%{house}" id="warehouse"
                                                        cssClass="list-menu myclass" disabled="${coraxValue}" cssStyle="width: 192px;"></s:select>
                                                </td>
                                                </c:otherwise> 
                                                </c:choose>
											
											<%-- <td width="110px" align="right" class="listwhitetext">Warehouse</td>
                                                <td align="left" colspan="1"><s:select
                                                        name="itemData.warehouse" list="%{house}" id="warehouse"
                                                        cssClass="list-menu myclass" disabled="${coraxValue}" cssStyle="width: 192px;"></s:select>
                                                </td> --%>
											
												<%-- <td width="85px" align="right" class="listwhitetext">Line</td>
												<td colspan="1"><table style="margin:0px;" cellspacing="0" cellpadding="0" border="0">
												<tr>
												<td class="listwhitetext"><s:textfield
														cssClass="input-textUpper myclass" id="line" cssStyle="text-align: right;width:60px;"
														maxLength="10" name="itemData.line" readonly="true"></s:textfield></td>
														
												<td width="63px" align="right" class="listwhitetext">Item&nbsp;UomID&nbsp;</td>
                                                <td class="listwhitetext"><s:textfield
                                                        cssClass="input-textUpper myclass" id="itemUomid" cssStyle="text-align: right;width:60px;"
                                                        maxLength="10" name="itemData.itemUomid"
                                                        onchange="onlyNumberAllowed(this);"></s:textfield></td>		
                                                 </tr>
                                                 </table>
                                                 </td> --%>
                                                 
                                                 
												 <td width="130px" align="right" class="listwhitetext">Quantity&nbsp;Ordered</td>
												<td align="left" colspan="1"><s:textfield
														name="itemData.quantityOrdered" id="quantityOrdered"
														cssClass="input-textUpper myclass" readonly="true"
														cssStyle="text-align: right;width: 64px;" ></s:textfield></td>

												<td width="60px" align="right" class="listwhitetext"></td>


											</tr>
											<tr>
                                                <td height="1px"></td>
                                            </tr>
                                            <tr>
                                            <%-- <td width="60px" align="right" class="listwhitetext">Quantity&nbsp;Expected<font size="2" color="red">*</font></td>
                                                <td class="listwhitetext"><s:textfield
                                                        name="itemData.qtyExpected" id="qtyExpected" size="7"
                                                        maxLength="8" cssClass="input-text myclass"
                                                        cssStyle="text-align: right;width: 64px;" onchange="onlyNumberAllowed(this)"></s:textfield></td> --%>
                                                        
                                                        <c:choose>
                                            <c:when test="${flagForHO eq 'HO' }">
                                            <td width="60px" align="right" class="listwhitetext">Quantity&nbsp;Expected<font size="2" color="red">*</font></td>
                                                <td class="listwhitetext"><s:textfield
                                                        name="itemData.qtyExpected" id="qtyExpected" size="7"
                                                        maxLength="8" cssClass="input-textUpper myclass" readonly="true"
                                                        cssStyle="text-align: right;width: 64px;" onchange="onlyNumberAllowed(this)"></s:textfield></td>
                                             </c:when>
                                             <c:otherwise>
                                             <td width="60px" align="right" class="listwhitetext">Quantity&nbsp;Expected<font size="2" color="red">*</font></td>
                                                <td class="listwhitetext"><s:textfield
                                                        name="itemData.qtyExpected" id="qtyExpected" size="7"
                                                        maxLength="8" cssClass="input-text myclass"
                                                        cssStyle="text-align: right;width: 64px;" onchange="onlyNumberAllowed(this)"></s:textfield></td>
                                             
                                             </c:otherwise>
                                              </c:choose>  
                                                
                                                <%-- <td width="85" align="right" class="listwhitetext">Quantity&nbsp;Received</td>
                                                <td><s:textfield name="itemData.quantityReceived"
                                                        size="7" maxLength="10" id="qtyReceived"
                                                        cssClass="input-textUpper myclass" readonly="true"
                                                        cssStyle="text-align: right;width: 64px;" 
                                                        onchange="onlyNumberAllowed(this);"></s:textfield></td> --%>
                                                        
                                                        <c:choose>
                                            <c:when test="${flagForHO eq 'HO' }">
                                             <td width="85" align="right" class="listwhitetext">Quantity&nbsp;Received</td>
                                                <td><s:textfield name="itemData.quantityReceived"
                                                        size="7" maxLength="10" id="qtyReceived"
                                                        cssClass="input-text myclass"
                                                        cssStyle="text-align: right;width: 64px;" 
                                                        onchange="onlyNumberAllowed(this);checkQtyReceived('${itemData.quantityReceived}');" ></s:textfield></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td width="85" align="right" class="listwhitetext">Quantity&nbsp;Received</td>
                                                <td><s:textfield name="itemData.quantityReceived"
                                                        size="7" maxLength="10" id="qtyReceived"
                                                        cssClass="input-textUpper myclass" readonly="true"
                                                        cssStyle="text-align: right;width: 64px;" 
                                                        onchange="onlyNumberAllowed(this);"></s:textfield></td>
                                                        </c:otherwise> 
                                                        </c:choose>
                                                 </tr>
											
											<tr>
												<td height="1px"></td>
											</tr>

											<tr>
												<%-- <td width="" align="right" class="listwhitetext">Volume&nbsp;Expected</td>
												<td><s:textfield name="itemData.volume" size="20" 
														maxlength="7" id="volume" cssClass="input-text myclass"
														cssStyle="text-align: right;width: 64px;"
														onchange="onlyFloatAllowed(this)"></s:textfield></td>
														
												<td width="" align="right" class="listwhitetext">Volume&nbsp;Received</td>
                                                <td><s:textfield name="itemData.volumeReceived" size="12" id="volumeReceived"
                                                        maxlength="20" cssClass="input-textUpper myclass"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        readonly="true"></s:textfield></td>	 --%>
                                                        
                                                        <c:choose>
                                            <c:when test="${flagForHO eq 'HO' }">
                                            <td width="" align="right" class="listwhitetext">Volume&nbsp;Expected</td>
                                                <td><s:textfield name="itemData.volume" size="20" 
                                                        maxlength="7" id="volume" cssClass="input-textUpper myclass"
                                                        cssStyle="text-align: right;width: 64px;" readonly="true"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td width="" align="right" class="listwhitetext">Volume&nbsp;Expected</td>
                                                <td><s:textfield name="itemData.volume" size="20" 
                                                        maxlength="7" id="volume" cssClass="input-text myclass"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>
                                                </c:otherwise>
                                                </c:choose>
                                                
                                                <c:choose>
                                                <c:when test="${flagForHO eq 'HO' }">   
                                                <td width="" align="right" class="listwhitetext">Volume&nbsp;Received</td>
                                                <td><s:textfield name="itemData.volumeReceived" size="12" id="volumeReceived"
                                                        maxlength="20" cssClass="input-text myclass"
                                                        cssStyle="text-align: right;width: 64px;" onchange="onlyFloatAllowed(this);checkVolumeReceived('${itemData.volumeReceived}');"></s:textfield></td>     
                                                </c:when>
                                                
                                                <c:otherwise>
												 <td width="" align="right" class="listwhitetext">Volume&nbsp;Received</td>
                                                <td><s:textfield name="itemData.volumeReceived" size="12" id="volumeReceived"
                                                        maxlength="20" cssClass="input-textUpper myclass"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        readonly="true"></s:textfield></td>     
                                                </c:otherwise>  
                                                </c:choose>	
												
												<td width="91px" align="right" class="listwhitetext">Unit of Volume</td>
												<td align="left"><s:textfield
														cssClass="input-textUpper myclass" id="unitOfVolume" size="14"
														maxLength="20" name="itemData.unitOfVolume" readonly="true"
														cssStyle="text-align: ;width: 64px;"></s:textfield></td>
											</tr>

											<tr>
												<td height="1px"></td>
											</tr>
											<tr>
												<%-- <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Expected</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-text myclass" id="weight" size="14"
                                                        maxLength="7" name="itemData.weight" 
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>	
                                                        
                                                        <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Received</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-textUpper myclass" id="weightReceived" size="14"
                                                        maxLength="7" name="itemData.weightReceived"  readonly="true"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>   --%>
                                                        
                                                        <c:choose>
                                                <c:when test="${flagForHO eq 'HO' }">
                                                <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Expected</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-textUpper myclass" id="weight" size="14"
                                                        maxLength="7" name="itemData.weight"  readonly="true"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>
                                                   
                                                </c:when>
                                                <c:otherwise>
                                                <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Expected</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-text myclass" id="weight" size="14"
                                                        maxLength="7" name="itemData.weight" 
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>
                                                 </c:otherwise>
                                                 </c:choose>        
                                                   
                                                   <c:choose>
                                                <c:when test="${flagForHO eq 'HO' }">
                                                <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Received</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-text myclass" id="weightReceived" size="14"
                                                        maxLength="7" name="itemData.weightReceived" 
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this),checkWeightReceived('${itemData.weightReceived}');"></s:textfield></td>   
                                                
                                                 </c:when>
                                                 <c:otherwise>  
                                                        <td width="91px" align="right" class="listwhitetext">Weight&nbsp;Received</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-textUpper myclass" id="weightReceived" size="14"
                                                        maxLength="7" name="itemData.weightReceived"  readonly="true"
                                                        cssStyle="text-align: right;width: 64px;"
                                                        onchange="onlyFloatAllowed(this)"></s:textfield></td>       
                                                </c:otherwise>
                                                </c:choose>
								 	
                                                
                                                <td width="91px" align="right" class="listwhitetext">Unit of Weight</td>
                                                <td align="left"><s:textfield
                                                        cssClass="input-textUpper myclass" id="unitOfWeight" size="14"
                                                        maxLength="20" name="itemData.unitOfWeight" readonly="true"
                                                        cssStyle="text-align: ;width: 64px;"></s:textfield></td>        
											</tr>
											<tr>
												<td width="" align="right" class="listwhitetext">Notes</td>
												<td colspan="5"><s:textarea name="itemData.notes"
														cols="" rows="4" id="notes" cssClass="textarea myclass"
														cssStyle="width: 395px;" onkeydown="limitText()" onkeyup="limitText()"></s:textarea></td>
											</tr>
											<tr>
												<td></td>
												<td><table style="margin-top: 10px;">
														<tr>
														<c:choose>
                                                            <c:when test="${ticketAssignedStatus !='Assigned'}">
                                                            <td><s:submit id="save" cssClass="cssbuttonA" method="save"
                                                                    cssStyle="margin-left:0px;" key="button.save"
                                                                    theme="simple" /></td>
                                                            <td></td>
                                                            <td><td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td></td>
                                                            </td>
                                                            </c:when>
                                                            <c:otherwise>
                                                             <td><input type="button" id="save" class="cssbuttonA" method="save" value="Cancel"
                                                                    style="margin-left:0px;" onclick="closeWIN();"
                                                                    theme="simple" /></td>
                                                            
                                                            </c:otherwise>
                                                            </c:choose>
														<%-- 
															<!-- <td><input type="button" Class="cssbutton" Style="width: 55px;" id="okButton" value="Save" onclick="" /> -->
															<td><s:submit cssClass="cssbuttonA" method="save"
																	cssStyle="margin-left:0px;" key="button.save"
																	theme="simple" /></td>
															<td></td>
															<td><td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td></td>
															</td> --%>
														</tr>
													</table></td>
											</tr>
											<tr>
												<td height="10px"></td>
											</tr>
										</tbody>
									</table>

								</div>
								<div class="bottom-header">
									<span></span>
								</div>
							</div>
						</div>
						
			<table class="detailTabLabel" border="0" style="width: 850px">
							<tbody>
								<tr>
									<td align="left" class="listwhitetext" width="30px"></td>
									<td colspan="5"></td>
								</tr>
								<tr>
									<td align="left" class="listwhitetext" width="30px"></td>
									<td colspan="5"></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width: 50px"><fmt:formatDate
											var="cartonCreatedOnFormattedValue"
											value="${itemData.createdOn}"
											pattern="${displayDateTimeEditFormat}" /></td>
									<td align="right" class="listwhitetext" style="width: 50px"><b><fmt:message
												key='carton.createdOn' /></td>
									<s:hidden name="itemData.createdOn"
										value="${cartonCreatedOnFormattedValue}" />
									<td style="width: 190px"><fmt:formatDate
											value="${itemData.createdOn}"
											pattern="${displayDateTimeFormat}" /></td>


									<td align="right" class="listwhitetext" style="width: 50px"><b><fmt:message
												key='customerFile.createdBy' /></b></td>
									<c:if test="${not empty itemData.id}">
										<s:hidden name="itemData.createdBy" />
										<td style="width: 85px"><s:label name="createdBy"
												value="%{itemData.createdBy}" /></td>
									</c:if>
									<c:if test="${empty itemData.id}">
										<s:hidden name="itemData.createdBy"
											value="${pageContext.request.remoteUser}" />
										<td style="width: 85px"><s:label name="createdBy"
												value="${pageContext.request.remoteUser}" /></td>
									</c:if>
									
									<!-- <td align="right" class="listwhitetext" style="width: 20px"> --><fmt:formatDate
                                            var="cartonCreatedOnFormattedValue"
                                            value="${itemData.updatedOn}"
                                            pattern="${displayDateTimeEditFormat}" /><!-- </td> -->
                                    <td align="right" class="listwhitetext" style="width: 110px"><b><fmt:message
                                                key='carton.updatedOn' /></td>
                                    <s:hidden name="itemData.updatedOn"
                                        value="${cartonCreatedOnFormattedValue}" />
                                    <td style="width: 200px"><fmt:formatDate
                                            value="${itemData.updatedOn}"
                                            pattern="${displayDateTimeFormat}" /></td>

                                    <td align="right" class="listwhitetext" style="width: 75px"><b><fmt:message
                                                key='customerFile.updatedBy' /></b></td>
                                    <c:if test="${not empty itemData.id}">
                                        <s:hidden name="itemData.updatedBy" />
                                        <td style="width: 85px"><s:label name="updatedBy"
                                                value="%{itemData.updatedBy}" /></td>
                                    </c:if>
                                    <c:if test="${empty itemData.id}">
                                        <s:hidden name="itemData.updatedBy"
                                            value="${pageContext.request.remoteUser}" />
                                        <td style="width: 85px"><s:label name="updatedBy"
                                                value="${pageContext.request.remoteUser}" /></td>
                                    </c:if>
								</tr>
								<tr>
									<td align="left" height="5px"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>

			</tbody>
		</table>
	</s:form>
	
	<script language="javascript" type="text/javascript">
       <c:if test="${closeFlag=='CLSD'}">
            window.opener.findAllResource1();
             closeWIN();
             </c:if> 
             
             <c:if test="${closeFlag=='CLSDForHO'}">
             window.opener.findAllResource1();
              closeWIN();
              </c:if> 
       </script>
       
	   <%-- <script language="javascript" type="text/javascript">
	   <c:if test="${closeFlag=='CLSD'}">
	        window.opener.findAllResource1();
             closeWIN();
             </c:if>  
       </script> --%>
</body>
