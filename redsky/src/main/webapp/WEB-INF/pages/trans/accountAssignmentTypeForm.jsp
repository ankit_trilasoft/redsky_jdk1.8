<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Assignment Details</title>   
    <meta name="heading" content="AccountAssignmentType"/>  
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
 
</head>

<c:set var="buttons"> 
    <input type="submit"" class="cssbutton1" onclick="return validation();" value="Save" style="width:55px; height:25px" tabindex="83"/> 
	<input type="reset" class="cssbutton1" onclick="closeWIN();" value="Close" style="width:55px; height:25px" tabindex="83"/> 
</c:set>

<s:form id="accountAssignmentTypeForm" action="saveAccountAssignmentType.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">


<s:hidden name="accountAssignmentType.id" />
<s:hidden name="id" value="${accountAssignmentType.id}"/>  
<s:hidden name="accountAssignmentType.partnerPrivateId" value="<%=request.getParameter("partnerPrivateId")%>"/>
<s:hidden name="accountAssignmentType.partnerCode" value="<%=request.getParameter("partnerCode")%>"/>


<div id="layer1" style="width:500px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Assignment<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:95%">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:95%;margin:0px;" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0"  style="margin-left:30px">
<tbody>
<tr>
<td align="right" width="75px" class="listwhitetext" valign="bottom">Assignment<font color="red" size="2">*</font></td> 
<td style="width:10px"></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="accountAssignmentType.assignment" onchange="checkChar();" maxlength="60"  required="true" cssClass="input-text" size="60" tabindex="1" /></td>
</tr>
<tr>
<td align="left" width="75px" class="listwhitetext" valign="top">Description</font></td>
<td style="width:10px"></td>
<td  align="left" valign="top"><s:textarea name="accountAssignmentType.description" required="true" cssClass="textarea" tabindex="1" cols="57" rows="5" /></td> 
 </tr>
<tr>	<td align="left" colspan="2">&nbsp;</td>
     <td align="left" class="listwhitetext" colspan="2" >						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
	</tr>
	<tr></tr>
	<tr></tr>
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 

<table width="500px">
				<tbody>					
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${accountAssignmentType.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="accountAssignmentType.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:100px"><fmt:formatDate value="${accountAssignmentType.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty accountAssignmentType.id}">
								<s:hidden name="accountAssignmentType.createdBy"/>
								<td ><s:label name="createdBy" value="%{accountAssignmentType.createdBy}"/></td>
							</c:if>
							<c:if test="${empty accountAssignmentType.id}">
								<s:hidden name="accountAssignmentType.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					<tr>
					<tr>		
							
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${accountAssignmentType.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="accountAssignmentType.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:100px"><fmt:formatDate value="${accountAssignmentType.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty accountAssignmentType.id}">
							<s:hidden name="accountAssignmentType.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{accountAssignmentType.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty accountAssignmentType.id}">
							<s:hidden name="accountAssignmentType.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
 
</div>
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>

<%-- Script Shifted from Top to Botton on 06-Sep-2012 By Kunal --%>
<script type="text/javascript">  
	var http2 = getHTTPObject();
	var http2 = getHTTPObject();
	function closeWIN(){
    	window.close();
    } 
	function validation(){
    	var opt = document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
    	if(opt.length == 0 ){
    		alert("Assignment is a required field");
    		return false;
    	}else  {
    		return checkChar();
    	}
    	if(document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value.length ==0){
    		alert("Assignment is a required field");
    		return false;
    	}
    	return true;
	}

	 function checkValidAssignment(){
        var option=document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
        option=option.trim();
        if(option==''){
        	alert('Please enter valid value.');
        	document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value='';
			return false;
		}
	 }

	 function checkAssignmentDuplicate(){
 		var assignment = document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
 		var partnerCode = "<%=request.getParameter("partnerCode")%>";
 		if(assignment.trim() != ''){	    		
    		var url="assignmentDuplicateCheckAjax.html?ajax=1&decorator=simple&popup=true&partnerCode=" +partnerCode+"&assignment="+ encodeURI(assignment);
    	    http2.open("GET", url, true);
    	    http2.onreadystatechange = handleHttpResponse44;
    	    http2.send(null);
 		}
     }
 	function handleHttpResponse44(){
 		if (http2.readyState == 4) {
	          var results = http2.responseText
	          results = results.trim();
	          results =	results.replace('[','');
	          results =	results.replace(']','');
	          if(results == 'YES'){
	        	  alert('Assignment with the same name already exists.');
	        	 document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value='';
	          }
 	    }
 	}
 	 
 	function getHTTPObject(){
 	    var xmlhttp;
 	    if(window.XMLHttpRequest)
 	    {
 	        xmlhttp = new XMLHttpRequest();
 	    }
 	    else if (window.ActiveXObject)
 	    {
 	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
 	        if (!xmlhttp)
 	        {
 	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
 	        }
 	    }
 	    return xmlhttp;
 	} 
</script>
 
 <%-- Shifting Closed Here --%>
 
<script type="text/javascript">  

  <c:if test="${hitFlag=='1'}">
    pick();
    window.close();
  </c:if>
  function pick() {
		try {
			window.opener.clickOnRequest();
		}catch(e){
			parent.window.opener.document.location.reload();
		}
  }	

  function checkChar() {
		var capString = document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
		document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value = capString.charAt(0).toUpperCase() +capString.substring(1);
		
		 valid(document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'],'special');
//		 return;
			 var chArr = new Array("%","&","'","|","\\","\"");
		 	 var origString = document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
		 	for ( var i = 0; i < chArr.length; i++ ) {
		 		origString = origString.split(chArr[i]).join(''); 
		 	}
		 	document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value = origString;
		 <%--	var strString = document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value;
			 if(parseInt(strString) == strString){
				 alert('only number is not valid');
				 document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value='';
				 return false;
			 }else if(/^\d+\.\d+$/.test(strString)){
				 alert('only decimal number is not valid');
				 document.forms['accountAssignmentTypeForm'].elements['accountAssignmentType.assignment'].value='';
				  return false;
			 }--%>
			 checkAssignmentDuplicate();
			 checkValidAssignment();			 
		 	return true;
	 }
</script>