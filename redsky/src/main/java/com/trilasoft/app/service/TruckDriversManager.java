package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TruckDrivers;

public interface TruckDriversManager extends GenericManager<TruckDrivers, Long>{
	public List getDriverValidation(String driverCode,String truckNumber,String partnerCode, String corpId);
	public List findDriverList(String truckNumber, String corpId);
	public List getPrimaryFlagStatus(String truckNumber, String corpId);
}
