<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Storage Library Extract</title>
<meta name="heading" content="Storage Library Extract" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}

.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
       function fieldValidate(){
       
	       if(document.forms['storageExtractForm'].elements['beginDate'].value=='')
	       {
	       	alert("Please enter the begin date"); 
	       	return false;
	       }
	       if(document.forms['storageExtractForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       	} 
	       else
	       {
	       	document.forms['storageExtractForm'].submit();
	         return true;
	       }
       
       }  
</script>
</head>

<s:form name="storageExtractForm" id="storageExtractForm" action="storageExtracts" method="post" validate="true" cssClass="form_magn">
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
	<div id="Layer1" style="width:100%">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Storage Library Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:11px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">	  		
  		<tr> 
  			<td height="5px"></td>
			<td class="listwhitetext" align="right">Location&nbsp;Warehouse&nbsp;&nbsp;</td>
			<td width="260px"><s:select name="locWarehouse" list="%{house}"  cssStyle="width:220px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
			<td class="listwhitetext" align="right">Storage&nbsp;Type&nbsp;&nbsp;</td>
		 	<td><s:select name="typeSto" list="%{stotype}"  cssStyle="width:220px;" cssClass="list-menu" headerKey="" headerValue="All" /></td>
 		</tr> 
		<tr><td height="20px"></td></tr>
	  	<tr>	
	  		<td width=""></td>			
			<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:105px; height:25px; margin-left:108px;" align="top" method="storageLibExtract" value="Extract"/></td>
	    </tr>
	    <tr><td height="20"></td></tr>	 				
</table>       
</s:form>
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

</div>