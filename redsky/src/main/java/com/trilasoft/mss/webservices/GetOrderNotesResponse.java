
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrderNotesResult" type="{http://www.mss1.com/webservices/}ArrayOfOrderNote" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrderNotesResult"
})
@XmlRootElement(name = "GetOrderNotesResponse")
public class GetOrderNotesResponse {

    @XmlElement(name = "GetOrderNotesResult")
    protected ArrayOfOrderNote getOrderNotesResult;

    /**
     * Gets the value of the getOrderNotesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrderNote }
     *     
     */
    public ArrayOfOrderNote getGetOrderNotesResult() {
        return getOrderNotesResult;
    }

    /**
     * Sets the value of the getOrderNotesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrderNote }
     *     
     */
    public void setGetOrderNotesResult(ArrayOfOrderNote value) {
        this.getOrderNotesResult = value;
    }

}
