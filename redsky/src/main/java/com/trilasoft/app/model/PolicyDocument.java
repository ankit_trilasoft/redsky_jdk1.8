package com.trilasoft.app.model;

import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "policydocument")
public class PolicyDocument extends BaseObject{
 private Long id;
 private String documentName;
 private String fileSize;
 private String corpID;
 private String createdBy;
 private Date createdOn;
 private String updatedBy;
 private Date updatedOn;
 private String description;
 private String partnerCode;
 private File file;
 private String sectionName;
 private String language;
 private String fileName;
 private String fileLocation;
 private String contentFileType;
 private Long parentId;
 private Long docSequenceNumber;
@Override
public String toString() {
	return new ToStringBuilder(this).append("id", id)
			.append("documentName", documentName)
			.append("fileSize", fileSize).append("corpID", corpID)
			.append("createdBy", createdBy).append("createdOn", createdOn)
			.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
			.append("description", description)
			.append("partnerCode", partnerCode).append("file", file)
			.append("sectionName", sectionName)
			.append("language", language).append("fileName", fileName)
			.append("fileLocation", fileLocation)
			.append("contentFileType", contentFileType)
			.append("parentId", parentId)
			.append("docSequenceNumber", docSequenceNumber).toString();
}
@Override
public boolean equals(final Object other) {
	if (!(other instanceof PolicyDocument))
		return false;
	PolicyDocument castOther = (PolicyDocument) other;
	return new EqualsBuilder().append(id, castOther.id)
			.append(documentName, castOther.documentName)
			.append(fileSize, castOther.fileSize)
			.append(corpID, castOther.corpID)
			.append(createdBy, castOther.createdBy)
			.append(createdOn, castOther.createdOn)
			.append(updatedBy, castOther.updatedBy)
			.append(updatedOn, castOther.updatedOn)
			.append(description, castOther.description)
			.append(partnerCode, castOther.partnerCode)
			.append(file, castOther.file)
			.append(sectionName, castOther.sectionName)
			.append(language, castOther.language)
			.append(fileName, castOther.fileName)
			.append(fileLocation, castOther.fileLocation)
			.append(contentFileType, castOther.contentFileType)
			.append(parentId, castOther.parentId)
			.append(docSequenceNumber, castOther.docSequenceNumber)
			.isEquals();
}
@Override
public int hashCode() {
	return new HashCodeBuilder().append(id).append(documentName)
			.append(fileSize).append(corpID).append(createdBy)
			.append(createdOn).append(updatedBy).append(updatedOn)
			.append(description).append(partnerCode).append(file)
			.append(sectionName).append(language).append(fileName)
			.append(fileLocation).append(contentFileType).append(parentId)
			.append(docSequenceNumber).toHashCode();
}
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
@Column
public String getDocumentName() {
	return documentName;
}
public void setDocumentName(String documentName) {
	this.documentName = documentName;
}
@Column
public String getFileSize() {
	return fileSize;
}
public void setFileSize(String fileSize) {
	this.fileSize = fileSize;
}
@Column
public String getCorpID() {
	return corpID;
}
public void setCorpID(String corpID) {
	this.corpID = corpID;
}
@Column
public String getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}
@Column
public Date getCreatedOn() {
	return createdOn;
}
public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
}
@Column
public String getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}
@Column
public Date getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Date updatedOn) {
	this.updatedOn = updatedOn;
}
@Column
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
@Column
public String getPartnerCode() {
	return partnerCode;
}
public void setPartnerCode(String partnerCode) {
	this.partnerCode = partnerCode;
}
@Column
public File getFile() {
	return file;
}
public void setFile(File file) {
	this.file = file;
}
@Column
public String getSectionName() {
	return sectionName;
}
public void setSectionName(String sectionName) {
	this.sectionName = sectionName;
}
@Column
public String getLanguage() {
	return language;
}
public void setLanguage(String language) {
	this.language = language;
}
@Column
public String getFileName() {
	return fileName;
}
public void setFileName(String fileName) {
	this.fileName = fileName;
}
@Column( length=254 )
public String getFileLocation() {
	return fileLocation;
}
public void setFileLocation(String fileLocation) {
	this.fileLocation = fileLocation;
}
@Column
public String getContentFileType() {
	return contentFileType;
}
public void setContentFileType(String contentFileType) {
	this.contentFileType = contentFileType;
}
public Long getParentId() {
	return parentId;
}
public void setParentId(Long parentId) {
	this.parentId = parentId;
}
@Column
public Long getDocSequenceNumber() {
	return docSequenceNumber;
}
public void setDocSequenceNumber(Long docSequenceNumber) {
	this.docSequenceNumber = docSequenceNumber;
}
}
