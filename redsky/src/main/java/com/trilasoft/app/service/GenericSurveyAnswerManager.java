package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.GenericSurveyAnswer;

public interface GenericSurveyAnswerManager extends GenericManager<GenericSurveyAnswer, Long>{
	
	public List getSurveyAnswerList(Long parentId, String sessionCorpID);

}
