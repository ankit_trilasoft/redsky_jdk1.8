package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ResourceGridDao;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.model.TableCatalog;

public class ResourceGridDaoHibernate extends GenericDaoHibernate<ResourceGrid, Long>implements ResourceGridDao{

	public ResourceGridDaoHibernate() {
		super(ResourceGrid.class);
	}
	String workLimitDate="";
	public List findResource(String hub, Date workDate,String sessionCorpID) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
		workLimitDate = nowYYYYMMDD1.toString();
		List list = getHibernateTemplate().find("from ResourceGrid where workDate='"+workLimitDate+"' and hubID='"+hub+"'");
		return list;
	}
	public List getListById(Long id, String sessionCorpID){
		List list = getHibernateTemplate().find("from ResourceGrid where parentId='"+id+"' and corpid='"+sessionCorpID+"' ");
		return list;
	}
}
