<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Transfer Materials</title>
<meta name="heading"
	content="Transfer Materials" />
	<script type="text/javascript">
	 function  InventryTransfer(){
	 var checkFlag=false;
	 for (i=0; i<document.forms['popupForMaterialTransfer'].ticketList.length; i++){
	 if (document.forms['popupForMaterialTransfer'].ticketList[i].checked==true){
	 checkFlag=true;
	 }
	 }if(checkFlag==false){
	   alert("Please select Ticket");
	 }else{
	   document.forms['popupForMaterialTransfer'].action="selectedMaterialsTransfer.html?decorator=popup&popup=true";
	   document.forms['popupForMaterialTransfer'].submit(); 
	  }	 
	 }	
	 function InventryTransferAll(){
	document.forms['popupForMaterialTransfer'].action="popupMaterialsTransfer.html?decorator=popup&popup=true";
	document.forms['popupForMaterialTransfer'].submit();
    }
    
    function pick() {	
  		parent.window.opener.document.location='inventoryDataList.html?cid=${cid}&id=${id}';
  		window.close();
	   }	
	</SCRIPT>
	<SCRIPT type="text/javascript">
   <c:if test="${hitflag=='1'}">
   pick();
   </c:if> 
</script>
</head>
<body>
<s:form action="" name="popupForMaterialTransfer">
<s:hidden name="ticketList" id="ticketList" />
<s:hidden name="id" value="${id}"/>
<s:hidden name="cid" value="${cid}"/>
<table cellspacing="0" cellpadding="0" border="0" class="table">  	
	  	    <tr>
	  			<th height="5px" class="listwhitetext" colspan="2"><b>Transfer Materials</b></th>
	  			<th height="5px" class="listwhitetext" colspan="1"><b>Service</b></th>
	  		</tr>
	  		<c:forEach var="articlesList" items="${workTicketList}" >
	  		<tr>
	  		<td height="5px" class="listwhitetext"><input type="text" class="input-textUpper" readonly="readonly"  value="${articlesList.ticket}"></td>
	  		<td height="5px" class="listwhitetext"><input type="checkbox" name="ticketList" value="${articlesList.ticket}"></td>
	  		<td height="5px" class="listwhitetext"><input type="text" class="input-textUpper" readonly="readonly"  value="${articlesList.service}" style="width:50px;"></td>
	  		</tr>
	  		</c:forEach>
</table>
</s:form >
<input type="button"  Class="cssbutton" Style="width:130px; height:25px;margin-left:25px;"  value="Transfer Selected" onclick="InventryTransfer();"/>		
<input type="button"  Class="cssbutton" Style="width:100px; height:25px;margin-left:25px;"  value="Transfer All" onclick="InventryTransferAll();"/>