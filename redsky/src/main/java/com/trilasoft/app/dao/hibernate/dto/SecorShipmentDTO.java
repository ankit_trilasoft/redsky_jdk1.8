package com.trilasoft.app.dao.hibernate.dto;

public class SecorShipmentDTO{	
    private Object quoteId;
	private Object firstName;
    private Object lastName;
	private Object createdOn;
	private Object originCity;
	private Object originCountry;
	private Object destinationCity;
	private Object destinationCountry;
	
    public Object getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Object quoteId) {
		this.quoteId = quoteId;
	}
	public Object getFirstName() {
		return firstName;
	}
	public void setFirstName(Object firstName) {
		this.firstName = firstName;
	}
	public Object getLastName() {
		return lastName;
	}
	public void setLastName(Object lastName) {
		this.lastName = lastName;
	}
	public Object getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Object createdOn) {
		this.createdOn = createdOn;
	}
	public Object getOriginCity() {
		return originCity;
	}
	public void setOriginCity(Object originCity) {
		this.originCity = originCity;
	}
	public Object getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(Object originCountry) {
		this.originCountry = originCountry;
	}
	public Object getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(Object destinationCity) {
		this.destinationCity = destinationCity;
	}
	public Object getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(Object destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
}
