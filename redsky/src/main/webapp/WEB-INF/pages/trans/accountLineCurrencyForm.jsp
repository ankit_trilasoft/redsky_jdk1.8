<%@ include file="/common/taglibs.jsp"%>
<title>Pricing Currency Form</title>
<meta name="heading" content="Pricing Currency Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>


<div id="overlay">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td ></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
<s:form id="accountLineCurrencyForm" name="accountLineCurrencyForm" action="" onsubmit="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="accountLine.estimateExpense"  /> 
 <s:hidden name="accountLine.estimateQuantity"  />
 <s:hidden name="accountLine.estimateDeviation"  />  
 <s:hidden id="formName" name="formName" value="<%=request.getParameter("formName") %>"/>
 <s:hidden name="accountLine.basis"  />
 <s:hidden name="accountLine.estimateRate"  />
 <s:hidden name="aid" value="${accountLine.id}" />  
 <s:hidden name="accountLineBasis" value="${accountLineBasis}" />
 <s:hidden name="accountLineEstimateQuantity" value="${accountLineEstimateQuantity}"  />  
<s:hidden name="formStatus"  />
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Buy&nbsp;Rate&nbsp;Currency&nbsp;Data</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    

<table border="0" style="margin:0px;padding:0px;">
															<c:if test="${contractType}"> 
															 <tr>
															 <td colspan="9">
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="1px"></td>
															 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															 <td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.estimatePayableContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();findEstimatePayableContractExchangeRate('accountLine.estimatePayableContractRate');" headerValue="" tabindex="12"  /></td>
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 
															 <c:if test="${not empty accountLine.estimatePayableContractValueDate}"> 
					                                          <s:text id="accountLineFormattedEstimatePayableContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.estimatePayableContractValueDate"/></s:text>
				                                              <td ><s:textfield id="estimatePayableContractValueDate" name="accountLine.estimatePayableContractValueDate" value="%{accountLineFormattedEstimatePayableContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
				                                              <img id="estimatePayableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.estimatePayableContractValueDate}">
					                                          <td><s:textfield id="estimatePayableContractValueDate" name="accountLine.estimatePayableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/>
					                                          <img id="estimatePayableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             
				                                             <td  align="right" class="listwhitetext" width="55px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePayableContractExchangeRate" size="8" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateEstimateRateByContractRate('accountLine.estimatePayableContractRate');"  onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePayableContractRate" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateEstimateRateByContractRate('accountLine.estimatePayableContractRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.estimatePayableContractRateAmmount" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr>
															</c:if>
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">
<tr>
													 <c:choose>
													 <c:when test="${contractType}">
													 <td align="right" class="listwhitetext" width="102">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
													 </c:when>
													 <c:otherwise>
													 <td align="right" class="listwhitetext">Currency</td>
													 </c:otherwise>
													 </c:choose>
<td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.estCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();findExchangeEstRate('accountLine.estimatePayableContractRate');" headerValue="" tabindex="18"  /></td>
<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
<c:if test="${not empty accountLine.estValueDate}"> 
<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.estValueDate"/></s:text>
<td width="102px" ><s:textfield id="estValueDate" name="accountLine.estValueDate" value="%{accountLineFormattedValue}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="estValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<c:if test="${empty accountLine.estValueDate}">
<td width="102px"><s:textfield id="estValueDate" name="accountLine.estValueDate" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="estValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<td  align="right" class="listwhitetext">Ex.&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estExchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateEstimateExpense('accountLine.estimatePayableContractRate');"/></td>
<td align="right" class="listwhitetext" width="80px">Buy&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estLocalRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateEstimateLocalAmount('accountLine.estLocalRate');" onblur=""/></td>	 
<td align="right" class="listwhitetext" width="86px">Curr&nbsp;Amount</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estLocalAmount" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateEstimateExpense('accountLine.estLocalAmount');" onblur=""/></td>	 
   			
</tr> 
</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="calculateEstRate();"/></td>
<td><input type="button"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<s:form id="accountLineCurrencyForm1"  name="accountLineCurrencyForm1" action="" method="post" validate="true">
<input type="hidden" name="aid">
<input type="hidden" name="estCurrency">
<input type="hidden" name="estValueDate">
<input type="hidden" name="estExchangeRate">
<input type="hidden" name="estLocalAmount">
<input type="hidden" name="estLocalRate">

<c:if test="${contractType}"> 
<input type="hidden" name="estimatePayableContractCurrency">
<input type="hidden" name="estimatePayableContractValueDate">
<input type="hidden" name="estimatePayableContractExchangeRate">
<input type="hidden" name="estimatePayableContractRate">
<input type="hidden" name="estimatePayableContractRateAmmount">
</c:if>
</s:form>
<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http77 = getHTTPObject77();
    var http88 = getHTTPObject77();
    function findEstimatePayableContractExchangeRate(target){
        var country =document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value; 
     //   var url="findExchangeRate.html?decorator=simple&popup=true&country="+encodeURI(country);
	  //  http88.open("GET", url, true);
	  //  http88.onreadystatechange = handleHttpResponseEstimatePayableContractRate;
	  //  http88.send(null);

 		 var rec='1';
			<c:forEach var="entry" items="${currencyExchangeRate}">
				if('${entry.key}'==country.trim()){
					rec='${entry.value}';
				}
			</c:forEach>
			document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value=rec;
            var mydate=new Date();
	          var daym;
	          var year=mydate.getFullYear()
	          var y=""+year;
	          if (year < 1000)
	          year+=1900
	          var day=mydate.getDay()
	          var month=mydate.getMonth()+1
	          if(month == 1)month="Jan";
	          if(month == 2)month="Feb";
	          if(month == 3)month="Mar";
			  if(month == 4)month="Apr";
			  if(month == 5)month="May";
			  if(month == 6)month="Jun";
			  if(month == 7)month="Jul";
			  if(month == 8)month="Aug";
			  if(month == 9)month="Sep";
			  if(month == 10)month="Oct";
			  if(month == 11)month="Nov";
			  if(month == 12)month="Dec";
			  var daym=mydate.getDate()
			  if (daym<10)
			  daym="0"+daym
			  var datam = daym+"-"+month+"-"+y.substring(2,4); 
			  document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value=datam;  
			  var estCurrency = document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value;
              if(estCurrency==country){
              	document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value;
              	document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value;
              }
			  calculateEstimateRateByContractRate(target);
  }
 /*function handleHttpResponseEstimatePayableContractRate() { 
             if (http88.readyState == 4) {
                var results = http88.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>1) {
                  document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value=results 
                  } else {
                  document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value=1;
                  }
                  var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4); 
				  document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value=datam;  
				  calculateEstimateRateByContractRate('none');
             }   }*/

 function  calculateEstimateRateByContractRate(target) {
	   <c:if test="${contractType}">
	        var country =document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value; 
	        if(country=='') {
	          alert("Please select Contract Currency ");
	          document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value=0;
	          document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value=1;
	        } else if(country!=''){
	        var estimatePayableContractExchangeRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value
	        var estimatePayableContractRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value
	        estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
	        var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
	        var roundValue=Math.round(estimateRate*10000)/10000;
	        document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value=roundValue ;  
	        calEstLocalRate(target);
	        calEstimatePayableContractRateAmmount();
	        }   
	   </c:if>
	   }
 function calEstimatePayableContractRateAmmount() { 
	   <c:if test="${contractType}">
	   var estimate=0;
	   var estimaternd=0;
	   var receivableSellDeviation=0;
	       var quantity = document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value;
	       var rate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
	   	   if(quantity<999999999999&& rate<999999999999) {
	  	 	estimate = (quantity*rate);
	  	 	estimaternd=Math.round(estimate*10000)/10000;
	  	 	} else { 
	  	 	 estimate=0;
	  	 	 estimaternd=Math.round(estimate*10000)/10000;
	  	 	} 
	         if( document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value=="cwt" || document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value=="%age") {
	      	    estimate = estimate/100; 
	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	      	    document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value = estimaternd; 
	      	      }
	          else if( document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value=="per 1000")  {
	      	    estimate = estimate/1000; 
	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	      	    document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value = estimaternd; 
	      	    } else{
	            document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value = estimaternd;
	              } 
	         estimateDeviation=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value;
	             if(estimateDeviation!='' && estimateDeviation>0){
	               estimaternd=estimaternd*estimateDeviation/100;
	               estimaternd=Math.round(estimaternd*10000)/10000;
	               document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value = estimaternd;	 
	           }
	             calculateEstimateExpenseByContractRate();   
	   </c:if>
	  		}
 function calculateEstimateExpenseByContractRate(){ 
	   var estLocalAmount= document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value ;
	   var estExchangeRate= document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ;
	   var roundValue=0; 
	  	 if(estExchangeRate ==0) {
	         document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value=0*1;
	     } else if(estExchangeRate >0) {
	       var amount=estLocalAmount/estExchangeRate; 
	       roundValue=Math.round(amount*10000)/10000;
	       document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value=roundValue ; 
	     }
	  	calculateEstimateRate("NONE");
		 }
 function calEstLocalRate(target){
	   
     var country =document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value; 
     if(country=='') {
     alert("Please select currency "); 
     document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=0; 
   } else if(country!=''){  
   <c:if test="${contractType}"> 
 	 if(target=='accountLine.estLocalAmount'){
		 var roundValue='0.00';
		 var quantityVal=document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value; 
		 var bassisVal =document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value; 
		 var estimateDeviationVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value;
		 var baseRateVal=1;
	 	 var ATemp=0.00; 
	     var ETemp =0.00;	    		 
		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			 quantityVal = document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value=1;
	         } 
	        if( bassisVal=="cwt" || bassisVal=="%age"){
	        	baseRateVal=100;
	         }  else if(bassisVal=="per 1000"){
	        	 baseRateVal=1000;
	       	 } else {
	       		baseRateVal=1;  	
		    }
	        ATemp=document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value;
	        ATemp=(ATemp*baseRateVal)/quantityVal;
		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    	ATemp=ATemp*100/estimateDeviationVal;
		    //	ATemp=Math.round(ATemp*100)/100;
		    }
   document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;

	 }else if(target=='accountLine.estimateRate'){
		 var roundValue='0.00';
	     var ETemp=0.00;
	     var ATemp=0.00;
	     ETemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;  
	     ATemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value;
	     ATemp=ATemp*ETemp;
	  //   roundValue=Math.round(ATemp*100)/100;
  	document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;
		 
	 }else if(target=='accountLine.estimateExpense'){
		 var roundValue='0.00';
	     var ETemp=0.00;
	     var ATemp=0.00;
		 var bassisVal=document.forms['accountLineCurrencyForm'].elements['accountLine.basis'].value;
		 var quantityVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value;
		 var estimateDeviationVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value;
		 var baseRateVal=1;
		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			 quantityVal = document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
	         } 
	        if( bassisVal=="cwt" || bassisVal=="%age"){
	        	baseRateVal=100;
	         }  else if(bassisVal=="per 1000"){
	        	 baseRateVal=1000;
	       	 } else {
	       		baseRateVal=1;  	
		    }
	        ATemp=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value;  
	        ATemp=(ATemp*baseRateVal)/quantityVal;
	        
		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    	ATemp=ATemp*100/estimateDeviationVal;
		    }
	     ETemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;  
	     ATemp=ATemp*ETemp;
	   //  roundValue=Math.round(ATemp*100)/100;
  	document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;
		 
	 }else if(target=='accountLine.estLocalRate'){
	 }else{
		 var roundValue='0.00';
		 	var QTemp=0.00; 
		    var ATemp =0.00;
		    var FTemp =0.00;
		    var ETemp =0.00;
		    ETemp= document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ; 
		    FTemp =document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
		    FTemp=FTemp/ETemp;
		    ETemp= document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value*1 ;				    
		    FTemp=FTemp*ETemp;
  // roundValue=Math.round(FTemp*100)/100;
   document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=FTemp;
	 }		       </c:if>
     <c:if test="${!contractType}"> 
     var recRateExchange=document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;
     var recRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value;
     var recCurrencyRate=recRate*recRateExchange;
  //   var roundValue=Math.round(recCurrencyRate*100)/100;
     document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=recCurrencyRate ; 
      </c:if>
    calculateEstimateLocalAmount(target);
   } 
}
function findExchangeEstRate(target){
        var country =document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value; 
     //   var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
	   // http77.open("GET", url, true);
	  //  http77.onreadystatechange = handleHttpResponseEstRate;
	 //   http77.send(null);


		 var rec='1';
			<c:forEach var="entry" items="${currencyExchangeRate}">
				if('${entry.key}'==country.trim()){
					rec='${entry.value}';
				}
			</c:forEach>
			document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value=rec;
         var mydate=new Date();
	          var daym;
	          var year=mydate.getFullYear()
	          var y=""+year;
	          if (year < 1000)
	          year+=1900
	          var day=mydate.getDay()
	          var month=mydate.getMonth()+1
	          if(month == 1)month="Jan";
	          if(month == 2)month="Feb";
	          if(month == 3)month="Mar";
			  if(month == 4)month="Apr";
			  if(month == 5)month="May";
			  if(month == 6)month="Jun";
			  if(month == 7)month="Jul";
			  if(month == 8)month="Aug";
			  if(month == 9)month="Sep";
			  if(month == 10)month="Oct";
			  if(month == 11)month="Nov";
			  if(month == 12)month="Dec";
			  var daym=mydate.getDate()
			  if (daym<10)
			  daym="0"+daym
			  var datam = daym+"-"+month+"-"+y.substring(2,4); 
			  document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value=datam; 
			  <c:if test="${contractType}"> 
			  var estPayableContractCurrency = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value;
              if(estPayableContractCurrency==country){
              	document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;
              	document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value = document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value;
              }
              </c:if>
			  calculateEstimateExpense(target);

	    
  }
 /*function handleHttpResponseEstRate() { 
             if (http77.readyState == 4) {
                var results = http77.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>1) {
                  document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value=results 
                  } else {
                  document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value=1;
                  document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].readOnly=false; 
                  }
                  var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4); 
				  document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value=datam;  
				  calculateEstimateExpense();
             }   }*/
  function calculateEstimateExpense(target){
  var checkValidation =  chkSelect(); 
         if(checkValidation == true){

       	  var contractRate=0;
    	  <c:if test="${contractType}">
          contractRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
          </c:if> 
          if(contractRate==0){ 
             
   var estLocalAmount= document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value ;
   var estExchangeRate= document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value*1 ;
   var roundValue=0; 
  	 if(estExchangeRate ==0) {
         document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value=0*1;
     } else  {
       var amount=estLocalAmount/estExchangeRate; 
       roundValue=Math.round(amount*10000)/10000;
       document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value=roundValue ; 
     } 
     calculateEstimateRate(target);
     calculateEstimateLocalRate(target);
          }else{
        	  calEstLocalRate(target);    
          }
  }   } 
   function calculateEstimateLocalRate(target){ 

	   <c:if test="${contractType}"> 
	 	 if(target=='accountLine.estLocalAmount'){
			 var roundValue='0.00';
			 var quantityVal=document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value; 
			 var bassisVal =document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value; 
			 var estimateDeviationVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value;
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value;
		        ATemp=(ATemp*baseRateVal)/quantityVal;
			    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			    	ATemp=ATemp*100/estimateDeviationVal;
			    	//ATemp=Math.round(ATemp*100)/100;
			    }
	   document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;

		 }else if(target=='accountLine.estimateRate'){
			 var roundValue='0.00';
		     var ETemp=0.00;
		     var ATemp=0.00;
		     ETemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;  
		     ATemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value;
		     ATemp=ATemp*ETemp;
		    // roundValue=Math.round(ATemp*100)/100;
	  	document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;
			 
		 }else if(target=='accountLine.estimateExpense'){
			 var roundValue='0.00';
		     var ETemp=0.00;
		     var ATemp=0.00;
			 var bassisVal=document.forms['accountLineCurrencyForm'].elements['accountLine.basis'].value;
			 var quantityVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value;
			 var estimateDeviationVal=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value;
			 var baseRateVal=1;
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value;  
		        ATemp=(ATemp*baseRateVal)/quantityVal;
		        
			    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			    	ATemp=ATemp*100/estimateDeviationVal;
			    }
		     ETemp = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;  
		     ATemp=ATemp*ETemp;
		   //  roundValue=Math.round(ATemp*100)/100;
	  	document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=ATemp;
			 
		 }else if(target=='accountLine.estLocalRate'){
		 }else{
			 var roundValue='0.00';
			 	var QTemp=0.00; 
			    var ATemp =0.00;
			    var FTemp =0.00;
			    var ETemp =0.00;
			    ETemp= document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ; 
			    FTemp =document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
			    FTemp=FTemp/ETemp;
			    ETemp= document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value*1 ;				    
			    FTemp=FTemp*ETemp;
	 //  roundValue=Math.round(FTemp*100)/100;
	   document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=FTemp;
		 }		       </c:if>
	     <c:if test="${!contractType}"> 
	     var estimateQuantity =0;
	     var estimateExpense=0;
	 	    estimateQuantity=document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value; 
	 	    var basis =document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value; 
	 	     estimateExpense =document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value;
	 	   
	 	    var estimateRate=0;
	 	    var estimateRateRound=0; 
	 	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
	          estimateQuantity =document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
	          } 
	         if( basis=="cwt" || basis=="%age"){
	        	  estimateRate=(estimateExpense/estimateQuantity)*100;
	        	//  estimateRateRound=Math.round(estimateRate*100)/100;
	        	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=estimateRate;	  	
	        	}  else if(basis=="per 1000"){
	        	  estimateRate=(estimateExpense/estimateQuantity)*1000;
	        //	  estimateRateRound=Math.round(estimateRate*100)/100;
	        	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=estimateRate;		  	
	        	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	           estimateRate=(estimateExpense/estimateQuantity); 
	        	//  estimateRateRound=Math.round(estimateRate*100)/100; 
	        	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=estimateRate;		  	
	 	    } 
	         estimateDeviation=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value; 
	 	    if(estimateDeviation!='' && estimateDeviation>0){
	 	    	estimateRateRound=estimateRateRound*100/estimateDeviation;
	 	    //	estimateRateRound=Math.round(estimateRateRound*100)/100;
	 	    document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value=estimateRateRound;		
	 	    }
	      </c:if>
 }
 function calculateEstimateLocalAmount(target){
  var checkValidation =  chkSelect(); 
         if(checkValidation == true){
var estimateQuantity =0;
var estLocalRate=0;
	    estimateQuantity=document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value; 
	    var basis =document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value; 
	     estLocalRate =document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value; 
	    var estimateLocalAmount=0;
	    var estimateLocalAmountRound=0; 
	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
         estimateQuantity =document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
         } 
        if( basis=="cwt" || basis=="%age"){
       	  estimateLocalAmount=(estLocalRate*estimateQuantity)/100;
       	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000;
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;	  	
       	}  else if(basis=="per 1000"){
       	  estimateLocalAmount=(estLocalRate*estimateQuantity)/1000;
       	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000;
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;		  	
       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
          estimateLocalAmount=(estLocalRate*estimateQuantity); 
       	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000; 
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;		  	
	    }  
        var contractRate=0;
    	  <c:if test="${contractType}">
          contractRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
          </c:if>  
         if(contractRate==0){   
  	    calculateEstimateExpense(target);
        }
  		    calEstimatePayableContractRateByBase(target);
  		    
  	     
}    }  
 function calEstimatePayableContractRateByBase(target){
     <c:if test="${contractType}"> 
       var country =document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value; 
       if(country=='') {
       document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value=0; 
     } else if(country!=''){  
     var estimatePayableContractExchangeRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value;
     var estLocalRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value;
     var estExchangeRate=document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value
     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
     var roundValue=Math.round(recCurrencyRate*10000)/10000; 
     if(target!='accountLine.estimatePayableContractRate'){
     document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value=roundValue ;
     }
     calEstimatePayableContractRateAmmount();
     }
   </c:if>
} 
 
  function calculateEstimateRate(target){
	    var estimateQuantity =0;
	    var estimateDeviation=100;	  
	    estimateQuantity =eval(document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value); 
	    var basis =document.forms['accountLineCurrencyForm'].elements['accountLineBasis'].value; 
	    var estimateExpense =eval(document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value); 
	    var estimateRate=0;
	    var estimateRateRound=0;
	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
         estimateQuantity =document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
         } 
        if( basis=="cwt" || basis=="%age"){
       	  estimateRate=(estimateExpense/estimateQuantity)*100;
       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value=estimateRateRound;	  	
       	}  else if(basis=="per 1000"){
       	  estimateRate=(estimateExpense/estimateQuantity)*1000;
       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
          estimateRate=(estimateExpense/estimateQuantity); 
       	  estimateRateRound=Math.round(estimateRate*10000)/10000; 
       	  document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
	    }    
	    estimateDeviation=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateDeviation'].value 
	    if(estimateDeviation!='' && estimateDeviation>0){
	    	estimateRateRound=estimateRateRound*100/estimateDeviation;
	    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
	    document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value=estimateRateRound;		
	    }  
	    }

function changeStatus() {
   document.forms['accountLineCurrencyForm'].elements['formStatus'].value = '2'; 
}
function calculateEstRate(){ 
         var checkValidation =  chkSelect(); 
         if(checkValidation == true){ 
         var aid = document.forms['accountLineCurrencyForm'].elements['aid'].value; 
         var formName = document.forms['accountLineCurrencyForm'].elements['formName'].value;
         var estCurrency = document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value; 
         var estValueDate = document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value; 
         var estExchangeRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;
         var estLocalAmount = document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value; 
         var estLocalRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value;
         <c:if test="${contractType}">
         var estimatePayableContractCurrency = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value; 
         var estimatePayableContractValueDate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value; 
         var estimatePayableContractExchangeRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value;
         var estimatePayableContractRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value; 
         var estimatePayableContractRateAmmount = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value;
         </c:if> 
         if(formName!='operationResourceForm')
  	   {
           window.opener.document.forms['serviceOrderForm'].elements['accId'].value=document.forms['accountLineCurrencyForm'].elements['aid'].value;
         window.opener.document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value;
         window.opener.document.forms['serviceOrderForm'].elements['accEstimateRate'].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value;
         window.opener.document.forms['serviceOrderForm'].elements['accEstimateExpense'].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value;         
  	   }
  	   
         if(document.forms['accountLineCurrencyForm'].elements['formStatus'].value=='2'){
        	  if(formName!='operationResourceForm')
         	   {
       	 window.opener.calculateEstimateRate(aid);
         	   }
         window.opener.setFieldValue('estLocalAmountNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value);
         
   
     if(formName!='operationResourceForm')
    	   {
         window.opener.calculateVatAmtEstExpTemp('payVatPercent'+aid,'estExpVatAmt'+aid,aid);
         }
         window.opener.changeStatus();
        <c:if test="${contractType}">
         
         window.opener.setFieldValue('estimatePayableContractCurrencyNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value);
         window.opener.setFieldValue('estimatePayableContractValueDateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value);
         window.opener.setFieldValue('estimatePayableContractExchangeRateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value);
         window.opener.setFieldValue('estimatePayableContractRateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value);
         window.opener.setFieldValue('estimatePayableContractRateAmmountNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value);
         
         <%--
         window.opener.document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractCurrency'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractValueDate'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value
         window.opener.document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value
	     --%>
	     </c:if>
         
	     window.opener.setFieldValue('estCurrencyNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value);
	     window.opener.setFieldValue('estValueDateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value);
	     window.opener.setFieldValue('estExchangeRateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value);
	     window.opener.setFieldValue('estLocalRateNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value);
	     window.opener.setFieldValue('estLocalAmountNew'+aid,document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value);
         
         <%--
	 	 window.opener.document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estCurrency'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estValueDate'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value;
         window.opener.document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value
         window.opener.document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value
         --%>
         }
         showOrHideAutoSave('1'); 
         document.forms['accountLineCurrencyForm1'].elements['aid'].value=aid;
         document.forms['accountLineCurrencyForm1'].elements['estCurrency'].value=estCurrency;
         document.forms['accountLineCurrencyForm1'].elements['estValueDate'].value=estValueDate;
         document.forms['accountLineCurrencyForm1'].elements['estExchangeRate'].value=estExchangeRate;
         document.forms['accountLineCurrencyForm1'].elements['estLocalAmount'].value=estLocalAmount;
         document.forms['accountLineCurrencyForm1'].elements['estLocalRate'].value=estLocalRate;
         <c:if test="${contractType}">
         document.forms['accountLineCurrencyForm1'].elements['estimatePayableContractCurrency'].value=estimatePayableContractCurrency;
         document.forms['accountLineCurrencyForm1'].elements['estimatePayableContractValueDate'].value=estimatePayableContractValueDate;
         document.forms['accountLineCurrencyForm1'].elements['estimatePayableContractExchangeRate'].value=estimatePayableContractExchangeRate;
         document.forms['accountLineCurrencyForm1'].elements['estimatePayableContractRate'].value=estimatePayableContractRate;
         document.forms['accountLineCurrencyForm1'].elements['estimatePayableContractRateAmmount'].value=estimatePayableContractRateAmmount;
        </c:if>

        if(formName=='operationResourceForm')
 	   {
        	var aid=document.forms['accountLineCurrencyForm'].elements['aid'].value;
        	window.opener.document.getElementById('estimateExpense'+aid).value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateExpense'].value;;
        	window.opener.document.getElementById('estimateRate'+aid).value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateRate'].value;
        	window.opener.document.getElementById('estimateQuantity'+aid).value=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value;
      	
 	   }
        
  		window.close();
} }


function showOrHideAutoSave(value) {
		    if (value==0) {
		        if (document.layers)
		           document.layers["overlay"].visibility='hide';
		        else
		           document.getElementById("overlay").style.visibility='hidden';
		   }
		   else if (value==1) {
		       if (document.layers)
		          document.layers["overlay"].visibility='show';
		       else
		          document.getElementById("overlay").style.visibility='visible';
		   }
		}


function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}	
function onlyRateAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
		return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)||(keyCode==189)|| (keyCode==110); 
	}	
	
function chkSelect() { 
    var contractRate=0;
	  <c:if test="${contractType}">
				contractRate = document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value;
			</c:if>	
     if (checkFloatWithArth('accountLineCurrencyForm','accountLine.estLocalAmount','Invalid data in Curr Amount') == false)  { 
              document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].value='0.00';
              document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalAmount'].select(); 	 
              return false
     } else if(checkFloatWithArth('accountLineCurrencyForm','accountLine.estExchangeRate','Invalid data in Exch Rate') == false) {
              document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].value='0.00';
              document.forms['accountLineCurrencyForm'].elements['accountLine.estExchangeRate'].select(); 	 
              return false
     }else if(checkFloatWithArth('accountLineCurrencyForm','accountLine.estLocalRate','Invalid data in Buy Rate') == false) {
              document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].value='0.00';
              document.forms['accountLineCurrencyForm'].elements['accountLine.estLocalRate'].select(); 	 
     } else if(contractRate==0){
            return true;
     }else{
			return chkSelect1();
         }
    }	
function chkSelect1(){
    if (checkFloatWithArth('accountLineCurrencyForm','accountLine.estimatePayableContractRateAmmount','Invalid data in Curr Amount') == false)  { 
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].value='0.00';
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRateAmmount'].select(); 	 
        return false;
	} else if(checkFloatWithArth('accountLineCurrencyForm','accountLine.estimatePayableContractExchangeRate','Invalid data in Exch Rate') == false) {
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].value='0.00';
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractExchangeRate'].select(); 	 
        return false;
	}else if(checkFloatWithArth('accountLineCurrencyForm','accountLine.estimatePayableContractRate','Invalid data in Buy Rate') == false) {
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].value='0.00';
        document.forms['accountLineCurrencyForm'].elements['accountLine.estimatePayableContractRate'].select(); 	 
	} else {
    	  return true;
	}
}
	
</script>
<%-- Shifting Closed Here --%>
<script type="text/javascript">
<c:if test="${(((!discountUserFlag.pricingActual && !discountUserFlag.pricingRevision) &&  ((accountLine.revisionExpense!='0.00' && accountLine.revisionExpense!='0.0000') || (accountLine.revisionRevenueAmount!='0.00' && accountLine.revisionRevenueAmount!='0.0000' )|| (accountLine.actualRevenue!='0.00' && accountLine.actualRevenue!='0.0000' ) || (accountLine.actualExpense!='0.00' && accountLine.actualExpense!='0.0000') || (accountLine.distributionAmount!='0.00' && accountLine.distributionAmount!='0.0000' ) || accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || ((discountUserFlag.pricingActual || discountUserFlag.pricingRevision) &&  (accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking'))}">
var elementsLen=document.forms['accountLineCurrencyForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if> 
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}"> 
var elementsLen=document.forms['accountLineCurrencyForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
try{
estimateQuantity=document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value;
if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
    document.forms['accountLineCurrencyForm'].elements['accountLineEstimateQuantity'].value=1;
    }
}catch(e){}
try{
	estimateQuantity=document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value;
	if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		document.forms['accountLineCurrencyForm'].elements['accountLine.estimateQuantity'].value=1;
	    }
	}catch(e){}

</script>
<script type="text/javascript">
showOrHideAutoSave(0);
</script>
