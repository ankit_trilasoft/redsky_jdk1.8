package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.BrokerExamDetails;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.BrokerExamDetailsManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class BrokerExamDetailsAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sessionCorpID;
	private Long sid;
    private Long id;
    private ServiceOrder serviceOrder;
    private ServiceOrderManager serviceOrderManager;
    private Billing billing;
    private BillingManager billingManager;
    private TrackingStatus trackingStatus;
    private TrackingStatusManager trackingStatusManager;
    private Miscellaneous miscellaneous;
    private MiscellaneousManager miscellaneousManager;
    private BrokerExamDetails brokerExamDetails;
    private BrokerExamDetailsManager brokerExamDetailsManager;
    Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(BrokerExamDetailsAction.class);
    private ErrorLogManager errorLogManager;
    private String hitFlag;
    private List brokerExamDetailsList;
    private  Map<String, String> refExamPay;
    private RefMasterManager refMasterManager;
    private String userName;
    private String shipNumber;
	private String fieldName;
	private String fieldValue;
	private String fieldType;
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		try{
			refExamPay = refMasterManager.findByParameter(sessionCorpID, "REFEXAMPAY");
			if(refExamPay==null || refExamPay.size()<0){
				refExamPay=new HashMap<String, String>();
			}
			}catch(NullPointerException np){
				np.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	}
	
	public  BrokerExamDetailsAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		
	}
	
	
	@SkipValidation
	public String addNewLine(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String user = getRequest().getRemoteUser();
			serviceOrder = serviceOrderManager.get(sid);
			billing=billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			brokerExamDetails = new BrokerExamDetails();
			brokerExamDetails.setCorpId(sessionCorpID);
			brokerExamDetails.setServiceOrder(serviceOrder);
			brokerExamDetails.setServiceOrderId(serviceOrder.getId());
			brokerExamDetails.setSequenceNumber(serviceOrder.getSequenceNumber());
			brokerExamDetails.setShipNumber(serviceOrder.getShipNumber());
			brokerExamDetails.setShip(serviceOrder.getShip());
			brokerExamDetails.setCreatedOn(new Date());
			brokerExamDetails.setCreatedBy(user);
			brokerExamDetails.setUpdatedOn(new Date());
			brokerExamDetails.setUpdatedBy(user); 
			brokerExamDetailsManager.save(brokerExamDetails);
			try{
				brokerExamDetailsList=new ArrayList(brokerExamDetailsManager.findByBrokerExamDetailsList(serviceOrder.getShipNumber()));
				}
				catch(Exception e){
					e.printStackTrace();
				}
		} catch (NumberFormatException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return CANCEL;
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	public String updateBrokerDetailsAjax(){
		 	userName=getRequest().getRemoteUser();
		 	brokerExamDetailsManager.updateBrokerDetailsAjaxFromList(id,fieldName,fieldValue,sessionCorpID,shipNumber,userName,fieldType);
    		return SUCCESS;
	}
	
	@SkipValidation
	public String getBrokerExamListDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			brokerExamDetailsList=new ArrayList(brokerExamDetailsManager.findByBrokerExamDetailsList(serviceOrder.getShipNumber()));
		} catch (Exception e) {			
	    	 return CANCEL;
		}		
		return SUCCESS;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public BillingManager getBillingManager() {
		return billingManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public MiscellaneousManager getMiscellaneousManager() {
		return miscellaneousManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public BrokerExamDetails getBrokerExamDetails() {
		return brokerExamDetails;
	}

	public void setBrokerExamDetails(BrokerExamDetails brokerExamDetails) {
		this.brokerExamDetails = brokerExamDetails;
	}

	public BrokerExamDetailsManager getBrokerExamDetailsManager() {
		return brokerExamDetailsManager;
	}

	public void setBrokerExamDetailsManager(
			BrokerExamDetailsManager brokerExamDetailsManager) {
		this.brokerExamDetailsManager = brokerExamDetailsManager;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public ErrorLogManager getErrorLogManager() {
		return errorLogManager;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static Logger getLogger() {
		return logger;
	}

	public List getBrokerExamDetailsList() {
		return brokerExamDetailsList;
	}

	public void setBrokerExamDetailsList(List brokerExamDetailsList) {
		this.brokerExamDetailsList = brokerExamDetailsList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getRefExamPay() {
		return refExamPay;
	}

	public void setRefExamPay(Map<String, String> refExamPay) {
		this.refExamPay = refExamPay;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	
	

}
