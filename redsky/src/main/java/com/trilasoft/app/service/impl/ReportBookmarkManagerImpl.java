package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Set;

import org.appfuse.model.Role;
import org.appfuse.service.impl.GenericManagerImpl;





import com.trilasoft.app.dao.ReportBookmarkDao;
import com.trilasoft.app.model.ReportBookmark;
import com.trilasoft.app.service.ReportBookmarkManager;

	
	public class ReportBookmarkManagerImpl extends GenericManagerImpl<ReportBookmark, Long> implements ReportBookmarkManager {
		ReportBookmarkDao reportBookmarkDao;

	    public ReportBookmarkManagerImpl(ReportBookmarkDao reportBookmarkDao) {
	        super(reportBookmarkDao);
	        this.reportBookmarkDao = reportBookmarkDao;
	    }

		public List isExist(Long reportId, String userName) {
		
			return reportBookmarkDao.isExist(reportId, userName);
		}

		public List getBookMarkedReportList( String userName, String module, String menu, String subModule,String description, String sessionCorpID, Set<Role> roles) {
			return reportBookmarkDao.getBookMarkedReportList(userName, module,  menu,  subModule,description,  sessionCorpID, roles);
		}

		
		public List getBookMarkedFormList( String userName, String module, String menu, String subModule,String description, String sessionCorpID) {
			return reportBookmarkDao.getBookMarkedFormList(userName, module,  menu,  subModule,description,  sessionCorpID);
		}
	

		
	}


		

	