/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "Insurance Report"  in Redsky for STVF its use "QUARTZ".
 * @Class Name	DynamicInsuranceReportJob
 * @Author      Ashish Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        10-OCT-2017
 */
package com.trilasoft.app.webapp.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.ooxml.*;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.ServiceOrderManager; 
import com.trilasoft.app.webapp.action.PrintDailyPackageAction;

import java.util.Calendar;

public class DynamicInsuranceReportJob extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private static final String SERVLET_CONTEXT_KEY = "servletContext";
	private ApplicationContext appCtx;
	private ReportsManager reportsManager;
	private EmailSetupManager emailSetupManager;
	


	static final Logger logger = Logger.getLogger(DynamicSQLExtractJob.class);   
	
	private ApplicationContext getApplicationContext(JobExecutionContext context) throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(APPLICATION_CONTEXT_KEY);
		reportsManager=(ReportsManager)appCtx.getBean("reportsManager"); 
		emailSetupManager=(EmailSetupManager)appCtx.getBean("emailSetupManager");
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	private ServletContext getServletContext(JobExecutionContext context)throws Exception {
		ServletContext servletContext =(ServletContext)context.getJobDetail( ).getJobDataMap().get(SERVLET_CONTEXT_KEY);
		if (servletContext == null) {
			throw new JobExecutionException(
					"No servlet context available in scheduler context for key \""+SERVLET_CONTEXT_KEY+"\"");
		}
		return servletContext;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { 
		try {
		String pathSeparator = System.getProperty("file.separator");
		appCtx = getApplicationContext(context); 
		ServletContext servletContext = getServletContext(context);
		String path = servletContext.getRealPath("/jasper");
		JasperReport reportTemplate = null;
		reportTemplate = JasperCompileManager.compileReport(path+pathSeparator+"STVF"+pathSeparator+ "InsuranceReport.jrxml"); 
		String uploadDir = servletContext.getRealPath("/images") +"/" ;
		String rightUploadDir = uploadDir.replace(uploadDir,pathSeparator + "usr" + pathSeparator + "local" + pathSeparator + "redskydoc" + pathSeparator + "InsuranceReport" + pathSeparator + "STVF" );
		File dir = new File(rightUploadDir);
		if (!dir.exists()) { 
			dir.mkdirs();
		}
		String fromDate="";
		String toDate ="";
		Map<String,Object> parameters = new HashMap<String, Object>();
		List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
		if(reportTemplate != null){ 
			JRParameter[] allParameters = reportTemplate.getParameters(); 
			SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(); 
			 fromDate=dfm.format(date);
			Date fromDateDate=dfm.parse(fromDate);
			Calendar cal = Calendar.getInstance();
	        cal.add(Calendar.DATE, -15);
	        Date todate1 = cal.getTime();    
	        toDate = dfm.format(todate1); 
	        Date toDateDate=dfm.parse(toDate);
			for (JRParameter parameter : allParameters) {
				if (!parameter.isSystemDefined()) {
					String paramName = parameter.getName();
					
					if(paramName.equalsIgnoreCase("Corporate ID")){
						parameters.put(parameter.getName(), new String("STVF"));
					}else if(paramName.equalsIgnoreCase("Date_From")){
						parameters.put(parameter.getName(),toDateDate);
                    }
					else if(paramName.equalsIgnoreCase("Date_To")){
						parameters.put(parameter.getName(), fromDateDate);
					}
                   else{
						parameters.put(parameter.getName(), "");
					}							
				}
			}
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			
		}
		JasperPrint jasperPrint = new JasperPrint();
		try {
			jasperPrint = reportsManager.generateReport(reportTemplate, parameters);
			jasperPrints.add(jasperPrint);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour while calling reportsManager.generateReport(reportTemplate, parameters): "+ e.getStackTrace()[0]);
		}
		if (jasperPrints != null && jasperPrints.size() > 0) {
			//ouputStream = response.getOutputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			/*JRExporter exporterPDF = new JRPdfExporter();
			exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, true);
			exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrints);
			exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterPDF.exportReport();*/
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporterXLS.exportReport();
			//output=byteArrayOutputStream.toByteArray();
			byte[] output = byteArrayOutputStream.toByteArray();
			
			String filePath=rightUploadDir+pathSeparator+"InsuranceReport.xls";
	        try {
	        	FileOutputStream fos = new FileOutputStream(filePath);
				fos.write(output);
				Map<String,String> pdfPathMap1 = new HashMap<String, String>();
				
				//pdfPathMap1.put(map.getKey().split("~")[0], filePath);
				String from = "";
				from="support@redskymobility.com";
				String subject ="";
				subject="Monthly SIF insurance request for All Sales -("+toDate+" to "+fromDate+")";
				String moveMangerEmail="gkumar@trilasoft.com";
				String cc="amishra@trilasoft.com";
				 String msgText1 ="Please check the attached auto generated extract file.";
				try{
					  emailSetupManager.globalEmailSetupProcess(from, moveMangerEmail, cc, "", filePath, msgText1, subject, "STVF","","Insurance Report","");
						  
				  }
				  catch(Exception sfe)
				   {
					  sfe.printStackTrace();  
					 
				   }
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception Occour: while inserting record in emailsetup table"+ e.getStackTrace()[0]);
			}
		}
		/*String ipAddress = "";
		try {
			ipAddress = "" + InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			logger.error("Error executing query " + e.getStackTrace()[0]);
		}*/
		try {/*
			//logger.warn(emailOut+"\n\n\n\n\n\n\n\n\n\n\n\n\n sql extract");
			String executionTime="";
			appCtx = getApplicationContext(context);  
			ExtractQueryFileManager extractManager=(ExtractQueryFileManager)appCtx.getBean("extractManager");
			ClaimManager claimManager=(ClaimManager)appCtx.getBean("claimManager");
			//sqlExtractManager=(SQLExtractManager)appCtx.getBean("sqlExtractManager");  
			//UserManager userManager=(UserManager)appCtx.getBean("userManager");  
			//List schedulerQueryList =sqlExtractManager.getSchedulerQueryList();
			//Iterator schedulerIterator = schedulerQueryList.iterator(); 
			//SimpleDateFormat format = new SimpleDateFormat("MM-dd");
		    //StringBuilder systemDate = new StringBuilder(format.format(new Date())); 
			//String queryScheduler=new String(""); 
			//while(schedulerIterator.hasNext()){
				
				try {
						String host = "localhost";
						   String from = "support@redskymobility.com"; 
						   String UserEmailId=sqlExtract.getEmail();
						   logger.warn("sqlExtract.getEmail()="+sqlExtract.getEmail()+"\n");
							String tempRecipient="";
							String tempRecipientArr[]=UserEmailId.split(",");
							for(String str1:tempRecipientArr){
								if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
									if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
									tempRecipient += str1;
								}
							}
							UserEmailId=tempRecipient;
							 logger.warn("After Filter from doNotEmailFlag ="+UserEmailId+"\n");
							 logger.warn("emailDomain ="+emailDomain+"\n");
							 logger.warn("emailOut ="+emailOut+"\n");
							if((emailOut!=null)&&(!emailOut.equalsIgnoreCase(""))&&(emailOut.equalsIgnoreCase("YES")) && dataReturn){						
								//Run for Dev1 & dev2
								if((emailDomain!=null)&&(!emailDomain.equalsIgnoreCase(""))){
									 String chk[]= UserEmailId.split(",");
									 String strtemp="";
									 for(String str:chk){
										 if((emailDomain.indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
												if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
												strtemp += str;
										 }
									 }
									 UserEmailId=strtemp;
									}
								 emailTo=UserEmailId;
								 logger.warn("Final TO address is ="+UserEmailId+"\n");
								 if(!UserEmailId.equalsIgnoreCase("")){
								   String to[] = UserEmailId.split(","); 
								   logger.warn("Final TO address is ="+UserEmailId+"\n");
								   //String filename = file.getName() + ".xls";
								   // Get system properties
								   Properties props = System.getProperties();
								   props.put("mail.transport.protocol", "smtp");
								   props.put("mail.smtp.host", host); 
								   Session session = Session.getInstance(props, null);
								   session.setDebug(true);
								   logger.warn(session.getProperties());
								   Message message = new MimeMessage(session);
								   message.setFrom(new InternetAddress(from)); 
								   InternetAddress[] toAddress = new InternetAddress[to.length];
								   for (int i = 0; i < to.length; i++)
								     toAddress[i] = new InternetAddress(to[i]);
								   message.setRecipients(Message.RecipientType.TO, toAddress);    
								   message.setSubject(subject);
								   BodyPart messageBodyPart = new MimeBodyPart();
								   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
								   msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
								   messageBodyPart.setText(msgText1);
								   Multipart multipart = new MimeMultipart();
								   multipart.addBodyPart(messageBodyPart);
								   messageBodyPart = new MimeBodyPart();
								   DataSource source = new FileDataSource(file_name);
								   messageBodyPart.setDataHandler(new DataHandler(source));
								   messageBodyPart.setFileName(fileName); 
								   multipart.addBodyPart(messageBodyPart);
								   message.setContent(multipart);  
								  try{
								       Transport.send(message);
								       logger.warn("\n\n\n\n\n\n\n Email11 ex    ");
								  }
								  catch(SendFailedException sfe)
								   {  
								 	 message.setRecipients(Message.RecipientType.TO,  sfe.getValidUnsentAddresses());
								 	 Transport.send(message); 
								   } 
							}
							}
						  }catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace(); 
						 }
						
						 
			//}catch(Exception e4){e4.printStackTrace();}
			//}
			
		*/}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}	
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	} 

}

