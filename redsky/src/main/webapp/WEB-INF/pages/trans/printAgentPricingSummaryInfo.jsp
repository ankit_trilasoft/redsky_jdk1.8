<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.trilasoft.app.model.PricingControlDetails"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agent Pricing Wizard | Red Sky</title>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('info', 'fade=0,persist=0,hide=0')
animatedcollapse.addDiv('origin', 'fade=0,persist=0,hide=0')
animatedcollapse.addDiv('destination', 'fade=0,persist=0,hide=0')
animatedcollapse.addDiv('originoption', 'fade=0,persist=0,hide=0')
animatedcollapse.addDiv('destinationoption', 'fade=0,persist=0,hide=0')
animatedcollapse.addDiv('freight', 'fade=0,persist=0,hide=0')
animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("tab_open.png", "tab_close.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}
 

function calculateTotal()
{
	var originPrice='${pricingControlDetailsOrigin.rate}';
	var freightPrice='${refFreightRates.totalPrice}';
	var destinationPrice='${pricingControlDetailsDestination.rate}';
	
	var currency='${pricingControlDetailsOrigin.currency}';
	
	var totalAmount=originPrice*1+freightPrice*1+destinationPrice*1;
	document.forms['agentPricing'].elements['totalPrice'].value=totalAmount;
	
	

}

function checkOriginScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
if(destinationScope!='true')
{
	alert('Origin information not provided.');
	return false;
}

}

function checkDestinationScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].value;
if(destinationScope!='true')
{
	alert('Destination information not provided.');
	return false;
}

}
function checkFreightScope(){
var freightScope=document.forms['agentPricing'].elements['pricingControl.isFreight'].value;
if(freightScope!='true')
{
	alert('Origin and/or Destination Ports information not provided.');
	return false;
}
}

function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
	
}

function handleHttpResponseCheckControlFlag(){
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                var sid='${pricingControl.serviceOrderId}';
                var newAccountingFlag=false
                <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
                newAccountingFlag=true
                </sec-auth:authComponent>
                if(newAccountingFlag==true){
                	location.href='pricingList.html?sid='+sid;	
                }else{
                	location.href='accountLineList.html?sid='+sid;
                }
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();
    
    function printPricingReport(){
    var id = 'S{pricingControl.id}';
	var date2 = document.forms['summaryForm'].elements['toDate'].value; 
	var countryCode = document.forms['summaryForm'].elements['countryCode'].value; 
	window.open("printAgentPricingSummaryReport.html?id="+id+"&decorator=popup&popup=true","scrollbars=1,status=1,menubar=no")
    
    }

</script>
<style>
.listblacktext {
color:#2F2F2F;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
}
</style>

</head>

<s:form id="agentPricing" name="agentPricing" action='locatePartners1.html' method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy hh:mm:ss}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isDestination"/>
<s:hidden key="pricingControl.isFreight"/>
		<div class="spn">&nbsp;</div>

<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width:100%;margin:0px;padding:0px;" >
  <tbody>
    <tr>
      <td width="100%" valign="top" style="margin:0px;padding: 0px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0px;padding: 0px;">
          <tbody>
            <tr>
            <tr>
              <td  colspan="8" width="100%" style="margin:0px;padding:0px;"><span onclick="swapImg(img1);">
                <div onClick="javascript:animatedcollapse.toggle('info');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Shipment Information</div></div>
                </div>
                </span>
                <div id="info" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB;">                
                <table width="100%"  cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" class="listblacktext">
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Expected Load Date:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Mode:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Weight:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" style="background-color:#EEEEEE" width="260">
					<table style="margin:0px;padding:0px;">
					<tr>
					<fmt:formatDate var="expectedDateFormattedValue" value="${pricingControl.expectedLoadDate}" pattern="${displayDateTimeEditFormat}"/>
					<td align="left"  valign="top" class="listblacktext"><fmt:formatDate value="${pricingControl.expectedLoadDate}" pattern="${displayDateFormat}"/>&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.mode}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.weight} (lbs)&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td width="90">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Packing Mode:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Container Size:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Volume:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.packing}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.containerSize}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.volume} (cft)&nbsp;</td>
					</tr>
					</table>					
					</td>
					
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Pricing Id:</b></td>
					</tr>					
					</table>
					</td>
					
					
					<td valign="top" class="listblacktext" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.pricingID}&nbsp;</td>
					</tr>
					
					</tr>
                	</table>
                	
                	</td>
                	</tr>
                	</table>
                
					                  
                	</div>
                        </td>
                    </tr>
                  </table>
                </div></td>
            </tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img2);">
                <div onClick="javascript:animatedcollapse.toggle('origin');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Origin Address</div></div>
                </div>
                </span>
                <div id="origin" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Address:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.originAddress1}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Latitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Longitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Port Of Entry:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.originLatitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originLongitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originPOEName}&nbsp;</td>
					</tr>
					</table>					
					</td>					
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                	
                	</div></td>
            </tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img3);">
                <div onClick="javascript:animatedcollapse.toggle('destination');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Destination Address</div></div>
                </div>
                </div>
                </span>
                <div id="destination" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                 <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Address:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.destinationAddress1}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Latitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Longitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Port Of Entry:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.destinationLatitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationLongitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationPOEName}&nbsp;</td>
					</tr>
					</table>					
					</td>					
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                </div></td>
            </tr>
            
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img4);">
                <div onClick="javascript:animatedcollapse.toggle('originoption');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Origin Agent</div></div>
                </div>
                </div>
                </span>
                <div id="originoption" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Name:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${originPartner.lastName }&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Fax:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Phone:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Telex:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Email:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${originPartner.terminalFax}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalPhone}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalTelex}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext"><a href="#">${originPartner.terminalEmail}</a>&nbsp;</td>
					</tr>
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Orgin Price:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsOrigin.baseCurrency}&nbsp; 
					<script type="text/javascript">
					var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
									
					totalAmount=originPrice*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table></div></td>
            </tr>
            
            
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img4);">
                <div onClick="javascript:animatedcollapse.toggle('originoption');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Origin Price Detail</div></div>
                </div>
                </div>
                </span>
                <div id="originoption" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					
					</td>
					
					<td valign="top" width="90" align="right">
					
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
									
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Detail:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					 <td align="left" colspan="2" valign="top">
					       <div id="content-containertable" class="override">
								 <div id="originDetailList"></div>
								 </div>
					      </td>
					
					</tr>
					<tr>			
					<script type="text/javascript">
					var d=document.getElementById("originDetailList");
					d.innerHTML='';
					var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
										
					var chargeDetailMarkup='${pricingControlDetailsOrigin.chargeDetailMarkUp}';
	           			
					var weightDetailMarkup=chargeDetailMarkup.substring(7,chargeDetailMarkup.indexOf(","));
	           		var haulingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Hauling:")+8,chargeDetailMarkup.indexOf("Forwarding")-1);
	           		var forwardingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Forwarding")+11,chargeDetailMarkup.length);
	           		
	           		var chargesMarkUp=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf(",")+1,chargeDetailMarkup.indexOf("Hauling")-1);
	           		if(chargesMarkUp==',')
	           		{
	           			chargesMarkUp='';
	           		}
	           									
					var chargeDetailArray=new Array();
					var chargeDetailArray=chargesMarkUp.split(",");
					d.innerHTML+='<table width="142" border="0" cellspacing="2" cellpadding="1" style="margin:0px; padding:0px;"><tr><td width="10%" height="0" valign="top" colspan="2" class="inner-tab" class="listblacktext"></td>'+
					'</tr>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Base Rate: </td><td class="listblacktext" valign="top">'+(Math.round(weightDetailMarkup*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Hauling: </td><td class="listblacktext" valign="top">'+(Math.round(haulingMarkup*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Forwarding: </td><td class="listblacktext" valign="top">'+(Math.round(forwardingMarkup*exchangeRate*100)/100).toFixed(2)+'</td></table>';
					
					for(var i = 0; i < chargeDetailArray.length; i++){
						
						if(chargeDetailArray[i].trim()!=''){
												
						var str=chargeDetailArray[i];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
												
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1"  style="margin:0px; padding:0px;"><tr><td valign="top"></td><td class="listblacktext" valign="top" width="57px">'+st1+'</td><td class="listblacktext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						
						
						}
						
					}
					
				    </script>
					
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table></div></td>
            </tr>
            
             <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img6);">
                <div onClick="javascript:animatedcollapse.toggle('freight');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Freight</div></div>
                </div>
                </div>
                </span>
                <div id="freight" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Carrier:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Origin Port:</b></td>
					</tr>					
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					<table border="0" style="margin:0px;padding:0px;">					
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.carrier}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.originPortCity},${refFreightRates.originPortCountry}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">					
					<tr><td align="right"  valign="top" class="listblacktext"><b>Origin:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Destination:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${refFreightRates.originCity} , ${refFreightRates.originCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.destinationCity} , ${refFreightRates.destinationCountry}&nbsp;</td>
					</tr>					
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Freight Quote:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Doc Fee:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsFreight.baseCurrency}&nbsp; 
					<script type="text/javascript">
					var originPrice='${refFreightRates.totalPrice}';
					var exchangeRate='${pricingControlDetailsFreight.exchangeRate}';
									
					totalAmount=originPrice*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					&nbsp; 
					
					</td>
					</tr>
					
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsFreight.baseCurrency} 
					<script type="text/javascript">
					var frieghtMarkup='${pricingControlDetailsFreight.frieghtMarkup}';
					var exchangeRate='${pricingControlDetailsFreight.exchangeRate}';
									
					totalAmount=frieghtMarkup*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					&nbsp; 
					
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                
                
                
                 </div></td>
            </tr>
            
             <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img5);">
                <div onClick="javascript:animatedcollapse.toggle('destinationoption');" style="margin:0px;">
                <div class="pricetab_bg">&nbsp;<div class="price_tleft"></a></div> <div  class="price_tright">&nbsp;Destination Agent</div></div>
                </div>
                </div>
                </span>
                <div id="destinationoption" style="margin:0px;padding:0px;">
                
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Name:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${destinationPartner.lastName}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Fax:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Phone:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Telex:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Email:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalFax}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalPhone}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalTelex}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext"><a href="#">${destinationPartner.terminalEmail}</a>&nbsp;</td>
					</tr>
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Dest Price:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsDestination.baseCurrency} 
					<script type="text/javascript">
					var originPrice='${pricingControlDetailsDestination.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
					var chargeMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
					var chargeArrayMarkUp=new Array();
					chargeArrayMarkUp = chargeMarkup.split(",");
					var dthcFlag='${pricingControlDetailsDestination.dthcFlag}';
					if(dthcFlag=='true'){
					
					for(var k =0; k<= chargeArrayMarkUp.length-2; k++)
					{
						var str=chargeArrayMarkUp[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);
						if(st1=='THC:')
						{
							originPrice= originPrice*1*exchangeRate - st2*1;
						}
						
					}
					
					}
									
					totalAmount=originPrice*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                </div></td>
            </tr>
            
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img4);">
                <div onClick="javascript:animatedcollapse.toggle('originoption');" style="margin:0px;">
                  <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Destination Price Detail</div></div>
                </div>
                </div>
                </span>
                <div id="originoption" style="margin:0px;padding:0px;border-bottom:1px solid #3DAFCB; ">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="260">
					
					</td>
					
					<td valign="top" width="90" align="right">
					
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="220">
									
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Detail:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="152">
					<table style="margin:0px;padding:0px;">
					<tr>					
					 <td align="left" colspan="2">
					       <div id="content-containertable" class="override">
								 <div id="destinationDetailList"></div>
								 </div>
					      </td>
					
					</tr>
					<tr>			
					<script type="text/javascript">
					var d=document.getElementById("destinationDetailList");
					d.innerHTML='';
					var destinationPrice='${pricingControlDetailsDestination.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
										
					var chargeDetailMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
	           			
					var weightDetailMarkup=chargeDetailMarkup.substring(7,chargeDetailMarkup.indexOf(","));
	           		var haulingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Hauling:")+8,chargeDetailMarkup.indexOf("Forwarding")-1);
	           		var forwardingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Forwarding")+11,chargeDetailMarkup.length);
	           		
	           		var chargesMarkUp=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf(",")+1,chargeDetailMarkup.indexOf("Hauling")-1);
	           		if(chargesMarkUp==',')
	           		{
	           			chargesMarkUp='';
	           		}
	           									
					var chargeDetailArray=new Array();
					var chargeDetailArray=chargesMarkUp.split(",");
					d.innerHTML+='<table width="142" border="0" cellspacing="2" cellpadding="1" style="margin:0px; padding:0px;"><tr><td width="10%" height="0" colspan="2" class="inner-tab" class="listblacktext"></td>'+
					'</tr>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Base Rate: </td><td class="listblacktext" valign="top">'+(Math.round(weightDetailMarkup*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Hauling: </td><td class="listblacktext" valign="top">'+(Math.round(haulingMarkup*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listblacktext" valign="top">Forwarding: </td><td class="listblacktext" valign="top">'+(Math.round(forwardingMarkup*exchangeRate*100)/100).toFixed(2)+'</td></table>';
					
					for(var i = 0; i < chargeDetailArray.length; i++){
						
						if(chargeDetailArray[i].trim()!=''){
												
						var str=chargeDetailArray[i];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
						
						if(dthcFlag=='true'){
						if(st1!='THC:')
						{						
							d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1"  style="margin:0px; padding:0px;"><tr><td valign="top"></td><td class="listblacktext" valign="top" width="57px">'+st1+'</td><td class="listblacktext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
						}else{
							d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1"  style="margin:0px; padding:0px;"><tr><td valign="top"></td><td class="listblacktext" valign="top" width="57px">'+st1+'</td><td class="listblacktext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
					}
						
					}
					
				    </script>
					
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table></div></td>
            </tr>
            
          </tbody>
        </table>
        
        
        </td>
    </tr>
   
  </tbody>
</table>
<table class="detailTabLabel" style="margin:0px;padding:0px;width:785px;" border="0"> 
 <tr>
 <td align="left" valign="top" class="listblacktext" style="width:100px"></td>
	<td align="right"  valign="middle" class="listblacktext"><b>Total Price:</b></td>
	<td align="left" valign="top" class="listblacktext" style="width:100px">
<c:choose>
	<c:when test="${pricingControl.isOrigin==true}" >
		${pricingControlDetailsOrigin.baseCurrency} 
	</c:when>
	<c:when test="${pricingControl.isOrigin==true}" >
			${pricingControlDetailsDestination.baseCurrency} 
	</c:when>
	<c:otherwise>
		${pricingControlDetailsFreight.baseCurrency}
	</c:otherwise>
</c:choose>

	<script type="text/javascript">
	var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
	var freightPrice='${refFreightRates.totalPrice}';
	var destinationPrice='${pricingControlDetailsDestination.rateMarkUp}';
	var chargeMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
	var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
	var chargeArrayMarkUp=new Array();
	chargeArrayMarkUp = chargeMarkup.split(",");
	var dthcFlag='${pricingControlDetailsDestination.dthcFlag}';
	if(dthcFlag=='true'){
	
	for(var k =0; k<= chargeArrayMarkUp.length-2; k++)
	{
		var str=chargeArrayMarkUp[k];
	    var st1 = str.substring('0',str.indexOf(":")+1);
	    var st22 = str.substring(str.indexOf(":")+1,str.length)
	    var st2 = st22.substring('0',st22.indexOf(" "));
	    var st33 = st22.substring(st22.indexOf(" "),st22.length);
	    var st3 = st33.substring('0',st33.length);
		if(st1=='THC:')
		{
			destinationPrice= destinationPrice*1*exchangeRate - st2*1;
		}
		
	}
	
	}
	var originExchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
	var destinationExchangeRate='${pricingControlDetailsDestination.exchangeRate}';
	var freightExchangeRate='${pricingControlDetailsFreight.exchangeRate}';
	var frieghtMarkup='${pricingControlDetailsFreight.frieghtMarkup}';	
	totalAmount=originPrice*1.00*originExchangeRate+freightPrice*1.00*freightExchangeRate+destinationPrice*1.00*destinationExchangeRate + frieghtMarkup*1.00*freightExchangeRate;
	
	document.write(totalAmount.toFixed(2));
	</script>
	</td>					
					
</tr>

 </table>	
 			
</div>
</s:form>
<script type="text/javascript">
window.print();
</script>
