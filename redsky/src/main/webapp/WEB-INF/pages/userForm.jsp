<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%> 

<style type="text/css">
/* collapse */

div.error, span.error, li.error, div.message {
width:450px;
}

table.pickList11 td select {
    width: 400px;
}
form .info {
text-align:left;
padding-left:15px;
}

input.button, button {
padding:2px 2px 0 0;
width:4em;
}
</style>

<head>
    <title><fmt:message key="userProfile.title"/></title>
    <meta name="heading" content="<fmt:message key='userProfile.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    
    <script type="text/javascript"><!--
    function  setMobileService(){
        if(${user.id!=null}){
           if(${company.accessRedSkyCportalapp==true}){
        	   if(${user.macid=='ACCOUNT_DISABLED'}){
        	   }else{
        	   document.forms['userForm'].elements['mobile_serv_access'].checked=true;
        	   }
           }else if(${company.accessRedSkyCportalapp==false}){
        	   document.forms['userForm'].elements['mobile_serv_access'].checked=false;
        	   document.forms['userForm'].elements['mobile_serv_access'].disabled=true;
           }
       }else {     
    	   if(${company.accessRedSkyCportalapp==true}){
     	   	   document.forms['userForm'].elements['mobile_serv_access'].checked=true;   
    	    }else{
    	    	 document.forms['userForm'].elements['mobile_serv_access'].checked=false; 
    	    	 document.forms['userForm'].elements['mobile_serv_access'].disabled=true;
    	    	 
    	    }
    }
    }
    
    function  checkLength(){
	var txt = document.forms['userForm'].elements['user.signature'].value;
	if(txt.length >600){
	    alert("Cannot enter more than 600 characters in Signature.");
	    document.forms['userForm'].elements['user.signature'].value = txt.substring(0,600);
		return false;
	}
}

function getChange(val){
	var fileCabinetValue=val.value;
	var e12 = document.getElementById('reasonHid1');
	var e13 = document.getElementById('reasonHid2');
	var e15 = document.getElementById('reasonHid4');
	  if(fileCabinetValue=='Document List' || fileCabinetValue==''){
		  e12.style.display="none";
		  e13.style.display="block";
		  e15.style.display="none";
	    } else {
		      e12.style.display="block";
			  e13.style.display="none";
			  e15.style.display="block";
	    }
}
</script>
    
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
 <script type="text/javascript">
 
 function toggleLayer( Layer4 ){ 
  var elem, vis;  if( document.getElementById ) // this is the way the standards work    
  elem = document.getElementById( Layer4 );  
  else if( document.all ) // this is the way old msie versions work      
  elem = document.all[Layer4];  
  else if( document.layers ) // this is the way nn4 works    
  elem = document.layers[Layer4];  
  vis = elem.style;  // if the style.display value is blank we try to figure it out here  
  if(vis.display==''&&elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)    
  vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';  
  vis.display = (vis.display==''||vis.display=='block')?'none':'block';}
  
   function getAgentCode(){
 		var countryCode= document.forms['userForm'].elements['user.address.country'].value;
 		var stateCode= document.forms['userForm'].elements['user.address.province'].value;
 		var userType= document.forms['userForm'].elements['user.userType'].value;
 		if(userType == 'ACCOUNT'){
 				openWindow('destinationPartners.html?userType='+userType+'&destination='+countryCode+'&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentName&fld_code=user.parentAgent');
				document.forms['userForm'].elements['user.parentAgent'].select();
 		}else{
 			openWindow('destinationPartners.html?userType='+userType+'&destination='+countryCode+'&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentName&fld_code=user.parentAgent');
			document.forms['userForm'].elements['user.parentAgent'].select();
 		}
	
	
	}
	
	function getBookingAgentCode(){
 			openWindow('bookingAgentPopupNew.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentName&fld_code=user.bookingAgent');
			document.forms['userForm'].elements['user.bookingAgent'].select();	
	}
function getContract()
{
openWindow('contracts.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=user.contract');
document.forms['userForm'].elements['user.contract'].select();
}
	
function checkForCurrentUser(){
var remoteUser= document.forms['userForm'].elements['remoteUser'].value;
if('${user.username}'==remoteUser){
alert('You cannot change your Username');
document.forms['userForm'].elements['user.username'].value=remoteUser;
}
else
{
	document.forms['userForm'].elements['user.username'].value=document.forms['userForm'].elements['user.username'].value.trim();
}
}

function parentAgentRequired()
{
		var userType=document.forms['userForm'].elements['user.userType'].value;
		try{        
		var el = document.getElementById('coordRequiredTrue');
		var el1 = document.getElementById('coordRequiredFalse');
		if(userType == 'ACCOUNT' || userType == 'AGENT'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		}
		}catch(e){}
		try{
		var e3 = document.getElementById('userTitleRequiredTrue');
		var e33 = document.getElementById('userTitleRequiredFalse');
		if(userType == 'ACCOUNT' || userType == 'AGENT'){
		e3.style.display = 'block';		
		e33.style.display = 'none';		
		}else{
		e3.style.display = 'none';
		e33.style.display = 'block';
		}
		}catch(e){}
		
}
function findbasedAt(){
    var basedAtCode = document.forms['userForm'].elements['user.basedAt'].value;
    if(basedAtCode==''){
       document.forms['userForm'].elements['user.basedAtName'].value="";
    }
}

var httpRole = getHTTPRole();
function getHTTPRole(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}

function feedRoleOnChange(){
		var category = document.forms['userForm'].elements['user.userType'].value;
		var url="ajaxRoleBasedOnCategory.html?decorator=simple&popup=true&category=" + category + "&leftId=availRoles && !userRoles&rightId=userRoles";		
		httpRole.open("GET", url, true);
		httpRole.onreadystatechange = httpRoleName;
		httpRole.send(null);		
	} 
function httpRoleName(){
	 if (httpRole.readyState == 4){
         var results = httpRole.responseText;
         results = results.trim();
         if(document.getElementById('availRoles && !userRoles') != null){
        	 document.getElementById('availRoles && !userRoles').innerHTML = results;
         }else{
        	 document.getElementById('availRoles').innerHTML = results;
         }
	 }
}
function emailValidate(form_id,email,message) {
	 var reg = /^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;
	 var address = document.forms['userForm'].elements['user.email'].value;
	    if(address!=''){
	   if(reg.test(address) == false) {
	      alert('Invalid '+message+' Address');
	      document.forms['userForm'].elements['user.email'].value='';
	      return false;
	     }
	   }
	}
</script>
   
</head> 
<s:form cssClass="form_magn" name="userForm" id="userForm" action="saveUser" onsubmit="return submit_form(),checkJobValids();" enctype="multipart/form-data" method="post" validate="true" >
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>

<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="category"/>
<s:hidden name="formStatus" value=""/>
<s:hidden name="agentName" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="description" />
<s:hidden name="user.jobFunction"/>
<s:hidden name="user.login_time"/>
<s:hidden name="user.login_status"/>
<s:hidden name="user.macid"/>
<s:hidden name = "user.corpID" />
<s:hidden name = "user.relocationContact" />
<s:hidden name = "user.NPSscore" />
<s:hidden name="user.loginFailureReason" value="${user.loginFailureReason}"/>
<s:hidden name="remoteUser" value="${pageContext.request.remoteUser}"/>
<s:hidden name="chngPwsd" value="0"/>
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<c:choose>
<c:when test="${gotoPageString == 'gototab.user' && validateFormNav!='NOTOK'}">
   <c:redirect url="/myUsers.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<c:set var="rolesAssignFlag" value="N" />
<c:set var="rolesAssign" value="${roles}" />
<c:if test="${fn:contains(rolesAssign, 'ROLE_ADMIN')}">
<c:set var="rolesAssignFlag" value="Y" />
</c:if>

<div id="newmnav" class="nav_tabs"><!-- sandeep -->
  <ul>
   <c:if test="${param.from == 'list'}" >
   <li><a onclick="selectAll('userRoles');selectAll('permissions');
	<c:if test="${user.userType== 'USER' || user.userType == 'ACCOUNT' || user.userType == 'AGENT'}">
   selectAll('userJobType');
   	</c:if>
   setReturnString('gototab.user');return autoSaveFunc('none');onFormSubmit(this.form);return IsValidTime();" onmouseover="completeTimeString();return validateRole();"><span>Users</span></a></li>
   </c:if>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>User Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
   <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${user.id}&tableName=app_user,user_role,userdatasecurity&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
</ul>
</div>
<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!padding-bottom:6px;"></div>
<s:hidden key="user.id"/>
<s:hidden key="user.version"/>
<input type="hidden" name="from" value="${param.from}"/>
	<c:if test="${cookieLogin == 'true'}">
	    <s:hidden key="user.password" />
	    <s:hidden key="user.confirmPassword"/>
	</c:if>
	<s:if test="user.version == null">
    	<input type="hidden" name="encryptPass" value="true" />
	</s:if>

	<c:set var="buttons" >
	<s:submit cssClass="cssbuttonA" key="button.save" method="save" onclick="return onFormSubmit(this.form);" onmouseover="completeTimeString();return validateRole();" /> 
	     <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/myUsers.html"/>'" 
	        value="<fmt:message  key="button.cancel"/>" tabindex="88"/>
	   	<%-- <s:submit type="button" cssClass="cssbuttonA" cssStyle="!margin-left:5px;" key="button.cancel" method="cancel"/> --%>
	   	<s:reset type="button" cssClass="cssbuttonA" cssStyle="!margin-left:5px;" key="Reset" onclick="resetFields();getChange1();"/>
	 <c:if test='${not empty user.id }'>
	 <c:if test="${adminAccess}">	
	<input type="button" class="cssbutton1"
	        onclick="location.href= '<c:url  value="copyUser.html?from=list&cid=${user.id}"/>'"
	        value="Copy User"  style="width: 69px;height:25px;margin-bottom:5px;" tabindex="88"/>
    </c:if></c:if>
	</c:set>

<div id="Layer1" style="width:100%" onkeydown="changeStatus();" > 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">

 <table class="" cellspacing="0" cellpadding="0" border="0" width="100%">
  <tbody>
	<tr>
	<td colspan="2">
	
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;User Profile
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>

	</td>
	</tr>
	<tr><td align="left" class="info">
	    			        <c:choose>
			            <c:when test="${param.from == 'list'}">
			                <p><fmt:message key="userProfile.admin.message"/></p>
			            </c:when>
			            <c:otherwise>
			                <p><fmt:message key="userProfile.message"/></p>
			            </c:otherwise>
			        </c:choose>
			   
 	</td></tr>
 	
 	
	<tr>
	<td>
	<fieldset style="width:990px;">	
	<legend>General Info</legend>	
	<table class="detailTabLabel" border="0">
 	
 	<tr><td align="left" class="listwhitetext">
       		<table  class="detailTabLabel" border="0">
	 		  	<tbody>
	 		  			<tr><td align="left">
	 		  			
	 		  			</td></tr>
	 		  			<tr>
	 		  			  		<td align="left" width="90px"></td>
			 		  			<td align="left"><fmt:message key="user.firstName"/><font color="red" size="2">*</font></td>
			 		  			<td></td>
			 		  			<td align="left"><fmt:message key="user.initial"/></td>
			 		  			<td align="left"><fmt:message key="user.lastName"/><font color="red" size="2">*</font></td>
			 		  			<td align="left" colspan="2"><fmt:message key="user.username"/><font color="red" size="2">*</font></td>
			 		  			<%-- <td align="left"><fmt:message key="user.corpID"/><font color="red" size="2">*</font></td> --%>
			 		  			   <sec-auth:authComponent componentId="module.section.userForm.configuration">
			 		  			<td align="left"><fmt:message key="user.supervisor"/></td>
			 		  			</sec-auth:authComponent>
			 		  			
			 		  	</tr>
			 		  	<tr>
	 		  					<td align="left" width="90px"></td>
			 		  			<td align="left" colspan="2"><s:textfield key="user.firstName"  required="true" cssClass="input-text" maxlength="40" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
			 		  			<td align="left"><s:textfield key="user.initial"  cssClass="input-text" size="1" maxlength="5" onkeydown="return onlyCharsAllowed(event)" tabindex="2" /></td>
			 		  			<td align="left"><s:textfield key="user.lastName"  required="true" cssClass="input-text" size="18" maxlength="40" onkeydown="return onlyCharsAllowed(event)" tabindex="3" /></td>
			 		  			<c:choose>
			    				<c:when test="${empty user.id}">
			 		  			<td align="left" colspan="2"><s:textfield key="user.username" cssClass="input-text" required="true" cssStyle="font-weight:bold;" size="26" maxlength="25" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="4" onchange="checkForCurrentUser()"/></td>
			 		  			</c:when>
								    <c:otherwise>
								    <td align="left" colspan="2"><s:textfield key="user.username" cssClass="input-textUpper" required="true" readonly="true"  cssStyle="font-weight:bold;" size="26" maxlength="25" onkeydown="return onlyAlphaNumericAllowed(event)" onchange="checkForCurrentUser()" tabindex="4"/></td>
								    </c:otherwise>
								 </c:choose>
			 		  			<%-- <td align="left"><s:textfield key="user.corpID"  cssClass="input-textUpper" required="true" readonly="true" size="7" maxlength="4" onkeydown="return onlyCharsAllowed(event)" tabindex="5"/></td> --%>
			 		  			<td align="left">
			 		  			<sec-auth:authComponent componentId="module.section.userForm.configuration">
			 		  			<c:if test="${param.from == 'list'}">
			 		  			<s:select list="%{supervisorList}" key="user.supervisor" cssClass="list-menu" cssStyle="width:95px" headerKey="" headerValue="" tabindex="6"/>
			 		  			</c:if>
			 		  			<c:if test="${param.from != 'list'}">
			 		  			<s:textfield key="user.supervisor" cssClass="input-textUpper" cssStyle="width:95px" required="true" readonly="true"  tabindex="6"/>
			 		  			</c:if>
			 		  			</sec-auth:authComponent>
		
	 	                </tr>
	 	             <tr><td align="left" height="1"></td></tr>
	 	                <tr>
	 	                <td align="left" width="90px"></td>
	 	                
	 	                <td align="left" colspan="4"><fmt:message key="user.alias" /></td>
	 	                <td align="left" width="115px">Password Expiry Date</td>
	 	                <td align="left" >Do not Email</td>
						<td align="left" id="genderRequiredTrue" class="listwhitetext">Gender</td>
						
	 	                </tr>
	 	                
	 	             
	 	                <tr>
		 	                <td align="left" width="90px"></td>
				 		  	<td align="left" colspan="4"><s:textfield name="user.alias"  required="true" cssClass="input-text" cssStyle="width:300px; margin-right:24px;" maxlength="30" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="7"/></td>
				 		  	<td align="left" colspan="1">
				 		  	<table cellspacing="0" cellpadding="0" border="0" class="detailTabLabel" style="margin:0px;padding:0px">
				 		  	<tr>				 		  	
							<c:if test="${not empty user.pwdexpiryDate}">
								<s:text id="pwdexpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="user.pwdexpiryDate"/></s:text>
								<td width="65">
								<c:if test="${param.from == 'list'}">
								<s:textfield cssClass="input-text" id="pwdexpiryDate" name="user.pwdexpiryDate" value="%{pwdexpiryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event)" /></td>
								<td>
									<img id="pwdexpiryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
									</c:if>
									
									<c:if test="${param.from != 'list'}">
								<s:textfield cssClass="input-textUpper" required="true" readonly="true" id="pwdexpiryDate" name="user.pwdexpiryDate" value="%{pwdexpiryDateFormattedValue}"  cssStyle="width:65px" maxlength="11" /></td>
								<td>
									</c:if>
								</td>						
							</c:if>
							<c:if test="${empty user.pwdexpiryDate}">
							<c:if test="${param.from == 'list'}">
								<td width="65"><s:textfield cssClass="input-text" id="pwdexpiryDate"  name="user.pwdexpiryDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event)" /></td><td>								
										<img id="pwdexpiryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
									</c:if>
									<c:if test="${param.from != 'list'}">
								<td width="65"><s:textfield cssClass="input-textUpper" required="true" readonly="true" id="pwdexpiryDate" name="user.pwdexpiryDate" cssStyle="width:65px" maxlength="11" /></td><td>
																	
									</c:if>
								</td>
							</c:if>
			 		  		</tr>
			 		  		</table>
			 		  		</td>
			 		  		<td align="left"><s:checkbox key="user.doNotEmail" id="user.doNotEmail" fieldValue="true" onclick="changeStatus()" /></td>
							<td align="left" colspan="1"><div id="genderRequiredTrueText"><s:select cssClass="list-menu" name="user.gender" list="%{gender}" headerKey="" headerValue="" cssStyle="width:95px"/></div></td>
							
    	
			 		  	</tr>			 		  
			 		</tbody>
	 	  	</table></td></tr>	 	  	
    		<c:if test="${cookieLogin != 'true'}">
    		<tr><td align="left" class="listwhitetext">
    		<table  class="detailTabLabel" border="0">
	 		  	<tbody> 
	 		   			<tr>
	 		  			  		<td align="left" width="90px"></td>
			 		  			<td align="left"><fmt:message key="user.password"/><font color="red" size="2">*</font></td>
			 		  			<td align="left">&nbsp;<fmt:message key="user.confirmPassword"/><font color="red" size="2">*</font></td>			 		  			
			 		  	</tr>
	 		  			
						<tr>
			 		  			<td align="left"></td>
			 		  			<td align="left"><s:password key="user.password" showPassword="true" required="true" cssStyle="width:138px; margin-right:16px;" cssClass="input-text" onchange="passwordChanged(this)" tabindex="8"/></td>
			 		  			<td align="left"><s:password key="user.confirmPassword" required="true" cssStyle="width:138px; margin-right:21px;" showPassword="true" cssClass="input-text" onchange="passwordChanged(this)" tabindex="9"/></td>
			 		  			<td align="left" colspan="3">
			 		  			<table style="margin:0px;padding:0px;">
			 		  			<tr>			 		  			
			 		  			<c:choose>
			 		  			<c:when test="${systemDefault.passwordPolicy=='true'}">			 		  					 		  			
			 		  			<td width="20">
			 		  			<div id="pswdpolicyId">
			 		  			<table style="margin:0px;padding:0px;">
			 		    		<tr>
			 		  			<td align="left"><a onmouseout="ajax_hideTooltip();" onmouseover="ajax_showBigTooltip('findtoolTipForPswd.html?ajax=1&decorator=simple&popup=true',this)"><font style="font-weight:bold;">Password&nbsp;Policy</font></a></font></td>			 		  			
			 		  			</tr>
			 		  			</table>
			 		  			</div>
			 		  			</td>	  			
			 		  			
			 		  			</c:when>			 		  			
			 		  			<c:otherwise>			 		  			
			 		  			</c:otherwise>		  			
			 		  		</c:choose>
			 		  		<c:choose>
			    				<c:when test="${param.from == 'list' or param.method == 'Add'}">
			 		  			<td align="left" width="20"><s:checkbox key="user.passwordReset" id="user.passwordReset" fieldValue="true" onclick="changeStatus()" tabindex="10"/></td>
			 		  			<td align="left"><fmt:message key="user.passwordReset"/></td>
			 		  			</c:when>
			 		  			</c:choose>	
			 		  			</tr>
			 		  			</table>
			 		  			</td>
			 		  
			 		  	</tr>
			 		  	<tr><td align="left" height="1"></td></tr>
			 		  	<tr>
	 		  			  		<td align="left" width="90px"></td>
			 		  			<td align="left" colspan="2"><fmt:message key="user.passwordHintQues"/><font color="red" size="2">*</font></td>
			 		  			<td align="left" colspan="4"><fmt:message key="user.passwordHint"/><font color="red" size="2">*</font></td>
			 		  	</tr>
			 		  	<tr>	
			 		  	<td align="left" width="90px"></td>
			 		  	<td align="left" colspan="2"><s:select cssClass="list-menu" key="user.passwordHintQues" list="%{PWD_HINT}"  cssStyle="width:267px" onclick="changeStatus()" tabindex="11"/></td>
			 		  	
			 		  	<td colspan="8">	 		  			
	 		  			<table style="margin:0px;padding:0px;">
	 		    		<tr>
			 		  	<td align="left" ><s:textfield key="user.passwordHint" required="true" cssClass="input-text" cssStyle="width:121px" maxlength="40" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="12"/></td>
			 		  	<td align="right"  width="75"><fmt:message key="user.userType"/><font color="red" size="2">*</font></td></td>
			 		    <td align="left">
			 		    <c:if test="${param.from == 'list'}">
			 		    <s:select cssClass="list-menu" key="user.userType" list="%{usertype}"  cssStyle="width:99px" headerKey="" headerValue="" onclick="changeStatus()" onchange="feedRoleOnChange();parentAgentRequired();showMobiliceService(this);showPsswdPolicy(this);showConfigDefaultInfo(this);" tabindex="13"/>
			            </c:if>
			 		    <c:if test="${param.from != 'list'}">
			 		    <s:textfield cssClass="input-textUpper" required="true" readonly="true" key="user.userType"  cssStyle="width:94px"  tabindex="13"/>
			 		    </c:if>
			 		    </td>
			 		    <td colspan="2">
			 		    <div id='maccess' style="display:none;">
			 		    <table style="margin:0px;padding:0px;">
			 		    <tr>
			 		    <td align="right"  width="65">Enable&nbsp;Mobile&nbsp;Access</td>			 		   
			 		    <td><s:checkbox  name="mobile_serv_access" cssStyle="margin:0px;" fieldValue="true" /></td>
			 		    </tr>
			 		    </table>
			 		    </div>
			 		    </td>
			 		  	</tr>
			 		  	</table>
			 		  	</td>
			 		  	<tr><td align="left" height="1"></td></tr>
			 	<tr>
					<td align="right" id="userTitleRequiredFalse" width="90px">Job&nbsp;Title</td>
					<td align="right" id="userTitleRequiredTrue" width="90px">Job&nbsp;Title<font color="red" size="2">*</font></td>
 		  			<td align="left" colspan="2"><s:textfield name="user.userTitle" cssClass="input-text" cssStyle="width:265px;" maxlength="50" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="14"/></td>
 		  	    	<td align="right" width="">Job&nbsp;Type</td>
 		  			<td align="left" colspan="2">
 		  			<c:if test="${param.from == 'list'}">
 		  			<s:textfield name="user.jobType" cssClass="input-text" cssStyle="width:226px;" maxlength="73" tabindex="14"/>
 		  			</c:if>
 		  			<c:if test="${param.from != 'list'}">
 		  			<s:textfield name="user.jobType" cssClass="input-textUpper" required="true" readonly="true" cssStyle="width:226px;" maxlength="73" tabindex="14"/>
 		  			</c:if>
 		  			</td>
 		  			<td width="120px" align="left" class="listwhitetext"><b>e.g. ('INT','DOM',etc.)</b></td>
 		  		</tr>
 		  		<tr>
 		  		<td align="right">Company</td>
			 	<td align="left" colspan="2"><s:textfield name="user.branch" cssClass="input-text" maxlength="49" tabindex="20" cssStyle="width:265px;"/></td>
			 	<td align="right">Func. Area</td>
			 	<td align="left"><s:textfield name="user.functionalArea" cssClass="input-text" cssStyle="width:226px;" maxlength="73" tabindex="21"/></td>
 		  		</tr>
			 		  	<tr>
	 	  	<td align="right" class="listwhitetext">Based&nbsp;At</td>
	 	  	<td colspan="2"><s:textfield cssClass="input-text" name="user.basedAt" readonly="true" onkeypress="findbasedAt()" onkeydown="return onlyDel(event,this)" cssStyle="width:99px;" />
	 	  	
	 	  	<c:choose>
	 	  	<c:when test="${hasDataSecuritySet}">
	 	  	<img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=user.basedAtName&fld_code=user.basedAt');" src="<c:url value='/images/open-popup.gif'/>" />
	 	  	</c:when>
	 	  	<c:otherwise>
	 	  	<img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=user.basedAtName&fld_code=user.basedAt');" src="<c:url value='/images/open-popup.gif'/>" />
	 	  	</c:otherwise>
	 	  	</c:choose>
	 	  	</td>
			<td align="right" class="listwhitetext">Based&nbsp;At&nbsp;Name</td>
			<td colspan="5"><s:textfield cssClass="input-textUpper" readonly="true" name="user.basedAtName"  cssStyle="width:226px;"/></td>
	 	  	</tr>
	 	  	 <tr>
	 	  	 <td align="right" valign="top" id="coordRequiredFalse" class="listwhitetext">Parent&nbsp;Agent</td>
			<td align="right" valign="top" id="coordRequiredTrue" class="listwhitetext">Parent&nbsp;Agent<font color="red" size="2">*</font></td>				 		  						
			<c:if test="${param.from == 'list'}">
			<td align="left" width="127">
			<table style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
            <tr>
            <td>
            <c:if test="${user.userType== 'USER'}">
            <c:if test="${adminAccess}">
			<s:textfield name="user.parentAgent" cssClass="input-text" cssStyle="width:99px;" maxlength="10" tabindex="27"  onkeydown="onlyDel(event,this)" onchange="findParentAgentCode('PA')"/>
			</c:if>
			<c:if test="${!adminAccess}">
			<s:textfield name="user.parentAgent" cssClass="input-textUpper" cssStyle="width:99px;" maxlength="10" tabindex="27" readonly="true"/>
			</c:if>
			</c:if>
			<c:if test="${user.userType!= 'USER'}">
			<s:textfield name="user.parentAgent" cssClass="input-text" cssStyle="width:99px;" maxlength="10" tabindex="27" readonly="true"/>
			</c:if>
			</td>
			<c:if test="${user.userType== 'USER'}">
			<c:if test="${adminAccess}">
			<td>&nbsp;<img align="left" class="openpopup" width="17" height="20" style="float:right;" onclick="javascript:getAgentCode()" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>	
			</c:if>
			</c:if>	
			<c:if test="${user.userType!= 'USER'}">
			<td>&nbsp;<img align="left" class="openpopup" width="17" height="20" style="float:right;" onclick="javascript:getAgentCode()" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>	
			</c:if>
			</tr>
			</table>
			</td>
			</c:if>			
			<c:if test="${param.from != 'list'}">
			<td width="127"><s:textfield name="user.parentAgent" cssClass="input-textUpper" required="true" readonly="true" cssStyle="width:99px" maxlength="10" tabindex="27"/></td>
			</c:if>	
			<c:if test="${user.corpID == 'VOER' ||  user.corpID == 'VORU'}">
			<td></td>
			<td align="right" class="listwhitetext">Employee#</td>
			<td colspan="5"><s:textfield cssClass="input-text"  name="user.employee"  maxlength="25" cssStyle="width:226px;"/></td>
	 	    </c:if>
	 	  	 </tr>		  	

	 		  	</tbody>
	 	  	</table></td></tr>	 	  	
	 	  	  </c:if>	 	  	  
	 	  	<tr><td align="left" class="listwhitetext">
	 	  	<table  class="detailTabLabel" border="0">
	 		  	<tbody>
	 		  			
						
			 		  	<tr>	<td width="39"></td>
			 		  			<td align="right"><fmt:message key="user.signature"/></td>
			 		  			<td align="left" rowspan="1" valign="bottom"><s:textarea name="user.signature" cssStyle="margin-bottom:4px;width:266px;height:75px;"  cssClass="textarea" onkeypress="" tabindex="15"/></td>
			 		  			<td colspan="4">
			 		  	<table style="margin:0px;padding:0px;">
			 		  	<tr>
			 		  	<c:if test="${user.digitalSignature == null || user.digitalSignature == ''}">
						<td align="center" class="listwhitetext" colspan="2"><img id="userSignature" src="${pageContext.request.contextPath}/images/no-signature.png" alt="" width="80" height="80"  style="border:thin solid #219DD1;"/></td>
			 		  	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Digital&nbsp;Signature</td>
			 		  	</c:if>
						<c:if test="${!(user.digitalSignature == null || user.digitalSignature == '')}">
			 		  		<td align="right" class="listwhitetext" width="1px"  colspan="2"><img id="userSignature" src="UserImage?location=${user.digitalSignature}" alt="" width="80" height="90"  style="border:thin solid #219DD1;"/></td>
			 		  		<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Digital&nbsp;Signature</td>
			 		  	</c:if>
							<td align="left" valign="bottom" class="listwhitetext" ><s:file name="signatureFile" cssClass="text file" required="true" tabindex="26"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px;" onclick="removePhotograph1();" tabindex="27"/></td>
							<%--<td align="right"><s:submit type="button" class="cssbutton" name="Remove" style="width:60px; height:20px" value="Remove" onclick="removePhotograph()" /></td>
						
						--%>
						</tr>
						</table>
						</td>
			 		  	</tr>
						</table>
						</td>
						</tr>
						
						</table>
						</fieldset>
						</td>
						</tr>
						
						<tr>
						<td>
						<fieldset style="width:990px;">
						<legend>Contact Details</legend>	
						<table class="detailTabLabel" border="0">
			
			 			<tr class="listwhitetext">
			 		  			<td align="right"><fmt:message key="user.address.address"/></td>
			 		  			<td align="left"><s:textfield name="user.address.address" cssClass="input-text" cssStyle="width:270px;" maxlength="100" tabindex="21"/></td>
			 		  			
			 		  			<td align="right"><fmt:message key="user.email"/><font color="red" size="2">*</font></td>
			 		  			<td align="left"><s:textfield name="user.email" cssClass="input-text"  cssStyle="width:250px;" maxlength="60" tabindex="18" onchange="emailValidate('user','user.email','Email');"/></td>
			 		  			
			 		  			<td align="right" width="85"><fmt:message key="user.mobileNumber"/></td>
			 		  			<td align="left" valign="bottom"><s:textfield name="user.mobileNumber" cssClass="input-text" maxlength="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="16"/></td>
			 		  			
			 		  			
			 		  			
			 		  	</tr>
			 		  	
			 		  	<tr class="listwhitetext">
			 		  			<td align="right" width="90px"><fmt:message key="user.address.city"/><font color="red" size="2">*</font></td>
			 		  			
			 		  			<td>
			 		  			<table style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
			 		  			<tr>
			 		  			<td align="left"><s:textfield name="user.address.city" cssClass="input-text" maxlength="40" tabindex="22"/></td>
			 		  			<td align="right" width="40px"><fmt:message key="user.address.province"/>&nbsp;</td>
			 		  			<td align="left"><s:textfield name="user.address.province" cssClass="input-text" cssStyle="width:105px;"  maxlength="40" tabindex="23"/></td>
			 		  			<tr>
			 		  			</table>
			 		  			</td>
			 		  			<td align="right"><fmt:message key="user.website"/></td>
			 		  			<td align="left"><s:textfield name="user.website" cssClass="input-text" cssStyle="width:250px;" maxlength="40" tabindex="19"/></td>
			 		  			
			 		  			<td align="right"><fmt:message key="user.phoneNumber"/></td>
			 		  			<td align="left" valign="bottom"><s:textfield name="user.phoneNumber" cssClass="input-text" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="16"/></td>
			 		  	</tr>
			 		  	
			 		  	<tr class="listwhitetext">
			 		  	<td align="right" width=""><fmt:message key="user.address.country"/><font color="red" size="2">*</font></td>
			 		  	<td align="left"><s:select cssClass="list-menu" name="user.address.country" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:272px" tabindex="25"/></td>
			 		  	
			 		  	<td align="right" style="width:67px;"><fmt:message key="user.skypeID"/></td>
			 		  	<td align="left"><s:textfield name="user.skypeID" cssClass="input-text" cssStyle="width:250px;" maxlength="60" tabindex="18"/></td>	
			 		  	
			 		  	<td align="right"><fmt:message key="user.faxNumber"/></td>
			 		  			<td align="left"><s:textfield name="user.faxNumber" cssClass="input-text" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="17"/></td>	
			 		  	</tr>
			 		  	
			 		  	<tr class="listwhitetext">
			 		  	<td align="right" width=""><fmt:message key="user.address.postalCode"/></td>
			 		  	<td align="left"><s:textfield name="user.address.postalCode" cssClass="input-text" size="20" maxlength="10"  tabindex="24" cssStyle="width:268px;"/></td>
			 		  	</tr>
			 		  	
			 		  	<tr class="listwhitetext">
			 		  	<td align="left"></td>
			 		  	<td colspan="7">
			 		  	<table style="margin:0px 0px 7px 0px;paddding:0px;">
			 		  	<tr></tr>
						<tr>
				
						<c:if test="${user.location == null || user.location == ''}">
						<td align="center" class="listwhitetext" colspan="2"><img id="userImage" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="80" height="90"  style="border:thin solid #219DD1;"/></td>
			 		  	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph</td>
			 		  	</c:if>
						<c:if test="${!(user.location == null || user.location == '')}">
			 		  		<td align="right" class="listwhitetext" width="1px"><img id="userImage" src="UserImage?location=${user.location}" alt="" width="80" height="90"  style="border:thin solid #219DD1;"/></td>
			 		  		<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph</td>
			 		  	</c:if>
							<td align="left" valign="bottom" class="listwhitetext" ><s:file name="file" cssClass="text file" required="true" tabindex="26"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px;" onclick="removePhotograph();" tabindex="27"/></td>
							<%--<td align="right"><s:submit type="button" class="cssbutton" name="Remove" style="width:60px; height:20px" value="Remove" onclick="removePhotograph()" /></td>
						
						--%>
						</tr>						
						</table>
						</td>						
						</tr>
			 	
	 	  	</table></fieldset></td></tr>
	 	  	
	 	  	
	 	  	
	 	  	<tr><td align="left" class="listwhitetext">
	 	  	<table  class="detailTabLabel" border="0">
	 		  	<tbody>
						
						<tr>						
						<%-- <td align="right">Login Time: </td>
						<td colspan="5" >						
						<table style="margin:0px;paddding:0px;">
						<tr>
						
						<td style="width:150px"><b>
						<fmt:timeZone value="${sessionTimeZone}" >
						<fmt:formatDate value="${user.login_time}" pattern="${displayDateTimeFormat}"/>
						</fmt:timeZone>
						</b></td>											
						<td align="right" style="width:80px;">Login&nbsp;Status: </td>
		 		  		<td align="left" valign="bottom"><b><c:out value="${user.login_status}"/></b></td>
		 		  		</tr>
		 		  		</table>		 		  		
			 		</td> --%>
			 		
			 		 <c:if test="${not empty user.pricePointStartDate }"> 
			 		 <td width="50"></td>
           			<td class="listwhitetext" align="right" >Price&nbsp;Point&nbsp;Start&nbsp;Date:</td>      
					<td style="width:150px" colspan="2">
					<b><fmt:formatDate pattern="dd-MM-yyyy"   value="${user.pricePointStartDate}" /></b>
					</td>
		 			</c:if> 
			 		</tr>
			  	</tbody>
	 	  	</table>
    	</td></tr>
    	
    	
    	<tr>
    	
    <sec-auth:authComponent componentId="module.section.userForm.configuration">
   <c:choose> 
   <c:when test = "${(empty user.id  && empty cid )  || user.userType== 'USER' || adminAccess}">
    <tr>
	<td height="10" align="left" class="listwhitetext">
		<div class=""><a href="javascript:;"
	class="dsphead" onclick="javascript:toggleLayer('Layer5');"><span> </span>
  
<table border="0" cellpadding="0" cellspacing="0" style="margin:0px;width:100%;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Configuration&nbsp;Defaults
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
 </a></div>
  		<div class="dspcont" id="Layer5" style="display:block;">
		<table class="detailTabLabel" border="0">
 		  <tbody>
			<tr><td align="left" class="listwhitetext">
			
							<tr>
							<td colspan="15" style="padding-left:0px;">
							<fieldset style="width:990px;">
							<legend>Default</legend>	
							<table class="detailTabLabel" border="0" >	
							<tr class="listwhitetext">
			 		  			<td align="right"><fmt:message key="user.workStartTime"/></td>
			 		  			<td colspan="4">
			 		  			<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;">
			 		  			<tr>
			 		  			<td align="left"><s:textfield name="user.workStartTime" cssClass="input-text" size="3" maxlength="5"  onchange = "completeTimeString();" onkeydown="return onlyTimeFormatAllowed(event)" tabindex="27"/></td>
			 		  			<td align="right">&nbsp;&nbsp;&nbsp;End Hours&nbsp;</td>
			 		  			<td align="left" width="90"><s:textfield name="user.workEndTime" cssClass="input-text" size="3" maxlength="5" onchange = "completeTimeString();" onkeydown="return onlyTimeFormatAllowed(event)" tabindex="27"/><img src="${pageContext.request.contextPath}/images/time-HH.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
			 		  			<c:if test="${empty user.id}">
			 		  			<td align="right" class="listwhitetext">Active&nbsp;CF/SOs&nbsp;by&nbsp;default</td>
		                        <td align="left"><s:checkbox key="user.defaultSearchStatus" fieldValue="true" value="true" tabindex="27"/></td>
		                        </c:if>
		                        <c:if test="${not empty user.id}">
		                        <td align="right" class="listwhitetext">Active&nbsp;CF/SOs&nbsp;by&nbsp;default</td>
		                        <td align="left"><s:checkbox key="user.defaultSearchStatus" fieldValue="true" tabindex="27"/></td>
		                        </c:if>
		                        </tr>
		                        </table>
		                        </td>
		                        
		                        <td align="right" class="listwhitetext">Default&nbsp;Warehouse</td>
	        					<td align="left"><s:select cssClass="list-menu" cssStyle="width:115px" id="defaultWhse" name="user.defaultWarehouse" list="%{defaultWarehouse}" tabindex="27" headerKey="" headerValue="" /></td>
			 		  			</tr>	

			<tr class="listwhitetext"> 
			<td align="right" width="94"><fmt:message key="user.defaultURL"/></td>
			<td align="left" ><s:select cssClass="list-menu" name="user.defaultURL"  list="%{defURL}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			<td align="right">&nbsp;<fmt:message key="user.defaultS0URL"/></td>
			<td align="left" colspan="2"><s:select cssClass="list-menu" name="user.defaultSoURL"  list="%{defSoURL}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			
			<td align="right" class="listwhitetext" width="149" >Default&nbsp;Company&nbsp;Division</td>
	        <td align="left">
			<s:select name="user.companyDivision" list="%{companyDivis}" cssClass="list-menu" cssStyle="width:115px;margin-right:18px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</tr>
			
			<tr class="listwhitetext">
			<%-- 
			<td align="right"><fmt:message key="user.defaultCss"/></td>
			<td align="left" colspan=""><s:select cssClass="list-menu" name="user.defaultCss"  list="{'orange','blue','black'}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/>
				
			<img src="${pageContext.request.contextPath}/images/theme.png" align="top">
			</td> --%>
			
           
           <%-- 
           <td align="right"><fmt:message key="user.reminderDuration"/></td>
			 		  			<td align="left"><s:select cssClass="list-menu" name="user.reminderDuration"  list="%{RMDINTRVL}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			 		  			--%>
               
			</tr>
			
			
			<tr class="listwhitetext">
            <td colspan="10" align="left" >
            <div id="configDefaultInfoId">
            <table style="margin:0px;padding:0px;">
            <tr>
            <td width="98px" class="listwhitetext" align="right" >Extract&nbsp;Job&nbsp;Type</td>			
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:114px" id="extractJobType" name="user.extractJobType" tabindex="27" value="%{user.extractJobType}"  list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
	  		<td width="69"></td>
	  		<td align="right">Booking&nbsp;Agent</td>
	  		<td> <s:textfield name="user.bookingAgent" cssClass="input-text" cssStyle="width:113px;" maxlength="10" tabindex="27" onkeydown="onlyDel(event,this)" onchange="findParentAgentCode('BA')"/></td>
			<td width="17"><img align="left" class="openpopup" width="18" height="20" onclick="javascript:getBookingAgentCode()" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>		
			<td width="67"></td>
			<td align="right" style="width:57px;">File&nbsp;Cabinet</td>
			<td><s:select cssClass="list-menu" cssStyle="width:115px" id="fileCabinetView" name="user.fileCabinetView" value="%{user.fileCabinetView}"  list="{'','Document Centre','Document List'}" onchange="getChange(this);"/></td> 
			</tr>
			</table>
			</div>
			</td>
			</tr>	
			
			<tr>
			<td align="right" valign="" id="coordRequiredFalse" class="listwhitetext">Service&nbsp;Order&nbsp;View</td>
			<td><s:select cssClass="list-menu" cssStyle="width:114px" name="user.soListView" list="%{soDashboardViewList}" tabindex="27" headerKey="" headerValue="" /></td>			
			<c:if test="${company.cmmdmmAgent==true}">
			<td colspan="1" align="right" class="listwhitetext">Network&nbsp;Coordinator</td>	
	      	<td><%-- <s:select cssClass="list-menu" cssStyle="width:114px" name="user.networkCoordinator" list="%{coordinatorList}" tabindex="27" headerKey="" headerValue="" /> --%>
	      	<s:hidden name="user.networkCoordinator  "></s:hidden>
	      	 <s:checkbox key="user.networkCoordinatorStatus" cssStyle="margin:0px" id="user.networkCoordinatorStatus" fieldValue="true" theme="simple" />
	      	</td>
	       	</c:if>
	       	
	       	 <c:if test="${accessQuotationFromCustomerFileFlag && company.cmmdmmAgent==true}">
		     <td align="right" colspan="2" class="listwhitetext">Default&nbsp;Move&nbsp;Type</td>
		     <td>
		     <s:select name="user.moveType" list="%{moveTypeList}"  cssClass="list-menu" cssStyle="width:115px" headerKey="" headerValue=""/>
		    </td>	    
		    </c:if>
		    <c:if test="${accessQuotationFromCustomerFileFlag && company.cmmdmmAgent==false}">
		     <td align="right" colspan="1" class="listwhitetext">Default&nbsp;Move&nbsp;Type</td>
		     <td>
		     <s:select name="user.moveType" list="%{moveTypeList}"  cssClass="list-menu" cssStyle="width:115px" headerKey="" headerValue=""/>
		    </td>	    
		    </c:if>  
			</tr>
			
		<tr>
		<td align="right" class="listwhitetext">Commodity</td>
	    <td><s:select cssClass="list-menu" cssStyle="width:114px;margin-top:2px;" name="user.commodity" list="%{commodits}" /></td> 
			
		<td align="right" class="listwhitetext">Service</td>
	    <td><s:select cssClass="list-menu" name="user.serviceType" list="%{service}"   cssStyle="width:115px" /></td>
	    
	    <td align="right" colspan="2" class="listwhitetext">Default&nbsp;Operation&nbsp;Calendar</td>
	     <td>
	     <s:select name="user.defaultOperationCalendar" list="%{opscalendar}"  cssClass="list-menu" cssStyle="width:115px" headerKey="" headerValue=""/>
	    </td>
		</tr>	
		<tr>
		<c:set var="isfollowUpEmailAlert" value="false"/>
		<c:if test="${user.followUpEmailAlert}">
		<c:set var="isfollowUpEmailAlert" value="true"/>
		</c:if>												
		<td align="right" class="listwhitetext" colspan="1">Follow&nbsp;Up&nbsp;Email&nbsp;Alert</td>
		<td align="left"><s:checkbox key="user.followUpEmailAlert" value="${isfollowUpEmailAlert}" fieldValue="true" cssStyle="margin:0px;"/></td>	
		
		<td align="right" class="listwhitetext">SO Dashboard Search Limit</td>
	    <td><s:select cssClass="list-menu" name="user.recordsForSoDashboard" list="%{recordLimitList}" cssStyle="width:115px" /></td>
	    
		<configByCorp:fieldVisibility componentId="component.user.salesPortalAccess.showCWMS">
	  	<c:set var="isSalesPortalAccess" value="false"/>
		<c:if test="${user.salesPortalAccess}">
		<c:set var="isSalesPortalAccess" value="true"/>
		</c:if>
		<td colspan="3">
		<table style="margin:0px;width:172px;">
		<tr>
		<td align="right" class="listwhitetext">Armstrong&nbsp;Portal</td>
		<c:if test="${salesPortalAccess=='false'}">
		<td align="left"><s:checkbox key="user.salesPortalAccess" value="${isSalesPortalAccess}" onclick="" fieldValue="true" cssStyle="margin:0px;"/></td>
		</c:if>
		<c:if test="${salesPortalAccess=='true'}">
		<td align="left"><s:checkbox key="user.salesPortalAccess" value="${isSalesPortalAccess}" onclick="return readOnlyCheckBox();"  fieldValue="true" cssStyle="margin:0px;"/></td>
		</c:if>
		</tr>
		</table>
		</td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.execSaleAdditionalComm">
		<td align="right" class="listwhitetext" colspan="2">Additional&nbsp;Commission&nbsp;Type</td>
	     <td>
	     <s:select name="user.additionalCommissionType" list="{'','Exception 1%','Exception 10%'}"  cssClass="list-menu" cssStyle="width:115px" />
	    </td>
	    </configByCorp:fieldVisibility>
		</tr>
		<c:if test="${company.enablePricePoint=='true'}">	
		<tr>
		<td align="right" class="listwhitetext" colspan="1">Price&nbsp;Point&nbsp;Start&nbsp;Date</td>
		<td align="left">
		   	<c:if test="${not empty user.pricePointStartDate}">
		   		<c:choose>
					<c:when test="${rolesAssignFlag=='Y'}">
						<s:text id="pricePointStartDateFormattedValue" name="${FormDateValue}" ><s:param name="value" value="user.pricePointStartDate"/></s:text>
				 		<s:textfield cssClass="input-text" id="pricePointStartDate" name="user.pricePointStartDate" value="%{pricePointStartDateFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)" cssStyle="width:86px;" />
				 		<img id="pricePointStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
					 </c:when>
					 <c:otherwise>
						<s:text id="pricePointStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="user.pricePointStartDate"/></s:text>
				 		<s:textfield cssClass="input-text" id="pricePointStartDate" name="user.pricePointStartDate" value="%{pricePointStartDateFormattedValue}" required="true" size="8" maxlength="11"  readonly="true"/>		
					 </c:otherwise>
				</c:choose> 
			</c:if>	
			<c:if test="${empty user.pricePointStartDate}">
				<c:choose>
					<c:when test="${rolesAssignFlag=='Y'}">
						<s:textfield cssClass="input-text" id="pricePointStartDate" name="user.pricePointStartDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"  />
						<img id="pricePointStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
					 </c:when>
					 <c:otherwise>
						<s:textfield cssClass="input-text" id="pricePointStartDate" name="user.pricePointStartDate" required="true" size="8" maxlength="11"   readonly="true"/>		
					 </c:otherwise>
				</c:choose> 
			</c:if>
		</td>
		</tr>	
		</c:if>
		</table>
		</fieldset>
		</td>
		</tr>			
			
			<tr>
		<td colspan="15" style="padding-left:0;">
		<fieldset style="width:990px;padding-top:0px;">
		<legend>Default Sort For</legend>	
		<table class="detailTabLabel" border="0" >
			<tr class="listwhitetext">
			<td colspan="3"></td>
			<td align="left"><fmt:message key="user.sortOrderForCso"/></td>
			</tr>			
            <tr class="listwhitetext">
            <td align="right" width="">Quotation/Customer file</td>
			<td align="left" ><s:select cssClass="list-menu" name="user.defaultSortForCso"  list="%{defaultSortForCso}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			<td align="right" width="60"></td>
			<td align="left" colspan="2"><s:select cssClass="list-menu" name="user.sortOrderForCso"  list="%{sortOrderForCso}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			
			</tr>
			<tr class="listwhitetext">
			<td align="right">Service order</td>
			<td align="left" ><s:select cssClass="list-menu" name="user.defaultSortForJob"  list="%{defaultSortForJob}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			<td align="right"></td>
			<td align="left" colspan="2"><s:select cssClass="list-menu" name="user.sortOrderForJob"  list="%{sortOrderForJob}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			
		</tr>
		
		<tr class="listwhitetext">
			<td align="right">Ticket</td>
			<td align="left"><s:select cssClass="list-menu" name="user.defaultSortForTicket"  list="%{defaultSortForTicket}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			<td align="right"></td>
			<td align="left" colspan="2"><s:select cssClass="list-menu" name="user.sortOrderTicket"  list="%{sortOrderForTicket}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
			</tr>
			<configByCorp:fieldVisibility componentId="component.standard.claimTab">
			<tr class="listwhitetext">
			<td align="right">Claim</td>
			<td align="left"><s:select cssClass="list-menu" name="user.defaultSortForClaim"  list="%{defaultSortForClaim}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>
	        <td align="right"></td>
			<td align="left" colspan="2"><s:select cssClass="list-menu" name="user.sortOrderClaim"  list="%{sortOrderForClaim}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px;" tabindex="27"/></td>			
			</tr>
			</configByCorp:fieldVisibility>
		 <c:if test="${user.userType == 'USER' || user.userType == 'DRIVER' || user.corpID != 'VOER'}">
		 
		 <div id="configDefaultInfoId1">
		 <tr class="listwhitetext">	
		 <td align="right" class="listwhitetext" width="">File&nbsp;Cabinet</td>
		 <td id="reasonHid1" align="left"><s:select name="user.defaultSortforFileCabinet" cssClass="list-menu" multiple="true" id="select-multiple"  list="%{defaultSortforFileCabinet}" value="%{multiSortforFileCabinet}"    headerKey="" headerValue="" onclick="changeStatus();testScript(this)" cssStyle="height: 60px; width: 115px;"/></td>
		  
		 <!--<td id="reasonHid6" align="right" class="listwhitetext" width="">Default&nbsp;Sort&nbsp;For&nbsp;File&nbsp;Cabinet</td>-->
		 <td id="reasonHid2"align="left"><s:select cssClass="list-menu" id="select-single"  list="%{defaultSortforFileCabinet}"  headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width: 115px;"/></td>
		
		 <td align="right"></td>
		 <td  align="left" colspan="2" valign="top"><s:select cssClass="list-menu" name="user.sortOrderForFileCabinet" list="%{sortOrderForFileCabinet}" headerKey="" headerValue="" onclick="changeStatus()" cssStyle="width:115px"/></td> 
		</tr>			
		<tr>
		<td align="right"></td>
		<td id="reasonHid4" class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
		  	<b>* Drag and Drop the value A/C to Sorting</b>
   			<br><b>* Use Control + mouse to select multiple Value</b>
 		  </td>	
		</tr>
		<!--</div>
		--></c:if>
		 <!--<c:if test="${user.userType != 'USER' || user.userType != 'DRIVER'}">
	 	  	 <s:hidden name = "user.defaultSortforFileCabinet" />
	 	  	 <s:hidden name = "user.sortOrderForFileCabinet" />
			 </c:if>
   	    --></table>
   	    </fieldset>
   	    </td>
   	    </tr> 
   	     <tr>
		<td colspan="15" style="padding-left:0;">
		<fieldset style="width:990px;">
		<legend>Accounting</legend>	
		<table class="detailTabLabel" border="0">
	     <tr>
	     <td align="right" class="listwhitetext">Quote Contract</td>
	  		<td><s:textfield name="user.contract" cssClass="input-text" size="67" maxlength="30" onkeydown="return onlyDel(event,this)" readonly="true" tabindex="27" cssStyle="width:404px;"/>
	  		<img style="vertical-align:top;" class="openpopup" width="17" height="20" onclick="javascript:getContract()" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
	    
	     
	     </tr>
	     <tr>
	     <td align="right" class="listwhitetext">Accounting List:</td>
	     <td colspan="3">
	     <fieldset style="margin: 0px; padding: 0px; width: 428px;">
	     <table class="detailTabLabel" border="0">
	     <tr>
	     <td align="right" class="listwhitetext">Display the following :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Entitlement</td>
	     <td align="left"><s:checkbox key="user.acctDisplayEntitled" fieldValue="true" /></td>
	     <td width="10px" ></td>
	     <td align="right" class="listwhitetext">Estimate</td>
	     <td align="left"><s:checkbox key="user.acctDisplayEstimate" fieldValue="true"  /></td>
	     <td width="10px" ></td>
	     <td align="right" class="listwhitetext">Revision</td>
	     <td align="left"><s:checkbox key="user.acctDisplayRevision" fieldValue="true"  /></td>
	     </tr>
	     </table>
	     </fieldset>
	     </td>	     
	     </tr>	
	     
	     <tr>
	     <td align="right" class="listwhitetext">Pricing / Accounting</td>
	     <td colspan="7">
	     <fieldset style="margin: 0px; padding: 0px; width: 428px;">
	     <table class="detailTabLabel" border="0">
	     <tr>
	     <td align="right" class="listwhitetext">Display the following :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revision</td>
	     <td align="left"><s:checkbox key="user.pricingRevision" fieldValue="true"  /></td>
	     <td width="10px" ></td>
	     <td align="right" class="listwhitetext">Actual</td>
	     <td align="left"><s:checkbox key="user.pricingActual" fieldValue="true"  /></td>
	     <td width="10px" ></td>	     
	     </tr>
	     </table>
	     </fieldset>
	     </td>
	     </tr>
	     </table>
	     </fieldset>
	     </td>
	     </tr>
	     
	     
	      </tbody>
 		</table></div></td>
 		 </tr>
 		 </c:when>
 		 <c:otherwise>
 		 <s:hidden name="user.defaultSearchStatus" value="true"/>
 		 </c:otherwise>
 		 </c:choose>
    	 
	      <c:if test="${company.cmmdmmAgent!=true}">
	      <s:hidden name="user.networkCoordinator"/>
	      </c:if>
	      
    	<tr>
	<td height="10" align="left" class="listwhitetext">
		<div class=""><a href="javascript:;"
	class="dsphead" onclick="javascript:toggleLayer('Layer6');">
  
<table border="0" cellpadding="0" cellspacing="0" style="margin:0px;width:100%;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Access Right
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</a></div>
  		<div class="dspcont" id="Layer6" style="display:block;">
		<table class="detailTabLabel" border="0" width="990px">
 		  <tbody>
			<tr><td align="left" class="listwhitetext" style="padding-left:0;">
			
			<c:choose>
			    <c:when test="${param.from == 'list' or param.method == 'Add'}">			    
			        <fieldset style="width:990px;">
			            <legend><fmt:message key="userProfile.accountSettings"/></legend>
			            <table border="0"  class="detailTabLabel"><tr><td>
			            <c:if test="${empty user.id}">
			             <s:checkbox key="user.enabled" id="user.enabled" fieldValue="true" value="true" onclick="changeStatus()" theme="simple" tabindex="28"/>
			            <label for="user.enabled" class="choice"><fmt:message key="user.enabled"/></label>
						</c:if>
			            <c:if test="${not empty user.id}">
			            <s:checkbox key="user.enabled" id="user.enabled" fieldValue="true" onclick="changeStatus()" theme="simple" tabindex="28"/>
			            <label for="user.enabled" class="choice"><fmt:message key="user.enabled"/></label>
						</c:if>
			            <s:checkbox key="user.accountExpired" id="user.accountExpired" fieldValue="true" onclick="changeStatus()"  theme="simple" tabindex="29"/>
			            <label for="user.accountExpired" class="choice"><fmt:message key="user.accountExpired"/></label>
			
			            <s:checkbox key="user.accountLocked" id="user.accountLocked" fieldValue="true" onclick="changeStatus()" theme="simple" tabindex="30"/>
			            <label for="user.accountLocked" class="choice"><fmt:message key="user.accountLocked"/></label>
			
			            <s:checkbox key="user.credentialsExpired" id="user.credentialsExpired" fieldValue="true" onclick="changeStatus()" theme="simple" tabindex="31"/>
			            <label for="user.credentialsExpired" class="choice"><fmt:message key="user.credentialsExpired"/></label>
			        	</td></tr></table>
			        </fieldset>
			    
			  <c:if test="${adminAccess}">
			        <fieldset>
			            <legend><fmt:message key="userProfile.assignRoles"/></legend>
			            <table class="pickList11">
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required"><fmt:message key="user.availableRoles"/></label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required"><fmt:message key="user.roles"/></label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${availRoles}" scope="request"/>
			                <s:set name="rightList" value="user.roleList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availRoles && !userRoles"/>
			                    <c:param name="rightId" value="userRoles"/>
			                </c:import>
			            </table>
			        </fieldset>
			    <fieldset>
			            <legend>Permissions</legend>
			            <table class="pickList11" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Available Set</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Current Set</label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${availPermissions}" scope="request"/>
			                <s:set name="rightList" value="user.permissionList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availPermissions && !permissions"/>
			                    <c:param name="rightId" value="permissions"/>
			                </c:import>
			            </table>
			        </fieldset>
			        </c:if>
			         <c:if test="${!adminAccess}">	
			        <fieldset style="width: 990px;" class="listwhitetext">           
			        <strong><fmt:message key="user.roles"/>:</strong>
			        <s:iterator value="user.roleList" status="status">
			          <s:property value="label"/><s:if test="!#status.last">,</s:if>
			          <input type="hidden" name="userRoles" value="<s:property value="value"/>"/>
			          </s:iterator>
			          <br><strong><label class="required">Current Set</label>:</strong>
			        <s:iterator value="user.permissionList" status="status1">
			          <s:property value="label"/><s:if test="!#status1.last">,</s:if>
			          <input type="hidden" name="permissions" value="<s:property value="value"/>"/>
			        </s:iterator>
			        <s:hidden name="user.enabled" value="%{user.enabled}"/>
			        <s:hidden name="user.accountExpired" value="%{user.accountExpired}"/>
			        <s:hidden name="user.accountLocked" value="%{user.accountLocked}"/>
			        <s:hidden name="user.credentialsExpired" value="%{user.credentialsExpired}"/>
			        <s:hidden name="user.cityHomeCode" value="%{user.cityHomeCode}"/>
			        <s:hidden name="user.countryHomeCode" value="%{user.countryHomeCode}"/>
			     <s:hidden name="userAccountSection" value="account"/>
			     </fieldset>
			          </c:if>
			    </c:when>
			    
			    <c:otherwise>
			   		<fieldset style="width: 990px;" class="listwhitetext">           
			        <strong><fmt:message key="user.roles"/>:</strong>
			        <s:iterator value="user.roleList" status="status">
			          <s:property value="label"/><s:if test="!#status.last">,</s:if>
			          <input type="hidden" name="userRoles" value="<s:property value="value"/>"/>
			          </s:iterator>
			          <br><strong><label class="required">Current Set</label>:</strong>
			        <s:iterator value="user.permissionList" status="status1">
			          <s:property value="label"/><s:if test="!#status1.last">,</s:if>
			          <input type="hidden" name="permissions" value="<s:property value="value"/>"/>
			        </s:iterator>
			        <s:hidden name="user.enabled" value="%{user.enabled}"/>
			        <s:hidden name="user.accountExpired" value="%{user.accountExpired}"/>
			        <s:hidden name="user.accountLocked" value="%{user.accountLocked}"/>
			        <s:hidden name="user.credentialsExpired" value="%{user.credentialsExpired}"/>
			        <s:hidden name="user.cityHomeCode" value="%{user.cityHomeCode}"/>
			        <s:hidden name="user.countryHomeCode" value="%{user.countryHomeCode}"/>
			     <s:hidden name="userAccountSection" value="account"/>
			     </fieldset>
			    </c:otherwise>
			</c:choose>
			<c:if test="${user.userType == 'USER' || user.userType == 'ACCOUNT' || user.userType == 'AGENT'}">
			       <fieldset >
			            <legend>Job Functions</legend>
			            <table class="pickList111" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Function List</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Selected Functions</label>
			                    </th>
			                </tr>
			          
			                <c:set var="leftList" value="${JobFunction}" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList12.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="JobFunction && !userJobType"/>
			                    <c:param name="rightId" value="userJobType"/>
			                </c:import>
			            </table>
			        </fieldset>
			 </c:if>

			</td></tr>
	      </tbody>
 		</table></div></td>
 		  </tr> 

    	
    	
    	
<tr>
	<td height="10" align="left" class="listwhitetext">
	<div  onclick="javascript:toggleLayer('Layer4');"><a href="javascript:;"class="dsphead">
 
<table border="0" cellpadding="0" cellspacing="0" style="margin:0px;width:100%;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Home Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</a></div>
  		<div class="dspcont" id="Layer4">
  		
  		<fieldset class="listwhitetext" style="margin: 5px 20px; width: 900px;">    
		<table class="detailTabLabel" border="0">
 		  <tbody>
			<tr>
			<td align="left" class="listwhitetext">
			
	 		  			<tr>
			 		  			<td align="right"><fmt:message key="user.address.address"/></td>
			 		  			<td align="left" colspan="5"><s:textfield name="user.addressHome" cssClass="input-text" cssStyle="width:378px;" maxlength="100" tabindex="36"/></td>
			 		  	</tr>
						
	 		  			
			
						<tr>
			 		  			<td align="right" width=""><fmt:message key="user.address.city"/></td>
			 		  			<td align="left"><s:textfield name="user.cityHome" cssClass="input-text" cssStyle="width:185px;" maxlength="40" tabindex="37"/></td>
			 		  			<td align="right"><fmt:message key="user.address.postalCode"/></td>
			 		  			<td align="left"><s:textfield name="user.postalCodeHome" cssClass="input-text" cssStyle="width:120px;" maxlength="40"  tabindex="38"/></td>
			 		  			
			 		  	</tr>
			 		  	<tr>
			 		  			<td align="right" width=""><fmt:message key="user.address.country"/></td>
			 		  			<td align="left"><s:select cssClass="list-menu" name="user.countryHome" list="%{ocountry}" cssStyle="width:190px" headerKey="" headerValue="" onchange="autoPopulate_user_countryHome(this);getState(this)" onclick="changeStatus()" tabindex="39"/></td>
			 		  			<td align="right" ><fmt:message key="user.address.province"/></td>
			 		  			<td align="left"><s:select cssClass="list-menu" name="user.provinceHome" list="%{ostates}" cssStyle="width:125px" headerKey="" headerValue="" onchange="autoPopulate_user_cityHomeCode" onclick="changeStatus()" tabindex="40"/></td>
			 		  			
			 		  	</tr>
			 		  	<tr>
								<td align="right"><fmt:message key="user.phoneCell"/></td>
			 		  			<td align="left"><s:textfield name="user.phoneCell" cssClass="input-text" cssStyle="width:186px;" maxlength="40" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="41"/></td>
			 		  			<td align="right"><fmt:message key="user.phoneHome"/></td>
			 		  			<td align="left" colspan="3"><s:textfield name="user.phoneHome" cssClass="input-text" cssStyle="width:120px;" maxlength="40" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="42"/></td>
			 		  	</tr>
			 		  	
			
			</td>
			</tr>
	      </tbody>
 		</table>
 		</fieldset>
 		</div>
 		</td>
 		 </tr> 

    
   </sec-auth:authComponent>
   
    <sec-auth:authComponent componentId="module.section.userForm.configurationAgent">
    
    				<s:iterator value="user.roleList" status="status">
			           <input type="hidden" name="userRoles" value="<s:property value="value"/>"/>
			        </s:iterator>
			        <!--<s:iterator value="user.permissionList" status="status">
			           <input type="hidden" name="permissions" value="<s:property value="value"/>"/>
			        </s:iterator>
    
    --></sec-auth:authComponent>
			
		
	      </tbody>
 		</table></div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
  <table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.createdOn'/></b></td>
							<s:hidden name="user.createdOn" />
							<td style="width:140px"><fmt:formatDate value="${user.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.createdBy' /></b></td>
							<c:if test="${not empty user.id}">
								<s:hidden name="user.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{user.createdBy}"/></td>
							</c:if>
							<c:if test="${empty user.id}">
								<s:hidden name="user.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.updatedOn'/></b></td>
							<s:hidden name="user.updatedOn"/>
							<td style="width:140px"><fmt:formatDate value="${user.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.updatedBy' /></b></td>
						 	<c:if test="${not empty user.id}">
								<s:hidden name="user.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{user.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty user.id}">
								<s:hidden name="user.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
						</tr>
					</tbody>
				</table>

 		  <c:out value="${buttons}" escapeXml="false"/>

    </div>
    
<td align="left"><s:hidden name="user.location" cssClass="input-text" /></td>
			 <s:hidden name="user.digitalSignature" cssClass="input-text" />
</s:form>

<script type="text/javascript">
function readOnlyCheckBox() {
	   return false;
	}
function checkRole(target)
{
	var remoteUser='${pageContext.request.remoteUser}';
	var currentUser=document.forms['userForm'].elements['user.username'].value;
	if(remoteUser==currentUser)
	{
		if(target.value=='ROLE_ADMIN'){
			alert('Since you are admin, you can not remove your admin role');
			document.forms['userForm'].reset();
		}
	}

}

function checkUserType(target){
	var userType= document.forms['userForm'].elements['user.userType'].value;
	if(target.value=='ROLE_ADMIN' && userType !='USER'){
				alert('You can not assign your admin role as the user type is not user');
				//selIdx = document.forms['userForm'].elements['availRoles && !userRoles'].selectedIndex;
			   document.forms['userForm'].elements['availRoles && !userRoles'].options.selectedIndex=0;
			}

}
    function passwordChanged(passwordField) {
        if (passwordField.name == "user.password") {
            var origPassword = "<s:property value="user.password"/>";
            document.forms['userForm'].elements['chngPwsd'].value='1';
        } else if (passwordField.name == "user.confirmPassword") {
            var origPassword = "<s:property value="user.confirmPassword"/>";
            document.forms['userForm'].elements['chngPwsd'].value='1';
        }
        
        if (passwordField.value != origPassword) {
            createFormElement("input", "hidden",  "encryptPass", "encryptPass",
                              "true", passwordField.form);
        }
    }

function onFormSubmit(theForm) {
<c:if test="${param.from == 'list'}">
    selectAll('userRoles');
    selectAll('permissions');
</c:if>
<c:if test="${user.userType == 'USER' || user.userType == 'ACCOUNT' || user.userType == 'AGENT'}">
selectAll('userJobType');
</c:if>
	var userType=document.forms['userForm'].elements['user.userType'].value;
	var userId=document.forms['userForm'].elements['user.id'].value;
	var Que=document.forms['userForm'].elements['user.passwordHintQues'].value;
	try{
	var parentAgent=document.forms['userForm'].elements['user.parentAgent'].value;
	}catch(e){}
	var gender=document.forms['userForm'].elements['user.gender'].value;
	gender=gender.trim();
	var userTitle=document.forms['userForm'].elements['user.userTitle'].value;
	userTitle=userTitle.trim();
	if(document.forms['userForm'].elements['user.firstName'].value=='')
  		{
  			alert("Please enter the First Name");
  			document.forms['userForm'].elements['user.firstName'].focus();
  			return false;
  		}
    if(userTitle=='' && (userType=='ACCOUNT' || userType=='AGENT'))        
	{
		alert("Please enter the Title.");
		document.forms['userForm'].elements['user.userTitle'].focus();
		return false;
	}	
  		if(document.forms['userForm'].elements['user.lastName'].value=='')
  		{
  			alert("Please enter the Last Name");
  			document.forms['userForm'].elements['user.lastName'].focus();
  			return false;
  		}
  		if(document.forms['userForm'].elements['user.username'].value=='')
  		{
  			alert("Please enter the User Name");
  			document.forms['userForm'].elements['user.username'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.password'].value=='')
  		{
  			alert("Please enter the Password");
  			document.forms['userForm'].elements['user.password'].focus();
  			return false;
  		}
  		var chkvalflagReq="0";
  		var sysPaswdPolicy='${systemDefault.passwordPolicy}';
  		var userType= document.forms['userForm'].elements['user.userType'].value;
  		var ispsdChng=document.forms['userForm'].elements['chngPwsd'].value;
  		//alert(sysPaswdPolicy);
  		if((sysPaswdPolicy!=null && sysPaswdPolicy=='true') && userType!=null && userType=='USER' && (ispsdChng!=null && ispsdChng=='1')){
  			chkvalflagReq="1";
  		}  		
  		var pwd= document.forms['userForm'].elements['user.password'].value;
  		//alert(pwd);
  	//var regx=/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/;
  	var regx=/^(?=.*[A-Z])(?=.*[+/*-<>/?!@#$()]).{6,}$/;
  	var msgval="The entered password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
  		if((chkvalflagReq=='1') && pwd!=null && pwd!='' && !(pwd.match(regx)))
  		{
  			alert(msgval);
  			document.forms['userForm'].elements['user.password'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.confirmPassword'].value=='')
  		{
  			alert("Please enter the same password in the Confirm Password  field as in the Password field");	
  					
  			document.forms['userForm'].elements['user.confirmPassword'].focus();
  			return false;
  		}
  		var pwd1= document.forms['userForm'].elements['user.confirmPassword'].value;  	  	
  	  	var msgval="The entered confirm password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
  	  		if((chkvalflagReq=='1')&& pwd1!=null && pwd1!='' && !(pwd1.match(regx)))
  	  		{
  	  			alert(msgval);
  	  			document.forms['userForm'].elements['user.confirmPassword'].focus();
  	  			return false;
  	  		}
  		
  		if(document.forms['userForm'].elements['user.password'].value!=document.forms['userForm'].elements['user.confirmPassword'].value)
  		{
  		
  			alert("Please enter the same password in the Confirm Password  field as in the Password field");
  					
  			document.forms['userForm'].elements['user.confirmPassword'].value='';
  			document.forms['userForm'].elements['user.confirmPassword'].focus();
  			return false;
  		
  		}
  		if(document.forms['userForm'].elements['user.passwordHintQues'].value==' DEFAULT')
  		{ 
  	  	
  			alert("Please enter the Password Hint Question");

  			document.forms['userForm'].elements['user.passwordHint'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.passwordHintQues'].value!=' DEFAULT')
  		{  
  			if(document.forms['userForm'].elements['user.passwordHint'].value=='')
	  		{
	  			alert("Please enter the Password Hint Answer");
	  			document.forms['userForm'].elements['user.passwordHint'].focus();
	  			return false;
	  		}
  			
  		}
  		
  		
  		if(document.forms['userForm'].elements['user.email'].value=='')
  		{
  			alert("Please enter the E-mail");
  			document.forms['userForm'].elements['user.email'].focus();
  			return false;
  		}
  		
  		
  		
  		if(document.forms['userForm'].elements['user.address.city'].value=='')
  		{
  			alert("Please enter the City");
  			document.forms['userForm'].elements['user.address.city'].focus();
  			return false;
  		}
  		
  		
  		
  		if(document.forms['userForm'].elements['user.address.country'].value=='')
  		{
  			alert("Please enter the Country");
  			document.forms['userForm'].elements['user.address.country'].focus();
  			return false;
  		}
  		
  		//if(document.forms['userForm'].elements['user.pwdexpiryDate'].value=='')
  		//{
  		//	alert("Please enter the password expiry date");
  		//	document.forms['userForm'].elements['user.pwdexpiryDate'].focus();
  		//	return false;
  		//}
	if((userType=='ACCOUNT' || userType=='AGENT') && parentAgent=='')
  		{
  			alert("For Account or Agent Portal, please fill Parent Agent.");
  			document.forms['userForm'].elements['user.parentAgent'].focus();
  			return false;
  		}
  	 var per = document.getElementById('permissions');
	 var check=false;
	 var agent=false;
     for(var i = 0; i < per.length; i++)
      {
    	 if(per[i].value.indexOf('DATA_SECURITY_SET_SO_BILLTOCODE_')>-1 && userType=='ACCOUNT')
    		{
    		check=true;
    		 break;
    		 }
        
    	 if(per[i].value.indexOf('DATA_SECURITY_SET_AGENT_')> -1 && userType=='AGENT')
    		 {
    		agent=true;
    		 break;
    		 }
        }
     if(!check && userType=='ACCOUNT'){
    	 alert("Please assign  Permissions set for account portal.");
    	 return false;
     } 
     if(!agent && userType=='AGENT'){
    	 
    	 alert("Please assign  Permissions set for Agent portal.");
    	 return false;
     }
  	if(document.forms['userForm'].elements['user.fileCabinetView'].value != 'Document Centre'){
		document.forms['userForm'].elements['user.defaultSortforFileCabinet'].value = document.getElementById('select-single').value;
	}
	return IsValidTime();
	
}

	function completeTimeString() {
	
		stime1 = document.forms['userForm'].elements['user.workStartTime'].value;
		stime2 = document.forms['userForm'].elements['user.workEndTime'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['userForm'].elements['user.workStartTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['userForm'].elements['user.workStartTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['userForm'].elements['user.workStartTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['userForm'].elements['user.workStartTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['userForm'].elements['user.workStartTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['userForm'].elements['user.workStartTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['userForm'].elements['user.workEndTime'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['userForm'].elements['user.workEndTime'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['userForm'].elements['user.workEndTime'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['userForm'].elements['user.workEndTime'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['userForm'].elements['user.workEndTime'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['userForm'].elements['user.workEndTime'].value = "0" + stime2;
			}
		}
	}

function validateRole()
{
	 var ListBoxOptions = document.getElementsByName('userRoles')[0].options;
	 var ListPermissionOptions = document.getElementsByName('permissions')[0].options;
	 var userType = document.forms['userForm'].elements['user.userType'].value;
        var total = '0';
         for(var i = 0; i < ListBoxOptions.length; i++) {
                if(ListBoxOptions[i].value == 'ROLE_USER' || ListBoxOptions[i].value == 'ROLE_ADMIN' || ListBoxOptions[i].value == 'ROLE_CUSTOMER')
                {
                	total = total+1;
                }
        }
        if(total == 0)
        {
        	alert("Please select ROLE_USER,ROLE_ADMIN or ROLE_CUSTOMER from Access Right ");
        	return false;
        }
        var setTotal='0';
        if(userType=='AGENT' || userType=='PARTNER'){
            for(var i = 0; i < ListPermissionOptions.length; i++) {           
              
                if((ListPermissionOptions[i].value.indexOf('DATA_SECURITY_SET_AGENT_')> -1)  || (ListPermissionOptions[i].value.indexOf('DATA_SECURITY_SET_SO_')> -1))
                {
                	setTotal = setTotal+1;
                }
        }
            if(setTotal == 0)
            {
            	alert("Please select Any Set For "+userType+" Portal");
            	return false;
            }
        }
        return true;
}
function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['userForm'].elements['user.workStartTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['userForm'].elements['user.workStartTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Begin Hours' time must between 0 to 23 (Hrs)");
document.forms['userForm'].elements['user.workStartTime'].select();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Begin Hours' time must between 0 to 59 (Min)");
document.forms['userForm'].elements['user.workStartTime'].select();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Begin Hours' time must between 0 to 59 (Sec)");
document.forms['userForm'].elements['user.workStartTime'].select();
return false;
}

var time2Str = document.forms['userForm'].elements['user.workEndTime'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['userForm'].elements['user.workEndTime'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'End Hours' time must between 0 to 23 (Hrs)");
document.forms['userForm'].elements['user.workEndTime'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'End Hours' time must between 0 to 59 (Min)");
document.forms['userForm'].elements['user.workEndTime'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'End Hours' time must between 0 to 59 (Sec)");
document.forms['userForm'].elements['user.workEndTime'].focus();
return false;
}
 return checkJobValids('save');
//return checkTime();
}

function checkTime()
{
	var tim1=document.forms['userForm'].elements['user.workStartTime'].value;
	var tim2=document.forms['userForm'].elements['user.workEndTime'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("Begin hours cannot be less than end hours");
		document.forms['userForm'].elements['user.workStartTime'].select();
		return false;
	}else{
		return fillAction();
	}
}


	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==190) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	function checkEmail(elementValue){  
		 
	 } 	
function validateEmail(elementValue){
	   var emailPattern =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;
	 var fieldName = elementValue.name;   
	   if(emailPattern.test(elementValue.value)==false){
		   alert("Please enter valid email");
		   elementValue.value="";
		   document.forms['userForm'].elements[fieldName].focus();
		   return false;
	   } 
}
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==107) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}

	function autoPopulate_user_countryHome(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['userForm'].elements['user.provinceHome'].disabled = false;
			getState(targetElement);
		}else{
			document.forms['userForm'].elements['user.provinceHome'].disabled = true;
			document.forms['userForm'].elements['user.provinceHome'].value = '';
		}
		autoPopulate_user_cityHomeCode(targetElement);
		}

	
		function autoPopulate_user_cityHomeCode(targetElement) {
		if(document.forms['userForm'].elements['user.cityHome'].value != ''){
			if(document.forms['userForm'].elements['user.provinceHome'].value != ''){
				document.forms['userForm'].elements['user.cityHomeCode'].value=document.forms['userForm'].elements['user.cityHome'].value+','+document.forms['userForm'].elements['user.provinceHome'].value;
			}else{
				document.forms['userForm'].elements['user.cityHomeCode'].value=document.forms['userForm'].elements['user.cityHome'].value;
			}
		}
	}
	
	function getState(targetElement) {
	var countryHomeCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryHomeCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse8;
     http3.send(null);
	
	}
	
	function handleHttpResponse8()
        {
             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['userForm'].elements['user.provinceHome'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['userForm'].elements['user.provinceHome'].options[i].text = '';
					document.forms['userForm'].elements['user.provinceHome'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['userForm'].elements['user.provinceHome'].options[i].text = stateVal[1];
					document.forms['userForm'].elements['user.provinceHome'].options[i].value = stateVal[0];
					}
					}
             }
        }         
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject();
	

function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
}
	
 function removePhotograph(){
	document.forms['userForm'].elements['user.location'].value = '';
	document.getElementById("userImage").src = "${pageContext.request.contextPath}/images/no-image.png";
	//document.getElementById("userImage").src = "UserImage?location='${pageContext.request.contextPath}/userData/images/nopicture.jpg'";
	}
 function removePhotograph1(){
		document.forms['userForm'].elements['user.digitalSignature'].value = '';
		document.getElementById("userSignature").src = "${pageContext.request.contextPath}/images/no-signature.png";
		//document.getElementById("userImage").src = "UserImage?location='${pageContext.request.contextPath}/userData/images/nopicture.jpg'";
		}
function userauth()
{
 alert("You do not have right to Save");
}


function checkJobValids(target){
	var job=document.forms['userForm'].elements['user.jobType'].value;
	job=job.trim();
	if(job!=''){
	var jobLength = job.length;
	var reminder = ((jobLength-1)%6);
	var startPoint = job.substring(0,2)
	var endPoint = job.substring(jobLength-2,jobLength)
	if(startPoint!="('")
	{	
		
		alert("Incorrect Job Type");
		document.forms['userForm'].elements['user.jobType'].select();
		return false;
	}
	else if(endPoint!="')")
	{
		
		alert("Incorrect Job Type");
		document.forms['userForm'].elements['user.jobType'].select();
		return false;
	}
	else if(reminder > 0)
	{
		
		alert("Incorrect Job Type");
		document.forms['userForm'].elements['user.jobType'].select();
		return false;
	}
	}
	var userType = document.forms['userForm'].elements['user.userType'].value;
	if(userType==''){
		alert("Please select the User Type.");
		document.forms['userForm'].elements['user.userType'].focus();
		return false;
	}

	return true;
	}

function autoSaveFunc(clickType)
{
    var userType=document.forms['userForm'].elements['user.userType'].value;
	var userId=document.forms['userForm'].elements['user.id'].value;
	try{
	var parentAgent=document.forms['userForm'].elements['user.parentAgent'].value;
	}catch(e){}
	var gender=document.forms['userForm'].elements['user.gender'].value;
	gender=gender.trim();
	var userTitle=document.forms['userForm'].elements['user.userTitle'].value;
	userTitle=userTitle.trim();
    if(document.forms['userForm'].elements['user.firstName'].value=='')
  		{
  			alert("Please enter the first Name");
  			document.forms['userForm'].elements['user.firstName'].focus();
  			return false;
  		}

    if(userTitle=='' && (userType=='ACCOUNT' || userType=='AGENT'))
	{
    	alert("Please enter the Title.");
		document.forms['userForm'].elements['user.userTitle'].focus();
		return false;
	}
	
  		if(document.forms['userForm'].elements['user.lastName'].value=='')
  		{
  			alert("Please enter the last Name");
  			document.forms['userForm'].elements['user.lastName'].focus();
  			return false;
  		}
  		if(document.forms['userForm'].elements['user.username'].value=='')
  		{
  			alert("Please enter the userName");
  			document.forms['userForm'].elements['user.username'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.password'].value=='')
  		{
  			alert("Please enter the password");
  			document.forms['userForm'].elements['user.password'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.confirmPassword'].value=='')
  		{
  			alert("Please enter the same password in the Confirm Password  field as in the Password field");	
  					
  			document.forms['userForm'].elements['user.confirmPassword'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.password'].value!=document.forms['userForm'].elements['user.confirmPassword'].value)
  		{
  		
  			alert("Please enter the same password in the Confirm\nPassword  field as in the Password field");
  			document.forms['userForm'].elements['user.confirmPassword'].value='';
  			document.forms['userForm'].elements['user.confirmPassword'].focus();
  			return false;
  		
  		}
  		
  		if(document.forms['userForm'].elements['user.passwordHintQues'].value!=' DEFAULT')
  		{
  			if(document.forms['userForm'].elements['user.passwordHint'].value=='')
	  		{
	  			alert("Please enter the password hint ans");
	  			document.forms['userForm'].elements['user.passwordHint'].focus();
	  			return false;
	  		}
  			
  		}
  		
  		if(document.forms['userForm'].elements['user.passwordHint'].value=='')
  		{
  			alert("Please enter the password hint ans");
  			document.forms['userForm'].elements['user.passwordHint'].focus();
  			return false;
  		}
  		if(userType==''){
  			alert("Please select the User Type.");
  			document.forms['userForm'].elements['user.userType'].focus();
  			return false;
  		}
  		
  		if(document.forms['userForm'].elements['user.email'].value=='')
  		{
  			alert("Please enter the email");
  			document.forms['userForm'].elements['user.email'].focus();
  			return false;
  		}
  		
  		
  		
  		if(document.forms['userForm'].elements['user.address.city'].value=='')
  		{
  			alert("Please enter the city");
  			document.forms['userForm'].elements['user.address.city'].focus();
  			return false;
  		}
  		
  		
  		
  		if(document.forms['userForm'].elements['user.address.country'].value=='')
  		{
  			alert("Please enter the country");
  			document.forms['userForm'].elements['user.address.country'].focus();
  			return false;
  		}
  		
  		//if(document.forms['userForm'].elements['user.pwdexpiryDate'].value=='')
  		//{
  		//	alert("Please enter the password expiry date");
  		//	document.forms['userForm'].elements['user.pwdexpiryDate'].focus();
  		//	return false;
  		//}
  		
	if((userType=='ACCOUNT' || userType=='AGENT') && parentAgent=='')
  		{
  			alert("For Account or Agent Portal, please fill Parent Agent.");
  			document.forms['userForm'].elements['user.parentAgent'].focus();
  			return false;
  		}
    if(checkJobValids('save')){    
        progressBarAutoSave('1');
	    if ('${autoSavePrompt}' == 'No'){
	
	          var noSaveAction = '<c:out value="${user.id}"/>';
      		  var id1 =document.forms['userForm'].elements['user.id'].value;
      		
               if(document.forms['userForm'].elements['gotoPageString'].value == 'gototab.user'){
               noSaveAction = 'myUsers.html';
               }
          processAutoSave(document.forms['userForm'], 'myUsers!save.html', noSaveAction);
	
	
	}else{
	
   if(!(clickType == 'save'))
   {
     var id1 =document.forms['userForm'].elements['user.id'].value;
     if (document.forms['userForm'].elements['formStatus'].value == '1')
     {
               location.href = 'myUsers.html';
     }
   else
   {
     if(id1 != '')
      {
       if(document.forms['userForm'].elements['gotoPageString'].value == 'gototab.user'){
               location.href = 'myUsers.html';
               }
      }
   }
  }
  }
  }
  }

parentAgentRequired();
function changeStatus()
{
   document.forms['userForm'].elements['formStatus'].value = '1';
}

function showMobiliceService()
{
   if(document.forms['userForm'].elements['user.userType'].value=='CUSTOMER'){	  
	   document.getElementById('maccess').style.display='block';
}else{
	 document.getElementById('maccess').style.display='none';	
}
}
function showPsswdPolicy()
{	
	try{
   if(document.forms['userForm'].elements['user.userType'].value=='USER'){	  
	   document.getElementById("pswdpolicyId").style.display='block';
}else{	
	
	 document.getElementById("pswdpolicyId").style.display='none';	
}}catch(e){}
}
function showConfigDefaultInfo()
{	
	
   if(document.forms['userForm'].elements['user.userType'].value=='USER'){	  
	   document.getElementById("configDefaultInfoId").style.display='block';
	   document.getElementById("configDefaultInfoId1").style.display='block';
}else{	
	
	 document.getElementById("configDefaultInfoId").style.display='none';
	 document.getElementById("configDefaultInfoId1").style.display='none';	
}
}
function resetFields(){
	document.forms['userForm'].reset();
	showPsswdPolicy();
	showConfigDefaultInfo();
	resetSOLIstView();
}

	// getState(document.forms['userForm'].elements['user.countryHome']);
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	setMobileService();
	showMobiliceService();
</script>

<script type="text/javascript">

	<c:if test="${empty user.id}">
	
	var f = document.getElementById('pwdexpiryDate'); 
	f.setAttribute("autocomplete", "off");
	
	function clearPwBox(){
		if (document.getElementById){
			var pw = document.getElementById('pwdexpiryDate');
			if (pw != null){
				pw.value = '';
			}
		}
	}
	window.setTimeout("clearPwBox()", 100);
	</c:if>
try{
	showPsswdPolicy();
	showConfigDefaultInfo();	
}catch(e){
	
}

function resetSOLIstView(){	
	document.forms['userForm'].elements['user.soListView'].value='${user.soListView}';	
}
</script>
<script type="text/javascript">
function getChange1(){
	var temp = '${multiSortforFileCabinet}';
	temp = temp.replace('[','');
	temp = temp.replace(']','');
	var temarr = temp.split(',');
	temp ="";
	for(var i=0 ; i<temarr.length ; i++){
		if(temp==''){
			temp=temarr[i].trim();
		}else{
			temp = temp.trim() +",".trim()+temarr[i].trim();
		}
	}
	$('#select-multiple').val(temp.split(','));
}
</script>
<!--<script src="https://code.jquery.com/jquery-1.11.1.js" type="text/javascript" language="javascript"></script>
	 --><script src="scripts/jquery-1.7.2.min.js" type="text/javascript" language="javascript"></script>
	<script src="scripts/jquery.dragoptions-1.0.js" type="text/javascript" language="javascript"></script>
	 <script>
	 var $j = jQuery.noConflict();
	 $j(document).ready(function($){
		 $j('#select-multiple').dragOptions({highlight: ' '});
        });
    </script>
    <script type="text/javascript">
    window.onload = function() {
     var fileCabinetValue='${user.fileCabinetView}';
	 var e12 = document.getElementById('reasonHid1');
	 var e13 = document.getElementById('reasonHid2');
	 var e15 = document.getElementById('reasonHid4');
		  if(fileCabinetValue=='Document List' || fileCabinetValue==''){
			  e12.style.display="none";
			  e13.style.display="block";
			  e15.style.display="none";
			  document.getElementById('select-single').value='${user.defaultSortforFileCabinet}';
		    } else {
		    	 e12.style.display="block";
				 e13.style.display="none";
				 e15.style.display="block";
				  }
    }
    function getHTTPObjectparentAgentCode()
    {
        var xmlhttp;
        if(window.XMLHttpRequest){
            xmlhttp = new XMLHttpRequest();
        }else if (window.ActiveXObject){
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp){
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    } 
    var httpparentAgentCode = getHTTPObjectparentAgentCode();
    function findParentAgentCode(partnertype){
        var agentCode="";
        if(partnertype=='PA'){
        	agentCode= document.forms['userForm'].elements['user.parentAgent'].value;
        }
        if(partnertype=='BA'){
        	agentCode= document.forms['userForm'].elements['user.bookingAgent'].value;
            }
        if(agentCode!=''){ 
    	     var url="findParentAgentCode.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(agentCode)+"&partnerType="+encodeURIComponent(partnertype);
    	     httpparentAgentCode.open("GET", url, true);
    	     httpparentAgentCode.onreadystatechange = function(){ handleHttpResponse2(partnertype);};
    	     httpparentAgentCode.send(null);
         }
    }
    
    function handleHttpResponse2(partnertype){
        if (httpparentAgentCode.readyState == 4){
           var results = httpparentAgentCode.responseText
           results = results.trim(); 
            var res = results.split("~");
           if(res.length >= 2){ 
          		
          	}else{
          		if(partnertype=='PA'){
          			alert("Parent Agent not valid");  
    	            document.forms['userForm'].elements['user.parentAgent'].value="";
    				document.forms['userForm'].elements['user.parentAgent'].select();
                }
                if(partnertype=='BA'){
                	alert("Booking Agent not valid");  
                	document.forms['userForm'].elements['user.bookingAgent'].value="";
                	document.forms['userForm'].elements['user.bookingAgent'].select();
                    }
	            
		   }    } }
    </script>
