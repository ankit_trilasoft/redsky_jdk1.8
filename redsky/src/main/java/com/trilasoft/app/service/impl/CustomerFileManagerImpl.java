/**
 * Implementation of CustomerFileManager interface.</p>
 * Set the Dao for communication with the data layer.
 * This class represents the basic "CustomerFile" object in Redsky that allows for CustomerFile management.
 * @Class Name	CustomerFileManagerImpl
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * @param 		dao
 */


package com.trilasoft.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;  

import org.appfuse.model.User;
import org.appfuse.service.impl.GenericManagerImpl; 

import com.trilasoft.app.dao.CustomerFileDao;
import com.trilasoft.app.dao.hibernate.dto.ScheduledSurveyDTO;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CustomerFileManager;
   
public class CustomerFileManagerImpl extends GenericManagerImpl<CustomerFile, Long> implements CustomerFileManager {   
    CustomerFileDao customerFileDao;   
  
    public CustomerFileManagerImpl(CustomerFileDao customerFileDao) {   
        super(customerFileDao);   
        this.customerFileDao = customerFileDao;   
    } 
    public List findByBillToCode(String billToCode, String sessionCorpID) { 
        return customerFileDao.findByBillToCode(billToCode, sessionCorpID);
    } 
    public List findByBillToCodeSQLQuery(String billToCode, String sessionCorpID){
    	return customerFileDao.findByBillToCodeSQLQuery(billToCode,sessionCorpID);
    }
    public List findMaximum(String corpID) {
    	return customerFileDao.findMaximum(corpID);
    }
    public List findMaximumId(){
    	return customerFileDao.findMaximumId(); 
    }
    public List findPortalId(String portalId){
    	return customerFileDao.findPortalId(portalId); 
    }
    public List findMaximumShip(String seqNum, String job,String CorpID) {
    	return customerFileDao.findMaximumShip(seqNum, job,CorpID);
    }
    public List findMinimumShip(String seqNum, String job,String CorpID){
    	return customerFileDao.findMinimumShip(seqNum, job,CorpID);	
    }
    public List findCountShip(String seqNum, String job,String CorpID){
    	return customerFileDao.findCountShip(seqNum, job,CorpID);
    }
    public List findMaximumShip12(String seqNum) {
    	return customerFileDao.findMaximumShip12(seqNum);
    }
    public List findAllRecords(){
    	return customerFileDao.findAllRecords();
    }
    public List<CustomerFile> findBySurveyDetail(String estimator,String originCity,Date survey) {   
        return customerFileDao.findBySurveyDetail(estimator,originCity,survey);   
    }
    public List<CustomerFile> findByCSODetail(String last,String first,String seqNum, String billToC, String job, String status,String coord) {   
        return customerFileDao.findByCSODetail(last,first,seqNum,billToC,job,status,coord);   
    }
    public List findSequenceNumber(String seqNum){
    	return customerFileDao.findSequenceNumber(seqNum);   
    }
    public List findByUserName(String userName){
    	return customerFileDao.findByUserName(userName);
    }
    public List findCustomerDetails(String userName){
    	return customerFileDao.findCustomerDetails(userName);
    }
    
    public List findUserFullName(String userName){
    	return customerFileDao.findUserFullName(userName);
    }
    public List<User> findUserForRoleTransfer(String userName){
    	return customerFileDao.findUserForRoleTransfer(userName);
    }
    public boolean findUser(String portalId){
    	return customerFileDao.findUser(portalId);
    }
    public boolean findUserEmail(String email){
    	return customerFileDao.findUserEmail(email);
    }
    public List<CustomerFile> getSurveyDetails(){
    	return customerFileDao.getSurveyDetails();
    }
	public List<CustomerFile> findByCoordinator( String userName ){
    	return customerFileDao.findByCoordinator( userName );
    }
	public List<DataCatalog> findToolTip(String fieldName,String tableName,String corpId) {
		return customerFileDao.findToolTip(fieldName,tableName,corpId);
	}
	public List<SystemDefault> findDefaultBookingAgentDetail(String corpID){
    	return customerFileDao.findDefaultBookingAgentDetail(corpID);
    }
	public List findStateList(String bucket2, String corpId){
		return customerFileDao.findStateList(bucket2, corpId);
	}
	public List findActiveStateList(String bucket2, String corpId){
		return customerFileDao.findActiveStateList(bucket2, corpId);
	}
	public List<CustomerFile> searchQuotationFile(String last,String first,String seqNum, String billToC, String job, String quotationStatus,String coord) {   
        return customerFileDao.searchQuotationFile(last,first,seqNum,billToC,job,quotationStatus,coord);   
    }
	public List<CustomerFile> customerList(){
		return customerFileDao.customerList();
	}
	public List<CustomerFile> findByCoordinatorQuotation( String userName ){
		return customerFileDao.findByCoordinatorQuotation(userName);
	}
	public List<CustomerFile> quotationList(){
		return customerFileDao.quotationList();
	}
	public Map<String, String> findDefaultStateList(String bucket2, String corpId){
		return customerFileDao.findDefaultStateList(bucket2, corpId);
	}
	public List<ScheduledSurveyDTO> findAllScheduledSurveys(String corpID, String consult){
		return customerFileDao.findAllScheduledSurveys( corpID, consult);
	}
	public List<ScheduledSurveyDTO> scheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob){
		return customerFileDao.scheduledSurveys(sessionCorpID,consult,surveyFrom,surveyTo,surveyCity,surveyJob);
			
	}
	public List driverEventList(String sessionCorpID,String eventFrom,String eventTo){
		return customerFileDao.driverEventList(sessionCorpID, eventFrom, eventTo);
	}
	
	public List<ScheduledSurveyDTO> searchAllScheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob){
		return customerFileDao.searchAllScheduledSurveys(sessionCorpID,consult,surveyFrom,surveyTo,surveyCity,surveyJob);
	}
	public List searchAllScheduledEvent(String sessionCorpID,String eventFrom,String eventTo,String driverCode,String driverFirstName,String driverlastName){
		return customerFileDao.searchAllScheduledEvent(sessionCorpID, eventFrom, eventTo, driverCode, driverFirstName, driverlastName);
	}
	public List findServiceOrderList(String sequenceNumber, String corpId) {
		return customerFileDao.findServiceOrderList(sequenceNumber, corpId);
	}
	public List findPartnerQuotesList(String sequenceNumber, String corpId) {
		return customerFileDao.findPartnerQuotesList(sequenceNumber, corpId);
	}
	public List findBookingCode()
	{
		return customerFileDao.findBookingCode();
	}
	public List findDefaultBookingAgentCode(String companyDivision){
		return customerFileDao.findDefaultBookingAgentCode(companyDivision);
	}
	public List findBillToCode()
	{
		return customerFileDao.findBillToCode();
	}
	public List findCustJobContract(String billToCode, String job, Date createdon, String corpID,String companyDivision, String bookingAgentCode){
		return customerFileDao.findCustJobContract(billToCode, job, createdon, corpID,companyDivision, bookingAgentCode);
	}
	public List findCompanyDivision(String corpId){
		return customerFileDao.findCompanyDivision(corpId);
	}
	public List findCompanyDivisionByJob(String corpId, String job){
		return customerFileDao.findCompanyDivisionByJob(corpId,job);
	}
	public List findCompanyDivisionByBookAg(String corpId, String bookAg){
		return customerFileDao.findCompanyDivisionByBookAg(corpId,bookAg);
	}
	public int setStatus(String seqNum, String status,String statusNumber){
		
		return customerFileDao.setStatus(seqNum, status,statusNumber);
	}
	public int setFnameLname(String shipNum, String firstName,String lastName){
		return customerFileDao.setFnameLname(shipNum, firstName, lastName);
	}
	public List<CompanyDivision> defBookingAgent(String companyDivision,String corpID){
		
		return customerFileDao.defBookingAgent(companyDivision,corpID);
	}
	public List findByCorpId(String corpID){
		return customerFileDao.findByCorpId(corpID);
	}
	public ArrayList getServiceOrderClosedStatus(String sequenceNumberForStatus,String corpID) {
		return customerFileDao.getServiceOrderClosedStatus(sequenceNumberForStatus,corpID);
	}
	public int workTicketUpdate(String shipNum, String firstName,String lastName){
		return customerFileDao.workTicketUpdate(shipNum, firstName, lastName);	
	}
	public int setFirstName(String shipNum, String firstName){
		return customerFileDao.setFirstName(shipNum, firstName);
	}
	public int setSSNo(String shipNum, String socialSecurityNumber){
		return customerFileDao.setSSNo(shipNum, socialSecurityNumber);
	}
	public int workTicketFirstName(String shipNum, String firstName){
		return customerFileDao.workTicketFirstName(shipNum, firstName);
	}
	public List findCoordEmailAddress(String userName){
		return customerFileDao.findCoordEmailAddress(userName);
	}
	public List findCoordSignature(String userName){
		return customerFileDao.findCoordSignature(userName);
	}
	
	public List addAndCopyCustomerFile(Long cid,String corpID){
		return customerFileDao.addAndCopyCustomerFile(cid, corpID);
	}
	public void updateEmailInUser(String cPortalId, String corpID,String emailId){
		   customerFileDao.updateEmailInUser(cPortalId, corpID,emailId);
	}
	public void resetPassword(String passwordNew,String confirmPasswordNew,String email,Date compareWithDate){
		customerFileDao.resetPassword(passwordNew, confirmPasswordNew, email,compareWithDate);
	}
	public int setSalesStatus(String seqNum){
		return customerFileDao.setSalesStatus(seqNum);
	}
	public ArrayList getServiceOrderCancelledStatus(String sequenceNumberForStatus, String corpID) { 
		return customerFileDao.getServiceOrderCancelledStatus(sequenceNumberForStatus, corpID);
	}
	public void resetAccount(String name,String corpID){
		customerFileDao.resetAccount(name, corpID);
	}
	public void resetUpdateAccount(String name,String corpID){
		customerFileDao.resetUpdateAccount(name, corpID);
	}
	public List findCustomerPortalListDetails(String userName){
		return customerFileDao.findCustomerPortalListDetails(userName);
	}
	public List findCompanyName(String corpID){
		return customerFileDao.findCompanyName(corpID);
	}
	public int updateServiceOrderOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact, String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String email,String email2,String originCityCode,String originCountryCode,String sequenceNumber, String userName,String linkedUpdate){
	return customerFileDao.updateServiceOrderOriginAddress(originAddress1, originAddress2, originAddress3, originCompany, originCountry, originState, originCity, originZip, originPreferredContact, originDayPhone, originDayPhoneExt, originHomePhone, originMobile, originFax, email, email2,originCityCode,originCountryCode,sequenceNumber,userName,linkedUpdate);
	}
	public int updateWorkTicketOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact,String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String sequenceNumber, String userName)
	{
		return customerFileDao.updateWorkTicketOriginAddress(originAddress1, originAddress2, originAddress3, originCompany, originCountry, originState, originCity, originZip, originPreferredContact,originDayPhone, originDayPhoneExt, originHomePhone, originMobile, originFax, sequenceNumber, userName);
	}
	public int updateServiceOrderDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact,String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String destinationEmail,String destinationEmail2,String destinationCityCode,String destinationCountryCode,String sequenceNumber, String userName,String linkedUpdate)
	{
		return customerFileDao.updateServiceOrderDestinationAddress(destinationAddress1, destinationAddress2, destinationAddress3, destinationCompany, destinationCountry, destinationState, destinationCity, destinationZip, destPreferredContact, destinationDayPhone, destinationDayPhoneExt, destinationHomePhone, destinationMobile, destinationFax, destinationEmail, destinationEmail2,destinationCityCode,destinationCountryCode, sequenceNumber,userName,linkedUpdate);
	}
	public int updateWorkTicketDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact,String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String sequenceNumber, String userName,String destinationCountryCode)
	{
		return customerFileDao.updateWorkTicketDestinationAddress(destinationAddress1, destinationAddress2, destinationAddress3, destinationCompany, destinationCountry, destinationState, destinationCity, destinationZip, destPreferredContact, destinationDayPhone, destinationDayPhoneExt, destinationHomePhone, destinationMobile, destinationFax, sequenceNumber,userName,destinationCountryCode);
	}
	public List findCityState(String zipCode, String corpID){
		return customerFileDao.findCityState(zipCode, corpID);
	}
	public String findCityStateFlex(String countryCodeFlex, String corpID){
		return customerFileDao.findCityStateFlex(countryCodeFlex, corpID);
	}
	public List findCityStateNotPrimary(String zipCode, String corpID,String country){
		return customerFileDao.findCityStateNotPrimary(zipCode, corpID,country);
	}
	public List surveySchedule(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant){
		return customerFileDao.surveySchedule(job, corpID,DateInterval,fromDate,toDate,surveyTime,surveyTime2,consultant);
	}
	public int setPrefix(String shipNum, String prefix){
		return customerFileDao.setPrefix(shipNum, prefix);
	}
	public List findsurveyScheduleList(String estimator,String surveyDate,String consultantTime,String consultantTime2,String corpID)
	{
		return customerFileDao.findsurveyScheduleList(estimator, surveyDate, consultantTime, consultantTime2,corpID);
	}
	public List findCordinaorListRef(String custJobType, String sessionCorpID) {
		return customerFileDao.findCordinaorListRef(custJobType, sessionCorpID);
	}
	public List findEstimatorListRef(String custJobType, String sessionCorpID) {
		return customerFileDao.findEstimatorListRef(custJobType, sessionCorpID);
	}
	public List findEstimatorName(String estimatorEmail, String sessionCorpID){
		return customerFileDao.findEstimatorName(estimatorEmail, sessionCorpID);
	}
	public List findEstimatorFirstName(String estimatorEmail, String sessionCorpID){
		return customerFileDao.findEstimatorFirstName(estimatorEmail, sessionCorpID);
	}
	public List findUserPhone(String remoteUser, String sessionCorpID){
		return customerFileDao.findUserPhone(remoteUser, sessionCorpID);
	}
	public List findWebsite(String remoteUser, String sessionCorpID){
		return customerFileDao.findWebsite(remoteUser, sessionCorpID);
	}
	public List surveyScheduleFromSurveyList(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant,String surveyCity)
	{
		return customerFileDao.surveyScheduleFromSurveyList(job, corpID, DateInterval, fromDate, toDate, surveyTime, surveyTime2, consultant,surveyCity);
	}
	public ArrayList getWorkTicketStatusClosedStatus(String sequenceNumberForStatus, String sessionCorpID){
		return customerFileDao.getWorkTicketStatusClosedStatus(sequenceNumberForStatus, sessionCorpID);	
	}
	public int updateSOStatus(String sequenceNumberForStatus, String statusReas, String sessionCorpID,String userName){
		return customerFileDao.updateSOStatus(sequenceNumberForStatus, statusReas, sessionCorpID,userName);
	}
	public Map<String, String> findExistingStateList(String bucket2, String corpId,String stateCode){
		return customerFileDao.findExistingStateList(bucket2, corpId, stateCode);
	}
	public List getPartnerDitailList(String sessionCorpID, String parentAgent) {
		return customerFileDao.getPartnerDitailList(sessionCorpID, parentAgent);
	}
	public List getPartnerAccountmanager(String sessionCorpID, String parentAgent) {
		return customerFileDao.getPartnerAccountmanager(sessionCorpID, parentAgent);
	}
	public List getBookingAgentDetailList(String sessionCorpID, String parentAgent) {
		return customerFileDao.getBookingAgentDetailList(sessionCorpID, parentAgent);
	}
	public int updateOrderInitiationStatus(Long id, String sessionCorpID, String quotationStatus) {
		return customerFileDao.updateOrderInitiationStatus(id, sessionCorpID, quotationStatus);
	}
	public int updateOrderManagementStatus(Long id, String sessionCorpID, String quotationStatus,String orderAction,boolean accessQuotationFromCustomerFile) {
		return customerFileDao.updateOrderManagementStatus(id, sessionCorpID, quotationStatus, orderAction, accessQuotationFromCustomerFile);
	}
	public List getRefJobTypeCountry(String job,String sessionCorpID,String compDivision){
		return customerFileDao.getRefJobTypeCountry(job, sessionCorpID,compDivision);
	}
	public List findMaximumSequenceNumber(String externalCorpID) {
		return customerFileDao.findMaximumSequenceNumber(externalCorpID);
	}
	public List findMaximumShipExternal(String sequenceNumber, String externalCorpID) {
		return customerFileDao.findMaximumShipExternal(sequenceNumber, externalCorpID);
	}
	public Boolean checkNetworkRecord(String sessionCorpID,	String bookingAgentCode) {
		return customerFileDao.checkNetworkRecord(sessionCorpID, bookingAgentCode);
	}
	public void updateIsNetworkFlag(Long customerFileId, String corpID,Long serviceOrderId, String contractType, Boolean isNetworkGroup) {
		customerFileDao.updateIsNetworkFlag(customerFileId, corpID, serviceOrderId,  contractType,   isNetworkGroup);
		
	}
	public List getLinkedSequenceNumber(String sequenceNumber, String recordType) {
		return customerFileDao.getLinkedSequenceNumber(sequenceNumber, recordType);
	}
	public Long findRemoteCustomerFile(String sequenceNumber) {
		return customerFileDao.findRemoteCustomerFile(sequenceNumber);
	}
	public List findFieldToSync(String modalClass, String fieldType, String uniqueFieldType) {
		return customerFileDao.findFieldToSync(modalClass,  fieldType,  uniqueFieldType);
	}
	public List findFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType) {
		return customerFileDao.findFieldToSyncCreate(modalClass,   fieldType,   uniqueFieldType);
	}
	public List findAddressList(String sequenceNumber, String sessionCorpID){
		return customerFileDao.findAddressList(sequenceNumber, sessionCorpID);
	}
	public List<ServiceOrder> findAddress(String countryName, String sessionCorpID){
		return customerFileDao.findAddress(countryName, sessionCorpID);
		}
	public List<ServiceOrder> findCportalResourceMgmtDoc(String seq,String sessionCorpID,String usertype){
		return customerFileDao.findCportalResourceMgmtDoc(seq, sessionCorpID, usertype);
	}
	public List<ServiceOrder> findCustomerResourceMgmtDoc(String seq,String sessionCorpID,String usertype){
		return customerFileDao.findCustomerResourceMgmtDoc(seq, sessionCorpID,usertype);
	}
	public Boolean isNetworkedRecord(Long customerFileId) {
		return customerFileDao.isNetworkedRecord(customerFileId);
	}
	public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID) {
		return customerFileDao.findResponsiblePerson(custJobType, companyDivision, sessionCorpID);
	}
	public List findCityStateByCountry(String zipCode, String corpID ,String country){
		return customerFileDao.findCityStateByCountry(zipCode, corpID, country);
		}
	public List universalSearch(String firstName, String lastName, String sessionCorpID, String sequenceNumber, String billToName, String job, String coordinator, String companyDivision, String registrationNumber, Boolean activeStatus, String status, String personPricing, String serviceOrderSearchVal, String usertype, String email,String accountName,String cityCountryZipOption,String originCityOrZip,String destinationCityOrZip,String originCountry,String destinationCountry) {
		return customerFileDao.universalSearch(firstName, lastName, sessionCorpID, sequenceNumber, billToName, job, coordinator, companyDivision, registrationNumber, activeStatus, status, personPricing, serviceOrderSearchVal,  usertype, email, accountName,cityCountryZipOption,originCityOrZip,destinationCityOrZip,originCountry,destinationCountry);
	}
	public String getNetworkCoordinator(String externalCorpID) {
		return customerFileDao.getNetworkCoordinator(externalCorpID);
	}
	public String getSystemVolumeUnit(String sessionCorpID) { 
		return customerFileDao.getSystemVolumeUnit(sessionCorpID);
	}
	public String getSystemWeightUnit(String sessionCorpID) {
		return customerFileDao.getSystemWeightUnit(sessionCorpID); 
	}
	public List getQuotationServiceOrderlist(Long id, String sessionCorpID) {
		return customerFileDao.getQuotationServiceOrderlist(id, sessionCorpID);
	}
	public int updateSOStatusREOPEN(String sequenceNumber,  String sessionCorpID) {
		return customerFileDao.updateSOStatusREOPEN(sequenceNumber,  sessionCorpID);
	}
	public String findRemovalReloCheck(String customerFileId){
		return customerFileDao.findRemovalReloCheck(customerFileId);
	}
	public List getQuotesToGoList(Long id, String sessionCorpID,String companyDivision,String bookingAgentCode){
		return customerFileDao.getQuotesToGoList(id, sessionCorpID, companyDivision,bookingAgentCode);
	}
	public List getQuotesToValidate(String sessionCorpID,String jobCode,String companyDivision){
		return customerFileDao.getQuotesToValidate(sessionCorpID, jobCode , companyDivision);
	}
	public void updateCustomerFileRegNumber(String trackingCodeValue,Long customerFileId, String corpID){
		customerFileDao.updateCustomerFileRegNumber(trackingCodeValue,customerFileId, corpID);
	}
	public void updateVoxmeFlag(Long id){
		customerFileDao.updateVoxmeFlag(id);
	}
	public String findSurveyTool(String corpID, String job, String companyDivision){
		return customerFileDao.findSurveyTool(corpID, job, companyDivision);
	}
	public List findResourceIdPassword(String bookingAgentCode,String corpId){
		return customerFileDao.findResourceIdPassword(bookingAgentCode, corpId);
	}
	public String getMessageText(String userPhone, String companyDivision, String sessionCorpID){
		return customerFileDao.getMessageText(userPhone, companyDivision, sessionCorpID);
	}
	public void updateServiceOrderBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID)
	{
		customerFileDao.updateServiceOrderBillToCode(billTocode, billToName, sequenceNumber, corpID);		
	}
	public void updateBillingBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID){
		customerFileDao.updateBillingBillToCode(billTocode, billToName, sequenceNumber, corpID);
	}
	public int quoteReasonStatus(String corpID,Long id){
		return customerFileDao.quoteReasonStatus(corpID, id);
	}
	public Long findNewRemoteCustomerFile(String sequenceNumber, String externalCorpID) {
		return customerFileDao.findNewRemoteCustomerFile(sequenceNumber, externalCorpID);
	}
	 public List  BookingAgentName(String partnerCode,String corpid,Boolean cmmDmmFlag,String pType){
		 return customerFileDao.BookingAgentName(partnerCode,corpid,cmmDmmFlag, pType);
	 }
	public List findAccFieldToSyncCreate(String modalClass, String type) {
		return customerFileDao.findAccFieldToSyncCreate(modalClass,  type);
	}
	public List findAccFieldToSync(String modalClass, String type) {
		return customerFileDao.findAccFieldToSync(modalClass,  type);
	}
	public List findBillingFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType) {
		return customerFileDao.findBillingFieldToSyncCreate(modalClass,  fieldType, uniqueFieldType);
	}
	public List findBillingFieldToSync(String modalClass, String fieldType, String uniqueFieldType) {
		return customerFileDao.findBillingFieldToSync(modalClass,  fieldType, uniqueFieldType);
	}
	public List findAccDMMNetWorkFieldToSync(String modalClass, String type) {
		return customerFileDao.findAccDMMNetWorkFieldToSync(modalClass, type);
	}
	public List getFlatFileExtraction(String fileType, String parentAgent1, String corpId,String fromDt, String toDt, String addChargesType,boolean checkFieldVisibility){
		return customerFileDao.getFlatFileExtraction(fileType, parentAgent1, corpId, fromDt, toDt, addChargesType,checkFieldVisibility);
	}
	public boolean getQualitySurvey(String corpId){
		return customerFileDao.getQualitySurvey(corpId);
	}
	public List<ServiceOrder> getServiceOrderList(Long id) {
		return customerFileDao.getServiceOrderList(id);
	}
	public String enableStateList(String corpId){
		return customerFileDao.enableStateList(corpId);
	}
	public String enableStateList1(String corpId){
		return customerFileDao.enableStateList1(corpId);
	}
	/* Added by kunal for ticket number: 6183*/ 
	public List getFilterData(Long userId, String sessionCorpID){
		return customerFileDao.getFilterData(userId, sessionCorpID);
	}
	public List getOrderInitiationBillToCode(String sessionCorpID, String parentCode){
		return customerFileDao.getOrderInitiationBillToCode(sessionCorpID, parentCode);
	}
	public List searchOrderInitiationBillToCode(String sessionCorpID, String parentCode, String partnerCode, String description, String billingCountryCode, String billingCountry, String extReference,String billingCity)
	{
		return customerFileDao.searchOrderInitiationBillToCode(sessionCorpID, parentCode, partnerCode, description,billingCountryCode, billingCountry,extReference,  billingCity);
	}
	public List findMergeSOList(String sessionCorpID, String bookingAgentCode,Boolean checkAccessQuotation){
		return customerFileDao.findMergeSOList(sessionCorpID, bookingAgentCode, checkAccessQuotation);
	}
	public List findLinkSOtoCF(String sessionCorpID, String seqNumber){
		return customerFileDao.findLinkSOtoCF(sessionCorpID, seqNumber);
	}
	public List getQFLinkAccLine(String sessionCorpID, Long sid){
		return customerFileDao.getQFLinkAccLine(sessionCorpID,sid);
	}
	public List getQFFilesId(String sessionCorpID, String fileId){
		return customerFileDao.getQFFilesId(sessionCorpID, fileId);
	}
	public List getQFNotesId(String sessionCorpID, String notesId,Long notesKeyId){
		return customerFileDao.getQFNotesId(sessionCorpID, notesId, notesKeyId);
	}
	public List getQFInventoryData(String sessionCorpID, Long cid){
		return customerFileDao.getQFInventoryData(sessionCorpID, cid);
	}
	public List getQuotesInventoryData(String sessionCorpID,Long customerFileId, Long sid, String mode){
		return customerFileDao.getQuotesInventoryData(sessionCorpID, customerFileId, sid, mode);
	}
	public List getQFInventoryPathList(String sessionCorpID, Long invId, Long accInfoId){
		return customerFileDao.getQFInventoryPathList(sessionCorpID, invId, accInfoId);
	}
	public List getQFAccessInfo(String sessionCorpID, Long id){
		return customerFileDao.getQFAccessInfo(sessionCorpID, id);
	}
	public List getServiceOrderListRemote(Long id) {
		return customerFileDao.getServiceOrderListRemote(id);
	}
	public String findPrivatepartyBilltoCode(String billToCode, String corpID) {
		return customerFileDao.findPrivatepartyBilltoCode(billToCode, corpID);
	}
	public Boolean getInsurancebooking(String partnerCode,String billToCode, String corpID){
		return customerFileDao.getInsurancebooking(partnerCode,billToCode, corpID);
	}
	public List findMaximumShipCF(String seqNum, String job, String CorpID) {
		return customerFileDao.findMaximumShipCF(seqNum, job, CorpID);
	}
	public List getInsuranceValue(String billToCode, String corpID){
		return customerFileDao.getInsuranceValue(billToCode, corpID);
	}
	public List getAdditionalChargesExtraction(String fileType, String parentAgent1, String chargeCode, String corpId){
		return customerFileDao.getAdditionalChargesExtraction(fileType, parentAgent1,chargeCode, corpId);
	}
	public String findAgentparentByBillToCode(String billToCode, String corpID){
		return customerFileDao.findAgentparentByBillToCode(billToCode, corpID);
	}
	public Boolean checkInsurance(String billToCode, String corpID){
		return customerFileDao.checkInsurance(billToCode,corpID);
	}
	public List findAgentAccFieldToSyncCreate(String modalClass, String type) {
		return customerFileDao.findAgentAccFieldToSyncCreate(modalClass, type);
	}
	public void getUpdateBilling(String noInsurance,String seqNumber,String sessionCorpID){
		customerFileDao.getUpdateBilling( noInsurance, seqNumber, sessionCorpID);
	}
	public List findOrderInitiationBillToCode(String sessionCorpID, String parentCode, String bCode) {
		return customerFileDao.findOrderInitiationBillToCode(sessionCorpID, parentCode, bCode);
	}
	public List surveyCalendarList(String corpID,String consultant){
		return customerFileDao.surveyCalendarList(corpID,consultant);
	}
	public List getUserURL(String userRole, String sessionCorpID) {
		return customerFileDao.getUserURL(userRole, sessionCorpID);
	}
	public void updateInAllSoCordName(Long custid,String cordName,String sessionCorpID){
		 customerFileDao.updateInAllSoCordName(custid,cordName, sessionCorpID);
	}

	public List getChildAgentCode(String parentCode, String sessionCorpID) {
		return customerFileDao.getChildAgentCode(parentCode, sessionCorpID);
	}
	public List getPartnerDetails(String parentAgent ,String sessionCorpID){
		return customerFileDao.getPartnerDetails(parentAgent ,sessionCorpID);
	}
	public List findLeadAutocompleteList(String stateNameOri,String countryNameOri,String cityNameOri, String corpId){
		return customerFileDao.findLeadAutocompleteList(stateNameOri ,countryNameOri,cityNameOri,corpId);
	}
	public List findLeadAutocompleteListDest(String stateNameDes,String countryNameDes,String cityNameDes, String corpId){
		return customerFileDao.findLeadAutocompleteListDest(stateNameDes ,countryNameDes,cityNameDes,corpId);
	}
	public String findParentByAccCode(String accCode, String sessionCorpID){
		return customerFileDao.findParentByAccCode(accCode ,sessionCorpID);
	}
	public List findLeadCaptureList(String sequenceNumber,String lastName,String firstName,Date createdOn, String job,String salesPerson,String status,Date moveDate,String serviceOrderSearchType, String sessionCorpID){
		return customerFileDao.findLeadCaptureList(sequenceNumber,lastName,firstName,createdOn,job,salesPerson,status,moveDate,serviceOrderSearchType ,sessionCorpID);
	}
	public String findCountryNameByCode(String countyName,String sessionCorpID,String parameter){
		return customerFileDao.findCountryNameByCode(countyName ,sessionCorpID,parameter);
	}
	public String getCompanyDivisionNetworkCoordinator(String companyDivision, String externalCorpID) {
		return customerFileDao.getCompanyDivisionNetworkCoordinator(companyDivision, externalCorpID);
	}
	public List findPartnerDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag){
		return customerFileDao.findPartnerDetailsForAutoComplete(partnerName,partnerNameType ,corpid,cmmDmmFlag);
	}
	public List findBillDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid){
		return customerFileDao.findBillDetailsForAutoComplete(partnerName, partnerNameType, corpid);
	}
	/*public void  updateSOExtractFieldOnCFSave(Long id) {
		 customerFileDao.updateSOExtractFieldOnCFSave(id);
	}*/
	public void updateUpdaterOnCFSave(Long id){
		customerFileDao.updateUpdaterOnCFSave(id);
	}
	public List getEstimatorNumber(String estimator,String corpid){
		return customerFileDao.getEstimatorNumber(estimator, corpid);
	}
	public String findDefaultValFromPartner(String bCode, String sessionCorpID){
		return customerFileDao.findDefaultValFromPartner(bCode, sessionCorpID);
	}
	public void  updateSODashboardSurveyDt(CustomerFile cf) {
		 customerFileDao.updateSODashboardSurveyDt(cf);
	}
	public List findNotesValueForOutlook(String seqNumber,Long id, String sessionCorpID){
		return customerFileDao.findNotesValueForOutlook(seqNumber,id, sessionCorpID);
	}
	public List getAllUserContactListByPartnerCode(String billToCode , String sessionCorpID , String name , String emailId)
	{
		return customerFileDao.getAllUserContactListByPartnerCode(billToCode,sessionCorpID,name,emailId);
	}
	public void updateServiceOrderLinkedField(Long id, String lastName, String prefix, String rank, String firstName, String socialSecurityNumber, Boolean vip,String userName) {
		customerFileDao.updateServiceOrderLinkedField(id, lastName, prefix, rank, firstName, socialSecurityNumber, vip, userName);
	}
	public List findBilltoNameAutocompleteList(String billtoName, String corpId){
		return customerFileDao.findBilltoNameAutocompleteList(billtoName,corpId);
	}
	public void updateLinkedCustomerFileStatus(String status, int statusNumber, Date statusDate, Long customerFileId,String userName) {
		customerFileDao.updateLinkedCustomerFileStatus(status, statusNumber, statusDate, customerFileId,userName);
		
	}
	public String getNetworkUnit(String corpID) { 
		return customerFileDao.getNetworkUnit(corpID);
	}
	public void updateBillingNetworkBillToCode(String sequenceNumber, String accountCode, String accountName, String oldCustomerFileAccountCode) {
		customerFileDao.updateBillingNetworkBillToCode(sequenceNumber, accountCode, accountName, oldCustomerFileAccountCode);		
	}
	public List findCompanyDivisionByBookAgAndCompanyDivision(String corpId, String bookAg,String companyDivision){
		return customerFileDao.findCompanyDivisionByBookAgAndCompanyDivision(corpId,bookAg,companyDivision);
	}
	public List findCompanyDivisionWithoutBookAgAndCompanyDivision(String corpId){
		return customerFileDao.findCompanyDivisionWithoutBookAgAndCompanyDivision(corpId);
	}
	public String checkNetworkCoordinatorInUser(String cfContractType, String sessionCorpID){
		return customerFileDao.checkNetworkCoordinatorInUser(cfContractType,sessionCorpID);
	}
	public String checkCorpIdInCompDiv(String bookAg){
		return customerFileDao.checkCorpIdInCompDiv(bookAg);
	}
	public List findOpenCompanyDivision(String sessionCorpID) { 
		return customerFileDao.findOpenCompanyDivision(sessionCorpID);
	}
	public void updateBillToName(String billToCodeVal, String billToNameVal, String corpid, Long id, String sequenceNo){
		customerFileDao.updateBillToName(billToCodeVal, billToNameVal, corpid, id, sequenceNo);
	}
	public Boolean findCustPortalId(String linkedSeqNumber,	String sequenceNumber){
		return customerFileDao.findCustPortalId(linkedSeqNumber,sequenceNumber);
	}
	public String getCompanyCodeAndJobFromCompanyDivision(String sessionCorpID, String bookAg){
		return customerFileDao.getCompanyCodeAndJobFromCompanyDivision(sessionCorpID,bookAg);
	}
	public List checkPrivatePartyFromPartner(String billToCode,String sessionCorpID,String bookingAgentCode) { 
		return customerFileDao.checkPrivatePartyFromPartner(billToCode,sessionCorpID,bookingAgentCode);
	}
	public String getSurveyMessageText(String parameter, String language, String sessionCorpID){
		return customerFileDao.getSurveyMessageText(parameter, language, sessionCorpID);
	}
	public String getPreffixFromRefmaster(String parameter, String language, String code){
		return customerFileDao.getPreffixFromRefmaster(parameter, language, code);
	}
	public String getDataUpdateDiffLanguage(String services, String corpId,String changedLanguage,Long serviceId)
	{
		return customerFileDao.getDataUpdateDiffLanguage(services, corpId, changedLanguage,serviceId);
	}
	public Map<String, String> getContractAccountMap(String sessionCorpID) {
		return customerFileDao.getContractAccountMap(sessionCorpID);
	}
	public String getContractAccountDetail(String externalCorpID, String contract) {
		return customerFileDao.getContractAccountDetail(externalCorpID, contract);
	}
	public List findAccEstimateRevisionFieldToSync(String modalClass, String type) {
		return customerFileDao.findAccEstimateRevisionFieldToSync(modalClass, type);
	}
	public List findIkeaOrderPartnerDetailsForAutoComplete(String partnerName,String partnerCodes,String corpid){
		return customerFileDao.findIkeaOrderPartnerDetailsForAutoComplete(partnerName, partnerCodes,corpid);
	}
	public List getIkeaInfo(String sessionCorpID,String parentCode,String partnerCode){
		
		return customerFileDao.getIkeaInfo(sessionCorpID,parentCode,partnerCode);
	}
	public List findUserSignature(String fromEmail){
		return customerFileDao.findUserSignature(fromEmail);
	}
	public String findRegNumber(String corpId,Long id){
		return customerFileDao.findRegNumber(corpId, id);
	}
	public Map<String, String> parentAccountCodeMap(String sessionCorpID) {
		return customerFileDao.parentAccountCodeMap(sessionCorpID);
	}
	public Map<String, List> childAccountCodeMap(String sessionCorpID) {
		return customerFileDao.childAccountCodeMap(sessionCorpID); 
	}
	public List findAgentVendors( String parentCode){
		return customerFileDao.findAgentVendors(parentCode);
	}
	public List findAgentBillToCode(String parentCode, String bCode) {
		
		return customerFileDao.findAgentBillToCode(parentCode,bCode);
	}
	public List searchAgentVendors(String sessionCorpID, String parentCode, String partnerCode, String description, String billingCountryCode, String billingCountry, String extReference, String billingCity) {
		return customerFileDao.searchAgentVendors(sessionCorpID, parentCode, partnerCode, description, billingCountryCode, billingCountry, extReference, billingCity);
	}
}  