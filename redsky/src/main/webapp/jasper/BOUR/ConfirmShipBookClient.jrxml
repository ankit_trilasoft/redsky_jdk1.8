<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="ConfirmShipBookClient"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="841"
		 columnWidth="595"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="0"
		 topMargin="0"
		 bottomMargin="0"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<parameter name="User Name" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Send Option" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "ConfirmShipBookClient.jrxml",
	DATE_FORMAT(now(),'%W, %D %M %Y') AS curr_date,
	concat(if(customerfile.rank is null || trim(customerfile.rank)='',Concat(if(customerfile.prefix is null || trim(customerfile.prefix)='',"",trim(customerfile.prefix)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName)))),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)='') AND (customerfile.firstName is null || trim(customerfile.firstName)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))),Concat(if(customerfile.rank is null || trim(customerfile.rank)='',"",trim(customerfile.rank)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.rank is null || trim(customerfile.rank)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName)))),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.rank is null || trim(customerfile.rank)='') AND (customerfile.firstName is null || trim(customerfile.firstName)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))))) AS customerfileName,
	concat("Dear ",if(customerfile.rank is null || trim(customerfile.rank)='',Concat(if(customerfile.prefix is null || trim(customerfile.prefix)='',"",trim(customerfile.prefix)),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))),Concat(if(customerfile.rank is null || trim(customerfile.rank)='',"",trim(customerfile.rank)),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.rank is null || trim(customerfile.rank)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))))) AS customerfile_RankPrefixLastname,
   	if(serviceorder.destinationAddressLine1 is null || trim(serviceorder.destinationAddressLine1)='','',trim(serviceorder.destinationAddressLine1)) AS serviceorder_destinationAddressLine1,
	if(serviceorder.destinationAddressLine2 is null || trim(serviceorder.destinationAddressLine2)='','',trim(serviceorder.destinationAddressLine2)) AS serviceorder_destinationAddressLine2,
	if(serviceorder.destinationAddressLine3 is null || trim(serviceorder.destinationAddressLine3)='','',trim(serviceorder.destinationAddressLine3)) AS serviceorder_destinationAddressLine3,
	if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='','',trim(serviceorder.destinationCity)) AS serviceorder_destinationCity,
	if(serviceorder.destinationZip is null || trim(serviceorder.destinationZip)='','',trim(serviceorder.destinationZip)) AS serviceorder_destinationZip,
	if(serviceorder.destinationCountry is null || serviceorder.destinationCountry='','',serviceorder.destinationCountry) AS serviceorder_destinationCountry,
	serviceorder.`companyDivision` AS serviceorder_companyDivision,
	serviceorder.job AS serviceorder_job,
	concat("Client ID: ",customerfile.`sequenceNumber`) AS customerfile_sequenceNumber,
	concat("Thank you once again for entrusting your consignment to ",if(company.`companyName` is null || trim(company.`companyName`)='','',trim(company.`companyName`)),", we look forward to being of service to you in the future.","\n\n","Yours sincerely,") AS company_companyName,
	concat(app_user.`first_name`," ",app_user.`last_name`) AS appUser_firstNameLastName,
	app_user.`userTitle` AS app_user_userTitle,
	servicepartner.cntnrNumber AS servicepartner_cntnrNumber,
	DATE_FORMAT(servicepartner.`etArrival`,'%W, %D %M %Y') AS servicepartner_etArrival,
	servicepartner_max.carrierArrival AS servicepartner_max_carrierArrival,
	DATE_FORMAT(servicepartner.`atDepart`,'%W, %D %M %Y') AS servicepartner_atDepart,
	servicepartner.`carrierName` AS servicepartner_carrierName,
	servicepartner.`carrierVessels` AS servicepartner_carrierVessels,
	if((partnerpublic.`firstName` is null || trim(partnerpublic.`firstName`)=''),trim(partnerpublic.`lastName`),concat(trim(partnerpublic.`firstName`)," ",trim(partnerpublic.`lastName`)))AS partnerpublicFirstNameLastName,
	partnerpublic.`mailingEmail` AS partnerpublic_mailingEmail,
	partnerpublic.`mailingFax` AS partnerpublic_mailingFax,
	partnerpublic.`mailingPhone` AS partnerpublic_mailingPhone,
	concat("For ",if(companydivision.description is null || trim(companydivision.description)='',"",companydivision.description)) AS companydivision_description
FROM customerfile customerfile
	INNER JOIN `serviceorder` serviceorder on customerfile.id=serviceorder.customerfileid and serviceorder.corpid=$P{Corporate ID}
	INNER JOIN `trackingstatus` trackingstatus on serviceorder.id=trackingstatus.id and trackingstatus.corpid=$P{Corporate ID}
	LEFT OUTER JOIN `servicepartner` servicepartner on servicepartner.serviceorderid=serviceorder.id and servicepartner.corpid=$P{Corporate ID} and servicepartner.status is true and servicepartner.carrierNumber = (select min(aa.carrierNumber) from servicepartner aa where servicepartner.serviceOrderId = aa.serviceOrderId and aa.corpid=$P{Corporate ID} and aa.status is true group by servicepartner.carrierNumber)
	LEFT OUTER JOIN `servicepartner` servicepartner_max on servicepartner_max.serviceorderid=serviceorder.id and servicepartner_max.corpid=$P{Corporate ID} and servicepartner_max.status is true and servicepartner_max.carrierNumber = (select max(aa.carrierNumber) from servicepartner aa where servicepartner_max.serviceOrderId = aa.serviceOrderId and aa.corpid=$P{Corporate ID} and aa.status is true group by servicepartner_max.carrierNumber)
	LEFT OUTER JOIN `app_user` app_user on app_user.username=$P{User Name} and app_user.corpid=$P{Corporate ID}
	LEFT OUTER JOIN `company` company on company.corpid=customerfile.corpid and company.corpid=$P{Corporate ID}
	LEFT OUTER JOIN `partnerpublic` partnerpublic on partnerpublic.partnercode=trackingstatus.destinationAgentCode and partnerpublic.corpid in ('TSFT',$P{Corporate ID})
	LEFT OUTER JOIN `companydivision` companydivision ON serviceorder.`companydivision` = companydivision.`companyCode` and companydivision.`corpID` = $P{Corporate ID}
where customerfile.corpid=$P{Corporate ID}
and serviceorder.shipNumber=$P{Service Order Number}]]></queryString>

	<field name="ConfirmShipBookClient.jrxml" class="java.lang.String"/>
	<field name="curr_date" class="java.lang.String"/>
	<field name="customerfileName" class="java.lang.String"/>
	<field name="customerfile_RankPrefixLastname" class="java.lang.String"/>
	<field name="serviceorder_destinationAddressLine1" class="java.lang.String"/>
	<field name="serviceorder_destinationAddressLine2" class="java.lang.String"/>
	<field name="serviceorder_destinationAddressLine3" class="java.lang.String"/>
	<field name="serviceorder_destinationCity" class="java.lang.String"/>
	<field name="serviceorder_destinationZip" class="java.lang.String"/>
	<field name="serviceorder_destinationCountry" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="company_companyName" class="java.lang.String"/>
	<field name="appUser_firstNameLastName" class="java.lang.String"/>
	<field name="app_user_userTitle" class="java.lang.String"/>
	<field name="servicepartner_cntnrNumber" class="java.lang.String"/>
	<field name="servicepartner_etArrival" class="java.lang.String"/>
	<field name="servicepartner_max_carrierArrival" class="java.lang.String"/>
	<field name="servicepartner_atDepart" class="java.lang.String"/>
	<field name="servicepartner_carrierName" class="java.lang.String"/>
	<field name="servicepartner_carrierVessels" class="java.lang.String"/>
	<field name="partnerpublicFirstNameLastName" class="java.lang.String"/>
	<field name="partnerpublic_mailingEmail" class="java.lang.String"/>
	<field name="partnerpublic_mailingFax" class="java.lang.String"/>
	<field name="partnerpublic_mailingPhone" class="java.lang.String"/>
	<field name="companydivision_description" class="java.lang.String"/>

		<background>
			<band height="841"  isSplitAllowed="true" >
				<image  scaleImage="FillFrame" vAlign="Middle" hAlign="Center" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="9"
						width="595"
						height="832"
						key="image-4"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{Send Option}.equals("Print") ? "../../images/blank.png " :($F{serviceorder_companyDivision}.equalsIgnoreCase("Bournes") ?($F{serviceorder_job}.equalsIgnoreCase("DOM") ? "../../images/Bournes-Home-Moves-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("EUR") ? "../../images/Bournes-International-Moves-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("INT") ? "../../images/Bournes-International-Moves-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("LOG") ? "../../images/Bournes-Business-Moves-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("OFF") ? "../../images/Bournes-Business-Moves-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("RLO") ? "../../images/Bournes-Relocation-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("STO") ? "../../images/Bournes-Generic-Letterhead.jpg" : $F{serviceorder_job}.equalsIgnoreCase("3RD") ? "../../images/Bournes-International-Moves-Letterhead.jpg" :""): "")]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="352"
						y="78"
						width="95"
						height="12"
						forecolor="#78787A"
						key="textField-13">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("DOM")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["home@bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["mailto:home@bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="465"
						y="78"
						width="114"
						height="12"
						forecolor="#78787A"
						key="textField-14">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("DOM")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["www.bournesmoves.com/home"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["www.bournesmoves.com/home"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="312"
						y="77"
						width="120"
						height="12"
						forecolor="#78787A"
						key="textField-15">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("EUR") || $F{serviceorder_job}.equals("INT")||$F{serviceorder_job}.equals("3RD")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["international@bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["mailto:international@bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="445"
						y="76"
						width="140"
						height="12"
						forecolor="#78787A"
						key="textField-16">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("EUR") || $F{serviceorder_job}.equals("INT")||$F{serviceorder_job}.equals("3RD")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["www.bournesmoves.com/international"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["www.bournesmoves.com/international"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="333"
						y="75"
						width="108"
						height="12"
						forecolor="#78787A"
						key="textField-17">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("LOG") || $F{serviceorder_job}.equals("OFF")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["business@bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["mailto:business@bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="454"
						y="74"
						width="131"
						height="12"
						forecolor="#78787A"
						key="textField-18">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("LOG") || $F{serviceorder_job}.equals("OFF")))]]></printWhenExpression>
						</reportElement>
					<box leftPadding="1"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["www.bournesmoves.com/business"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["www.bournesmoves.com/business"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="452"
						y="67"
						width="130"
						height="12"
						forecolor="#6D6E71"
						key="textField-19">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("RLO")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["www.bournesmoves.com/relocation"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["www.bournesmoves.com/relocation"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="327"
						y="67"
						width="108"
						height="12"
						forecolor="#6D6E71"
						key="textField-20">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("RLO")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["relocation@bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["mailto:relocation@bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="371"
						y="79"
						width="95"
						height="12"
						forecolor="#78787A"
						key="textField-21">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("STO")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["info@bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["mailto:info@bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="Reference"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="483"
						y="79"
						width="100"
						height="12"
						forecolor="#78787A"
						key="textField-22">
							<printWhenExpression><![CDATA[new Boolean(($P{Send Option}.equals("Email")) & ($F{serviceorder_companyDivision}.equals("Bournes")) & ($F{serviceorder_job}.equals("STO")))]]></printWhenExpression>
						</reportElement>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="7" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["www.bournesmoves.com"]]></textFieldExpression>
						<hyperlinkReferenceExpression><![CDATA["www.bournesmoves.com"]]></hyperlinkReferenceExpression>
				</textField>
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="764"  isSplitAllowed="true" >
				<image  scaleImage="FillFrame" vAlign="Middle" hAlign="Center" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="48"
						y="19"
						width="145"
						height="45"
						key="image-1"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{Send Option}.equals("Print") ? "../../images/blank.png ":($F{serviceorder_companyDivision}.equals("Bournes") ? "" : "")]]></imageExpression>
				</image>
				<image  scaleImage="FillFrame" vAlign="Middle" hAlign="Center" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="436"
						y="12"
						width="145"
						height="45"
						key="image-2"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{Send Option}.equals("Print") ? "../../images/blank.png ": ($F{serviceorder_companyDivision}.equals("Turks") ? "../../images/Logo_turks.jpg" : "")]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="106"
						width="534"
						height="14"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfileName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="120"
						width="280"
						height="90"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(	($F{serviceorder_destinationAddressLine1}==null || $F{serviceorder_destinationAddressLine1}.equals("") ? "" : ($F{serviceorder_destinationAddressLine1})+"\n")+
	($F{serviceorder_destinationAddressLine2}==null || $F{serviceorder_destinationAddressLine2}.equals("") ? "" : ($F{serviceorder_destinationAddressLine2})+"\n")+
	($F{serviceorder_destinationAddressLine3}==null || $F{serviceorder_destinationAddressLine3}.equals("") ? "" : ($F{serviceorder_destinationAddressLine3})+"\n")+
	($F{serviceorder_destinationCity}==null || $F{serviceorder_destinationCity}.equals("") ? "" : ($F{serviceorder_destinationCity})+"\n")+
	($F{serviceorder_destinationZip}==null || $F{serviceorder_destinationZip}.equals("") ? "" : ($F{serviceorder_destinationZip})+"\n")+
	($F{serviceorder_destinationCountry}==null || $F{serviceorder_destinationCountry}.equals("") ? "" :$F{serviceorder_destinationCountry})
)]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="47"
						y="256"
						width="535"
						height="39"
						key="staticText-1"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[We are pleased to advise that your personal/household effects have now been shipped.  The details are as follows:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="48"
						y="213"
						width="279"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="238"
						width="534"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_RankPrefixLastname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="EEEEE dd MMMMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="391"
						y="213"
						width="191"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{curr_date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="47"
						y="298"
						width="96"
						height="18"
						key="staticText-3"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipping line]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="47"
						y="316"
						width="96"
						height="18"
						key="staticText-4"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Container No]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="47"
						y="334"
						width="96"
						height="18"
						key="staticText-5"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Vessel]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="48"
						y="352"
						width="95"
						height="18"
						key="staticText-6"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Sailing Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="48"
						y="370"
						width="95"
						height="18"
						key="staticText-7"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Due to Dock]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="298"
						width="280"
						height="18"
						key="textField-4"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_carrierName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="316"
						width="280"
						height="18"
						key="textField-5"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_cntnrNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="334"
						width="280"
						height="18"
						key="textField-6"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_carrierVessels}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="EEEEE dd MMMMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="352"
						width="280"
						height="18"
						key="textField-7"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_atDepart}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="EEEEE dd MMMMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="370"
						width="280"
						height="18"
						key="textField-8"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_etArrival}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="47"
						y="409"
						width="535"
						height="58"
						key="staticText-2"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Please contact the destination agent prior to the arrival of the ship in order to arrange for delivery and customs clearance.  You should be aware that this contact is very important as failure to do so may result in substantial additional costs being incurred.  Our agents who will do everything to assist you are:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="573"
						width="535"
						height="66"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{company_companyName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="648"
						width="534"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{appUser_firstNameLastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="666"
						width="534"
						height="18"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{app_user_userTitle}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="470"
						width="534"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{partnerpublicFirstNameLastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="510"
						width="438"
						height="18"
						key="textField-9"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{partnerpublic_mailingPhone}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="47"
						y="488"
						width="534"
						height="18"
						key="textField-10"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="546"
						width="438"
						height="18"
						key="textField-11"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{partnerpublic_mailingEmail}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="528"
						width="438"
						height="18"
						key="textField-12"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{partnerpublic_mailingFax}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="47"
						y="510"
						width="96"
						height="18"
						key="staticText-8"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Telephone]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="48"
						y="528"
						width="95"
						height="18"
						key="staticText-9"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Fax]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="48"
						y="546"
						width="95"
						height="18"
						key="staticText-10"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Email]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="48"
						y="684"
						width="533"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{companydivision_description}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="48"
						y="388"
						width="95"
						height="18"
						key="staticText-11"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[POE]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="143"
						y="388"
						width="280"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{servicepartner_max_carrierArrival}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="72"  isSplitAllowed="true" >
				<image  scaleImage="RetainShape" vAlign="Middle" hAlign="Center" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="7"
						width="595"
						height="65"
						key="image-3"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{Send Option}.equals("Print") ? "../../images/blank.png ":($F{serviceorder_companyDivision}.equals("Turks") ? "../../images/TurkFooter.jpg" : "")]]></imageExpression>
				</image>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
