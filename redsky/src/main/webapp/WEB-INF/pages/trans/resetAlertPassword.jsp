<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%> 
<%@ page import="org.acegisecurity.context.SecurityContextHolder" %>
 <%@ page import="org.appfuse.model.User" %>
<%@ page import="org.acegisecurity.Authentication" %>
 <%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User quickUser = (User) auth.getPrincipal();
String quickCorpId = quickUser.getCorpID();
String quickUserType = quickUser.getUserType();
Boolean newsUpdateFlag=quickUser.isNewsUpdateFlag();
String quickFileCabinetView = quickUser.getFileCabinetView();
%>
<head>
    <title><fmt:message key="changeLoginPassword.title"/></title>
    <meta name="heading" content="<fmt:message key='changeLoginPassword.heading'/>"/>
    <meta name="menu" content="Login"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />    
    <script language="javascript" type="text/javascript">
       function clear_fileds(){
    var i;
			for(i=0;i<=3;i++)
 	{
       	document.forms['loginForm'].elements['passwordNew'].value = "";
    	document.forms['loginForm'].elements['confirmPasswordNew'].value = "";
    	
    }
    }
    </script>
<script>
function validateFields(){
	var passwordNew = document.forms['loginForm'].elements['passwordNew'].value;
	var confirmPasswordNew = document.forms['loginForm'].elements['confirmPasswordNew'].value;
	//
	var chkvalflagReq="0";
		var sysPaswdPolicy='${systemDefault.passwordPolicy}';
		var userType= '<%=quickUserType%>';
		//alert(userType);
		//alert(sysPaswdPolicy);
		if((sysPaswdPolicy!=null && sysPaswdPolicy=='true') && userType!=null && userType=='USER'){
			chkvalflagReq="1";
		}  		
		//var pwd= document.forms['userForm'].elements['user.password'].value;userType
	//var regx=/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/;
	//var regx=/^(?=.*[A-Z])(?=.*[+/*-<>/?!@#$()]).{6,}$/;
	var regx=/^(?=.*[A-Z]).{6,}$/;
	var charchk="+/*-<>/?!@#$()";
	var flag=false;
	//alert((regx.test(pwd)));
	if(regx.test(passwordNew)){
		for(var i=0;i<charchk.length;i++){
		if(passwordNew.indexOf(charchk.charAt(i))>=0){
			//alert("true")
			flag=true;
			break;
		}
		}
	}
	var msgval="The entered password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
	//
	if((chkvalflagReq=='1') && passwordNew!=null && passwordNew!='' && !(flag))
  		{
  			alert(msgval);
  			document.forms['loginForm'].elements['passwordNew'].focus();
  			return false;
  		}
	var flag1=false;
	//alert((regx.test(confirmPasswordNew)));
	if(regx.test(confirmPasswordNew)){
		for(var i=0;i<charchk.length;i++){
		if(confirmPasswordNew.indexOf(charchk.charAt(i))>=0){
			//alert("true")
			flag1=true;
			break;
		}
		}
	}
	 msgval="The entered confirm password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
	  		if((chkvalflagReq=='1')&& confirmPasswordNew!=null && confirmPasswordNew!='' && !(flag1))
	  		{
	  			alert(msgval);
	  			document.forms['loginForm'].elements['confirmPasswordNew'].focus();
	  			return false;
	  		}
	
	if(passwordNew=='' || confirmPasswordNew==''){
	alert("Fill all Mandatory Fields");
		return false;
	}
	
	if(passwordNew != confirmPasswordNew){
		alert('Password and Confirm Password should be the same.');
		document.forms['loginForm'].elements['confirmPasswordNew'].value = '';
		return false;
	}
		else{
		return true;
	}
}

</script>
 <style>
 body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}
 #logincontainer{ width:925px; position:absolute; height:417px; background-image:url(images/loginbg-opt2.png); background-repeat:no-repeat; margin-top:80px;}
 #errormessage {width:915px; padding-left:20px;}
 #leftcolumn {float:left;margin-top:50px;top:150px;width:440px;}

 #rightcolumn { width:430px; float:left; margin-top:25px;}
		
 div#branding {float: left; width: 100%; margin: 0; text-align:left; height:0px; } 
        	
div#header {
margin:0;
text-align:left;
display:none;
}
div.error, span.error, li.error, div.message {
background:#FFFFCC none repeat scroll 0 0;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:normal;
margin:10px auto;
padding:3px;
text-align:left;
vertical-align:bottom;
width:500px;
position:absolute;
top:45px;
left:200px;
}
</style>
</head>
<body id="login"/>
<form method="post" id="loginForm" name="loginForm" action="submitResetAlertPassword.html?decorator=popup&popup=true">
   <c:set var="userType" value="<%=quickUserType%>"/>  
   <div id="wrapper">

  <table  style="border:1px dotted #CFCFCF;" cellspacing="0" cellpadding="0">
      <tr>
        <td height="160" valign="top" align="left">
        <table width="100%" border="0" style="font:normal 12px arial,verdana; color:#FFFFFF;" cellspacing="0" cellpadding="4">
          <tr align="center">
            <td colspan="2">&nbsp;</td>
            </tr>
          
         <tr>
            <td width="172" height="30" align="right" class="content"><label for="j_password" class="required desc">
            	<fmt:message key="label.passwordNew"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><s:password name="passwordNew" required="true" cssClass="input-text" onchange="passwordChanged(this)"/></td>
          </tr>
          <tr>
            <td width="" height="30" align="right" class="content"><label for="j_confirmPassword" class="required desc">
            	<fmt:message key="label.confirmPassword"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><s:password name="confirmPasswordNew"  required="true" cssClass="input-text" onchange="passwordChanged(this)" /></td>
          </tr>
       
		 <tr>
	       <td>&nbsp;</td>
          <td><input type="submit" class="cssbutton" name="submit"  onclick="return validateFields();" value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" class="cssbutton" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
          </tr>
           <tr>
         <c:choose>
			 		  			<c:when test="${systemDefault.passwordPolicy=='true' && userType=='USER'}">	  			
			 		  			
			 		  			<div id="pswdpolicyId">			 		  			
			 		    		
			 		    		<td>&nbsp;</td>
			 		  			<td align="left"><a onmouseout="ajax_hideTooltip();" onmouseover="ajax_showTooltip('findtoolTipForPswd.html?ajax=1&decorator=simple&popup=true',this)"><font style="font-weight:bold;">Password&nbsp;Policy</font></a></font></td>			 		  			
			 		  			
			 		  			
			 		  			</div>
			 		  				  			
			 		  			
			 		  			</c:when>			 		  			
			 		  			<c:otherwise>			 		  			
			 		  			</c:otherwise>		  			
			 		  		</c:choose>
			 		  		</tr>
         </table></td>
        
      </tr>
    </table> 
  </div>

</form>
<script type="text/javascript">
 try{
 onPageLoads();
 }
 catch(e){}
    try{
    Form.focusFirstElement(document.forms["loginForm"]);
    }
    catch(e){}
    try{
    highlightFormElements();
}
catch(e){}
    function passwordChanged(passwordField) {
    try{
        if (passwordField.name == "user.password") {
            var origPassword = "<s:property value="user.passwordNew"/>";
        } else if (passwordField.name == "user.confirmPassword") {
            var origPassword = "<s:property value="user.confirmPasswordNew"/>";
        }
        
        if (passwordField.value != origPassword) {
            createFormElement("input", "hidden",  "encryptPass", "encryptPass",
                              "true", passwordField.form);
        }
    }
catch(e){}
}
try{
<c:if test="${hitFlag == 22}">
alert('New Password can not be same as old password, please select a new password!');
</c:if>
<c:if test="${hitFlag == 1}">
alert('Password has been reset successfully.');
window.close();
window.opener.location.reload();
</c:if>
}
catch(e){}
</script>
<%@ include file="/scripts/login.js"%>