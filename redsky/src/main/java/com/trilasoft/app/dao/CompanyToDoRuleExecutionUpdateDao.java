package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CompanyToDoRuleExecutionUpdate;

public interface CompanyToDoRuleExecutionUpdateDao extends GenericDao<CompanyToDoRuleExecutionUpdate, Long>  {
	
	public List<CompanyToDoRuleExecutionUpdate> findByCorpID(String corpId);
}
