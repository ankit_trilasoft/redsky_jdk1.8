package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DriverCommissionPlan;
import com.trilasoft.app.model.VanLine;

public interface DriverCommissionManager extends GenericManager<DriverCommissionPlan, Long> {
	
	public List getPlanList(String corpId);
	public List getcontractList(String corpId);
	public List findChargeList(String corpId);
	public List getChargeList(String charge,String description,String corpId);
	public List getPlanListValue(String planValue,String corpId);
	public List getChargeListValue(String planValue,String chargeValue,String corpId);
	public List getCommissionPlanList(String corpId);
	public List getChargeList(String corpId);
	public List getCommissionSearchPlanList(String plan,String charge,String description,String contract,String corpId);
    public void updateCommissionPercent(BigDecimal percent,String planAmount,String plan,String contract,String charge,String corpId);
    public int delDriverCommission(String delCommissionId);
    public int checkPlan(String plan,String charge,String contract,String corpId);
    public List getChargedriverList(String charge,String corpId);
}
