<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><style type="text/css">
</style>
 <script type="text/javascript">  
   
    function getEdit(tmp,des,branch,div,max,min,avlq,sp,op,ucost,eqmateid,category){   	
    	   	  	//alert(tmp);
    	   	  	//alert(eqmateid);
    		   	document.forms['gridForm'].elements['equipMaterialsLimits.id'].value=tmp;
            	document.forms['gridForm'].elements['equipMaterialsLimits.category'].value=category;
            	document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value=branch;
            	document.forms['gridForm'].elements['equipMaterialsLimits.division'].value=div;
            	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value=des;            	
            	document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value=min;  
            	document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value=max;
            	document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value=avlq;    		        	
        	    document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value=sp;    	
        	    document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value=op;
        	    document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value=ucost;
        	    document.forms['gridForm'].elements['equipMaterialsCost.equipMaterialsId'].value=tmp;
        	    document.forms['gridForm'].elements['equipMaterialsCost.id'].value=eqmateid;         	    
        	    document.forms['gridForm'].elements['equipMaterialsLimits.resource'].readOnly=true; 
        	    document.forms['gridForm'].elements['backUp'].value=tmp+"~"+des+"~"+branch+"~"+div+"~"+max+"~"+min+"~"+avlq+"~"+sp+"~"+op+"~"+ucost+"~"+eqmateid+"~"+category;         	   	
    	        document.getElementById('saveBox').style.display="block";
      	        document.getElementById('searchBox').style.display="none";
      	        document.forms['gridForm'].action="saveOnEdit.html";
      	      document.forms['gridForm'].elements['chkSearch'].value="1";
      	
    }
  
    addRow = function(){      
     	document.forms['gridForm'].elements['equipMaterialsLimits.category'].value="";
         document.forms['gridForm'].elements['equipMaterialsLimits.id'].value="";
     	document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value="";
     	document.forms['gridForm'].elements['equipMaterialsLimits.division'].value="";
     	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value="";           	
     	document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value=""; 
     	document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value="";
     	document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value="";
     	document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value="";    	
    	document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value="";
    	document.forms['gridForm'].elements['equipMaterialsCost.equipMaterialsId'].value="";
    	document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value="";
    	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].readOnly=false;
    	document.forms['gridForm'].elements['backUp'].value=""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+""+"~"+"";
    	document.forms['gridForm'].action="saveEquipMaterialLimit.html";
    	document.getElementById('saveBox').style.display="block";
    	document.getElementById('searchBox111').style.display="none";
    	document.forms['gridForm'].elements['chkSearch'].value="1";
       }
    function confirmSubmit(targetElement){
    	var agree=confirm("Are you sure you wish to remove from inventory list?");
    	var did = targetElement;
    	if (agree){
    		location.href="deleteInvResource.html?id="+did+"";
    	}
    	else{
    		return false;
    	}
    }
 
</script>
  
	<div id="newmnav">
         <ul>
         <c:choose>
          <c:when test="${ship=='MS' }">          
           <li><a href="itemsJbkEquipExtraMasterInfo.html"><span>Material&nbsp;And&nbsp;Equipment&nbsp;List</span></a></li>
           <li id="newmnav1"><a class="current"><span>Material And Equipment Request</span></a></li>
           <li>
          <a href="miscellaneousRequestReturn.html?itemType=${itemType}&ship=${ship}"><span>Material And Equipment Return</span></a></li>
          <%-- <a href="itemsJbkEquipCustomerSales.html?ship=CS&itemType=${itemType}"><span>Material And Equipment Customer Sales</span></a> --%>
          </c:when>
          <c:otherwise>
          <li id="newmnav1"><a class="current"><span>Material&nbsp;And&nbsp;Equipment&nbsp;List</span></a></li>
            <li>
          <a href="miscellaneousRequest.html?itemType=${itemType}&ship=${ship}"><span>Material And Equipment Request</span></a></li>
          <li>
          <a href="miscellaneousRequestReturn.html?itemType=${itemType}&ship=${ship}"><span>Material And Equipment Return</span></a></li>
          </c:otherwise>
          </c:choose>         
         </ul>
       </div>
       <div class="spnblk">&nbsp;</div>   
    <display:table name="materialsList" class="table" requestURI="" id="materialsListId" export="true"  pagesize="10" style="width:100%;!margin-top:-2px;">   
  <display:column sortable="true" style="width:20px" titleKey="itemsJbkEquip.id" ><a href="#" onclick="getEdit('${materialsListId.id}','${materialsListId.descript}','${materialsListId.branch}','${materialsListId.division}','${materialsListId.maxResourceLimit}','${materialsListId.minLevel}','${materialsListId.freeQty}','${materialsListId.salesPrice}','${materialsListId.ooprice}','${materialsListId.cost}','${materialsListId.equipMaterialsId}','${materialsListId.type}')"><c:out value="${materialsListId.id}"/></a></display:column>
  <display:column sortable="true" style="width:150px" titleKey="itemsJbkEquip.descript" ><c:out value="${materialsListId.descript}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:10%;" titleKey="itemsJbkEquip.freeQty" ><c:out value="${materialsListId.freeQty}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:20px" titleKey="itemsJbkEquip.cost" ><c:out value="${materialsListId.cost}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:20px" titleKey="itemsJbkEquip.salesPrice" ><c:out value="${materialsListId.salesPrice}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:20px" titleKey="itemsJbkEquip.ooprice" ><c:out value="${materialsListId.ooprice}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:20px" titleKey="itemsJbkEquip.minLevel" ><c:out value="${materialsListId.minLevel}"/></a></display:column>
  <display:column sortable="true" style="text-align: right;width:20px" titleKey="itemsJbkEquip.maxResourceLimit" ><c:out value="${materialsListId.maxResourceLimit}"/></a></display:column>
   <display:column sortable="true" style="text-align: left;width:20px" title="Warehouse" ><c:out value="${materialsListId.branch}"/></a></display:column>
    <display:column sortable="true" style="text-align: left;width:20px" title="Division" ><c:out value="${materialsListId.division}"/></a></display:column>
   <display:column title="Remove" style="width:25px;">
	<a><img align="middle" onclick="confirmSubmit(${materialsListId.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
   </display:table> 
<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;">
</table>
<div id="controls">
<table cellpadding="4" cellspacing="2" style="margin:0px;padding:0px;"><tr><td>
 <td>
 <div id="adddiv">
 <a href="#saveBox" >
<s:submit cssClass="cssbutton1" key="button.add" method="delete" onclick="addRow()" />
</a>
 
 </div></td>
 <td>
 </td></tr></table>
</div>
<br/>
<script type="text/javascript">
if(document.forms['gridForm'].elements['showAdd'].value=='Yes')
{
document.getElementById("adddiv").style.display="block";
}
 if(document.forms['gridForm'].elements['showAdd'].value=='No')
{
document.getElementById("adddiv").style.display="none";
}
</script>