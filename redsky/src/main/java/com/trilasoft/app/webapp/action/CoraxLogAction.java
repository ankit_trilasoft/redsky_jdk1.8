package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.service.CoraxLogManager;

public class CoraxLogAction extends BaseAction implements Preparable{
	
	private String sessionCorpID;
	private User user;
	private List coraxLogLists;
	private CoraxLogManager coraxLogManager;
	
	public void prepare() throws Exception {

	}
	
	 static final Logger logger = Logger.getLogger(CoraxLogAction.class);
		
	    
		public CoraxLogAction() {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			user = (User) auth.getPrincipal(); 
			this.sessionCorpID = user.getCorpID(); 
			
		}

		
		public String list(){ 
			coraxLogLists = coraxLogManager.getAll();
	    	return SUCCESS;   
	    }
		
		public String getSessionCorpID() {
			return sessionCorpID;
		}


		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}


		public User getUser() {
			return user;
		}


		public void setUser(User user) {
			this.user = user;
		}
		public List getCoraxLogLists() {
			return coraxLogLists;
		}


		public void setCoraxLogList(List coraxLogLists) {
			this.coraxLogLists = coraxLogLists;
		}


		public CoraxLogManager getCoraxLogManager() {
			return coraxLogManager;
		}


		public void setCoraxLogManager(CoraxLogManager coraxLogManager) {
			this.coraxLogManager = coraxLogManager;
		}

}
