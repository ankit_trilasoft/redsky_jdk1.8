delimiter $$

CREATE trigger redsky.trigger_add_history_corp_comp_permission
BEFORE UPDATE on redsky.corp_comp_permission
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";








   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'corp_comp_permission.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;




   IF (OLD.mask <> NEW.mask ) THEN
      select CONCAT(fieldNameList,'corp_comp_permission.mask~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mask,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mask,'~') into newValueList;
  END IF;






   IF (OLD.corp_id <> NEW.corp_id ) THEN
      select CONCAT(fieldNameList,'corp_comp_permission.corp_id~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corp_id,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corp_id,'~') into newValueList;
  END IF;




   IF (OLD.componentId <> NEW.componentId ) THEN
      select CONCAT(fieldNameList,'corp_comp_permission.componentId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.componentId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.componentId,'~') into newValueList;
  END IF;



 


  CALL add_tblHistory (OLD.id,"corp_comp_permission", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corp_ID, now());

END $$


delimiter;