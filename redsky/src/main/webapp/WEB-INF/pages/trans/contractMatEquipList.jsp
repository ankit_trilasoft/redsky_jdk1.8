<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
<title>Resource List</title>
<meta name="heading" content="Resource List"/> 
<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:5px;
	margin-top:-21px;
	!margin-top:-23px;
	padding:2px 0px;
	text-align:right;
	width:100%;
}
    form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}


</style>

</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="serviceForm10" name="serviceForm10" action="searchItemEquipList" method="post">
<div id="Layer1" style="width: 100%">

<s:hidden name="id" value="<%=request.getParameter("id")%>"/> 

<s:hidden name="contracts.contract" />
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>


<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="itemsJEquip.type"/></th>
<th><fmt:message key="itemsJEquip.descript"/></th>
<th><fmt:message key="itemsJEquip.controlled"/></th>
<th style="border-left:hidden;"></th>
</tr></thead>	
		<tbody>
		<tr>
			<td><s:select cssClass="list-menu" name="cType" list="%{resourceCategory}" cssStyle="width:240px" /></td>
			<td><s:textfield name="cDescript" size="35" required="true" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			<td><s:checkbox name="cControlled" /></td>
			<td width="230px" style="border-left:hidden;" align="right">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px; " onclick="location.href='<c:url value="/addMatEquip.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Resource List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<s:set name="itemList" value="itemList" scope="session"/>
   
<display:table name="itemList" class="table" requestURI="" id="itemssList" export="true" pagesize="50" >   
    <display:column sortProperty="type" sortable="true" titleKey="itemsJEquip.type">
	   <a onclick="editResourceMethod(${itemssList.id});" ><c:out value="${itemssList.typeDescript}" /></a>
    </display:column>
    <display:column property="descript" sortable="true" titleKey="itemsJEquip.descript"/>
    <display:column  titleKey="itemsJEquip.controlled"  style="text-align: left" >
	    <c:if test="${itemssList.controlled=='true'}">	    
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${itemssList.controlled=='false'}">	
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column>   
  <display:column title="Remove" style="width: 15px;">
  <a>
	<img  align="middle" title="" onclick="confirmSubmit('${itemssList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
 </a>
    </display:column>
    <display:setProperty name="paging.banner.item_name" value="itemsJEquip"/>  
    <display:setProperty name="paging.banner.items_name" value="itemsJEquip"/>
  
    <display:setProperty name="export.excel.filename" value="ItemsJEquip List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ItemsJEquip List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ItemsJEquip List.pdf"/>   
</display:table>   
  
 <c:out value="${buttons}" escapeXml="false" />

 </div>
 </s:form>

<script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['serviceForm10'].elements['cDescript'].value = '';
		document.forms['serviceForm10'].elements['cType'].value = '';
		document.forms['serviceForm10'].elements['cControlled'].checked = false;
	}
	function editResourceMethod(targetElement2){
		if(targetElement2=='none')
		{
			location.href='addMatEquip.html';
		}
		else
		{
			location.href='addMatEquip.html?id='+targetElement2;
		}
	}
	function confirmSubmit(targetElement2){
		var agree=confirm("Please confirm that you want to delete this Category Type permanently?");
		var id=targetElement2;
		if (agree){
	        window.location="deleteMatEquip.html?id="+id;
		}
	} 
</script>  

<script type="text/javascript">   
    highlightTableRows("itemList");
</script>  
