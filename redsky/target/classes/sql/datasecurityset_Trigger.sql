delimiter $$

CREATE trigger redsky.trigger_add_history_datasecurityset
BEFORE UPDATE on redsky.datasecurityset
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.name <> NEW.name ) THEN
      select CONCAT(fieldNameList,'datasecurityset.name~') into fieldNameList;
      select CONCAT(oldValueList,OLD.name,'~') into oldValueList;
      select CONCAT(newValueList,NEW.name,'~') into newValueList;
  END IF;




  IF (OLD.corpid <> NEW.corpid ) THEN
      select CONCAT(fieldNameList,'datasecurityset.corpid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
  END IF;




   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'datasecurityset.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;


  CALL add_tblHistory (OLD.id,"datasecurityset", fieldNameList, oldValueList, newValueList, NEW.updatedBy, OLD.corpID, now());

END $$


delimiter;