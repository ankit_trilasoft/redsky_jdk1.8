package com.trilasoft.app.dao.hibernate;

import java.util.List;   

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.model.Person;   
import com.trilasoft.app.dao.PersonDao;   
  
public class PersonDaoHibernate extends GenericDaoHibernate<Person, Long> implements PersonDao {   
  
    public PersonDaoHibernate() {   
        super(Person.class);   
    }   
  
    public List<Person> findByLastName(String lastName) {   
        return getHibernateTemplate().find("from Person where lastName=?", lastName);   
    }   
}  
