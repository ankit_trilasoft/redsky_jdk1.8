package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.trilasoft.app.dao.UrlPatternDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.UrlPattern;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

public class UrlPatternDaoHibernate extends GenericDaoHibernate<UrlPattern, Long>  implements UrlPatternDao {
	
	public UrlPatternDaoHibernate() {
		super(UrlPattern.class);
		// TODO Auto-generated constructor stub
	}

	private HibernateUtil hibernateUtil;

	

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}


	public List<UrlPattern> listUrlPatterns() {
		List urlPatternData = this.getSession()
				.createSQLQuery(
						"SELECT concat('/', m.corpID,  (REPLACE(REPLACE(REPLACE(m.url,'\r\n',''), ' ', ''), ',', CONCAT(',/', m.corpID) ))) as url, group_concat(mp.recipient, ',') as roles FROM menu_item m, menu_item_permission mp where "
								+ "m.id = mp.menuitemid and mp.permission >=1 and m.url is not null GROUP BY m.corpID, m.url "
								+ "UNION "
								+ "SELECT concat('/', rc.corpID,  (REPLACE(REPLACE(REPLACE(rc.url,'\r\n',''), ' ', ''), ',', CONCAT(',/', rc.corpID) ))), group_concat(rc.role, ',') FROM rolebased_comp_permission rc "
								+ "where url is not null GROUP BY rc.corpID, rc.url order by 1").
								addScalar("url",Hibernate.STRING).addScalar("roles",Hibernate.STRING).list();

		List<UrlPattern> urlPatternList = new ArrayList<UrlPattern>();
		Iterator it = urlPatternData.iterator();
		while (it.hasNext()) {
			Object[] obj = (Object[]) it.next();
			UrlPattern urlPattern = new UrlPattern();
			urlPattern.setUrlPattern((String)obj[0]);
			List<Role> roles = new ArrayList<Role>();
			String[] rolesStrArr = ((String) obj[1]).split(",");
			for (String roleStr : rolesStrArr) {
				if (!roleStr.equals("") && roleStr.startsWith("ROLE_")) {
					Role role = new Role(roleStr);
					roles.add(role);
				}
			}
			urlPattern.setRoles(roles);
			urlPatternList.add(urlPattern);
		}

		return urlPatternList;
	}

}
