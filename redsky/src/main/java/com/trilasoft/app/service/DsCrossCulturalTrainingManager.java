package com.trilasoft.app.service;


import java.util.List;

import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.DsCrossCulturalTraining;

public interface DsCrossCulturalTrainingManager extends GenericManager<DsCrossCulturalTraining, Long>{
	List getListById(Long id);
}
