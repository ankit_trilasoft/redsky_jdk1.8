<% if (request.getAttribute("struts.valueStack") != null) { %>
<%-- ActionError Messages - usually set in Actions --%>
<s:if test="hasActionErrors()">
    <div class="error" id="errorMessages">    
      <s:iterator value="actionErrors">
        <img src="<c:url value="/images/iconWarning.gif"/>"
            alt="<fmt:message key="icon.warning"/>" class="icon" />
        <s:property escape="false"/><br />
      </s:iterator>
   </div>
</s:if>
<script language="JavaScript" type="text/javascript">
	var errorMessage="";
</script>
<%-- FieldError Messages - usually set by validation rules --%>
<s:if test="hasFieldErrors()">
    <div class="error" id="errorMessages">    
      <s:iterator value="fieldErrors">
          <s:iterator value="value">
            <img src="<c:url value="/images/iconWarning.gif"/>" alt="<fmt:message key="icon.warning"/>" class="icon" />
             <s:property escape="false"/><br />
				<script language="JavaScript" type="text/javascript">
					errorMessage = errorMessage + '<s:property escape="false"/>\n';
				</script>
             <c:set var="test" value="<s:property escape='false'>" />
          </s:iterator>
      </s:iterator>
      <script language="JavaScript" type="text/javascript">
		alert(errorMessage);
	</script>
   </div>
   
</s:if>

<% } %>

<%-- Success Messages --%>
<c:if test="${not empty messages}">
    <div class="message" id="successMessages">    
        <c:forEach var="msg" items="${messages}">
            <img src="<c:url value="/images/iconInformation.gif"/>"
                alt="<fmt:message key="icon.information"/>" class="icon" />
            <c:out value="${msg}" escapeXml="false"/><br />
        </c:forEach>
    </div>
    <c:remove var="messages" scope="session"/>
</c:if>

<%--Success Messages for Resource Transfer--%>
<div class="message" id="successMsg" style="display: none;">    
    <img src="<c:url value="images/iconInformation.gif"/>"
        alt="<fmt:message key="icon.information"/>" class="icon" />
    <c:out value="Estimated Resources transferred Successfully" escapeXml="false"/><br />
</div>

<%-- Error Messages --%>
<c:if test="${not empty errorMessages}">
    <div class="error" id="errorMessages">     
        <c:forEach var="errorMsg" items="${errorMessages}">
            <img src="<c:url value="/images/iconWarning.gif"/>"
                alt="<fmt:message key="icon.warning"/>" class="icon" />
            <c:out value="${errorMsg}" escapeXml="false"/><br />
        </c:forEach>
    </div>
    <c:remove var="errorMessages" scope="session"/>
</c:if>