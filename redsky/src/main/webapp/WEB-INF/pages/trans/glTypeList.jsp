<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="glCommissionTypeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='glCommissionList.heading'/>"/> 
<script>
 function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){  
	     location.href = "deleteGLCommissionType.html?glDeleteButton=yes&id="+encodeURI(targetElement)+"&glId=${glId}&contract=${contract}&charge=${charge}&contractId=${contractId}"
     }else{
		return false;
	}
}
</script>     
</head>  

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;"  
        onclick="location.href='<c:url value="/editGLCommissionType.html?glId=${glId}&contract=${contract}&charge=${charge}&contractId=${contractId}"/>'"  
        value="<fmt:message key="button.add"/>" />   
</c:set>  
<s:form id="searchForm" action="searchVanLine" method="post" validate="true" >  
<s:hidden name="glDeleteButton" value="<%=request.getParameter("glDeleteButton")%>"/> 
<c:set var="FormDateValue" value="<%=request.getParameter("glDeleteButton")%>"/>
<div id="Layer1" style="width:100%" >
<div id="newmnav">
		  <ul>
		  	<li><a href="contracts.html"><span>Contract List</span></a></li>
		  	<li><a href="editCharges.html?id=${charges.id}&contractId=${contractId}"><span>Charge&nbsp;Detail</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>GL Type List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
		</div><div class="spn">&nbsp;</div>


 
<s:set name="gLCommissionTypeList" value="gLCommissionTypeList" scope="request"/>


<display:table name="gLCommissionTypeList" class="table" requestURI="" id="gLCommissionTypeList" style="width:100%;margin-top:2px;!margin-top:5px; " defaultsort="1" pagesize="10"  >   
		  <display:column sortable="true" property="id"  title="ID" style="width:60px" url="/editGLCommissionType.html?glId=${glId}&contractId=${contractId}" paramId="id" paramProperty="id"/>
		 <display:column property="contract" sortable="true" titleKey="gLCommissionType.contract"  style="width:80px"/> 
		 <display:column property="charge" sortable="true" titleKey="gLCommissionType.charge"   style="width:20px"/> 
		 <display:column property="glType" sortable="true" titleKey="gLCommissionType.glType" style="width:70px" />
		 <display:column property="type" sortable="true" titleKey="gLCommissionType.type"   style="width:60px"/>
	     <display:column property="code" sortable="true"  titleKey="gLCommissionType.code" style="width:60px"/>
	      <display:column title="Remove" style="width:45px; text-align: center;">
				    <a><img align="middle" onclick="confirmSubmit(${gLCommissionTypeList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
				    </display:column> 
</display:table>


</div>
<c:if test="${glDeleteButton=='yes'}">
	<c:redirect url="/glCommissionTypeList.html?contract=${charges.contract}&charge=${charges.charge}&glId=${charges.id}&contractId=${contractId}"/>
</c:if> 
</s:form>


<c:out value="${buttons}" escapeXml="false" /> 

<!-- 
    
    <s:hidden name="buttonType" />
    <c:out value="${buttons}" escapeXml="false" />
-->

<script type="text/javascript">  
<c:if test="${hitFlag=='5'}">
<c:redirect url="/glCommissionTypeList.html?contract=${charges.contract}&charge=${charges.charge}&glId=${charges.id}&contractId=${contractId}"/>
</c:if> 
</script>  
		  	