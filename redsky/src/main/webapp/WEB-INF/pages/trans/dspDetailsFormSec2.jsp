<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<c:set var="tenancyManagementFlag" value="false"/>
<configByCorp:fieldVisibility componentId="component.dspdetails.tenancyManagement">
<c:set var="tenancyManagementFlag" value="true"/>
<s:hidden name="tenancyManagementFlag" value="true" />
</configByCorp:fieldVisibility>
	<!-- start aio -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.AIO_vendorCode)>-1 && dspDetails.AIO_vendorCode !='' && dspDetails.AIO_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('aio')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='AIO'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="aio">
  	   <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),chkIsVendorRedSky('AIO_'),changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.AIO_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','AIO_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.AIO_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceStartDate"/></s:text>
			 <td><s:textfield id="AIO_serviceStartDate" cssClass="input-text" name="dspDetails.AIO_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceStartDate}">
		<td><s:textfield id="AIO_serviceStartDate" cssClass="input-text" name="dspDetails.AIO_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td> 
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
	<c:choose>
		<c:when test="${countDsRelocationAreaInfoOrientationNotes == '0' || countDsRelocationAreaInfoOrientationNotes == '' || countDsRelocationAreaInfoOrientationNotes == null}">
		     <td  align="right" style="width:115px;!width:190px;"><img id="countDsRelocationAreaInfoOrientationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);" ></a></td>
		</c:when>
		<c:otherwise> 
			<td  align="right" style="width:115px;!width:100px;"><img id="countDsRelocationAreaInfoOrientationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);" ></a></td>
		</c:otherwise>
	</c:choose> 
</c:if> 
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.AIO_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceEndDate"/></s:text>
			 <td><s:textfield id="AIO_serviceEndDate" cssClass="input-text" name="dspDetails.AIO_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceEndDate}">
		<td><s:textfield id="AIO_serviceEndDate" cssClass="input-text" name="dspDetails.AIO_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.AIO_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.AIO_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedAIO_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.AIO_displyOtherVendorCode}">
	 <c:set var="ischeckedAIO_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.AIO_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedAIO_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>   
<table cellspacing="0" cellpadding="3" border="0" class="detailTabLabel" style="margin-left:-20px">
<tbody>
<tr>
<td align="right"  width="250px"  class="listwhitetext">Flight Departure</td>
	    <c:if test="${not empty dspDetails.AIO_flightDeparture}">
			 <s:text id="customerFileFlightDeparture" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightDeparture"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-text" name="dspDetails.AIO_flightDeparture" value="%{customerFileFlightDeparture}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_flightDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightDeparture}">
		<td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-text" name="dspDetails.AIO_flightDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_flightDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext">Flight Arrival</td>
	    <c:if test="${not empty dspDetails.AIO_flightArrival}">
			 <s:text id="customerFileFlightArrival" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightArrival"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-text" name="dspDetails.AIO_flightArrival" value="%{customerFileFlightArrival}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_flightArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightArrival}">
		<td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-text" name="dspDetails.AIO_flightArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_flightArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext" >Airline Flight</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_airlineFlight" readonly="false" size="30" maxlength="25" onchange="changeStatus();"/></td>
</tr>
<tr>
<td align="right" valign="top" class="listwhitetext" >Temporary accommodation contact information</td>
<td align="left" class="listwhitetext" >
<s:textarea  rows="2" cssStyle="width:178px;" id="dspDetails.AIO_contactInformation" name="dspDetails.AIO_contactInformation"  cssClass="textarea" onchange="changeStatus();"/>	
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Days of service authorized # </td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_serviceAuthorized" readonly="false" size="10" maxlength="10" onchange="changeStatus();"/></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Initial date of service requested</td>
	    <c:if test="${not empty dspDetails.AIO_initialDateOfServiceRequested}">
			 <s:text id="customerFileInitialDateOfServiceRequested" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_initialDateOfServiceRequested"/></s:text>
			 <td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-text" name="dspDetails.AIO_initialDateOfServiceRequested" value="%{customerFileInitialDateOfServiceRequested}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_initialDateOfServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_initialDateOfServiceRequested}">
		<td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-text" name="dspDetails.AIO_initialDateOfServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_initialDateOfServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider notification sent</td>
	    <c:if test="${not empty dspDetails.AIO_providerNotificationSent}">
			 <s:text id="customerFileProviderNotificationSent" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerNotificationSent"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-text" name="dspDetails.AIO_providerNotificationSent" value="%{customerFileProviderNotificationSent}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerNotificationSent}">
		<td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-text" name="dspDetails.AIO_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  class="listwhitetext">Provider confirmation received </td>
	    <c:if test="${not empty dspDetails.AIO_providerConfirmationReceived}">
			 <s:text id="customerFileProviderConfirmationReceived" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerConfirmationReceived"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-text" name="dspDetails.AIO_providerConfirmationReceived" value="%{customerFileProviderConfirmationReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerConfirmationReceived}">
		<td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-text" name="dspDetails.AIO_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext">Prearrival discussion between provider and family</td>
	    <c:if test="${not empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
			 <s:text id="customerFilePrearrivalDiscussionOfProviderAndFamily" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily"/></s:text>
			 <td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-text" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" value="%{customerFilePrearrivalDiscussionOfProviderAndFamily}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_prearrivalDiscussionOfProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
		<td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-text" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_prearrivalDiscussionOfProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.AIO_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr>
<td align="right" class="listwhitetext" height="10"></td>
</tr>
</tbody>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end aio -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.AIO_vendorCode)<0 && dspDetails.AIO_vendorCode !='' && dspDetails.AIO_vendorCode !=null && dspDetails.AIO_displyOtherVendorCode == false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">	
	<s:hidden name="dspDetails.AIO_airlineFlight" />
<s:hidden name="dspDetails.AIO_contactInformation" />
<c:if test="${not empty dspDetails.AIO_flightArrival}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_flightArrival" /></s:text>
	<s:hidden  name="dspDetails.AIO_flightArrival" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_flightArrival}">
		 <s:hidden   name="dspDetails.AIO_flightArrival"/> 
</c:if>
<c:if test="${not empty dspDetails.AIO_flightDeparture}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_flightDeparture" /></s:text>
	<s:hidden  name="dspDetails.AIO_flightDeparture" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_flightDeparture}">
		 <s:hidden   name="dspDetails.AIO_flightDeparture"/> 
</c:if>
<c:if test="${not empty dspDetails.AIO_initialDateOfServiceRequested}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_initialDateOfServiceRequested" /></s:text>
	<s:hidden  name="dspDetails.AIO_initialDateOfServiceRequested" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_initialDateOfServiceRequested}">
		 <s:hidden   name="dspDetails.AIO_initialDateOfServiceRequested"/> 
</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<c:if test="${not empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" /></s:text>
	<s:hidden  name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
		 <s:hidden   name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily"/> 
</c:if>

<c:if test="${not empty dspDetails.AIO_providerConfirmationReceived}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_providerConfirmationReceived" /></s:text>
	<s:hidden  name="dspDetails.AIO_providerConfirmationReceived" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_providerConfirmationReceived}">
		 <s:hidden   name="dspDetails.AIO_providerConfirmationReceived"/> 
</c:if>
</configByCorp:fieldVisibility>
<c:if test="${not empty dspDetails.AIO_providerNotificationSent}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_providerNotificationSent" /></s:text>
	<s:hidden  name="dspDetails.AIO_providerNotificationSent" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_providerNotificationSent}">
		 <s:hidden   name="dspDetails.AIO_providerNotificationSent"/> 
</c:if>
<s:hidden name="dspDetails.AIO_serviceAuthorized" />
<s:hidden name="dspDetails.AIO_vendorCode" />
<s:hidden name="dspDetails.AIO_vendorName" />
<s:hidden name="dspDetails.AIO_vendorContact" />
<s:hidden name="dspDetails.AIO_vendorEmail" />
<s:hidden name="dspDetails.AIO_paymentResponsibility" />
<c:if test="${not empty dspDetails.AIO_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.AIO_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_serviceStartDate}">
		 <s:hidden   name="dspDetails.AIO_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.AIO_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.AIO_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.AIO_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.AIO_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.AIO_serviceEndDate}">
		 <s:hidden   name="dspDetails.AIO_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.AIO_vendorCodeEXSO" />
<s:hidden name="dspDetails.AIO_comment" />	
	</c:if>
	</c:if>
			<!-- start aio displyOtherVendorCode-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.AIO_vendorCode)<0 && dspDetails.AIO_vendorCode !='' && dspDetails.AIO_vendorCode !=null && dspDetails.AIO_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('aio')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='AIO'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="aio">
  	   <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),chkIsVendorRedSky('AIO_'),changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.AIO_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.AIO_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceStartDate"/></s:text>
			 <td><s:textfield id="AIO_serviceStartDate" cssClass="input-textUpper" name="dspDetails.AIO_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceStartDate}">
		<td><s:textfield id="AIO_serviceStartDate" cssClass="input-textUpper" name="dspDetails.AIO_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</td>
 
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.AIO_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceEndDate"/></s:text>
			 <td><s:textfield id="AIO_serviceEndDate" cssClass="input-textUpper" name="dspDetails.AIO_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceEndDate}">
		<td><s:textfield id="AIO_serviceEndDate" cssClass="input-textUpper" name="dspDetails.AIO_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.AIO_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedAIO_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.AIO_displyOtherVendorCode}">
	 <c:set var="ischeckedAIO_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.AIO_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedAIO_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>   
<table cellspacing="0" cellpadding="3" border="0" class="detailTabLabel" style="margin-left:-20px">
<tbody>
<tr>
<td align="right"  width="250px"  class="listwhitetext">Flight Departure</td>
	    <c:if test="${not empty dspDetails.AIO_flightDeparture}">
			 <s:text id="customerFileFlightDeparture" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightDeparture"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-textUpper" name="dspDetails.AIO_flightDeparture" value="%{customerFileFlightDeparture}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightDeparture}">
		<td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-textUpper" name="dspDetails.AIO_flightDeparture" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext">Flight Arrival</td>
	    <c:if test="${not empty dspDetails.AIO_flightArrival}">
			 <s:text id="customerFileFlightArrival" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightArrival"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-textUpper" name="dspDetails.AIO_flightArrival" value="%{customerFileFlightArrival}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightArrival}">
		<td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-textUpper" name="dspDetails.AIO_flightArrival" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext" >Airline Flight</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_airlineFlight" readonly="true" size="30" maxlength="25" onchange="changeStatus();"/></td>
</tr>
<tr>
<td align="right" valign="top" class="listwhitetext" >Temporary accommodation contact information</td>
<td align="left" class="listwhitetext" >
<s:textarea  rows="2" cssStyle="width:178px;" id="dspDetails.AIO_contactInformation" name="dspDetails.AIO_contactInformation" disabled="true" cssClass="textarea" onchange="changeStatus();"/>	
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Days of service authorized # </td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.AIO_serviceAuthorized" readonly="true" size="10" maxlength="10" onchange="changeStatus();"/></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Initial date of service requested</td>
	    <c:if test="${not empty dspDetails.AIO_initialDateOfServiceRequested}">
			 <s:text id="customerFileInitialDateOfServiceRequested" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_initialDateOfServiceRequested"/></s:text>
			 <td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-textUpper" name="dspDetails.AIO_initialDateOfServiceRequested" value="%{customerFileInitialDateOfServiceRequested}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_initialDateOfServiceRequested}">
		<td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-textUpper" name="dspDetails.AIO_initialDateOfServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider notification sent</td>
	    <c:if test="${not empty dspDetails.AIO_providerNotificationSent}">
			 <s:text id="customerFileProviderNotificationSent" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerNotificationSent"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-textUpper" name="dspDetails.AIO_providerNotificationSent" value="%{customerFileProviderNotificationSent}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerNotificationSent}">
		<td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-textUpper" name="dspDetails.AIO_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  class="listwhitetext">Provider confirmation received </td>
	    <c:if test="${not empty dspDetails.AIO_providerConfirmationReceived}">
			 <s:text id="customerFileProviderConfirmationReceived" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerConfirmationReceived"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-textUpper" name="dspDetails.AIO_providerConfirmationReceived" value="%{customerFileProviderConfirmationReceived}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerConfirmationReceived}">
		<td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-textUpper" name="dspDetails.AIO_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext">Prearrival discussion between provider and family</td>
	    <c:if test="${not empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
			 <s:text id="customerFilePrearrivalDiscussionOfProviderAndFamily" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily"/></s:text>
			 <td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-textUpper" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" value="%{customerFilePrearrivalDiscussionOfProviderAndFamily}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
			 </td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
		<td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-textUpper" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/>
		</td>
		</c:if> 
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.AIO_comment" onkeyup="textLimit(this,4999)" disabled="true" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr>
<td align="right" class="listwhitetext" height="10"></td>

</tr>
</tbody>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end aio -->		
	<!-- start exp -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.EXP_vendorCode)>-1 && dspDetails.EXP_vendorCode !='' && dspDetails.EXP_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('exp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='EXP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="exp">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel"  cellpadding="0"  cellspacing="0" border="0" style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.EXP_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),chkIsVendorRedSky('EXP_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.EXP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();"  />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','EXP_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.EXP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceStartDate"/></s:text>
			 <td><s:textfield id="EXP_serviceStartDate" cssClass="input-text" name="dspDetails.EXP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceStartDate}">
		<td><s:textfield id="EXP_serviceStartDate" cssClass="input-text" name="dspDetails.EXP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSRelocationExpenseManagementNotes == '0' || countDSRelocationExpenseManagementNotes == '' || countDSRelocationExpenseManagementNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRelocationExpenseManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRelocationExpenseManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.EXP_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.EXP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceEndDate"/></s:text>
			 <td><s:textfield id="EXP_serviceEndDate" cssClass="input-text" name="dspDetails.EXP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceEndDate}">
		<td><s:textfield id="EXP_serviceEndDate" cssClass="input-text" name="dspDetails.EXP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.EXP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.EXP_vendorEmail" readonly="false" size="57" maxlength="65"  onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.EXP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedEXP_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.EXP_displyOtherVendorCode}">
	 <c:set var="ischeckedEXP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.EXP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedEXP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
 </configByCorp:fieldVisibility> 
  </tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:40px" class="detailTabLabel">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  width="100px"  class="listwhitetext">Paid Status  </td>
	     <td width="275px" ><s:select cssClass="list-menu" key="dspDetails.EXP_paidStatus" list="%{paidStatus}" headerKey="" headerValue="" cssStyle="width:105px" onchange="changeStatus();"/></td>
       <td></td>
		
<td align="right" class="listwhitetext">Provider Notification Date</td>
	    <c:if test="${not empty dspDetails.EXP_providerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue1" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_providerNotification"/></s:text>
			 <td><s:textfield id="EXP_providerNotification" cssClass="input-text" name="dspDetails.EXP_providerNotification" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_providerNotification}">
		<td><s:textfield id="EXP_providerNotification" cssClass="input-text" name="dspDetails.EXP_providerNotification" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.EXP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	<tr><td></td></tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end exp -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.EXP_vendorCode)<0 && dspDetails.EXP_vendorCode !='' && dspDetails.EXP_vendorCode !=null && dspDetails.EXP_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	<s:hidden name="dspDetails.EXP_vendorCode" />
<s:hidden name="dspDetails.EXP_vendorName" />
<s:hidden name="dspDetails.EXP_vendorContact" />
<s:hidden name="dspDetails.EXP_vendorEmail" />
<s:hidden name="dspDetails.EXP_paymentResponsibility" />
<c:if test="${not empty dspDetails.EXP_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.EXP_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.EXP_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.EXP_serviceStartDate}">
		 <s:hidden   name="dspDetails.EXP_serviceStartDate"/> 
</c:if>
<c:if test="${not empty dspDetails.EXP_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.EXP_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.EXP_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.EXP_serviceEndDate}">
		 <s:hidden   name="dspDetails.EXP_serviceEndDate"/> 
</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<s:hidden name="dspDetails.EXP_paidStatus" />
<c:if test="${not empty dspDetails.EXP_providerNotification}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.EXP_providerNotification" /></s:text>
	<s:hidden  name="dspDetails.EXP_providerNotification" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.EXP_providerNotification}">
		 <s:hidden   name="dspDetails.EXP_providerNotification"/> 
</c:if>
</configByCorp:fieldVisibility>
<s:hidden name="dspDetails.EXP_vendorCodeEXSO" />
<s:hidden name="dspDetails.EXP_comment" />
<s:hidden name="dspDetails.EXP_displyOtherVendorCode" />
	</c:if>
	</c:if>
			<!-- start exp displyOtherVendorCode-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.EXP_vendorCode)<0 && dspDetails.EXP_vendorCode !='' && dspDetails.EXP_vendorCode !=null && dspDetails.EXP_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('exp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='EXP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="exp">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel"  cellpadding="0"  cellspacing="0" border="0" style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.EXP_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),chkIsVendorRedSky('EXP_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.EXP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();"  />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.EXP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceStartDate"/></s:text>
			 <td><s:textfield id="EXP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.EXP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceStartDate}">
		<td><s:textfield id="EXP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.EXP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.EXP_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.EXP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceEndDate"/></s:text>
			 <td><s:textfield id="EXP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.EXP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceEndDate}">
		<td><s:textfield id="EXP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.EXP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.EXP_vendorEmail" readonly="true" size="57" maxlength="65"  onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.EXP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedEXP_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.EXP_displyOtherVendorCode}">
	 <c:set var="ischeckedEXP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.EXP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedEXP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
  </tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:40px" class="detailTabLabel">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  width="100px"  class="listwhitetext">Paid Status  </td>
	     <td width="275px" ><s:select cssClass="list-menu" key="dspDetails.EXP_paidStatus" list="%{paidStatus}" disabled="true" headerKey="" headerValue="" cssStyle="width:105px" onchange="changeStatus();"/></td>
       <td></td>
		
<td align="right" class="listwhitetext">Provider Notification Date</td>
	    <c:if test="${not empty dspDetails.EXP_providerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue1" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_providerNotification"/></s:text>
			 <td><s:textfield id="EXP_providerNotification" cssClass="input-textUpper" name="dspDetails.EXP_providerNotification" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_providerNotification}">
		<td><s:textfield id="EXP_providerNotification" cssClass="input-textUpper" name="dspDetails.EXP_providerNotification" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.EXP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	<tr><td></td></tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end exp -->			
		<!-- start rpt -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RPT_vendorCode)>-1 && dspDetails.RPT_vendorCode !='' && dspDetails.RPT_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rpt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RPT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rpt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),chkIsVendorRedSky('RPT_'),changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.RPT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RPT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RPT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RPT_serviceStartDate" cssClass="input-text" name="dspDetails.RPT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_serviceStartDate}">
		<td><s:textfield id="RPT_serviceStartDate" cssClass="input-text" name="dspDetails.RPT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
 <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSRepatriationNotes == '0' || countDSRepatriationNotes == '' || countDSRepatriationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRepatriationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRepatriationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if> 
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RPT_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.RPT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RPT_serviceEndDate" cssClass="input-text" name="dspDetails.RPT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_serviceEndDate}">
		     <td><s:textfield id="RPT_serviceEndDate" cssClass="input-text" name="dspDetails.RPT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RPT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RPT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RPT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRPT_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.RPT_displyOtherVendorCode}">
	 <c:set var="ischeckedRPT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RPT_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedRPT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:42px" class="detailTabLabel">
<tr><td align="right" class="listwhitetext">Damage Cost</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.RPT_damageCost" readonly="false" size="12" maxlength="10" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Security Deposit</td>
<td align="left" class="listwhitetext" colspan="2" cssStyle="width:65px;"><s:textfield cssClass="input-text" key="dspDetails.RPT_securityDeposit" readonly="false" size="12" maxlength="10" onchange="changeStatus();" /></td></tr>
<tr>
<td align="right"    class="listwhitetext">Landlord Paid Notification Date </td>
	    <c:if test="${not empty dspDetails.RPT_landlordPaidNotificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_landlordPaidNotificationDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-text" name="dspDetails.RPT_landlordPaidNotificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_landlordPaidNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_landlordPaidNotificationDate}">
		<td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-text" name="dspDetails.RPT_landlordPaidNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_landlordPaidNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
<td align="right" class="listwhitetext"  width="243px">Professional Cleaning Date</td>
			 <c:if test="${not empty dspDetails.RPT_professionalCleaningDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_professionalCleaningDate"/></s:text>
			 <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-text" name="dspDetails.RPT_professionalCleaningDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_professionalCleaningDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_professionalCleaningDate}">
		     <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-text" name="dspDetails.RPT_professionalCleaningDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_professionalCleaningDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>		
</tr>
<tr>
<td align="right"   class="listwhitetext">Negotiated Deposit Returned Date </td>
	    <c:if test="${not empty dspDetails.RPT_negotiatedDepositReturnedDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_negotiatedDepositReturnedDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-text" name="dspDetails.RPT_negotiatedDepositReturnedDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_negotiatedDepositReturnedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_negotiatedDepositReturnedDate}">
		<td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-text" name="dspDetails.RPT_negotiatedDepositReturnedDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_negotiatedDepositReturnedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
	<td align="right"   class="listwhitetext">Utilities Shut Off  Date</td>
	    <c:if test="${not empty dspDetails.RPT_utilitiesShutOff}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_utilitiesShutOff"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-text" name="dspDetails.RPT_utilitiesShutOff" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_utilitiesShutOff-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_utilitiesShutOff}">
		<td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-text" name="dspDetails.RPT_utilitiesShutOff" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_utilitiesShutOff-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td colspan="3"></td>
<td align="right"  class="listwhitetext">Move Out Date</td>
	    <c:if test="${not empty dspDetails.RPT_moveOutDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_moveOutDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-text" name="dspDetails.RPT_moveOutDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_moveOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_moveOutDate}">
		<td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-text" name="dspDetails.RPT_moveOutDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_moveOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>	
</tr>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel" border="0" style="margin-left:55px;">
<tr>
<td align="right" class="listwhitetext" >Deposit Returned To</td>
<td rowspan="" ><s:select cssClass="list-menu" name="dspDetails.RPT_depositReturnedTo" list="%{depositby}" cssStyle="width:242px;" headerKey="" headerValue="" /></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Comment</td>
<td colspan="5">
<s:textarea name="dspDetails.RPT_comment" cols="39" cssClass="textarea" onkeyup="textLimit(this,199)" onblur="textLimit(this,199)"  rows="3" onchange="changeStatus()"/>
</td>
</tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>	
	</c:if>
		<!-- end rpt -->
		 <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RPT_vendorCode)<0 && dspDetails.RPT_vendorCode !='' && dspDetails.RPT_vendorCode !=null && dspDetails.RPT_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
					<s:hidden name="dspDetails.RPT_vendorName" />
			<s:hidden name="dspDetails.RPT_vendorCode" />
			<s:hidden name="dspDetails.RPT_vendorContact" />
			<s:hidden name="dspDetails.RPT_vendorEmail" />
			<s:hidden name="dspDetails.RPT_paymentResponsibility" />
			<s:hidden name="dspDetails.RPT_displyOtherVendorCode" />
			<c:if test="${not empty dspDetails.RPT_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_serviceStartDate" /></s:text>
			<s:hidden  name="dspDetails.RPT_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_serviceStartDate}">
		 	<s:hidden   name="dspDetails.RPT_serviceStartDate"/> 
			</c:if>
			<c:if test="${not empty dspDetails.RPT_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_serviceEndDate" /></s:text>
			<s:hidden  name="dspDetails.RPT_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_serviceEndDate}">
		 	<s:hidden   name="dspDetails.RPT_serviceEndDate"/> 
			</c:if>
			<c:if test="${not empty dspDetails.RPT_landlordPaidNotificationDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_landlordPaidNotificationDate" /></s:text>
			<s:hidden  name="dspDetails.RPT_landlordPaidNotificationDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_landlordPaidNotificationDate}">
		 		<s:hidden   name="dspDetails.RPT_landlordPaidNotificationDate"/> 
			</c:if>
			<c:if test="${not empty dspDetails.RPT_professionalCleaningDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_professionalCleaningDate" /></s:text>
				<s:hidden  name="dspDetails.RPT_professionalCleaningDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_professionalCleaningDate}">
		 		<s:hidden   name="dspDetails.RPT_professionalCleaningDate"/> 
			</c:if>
			<s:hidden name="dspDetails.RPT_comment" />
			<c:if test="${not empty dspDetails.RPT_utilitiesShutOff}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_utilitiesShutOff" /></s:text>
			<s:hidden  name="dspDetails.RPT_utilitiesShutOff" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_utilitiesShutOff}">
		 		<s:hidden   name="dspDetails.RPT_utilitiesShutOff"/> 
			</c:if>
			<c:if test="${not empty dspDetails.RPT_moveOutDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_moveOutDate" /></s:text>
			<s:hidden  name="dspDetails.RPT_moveOutDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_moveOutDate}">
		 		<s:hidden   name="dspDetails.RPT_moveOutDate"/> 
			</c:if>
			<c:if test="${not empty dspDetails.RPT_negotiatedDepositReturnedDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
			<s:param name="value" value="dspDetails.RPT_negotiatedDepositReturnedDate" /></s:text>
			<s:hidden  name="dspDetails.RPT_negotiatedDepositReturnedDate" value="%{customerFileSurveyFormattedValue}" /> 
			</c:if>
			<c:if test="${empty dspDetails.RPT_negotiatedDepositReturnedDate}">
		 	<s:hidden   name="dspDetails.RPT_negotiatedDepositReturnedDate"/> 
			</c:if>
			<s:hidden name="dspDetails.RPT_damageCost" />
			<s:hidden name="dspDetails.RPT_securityDeposit" />
			<s:hidden name="dspDetails.RPT_depositReturnedTo" />
			<s:hidden name="dspDetails.RPT_vendorCodeEXSO" />
			<s:hidden name="RPT_displyOtherVendorCode" />
		</c:if>
		</c:if>
		<!-- start rpt displyOtherVendorCode-->
 <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RPT_vendorCode)<0 && dspDetails.RPT_vendorCode !='' && dspDetails.RPT_vendorCode !=null && dspDetails.RPT_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rpt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RPT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rpt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),chkIsVendorRedSky('RPT_'),changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.RPT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RPT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RPT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RPT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_serviceStartDate}">
		<td><s:textfield id="RPT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RPT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</td> 
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.RPT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RPT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RPT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_serviceEndDate}">
		     <td><s:textfield id="RPT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RPT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RPT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRPT_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.RPT_displyOtherVendorCode}">
	 <c:set var="ischeckedRPT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RPT_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedRPT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
  </tr>
</c:if>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:42px" class="detailTabLabel">
<tr><td align="right" class="listwhitetext">Damage Cost</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_damageCost" readonly="true" size="12" maxlength="10" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Security Deposit</td>
<td align="left" class="listwhitetext" colspan="2" cssStyle="width:65px;"><s:textfield cssClass="input-textUpper" key="dspDetails.RPT_securityDeposit" readonly="true" size="12" maxlength="10" onchange="changeStatus();" /></td></tr>
<tr>
<td align="right"    class="listwhitetext">Landlord Paid Notification Date </td>
	    <c:if test="${not empty dspDetails.RPT_landlordPaidNotificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_landlordPaidNotificationDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-textUpper" name="dspDetails.RPT_landlordPaidNotificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_landlordPaidNotificationDate}">
		<td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-textUpper" name="dspDetails.RPT_landlordPaidNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if> 
<td align="right" class="listwhitetext"  width="243px">Professional Cleaning Date</td>
			 <c:if test="${not empty dspDetails.RPT_professionalCleaningDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_professionalCleaningDate"/></s:text>
			 <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-textUpper" name="dspDetails.RPT_professionalCleaningDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_professionalCleaningDate}">
		     <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-textUpper" name="dspDetails.RPT_professionalCleaningDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>		
</tr>
<tr>
<td align="right"   class="listwhitetext">Negotiated Deposit Returned Date </td>
	    <c:if test="${not empty dspDetails.RPT_negotiatedDepositReturnedDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_negotiatedDepositReturnedDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-textUpper" name="dspDetails.RPT_negotiatedDepositReturnedDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_negotiatedDepositReturnedDate}">
		<td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-textUpper" name="dspDetails.RPT_negotiatedDepositReturnedDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if> 
	<td align="right"   class="listwhitetext">Utilities Shut Off  Date</td>
	    <c:if test="${not empty dspDetails.RPT_utilitiesShutOff}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_utilitiesShutOff"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-textUpper" name="dspDetails.RPT_utilitiesShutOff" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_utilitiesShutOff}">
		<td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-textUpper" name="dspDetails.RPT_utilitiesShutOff" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td colspan="3"></td>
<td align="right"  class="listwhitetext">Move Out Date</td>
	    <c:if test="${not empty dspDetails.RPT_moveOutDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_moveOutDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-textUpper" name="dspDetails.RPT_moveOutDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_moveOutDate}">
		<td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-textUpper" name="dspDetails.RPT_moveOutDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>	
</tr>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel" border="0" style="margin-left:55px;">
<tr>
<td align="right" class="listwhitetext" >Deposit Returned To</td>
<td rowspan="" ><s:select cssClass="list-menu" name="dspDetails.RPT_depositReturnedTo" list="%{depositby}" disabled="true" cssStyle="width:242px;" headerKey="" headerValue="" /></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Comment</td>
<td colspan="5">
<s:textarea name="dspDetails.RPT_comment" cols="39" cssClass="textarea" onkeyup="textLimit(this,199)" disabled="true" onblur="textLimit(this,199)"  rows="3" onchange="changeStatus()"/>
</td>
</tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>	
	</c:if>
		<!-- end rpt -->		
	<!-- start sch -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SCH_schoolSelected)>-1 && dspDetails.SCH_schoolSelected !='' && dspDetails.SCH_schoolSelected !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('sch')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SCH'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="sch">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83">School Selected</td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_schoolSelected" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo1('SCH_','${dspDetails.SCH_vendorCodeEXSO}'),chkIsVendorRedSky('SCH_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest1('SCH_','${dspDetails.SCH_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.SCH_schoolName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent1(this,'OA','SCH_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SCH_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_serviceStartDate"/></s:text>
			 <td width="60px"><s:textfield id="SCH_serviceStartDate" cssClass="input-text" name="dspDetails.SCH_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_serviceStartDate}">
		<td width="60px"><s:textfield id="SCH_serviceStartDate" cssClass="input-text" name="dspDetails.SCH_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSSchoolEducationalCounselingNotes == '0' || countDSSchoolEducationalCounselingNotes == '' || countDSSchoolEducationalCounselingNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSSchoolEducationalCounselingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSchooling&imageId=countDSSchoolEducationalCounselingNotesImage&fieldId=countDSSchoolEducationalCounselingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSchooling&imageId=countDSSchoolEducationalCounselingNotesImage&fieldId=countDSSchoolEducationalCounselingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSSchoolEducationalCounselingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSchooling&imageId=countDSSchoolEducationalCounselingNotesImage&fieldId=countDSSchoolEducationalCounselingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSchooling&imageId=countDSSchoolEducationalCounselingNotesImage&fieldId=countDSSchoolEducationalCounselingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Contact Person</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SCH_contactPerson" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SCH_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_serviceEndDate"/></s:text>
			 <td><s:textfield id="SCH_serviceEndDate" cssClass="input-text" name="dspDetails.SCH_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_serviceEndDate}">
		<td><s:textfield id="SCH_serviceEndDate" cssClass="input-text" name="dspDetails.SCH_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.SCH_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SCH_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">School Website</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SCH_website" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext" width="83">City</td>
<td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" key="dspDetails.SCH_city" readonly="false" size="20" maxlength="20" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSCH_displyOtherSchoolSelected" value="false" />
    <c:if test="${dspDetails.SCH_displyOtherSchoolSelected}">
	 <c:set var="ischeckedSCH_displyOtherSchoolSelected" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SCH_displyOtherSchoolSelected" onclick="changeStatus();" disabled="true" value="${ischeckedSCH_displyOtherSchoolSelected}" fieldValue="true" tabindex="17" /></td>
   </configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:25px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext">Prearrival Discussion Between Provider And Family</td>
	    <c:if test="${not empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue2" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily"/></s:text>
			 <td width="10"><s:textfield id="SCH_prearrivalDiscussionBetweenProviderAndFamily" cssClass="input-text" name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" value="%{customerFileSubmissionToTranfFormattedValue2}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_prearrivalDiscussionBetweenProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}">
		<td width="10"><s:textfield id="SCH_prearrivalDiscussionBetweenProviderAndFamily" cssClass="input-text" name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_prearrivalDiscussionBetweenProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>  	
<td width="175" align="right" class="listwhitetext">Admission Date</td>
	    <c:if test="${not empty dspDetails.SCH_admissionDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue1" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_admissionDate"/></s:text>
			 <td><s:textfield id="SCH_admissionDate" cssClass="input-text" name="dspDetails.SCH_admissionDate" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_admissionDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_admissionDate}">
		<td><s:textfield id="SCH_admissionDate" cssClass="input-text" name="dspDetails.SCH_admissionDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SCH_admissionDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td colspan="3"></td>
<td align="right" class="listwhitetext" >Number Of Children</td>
<td align="left" class="listwhitetext" colspan="5"><s:textfield cssClass="input-text" key="dspDetails.SCH_noOfChildren" readonly="false" size="7" maxlength="7" onchange="isInteger(this);changeStatus();"/></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SCH_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end sch -->
			<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SCH_schoolSelected)<0 && dspDetails.SCH_schoolSelected !='' && dspDetails.SCH_schoolSelected !=null && dspDetails.SCH_displyOtherSchoolSelected==false }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">
	<s:hidden name="dspDetails.SCH_schoolSelected" />
<s:hidden name="dspDetails.SCH_schoolName" />
<s:hidden name="dspDetails.SCH_contactPerson" />
<s:hidden name="dspDetails.SCH_website" />
<s:hidden name="dspDetails.SCH_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.SCH_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SCH_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.SCH_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.SCH_serviceStartDate}">
		 <s:hidden   name="dspDetails.SCH_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.SCH_paymentResponsibility" />
<c:if test="${not empty dspDetails.SCH_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SCH_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.SCH_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.SCH_serviceEndDate}">
		 <s:hidden   name="dspDetails.SCH_serviceEndDate"/> 
</c:if>
<c:if test="${not empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" /></s:text>
	<s:hidden  name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}">
		 <s:hidden   name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily"/> 
</c:if>
<c:if test="${not empty dspDetails.SCH_admissionDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SCH_admissionDate" /></s:text>
	<s:hidden  name="dspDetails.SCH_admissionDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.SCH_admissionDate}">
		 <s:hidden   name="dspDetails.SCH_admissionDate"/> 
</c:if>
<s:hidden name="dspDetails.SCH_city" />

<s:hidden name="dspDetails.SCH_noOfChildren" />
<s:hidden name="dspDetails.SCH_vendorCodeEXSO" />
<s:hidden name="dspDetails.SCH_comment" />
<s:hidden name="dspDetails.SCH_displyOtherSchoolSelected" />
	</c:if>
	</c:if>		
		<!-- start sch  displyOtherSchoolSelected-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SCH_schoolSelected)<0 && dspDetails.SCH_schoolSelected !='' && dspDetails.SCH_schoolSelected !=null && dspDetails.SCH_displyOtherSchoolSelected }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('sch')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SCH'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="sch">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83">School Selected</td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_schoolSelected" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo1('SCH_','${dspDetails.SCH_vendorCodeEXSO}'),chkIsVendorRedSky('SCH_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest1('SCH_','${dspDetails.SCH_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.SCH_schoolName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SCH_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_serviceStartDate"/></s:text>
			 <td width="60px"><s:textfield id="SCH_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SCH_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_serviceStartDate}">
		<td width="60px"><s:textfield id="SCH_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SCH_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>

</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Contact Person</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_contactPerson" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SCH_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_serviceEndDate"/></s:text>
			 <td><s:textfield id="SCH_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SCH_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_serviceEndDate}">
		<td><s:textfield id="SCH_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SCH_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SCH_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">School Website</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_website" readonly="true" size="57" maxlength="65" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext" width="83">City</td>
<td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_city" readonly="true" size="20" maxlength="20" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSCH_displyOtherSchoolSelected" value="false" />
    <c:if test="${dspDetails.SCH_displyOtherSchoolSelected}">
	 <c:set var="ischeckedSCH_displyOtherSchoolSelected" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SCH_displyOtherSchoolSelected" onclick="changeStatus();" disabled="true" value="${ischeckedSCH_displyOtherSchoolSelected}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>  
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" style="margin-left:25px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext">Prearrival Discussion Between Provider And Family</td>
	    <c:if test="${not empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue2" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily"/></s:text>
			 <td width="10"><s:textfield id="SCH_prearrivalDiscussionBetweenProviderAndFamily" cssClass="input-textUpper" name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" value="%{customerFileSubmissionToTranfFormattedValue2}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily}">
		<td width="10"><s:textfield id="SCH_prearrivalDiscussionBetweenProviderAndFamily" cssClass="input-textUpper" name="dspDetails.SCH_prearrivalDiscussionBetweenProviderAndFamily" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>  	
<td width="175" align="right" class="listwhitetext">Admission Date</td>
	    <c:if test="${not empty dspDetails.SCH_admissionDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue1" name="${FormDateValue}"><s:param name="value" value="dspDetails.SCH_admissionDate"/></s:text>
			 <td><s:textfield id="SCH_admissionDate" cssClass="input-textUpper" name="dspDetails.SCH_admissionDate" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SCH_admissionDate}">
		<td><s:textfield id="SCH_admissionDate" cssClass="input-textUpper" name="dspDetails.SCH_admissionDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td colspan="3"></td>
<td align="right" class="listwhitetext" >Number Of Children</td>
<td align="left" class="listwhitetext" colspan="5"><s:textfield cssClass="input-textUpper" key="dspDetails.SCH_noOfChildren" readonly="true" size="7" maxlength="7" onchange="isInteger(this);changeStatus();"/></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SCH_comment" disabled="true" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end sch -->		
	<!-- start tax -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAX_vendorCode)>-1 && dspDetails.TAX_vendorCode !='' && dspDetails.TAX_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tax')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
 <c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TAX'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tax">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TAX_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('TAX_','${dspDetails.TAX_vendorCodeEXSO}');chkIsVendorRedSky('TAX_');changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TAX_','${dspDetails.TAX_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TAX_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TAX_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TAX_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_serviceStartDate"/></s:text>
			 <td><s:textfield id="TAX_serviceStartDate" cssClass="input-text" name="dspDetails.TAX_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_serviceStartDate}">
		<td><s:textfield id="TAX_serviceStartDate" cssClass="input-text" name="dspDetails.TAX_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countdsTaxServicesNotes == '0' || countdsTaxServicesNotes == '' || countdsTaxServicesNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countdsTaxServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTaxes&imageId=countdsTaxServicesNotesImage&fieldId=countdsTaxServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTaxes&imageId=countdsTaxServicesNotesImage&fieldId=countdsTaxServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countdsTaxServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTaxes&imageId=countdsTaxServicesNotesImage&fieldId=countdsTaxServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTaxes&imageId=countdsTaxServicesNotesImage&fieldId=countdsTaxServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TAX_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TAX_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_serviceEndDate"/></s:text>
			 <td><s:textfield id="TAX_serviceEndDate" cssClass="input-text" name="dspDetails.TAX_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_serviceEndDate}">
		<td><s:textfield id="TAX_serviceEndDate" cssClass="input-text" name="dspDetails.TAX_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TAX_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TAX_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAX_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTAX_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TAX_displyOtherVendorCode}">
	 <c:set var="ischeckedTAX_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TAX_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTAX_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
  </tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" width="791" style="margin-left:45px" class="detailTabLabel">
<tr>
<td align="right"  width="84px"  class="listwhitetext">Notification Date </td>
	    <c:if test="${not empty dspDetails.TAX_notificationdate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_notificationdate"/></s:text>
			 <td width="65px" ><s:textfield id="TAX_notificationdate" cssClass="input-text" name="dspDetails.TAX_notificationdate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_notificationdate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_notificationdate}">
		<td width="65px" ><s:textfield id="TAX_notificationdate" cssClass="input-text" name="dspDetails.TAX_notificationdate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAX_notificationdate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td align="right" width="93px" class="listwhitetext" colspan="0">Allowance</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.TAX_allowance" list="%{allowance}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
		</configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" width="150" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TAX_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end tax -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAX_vendorCode)<0 && dspDetails.TAX_vendorCode !='' && dspDetails.TAX_vendorCode !=null && dspDetails.TAX_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">
		<s:hidden name="dspDetails.TAX_vendorCode" />
	<s:hidden name="dspDetails.TAX_vendorName" />
	<s:hidden name="dspDetails.TAX_vendorContact" />
	<s:hidden name="dspDetails.TAX_vendorEmail" />
	<s:hidden name="dspDetails.TAX_paymentResponsibility" />
	<c:if test="${not empty dspDetails.TAX_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TAX_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.TAX_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.TAX_serviceStartDate}">
		 <s:hidden   name="dspDetails.TAX_serviceStartDate"/> 
</c:if>
	<c:if test="${not empty dspDetails.TAX_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TAX_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.TAX_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.TAX_serviceEndDate}">
		 <s:hidden   name="dspDetails.TAX_serviceEndDate"/> 
</c:if>
	<c:if test="${not empty dspDetails.TAX_notificationdate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TAX_notificationdate" /></s:text>
	<s:hidden  name="dspDetails.TAX_notificationdate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.TAX_notificationdate}">
		 <s:hidden   name="dspDetails.TAX_notificationdate"/> 
</c:if>
	<s:hidden name="dspDetails.TAX_vendorCodeEXSO" />
	<s:hidden name="dspDetails.TAX_comment" />
	<s:hidden name="dspDetails.TAX_displyOtherVendorCode" />
	<s:hidden name="dspDetails.TAX_allowance" />
		
	</c:if>
	</c:if>		
			<!-- start tax displyOtherVendorCode-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAX_vendorCode)<0 && dspDetails.TAX_vendorCode !='' && dspDetails.TAX_vendorCode !=null && dspDetails.TAX_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tax')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
 <c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TAX'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tax">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TAX_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('TAX_','${dspDetails.TAX_vendorCodeEXSO}');chkIsVendorRedSky('TAX_');changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TAX_','${dspDetails.TAX_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TAX_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TAX_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_serviceStartDate"/></s:text>
			 <td><s:textfield id="TAX_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TAX_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_serviceStartDate}">
		<td><s:textfield id="TAX_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TAX_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TAX_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TAX_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_serviceEndDate"/></s:text>
			 <td><s:textfield id="TAX_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TAX_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_serviceEndDate}">
		<td><s:textfield id="TAX_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TAX_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TAX_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAX_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTAX_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TAX_displyOtherVendorCode}">
	 <c:set var="ischeckedTAX_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TAX_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTAX_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" width="100%" style="margin-left:45px" class="detailTabLabel">
<tr>
<td align="right"  width="230px"  class="listwhitetext">Notification Date </td>
	    <c:if test="${not empty dspDetails.TAX_notificationdate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAX_notificationdate"/></s:text>
			 <td width="65px" ><s:textfield id="TAX_notificationdate" cssClass="input-textUpper" name="dspDetails.TAX_notificationdate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAX_notificationdate}">
		<td width="65px" ><s:textfield id="TAX_notificationdate" cssClass="input-textUpper" name="dspDetails.TAX_notificationdate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if> 
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td align="right" class="listwhitetext" colspan="0">Allowance</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.TAX_allowance" list="%{allowance}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
		</configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" width="150">Comment</td><td colspan="3"><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.TAX_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end tax -->
		<!-- start tac -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAC_vendorCode)>-1 && dspDetails.TAC_vendorCode !='' && dspDetails.TAC_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tac')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TAC'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tac">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TAC_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('TAC_','${dspDetails.TAC_vendorCodeEXSO}'),chkIsVendorRedSky('TAC_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TAC_','${dspDetails.TAC_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TAC_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TAC_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TAC_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_serviceStartDate"/></s:text>
			 <td><s:textfield id="TAC_serviceStartDate" cssClass="input-text" name="dspDetails.TAC_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_serviceStartDate}">
		<td><s:textfield id="TAC_serviceStartDate" cssClass="input-text" name="dspDetails.TAC_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSTemporaryAccommodationNotes == '0' || countDSTemporaryAccommodationNotes == '' || countDSTemporaryAccommodationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSTemporaryAccommodationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTempAccomodation&imageId=countDSTemporaryAccommodationNotesImage&fieldId=countDSTemporaryAccommodationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTempAccomodation&imageId=countDSTemporaryAccommodationNotesImage&fieldId=countDSTemporaryAccommodationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSTemporaryAccommodationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTempAccomodation&imageId=countDSTemporaryAccommodationNotesImage&fieldId=countDSTemporaryAccommodationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTempAccomodation&imageId=countDSTemporaryAccommodationNotesImage&fieldId=countDSTemporaryAccommodationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TAC_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TAC_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_serviceEndDate"/></s:text>
			 <td><s:textfield id="TAC_serviceEndDate" cssClass="input-text" name="dspDetails.TAC_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_serviceEndDate}">
		<td><s:textfield id="TAC_serviceEndDate" cssClass="input-text" name="dspDetails.TAC_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TAC_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TAC_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAC_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">

<c:set var="ischeckedTAC_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TAC_displyOtherVendorCode}">
	 <c:set var="ischeckedTAC_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TAC_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTAC_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
  </tr>
</tbody>
</table>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">  
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0"  style="margin-left: 88px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext"></td>
			 <td colspan="2" ></td>
<td align="right" class="listwhitetext" >Time&nbsp;Authorized&nbsp;</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TAC_monthsWeeksDaysAuthorized" readonly="false" size="7" maxlength="2" onchange="changeStatus();"/></td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAC_timeAuthorized" list="%{timeAuthorized}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</tr>
<tr> <td></td></tr>
</table>

<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
</configByCorp:fieldVisibility>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="margin-left:20px">

<tr><td align="right" class="listwhitetext">Monthly Rental Allowance</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.TAC_monthlyRentalAllowance" cssStyle="width:65px" maxlength="10"  onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext" >Lease Start Date</td>
     <c:if test="${not empty dspDetails.TAC_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_leaseStartDate"/></s:text>
			 <td width="60"><s:textfield id="TAC_leaseStartDate" cssClass="input-text" name="dspDetails.TAC_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_leaseStartDate}">
		<td width="60"><s:textfield id="TAC_leaseStartDate" cssClass="input-text" name="dspDetails.TAC_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td  align="right" class="listwhitetext" >Security Deposit&nbsp;${currencySign}</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.TAC_securityDeposit" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext" width="142">Lease Expire Date</td>
   <c:if test="${not empty dspDetails.TAC_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_leaseExpireDate"/></s:text>
			 <td><s:textfield id="TAC_leaseExpireDate" cssClass="input-text" name="dspDetails.TAC_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_leaseExpireDate}">
		<td><s:textfield id="TAC_leaseExpireDate" cssClass="input-text" name="dspDetails.TAC_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td></td>
<td align="right" colspan="2"  width="170" class="listwhitetext" >Expiry Reminder Prior To Expiry</td>
   <c:if test="${not empty dspDetails.TAC_expiryReminderPriorToExpiry}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_expiryReminderPriorToExpiry"/></s:text>
			 <td><s:textfield id="TAC_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TAC_expiryReminderPriorToExpiry" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_expiryReminderPriorToExpiry}">
		<td><s:textfield id="TAC_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TAC_expiryReminderPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAC_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Negotiated Rent</td>
<td rowspan="" ><s:textarea cssClass="textarea"  rows="2" cols="40" name="dspDetails.TAC_negotiatedRent"  onchange="changeStatus();" /> </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Utilities Included</td>
<td rowspan="" ><s:select cssClass="list-menu" id="leaseReviewUtilities1" name="dspDetails.TAC_utilitiesIncluded" list="%{utilities}" value="%{multiplutilities1}" multiple="true" cssStyle="width:247px; height:100px" headerKey="" headerValue="" /></td>
   <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Utilities Type</b>
             </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Deposit Paid By</td>
<td rowspan="" ><s:select cssClass="list-menu" name="dspDetails.TAC_depositPaidBy" list="%{depositby}" cssStyle="width:247px;" headerKey="" headerValue="" /></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td><s:textarea cssClass="textarea"  rows="3" cols="40" name="dspDetails.TAC_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end tac -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAC_vendorCode)<0 && dspDetails.TAC_vendorCode !='' && dspDetails.TAC_vendorCode !=null && dspDetails.TAC_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">
			<s:hidden name="dspDetails.TAC_vendorCode" />
		<s:hidden name="dspDetails.TAC_vendorName" />
		<s:hidden name="dspDetails.TAC_vendorContact" />
		<s:hidden name="dspDetails.TAC_vendorEmail" />
		<s:hidden name="dspDetails.TAC_paymentResponsibility" />
		<c:if test="${not empty dspDetails.TAC_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.TAC_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.TAC_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.TAC_serviceStartDate}">
		 <s:hidden   name="dspDetails.TAC_serviceStartDate"/> 
		</c:if>
		<c:if test="${not empty dspDetails.TAC_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.TAC_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.TAC_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.TAC_serviceEndDate}">
		 <s:hidden   name="dspDetails.TAC_serviceEndDate"/> 
		</c:if>
		<s:hidden name="dspDetails.TAC_monthsWeeksDaysAuthorized" />
		<s:hidden name="dspDetails.TAC_timeAuthorized" />
		
		<s:hidden name="dspDetails.TAC_monthlyRentalAllowance" />
		<c:if test="${not empty dspDetails.TAC_leaseStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.TAC_leaseStartDate" /></s:text>
		<s:hidden  name="dspDetails.TAC_leaseStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.TAC_leaseStartDate}">
		 <s:hidden   name="dspDetails.TAC_leaseStartDate"/> 
		</c:if>
		<s:hidden name="dspDetails.TAC_securityDeposit" />
		<c:if test="${not empty dspDetails.TAC_leaseExpireDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.TAC_leaseExpireDate" /></s:text>
		<s:hidden  name="dspDetails.TAC_leaseExpireDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.TAC_leaseExpireDate}">
		 <s:hidden   name="dspDetails.TAC_leaseExpireDate"/> 
		</c:if>
		<c:if test="${not empty dspDetails.TAC_expiryReminderPriorToExpiry}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.TAC_expiryReminderPriorToExpiry" /></s:text>
		<s:hidden  name="dspDetails.TAC_expiryReminderPriorToExpiry" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.TAC_expiryReminderPriorToExpiry}">
		 <s:hidden   name="dspDetails.TAC_expiryReminderPriorToExpiry"/> 
		</c:if>
		<s:hidden name="dspDetails.TAC_negotiatedRent" />
		<s:hidden name="dspDetails.TAC_utilitiesIncluded" />
		<s:hidden name="dspDetails.TAC_depositPaidBy" />
		<s:hidden name="dspDetails.TAC_comment" />
		<s:hidden name="dspDetails.TAC_vendorCodeEXSO" />
		<s:hidden name="dspDetails.TAC_displyOtherVendorCode" />
	</c:if>
	</c:if>
			<!-- start tac displyOtherVendorCode-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TAC_vendorCode)<0 && dspDetails.TAC_vendorCode !='' && dspDetails.TAC_vendorCode !=null && dspDetails.TAC_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tac')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TAC'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tac">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TAC_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('TAC_','${dspDetails.TAC_vendorCodeEXSO}'),chkIsVendorRedSky('TAC_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TAC_','${dspDetails.TAC_vendorCodeEXSO}');" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TAC_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TAC_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_serviceStartDate"/></s:text>
			 <td><s:textfield id="TAC_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TAC_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_serviceStartDate}">
		<td><s:textfield id="TAC_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TAC_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TAC_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TAC_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_serviceEndDate"/></s:text>
			 <td><s:textfield id="TAC_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TAC_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_serviceEndDate}">
		<td><s:textfield id="TAC_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TAC_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TAC_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAC_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTAC_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TAC_displyOtherVendorCode}">
	 <c:set var="ischeckedTAC_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TAC_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTAC_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
 </configByCorp:fieldVisibility>
</tr>
</tbody>
</table>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG"> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0"  style="margin-left: 88px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext"></td>
			 <td colspan="2" ></td>
<td align="right" class="listwhitetext" >Time&nbsp;Authorized&nbsp;</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TAC_monthsWeeksDaysAuthorized" readonly="true" size="7" maxlength="2" onchange="changeStatus();"/></td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TAC_timeAuthorized" list="%{timeAuthorized}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</tr>
<tr> <td></td></tr>
</table>

<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
</configByCorp:fieldVisibility>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="margin-left:20px">
<tr><td align="right" class="listwhitetext">Monthly Rental Allowance</td>
    <td ><s:textfield  cssClass="input-textUpper" name="dspDetails.TAC_monthlyRentalAllowance" readonly="true" cssStyle="width:65px" maxlength="10"  onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext" >Lease Start Date</td>
     <c:if test="${not empty dspDetails.TAC_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_leaseStartDate"/></s:text>
			 <td width="60"><s:textfield id="TAC_leaseStartDate" cssClass="input-textUpper" name="dspDetails.TAC_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_leaseStartDate}">
		<td width="60"><s:textfield id="TAC_leaseStartDate" cssClass="input-textUpper" name="dspDetails.TAC_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>

</tr>
<tr><td  align="right" class="listwhitetext" >Security Deposit&nbsp;${currencySign}</td>
    <td ><s:textfield  cssClass="input-textUpper" name="dspDetails.TAC_securityDeposit" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext" width="142">Lease Expire Date</td>
   <c:if test="${not empty dspDetails.TAC_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_leaseExpireDate"/></s:text>
			 <td><s:textfield id="TAC_leaseExpireDate" cssClass="input-textUpper" name="dspDetails.TAC_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_leaseExpireDate}">
		<td><s:textfield id="TAC_leaseExpireDate" cssClass="input-textUpper" name="dspDetails.TAC_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr><td></td>
<td align="right" colspan="2"  width="170" class="listwhitetext" >Expiry Reminder Prior To Expiry</td>
   <c:if test="${not empty dspDetails.TAC_expiryReminderPriorToExpiry}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAC_expiryReminderPriorToExpiry"/></s:text>
			 <td><s:textfield id="TAC_expiryReminderPriorToExpiry" cssClass="input-textUpper" name="dspDetails.TAC_expiryReminderPriorToExpiry" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TAC_expiryReminderPriorToExpiry}">
		<td><s:textfield id="TAC_expiryReminderPriorToExpiry" cssClass="input-textUpper" name="dspDetails.TAC_expiryReminderPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Negotiated Rent</td>
<td rowspan="" ><s:textarea cssClass="textarea" disabled="true" rows="2" cols="40" name="dspDetails.TAC_negotiatedRent"  onchange="changeStatus();" /> </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Utilities Included</td>
<td rowspan="" ><s:select cssClass="list-menu" id="leaseReviewUtilities1" name="dspDetails.TAC_utilitiesIncluded" disabled="true" list="%{utilities}" value="%{multiplutilities1}" multiple="true" cssStyle="width:247px; height:100px" headerKey="" headerValue="" /></td>
   <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Utilities Type</b>
             </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Deposit Paid By</td>
<td rowspan="" ><s:select cssClass="list-menu" name="dspDetails.TAC_depositPaidBy" list="%{depositby}" disabled="true" cssStyle="width:247px;" headerKey="" headerValue="" /></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td><s:textarea cssClass="textarea"  disabled="true" rows="3" cols="40" name="dspDetails.TAC_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end tac -->
		
		
	<!-- start ten -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TEN_vendorCode)>-1 && dspDetails.TEN_vendorCode !='' && dspDetails.TEN_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ten')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TEN'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ten">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TEN_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('TEN_','${dspDetails.TEN_vendorCodeEXSO}'),chkIsVendorRedSky('TEN_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TEN_','${dspDetails.TEN_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.TEN_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TEN_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="102px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TEN_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_serviceStartDate"/></s:text>
			 <td><s:textfield id="TEN_serviceStartDate" cssClass="input-text" name="dspDetails.TEN_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TEN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_serviceStartDate}">
		<td><s:textfield id="TEN_serviceStartDate" cssClass="input-text" name="dspDetails.TEN_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="TEN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSTenancyManagementNotes == '0' || countDSTenancyManagementNotes == '' || countDSTenancyManagementNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSTenancyManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTenancyManagement&imageId=countDSTenancyManagementNotesImage&fieldId=countDSTenancyManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTenancyManagement&imageId=countDSTenancyManagementNotesImage&fieldId=countDSTenancyManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSTenancyManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTenancyManagement&imageId=countDSTenancyManagementNotesImage&fieldId=countDSTenancyManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTenancyManagement&imageId=countDSTenancyManagementNotesImage&fieldId=countDSTenancyManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TEN_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TEN_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_serviceEndDate"/></s:text>
			 <td><s:textfield id="TEN_serviceEndDate" cssClass="input-text" name="dspDetails.TEN_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TEN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_serviceEndDate}">
		<td><s:textfield id="TEN_serviceEndDate" cssClass="input-text" name="dspDetails.TEN_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TEN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TEN_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TEN_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TEN_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTEN_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TEN_displyOtherVendorCode}">
	 <c:set var="ischeckedTEN_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TEN_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTEN_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TEN_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr> <td></td></tr>
</tbody>
</table> 
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showTFMS">
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Property&nbsp;Lease&nbsp;Details</legend>
<table class="detailTabLabel">
<tr>
	<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Name</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordName" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Lease&nbsp;Start&nbsp;Date</td>
		<c:if test="${not empty dspDetails.TEN_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseStartDate"/></s:text>
			 <td width=10><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseStartDate}">
		<td width=10><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Contact&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordContactNumber" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Lease&nbsp;Expire&nbsp;Date</td>
		<c:if test="${not empty dspDetails.TEN_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseExpireDate"/></s:text>
			 <td width=10><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseExpireDate}">
		<td width=10><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Email&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordEmailAddress" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Expiry&nbsp;Reminder&nbsp;Prior&nbsp;to&nbsp;Expiry</td>
		<c:if test="${not empty dspDetails.TEN_expiryReminderPriorToExpiry}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_expiryReminderPriorToExpiry"/></s:text>
			 <td width=10><s:textfield id="TEN_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TEN_expiryReminderPriorToExpiry" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_expiryReminderPriorToExpiry}">
		<td width=10><s:textfield id="TEN_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TEN_expiryReminderPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Property&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_propertyAddress" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Monthly&nbsp;Rental&nbsp;Allowance</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_monthlyRentalAllowance" readonly="false" size="10" maxlength="10" onchange="changeStatus();" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Account&nbsp;in&nbsp;the&nbsp;name&nbsp;of</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_accountHolder" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Security&nbsp;Deposit&nbsp;${currencySign}</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_securityDeposit" readonly="false" size="10" maxlength="10" onchange="changeStatus();" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Name</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankName" readonly="false" size="57" maxlength="98" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Lease&nbsp;Currency</td>
	<td align="left" colspan="0"><s:select name="dspDetails.TEN_leaseCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankAddress" readonly="false" size="57" maxlength="110" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Lease&nbsp;Signee</td>
	<td align="left" colspan="0"><s:select name="dspDetails.TEN_leaseSignee" list="%{leaseSignee}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Account&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankAccountNumber" readonly="false" size="57" maxlength="45" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;IBAN&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankIbanNumber" readonly="false" size="57" maxlength="45" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">ABA#(9-&nbsp;digits)&nbsp;USA&nbsp;bank&nbsp;accounts</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_abaNumber" readonly="false" size="57" maxlength="45" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Account&nbsp;Type</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_accountType" readonly="false" size="57" maxlength="45" onchange="changeStatus();" /></td>
	</tr>
	</table>
	</fieldset>
</table>
</configByCorp:fieldVisibility>
	
	
	<c:if test="${tenancyManagementFlag=='false'}">
	<s:hidden name="dspDetails.TEN_propertyName"/>
	 <s:hidden name="dspDetails.TEN_city"/>
	 <s:hidden name="dspDetails.TEN_zipCode"/>
	 <s:hidden name="dspDetails.TEN_addressLine1"/>
	 <s:hidden name="dspDetails.TEN_country"/>
	 <s:hidden name="dspDetails.TEN_addressLine2"/>
	 <s:hidden name="dspDetails.TEN_state"/>
	  <s:hidden name="dspDetails.TEN_leasedBy"/>
	 <s:hidden name="dspDetails.TEN_followUpNeeded"/>
	 <s:hidden name="dspDetails.TEN_termOfNotice"/>
	 <s:hidden name="dspDetails.TEN_rentalComment"/>
	 
	 
	 <s:hidden name="dspDetails.TEN_rentAmount"/>
	 <s:hidden name="dspDetails.TEN_rentCurrency"/>
	 <s:hidden name="dspDetails.TEN_allowanceCurrency"/>
	 <s:hidden name="dspDetails.TEN_rentAllowance"/>
	 <s:hidden name="dspDetails.TEN_utilitiesIncluded"/>
	 <s:hidden name="dspDetails.TEN_rentPaidTo"/>
	 <s:hidden name="dspDetails.TEN_rentalIncreaseDate"/>
	 
	 <s:hidden name="dspDetails.TEN_depositAmount"/>
	 <s:hidden name="dspDetails.TEN_depositReturned"/>
	 <s:hidden name="dspDetails.TEN_depositCurrency"/>
	 <s:hidden name="dspDetails.TEN_depositReturnedAmount"/>
	 <s:hidden name="dspDetails.TEN_depositReturnedCurrency"/>
	 <s:hidden name="dspDetails.TEN_bankAccountNumber"/>
	 <s:hidden name="dspDetails.TEN_paymentDescription"/>
	 <s:hidden name="dspDetails.TEN_bankIbanNumber"/>
	 <s:hidden name="dspDetails.TEN_swiftCode"/>
	  <s:hidden name="dspDetails.TEN_Gas"/>
	 <s:hidden name="dspDetails.TEN_Electricity"/>
	 <s:hidden name="dspDetails.TEN_Miscellaneous"/>
	 
	 <s:hidden name="dspDetails.TEN_exceptionComments"/>
	 <s:hidden name="dspDetails.TEN_contributionAmount"/>
	 <s:hidden name="dspDetails.TEN_assigneeContributionAmount"/>
	 <s:hidden name="dspDetails.TEN_Utility_Gas_Water"/>
	 <s:hidden name="dspDetails.TEN_Utility_TV_Internet_Phone"/>
	 <s:hidden name="dspDetails.TEN_Utility_mobilePhone"/>
	 <s:hidden name="dspDetails.TEN_Utility_furnitureRental"/>
	 <s:hidden name="dspDetails.TEN_Utility_cleaningServices"/>
	 <s:hidden name="dspDetails.TEN_Utility_parkingPermit"/>
	 <s:hidden name="dspDetails.TEN_Utility_communityTax"/>
	 <s:hidden name="dspDetails.TEN_Utility_insurance"/>
	 <s:hidden name="dspDetails.TEN_Utility_gardenMaintenance"/>
	 <s:hidden name="dspDetails.TEN_Utility_Gas"/>
	 <s:hidden name="dspDetails.TEN_Utility_Electricity"/>
	 <s:hidden name="dspDetails.TEN_Utility_Miscellaneous"/>
	 <s:hidden name="dspDetails.TEN_exceptionAddedValue"/>
	 <s:hidden name="dspDetails.TEN_billThroughDate"/>
	 <s:hidden name="dspDetails.TEN_housingRentVatPercent"/>
	 
	 	<c:if test="${not empty dspDetails.TEN_leaseStartDate}">
             <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseStartDate"/></s:text>
             <s:hidden name="dspDetails.TEN_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" />
        </c:if>
        <c:if test="${empty dspDetails.TEN_leaseStartDate}">
        	<s:hidden name="dspDetails.TEN_leaseStartDate" />
        </c:if>  
        
        <c:if test="${not empty dspDetails.TEN_leaseExpireDate}">
             <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseExpireDate"/></s:text>
             <s:hidden name="dspDetails.TEN_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}"/>
        </c:if>
        <c:if test="${empty dspDetails.TEN_leaseExpireDate}">
        	<s:hidden name="dspDetails.TEN_leaseExpireDate" cssStyle="width:65px" />
        </c:if>   
	 
	 </c:if>
	
	<!-- Rental Info 12158-->
	 <c:if test="${tenancyManagementFlag=='true'}">
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Rental Property Information</td>
	</tr>
	</table>

	<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel">
	<tbody>	
	<tr><td style="height:1px"></td></tr>											
	<tr>
		<td style="width:102px">&nbsp;</td>
		<td align="left" class="listwhitetext">Property&nbsp;Name</td>
		<td style="width:15px"></td>
		<td align="left" class="listwhitetext">City<font size="2" color="red">*</font></td>
		<td style="width:5px"></td>
		<td align="left" class="listwhitetext" style="width:25px">Zip&nbsp;Code<font size="2" color="red">*</font></td>
	</tr>
	<tr>
	<td style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_propertyName" cssClass="input-text" name="dspDetails.TEN_propertyName" cssStyle="width:305px" /></td>
	<td style="width:23px"></td>
	<td><s:textfield id="TEN_city" cssClass="input-text" name="dspDetails.TEN_city" cssStyle="width:140px" /></td>
	<td align="left" style="width:5px"></td>
	<td><s:textfield id="TEN_zipCode" cssClass="input-text" name="dspDetails.TEN_zipCode" cssStyle="width:65px" /></td>
 </tr>

 <tr>
	<td style="width:85px">&nbsp;</td>
	<td align="left" class="listwhitetext">Address&nbsp;line&nbsp;1<font size="2" color="red">*</font></td>
	<td style="width:15px"></td>
	
	<td align="left" class="listwhitetext">Country<font size="2" color="red">*</font></td>
	<td style="width:5px"></td>	
	<td style="width:15px"></td>
	</tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_addressLine1" cssClass="input-text" name="dspDetails.TEN_addressLine1" cssStyle="width:305px" /></td>
	<td style="width:15px"></td>
	<td colspan="3">	
	<s:select cssClass="list-menu" id="TEN_country"  name="dspDetails.TEN_country" list="%{country}" onchange="getState(this,'userChange');enableStateListOrigin();" headerKey="" headerValue="" cssStyle="width:226px" />
	</td>
	<td><img src="${pageContext.request.contextPath}/images/globe.png" onclick="openOriginLocation();"/></td>
	
 </tr>
 
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td align="left" class="listwhitetext">Address&nbsp;line&nbsp;2</td>
	<td align="left" style="width:15px"></td>
	<td align="left" class="listwhitetext">State&nbsp;/&nbsp;Province</td>
	<td align="left" style="width:5px"></td>	
	<td align="left" style="width:15px"></td>
</tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_addressLine2" cssClass="input-text" name="dspDetails.TEN_addressLine2" cssStyle="width:305px" /></td>
	<td align="left" style="width:15px"></td>
	<td colspan="3">	
	<s:select cssClass="list-menu" id="TEN_state"  name="dspDetails.TEN_state" list="%{state}" value="${dspDetails.TEN_state}" headerKey="" headerValue="" cssStyle="width:226px" />
	</td>
</tr>
<tr><td style="height:3px"></td></tr>													
</tbody>
</table>

<!-- End Rental Info -->	


<!-- Housing Rental Details -->	
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Housing Rental Details</td>
	</tr>
	</table>
	
	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
  <tbody>
  	<tr><td style="height:1px"></td></tr>
  	<tr>  
  		<td class="listwhitetext" align="right">Leased&nbsp;By<font size="2" color="red">*</font></td>     
      	<td colspan="4">		
		<s:select cssClass="list-menu" id="TEN_leasedBy"  name="dspDetails.TEN_leasedBy" list="%{reloTenancyProperty}" headerKey="" headerValue="" cssStyle="width:313px" />
	  </td>
      <td class="listwhitetext" align="right">Lease Start Date</td>
      <c:if test="${not empty dspDetails.TEN_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseStartDate"/></s:text>
			 <td width=60><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseStartDate}">
		<td width=60><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		<td class="listwhitetext" align="right" width="90">Lease&nbsp;End&nbsp;Date</td>
      <c:if test="${not empty dspDetails.TEN_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseExpireDate"/></s:text>
			 <td width=60><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseExpireDate}">
		<td width=60><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
		<td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
    </tr>
    
    <tr>
      <td class="listwhitetext" align="right">Rent&nbsp;Amount<font size="2" color="red">*</font></td>
  	  <td><s:textfield id="TEN_rentAmount" cssClass="input-text" name="dspDetails.TEN_rentAmount" cssStyle="width:85px" maxlength="10" onchange="showAssigneeContributionAmount();" onkeydown="return onlyRateAllowed(event)"/></td>
   
      <td class="listwhitetext" align="right" width="139">Rent&nbsp;Currency<font size="2" color="red">*</font></td>
      <td colspan="2" align="left">
      <s:select name="dspDetails.TEN_rentCurrency" id="TEN_rentCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:70px" headerKey="" headerValue="" onchange="showAssigneeContributionAmount();changeStatus();" />
      </td>        
       <td class="listwhitetext" align="right">VAT&nbsp;Desc</td>
      <td colspan="2" align="left">
      <s:select name="dspDetails.TEN_housingRentVatDesc" id="TEN_housingRentVatDesc" list="%{euVatList}" cssClass="list-menu" cssStyle="width:90px" onchange="getVatPercent();changeStatus();" />
      <s:hidden name="dspDetails.TEN_housingRentVatPercent" id="TEN_housingRentVatPercent" />
      </td>
      
      <td class="listwhitetext" align="right" width="90">Bill&nbsp;Through&nbsp;Date</td>
      <c:if test="${not empty dspDetails.TEN_billThroughDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_billThroughDate"/></s:text>
			 <td width=60><s:textfield id="TEN_billThroughDate" cssClass="input-text" name="dspDetails.TEN_billThroughDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td><img id="TEN_billThroughDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_billThroughDate}">
		<td width=60><s:textfield id="TEN_billThroughDate" cssClass="input-text" name="dspDetails.TEN_billThroughDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
		<td><img id="TEN_billThroughDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
    </tr>
    
    <tr>
      <td class="listwhitetext" align="right" width="60">Rent&nbsp;Allowance</td>
     <td><s:textfield id="TEN_rentAllowance" cssClass="input-text" name="dspDetails.TEN_rentAllowance" cssStyle="width:85px" maxlength="10" onchange="showAssigneeContributionAmount();" onkeydown="return onlyRateAllowed(event)"/></td>
     
     <td class="listwhitetext" colspan="1" align="right">Allowance&nbsp;Currency</td>
      <td colspan="2" align="left">
      <s:select name="dspDetails.TEN_allowanceCurrency" id="TEN_allowanceCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:70px" headerKey="" headerValue="" onchange="showAssigneeContributionAmount();changeStatus();" />
      </td>
     
      <td colspan="2" align="right">
      <table class="tableNomarg" id="assigneeContributionAmount" cellspacing="0" cellpadding="0">
      <tr>
     <td class="listwhitetext" align="right">Savings&nbsp;</td>
	 <td><s:textfield id="TEN_assigneeContributionAmount" cssClass="input-text" name="dspDetails.TEN_assigneeContributionAmount" cssStyle="width:65px" maxlength="10" readonly="true" onkeydown="return onlyRateAllowed(event)"/></td>
     </tr>
     </table>
     </td> 
            
    </tr>
    
    
    <tr id="assigneeContribution">     
    <td colspan="10">
    <fieldset style="margin:0px;padding:0px 2px 5px 0px; width:415px;" id="rentAmt">
    <table class="tableNomarg" cellspacing="0" cellpadding="0">
    <div class="subcontenttabChild" style="height:14px;color:#03a4e9;">Assignee Rent Contribution</div>
    <tr><td style="height:3px"></td></tr> 
     <tr>     
      <td class="listwhitetext" align="right">To&nbsp;be&nbsp;paid&nbsp;by&nbsp;</td>
   	 <td align="left"><s:select name="dspDetails.TEN_toBePaidBy" list="%{reloTenancyProperty}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange=""/></td>
	 <td class="listwhitetext" align="right">Contribution&nbsp;amount&nbsp;</td>
	 <td><s:textfield id="TEN_contributionAmount" cssClass="input-text" name="dspDetails.TEN_contributionAmount" cssStyle="width:63px" maxlength="10" readonly="true" onkeydown="return onlyRateAllowed(event)"/></td>	 
     </tr>
      <tr><td style="height:4px"></td></tr>		
      <tr>
      <c:set var="isTenDirectDebitFlag" value="false" />
		<c:if test="${dspDetails.TEN_directDebit}">
			<c:set var="isTenDirectDebitFlag" value="true" />
		</c:if>
      <td align="right" class="listwhitetext" width="">Direct&nbsp;Debit&nbsp;</td>    
      <td class="listwhitetext" align="left" width=""><s:checkbox key="dspDetails.TEN_directDebit" value="${isTenDirectDebitFlag}" fieldValue="true" onclick="saveTenancyManagementCheckBox(this,'TEN_directDebit');changeStatus();"/></td>
      <td class="listwhitetext" align="right">Contribution&nbsp;currency&nbsp;</td>
	  <td align="left"><s:select name="dspDetails.TEN_assigneeContributionCurrency" id="TEN_assigneeContributionCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:66px" headerKey="" headerValue="" onchange=""/></td>  
     </tr>
     <tr><td style="height:4px"></td></tr>   
 	<tr>
 	<td align="right" class="listwhitetext">&nbsp;IBAN/Bank&nbsp;account#&nbsp;</td>
	<td align="left" colspan="3"><s:textfield id="TEN_IBAN_BankAccountNumber" cssClass="input-text" name="dspDetails.TEN_IBAN_BankAccountNumber" cssStyle="width:305px" /></td>
	</tr>
	<tr><td style="height:4px"></td></tr> 
	<tr>	
	<td align="right" class="listwhitetext">&nbsp;BIC&nbsp;/&nbsp;SWIFT&nbsp;Code&nbsp;</td>
	<td colspan="3" align="left"><s:textfield id="TEN_BIC_SWIFT" cssClass="input-text" name="dspDetails.TEN_BIC_SWIFT" cssStyle="width:305px" /></td>
	</tr>
	<tr><td style="height:4px"></td></tr> 
	<tr>
	<td class="listwhitetext" align="right">Description&nbsp;</td>
	  <td colspan="3" align="left"><s:textfield id="TEN_description" cssClass="input-text" name="dspDetails.TEN_description" cssStyle="width:305px;" /></td>	
	</tr> 
    </table>
    </fieldset>
    </td>
    </tr>
    
    
    
    <tr>
      <td class="listwhitetext" align="right">Utilities&nbsp;included</td>
       <td align="left" valign="top">
   	 <s:select name="dspDetails.TEN_utilitiesIncluded" list="%{utilitiesIncluded}" id="TEN_utilitiesIncluded" cssClass="list-menu" cssStyle="width:89px" headerKey="" headerValue="" onchange="changeStatus();showUtilitiesInc();"/>   	
   	 </td>
        <%-- <td class="listwhitetext" align="right">Rent&nbsp;Paid&nbsp;To</td>
        <td align="left" valign="top">
   	 <s:select name="dspDetails.TEN_rentPaidTo" list="%{reloTenancyProperty}" cssClass="list-menu" cssStyle="width:85px" headerKey="" headerValue="" onchange="changeStatus();"/>   	
   	 </td> --%>
   	 <td class="listwhitetext" align="right">Check&nbsp;In&nbsp;/&nbsp;Move&nbsp;In&nbsp;Date</td>
      
       <c:if test="${not empty dspDetails.TEN_checkInMoveIn}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_checkInMoveIn"/></s:text>
			 <td width=60><s:textfield id="TEN_checkInMoveIn" cssClass="input-text" name="dspDetails.TEN_checkInMoveIn" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td width=20><img id="TEN_checkInMoveIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_checkInMoveIn}">
		<td width=60><s:textfield id="TEN_checkInMoveIn" cssClass="input-text" name="dspDetails.TEN_checkInMoveIn" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
		<td width=20><img id="TEN_checkInMoveIn-trigger" style="vertical-align:bottom;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
   	     
   	     <td class="listwhitetext" align="right" style="width:130px;">Check&nbsp;Out&nbsp;/ Move&nbsp;Out&nbsp;Date</td>
      <c:if test="${not empty dspDetails.TEN_checkOutMoveOut}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_checkOutMoveOut"/></s:text>
			 <td width=60><s:textfield id="TEN_checkOutMoveOut" cssClass="input-text" name="dspDetails.TEN_checkOutMoveOut" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td><img id="TEN_checkOutMoveOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_checkOutMoveOut}">
			<td width=60><s:textfield id="TEN_checkOutMoveOut" cssClass="input-text" name="dspDetails.TEN_checkOutMoveOut" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			<td><img id="TEN_checkOutMoveOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
    </tr>
    
    
    <tr id="utilitiesIncShow">
    <td></td>
    <td colspan="10">
    <fieldset style="margin:0px;padding:5px 0px; width:750px;" id="utilitiesIncludedShow">
    <table class="tableNomarg" cellspacing="0" cellpadding="0">
     <tr>
     <c:set var="isTenGasWaterFlag" value="false" />
		<c:if test="${dspDetails.TEN_Gas_Water}">
			<c:set var="isTenGasWaterFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width="16"><s:checkbox key="dspDetails.TEN_Gas_Water" id="TEN_Gas_Water" value="${isTenGasWaterFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_Gas_Water','TEN_Utility_Gas_Water');changeStatus();"  fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Water</td>
	 
	 <c:set var="isTenGasFlag" value="false" />
		<c:if test="${dspDetails.TEN_Gas}">
			<c:set var="isTenGasFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Gas" id="TEN_Gas" value="${isTenGasFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_Gas','TEN_Utility_Gas');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Gas</td>
	 
	 <c:set var="isTenElectricityFlag" value="false" />
		<c:if test="${dspDetails.TEN_Electricity}">
			<c:set var="isTenElectricityFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Electricity" id="TEN_Electricity" value="${isTenElectricityFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_Electricity','TEN_Utility_Electricity');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Electricity</td>
	 
	 <c:set var="isTenTvInternetPhoneFlag" value="false" />
		<c:if test="${dspDetails.TEN_TV_Internet_Phone}">
			<c:set var="isTenTvInternetPhoneFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_TV_Internet_Phone" id="TEN_TV_Internet_Phone" value="${isTenTvInternetPhoneFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_TV_Internet_Phone','TEN_Utility_TV_Internet_Phone');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;TV/Internet/Phone</td>
	 <c:set var="isTenMobilePhoneFlag" value="false" />
		<c:if test="${dspDetails.TEN_mobilePhone}">
			<c:set var="isTenMobilePhoneFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_mobilePhone" id="TEN_mobilePhone" value="${isTenMobilePhoneFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_mobilePhone','TEN_Utility_mobilePhone');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Mobile Phone</td>
	 <c:set var="isTenFurnitureRentalFlag" value="false" />
		<c:if test="${dspDetails.TEN_furnitureRental}">
			<c:set var="isTenFurnitureRentalFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_furnitureRental" id="TEN_furnitureRental" value="${isTenFurnitureRentalFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_furnitureRental','TEN_Utility_furnitureRental');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Furniture Rental</td>
	 
	 <c:set var="isTenGasElectricFlag" value="false" />
		<c:if test="${dspDetails.TEN_Gas_Electric}">
			<c:set var="isTenGasElectricFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Gas_Electric" id="TEN_Gas_Electric" value="${isTenGasElectricFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_Gas_Electric','TEN_Utility_Gas_Electric');changeStatus();"  fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Gas/Electr</td>
     </tr>
      <tr><td style="height:8px"></td></tr>		
      <tr>
      <c:set var="isTenCleaningServicesFlag" value="false" />
		<c:if test="${dspDetails.TEN_cleaningServices}">
			<c:set var="isTenCleaningServicesFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width=""><s:checkbox key="dspDetails.TEN_cleaningServices" id="TEN_cleaningServices" value="${isTenCleaningServicesFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_cleaningServices','TEN_Utility_cleaningServices');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Cleaning Services</td>
	 <c:set var="isTenParkingPermitFlag" value="false" />
		<c:if test="${dspDetails.TEN_parkingPermit}">
			<c:set var="isTenParkingPermitFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_parkingPermit" id="TEN_parkingPermit" value="${isTenParkingPermitFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_parkingPermit','TEN_Utility_parkingPermit');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Parking Permit</td>
	 <c:set var="isTenCommunityTaxFlag" value="false" />
		<c:if test="${dspDetails.TEN_communityTax}">
			<c:set var="isTenCommunityTaxFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_communityTax" id="TEN_communityTax" value="${isTenCommunityTaxFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_communityTax','TEN_Utility_communityTax');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Community Tax</td>
	 <c:set var="isTenInsuranceFlag" value="false" />
		<c:if test="${dspDetails.TEN_insurance}">
			<c:set var="isTenInsuranceFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_insurance" id="TEN_insurance" value="${isTenInsuranceFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_insurance','TEN_Utility_insurance');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Insurance</td>
	 
	 <c:set var="isTenGardenMaintenanceFlag" value="false" />
		<c:if test="${dspDetails.TEN_gardenMaintenance}">
			<c:set var="isTenGardenMaintenanceFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_gardenMaintenance" id="TEN_gardenMaintenance" value="${isTenGardenMaintenanceFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_gardenMaintenance','TEN_Utility_gardenMaintenance');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Garden Maintenance</td>
	 
	 <c:set var="isTenMiscellaneousFlag" value="false" />
		<c:if test="${dspDetails.TEN_Miscellaneous}">
			<c:set var="isTenMiscellaneousFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Miscellaneous" id="TEN_Miscellaneous" value="${isTenMiscellaneousFlag}" onclick="saveTenancyManagementCheckBox(this,'TEN_Miscellaneous','TEN_Utility_Miscellaneous');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Miscellaneous</td>
	 
     </tr>
    </table>
    </fieldset>
    </td>
    </tr>
    
    <tr>
      <td class="listwhitetext" align="right">Rental&nbsp;Increase&nbsp;Date</td>
       <td colspan="4">
       <table class="tableNomarg" cellspacing="0" cellpadding="0">
       <tr>       
        <c:if test="${not empty dspDetails.TEN_rentalIncreaseDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_rentalIncreaseDate"/></s:text>
			 <td width=60><s:textfield id="TEN_rentalIncreaseDate" cssClass="input-text" name="dspDetails.TEN_rentalIncreaseDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td>&nbsp;<img id="TEN_rentalIncreaseDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_rentalIncreaseDate}">
			<td width=60><s:textfield id="TEN_rentalIncreaseDate" cssClass="input-text" name="dspDetails.TEN_rentalIncreaseDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			<td>&nbsp;<img id="TEN_rentalIncreaseDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
      </tr>
      </table>
      </td>
      
      <td colspan="5">
   	 <table class="tableNomarg" cellspacing="0" cellpadding="0">
     <tr>
      <td  class="listwhitetext" align="right" width="136">Pre&nbsp;Check&nbsp;Out&nbsp;Date&nbsp;</td>
      <c:if test="${not empty dspDetails.TEN_preCheckOut}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_preCheckOut"/></s:text>
			 <td ><s:textfield id="TEN_preCheckOut" cssClass="input-text" name="dspDetails.TEN_preCheckOut" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:64px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
			 <td>&nbsp;<img id="TEN_preCheckOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_preCheckOut}">
		<td ><s:textfield id="TEN_preCheckOut" cssClass="input-text" name="dspDetails.TEN_preCheckOut" cssStyle="width:64px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
		<td>&nbsp;<img id="TEN_preCheckOut-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		</table>
		</td>
    </tr>
    
    <tr>
    <td align="right" class="listwhitetext" >Comment</td>
    <td colspan="3"><s:textarea cssClass="textarea"  cssStyle="width:308px;" rows="4" cols="47" name="dspDetails.TEN_rentalComment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
   	<td colspan="5" rowspan="2" style="padding:0px 70px" valign="top">
	<table class="tableNomarg">
	<tr>   	 
   	 <td class="listwhitetext" align="right">Extension&nbsp;Needed</td>
   	 <td align="left">
   	 <s:select name="dspDetails.TEN_followUpNeeded" list="%{utilitiesIncluded}" cssClass="list-menu" cssStyle="width:69px" headerKey="" headerValue="" onchange="changeStatus();"/>
   	 </td>
   	 </tr>
   	 <tr><td style="height:1px"></td></tr>		
   	 <tr>
   	 <td class="listwhitetext" align="right">Term&nbsp;of&nbsp;Notice</td>
   	 <td align="left">
   	 <s:select name="dspDetails.TEN_termOfNotice" list="%{termOfNotice}" cssClass="list-menu" cssStyle="width:69px" headerKey="" headerValue="" onchange="changeStatus();"/>
   	 </td>         
   	 </tr>
   	 </table>
   	 </td>
    </tr> 
    <tr><td style="height:1px"></td></tr>   
  </tbody>
</table>	
<!-- Housing Rental Details End-->	

<!-- Deposit Start-->
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Deposit</td>
	</tr>
	</table>
	
	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
  	<tbody> 
  	<tr><td style="height:1px"></td></tr> 
    <tr>
      <td class="listwhitetext" align="right" width="100">Deposit&nbsp;Amount</td>
  	  <td><s:textfield id="TEN_depositAmount" cssClass="input-text" name="dspDetails.TEN_depositAmount" cssStyle="width:85px" maxlength="10" onkeydown="return onlyRateAllowed(event)"/></td>   
      <td class="listwhitetext" align="right" width="142">Deposit&nbsp;returned</td>
      <td align="left">
      <s:select name="dspDetails.TEN_depositReturned" list="%{utilitiesIncluded}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" onchange="changeStatus();"/>
      </td>
    </tr>
    
    <tr>
     <td class="listwhitetext" align="right" width="">Deposit&nbsp;Currency</td>
      <td align="left">
      <s:select name="dspDetails.TEN_depositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:89px" headerKey="" headerValue="" onchange="changeStatus();"/>
      </td>
      <td class="listwhitetext" align="right" width="">Deposit&nbsp;returned&nbsp;amount</td>
  	  <td><s:textfield id="TEN_depositReturnedAmount" cssClass="input-text" name="dspDetails.TEN_depositReturnedAmount" cssStyle="width:76px" maxlength="10" onkeydown="return onlyRateAllowed(event)"/></td>     
    </tr>
    
     <tr>
     <td class="listwhitetext" align="right" width="">Deposit&nbsp;Paid&nbsp;By</td>
     <td align="left">
      <s:select name="dspDetails.TEN_depositPaidBy" list="%{reloTenancyProperty}" cssClass="list-menu" cssStyle="width:89px" headerKey="" headerValue="" onchange="changeStatus();"/>
      </td>      
      <td class="listwhitetext" align="right" width="">Deposit&nbsp;returned&nbsp;currency</td>
  	  <td align="left">
      <s:select name="dspDetails.TEN_depositReturnedCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" onchange="changeStatus();"/>
      </td>   
    </tr>
    
     <tr>
    <td align="right" class="listwhitetext" >Comment</td>
    <td colspan="0"><s:textarea cssClass="textarea"  cssStyle="width:308px;" rows="4" cols="47" name="dspDetails.TEN_depositComment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>      	 
   	 <td class="listwhitetext" align="right" valign="top" style="padding-top:6px;">Refundable&nbsp;To</td>
   	 <td align="left" valign="top">
   	 <s:select name="dspDetails.TEN_refundableTo" list="%{reloTenancyProperty}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" onchange="changeStatus();"/>   	
   	 </td>
    </tr> 
    <tr><td style="height:1px"></td></tr>   
    </tbody>
    </table>   
<!-- Deposit End-->

<!-- Landlord start -->
	
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Landlord</td>
	</tr>
	</table>

<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel">
<tbody>	
	<tr><td style="height:1px"></td></tr>											
	<tr>
		<td style="width:102px">&nbsp;</td>
		<td align="left" class="listwhitetext">Landlord&nbsp;Name</td>
		<td style="width:15px"></td>
		<td align="left" class="listwhitetext">Landlord&nbsp;E-mail</td>
		<td style="width:5px"></td>
		<td align="left" class="listwhitetext" style="width:25px">Landlord&nbsp;Phone#</td>
	</tr>
	<tr>
	<td style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_landlordName" cssClass="input-text" name="dspDetails.TEN_landlordName" cssStyle="width:205px" /></td>
	<td style="width:15px"></td>
	<td><s:textfield id="TEN_landlordEmailAddress" cssClass="input-text" name="dspDetails.TEN_landlordEmailAddress" cssStyle="width:190px" onchange="validate_email(this,'dspDetails.TEN_landlordEmailAddress')"/></td>
	<td align="left" style="width:9px"></td>
	<td><s:textfield id="TEN_landlordContactNumber" cssClass="input-text" name="dspDetails.TEN_landlordContactNumber" cssStyle="width:100px" /></td>
 </tr>
	<tr><td style="height:1px"></td></tr>
 <tr>
	<td style="width:85px">&nbsp;</td>
	<td align="left" class="listwhitetext" valign="top">Bank&nbsp;Account&nbsp;#</td>
	<td style="width:15px"></td>
	<td align="left" class="listwhitetext">Payment&nbsp;Description</td>
	<td style="width:5px"></td>	
	<td style="width:15px"></td>
	</tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td valign="top"><s:textfield id="TEN_bankAccountNumber" cssClass="input-text" name="dspDetails.TEN_bankAccountNumber" cssStyle="width:205px" /></td>
	<td style="width:15px"></td>
	<td colspan="3" rowspan="4">	
	<s:textarea cssClass="textarea"  cssStyle="width:311px;height:59px;" rows="4" cols="47" name="dspDetails.TEN_paymentDescription" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
	</td>
 </tr>
 <tr><td style="height:1px"></td></tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td align="left" class="listwhitetext">IBAN&nbsp;/&nbsp;Routing Code</td>
	<td align="left" style="width:15px"></td>	
</tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_bankIbanNumber" cssClass="input-text" name="dspDetails.TEN_bankIbanNumber" cssStyle="width:205px" /></td>
	<td align="left" style="width:15px"></td>	
</tr>
<tr><td style="height:1px"></td></tr>				
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td align="left" class="listwhitetext">BIC&nbsp;/&nbsp;SWIFT Code</td>
	<td align="left" style="width:15px"></td>	
</tr>
 <tr>
	<td align="left" style="width:85px">&nbsp;</td>
	<td><s:textfield id="TEN_swiftCode" cssClass="input-text" name="dspDetails.TEN_swiftCode" cssStyle="width:205px" /></td>
	<td align="left" style="width:15px"></td>	
</tr>
<tr><td style="height:3px"></td></tr>													
</tbody>
</table>
<!-- End Landlord -->	

<!-- Utilities start -->	
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Utilities</td>
	</tr>
	</table>
	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
  	<tbody> 
  	<tr><td style="height:1px"></td></tr>
  	
  	<tr>
    <td></td>
    <td colspan="10">
    <fieldset style="margin-left:100px;padding:5px 0px; width:730px;" id="utilitiesIncludedShow">
    <table class="tableNomarg" cellspacing="0" cellpadding="0">
     <tr>
     <c:set var="isTenUtilGasWaterFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_Gas_Water}">
			<c:set var="isTenUtilGasWaterFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width="16"><s:checkbox key="dspDetails.TEN_Utility_Gas_Water" id="TEN_Utility_Gas_Water" value="${isTenUtilGasWaterFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_Gas_Water','clicked');changeStatus();"  fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Water</td>
	 
	 <c:set var="isTenUtilGasFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_Gas}">
			<c:set var="isTenUtilGasFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_Gas" id="TEN_Utility_Gas" value="${isTenUtilGasFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_Gas','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Gas</td>
	 
	 <c:set var="isTenUtilElectricityFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_Electricity}">
			<c:set var="isTenUtilElectricityFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_Electricity" id="TEN_Utility_Electricity" value="${isTenUtilElectricityFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_Electricity','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Electricity</td>
	 
	 <c:set var="isTenUtilTvInternetPhoneFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_TV_Internet_Phone}">
			<c:set var="isTenUtilTvInternetPhoneFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_TV_Internet_Phone" id="TEN_Utility_TV_Internet_Phone" value="${isTenUtilTvInternetPhoneFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_TV_Internet_Phone','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;TV/Internet/Phone</td>
	 <c:set var="isTenUtilMobilePhoneFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_mobilePhone}">
			<c:set var="isTenUtilMobilePhoneFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_mobilePhone" id="TEN_Utility_mobilePhone" value="${isTenUtilMobilePhoneFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_mobilePhone','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Mobile Phone</td>
	 <c:set var="isTenUtilFurnitureRentalFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_furnitureRental}">
			<c:set var="isTenUtilFurnitureRentalFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_furnitureRental" id="TEN_Utility_furnitureRental" value="${isTenUtilFurnitureRentalFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_furnitureRental','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Furniture Rental</td>
	 
	 <c:set var="isTenUtilGasElectricFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_Gas_Electric}">
			<c:set var="isTenUtilGasElectricFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width="23"><s:checkbox key="dspDetails.TEN_Utility_Gas_Electric" id="TEN_Utility_Gas_Electric" value="${isTenUtilGasElectricFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_Gas_Electric','clicked');changeStatus();"  fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Gas/Electr</td>
     </tr>
      <tr><td style="height:8px"></td></tr>		
      <tr>
      <c:set var="isTenUtilCleaningServicesFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_cleaningServices}">
			<c:set var="isTenUtilCleaningServicesFlag" value="true" />
		</c:if>     
     <td class="listwhitetext" align="right" width=""><s:checkbox key="dspDetails.TEN_Utility_cleaningServices" id="TEN_Utility_cleaningServices" value="${isTenUtilCleaningServicesFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_cleaningServices','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Cleaning Services</td>
	 <c:set var="isTenUtilParkingPermitFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_parkingPermit}">
			<c:set var="isTenUtilParkingPermitFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Utility_parkingPermit" id="TEN_Utility_parkingPermit" value="${isTenUtilParkingPermitFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_parkingPermit','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Parking Permit</td>
	 <c:set var="isTenUtilCommunityTaxFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_communityTax}">
			<c:set var="isTenUtilCommunityTaxFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Utility_communityTax" id="TEN_Utility_communityTax" value="${isTenUtilCommunityTaxFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_communityTax','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Community Tax</td>
	 <c:set var="isTenUtilInsuranceFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_insurance}">
			<c:set var="isTenUtilInsuranceFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Utility_insurance" id="TEN_Utility_insurance" value="${isTenUtilInsuranceFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_insurance','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Insurance</td>
	 
	 <c:set var="isTenUtilGardenMaintenanceFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_gardenMaintenance}">
			<c:set var="isTenUtilGardenMaintenanceFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Utility_gardenMaintenance" id="TEN_Utility_gardenMaintenance" value="${isTenUtilGardenMaintenanceFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_gardenMaintenance','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Garden Maintenance</td>
	 		
	 <c:set var="isTenUtilMiscellaneousFlag" value="false" />
		<c:if test="${dspDetails.TEN_Utility_Miscellaneous}">
			<c:set var="isTenUtilMiscellaneousFlag" value="true" />
		</c:if>
	 <td class="listwhitetext" align="right" width="18"><s:checkbox key="dspDetails.TEN_Utility_Miscellaneous" id="TEN_Utility_Miscellaneous" value="${isTenUtilMiscellaneousFlag}" onclick="updateUtilitiesFieldValues(this,'TEN_Utility_Miscellaneous','clicked');changeStatus();" fieldValue="true"/></td>
	 <td align="left" class="listwhitetext" width="">&nbsp;Miscellaneous</td>
	 
     </tr>
    </table>
    </fieldset>
    </td>
    </tr>
    </tbody>
    </table>
    <div id="tenancyUtilityServiceAjaxDetail">
			<jsp:include flush="true" page="tenancyUtilityServiceAjaxList.jsp"></jsp:include>
		</div>
<!-- Utilities end -->

<!-- Exceptions start -->
	
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	<tr>
	<td class="relo_tenbg" width="100%" align="left" >&nbsp;&nbsp;Exceptions</td>
	</tr>
	</table>
	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
  	<tbody> 
  	<tr><td style="height:1px"></td></tr> 
    <tr>
    <td align="right" class="listwhitetext" width="100">Comment</td>
    <td align="left"><s:textarea cssClass="textarea"  cssStyle="width:308px;" rows="4" cols="47" name="dspDetails.TEN_exceptionComments" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>     	 
    <td align="right" class="listwhitetext" width="100">Added&nbsp;Value</td>
    <td align="left"><s:textarea cssClass="textarea"  cssStyle="width:308px;" rows="4" cols="47" name="dspDetails.TEN_exceptionAddedValue" onchange="changeStatus();" /> </td>
    </tr>
    </tbody>
    </table> 
<!-- Exceptions end -->
	</c:if>
	
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr><td style="height:30px;">&nbsp;</td></tr>
	</c:if>
	</c:if>
		<!-- end ten -->
		
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TEN_vendorCode)<0 && dspDetails.TEN_vendorCode !='' && dspDetails.TEN_vendorCode !=null && dspDetails.TEN_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}">
	<s:hidden name="dspDetails.TEN_vendorCode" />
	<s:hidden name="dspDetails.TEN_vendorName" />
	<s:hidden name="dspDetails.TEN_vendorContact" />
	<s:hidden name="dspDetails.TEN_vendorEmail" />
	<s:hidden name="dspDetails.TEN_paymentResponsibility" />
	<c:if test="${not empty dspDetails.TEN_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TEN_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.TEN_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.TEN_serviceStartDate}">
		 <s:hidden   name="dspDetails.TEN_serviceStartDate"/> 
	</c:if>
	 <c:if test="${not empty dspDetails.TEN_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TEN_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.TEN_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TEN_serviceEndDate}">
		 <s:hidden   name="dspDetails.TEN_serviceEndDate"/> 
	 </c:if>
	<s:hidden name="dspDetails.TEN_vendorCodeEXSO" />
	<s:hidden name="dspDetails.TEN_comment" />
	<s:hidden name="dspDetails.TEN_displyOtherVendorCode" />
	<s:hidden name="dspDetails.TEN_landlordName" />
	<s:hidden name="dspDetails.TEN_landlordContactNumber" />
	<s:hidden name="dspDetails.TEN_landlordEmailAddress" />
	<s:hidden name="dspDetails.TEN_propertyAddress" />
	<s:hidden name="dspDetails.TEN_accountHolder" />
	<s:hidden name="dspDetails.TEN_bankName" />
	<s:hidden name="dspDetails.TEN_bankAddress" />
	<s:hidden name="dspDetails.TEN_bankAccountNumber" />
	<s:hidden name="dspDetails.TEN_bankIbanNumber" />
	<s:hidden name="dspDetails.TEN_abaNumber" />
	<s:hidden name="dspDetails.TEN_accountType" />
	<c:if test="${not empty dspDetails.TEN_leaseStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TEN_leaseStartDate" /></s:text>
	<s:hidden  name="dspDetails.TEN_leaseStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.TEN_leaseStartDate}">
		 <s:hidden   name="dspDetails.TEN_leaseStartDate"/> 
	</c:if>
	<c:if test="${not empty dspDetails.TEN_leaseExpireDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TEN_leaseExpireDate" /></s:text>
	<s:hidden  name="dspDetails.TEN_leaseExpireDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.TEN_leaseExpireDate}">
		 <s:hidden   name="dspDetails.TEN_leaseExpireDate"/> 
	</c:if>
	<c:if test="${not empty dspDetails.TEN_expiryReminderPriorToExpiry}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.TEN_expiryReminderPriorToExpiry" /></s:text>
	<s:hidden  name="dspDetails.TEN_expiryReminderPriorToExpiry" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.TEN_expiryReminderPriorToExpiry}">
		 <s:hidden   name="dspDetails.TEN_expiryReminderPriorToExpiry"/> 
	</c:if>
	<s:hidden name="dspDetails.TEN_monthlyRentalAllowance" />
	<s:hidden name="dspDetails.TEN_securityDeposit" />
	<s:hidden name="dspDetails.TEN_leaseCurrency" />
	<s:hidden name="dspDetails.TEN_leaseSignee" />	
	</c:if>
	</c:if>			
			<!-- start ten displyOtherVendorCode -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.TEN_vendorCode)<0 && dspDetails.TEN_vendorCode !='' && dspDetails.TEN_vendorCode !=null && dspDetails.TEN_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ten')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TEN'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ten">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TEN_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('TEN_','${dspDetails.TEN_vendorCodeEXSO}'),chkIsVendorRedSky('TEN_'),changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TEN_','${dspDetails.TEN_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.TEN_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
<td align="right" width="102px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TEN_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_serviceStartDate"/></s:text>
			 <td><s:textfield id="TEN_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TEN_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_serviceStartDate}">
		<td><s:textfield id="TEN_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TEN_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onchange="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TEN_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TEN_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_serviceEndDate"/></s:text>
			 <td><s:textfield id="TEN_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TEN_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_serviceEndDate}">
		<td><s:textfield id="TEN_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TEN_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TEN_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TEN_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTEN_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TEN_displyOtherVendorCode}">
	 <c:set var="ischeckedTEN_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TEN_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTEN_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
  </tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.TEN_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr> <td></td></tr>
</tbody>
</table>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showTFMS">
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Property&nbsp;Lease&nbsp;Details</legend>
<table class="detailTabLabel">
<tr>
	<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Name</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordName" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Lease&nbsp;Start&nbsp;Date</td>
		<c:if test="${not empty dspDetails.TEN_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseStartDate"/></s:text>
			 <td width=10><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseStartDate}">
		<td width=10><s:textfield id="TEN_leaseStartDate" cssClass="input-text" name="dspDetails.TEN_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Contact&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordContactNumber" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Lease&nbsp;Expire&nbsp;Date</td>
		<c:if test="${not empty dspDetails.TEN_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_leaseExpireDate"/></s:text>
			 <td width=10><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_leaseExpireDate}">
		<td width=10><s:textfield id="TEN_leaseExpireDate" cssClass="input-text" name="dspDetails.TEN_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Landlord&nbsp;Email&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_landlordEmailAddress" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" colspan=""  width="216" class="listwhitetext" >Expiry&nbsp;Reminder&nbsp;Prior&nbsp;to&nbsp;Expiry</td>
		<c:if test="${not empty dspDetails.TEN_expiryReminderPriorToExpiry}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TEN_expiryReminderPriorToExpiry"/></s:text>
			 <td width=10><s:textfield id="TEN_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TEN_expiryReminderPriorToExpiry" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TEN_expiryReminderPriorToExpiry}">
		<td width=10><s:textfield id="TEN_expiryReminderPriorToExpiry" cssClass="input-text" name="dspDetails.TEN_expiryReminderPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="TEN_expiryReminderPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Property&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_propertyAddress" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Monthly&nbsp;Rental&nbsp;Allowance</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_monthlyRentalAllowance" readonly="false" size="10" maxlength="10" onchange="changeStatus();" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Account&nbsp;in&nbsp;the&nbsp;name&nbsp;of</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_accountHolder" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Security&nbsp;Deposit&nbsp;${currencySign}</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_securityDeposit" readonly="false" size="10" maxlength="10" onchange="changeStatus();" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Name</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankName" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Lease&nbsp;Currency</td>
	<td align="left" colspan="0"><s:select name="dspDetails.TEN_leaseCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Address</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankName" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	<td align="right" class="listwhitetext" width="83">Lease&nbsp;Signee</td>
	<td align="left" colspan="0"><s:select name="dspDetails.TEN_leaseSignee" list="%{leaseSignee}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;Account&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankAccountNumber" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Bank&nbsp;IBAN&nbsp;Number</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_bankIbanNumber" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">ABA#(9-&nbsp;digits)&nbsp;USA&nbsp;bank&nbsp;accounts</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_abaNumber" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Account&nbsp;Type</td>
	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.TEN_accountType" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
	</tr>
	</table>
	</fieldset>
</table>
</configByCorp:fieldVisibility> 
</div>
	</td>
	</tr>

	</table>
	</td>
	</tr>
	<tr><td style="height:30px;">&nbsp;</td></tr>
	</c:if>
	</c:if>	<!-- end ten --> <!-- start vis -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.VIS_vendorCode)>-1 && dspDetails.VIS_vendorCode !='' && dspDetails.VIS_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('vis')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='VIS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="vis">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.VIS_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('VIS_','${dspDetails.VIS_vendorCodeEXSO}');chkIsVendorRedSky('VIS_');changeStatus();" /></td>
			<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('VIS_','${dspDetails.VIS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.VIS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','VIS_');" src="<c:url value='/images/address2.png'/>" />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.VIS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_serviceStartDate"/></s:text>
			 <td><s:textfield id="VIS_serviceStartDate" cssClass="input-text" name="dspDetails.VIS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.VIS_serviceStartDate}">
		<td><s:textfield id="VIS_serviceStartDate" cssClass="input-text" name="dspDetails.VIS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSVisaImmigrationNotes == '0' || countDSVisaImmigrationNotes == '' || countDSVisaImmigrationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr><tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.VIS_vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.VIS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_serviceEndDate"/></s:text>
			 <td><s:textfield id="VIS_serviceEndDate" cssClass="input-text" name="dspDetails.VIS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.VIS_serviceEndDate}">
		<td><s:textfield id="VIS_serviceEndDate" cssClass="input-text" name="dspDetails.VIS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.VIS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.VIS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.VIS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedVIS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.VIS_displyOtherVendorCode}">
	 <c:set var="ischeckedVIS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.VIS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedVIS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  		</configByCorp:fieldVisibility>				
			</tr></tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Visa</legend>
<table class="detailTabLabel">
			<tr>
				<td align="right" class="listwhitetext">Visa&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="6"><s:textfield cssClass="input-text" key="dspDetails.VIS_workPermitVisaHolderName" readonly="false"  cssStyle="width:240px;" maxlength="100" onchange="changeStatus();"/></td>
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider Notification Date</td>
			    <c:if test="${not empty dspDetails.VIS_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="VIS_providerNotificationDate" cssClass="input-text" name="dspDetails.VIS_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.VIS_providerNotificationDate}">
				<td width="65px" ><s:textfield id="VIS_providerNotificationDate" cssClass="input-text" name="dspDetails.VIS_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
				<td align="right" class="listwhitetext">Immigration&nbsp;Status</td>
		    	<td align="left" colspan="2"><s:select name="dspDetails.VIS_immigrationStatus" list="%{immigrationStatus}" cssClass="list-menu" cssStyle="width:180px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
		    	</configByCorp:fieldVisibility>
				
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Visa&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.VIS_visaStartDate}">
							 <s:text id="visaStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_visaStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="VIS_visaStartDate" cssClass="input-text" name="dspDetails.VIS_visaStartDate" value="%{visaStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.VIS_visaStartDate}">
						<td width="65px" ><s:textfield id="VIS_visaStartDate" cssClass="input-text" name="dspDetails.VIS_visaStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
				<td align="right"  class="listwhitetext">Arrival&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.VIS_arrivalDate}">
					 <s:text id="arrivalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_arrivalDate"/></s:text>
					 <td width="65px" ><s:textfield id="VIS_arrivalDate" cssClass="input-text" name="dspDetails.VIS_arrivalDate" value="%{arrivalDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.VIS_arrivalDate}">
				<td width="65px" ><s:textfield id="VIS_arrivalDate" cssClass="input-text" name="dspDetails.VIS_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</configByCorp:fieldVisibility>
						
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Visa&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.VIS_visaExpiryDate}">
							 <s:text id="visaExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_visaExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="VIS_visaExpiryDate" cssClass="input-text" name="dspDetails.VIS_visaExpiryDate" value="%{visaExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.VIS_visaExpiryDate}">
						<td width="65px" ><s:textfield id="VIS_visaExpiryDate" cssClass="input-text" name="dspDetails.VIS_visaExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
												<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
					<td align="right" class="listwhitetext">Port&nbsp;of&nbsp;Entry</td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.VIS_portofEntry" readonly="false"  cssStyle="width:115px;" maxlength="100" onchange="changeStatus();"/></td>
				</configByCorp:fieldVisibility>						
						
			</tr>
			
			
			<%-- <tr>
			<td align="right"   class="listwhitetext">Expiry Reminder 3 Mos Prior To Expiry</td>
		   	<c:if test="${not empty dspDetails.VIS_expiryReminder3MosPriorToExpiry}">
			 <s:text id="expiryReminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_expiryReminder3MosPriorToExpiry"/></s:text>
			 <td width="65px" ><s:textfield id="VIS_expiryReminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.VIS_expiryReminder3MosPriorToExpiry" value="%{expiryReminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_expiryReminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
		   <c:if test="${empty dspDetails.VIS_expiryReminder3MosPriorToExpiry}">
			<td width="65px" ><s:textfield id="VIS_expiryReminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.VIS_expiryReminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_expiryReminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		 </c:if>
			</tr> --%>
	<%-- <tr>
	<td align="right"   class="listwhitetext">Application Date</td>
   	<c:if test="${not empty dspDetails.VIS_applicationDateWork}">
	 <s:text id="applicationDateWorkFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_applicationDateWork"/></s:text>
	 <td width="65px" ><s:textfield id="VIS_applicationDateWork" cssClass="input-text" name="dspDetails.VIS_applicationDateWork" value="%{applicationDateWorkFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_applicationDateWork-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.VIS_applicationDateWork}">
	<td width="65px" ><s:textfield id="VIS_applicationDateWork" cssClass="input-text" name="dspDetails.VIS_applicationDateWork" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_applicationDateWork-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr> --%>
	<%-- <tr>
	<td align="right"   class="listwhitetext">Permit Start Date</td>
   	<c:if test="${not empty dspDetails.VIS_permitStartDateWork}">
	 <s:text id="permitStartDateWorkFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_permitStartDateWork"/></s:text>
	 <td width="65px" ><s:textfield id="VIS_permitStartDateWork" cssClass="input-text" name="dspDetails.VIS_permitStartDateWork" value="%{permitStartDateWorkFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_permitStartDateWork-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.VIS_permitStartDateWork}">
	<td width="65px" ><s:textfield id="VIS_permitStartDateWork" cssClass="input-text" name="dspDetails.VIS_permitStartDateWork" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_permitStartDateWork-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr> --%>
	<%-- <tr>
	<td align="right"  class="listwhitetext">Work Permit Expiry</td>
			    <c:if test="${not empty dspDetails.VIS_workPermitExpiry}">
					 <s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_workPermitExpiry"/></s:text>
					 <td width="65px" ><s:textfield id="VIS_workPermitExpiry" cssClass="input-text" name="dspDetails.VIS_workPermitExpiry" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_workPermitExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.VIS_workPermitExpiry}">
				<td width="65px" ><s:textfield id="VIS_workPermitExpiry" cssClass="input-text" name="dspDetails.VIS_workPermitExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_workPermitExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	</tr> --%>
	<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.VIS_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="VIS_questionnaireSentDate" cssClass="input-text" name="dspDetails.VIS_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.VIS_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="VIS_questionnaireSentDate" cssClass="input-text" name="dspDetails.VIS_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr>
				<configByCorp:fieldVisibility componentId="component.Portal.visaExtensionNeeded.showVOER">
					<tr>
					<td align="right"   class="listwhitetext">Visa Extension Needed</td>
					<td align="left" colspan="0">
					<s:select name="dspDetails.VIS_visaExtensionNeeded" list="%{yesno}" cssClass="list-menu" cssStyle="width:50px" onchange="changeStatus();" tabindex=""/>
					</td>				
					</tr>
					</configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.status.VIS.HidePrimaryEmailField">
				<tr>
					<td align="right"  class="listwhitetext">Primary Email</td>
					<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" key="dspDetails.VIS_primaryEmail" readonly="false" size="18" maxlength="65" onchange="changeStatus();" /></td>
				</tr>
			</configByCorp:fieldVisibility>	
</table>
</fieldset>
</td>

</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="195" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.VIS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table>
<configByCorp:fieldVisibility componentId="component.status.VIS.HideDocumentReqSection">
<div  onClick="javascript:animatedcollapse.toggle('doc')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Document Requirements
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>						
  	<div id="doc">
  	<table border="0" class="detailTabLabel" style="border:1px solid #219DD1;width:100%;margin-top:2px;">
  	<tr>
  	<td>
  	<p style="line-height:1.8em;" class="dspdoclist">
  	<b>
  	Most important to asses all immigration:
	<br>
	*  Copy passport
	<br>
	*  Copy employment contract 
   	<br>
	Eurohome questionnaire for immigration
	</b>
	</p>
  	</td>
  	</tr>
  	</table>
  	<p></p>
  	<p align="justify" class="dspdoclist">
  	Duration of stay: within 3 months short stay visa. Over 3 months need for MVV/ VVR and workpermit.</p>
  	<p></p>
  							<c:set var="isVIS_authorizationLetter" value="false"/>
								<c:if test="${dspDetails.VIS_authorizationLetter}">
									<c:set var="isVIS_authorizationLetter" value="true"/>
								</c:if>
  							<c:set var="isVIS_copyResidence" value="false"/>
								<c:if test="${dspDetails.VIS_copyResidence}">
									<c:set var="isVIS_copyResidence" value="true"/>
								</c:if>								
  	<table style="margin-bottom:0.5em">
  	<tr><td><s:checkbox key="dspDetails.VIS_authorizationLetter" value="${isVIS_authorizationLetter}" fieldValue="true" onclick="changeStatus()" /></td>
  	<td class="dspdoclist"><font size="3">&bull;</font>&nbsp;&nbsp;Authorization letter, authorizing Eurohome to act on behalf</td></tr>
  	<tr><td></td><td><p align="justify" class="dspdoclist">For extentions:</p></td></tr>
  	<tr><td><s:checkbox key="dspDetails.VIS_copyResidence" value="${isVIS_copyResidence}" fieldValue="true" onclick="changeStatus()" /></td>
  	<td class="dspdoclist"><font size="3">&bull;</font>&nbsp;&nbsp;Copy residence permit, front and backside</td></tr>
  	</table>
  	<p align="justify" class="dspdoclist">Useful websites for residence permit and required documentation:</p>
  	<br>
  	<pre class="dspdoclist"><a href="http://www.indklantdienstwijzer.nl/default.aspx" target="_blank"><u>http://www.indklantdienstwijzer.nl/default.aspx</u></a></pre>
  	<p align="justify" class="dspdoclist">Useful website for workpermit (TWV)</p>
	<pre class="dspdoclist"><a href="http://www.arbeidsmigratie.nl" target="_blank"><u>www.arbeidsmigratie.nl</u></a> e-form</pre>
	<pre style="line-height:2em;" class="dspdoclist"><b>Procedures:<br>MVV and VVR (soon to be one procedure) <br>TWV : arbeid in loondienst (regular)<br>Knowlegde Migrant (TWV not needed)<br>Intercompany Transfer
	</b></pre>
	<table border="1" style="border:1px solid #219DD1;width:100%;margin-bottom:0.5em;">
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">MVV</font> Machtiging Voorlopig Verblijf: Entry Visa (not all nationalities require this but most of the times it is more convenient for the authorities and efficiency's sake)</pre></td></tr>
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">VVR</font> Verblijfs Vergunning Regulier:  regular residence permit</pre></td></tr>
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">TWV</font> Werkstellings Vergunning: Work permit</pre></td></tr>		
	</table>
	<p></p>
	<pre align="justify" class="dspdoclist"><b>Assignees:</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="assignees1" value="assignees1" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy payslips last 3 months</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees2" value="assignees2" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy annual salary statement</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees3" value="assignees3" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy resume</p></td></tr>  	
  	<tr><td><input type="checkbox" id="assignees4" value="assignees4" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy diplomas</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees5" value="assignees5" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* ICT for trainees: detailed trainee program, regular employment; job description</p></td></tr>  	
  	</table>
	<pre align="justify" class="dspdoclist" style="font-style:italics;">
	<i>For family: translated and legalized birth & marriage certificates. Sworn translation required for all languages other than: Dutch, English, German, French 
	Legalization depends on bi-lateral Treaties<font size="4">&rarr;</font> minbuza.nl</i> 	
	</pre>
	<p class="dspdoclist">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Depending on nationality: TBC check.</i></p>
	<p style="line-height:6px">&nbsp;</p>
	<pre align="justify" class="dspdoclist"><b>Employers</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="employers1" value="employers1" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* employment declaration, official document of IND</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers2" value="employers2" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* copy extract Chamber of Commerce - not older than 3 months -</p></td></tr>
  	<tr><td><input type="checkbox" id="employers3" value="employers3" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* total annual turnover worldwide // annual year report</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers4" value="employers4" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* trainee program (detailed) or job description</p></td></tr>
  	<tr><td><input type="checkbox" id="employers5" value="employers5" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* bank account numbers, VAT wage taxation numbers</p></td></tr>  	
  	</table>
	<p></p>
	<pre align="justify" class="dspdoclist">From employer abroad:</pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="employers6" value="employers6" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* signed assignment letter if remaining on home payroll</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers7" value="employers7" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* salary requirements:, for trainee min. &euro; 2200,= gross/ month - only holiday allowance is to be included -</p></td></tr>
  	<tr><td></td><td><p align="justify" class="dspdoclist">&nbsp;&nbsp;Regular employee: &euro; 51.239,= gross/ year - only holiday allowance to be included -</p></td></tr>
  	</table>  	
  	<p></p>
	<pre align="justify" class="dspdoclist"><b>Convenant Knowlegde Migrant:</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="convenant1" value="convenant1" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Chamber of Commerce extract - not older than 30 days -</p></td></tr>  	
  	<tr><td><input type="checkbox" id="convenant2" value="convenant2" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Statement of Payment Record issued by Tax Authorities - verklaring betalingsgedrag-</p></td></tr>
  	<tr><td><input type="checkbox" id="convenant3" value="convenant3" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Annual report, financial statements</p></td></tr>  	
  	</table>
  		</configByCorp:fieldVisibility>	
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!--</td></tr></table>
		--><!-- end vis -->
		
		
		
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.VIS_vendorCode)<0 && dspDetails.VIS_vendorCode !='' && dspDetails.VIS_vendorCode !=null && dspDetails.VIS_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">		
		<s:hidden name="dspDetails.VIS_vendorCode" />
<s:hidden name="dspDetails.VIS_visaExtensionNeeded" />
<s:hidden name="dspDetails.VIS_vendorName" />
<s:hidden name="dspDetails.VIS_vendorContact" />
<s:hidden name="dspDetails.VIS_vendorEmail" />
<s:hidden name="dspDetails.VIS_paymentResponsibility" />
<c:if test="${not empty dspDetails.VIS_serviceStartDate}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_serviceStartDate}">
		 <s:hidden   name="dspDetails.VIS_serviceStartDate"/> 
</c:if>
<c:if test="${not empty dspDetails.VIS_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.VIS_serviceEndDate}">
		 <s:hidden   name="dspDetails.VIS_serviceEndDate"/> 
	</c:if>
<s:hidden name="dspDetails.VIS_workPermitVisaHolderName" />

<c:if test="${not empty dspDetails.VIS_providerNotificationDate}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_providerNotificationDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_providerNotificationDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_providerNotificationDate}">
		 <s:hidden   name="dspDetails.VIS_providerNotificationDate"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_visaExpiryDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_visaExpiryDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_visaExpiryDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_visaExpiryDate}">
		 <s:hidden   name="dspDetails.VIS_visaExpiryDate"/> 
</c:if>
<c:if test="${not empty dspDetails.VIS_workPermitExpiry}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_workPermitExpiry" /></s:text>
	<s:hidden  name="dspDetails.VIS_workPermitExpiry" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_workPermitExpiry}">
		 <s:hidden   name="dspDetails.VIS_workPermitExpiry"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_expiryReminder3MosPriorToExpiry}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_expiryReminder3MosPriorToExpiry" /></s:text>
	<s:hidden  name="dspDetails.VIS_expiryReminder3MosPriorToExpiry" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_expiryReminder3MosPriorToExpiry}">
		 <s:hidden   name="dspDetails.VIS_expiryReminder3MosPriorToExpiry"/> 
</c:if>
<c:if test="${not empty dspDetails.VIS_visaStartDate}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_visaStartDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_visaStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_visaStartDate}">
		 <s:hidden   name="dspDetails.VIS_visaStartDate"/> 
</c:if>

<s:hidden name="dspDetails.VIS_assignees" />
<s:hidden name="dspDetails.VIS_employers" />
<s:hidden name="dspDetails.VIS_convenant" />
<s:hidden name="dspDetails.VIS_residencePermitHolderNameRP" />
<c:if test="${not empty dspDetails.VIS_providerNotificationDateRP}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_providerNotificationDateRP" /></s:text>
	<s:hidden  name="dspDetails.VIS_providerNotificationDateRP" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_providerNotificationDateRP}">
		 <s:hidden   name="dspDetails.VIS_providerNotificationDateRP"/> 
</c:if>
<c:if test="${not empty dspDetails.VIS_visaExpiryDateRP}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_visaExpiryDateRP" /></s:text>
	<s:hidden  name="dspDetails.VIS_visaExpiryDateRP" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_visaExpiryDateRP}">
		 <s:hidden   name="dspDetails.VIS_visaExpiryDateRP"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_expiryReminder3MosPriorToExpiryRP}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_expiryReminder3MosPriorToExpiryRP" /></s:text>
	<s:hidden  name="dspDetails.VIS_expiryReminder3MosPriorToExpiryRP" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_expiryReminder3MosPriorToExpiryRP}">
		 <s:hidden   name="dspDetails.VIS_expiryReminder3MosPriorToExpiryRP"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_residenceExpiryRP}">	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_residenceExpiryRP" /></s:text>
	<s:hidden  name="dspDetails.VIS_residenceExpiryRP" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_residenceExpiryRP}">
		 <s:hidden   name="dspDetails.VIS_residenceExpiryRP"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_questionnaireSentDate}">
	<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_questionnaireSentDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_questionnaireSentDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.VIS_questionnaireSentDate}">
		 <s:hidden   name="dspDetails.VIS_questionnaireSentDate"/> 
	</c:if>
<s:hidden name="dspDetails.VIS_primaryEmail" />
<s:hidden name="dspDetails.VIS_authorizationLetter" />
<s:hidden name="dspDetails.VIS_copyResidence" />
<s:hidden name="dspDetails.VIS_vendorCodeEXSO" />
<c:if test="${not empty dspDetails.VIS_applicationDateWork}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_applicationDateWork" /></s:text>
	<s:hidden  name="dspDetails.VIS_applicationDateWork" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_applicationDateWork}">
		 <s:hidden   name="dspDetails.VIS_applicationDateWork"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_permitStartDateWork}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_permitStartDateWork" /></s:text>
	<s:hidden  name="dspDetails.VIS_permitStartDateWork" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
<c:if test="${empty dspDetails.VIS_permitStartDateWork}">
		 <s:hidden   name="dspDetails.VIS_permitStartDateWork"/> 
</c:if>

<c:if test="${not empty dspDetails.VIS_applicationDateResidence}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_applicationDateResidence" /></s:text>
	<s:hidden  name="dspDetails.VIS_applicationDateResidence" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_applicationDateResidence}">
		 <s:hidden   name="dspDetails.VIS_applicationDateResidence"/> 
</c:if>	

<c:if test="${not empty dspDetails.VIS_permitStartDateResidence}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_permitStartDateResidence" /></s:text>
	<s:hidden  name="dspDetails.VIS_permitStartDateResidence" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_permitStartDateResidence}">
		 <s:hidden   name="dspDetails.VIS_permitStartDateResidence"/> 
</c:if>


<s:hidden name="dspDetails.VIS_comment" />
<s:hidden name="dspDetails.VIS_displyOtherVendorCode" />
<s:hidden name="dspDetails.VIS_portofEntry" />
<c:if test="${not empty dspDetails.VIS_arrivalDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.VIS_arrivalDate" /></s:text>
	<s:hidden  name="dspDetails.VIS_arrivalDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.VIS_arrivalDate}">
		 <s:hidden   name="dspDetails.VIS_arrivalDate"/> 
</c:if>
<s:hidden name="dspDetails.VIS_immigrationStatus" />
   
		</c:if>
		</c:if>
		<!-- start vis displyOtherVendorCode-->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.VIS_vendorCode)<0 && dspDetails.VIS_vendorCode !='' && dspDetails.VIS_vendorCode !=null && dspDetails.VIS_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('vis')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='VIS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="vis">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.VIS_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('VIS_','${dspDetails.VIS_vendorCodeEXSO}');chkIsVendorRedSky('VIS_');changeStatus();" /></td>
			<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('VIS_','${dspDetails.VIS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.VIS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.VIS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_serviceStartDate"/></s:text>
			 <td><s:textfield id="VIS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.VIS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.VIS_serviceStartDate}">
		<td><s:textfield id="VIS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.VIS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
		<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSVisaImmigrationNotes == '0' || countDSVisaImmigrationNotes == '' || countDSVisaImmigrationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		</tr>
<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.VIS_vendorContact" readonly="true" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.VIS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_serviceEndDate"/></s:text>
			 <td><s:textfield id="VIS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.VIS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.VIS_serviceEndDate}">
		<td><s:textfield id="VIS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.VIS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.VIS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.VIS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedVIS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.VIS_displyOtherVendorCode}">
	 <c:set var="ischeckedVIS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.VIS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedVIS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td> 
	</configByCorp:fieldVisibility>			
			</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>VISA</legend>
<table class="detailTabLabel">
			<tr>
				<td align="right" class="listwhitetext">Visa&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="6"><s:textfield cssClass="input-text" key="dspDetails.VIS_workPermitVisaHolderName" readonly="false"  cssStyle="width:240px;" maxlength="100" onchange="changeStatus();"/></td>
				
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider&nbsp;Notification&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.VIS_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="VIS_providerNotificationDate" cssClass="input-text" name="dspDetails.VIS_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.VIS_providerNotificationDate}">
				<td width="65px" ><s:textfield id="VIS_providerNotificationDate" cssClass="input-text" name="dspDetails.VIS_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
				<td align="right" class="listwhitetext">Immigration&nbsp;Status</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.VIS_immigrationStatus" list="%{immigrationStatus}" cssClass="list-menu" cssStyle="width:180px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
		    	</configByCorp:fieldVisibility>
				
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Visa&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.VIS_visaStartDate}">
							 <s:text id="visaStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_visaStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="VIS_visaStartDate" cssClass="input-text" name="dspDetails.VIS_visaStartDate" value="%{visaStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.VIS_visaStartDate}">
						<td width="65px" ><s:textfield id="VIS_visaStartDate" cssClass="input-text" name="dspDetails.VIS_visaStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
				<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
				<td align="right"  class="listwhitetext">Arrival&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.VIS_arrivalDate}">
					 <s:text id="arrivalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_arrivalDate"/></s:text>
					 <td width="65px" ><s:textfield id="VIS_arrivalDate" cssClass="input-text" name="dspDetails.VIS_arrivalDate" value="%{arrivalDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.VIS_arrivalDate}">
				<td width="65px" ><s:textfield id="VIS_arrivalDate" cssClass="input-text" name="dspDetails.VIS_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</configByCorp:fieldVisibility>				
						
					
			</tr> 
			<tr>			
				<td align="right"   class="listwhitetext">Visa&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.VIS_visaExpiryDate}">
							 <s:text id="visaExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_visaExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="VIS_visaExpiryDate" cssClass="input-text" name="dspDetails.VIS_visaExpiryDate" value="%{visaExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.VIS_visaExpiryDate}">
						<td width="65px" ><s:textfield id="VIS_visaExpiryDate" cssClass="input-text" name="dspDetails.VIS_visaExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_visaExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
					<td align="right" class="listwhitetext">Port&nbsp;of&nbsp;Entry</td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.VIS_portofEntry" readonly="false"  cssStyle="width:115px;" maxlength="100" onchange="changeStatus();"/></td>
				</configByCorp:fieldVisibility>							
			</tr> 
	<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.VIS_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="VIS_questionnaireSentDate" cssClass="input-text" name="dspDetails.VIS_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.VIS_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="VIS_questionnaireSentDate" cssClass="input-text" name="dspDetails.VIS_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="VIS_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr> 
				<configByCorp:fieldVisibility componentId="component.Portal.visaExtensionNeeded.showVOER">
								<tr>
					<td align="right"   class="listwhitetext">Visa Extension Needed</td>
					<td align="left" colspan="0">
					<s:select name="dspDetails.VIS_visaExtensionNeeded" list="%{yesno}" cssClass="list-menu" cssStyle="width:50px" onchange="changeStatus();" tabindex=""/>
					</td>				
					</tr>
					</configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.status.VIS.HidePrimaryEmailField">
				<tr>
					<td align="right"  class="listwhitetext">Primary Email</td>
					<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-textUpper" key="dspDetails.VIS_primaryEmail" readonly="true" size="18" maxlength="65" onchange="changeStatus();" /></td>
				</tr>
			</configByCorp:fieldVisibility>	
</table>
</fieldset>
</td>
</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="195" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.VIS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table>
<configByCorp:fieldVisibility componentId="component.status.VIS.HideDocumentReqSection">
<div  onClick="javascript:animatedcollapse.toggle('doc')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Document Requirements
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>						
  	<div id="doc">
  	<table border="0" class="detailTabLabel" style="border:1px solid #219DD1;width:100%;margin-top:2px;">
  	<tr>
  	<td>
  	<p style="line-height:1.8em;" class="dspdoclist">
  	<b>
  	Most important to asses all immigration:
	<br>
	*  Copy passport
	<br>
	*  Copy employment contract 
   	<br>
	Eurohome questionnaire for immigration
	</b>
	</p>
  	</td>
  	</tr>
  	</table>
  	<p></p>
  	<p align="justify" class="dspdoclist">
  	Duration of stay: within 3 months short stay visa. Over 3 months need for MVV/ VVR and workpermit.</p>
  	<p></p>
  							<c:set var="isVIS_authorizationLetter" value="false"/>
								<c:if test="${dspDetails.VIS_authorizationLetter}">
									<c:set var="isVIS_authorizationLetter" value="true"/>
								</c:if>
  							<c:set var="isVIS_copyResidence" value="false"/>
								<c:if test="${dspDetails.VIS_copyResidence}">
									<c:set var="isVIS_copyResidence" value="true"/>
								</c:if>								
  	<table style="margin-bottom:0.5em">
  	<tr><td><s:checkbox key="dspDetails.VIS_authorizationLetter" value="${isVIS_authorizationLetter}" fieldValue="true" onclick="changeStatus()" /></td>
  	<td class="dspdoclist"><font size="3">&bull;</font>&nbsp;&nbsp;Authorization letter, authorizing Eurohome to act on behalf</td></tr>
  	<tr><td></td><td><p align="justify" class="dspdoclist">For extentions:</p></td></tr>
  	<tr><td><s:checkbox key="dspDetails.VIS_copyResidence" value="${isVIS_copyResidence}" fieldValue="true" onclick="changeStatus()" /></td>
  	<td class="dspdoclist"><font size="3">&bull;</font>&nbsp;&nbsp;Copy residence permit, front and backside</td></tr>
  	</table>
  	<p align="justify" class="dspdoclist">Useful websites for residence permit and required documentation:</p>
  	<br>
  	<pre class="dspdoclist"><a href="http://www.indklantdienstwijzer.nl/default.aspx" target="_blank"><u>http://www.indklantdienstwijzer.nl/default.aspx</u></a></pre>
  	<p align="justify" class="dspdoclist">Useful website for workpermit (TWV)</p>
	<pre class="dspdoclist"><a href="http://www.arbeidsmigratie.nl" target="_blank"><u>www.arbeidsmigratie.nl</u></a> e-form</pre>
	<pre style="line-height:2em;" class="dspdoclist"><b>Procedures:<br>MVV and VVR (soon to be one procedure) <br>TWV : arbeid in loondienst (regular)<br>Knowlegde Migrant (TWV not needed)<br>Intercompany Transfer
	</b></pre>
	<table border="1" style="border:1px solid #219DD1;width:100%;margin-bottom:0.5em;">
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">MVV</font> Machtiging Voorlopig Verblijf: Entry Visa (not all nationalities require this but most of the times it is more convenient for the authorities and efficiency's sake)</pre></td></tr>
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">VVR</font> Verblijfs Vergunning Regulier:  regular residence permit</pre></td></tr>
	<tr><td style="border:1px solid #E0E0E0;padding-left:5px;"><pre align="justify" class="dspdoclist"><font color="#003366">TWV</font> Werkstellings Vergunning: Work permit</pre></td></tr>		
	</table>
	<p></p>
	<pre align="justify" class="dspdoclist"><b>Assignees:</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="assignees1" value="assignees1" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy payslips last 3 months</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees2" value="assignees2" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy annual salary statement</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees3" value="assignees3" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy resume</p></td></tr>  	
  	<tr><td><input type="checkbox" id="assignees4" value="assignees4" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* copy diplomas</p></td></tr>
  	<tr><td><input type="checkbox" id="assignees5" value="assignees5" onclick="selectColumn1(this);"/></td><td><p align="justify" class="dspdoclist">* ICT for trainees: detailed trainee program, regular employment; job description</p></td></tr>  	
  	</table>
	<pre align="justify" class="dspdoclist" style="font-style:italics;">
	<i>For family: translated and legalized birth & marriage certificates. Sworn translation required for all languages other than: Dutch, English, German, French 
	Legalization depends on bi-lateral Treaties<font size="4">&rarr;</font> minbuza.nl</i> 	
	</pre>
	<p class="dspdoclist">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Depending on nationality: TBC check.</i></p>
	<p style="line-height:6px">&nbsp;</p>
	<pre align="justify" class="dspdoclist"><b>Employers</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="employers1" value="employers1" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* employment declaration, official document of IND</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers2" value="employers2" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* copy extract Chamber of Commerce - not older than 3 months -</p></td></tr>
  	<tr><td><input type="checkbox" id="employers3" value="employers3" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* total annual turnover worldwide // annual year report</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers4" value="employers4" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* trainee program (detailed) or job description</p></td></tr>
  	<tr><td><input type="checkbox" id="employers5" value="employers5" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* bank account numbers, VAT wage taxation numbers</p></td></tr>  	
  	</table>
	<p></p>
	<pre align="justify" class="dspdoclist">From employer abroad:</pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="employers6" value="employers6" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* signed assignment letter if remaining on home payroll</p></td></tr>  	
  	<tr><td><input type="checkbox" id="employers7" value="employers7" onclick="selectColumn2(this);"/></td><td><p align="justify" class="dspdoclist">* salary requirements:, for trainee min. &euro; 2200,= gross/ month - only holiday allowance is to be included -</p></td></tr>
  	<tr><td></td><td><p align="justify" class="dspdoclist">&nbsp;&nbsp;Regular employee: &euro; 51.239,= gross/ year - only holiday allowance to be included -</p></td></tr>
  	</table>  	
  	<p></p>
	<pre align="justify" class="dspdoclist"><b>Convenant Knowlegde Migrant:</b></pre>
  	<table style="margin-bottom:0.5em">
  	<tr><td><input type="checkbox" id="convenant1" value="convenant1" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Chamber of Commerce extract - not older than 30 days -</p></td></tr>  	
  	<tr><td><input type="checkbox" id="convenant2" value="convenant2" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Statement of Payment Record issued by Tax Authorities - verklaring betalingsgedrag-</p></td></tr>
  	<tr><td><input type="checkbox" id="convenant3" value="convenant3" onclick="selectColumn3(this);"/></td><td><p align="justify" class="dspdoclist">* Annual report, financial statements</p></td></tr>  	
  	</table>
  		</configByCorp:fieldVisibility>	
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	
	<!-- start wop -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.WOP_vendorCode)>-1 && dspDetails.WOP_vendorCode !='' && dspDetails.WOP_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('wop')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='WOP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="wop">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.WOP_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('WOP_','${dspDetails.WOP_vendorCodeEXSO}');chkIsVendorRedSky('WOP_');changeStatus();" /></td>
			<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('VIS_','${dspDetails.VIS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.WOP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','WOP_');" src="<c:url value='/images/address2.png'/>" />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.WOP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_serviceStartDate"/></s:text>
			 <td><s:textfield id="WOP_serviceStartDate" cssClass="input-text" name="dspDetails.WOP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.WOP_serviceStartDate}">
		<td><s:textfield id="WOP_serviceStartDate" cssClass="input-text" name="dspDetails.WOP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSWorkPermitNotes == '0' || countDSWorkPermitNotes == '' || countDSWorkPermitNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSWorkPermitNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsWorkPermit&imageId=countDSWorkPermitNotesImage&fieldId=countDSWorkPermitNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsWorkPermit&imageId=countDSWorkPermitNotesImage&fieldId=countDSWorkPermitNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSWorkPermitNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsWorkPermit&imageId=countDSWorkPermitNotesImage&fieldId=countDSWorkPermitNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsWorkPermit&imageId=countDSWorkPermitNotesImage&fieldId=countDSWorkPermitNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr><tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.WOP_vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.WOP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_serviceEndDate"/></s:text>
			 <td><s:textfield id="WOP_serviceEndDate" cssClass="input-text" name="dspDetails.WOP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.WOP_serviceEndDate}">
		<td><s:textfield id="WOP_serviceEndDate" cssClass="input-text" name="dspDetails.WOP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.WOP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.WOP_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.WOP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedWOP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.WOP_displyOtherVendorCode}">
	 <c:set var="ischeckedWOP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.WOP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedWOP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  		</configByCorp:fieldVisibility>				
			</tr></tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Work&nbsp;Permit</legend>
<table class="detailTabLabel">
			<tr>
				<td align="right" class="listwhitetext">Work&nbsp;Permit&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.WOP_workPermitHolderName" readonly="false"  cssStyle="width:195px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext" style="padding-left:45px;">Lease&nbsp;Extension&nbsp;Needed</td>
    			<td  class="listwhitetext" >
    				<s:select list="{'','No','Yes'}" name="dspDetails.WOP_leaseExNeeded" cssStyle="width:50px;"  cssClass="list-menu"/>
    			</td>
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider Notification Date</td>
			    <c:if test="${not empty dspDetails.WOP_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="WOP_providerNotificationDate" cssClass="input-text" name="dspDetails.WOP_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.WOP_providerNotificationDate}">
				<td width="65px" ><s:textfield id="WOP_providerNotificationDate" cssClass="input-text" name="dspDetails.WOP_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.WOP_permitStartDate}">
							 <s:text id="permitStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_permitStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="WOP_permitStartDate" cssClass="input-text" name="dspDetails.WOP_permitStartDate" value="%{permitStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.WOP_permitStartDate}">
						<td width="65px" ><s:textfield id="WOP_permitStartDate" cssClass="input-text" name="dspDetails.WOP_permitStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.WOP_permitExpiryDate}">
							 <s:text id="permitExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_permitExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="WOP_permitExpiryDate" cssClass="input-text" name="dspDetails.WOP_permitExpiryDate" value="%{permitExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.WOP_permitExpiryDate}">
						<td width="65px" ><s:textfield id="WOP_permitExpiryDate" cssClass="input-text" name="dspDetails.WOP_permitExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminder3MosPriorToExpiry">
			<tr>
			<td align="right"   class="listwhitetext">Reminder 3 Mos Prior To Expiry</td>
		   	<c:if test="${not empty dspDetails.WOP_reminder3MosPriorToExpiry}">
			 <s:text id="reminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_reminder3MosPriorToExpiry"/></s:text>
			 <td width="65px" ><s:textfield id="WOP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.WOP_reminder3MosPriorToExpiry" value="%{reminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
		   <c:if test="${empty dspDetails.WOP_reminder3MosPriorToExpiry}">
			<td width="65px" ><s:textfield id="WOP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.WOP_reminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		 </c:if>
			</tr>
			</configByCorp:fieldVisibility>
			<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.WOP_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="WOP_questionnaireSentDate" cssClass="input-text" name="dspDetails.WOP_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.WOP_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="WOP_questionnaireSentDate" cssClass="input-text" name="dspDetails.WOP_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr> 
				 
</table>
</fieldset>
</td>
</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="150" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.WOP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table> 
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!-- end wop -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.WOP_vendorCode)<0 && dspDetails.WOP_vendorCode !='' && dspDetails.WOP_vendorCode !=null && dspDetails.WOP_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">		
		<s:hidden name="dspDetails.WOP_vendorCode" />
<s:hidden name="dspDetails.WOP_vendorName" />
<s:hidden name="dspDetails.WOP_vendorContact" />
<s:hidden name="dspDetails.WOP_vendorEmail" />
<s:hidden name="dspDetails.WOP_paymentResponsibility" />
<s:hidden name="dspDetails.WOP_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.WOP_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_serviceStartDate}">
		 <s:hidden   name="dspDetails.WOP_serviceStartDate"/> 
</c:if>

<c:if test="${not empty dspDetails.WOP_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_serviceEndDate}">
		 <s:hidden   name="dspDetails.WOP_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.WOP_workPermitHolderName" />
<c:if test="${not empty dspDetails.WOP_providerNotificationDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_providerNotificationDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_providerNotificationDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_providerNotificationDate}">
		 <s:hidden   name="dspDetails.WOP_providerNotificationDate"/> 
</c:if>

<c:if test="${not empty dspDetails.WOP_permitStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_permitStartDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_permitStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_permitStartDate}">
		 <s:hidden   name="dspDetails.WOP_permitStartDate"/> 
</c:if>
<c:if test="${not empty dspDetails.WOP_reminder3MosPriorToExpiry}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_reminder3MosPriorToExpiry" /></s:text>
	<s:hidden  name="dspDetails.WOP_reminder3MosPriorToExpiry" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_reminder3MosPriorToExpiry}">
		 <s:hidden   name="dspDetails.WOP_reminder3MosPriorToExpiry"/> 
</c:if>
<c:if test="${not empty dspDetails.WOP_permitExpiryDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_permitExpiryDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_permitExpiryDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_permitExpiryDate}">
		 <s:hidden   name="dspDetails.WOP_permitExpiryDate"/> 
</c:if>
<c:if test="${not empty dspDetails.WOP_questionnaireSentDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.WOP_questionnaireSentDate" /></s:text>
	<s:hidden  name="dspDetails.WOP_questionnaireSentDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.WOP_questionnaireSentDate}">
		 <s:hidden   name="dspDetails.WOP_questionnaireSentDate"/> 
</c:if>
<s:hidden name="dspDetails.WOP_vendorCodeEXSO" />
<s:hidden name="dspDetails.WOP_comment" />
<s:hidden name="WOP_displyOtherVendorCode" />
		</c:if>
		</c:if> 
	 
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.WOP_vendorCode)<0 && dspDetails.WOP_vendorCode !='' && dspDetails.WOP_vendorCode !=null && dspDetails.WOP_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('wop')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='WOP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="wop">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>

			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.WOP_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('WOP_','${dspDetails.WOP_vendorCodeEXSO}');chkIsVendorRedSky('WOP_');changeStatus();" /></td>
			<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('VIS_','${dspDetails.VIS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.WOP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.WOP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_serviceStartDate"/></s:text>
			 <td><s:textfield id="WOP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.WOP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.WOP_serviceStartDate}">
		<td><s:textfield id="WOP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.WOP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.WOP_vendorContact" readonly="true" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.WOP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_serviceEndDate"/></s:text>
			 <td><s:textfield id="WOP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.WOP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.WOP_serviceEndDate}">
		<td><s:textfield id="WOP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.WOP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.WOP_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.WOP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedWOP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.WOP_displyOtherVendorCode}">
	 <c:set var="ischeckedWOP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.WOP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedWOP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td> 
	</configByCorp:fieldVisibility>			
			</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
 <table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Work&nbsp;Permit</legend>
<table class="detailTabLabel">
			 <tr>
				<td align="right" class="listwhitetext">Work&nbsp;Permit&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.WOP_workPermitHolderName" readonly="false"  cssStyle="width:195px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext" style="padding-left:45px;">Lease&nbsp;Extension&nbsp;Needed</td>
    			<td  class="listwhitetext" >
    				<s:select list="{'','No','Yes'}" disabled="true"  name="dspDetails.WOP_leaseExNeeded" cssStyle="width:50px;"  cssClass="list-menu"/>
    			</td>
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider Notification Date</td>
			    <c:if test="${not empty dspDetails.WOP_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="WOP_providerNotificationDate" cssClass="input-text" name="dspDetails.WOP_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.WOP_providerNotificationDate}">
				<td width="65px" ><s:textfield id="WOP_providerNotificationDate" cssClass="input-text" name="dspDetails.WOP_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr> 
			  <tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.WOP_permitStartDate}">
							 <s:text id="permitStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_permitStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="WOP_permitStartDate" cssClass="input-text" name="dspDetails.WOP_permitStartDate" value="%{permitStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.WOP_permitStartDate}">
						<td width="65px" ><s:textfield id="WOP_permitStartDate" cssClass="input-text" name="dspDetails.WOP_permitStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr> 
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.WOP_permitExpiryDate}">
							 <s:text id="permitExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_permitExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="WOP_permitExpiryDate" cssClass="input-text" name="dspDetails.WOP_permitExpiryDate" value="%{permitExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.WOP_permitExpiryDate}">
						<td width="65px" ><s:textfield id="WOP_permitExpiryDate" cssClass="input-text" name="dspDetails.WOP_permitExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminder3MosPriorToExpiry">
			<tr>
			<td align="right"   class="listwhitetext">Reminder 3 Mos Prior To Expiry</td>
		   	<c:if test="${not empty dspDetails.WOP_reminder3MosPriorToExpiry}">
			 <s:text id="reminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_reminder3MosPriorToExpiry"/></s:text>
			 <td width="65px" ><s:textfield id="WOP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.WOP_reminder3MosPriorToExpiry" value="%{reminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
		   <c:if test="${empty dspDetails.WOP_reminder3MosPriorToExpiry}">
			<td width="65px" ><s:textfield id="WOP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.WOP_reminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		 </c:if>
			</tr>
			</configByCorp:fieldVisibility>
			<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.WOP_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.WOP_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="WOP_questionnaireSentDate" cssClass="input-text" name="dspDetails.WOP_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.WOP_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="WOP_questionnaireSentDate" cssClass="input-text" name="dspDetails.WOP_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="WOP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</tr> 
</table>
</fieldset>
</td>
<td>
</td>
</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="195" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.WOP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table> 	
  	</div>
  
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!-- end wop -->
	<!-- start rep -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.REP_vendorCode)>-1 && dspDetails.REP_vendorCode !='' && dspDetails.REP_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rep')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='REP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rep">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.REP_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('REP_','${dspDetails.REP_vendorCodeEXSO}');chkIsVendorRedSky('REP_');changeStatus();" /></td>
			<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('VIS_','${dspDetails.VIS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.REP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','REP_');" src="<c:url value='/images/address2.png'/>" />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.REP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_serviceStartDate"/></s:text>
			 <td><s:textfield id="REP_serviceStartDate" cssClass="input-text" name="dspDetails.REP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.REP_serviceStartDate}">
		<td><s:textfield id="REP_serviceStartDate" cssClass="input-text" name="dspDetails.REP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSResidencePermitNotes == '0' || countDSResidencePermitNotes == '' || countDSResidencePermitNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSResidencePermitNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsResidencePermit&imageId=countDSResidencePermitNotesImage&fieldId=countDSResidencePermitNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsResidencePermit&imageId=countDSResidencePermitNotesImage&fieldId=countDSResidencePermitNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSResidencePermitNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsResidencePermit&imageId=countDSResidencePermitNotesImage&fieldId=countDSResidencePermitNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsResidencePermit&imageId=countDSResidencePermitNotesImage&fieldId=countDSResidencePermitNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr><tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.REP_vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.REP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_serviceEndDate"/></s:text>
			 <td><s:textfield id="REP_serviceEndDate" cssClass="input-text" name="dspDetails.REP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.REP_serviceEndDate}">
		<td><s:textfield id="REP_serviceEndDate" cssClass="input-text" name="dspDetails.REP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.REP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.REP_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.REP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedREP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.REP_displyOtherVendorCode}">
	 <c:set var="ischeckedREP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.REP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedREP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  		</configByCorp:fieldVisibility>				
			</tr></tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Residence&nbsp;Permit</legend>
<table class="detailTabLabel">
			<tr>
				<td align="right" class="listwhitetext">Residence&nbsp;Permit&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.REP_workPermitHolderName" readonly="false"  cssStyle="width:195px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext" style="padding-left:45px;">Lease&nbsp;Extension&nbsp;Needed</td>
    			<td  class="listwhitetext" >
    				<s:select list="{'','No','Yes'}" name="dspDetails.REP_leaseExNeeded" cssStyle="width:50px;"  cssClass="list-menu"/>
    			</td>
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider&nbsp;Notification&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.REP_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="REP_providerNotificationDate" cssClass="input-text" name="dspDetails.REP_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.REP_providerNotificationDate}">
				<td width="65px" ><s:textfield id="REP_providerNotificationDate" cssClass="input-text" name="dspDetails.REP_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr>
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.REP_permitStartDate}">
							 <s:text id="permitStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_permitStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="REP_permitStartDate" cssClass="input-text" name="dspDetails.REP_permitStartDate" value="%{permitStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.REP_permitStartDate}">
						<td width="65px" ><s:textfield id="REP_permitStartDate" cssClass="input-text" name="dspDetails.REP_permitStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.REP_permitExpiryDate}">
							 <s:text id="permitExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_permitExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="REP_permitExpiryDate" cssClass="input-text" name="dspDetails.REP_permitExpiryDate" value="%{permitExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.REP_permitExpiryDate}">
						<td width="65px" ><s:textfield id="REP_permitExpiryDate" cssClass="input-text" name="dspDetails.REP_permitExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
				
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminder3MosPriorToExpiry">
			<tr>
			<td align="right"   class="listwhitetext">Reminder 3 Mos Prior To Expiry</td>
		   	<c:if test="${not empty dspDetails.REP_reminder3MosPriorToExpiry}">
			 <s:text id="reminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_reminder3MosPriorToExpiry"/></s:text>
			 <td width="65px" ><s:textfield id="REP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.REP_reminder3MosPriorToExpiry" value="%{reminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
		   <c:if test="${empty dspDetails.REP_reminder3MosPriorToExpiry}">
			<td width="65px" ><s:textfield id="REP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.REP_reminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		 </c:if>
			</tr>
			</configByCorp:fieldVisibility>
			
			<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.REP_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="REP_questionnaireSentDate" cssClass="input-text" name="dspDetails.REP_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.REP_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="REP_questionnaireSentDate" cssClass="input-text" name="dspDetails.REP_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr> 
</table>
</fieldset>
</td>

</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="150" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.REP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table>
	
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!-- end rep -->
		
	 	
		
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.REP_vendorCode)<0 && dspDetails.REP_vendorCode !='' && dspDetails.REP_vendorCode !=null && dspDetails.REP_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">		
		<s:hidden name="dspDetails.REP_vendorCode" />
<s:hidden name="dspDetails.REP_vendorName" />
<s:hidden name="dspDetails.REP_vendorContact" />
<s:hidden name="dspDetails.REP_vendorEmail" />
<s:hidden name="dspDetails.REP_paymentResponsibility" />
<s:hidden name="dspDetails.REP_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.REP_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.REP_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_serviceStartDate}">
		 <s:hidden   name="dspDetails.REP_serviceStartDate"/> 
</c:if>
<c:if test="${not empty dspDetails.REP_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.REP_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_serviceEndDate}">
		 <s:hidden   name="dspDetails.REP_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.REP_workPermitHolderName" />
<c:if test="${not empty dspDetails.REP_providerNotificationDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_providerNotificationDate" /></s:text>
	<s:hidden  name="dspDetails.REP_providerNotificationDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_providerNotificationDate}">
		 <s:hidden   name="dspDetails.REP_providerNotificationDate"/> 
</c:if>
<c:if test="${not empty dspDetails.REP_permitStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_permitStartDate" /></s:text>
	<s:hidden  name="dspDetails.REP_permitStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_permitStartDate}">
		 <s:hidden   name="dspDetails.REP_permitStartDate"/> 
</c:if>
<c:if test="${not empty dspDetails.REP_reminder3MosPriorToExpiry}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_reminder3MosPriorToExpiry" /></s:text>
	<s:hidden  name="dspDetails.REP_reminder3MosPriorToExpiry" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_reminder3MosPriorToExpiry}">
		 <s:hidden   name="dspDetails.REP_reminder3MosPriorToExpiry"/> 
</c:if>
<c:if test="${not empty dspDetails.REP_permitExpiryDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_permitExpiryDate" /></s:text>
	<s:hidden  name="dspDetails.REP_permitExpiryDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_permitExpiryDate}">
		 <s:hidden   name="dspDetails.REP_permitExpiryDate"/> 
</c:if>
<c:if test="${not empty dspDetails.REP_questionnaireSentDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.REP_questionnaireSentDate" /></s:text>
	<s:hidden  name="dspDetails.REP_questionnaireSentDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.REP_questionnaireSentDate}">
		 <s:hidden   name="dspDetails.REP_questionnaireSentDate"/> 
</c:if>
<s:hidden name="dspDetails.REP_vendorCodeEXSO" />
<s:hidden name="dspDetails.REP_comment" />
<s:hidden name="REP_displyOtherVendorCode" />
		</c:if>
		</c:if>
		
		 
		<!-- start rep displyOtherVendorCode-->
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.REP_vendorCode)<0 && dspDetails.REP_vendorCode !='' && dspDetails.REP_vendorCode !=null && dspDetails.REP_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rep')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='REP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rep">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>

			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.REP_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('REP_','${dspDetails.REP_vendorCodeEXSO}');chkIsVendorRedSky('REP_');changeStatus();" /></td>
			<td align="left"width="10"> 
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.REP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.REP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_serviceStartDate"/></s:text>
			 <td><s:textfield id="REP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.REP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.REP_serviceStartDate}">
		<td><s:textfield id="REP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.REP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.REP_vendorContact" readonly="true" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.REP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_serviceEndDate"/></s:text>
			 <td><s:textfield id="REP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.REP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.REP_serviceEndDate}">
		<td><s:textfield id="REP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.REP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.REP_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
			<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
			<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.REP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedREP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.REP_displyOtherVendorCode}">
	 <c:set var="ischeckedREP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.REP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedREP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td> 
	</configByCorp:fieldVisibility>			
			</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
	<tr>
	<td>
	<fieldset>
	<legend>Residence&nbsp;Permit</legend>
<table class="detailTabLabel">
			 <tr>
				<td align="right" class="listwhitetext">Residence&nbsp;Permit&nbsp;Holder&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.REP_workPermitHolderName" readonly="false"  cssStyle="width:195px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext" style="padding-left:45px;">Lease&nbsp;Extension&nbsp;Needed</td>
    			<td  class="listwhitetext" >
    				<s:select list="{'','No','Yes'}" disabled="true" name="dspDetails.REP_leaseExNeeded"  cssStyle="width:50px;"  cssClass="list-menu"/>
    			</td>
				</tr>
				<tr>
				<td align="right"  class="listwhitetext">Provider&nbsp;Notification&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.REP_providerNotificationDate}">
					 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_providerNotificationDate"/></s:text>
					 <td width="65px" ><s:textfield id="REP_providerNotificationDate" cssClass="input-text" name="dspDetails.REP_providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.REP_providerNotificationDate}">
				<td width="65px" ><s:textfield id="REP_providerNotificationDate" cssClass="input-text" name="dspDetails.REP_providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_providerNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr>
			  <tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Start&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.REP_permitStartDate}">
							 <s:text id="permitStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_permitStartDate"/></s:text>
							 <td width="65px" ><s:textfield id="REP_permitStartDate" cssClass="input-text" name="dspDetails.REP_permitStartDate" value="%{permitStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.REP_permitStartDate}">
						<td width="65px" ><s:textfield id="REP_permitStartDate" cssClass="input-text" name="dspDetails.REP_permitStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			
			<tr>			
				<td align="right"   class="listwhitetext">Permit&nbsp;Expiry&nbsp;Date </td>
					    <c:if test="${not empty dspDetails.REP_permitExpiryDate}">
							 <s:text id="permitExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_permitExpiryDate"/></s:text>
							 <td width="65px" ><s:textfield id="REP_permitExpiryDate" cssClass="input-text" name="dspDetails.REP_permitExpiryDate" value="%{permitExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					    <c:if test="${empty dspDetails.REP_permitExpiryDate}">
						<td width="65px" ><s:textfield id="REP_permitExpiryDate" cssClass="input-text" name="dspDetails.REP_permitExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_permitExpiryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
			</tr>
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminder3MosPriorToExpiry">
			<tr>
			<td align="right"   class="listwhitetext">Reminder 3 Mos Prior To Expiry</td>
		   	<c:if test="${not empty dspDetails.REP_reminder3MosPriorToExpiry}">
			 <s:text id="reminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_reminder3MosPriorToExpiry"/></s:text>
			 <td width="65px" ><s:textfield id="REP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.REP_reminder3MosPriorToExpiry" value="%{reminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
		   <c:if test="${empty dspDetails.REP_reminder3MosPriorToExpiry}">
			<td width="65px" ><s:textfield id="REP_reminder3MosPriorToExpiry" cssClass="input-text" name="dspDetails.REP_reminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_reminder3MosPriorToExpiry-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		 </c:if>
			</tr>
			</configByCorp:fieldVisibility>
			<tr>
					<td align="right"   class="listwhitetext">Questionnaire Sent Date</td>
					 <c:if test="${not empty dspDetails.REP_questionnaireSentDate}">
						<s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.REP_questionnaireSentDate"/></s:text>
						<td width="65px" ><s:textfield id="REP_questionnaireSentDate" cssClass="input-text" name="dspDetails.REP_questionnaireSentDate" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty dspDetails.REP_questionnaireSentDate}">
						<td width="65px" ><s:textfield id="REP_questionnaireSentDate" cssClass="input-text" name="dspDetails.REP_questionnaireSentDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="REP_questionnaireSentDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr>
			
		
	<%-- 
				<configByCorp:fieldVisibility componentId="component.status.REP.HidePrimaryEmailField">
				<tr>
					<td align="right"  class="listwhitetext">Primary Email</td>
					<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" key="dspDetails.REP_primaryEmail" readonly="false" size="18" maxlength="65" onchange="changeStatus();" /></td>
				</tr>
			</configByCorp:fieldVisibility>	 --%>
</table>
</fieldset>
</td>
<td>
</td>
</tr>
<tr><td colspan="6" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="150" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.REP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</table>	
  	</div>
  
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	
		<!-- end rep -->
		<!--Final START RLS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RLS_vendorCode)>-1) && dspDetails.RLS_vendorCode !='' && dspDetails.RLS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RLS')>-1)}">
	<!-- begin rls -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rls')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RLS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rls">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RLS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('RLS_','${dspDetails.RLS_vendorCodeEXSO}'),chkIsVendorRedSky('RLS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RLS_','${dspDetails.RLS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.RLS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RLS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RLS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RLS_serviceStartDate"/></s:text>
			 <td><s:textfield id="RLS_serviceStartDate" cssClass="input-text" name="dspDetails.RLS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RLS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RLS_serviceStartDate}">
		<td><s:textfield id="RLS_serviceStartDate" cssClass="input-text" name="dspDetails.RLS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="RLS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSRentalSearchNotes == '0' || countDSRentalSearchNotes == '' || countDSRentalSearchNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRentalSearchNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRentalSearch&imageId=countDSRentalSearchNotesImage&fieldId=countDSRentalSearchNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRentalSearch&imageId=countDSRentalSearchNotesImage&fieldId=countDSRentalSearchNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRentalSearchNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRentalSearch&imageId=countDSRentalSearchNotesImage&fieldId=countDSRentalSearchNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRentalSearch&imageId=countDSRentalSearchNotesImage&fieldId=countDSRentalSearchNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RLS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RLS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RLS_serviceEndDate"/></s:text>
			 <td><s:textfield id="RLS_serviceEndDate" cssClass="input-text" name="dspDetails.RLS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RLS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RLS_serviceEndDate}">
		<td><s:textfield id="RLS_serviceEndDate" cssClass="input-text" name="dspDetails.RLS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RLS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RLS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RLS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RLS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedRLS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RLS_displyOtherVendorCode}">
	 <c:set var="ischeckedRLS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RLS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedRLS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end rls -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RLS_vendorCode)<0) && dspDetails.RLS_vendorCode !='' && dspDetails.RLS_vendorCode !=null && dspDetails.RLS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RLS')>-1)}">
<s:hidden name="dspDetails.RLS_vendorCode" />
<s:hidden name="dspDetails.RLS_vendorName" />
	 <c:if test="${not empty dspDetails.RLS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.RLS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.RLS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.RLS_serviceStartDate}">
		 <s:hidden   name="dspDetails.RLS_serviceStartDate"/> 
	 </c:if>
<s:hidden name="dspDetails.RLS_vendorContact" />
	 <c:if test="${not empty dspDetails.RLS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.RLS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.RLS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.RLS_serviceEndDate}">
		 <s:hidden   name="dspDetails.RLS_serviceEndDate"/> 
	 </c:if>

<s:hidden name="dspDetails.RLS_vendorEmail" />
<s:hidden name="dspDetails.RLS_vendorCodeEXSO" />
<s:hidden name="dspDetails.RLS_displyOtherVendorCode" />
<s:hidden name="dspDetails.RLS_paymentResponsibility" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RLS_vendorCode)<0) && dspDetails.RLS_vendorCode !='' && dspDetails.RLS_vendorCode !=null && dspDetails.RLS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RLS')>-1)}">
	<!-- begin rls -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rls')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RLS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rls">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RLS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('RLS_','${dspDetails.RLS_vendorCodeEXSO}'),chkIsVendorRedSky('RLS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RLS_','${dspDetails.RLS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.RLS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RLS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RLS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RLS_serviceStartDate"/></s:text>
			 <td><s:textfield id="RLS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RLS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RLS_serviceStartDate}">
		<td><s:textfield id="RLS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RLS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RLS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RLS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RLS_serviceEndDate"/></s:text>
			 <td><s:textfield id="RLS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RLS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RLS_serviceEndDate}">
		<td><s:textfield id="RLS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RLS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RLS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RLS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedRLS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RLS_displyOtherVendorCode}">
	 <c:set var="ischeckedRLS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.RLS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedRLS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end rls -->
	</c:if>
	</c:if>	

<!--Final END RLS -->


<!--Final START CAT -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAT_vendorCode)>-1) && dspDetails.CAT_vendorCode !='' && dspDetails.CAT_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAT')>-1)}">
	<!-- begin cat -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('cat')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CAT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="cat">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CAT_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CAT_','${dspDetails.CAT_vendorCodeEXSO}'),chkIsVendorRedSky('CAT_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CAT_','${dspDetails.CAT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.CAT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CAT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CAT_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAT_serviceStartDate"/></s:text>
			 <td><s:textfield id="CAT_serviceStartDate" cssClass="input-text" name="dspDetails.CAT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAT_serviceStartDate}">
		<td><s:textfield id="CAT_serviceStartDate" cssClass="input-text" name="dspDetails.CAT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="CAT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSCandidateAssessmentNotes == '0' || countDSCandidateAssessmentNotes == '' || countDSCandidateAssessmentNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSCandidateAssessmentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCandidateAssessment&imageId=countDSCandidateAssessmentNotesImage&fieldId=countDSCandidateAssessmentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCandidateAssessment&imageId=countDSCandidateAssessmentNotesImage&fieldId=countDSCandidateAssessmentNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSCandidateAssessmentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCandidateAssessment&imageId=countDSCandidateAssessmentNotesImage&fieldId=countDSCandidateAssessmentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCandidateAssessment&imageId=countDSCandidateAssessmentNotesImage&fieldId=countDSCandidateAssessmentNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CAT_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CAT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAT_serviceEndDate"/></s:text>
			 <td><s:textfield id="CAT_serviceEndDate" cssClass="input-text" name="dspDetails.CAT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAT_serviceEndDate}">
		<td><s:textfield id="CAT_serviceEndDate" cssClass="input-text" name="dspDetails.CAT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.CAT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CAT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CAT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCAT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CAT_displyOtherVendorCode}">
	 <c:set var="ischeckedCAT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.CAT_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCAT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="152" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CAT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end cat -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAT_vendorCode)<0) && dspDetails.CAT_vendorCode !='' && dspDetails.CAT_vendorCode !=null && dspDetails.CAT_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAT')>-1)}">
<s:hidden name="dspDetails.CAT_vendorCode" />
<s:hidden name="dspDetails.CAT_vendorName" />
	 <c:if test="${not empty dspDetails.CAT_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAT_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.CAT_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAT_serviceStartDate}">
		 <s:hidden   name="dspDetails.CAT_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.CAT_vendorContact" />
	 <c:if test="${not empty dspDetails.CAT_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAT_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.CAT_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAT_serviceEndDate}">
		 <s:hidden   name="dspDetails.CAT_serviceEndDate"/> 
	 </c:if>

<s:hidden name="dspDetails.CAT_vendorEmail" />
<s:hidden name="dspDetails.CAT_vendorCodeEXSO" />
<s:hidden name="dspDetails.CAT_displyOtherVendorCode" />
<s:hidden name="dspDetails.CAT_paymentResponsibility" />
<s:hidden name="dspDetails.CAT_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAT_vendorCode)<0) && dspDetails.CAT_vendorCode !='' && dspDetails.CAT_vendorCode !=null && dspDetails.CAT_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAT')>-1)}">
	<!-- begin cat -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('cat')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CAT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="cat">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CAT_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CAT_','${dspDetails.CAT_vendorCodeEXSO}'),chkIsVendorRedSky('CAT_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CAT_','${dspDetails.CAT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.CAT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CAT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CAT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAT_serviceStartDate"/></s:text>
			 <td><s:textfield id="CAT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CAT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAT_serviceStartDate}">
		<td><s:textfield id="CAT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CAT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CAT_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CAT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAT_serviceEndDate"/></s:text>
			 <td><s:textfield id="CAT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CAT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAT_serviceEndDate}">
		<td><s:textfield id="CAT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CAT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CAT_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CAT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCAT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CAT_displyOtherVendorCode}">
	 <c:set var="ischeckedCAT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.CAT_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCAT_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CAT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end cat -->
	</c:if>
	</c:if>	

<!--Final END CAT -->



<!--Final START CLS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CLS_vendorCode)>-1) && dspDetails.CLS_vendorCode !='' && dspDetails.CLS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CLS')>-1)}">
	<!-- begin cls -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('cls')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CLS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="cls">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CLS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CLS_','${dspDetails.CLS_vendorCodeEXSO}'),chkIsVendorRedSky('CLS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CLS_','${dspDetails.CLS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.CLS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CLS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CLS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CLS_serviceStartDate"/></s:text>
			 <td><s:textfield id="CLS_serviceStartDate" cssClass="input-text" name="dspDetails.CLS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CLS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CLS_serviceStartDate}">
		<td><s:textfield id="CLS_serviceStartDate" cssClass="input-text" name="dspDetails.CLS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="CLS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSClosingServicesNotes == '0' || countDSClosingServicesNotes == '' || countDSClosingServicesNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSClosingServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsClosingServices&imageId=countDSClosingServicesNotesImage&fieldId=countDSClosingServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsClosingServices&imageId=countDSClosingServicesNotesImage&fieldId=countDSClosingServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSClosingServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsClosingServices&imageId=countDSClosingServicesNotesImage&fieldId=countDSClosingServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsClosingServices&imageId=countDSClosingServicesNotesImage&fieldId=countDSClosingServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CLS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CLS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CLS_serviceEndDate"/></s:text>
			 <td><s:textfield id="CLS_serviceEndDate" cssClass="input-text" name="dspDetails.CLS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CLS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CLS_serviceEndDate}">
		<td><s:textfield id="CLS_serviceEndDate" cssClass="input-text" name="dspDetails.CLS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CLS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.CLS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CLS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CLS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCLS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CLS_displyOtherVendorCode}">
	 <c:set var="ischeckedCLS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.CLS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCLS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CLS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end cls -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CLS_vendorCode)<0) && dspDetails.CLS_vendorCode !='' && dspDetails.CLS_vendorCode !=null && dspDetails.CLS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CLS')>-1)}">
<s:hidden name="dspDetails.CLS_vendorCode" />
<s:hidden name="dspDetails.CLS_vendorName" />
	 <c:if test="${not empty dspDetails.CLS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CLS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.CLS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CLS_serviceStartDate}">
		 <s:hidden   name="dspDetails.CLS_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.CLS_vendorContact" />
	 <c:if test="${not empty dspDetails.CLS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CLS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.CLS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CLS_serviceEndDate}">
		 <s:hidden   name="dspDetails.CLS_serviceEndDate"/> 
	 </c:if>
<s:hidden name="dspDetails.CLS_vendorEmail" />
<s:hidden name="dspDetails.CLS_vendorCodeEXSO" />
<s:hidden name="dspDetails.CLS_displyOtherVendorCode" />
<s:hidden name="dspDetails.CLS_paymentResponsibility" />
<s:hidden name="dspDetails.CLS_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CLS_vendorCode)<0) && dspDetails.CLS_vendorCode !='' && dspDetails.CLS_vendorCode !=null && dspDetails.CLS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CLS')>-1)}">
	<!-- begin cls -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('cls')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CLS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="cls">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CLS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CLS_','${dspDetails.CLS_vendorCodeEXSO}'),chkIsVendorRedSky('CLS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CLS_','${dspDetails.CLS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.CLS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CLS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CLS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CLS_serviceStartDate"/></s:text>
			 <td><s:textfield id="CLS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CLS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CLS_serviceStartDate}">
		<td><s:textfield id="CLS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CLS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CLS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CLS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CLS_serviceEndDate"/></s:text>
			 <td><s:textfield id="CLS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CLS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CLS_serviceEndDate}">
		<td><s:textfield id="CLS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CLS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CLS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CLS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCLS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CLS_displyOtherVendorCode}">
	 <c:set var="ischeckedCLS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.CLS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCLS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CLS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end cls -->
	</c:if>
	</c:if>	

<!--Final END CLS -->


<!--Final START CHS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CHS_vendorCode)>-1) && dspDetails.CHS_vendorCode !='' && dspDetails.CHS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CHS')>-1)}">
	<!-- begin chs -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('chs')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CHS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="chs">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CHS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CHS_','${dspDetails.CHS_vendorCodeEXSO}'),chkIsVendorRedSky('CHS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CHS_','${dspDetails.CHS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.CHS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CHS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CHS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CHS_serviceStartDate"/></s:text>
			 <td><s:textfield id="CHS_serviceStartDate" cssClass="input-text" name="dspDetails.CHS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CHS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CHS_serviceStartDate}">
		<td><s:textfield id="CHS_serviceStartDate" cssClass="input-text" name="dspDetails.CHS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="CHS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSComparableHousingStudyNotes == '0' || countDSComparableHousingStudyNotes == '' || countDSComparableHousingStudyNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSComparableHousingStudyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsComparableHousingStudy&imageId=countDSComparableHousingStudyNotesImage&fieldId=countDSComparableHousingStudyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsComparableHousingStudy&imageId=countDSComparableHousingStudyNotesImage&fieldId=countDSComparableHousingStudyNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSComparableHousingStudyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsComparableHousingStudy&imageId=countDSComparableHousingStudyNotesImage&fieldId=countDSComparableHousingStudyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsComparableHousingStudy&imageId=countDSComparableHousingStudyNotesImage&fieldId=countDSComparableHousingStudyNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CHS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CHS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CHS_serviceEndDate"/></s:text>
			 <td><s:textfield id="CHS_serviceEndDate" cssClass="input-text" name="dspDetails.CHS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CHS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CHS_serviceEndDate}">
		<td><s:textfield id="CHS_serviceEndDate" cssClass="input-text" name="dspDetails.CHS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CHS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.CHS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CHS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CHS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCHS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CHS_displyOtherVendorCode}">
	 <c:set var="ischeckedCHS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.CHS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCHS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CHS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end chs -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CHS_vendorCode)<0) && dspDetails.CHS_vendorCode !='' && dspDetails.CHS_vendorCode !=null && dspDetails.CHS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CHS')>-1)}">
<s:hidden name="dspDetails.CHS_vendorCode" />
<s:hidden name="dspDetails.CHS_vendorName" />
	 <c:if test="${not empty dspDetails.CHS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CHS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.CHS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CHS_serviceStartDate}">
		 <s:hidden   name="dspDetails.CHS_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.CHS_vendorContact" />
	 <c:if test="${not empty dspDetails.CHS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CHS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.CHS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CHS_serviceEndDate}">
		 <s:hidden   name="dspDetails.CHS_serviceEndDate"/> 
	 </c:if>
<s:hidden name="dspDetails.CHS_vendorEmail" />
<s:hidden name="dspDetails.CHS_vendorCodeEXSO" />
<s:hidden name="dspDetails.CHS_displyOtherVendorCode" />
<s:hidden name="dspDetails.CHS_paymentResponsibility" />
<s:hidden name="dspDetails.CHS_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CHS_vendorCode)<0) && dspDetails.CHS_vendorCode !='' && dspDetails.CHS_vendorCode !=null && dspDetails.CHS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CHS')>-1)}">
	<!-- begin chs -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('chs')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CHS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="chs">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CHS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CHS_','${dspDetails.CHS_vendorCodeEXSO}'),chkIsVendorRedSky('CHS_');changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CHS_','${dspDetails.CHS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.CHS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CHS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CHS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CHS_serviceStartDate"/></s:text>
			 <td><s:textfield id="CHS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CHS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CHS_serviceStartDate}">
		<td><s:textfield id="CHS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CHS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CHS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CHS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CHS_serviceEndDate"/></s:text>
			 <td><s:textfield id="CHS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CHS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CHS_serviceEndDate}">
		<td><s:textfield id="CHS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CHS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CHS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CHS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCHS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CHS_displyOtherVendorCode}">
	 <c:set var="ischeckedCHS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.CHS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCHS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CHS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end chs -->
	</c:if>
	</c:if>	

<!--Final END CHS -->


<!--Final START DPS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DPS_vendorCode)>-1) && dspDetails.DPS_vendorCode !='' && dspDetails.DPS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DPS')>-1)}">
	<!-- begin dps -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('dps')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='DPS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="dps">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.DPS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('DPS_','${dspDetails.DPS_vendorCodeEXSO}'),chkIsVendorRedSky('DPS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('DPS_','${dspDetails.DPS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.DPS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','DPS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.DPS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_serviceStartDate"/></s:text>
			 <td><s:textfield id="DPS_serviceStartDate" cssClass="input-text" name="dspDetails.DPS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_serviceStartDate}">
		<td><s:textfield id="DPS_serviceStartDate" cssClass="input-text" name="dspDetails.DPS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="DPS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSDepartureServicesNotes == '0' || countDSDepartureServicesNotes == '' || countDSDepartureServicesNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSDepartureServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsDepartureServices&imageId=countDSDepartureServicesNotesImage&fieldId=countDSDepartureServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsDepartureServices&imageId=countDSDepartureServicesNotesImage&fieldId=countDSDepartureServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSDepartureServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsDepartureServices&imageId=countDSDepartureServicesNotesImage&fieldId=countDSDepartureServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsDepartureServices&imageId=countDSDepartureServicesNotesImage&fieldId=countDSDepartureServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.DPS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.DPS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_serviceEndDate"/></s:text>
			 <td><s:textfield id="DPS_serviceEndDate" cssClass="input-text" name="dspDetails.DPS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_serviceEndDate}">
		<td><s:textfield id="DPS_serviceEndDate" cssClass="input-text" name="dspDetails.DPS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.DPS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.DPS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.DPS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedDPS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.DPS_displyOtherVendorCode}">
	 <c:set var="ischeckedDPS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.DPS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedDPS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>

<td align="right" class="listwhitetext" width="83">Services</td>
<td align="left" class="listwhitetext" colspan="1"><s:select cssClass="list-menu" name="dspDetails.DPS_serviceType" list="%{dpsservices}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
<td align="right" class="listwhitetext">Completion Date</td>
	    <c:if test="${not empty dspDetails.DPS_completionDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_completionDate"/></s:text>
			 <td><s:textfield id="DPS_completionDate" cssClass="input-text" name="dspDetails.DPS_completionDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_completionDate}">
		<td><s:textfield id="DPS_completionDate" cssClass="input-text" name="dspDetails.DPS_completionDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_completionDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

</tr>
<tr>
<td align="right" class="listwhitetext" >Comment</td>
<td colspan="3">
<s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.DPS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end dps -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DPS_vendorCode)<0) && dspDetails.DPS_vendorCode !='' && dspDetails.DPS_vendorCode !=null && dspDetails.DPS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DPS')>-1)}">
<s:hidden name="dspDetails.DPS_vendorCode" />
<s:hidden name="dspDetails.DPS_vendorName" />
<c:if test="${not empty dspDetails.DPS_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DPS_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.DPS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DPS_serviceStartDate}">
		 <s:hidden   name="dspDetails.DPS_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.DPS_vendorContact" />
<c:if test="${not empty dspDetails.DPS_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DPS_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.DPS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DPS_serviceEndDate}">
		 <s:hidden   name="dspDetails.DPS_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.DPS_vendorEmail" />
<s:hidden name="dspDetails.DPS_vendorCodeEXSO" />
<s:hidden name="dspDetails.DPS_displyOtherVendorCode" />
<s:hidden name="dspDetails.DPS_paymentResponsibility" />
<s:hidden name="dspDetails.DPS_serviceType" />
<c:if test="${not empty dspDetails.DPS_completionDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DPS_completionDate" /></s:text>
	<s:hidden  name="dspDetails.DPS_completionDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DPS_completionDate}">
		 <s:hidden   name="dspDetails.DPS_completionDate"/> 
</c:if>
<s:hidden name="dspDetails.DPS_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DPS_vendorCode)<0) && dspDetails.DPS_vendorCode !='' && dspDetails.DPS_vendorCode !=null && dspDetails.DPS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DPS')>-1)}">
	<!-- begin dps -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('dps')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='DPS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="dps">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.DPS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('DPS_','${dspDetails.DPS_vendorCodeEXSO}'),chkIsVendorRedSky('DPS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('DPS_','${dspDetails.DPS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.DPS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','DPS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.DPS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_serviceStartDate"/></s:text>
			 <td><s:textfield id="DPS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.DPS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_serviceStartDate}">
		<td><s:textfield id="DPS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.DPS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.DPS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.DPS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_serviceEndDate"/></s:text>
			 <td><s:textfield id="DPS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.DPS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_serviceEndDate}">
		<td><s:textfield id="DPS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.DPS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.DPS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.DPS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedDPS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.DPS_displyOtherVendorCode}">
	 <c:set var="ischeckedDPS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.DPS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedDPS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>

<td align="right" class="listwhitetext" width="83">Services</td>
<td align="left" class="listwhitetext" colspan="2"><s:select cssClass="list-menu" name="dspDetails.DPS_serviceType" list="%{dpsservices}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
<td align="right" class="listwhitetext">Completion Date</td>
	    <c:if test="${not empty dspDetails.DPS_completionDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DPS_completionDate"/></s:text>
			 <td><s:textfield id="DPS_completionDate" cssClass="input-text" name="dspDetails.DPS_completionDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DPS_completionDate}">
		<td><s:textfield id="DPS_completionDate" cssClass="input-text" name="dspDetails.DPS_completionDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DPS_completionDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

</tr>
<tr>
<td align="right" class="listwhitetext" >Comment</td>
<td colspan="3">
<s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.DPS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end dps -->
	</c:if>
	</c:if>	

<!--Final END DPS -->


<!--Final START HSM -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HSM_vendorCode)>-1) && dspDetails.HSM_vendorCode !='' && dspDetails.HSM_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'HSM')>-1)}">
	<!-- begin hsm -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hsm')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HSM'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hsm">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.HSM_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('HSM_','${dspDetails.HSM_vendorCodeEXSO}'),chkIsVendorRedSky('HSM_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('HSM_','${dspDetails.HSM_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.HSM_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','HSM_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.HSM_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_serviceStartDate"/></s:text>
			 <td><s:textfield id="HSM_serviceStartDate" cssClass="input-text" name="dspDetails.HSM_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_serviceStartDate}">
		<td><s:textfield id="HSM_serviceStartDate" cssClass="input-text" name="dspDetails.HSM_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSHomeSaleMarketingAssistanceNotes == '0' || countDSHomeSaleMarketingAssistanceNotes == '' || countDSHomeSaleMarketingAssistanceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSHomeSaleMarketingAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeSaleMarketingAssistance&imageId=countDSHomeSaleMarketingAssistanceNotesImage&fieldId=countDSHomeSaleMarketingAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeSaleMarketingAssistance&imageId=countDSHomeSaleMarketingAssistanceNotesImage&fieldId=countDSHomeSaleMarketingAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSHomeSaleMarketingAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeSaleMarketingAssistance&imageId=countDSHomeSaleMarketingAssistanceNotesImage&fieldId=countDSHomeSaleMarketingAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeSaleMarketingAssistance&imageId=countDSHomeSaleMarketingAssistanceNotesImage&fieldId=countDSHomeSaleMarketingAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.HSM_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.HSM_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_serviceEndDate"/></s:text>
			 <td><s:textfield id="HSM_serviceEndDate" cssClass="input-text" name="dspDetails.HSM_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_serviceEndDate}">
		<td><s:textfield id="HSM_serviceEndDate" cssClass="input-text" name="dspDetails.HSM_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.HSM_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.HSM_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HSM_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedHSM_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HSM_displyOtherVendorCode}">
	 <c:set var="ischeckedHSM_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HSM_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedHSM_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table width="100%" cellpadding="2" class="detailTabLabel">
<tbody><tr>
<td align="left" class="vertlinedata"></td>
</tr>
</tbody></table>
<table>
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83">Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.HSM_agentCode','dspDetails.HSM_agentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.HSM_agentName" readonly="true" cssStyle="width:18.7em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
	    <td align="right" width="100px" class="listwhitetext">Marketing&nbsp;Plan&nbsp;and BMA&nbsp;received</td>
	    <c:if test="${not empty dspDetails.HSM_marketingPlanNBMAReceived}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_marketingPlanNBMAReceived"/></s:text>
			 <td width="68"><s:textfield id="HSM_marketingPlanNBMAReceived" cssClass="input-text" name="dspDetails.HSM_marketingPlanNBMAReceived" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_marketingPlanNBMAReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_marketingPlanNBMAReceived}">
		<td width="68"><s:textfield id="HSM_marketingPlanNBMAReceived" cssClass="input-text" name="dspDetails.HSM_marketingPlanNBMAReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_marketingPlanNBMAReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentPhone" readonly="false" size="57" maxlength="58" onchange=""  /></td>
		<td align="right" class="listwhitetext" >Status</td>
		<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HSM_status" list="%{hsmstatus}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
		
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentEmail" readonly="false" size="57" maxlength="58" onchange=""  /></td>
		
		<td align="right" class="listwhitetext" width="169">Estimated&nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td align="left"  colspan="5" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HSM_estimatedHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
		</tr>
		
		

<tr>
<td align="right" class="listwhitetext" >Comment</td>
<td colspan="1">
<s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.HSM_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td>
<td colspan="3" rowspan="2">
<table>
<tr>
<td align="right" class="listwhitetext" width="166">Actual &nbsp;HSR&nbsp;Referral&nbsp;$</td>
<td align="left"  colspan="2" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HSM_actualHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  /></td>
</tr>
<tr>
<td align="right" width="100px" class="listwhitetext">Offer&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HSM_offerDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_offerDate"/></s:text>
					 <td><s:textfield id="HSM_offerDate" cssClass="input-text" name="dspDetails.HSM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HSM_offerDate}">
				<td><s:textfield id="HSM_offerDate" cssClass="input-text" name="dspDetails.HSM_offerDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
</tr>
<tr>
<td align="right" width="119px" class="listwhitetext">Closing&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HSM_closingDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_closingDate"/></s:text>
					 <td width="50"><s:textfield id="HSM_closingDate" cssClass="input-text" name="dspDetails.HSM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HSM_closingDate}">
				<td  width="50"><s:textfield id="HSM_closingDate" cssClass="input-text" name="dspDetails.HSM_closingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	       
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end hsm -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HSM_vendorCode)<0) && dspDetails.HSM_vendorCode !='' && dspDetails.HSM_vendorCode !=null && dspDetails.HSM_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'HSM')>-1)}">
<s:hidden name="dspDetails.HSM_vendorCode" />
<s:hidden name="dspDetails.HSM_vendorName" />
<c:if test="${not empty dspDetails.HSM_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HSM_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.HSM_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HSM_serviceStartDate}">
		 <s:hidden   name="dspDetails.HSM_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.HSM_vendorContact" />
<c:if test="${not empty dspDetails.HSM_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HSM_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.HSM_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HSM_serviceEndDate}">
		 <s:hidden   name="dspDetails.HSM_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.HSM_vendorEmail" />
<s:hidden name="dspDetails.HSM_vendorCodeEXSO" />
<s:hidden name="dspDetails.HSM_displyOtherVendorCode" />
<s:hidden name="dspDetails.HSM_paymentResponsibility" />
<s:hidden name="dspDetails.HSM_actualHSRReferral"/>
<s:hidden name="dspDetails.HSM_estimatedHSRReferral"/>
<s:hidden name="dspDetails.HSM_agentEmail"/>
<s:hidden name="dspDetails.HSM_agentPhone"/>
<s:hidden name="dspDetails.HSM_agentName"/>
<s:hidden name="dspDetails.HSM_agentCode" />
<c:if test="${not empty dspDetails.HSM_offerDate}">
<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_offerDate"/></s:text>
<td><s:hidden name="dspDetails.HSM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}"  />
</c:if>
<c:if test="${empty dspDetails.HSM_offerDate}">
<td><s:hidden name="dspDetails.HSM_offerDate" />
</c:if>
<c:if test="${not empty dspDetails.HSM_closingDate}">
<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_closingDate"/></s:text>
 <td width="50"><s:hidden name="dspDetails.HSM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" />
</c:if>
<c:if test="${empty dspDetails.HSM_closingDate}">
<td  width="50"><s:hidden name="dspDetails.HSM_closingDate" />
</c:if>			

<c:if test="${not empty dspDetails.HSM_marketingPlanNBMAReceived}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HSM_marketingPlanNBMAReceived" /></s:text>
	<s:hidden  name="dspDetails.HSM_marketingPlanNBMAReceived" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HSM_marketingPlanNBMAReceived}">
		 <s:hidden   name="dspDetails.HSM_marketingPlanNBMAReceived"/> 
</c:if>
<s:hidden name="dspDetails.HSM_status" />
<s:hidden name="dspDetails.HSM_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HSM_vendorCode)<0) && dspDetails.HSM_vendorCode !='' && dspDetails.HSM_vendorCode !=null && dspDetails.HSM_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'HSM')>-1)}">
	<!-- begin hsm -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hsm')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HSM'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hsm">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.HSM_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('HSM_','${dspDetails.HSM_vendorCodeEXSO}'),chkIsVendorRedSky('HSM_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('HSM_','${dspDetails.HSM_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.HSM_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','HSM_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.HSM_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_serviceStartDate"/></s:text>
			 <td><s:textfield id="HSM_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HSM_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_serviceStartDate}">
		<td><s:textfield id="HSM_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HSM_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.HSM_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.HSM_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_serviceEndDate"/></s:text>
			 <td><s:textfield id="HSM_serviceEndDate" cssClass="input-textUpper" name="dspDetails.HSM_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_serviceEndDate}">
		<td><s:textfield id="HSM_serviceEndDate" cssClass="input-textUpper" name="dspDetails.HSM_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.HSM_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HSM_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedHSM_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HSM_displyOtherVendorCode}">
	 <c:set var="ischeckedHSM_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.HSM_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedHSM_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table width="100%" cellpadding="2" class="detailTabLabel">
<tbody><tr>
<td align="left" class="vertlinedata"></td>
</tr>
</tbody></table>
<table>
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83">Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.HSM_agentCode','dspDetails.HSM_agentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.HSM_agentName" readonly="true" cssStyle="width:18.7em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
	    <td align="right" width="100px" class="listwhitetext">Marketing&nbsp;Plan&nbsp;and BMA&nbsp;received</td>
	    <c:if test="${not empty dspDetails.HSM_marketingPlanNBMAReceived}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_marketingPlanNBMAReceived"/></s:text>
			 <td width="68"><s:textfield id="HSM_marketingPlanNBMAReceived" cssClass="input-text" name="dspDetails.HSM_marketingPlanNBMAReceived" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_marketingPlanNBMAReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HSM_marketingPlanNBMAReceived}">
		<td width="68"><s:textfield id="HSM_marketingPlanNBMAReceived" cssClass="input-text" name="dspDetails.HSM_marketingPlanNBMAReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_marketingPlanNBMAReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentPhone" readonly="false" size="57" maxlength="58" onchange=""  /></td>
		<td align="right" class="listwhitetext" >Status</td>
		<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HSM_status" list="%{hsmstatus}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
		
		</tr>
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HSM_agentEmail" readonly="false" size="57" maxlength="58" onchange=""  /></td>
		<td align="right" class="listwhitetext" width="169">Estimated&nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td align="left"  colspan="5" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HSM_estimatedHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
		
		</tr>
<tr>
<td align="right" class="listwhitetext" >Comment</td>
<td colspan="1">
<s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.HSM_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td>
<td colspan="3" rowspan="2">
<table>
<tr>
<td align="right" class="listwhitetext" width="166">Actual &nbsp;HSR&nbsp;Referral&nbsp;$</td>
<td align="left"  colspan="2" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HSM_actualHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  /></td>
</tr>
<tr>
<td align="right" width="100px" class="listwhitetext">Offer&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HSM_offerDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_offerDate"/></s:text>
					 <td><s:textfield id="HSM_offerDate" cssClass="input-text" name="dspDetails.HSM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HSM_offerDate}">
				<td><s:textfield id="HSM_offerDate" cssClass="input-text" name="dspDetails.HSM_offerDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
</tr>
<tr>
<td align="right" width="119px" class="listwhitetext">Closing&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HSM_closingDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HSM_closingDate"/></s:text>
					 <td width="50"><s:textfield id="HSM_closingDate" cssClass="input-text" name="dspDetails.HSM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HSM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HSM_closingDate}">
				<td  width="50"><s:textfield id="HSM_closingDate" cssClass="input-text" name="dspDetails.HSM_closingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HSM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end hsm -->
	</c:if>
	</c:if>	

<!--Final END HSM -->



<!--Final START PDT -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.PDT_vendorCode)>-1) && dspDetails.PDT_vendorCode !='' && dspDetails.PDT_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'PDT')>-1)}">
	<!-- begin pdt -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('pdt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='PDT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="pdt">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.PDT_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('PDT_','${dspDetails.PDT_vendorCodeEXSO}'),chkIsVendorRedSky('PDT_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('PDT_','${dspDetails.PDT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.PDT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','PDT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.PDT_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PDT_serviceStartDate"/></s:text>
			 <td><s:textfield id="PDT_serviceStartDate" cssClass="input-text" name="dspDetails.PDT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PDT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PDT_serviceStartDate}">
		<td><s:textfield id="PDT_serviceStartDate" cssClass="input-text" name="dspDetails.PDT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="PDT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSPolicyDevelopmentNotes == '0' || countDSPolicyDevelopmentNotes == '' || countDSPolicyDevelopmentNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSPolicyDevelopmentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsPolicyDevelopment&imageId=countDSPolicyDevelopmentNotesImage&fieldId=countDSPolicyDevelopmentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsPolicyDevelopment&imageId=countDSPolicyDevelopmentNotesImage&fieldId=countDSPolicyDevelopmentNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSPolicyDevelopmentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsPolicyDevelopment&imageId=countDSPolicyDevelopmentNotesImage&fieldId=countDSPolicyDevelopmentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsPolicyDevelopment&imageId=countDSPolicyDevelopmentNotesImage&fieldId=countDSPolicyDevelopmentNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PDT_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.PDT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PDT_serviceEndDate"/></s:text>
			 <td><s:textfield id="PDT_serviceEndDate" cssClass="input-text" name="dspDetails.PDT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PDT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PDT_serviceEndDate}">
		<td><s:textfield id="PDT_serviceEndDate" cssClass="input-text" name="dspDetails.PDT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PDT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.PDT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PDT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.PDT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedPDT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.PDT_displyOtherVendorCode}">
	 <c:set var="ischeckedPDT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.PDT_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedPDT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Service&nbsp;Type</td>
<td align="left"  colspan="5" class="listwhitetext" width="300"><s:select cssClass="list-menu" name="dspDetails.PDT_serviceType" list="%{pdtservices}" cssStyle="width:150px;" headerKey="" headerValue="" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Budget&nbsp;$</td>
<td align="left"  colspan="5" class="listwhitetext" width="300"><s:textfield cssClass="input-text" key="dspDetails.PDT_budget" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.PDT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end pdt -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.PDT_vendorCode)<0) && dspDetails.PDT_vendorCode !='' && dspDetails.PDT_vendorCode !=null && dspDetails.PDT_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'PDT')>-1)}">
<s:hidden name="dspDetails.PDT_vendorCode" />
<s:hidden name="dspDetails.PDT_vendorName" />
	 <c:if test="${not empty dspDetails.PDT_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.PDT_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.PDT_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.PDT_serviceStartDate}">
		 <s:hidden   name="dspDetails.PDT_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.PDT_vendorContact" />
	 <c:if test="${not empty dspDetails.PDT_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.PDT_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.PDT_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.PDT_serviceEndDate}">
		 <s:hidden   name="dspDetails.PDT_serviceEndDate"/> 
	 </c:if>

<s:hidden name="dspDetails.PDT_vendorEmail" />
<s:hidden name="dspDetails.PDT_vendorCodeEXSO" />
<s:hidden name="dspDetails.PDT_displyOtherVendorCode" />
<s:hidden name="dspDetails.PDT_paymentResponsibility" />
<s:hidden name="dspDetails.PDT_serviceType" />
<s:hidden name="dspDetails.PDT_budget" />
<s:hidden name="dspDetails.PDT_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.PDT_vendorCode)<0) && dspDetails.PDT_vendorCode !='' && dspDetails.PDT_vendorCode !=null && dspDetails.PDT_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'PDT')>-1)}">
	<!-- begin pdt -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('pdt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='PDT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="pdt">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.PDT_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('PDT_','${dspDetails.PDT_vendorCodeEXSO}'),chkIsVendorRedSky('PDT_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('PDT_','${dspDetails.PDT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.PDT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','PDT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.PDT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PDT_serviceStartDate"/></s:text>
			 <td><s:textfield id="PDT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.PDT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PDT_serviceStartDate}">
		<td><s:textfield id="PDT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.PDT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.PDT_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.PDT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PDT_serviceEndDate"/></s:text>
			 <td><s:textfield id="PDT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.PDT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PDT_serviceEndDate}">
		<td><s:textfield id="PDT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.PDT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.PDT_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.PDT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedPDT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.PDT_displyOtherVendorCode}">
	 <c:set var="ischeckedPDT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.PDT_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedPDT_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Service&nbsp;Type</td>
<td align="left"  colspan="5" class="listwhitetext" width="300"><s:select cssClass="list-menu" name="dspDetails.PDT_serviceType" list="%{pdtservices}" cssStyle="width:150px;" headerKey="" headerValue="" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Budget&nbsp;$</td>
<td align="left"  colspan="5" class="listwhitetext" width="300"><s:textfield cssClass="input-text" key="dspDetails.PDT_budget" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.PDT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end pdt -->
	</c:if>
	</c:if>	

<!--Final END PDT -->


<!--Final START RCP -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RCP_vendorCode)>-1) && dspDetails.RCP_vendorCode !='' && dspDetails.RCP_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RCP')>-1)}">
	<!-- begin rcp -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rcp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RCP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rcp">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RCP_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('RCP_','${dspDetails.RCP_vendorCodeEXSO}'),chkIsVendorRedSky('RCP_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RCP_','${dspDetails.RCP_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.RCP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RCP_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RCP_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RCP_serviceStartDate"/></s:text>
			 <td><s:textfield id="RCP_serviceStartDate" cssClass="input-text" name="dspDetails.RCP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RCP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RCP_serviceStartDate}">
		<td><s:textfield id="RCP_serviceStartDate" cssClass="input-text" name="dspDetails.RCP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="RCP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSRelocationCostProjectionsNotes == '0' || countDSRelocationCostProjectionsNotes == '' || countDSRelocationCostProjectionsNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRelocationCostProjectionsNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRelocationCostProjections&imageId=countDSRelocationCostProjectionsNotesImage&fieldId=countDSRelocationCostProjectionsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRelocationCostProjections&imageId=countDSRelocationCostProjectionsNotesImage&fieldId=countDSRelocationCostProjectionsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRelocationCostProjectionsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRelocationCostProjections&imageId=countDSRelocationCostProjectionsNotesImage&fieldId=countDSRelocationCostProjectionsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRelocationCostProjections&imageId=countDSRelocationCostProjectionsNotesImage&fieldId=countDSRelocationCostProjectionsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RCP_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RCP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RCP_serviceEndDate"/></s:text>
			 <td><s:textfield id="RCP_serviceEndDate" cssClass="input-text" name="dspDetails.RCP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RCP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RCP_serviceEndDate}">
		<td><s:textfield id="RCP_serviceEndDate" cssClass="input-text" name="dspDetails.RCP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RCP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RCP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RCP_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RCP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedRCP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RCP_displyOtherVendorCode}">
	 <c:set var="ischeckedRCP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RCP_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedRCP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Estimated&nbsp;Cost&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.RCP_estimateCost" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Actual&nbsp;Cost&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.RCP_actualCost" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.RCP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end rcp -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RCP_vendorCode)<0) && dspDetails.RCP_vendorCode !='' && dspDetails.RCP_vendorCode !=null && dspDetails.RCP_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RCP')>-1)}">
<s:hidden name="dspDetails.RCP_vendorCode" />
<s:hidden name="dspDetails.RCP_vendorName" />
	 <c:if test="${not empty dspDetails.RCP_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.RCP_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.RCP_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.RCP_serviceStartDate}">
		 <s:hidden   name="dspDetails.RCP_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.RCP_vendorContact" />
	 <c:if test="${not empty dspDetails.RCP_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.RCP_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.RCP_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.RCP_serviceEndDate}">
		 <s:hidden   name="dspDetails.RCP_serviceEndDate"/> 
	 </c:if>

<s:hidden name="dspDetails.RCP_vendorEmail" />
<s:hidden name="dspDetails.RCP_vendorCodeEXSO" />
<s:hidden name="dspDetails.RCP_displyOtherVendorCode" />
<s:hidden name="dspDetails.RCP_paymentResponsibility" />  
<s:hidden name="dspDetails.RCP_estimateCost" />
<s:hidden name="dspDetails.RCP_actualCost" />
<s:hidden name="dspDetails.RCP_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.RCP_vendorCode)<0) && dspDetails.RCP_vendorCode !='' && dspDetails.RCP_vendorCode !=null && dspDetails.RCP_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'RCP')>-1)}">
	<!-- begin rcp -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rcp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RCP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rcp">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RCP_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('RCP_','${dspDetails.RCP_vendorCodeEXSO}'),chkIsVendorRedSky('RCP_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RCP_','${dspDetails.RCP_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.RCP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RCP_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RCP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RCP_serviceStartDate"/></s:text>
			 <td><s:textfield id="RCP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RCP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RCP_serviceStartDate}">
		<td><s:textfield id="RCP_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RCP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RCP_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RCP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RCP_serviceEndDate"/></s:text>
			 <td><s:textfield id="RCP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RCP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RCP_serviceEndDate}">
		<td><s:textfield id="RCP_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RCP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RCP_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RCP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedRCP_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RCP_displyOtherVendorCode}">
	 <c:set var="ischeckedRCP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.RCP_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedRCP_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Estimated&nbsp;Cost&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.RCP_estimateCost" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Actual&nbsp;Cost&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.RCP_actualCost" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.RCP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end rcp -->
	</c:if>
	</c:if>	

<!--Final END RCP -->

<!--Final START SPA -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.SPA_vendorCode)>-1) && dspDetails.SPA_vendorCode !='' && dspDetails.SPA_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'SPA')>-1)}">
	<!-- begin spa -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('spa')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SPA'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="spa">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SPA_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('SPA_','${dspDetails.SPA_vendorCodeEXSO}'),chkIsVendorRedSky('SPA_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SPA_','${dspDetails.SPA_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.SPA_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SPA_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SPA_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SPA_serviceStartDate"/></s:text>
			 <td><s:textfield id="SPA_serviceStartDate" cssClass="input-text" name="dspDetails.SPA_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SPA_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SPA_serviceStartDate}">
		<td><s:textfield id="SPA_serviceStartDate" cssClass="input-text" name="dspDetails.SPA_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SPA_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSSpousalAssistanceNotes == '0' || countDSSpousalAssistanceNotes == '' || countDSSpousalAssistanceNotes == null}">
<td  align="right" style="width:95px;!width:190px;"><img id="countDSSpousalAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSpousalAssistance&imageId=countDSSpousalAssistanceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSpousalAssistance&imageId=countDSSpousalAssistanceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:95px;!width:100px;"><img id="countDSSpousalAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSpousalAssistance&imageId=countDSSpousalAssistanceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSpousalAssistance&imageId=countDSSpousalAssistanceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SPA_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SPA_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SPA_serviceEndDate"/></s:text>
			 <td><s:textfield id="SPA_serviceEndDate" cssClass="input-text" name="dspDetails.SPA_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SPA_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SPA_serviceEndDate}">
		<td><s:textfield id="SPA_serviceEndDate" cssClass="input-text" name="dspDetails.SPA_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SPA_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.SPA_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SPA_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SPA_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedSPA_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SPA_displyOtherVendorCode}">
	 <c:set var="ischeckedSPA_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SPA_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedSPA_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr><td colspan="5" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SPA_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td>
 <c:if test="${cmmDmmAgent || networkAgent}">
 <td align="right" width="230px" class="listwhitetext">Vendor Initiation</td>
	    <c:if test="${not empty dspDetails.SPA_vendorInitiation}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SPA_vendorInitiation"/></s:text>
			 <td style="width:68px;"><s:textfield id="SPA_vendorInitiation" cssClass="input-text" name="dspDetails.SPA_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SPA_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SPA_vendorInitiation}">
		<td style="width:68px;"><s:textfield id="SPA_vendorInitiation" cssClass="input-text" name="dspDetails.SPA_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SPA_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</c:if>
</tr>
</table>
</td>
</tr>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end spa -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.SPA_vendorCode)<0) && dspDetails.SPA_vendorCode !='' && dspDetails.SPA_vendorCode !=null && dspDetails.SPA_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'SPA')>-1)}">
<s:hidden name="dspDetails.SPA_vendorCode" />
<s:hidden name="dspDetails.SPA_vendorName" />
	 <c:if test="${not empty dspDetails.SPA_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.SPA_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.SPA_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.SPA_serviceStartDate}">
		 <s:hidden   name="dspDetails.SPA_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.SPA_vendorContact" />
	 <c:if test="${not empty dspDetails.SPA_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.SPA_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.SPA_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.SPA_serviceEndDate}">
		 <s:hidden   name="dspDetails.SPA_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.SPA_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.SPA_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.SPA_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.SPA_vendorInitiation}">
		 <s:hidden   name="dspDetails.SPA_vendorInitiation"/> 
	 </c:if>

<s:hidden name="dspDetails.SPA_vendorEmail" />
<s:hidden name="dspDetails.SPA_vendorCodeEXSO" />
<s:hidden name="dspDetails.SPA_displyOtherVendorCode" />
<s:hidden name="dspDetails.SPA_paymentResponsibility" />
<s:hidden name="dspDetails.SPA_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.SPA_vendorCode)<0) && dspDetails.SPA_vendorCode !='' && dspDetails.SPA_vendorCode !=null && dspDetails.SPA_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'SPA')>-1)}">
	<!-- begin spa -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('spa')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SPA'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="spa">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SPA_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('SPA_','${dspDetails.SPA_vendorCodeEXSO}'),chkIsVendorRedSky('SPA_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SPA_','${dspDetails.SPA_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.SPA_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SPA_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SPA_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SPA_serviceStartDate"/></s:text>
			 <td><s:textfield id="SPA_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SPA_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SPA_serviceStartDate}">
		<td><s:textfield id="SPA_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SPA_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SPA_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SPA_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SPA_serviceEndDate"/></s:text>
			 <td><s:textfield id="SPA_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SPA_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SPA_serviceEndDate}">
		<td><s:textfield id="SPA_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SPA_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SPA_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SPA_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedSPA_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SPA_displyOtherVendorCode}">
	 <c:set var="ischeckedSPA_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.SPA_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedSPA_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SPA_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end spa -->
	</c:if>
	</c:if>	

<!--Final END SPA -->


<!--Final START TCS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TCS_vendorCode)>-1) && dspDetails.TCS_vendorCode !='' && dspDetails.TCS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'TCS')>-1)}">
	<!-- begin tcs -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tcs')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TCS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tcs">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TCS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('TCS_','${dspDetails.TCS_vendorCodeEXSO}'),chkIsVendorRedSky('TCS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TCS_','${dspDetails.TCS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.TCS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TCS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TCS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TCS_serviceStartDate"/></s:text>
			 <td><s:textfield id="TCS_serviceStartDate" cssClass="input-text" name="dspDetails.TCS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TCS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TCS_serviceStartDate}">
		<td><s:textfield id="TCS_serviceStartDate" cssClass="input-text" name="dspDetails.TCS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="TCS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSTechnologySolutionsNotes == '0' || countDSTechnologySolutionsNotes == '' || countDSTechnologySolutionsNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSTechnologySolutionsNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTechnologySolutions&imageId=countDSTechnologySolutionsNotesImage&fieldId=countDSTechnologySolutionsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTechnologySolutions&imageId=countDSTechnologySolutionsNotesImage&fieldId=countDSTechnologySolutionsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSTechnologySolutionsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTechnologySolutions&imageId=countDSTechnologySolutionsNotesImage&fieldId=countDSTechnologySolutionsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTechnologySolutions&imageId=countDSTechnologySolutionsNotesImage&fieldId=countDSTechnologySolutionsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TCS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TCS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TCS_serviceEndDate"/></s:text>
			 <td><s:textfield id="TCS_serviceEndDate" cssClass="input-text" name="dspDetails.TCS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TCS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TCS_serviceEndDate}">
		<td><s:textfield id="TCS_serviceEndDate" cssClass="input-text" name="dspDetails.TCS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TCS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TCS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TCS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TCS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedTCS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TCS_displyOtherVendorCode}">
	 <c:set var="ischeckedTCS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TCS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTCS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TCS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end tcs -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TCS_vendorCode)<0) && dspDetails.TCS_vendorCode !='' && dspDetails.TCS_vendorCode !=null && dspDetails.TCS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'TCS')>-1)}">
<s:hidden name="dspDetails.TCS_vendorCode" />
<s:hidden name="dspDetails.TCS_vendorName" />
	 <c:if test="${not empty dspDetails.TCS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TCS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.TCS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TCS_serviceStartDate}">
		 <s:hidden   name="dspDetails.TCS_serviceStartDate"/> 
	 </c:if>

<s:hidden name="dspDetails.TCS_vendorContact" />
	 <c:if test="${not empty dspDetails.TCS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TCS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.TCS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TCS_serviceEndDate}">
		 <s:hidden   name="dspDetails.TCS_serviceEndDate"/> 
	 </c:if>
<s:hidden name="dspDetails.TCS_vendorEmail" />
<s:hidden name="dspDetails.TCS_vendorCodeEXSO" />
<s:hidden name="dspDetails.TCS_displyOtherVendorCode" />
<s:hidden name="dspDetails.TCS_paymentResponsibility" />
<s:hidden name="dspDetails.TCS_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TCS_vendorCode)<0) && dspDetails.TCS_vendorCode !='' && dspDetails.TCS_vendorCode !=null && dspDetails.TCS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'TCS')>-1)}">
	<!-- begin tcs -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tcs')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TCS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tcs">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TCS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('TCS_','${dspDetails.TCS_vendorCodeEXSO}'),chkIsVendorRedSky('TCS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TCS_','${dspDetails.TCS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.TCS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TCS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TCS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TCS_serviceStartDate"/></s:text>
			 <td><s:textfield id="TCS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TCS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TCS_serviceStartDate}">
		<td><s:textfield id="TCS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TCS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TCS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TCS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TCS_serviceEndDate"/></s:text>
			 <td><s:textfield id="TCS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TCS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TCS_serviceEndDate}">
		<td><s:textfield id="TCS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TCS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TCS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TCS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedTCS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TCS_displyOtherVendorCode}">
	 <c:set var="ischeckedTCS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.TCS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedTCS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TCS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end tcs -->
	</c:if>
	</c:if>	

<!--Final END TCS -->

<!--Final START MTS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.MTS_vendorCode)>-1) && dspDetails.MTS_vendorCode !='' && dspDetails.MTS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'MTS')>-1)}">
	<!-- begin mts -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('mts')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='MTS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="mts">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.MTS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('MTS_','${dspDetails.MTS_vendorCodeEXSO}'),chkIsVendorRedSky('MTS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('MTS_','${dspDetails.MTS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.MTS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','MTS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.MTS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_serviceStartDate"/></s:text>
			 <td><s:textfield id="MTS_serviceStartDate" cssClass="input-text" name="dspDetails.MTS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MTS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_serviceStartDate}">
		<td><s:textfield id="MTS_serviceStartDate" cssClass="input-text" name="dspDetails.MTS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MTS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSMortgageServicesNotes == '0' || countDSMortgageServicesNotes == '' || countDSMortgageServicesNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSMortgageServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMortgageServices&imageId=countDSMortgageServicesNotesImage&fieldId=countDSMortgageServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMortgageServices&imageId=countDSMortgageServicesNotesImage&fieldId=countDSMortgageServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSMortgageServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMortgageServices&imageId=countDSMortgageServicesNotesImage&fieldId=countDSMortgageServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMortgageServices&imageId=countDSMortgageServicesNotesImage&fieldId=countDSMortgageServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.MTS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.MTS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_serviceEndDate"/></s:text>
			 <td><s:textfield id="MTS_serviceEndDate" cssClass="input-text" name="dspDetails.MTS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MTS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_serviceEndDate}">
		<td><s:textfield id="MTS_serviceEndDate" cssClass="input-text" name="dspDetails.MTS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MTS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.MTS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.MTS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.MTS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedMTS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.MTS_displyOtherVendorCode}">
	 <c:set var="ischeckedMTS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.MTS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedMTS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Lender</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" key="dspDetails.MTS_lender" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/>
</td>
</configByCorp:fieldVisibility>
</tr>

<tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<td align="right" class="listwhitetext">Initial Contact</td>
	    <c:if test="${not empty dspDetails.MTS_initialContact}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_initialContact"/></s:text>
			 <td colspan="1"><s:textfield id="MTS_initialContact" cssClass="input-text" name="dspDetails.MTS_initialContact" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="MTS_initialContact-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_initialContact}">
		<td colspan="1"><s:textfield id="MTS_initialContact" cssClass="input-text" name="dspDetails.MTS_initialContact" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="MTS_initialContact-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>	
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">	
<td align="right" class="listwhitetext" width="83">Status</td>
<td align="left" class="listwhitetext" colspan="3"><s:select cssClass="list-menu" name="dspDetails.MTS_status" list="%{mtsstatus}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Amount&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.MTS_mortgageAmount" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Rate&nbsp;%</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.MTS_mortgageRate" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Term</td>
<td align="left"  class="listwhitetext" width="150"><s:select cssClass="list-menu" name="dspDetails.MTS_mortgageTerm" list="%{''}" cssStyle="width:60px;" headerKey="" headerValue="" />
</td>
</tr>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.MTS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end mts -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.MTS_vendorCode)<0) && dspDetails.MTS_vendorCode !='' && dspDetails.MTS_vendorCode !=null && dspDetails.MTS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'MTS')>-1)}">
<s:hidden name="dspDetails.MTS_vendorCode" />
<s:hidden name="dspDetails.MTS_vendorName" />
<c:if test="${not empty dspDetails.MTS_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.MTS_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.MTS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.MTS_serviceStartDate}">
		 <s:hidden   name="dspDetails.MTS_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.MTS_vendorContact" />
<c:if test="${not empty dspDetails.MTS_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.MTS_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.MTS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.MTS_serviceEndDate}">
		 <s:hidden   name="dspDetails.MTS_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.MTS_vendorEmail" />
<s:hidden name="dspDetails.MTS_vendorCodeEXSO" />
<s:hidden name="dspDetails.MTS_displyOtherVendorCode" />
<s:hidden name="dspDetails.MTS_paymentResponsibility" />
<s:hidden name="dspDetails.MTS_lender" />
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<c:if test="${not empty dspDetails.MTS_initialContact}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.MTS_initialContact" /></s:text>
	<s:hidden  name="dspDetails.MTS_initialContact" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.MTS_initialContact}">
		 <s:hidden   name="dspDetails.MTS_initialContact"/> 
</c:if>
</configByCorp:fieldVisibility>
<s:hidden name="dspDetails.MTS_status" />
<s:hidden name="dspDetails.MTS_mortgageAmount" />
<s:hidden name="dspDetails.MTS_mortgageRate" />
<s:hidden name="dspDetails.MTS_mortgageTerm" />
<s:hidden name="dspDetails.MTS_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.MTS_vendorCode)<0) && dspDetails.MTS_vendorCode !='' && dspDetails.MTS_vendorCode !=null && dspDetails.MTS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'MTS')>-1)}">
	<!-- begin mts -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('mts')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='MTS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="mts">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.MTS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('MTS_','${dspDetails.MTS_vendorCodeEXSO}'),chkIsVendorRedSky('MTS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('MTS_','${dspDetails.MTS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.MTS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','MTS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.MTS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_serviceStartDate"/></s:text>
			 <td><s:textfield id="MTS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.MTS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_serviceStartDate}">
		<td><s:textfield id="MTS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.MTS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.MTS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.MTS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_serviceEndDate"/></s:text>
			 <td><s:textfield id="MTS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.MTS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_serviceEndDate}">
		<td><s:textfield id="MTS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.MTS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.MTS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.MTS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedMTS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.MTS_displyOtherVendorCode}">
	 <c:set var="ischeckedMTS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.MTS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedMTS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Lender</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" key="dspDetails.MTS_lender" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/>
</td>
</tr>
</configByCorp:fieldVisibility>
<tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<td align="right" class="listwhitetext">Initial Contact</td>
	    <c:if test="${not empty dspDetails.MTS_initialContact}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MTS_initialContact"/></s:text>
			 <td colspan="2"><s:textfield id="MTS_initialContact" cssClass="input-text" name="dspDetails.MTS_initialContact" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="MTS_initialContact-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MTS_initialContact}">
		<td colspan="2"><s:textfield id="MTS_initialContact" cssClass="input-text" name="dspDetails.MTS_initialContact" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="MTS_initialContact-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>	
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">	
<td align="right" class="listwhitetext" width="83">Status</td>
<td align="left" class="listwhitetext" colspan="3"><s:select cssClass="list-menu" name="dspDetails.MTS_status" list="%{mtsstatus}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</tr>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Amount&nbsp;$</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.MTS_mortgageAmount" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Rate&nbsp;%</td>
<td align="left"  class="listwhitetext" width="150"><s:textfield cssClass="input-text" key="dspDetails.MTS_mortgageRate" readonly="false" size="5" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Mortgage&nbsp;Term</td>
<td align="left"  class="listwhitetext" width="150"><s:select cssClass="list-menu" name="dspDetails.MTS_mortgageTerm" list="%{''}" cssStyle="width:60px;" headerKey="" headerValue="" />
</td>
</tr>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.MTS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end mts -->
	</c:if>
	</c:if>	

<!--Final END MTS -->



<!--Final START DSS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DSS_vendorCode)>-1) && dspDetails.DSS_vendorCode !='' && dspDetails.DSS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DSS')>-1)}">
	<!-- begin dss -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('dss')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='DSS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="dss">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.DSS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('DSS_','${dspDetails.DSS_vendorCodeEXSO}'),chkIsVendorRedSky('DSS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('DSS_','${dspDetails.DSS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.DSS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','DSS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.DSS_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_serviceStartDate"/></s:text>
			 <td><s:textfield id="DSS_serviceStartDate" cssClass="input-text" name="dspDetails.DSS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DSS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_serviceStartDate}">
		<td><s:textfield id="DSS_serviceStartDate" cssClass="input-text" name="dspDetails.DSS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="DSS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSDestinationsSchoolServicesNotes == '0' || countDSDestinationsSchoolServicesNotes == '' || countDSDestinationsSchoolServicesNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSDestinationsSchoolServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsDestinationsSchoolServices&imageId=countDSDestinationsSchoolServicesNotesImage&fieldId=countDSDestinationsSchoolServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsDestinationsSchoolServices&imageId=countDSDestinationsSchoolServicesNotesImage&fieldId=countDSDestinationsSchoolServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSDestinationsSchoolServicesNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsDestinationsSchoolServices&imageId=countDSDestinationsSchoolServicesNotesImage&fieldId=countDSDestinationsSchoolServicesNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsDestinationsSchoolServices&imageId=countDSDestinationsSchoolServicesNotesImage&fieldId=countDSDestinationsSchoolServicesNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
		
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.DSS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.DSS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_serviceEndDate"/></s:text>
			 <td><s:textfield id="DSS_serviceEndDate" cssClass="input-text" name="dspDetails.DSS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DSS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_serviceEndDate}">
		<td><s:textfield id="DSS_serviceEndDate" cssClass="input-text" name="dspDetails.DSS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DSS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.DSS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.DSS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.DSS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedDSS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.DSS_displyOtherVendorCode}">
	 <c:set var="ischeckedDSS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.DSS_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedDSS_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" colspan="3" class="listwhitetext">Initial&nbsp;Contact&nbsp;Date</td>

	    <c:if test="${not empty dspDetails.DSS_initialContactDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_initialContactDate"/></s:text>
			 <td><s:textfield id="DSS_initialContactDate" cssClass="input-text" name="dspDetails.DSS_initialContactDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DSS_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_initialContactDate}">
		<td><s:textfield id="DSS_initialContactDate" cssClass="input-text" name="dspDetails.DSS_initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="DSS_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

</tr>
<tr><td></td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.DSS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end dss -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DSS_vendorCode)<0) && dspDetails.DSS_vendorCode !='' && dspDetails.DSS_vendorCode !=null && dspDetails.DSS_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DSS')>-1)}">
<s:hidden name="dspDetails.DSS_vendorCode" />
<s:hidden name="dspDetails.DSS_vendorName" />
<c:if test="${not empty dspDetails.DSS_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DSS_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.DSS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DSS_serviceStartDate}">
		 <s:hidden   name="dspDetails.DSS_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.DSS_vendorContact" />
<c:if test="${not empty dspDetails.DSS_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DSS_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.DSS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DSS_serviceEndDate}">
		 <s:hidden   name="dspDetails.DSS_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.DSS_vendorEmail" />
<s:hidden name="dspDetails.DSS_vendorCodeEXSO" />
<s:hidden name="dspDetails.DSS_displyOtherVendorCode" />
<s:hidden name="dspDetails.DSS_paymentResponsibility" />
<s:hidden name="dspDetails.DSS_comment" />
<c:if test="${not empty dspDetails.DSS_initialContactDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.DSS_initialContactDate" /></s:text>
	<s:hidden  name="dspDetails.DSS_initialContactDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.DSS_initialContactDate}">
		 <s:hidden   name="dspDetails.DSS_initialContactDate"/> 
</c:if>

</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.DSS_vendorCode)<0) && dspDetails.DSS_vendorCode !='' && dspDetails.DSS_vendorCode !=null && dspDetails.DSS_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'DSS')>-1)}">
	<!-- begin dss -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('dss')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='DSS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="dss">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.DSS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('DSS_','${dspDetails.DSS_vendorCodeEXSO}'),chkIsVendorRedSky('DSS_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('DSS_','${dspDetails.DSS_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.DSS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','DSS_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.DSS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_serviceStartDate"/></s:text>
			 <td><s:textfield id="DSS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.DSS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_serviceStartDate}">
		<td><s:textfield id="DSS_serviceStartDate" cssClass="input-textUpper" name="dspDetails.DSS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.DSS_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.DSS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_serviceEndDate"/></s:text>
			 <td><s:textfield id="DSS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.DSS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_serviceEndDate}">
		<td><s:textfield id="DSS_serviceEndDate" cssClass="input-textUpper" name="dspDetails.DSS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.DSS_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.DSS_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:200px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedDSS_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.DSS_displyOtherVendorCode}">
	 <c:set var="ischeckedDSS_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.DSS_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedDSS_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">


<tr>
<td align="right" colspan="3" class="listwhitetext">Initial&nbsp;Contact&nbsp;Date</td>

	    <c:if test="${not empty dspDetails.DSS_initialContactDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.DSS_initialContactDate"/></s:text>
			 <td><s:textfield id="DSS_initialContactDate" cssClass="input-textUpper" name="dspDetails.DSS_initialContactDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="DSS_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.DSS_initialContactDate}">
		<td><s:textfield id="DSS_initialContactDate" cssClass="input-textUpper" name="dspDetails.DSS_initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="DSS_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

</tr>
<tr><td></td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.DSS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</configByCorp:fieldVisibility>
</tbody>
</table> 
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end dss -->
	</c:if>
	</c:if>	

<!--Final END DSS -->
<!-- Start HOB -->

		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.HOB_vendorCode)>-1 && dspDetails.HOB_vendorCode !='' && dspDetails.HOB_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hob')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HOB'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hob">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.HOB_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('HOB_','${dspDetails.HOB_vendorCodeEXSO}');chkIsVendorRedSky('HOB_');changeStatus();" /></td>
			<td align="left"width="10"> </td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.HOB_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','HOB_');" src="<c:url value='/images/address2.png'/>" />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.HOB_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_serviceStartDate"/></s:text>
			 <td><s:textfield id="HOB_serviceStartDate" cssClass="input-text" name="dspDetails.HOB_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_serviceStartDate}">
		<td><s:textfield id="HOB_serviceStartDate" cssClass="input-text" name="dspDetails.HOB_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDsHotelBookingsNotes == '0' || countDsHotelBookingsNotes == '' || countDsHotelBookingsNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDsHotelBookingsNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHotelBookings&imageId=countDsHotelBookingsNotesImage&fieldId=countDsHotelBookingsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHotelBookings&imageId=countDsHotelBookingsNotesImage&fieldId=countDsHotelBookingsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDsHotelBookingsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHotelBookings&imageId=countDsHotelBookingsNotesImage&fieldId=countDsHotelBookingsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHotelBookings&imageId=countDsHotelBookingsNotesImage&fieldId=countDsHotelBookingsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr><tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.HOB_vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.HOB_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_serviceEndDate"/></s:text>
			 <td><s:textfield id="HOB_serviceEndDate" cssClass="input-text" name="dspDetails.HOB_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_serviceEndDate}">
		<td><s:textfield id="HOB_serviceEndDate" cssClass="input-text" name="dspDetails.HOB_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.HOB_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.HOB_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedHOB_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HOB_displyOtherVendorCode}">
	 <c:set var="ischeckedHOB_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HOB_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedHOB_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  		</configByCorp:fieldVisibility>				
			</tr></tbody>
</table> 

<table width="100%" cellpadding="1" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="150px">Start&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.HOB_startDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_startDate"/></s:text>
			 <td width="50"><s:textfield id="HOB_startDate" cssClass="input-text" name="dspDetails.HOB_startDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_startDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_startDate}">
		<td width="50"><s:textfield id="HOB_startDate" cssClass="input-text" name="dspDetails.HOB_startDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_startDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		<td align="right" class="listwhitetext" width="150px">End&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.HOB_endDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_endDate"/></s:text>
			 <td width="50"><s:textfield id="HOB_endDate" cssClass="input-text" name="dspDetails.HOB_endDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_endDate}">
		<td width="50"><s:textfield id="HOB_endDate" cssClass="input-text" name="dspDetails.HOB_endDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
				<td align="right" class="listwhitetext">Hotel&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.HOB_hotelName" readonly="false"  cssStyle="width:239px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext">City&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.HOB_city" readonly="false"  cssStyle="width:137px;" maxlength="100" onchange="changeStatus();"/></td>

</tr>
</table> 
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!-- end HOB -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.HOB_vendorCode)<0 && dspDetails.HOB_vendorCode !='' && dspDetails.HOB_vendorCode !=null && dspDetails.HOB_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">		
		<s:hidden name="dspDetails.HOB_vendorCode" />
<s:hidden name="dspDetails.HOB_vendorName" />
<s:hidden name="dspDetails.HOB_vendorContact" />
<s:hidden name="dspDetails.HOB_vendorEmail" />
<s:hidden name="dspDetails.HOB_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.HOB_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HOB_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.HOB_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HOB_serviceStartDate}">
		 <s:hidden   name="dspDetails.HOB_serviceStartDate"/> 
</c:if>

<c:if test="${not empty dspDetails.HOB_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HOB_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.HOB_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HOB_serviceEndDate}">
		 <s:hidden   name="dspDetails.HOB_serviceEndDate"/> 
</c:if>

<s:hidden name="dspDetails.HOB_hotelName" />
<c:if test="${not empty dspDetails.HOB_startDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HOB_startDate" /></s:text>
	<s:hidden  name="dspDetails.HOB_startDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HOB_startDate}">
		 <s:hidden   name="dspDetails.HOB_startDate"/> 
</c:if>
<s:hidden name="dspDetails.HOB_city" />
<c:if test="${not empty dspDetails.HOB_endDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.HOB_endDate" /></s:text>
	<s:hidden  name="dspDetails.HOB_endDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.HOB_endDate}">
		 <s:hidden   name="dspDetails.HOB_endDate"/> 
</c:if>
<s:hidden name="dspDetails.HOB_vendorCodeEXSO" />
<s:hidden name="HOB_displyOtherVendorCode" />
		</c:if>
		</c:if> 
	 
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.HOB_vendorCode)<0 && dspDetails.HOB_vendorCode !='' && dspDetails.HOB_vendorCode !=null && dspDetails.HOB_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hob')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HOB'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hob">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>

			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.HOB_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('HOB_','${dspDetails.HOB_vendorCodeEXSO}');chkIsVendorRedSky('HOB_');changeStatus();" /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.HOB_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.HOB_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_serviceStartDate"/></s:text>
			 <td><s:textfield id="HOB_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HOB_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_serviceStartDate}">
		<td><s:textfield id="HOB_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HOB_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.HOB_vendorContact" readonly="true" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.HOB_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_serviceEndDate"/></s:text>
			 <td><s:textfield id="HOB_serviceEndDate" cssClass="input-textUpper" name="dspDetails.HOB_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_serviceEndDate}">
		<td><s:textfield id="HOB_serviceEndDate" cssClass="input-textUpper" name="dspDetails.HOB_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.HOB_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedHOB_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HOB_displyOtherVendorCode}">
	 <c:set var="ischeckedHOB_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HOB_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedHOB_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td> 
	</configByCorp:fieldVisibility>			
			</tr>
</tbody>
</table> 
<table width="100%" cellpadding="1" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="150px">Start&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.HOB_startDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_startDate"/></s:text>
			 <td width="50"><s:textfield id="HOB_startDate" cssClass="input-text" name="dspDetails.HOB_startDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_startDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_startDate}">
		<td width="50"><s:textfield id="HOB_startDate" cssClass="input-text" name="dspDetails.HOB_startDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_startDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		<td align="right" class="listwhitetext" width="150px">End&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.HOB_endDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOB_endDate"/></s:text>
			 <td width="50"><s:textfield id="HOB_endDate" cssClass="input-text" name="dspDetails.HOB_endDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.HOB_endDate}">
		<td width="50"><s:textfield id="HOB_endDate" cssClass="input-text" name="dspDetails.HOB_endDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOB_endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
				<td align="right" class="listwhitetext">Hotel&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.HOB_hotelName" readonly="false"  cssStyle="width:239px;" maxlength="100" onchange="changeStatus();"/></td>
				<td align="right" class="listwhitetext">City&nbsp;Name </td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.HOB_city" readonly="false"  cssStyle="width:137px;" maxlength="100" onchange="changeStatus();"/></td>

</tr>
</table> 
 	
  	</div>
  
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	

<!-- end HOB -->
<!-- Start FLB -->

		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FLB_vendorCode)>-1 && dspDetails.FLB_vendorCode !='' && dspDetails.FLB_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('flb')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='FLB'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="flb">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.FLB_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('FLB_','${dspDetails.FLB_vendorCodeEXSO}');chkIsVendorRedSky('FLB_');changeStatus();" /></td>
			<td align="left"width="10"> </td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.FLB_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','FLB_');" src="<c:url value='/images/address2.png'/>" />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.FLB_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_serviceStartDate"/></s:text>
			 <td><s:textfield id="FLB_serviceStartDate" cssClass="input-text" name="dspDetails.FLB_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_serviceStartDate}">
		<td><s:textfield id="FLB_serviceStartDate" cssClass="input-text" name="dspDetails.FLB_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDsFlightBookingsNotes == '0' || countDsFlightBookingsNotes == '' || countDsFlightBookingsNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDsFlightBookingsNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFlightBookings&imageId=countDsFlightBookingsNotesImage&fieldId=countDsFlightBookingsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFlightBookings&imageId=countDsFlightBookingsNotesImage&fieldId=countDsFlightBookingsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDsFlightBookingsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFlightBookings&imageId=countDsFlightBookingsNotesImage&fieldId=countDsFlightBookingsNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFlightBookings&imageId=countDsFlightBookingsNotesImage&fieldId=countDsFlightBookingsNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr><tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.FLB_vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.FLB_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_serviceEndDate"/></s:text>
			 <td><s:textfield id="FLB_serviceEndDate" cssClass="input-text" name="dspDetails.FLB_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_serviceEndDate}">
		<td><s:textfield id="FLB_serviceEndDate" cssClass="input-text" name="dspDetails.FLB_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.FLB_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.FLB_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedFLB_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.FLB_displyOtherVendorCode}">
	 <c:set var="ischeckedFLB_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.FLB_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedFLB_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  		</configByCorp:fieldVisibility>				
			</tr></tbody>
</table> 

<table width="100%" cellpadding="1" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
<tr>
		<td align="right" class="listwhitetext" width="150px">Departure&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_departureDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_departureDate"/></s:text>
			 <td><s:textfield id="FLB_departureDate" cssClass="input-text" name="dspDetails.FLB_departureDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_departureDate}">
		<td><s:textfield id="FLB_departureDate" cssClass="input-text" name="dspDetails.FLB_departureDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="100px">Arrival&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_arrivalDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_arrivalDate"/></s:text>
			 <td><s:textfield id="FLB_arrivalDate" cssClass="input-text" name="dspDetails.FLB_arrivalDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_arrivalDate}">
		<td><s:textfield id="FLB_arrivalDate" cssClass="input-text" name="dspDetails.FLB_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		
		<td align="right" class="listwhitetext" width="210px">Additional&nbsp;Booking&nbsp;Reminder&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate"/></s:text>
			 <td><s:textfield id="FLB_additionalBookingReminderDate" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_additionalBookingReminderDate}">
		<td><s:textfield id="FLB_additionalBookingReminderDate" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		
		<td align="right" width="30"><s:checkbox name="dspDetails.FLB_addOn" value="${dspDetails.FLB_addOn}" fieldValue="true" onclick="changeStatus();checkFLBDetails(this);"  cssStyle="margin-left:0px;" /></td>	
		<td align="right" class="listwhitetext" width="">Add&nbsp;for&nbsp;Second&nbsp;Flight</td>
</tr>
</table> 

<div id="checkFLBId">

 	 <table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" >
		
	
		<td align="right" class="listwhitetext" width="150px">Departure&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_departureDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_departureDate1"/></s:text>
			 <td><s:textfield id="FLB_departureDate1" cssClass="input-text" name="dspDetails.FLB_departureDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_departureDate1}">
		<td><s:textfield id="FLB_departureDate1" cssClass="input-text" name="dspDetails.FLB_departureDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="100px">Arrival&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_arrivalDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_arrivalDate1"/></s:text>
			 <td><s:textfield id="FLB_arrivalDate1" cssClass="input-text" name="dspDetails.FLB_arrivalDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_arrivalDate1}">
		<td><s:textfield id="FLB_arrivalDate1" cssClass="input-text" name="dspDetails.FLB_arrivalDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="211px">Additional&nbsp;Booking&nbsp;Reminder&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate1"/></s:text>
			 <td><s:textfield id="FLB_additionalBookingReminderDate1" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_additionalBookingReminderDate1}">
		<td><s:textfield id="FLB_additionalBookingReminderDate1" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</table>
</div>
  	</div>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	
	<!-- end FLB -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FLB_vendorCode)<0 && dspDetails.FLB_vendorCode !='' && dspDetails.FLB_vendorCode !=null && dspDetails.FLB_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">		
		<s:hidden name="dspDetails.FLB_vendorCode" />
<s:hidden name="dspDetails.FLB_vendorName" />
<s:hidden name="dspDetails.FLB_vendorContact" />
<s:hidden name="dspDetails.FLB_vendorEmail" />
<s:hidden name="dspDetails.FLB_displyOtherVendorCode" />
<c:if test="${not empty dspDetails.FLB_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.FLB_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_serviceStartDate}">
		 <s:hidden   name="dspDetails.FLB_serviceStartDate"/> 
</c:if>

<c:if test="${not empty dspDetails.FLB_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.FLB_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_serviceEndDate}">
		 <s:hidden   name="dspDetails.FLB_serviceEndDate"/> 
</c:if>
<c:if test="${not empty dspDetails.FLB_departureDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_departureDate" /></s:text>
	<s:hidden  name="dspDetails.FLB_departureDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_departureDate}">
		 <s:hidden   name="dspDetails.FLB_departureDate"/> 
</c:if>
<c:if test="${not empty dspDetails.FLB_arrivalDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_arrivalDate" /></s:text>
	<s:hidden  name="dspDetails.FLB_arrivalDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_arrivalDate}">
		 <s:hidden   name="dspDetails.FLB_arrivalDate"/> 
</c:if>	
<c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate" /></s:text>
	<s:hidden  name="dspDetails.FLB_additionalBookingReminderDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_additionalBookingReminderDate}">
		 <s:hidden   name="dspDetails.FLB_additionalBookingReminderDate"/> 
</c:if>	
		
 <s:hidden   name="dspDetails.FLB_addOn"/> 		
<c:if test="${not empty dspDetails.FLB_departureDate1}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_departureDate1" /></s:text>
	<s:hidden  name="dspDetails.FLB_departureDate1" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_departureDate1}">
		 <s:hidden   name="dspDetails.FLB_departureDate1"/> 
</c:if>
<c:if test="${not empty dspDetails.FLB_arrivalDate1}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_arrivalDate1" /></s:text>
	<s:hidden  name="dspDetails.FLB_arrivalDate1" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_arrivalDate1}">
		 <s:hidden   name="dspDetails.FLB_arrivalDate1"/> 
</c:if>	
<c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate1}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate1" /></s:text>
	<s:hidden  name="dspDetails.FLB_additionalBookingReminderDate1" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.FLB_additionalBookingReminderDate1}">
		 <s:hidden   name="dspDetails.FLB_additionalBookingReminderDate1"/> 
</c:if>	
<s:hidden name="dspDetails.FLB_vendorCodeEXSO" />
<s:hidden name="FLB_displyOtherVendorCode" />
		</c:if>
		</c:if> 
	 
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FLB_vendorCode)<0 && dspDetails.FLB_vendorCode !='' && dspDetails.FLB_vendorCode !=null && dspDetails.FLB_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('flb')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='FLB'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="flb">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>

			<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
			<td align="left" width="350">
			<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
			<tr>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.FLB_vendorCode" readonly="true" size="5" maxlength="10" 
			onchange="checkVendorNameRelo('FLB_','${dspDetails.FLB_vendorCodeEXSO}');chkIsVendorRedSky('FLB_');changeStatus();" /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.FLB_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			</td></tr>
			</table>
			</td>
<td align="right" class="listwhitetext" width="102px">Service Start</td>
	    <c:if test="${not empty dspDetails.FLB_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_serviceStartDate"/></s:text>
			 <td><s:textfield id="FLB_serviceStartDate" cssClass="input-textUpper" name="dspDetails.FLB_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_serviceStartDate}">
		<td><s:textfield id="FLB_serviceStartDate" cssClass="input-textUpper" name="dspDetails.FLB_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.FLB_vendorContact" readonly="true" size="57" maxlength="225" onchange="changeStatus();" /></td>
	
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.FLB_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_serviceEndDate"/></s:text>
			 <td><s:textfield id="FLB_serviceEndDate" cssClass="input-textUpper" name="dspDetails.FLB_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_serviceEndDate}">
		<td><s:textfield id="FLB_serviceEndDate" cssClass="input-textUpper" name="dspDetails.FLB_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
			<tr>
			<td align="right" class="listwhitetext" width="83">Vendor Email</td>
			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.FLB_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
			<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
			<c:set var="ischeckedFLB_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.FLB_displyOtherVendorCode}">
	 <c:set var="ischeckedFLB_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.FLB_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedFLB_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td> 
	</configByCorp:fieldVisibility>			
			</tr>
</tbody>
</table> 
<table width="100%" cellpadding="1" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel">
<tr>
		<td align="right" class="listwhitetext" width="150px">Departure&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_departureDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_departureDate"/></s:text>
			 <td><s:textfield id="FLB_departureDate" cssClass="input-text" name="dspDetails.FLB_departureDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_departureDate}">
		<td><s:textfield id="FLB_departureDate" cssClass="input-text" name="dspDetails.FLB_departureDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="100px">Arrival&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_arrivalDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_arrivalDate"/></s:text>
			 <td><s:textfield id="FLB_arrivalDate" cssClass="input-text" name="dspDetails.FLB_arrivalDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_arrivalDate}">
		<td><s:textfield id="FLB_arrivalDate" cssClass="input-text" name="dspDetails.FLB_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		
		<td align="right" class="listwhitetext" width="210px">Additional&nbsp;Booking&nbsp;Reminder&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate"/></s:text>
			 <td><s:textfield id="FLB_additionalBookingReminderDate" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_additionalBookingReminderDate}">
		<td><s:textfield id="FLB_additionalBookingReminderDate" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		
		<td align="right" width="30"><s:checkbox name="dspDetails.FLB_addOn" value="${dspDetails.FLB_addOn}" fieldValue="true" onclick="changeStatus();checkFLBDetails(this);"  cssStyle="margin-left:0px;" /></td>	
		<td align="right" class="listwhitetext" width="">Add&nbsp;for&nbsp;Second&nbsp;Flight</td>
</tr>
</table> 

<div id="checkFLBId">

 	 <table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" >
		
	
		<td align="right" class="listwhitetext" width="150px">Departure&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_departureDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_departureDate1"/></s:text>
			 <td><s:textfield id="FLB_departureDate1" cssClass="input-text" name="dspDetails.FLB_departureDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_departureDate1}">
		<td><s:textfield id="FLB_departureDate1" cssClass="input-text" name="dspDetails.FLB_departureDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_departureDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="100px">Arrival&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_arrivalDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_arrivalDate1"/></s:text>
			 <td><s:textfield id="FLB_arrivalDate1" cssClass="input-text" name="dspDetails.FLB_arrivalDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_arrivalDate1}">
		<td><s:textfield id="FLB_arrivalDate1" cssClass="input-text" name="dspDetails.FLB_arrivalDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_arrivalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td align="right" class="listwhitetext" width="211px">Additional&nbsp;Booking&nbsp;Reminder&nbsp;Date</td>
	    <c:if test="${not empty dspDetails.FLB_additionalBookingReminderDate1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FLB_additionalBookingReminderDate1"/></s:text>
			 <td><s:textfield id="FLB_additionalBookingReminderDate1" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FLB_additionalBookingReminderDate1}">
		<td><s:textfield id="FLB_additionalBookingReminderDate1" cssClass="input-text" name="dspDetails.FLB_additionalBookingReminderDate1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FLB_additionalBookingReminderDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</table>
</div>

 	
  	</div>
  
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>	


<!-- END FLB -->

<!-- start frl -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FRL_vendorCode)>-1 && dspDetails.FRL_vendorCode !='' && dspDetails.FRL_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('frl')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='FRL'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="frl">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.FRL_vendorCode" id="FRL_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('FRL_','${dspDetails.FRL_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('FRL_');" onblur="showContactImage('FRL_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('FRL_','${dspDetails.FRL_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.FRL_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
   <td align="left" valign="top" width="20">
	<div id="hidImageFRL" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','FRL_');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
<div id="hidNTImageFRL" style="vertical-align: middle;display: block;">
<a>
<img class="openpopup"  align="top" src="${pageContext.request.contextPath}/images/navarrows_05.png" 
onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCode'].value,
'hidNTImageFRL');">
</a>
</div>
</td>
	</td>
<td align="left">
<div style="float:left;width:52px;">
<div id="hidFRL_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidFRL_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.FRL_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_serviceStartDate"/></s:text>
			 <td><s:textfield id="FRL_serviceStartDate" cssClass="input-text" name="dspDetails.FRL_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FRL_serviceStartDate}">
		<td><s:textfield id="FRL_serviceStartDate" cssClass="input-text" name="dspDetails.FRL_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="FRL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSFurnitureRentalNotes == '0' || countDSFurnitureRentalNotes == '' || countDSFurnitureRentalNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSFurnitureRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSFurnitureRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'FRL')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSFurnitureRentalFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSFurnitureRentalFeebackId','FRL');"/></td>
<div id="DSFurnitureRentalFeebackId" class="cfDiv"></div>
</c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_FRL_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('FRL_','${dspDetails.FRL_vendorCode}','${dspDetails.FRL_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="350px" style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.FRL_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true"/></td>
 </tr></table></td>
 </c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isFRLmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'FRL')>-1}">
	 	<c:set var="isFRLmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="frl" id="frl" onclick="changeStatus();sendMailVendor('FRL');" value="${isFRLmailServiceType}" fieldValue="true" disabled="${isFRLmailServiceType}"/></td>
 </configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'FRL')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2">
<s:textfield cssClass="input-text" id="FRL_vendorContact" key="dspDetails.FRL_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="FRL_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('FRL',this);" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.FRL_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_serviceEndDate"/></s:text>
			 <td><s:textfield id="FRL_serviceEndDate" cssClass="input-text" name="dspDetails.FRL_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FRL_serviceEndDate}">
		<td><s:textfield id="FRL_serviceEndDate" cssClass="input-text" name="dspDetails.FRL_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.FRL_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.FRL_emailSent && fn1:indexOf(surveyEmailList,'FRL')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_emailSent"/></s:text>
			 <td><s:textfield id="FRL_emailSent" cssClass="input-textUpper" name="dspDetails.FRL_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendFRL" value="Resend Email" disabled=true onclick="resendEmail('FRL');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="FRL_vendorEmail" key="dspDetails.FRL_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageFRL" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorEmail'].value)" id="emailFRL" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.FRL_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.FRL_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedFRL_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.FRL_displyOtherVendorCode}">
	 <c:set var="ischeckedFRL_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.FRL_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedFRL_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" style="margin:0px;">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>





	<table>
	    <tr>
	      <td align="right"   class="listwhitetext">Rental Rate</td>
   	       <td align="left" class="listwhitetext" width="170"><s:textfield cssClass="input-text" id="FRL_rentalRate" key="dspDetails.FRL_rentalRate"/></td>
   	        
			<td align="right" class="listwhitetext" width="83">Currency</td>
			<td align="left" colspan="0"><s:select name="dspDetails.FRL_rentalCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
			
			<td align="right" class="listwhitetext" width="83">Month</td>
			<td align="left" colspan="0"><s:select name="dspDetails.FRL_month" list="{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'}" cssClass="list-menu" cssStyle="width:69px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
	       <td align="right"   class="listwhitetext">Rental Start Date</td>
   	       <c:if test="${not empty dspDetails.FRL_rentalStartDate}">
		       <s:text id="rentalStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_rentalStartDate"/></s:text>
		       <td width="65px" ><s:textfield id="FRL_rentalStartDate" cssClass="input-text" name="dspDetails.FRL_rentalStartDate" value="%{rentalStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.FRL_rentalStartDate}">
		       <td width="65px" ><s:textfield id="FRL_rentalStartDate" cssClass="input-text" name="dspDetails.FRL_rentalStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
			
	     </tr>
	    <tr>
	      <td align="right"   class="listwhitetext">Deposit</td>
   	       <td align="left" class="listwhitetext" width=""><s:textfield cssClass="input-text" id="FRL_deposit" key="dspDetails.FRL_deposit"/></td>
   	        
			<td align="right" class="listwhitetext" width="83">Currency</td>
			<td align="left" colspan="3"><s:select name="dspDetails.FRL_depositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
			
	       <td align="right"   class="listwhitetext">Rental End Date</td>
   	       <c:if test="${not empty dspDetails.FRL_rentalEndDate}">
		       <s:text id="rentalEndDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_rentalEndDate"/></s:text>
		       <td width="65px" ><s:textfield id="FRL_rentalEndDate" cssClass="input-text" name="dspDetails.FRL_rentalEndDate" value="%{rentalEndDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.FRL_rentalEndDate}">
		       <td width="65px" ><s:textfield id="FRL_rentalEndDate" cssClass="input-text" name="dspDetails.FRL_rentalEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
			
	     </tr>
		    <tr>
	      <td align="right"   class="listwhitetext">Termination Notice</td>
   	       <td align="left" class="listwhitetext" width=""><s:textfield cssClass="input-text" id="FRL_terminationNotice" key="dspDetails.FRL_terminationNotice"/></td>
	     </tr>
 		<tr>
	   <td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="54" name="dspDetails.FRL_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
	   </tr>		     
             	
           </table>	
</div>
</td>
</tr>
</table>
</td>
</tr>
		</c:if>
	</c:if>	
	<!-- end frl -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FRL_vendorCode)<0 && dspDetails.FRL_vendorCode !='' && dspDetails.FRL_vendorCode !=null && dspDetails.FRL_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">		
			<s:hidden name="dspDetails.FRL_vendorCode" />
	<s:hidden name="dspDetails.FRL_vendorName" />
	<s:hidden name="dspDetails.FRL_vendorCodeEXSO" />
	<s:hidden name="dspDetails.FRL_vendorContact" />
	<c:if test="${not empty dspDetails.FRL_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.FRL_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.FRL_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.FRL_serviceStartDate}">
		<s:hidden   name="dspDetails.FRL_serviceStartDate"/> 
	</c:if>
	<c:if test="${not empty dspDetails.FRL_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.FRL_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.FRL_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.FRL_serviceEndDate}">
			 <s:hidden   name="dspDetails.FRL_serviceEndDate"/> 
	</c:if>

	<s:hidden name="dspDetails.FRL_vendorEmail" />
	<s:hidden name="dspDetails.FRL_paymentResponsibility" />
	<s:hidden name="dspDetails.FRL_displyOtherVendorCode" />
	<s:hidden name="dspDetails.FRL_comment" />
	<c:if test="${not empty dspDetails.FRL_emailSent}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.FRL_emailSent" /></s:text>
		<s:hidden  name="dspDetails.FRL_emailSent" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.FRL_emailSent}">
		<s:hidden   name="dspDetails.FRL_emailSent"/> 
	</c:if>

	<c:if test="${not empty dspDetails.FRL_rentalStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.FRL_rentalStartDate" /></s:text>
		<s:hidden  name="dspDetails.FRL_rentalStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.FRL_rentalStartDate}">
		<s:hidden   name="dspDetails.FRL_rentalStartDate"/> 
	</c:if>


	<c:if test="${not empty dspDetails.FRL_rentalEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.FRL_rentalEndDate" /></s:text>
		<s:hidden  name="dspDetails.FRL_rentalEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.FRL_rentalEndDate}">
		<s:hidden   name="dspDetails.FRL_rentalEndDate"/> 
	</c:if>
	<s:hidden name="dspDetails.FRL_rentalRate" />
	<s:hidden name="dspDetails.FRL_deposit" />
	<s:hidden name="dspDetails.FRL_rentalCurrency" />
	<s:hidden name="dspDetails.FRL_depositCurrency" />
	<s:hidden name="dspDetails.FRL_month" />
	<s:hidden name="dspDetails.FRL_terminationNotice" />

		</c:if>
		</c:if> 
	 
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.FRL_vendorCode)<0 && dspDetails.FRL_vendorCode !='' && dspDetails.FRL_vendorCode !=null && dspDetails.FRL_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('frl')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='FRL'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="frl">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.FRL_vendorCode" id="FRL_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('FRL_','${dspDetails.FRL_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('FRL_');" onblur="showContactImage('FRL_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('FRL_','${dspDetails.FRL_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.FRL_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
   <td align="left" valign="top" width="20">
	<div id="hidImageFRL" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','FRL_');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
<div id="hidNTImageFRL" style="vertical-align: middle;display: block;">
<a>
<img class="openpopup"  align="top" src="${pageContext.request.contextPath}/images/navarrows_05.png" 
onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCode'].value,
'hidNTImageFRL');">
</a>
</div>
</td>
	</td>
<td align="left">
<div style="float:left;width:52px;">
<div id="hidFRL_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidFRL_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.FRL_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_serviceStartDate"/></s:text>
			 <td><s:textfield id="FRL_serviceStartDate" cssClass="input-text" name="dspDetails.FRL_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FRL_serviceStartDate}">
		<td><s:textfield id="FRL_serviceStartDate" cssClass="input-text" name="dspDetails.FRL_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="FRL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSFurnitureRentalNotes == '0' || countDSFurnitureRentalNotes == '' || countDSFurnitureRentalNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSFurnitureRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSFurnitureRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsFurnitureRental&imageId=countDSFurnitureRentalNotesImage&fieldId=countDSFurnitureRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'FRL')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSFurnitureRentalFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSFurnitureRentalFeebackId','FRL');"/></td>
<div id="DSFurnitureRentalFeebackId" class="cfDiv"></div>
</c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_FRL_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('FRL_','${dspDetails.FRL_vendorCode}','${dspDetails.FRL_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="350px" style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.FRL_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true"/></td>
 </tr></table></td>
 </c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isFRLmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'FRL')>-1}">
	 	<c:set var="isFRLmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="frl" id="frl" onclick="changeStatus();sendMailVendor('FRL');" value="${isFRLmailServiceType}" fieldValue="true" disabled="${isFRLmailServiceType}"/></td>
 </configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'FRL')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2">
<s:textfield cssClass="input-text" id="FRL_vendorContact" key="dspDetails.FRL_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="FRL_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('FRL',this);" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.FRL_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_serviceEndDate"/></s:text>
			 <td><s:textfield id="FRL_serviceEndDate" cssClass="input-text" name="dspDetails.FRL_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.FRL_serviceEndDate}">
		<td><s:textfield id="FRL_serviceEndDate" cssClass="input-text" name="dspDetails.FRL_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.FRL_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.FRL_emailSent && fn1:indexOf(surveyEmailList,'FRL')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_emailSent"/></s:text>
			 <td><s:textfield id="FRL_emailSent" cssClass="input-textUpper" name="dspDetails.FRL_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendFRL" value="Resend Email" disabled=true onclick="resendEmail('FRL');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="FRL_vendorEmail" key="dspDetails.FRL_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageFRL" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorEmail'].value)" id="emailFRL" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.FRL_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.FRL_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedFRL_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.FRL_displyOtherVendorCode}">
	 <c:set var="ischeckedFRL_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.FRL_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedFRL_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" style="margin:0px;">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>





	<table>
	    <tr>
	      <td align="right"   class="listwhitetext">Rental Rate</td>
   	       <td align="left" class="listwhitetext" width="170"><s:textfield cssClass="input-text" id="FRL_rentalRate" key="dspDetails.FRL_rentalRate"/></td>
   	        
			<td align="right" class="listwhitetext" width="83">Currency</td>
			<td align="left" colspan="0"><s:select name="dspDetails.FRL_rentalCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
			
			<td align="right" class="listwhitetext" width="83">Month</td>
			<td align="left" colspan="0"><s:select name="dspDetails.FRL_month" list="{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'}" cssClass="list-menu" cssStyle="width:69px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
	       <td align="right"   class="listwhitetext">Rental Start Date</td>
   	       <c:if test="${not empty dspDetails.FRL_rentalStartDate}">
		       <s:text id="rentalStartDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_rentalStartDate"/></s:text>
		       <td width="65px" ><s:textfield id="FRL_rentalStartDate" cssClass="input-text" name="dspDetails.FRL_rentalStartDate" value="%{rentalStartDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.FRL_rentalStartDate}">
		       <td width="65px" ><s:textfield id="FRL_rentalStartDate" cssClass="input-text" name="dspDetails.FRL_rentalStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
			
	     </tr>
	    <tr>
	      <td align="right"   class="listwhitetext">Deposit</td>
   	       <td align="left" class="listwhitetext" width=""><s:textfield cssClass="input-text" id="FRL_deposit" key="dspDetails.FRL_deposit"/></td>
   	        
			<td align="right" class="listwhitetext" width="83">Currency</td>
			<td align="left" colspan="3"><s:select name="dspDetails.FRL_depositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
			
	       <td align="right"   class="listwhitetext">Rental End Date</td>
   	       <c:if test="${not empty dspDetails.FRL_rentalEndDate}">
		       <s:text id="rentalEndDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.FRL_rentalEndDate"/></s:text>
		       <td width="65px" ><s:textfield id="FRL_rentalEndDate" cssClass="input-text" name="dspDetails.FRL_rentalEndDate" value="%{rentalEndDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.FRL_rentalEndDate}">
		       <td width="65px" ><s:textfield id="FRL_rentalEndDate" cssClass="input-text" name="dspDetails.FRL_rentalEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="FRL_rentalEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
			
	     </tr>
		    <tr>
	      <td align="right"   class="listwhitetext">Termination Notice</td>
   	       <td align="left" class="listwhitetext" width=""><s:textfield cssClass="input-text" id="FRL_terminationNotice" key="dspDetails.FRL_terminationNotice"/></td>
	     </tr>
 		<tr>
	   <td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="54" name="dspDetails.FRL_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
	   </tr>		     
             	
           </table>	
</div>
</td>
</tr>
</table>
</td>
</tr></c:if>
	</c:if>	
	<!-- end frl -->
	
<!-- start apu -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.APU_vendorCode)>-1 && dspDetails.APU_vendorCode !='' && dspDetails.APU_vendorCode !=null}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">	
		<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('apu')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='APU'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="apu">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.APU_vendorCode" id="APU_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('APU_','${dspDetails.APU_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('APU_');" onblur="showContactImage('APU_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('APU_','${dspDetails.APU_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.APU_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
   <td align="left" valign="top" width="20">
	<div id="hidImageAPU" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','APU_');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
<div id="hidNTImageAPU" style="vertical-align: middle;display: block;">
<a>
<img class="openpopup"  align="top" src="${pageContext.request.contextPath}/images/navarrows_05.png" 
onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCode'].value,
'hidNTImageAPU');">
</a>
</div>
</td>
	</td>
<td align="left">
<div style="float:left;width:52px;">
<div id="hidAPU_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidAPU_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.APU_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_serviceStartDate"/></s:text>
			 <td><s:textfield id="APU_serviceStartDate" cssClass="input-text" name="dspDetails.APU_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.APU_serviceStartDate}">
		<td><s:textfield id="APU_serviceStartDate" cssClass="input-text" name="dspDetails.APU_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="APU_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSAirportPickUpNotes == '0' || countDSAirportPickUpNotes == '' || countDSAirportPickUpNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSAirportPickUpNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAirportPickUpNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'APU')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAirportPickUpFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSAirportPickUpFeebackId','APU');"/></td>
<div id="DSAirportPickUpFeebackId" class="cfDiv"></div>
</c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_APU_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('APU_','${dspDetails.APU_vendorCode}','${dspDetails.APU_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="350px" style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.APU_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true"/></td>
 </tr></table></td>
 </c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isAPUmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'APU')>-1}">
	 	<c:set var="isAPUmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="apu" id="apu" onclick="changeStatus();sendMailVendor('APU');" value="${isAPUmailServiceType}" fieldValue="true" disabled="${isAPUmailServiceType}"/></td>
 </configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'APU')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2">
<s:textfield cssClass="input-text" id="APU_vendorContact" key="dspDetails.APU_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="APU_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('APU',this);" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.APU_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_serviceEndDate"/></s:text>
			 <td><s:textfield id="APU_serviceEndDate" cssClass="input-text" name="dspDetails.APU_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.APU_serviceEndDate}">
		<td><s:textfield id="APU_serviceEndDate" cssClass="input-text" name="dspDetails.APU_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.APU_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.APU_emailSent && fn1:indexOf(surveyEmailList,'APU')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_emailSent"/></s:text>
			 <td><s:textfield id="APU_emailSent" cssClass="input-textUpper" name="dspDetails.APU_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendAPU" value="Resend Email" disabled=true onclick="resendEmail('APU');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="APU_vendorEmail" key="dspDetails.APU_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageAPU" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorEmail'].value)" id="emailAPU" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.APU_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.APU_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedAPU_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.APU_displyOtherVendorCode}">
	 <c:set var="ischeckedAPU_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.APU_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedAPU_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" style="margin:0px;">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>
		<table>
	    <tr>
	      <td align="right"   class="listwhitetext">Flight Details</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:287px;" id="APU_flightDetails" key="dspDetails.APU_flightDetails"/></td>
   	        <td align="right"   class="listwhitetext" style="width:243px;">Arrival Date</td>
   	       <c:if test="${not empty dspDetails.APU_arrivalDate}">
		       <s:text id="arrivalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_arrivalDate"/></s:text>
		       <td width="65px" ><s:textfield id="APU_arrivalDate" cssClass="input-text" name="dspDetails.APU_arrivalDate" value="%{arrivalDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td>
		       <td><img id="APU_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.APU_arrivalDate}">
		       <td width="65px" ><s:textfield id="APU_arrivalDate" cssClass="input-text" name="dspDetails.APU_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td>
		       <td><img id="APU_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
	     </tr>
 	     <tr>
	      <td align="right"   class="listwhitetext">Driver Name</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:287px;" id="APU_driverName" key="dspDetails.APU_driverName"/></td>
	      <td align="right"   class="listwhitetext">Car Make</td>
   	       <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carMake" key="dspDetails.APU_carMake"/></td>
	     </tr>
 	     <tr>
	      <td align="right"   class="listwhitetext">Driver Phone Number</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:150px;" id="APU_driverPhoneNumber" key="dspDetails.APU_driverPhoneNumber"/></td>
	      <td align="right"   class="listwhitetext">Car Model</td>
   	       <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carModel" key="dspDetails.APU_carModel"/></td>
	     </tr>
 	     <tr>
	      <td align="right"   colspan="3" class="listwhitetext">Car Color</td>
   	       <td align="left" class="listwhitetext" colspan="4"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carColor" key="dspDetails.APU_carColor"/></td>
	     </tr>
        </table>		
</div>
</td>
</tr>
</table>
</td>
</tr>
		</c:if>
	</c:if>	
	<!-- end apu -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.APU_vendorCode)<0 && dspDetails.APU_vendorCode !='' && dspDetails.APU_vendorCode !=null && dspDetails.APU_displyOtherVendorCode==false}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">		
	<s:hidden name="dspDetails.APU_vendorCode" />
	<s:hidden name="dspDetails.APU_vendorName" />
	<s:hidden name="dspDetails.APU_vendorCodeEXSO" />
	<s:hidden name="dspDetails.APU_vendorContact" />
	<c:if test="${not empty dspDetails.APU_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.APU_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.APU_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.APU_serviceStartDate}">
		<s:hidden   name="dspDetails.APU_serviceStartDate"/> 
	</c:if>
	<c:if test="${not empty dspDetails.APU_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.APU_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.APU_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.APU_serviceEndDate}">
			 <s:hidden   name="dspDetails.APU_serviceEndDate"/> 
	</c:if>

	<s:hidden name="dspDetails.APU_vendorEmail" />
	<s:hidden name="dspDetails.APU_displyOtherVendorCode" />
	<s:hidden name="dspDetails.APU_paymentResponsibility" />
	<c:if test="${not empty dspDetails.APU_emailSent}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.APU_emailSent" /></s:text>
		<s:hidden  name="dspDetails.APU_emailSent" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.APU_emailSent}">
		<s:hidden   name="dspDetails.APU_emailSent"/> 
	</c:if>

	<c:if test="${not empty dspDetails.APU_arrivalDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.APU_arrivalDate" /></s:text>
		<s:hidden  name="dspDetails.APU_arrivalDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.APU_arrivalDate}">
		<s:hidden   name="dspDetails.APU_arrivalDate"/> 
	</c:if>
	<s:hidden name="dspDetails.APU_flightDetails" />
	<s:hidden name="dspDetails.APU_driverName" />
	<s:hidden name="dspDetails.APU_driverPhoneNumber" />
	<s:hidden name="dspDetails.APU_carMake" />
	<s:hidden name="dspDetails.APU_carModel" />
	<s:hidden name="dspDetails.APU_carColor" />
		</c:if>
		</c:if> 
	 
		  <c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.APU_vendorCode)<0 && dspDetails.APU_vendorCode !='' && dspDetails.APU_vendorCode !=null && dspDetails.APU_displyOtherVendorCode}">
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('apu')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='APU'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="apu">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.APU_vendorCode" id="APU_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('APU_','${dspDetails.APU_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('APU_');" onblur="showContactImage('APU_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('APU_','${dspDetails.APU_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.APU_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
   <td align="left" valign="top" width="20">
	<div id="hidImageAPU" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','APU_');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
<div id="hidNTImageAPU" style="vertical-align: middle;display: block;">
<a>
<img class="openpopup"  align="top" src="${pageContext.request.contextPath}/images/navarrows_05.png" 
onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCode'].value,
'hidNTImageAPU');">
</a>
</div>
</td>
	</td>
<td align="left">
<div style="float:left;width:52px;">
<div id="hidAPU_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidAPU_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.APU_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_serviceStartDate"/></s:text>
			 <td><s:textfield id="APU_serviceStartDate" cssClass="input-text" name="dspDetails.APU_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.APU_serviceStartDate}">
		<td><s:textfield id="APU_serviceStartDate" cssClass="input-text" name="dspDetails.APU_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="APU_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSAirportPickUpNotes == '0' || countDSAirportPickUpNotes == '' || countDSAirportPickUpNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSAirportPickUpNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAirportPickUpNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAirportPickUp&imageId=countDSAirportPickUpNotesImage&fieldId=countDSAirportPickUpNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'APU')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAirportPickUpFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSAirportPickUpFeebackId','APU');"/></td>
<div id="DSAirportPickUpFeebackId" class="cfDiv"></div>
</c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_APU_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('APU_','${dspDetails.APU_vendorCode}','${dspDetails.APU_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="350px" style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.APU_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true"/></td>
 </tr></table></td>
 </c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isAPUmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'APU')>-1}">
	 	<c:set var="isAPUmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="apu" id="apu" onclick="changeStatus();sendMailVendor('APU');" value="${isAPUmailServiceType}" fieldValue="true" disabled="${isAPUmailServiceType}"/></td>
 </configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'APU')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2">
<s:textfield cssClass="input-text" id="APU_vendorContact" key="dspDetails.APU_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="APU_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('APU',this);" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.APU_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_serviceEndDate"/></s:text>
			 <td><s:textfield id="APU_serviceEndDate" cssClass="input-text" name="dspDetails.APU_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.APU_serviceEndDate}">
		<td><s:textfield id="APU_serviceEndDate" cssClass="input-text" name="dspDetails.APU_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="APU_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.APU_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.APU_emailSent && fn1:indexOf(surveyEmailList,'APU')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_emailSent"/></s:text>
			 <td><s:textfield id="APU_emailSent" cssClass="input-textUpper" name="dspDetails.APU_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendAPU" value="Resend Email" disabled=true onclick="resendEmail('APU');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="APU_vendorEmail" key="dspDetails.APU_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageAPU" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorEmail'].value)" id="emailAPU" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.APU_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.APU_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedAPU_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.APU_displyOtherVendorCode}">
	 <c:set var="ischeckedAPU_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.APU_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedAPU_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" style="margin:0px;">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>
		<table>
	    <tr>
	      <td align="right"   class="listwhitetext">Flight Details</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:287px;" id="APU_flightDetails" key="dspDetails.APU_flightDetails"/></td>
   	        <td align="right"   class="listwhitetext" style="width:243px;">Arrival Date</td>
   	       <c:if test="${not empty dspDetails.APU_arrivalDate}">
		       <s:text id="arrivalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.APU_arrivalDate"/></s:text>
		       <td width="65px" ><s:textfield id="APU_arrivalDate" cssClass="input-text" name="dspDetails.APU_arrivalDate" value="%{arrivalDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td>
		       <td><img id="APU_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	       </c:if>
               <c:if test="${empty dspDetails.APU_arrivalDate}">
		       <td width="65px" ><s:textfield id="APU_arrivalDate" cssClass="input-text" name="dspDetails.APU_arrivalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td>
		       <td><img id="APU_arrivalDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
               </c:if> 
	     </tr>
 	     <tr>
	      <td align="right"   class="listwhitetext">Driver Name</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:287px;" id="APU_driverName" key="dspDetails.APU_driverName"/></td>
	      <td align="right"   class="listwhitetext">Car Make</td>
   	       <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carMake" key="dspDetails.APU_carMake"/></td>
	     </tr>
 	     <tr>
	      <td align="right"   class="listwhitetext">Driver Phone Number</td>
   	       <td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" cssStyle="width:150px;" id="APU_driverPhoneNumber" key="dspDetails.APU_driverPhoneNumber"/></td>
	      <td align="right"   class="listwhitetext">Car Model</td>
   	       <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carModel" key="dspDetails.APU_carModel"/></td>
	     </tr>
 	     <tr>
	      <td align="right"   colspan="3" class="listwhitetext">Car Color</td>
   	       <td align="left" class="listwhitetext" colspan="4"><s:textfield cssClass="input-text" cssStyle="width:91px;" id="APU_carColor" key="dspDetails.APU_carColor"/></td>
	     </tr>
        </table>		
</div>
</td>
</tr>
</table>
</td>
</tr></c:if>
	</c:if>	
	<!-- end apu -->
<!-- begin INS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.INS_vendorCode)>-1) && dspDetails.INS_vendorCode !='' && dspDetails.INS_vendorCode !=null}">
<c:if test="${fn1:indexOf(serviceOrder.serviceType,'INS')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ins')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='INS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ins">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.INS_vendorCode" id="INS_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('INS_','${dspDetails.INS_vendorCodeEXSO}'),chkIsVendorRedSky('INS_'),changeStatus();" onblur="showContactImage('INS_')" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.INS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
 <c:if test="${not empty dspDetails.INS_vendorInitiation}">
	<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INS_vendorInitiation"/></s:text>
	<td><s:textfield id="INS_vendorInitiation" cssClass="input-text" name="dspDetails.INS_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INS_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.INS_vendorInitiation}">
	<td><s:textfield id="INS_vendorInitiation" cssClass="input-text" name="dspDetails.INS_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="INS_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSInternationalInsuranceNotes == '0' || countDSInternationalInsuranceNotes == '' || countDSInternationalInsuranceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSInternationalInsuranceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsInternationalInsurance&imageId=countDSInternationalInsuranceNotesImage&fieldId=countDSInternationalInsuranceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DSInternationalInsurance&imageId=countDSInternationalInsuranceNotesImage&fieldId=countDSInternationalInsuranceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSInternationalInsuranceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsInternationalInsurance&imageId=countDSInternationalInsuranceNotesImage&fieldId=countDSInternationalInsuranceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DSInternationalInsurance&imageId=countDSInternationalInsuranceNotesImage&fieldId=countDSInternationalInsuranceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.INS_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INS_serviceStartDate"/></s:text>
			 <td style="width:65px;"><s:textfield id="INS_serviceStartDate" cssClass="input-text" name="dspDetails.INS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.INS_serviceStartDate}">
		<td style="width:65px;"><s:textfield id="INS_serviceStartDate" cssClass="input-text" name="dspDetails.INS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="INS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.INS_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INS_serviceEndDate"/></s:text>
			 <td><s:textfield id="INS_serviceEndDate" cssClass="input-text" name="dspDetails.INS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.INS_serviceEndDate}">
			<td><s:textfield id="INS_serviceEndDate" cssClass="input-text" name="dspDetails.INS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.INS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="INS_vendorContact" key="dspDetails.INS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300" colspan="2"><s:textfield cssClass="input-text" id="INS_vendorEmail" key="dspDetails.INS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
</td>
</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="100" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.INS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</c:if>
</c:if>

<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.INS_vendorCode)<0) && dspDetails.INS_vendorCode !='' && dspDetails.INS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'INS')>-1)}">
<s:hidden name="dspDetails.INS_vendorCode" />
<s:hidden name="dspDetails.INS_vendorName" />
<s:hidden name="dspDetails.INS_vendorContact" />
<s:hidden name="dspDetails.INS_vendorEmail" />
<s:hidden name="dspDetails.INS_vendorCodeEXSO" />
<s:hidden name="dspDetails.INS_comment" />
	 <c:if test="${not empty dspDetails.INS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.INS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INS_serviceStartDate}">
		 <s:hidden   name="dspDetails.INS_serviceStartDate"/> 
	 </c:if>

	 <c:if test="${not empty dspDetails.INS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.INS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INS_serviceEndDate}">
		 <s:hidden   name="dspDetails.INS_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.INS_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INS_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.INS_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INS_vendorInitiation}">
		 <s:hidden   name="dspDetails.INS_vendorInitiation"/> 
	 </c:if>
</c:if>
</c:if>		
<!-- END INS -->
<!-- begin INP -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.INP_vendorCode)>-1) && dspDetails.INP_vendorCode !='' && dspDetails.INP_vendorCode !=null}">
<c:if test="${fn1:indexOf(serviceOrder.serviceType,'INP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('inp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='INP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="inp">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.INP_vendorCode" id="INP_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('INP_','${dspDetails.INS_vendorCodeEXSO}'),chkIsVendorRedSky('INP_'),changeStatus();" onblur="showContactImage('INP_')" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-textUpper" key="dspDetails.INP_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
 <c:if test="${not empty dspDetails.INP_vendorInitiation}">
	<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INP_vendorInitiation"/></s:text>
	<td><s:textfield id="INP_vendorInitiation" cssClass="input-text" name="dspDetails.INP_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INP_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.INP_vendorInitiation}">
	<td><s:textfield id="INP_vendorInitiation" cssClass="input-text" name="dspDetails.INP_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="INP_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSInternationalPensionNotes == '0' || countDSInternationalPensionNotes == '' || countDSInternationalPensionNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSInternationalPensionNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsInternationalPension&imageId=countDSInternationalPensionNotesImage&fieldId=countDSInternationalPensionNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsInternationalPension&imageId=countDSInternationalPensionNotesImage&fieldId=countDSInternationalPensionNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSInternationalPensionNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsInternationalPension&imageId=countDSInternationalPensionNotesImage&fieldId=countDSInternationalPensionNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsInternationalPension&imageId=countDSInternationalPensionNotesImage&fieldId=countDSInternationalPensionNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.INP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INP_serviceStartDate"/></s:text>
			 <td style="width:65px;"><s:textfield id="INP_serviceStartDate" cssClass="input-text" name="dspDetails.INP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.INP_serviceStartDate}">
		<td style="width:65px;"><s:textfield id="INP_serviceStartDate" cssClass="input-text" name="dspDetails.INP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="INP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.INP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.INP_serviceEndDate"/></s:text>
			 <td><s:textfield id="INP_serviceEndDate" cssClass="input-text" name="dspDetails.INP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.INP_serviceEndDate}">
		<td><s:textfield id="INP_serviceEndDate" cssClass="input-text" name="dspDetails.INP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="INP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.INP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="INP_vendorContact" key="dspDetails.INP_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300" colspan="2"><s:textfield cssClass="input-text" id="INP_vendorEmail" key="dspDetails.INP_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
</td>

</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="100" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.INP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</c:if>
</c:if>

<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.INP_vendorCode)<0) && dspDetails.INP_vendorCode !='' && dspDetails.INP_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'INP')>-1)}">
<s:hidden name="dspDetails.INP_vendorCode" />
<s:hidden name="dspDetails.INP_vendorName" />
<s:hidden name="dspDetails.INP_vendorContact" />
<s:hidden name="dspDetails.INP_vendorEmail" />
<s:hidden name="dspDetails.INP_vendorCodeEXSO" />
<s:hidden name="dspDetails.INP_comment" />
	 <c:if test="${not empty dspDetails.INP_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INP_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.INP_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INP_serviceStartDate}">
		 <s:hidden   name="dspDetails.INP_serviceStartDate"/> 
	 </c:if>

	 <c:if test="${not empty dspDetails.INP_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INP_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.INP_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INP_serviceEndDate}">
		 <s:hidden   name="dspDetails.INP_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.INP_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.INP_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.INP_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.INP_vendorInitiation}">
		 <s:hidden   name="dspDetails.INP_vendorInitiation"/> 
	 </c:if>
</c:if>
</c:if>		
<!-- END INP -->
<!-- begin EDA -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.EDA_vendorCode)>-1) && dspDetails.EDA_vendorCode !='' && dspDetails.EDA_vendorCode !=null}">
<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EDA')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('eda')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='EDA'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="eda">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.EDA_vendorCode" id="EDA_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('EDA_','${dspDetails.EDA_vendorCodeEXSO}'),chkIsVendorRedSky('EDA_'),changeStatus();" onblur="showContactImage('EDA_')" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-textUpper" key="dspDetails.EDA_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
 <c:if test="${not empty dspDetails.EDA_vendorInitiation}">
	<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EDA_vendorInitiation"/></s:text>
	<td><s:textfield id="EDA_vendorInitiation" cssClass="input-text" name="dspDetails.EDA_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EDA_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.EDA_vendorInitiation}">
	<td><s:textfield id="EDA_vendorInitiation" cssClass="input-text" name="dspDetails.EDA_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="EDA_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSEducationAdviceNotes == '0' || countDSEducationAdviceNotes == '' || countDSEducationAdviceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSEducationAdviceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsEducationAdvice&imageId=countDSEducationAdviceNotesImage&fieldId=countDSEducationAdviceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsEducationAdvice&imageId=countDSEducationAdviceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSEducationAdviceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsEducationAdvice&imageId=countDSEducationAdviceNotesImage&fieldId=countDSEducationAdviceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsEducationAdvice&imageId=countDSEducationAdviceNotesImage&fieldId=countDSSpousalAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
	<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.EDA_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EDA_serviceStartDate"/></s:text>
			 <td style="width:65px;"><s:textfield id="EDA_serviceStartDate" cssClass="input-text" name="dspDetails.EDA_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EDA_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EDA_serviceStartDate}">
		<td style="width:65px;"><s:textfield id="EDA_serviceStartDate" cssClass="input-text" name="dspDetails.EDA_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="EDA_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
	<td align="right" class="listwhitetext">Service Finish</td>
    <c:if test="${not empty dspDetails.EDA_serviceEndDate}">
		 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EDA_serviceEndDate"/></s:text>
		 <td><s:textfield id="EDA_serviceEndDate" cssClass="input-text" name="dspDetails.EDA_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EDA_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
    <c:if test="${empty dspDetails.EDA_serviceEndDate}">
	<td><s:textfield id="EDA_serviceEndDate" cssClass="input-text" name="dspDetails.EDA_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EDA_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.EDA_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'EDA')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="EDA_vendorContact" key="dspDetails.EDA_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300" colspan="2"><s:textfield cssClass="input-text" id="EDA_vendorEmail" key="dspDetails.EDA_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
</td>

</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="100" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.EDA_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</c:if>
</c:if>

<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.EDA_vendorCode)<0) && dspDetails.EDA_vendorCode !='' && dspDetails.EDA_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'EDA')>-1)}">
<s:hidden name="dspDetails.EDA_vendorCode" />
<s:hidden name="dspDetails.EDA_vendorName" />
<s:hidden name="dspDetails.EDA_vendorContact" />
<s:hidden name="dspDetails.EDA_vendorEmail" />
<s:hidden name="dspDetails.EDA_vendorCodeEXSO" />
<s:hidden name="dspDetails.EDA_comment" />
	 <c:if test="${not empty dspDetails.EDA_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.EDA_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.EDA_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.EDA_serviceStartDate}">
		 <s:hidden   name="dspDetails.EDA_serviceStartDate"/> 
	 </c:if>

	 <c:if test="${not empty dspDetails.EDA_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.EDA_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.EDA_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.EDA_serviceEndDate}">
		 <s:hidden   name="dspDetails.EDA_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.EDA_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.EDA_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.EDA_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.EDA_vendorInitiation}">
		 <s:hidden   name="dspDetails.EDA_vendorInitiation"/> 
	 </c:if>
</c:if>
</c:if>		
<!-- END EDA -->
<!-- begin TAS -->
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TAS_vendorCode)>-1) && dspDetails.TAS_vendorCode !='' && dspDetails.TAS_vendorCode !=null}">
<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAS')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('tas')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TAS'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="tas">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TAS_vendorCode" id="TAS_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('TAS_','${dspDetails.TAS_vendorCodeEXSO}'),chkIsVendorRedSky('TAS_'),changeStatus();" onblur="showContactImage('TAS_')" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-textUpper" key="dspDetails.TAS_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
 <c:if test="${not empty dspDetails.TAS_vendorInitiation}">
	<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAS_vendorInitiation"/></s:text>
	<td><s:textfield id="TAS_vendorInitiation" cssClass="input-text" name="dspDetails.TAS_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAS_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.TAS_vendorInitiation}">
	<td><s:textfield id="TAS_vendorInitiation" cssClass="input-text" name="dspDetails.TAS_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="TAS_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSTaxSupportNotes == '0' || countDSTaxSupportNotes == '' || countDSTaxSupportNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSTaxSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTaxSupport&imageId=countDSTaxSupportNotesImage&fieldId=countDSTaxSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTaxSupport&imageId=countDSTaxSupportNotesImage&fieldId=countDSTaxSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSTaxSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsTaxSupport&imageId=countDSTaxSupportNotesImage&fieldId=countDSTaxSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsTaxSupport&imageId=countDSTaxSupportNotesImage&fieldId=countDSTaxSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
	<td align="right" width="100px" class="listwhitetext">Service Start</td>
    <c:if test="${not empty dspDetails.TAS_serviceStartDate}">
		 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAS_serviceStartDate"/></s:text>
		 <td><s:textfield id="TAS_serviceStartDate" cssClass="input-text" name="dspDetails.TAS_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
    <c:if test="${empty dspDetails.TAS_serviceStartDate}">
	<td><s:textfield id="TAS_serviceStartDate" cssClass="input-text" name="dspDetails.TAS_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="TAS_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<td align="right" class="listwhitetext">Service Finish</td>
    <c:if test="${not empty dspDetails.TAS_serviceEndDate}">
		 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TAS_serviceEndDate"/></s:text>
		 <td><s:textfield id="TAS_serviceEndDate" cssClass="input-text" name="dspDetails.TAS_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
    <c:if test="${empty dspDetails.TAS_serviceEndDate}">
	<td><s:textfield id="TAS_serviceEndDate" cssClass="input-text" name="dspDetails.TAS_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TAS_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TAS_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'TAS')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="TAS_vendorContact" key="dspDetails.TAS_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="TAS_vendorEmail" key="dspDetails.TAS_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
</td>

</tr>
<tr><td colspan="4" align="right" class="listwhitetext" >
<table cellpadding="2" cellspacing="0" width="100%" border="0"  style="margin: 0px;">
<tr><td width="83" class="listwhitetext" align="right" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TAS_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> 
</td></tr>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</c:if>
</c:if>

<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TAS_vendorCode)<0) && dspDetails.TAS_vendorCode !='' && dspDetails.TAS_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'TAS')>-1)}">
<s:hidden name="dspDetails.TAS_vendorCode" />
<s:hidden name="dspDetails.TAS_vendorName" />
<s:hidden name="dspDetails.TAS_vendorContact" />
<s:hidden name="dspDetails.TAS_vendorEmail" />
<s:hidden name="dspDetails.TAS_vendorCodeEXSO" />
<s:hidden name="dspDetails.TAS_comment" />
	 <c:if test="${not empty dspDetails.TAS_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TAS_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.TAS_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TAS_serviceStartDate}">
		 <s:hidden   name="dspDetails.TAS_serviceStartDate"/> 
	 </c:if>

	 <c:if test="${not empty dspDetails.TAS_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TAS_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.TAS_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TAS_serviceEndDate}">
		 <s:hidden   name="dspDetails.TAS_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.TAS_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TAS_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.TAS_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.TAS_vendorInitiation}">
		 <s:hidden   name="dspDetails.TAS_vendorInitiation"/> 
	 </c:if>
</c:if>
</c:if>		
<!-- END TAS -->	