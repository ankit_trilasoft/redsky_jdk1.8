package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.QuotesFile;
import java.util.List;

public interface QuotesFileDao extends GenericDao<QuotesFile, Long> {   
   
	public List<QuotesFile> findByLastName(String lastName);
	
}