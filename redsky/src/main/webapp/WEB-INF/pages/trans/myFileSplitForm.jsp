<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />
<style>
	.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
<script>
function slpitDoc(){
	var pageIndex = getPageNumber();
	if(pageIndex >= 0){
		window.open('splitDocForm.html?id=${fileId}&pageIndex='+pageIndex+'&decorator=popup&popup=true','docs','left=100,top=100,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
	}else{
		alert('Please select the page to split.');
		return false;
	}	
}
</script>
</head>
<s:form>
<s:hidden name="from"  value="<%=request.getParameter("from") %>"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>	
<s:hidden name="relatedDocs"  value="<%=request.getParameter("relatedDocs") %>" />
<c:set var="relatedDocs" value="<%=request.getParameter("relatedDocs") %>" />
<s:hidden id="forQuotation" name="forQuotation" />
	<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
	<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
	
	<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
	<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
	
	<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
	<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<c:if test="${from == 'main'}">
	<c:if test="${myFileFor=='CF'}">
		<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
		<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
		<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
		<c:set var="noteFor" value="CustomerFile" scope="session"/>
		<c:if test="${empty customerFile.id}">
			<c:set var="isTrue" value="false" scope="request"/>
		</c:if>
		<c:if test="${not empty customerFile.id}">
			<c:set var="isTrue" value="true" scope="request"/>
		</c:if>
	</c:if>
	<c:if test="${myFileFor!='CF'}">
		<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
		<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
		<c:set var="noteFor" value="ServiceOrder" scope="session"/>
		<c:if test="${empty serviceOrder.id}">
			<c:set var="isTrue" value="false" scope="request"/>
		</c:if>
		<c:if test="${not empty serviceOrder.id}">
			<c:set var="isTrue" value="true" scope="request"/>
		</c:if>
	</c:if>
</c:if>

<c:if test="${from == 'main'}">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${myFileFor!='CF'}"> 
<div id="layer6" style="width:100%; ">
<div id="newmnav" style="float:left; ">
               <c:choose>
	            <c:when test="${forQuotation!='QC'}">
            <ul>

	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     	 <c:if test="${forwardingTabVal!='Y'}"> 
	     				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	     			</c:if>
	     			<c:if test="${forwardingTabVal=='Y'}">
	     				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	     			</c:if>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	             <c:if test="${serviceOrder.job =='RLO'}">
	             <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <c:if test="${serviceOrder.job !='RLO'}">
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </sec-auth:authComponent>
	              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	       </ul> </c:when>
	      <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
       
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</div>
</div>
</c:if>
<c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:85%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
		<div id="newmnav">
		  <ul>
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		  	
		  </ul>
		</div>
		 </c:when>
	      <c:otherwise>
	      <div id="newmnav">
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		   </div>
		</c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top: -14px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:770px">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  
</c:if>


	<div id="Layer1" style="width: 80%;">
		<div id="newmnav">
			<ul>
			<c:choose>
				<c:when test="${from == 'main'}">
					<c:if test="${relatedDocs == 'No'}">
					<c:if test="${fileCabinetView ne 'Document Centre' }">
						<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&secure=false&active=true&forQuotation=${forQuotation}"><span>Document List</span></a></li>
					</c:if>
					<c:if test="${fileCabinetView == 'Document Centre' }">
						<li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&secure=false&active=true&forQuotation=${forQuotation}"><span>Document Centre</span></a></li>
					</c:if>
					</c:if>
					<c:if test="${relatedDocs == 'Yes'}">
						<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&secure=false&active=true&forQuotation=${forQuotation}"><span>Related Document List</span></a></li>
					</c:if>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Split Document<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				</c:when>
				<c:otherwise>
					<li><a href="editFileUploadDOC.html?id=${fileId}"><span>Upload Document</span></a></li>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Split Document<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${fileCabinetView ne 'Document Centre' }">
					<li><a href="docProcess.html"><span>Document List</span></a></li>
				</c:if>
					<c:if test="${fileCabinetView == 'Document Centre' }">
					<li><a href="docProcess.html"><span>Document Centre</span></a></li>
				</c:if>
				</c:otherwise>
			</c:choose>
				
			</ul>
		</div>	
		
		<div class="spn">&nbsp;</div>
		<c:set var="fileId" value="<%= (String)request.getParameter("fileId")%>" />
		<s:hidden name="fileId" value="<%= (String)request.getParameter("fileId")%>" />
		<div id="content" align="center">
			<div id="liquid-round-top">
			   	<div class="top"><span></span></div>
			   	<div class="center-content">
					<table cellspacing="1" cellpadding="0" border="0" style="" width="100%">
						<tr><td height="10"></td></tr>
						<tr>
							<td class="bgblue" colspan="2">Uploaded Document To Split</td>
						</tr>
						<tr><td height="10px"></td></tr>
						<tr>	
							<td height="100px">
							<td>
								<%@ include file="/common/enable-dojo-imageviewer.jsp"%>
							</td>
						</tr>	
						
						<tr><td height="15px"></td></tr>
						<tr>	
						  	<td width="25px"></td>			
							<td colspan="2"><input type="button" class="cssbutton" style="width:175px; height:25px" align="top" value="Split Document" onclick="return slpitDoc();"/></td>
						</tr>
						<tr><td height="20"></td></tr>		
					</table> 
			   	</div>
				<div class="bottom-header"><span></span></div>
			</div>
		</div> 
	</div>
</s:form>
<script type="text/javascript">
progressBarAutoSave('1');
</script>