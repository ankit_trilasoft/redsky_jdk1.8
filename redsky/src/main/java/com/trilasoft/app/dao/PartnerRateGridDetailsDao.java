package com.trilasoft.app.dao;

import java.util.HashMap;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.model.PartnerRateGridDetails;

public interface PartnerRateGridDetailsDao extends GenericDao<PartnerRateGridDetails, Long> {
	public List  getRateGridDetais(Long id, String sessionCorpID);
	public int updateRateGridData(Long id, String sessionCorpID, HashMap saveDataDetailsMap);
	public List checkPartnerRateGridId(Long id, String sessionCorpID);
	public int updateRateGridBasis(Long id, String sessionCorpID, HashMap saveBasisMap); 
	public int updateRateGridLabel(Long id, String sessionCorpID, HashMap saveLabelMap);
	public List getRateGridDetaisData(long id, String sessionCorpID);
	
}
