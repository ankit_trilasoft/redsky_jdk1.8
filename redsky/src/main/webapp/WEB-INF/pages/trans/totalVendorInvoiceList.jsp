<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="reverseInvoiceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='reverseInvoiceList.heading'/>"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<c:if test="${totalVendorInvoiceList!='[]'}"> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" >
<tr valign="top"> 	
	<td align="left"><b>Approved Vendor Invoice List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="totalVendorInvoiceList" action="" method="post">  
<s:set name="totalVendorInvoiceList" value="totalVendorInvoiceList" scope="request"/> 
<display:table name="totalVendorInvoiceList" class="table" requestURI="" id="totalVendorInvoiceList" style="width:200px" defaultsort="1" pagesize="100" >   
 		 <display:column property="estimateVendorName" headerClass="headerColorInv" title="Vendor Name" style="width:40px" maxLength="10"/>
 		 <display:column property="invoiceNumber"  title="Invoice #" style="width:40px"  maxLength="5"/> 
		 <display:column   title="Total" headerClass="containeralign"   style="width:60px">  
		      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
             groupingUsed="true" value="${totalVendorInvoiceList.actualExpenseSum}" /></div>
         </display:column>
 <display:column  sortable="" title="P/S"  style="width:40px" >
        <c:if test="${totalVendorInvoiceList.payingStatus==''}">
         </c:if>
         <c:if test="${totalVendorInvoiceList.payingStatus=='A'}">
         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        <c:if test="${totalVendorInvoiceList.payingStatus=='N'}">
        <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        <c:if test="${totalVendorInvoiceList.payingStatus=='P'}">
        <img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        <c:if test="${totalVendorInvoiceList.payingStatus=='S'}">
         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
         <c:if test="${totalVendorInvoiceList.payingStatus=='I'}">
         <img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
         </c:if>
        </display:column>         
</display:table>  
</s:form>
</c:if>
<c:if test="${totalVendorInvoiceList=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" >
<tr valign="top"> 	
	<td align="left"><b>No Vendor Invoice List found</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</c:if>
		  	