<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="InvoiceUploaded"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="861"
		 pageHeight="842"
		 columnWidth="801"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "InvoiceUploaded-R-PDF-VICT.jrxml",
	serviceorder.companyDivision AS serviceorder_companyDivision,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.bookingAgentCode AS serviceorder_bookingAgentCode,
	trim(accountline.billtoname) AS billing_billtoname,
	accountline.billToCode AS billing_billToCode,
	accountline.recInvoiceNumber AS accountline_recInvoiceNumber,
	DATE_FORMAT(accountline.receivedInvoiceDate,'%d-%b-%y') AS accountline_receivedInvoiceDate,
	if(accountline.actualRevenue is null || accountline.actualRevenue='',0,accountline.actualRevenue) AS accountline_actualRevenue,
	DATE_FORMAT(accountline.recPostDate,'%d-%b-%y') AS accountline_recPostDate,
	accountline.recGl AS accountline_recGl,
	if(accountline.distributionAmount is null || accountline.distributionAmount='',0,accountline.distributionAmount) AS accountline_distributionAmount,
	DATE_FORMAT(now(),'%d-%b-%y') As currentDate
FROM serviceorder serviceorder
	INNER JOIN accountline accountline ON serviceorder.id=accountline.serviceOrderId and accountline.corpId = $P{Corporate ID}
	INNER JOIN partnerprivate partner ON accountline.billToCode =partner.partnerCode and partner.corpId = $P{Corporate ID}
WHERE serviceorder.corpId = $P{Corporate ID} 
	AND accountline.actualRevenue <> 0
	AND accountline.recAccDate is null
	AND accountline.recPostDate >= '2008-12-31'
	AND accountline.status = true
	AND accountline.chargeCode NOT IN('PREPAY','0PREPAY')
	AND (partner.InvoiceUploadCheck = false or partner.InvoiceUploadCheck is null)
ORDER BY serviceorder.CompanyDivision,accountline.receivedInvoiceDate,accountline.recInvoiceNumber]]></queryString>

	<field name="InvoiceUploaded-R-PDF-VICT.jrxml" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_bookingAgentCode" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="accountline_recInvoiceNumber" class="java.lang.String"/>
	<field name="accountline_receivedInvoiceDate" class="java.lang.String"/>
	<field name="accountline_actualRevenue" class="java.math.BigDecimal"/>
	<field name="accountline_recPostDate" class="java.lang.String"/>
	<field name="accountline_recGl" class="java.lang.String"/>
	<field name="accountline_distributionAmount" class="java.math.BigDecimal"/>
	<field name="currentDate" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="27"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="2"
						width="801"
						height="24"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoices Not Uploaded]]></text>
				</staticText>
			</band>
		</title>
		<pageHeader>
			<band height="35"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="1"
						y="10"
						width="51"
						height="22"
						key="staticText-2"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Div]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="52"
						y="10"
						width="90"
						height="22"
						key="staticText-3"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Ship #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="207"
						y="10"
						width="62"
						height="22"
						key="staticText-4"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Bill To]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="269"
						y="10"
						width="132"
						height="22"
						key="staticText-5"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Billing Party]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="401"
						y="10"
						width="85"
						height="22"
						key="staticText-6"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Rec Invoice #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="486"
						y="10"
						width="67"
						height="22"
						key="staticText-7"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoice Dt]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="553"
						y="10"
						width="126"
						height="22"
						key="staticText-8"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Actual Rev. / Dist. Amt.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="746"
						y="10"
						width="55"
						height="22"
						key="staticText-9"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Recgl]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="142"
						y="10"
						width="65"
						height="22"
						key="staticText-10"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[BK Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="679"
						y="10"
						width="67"
						height="22"
						key="staticText-11"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Post Date]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="33"
						width="801"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="52"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="52"
						y="0"
						width="90"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="269"
						y="0"
						width="132"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billtoname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="207"
						y="0"
						width="62"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="401"
						y="0"
						width="85"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_recInvoiceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="486"
						y="0"
						width="67"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_receivedInvoiceDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="553"
						y="0"
						width="62"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_actualRevenue}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="746"
						y="0"
						width="55"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_recGl}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="142"
						y="0"
						width="65"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_bookingAgentCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="679"
						y="0"
						width="67"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_recPostDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="629"
						y="0"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_distributionAmount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="615"
						y="0"
						width="14"
						height="18"
						key="staticText-13"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<text><![CDATA[/]]></text>
				</staticText>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="627"
						y="4"
						width="118"
						height="15"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="745"
						y="4"
						width="56"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="4"
						width="141"
						height="15"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times-Roman" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentDate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="142"
						y="4"
						width="485"
						height="15"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[InvoiceUploaded.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="801"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
