package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.StandardDeductions;
import com.trilasoft.app.model.SubcontractorCharges;

public interface StandardDeductionsDao extends GenericDao<StandardDeductions, Long>  {
	
	public List  getSDListByPartnerCode(String partnerCode);
	
	public void deleteSubContChargesForStandardDeduction(String companyDivision, String corpID);
	
	public List deuctionPreviewList(Date fromDate, String companyDivision, String sessionCorpID, String process, String ownerPayTo);
	
	public List getStandardDeductionPreviewList(Date fromDate, String companyDivision, String corpID, String process,String ownerPayTo);
	
	public void updateSubContChargesForStandardDeduction(String personId,Long id,String userName);
	
	public void updateStandardDeduction(Long id, BigDecimal dedAmount, BigDecimal balancedAmount, Date lastDeductionDate, String status);
	
	public List getTripCount(Date fromDate, String partnerCode, String corpID);
	
	public void updateDomestic(String partnerCode);
	
	public List<SubcontractorCharges> getSubContChargesForFinalize(String personId);
	
	public void updateSubContractorCharges(Long id);
	
	public List validatePayTo(String ownerPayTo, String corpId);

	public void updatePartnerPrivate(String ownerPayTo, String corpId,BigDecimal personalEscrowAmount);
	
	public List partnerPrivateObject(String ownerPayTo, String corpId);
	
	public List<SubcontractorCharges> getSubContChargesForPersonalEscrow(String partnerCode);
	
	public List getUpdatedRecord(Long id);
	
	public List partnerPrivatePersonalEscrow(String ownerPayTo, String corpId);
	
	public List subContractorChargesFinalList(String ownerPayTo, String corpId, String companyDivision);
	
	public List chargesObject(String chargeCode, String corpId);
}
