package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.*;

import javax.persistence.Transient;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.PartnerDao;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.ListLinkData;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMaster;

import java.util.Map;

public class PartnerDaoHibernate extends GenericDaoHibernate<Partner, Long> implements PartnerDao {
	private List partners;
	private HibernateUtil hibernateUtil; 
	private List popupList;

	public PartnerDaoHibernate() {
		super(Partner.class);
	}

	public List<Partner> findByLastName(String lastName) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where lastName=?", lastName);
	}

	public List<Partner> findByBroker() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isVendor is true and status='Approved'");
	}

	public List findMaximum() {
		return getHibernateTemplate().find("select max(id) from Partner");
	}

	public List<Partner> findByCarriers(String sessionCorpID) {
		getHibernateTemplate().setMaxResults(100);
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
			return getHibernateTemplate().find("from PartnerPublic where isCarrier is true and status='Approved'  ");	
		}else{
			return getHibernateTemplate().find("from Partner where isCarrier is true and status='Approved'  ");
		}
		
	}
	public List<Partner> findByCarriersForHeavy(){
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isCarrier is true and status='Approved' and surface is true");
	}
	public List<Partner> findByOwner() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isOwnerOp is true and status='Approved'");
	}
	public List<Partner> findByCrewDrivers(){
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where (isOwnerOp is true OR isVendor is true OR isCarrier is true)  and status='Approved'");
	}
	public String checkDriverIdAjax(String driverCode,String corpId){
		String str="";
		List al=this.getSession().createSQLQuery("select concat(if(firstName=null,'',firstName),' ',if(lastName=null,'',lastName)) from partner where (isOwnerOp is true OR isVendor is true OR isCarrier is true) and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and status='Approved' and partnerCode='"+driverCode+"' limit 1").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
			str=al.get(0).toString().trim();
		}
		return str;
	}
	public List<Partner> findByAllOwner() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isOwnerOp is true and status='Approved' and soAllDrivers is true");
	}
	public List<Partner> findByOwnerOp(String perId) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("select partnerCode from Partner where isOwnerOp = true AND partnerCode=?", perId);

	}
	public String findByOwnerOpLastNameByCode(String perId) {
		//getHibernateTemplate().setMaxResults(100);
		String str="";
		List al=this.getSession().createSQLQuery("select if(lastName=null,'',if(firstName is not null and firstName!='',concat(firstName,' ',lastName),lastName)) from partner where isOwnerOp = true AND status='Approved' AND partnerCode='"+perId+"'").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
			str=al.get(0).toString().trim();
		}
		return str;
	}

	public List<Partner> findByOwnerOps(String perId) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("select lastName,status from Partner where isOwnerOp = true AND partnerCode=?", perId);

	}
	public List<Partner> checkValidNationalCode(String vlCode,String sessionCorpID){
		getHibernateTemplate().setMaxResults(1);		
		return getHibernateTemplate().find("select concat(lastName,'~',partnerCode) from PartnerPrivate where validNationalCode='"+vlCode+"' and corpID ='"+sessionCorpID+"'");		
	}
	public List<Partner> findByBillToCode(String shipNumber) {
		getHibernateTemplate().setMaxResults(100);
		List b = this.getSession().createSQLQuery("select CONCAT(if(billToCode is null,' ',billToCode),''',''',if(billTo2Code is null,' ',billTo2Code),''',''',if(privatePartyBillingCode is null,' ',privatePartyBillingCode)) from billing where shipNumber='" + shipNumber + "'").list();
		String s = "";
		if (b.isEmpty()) {
			s = "";
		} else {
			s = b.get(0).toString();
		}
		List a = getHibernateTemplate().find("from Partner where partnerCode in ('" + s + "')");
		return a;
	}

	public List<Partner> findByAccount() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAccount is true");
	}

	public List<Partner> findByAgent() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAgent is true and status='Approved'");
	}

	public List<Partner> findByBillingCountryCode(String billingCountryCode) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where billingCountryCode=?", billingCountryCode);
	}

	public List<Partner> findByOrigin(String origin) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAgent is true and status='Approved' AND billingCountryCode=?", origin);
	}
	
	public List findByOriginAgent(String origin) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAgent is true and status='Approved' AND billingCountryCode='"+origin+"' ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')");
	}

	public List<Partner> findByDestination(String destination,String userType) {
		getHibernateTemplate().setMaxResults(100);
		if("ACCOUNT".equals(userType)){
			return getHibernateTemplate().find("from Partner where isAccount is true and status='Approved' AND billingCountryCode like '" + destination.replaceAll("'", "''") + "%'");
		}else {
			return getHibernateTemplate().find("from Partner where isAgent is true and status='Approved' AND billingCountryCode like '" + destination.replaceAll("'", "''") + "%'");
		}
	}

	public List<Partner> findByFreight(String origin) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAgent is true OR isCarrier is true OR billingCountryCode=?", origin);
	}

	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original;
		}

		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());

		return partBefore + replacement + partAfter;
	}

	public List<Partner> findForPartner(String lastName, String partnerCode, String billingCountryCode, String findFor) {
		getHibernateTemplate().setMaxResults(100);
		if (findFor.equals("")) {
			partners = getHibernateTemplate().find(
					"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%" + lastName.replaceAll("'", "''")
					+ "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%'");
		} else {
			if (findFor.equals("redskypartner")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND isAccount is true ");
			}else if (findFor.equals("account")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND isAccount is true ");
			}else if (findFor.equals("carrier")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND isCarrier is true ");
			}else if (findFor.equals("owner")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND isCarrier is true ");
			}else if (findFor.equals("agent")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND isAgent is true ");
			}
		}
		return partners;
	}

	public List<Partner> searchForPartner(String lastName, String partnerCode, String billingCountryCode, String billingStateCode, String billingCountry, String findFor) {
		getHibernateTemplate().setMaxResults(100);
		if (findFor.equals("")) {
			partners = getHibernateTemplate().find(
					"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '" + lastName.replaceAll("'", "''")
					+ "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%'");
		} else {
			if (findFor.equals("account")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%' AND isAccount is true");

			}
			if (findFor.equals("carrier")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%' AND isCarrier is true");

			}

			if (findFor.equals("owner")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND isCarrier is true ");
			}
			if (findFor.equals("agent")) {
				partners = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND billingCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%' AND isAgent is true");
			}
		}
		return partners;
	}

	public List findPartnerCode(String partnerCode) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where status <> 'Inactive' and partnerCode=?", partnerCode);
	}
	public List getCarrierListForHeavy(String corpID,String lastName, String partnerCode){
		popupList = getHibernateTemplate().find(
				"from Partner where isCarrier=true and status='Approved' and surface is true and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
				+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' ");
		return popupList;
	}
	public List getPayToForHeavy(String corpID,String partnerCode){
		
		return this.getSession().createSQLQuery("select pp.payTo from partnerpublic p , partnerprivate pp where p.id=pp.partnerPublicId and pp.partnerCode ='"+partnerCode+"' and p.isCarrier=true and p.status='Approved' and p.surface is true and pp.corpID  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"')").list();
	}
	public List converObjToDTO(List list){
		Iterator it = list.iterator();
		List newList = new ArrayList();
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			Partner p = new Partner();
			if(row[0] != null){
				p.setPartnerCode(row[0].toString());
			}
			if(row[1] != null){
				p.setLastName(row[1].toString());
			}
			if(row[2] != null){
				p.setBillingAddress1(row[2].toString());
			}
			if(row[3] != null){
					p.setTerminalCountryCode(row[3].toString());
					p.setBillingCountryCode(row[3].toString());	
			}
			if(row[4] != null){
					p.setTerminalState(row[4].toString());
					p.setBillingState(row[4].toString());	
				
			}
			if(row[5] != null){
					p.setTerminalCity(row[5].toString());
					p.setBillingCity(row[5].toString());
			}
			if(row[6] != null){
				p.setStatus(row[6].toString());
			}
			if(row[7] != null){
				p.setExtReference(row[7].toString());
			}
			if(row[8] != null){
				p.setAliasName(row[8].toString());
			}
			if(row[10] != null){
				p.setId(Long.parseLong(row[10].toString()));
			}
			if(row[11] != null){
				p.setAgentClassification(row[11].toString());
			}
			newList.add(p);
		}
		return newList;
	}
	public List	getCrewPartnerPopupList(String corpID, String lastName,String aliasName, String partnerCode, String billingCountry, String billingStateCode,String extReference){
		getHibernateTemplate().setMaxResults(100);
		popupList = getHibernateTemplate().find(
				"from Partner where (isOwnerOp is true OR isVendor is true OR isCarrier is true) and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
				+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountry like '%"
				+ billingCountry.replaceAll("'", "''") + "%'  AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		return popupList;								
	}
	public List getPartnerPopupList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag) {
		getHibernateTemplate().setMaxResults(100);
		if (!partnerType.equals("")) {
			if (partnerType.equals("RSKY")) {
				String query = "select n.agentPartnerCode,n.agentName,p.billingAddress1,p.billingCountryCode,p.billingState,p.billingCity,p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
								" from networkpartners n,partner p where"+
								" n.agentpartnerCode=p.partnerCode "+
								" and n.agentPartnerCode like '%"+partnerCode+"%' and n.agentName like '%"+lastName+"%' AND  aliasName like '%" + aliasName.replaceAll("'", "''") + "%' and p.billingCountryCode like '%"+billingCountryCode+"%' and p.billingCountry like '%"+billingCountry+"%' and"+
								" p.billingState like '%"+billingStateCode+"%' and p.billingCity like '%%'"+
								" and p.corpID='"+corpID+"'"+
								" group by p.partnerCode";
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
			}else if (partnerType.equals("PP")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}else if (partnerType.equals("AC")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
				
			}else if (partnerType.equals("VN")) {
				if(customerVendor.equals("true")){
					popupList = getHibernateTemplate().find(
							"from Partner where isVendor=true and status='Approved' and typeOfVendor='Agent Broker' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
				}else{
					popupList = getHibernateTemplate().find(
						"from Partner where isVendor=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%'AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
				}
			}else if (partnerType.equals("CR")) {
				if(corpID.equalsIgnoreCase("TSFT")){
					popupList = getHibernateTemplate().find(
							"from PartnerPublic where isCarrier=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
				}else{
					popupList = getHibernateTemplate().find(
							"from Partner where isCarrier=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
				}
				
			}else if (partnerType.equals("OO")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isOwnerOp=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
			}else if (partnerType.equals("AG")) {
				if(!vanlineCode.equalsIgnoreCase("")){
					String query = " select p.partnerCode,p.lastName,p.billingAddress1," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
							       " p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
							       " from partnervanlineref n,partner p where"+					
							       " n.partnerCode=p.partnerCode and p.isAgent=true and p.status='Approved' and n.vanLineCode like '%"+vanlineCode+"%'"+
							       " and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
							       " AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
							       " AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
							       " AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
							       " AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
							       " AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
							       " AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
							       " AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')";					
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
				}else{
				String extRef="";
				if(extReference!=null && !extReference.equalsIgnoreCase("")){
					extRef = " extReference like '%" + extReference.replaceAll("'", "''") + "%'";
				}else{
					extRef = " (extReference like '%%' or extReference is null)";
				}
				popupList = getHibernateTemplate().find(
						"from Partner where isAgent=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND "+extRef+" ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')");
				}
			}else if (partnerType.equals("DE")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'");
			}else if (partnerType.equals("DF")) {
				popupList = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
		} else {
			popupList = getHibernateTemplate().find(
					"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		}
		
				return popupList;
	}

	public List getPartnerPopupAllList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference) {
		getHibernateTemplate().setMaxResults(100);
						popupList = getHibernateTemplate().find(
						"from Partner where isOwnerOp=true and status='Approved' and soAllDrivers is true and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
				return popupList;
	}
	
	
	public List findMaxByCode() {
		Long l = Long.parseLong((getHibernateTemplate().find("select max(id) from Partner")).get(0).toString());
		List pList = getHibernateTemplate().find("select partnerCode from Partner where id =?", l);
		return pList;
	}

	public List partnerExtract(String billToCode, String corpID) {
		//getHibernateTemplate().setMaxResults(100);
		getHibernateTemplate().setMaxResults(50000);
		return getHibernateTemplate().find("from Partner where partnerCode in(" + billToCode + ") and  (isPrivateParty=true or isAgent=true or isAccount=true or isVendor=true or isCarrier=true) ");
	}

	public List<Partner> findMultiAuthorization(String accPartnerCode) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("select multiAuthorization from Partner where partnerCode=?", accPartnerCode);
	}

	public List findPartnerForActgCode(String partnerCode, String corpID, String companyDivision) {
		getHibernateTemplate().setMaxResults(100);
		return this.getSession().createSQLQuery("select p1.lastName,if(pa.accountCrossReference is null,'' ,pa.accountCrossReference), p1.status,if(p1.isOwnerOp is true, if(p2.commission is null or p2.commission ='','Commission',p2.commission),'NoCommission') from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.companyDivision='" + companyDivision + "' and pa.corpID IN ('"+corpID+"') where binary p1.partnerCode= '" + partnerCode + "' and p1.id = p2.partnerPublicId and p2.corpID  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"')").list();

	}
	public List findPartnerForActgCodeLine(String partnerCode, String corpID, String companyDivision,String actgCodeFlag, Boolean cmmDmmFlag) {
		String actgFl="N";
		 List Bucket2List= new ArrayList();
		 if((actgCodeFlag!=null)&&(actgCodeFlag.equalsIgnoreCase("YES"))){
			 String query="select p.partnerCode from partnerpublic p,partnerprivate pp " +
			 		" where p.id=pp.partnerPublicId  " +
			 		" and p.partnerCode is not null and p.partnerCode!='' " +
			 		" and p.isOwnerOp is true  and p.corpId in ('TSFT','"+corpID+"') " +
			 		" and pp.corpId='"+corpID+"' and p.status ='Approved' and p.partnerCode='"+partnerCode+"'";
			 Bucket2List= this.getSession().createSQLQuery(query).list();	 
		     if(Bucket2List !=null && (!(Bucket2List.isEmpty())) && Bucket2List.get(0)!=null){
		    	 actgFl="Y"; 
		     }
		 }
		List al = new ArrayList();
		if(actgFl.equalsIgnoreCase("Y")){
			al=this.getSession().createSQLQuery("select p1.lastName,if(pa.accountCrossReference is null,'' ,pa.accountCrossReference), p1.status,if(p1.isOwnerOp is true, if(p2.commission is null or p2.commission ='','Commission',p2.commission),'NoCommission'),if(p1.billingCurrency is null,' ',p1.billingCurrency),if(p1.isAgent is true,'T','F'),if(p1.isAccount is true,'T','F'),if(pa.companyDivision is null,' ',pa.companyDivision) from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.refType ='P' and pa.corpID IN ('"+corpID+"') where binary p1.partnerCode= '" + partnerCode + "' and p1.id = p2.partnerPublicId and p2.corpID  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') limit 1").list();			
		}else{
			al=this.getSession().createSQLQuery("select p1.lastName,if(pa.accountCrossReference is null,'' ,pa.accountCrossReference), p1.status,if(p1.isOwnerOp is true, if(p2.commission is null or p2.commission ='','Commission',p2.commission),'NoCommission'),if(p1.billingCurrency is null,' ',p1.billingCurrency),if(p1.isAgent is true,'T','F'),if(p1.isAccount is true,'T','F'),'' from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.companyDivision='" + companyDivision + "' and pa.refType ='P' and pa.corpID IN ('"+corpID+"') where binary p1.partnerCode= '" + partnerCode + "' and p1.id = p2.partnerPublicId and p2.corpID  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') limit 1").list();			
		}
		return al;
	}
	public String getAccountCrossReference(String vendorCode,String companyDiv ,String sessionCorpID){ 
		String accountCrossReference="";
		List accountCrossReferencelist = new ArrayList();
		if(vendorCode!=null && (!(vendorCode.equals("")))){
			accountCrossReferencelist = this.getSession().createSQLQuery("select if(accountCrossReference is null,'' ,accountCrossReference) from partneraccountref where corpID='"+sessionCorpID+"' and companyDivision='"+companyDiv+"' and partnerCode='"+vendorCode+"' and refType='P' ").list();
			if(accountCrossReferencelist!= null && (!(accountCrossReferencelist.isEmpty())) && accountCrossReferencelist.get(0)!= null ){
				accountCrossReference=accountCrossReferencelist.get(0).toString();
			} 
		}
		
		 return accountCrossReference;
	}
	public String getAccountCrossReferenceUnique(String vendorCode,String sessionCorpID){
		String accountCrossReference="";
		List accountCrossReferencelist = new ArrayList();
		if(vendorCode!=null && (!(vendorCode.equals("")))){
			accountCrossReferencelist = this.getSession().createSQLQuery("select if(accountCrossReference is null,'' ,accountCrossReference) from partneraccountref where corpID='"+sessionCorpID+"' and partnerCode='"+vendorCode+"' and refType='P' ").list();
			if(accountCrossReferencelist!= null && (!(accountCrossReferencelist.isEmpty())) && accountCrossReferencelist.get(0)!= null ){
				accountCrossReference=accountCrossReferencelist.get(0).toString();
			} 
		}
		 return accountCrossReference;
	}
	public List<Partner> findByOriginAccRef(String origin) {
		getHibernateTemplate().setMaxResults(100);
		//return getHibernateTemplate().find("from Partner where accountCrossReference is not null and accountCrossReference <>'' and isAgent is true AND status='Approved' AND billingCountryCode=?",origin);
		return getHibernateTemplate().find("from Partner where isAgent is true AND status='Approved' AND terminalCountryCode=?",origin);
	}

	public List<Partner> findByDestAccRef(String destination) {
		getHibernateTemplate().setMaxResults(100);
		//return getHibernateTemplate().find("from Partner where accountCrossReference is not null and accountCrossReference <>'' and isAgent is true AND status='Approved' AND billingCountryCode=?",destination);
		return getHibernateTemplate().find("from Partner where isAgent is true AND status='Approved' AND terminalCountryCode=?",destination);
	}
/*
	public List<Partner> findByBrokerAccRef() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where accountCrossReference is not null and accountCrossReference <>'' and isBroker is true and status='Approved'");
	}

	public List<Partner> findByCarriersAccRef() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where accountCrossReference is not null and accountCrossReference <>'' and isCarrier is true and status='Approved'");
	}
*/

	public List getPartnerGeoMapDataList(String corpID, Boolean isAgt, Boolean isVndr, Boolean isCarr,String minLatitude,String maxLatitude,String minLogitude,String maxLogitude,Boolean isUts,Boolean isFidi,Boolean isOmniNo,Boolean isAmsaNo,Boolean isWercNo,Boolean isIamNo,Boolean isAct,String country,String lastName){
		List popupList = new ArrayList();
		String query1 = "";
		String query2 = "";
		String query = "select distinct p.id,p.partnerCode, " +
					"p.mailingCountryCode as country, " +
					"p.mailingCity as city, " +
					"p.mailingState as state, " +
					"p.mailingZip as zip, " +
					"p.mailingAddress1 as address,p.latitude,p.longitude,p.lastName as partnerName ,pr.status as partnerRateGridStatus, if((p.isAgent=true),'AG','') as agentStatus, if((p.isVendor=true),'VN','') as vendorStatus, if((p.isCarrier=true),'CR','') as carrierStatus ,if((p.isAccount=true),'AC','no') as accountStatus  " +
					"FROM partnerprivate pp,partnerpublic p left outer join partnerrategrid pr on (p.partnercode=pr.partnercode) and p.corpID=pr.corpID and pr.status='Submitted' " +
					"WHERE p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') AND p.status = 'Approved' AND pp.status = 'Approved' and pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and pp.partnerpublicid=p.id and p.latitude between '"+minLatitude+"' and '"+maxLatitude+"'  and p.longitude between '"+minLogitude+"' and '"+maxLogitude+"' and p.latitude not like '0.0000%' and p.longitude not like '0.0000%' and p.lastName like '%"+lastName+"%'";
		if(country !=null && (!(country.trim().equals("")))){
			query = "select distinct p.id,p.partnerCode, " +
			"p.mailingCountryCode as country, " +
			"p.mailingCity as city, " +
			"p.mailingState as state, " +
			"p.mailingZip as zip, " +
			"p.mailingAddress1 as address,p.latitude,p.longitude,p.lastName as partnerName ,pr.status as partnerRateGridStatus, if((p.isAgent=true),'AG','') as agentStatus, if((p.isVendor=true),'VN','') as vendorStatus, if((p.isCarrier=true),'CR','') as carrierStatus ,if((p.isAccount=true),'AC','no') as accountStatus  " +
			"FROM partnerprivate pp,partnerpublic p left outer join partnerrategrid pr on (p.partnercode=pr.partnercode) and p.corpID=pr.corpID and pr.status='Submitted' " +
			"WHERE p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') AND p.status = 'Approved' AND pp.status = 'Approved' and pp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and pp.partnerpublicid=p.id and p.latitude between '"+minLatitude+"' and '"+maxLatitude+"'  and p.longitude between '"+minLogitude+"' and '"+maxLogitude+"' and p.latitude not like '0.0000%' and p.longitude not like '0.0000%' and p.mailingCountryCode like '%"+country+"%' and p.lastName like '%"+lastName+"%' ";	
		}
		
		if(isAgt){
			query1 = query1+",p.isAgent=true";
		}
		if(isVndr){
			query1 = query1+",p.isVendor=true";
		}
		if(isCarr){
			query1 = query1+",p.isCarrier=true";
		}
		if(isAct){
			query1 = query1+",p.isAccount=true";
		}
		if(isUts){
			query2 = query2+",p.utsNumber='Y'";
		}
		if(isFidi){
			query2 = query2+",p.fidiNumber='Y'";
		}
		if(isOmniNo){
			query2 = query2+",p.OMNINumber!='' AND p.OMNINumber is not null";
		}
		if(isAmsaNo){
			query2 = query2+",p.AMSANumber='Y'";
		}
		if(isWercNo){
			query2 = query2+",p.WERCNumber='Y'";
		}
		if(isIamNo){
			query2 = query2+",p.IAMNumber='Y'";
		}

		if( isAgt || isVndr || isCarr ||isAct){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND p.isAgent = false AND p.isVendor = false AND p.isCarrier= false AND p.isAccount= false";
		}
		
		if(isUts || isFidi|| isOmniNo|| isAmsaNo|| isWercNo|| isIamNo){
			query2 = query2.substring(1, query2.length());
			String tempString[] = query2.split(",");
			int totalLength = tempString.length;
			if(totalLength == 1){
				query +=  " AND "+query2;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" AND ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-4);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
			
			
		}

		popupList = this.getSession().createSQLQuery(query).list();		
		return popupList;
		
		
	}
	
	
	public List getPartnerPopupListAdmin(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgInactive, String externalRef,String vanlineCode,String typeOfVendor, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA) {
		List <Object> partnerList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(100);
		String tempExtRef = "";
		if(externalRef.trim().length() <= 0){
			tempExtRef =  "OR pp.extReference is null ";
		}
		String query1 = "";
		String query="";
		if(vanlineCode!=null && !vanlineCode.equals("")){
			query="select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, pp.status, "+
            "if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country,"+
            " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city,"+
            " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state,"+
            "ap.stage ,pv.vanLineCode,pp.extReference,p.agentGroup,pp.agentClassification "+
            "FROM partnerpublic p,partnerprivate pp LEFT OUTER JOIN accountprofile ap on  pp.partnerCode = ap.partnerCode  and ap.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+
            "inner join partnervanlineref pv on pp.partnerCode = pv.partnerCode and pv.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+
            "WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND  pv.vanLineCode like '%" + vanlineCode.replaceAll("'", "''") + "%'  "+
            "AND (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
            "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', p.billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) "+
            "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalCountry like '%" + country.replaceAll("'", "''") + "%', p.billingCountry like '%" + country.replaceAll("'", "''") + "%')) "+
            "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalState like '%" + stateCode.replaceAll("'", "''") + "%', p.billingState like '%" + stateCode.replaceAll("'", "''") + "%')) "+
            "AND (pp.extReference like '%" + externalRef.replaceAll("'", "''") + "%' "+tempExtRef+" ) "+
           /* "AND p.status in ('Approved','Prospect') AND pp.status in ('Approved','Prospect') AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+*/
            "and p.partnerCode = pp.partnerCode and pp.partnerPublicId=p.id AND pp.corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')";
			if(status!=null && !status.equals("")){
          	  query=query+"AND p.status like '"+ status +"%' AND pp.status like '"+ status +"%' AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" ;
            }else{
          	  query=query+"AND p.status in ('Approved','Prospect') AND pp.status in ('Approved','Prospect') AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" ;
            }
		}else{
		query="select distinct p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, pp.status, "+
		              "if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country,"+
		              " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city,"+
		              " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state,"+
		              "ap.stage,'',pp.extReference,p.agentGroup,pp.agentClassification "+
		              "FROM partnerpublic p,partnerprivate pp LEFT OUTER JOIN accountprofile ap on  pp.partnerCode = ap.partnerCode  and ap.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')"+
		              "WHERE p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' "+
		              "AND (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
		              "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', p.billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) "+
		              "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalCountry like '%" + country.replaceAll("'", "''") + "%', p.billingCountry like '%" + country.replaceAll("'", "''") + "%')) "+
		              "AND (if(p.isAgent=true or p.isVendor=true or p.isCarrier=true, p.terminalState like '%" + stateCode.replaceAll("'", "''") + "%', p.billingState like '%" + stateCode.replaceAll("'", "''") + "%')) "+
		              "AND (pp.extReference like '%" + externalRef.replaceAll("'", "''") + "%' "+tempExtRef+" ) "+
		              "and p.partnerCode = pp.partnerCode and pp.partnerPublicId=p.id AND pp.corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')";
		              if(status!=null && !status.equals("")){
		            	  query=query+"AND p.status like '"+ status +"%' AND pp.status like '"+ status +"%' AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" ;
		              }else if(isIgInactive==false && status.equals("")){
		            	  query=query+"AND p.status in ('Approved','Prospect','Inactive') AND pp.status in ('Approved','Prospect','Inactive') AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" ;
		              }else{
		            	  query=query+"AND p.status in ('Approved','Prospect') AND pp.status in ('Approved','Prospect') AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" ;
		              }
		}
		//System.out.println("\n\n\n\n\n\n\n\n\n\n query"+query);
		String additionalSearch=""; 
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.fidiNumber ='"+fidiNumber+"' or p.fidiNumber is null or p.fidiNumber='')  ";
		}
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.fidiNumber ='"+fidiNumber+"'   ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.OMNINumber is null or p.OMNINumber='') ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.OMNINumber !='' and p.OMNINumber is not null  ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.AMSANumber ='"+AMSANumber+"'  or p.AMSANumber is null or p.AMSANumber='') ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.AMSANumber ='"+AMSANumber+"' ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.WERCNumber ='"+WERCNumber+"'  or p.WERCNumber is null or p.WERCNumber='') ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.WERCNumber ='"+WERCNumber+"' ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.IAMNumber ='"+IAMNumber+"'  or p.IAMNumber is null or p.IAMNumber='') ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.IAMNumber ='"+IAMNumber+"'  ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.utsNumber ='"+utsNumber+"'  or p.utsNumber is null or p.utsNumber='') ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.utsNumber ='"+utsNumber+"' ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.eurovanNetwork ='"+eurovanNetwork+"'  or p.eurovanNetwork is null or p.eurovanNetwork='') ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.eurovanNetwork ='"+eurovanNetwork+"'  ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.PAIMA ='"+PAIMA+"'  or p.PAIMA is null or p.PAIMA='') ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.PAIMA ='"+PAIMA+"' ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.LACMA ='"+LACMA+"'  or p.LACMA is null or p.LACMA='') ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.LACMA ='"+LACMA+"' ";
		}
		System.out.println("\n\n\n\n additionalSearch "+additionalSearch);
		query=query+additionalSearch;
		if(isIgInactive){
			query += "AND p.status <> 'Inactive' AND pp.status <> 'Inactive' ";
		}
		if( isVndr && typeOfVendor!=null && !typeOfVendor.equals("")){
			query +="AND p.typeOfVendor like '%"+typeOfVendor.replaceAll("'", "''") +"%'";	
		}
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
		}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND p.isAgent = false AND p.isVendor = false AND p.isCarrier= false AND p.isPrivateParty = false AND p.isAccount = false AND p.isOwnerOp = false";
		}
		query += " limit 500 ";
		System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list();
		
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			
			dto.setId(row[0]);
			dto.setPartnerCode(row[1]);
			dto.setLastName(row[2]);
			
			if(row[3] == null){
				dto.setFirstName("");
			}else{
				dto.setFirstName(row[3]);
			}
			if(row[4] == null){
				dto.setAliasName("");
			}else{
				dto.setAliasName(row[4]);
			}
			if(row[5]==null){
				dto.setIsAgent("");
			}else{
				dto.setIsAgent(row[5]);
			}
			
			if(row[6]==null){
				dto.setIsAccount("");
			}else{
				dto.setIsAccount(row[6]);
			}
			
			if(row[7]==null){
				dto.setIsPrivateParty("");
			}else{
				dto.setIsPrivateParty(row[7]);
			}
			
			if(row[8]==null){
				dto.setIsCarrier("");
			}else{
				dto.setIsCarrier(row[8]);
			}
			
			if(row[9]==null){
				dto.setIsVendor("");
			}else{
				dto.setIsVendor(row[9]);
			}
			
			if(row[10]==null){
				dto.setIsOwnerOp("");
			}else{
				dto.setIsOwnerOp(row[10]);
			}
			
			if(row[11]==null){
				dto.setStatus("");
			}else{
				dto.setStatus(row[11]);
			}
			
			if(row[12] == null){
				dto.setCountryName("");
			}else{
				dto.setCountryName(row[12]);
			}
			
			if(row[13] == null){
				dto.setCityName("");
			}else{
				dto.setCityName(row[13]);
			}
			
			if(row[14] == null){
				dto.setStateName("");
			}else{
				dto.setStateName(row[14]);
			}
			
			if(row[15] == null){
				dto.setStage("");
			}else{
				dto.setStage(row[15]);
			}
			if(row[16] == null){
				dto.setVanLineCode("");
			}else{
				dto.setVanLineCode(row[16]);
			}
			if(row[17] == null){
				dto.setExternalRef("");
			}else{
				dto.setExternalRef(row[17]);
			}
			if(row[18] == null){
				dto.setAgentGroup("");
			}else{
				dto.setAgentGroup(row[18]);
			}
			if(row[19]==null){
				dto.setAgentClassification("");
			}else{
				dto.setAgentClassification(row[19]);
			}
			partnerList.add(dto);
		}
		
		return partnerList;
	}

	public List findSubContExtractSeq(String corpID) {
		return getHibernateTemplate().find("select subContcExtractSeq from SystemDefault where subContcExtractSeq<>'' and subContcExtractSeq is not null and corpID=?", corpID);
	}

	public int updateSubContrcExtractSeq(String subConExtractSeq, String corpID) {
		int i = getHibernateTemplate().bulkUpdate("update SystemDefault set subContcExtractSeq='" + subConExtractSeq + "'  where corpID=?", corpID);
		return i;
	}
	public List getQuotationPartnerPopupListCompDiv(String partnerType, String corpID, String compDiv, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag) {
		//getHibernateTemplate().setMaxResults(100);

		if (!partnerType.equals("")) {
			if (partnerType.equals("PP")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv + "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//" OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
			}
			if (partnerType.equals("AC") || partnerType.equals("")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isAccount=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
				}

			if (partnerType.equals("VN")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isVendor=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("CR")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isCarrier=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("OO")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isOwnerOp=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("AG")) {
				if(vanlineCode.equalsIgnoreCase("")){
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isAgent=true and status in ('New','Approved') " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
				}else{
					String str="select p.partnerCode,p.lastName,p.billingAddress1," +
			        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
			        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
			        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
					" p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification from partner p "+
					" left outer join partneraccountref pr on p.partnerCode=pr.partnerCode and (pr.companyDivision='" + compDiv + "') and pr.corpid='"+corpID+"' "+
					" left outer join partnervanlineref pv on p.partnerCode=pv.partnerCode and pv.corpID in ('TSFT','"+corpID+"') "+
					" where p.corpID in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and isAgent is true and status in ('New','Approved') "+
					" and (p.companyDivision='" + compDiv	+ "' or p.companyDivision is null or p.companyDivision='') "+
					" and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or firstName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '" + lastName.replaceAll("'", "''") + "%') "+
		            " and (p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%') and (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
		            " and (terminalCountryCode like '%" + billingCountryCode.replaceAll("'", "''") + "%') and (terminalCountry like '%" + billingCountry.replaceAll("'", "''") + "%') "+
		            " and (terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%') "+
		            " and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) and (pv.vanlinecode like '%" + vanlineCode + "%')";
					
					List list = getSession().createSQLQuery(str).list();
					popupList = converObjToDTO(list);
				}
			}
			if (partnerType.equals("DE")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'");
			}
			if (partnerType.equals("DF")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
		} else {
			popupList = getHibernateTemplate().find(
					"select p from Partner p " +
					"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
					"where isAccount=true and status='Approved' " +
					"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
					//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
					"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		}
		return popupList;
	} 
	public List getQuotationPartnerPopupList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag) {
		getHibernateTemplate().setMaxResults(100);
		if (!partnerType.equals("")) {
			if (partnerType.equals("RSKY")) {
				String query = "select n.agentPartnerCode,n.agentName,p.billingAddress1,p.billingCountryCode,p.billingState,p.billingCity,p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
								" from networkpartners n,partner p where"+
								" n.agentpartnerCode=p.partnerCode and p.status='Approved'"+
								" and n.agentPartnerCode like '%"+partnerCode+"%' and n.agentName like '%"+lastName+"%' and p.billingCountryCode like '%"+billingCountryCode+"%' and p.billingCountry like '%"+billingCountry+"%' and"+
								" p.billingState like '%"+billingStateCode+"%' and p.billingCity like '%%'"+
								" group by p.partnerCode";
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
			}else if (partnerType.equals("PP")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}else if (partnerType.equals("AC")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
			}else if (partnerType.equals("VN")) {
				if(customerVendor.equals("true")){
					popupList = getHibernateTemplate().find(
							"from Partner where isVendor=true and status='Approved' and typeOfVendor='Agent Broker' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
				}else{
				popupList = getHibernateTemplate().find(
						"from Partner where isVendor=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%'AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
			}
			}else if (partnerType.equals("CR")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isCarrier=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}else if (partnerType.equals("OO")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isOwnerOp=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
			}else if (partnerType.equals("AG")) {
				if(vanlineCode.equalsIgnoreCase("")){
				popupList = getHibernateTemplate().find(						
						"from Partner where isAgent=true and status in('New','Approved') and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + extReference.replaceAll("'", "''") + "%' ");
				}else{
					String query = "select p.partnerCode,p.lastName,p.billingAddress1," +
					        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
					        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
					        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
							" p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+					
							" from partnervanlineref n,partner p where"+					
							" n.partnerCode=p.partnerCode and p.isAgent=true and status in('New','Approved') and n.vanLineCode like '%"+vanlineCode+"%'"+
							" and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
							" AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
							" AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
							" AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
							" AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
							" AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
							" AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
							" AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' ";					
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
				}			
			}else if (partnerType.equals("DE")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'");
			}else if (partnerType.equals("DF")) {
				popupList = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
		} else {
			popupList = getHibernateTemplate().find(
					"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		}
		
				return popupList;
	}
	public List getPartnerPopupListCompDiv(String partnerType, String corpID, String compDiv, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag) {
		//getHibernateTemplate().setMaxResults(100);

		if (!partnerType.equals("")) {
			if (partnerType.equals("PP")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv + "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//" OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
			}
			if (partnerType.equals("AC") || partnerType.equals("")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isAccount=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
				}

			if (partnerType.equals("VN")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isVendor=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("CR")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isCarrier=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("OO")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isOwnerOp=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
			if (partnerType.equals("AG")) {
				if(vanlineCode.equalsIgnoreCase("")){
					popupList = getHibernateTemplate().find(
							"select p from Partner p " +
							"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
							"where isAgent=true and status='Approved' " +
							"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
							//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
							"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')");
				}else{
					 String str="select p.partnerCode,p.lastName,p.billingAddress1," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
								" p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification from partner p "+
								" left outer join partneraccountref pr on p.partnerCode=pr.partnerCode and (pr.companyDivision='" + compDiv + "') and pr.corpid='"+corpID+"' "+
								" left outer join partnervanlineref pv on p.partnerCode=pv.partnerCode and pv.corpID in ('TSFT','"+corpID+"') "+
								" where p.corpID in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and isAgent is true and status='Approved' "+
								" and (p.companyDivision='" + compDiv	+ "' or p.companyDivision is null or p.companyDivision='') "+
								" and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or firstName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '" + lastName.replaceAll("'", "''") + "%') "+
					            " and (p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%') and (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
					            " and (terminalCountryCode like '%" + billingCountryCode.replaceAll("'", "''") + "%') and (terminalCountry like '%" + billingCountry.replaceAll("'", "''") + "%') "+
					            " and (terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%') "+
					            " and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) and (pv.vanlinecode like '%" + vanlineCode + "%') ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')";
					List list = getSession().createSQLQuery(str).list();
					popupList = converObjToDTO(list);
				}
			}
			if (partnerType.equals("DE")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'");
			}
			if (partnerType.equals("DF")) {
				popupList = getHibernateTemplate().find(
						"select p from Partner p " +
						"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
						"where isPrivateParty=true and status='Approved' " +
						"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
						//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
						"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
		} else {
			popupList = getHibernateTemplate().find(
					"select p from Partner p " +
					"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
					"where isAccount=true and status='Approved' " +
					"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
					//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
					"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		}
		return popupList;
	}

	public List getCountryCode(String mailingCountry) {
		return getHibernateTemplate().find("select code from RefMaster where description='" + mailingCountry + "' and parameter='COUNTRY' ");
	}

	public List financePartnerExtract(String beginDate, String endDate, String companyDivision,String corpID) {
		getHibernateTemplate().setMaxResults(50000);
		if (companyDivision.equals("") || companyDivision == null) {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')) ) and  (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true)");
		} else {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true) and companyDivision = '" + companyDivision + "'");
		}
	}
	public List financePartnerDataExtract(String corpID,String beginDate,String endDate) {
		getHibernateTemplate().setMaxResults(50000);		
		return getHibernateTemplate().find("from Partner where  ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true)  and (isPartnerExtract is null or isPartnerExtract=false)");
		
	}
	public void disableUser(Long id, String sessionCorpID, String status) {
		if (status.equalsIgnoreCase("Disable")) {
			getHibernateTemplate().bulkUpdate("update User set enabled=false where id='" + id + "' and corpID='" + sessionCorpID + "'");
		}
		if (status.equalsIgnoreCase("Enable")) {
			getHibernateTemplate().bulkUpdate("update User set enabled=true where id='" + id + "' and corpID='" + sessionCorpID + "'");
		}
	}
	public void updatePrtExtractFlag(String sessionCorpID, String pcode) {	
		
			getHibernateTemplate().bulkUpdate("update Partner set isPartnerExtract=true where partnerCode='" + pcode + "' and corpID='" + sessionCorpID + "'");
			getHibernateTemplate().bulkUpdate("update PartnerPublic set isPartnerExtract=true where partnerCode='" + pcode + "' and corpID='" + sessionCorpID + "'");
		
	}
	public List getPartnerChildAgentsList(String partnerCode, String sessionCorpID) {
		getHibernateTemplate().setMaxResults(100000);
		return getHibernateTemplate().find("from PartnerPublic where agentParent = '" + partnerCode + "' and status = 'Approved'");

	} 
	public List getPartnerIncludeChildAgentsList(String partnerCode, String sessionCorpID,String pageListType){
		if(pageListType.equalsIgnoreCase("AIS"))
		{
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+partnerCode+"' and p.agentParent<>p.partnerCode and (pp.excludeFromParentUpdate is false or pp.excludeFromParentUpdate is null) and p.status = 'Approved'").list();
		}else if(pageListType.equalsIgnoreCase("CPF")){
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+partnerCode+"' and p.agentParent<>p.partnerCode and (pp.excludeFromParentPolicyUpdate is false or pp.excludeFromParentPolicyUpdate is null) and p.status = 'Approved'").list();			
		}else if(pageListType.equalsIgnoreCase("FAQ")){
			return this.getSession().createSQLQuery("select concat(p.partnerCode,'~',p.id) from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+partnerCode+"' and p.agentParent<>p.partnerCode and (pp.excludeFromParentFaqUpdate is false or pp.excludeFromParentFaqUpdate is null) and p.status = 'Approved'").list();			
		}else if(pageListType.equalsIgnoreCase("PUB")){
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+partnerCode+"' and p.agentParent<>p.partnerCode and (pp.excludePublicFromParentUpdate is false or pp.excludePublicFromParentUpdate is null) and p.status = 'Approved'").list();			
		}else if(pageListType.equalsIgnoreCase("PRS")){
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+partnerCode+"' and p.agentParent<>p.partnerCode and (pp.excludePublicReloSvcsParentUpdate is false or pp.excludePublicReloSvcsParentUpdate is null) and p.status = 'Approved'").list();			
		}else{
			String[] temp=partnerCode.split("~");
			if(!temp[1].equalsIgnoreCase("Relocation")){
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+temp[0]+"' and p.agentParent<>p.partnerCode and (pp.excludeFromParentQualityUpdate is false or pp.excludeFromParentQualityUpdate is null) and p.status = 'Approved'").list();
			}else{
			return this.getSession().createSQLQuery("select p.partnerCode from partnerpublic p,partnerprivate pp where p.id=pp.partnerPublicId and pp.corpId='"+sessionCorpID+"' and p.agentParent = '"+temp[0]+"' and p.agentParent<>p.partnerCode and (pp.excludeFromParentQualityUpdateRelo is false or pp.excludeFromParentQualityUpdateRelo is null) and p.status = 'Approved'").list();				
			}
		}

}	
	public  class DTOPartnerIncludeChild{
		private Object partnerCode;

		public Object getPartnerCode() {
			return partnerCode;
		}

		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
	}
	  
	public  class DTOSynInvoicePartner{
		private Object partnerCode;
		private Object lastName;
		private Object partnerPrefix;
		private Object middleInitial;
		private Object mailingAddress1;
		private Object mailingAddress2;
		private Object mailingCity;
		private Object mailingCountryCode;
		private Object mailingState;
		private Object mailingZip; 
		private Object mailingPhone;
		private Object mailingFax;
		private Object billingAddress1;
		private Object billingAddress2;
		private Object billingCity;
		private Object billingCountryCode;
		private Object billingState;
		private Object billingZip; 
		private Object billingPhone;
		private Object billingFax;
		private Object coordinator;
		private Object firstName; 
		private Object vatNumber;
		private Object classcode;
		private Object accountManager;
		private Object salesMan;
		private Object accountHolder;
		private Object billingEmail;
		private Object abbreviation;
		private Object isAccount;
		private Object isAgent;
		private Object isCarrier;
		private Object isVendor;
		private Object isOwnerOp;
		private Object isPrivateParty;
		private Object mailingEmail;
		private Object collection;
		private Object creditTerms;
		private Object accountCrossReference;
		private Object companyDivision;
		
		
		public Object getCreditTerms() {
			return creditTerms;
		}
		public void setCreditTerms(Object creditTerms) {
			this.creditTerms = creditTerms;
		}
		public Object getBillingAddress1() {
			return billingAddress1;
		}
		public void setBillingAddress1(Object billingAddress1) {
			this.billingAddress1 = billingAddress1;
		}
		public Object getBillingAddress2() {
			return billingAddress2;
		}
		public void setBillingAddress2(Object billingAddress2) {
			this.billingAddress2 = billingAddress2;
		}
		public Object getBillingCity() {
			return billingCity;
		}
		public void setBillingCity(Object billingCity) {
			this.billingCity = billingCity;
		}
		public Object getBillingCountryCode() {
			return billingCountryCode;
		}
		public void setBillingCountryCode(Object billingCountryCode) {
			this.billingCountryCode = billingCountryCode;
		}
		public Object getBillingFax() {
			return billingFax;
		}
		public void setBillingFax(Object billingFax) {
			this.billingFax = billingFax;
		}
		public Object getBillingPhone() {
			return billingPhone;
		}
		public void setBillingPhone(Object billingPhone) {
			this.billingPhone = billingPhone;
		}
		public Object getBillingState() {
			return billingState;
		}
		public void setBillingState(Object billingState) {
			this.billingState = billingState;
		}
		public Object getBillingZip() {
			return billingZip;
		}
		public void setBillingZip(Object billingZip) {
			this.billingZip = billingZip;
		}
		public Object getCoordinator() {
			return coordinator;
		}
		public void setCoordinator(Object coordinator) {
			this.coordinator = coordinator;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getMailingAddress1() {
			return mailingAddress1;
		}
		public void setMailingAddress1(Object mailingAddress1) {
			this.mailingAddress1 = mailingAddress1;
		}
		public Object getMailingAddress2() {
			return mailingAddress2;
		}
		public void setMailingAddress2(Object mailingAddress2) {
			this.mailingAddress2 = mailingAddress2;
		}
		public Object getMailingCity() {
			return mailingCity;
		}
		public void setMailingCity(Object mailingCity) {
			this.mailingCity = mailingCity;
		}
		public Object getMailingCountryCode() {
			return mailingCountryCode;
		}
		public void setMailingCountryCode(Object mailingCountryCode) {
			this.mailingCountryCode = mailingCountryCode;
		}
		public Object getMailingFax() {
			return mailingFax;
		}
		public void setMailingFax(Object mailingFax) {
			this.mailingFax = mailingFax;
		}
		public Object getMailingPhone() {
			return mailingPhone;
		}
		public void setMailingPhone(Object mailingPhone) {
			this.mailingPhone = mailingPhone;
		}
		public Object getMailingState() {
			return mailingState;
		}
		public void setMailingState(Object mailingState) {
			this.mailingState = mailingState;
		}
		public Object getMailingZip() {
			return mailingZip;
		}
		public void setMailingZip(Object mailingZip) {
			this.mailingZip = mailingZip;
		}
		public Object getMiddleInitial() {
			return middleInitial;
		}
		public void setMiddleInitial(Object middleInitial) {
			this.middleInitial = middleInitial;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		public Object getPartnerPrefix() {
			return partnerPrefix;
		}
		public void setPartnerPrefix(Object partnerPrefix) {
			this.partnerPrefix = partnerPrefix;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getAccountManager() {
			return accountManager;
		}
		public void setAccountManager(Object accountManager) {
			this.accountManager = accountManager;
		}
		public Object getClasscode() {
			return classcode;
		}
		public void setClasscode(Object classcode) {
			this.classcode = classcode;
		}
		public Object getSalesMan() {
			return salesMan;
		}
		public void setSalesMan(Object salesMan) {
			this.salesMan = salesMan;
		}
		public Object getVatNumber() {
			return vatNumber;
		}
		public void setVatNumber(Object vatNumber) {
			this.vatNumber = vatNumber;
		}
		public Object getAccountHolder() {
			return accountHolder;
		}
		public void setAccountHolder(Object accountHolder) {
			this.accountHolder = accountHolder;
		}
		public Object getBillingEmail() {
			return billingEmail;
		}
		public void setBillingEmail(Object billingEmail) {
			this.billingEmail = billingEmail;
		}
		public Object getAbbreviation() {
			return abbreviation;
		}
		public void setAbbreviation(Object abbreviation) {
			this.abbreviation = abbreviation;
		}
		public Object getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Object isAccount) {
			this.isAccount = isAccount;
		}
		public Object getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Object isAgent) {
			this.isAgent = isAgent;
		}
		public Object getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Object isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Object getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Object isVendor) {
			this.isVendor = isVendor;
		}
		public Object getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Object isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Object getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Object isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Object getMailingEmail() {
			return mailingEmail;
		}
		public void setMailingEmail(Object mailingEmail) {
			this.mailingEmail = mailingEmail;
		}
		public Object getCollection() {
			return collection;
		}
		public void setCollection(Object collection) {
			this.collection = collection;
		}
		public Object getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(Object accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
	}
	
	public class PartnerDTO implements ListLinkData {
		private Object id;
		private Object partnerCode;
		private Object lastName;
		private Object firstName;
		private Object aliasName;
		private Object isAgent;
		private Object isAccount;
		private Object isPrivateParty;
		private Object isCarrier;
		private Object isVendor;
		private Object isOwnerOp;
		private Object status;
		private Object countryName;
		private Object stateName;
		private Object cityName;
		private Object stage;
		private Object vanLineCode;
		private Object add1;
		private Object add2;
		private Object zip;
		private Object companyDiv;
		private Object terminalCountryCode;		
		private Object externalRef;
		private Object terminalAddress1;
		private Object terminalAddress2;
		private Object terminalCity;
		private Object terminalZip;
		private Object terminalState;
		private Object terminalCountry;
		private Object NPSscore;
		private Object agentGroup;
		private Object agentClassification;
		private Object preferedagentType;
	
		
		@Transient
		public String getListCode() {
			return (String) partnerCode;
		}

		@Transient
		public String getListDescription() {
			String lName = (String) this.lastName;
			return lName;
		}		
		
		@Transient
		public String getListSecondDescription() {
			String vanLineCode ="";
			if(this.vanLineCode!=null){
				 vanLineCode = ((String) this.vanLineCode).replaceAll("&", "%26");
					vanLineCode = vanLineCode.replaceAll("#", "%23");	
			}
			return vanLineCode;
			//return  "";
		}

		@Transient
		public String getListThirdDescription() {
			return "";
		}

		@Transient
		public String getListFourthDescription() {
			return "";
		}

		@Transient
		public String getListFifthDescription() {
			return "";
		}

		@Transient
		public String getListSixthDescription() {
			return "";
		}
		@Transient
		public String getListEigthDescription() {
			return null;
		}
		@Transient
		public String getListNinthDescription() {
			return null;
		}
		@Transient
		public String getListSeventhDescription() {
			return null;
		}
		@Transient
		public String getListTenthDescription() {
			return null;
		}		
		
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Object isAccount) {
			this.isAccount = isAccount;
		}
		public Object getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Object isAgent) {
			this.isAgent = isAgent;
		}
		public Object getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Object isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Object getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Object isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Object getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Object isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Object getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Object isVendor) {
			this.isVendor = isVendor;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getCityName() {
			return cityName;
		}
		public void setCityName(Object cityName) {
			this.cityName = cityName;
		}
		public Object getCountryName() {
			return countryName;
		}
		public void setCountryName(Object countryName) {
			this.countryName = countryName;
		}
		public Object getStateName() {
			return stateName;
		}
		public void setStateName(Object stateName) {
			this.stateName = stateName;
		}
		public Object getStage() {
			return stage;
		}
		public void setStage(Object stage) {
			this.stage = stage;
		}
		public Object getVanLineCode() {
			return vanLineCode;
		}
		public void setVanLineCode(Object vanLineCode) {
			this.vanLineCode = vanLineCode;
		}

		public Object getAdd1() {
			return add1;
		}

		public void setAdd1(Object add1) {
			this.add1 = add1;
		}

		public Object getAdd2() {
			return add2;
		}

		public void setAdd2(Object add2) {
			this.add2 = add2;
		}

		public Object getZip() {
			return zip;
		}

		public void setZip(Object zip) {
			this.zip = zip;
		}

		public Object getCompanyDiv() {
			return companyDiv;
		}

		public void setCompanyDiv(Object companyDiv) {
			this.companyDiv = companyDiv;
		}

		public Object getTerminalCountryCode() {
			return terminalCountryCode;
		}

		public void setTerminalCountryCode(Object terminalCountryCode) {
			this.terminalCountryCode = terminalCountryCode;
		}

		public Object getAliasName() {
			return aliasName;
		}

		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}

		public Object getExternalRef() {
			return externalRef;
		}

		public void setExternalRef(Object externalRef) {
			this.externalRef = externalRef;
		}

		public Object getTerminalAddress1() {
			return terminalAddress1;
		}

		public void setTerminalAddress1(Object terminalAddress1) {
			this.terminalAddress1 = terminalAddress1;
		}

		public Object getTerminalAddress2() {
			return terminalAddress2;
		}

		public void setTerminalAddress2(Object terminalAddress2) {
			this.terminalAddress2 = terminalAddress2;
		}

		public Object getTerminalCity() {
			return terminalCity;
		}

		public void setTerminalCity(Object terminalCity) {
			this.terminalCity = terminalCity;
		}

		public Object getTerminalZip() {
			return terminalZip;
		}

		public void setTerminalZip(Object terminalZip) {
			this.terminalZip = terminalZip;
		}

		public Object getTerminalState() {
			return terminalState;
		}

		public void setTerminalState(Object terminalState) {
			this.terminalState = terminalState;
		}

		public Object getTerminalCountry() {
			return terminalCountry;
		}

		public void setTerminalCountry(Object terminalCountry) {
			this.terminalCountry = terminalCountry;
		}

		public Object getNPSscore() {
			return NPSscore;
		}

		public void setNPSscore(Object nPSscore) {
			NPSscore = nPSscore;
		}

		public Object getAgentGroup() {
			return agentGroup;
		}

		public void setAgentGroup(Object agentGroup) {
			this.agentGroup = agentGroup;
		}

		public Object getAgentClassification() {
			return agentClassification;
		}

		public void setAgentClassification(Object agentClassification) {
			this.agentClassification = agentClassification;
		}

		public Object getPreferedagentType() {
			return preferedagentType;
		}

		public void setPreferedagentType(Object preferedagentType) {
			this.preferedagentType = preferedagentType;
		}

	}

	public class PartnerBookingDTO implements ListLinkData {
		private Object partnerCode;
		private Object lastName;
		private Object companyDiv;
		private Object terminalCountryCode;		
		
		@Transient
		public String getListCode() {
			return (String) partnerCode;
		}

		@Transient
		public String getListDescription() {
			String lName = (String) this.lastName;
			return lName;
		}		
		
		@Transient
		public String getListSecondDescription() {
			//String vanLineCode = ((String) this.vanLineCode).replaceAll("&", "%26");
			//vanLineCode = vanLineCode.replaceAll("#", "%23");
			//return vanLineCode;
			return  "";
		}

		@Transient
		public String getListThirdDescription() {
			return "";
		}

		@Transient
		public String getListFourthDescription() {
			return "";
		}

		@Transient
		public String getListFifthDescription() {
			return "";
		}

		@Transient
		public String getListSixthDescription() {
			return "";
		}
		@Transient
		public String getListEigthDescription() {
			return null;
		}
		@Transient
		public String getListNinthDescription() {
			return null;
		}
		@Transient
		public String getListSeventhDescription() {
			return null;
		}
		@Transient
		public String getListTenthDescription() {
			return null;
		}

		public Object getCompanyDiv() {
			return companyDiv;
		}

		public void setCompanyDiv(Object companyDiv) {
			this.companyDiv = companyDiv;
		}

		public Object getLastName() {
			return lastName;
		}

		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}

		public Object getPartnerCode() {
			return partnerCode;
		}

		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}

		public Object getTerminalCountryCode() {
			return terminalCountryCode;
		}

		public void setTerminalCountryCode(Object terminalCountryCode) {
			this.terminalCountryCode = terminalCountryCode;
		}		
	}
		
	
	
	public List synInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID) {
		
		List <Object> partnerList = new ArrayList<Object>();
        List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p1.mailingAddress1,p1.mailingAddress2,p1.mailingCity,p1.mailingCountryCode,p1.mailingState,p1.mailingZip,p1.mailingPhone,p1.mailingFax,p1.billingAddress1,p1.billingAddress2,p1.billingCity,p1.billingCountryCode,p1.billingState,p1.billingZip,p1.billingPhone,p1.billingFax,p2.coordinator,p1.firstName ,p2.vatNumber,p2.classcode,p2.accountManager,p2.accountHolder,p1.billingEmail,p2.abbreviation,p1.isAccount,  p1.isAgent,  p1.isCarrier, p1.isVendor, p1.isOwnerOp, p1.isPrivateParty,p1.mailingEmail,p2.collection from partnerpublic p1, partnerprivate p2, accountline a, serviceorder s where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and s.corpid = '"+corpID+"' and (p1.createdon >= '"+sysInvoiceDates+"' or p1.updatedon >= '"+sysInvoiceDates+"') and a.serviceorderid = s.id and a.billtocode = p1.partnercode and p1.id=p2.partnerPublicId and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) and a.recaccdate is not null and s.companydivision = '"+companyDivision+"'").list();
		DTOSynInvoicePartner synInvoicePartnerDTO=null;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			synInvoicePartnerDTO=new DTOSynInvoicePartner();
			synInvoicePartnerDTO.setPartnerCode(obj[0]);
			synInvoicePartnerDTO.setLastName(obj[1]);
			synInvoicePartnerDTO.setPartnerPrefix(obj[2]);
			synInvoicePartnerDTO.setMiddleInitial(obj[3]);
			synInvoicePartnerDTO.setMailingAddress1(obj[4]);
			synInvoicePartnerDTO.setMailingAddress2(obj[5]);
			synInvoicePartnerDTO.setMailingCity(obj[6]);
			synInvoicePartnerDTO.setMailingCountryCode(obj[7]);
			synInvoicePartnerDTO.setMailingState(obj[8]);
			synInvoicePartnerDTO.setMailingZip(obj[9]);
			synInvoicePartnerDTO.setMailingPhone(obj[10]);
			synInvoicePartnerDTO.setMailingFax(obj[11]);
			synInvoicePartnerDTO.setBillingAddress1(obj[12]);
			synInvoicePartnerDTO.setBillingAddress2(obj[13]);
			synInvoicePartnerDTO.setBillingCity(obj[14]);
			synInvoicePartnerDTO.setBillingCountryCode(obj[15]);
			synInvoicePartnerDTO.setBillingState(obj[16]);
			synInvoicePartnerDTO.setBillingZip(obj[17]);
			synInvoicePartnerDTO.setBillingPhone(obj[18]);
			synInvoicePartnerDTO.setBillingFax(obj[19]);
			synInvoicePartnerDTO.setCoordinator(obj[20]);
			synInvoicePartnerDTO.setFirstName(obj[21]);
			synInvoicePartnerDTO.setVatNumber(obj[22]);
			synInvoicePartnerDTO.setClasscode(obj[23]);
			synInvoicePartnerDTO.setAccountManager(obj[24]);
			synInvoicePartnerDTO.setAccountHolder(obj[25]);
			synInvoicePartnerDTO.setBillingEmail(obj[26]);  
			synInvoicePartnerDTO.setAbbreviation(obj[27]);
			synInvoicePartnerDTO.setIsAccount(obj[28]);
			synInvoicePartnerDTO.setIsAgent(obj[29]);
			synInvoicePartnerDTO.setIsCarrier(obj[30]);
			synInvoicePartnerDTO.setIsVendor(obj[31]);
			synInvoicePartnerDTO.setIsOwnerOp(obj[32]);
			synInvoicePartnerDTO.setIsPrivateParty(obj[33]);
			synInvoicePartnerDTO.setMailingEmail(obj[34]);
			synInvoicePartnerDTO.setCollection(obj[35]);
			partnerList.add(synInvoicePartnerDTO);
	}
		return partnerList;
	}

	
	public List synInvoicePartnerExtractUTSI(String sysInvoiceDates, String companyDivision, String corpID) {
		
		List <Object> partnerList = new ArrayList<Object>();
        List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p1.mailingAddress1,p1.mailingAddress2,p1.mailingCity,p1.mailingCountryCode,p1.mailingState,p1.mailingZip,p1.mailingPhone,p1.mailingFax,p1.billingAddress1,p1.billingAddress2,p1.billingCity,p1.billingCountryCode,p1.billingState,p1.billingZip,p1.billingPhone,p1.billingFax,p2.coordinator,p1.firstName ,p2.vatNumber,p2.classcode,p2.accountManager,p2.accountHolder,p1.billingEmail,p2.abbreviation from partnerpublic p1, partnerprivate p2, accountline a, serviceorder s where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and s.corpid = '"+corpID+"' and (p1.createdon >= '"+sysInvoiceDates+"' or p1.updatedon >= '"+sysInvoiceDates+"') and a.serviceorderid = s.id and a.billtocode = p1.partnercode and p1.id=p2.partnerPublicId and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) and a.recaccdate is not null and s.companydivision = '"+companyDivision+"'").list();
		DTOSynInvoicePartner synInvoicePartnerDTO=null;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			synInvoicePartnerDTO=new DTOSynInvoicePartner();
			synInvoicePartnerDTO.setPartnerCode(obj[0]);
			synInvoicePartnerDTO.setLastName(obj[1]);
			synInvoicePartnerDTO.setPartnerPrefix(obj[2]);
			synInvoicePartnerDTO.setMiddleInitial(obj[3]);
			synInvoicePartnerDTO.setMailingAddress1(obj[4]);
			synInvoicePartnerDTO.setMailingAddress2(obj[5]);
			synInvoicePartnerDTO.setMailingCity(obj[6]);
			synInvoicePartnerDTO.setMailingCountryCode(obj[7]);
			synInvoicePartnerDTO.setMailingState(obj[8]);
			synInvoicePartnerDTO.setMailingZip(obj[9]);
			synInvoicePartnerDTO.setMailingPhone(obj[10]);
			synInvoicePartnerDTO.setMailingFax(obj[11]);
			synInvoicePartnerDTO.setBillingAddress1(obj[12]);
			synInvoicePartnerDTO.setBillingAddress2(obj[13]);
			synInvoicePartnerDTO.setBillingCity(obj[14]);
			synInvoicePartnerDTO.setBillingCountryCode(obj[15]);
			synInvoicePartnerDTO.setBillingState(obj[16]);
			synInvoicePartnerDTO.setBillingZip(obj[17]);
			synInvoicePartnerDTO.setBillingPhone(obj[18]);
			synInvoicePartnerDTO.setBillingFax(obj[19]);
			synInvoicePartnerDTO.setCoordinator(obj[20]);
			synInvoicePartnerDTO.setFirstName(obj[21]);
			synInvoicePartnerDTO.setVatNumber(obj[22]);
			synInvoicePartnerDTO.setClasscode(obj[23]);
			synInvoicePartnerDTO.setAccountManager(obj[24]);
			synInvoicePartnerDTO.setAccountHolder(obj[25]);
			synInvoicePartnerDTO.setBillingEmail(obj[26]);  
			synInvoicePartnerDTO.setAbbreviation(obj[27]);
			partnerList.add(synInvoicePartnerDTO);
	}
		return partnerList;
	}
	public List getAllAgent(String sessionUserName) {
		return this.getSession().createSQLQuery("select subString_index(dss.name,'_', -1) as partnercode from datasecurityset dss, userdatasecurity uds, app_user ap where dss.id=userdatasecurity_id and ap.id=uds.user_id and ap.username='"+ sessionUserName +"' and subString_index(dss.name,'_', -1) <> 'AGENT'").list();
	}

	public List searchAgentList(String firstName, String lastName, String partnerCode, String countryCode, String stateCode, String country, String status, String partnerIds, String corpID) {
		return getHibernateTemplate().find("FROM Partner WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' " +
			"AND lastName like '%" + lastName.replaceAll("'", "''") + "%' " +
			"AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
			"AND terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%'  " +
			"AND terminalCountry like '%" + country.replaceAll("'", "''") + "%' " +
			"AND terminalState like '%" + stateCode.replaceAll("'", "''") + "%' " +
			"AND status like '"+ status +"%'  and status <> 'Inactive' AND partnerCode in (" + partnerIds + ")");
	}
	public List findAcctRefNum(String code, String corpId){
		return getHibernateTemplate().find("from PartnerAccountRef where partnerCode='"+code+"'");
	}
	public List findPartnerProfile(String code, String corpId){
		return getHibernateTemplate().find("from Partner where partnerCode='"+code+"' ");
	}
	
	public List<Partner> financeAgentPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID) {
		getHibernateTemplate().setMaxResults(50000);
		if (companyDivision.equals("") || companyDivision == null) {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  isAgent=true");
		} else {
			return getHibernateTemplate().find(
					"select p from Partner p " +
					"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + companyDivision + "' " + 
					"where ((DATE_FORMAT(p.createdOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(p.updatedOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  isAgent=true and p.companyDivision = '" + companyDivision + "'");
		}
	}
	
	public List<Partner> financeAgentPartnerExtractUTSI(String beginDate, String endDate, String companyDivision,String corpID) {
		getHibernateTemplate().setMaxResults(50000);
		if (companyDivision.equals("") || companyDivision == null) {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  isAgent=true");
		} else {
			return getHibernateTemplate().find(
					"select p from Partner p " +
					"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + companyDivision + "' " + 
					"where ((DATE_FORMAT(p.createdOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(p.updatedOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  isAgent=true and p.companyDivision = '" + companyDivision + "'");
		}
	}

	public List<Partner> findVendorCode(Long sid, String sessionCorpID, String jobType,Long cid) {
		getHibernateTemplate().setMaxResults(100);
		List jobList=new ArrayList();
		List carrierCodeList=new ArrayList();
		List accountVendorCodeList=new ArrayList();
		List driverIdList=new ArrayList();
		List bookingAgentCodeList=new ArrayList();
		List crewCodeList=new ArrayList();
		String partnerCode=new String();
		String carrierCode=new String();
		String vandorCode=new String();
		String driverId=new String();
		String crewPartnerCode=new String();
		String bookingAgentCode=new String();
		
		List b = this.getSession().createSQLQuery("select CONCAT(if(originAgentCode is null,' ',originAgentCode),''',''',if(originSubAgentCode is null,' ',originSubAgentCode),''',''',if(destinationAgentCode is null,' ',destinationAgentCode),''',''',if(destinationSubAgentCode is null,' ',destinationSubAgentCode),''',''',if(forwarderCode is null,' ',forwarderCode),''',''',if(brokerCode is null,' ',brokerCode)) from trackingstatus where id='"+sid+"' and corpID='"+sessionCorpID+"'").list();
		String s = "";
		if (b.isEmpty()) {
			s = "";
		} else {
			s = b.get(0).toString();
		}
		s=s.trim();
		jobList = getHibernateTemplate().find("select partnerCode from Partner where accountingDefault is true and acctDefaultJobType Like'%"+jobType+"%' ");
		if(jobList!=null && (!jobList.isEmpty())){
		Iterator jobListIterator=jobList.iterator(); 
		while(jobListIterator.hasNext()){
			partnerCode=(String)jobListIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
			s=s+"','"+partnerCode+"','";
			else if(s.lastIndexOf("'") == s.length() - 1) 
		    s=s+partnerCode+"','";
		}
		}
		carrierCodeList=getHibernateTemplate().find("select carrierCode from ServicePartner where serviceOrderId='"+sid+"' and status=true and corpID='"+sessionCorpID+"'");
		if(carrierCodeList!=null && (!carrierCodeList.isEmpty())){
			Iterator carrierIterator=carrierCodeList.iterator(); 
			while(carrierIterator.hasNext()){
			carrierCode=(String)carrierIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+carrierCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+carrierCode+"','";
			}
		}
		accountVendorCodeList=getHibernateTemplate().find("select vendorCode from AccountLine where serviceOrderId='"+sid+"' and corpID='"+sessionCorpID+"' and vendorcode is not null and vendorcode <> ''");
		if(accountVendorCodeList!=null && (!accountVendorCodeList.isEmpty())){
			Iterator vendorIterator=accountVendorCodeList.iterator(); 
			while(vendorIterator.hasNext()){
				vandorCode=(String)vendorIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+vandorCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+vandorCode+"','";
			}
		}
		driverIdList=getHibernateTemplate().find("select driverId from Miscellaneous where id='"+sid+"' and corpID='"+sessionCorpID+"' and driverId is not null and driverId <> ''");
		if(driverIdList!=null && (!driverIdList.isEmpty())){
			Iterator driverIdIterator=driverIdList.iterator(); 
			while(driverIdIterator.hasNext()){
				driverId=(String)driverIdIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+driverId+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+driverId+"','";
			}
		}
		bookingAgentCodeList=getHibernateTemplate().find("select bookingAgentCode from CustomerFile where id='"+cid+"' and corpID='"+sessionCorpID+"' and bookingAgentCode is not null and bookingAgentCode <> ''");
		if(bookingAgentCodeList!=null && (!bookingAgentCodeList.isEmpty())){
			Iterator bookingAgentCodeListIterator=bookingAgentCodeList.iterator(); 
			while(bookingAgentCodeListIterator.hasNext()){
				bookingAgentCode=(String)bookingAgentCodeListIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+bookingAgentCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+bookingAgentCode+"','";
			}
		}
		String queryTemp="select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','N',driverCrewType) from systemdefault where corpid='"+sessionCorpID+"' limit 1";
		List alQueryTemp=this.getSession().createSQLQuery(queryTemp).list();
		String ts="";
		if((alQueryTemp!=null)&&(!alQueryTemp.isEmpty())&&(alQueryTemp.get(0)!=null)&&(!alQueryTemp.get(0).toString().equalsIgnoreCase("N"))){
			ts=alQueryTemp.get(0).toString();
		}
		if(!ts.equalsIgnoreCase("")){
			String query1="select distinct c.partnerCode from workticket w,timesheet t,crew c where w.serviceOrderId='"+sid+"' and w.corpId='"+sessionCorpID+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and t.crewType=c.typeOfWork and c.userName=t.userName and c.partnerCode!=''";
			crewCodeList=this.getSession().createSQLQuery(query1).list();
			if(crewCodeList!=null && (!crewCodeList.isEmpty())){
				Iterator crewCodeListIterator=crewCodeList.iterator(); 
				while(crewCodeListIterator.hasNext()){
					crewPartnerCode=(String)crewCodeListIterator.next();
				if(!(s.lastIndexOf("'") == s.length() - 1))
					s=s+"','"+crewPartnerCode+"','";
					else if(s.lastIndexOf("'") == s.length() - 1) 
				    s=s+crewPartnerCode+"','";
				}
			}
		}
		if(s.lastIndexOf("'") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    }
		if(s.lastIndexOf(",") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    } 
		if(s.lastIndexOf("'") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    }
		
		List a = getHibernateTemplate().find("from Partner where partnerCode in ('" + s + "') and corpID='"+sessionCorpID+"' and status in ('Approved','New')"); 
		return a;
	}

	public List searchDefaultVandor(Long sid, String job, String partnerType, String sessionCorpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, Long cid, String externalRef) {
		

		getHibernateTemplate().setMaxResults(100);
		List jobList=new ArrayList();
		List carrierCodeList=new ArrayList();
		List accountVendorCodeList=new ArrayList();
		List driverIdList=new ArrayList();
		List bookingAgentCodeList=new ArrayList();
		List crewCodeList=new ArrayList();
		String partnerCodes=new String();
		String carrierCode=new String();
		String vandorCode=new String();
		String driverId=new String();
		String crewPartnerCode=new String();
		String bookingAgentCode=new String();
		
		List b = this.getSession().createSQLQuery("select CONCAT(if(originAgentCode is null,' ',originAgentCode),''',''',if(originSubAgentCode is null,' ',originSubAgentCode),''',''',if(destinationAgentCode is null,' ',destinationAgentCode),''',''',if(destinationSubAgentCode is null,' ',destinationSubAgentCode),''',''',if(forwarderCode is null,' ',forwarderCode),''',''',if(brokerCode is null,' ',brokerCode)) from trackingstatus where id='"+sid+"' and corpID='"+sessionCorpID+"'").list();
		String s = "";
		if (b.isEmpty()) {
			s = "";
		} else {
			s = b.get(0).toString();
		}
		s=s.trim();
		jobList = getHibernateTemplate().find("select partnerCode from Partner where accountingDefault is true and acctDefaultJobType Like'%"+job+"%' ");
		if(jobList!=null && (!jobList.isEmpty())){
		Iterator jobListIterator=jobList.iterator(); 
		while(jobListIterator.hasNext()){
			partnerCodes=(String)jobListIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
			s=s+"','"+partnerCodes+"','";
			else if(s.lastIndexOf("'") == s.length() - 1) 
		    s=s+partnerCodes+"','";
		}
		}
		carrierCodeList=getHibernateTemplate().find("select carrierCode from ServicePartner where serviceOrderId='"+sid+"' and status=true and corpID='"+sessionCorpID+"'");
		if(carrierCodeList!=null && (!carrierCodeList.isEmpty())){
			Iterator carrierIterator=carrierCodeList.iterator(); 
			while(carrierIterator.hasNext()){
			carrierCode=(String)carrierIterator.next(); 
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+carrierCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+carrierCode+"','";
		}
		}
		accountVendorCodeList=getHibernateTemplate().find("select vendorCode from AccountLine where serviceOrderId='"+sid+"' and corpID='"+sessionCorpID+"' and vendorcode is not null and vendorcode <> ''");
		if(accountVendorCodeList!=null && (!accountVendorCodeList.isEmpty())){
			Iterator vendorIterator=accountVendorCodeList.iterator(); 
			while(vendorIterator.hasNext()){
				vandorCode=(String)vendorIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+vandorCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+vandorCode+"','";
			}
		}
		driverIdList=getHibernateTemplate().find("select driverId from Miscellaneous where id='"+sid+"' and corpID='"+sessionCorpID+"' and driverId is not null and driverId <> ''");
		if(driverIdList!=null && (!driverIdList.isEmpty())){
			Iterator driverIdIterator=driverIdList.iterator(); 
			while(driverIdIterator.hasNext()){
				driverId=(String)driverIdIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+driverId+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+driverId+"','";
			}
		}
		bookingAgentCodeList=getHibernateTemplate().find("select bookingAgentCode from CustomerFile where id='"+cid+"' and corpID='"+sessionCorpID+"' and bookingAgentCode is not null and bookingAgentCode <> ''");
		if(bookingAgentCodeList!=null && (!bookingAgentCodeList.isEmpty())){
			Iterator bookingAgentCodeListIterator=bookingAgentCodeList.iterator(); 
			while(bookingAgentCodeListIterator.hasNext()){
				bookingAgentCode=(String)bookingAgentCodeListIterator.next();
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+bookingAgentCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+bookingAgentCode+"','";
			}
		}
		String queryTemp="select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','N',driverCrewType) from systemdefault where corpid='"+sessionCorpID+"' limit 1";
		List alQueryTemp=this.getSession().createSQLQuery(queryTemp).list();
		String ts="";
		if((alQueryTemp!=null)&&(!alQueryTemp.isEmpty())&&(alQueryTemp.get(0)!=null)&&(!alQueryTemp.get(0).toString().equalsIgnoreCase("N"))){
			ts=alQueryTemp.get(0).toString();
		}
		if(!ts.equalsIgnoreCase("")){		
			String query1="select distinct c.partnerCode from workticket w,timesheet t,crew c where w.serviceOrderId='"+sid+"' and w.corpId='"+sessionCorpID+"' and w.ticket=t.ticket and c.partnerCode is not null "+(ts.equalsIgnoreCase("")?"":" AND c.typeOfWork in"+ts)+" and t.crewType=c.typeOfWork and c.userName=t.userName and c.partnerCode!=''";
			crewCodeList=this.getSession().createSQLQuery(query1).list();
			if(crewCodeList!=null && (!crewCodeList.isEmpty())){
				Iterator crewCodeListIterator=crewCodeList.iterator(); 
				while(crewCodeListIterator.hasNext()){
					crewPartnerCode=(String)crewCodeListIterator.next();
				if(!(s.lastIndexOf("'") == s.length() - 1))
					s=s+"','"+crewPartnerCode+"','";
					else if(s.lastIndexOf("'") == s.length() - 1) 
				    s=s+crewPartnerCode+"','";
				}
			}		
		}
		if(s.lastIndexOf("'") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    }
		if(s.lastIndexOf(",") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    } 
		if(s.lastIndexOf("'") == s.length() - 1) 
		   {
			  s = s.substring(0, s.length() - 1);
		    } 
		List a=new ArrayList();
		if(externalRef.trim().equals("")){ 
			a = getHibernateTemplate().find("from Partner where partnerCode in ('" + s + "')  and status in ('Approved','New')and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND billingCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%" + externalRef.replaceAll("'", "''") + "%' or extReference is null) ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')"); 
			
			
		}else{
		a = getHibernateTemplate().find("from Partner where partnerCode in ('" + s + "')  and status in ('Approved','New')and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + externalRef.replaceAll("'", "''") + "%' ORDER BY FIELD(agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER','')"); 
		}
		return a;
	
	}

	public List findBookingAgentCode(String companyDivision, String sessionCorpID) {
	 List <Object> partnerList = new ArrayList<Object>();	
	  //List partnerList1 = this.getSession().createSQLQuery("select p.partnercode, if(lastname is null,'', lastname), terminalcountrycode, p.companydivision from partner p where p.companydivision in ( '"+companyDivision+"','')and p.status = 'Approved' and p.corpid in ('TSFT', '"+sessionCorpID+"') and (isagent is true or isprivateparty is true or isaccount is true) union select p.partnercode, if(lastname is null,'', lastname), terminalcountrycode, r.companydivision from partner p, partneraccountref r where p.partnercode = r.partnercode and    r.corpid = '"+sessionCorpID+"' and r.companydivision = '"+companyDivision+"' and p.status = 'Approved' and p.corpid in ('TSFT', '"+sessionCorpID+"')  and (isagent is true or isprivateparty is true or isaccount is true) order by 1, 2;").list(); 
	 List partnerList1=this.getSession().createSQLQuery("select p.partnerCode, if(p.lastName is null,'', p.lastName) as lastName, p.terminalCountryCode as terminalCountryCode,p2.companyDivision as companyDiv from partnerpublic p, partnerprivate p2 where p2.companyDivision in ('"+companyDivision+"','') and p.status = 'Approved' and p.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and (p.isAgent is true or p.isPrivateParty is true or p.isAccount is true) and p.id = p2.partnerPublicId and p2.corpID in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') union select p.partnerCode, if(p2.lastName is null,'', p2.lastName)  as lastName, p.terminalCountryCode as terminalCountryCode, r.companyDivision  as companyDiv from partnerpublic p, partneraccountref r,  partnerprivate p2 where p.partnerCode = r.partnerCode and p.id = p2.partnerPublicId and p2.corpID  in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and r.corpID  in ('"+sessionCorpID+"') and r.companyDivision = '"+companyDivision+"' and p.status = 'Approved' and p.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and (p.isAgent is true or p.isPrivateParty is true or p.isAccount is true) order by 1, 2 limit 100;")
	 .addScalar("partnerCode",Hibernate.STRING)
	 .addScalar("lastName", Hibernate.STRING)
	 .addScalar("terminalCountryCode", Hibernate.STRING)
	 .addScalar("companyDiv", Hibernate.STRING)
	 .list();
	 //PartnerPublic partner;
	 PartnerBookingDTO pardto=null;
	 String samePartnerCode="";
		Iterator it=partnerList1.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			pardto=new PartnerBookingDTO();
			if(obj[0]==null){
				pardto.setPartnerCode("");	
			}else {
				pardto.setPartnerCode((String) obj[0]);
			}
			String newPartnerCode=(String) obj[0];
			if(obj[1]==null){
				pardto.setLastName("");
			}else{
				pardto.setLastName(obj[1]);	
			}
			if(obj[2]==null){
				pardto.setTerminalCountryCode("");
			}else{
				pardto.setTerminalCountryCode(obj[2]);
			}if(obj[3]==null){
				pardto.setCompanyDiv("");
			}else{
				pardto.setCompanyDiv(obj[3]);
			}
			if(samePartnerCode!=null && newPartnerCode !=null && (!(samePartnerCode.trim().equalsIgnoreCase(newPartnerCode.trim())))){
            partnerList.add(pardto);
			}
			samePartnerCode=newPartnerCode;
		}
		return partnerList;
	}

	public List searchBookingAgent(String companyDivision, String sessionCorpID, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode) { 
		 List <Object> partnerList = new ArrayList<Object>();	
		 List partnerList1 = this.getSession().createSQLQuery("select p.partnerCode, if(p.lastName is null,'', p.lastName) as lastName, terminalCountryCode as terminalCountryCode, pp.companyDivision as companyDiv from partnerpublic p, partnerprivate pp where pp.companyDivision in ( '"+companyDivision+"','') and p.status = 'Approved' and p.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and (isAgent is true or isPrivateParty is true or isAccount is true) and (p.lastName like '" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '"
           + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND terminalCountryCode like '"
           + billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
           + "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%' and p.id=pp.partnerPublicId union select p.partnerCode, if(p.lastName is null,'', p.lastName) as lastName, terminalCountryCode as terminalCountryCode, r.companyDivision as companyDiv from  partnerpublic p, partnerprivate pp, partneraccountref r where p.partnerCode = r.partnerCode and    r.corpID  in ('"+sessionCorpID+"') and r.companyDivision = '"+companyDivision+"' and p.status = 'Approved' and p.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')  and (isAgent is true or isPrivateParty is true or isAccount is true) and (p.lastName like '" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '"
           + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND terminalCountryCode like '"
           + billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
           + "%' AND billingState like '" + billingStateCode.replaceAll("'", "''") + "%' and p.id=pp.partnerPublicId order by 1, 2;") 
		 .addScalar("partnerCode",Hibernate.STRING)
		 .addScalar("lastName", Hibernate.STRING)
		 .addScalar("terminalCountryCode", Hibernate.STRING)
		 .addScalar("companyDiv", Hibernate.STRING)
		 .list();
		 PartnerBookingDTO pardto=null;
		 String samePartnerCode="";
		 Iterator it=partnerList1.iterator();
			while(it.hasNext()){		 
				Object []obj=(Object[]) it.next();
				pardto=new PartnerBookingDTO();
				if(obj[0]==null){
					pardto.setPartnerCode("");	
				}else {
					pardto.setPartnerCode((String) obj[0]);
				}
				String newPartnerCode=(String) obj[0];
				if(obj[1]==null){
					pardto.setLastName("");
				}else{
					pardto.setLastName(obj[1]);	
				}
				if(obj[2]==null){
					pardto.setTerminalCountryCode("");
				}else{
					pardto.setTerminalCountryCode(obj[2]);
				}if(obj[3]==null){
					pardto.setCompanyDiv("");
				}else{
					pardto.setCompanyDiv(obj[3]);
				}
				if(samePartnerCode!=null && newPartnerCode !=null && (!(samePartnerCode.trim().equalsIgnoreCase(newPartnerCode.trim())))){
	            partnerList.add(pardto);
				}
				samePartnerCode=newPartnerCode;
			}
			return partnerList;
	
	}
	
	
	public List ratingList(String partnerCode, String corpID) {
		List <Object> partnerRatingList = new ArrayList<Object>();
		List partnerDitailList = this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', op.lastname, Sum(if(oaevaluation=1,1,0)) as 'positive response', Sum(if(oaevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA), op.partnercode union select year(t.deliveryA) as 'Year', dp.lastname, Sum(if(daevaluation=1,1,0)) as 'positive response', Sum(if(daevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA),dp.partnercode order by 1").list();
		HibernateDTO partnerRating;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerRating=new HibernateDTO();
			partnerRating.setYear(obj[0]);
			partnerRating.setLastName(obj[1]);
			partnerRating.setOaeValuationP(obj[2]);
			partnerRating.setOaeValuationN(obj[3]);
			
			partnerRatingList.add(partnerRating);
		}
		return partnerRatingList;
	}

	public List getFeedbackList(String partnerCode, String year, String corpID) {
		List <Object> ratingFeedbackList = new ArrayList<Object>();
		//List feedbackList = this.getSession().createSQLQuery("select year(t.deliveryA), if(oaevaluation=1,'+1','-1'), s.feedback, s.shipNumber from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('TSFT', '"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and year(t.deliveryA) = '"+year+"' union select year(t.deliveryA), if(daevaluation=1,'+1','-1'), s.feedback, s.shipnumber from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT', '"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and year(t.deliveryA) = '"+year+"'").list();
		List feedbackList = this.getSession().createSQLQuery("select year(t.deliveryA), if(oaevaluation=1,'+1','-1'), s.feedback, s.shipNumber from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= '"+year+"' union select year(t.deliveryA), if(daevaluation=1,'+1','-1'), s.feedback, s.shipnumber from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= '"+year+"'").list();
		HibernateDTO partnerFeedback;
		Iterator it=feedbackList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerFeedback=new HibernateDTO();
			partnerFeedback.setYear(obj[0]);
			partnerFeedback.setOaeValuation(obj[1]);
			partnerFeedback.setFeedback(obj[2]);
			partnerFeedback.setShipNumber(obj[3]);
			
			ratingFeedbackList.add(partnerFeedback);
		}
		
		return ratingFeedbackList;
	}
	
	public List getPartnerActivityList(String partnerCode, String corpID){
		List <Object> activityList = new ArrayList<Object>();
		//List partnerActivityList = this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('TSFT', '"+corpID+"') and op.partnercode =  '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA), op.partnercode union select year(t.deliveryA) as 'Year', count(s.shipnumber) 'DA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT', '"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA),dp.partnercode order by 1").list();
		//on 3rd june 2011 List partnerActivityList=this.getSession().createSQLQuery("select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id inner join partnerpublic  op on op.partnercode = t.originagentcode and op.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"'  where s.status not in ('CNCL','DWNLD')  and s.corpid = '"+corpID+"'  and b.revenuerecognition >= '2007-01-01'  group by year(b.revenuerecognition), op.partnercode    union    select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'DA Count',  round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs'  from serviceorder s  inner join trackingstatus t on s.id = t.id  inner join miscellaneous m on s.id = m.id  inner join billing b on s.id = b.id  inner join partnerpublic  dp on dp.partnercode = t.destinationagentcode and dp.corpid  in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and dp.partnercode =  '"+partnerCode+"'  where s.status not in ('CNCL','DWNLD')  and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01'  group by year(b.revenuerecognition),dp.partnercode  order by 1 ").list();
		List partnerActivityList=this.getSession().createSQLQuery("select year(b.revenuerecognition) as 'Year', " +
				" count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) " +
				" 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id " +
				"inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id " +
				"where s.status not in ('CNCL','DWNLD')  and s.corpid = '"+corpID+"'  " +
				"and b.revenuerecognition >= '2007-01-01' and s.bookingagentcode='"+partnerCode+"' " +
				" group by year(b.revenuerecognition) order by 1 ").list();
		HibernateDTO partnerActivity;
		Iterator it=partnerActivityList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerActivity=new HibernateDTO();
			partnerActivity.setYear(obj[0]);
			partnerActivity.setOaCount(obj[1]);
			partnerActivity.setActualWeight(obj[2]);
			
			activityList.add(partnerActivity);
		}
		return activityList;
	}

	public List getPartnerSSCWActivityList(String partnerCode, String corpID) {
		List <Object> activitySSCWList = new ArrayList<Object>();
		//List partnerSSCWActivityList = this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('TSFT', '"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and op.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA), op.partnercode union select year(t.deliveryA) as 'Year', count(s.shipnumber) 'DA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id  inner join miscellaneous m on s.id = m.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT', '"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and dp.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and t.deliverya >= '2007-01-01' group by year(t.deliveryA),dp.partnercode order by 1").list();
		List partnerSSCWActivityList = this.getSession().createSQLQuery("select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id inner join partnerpublic  op on op.partnercode = t.originagentcode and op.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and op.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' group by year(b.revenuerecognition), op.partnercode union select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'DA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id  inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id inner join partnerpublic  dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and dp.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' group by year(b.revenuerecognition),dp.partnercode order by 1").list();
		HibernateDTO partnerSSCWActivity;
		Iterator it=partnerSSCWActivityList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerSSCWActivity=new HibernateDTO();
			partnerSSCWActivity.setYear(obj[0]);
			partnerSSCWActivity.setOaCount(obj[1]);
			partnerSSCWActivity.setActualWeight(obj[2]);
			
			activitySSCWList.add(partnerSSCWActivity);
		}
		return activitySSCWList;
	}
	
	public List getPartnerListNew(String lastName, String countryCode, String sessionCorpID, String billingCity){
		return getHibernateTemplate().find("From Partner where lastname like '%"+lastName.replaceAll("'", "''")+"%'  and (terminalcountrycode = '"+countryCode+"' or billingcountrycode = '"+countryCode+"') and billingCity='"+billingCity+"' order by 2,3");
	}

	public List getPartnerPopupListAccount(String partnerType, String corpID,String firstName, String lastName,String aliasName, 
			String partnerCode, String countryCode, String country, String stateCode, String status, 
			Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, String externalRef, Boolean cmmDmmFlag) {
		String query1 = "";
		String query ="";
		if(externalRef.trim().equals("")){
			if(isAgt|| isVndr|| isCarr){
				query = "from Partner "+"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%')" +
				"AND  partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
				"AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%')" +
				"AND  terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%' " +
				"AND  terminalCountry like '%" + country.replaceAll("'", "''") + "%' " +
				"AND  terminalState like '%" + stateCode.replaceAll("'", "''") + "%'" +
				"AND status like '"+ status +"%' AND (extReference like '%" + externalRef.replaceAll("'", "''") + "%' or extReference is null))";
			}else {
				query = "from Partner "+"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
				"AND  partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
				"AND  (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
				"AND  billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%'" +
				"AND  billingCountry like '%" + country.replaceAll("'", "''") + "%' " +
				"AND  billingState like '%" + stateCode.replaceAll("'", "''") + "%' " +
				"AND status like '"+ status +"%'  AND (extReference like '%" + externalRef.replaceAll("'", "''") + "%' or extReference is null)";
				}	
		}else{
		if(isAgt|| isVndr|| isCarr){
			query = "from Partner "+"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND(lastName like '%" + lastName.replaceAll("'", "''") + "%' OR aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
			"AND  partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
			"AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
			"AND  terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%' " +
			"AND  terminalCountry like '%" + country.replaceAll("'", "''") + "%' " +
			"AND  terminalState like '%" + stateCode.replaceAll("'", "''") + "%'" +
			"AND status like '"+ status +"%'  AND extReference like '%" + externalRef.replaceAll("'", "''") + "%' ";
		}else {
			query = "from Partner "+"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
			"AND  partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
			"AND  (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' )" +
			"AND  billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%'" +
			"AND  billingCountry like '%" + country.replaceAll("'", "''") + "%' " +
			"AND  billingState like '%" + stateCode.replaceAll("'", "''") + "%' " +
			"AND status like '"+ status +"%'  AND extReference like '%" + externalRef.replaceAll("'", "''") + "%'";
			}
		}
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
			}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false";
		}
		return getHibernateTemplate().find(query); 
	}
	
	public List getPartnerVanLinePopupList(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String vanLineCODE, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean cmmDmmFlag) {
		List <Object> partnerList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(100);
		String query1 = "";
		String query = "select p.id, p.partnerCode, lastName, firstName,aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, status, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pv.vanLineCode, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress1,billingAddress1) as Add1, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress2,billingAddress2) as Add2, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalZip,billingZip) as Zip,p.agentClassification " +
					"FROM partner p " +
					"LEFT OUTER JOIN partnervanlineref pv on  p.partnerCode = pv.partnerCode " +
					"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%')AND( lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") +"%')" +
					"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
					// "AND  pv.vanLineCode like '%" + vanLineCODE.replaceAll("'", "''") + "%' " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
					"AND status = 'Approved' AND p.corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
		
		if(vanLineCODE != null && vanLineCODE.length()>0){
			query = query+ "AND  pv.vanLineCode like '%" + vanLineCODE.replaceAll("'", "''") + "%' ";
		}
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
		}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1+" limit 100";
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd).concat(" limit 100");
			}	
		}else{
			query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false  limit 100";
		}
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list();
		
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			
			dto.setId(row[0]);
			dto.setPartnerCode(row[1]);
			dto.setLastName(row[2]);
			
			if(row[3] == null){
				dto.setFirstName("");
			}else{
				dto.setFirstName(row[3]);
			}
			if(row[4] == null){
				dto.setAliasName("");
			}else{
				dto.setAliasName(row[4]);
			}
			
			if(row[5]==null){
				dto.setIsAgent("");
			}else{
				dto.setIsAgent(row[5]);
			}
			
			if(row[6]==null){
				dto.setIsAccount("");
			}else{
				dto.setIsAccount(row[6]);
			}
			
			if(row[7]==null){
				dto.setIsPrivateParty("");
			}else{
				dto.setIsPrivateParty(row[7]);
			}
			
			if(row[8]==null){
				dto.setIsCarrier("");
			}else{
				dto.setIsCarrier(row[8]);
			}
			
			if(row[9]==null){
				dto.setIsVendor("");
			}else{
				dto.setIsVendor(row[9]);
			}
			
			if(row[10]==null){
				dto.setIsOwnerOp("");
			}else{
				dto.setIsOwnerOp(row[10]);
			}
			
			if(row[11]==null){
				dto.setStatus("");
			}else{
				dto.setStatus(row[11]);
			}
			
			if(row[12] == null){
				dto.setCountryName("");
			}else{
				dto.setCountryName(row[12]);
			}
			
			if(row[13] == null){
				dto.setCityName("");
			}else{
				dto.setCityName(row[13]);
			}
			
			if(row[14] == null){
				dto.setStateName("");
			}else{
				dto.setStateName(row[14]);
			}
			
			if(row[15] == null){
				dto.setVanLineCode("");
			}else{
				dto.setVanLineCode(row[15]);
			}
			
			if(row[16] == null){
				dto.setAdd1("");
			}else{
				dto.setAdd1(row[16]);
			}
				
			if(row[17] == null){
				dto.setAdd2("");
			}else{
				dto.setAdd2(row[17]);
			}
			
			if(row[18] == null){
				dto.setZip("");
			}else{
				dto.setZip(row[18]);
			}
			if(row[19] == null){
				dto.setAgentClassification("");;
			}else{
				dto.setAgentClassification(row[19]);
			}
			
			partnerList.add(dto);
		}
		
		return partnerList;
	}

	public List getAgentCompanyDivisionVanline(String string, String corpID, String firstName, String lastName, String partnerCode, String countryCode, String country, String stateCode, String vanLineCODE,String aliasName, String companyDivision) {
		List <Object> partnerList = new ArrayList<Object>();
		String tempQuery = "";
		if(vanLineCODE != null && vanLineCODE.length()>0){
			tempQuery = "AND  pv.vanLineCode like '%" + vanLineCODE.replaceAll("'", "''") + "%' ";
		}
		/*String query = "select p.id, p.partnercode, if(lastname is null,'', lastname), if(firstName is null,'', firstName),if(aliasName is null,'', aliasName)," +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress1,billingAddress1) as Add1, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress2,billingAddress2) as Add2, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalZip,billingZip) as Zip, p.companydivision, pv.vanlinecode " +
					"FROM partner p " +
					"LEFT OUTER JOIN partnervanlineref pv on  p.partnerCode = pv.partnerCode " +
					"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND(aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or  lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND (lastName like '%" + lastName.replaceAll("'", "''") + "%' or  aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
					"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' "+tempQuery+
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
					"AND status = 'Approved' AND p.corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') AND p.companydivision in ( '','"+companyDivision+"') " +
					"and (isagent is true or isprivateparty is true or isaccount is true) limit 100 " +
					"union " +
					"select p.id, p.partnercode, if(lastname is null,'', lastname), if(firstName is null,'', firstName),if(aliasName is null,'', aliasName), " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress1,billingAddress1) as Add1, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress2,billingAddress2) as Add2, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalZip,billingZip) as Zip, r.companydivision, '' " +
					"from partner p, partneraccountref r " +
					"where p.partnercode = r.partnercode and r.corpid  in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and r.companydivision = '"+companyDivision+"' " +
					"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'" +
					"and p.status = 'Approved' and p.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')" +
					"AND p.firstName like '%" + firstName.replaceAll("'", "''") +"%' AND (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%')  AND (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%', billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
					"and (isagent is true or isprivateparty is true or isaccount is true) order by 2, 3 limit 100";
		
		*/
		String str = "select p.id, p.partnercode, if(lastname is null,'', lastname), if(firstName is null,'', firstName),if(aliasName is null,'', aliasName)," +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, " +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress1,billingAddress1) as Add1, " +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress2,billingAddress2) as Add2, " +
		" if((isAgent=true or isVendor=true or isCarrier=true),terminalZip,billingZip) as Zip, p.companydivision, pv.vanlinecode " +
		" from partner p "+
		" left outer join partneraccountref pr on p.partnerCode=pr.partnerCode and (pr.companyDivision='" + companyDivision + "') and pr.corpid='"+corpID+"' "+
		" left outer join partnervanlineref pv on p.partnerCode=pv.partnerCode and pv.corpID in ('TSFT','"+corpID+"') "+
		" where p.corpID in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and isAgent is true and status in ('New','Approved') "+
		" and (p.companyDivision='" + companyDivision	+ "' or p.companyDivision is null or p.companyDivision='') "+
		" and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or firstName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '" + lastName.replaceAll("'", "''") + "%') "+
        " and (p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%') and (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
        " and (terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%') and (terminalCountry like '%" + country.replaceAll("'", "''") + "%') "+
        " and (terminalState like '%" + stateCode.replaceAll("'", "''") + "%') "+
        " and (pv.vanlinecode like '%" + vanLineCODE + "%') group by p.partnercode limit 100";
		
		
		popupList = this.getSession().createSQLQuery(str).list();
		
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			
			dto.setId(row[0]);
			dto.setPartnerCode(row[1]);
			dto.setLastName(row[2]);
			
			if(row[3] == null){
				dto.setFirstName("");
			}else{
				dto.setFirstName(row[3]);
			}
			if(row[4] == null){
				dto.setAliasName("");
			}else{
				dto.setAliasName(row[4]);
			}
			
			if(row[5] == null){
				dto.setCountryName("");
			}else{
				dto.setCountryName(row[5]);
			}
			
			if(row[6] == null){
				dto.setCityName("");
			}else{
				dto.setCityName(row[6]);
			}
			
			if(row[7] == null){
				dto.setStateName("");
			}else{
				dto.setStateName(row[7]);
			}
			if(row[8] == null){
				dto.setAdd1("");
			}else{
				dto.setAdd1(row[8]);
			}
				
			if(row[9] == null){
				dto.setAdd2("");
			}else{
				dto.setAdd2(row[9]);
			}
			
			if(row[10] == null){
				dto.setZip("");
			}else{
				dto.setZip(row[10]);
			}
			
			if(row[11] == null){
				dto.setCompanyDiv("");
			}else{
				dto.setCompanyDiv(row[11]);
			}
			
			if(row[12] == null){
				dto.setVanLineCode("");
			}else{
				dto.setVanLineCode(row[12]);
			}
			
			partnerList.add(dto);
		}
		
		return partnerList;
	}
    public List findPartnerId(String partnerCode, String sessionCorpID) {
		return getHibernateTemplate().find("select id from Partner where partnerCode='"+partnerCode+"' ");
	}

    public List ratingListSixMonths(String partnerCode, String corpID) {
		return this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', op.lastname, Sum(if(oaevaluation=1,1,0)) as 'positive response', Sum(if(oaevaluation=-1,-1,0)) as 'negative response' , s.id from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 183 group by op.partnercode union select year(t.deliveryA) as 'Year', dp.lastname, Sum(if(daevaluation=1,1,0)) as 'positive response', Sum(if(daevaluation=-1,-1,0)) as 'negative response' , s.id from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 183 group by dp.partnercode order by 1").list();
	}
	
	public List ratingListOneYr(String partnerCode, String corpID) {
		return this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', op.lastname, Sum(if(oaevaluation=1,1,0)) as 'positive response', Sum(if(oaevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 365 group by op.partnercode union select year(t.deliveryA) as 'Year', dp.lastname, Sum(if(daevaluation=1,1,0)) as 'positive response', Sum(if(daevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 365 group by dp.partnercode order by 1").list();
	}

	public List ratingListTwoYr(String partnerCode, String corpID) {
		return this.getSession().createSQLQuery("select year(t.deliveryA) as 'Year', op.lastname, Sum(if(oaevaluation=1,1,0)) as 'positive response', Sum(if(oaevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and op.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 730 group by op.partnercode union select year(t.deliveryA) as 'Year', dp.lastname, Sum(if(daevaluation=1,1,0)) as 'positive response', Sum(if(daevaluation=-1,-1,0)) as 'negative response' from serviceorder s inner join trackingstatus t on s.id = t.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') and dp.partnercode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), t.deliverya) <= 730 group by dp.partnercode order by 1").list();
	}
	
	
	public List accountRatingList(String partnerCode, String corpID, String days) {
		return this.getSession().createSQLQuery("select sum(if(clientOverallResponse='Y',1,0)),sum(if(clientOverallResponse='N',-1,0)) from serviceorder s inner join billing b on s.id = b.id and b.billtocode =  '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), b.revenueRecognition) <= "+days+" group by b.billtocode order by 1").list();
	}
	
	public List getAccountFeedbackList(String partnerCode, String year, String corpID) {
		List <Object> ratingFeedbackList = new ArrayList<Object>();
		List feedbackList = this.getSession().createSQLQuery("select year(t.deliveryA), if(clientOverallResponse='Y','+1','-1'), s.feedback, s.shipNumber from serviceorder s inner join trackingstatus t on s.id = t.id inner join billing b on b.id = s.id and b.billtocode = '"+partnerCode+"' and clientoverallresponse in ( 'Y','N') and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and datediff(now(), b.revenueRecognition) <= '"+year+"'").list();
		HibernateDTO partnerFeedback;
		Iterator it=feedbackList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerFeedback=new HibernateDTO();
			partnerFeedback.setYear(obj[0]);
			partnerFeedback.setOaeValuation(obj[1]);
			partnerFeedback.setFeedback(obj[2]);
			partnerFeedback.setShipNumber(obj[3]);
			
			ratingFeedbackList.add(partnerFeedback);
		}
		
		return ratingFeedbackList;
	}

	public List getPartnerAccountActivityList(String partnerCode, String corpID) {
		List <Object> activitySSCWList = new ArrayList<Object>();
		String queryString = "select year(b.revenuerecognition) as 'Year', '0' as 'Count','0' as 'Weight lbs',round(sum((a.actualRevenue)/1000),0) as 'Rev K' from billing b inner join serviceorder s on s.id = b.id inner join accountline a on s.id = a.serviceorderid where s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' and a.status is true and a.billToCode = '"+partnerCode+"' and (a.recInvoiceNumber != '' or a.recInvoiceNumber is not null) group by year(b.revenuerecognition), a.billToCode union select year(b.revenuerecognition) as 'Year', count(s.shipnumber) as 'Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) as 'Weight lbs', '0' from billing b inner join miscellaneous m on b.id = m.id inner join serviceorder s on s.id = b.id where s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' and b.billToCode = '"+partnerCode+"' group by year(b.revenuerecognition), b.billToCode ";
		//List partnerSSCWActivityList = this.getSession().createSQLQuery("select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'OA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id inner join partner op on op.partnercode = t.originagentcode and op.corpid in ('TSFT', '"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and op.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' group by year(b.revenuerecognition), op.partnercode union select year(b.revenuerecognition) as 'Year', count(s.shipnumber) 'DA Count', round(sum(if(mode='Air',m.actualgrossweight,m.actualnetweight)),0) 'Weight lbs' from serviceorder s inner join trackingstatus t on s.id = t.id  inner join miscellaneous m on s.id = m.id inner join billing b on s.id = b.id inner join partner dp on dp.partnercode = t.destinationagentcode and dp.corpid in ('TSFT', '"+corpID+"') inner join companydivision c on s.bookingagentcode = c.bookingagentcode and c.corpid = '"+corpID+"' and dp.partnercode = '"+partnerCode+"' and s.status not in ('CNCL','DWNLD') and s.corpid = '"+corpID+"' and b.revenuerecognition >= '2007-01-01' group by year(b.revenuerecognition),dp.partnercode order by 1").list();
		List partnerSSCWActivityList = this.getSession().createSQLQuery(queryString).list();
		HibernateDTO partnerSSCWActivity;
		Iterator it=partnerSSCWActivityList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerSSCWActivity=new HibernateDTO();
			partnerSSCWActivity.setYear(obj[0]);
			partnerSSCWActivity.setOaCount(obj[1]);
			partnerSSCWActivity.setActualWeight(obj[2]);
			partnerSSCWActivity.setActualRevenue(obj[3]);
			
			activitySSCWList.add(partnerSSCWActivity);
		}
		return activitySSCWList;
	
	}

	public List findByDestinationRelo(String destination, String jobRelo,String jobReloName,String sessionCorpID) {
		List destinationRelo=new ArrayList();
		String flag="";
		if(!jobReloName.equalsIgnoreCase("")){
			List al=getHibernateTemplate().find("from PartnerPublic where reloServices like '%"+jobRelo+"%' and typeOfVendor like '%"+jobReloName+"%' and status='Approved' AND billingCountryCode=?", destination);
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				flag="found";
			}
		}
		List rloPopupList=new ArrayList();
		if(!flag.equalsIgnoreCase("")){
			//getHibernateTemplate().setMaxResults(100);
			//return getHibernateTemplate().find("from PartnerPublic where reloServices like '%"+jobRelo+"%' and typeOfVendor like '%"+jobReloName+"%' and status='Approved' AND billingCountryCode=?", destination); 
			String query1="select p1.partnercode,p1.firstName,p1.lastName,p1.terminalAddress1,p1.terminalAddress2,p1.terminalCity,p1.terminalZip,p1.terminalState,p1.terminalCountry,p1.terminalCountryCode,p1.status,p2.NPSscore from partnerpublic p1 , partnerprivate p2  where p1.reloServices like '%"+jobRelo+"%' and p1.typeOfVendor like '%"+jobReloName+"%' and p1.status='Approved' AND p1.billingCountryCode='"+destination+"' and p1.partnercode=p2.partnercode and p2.corpID  IN ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') limit 100 ";
			rloPopupList = this.getSession().createSQLQuery(query1).list();
		}else{
		//getHibernateTemplate().setMaxResults(100);
  	    //return getHibernateTemplate().find("from PartnerPublic where isAgent is true and reloServices like '%"+jobRelo+"%' and status='Approved' AND billingCountryCode=?", destination);
		String query2="select p1.partnercode,p1.firstName,p1.lastName,p1.terminalAddress1,p1.terminalAddress2,p1.terminalCity,p1.terminalZip,p1.terminalState,p1.terminalCountry,p1.terminalCountryCode,p1.status,p2.NPSscore from partnerpublic p1 , partnerprivate p2  where p1.isAgent is true and  p1.reloServices like '%"+jobRelo+"%'  and p1.status='Approved' AND p1.billingCountryCode='"+destination+"' and p1.partnercode=p2.partnercode and p2.corpID  IN ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') limit 100 ";
		rloPopupList = this.getSession().createSQLQuery(query2).list();
		}
		Iterator it = rloPopupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			dto.setPartnerCode(row[0]);
			dto.setFirstName(row[1]);
			dto.setLastName(row[2]);
			dto.setTerminalAddress1(row[3]);
			dto.setTerminalAddress2(row[4]);
			dto.setTerminalCity(row[5]);
			dto.setTerminalZip(row[6]);
			dto.setTerminalState(row[7]);
			dto.setTerminalCountry(row[8]);
			dto.setTerminalCountryCode(row[9]);
			dto.setStatus(row[10]);
			dto.setNPSscore(row[11]);
			destinationRelo.add(dto);
		}
		return destinationRelo;
	}

	public List findPartnerRelo(String lastName, String partnerCode, String billingCountryCode, String billingState, String billingCountry, String jobRelo,String jobReloName,String searchOfVendorCode,String sessionCorpID,boolean cmmdmmflag) {
		List destinationRelo=new ArrayList();
		String flag="";
		String query2="";
		String query1="";
		if(!jobReloName.equalsIgnoreCase("")){
			List al=getHibernateTemplate().find("from PartnerPublic where  reloServices like '%"+jobRelo+"%' and typeOfVendor like '%"+jobReloName+"%' and status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
					+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND terminalCountryCode like '"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '" + billingCountry.replaceAll("'", "''")
					+ "%' AND terminalState like '" + billingState.replaceAll("'", "''")
					+ "%' AND typeOfVendor like '" + searchOfVendorCode + "%'  ");
			
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				flag="found";
			}
		}	
		List rloPopupList=new ArrayList();
		if(!flag.equalsIgnoreCase("")){
			query1="select p1.partnercode,p1.firstName,p1.lastName,p1.terminalAddress1,p1.terminalAddress2,p1.terminalCity,p1.terminalZip,p1.terminalState,p1.terminalCountry,p1.terminalCountryCode,p1.status,p2.NPSscore from partnerpublic p1 , partnerprivate p2  where p1.reloServices like '%"+jobRelo+"%' and p1.typeOfVendor like '%"+jobReloName+"%' and p1.status='Approved' " +
					" AND p1.terminalCountryCode like '"+ billingCountryCode.replaceAll("'", "''") + "%' and ( p1.lastName like '"+ lastName.replaceAll("'", "''") + "%' OR  p1.firstName like '"+ lastName.replaceAll("'", "''") + "%') AND p1.partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND p1.terminalCountry like '" + billingCountry.replaceAll("'", "''")+ "%' AND p1.terminalState like '" + billingState.replaceAll("'", "''") + "%' " +
					" and p1.partnercode=p2.partnercode and p2.corpID  IN ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND p1.typeOfVendor like '" + searchOfVendorCode + "%' limit 100 ";
			rloPopupList = this.getSession().createSQLQuery(query1).list();
	     /*	return getHibernateTemplate().find("from PartnerPublic where  reloServices like '%"+jobRelo+"%' and typeOfVendor like '%"+jobReloName+"%' and status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
				+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND terminalCountryCode like '"
				+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '" + billingCountry.replaceAll("'", "''")
				+ "%' AND terminalState like '" + billingState.replaceAll("'", "''") + "%'  ");*/
		}else{
			query2="select p1.partnercode,p1.firstName,p1.lastName,p1.terminalAddress1,p1.terminalAddress2,p1.terminalCity,p1.terminalZip,p1.terminalState,p1.terminalCountry,p1.terminalCountryCode,p1.status,p2.NPSscore from partnerpublic p1 , partnerprivate p2  " +
					" where p1.reloServices like '%"+jobRelo+"%'  and p1.status='Approved' AND terminalCountry like '" + billingCountry.replaceAll("'", "''")+ "%' " +
					" and (p1.lastName like '" + lastName.replaceAll("'", "''") + "%' OR  p1.firstName like '"+ lastName.replaceAll("'", "''") + "%') AND p1.terminalState like '" + billingState.replaceAll("'", "''") + "%' AND p1.partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND p1.terminalCountryCode like '"+ billingCountryCode.replaceAll("'", "''") + "%' and p1.partnercode=p2.partnercode " +
					" and p2.corpID  IN ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND p1.typeOfVendor like '" + searchOfVendorCode + "%' limit 100 ";
			rloPopupList = this.getSession().createSQLQuery(query2).list();
		/*return getHibernateTemplate().find("from PartnerPublic where  reloServices like '%"+jobRelo+"%' and status='Approved' and (lastName like '" + lastName.replaceAll("'", "''") + "%' OR  firstName like '"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND terminalCountryCode like '"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '" + billingState.replaceAll("'", "''") + "%'  ");*/
		}
		Iterator it = rloPopupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			dto.setPartnerCode(row[0]);
			dto.setFirstName(row[1]);
			dto.setLastName(row[2]);
			dto.setTerminalAddress1(row[3]);
			dto.setTerminalAddress2(row[4]);
			dto.setTerminalCity(row[5]);
			dto.setTerminalZip(row[6]);
			dto.setTerminalState(row[7]);
			dto.setTerminalCountry(row[8]);
			dto.setTerminalCountryCode(row[9]);
			dto.setStatus(row[10]);
			dto.setNPSscore(row[11]);
			destinationRelo.add(dto);
		}
		return destinationRelo;
		
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public List<Partner> findByNetwork() {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where isAgent is true and status='Approved'   and  networkGroup=true ");
	}

	public List vendorNameForUniqueActgCode(String partnerCode, String sessionCorpID, Boolean cmmDmmFlag) { 
		getHibernateTemplate().setMaxResults(100);
		return this.getSession().createSQLQuery("select p1.lastName,if(pa.accountCrossReference is null,'' ,pa.accountCrossReference), p1.status,if(p1.isOwnerOp is true, if(p2.commission is null or p2.commission ='','Commission',p2.commission),'NoCommission'),if(p1.billingCurrency is null,' ',p1.billingCurrency),if(p1.isAgent is true,'T','F'),if(p1.isAccount is true,'T','F') from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.refType ='P' and pa.corpID IN ('"+sessionCorpID+"') where binary p1.partnerCode= '" + partnerCode + "' and p1.partnercode = p2.partnercode and p1.id = p2.partnerPublicId and p1.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') limit 1 ").list();
	}
	public List getAllDriverLocation(String driverTypeValue,String corpID){
		String queryString = "";
		if(driverTypeValue!=null && !driverTypeValue.equals("")){
			queryString = "select partnerCode, concat(if((firstName is null ),'',firstName),' ',if((lastName is null),'',lastName)) as name, driverAgency, validNationalCode,vanLastLocation,concat(if((vanLastReportOn is null ),'',DATE_FORMAT(vanLastReportOn,'%Y-%m-%d')),' ',if((vanLastReportTime is null ),'',vanLastReportTime)) as reportTime, vanAvailCube,currentVanID from partner where validNationalCode <> '' and isOwnerOp is true and status = 'Approved' and corpid='"+corpID+"'  and noDispatch is not true and driverType='"+driverTypeValue+"' order by name ";
		}else{
			queryString = "select partnerCode, concat(if((firstName is null ),'',firstName),' ',if((lastName is null),'',lastName)) as name, driverAgency, validNationalCode,vanLastLocation,concat(if((vanLastReportOn is null ),'',DATE_FORMAT(vanLastReportOn,'%Y-%m-%d')),' ',if((vanLastReportTime is null ),'',vanLastReportTime)) as reportTime, vanAvailCube,currentVanID from partner where validNationalCode <> '' and isOwnerOp is true and status = 'Approved' and corpid='"+corpID+"' and noDispatch is not true order by name ";	
		}		
		List driverLocation = this.getSession().createSQLQuery(queryString).list();
		return driverLocation;
	}
	
	public  class DriverInvolvementListDTO{
		private Object driverId;
		private Object regNo;
		private Object location;
		private Object loadType;
		private Object keyDate;
		private Object name;
		private Object estWt;
		private Object estCube;
		private Object shipNumber;
		private Object listDate;
		private Object address;
		private Object actWt;
		private Object actCube;
		private Object cumVol;
		private Object job;
		private Object vanLineNationalCode;
		private Object selfHaul;
		private Object estimatedRevenue;
		private Object driverCell;
		private Object mile;
		private Object id;
		private Object tripNumber;
		
		public Object getDriverId() {
			return driverId;
		}
		public void setDriverId(Object driverId) {
			this.driverId = driverId;
		}
		public Object getEstCube() {
			return estCube;
		}
		public void setEstCube(Object estCube) {
			this.estCube = estCube;
		}
		public Object getEstWt() {
			return estWt;
		}
		public void setEstWt(Object estWt) {
			this.estWt = estWt;
		}
		public Object getKeyDate() {
			return keyDate;
		}
		public void setKeyDate(Object keyDate) {
			this.keyDate = keyDate;
		}
		public Object getLoadType() {
			return loadType;
		}
		public void setLoadType(Object loadType) {
			this.loadType = loadType;
		}
		public Object getLocation() {
			return location;
		}
		public void setLocation(Object location) {
			this.location = location;
		}
		public Object getRegNo() {
			return regNo;
		}
		public void setRegNo(Object regNo) {
			this.regNo = regNo;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getListDate() {
			return listDate;
		}
		public void setListDate(Object listDate) {
			this.listDate = listDate;
		}
		public Object getAddress() {
			return address;
		}
		public void setAddress(Object address) {
			this.address = address;
		}
		public Object getActCube() {
			return actCube;
		}
		public void setActCube(Object actCube) {
			this.actCube = actCube;
		}
		public Object getActWt() {
			return actWt;
		}
		public void setActWt(Object actWt) {
			this.actWt = actWt;
		}
		public Object getName() {
			return name;
		}
		public void setName(Object name) {
			this.name = name;
		}
		public Object getCumVol() {
			return cumVol;
		}
		public void setCumVol(Object cumVol) {
			this.cumVol = cumVol;
		}
		public Object getDriverCell() {
			return driverCell;
		}
		public void setDriverCell(Object driverCell) {
			this.driverCell = driverCell;
		}
		public Object getEstimatedRevenue() {
			return estimatedRevenue;
		}
		public void setEstimatedRevenue(Object estimatedRevenue) {
			this.estimatedRevenue = estimatedRevenue;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getSelfHaul() {
			return selfHaul;
		}
		public void setSelfHaul(Object selfHaul) {
			this.selfHaul = selfHaul;
		}
		public Object getVanLineNationalCode() {
			return vanLineNationalCode;
		}
		public void setVanLineNationalCode(Object vanLineNationalCode) {
			this.vanLineNationalCode = vanLineNationalCode;
		}
		public Object getMile() {
			return mile;
		}
		public void setMile(Object mile) {
			this.mile = mile;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getTripNumber() {
			return tripNumber;
		}
		public void setTripNumber(Object tripNumber) {
			this.tripNumber = tripNumber;
		}
	}
	
	public List getAllInvolvementOfDriver(String driverId,String sessionCorpID){		
		List driverInvolvementDtoList = new ArrayList();
		String tempDate="";
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
		tempDate = nowYYYYMMDD1.toString();
		String queryString = "select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'LD' as loadType , " +
				"if(beginload is not null,date_format(beginload,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
				"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber,beginload as listDate, " +
				"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',',if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address, " +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , " +
				"m.driverCell as driverCell , m.mile as mile, s.id,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
				"where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and driverid like  '"+driverId+"%' " +
				"and s.status not in ('CNCL','DWNLD','CLSD') " +
				"and (beginpacking >= '"+tempDate+"' or beginload >= '"+tempDate+"' or if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"') " +
				"union " +
				"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'DL' as loadType  , " +
				"date_format(if(deliveryta is not null, deliveryta, deliveryshipper),'%e %b, %Y') as keyDate,concat(s.firstName,' ', s.lastName) as name , " +
				"m.estimatednetweight as estWt,m.estimatecubicfeet as estCube, s.shipNumber as shipNumber , " +
				"if(deliveryta is not null,deliveryta,deliveryshipper) as listDate , " +
				"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address ," +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile, s.id,m.tripNumber as tripNumber " +
				"from serviceorder s, miscellaneous m , trackingstatus t where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and driverid like  '"+driverId+"%'" +
				"and s.status not in ('CNCL','DWNLD','CLSD') and (beginpacking >= '"+tempDate+"' or beginload >= '"+tempDate+"' or if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"') " +
				"union " +
				"select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'CPK' as loadType , " +
				"if(beginpacking is not null,date_format(beginpacking,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
				"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber, beginpacking as listDate , " +
				"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',',if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address , " +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul , " +
				"m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile, s.id,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
				"where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and packingdriverid like  '"+driverId+"%' " +
				"and s.status not in ('CNCL','DWNLD','CLSD') and beginpacking >= '"+tempDate+"' " +
				"union " +
				"select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'CLD' as loadType , " +
				"if(beginload is not null,date_format(beginload,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as EstWt , " +
				"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber, beginload as listDate , " +
				"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',', if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address , " +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell  , " +
				"m.mile as mile, s.id,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t  where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"' " +
				"and loadingdriverid like '"+driverId+"%' and s.status not in ('CNCL','DWNLD','CLSD')  and beginload >= '"+tempDate+"'  " +
				"union " +
				"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'CDL' as loadType , " +
				"if(deliveryta is not null, date_format(deliveryta,'%e %b, %Y') , if(deliveryshipper is not null , date_format(deliveryshipper,'%e %b, %Y'),'')) as keyDate, " +
				"concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt,m.estimatecubicfeet  as estCube, s.shipNumber as shipNumber , " +
				"if(deliveryta is not null,deliveryta, deliveryshipper) as listDate , " +
				"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address , " +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul , " +
				"m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile, s.id,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
				"where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"'  and deliverydriverid like '"+driverId+"%' " +
				"and s.status not in ('CNCL','DWNLD','CLSD') and  if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"' " +
				"union " +
				"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'SIT' as loadType , " +
				"if(sitDestinationA is not null,date_format(sitDestinationA,'%e %b, %Y'),'') as keyDate, concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
				"m.estimatecubicfeet  as estCube, s.shipNumber as shipNumber ,  " +
				"sitDestinationA as listDate , " +
				"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address , " +
				"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul,  m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell  , " +
				"m.mile as mile, s.id,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
				"where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"' and sitdestinationdriverid like '"+driverId+"%'  " +
				"and s.status not in ('CNCL','DWNLD','CLSD') and  sitDestinationA >= '"+tempDate+"' " +
				"UNION " +
				"select personid,'','','EV' as loadType,date_format(surveydate,'%e %b, %Y') as keyDate, activityname , " +
				"'','','', surveydate,'','','','','','','','','','','' " +
				"from calendarfile where category = 'Driver' " +
				"and personid like '"+driverId+"%' and surveydate >= '"+tempDate+"' " +
				"order by 10 " ;
		List driverLocation = this.getSession().createSQLQuery(queryString)
		.addScalar("driverId", Hibernate.STRING)
		.addScalar("regNo", Hibernate.STRING)
		.addScalar("location", Hibernate.STRING)
		.addScalar("loadType", Hibernate.STRING)
		.addScalar("keyDate", Hibernate.STRING)
		.addScalar("name", Hibernate.STRING)
		.addScalar("estWt", Hibernate.STRING)
		.addScalar("estCube", Hibernate.STRING)
		.addScalar("shipNumber", Hibernate.STRING)
		.addScalar("listDate", Hibernate.DATE)
		.addScalar("address", Hibernate.STRING)
		.addScalar("actWt", Hibernate.STRING)
		.addScalar("actCube", Hibernate.STRING)
		.addScalar("job", Hibernate.STRING)
		.addScalar("vanLineNationalCode", Hibernate.STRING)
		.addScalar("selfHaul", Hibernate.STRING)
		.addScalar("estimatedRevenue", Hibernate.STRING)
		.addScalar("driverCell", Hibernate.STRING)
		.addScalar("mile", Hibernate.STRING).addScalar("id", Hibernate.STRING).addScalar("tripNumber", Hibernate.STRING).list();
		Iterator it1=driverLocation.iterator();
		DriverInvolvementListDTO driverInvovementListDto=null;
		int tempLdDl=0;
		while(it1.hasNext()){
			Object [] row = (Object[])it1.next();
			driverInvovementListDto = new DriverInvolvementListDTO();
			if(row[0] == null){
				driverInvovementListDto.setDriverId("");
			}else{
				driverInvovementListDto.setDriverId(row[0]);
			}
			if(row[1] == null){
				driverInvovementListDto.setRegNo("");
			}else{
				driverInvovementListDto.setRegNo(row[1]);
			}
			if(row[2] == null){
				driverInvovementListDto.setLocation("");
			}else{
				driverInvovementListDto.setLocation(row[2]);
			}
			if(row[3] == null){
				driverInvovementListDto.setLoadType("");
			}else{
				driverInvovementListDto.setLoadType(row[3]);
			}
			if(row[4] == null){
				driverInvovementListDto.setKeyDate("");
			}else{
				driverInvovementListDto.setKeyDate(row[4]);
			}
			if(row[5] == null){
				driverInvovementListDto.setName("");
			}else{
				driverInvovementListDto.setName(row[5]);
			}
			if(row[6] != null && (!(row[6].equals("")))){			
				driverInvovementListDto.setEstWt(row[6]);
				driverInvovementListDto.setEstWt(row[6].toString().substring(0,row[6].toString().indexOf(".")));
			}else{
				driverInvovementListDto.setEstWt("");
				}
			if(row[7] != null && (!row[7].equals(""))){
				driverInvovementListDto.setEstCube(row[7]);
				driverInvovementListDto.setEstCube(row[7].toString().substring(0,row[7].toString().indexOf(".")));
			}else{
				driverInvovementListDto.setEstCube("");			
			}
			if(row[8] == null){
				driverInvovementListDto.setShipNumber("");
			}else{
				driverInvovementListDto.setShipNumber(row[8]);
			}
			
			driverInvovementListDto.setListDate(row[9]);
			
			if(row[10] == null){
				driverInvovementListDto.setAddress("");
			}else{
				driverInvovementListDto.setAddress(row[10]);
			}
			if(row[11] != null && (!row[11].equals(""))){
				driverInvovementListDto.setActWt(row[11]);
				driverInvovementListDto.setActWt(row[11].toString().substring(0,row[11].toString().indexOf(".")));
							}else{
								driverInvovementListDto.setActWt("");
			}
			if(row[12] != null && (!row[12].equals(""))){
				driverInvovementListDto.setActCube(row[12].toString().substring(0,row[12].toString().indexOf(".")));
			}else{
				driverInvovementListDto.setActCube("");
			}
			if(!driverInvovementListDto.getActCube().toString().equalsIgnoreCase(""))
			{
				if(driverInvovementListDto.getLoadType().toString().equalsIgnoreCase("LD"))
				{
				tempLdDl=tempLdDl+Integer.parseInt(driverInvovementListDto.getActCube().toString());
				driverInvovementListDto.setCumVol(tempLdDl);
				}
				else if(driverInvovementListDto.getLoadType().toString().equalsIgnoreCase("DL"))
				{
				tempLdDl=tempLdDl-Integer.parseInt(driverInvovementListDto.getActCube().toString());
				driverInvovementListDto.setCumVol(tempLdDl);
				}else{
					driverInvovementListDto.setCumVol("");
				}
				
			}else{
				if(!driverInvovementListDto.getEstCube().toString().equalsIgnoreCase(""))
				{
					if(driverInvovementListDto.getLoadType().toString().equalsIgnoreCase("LD"))
					{
					tempLdDl=tempLdDl+Integer.parseInt(driverInvovementListDto.getEstCube().toString());
					driverInvovementListDto.setCumVol(tempLdDl);
					}
					else if(driverInvovementListDto.getLoadType().toString().equalsIgnoreCase("DL"))
					{
					tempLdDl=tempLdDl-Integer.parseInt(driverInvovementListDto.getEstCube().toString());
					driverInvovementListDto.setCumVol(tempLdDl);
					}else{
						driverInvovementListDto.setCumVol("");
					}
				}else{
					if(tempLdDl>0)
					{
					driverInvovementListDto.setCumVol(tempLdDl);
					}
					else
					{
					driverInvovementListDto.setCumVol("");
					}
				}
				
			}
			if(row[13] == null){
				driverInvovementListDto.setJob("");
			}else{
				driverInvovementListDto.setJob(row[13].toString());
			}			
			if(row[14] == null){
				driverInvovementListDto.setVanLineNationalCode("");
			}else{
				driverInvovementListDto.setVanLineNationalCode(row[14].toString());
			}
			if(row[15] == null){
				driverInvovementListDto.setSelfHaul("");
			}else{
				driverInvovementListDto.setSelfHaul(row[15].toString());
			}
			if(row[16] != null && (!row[16].equals(""))){
				Double num=Double.parseDouble(row[16].toString());
				driverInvovementListDto.setEstimatedRevenue(Math.round(num));
			}else{
				driverInvovementListDto.setEstimatedRevenue("");
			}
			if(row[17] == null){
				driverInvovementListDto.setDriverCell("");
				}else{
				driverInvovementListDto.setDriverCell(row[17].toString());
				}
			if(row[18] != null && (!row[18].equals(""))){				
				Double num=Double.parseDouble(row[18].toString());
				driverInvovementListDto.setMile(Math.round(num));
				}else{
					driverInvovementListDto.setMile("");
			}
			if(row[19] != null && (!row[19].equals(""))){				
				Long num=Long.parseLong(row[19].toString());
				driverInvovementListDto.setId(num);
				}
			driverInvovementListDto.setTripNumber(row[20]);
			driverInvolvementDtoList.add(driverInvovementListDto);
		}

		return driverInvolvementDtoList;
		
	}
	public String getDriverHomeAddress(String driverId){
		String driverHomeAddress="";
		List dList= this.getSession().createSQLQuery("select concat(if(billingAddress1 is null,'',billingaddress1),',',billingCity,',', if(billingState is null,'',billingState),',',billingCountry,'-',billingZip) as billingAddress from partnerpublic  where partnercode='"+driverId+"' and isOwnerOp is true").list();
		if((dList!=null) && (!dList.isEmpty()))
		{
			driverHomeAddress=dList.get(0).toString();
		}
		return driverHomeAddress;
	}
	public List getDriverLocationList(String corpID){
		List <Object> driverLocationList = new ArrayList<Object>();
		String queryString = "select partnerCode, concat(if((firstName is null ),'',firstName),' ',if((lastName is null),'',lastName)) as name, driverAgency, validNationalCode,vanLastLocation,concat(if((vanLastReportOn is null ),'',DATE_FORMAT(vanLastReportOn,'%Y-%m-%d')),' ',if((vanLastReportTime is null ),'',vanLastReportTime)) as reportTime, vanAvailCube,nextVanLocation,nextReportOn,currentVanAgency,currentVanID,currentTractorAgency,currentTractorID from partner where validNationalCode <> '' and isOwnerOp is true and status = 'Approved' and corpid='"+corpID+"' and noDispatch is not true order by driverAgency, validNationalCode";
		List driverLocation = this.getSession().createSQLQuery(queryString).list();
		DriverLocationListDTO driverLocationDto=null;
		Iterator it=driverLocation.iterator();
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			driverLocationDto = new DriverLocationListDTO();
			if(row[0] == null){
				driverLocationDto.setPartnerCode("");
			}else{
				driverLocationDto.setPartnerCode(row[0]);
			}
			if(row[1] == null){
				driverLocationDto.setName("");
			}else{
				driverLocationDto.setName(row[1]);
			}
			if(row[2] == null){
				driverLocationDto.setDriverAgency("");
			}else{
				driverLocationDto.setDriverAgency(row[2]);
			}
			if(row[3] == null){
				driverLocationDto.setValidNationalCode("");
			}else{
				driverLocationDto.setValidNationalCode(row[3]);
			}
			if(row[4] == null){
				driverLocationDto.setVanLastLocation("");
			}else{
				driverLocationDto.setVanLastLocation(row[4]);
			}
			if(row[5] == null){
				driverLocationDto.setReportTime("");
			}else{
				driverLocationDto.setReportTime(row[5]);
			}
			if(row[6] == null){
				driverLocationDto.setVanAvailCube("");
			}else{
				driverLocationDto.setVanAvailCube(row[6].toString());
			}
			if(row[7] == null){
				driverLocationDto.setNextVanLocation("");
			}else{
				driverLocationDto.setNextVanLocation(row[7]);
			}
			if(row[8] == null){
				driverLocationDto.setNextReportOn("");
			}else{
				driverLocationDto.setNextReportOn(row[8]);
			}
			if(row[9] == null){
				driverLocationDto.setCurrentVanAgency("");
			}else{
				driverLocationDto.setCurrentVanAgency(row[9]);
			}if(row[10] == null){
				driverLocationDto.setCurrentVanID("");
			}else{
				driverLocationDto.setCurrentVanID(row[10]);
			}if(row[11] == null){
				driverLocationDto.setCurrentTractorAgency("");
			}else{
				driverLocationDto.setCurrentTractorAgency(row[11]);
			}if(row[12] == null){
				driverLocationDto.setCurrentTractorID("");
			}else{
				driverLocationDto.setCurrentTractorID(row[12]);
			}
			driverLocationList.add(driverLocationDto);
		}
		return driverLocationList;
	}
	public  class DriverLocationListDTO{
		private Object partnerCode;
		private Object name;
		private Object driverAgency;
		private Object validNationalCode;
		private Object vanLastLocation;
		private Object reportTime;
		private Object vanAvailCube;
	    private Object nextVanLocation;
		private Object nextReportOn;
		private Object currentVanAgency;
		private Object currentVanID;
		private Object currentTractorAgency;
		private Object currentTractorID;
		public Object getDriverAgency() {
			return driverAgency;
		}
		public void setDriverAgency(Object driverAgency) {
			this.driverAgency = driverAgency;
		}
		public Object getName() {
			return name;
		}
		public void setName(Object name) {
			this.name = name;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		public Object getReportTime() {
			return reportTime;
		}
		public void setReportTime(Object reportTime) {
			this.reportTime = reportTime;
		}
		public Object getValidNationalCode() {
			return validNationalCode;
		}
		public void setValidNationalCode(Object validNationalCode) {
			this.validNationalCode = validNationalCode;
		}
		public Object getVanAvailCube() {
			return vanAvailCube;
		}
		public void setVanAvailCube(Object vanAvailCube) {
			this.vanAvailCube = vanAvailCube;
		}
		public Object getVanLastLocation() {
			return vanLastLocation;
		}
		public void setVanLastLocation(Object vanLastLocation) {
			this.vanLastLocation = vanLastLocation;
		}
		public Object getNextReportOn() {
			return nextReportOn;
		}
		public void setNextReportOn(Object nextReportOn) {
			this.nextReportOn = nextReportOn;
		}
		public Object getNextVanLocation() {
			return nextVanLocation;
		}
		public void setNextVanLocation(Object nextVanLocation) {
			this.nextVanLocation = nextVanLocation;
		}
		public Object getCurrentTractorAgency() {
			return currentTractorAgency;
		}
		public void setCurrentTractorAgency(Object currentTractorAgency) {
			this.currentTractorAgency = currentTractorAgency;
		}
		public Object getCurrentTractorID() {
			return currentTractorID;
		}
		public void setCurrentTractorID(Object currentTractorID) {
			this.currentTractorID = currentTractorID;
		}
		public Object getCurrentVanAgency() {
			return currentVanAgency;
		}
		public void setCurrentVanAgency(Object currentVanAgency) {
			this.currentVanAgency = currentVanAgency;
		}
		public Object getCurrentVanID() {
			return currentVanID;
		}
		public void setCurrentVanID(Object currentVanID) {
			this.currentVanID = currentVanID;
		}
	}
	public List partnerSAPExtract(String billToCode, String corpID) {
		//getHibernateTemplate().setMaxResults(100);
		getHibernateTemplate().setMaxResults(50000);
		return getHibernateTemplate().find("from Partner where partnerCode in(" + billToCode + ") and  (isPrivateParty=true or isAgent=true or isAccount=true) ");
	}
	public List partnerSAPExtractForUGCN(String billToCode, String corpID) {
		//getHibernateTemplate().setMaxResults(100);
		getHibernateTemplate().setMaxResults(50000);
		return getHibernateTemplate().find("from Partner where partnerCode in(" + billToCode + ") and  (isPrivateParty=true or isAgent=true or isAccount=true) ");
	}
	public List financeSAPPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID) {
		getHibernateTemplate().setMaxResults(50000);
		if (companyDivision.equals("") || companyDivision == null) {
			return getHibernateTemplate().find(
					"from Partner where (DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "'))  and  (isPrivateParty=true or isAgent=true or isAccount=true)");
		} else {
			return getHibernateTemplate().find(
					"from Partner where (DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "'))  and  (isPrivateParty=true or isAgent=true or isAccount=true) and companyDivision = '" + companyDivision + "'");
		}
	}
	
public List synSAPInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID) {
		
		List <Object> partnerList = new ArrayList<Object>();
        List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p1.mailingAddress1,p1.mailingAddress2,p1.mailingCity,p1.mailingCountryCode,p1.mailingState,p1.mailingZip,p1.mailingPhone,p1.mailingFax,p1.billingAddress1,p1.billingAddress2,p1.billingCity,p1.billingCountryCode,p1.billingState,p1.billingZip,p1.billingPhone,p1.billingFax,p2.coordinator,p1.firstName ,p2.vatNumber,p2.classcode,p2.accountManager,p2.accountHolder,p1.billingEmail from partnerpublic p1, partnerprivate p2, accountline a, serviceorder s where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and s.corpid = '"+corpID+"' and (p1.createdon >= '"+sysInvoiceDates+"' or p1.updatedon >= '"+sysInvoiceDates+"') and a.serviceorderid = s.id and a.billtocode = p1.partnercode and p1.id=p2.partnerPublicId and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) and a.recaccdate is not null and s.companydivision = '"+companyDivision+"'").list();
		DTOSynInvoicePartner synInvoicePartnerDTO;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			synInvoicePartnerDTO=new DTOSynInvoicePartner();
			synInvoicePartnerDTO.setPartnerCode(obj[0]);
			synInvoicePartnerDTO.setLastName(obj[1]);
			synInvoicePartnerDTO.setPartnerPrefix(obj[2]);
			synInvoicePartnerDTO.setMiddleInitial(obj[3]);
			synInvoicePartnerDTO.setMailingAddress1(obj[4]);
			synInvoicePartnerDTO.setMailingAddress2(obj[5]);
			synInvoicePartnerDTO.setMailingCity(obj[6]);
			synInvoicePartnerDTO.setMailingCountryCode(obj[7]);
			synInvoicePartnerDTO.setMailingState(obj[8]);
			synInvoicePartnerDTO.setMailingZip(obj[9]);
			synInvoicePartnerDTO.setMailingPhone(obj[10]);
			synInvoicePartnerDTO.setMailingFax(obj[11]);
			synInvoicePartnerDTO.setBillingAddress1(obj[12]);
			synInvoicePartnerDTO.setBillingAddress2(obj[13]);
			synInvoicePartnerDTO.setBillingCity(obj[14]);
			synInvoicePartnerDTO.setBillingCountryCode(obj[15]);
			synInvoicePartnerDTO.setBillingState(obj[16]);
			synInvoicePartnerDTO.setBillingZip(obj[17]);
			synInvoicePartnerDTO.setBillingPhone(obj[18]);
			synInvoicePartnerDTO.setBillingFax(obj[19]);
			synInvoicePartnerDTO.setCoordinator(obj[20]);
			synInvoicePartnerDTO.setFirstName(obj[21]);
			synInvoicePartnerDTO.setVatNumber(obj[22]);
			synInvoicePartnerDTO.setClasscode(obj[23]);
			synInvoicePartnerDTO.setAccountManager(obj[24]);
			synInvoicePartnerDTO.setAccountHolder(obj[25]);
			synInvoicePartnerDTO.setBillingEmail(obj[26]);  
			partnerList.add(synInvoicePartnerDTO);
	}
		return partnerList;
	}
public List<Partner> financeSAPAgentPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID) {
	getHibernateTemplate().setMaxResults(50000);
	if (companyDivision.equals("") || companyDivision == null) {
		return getHibernateTemplate().find(
				"from Partner where (DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "'))  and  isAgent=true");
	} else {
		return getHibernateTemplate().find(
				"select p from Partner p " +
				"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + companyDivision + "' " + 
				"where (DATE_FORMAT(p.createdOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "') or DATE_FORMAT(p.updatedOn,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "'))  and  isAgent=true and p.companyDivision = '" + companyDivision + "'");
	}
}

	public  class DriverLocationDateSpreadsDTO{
		private Object BEGPACKA;
		private Object ENDPACKA;
		private Object PACKA;
		private Object BEGLOAD;
		private Object ENDLOAD;
		private Object LOADA;
		private Object DELSHIP;
		private Object DELLASTDAY;
		private Object ETA;
		public Object getBEGLOAD() {
			return BEGLOAD;
		}
		public void setBEGLOAD(Object begload) {
			BEGLOAD = begload;
		}
		public Object getBEGPACKA() {
			return BEGPACKA;
		}
		public void setBEGPACKA(Object begpacka) {
			BEGPACKA = begpacka;
		}
		public Object getDELSHIP() {
			return DELSHIP;
		}
		public void setDELSHIP(Object delship) {
			DELSHIP = delship;
		}
		public Object getETA() {
			return ETA;
		}
		public void setETA(Object eta) {
			ETA = eta;
		}
		public Object getLOADA() {
			return LOADA;
		}
		public void setLOADA(Object loada) {
			LOADA = loada;
		}
		public Object getPACKA() {
			return PACKA;
		}
		public void setPACKA(Object packa) {
			PACKA = packa;
		}
		public Object getDELLASTDAY() {
			return DELLASTDAY;
		}
		public void setDELLASTDAY(Object dellastday) {
			DELLASTDAY = dellastday;
		}
		public Object getENDLOAD() {
			return ENDLOAD;
		}
		public void setENDLOAD(Object endload) {
			ENDLOAD = endload;
		}
		public Object getENDPACKA() {
			return ENDPACKA;
		}
		public void setENDPACKA(Object endpacka) {
			ENDPACKA = endpacka;
		}
	}

	public List trackingInfoByShipNumber(String sessionCorpID, String sequenceNumber) {
		List trackingInfoListByShipNumber=new ArrayList();
		List DateList = this.getSession().createSQLQuery("select if(t.beginPacking is not null,CAST(DATE_FORMAT(t.beginPacking,'%d-%b') AS CHAR),'') AS BEGPACKA,if(t.packA is not null,CAST(DATE_FORMAT(t.packA,'%d-%b') AS CHAR),'') AS PACKA,if(t.beginLoad is not null,CAST(DATE_FORMAT(t.beginLoad,'%d-%b') AS CHAR),'') AS BEGLOAD,if(t.loadA is not null,CAST(DATE_FORMAT(t.loadA,'%d-%b') AS CHAR),'') AS LOADA,if(t.deliveryShipper is not null,CAST(DATE_FORMAT(t.deliveryShipper,'%d-%b') AS CHAR),'') AS DELSHIP,if(t.deliveryTA is not null,CAST(DATE_FORMAT(t.deliveryTA,'%d-%b') AS CHAR),'') AS ETA,if(t.endPacking is not null,CAST(DATE_FORMAT(t.endPacking,'%d-%b') AS CHAR),'') AS ENDPACKA,if(t.endLoad is not null,CAST(DATE_FORMAT(t.endLoad,'%d-%b') AS CHAR),'') AS ENDLOAD,if(t.deliveryLastDay is not null,CAST(DATE_FORMAT(t.deliveryLastDay,'%d-%b') AS CHAR),'') AS DELLASTDAY from trackingstatus t where shipNumber='"+sequenceNumber+"' and corpID='"+sessionCorpID+"'")
		   .addScalar("BEGPACKA", Hibernate.STRING)
		  .addScalar("PACKA" , Hibernate.STRING)
		  .addScalar("BEGLOAD", Hibernate.STRING)
		  .addScalar("LOADA", Hibernate.STRING)
		  .addScalar("DELSHIP", Hibernate.STRING)
		  .addScalar("ETA", Hibernate.STRING)
		  .addScalar("ENDPACKA", Hibernate.STRING)
		  .addScalar("ENDLOAD", Hibernate.STRING)
		  .addScalar("DELLASTDAY", Hibernate.STRING)
		.list();
		Iterator it =DateList.iterator();
		DriverLocationDateSpreadsDTO driverLocationDateSpreadsDTO=null;
		while(it.hasNext())
		  {
			  Object [] row=(Object[])it.next();
			  driverLocationDateSpreadsDTO = new DriverLocationDateSpreadsDTO();
			  driverLocationDateSpreadsDTO.setBEGPACKA(row[0]);
			  driverLocationDateSpreadsDTO.setPACKA(row[1]);
			  driverLocationDateSpreadsDTO.setBEGLOAD(row[2]);
			  driverLocationDateSpreadsDTO.setLOADA(row[3]);
			  driverLocationDateSpreadsDTO.setDELSHIP(row[4]);
			  driverLocationDateSpreadsDTO.setETA(row[5]);
			  driverLocationDateSpreadsDTO.setENDPACKA(row[6]);
			  driverLocationDateSpreadsDTO.setENDLOAD(row[7]);
			  driverLocationDateSpreadsDTO.setDELLASTDAY(row[8]);
			  trackingInfoListByShipNumber.add(driverLocationDateSpreadsDTO);
	}
		return trackingInfoListByShipNumber;
	}
	public Map<String, String> driverDropDownList(String sessionCorpID){
		String queryString = "from Partner where validNationalCode <> '' and isOwnerOp is true and status = 'Approved' and corpID='"+sessionCorpID+"'  and noDispatch is not true ORDER BY  firstName,lastName";
		List<Partner> list = getHibernateTemplate().find(queryString);
		Map<String, String> parameterDriverMap = new LinkedHashMap<String, String>();
		for (Partner partner : list) {
			parameterDriverMap.put(partner.getPartnerCode(), partner.getFirstName()+" "+partner.getLastName());
		}
		return parameterDriverMap;
	}
	public List nextDriverLocationList(String driverId,String sessionCorpID){
		String queryString = "select concat(if((firstName is null ),'',firstName),'~',if((lastName is null),'',lastName),'~',if(vanLastLocation is null ,'',vanLastLocation),'~',if(validNationalCode is null ,'',validNationalCode)) from partner where validNationalCode <> '' and isOwnerOp is true and status = 'Approved' and partnerCode='"+driverId+"' and corpID='"+sessionCorpID+"' " ;
		List list = this.getSession().createSQLQuery(queryString).list();
		return list;
	}

	public List findActgCodeForVendorCode(String vendorCode,String sessionCorpID, String companyDivision,String companyDivisionAcctgCodeUnique) {
		if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){ 
		return this.getSession().createSQLQuery("select if(pa.accountCrossReference is null,'' ,pa.accountCrossReference) from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.companyDivision='" + companyDivision + "' and pa.refType ='P' and pa.corpID IN ('"+sessionCorpID+"') where binary p1.partnerCode= '" + vendorCode + "' and p1.id = p2.partnerPublicId and p2.corpID  IN ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')").list();
		}else{
			return this.getSession().createSQLQuery("select if(pa.accountCrossReference is null,'' ,pa.accountCrossReference) from partnerprivate p2,partnerpublic p1 left outer join partneraccountref pa on binary p1.partnerCode = binary pa.partnerCode and pa.refType ='P' and pa.corpID IN ('"+sessionCorpID+"') where binary p1.partnerCode= '" + vendorCode + "' and p1.partnercode = p2.partnercode and p1.id = p2.partnerPublicId and p1.corpID  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') limit 1 ").list();	
			 
		}
		}
	
	public List getAllInvolvementOfDriverMap(String driverId,String sessionCorpID){
		String tempDate="";
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
		tempDate = nowYYYYMMDD1.toString();
		String queryString = "select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'LD' as loadType , " +
		"if(beginload is not null,date_format(beginload,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
		"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber,beginload as listDate, " +
		"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',',if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address, " +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , " +
		"m.driverCell as driverCell , m.mile as mile,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
		"where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and driverid like  '"+driverId+"%' " +
		"and s.status not in ('CNCL','DWNLD','CLSD') " +
		"and (beginpacking >= '"+tempDate+"' or beginload >= '"+tempDate+"' or if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"') " +
		"union " +
		"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'DL' as loadType  , " +
		"date_format(if(deliveryta is not null, deliveryta, deliveryshipper),'%e %b, %Y') as keyDate,concat(s.firstName,' ', s.lastName) as name , " +
		"m.estimatednetweight as estWt,m.estimatecubicfeet as estCube, s.shipNumber as shipNumber , " +
		"if(deliveryta is not null,deliveryta,deliveryshipper) as listDate , " +
		"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address ," +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile,m.tripNumber as tripNumber " +
		"from serviceorder s, miscellaneous m , trackingstatus t where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and driverid like  '"+driverId+"%'" +
		"and s.status not in ('CNCL','DWNLD','CLSD') and (beginpacking >= '"+tempDate+"' or beginload >= '"+tempDate+"' or if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"') " +
		"union " +
		"select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'CPK' as loadType , " +
		"if(beginpacking is not null,date_format(beginpacking,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
		"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber, beginpacking as listDate , " +
		"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',',if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address , " +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul , " +
		"m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
		"where  s.id = m.id and s.id = t.id and s.corpid = '"+sessionCorpID+"' and packingdriverid like  '"+driverId+"%' " +
		"and s.status not in ('CNCL','DWNLD','CLSD') and beginpacking >= '"+tempDate+"' " +
		"union " +
		"select driverid, s.registrationnumber as regNo,concat(s.origincity,', ', s.originstate) as location , 'CLD' as loadType , " +
		"if(beginload is not null,date_format(beginload,'%e %b, %Y'),'') as keyDate,concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as EstWt , " +
		"m.estimatecubicfeet as estCube, s.shipNumber as shipNumber, beginload as listDate , " +
		"concat(if(s.originAddressLine1 is null,'',s.originAddressLine1),',',s.originCity,',', if(s.originState is null,'',s.originState),',',s.originCountry,'-',s.originZip) as address , " +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul, m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell  , " +
		"m.mile as mile,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t  where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"' " +
		"and loadingdriverid like '"+driverId+"%' and s.status not in ('CNCL','DWNLD','CLSD')  and beginload >= '"+tempDate+"'  " +
		"union " +
		"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'CDL' as loadType , " +
		"if(deliveryta is not null, date_format(deliveryta,'%e %b, %Y') , if(deliveryshipper is not null , date_format(deliveryshipper,'%e %b, %Y'),'')) as keyDate, " +
		"concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt,m.estimatecubicfeet  as estCube, s.shipNumber as shipNumber , " +
		"if(deliveryta is not null,deliveryta, deliveryshipper) as listDate , " +
		"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address , " +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul , " +
		"m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell , m.mile as mile,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
		"where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"'  and deliverydriverid like '"+driverId+"%' " +
		"and s.status not in ('CNCL','DWNLD','CLSD') and  if(deliveryta is not null, deliveryta, deliveryshipper) >= '"+tempDate+"' " +
		"union " +
		"select driverid, s.registrationnumber as regNo, concat( s.destinationcity, ', ', s.destinationstate) as location, 'SIT' as loadType , " +
		"if(sitDestinationA is not null,date_format(sitDestinationA,'%e %b, %Y'),'') as keyDate, concat(s.firstName,' ', s.lastName) as name, m.estimatednetweight as estWt , " +
		"m.estimatecubicfeet  as estCube, s.shipNumber as shipNumber ,  " +
		"sitDestinationA as listDate , " +
		"concat(if(s.destinationAddressLine1 is null,'',s.destinationAddressLine1),',',s.destinationCity,',', if(s.destinationState is null,'',s.destinationState),',', s.destinationCountry,'-',s.destinationZip) as address , " +
		"m.actualNetWeight as actWt,m.actualCubicFeet as actCube, s.job as job, m.vanLineNationalCode as vanLineNationalCode, m.selfHaul as selfHaul,  m.estimatedRevenue as estimatedRevenue , m.driverCell as driverCell  , " +
		"m.mile as mile,m.tripNumber as tripNumber from serviceorder s, miscellaneous m , trackingstatus t " +
		"where  s.id = m.id and s.id = t.id and s.corpid =  '"+sessionCorpID+"' and sitdestinationdriverid like '"+driverId+"%'  " +
		"and s.status not in ('CNCL','DWNLD','CLSD') and  sitDestinationA >= '"+tempDate+"' " +
		"order by 10 " ;
        List driverLocation = this.getSession().createSQLQuery(queryString).list();
        return driverLocation;
	}
	public Map<String, String> findDriverTypeList(String sessionCorpID){
		String queryString = "from Partner where isOwnerOp is true and corpID='"+sessionCorpID+"'";
		List<Partner> list = getHibernateTemplate().find(queryString);
		Map<String, String> parameterDriverMap = new LinkedHashMap<String, String>();
		for (Partner partner : list) {
			parameterDriverMap.put(partner.getPartnerCode(), partner.getFirstName()+" "+partner.getLastName());
		}
		return parameterDriverMap;
	}
	public List getAmmount(String partnerCode, String sessionCorpID){
		String queryString = "select amount from subcontractorcharges where personId = '"+partnerCode+"' and corpID='"+sessionCorpID+"' order by id desc limit 1 ";
		List list = this.getSession().createSQLQuery(queryString).list();
		return list;
	}
	public List getPartnerPopupListForContractAgentForm(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor){
			List <Object> partnerList = new ArrayList<Object>();
		if (!partnerType.equals("")) { 
			String query = "select p.id, p.partnerCode, p.lastName, p.firstName,p.aliasName, p.isAgent, p.isAccount, p.isPrivateParty, p.isCarrier, p.isVendor, p.isOwnerOp, p.status, " +
			"if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country, " +
			"if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city, " +
			"if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state , cd.corpID " +
			",p.terminalAddress1 ,p.terminalAddress2 ,p.terminalZip "+
			"from partnerprivate p1 INNER JOIN  partnerpublic p on p.id=p1.partnerpublicid and p.partnercode=p1.partnercode "+
			"LEFT OUTER JOIN companydivision cd ON p.partnerCode=cd.bookingAgentCode where p.isAgent=true and p.status='Approved' and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"
			+ lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '" + partnerCode.replaceAll("'", "''") + "%' AND p.aliasName like '" + aliasName.replaceAll("'", "''") + "%' AND p.terminalCountryCode like '"
			+ billingCountryCode.replaceAll("'", "''") + "%' AND p.terminalCountry like '" + billingCountry.replaceAll("'", "''")
			+ "%' AND p.terminalState like '" + billingStateCode.replaceAll("'", "''") + "%' AND p1.extReference like '" + extReference.replaceAll("'", "''") + "%' and p1.corpID='"+corpID+"' and p.utsnumber='Y' and (cd.corpID not like '"+corpID+"%' or cd.corpID is null or cd.corpID='') ";

			 popupList = this.getSession().createSQLQuery(query).list();
				
				Iterator it = popupList.iterator();
				AgentDTO dto = null;
				while(it.hasNext()){
					Object [] row = (Object[])it.next();
					dto = new AgentDTO();
					
					dto.setId(row[0]);
					if(row[1] == null){
						dto.setPartnerCode("");
					}else{
						dto.setPartnerCode(row[1]);
					}
					if(row[2] == null){
						dto.setLastName("");
					}else{
						dto.setLastName(row[2]);
					}
					if(row[3] == null){
						dto.setFirstName("");
					}else{
						dto.setFirstName(row[3]);
					}
					if(row[4] == null){
						dto.setAliasName("");
					}else{
						dto.setAliasName(row[4]);
					}
					if(row[5]==null){
						dto.setIsAgent("");
					}else{
						dto.setIsAgent(row[5]);
					}
					
					if(row[6]==null){
						dto.setIsAccount("");
					}else{
						dto.setIsAccount(row[6]);
					}
					
					if(row[7]==null){
						dto.setIsPrivateParty("");
					}else{
						dto.setIsPrivateParty(row[7]);
					}
					
					if(row[8]==null){
						dto.setIsCarrier("");
					}else{
						dto.setIsCarrier(row[8]);
					}
					
					if(row[9]==null){
						dto.setIsVendor("");
					}else{
						dto.setIsVendor(row[9]);
					}
					
					if(row[10]==null){
						dto.setIsOwnerOp("");
					}else{
						dto.setIsOwnerOp(row[10]);
					}
					
					if(row[11]==null){
						dto.setStatus("");
					}else{
						dto.setStatus(row[11]);
					}
					
					if(row[12] == null){
						dto.setCountryName("");
					}else{
						dto.setCountryName(row[12]);
					}
					
					if(row[13] == null){
						dto.setCityName("");
					}else{
						dto.setCityName(row[13]);
					}
					if(row[14] == null){
						dto.setStateName("");
					}else{
						dto.setStateName(row[14]);
					}
					if(row[15] == null){
						dto.setAgentCorpID("");
					}else{
						dto.setAgentCorpID(row[15]);
					}
					if(row[16] == null){
						dto.setAdd1("");
					}else{
						dto.setAdd1(row[16]);
					}
					if(row[17] == null){
						dto.setAdd2("");
					}else{
						dto.setAdd2(row[17]);
					}
					if(row[18] == null){
						dto.setZip("");
					}else{
						dto.setZip(row[18]);
					}
							
					partnerList.add(dto);
				} 
		} 
		return partnerList;
 }
	
	public class AgentDTO implements ListLinkData{
		private Object id;
		private Object partnerCode;
		private Object lastName;
		private Object firstName;
		private Object aliasName;
		private Object isAgent;
		private Object isAccount;
		private Object isPrivateParty;
		private Object isCarrier;
		private Object isVendor;
		private Object isOwnerOp;
		private Object status;
		private Object countryName;
		private Object stateName;
		private Object cityName;
		private Object vanLineCode;
		private Object add1;
		private Object add2;
		private Object zip;
		private Object agentCorpID;
		
		@Transient
		public String getListCode() {
			return (String) partnerCode;
		}

		@Transient
		public String getListDescription() {
			String lName = (String) this.lastName;
			return lName;
		}		
		
		@Transient
		public String getListSecondDescription() {
			String aCorpId = (String) this.agentCorpID;
			return aCorpId;
		}

		@Transient
		public String getListThirdDescription() {
			return "";
		}

		@Transient
		public String getListFourthDescription() {
			return "";
		}

		@Transient
		public String getListFifthDescription() {
			return "";
		}

		@Transient
		public String getListSixthDescription() {
			return "";
		}
		@Transient
		public String getListEigthDescription() {
			return null;
		}
		@Transient
		public String getListNinthDescription() {
			return null;
		}
		@Transient
		public String getListSeventhDescription() {
			return null;
		}
		@Transient
		public String getListTenthDescription() {
			return null;
		}

		
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Object isAccount) {
			this.isAccount = isAccount;
		}
		public Object getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Object isAgent) {
			this.isAgent = isAgent;
		}
		public Object getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Object isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Object getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Object isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Object getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Object isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Object getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Object isVendor) {
			this.isVendor = isVendor;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getCityName() {
			return cityName;
		}
		public void setCityName(Object cityName) {
			this.cityName = cityName;
		}
		public Object getCountryName() {
			return countryName;
		}
		public void setCountryName(Object countryName) {
			this.countryName = countryName;
		}
		public Object getStateName() {
			return stateName;
		}
		public void setStateName(Object stateName) {
			this.stateName = stateName;
		}
		public Object getVanLineCode() {
			return vanLineCode;
		}
		public void setVanLineCode(Object vanLineCode) {
			this.vanLineCode = vanLineCode;
		}

		public Object getAdd1() {
			return add1;
		}

		public void setAdd1(Object add1) {
			this.add1 = add1;
		}

		public Object getAdd2() {
			return add2;
		}

		public void setAdd2(Object add2) {
			this.add2 = add2;
		}

		public Object getZip() {
			return zip;
		}

		public void setZip(Object zip) {
			this.zip = zip;
		}

		public Object getAliasName() {
			return aliasName;
		}

		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}

		public Object getAgentCorpID() {
			return agentCorpID;
		}

		public void setAgentCorpID(Object agentCorpID) {
			this.agentCorpID = agentCorpID;
		}
}



	public List getPartnerNetworkGroupPopupList(String partnerType, String corpID, String lastName, String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String customerVendor,String vanlineCode) {

		getHibernateTemplate().setMaxResults(100);
		if (!partnerType.equals("")) { 
			if (partnerType.equals("AG")) {
				if(vanlineCode.equalsIgnoreCase("")){
				popupList = getHibernateTemplate().find(
						"from Partner where isAgent=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND aliasName like '%" + aliasName.replaceAll("'", "''") + "%' AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + extReference.replaceAll("'", "''") + "%'   and ( networkGroup=true or ugwwNetworkGroup is true ) ");
				}else{
					String query = "select p.partnerCode,p.lastName,p.billingAddress1,p.billingCountryCode,p.billingState,p.billingCity,p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+					
					" from partnervanlineref n,partner p where"+					
					" n.partnerCode=p.partnerCode and p.isAgent=true and p.status='Approved' and n.vanLineCode like '%"+vanlineCode+"%'"+
					" and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%') " +
					" AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
					" AND p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' " +
					" AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
					" AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
					" AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
					" AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
					" AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' and ( p.networkGroup=true or p.ugwwNetworkGroup is true ) ";					
					List list = getSession().createSQLQuery(query).list();
					popupList = converObjToDTO(list);
				}
			}
		} else {
				popupList = getHibernateTemplate().find(
						"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND aliasName like '%" + aliasName.replaceAll("'", "''") + "%' AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'   and ( networkGroup=true or ugwwNetworkGroup is true )  ");
			}
				return popupList;
	
	}
	public List partnerExtractUTSI(String billToCode, String corpID) {
		//getHibernateTemplate().setMaxResults(100);
		getHibernateTemplate().setMaxResults(50000);
		return getHibernateTemplate().find("from Partner where partnerCode in(" + billToCode + ") and  (isPrivateParty=true or isAgent=true or isAccount=true) ");
	}
	public List financePartnerExtractUTSI(String beginDate, String endDate, String companyDivision,String corpID) {
		getHibernateTemplate().setMaxResults(50000);
		if (companyDivision.equals("") || companyDivision == null) {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  (isPrivateParty=true or isAgent=true or isAccount=true)");
		} else {
			return getHibernateTemplate().find(
					"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
					+ "')))  and  (isPrivateParty=true or isAgent=true or isAccount=true) and companyDivision = '" + companyDivision + "'");
		}
	}
	
	public List basedAtName(String partnerCode, String sessionCorpID){
		return this.getSession().createSQLQuery("select lastName from partner where  partnerCode='"+partnerCode+"' and (isAgent=true or isAccount=true) and status='Approved' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')").list();
	}
	
	public List basedAtNameForInvoicing(String partnerCode, String sessionCorpID){
		return this.getSession().createSQLQuery("select lastName from partner where  partnerCode='"+partnerCode+"' and corpID='" + sessionCorpID + " ' ").list();
	}

	public String checkPartnerType(String billToCode) {
		String partnerType="";
		List partnerList = new ArrayList();
		partnerList= getHibernateTemplate().find("select partnerType from PartnerPublic where partnerCode = '" + billToCode + "' and partnerType in ('CMM','DMM') ");
		if((partnerList!=null )&& (!(partnerList.isEmpty())) && (partnerList.get(0)!=null) && (!(partnerList.get(0).toString().equals(""))) && ((partnerList.get(0).toString().trim().equalsIgnoreCase("CMM"))|| (partnerList.get(0).toString().trim().equalsIgnoreCase("DMM")))){
			partnerType=partnerList.get(0).toString();
		}
		return partnerType;
	}
	public List financeAgentPartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID){
		List findVendorInvoices=new ArrayList();
		String query ="";
		if (companyDivision.equals("") || companyDivision == null) {
			
			query="select p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
				  " p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
				  " p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
				  " p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
				  " p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
				  " p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+
				  " pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
				  " from partnerpublic p "+
				  " inner join partnerprivate pp on p.id=pp.partnerPublicId and pp.corpid='"+corpID+"' "+
				  " left outer join partneraccountref r on p.partnercode=r.partnercode and r.corpid='"+corpID+"' and r.refType='R' "+
				  " where  p.corpid in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')  "+
			      " and ((DATE_FORMAT(p.createdon,'%Y-%m-%d')  BETWEEN ('"+beginDate+"') AND ('"+endDate+"') "+ 
			      " or DATE_FORMAT(p.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDate+"') AND ('"+endDate+"')))   "+
			      " and  p.isAgent=true limit 50000";
		}else{

			query="select p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
				  " p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
				  " p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
				  " p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
				  " p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
				  " p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+
				  " pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
				  " from partnerpublic p "+
				  " inner join partnerprivate pp on p.id=pp.partnerPublicId and pp.corpid='"+corpID+"' and pp.companyDivision = '"+companyDivision+"' "+
				  " left outer join partneraccountref r on p.partnercode=r.partnercode and r.companyDivision = '"+companyDivision+"' and r.corpid='"+corpID+"' and r.refType='R' "+
				  " where  p.corpid in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') " +
			      " and ((DATE_FORMAT(p.createdon,'%Y-%m-%d')  BETWEEN ('"+beginDate+"') AND ('"+endDate+"') "+ 
			      " or DATE_FORMAT(p.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDate+"') AND ('"+endDate+"')))   "+
				  " and  p.isAgent=true limit 50000";			
		}
		List list=this.getSession().createSQLQuery(query.toString()).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
		Iterator it=list.iterator();
		HollPartnerDTO hollPartnerDto=null;
		 while(it.hasNext())
		  {
			 Object [] row=(Object[])it.next();
			 hollPartnerDto=new HollPartnerDTO();
			 hollPartnerDto.setPartnerCode((String)(row[0]==null?"":row[0]));
			 hollPartnerDto.setFirstName((String)(row[1]==null?"":row[1]));
			 hollPartnerDto.setLastName((String)(row[2]==null?"":row[2]));
			 hollPartnerDto.setMailingAddress1((String)(row[3]==null?"":row[3]));
			 hollPartnerDto.setMailingAddress2((String)(row[4]==null?"":row[4]));
			 hollPartnerDto.setMailingCity((String)(row[5]==null?"":row[5]));
			 hollPartnerDto.setMailingCountryCode((String)(row[6]==null?"":row[6]));
			 hollPartnerDto.setMailingState((String)(row[7]==null?"":row[7]));
			 hollPartnerDto.setMailingZip((String)(row[8]==null?"":row[8]));
			 hollPartnerDto.setMailingPhone((String)(row[9]==null?"":row[9]));
			 hollPartnerDto.setMailingFax((String)(row[10]==null?"":row[10]));
			 hollPartnerDto.setBillingAddress1((String)(row[11]==null?"":row[11]));
			 hollPartnerDto.setBillingAddress2((String)(row[12]==null?"":row[12]));
			 hollPartnerDto.setBillingCity((String)(row[13]==null?"":row[13]));
			 hollPartnerDto.setBillingCountryCode((String)(row[14]==null?"":row[14]));
			 hollPartnerDto.setBillingState((String)(row[15]==null?"":row[15]));
			 hollPartnerDto.setBillingZip((String)(row[16]==null?"":row[16]));
			 hollPartnerDto.setBillingPhone((String)(row[17]==null?"":row[17]));
			 hollPartnerDto.setBillingFax((String)(row[18]==null?"":row[18]));
			 hollPartnerDto.setCoordinator((String)(row[19]==null?"":row[19]));
			 hollPartnerDto.setIsPrivateParty((Boolean)(row[20]==null?false:row[20]));
			 hollPartnerDto.setIsAccount((Boolean)(row[21]==null?false:row[21]));
			 hollPartnerDto.setIsAgent((Boolean)(row[22]==null?false:row[22]));
			 hollPartnerDto.setIsCarrier((Boolean)(row[23]==null?false:row[23]));
			 hollPartnerDto.setIsOwnerOp((Boolean)(row[24]==null?false:row[24]));			 
			 hollPartnerDto.setIsVendor((Boolean)(row[25]==null?false:row[25]));
			 hollPartnerDto.setAccountCrossReference((String)(row[26]==null?"":row[26]));
			 hollPartnerDto.setAccountHolder((String)(row[27]==null?"":row[27]));
			 hollPartnerDto.setCreditTerms((String)(row[28]==null?"":row[28]));
			 hollPartnerDto.setBillingEmail((String)(row[29]==null?"":row[29]));
			 hollPartnerDto.setMailingEmail((String)(row[30]==null?"":row[30]));
			 findVendorInvoices.add(hollPartnerDto);
		  }
		}
		return findVendorInvoices;
	}

	public List synInvoicePartnerExtractHoll(String sysInvoiceDates, String companyDivision, String corpID){
		List findVendorInvoices=new ArrayList();
		String query ="";
		query="select distinct p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
			  " p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
			  " p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
			  " p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
			  " p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
			  " p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+
			  " pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
			  " from partnerpublic p "+
			  " inner join partnerprivate pp on p.id=pp.partnerPublicId and pp.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') "+
			  " inner join accountline a on a.billtocode = p.partnercode  and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) "+
			  " and a.recaccdate is not null "+
			  " inner join serviceorder s on a.serviceorderid = s.id and s.corpid = '"+corpID+"' and s.companydivision = '"+corpID+"' "+
			  " left outer join partneraccountref r on p.partnercode=r.partnercode and r.corpid='"+corpID+"' and r.refType='R' "+
			  " where (p.createdon >= '"+sysInvoiceDates+"' or p.updatedon >= '"+sysInvoiceDates+"')";
		List list=this.getSession().createSQLQuery(query.toString()).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
		Iterator it=list.iterator();
		HollPartnerDTO hollPartnerDto=null;
		 while(it.hasNext())
		  {
			 Object [] row=(Object[])it.next();
			 hollPartnerDto=new HollPartnerDTO();
			 hollPartnerDto.setPartnerCode((String)(row[0]==null?"":row[0]));
			 hollPartnerDto.setFirstName((String)(row[1]==null?"":row[1]));
			 hollPartnerDto.setLastName((String)(row[2]==null?"":row[2]));
			 hollPartnerDto.setMailingAddress1((String)(row[3]==null?"":row[3]));
			 hollPartnerDto.setMailingAddress2((String)(row[4]==null?"":row[4]));
			 hollPartnerDto.setMailingCity((String)(row[5]==null?"":row[5]));
			 hollPartnerDto.setMailingCountryCode((String)(row[6]==null?"":row[6]));
			 hollPartnerDto.setMailingState((String)(row[7]==null?"":row[7]));
			 hollPartnerDto.setMailingZip((String)(row[8]==null?"":row[8]));
			 hollPartnerDto.setMailingPhone((String)(row[9]==null?"":row[9]));
			 hollPartnerDto.setMailingFax((String)(row[10]==null?"":row[10]));
			 hollPartnerDto.setBillingAddress1((String)(row[11]==null?"":row[11]));
			 hollPartnerDto.setBillingAddress2((String)(row[12]==null?"":row[12]));
			 hollPartnerDto.setBillingCity((String)(row[13]==null?"":row[13]));
			 hollPartnerDto.setBillingCountryCode((String)(row[14]==null?"":row[14]));
			 hollPartnerDto.setBillingState((String)(row[15]==null?"":row[15]));
			 hollPartnerDto.setBillingZip((String)(row[16]==null?"":row[16]));
			 hollPartnerDto.setBillingPhone((String)(row[17]==null?"":row[17]));
			 hollPartnerDto.setBillingFax((String)(row[18]==null?"":row[18]));
			 hollPartnerDto.setCoordinator((String)(row[19]==null?"":row[19]));
			 hollPartnerDto.setIsPrivateParty((Boolean)(row[20]==null?false:row[20]));
			 hollPartnerDto.setIsAccount((Boolean)(row[21]==null?false:row[21]));
			 hollPartnerDto.setIsAgent((Boolean)(row[22]==null?false:row[22]));
			 hollPartnerDto.setIsCarrier((Boolean)(row[23]==null?false:row[23]));
			 hollPartnerDto.setIsOwnerOp((Boolean)(row[24]==null?false:row[24]));			 
			 hollPartnerDto.setIsVendor((Boolean)(row[25]==null?false:row[25]));
			 hollPartnerDto.setAccountCrossReference((String)(row[26]==null?"":row[26]));
			 hollPartnerDto.setAccountHolder((String)(row[27]==null?"":row[27]));
			 hollPartnerDto.setCreditTerms((String)(row[28]==null?"":row[28]));
			 hollPartnerDto.setBillingEmail((String)(row[29]==null?"":row[29]));
			 hollPartnerDto.setMailingEmail((String)(row[30]==null?"":row[30]));
			 findVendorInvoices.add(hollPartnerDto);
		  }
		}
		return findVendorInvoices;
	}

public List partnerExtractHoll(String billToCode, String corpID) {
		List findVendorInvoices=new ArrayList();
		String query ="";
		query="select p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
			  " p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
			  " p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
			  " p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
			  " p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
			  " p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+
			  " pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
			  " from partnerpublic p "+
			  " inner join partnerprivate pp on p.id=pp.partnerpublicid and pp.corpid='"+corpID+"' "+
			  " left outer join partneraccountref r on p.partnercode=r.partnercode and r.corpid='"+corpID+"' and r.refType='R' "+
			  " where p.partnerCode in("+billToCode+") "+
			  " and (isPrivateParty=true or isAgent=true or isAccount=true or isVendor=true or "+
			  " isCarrier=true) and p.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') limit 50000";
		List list=this.getSession().createSQLQuery(query.toString()).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
		Iterator it=list.iterator();
		HollPartnerDTO hollPartnerDto=null;
		 while(it.hasNext())
		  {
			 Object [] row=(Object[])it.next();
			 hollPartnerDto=new HollPartnerDTO();
			 hollPartnerDto.setPartnerCode((String)(row[0]==null?"":row[0]));
			 hollPartnerDto.setFirstName((String)(row[1]==null?"":row[1]));
			 hollPartnerDto.setLastName((String)(row[2]==null?"":row[2]));
			 hollPartnerDto.setMailingAddress1((String)(row[3]==null?"":row[3]));
			 hollPartnerDto.setMailingAddress2((String)(row[4]==null?"":row[4]));
			 hollPartnerDto.setMailingCity((String)(row[5]==null?"":row[5]));
			 hollPartnerDto.setMailingCountryCode((String)(row[6]==null?"":row[6]));
			 hollPartnerDto.setMailingState((String)(row[7]==null?"":row[7]));
			 hollPartnerDto.setMailingZip((String)(row[8]==null?"":row[8]));
			 hollPartnerDto.setMailingPhone((String)(row[9]==null?"":row[9]));
			 hollPartnerDto.setMailingFax((String)(row[10]==null?"":row[10]));
			 hollPartnerDto.setBillingAddress1((String)(row[11]==null?"":row[11]));
			 hollPartnerDto.setBillingAddress2((String)(row[12]==null?"":row[12]));
			 hollPartnerDto.setBillingCity((String)(row[13]==null?"":row[13]));
			 hollPartnerDto.setBillingCountryCode((String)(row[14]==null?"":row[14]));
			 hollPartnerDto.setBillingState((String)(row[15]==null?"":row[15]));
			 hollPartnerDto.setBillingZip((String)(row[16]==null?"":row[16]));
			 hollPartnerDto.setBillingPhone((String)(row[17]==null?"":row[17]));
			 hollPartnerDto.setBillingFax((String)(row[18]==null?"":row[18]));
			 hollPartnerDto.setCoordinator((String)(row[19]==null?"":row[19]));
			 hollPartnerDto.setIsPrivateParty((Boolean)(row[20]==null?false:row[20]));
			 hollPartnerDto.setIsAccount((Boolean)(row[21]==null?false:row[21]));
			 hollPartnerDto.setIsAgent((Boolean)(row[22]==null?false:row[22]));
			 hollPartnerDto.setIsCarrier((Boolean)(row[23]==null?false:row[23]));
			 hollPartnerDto.setIsOwnerOp((Boolean)(row[24]==null?false:row[24]));			 
			 hollPartnerDto.setIsVendor((Boolean)(row[25]==null?false:row[25]));
			 hollPartnerDto.setAccountCrossReference((String)(row[26]==null?"":row[26]));
			 hollPartnerDto.setAccountHolder((String)(row[27]==null?"":row[27]));
			 hollPartnerDto.setCreditTerms((String)(row[28]==null?"":row[28]));
			 hollPartnerDto.setBillingEmail((String)(row[29]==null?"":row[29]));
			 hollPartnerDto.setMailingEmail((String)(row[30]==null?"":row[30]));
			 findVendorInvoices.add(hollPartnerDto);
		  }
		}
		return findVendorInvoices;		
	}

	public List financePartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID) {
		List findVendorInvoices=new ArrayList();
		String query ="";
		if (companyDivision.equals("") || companyDivision == null) {
			query="select p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
			" p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
			" p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
			" p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
			" p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
			" p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+
			" pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
			" from partnerpublic p "+
			" inner join partnerprivate pp on p.id=pp.partnerPublicId and pp.corpid='"+corpID+"' "+
			" left outer join partneraccountref r on p.partnercode=r.partnercode and r.corpid='"+corpID+"' and r.refType='R' "+
			" where  p.corpid in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') " +
			" and  ((DATE_FORMAT(p.createdon,'%Y-%m-%d')  BETWEEN ('"+beginDate+"') AND ('"+endDate+"') " +
			" or DATE_FORMAT(p.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDate+"') AND ('"+endDate+"'))) "+
			" and (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true) limit 50000";
		}else{
			query="select p.partnerCode,p.firstName,p.lastName,p.mailingAddress1,p.mailingAddress2, "+
			" p.mailingCity,p.mailingCountryCode,p.mailingState,p.mailingZip, "+
			" p.mailingPhone,p.mailingFax,p.billingAddress1,p.billingAddress2, "+
			" p.billingCity,p.billingCountryCode,p.billingState,p.billingZip, "+
			" p.billingPhone,p.billingFax,pp.coordinator,p.isPrivateParty, "+
			" p.isAccount,p.isAgent,p.isCarrier,p.isOwnerOp,p.isVendor,r.accountCrossReference, "+ 
			" pp.accountHolder,pp.creditTerms,p.billingEmail,p.mailingEmail "+
			" from partnerpublic p "+
			" inner join partnerprivate pp on p.id=pp.partnerPublicId and pp.corpid='"+corpID+"' and pp.companyDivision = '"+companyDivision+"' "+
			" left outer join partneraccountref r on p.partnercode=r.partnercode and r.corpid='"+corpID+"' and r.refType='R' "+
			" where  p.corpid in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') " +
			" and ((DATE_FORMAT(p.createdon,'%Y-%m-%d') BETWEEN ('"+beginDate+"') AND ('"+endDate+"') " +
			" or DATE_FORMAT(p.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDate+"') AND ('"+endDate+"'))) "+
			" and (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true) limit 50000";
		}
		List list=this.getSession().createSQLQuery(query.toString()).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
		Iterator it=list.iterator();
		HollPartnerDTO hollPartnerDto=null;
		 while(it.hasNext())
		  {
			 Object [] row=(Object[])it.next();
			 hollPartnerDto=new HollPartnerDTO();
			 hollPartnerDto.setPartnerCode((String)(row[0]==null?"":row[0]));
			 hollPartnerDto.setFirstName((String)(row[1]==null?"":row[1]));
			 hollPartnerDto.setLastName((String)(row[2]==null?"":row[2]));
			 hollPartnerDto.setMailingAddress1((String)(row[3]==null?"":row[3]));
			 hollPartnerDto.setMailingAddress2((String)(row[4]==null?"":row[4]));
			 hollPartnerDto.setMailingCity((String)(row[5]==null?"":row[5]));
			 hollPartnerDto.setMailingCountryCode((String)(row[6]==null?"":row[6]));
			 hollPartnerDto.setMailingState((String)(row[7]==null?"":row[7]));
			 hollPartnerDto.setMailingZip((String)(row[8]==null?"":row[8]));
			 hollPartnerDto.setMailingPhone((String)(row[9]==null?"":row[9]));
			 hollPartnerDto.setMailingFax((String)(row[10]==null?"":row[10]));
			 hollPartnerDto.setBillingAddress1((String)(row[11]==null?"":row[11]));
			 hollPartnerDto.setBillingAddress2((String)(row[12]==null?"":row[12]));
			 hollPartnerDto.setBillingCity((String)(row[13]==null?"":row[13]));
			 hollPartnerDto.setBillingCountryCode((String)(row[14]==null?"":row[14]));
			 hollPartnerDto.setBillingState((String)(row[15]==null?"":row[15]));
			 hollPartnerDto.setBillingZip((String)(row[16]==null?"":row[16]));
			 hollPartnerDto.setBillingPhone((String)(row[17]==null?"":row[17]));
			 hollPartnerDto.setBillingFax((String)(row[18]==null?"":row[18]));
			 hollPartnerDto.setCoordinator((String)(row[19]==null?"":row[19]));
			 hollPartnerDto.setIsPrivateParty((Boolean)(row[20]==null?false:row[20]));
			 hollPartnerDto.setIsAccount((Boolean)(row[21]==null?false:row[21]));
			 hollPartnerDto.setIsAgent((Boolean)(row[22]==null?false:row[22]));
			 hollPartnerDto.setIsCarrier((Boolean)(row[23]==null?false:row[23]));
			 hollPartnerDto.setIsOwnerOp((Boolean)(row[24]==null?false:row[24]));			 
			 hollPartnerDto.setIsVendor((Boolean)(row[25]==null?false:row[25]));
			 hollPartnerDto.setAccountCrossReference((String)(row[26]==null?"":row[26]));
			 hollPartnerDto.setAccountHolder((String)(row[27]==null?"":row[27]));
			 hollPartnerDto.setCreditTerms((String)(row[28]==null?"":row[28]));
			 hollPartnerDto.setBillingEmail((String)(row[29]==null?"":row[29]));
			 hollPartnerDto.setMailingEmail((String)(row[30]==null?"":row[30]));
			 findVendorInvoices.add(hollPartnerDto);
		  }
		}
		return findVendorInvoices;
	}

public class HollPartnerDTO{
		 private String partnerCode;
		 private String firstName;
		 private String lastName;
		 private String mailingAddress1;
		 private String mailingAddress2;
		 private String mailingCity;
		 private String mailingCountryCode;
		 private String mailingState;
		 private String mailingZip;
		 private String mailingPhone;
		 private String mailingFax;
		 private String billingAddress1;
		 private String billingAddress2;
		 private String billingCity;
		 private String billingCountryCode;
		 private String billingState;
		 private String billingZip;
		 private String billingPhone;
		 private String billingFax;
		 private String coordinator;
		 private Boolean isPrivateParty;
		 private Boolean isAccount;
		 private Boolean isAgent;		
		 private Boolean isCarrier;
		 private Boolean isOwnerOp;
		 private Boolean isVendor;
		 private String accountCrossReference;
		 private String accountHolder;
		 private String creditTerms;
		 private String mailingEmail;
		 private String billingEmail;
		public String getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(String partnerCode) {
			this.partnerCode = partnerCode;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getMailingAddress1() {
			return mailingAddress1;
		}
		public void setMailingAddress1(String mailingAddress1) {
			this.mailingAddress1 = mailingAddress1;
		}
		public String getMailingAddress2() {
			return mailingAddress2;
		}
		public void setMailingAddress2(String mailingAddress2) {
			this.mailingAddress2 = mailingAddress2;
		}
		public String getMailingCity() {
			return mailingCity;
		}
		public void setMailingCity(String mailingCity) {
			this.mailingCity = mailingCity;
		}
		public String getMailingCountryCode() {
			return mailingCountryCode;
		}
		public void setMailingCountryCode(String mailingCountryCode) {
			this.mailingCountryCode = mailingCountryCode;
		}
		public String getMailingState() {
			return mailingState;
		}
		public void setMailingState(String mailingState) {
			this.mailingState = mailingState;
		}
		public String getMailingZip() {
			return mailingZip;
		}
		public void setMailingZip(String mailingZip) {
			this.mailingZip = mailingZip;
		}
		public String getMailingPhone() {
			return mailingPhone;
		}
		public void setMailingPhone(String mailingPhone) {
			this.mailingPhone = mailingPhone;
		}
		public String getMailingFax() {
			return mailingFax;
		}
		public void setMailingFax(String mailingFax) {
			this.mailingFax = mailingFax;
		}
		public String getBillingAddress1() {
			return billingAddress1;
		}
		public void setBillingAddress1(String billingAddress1) {
			this.billingAddress1 = billingAddress1;
		}
		public String getBillingAddress2() {
			return billingAddress2;
		}
		public void setBillingAddress2(String billingAddress2) {
			this.billingAddress2 = billingAddress2;
		}
		public String getBillingCity() {
			return billingCity;
		}
		public void setBillingCity(String billingCity) {
			this.billingCity = billingCity;
		}
		public String getBillingCountryCode() {
			return billingCountryCode;
		}
		public void setBillingCountryCode(String billingCountryCode) {
			this.billingCountryCode = billingCountryCode;
		}
		public String getBillingState() {
			return billingState;
		}
		public void setBillingState(String billingState) {
			this.billingState = billingState;
		}
		public String getBillingZip() {
			return billingZip;
		}
		public void setBillingZip(String billingZip) {
			this.billingZip = billingZip;
		}
		public String getBillingPhone() {
			return billingPhone;
		}
		public void setBillingPhone(String billingPhone) {
			this.billingPhone = billingPhone;
		}
		public String getBillingFax() {
			return billingFax;
		}
		public void setBillingFax(String billingFax) {
			this.billingFax = billingFax;
		}
		public String getCoordinator() {
			return coordinator;
		}
		public void setCoordinator(String coordinator) {
			this.coordinator = coordinator;
		}
		public Boolean getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Boolean isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Boolean getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Boolean isAccount) {
			this.isAccount = isAccount;
		}
		public Boolean getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Boolean isAgent) {
			this.isAgent = isAgent;
		}
		public Boolean getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Boolean isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Boolean getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Boolean isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Boolean getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Boolean isVendor) {
			this.isVendor = isVendor;
		}
		public String getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(String accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
		public String getAccountHolder() {
			return accountHolder;
		}
		public void setAccountHolder(String accountHolder) {
			this.accountHolder = accountHolder;
		}
		public String getCreditTerms() {
			return creditTerms;
		}
		public void setCreditTerms(String creditTerms) {
			this.creditTerms = creditTerms;
		}
		public String getMailingEmail() {
			return mailingEmail;
		}
		public void setMailingEmail(String mailingEmail) {
			this.mailingEmail = mailingEmail;
		}
		public String getBillingEmail() {
			return billingEmail;
		}
		public void setBillingEmail(String billingEmail) {
			this.billingEmail = billingEmail;
		}
		 
	 }



public List findPartnerForActgCodeVanLine(String vendorCode, String sessionCorpID) {
List accountCrossReferencelist = this.getSession().createSQLQuery("select concat(if(accountCrossReference is null,'' ,accountCrossReference),'~',if((companyDivision is null or companyDivision =''),'NO' ,companyDivision)) from partneraccountref where corpID='"+sessionCorpID+"'  and partnerCode='"+vendorCode+"' and refType='P' ").list();
 return accountCrossReferencelist;
}

public List<String> getPartnerTypeList() {
	String partnerType="";
	List partnerList = new ArrayList();
	partnerList= this.getSession().createSQLQuery("select partnerCode from partnerpublic where  partnerType in ('CMM','DMM') ").list();
	/*if((partnerList!=null )&& (!(partnerList.isEmpty())) && (partnerList.get(0)!=null) && (!(partnerList.get(0).toString().equals(""))) && ((partnerList.get(0).toString().trim().equalsIgnoreCase("CMM"))|| (partnerList.get(0).toString().trim().equalsIgnoreCase("DMM")))){
		partnerType=partnerList.get(0).toString();
	}*/
	return partnerList;
}	
	public Set getVanlineJobDetails(){
		Set vanLineDetails=new TreeSet();
		List partnerList = new ArrayList();
		List partnerVanList = new ArrayList();
		partnerList= this.getSession().createSQLQuery("select distinct miscVl from systemdefault where miscVl is not null and miscVl!=''").list();
		for (Object object : partnerList) {
			String vl="";
			if(object!=null && !object.toString().equals("")){
				vl=object.toString().replace("(","").replace(")","");
				String [] vl1=vl.split(",");
				for (String string1 : vl1) {
					String vl2= string1.replaceAll("'", "").replaceAll("�", "").replaceAll("�", "").trim();
					partnerVanList.add(vl2);
				}
			}
		}
		List refJob = this.getSession().createSQLQuery("select code from refmaster where parameter='JOB'").list();
		for (Object objectJob : refJob) {
			if(!partnerVanList.contains(objectJob)){
				vanLineDetails.add(objectJob);	
			}
			
		}
		
		return vanLineDetails;
	}

	public List AFASPartnerExtract(String beginDate, String endDate, String corpID) {
	getHibernateTemplate().setMaxResults(50000);
    return getHibernateTemplate().find(
				"from Partner where ((DATE_FORMAT(createdon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "') or DATE_FORMAT(updatedon,'%Y-%m-%d') BETWEEN ('" + beginDate + " ') AND ('" + endDate
				+ "')) ) and  (isPrivateParty=true or isAgent=true or isAccount=true or isCarrier=true or isVendor=true) and status = 'Approved' ");
	
	}

	public Map<String, AccountContact> accountContactMap(String sessionCorpID) { 
		Map<String, AccountContact> parameterMap = new LinkedHashMap<String, AccountContact>();
		List<AccountContact> list;
		try {
		list = getHibernateTemplate().find("from AccountContact where corpID = '"+sessionCorpID+"' AND contractType='Secondary' order by  id desc");
		for (AccountContact accountContact : list) {
			parameterMap.put(accountContact.getPartnerCode(), accountContact);
		}
		}catch(Exception e){
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}
	
	public List getPartnerExtract(String billToCode, String corpID) {
		//getHibernateTemplate().setMaxResults(100);
		getHibernateTemplate().setMaxResults(50000);
		return getHibernateTemplate().find("from Partner where partnerCode in(" + billToCode + ") and  (isPrivateParty=true or isAgent=true or isAccount=true or isVendor=true or isCarrier=true) ");
	}

	public List<Partner> findByNetworkPartnerCode(String networkPartnerCode, String sessionCorpID, String lastName, String searchPartnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String aliasName, String vanlineCode) {
		getHibernateTemplate().setMaxResults(1000);
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,corpid,group_concat(partnercode) from partnerpublic where agentparent != '' and  agentparent is not null and agentparent !=partnercode and status ='Approved' and partnertype in ('CMM','DMM') group by agentparent order by agentparent";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list(); 
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[2] !=null){
		String partnercode = row[2].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[0].toString().trim(), codeList);
		}
		List partnerList = new ArrayList();
		if(networkPartnerCode!=null && !networkPartnerCode.equalsIgnoreCase("")){
			partnerList = this.getSession().createSQLQuery("(select partnercode from partnerpublic where agentparent='"+networkPartnerCode+"' and partnertype in ('CMM','DMM') ) union  (select  agentparent from partnerpublic where partnercode='"+networkPartnerCode+"' and partnertype in ('CMM','DMM'))").list();
		}else{
			partnerList = this.getSession().createSQLQuery("select partnercode from partnerpublic where partnertype in ('CMM','DMM')").list();
		}
		String s = "";
		if (partnerList.isEmpty()) {
			s = "";
		} else {
			String partnerCode=new String();
			Iterator jobListIterator=partnerList.iterator(); 
			while(jobListIterator.hasNext()){
				partnerCode=(String)jobListIterator.next();
				if(childAgentCodeMap!=null && childAgentCodeMap.containsKey(partnerCode.trim())){
				List tempAgentCode=childAgentCodeMap.get(partnerCode.trim());
				if(tempAgentCode!=null && !tempAgentCode.isEmpty()){
					Iterator iterator=tempAgentCode.iterator(); 
					while(iterator.hasNext()){
					String childCode =(String)iterator.next();
					if(!(s.lastIndexOf("'") == s.length() - 1))
						s=s+"','"+childCode+"','";
						else if(s.lastIndexOf("'") == s.length() - 1) 
					    s=s+childCode+"','";
					}
				}
				}
				if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+partnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+partnerCode+"','";
			
			}
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+networkPartnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+networkPartnerCode+"','";
		}
		List a = getHibernateTemplate().find("from Partner where partnerCode in ('" + s + "') and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + searchPartnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
		return a;
	}

	public List findByNetworkBillToName(String networkPartnerCode, String sessionCorpID, String networkBillToCode) {
		getHibernateTemplate().setMaxResults(1000);
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,corpid,group_concat(partnercode) from partnerpublic where agentparent != '' and  agentparent is not null and agentparent !=partnercode and status ='Approved' and partnertype in ('CMM','DMM') group by agentparent order by agentparent";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list(); 
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[2] !=null){
		String partnercode = row[2].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[0].toString().trim(), codeList);
		}
		
		List partnerList = this.getSession().createSQLQuery("(select partnercode from partnerpublic where agentparent='"+networkPartnerCode+"' and partnertype in ('CMM','DMM') ) union  (select  agentparent from partnerpublic where partnercode='"+networkPartnerCode+"' and partnertype in ('CMM','DMM'))").list();
		String s = "";
		if (partnerList.isEmpty()) {
			s = "";
		} else {
			String partnerCode=new String();
			Iterator jobListIterator=partnerList.iterator(); 
			while(jobListIterator.hasNext()){
				partnerCode=(String)jobListIterator.next();
				if(childAgentCodeMap!=null && childAgentCodeMap.containsKey(partnerCode.trim())){
				List tempAgentCode=childAgentCodeMap.get(partnerCode.trim());
				if(tempAgentCode!=null && !tempAgentCode.isEmpty()){
					Iterator iterator=tempAgentCode.iterator(); 
					while(iterator.hasNext()){
					String childCode =(String)iterator.next();
					if(!(s.lastIndexOf("'") == s.length() - 1))
						s=s+"','"+childCode+"','";
						else if(s.lastIndexOf("'") == s.length() - 1) 
					    s=s+childCode+"','";
					}
				}
				}
				if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+partnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+partnerCode+"','";
			
			}
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+networkPartnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+networkPartnerCode+"','";
		}
		List a = new ArrayList();
		if(s!=null && !s.equalsIgnoreCase("")){
			a = getHibernateTemplate().find("select lastName, status from Partner where partnerCode in ('" + s + "') and partnerCode='"+networkBillToCode+"' and partnertype in ('CMM','DMM')");
		}else{
			a = getHibernateTemplate().find("select lastName, status from Partner where partnerCode='"+networkBillToCode+"' and partnertype in ('CMM','DMM')");
		}
		return a;
	}
	public class partnerDetailsList{
		private Object partnercode;
		private Object lastname;
		private Object billingcountry;
		private Object billingstate;
		private Object billingcity;
		private Object partnerType;
		private Object bookingAgentCode;
		private Object agentClassification;
		private Object aliasName;
		public Object getPartnercode() {
			return partnercode;
		}
		public void setPartnercode(Object partnercode) {
			this.partnercode = partnercode;
		}
		public Object getLastname() {
			return lastname;
		}
		public void setLastname(Object lastname) {
			this.lastname = lastname;
		}
		public Object getBillingcountry() {
			return billingcountry;
		}
		public void setBillingcountry(Object billingcountry) {
			this.billingcountry = billingcountry;
		}
		public Object getBillingstate() {
			return billingstate;
		}
		public void setBillingstate(Object billingstate) {
			this.billingstate = billingstate;
		}
		public Object getBillingcity() {
			return billingcity;
		}
		public void setBillingcity(Object billingcity) {
			this.billingcity = billingcity;
		}
		public Object getPartnerType() {
			return partnerType;
		}
		public void setPartnerType(Object partnerType) {
			this.partnerType = partnerType;
		}
		public Object getBookingAgentCode() {
			return bookingAgentCode;
		}
		public void setBookingAgentCode(Object bookingAgentCode) {
			this.bookingAgentCode = bookingAgentCode;
		}
		public Object getAgentClassification() {
			return agentClassification;
		}
		public void setAgentClassification(Object agentClassification) {
			this.agentClassification = agentClassification;
		}
		public Object getAliasName() {
			return aliasName;
		}
		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}
	}

	public List findNetworkPartnerDetailsForAutoComplete(String partnerName,String networkPartnerCode, String sessionCorpID) {
		List partnerDetailList1=new ArrayList();
		getHibernateTemplate().setMaxResults(1000);
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,corpid,group_concat(partnercode) from partnerpublic where agentparent != '' and  agentparent is not null and agentparent !=partnercode and status ='Approved' and partnertype in ('CMM','DMM') group by agentparent order by agentparent";
		//System.out.print("\n\n\n query"+query);
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list(); 
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[2] !=null){
		String partnercode = row[2].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[0].toString().trim(), codeList);
		}
		//System.out.print("\n\n\n (select partnercode from partnerpublic where agentparent='"+networkPartnerCode+"' and partnertype in ('CMM','DMM') ) union  (select  agentparent from partnerpublic where partnercode='"+networkPartnerCode+"' and partnertype in ('CMM','DMM'))");
		List partnerList = this.getSession().createSQLQuery("(select partnercode from partnerpublic where agentparent='"+networkPartnerCode+"' and partnertype in ('CMM','DMM') ) union  (select  agentparent from partnerpublic where partnercode='"+networkPartnerCode+"' and partnertype in ('CMM','DMM'))").list();
		String s = "";
		if (partnerList.isEmpty()) {
			s = "";
		} else {
			String partnerCode=new String();
			Iterator jobListIterator=partnerList.iterator(); 
			while(jobListIterator.hasNext()){
				partnerCode=(String)jobListIterator.next();
				if(childAgentCodeMap!=null && childAgentCodeMap.containsKey(partnerCode.trim())){
				List tempAgentCode=childAgentCodeMap.get(partnerCode.trim());
				if(tempAgentCode!=null && !tempAgentCode.isEmpty()){
					Iterator iterator=tempAgentCode.iterator(); 
					while(iterator.hasNext()){
					String childCode =(String)iterator.next();
					if(!(s.lastIndexOf("'") == s.length() - 1))
						s=s+"','"+childCode+"','";
						else if(s.lastIndexOf("'") == s.length() - 1) 
					    s=s+childCode+"','";
					}
				}
				}
				if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+partnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+partnerCode+"','";
			
			}
			if(!(s.lastIndexOf("'") == s.length() - 1))
				s=s+"','"+networkPartnerCode+"','";
				else if(s.lastIndexOf("'") == s.length() - 1) 
			    s=s+networkPartnerCode+"','";
		}
		//System.out.print("\n\n\n s"+s);
		//System.out.print("\n\n\n select distinct(partnercode),lastname,billingcountry,billingstate,billingcity,concat(if(isAccount is true,'Account',if(isAgent is true,'Agent',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,'' from Partner where partnerCode in ('" + s + "') ");
		List partnerDetailList=this.getSession().createSQLQuery("select distinct(partnercode),lastname,billingcountry,billingstate,billingcity,concat(if(isAccount is true,'Account',if(isAgent is true,'Agent',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,'',agentClassification, aliasName from partner where partnerCode in ('" + s + "') and (lastname like '"+ partnerName.replaceAll("'", "''")+ "%' or aliasName like'" +partnerName.replaceAll("'", "''") + "%') and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID) +"','"  +sessionCorpID + "') ").list();
		partnerDetailsList dto = null;
		Iterator it = partnerDetailList.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new partnerDetailsList();
			dto.setPartnercode(row[0]);
			dto.setLastname(row[1]);
			dto.setBillingcountry(row[2]);
			dto.setBillingstate(row[3]);
			dto.setBillingcity(row[4]);
			dto.setPartnerType(row[5]);
			dto.setBookingAgentCode(row[6]);
			dto.setAgentClassification(row[7]);
			dto.setAliasName(row[8]);
			partnerDetailList1.add(dto);
		}
		return partnerDetailList1; 
	}
	public String codeCheckPartner(String partnerCode, String corpId,String partnerType) {
		try {
			String str="";
			String query="";
			if(partnerType.equals("PA")){
			query="select concat(id,'~',partnerCode) from partner where (isAccount is true or isAgent is true) and status='Approved' and partnerCode='"+partnerCode+"' and corpId='"+corpId+"'";
			}
			if(partnerType.equals("BA")){
				query="select concat(id,'~',partnerCode) from partner where (isAgent is true) and status='Approved' and partnerCode='"+partnerCode+"' and corpId='"+corpId+"'";
				}
			List al=this.getSession().createSQLQuery(query).list();
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
				str=al.get(0).toString().trim();
			}
			return str;
			
		} catch (Exception e) {
			  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.error("Error executing query "+ e.getStackTrace()[0]);
	    	 e.printStackTrace();
		}
		return null;
	}

	public List getPartnerPopupListWithBookingAgentSet(String partnerType, String corpID, String lastName, String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String customerVendor, String vanlineCode, Boolean cmmDmmFlag) {
		List list3 = new ArrayList();
		String bookingAgentSet="";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
    	List userList=new ArrayList();
    	try {
		if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
    		List partnerCorpIDList = new ArrayList(); 
				HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
				if(valueMap!=null){
					Iterator mapIterator = valueMap.entrySet().iterator();
					while (mapIterator.hasNext()) {
						Map.Entry entry = (Map.Entry) mapIterator.next();
						String key = (String) entry.getKey(); 
					    partnerCorpIDList= (List)entry.getValue(); 
					}
				}
				
    	Iterator listIterator= partnerCorpIDList.iterator();
    	while (listIterator.hasNext()) {
    		String code=listIterator.next().toString();
    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
    		list3.add("'" + code + "'");	
    		}
    		//list1.add("'" + listIterator.next().toString() + "'");	
    	}
    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
    	}
    	}catch (Exception e) {
			  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.error("Error executing query "+ e.getStackTrace()[0]);
	    	 e.printStackTrace();
		}
    	String bookingAgentCondision="  ";
    	if (bookingAgentSet != null && !bookingAgentSet.equals("") ) {
    		bookingAgentCondision="  and partnerCode in("+ bookingAgentSet + ")   ";
    	}
		getHibernateTemplate().setMaxResults(100);
		if (!partnerType.equals("")) {
			if(!vanlineCode.equalsIgnoreCase("")){
				String query = " select p.partnerCode,p.lastName,p.billingAddress1," +
						       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
						       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
						       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
						       " p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
						       " from partnervanlineref n,partner p where"+					
						       " n.partnerCode=p.partnerCode and p.isAgent=true and p.status='Approved' and n.vanLineCode like '%"+vanlineCode+"%'"+
						       " and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
						       " AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
						       " AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
						       " AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
						       " AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
						       " AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
						       " AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
						       " AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' " +bookingAgentCondision;					
			List list = getSession().createSQLQuery(query).list();
			popupList = converObjToDTO(list);
			}else{
			popupList = getHibernateTemplate().find(
					"from Partner where isAgent=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + extReference.replaceAll("'", "''") + "%' "+bookingAgentCondision);
			}
		}else{

			popupList = getHibernateTemplate().find(
					"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' "+bookingAgentCondision);
		
		}
		return popupList;
	}
	
	public List findByOriginforClientDirective(String origin, String corpID, String billToCode){
		getHibernateTemplate().setMaxResults(5000);
		String query="";
		List<Object> preferredList=new ArrayList<Object>();
		String preferredtempIdList = "";
		query="select preferredAgentCode from preferredagentforaccount where corpId='"+corpID+"' and partnercode='"+billToCode+"' and status is true";
		List al=this.getSession().createSQLQuery(query).list();
		if(al.size()>0 && !al.isEmpty() && al!=null){
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			if(preferredtempIdList.equals("")){
				preferredtempIdList = "'"+itr.next().toString()+"'";
			}
			else{
				preferredtempIdList = preferredtempIdList+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		if(preferredtempIdList.equals("")){
		preferredtempIdList="'"+preferredtempIdList+"'";
		}
		// 			   1         2          3           4         5      6
		query="select id, partnerCode,partnerPrefix,firstName,lastName,aliasName,"+
		//               7                    8                 9              10
					" terminalAddress1,terminalAddress2,terminalAddress3,terminalAddress4,"+
		//                11                   12              13           14          15           16
					" terminalCountryCode,terminalCountry,terminalCity,terminalState,terminalZip,terminalEmail,"+
		//                17                 18               19               20             21
					" billingCountryCode,billingAddress1,billingAddress2,billingAddress3,billingAddress4,"+
		//                22            23              24          25         26      27
					" billingCity,billingCountry,billingEmail,billingState,billingZip,status,"+
		//                28                       29
					" agentClassification,if(partnerCode in ("+preferredtempIdList+"),'Y','N') as preferedagentType "+
					" from partner where corpId='"+corpID+"' and isAgent is true and status='Approved'"+
					" AND (billingCountryCode='"+origin+"' OR billingCountryCode='' OR billingCountryCode is null)"+
					" group by 2 order by 29 desc limit 100";
		popupList = this.getSession().createSQLQuery(query).list();
		Iterator it=popupList.iterator();
		PartnerClassifiedDTO dto = null;
		  while(it.hasNext())
		  {
			  Object [] row=(Object[])it.next(); 
			  dto = new PartnerClassifiedDTO();
			  if(row[0] != null){
				  dto.setId(row[0]);
			  }if(row[1] != null){
				  dto.setPartnerCode(row[1]);
			  }
			  if(row[2] != null){
				  dto.setPartnerPrefix(row[2]);
			  }
			  if(row[3] != null){
				  dto.setFirstName(row[3]);
			  }
			  if(row[4] != null){
				  dto.setLastName(row[4]);
			  }
			  if(row[5] != null){
				  dto.setAliasName(row[5]);
			  }
			  if(row[6] != null){
				  dto.setTerminalAddress1(row[6]);
			  }
			  if(row[7] != null){
				  dto.setTerminalAddress2(row[7]);
			  }
			  if(row[8] != null){
				  dto.setTerminalAddress3(row[8]);
			  }
			  if(row[9] != null){
				  dto.setTerminalAddress4(row[9]);
			  }
			  if(row[10] != null){
				  dto.setTerminalCountryCode(row[10]);
			  }
			  if(row[11] != null){
				  dto.setTerminalCountry(row[11]);
			  }
			  if(row[12] != null){
				  dto.setTerminalCity(row[12]);
			  }
			  if(row[13] != null){
				  dto.setTerminalState(row[13]);
			  }
			  if(row[14] != null){
				  dto.setTerminalZip(row[14]);
			  }
			  if(row[15] != null){
				  dto.setTerminalEmail(row[15]);
			  }
			  if(row[16] != null){
				  dto.setBillingCountryCode(row[16]);
			  }
			  if(row[17] != null){
				  dto.setBillingAddress1(row[17]);
			  }
			  if(row[18] != null){
				  dto.setBillingAddress2(row[18]);
			  }
			  if(row[19] != null){
				  dto.setBillingAddress3(row[19]);
			  }
			  if(row[20] != null){
				  dto.setBillingAddress4(row[20]);
			  }
			  
			  if(row[21] != null){
				  dto.setBillingCity(row[21]);
			  }
			  if(row[22] != null){
				  dto.setBillingCountry(row[22]);
			  }
			  if(row[23] != null){
				  dto.setBillingEmail(row[23]);
			  }if(row[24] != null){
				  dto.setBillingState(row[24]);
			  }
			  if(row[25] != null){
				  dto.setBillingZip(row[25]);
			  }if(row[26] != null){
				  dto.setStatus(row[26]);
			  }if(row[27] != null){
				  dto.setAgentClassification(row[27]);
			  }
			  if(row[28] != null){
				  dto.setPreferedagentType(row[28]);
			  }
			  
			  preferredList.add(dto);
		  }
		
		 return preferredList;
	}
	
	public List findPartnerDetailsForAutoCompleteNew(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag,String billToCode){
		try {
			String query="";
			ArrayList preferredList=new ArrayList();
			List partnerDetailList1=new ArrayList();
			String preferredtempIdList = "";
			query="select preferredAgentCode from preferredagentforaccount where corpId='"+corpid+"' and  status is true and partnercode='"+billToCode+"' and preferredAgentName like'"+ partnerName.replaceAll("'", "''")+ "%' ";
			List al=this.getSession().createSQLQuery(query).list();
			if(al.size()>0 && !al.isEmpty() && al!=null){
			Iterator itr = al.iterator();
			while(itr.hasNext()){
				if(preferredtempIdList.equals("")){
					preferredtempIdList = "'"+itr.next().toString()+"'";
				}
				else{
					preferredtempIdList = preferredtempIdList+","+"'"+itr.next().toString()+"'";
				}
			}
			}
			
			if(preferredtempIdList.equals("")){
				preferredtempIdList="'"+preferredtempIdList+"'";
			}
			
			 query="select  distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAccount is true,'Account',if(isAgent is true,'Agent',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification,if(p.partnercode in ("+preferredtempIdList+"),'Y','N') as preferedagentType from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  (p.lastname like'"
					+ partnerName.replaceAll("'", "''")
					+ "%' or p.aliasName like'"
					+ partnerName.replaceAll("'", "''")
					+ "%')and p.corpID in ('"
					+ hibernateUtil.getParentCorpID(corpid)
					+ "','"
					+ corpid
					+ "') "+partnerNameType+" and p.status='Approved' order by 9 desc limit 100 ";
			/*query=query+" union ";
			query=query+" select  distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAccount is true,'Account',if(isAgent is true,'Agent',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification,'Y' as preferedagentType from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  "
					+ "(p.lastname like'"+ partnerName.replaceAll("'", "''")+ "%' or p.aliasName like'"+ partnerName.replaceAll("'", "''")
				+ "%') and p.corpID in ('"
				+ hibernateUtil.getParentCorpID(corpid)
				+ "','"
				+ corpid
				+ "') "+partnerNameType+" and p.status='Approved' and (p.partnertype not in('CMM','DMM','KGA') or p.partnertype is NULL or p.partnertype='') "
						+ "and partnercode in ("+preferredtempIdList+")  order by partnercode limit 100 ";*/
			List partnerDetailList=this.getSession().createSQLQuery(query).list();
			autoPreferredClientDirectiveDTO dto = null;
			Iterator it = partnerDetailList.iterator();
			while (it.hasNext()){
				Object [] newrow= (Object [])it.next();
				dto=new autoPreferredClientDirectiveDTO();
				if(newrow[0] != null){
				dto.setPartnerCode(newrow[0]);
				}
				if(newrow[1] != null){
				dto.setLastName(newrow[1]);
				}
				if(newrow[2] != null){
				dto.setBillingCountry(newrow[2]);
				}
				if(newrow[3] != null){
				dto.setBillingState(newrow[3]);
				}
				if(newrow[4] != null){
				dto.setBillingCity(newrow[4]);
				}
				if(newrow[5] != null){
				dto.setPartnerType(newrow[5]);
				}
				if(newrow[6] != null){
				dto.setBookingagentCode(newrow[6]);
				}
				if(newrow[7] != null){
				dto.setAgentClassification(newrow[7]);
				}
				if(newrow[8] != null){
				dto.setPreferedagentType(newrow[8]);
				}
				partnerDetailList1.add(dto);
			}
			return partnerDetailList1;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List getPartnerVanLinePopupListNew(String partnerType, String corpID,String firstName, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String vanLineCODE, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean cmmDmmFlag,String billToCode) {
		List <Object> partnerList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(100);
		String query1 = "";
		String quer = "";
		String preferredtempIdList = "";
		quer="select preferredAgentCode from preferredagentforaccount where corpId='"+corpID+"' and partnercode='"+billToCode+"' and status is true";
		List al=this.getSession().createSQLQuery(quer).list();
		if(al.size()>0 && !al.isEmpty() && al!=null){
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			if(preferredtempIdList.equals("")){
				preferredtempIdList = "'"+itr.next().toString()+"'";
			}
			else{
				preferredtempIdList = preferredtempIdList+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		
		if(preferredtempIdList.equals("")){
			preferredtempIdList="'"+preferredtempIdList+"'";
		}
		String query = "select p.id, p.partnerCode, lastName, firstName,aliasName, isAgent, isAccount, isPrivateParty, isCarrier, isVendor, isOwnerOp, status, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCountryCode,billingCountryCode) as country, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalCity,billingCity) as city, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalState,billingState) as state, pv.vanLineCode, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress1,billingAddress1) as Add1, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalAddress2,billingAddress2) as Add2, " +
					"if((isAgent=true or isVendor=true or isCarrier=true),terminalZip,billingZip) as Zip,p.agentClassification,if(p.partnerCode in ("+preferredtempIdList+"),'Y','N') as preferedagentType " +
					"FROM partner p " +
					"LEFT OUTER JOIN partnervanlineref pv on  p.partnerCode = pv.partnerCode " +
					"WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%')AND( lastName like '%" + lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") +"%')" +
					"AND  p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
					// "AND  pv.vanLineCode like '%" + vanLineCODE.replaceAll("'", "''") + "%' " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode like '%" + countryCode.replaceAll("'", "''") + "%' OR terminalCountryCode='' OR terminalCountryCode is null, billingCountryCode like '%" + countryCode.replaceAll("'", "''") + "%' OR billingCountryCode='' OR billingCountryCode is null)) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountry like '%" + country.replaceAll("'", "''") + "%', billingCountry like '%" + country.replaceAll("'", "''") + "%')) " +
					"AND (if(isAgent=true or isVendor=true or isCarrier=true, terminalState like '%" + stateCode.replaceAll("'", "''") + "%', billingState like '%" + stateCode.replaceAll("'", "''") + "%')) " +
					"AND status = 'Approved' AND p.corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') ";
		
		if(vanLineCODE != null && vanLineCODE.length()>0){
			query = query+ "AND  pv.vanLineCode like '%" + vanLineCODE.replaceAll("'", "''") + "%' ";
		}
		if(isPP){
			query1 = query1+",isPrivateParty=true";
		}
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		if(isVndr){
			query1 = query1+",isVendor=true";
		}
		if(isCarr){
			query1 = query1+",isCarrier=true";
		}
		if(isAcc){
			query1 = query1+",isAccount=true";
		}
		if(isOO){
			query1 = query1+",isOwnerOp=true";
		}
		
		if(isPP || isAgt || isVndr || isCarr || isAcc || isOO){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1+" group by 2 order by  21 desc limit 100";
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd).concat(" group by 2 order by  21 desc limit 100");
			}	
		}else{
			query += " AND isAgent = false AND isVendor = false AND isCarrier= false AND isPrivateParty = false AND isAccount = false AND isOwnerOp = false  group by 2 order by  20 desc limit 100";
		}
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list();
		
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			
			dto.setId(row[0]);
			dto.setPartnerCode(row[1]);
			dto.setLastName(row[2]);
			
			if(row[3] == null){
				dto.setFirstName("");
			}else{
				dto.setFirstName(row[3]);
			}
			if(row[4] == null){
				dto.setAliasName("");
			}else{
				dto.setAliasName(row[4]);
			}
			
			if(row[5]==null){
				dto.setIsAgent("");
			}else{
				dto.setIsAgent(row[5]);
			}
			
			if(row[6]==null){
				dto.setIsAccount("");
			}else{
				dto.setIsAccount(row[6]);
			}
			
			if(row[7]==null){
				dto.setIsPrivateParty("");
			}else{
				dto.setIsPrivateParty(row[7]);
			}
			
			if(row[8]==null){
				dto.setIsCarrier("");
			}else{
				dto.setIsCarrier(row[8]);
			}
			
			if(row[9]==null){
				dto.setIsVendor("");
			}else{
				dto.setIsVendor(row[9]);
			}
			
			if(row[10]==null){
				dto.setIsOwnerOp("");
			}else{
				dto.setIsOwnerOp(row[10]);
			}
			
			if(row[11]==null){
				dto.setStatus("");
			}else{
				dto.setStatus(row[11]);
			}
			
			if(row[12] == null){
				dto.setCountryName("");
			}else{
				dto.setCountryName(row[12]);
			}
			
			if(row[13] == null){
				dto.setCityName("");
			}else{
				dto.setCityName(row[13]);
			}
			
			if(row[14] == null){
				dto.setStateName("");
			}else{
				dto.setStateName(row[14]);
			}
			
			if(row[15] == null){
				dto.setVanLineCode("");
			}else{
				dto.setVanLineCode(row[15]);
			}
			
			if(row[16] == null){
				dto.setAdd1("");
			}else{
				dto.setAdd1(row[16]);
			}
				
			if(row[17] == null){
				dto.setAdd2("");
			}else{
				dto.setAdd2(row[17]);
			}
			
			if(row[18] == null){
				dto.setZip("");
			}else{
				dto.setZip(row[18]);
			}
			if(row[19] == null){
				dto.setAgentClassification("");;
			}else{
				dto.setAgentClassification(row[19]);
			}
			if(row[20] != null){
				dto.setPreferedagentType(row[20]);
			}
			
			partnerList.add(dto);
		}
		
		return partnerList;
	}
	
	
	public List getPartnerPopupListNew(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String billToCode) {
		getHibernateTemplate().setMaxResults(100);
		String preferredtempIdList="";
		popupList=new ArrayList();
		String q1="";
		if (!partnerType.equals("")) {
			if (partnerType.equals("RSKY")) {
				String query = "select n.agentPartnerCode,n.agentName,p.billingAddress1,p.billingCountryCode,p.billingState,p.billingCity,p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
								" from networkpartners n,partner p where"+
								" n.agentpartnerCode=p.partnerCode "+
								" and n.agentPartnerCode like '%"+partnerCode+"%' and n.agentName like '%"+lastName+"%' AND  aliasName like '%" + aliasName.replaceAll("'", "''") + "%' and p.billingCountryCode like '%"+billingCountryCode+"%' and p.billingCountry like '%"+billingCountry+"%' and"+
								" p.billingState like '%"+billingStateCode+"%' and p.billingCity like '%%'"+
								" and p.corpID='"+corpID+"'"+
								" group by p.partnerCode";
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
			}else if (partnerType.equals("PP")) {
				 q1="from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%' ) AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ";
				popupList = getHibernateTemplate().find(q1);
						
			}else if (partnerType.equals("AC")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)");
				}else if (partnerType.equals("VN")) {
				if(customerVendor.equals("true")){
					popupList = getHibernateTemplate().find(
							"from Partner where isVendor=true and status='Approved' and typeOfVendor='Agent Broker' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
				}else{
				popupList = getHibernateTemplate().find(
						"from Partner where isVendor=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%'AND (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null)  ");
			}
			}else if (partnerType.equals("CR")) {
				if(corpID.equalsIgnoreCase("TSFT")){
					popupList = getHibernateTemplate().find(
							"from PartnerPublic where isCarrier=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
				}else{
					popupList = getHibernateTemplate().find(
							"from Partner where isCarrier=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
				}
				
			}else if (partnerType.equals("OO")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isOwnerOp=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND (aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) ");
			}else if (partnerType.equals("AG")) {
				String quer="select preferredAgentCode from preferredagentforaccount where corpId='"+corpID+"' and partnercode='"+billToCode+"' and status is true";
				List al=this.getSession().createSQLQuery(quer).list();
				if(al.size()>0 && !al.isEmpty() && al!=null){
				Iterator itr = al.iterator();
				while(itr.hasNext()){
					if(preferredtempIdList.equals("")){
						preferredtempIdList = "'"+itr.next().toString()+"'";
					}
					else{
						preferredtempIdList = preferredtempIdList+","+"'"+itr.next().toString()+"'";
					}
				}
				}
				
				if(preferredtempIdList.equals("")){
					preferredtempIdList="'"+preferredtempIdList+"'";
				}
				
				
				if(!vanlineCode.equalsIgnoreCase("")){
					String query = " select p.partnerCode,p.lastName,p.billingAddress1," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
							       " p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification,if(p.partnerCode in ("+preferredtempIdList+"),'Y','N') as preferedagentType "+
							       " from partnervanlineref n,partner p where"+					
							       " n.partnerCode=p.partnerCode and p.isAgent=true and p.status='Approved' and n.vanLineCode like '%"+vanlineCode+"%'"+
							       " and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
							       " AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
							       " AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
							       " AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
							       " AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
							       " AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
							       " AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
							       " AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' group by 2 order by 29 desc limit 100 ";					
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTONew(list);
				}else{
				q1=" select id, partnerCode,partnerPrefix,firstName,lastName,aliasName,"+
					" terminalAddress1,terminalAddress2,terminalAddress3,terminalAddress4,"+
					" terminalCountryCode,terminalCountry,terminalCity,terminalState,terminalZip,terminalEmail,"+
					" billingCountryCode,billingAddress1,billingAddress2,billingAddress3,billingAddress4,"+
					" billingCity,billingCountry,billingEmail,billingState,billingZip,status,"+
					" agentClassification,if(partnerCode in ("+preferredtempIdList+"),'Y','N') as preferedagentType from partner where isAgent=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + extReference.replaceAll("'", "''") + "%'  and corpID in ('"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')  group by 2 order by 29 desc limit 100 ";
				List l1 = this.getSession().createSQLQuery(q1).list();
				Iterator it=l1.iterator();
				PartnerClassifiedDTO dto = null;
				  while(it.hasNext())
				  {
					  Object [] row=(Object[])it.next(); 
					  dto = new PartnerClassifiedDTO();
					  if(row[0] != null){
						  dto.setId(row[0]);
					  }if(row[1] != null){
						  dto.setPartnerCode(row[1]);
					  }
					  if(row[2] != null){
						  dto.setPartnerPrefix(row[2]);
					  }
					  if(row[3] != null){
						  dto.setFirstName(row[3]);
					  }
					  if(row[4] != null){
						  dto.setLastName(row[4]);
					  }
					  if(row[5] != null){
						  dto.setAliasName(row[5]);
					  }
					  if(row[6] != null){
						  dto.setTerminalAddress1(row[6]);
					  }
					  if(row[7] != null){
						  dto.setTerminalAddress2(row[7]);
					  }
					  if(row[8] != null){
						  dto.setTerminalAddress3(row[8]);
					  }
					  if(row[9] != null){
						  dto.setTerminalAddress4(row[9]);
					  }
					  if(row[10] != null){
						  dto.setTerminalCountryCode(row[10]);
					  }
					  if(row[11] != null){
						  dto.setTerminalCountry(row[11]);
					  }
					  if(row[12] != null){
						  dto.setTerminalCity(row[12]);
					  }
					  if(row[13] != null){
						  dto.setTerminalState(row[13]);
					  }
					  if(row[14] != null){
						  dto.setTerminalZip(row[14]);
					  }
					  if(row[15] != null){
						  dto.setTerminalEmail(row[15]);
					  }
					  if(row[16] != null){
						  dto.setBillingCountryCode(row[16]);
					  }
					  if(row[17] != null){
						  dto.setBillingAddress1(row[17]);
					  }
					  if(row[18] != null){
						  dto.setBillingAddress2(row[18]);
					  }
					  if(row[19] != null){
						  dto.setBillingAddress3(row[19]);
					  }
					  if(row[20] != null){
						  dto.setBillingAddress4(row[20]);
					  }
					  
					  if(row[21] != null){
						  dto.setBillingCity(row[21]);
					  }
					  if(row[22] != null){
						  dto.setBillingCountry(row[22]);
					  }
					  if(row[23] != null){
						  dto.setBillingEmail(row[23]);
					  }if(row[24] != null){
						  dto.setBillingState(row[24]);
					  }
					  if(row[25] != null){
						  dto.setBillingZip(row[25]);
					  }if(row[26] != null){
						  dto.setStatus(row[26]);
					  }if(row[27] != null){
						  dto.setAgentClassification(row[27]);
					  }
					  if(row[28] != null){
						  dto.setPreferedagentType(row[28]);
					  }
					  
					  popupList.add(dto);
				  }
				}
			}else if (partnerType.equals("DE")) {
				popupList = getHibernateTemplate().find(
						"from Partner where isPrivateParty=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%'");
			}else if (partnerType.equals("DF")) {
				popupList = getHibernateTemplate().find(
						"from Partner where status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
			}
		} else {
			popupList = getHibernateTemplate().find(
					"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' ");
		}
		
				return popupList;
	}
	
	public  class autoPreferredClientDirectiveDTO
	 {		
	private Object partnerCode;
	private Object lastName;
	private Object billingCountry;
	private Object billingState;
	private Object billingCity;
	private Object partnerType;
	private Object bookingagentCode;
	private Object agentClassification;
	private Object preferedagentType;
	public Object getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(Object partnerCode) {
		this.partnerCode = partnerCode;
	}
	public Object getLastName() {
		return lastName;
	}
	public void setLastName(Object lastName) {
		this.lastName = lastName;
	}
	public Object getBillingState() {
		return billingState;
	}
	public void setBillingState(Object billingState) {
		this.billingState = billingState;
	}
	public Object getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(Object billingCity) {
		this.billingCity = billingCity;
	}
	public Object getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(Object partnerType) {
		this.partnerType = partnerType;
	}
	public Object getBookingagentCode() {
		return bookingagentCode;
	}
	public void setBookingagentCode(Object bookingagentCode) {
		this.bookingagentCode = bookingagentCode;
	}
	public Object getAgentClassification() {
		return agentClassification;
	}
	public void setAgentClassification(Object agentClassification) {
		this.agentClassification = agentClassification;
	}
	public Object getPreferedagentType() {
		return preferedagentType;
	}
	public void setPreferedagentType(Object preferedagentType) {
		this.preferedagentType = preferedagentType;
	}
	public Object getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(Object billingCountry) {
		this.billingCountry = billingCountry;
	}
	 }
	
	public class PartnerClassifiedDTO implements ListLinkData {
		private Object id;
		private Object partnerCode;
		private Object lastName;
		private Object firstName;
		private Object aliasName;
		private Object status;
		private Object terminalCountryCode;		
		private Object terminalAddress1;
		private Object terminalAddress2;
		private Object terminalCity;
		private Object terminalZip;
		private Object terminalState;
		private Object terminalCountry;
		private Object agentClassification;
		private Object preferedagentType;
		private Object billingCountryCode;
		private Object billingState;
		private Object billingAddress1;
		private Object billingAddress2;
		private Object billingZip;
		private Object billingCountry;
		private Object billingCity;
		private Object partnerPrefix;
		private Object terminalAddress3;
		private Object terminalAddress4;
		private Object billingAddress3;
		private Object billingAddress4;
		private Object terminalEmail;
		private Object billingEmail;
		

		
		@Transient
		public String getListCode() {
			return (String) partnerCode;
		}

		@Transient
		public String getListDescription() {
			String lName = (String) this.lastName;
			return lName;
		}		
		
		@Transient
		public String getListSecondDescription() {
			return "";
		}

		@Transient
		public String getListThirdDescription() {
			return "";
		}

		@Transient
		public String getListFourthDescription() {
			return "";
		}

		@Transient
		public String getListFifthDescription() {
			return "";
		}

		@Transient
		public String getListSixthDescription() {
			return "";
		}
		@Transient
		public String getListEigthDescription() {
			return null;
		}
		@Transient
		public String getListNinthDescription() {
			return null;
		}
		@Transient
		public String getListSeventhDescription() {
			return null;
		}
		@Transient
		public String getListTenthDescription() {
			return null;
		}		
		
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		
		public Object getTerminalCountryCode() {
			return terminalCountryCode;
		}

		public void setTerminalCountryCode(Object terminalCountryCode) {
			this.terminalCountryCode = terminalCountryCode;
		}

		public Object getAliasName() {
			return aliasName;
		}

		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}

		
		public Object getTerminalAddress1() {
			return terminalAddress1;
		}

		public void setTerminalAddress1(Object terminalAddress1) {
			this.terminalAddress1 = terminalAddress1;
		}

		public Object getTerminalAddress2() {
			return terminalAddress2;
		}

		public void setTerminalAddress2(Object terminalAddress2) {
			this.terminalAddress2 = terminalAddress2;
		}

		public Object getTerminalCity() {
			return terminalCity;
		}

		public void setTerminalCity(Object terminalCity) {
			this.terminalCity = terminalCity;
		}

		public Object getTerminalZip() {
			return terminalZip;
		}

		public void setTerminalZip(Object terminalZip) {
			this.terminalZip = terminalZip;
		}

		public Object getTerminalState() {
			return terminalState;
		}

		public void setTerminalState(Object terminalState) {
			this.terminalState = terminalState;
		}

		public Object getTerminalCountry() {
			return terminalCountry;
		}

		public void setTerminalCountry(Object terminalCountry) {
			this.terminalCountry = terminalCountry;
		}

		public Object getAgentClassification() {
			return agentClassification;
		}

		public void setAgentClassification(Object agentClassification) {
			this.agentClassification = agentClassification;
		}

		public Object getPreferedagentType() {
			return preferedagentType;
		}

		public void setPreferedagentType(Object preferedagentType) {
			this.preferedagentType = preferedagentType;
		}

		public Object getBillingCountryCode() {
			return billingCountryCode;
		}

		public void setBillingCountryCode(Object billingCountryCode) {
			this.billingCountryCode = billingCountryCode;
		}

		public Object getBillingState() {
			return billingState;
		}

		public void setBillingState(Object billingState) {
			this.billingState = billingState;
		}

		public Object getBillingAddress1() {
			return billingAddress1;
		}

		public void setBillingAddress1(Object billingAddress1) {
			this.billingAddress1 = billingAddress1;
		}

		public Object getBillingAddress2() {
			return billingAddress2;
		}

		public void setBillingAddress2(Object billingAddress2) {
			this.billingAddress2 = billingAddress2;
		}

		public Object getBillingZip() {
			return billingZip;
		}

		public void setBillingZip(Object billingZip) {
			this.billingZip = billingZip;
		}

		public Object getBillingCountry() {
			return billingCountry;
		}

		public void setBillingCountry(Object billingCountry) {
			this.billingCountry = billingCountry;
		}

		public Object getBillingCity() {
			return billingCity;
		}

		public void setBillingCity(Object billingCity) {
			this.billingCity = billingCity;
		}

		public Object getPartnerPrefix() {
			return partnerPrefix;
		}

		public void setPartnerPrefix(Object partnerPrefix) {
			this.partnerPrefix = partnerPrefix;
		}

		public Object getTerminalAddress3() {
			return terminalAddress3;
		}

		public void setTerminalAddress3(Object terminalAddress3) {
			this.terminalAddress3 = terminalAddress3;
		}

		public Object getTerminalAddress4() {
			return terminalAddress4;
		}

		public void setTerminalAddress4(Object terminalAddress4) {
			this.terminalAddress4 = terminalAddress4;
		}

		public Object getBillingAddress3() {
			return billingAddress3;
		}

		public void setBillingAddress3(Object billingAddress3) {
			this.billingAddress3 = billingAddress3;
		}

		public Object getBillingAddress4() {
			return billingAddress4;
		}

		public void setBillingAddress4(Object billingAddress4) {
			this.billingAddress4 = billingAddress4;
		}

		public Object getTerminalEmail() {
			return terminalEmail;
		}

		public void setTerminalEmail(Object terminalEmail) {
			this.terminalEmail = terminalEmail;
		}

		public Object getBillingEmail() {
			return billingEmail;
		}

		public void setBillingEmail(Object billingEmail) {
			this.billingEmail = billingEmail;
		}

	}
	
	
	public List converObjToDTONew(List list){
		Iterator it = list.iterator();
		List newList = new ArrayList();
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			Partner p = new Partner();
			if(row[0] != null){
				p.setPartnerCode(row[0].toString());
			}
			if(row[1] != null){
				p.setLastName(row[1].toString());
			}
			if(row[2] != null){
				p.setBillingAddress1(row[2].toString());
			}
			if(row[3] != null){
					p.setTerminalCountryCode(row[3].toString());
					p.setBillingCountryCode(row[3].toString());	
			}
			if(row[4] != null){
					p.setTerminalState(row[4].toString());
					p.setBillingState(row[4].toString());	
				
			}
			if(row[5] != null){
					p.setTerminalCity(row[5].toString());
					p.setBillingCity(row[5].toString());
			}
			if(row[6] != null){
				p.setStatus(row[6].toString());
			}
			if(row[7] != null){
				p.setExtReference(row[7].toString());
			}
			if(row[8] != null){
				p.setAliasName(row[8].toString());
			}
			if(row[10] != null){
				p.setId(Long.parseLong(row[10].toString()));
			}
			if(row[11] != null){
				p.setAgentClassification(row[11].toString());
			}
			if(row[12] != null){
				p.setPreferedagentType(row[12].toString());
			}
			newList.add(p);
		}
		return newList;
	}
	
	public List<Partner> vendorNameAutocompleteAjaxlist(String partnerNameAutoCopmlete) {
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from Partner where  status='Approved' and  lastName like'"+partnerNameAutoCopmlete+"%' ");
	}
	
	
	public List getAgentPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag,String agentGroup){
		if (!partnerType.equals("")) {
				if (partnerType.equals("AG")) {
				if(vanlineCode.equalsIgnoreCase("")){
					popupList = getHibernateTemplate().find(
							"select p from Partner p " +
							"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
							"where isAgent=true and status='Approved' " +
							"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
							//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
							"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%' ) AND terminalCountryCode like '%"
							+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ");
				}else{
					 String str="select p.partnerCode,p.lastName,p.billingAddress1," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
						        " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
								" p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification from partner p "+
								" left outer join partneraccountref pr on p.partnerCode=pr.partnerCode and (pr.companyDivision='" + compDiv + "') and pr.corpid='"+corpID+"' "+
								" left outer join partnervanlineref pv on p.partnerCode=pv.partnerCode and pv.corpID in ('TSFT','"+corpID+"') "+
								" where p.corpID in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and isAgent is true and status='Approved' "+
								" and (p.companyDivision='" + compDiv	+ "' or p.companyDivision is null or p.companyDivision='') "+
								" and (lastName like '%" + lastName.replaceAll("'", "''") + "%' or firstName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '" + lastName.replaceAll("'", "''") + "%') "+
					            " and (p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%') and (p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') "+
					            " and (terminalCountryCode like '%" + billingCountryCode.replaceAll("'", "''") + "%') and (terminalCountry like '%" + billingCountry.replaceAll("'", "''") + "%') "+
					            " and (terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%') "+
					            " and (extReference like '%"+extReference.replaceAll("'", "''")+"%' or extReference is null) and (pv.vanlinecode like '%" + vanlineCode + "%') AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ";
					List list = getSession().createSQLQuery(str).list();
					popupList = converObjToDTO(list);
				}
			}
		} else {
			popupList = getHibernateTemplate().find(
					"select p from Partner p " +
					"LEFT JOIN p.partnerAccountRef pr with pr.companyDivision = '" + compDiv + "' " + 
					"where isAccount=true and status='Approved' " +
					"AND (p.companyDivision='" + compDiv	+ "' OR p.companyDivision IS NULL OR p.companyDivision='') " +
					//"OR r.companyDivision='" + compDiv	+ "' OR r.companyDivision IS NULL OR r.companyDivision='') " +
					"and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%' OR p.aliasName like '" + lastName.replaceAll("'", "''") + "%') AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ");
		}
		return popupList;
	}
	
	
	
	public List getAgentPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String agentGroup){
		getHibernateTemplate().setMaxResults(100);
		if (!partnerType.equals("")) {
			if (partnerType.equals("AG")) {
				if(!vanlineCode.equalsIgnoreCase("")){
					String query = " select p.partnerCode,p.lastName,p.billingAddress1," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCountryCode,p.billingCountryCode) as country," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalState,p.billingState) as state," +
							       " if((p.isAgent=true or p.isVendor=true or p.isCarrier=true),p.terminalCity,p.billingCity) as city," +
							       " p.status,p.extReference,p.aliasName,p.billingCountry,p.id,p.agentClassification "+
							       " from partnervanlineref n,partner p where"+					
							       " n.partnerCode=p.partnerCode and p.isAgent=true and p.status='Approved' and n.vanLineCode like '%"+vanlineCode+"%'"+
							       " and (p.lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  p.firstName like '%"+ lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") + "%') " +
							       " AND p.partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
							       " AND ( p.aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%') " +
							       " AND p.terminalCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' " +
							       " AND p.terminalCountry like '%" + billingCountry.replaceAll("'", "''")+ "%' " +
							       " AND p.terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' " +
							       " AND p.corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"') "+
							       " AND p.extReference like '%" + extReference.replaceAll("'", "''") + "%' AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ";					
				List list = getSession().createSQLQuery(query).list();
				popupList = converObjToDTO(list);
				}else{
				popupList = getHibernateTemplate().find(
						"from Partner where isAgent=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
						+ lastName.replaceAll("'", "''") + "%' or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
						+ billingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + billingCountry.replaceAll("'", "''")
						+ "%' AND terminalState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND extReference like '%" + extReference.replaceAll("'", "''") + "%' AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ");
				}
			}
		} else {
			popupList = getHibernateTemplate().find(
					"from Partner where isAccount=true and status='Approved' and (lastName like '%" + lastName.replaceAll("'", "''") + "%' OR  firstName like '%"
					+ lastName.replaceAll("'", "''") + "%'or aliasName like '%" + lastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' AND ( aliasName like '%" + aliasName.replaceAll("'", "''") + "%' or lastName like '%" + aliasName.replaceAll("'", "''") + "%') AND billingCountryCode like '%"
					+ billingCountryCode.replaceAll("'", "''") + "%' AND billingCountry like '%" + billingCountry.replaceAll("'", "''")
					+ "%' AND billingState like '%" + billingStateCode.replaceAll("'", "''") + "%' AND agentGroup like '%" + agentGroup.replaceAll("'", "''") + "%' ");
		}
		
				return popupList;
	}
	
	public String checkParnerCodeForInv(String billName, String sessionCorpID){
		String str ="";
		List al = getHibernateTemplate().find("select doNotInvoice from PartnerPrivate where partnerCode = '" + billName + "' and corpid = '" + sessionCorpID + "' ");
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
			str=al.get(0).toString().trim();
		}
		return str;
	}
	
	public List agentRatingList(String partnerCode, String corpID, String days) {
		String query="";
		query = " select  count(*),sum(OAEvaluation), 'Negative-OA' "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.originagentcode='" + partnerCode + "' "
				+ " and s.id = t.id and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.OAEvaluation is not null and s.OAEvaluation!='' and OAEvaluation<4 "
				+ " union "
				+ " select  count(*), sum(OAEvaluation),'Positive-OA' "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.originagentcode='" + partnerCode + "' "
				+ " and s.id = t.id and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.OAEvaluation is not null and s.OAEvaluation!='' and OAEvaluation>3 "
				+ " union "
				+ " select  count(*),sum(DAEvaluation), 'Negative-DA' "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.destinationagentcode='" + partnerCode + "' "
				+ " and s.id = t.id and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.DAEvaluation is not null and s.DAEvaluation!='' and DAEvaluation<4 "
				+ " union "
				+ " select  count(*), sum(DAEvaluation),'Positive-DA' "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.destinationagentcode='" + partnerCode + "' "
				+ " and s.id = t.id and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.DAEvaluation is not null and s.DAEvaluation!='' and DAEvaluation>3 ";
		return this.getSession().createSQLQuery(query).list();
	}
	
	public List getAgentRatingFeedbackList(String partnerCode, String days, String corpID) {
		List <Object> agentRatingFeedbackList = new ArrayList<Object>();

		String query="	select  year(t.deliveryA),s.OAEvaluation, s.feedback, s.shipNumber "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.originagentcode='" + partnerCode + "' "
				+ " and s.id = t.id  "
				+ " and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.OAEvaluation is not null and s.OAEvaluation!='' " ;
		List feedbackList = this.getSession().createSQLQuery(query).list();
		String query1= " select  year(t.deliveryA),s.DAEvaluation, s.feedback, s.shipNumber "
				+ " from serviceorder s, trackingstatus t "
				+ " where t.destinationagentcode='" + partnerCode + "' "
				+ " and s.id = t.id  "
				+ " and datediff(now(), t.deliverya) <= "+days+" "
				+ " and s.status not in ('CNCL','DWNLD') and s.corpid = '" + corpID + "' "
				+ " and s.DAEvaluation is not null and s.DAEvaluation!='' ";
		List feedbackList1 = this.getSession().createSQLQuery(query1).list();
		List feedBackFinalList = new ArrayList();
		feedBackFinalList.addAll(feedbackList);
		feedBackFinalList.addAll(feedbackList1);
		
		HibernateDTO partnerFeedback;
		Iterator it=feedBackFinalList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerFeedback=new HibernateDTO();
			partnerFeedback.setYear(obj[0]);
			partnerFeedback.setOaeValuation(obj[1]);
			partnerFeedback.setFeedback(obj[2]);
			partnerFeedback.setShipNumber(obj[3]);
			
			agentRatingFeedbackList.add(partnerFeedback);
		}
		
		return agentRatingFeedbackList;
	}

	public List synInvoiceMasBilltoPartnerExtract(String sysInvoiceDates, String corpID) {
		
		List <Object> partnerList = new ArrayList<Object>();
        //List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p2.creditTerms,if(pa.accountCrossReference is null ,'',pa.accountCrossReference) from partnerpublic p1, partnerprivate p2  INNER JOIN partneraccountref pa on p2.partnercode=pa.partnercode and pa.corpid IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and pa.refType='R' and pa.accountCrossReference is not null and pa.accountCrossReference !='', accountline a   where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and a.corpid = '"+corpID+"' and a.billtocode = p1.partnercode and p1.id=p2.partnerPublicId and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) and p2.billToExtractAccess =true  ").list();
		List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p2.creditTerms,p2.companyDivision from partnerpublic p1, partnerprivate p2, accountline a where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and a.corpid = '"+corpID+"'  and a.billtocode = p1.partnercode and p1.id=p2.partnerPublicId and (a.recinvoicenumber<>'' or  a.recinvoicenumber > 0 or a.recinvoicenumber is not null) and p2.billToExtractAccess =true  ").list();
		DTOSynInvoicePartner synInvoicePartnerDTO=null;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			synInvoicePartnerDTO=new DTOSynInvoicePartner();
			synInvoicePartnerDTO.setPartnerCode(obj[0]);
			synInvoicePartnerDTO.setLastName(obj[1]);
			synInvoicePartnerDTO.setPartnerPrefix(obj[2]);
			synInvoicePartnerDTO.setMiddleInitial(obj[3]);
			synInvoicePartnerDTO.setCreditTerms(obj[4]);
			synInvoicePartnerDTO.setCompanyDivision(obj[5]);
			//synInvoicePartnerDTO.setAccountCrossReference(obj[5]);
			partnerList.add(synInvoicePartnerDTO);
	}
		return partnerList;
	}

	public List synInvoiceMasPartnerVendorExtract(String sysInvoiceDates, String corpID) {

		
		List <Object> partnerList = new ArrayList<Object>();
        //List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p1.mailingAddress1,p1.mailingAddress2,p1.mailingCity,p1.mailingCountryCode,p1.mailingState,p1.mailingZip,p1.mailingPhone,p1.mailingFax,p1.billingAddress1,p1.billingAddress2,p1.billingCity,p1.billingCountryCode,p1.billingState,p1.billingZip,p1.billingPhone,p1.billingFax,p2.coordinator,p1.firstName ,p2.vatNumber,p2.classcode,p2.accountManager,p2.accountHolder,p1.billingEmail,p2.abbreviation,p2.creditTerms,if(pa.accountCrossReference is null ,'',pa.accountCrossReference) from partnerpublic p1 , partnerprivate p2 INNER JOIN partneraccountref pa on p2.partnercode=pa.partnercode and pa.corpid IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and pa.refType='P' and pa.accountCrossReference is not null and pa.accountCrossReference !='', accountline a  where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and a.corpid = '"+corpID+"'  and a.vendorCode = p1.partnercode and p1.id=p2.partnerPublicId and (a.invoicenumber<>'' or  a.invoicenumber > 0 or a.invoicenumber is not null) and p2.vendorExtractAccess =true ").list();
		List partnerDitailList = this.getSession().createSQLQuery("select distinct p1.partnerCode, p1.lastName,p1.partnerPrefix,p1.middleInitial,p1.mailingAddress1,p1.mailingAddress2,p1.mailingCity,p1.mailingCountryCode,p1.mailingState,p1.mailingZip,p1.mailingPhone,p1.mailingFax,p1.billingAddress1,p1.billingAddress2,p1.billingCity,p1.billingCountryCode,p1.billingState,p1.billingZip,p1.billingPhone,p1.billingFax,p2.coordinator,p1.firstName ,p2.vatNumber,p2.classcode,p2.accountManager,p2.accountHolder,p1.billingEmail,p2.abbreviation,p2.creditTerms,p2.companyDivision from partnerpublic p1, partnerprivate p2, accountline a  where p2.corpid  IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and a.corpid = '"+corpID+"' and a.vendorCode = p1.partnercode and p1.id=p2.partnerPublicId and (a.invoicenumber<>'' or  a.invoicenumber > 0 or a.invoicenumber is not null) and p2.vendorExtractAccess =true ").list();
		DTOSynInvoicePartner synInvoicePartnerDTO=null;
		Iterator it=partnerDitailList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			synInvoicePartnerDTO=new DTOSynInvoicePartner();
			synInvoicePartnerDTO.setPartnerCode(obj[0]);
			synInvoicePartnerDTO.setLastName(obj[1]);
			synInvoicePartnerDTO.setPartnerPrefix(obj[2]);
			synInvoicePartnerDTO.setMiddleInitial(obj[3]);
			synInvoicePartnerDTO.setMailingAddress1(obj[4]);
			synInvoicePartnerDTO.setMailingAddress2(obj[5]);
			synInvoicePartnerDTO.setMailingCity(obj[6]);
			synInvoicePartnerDTO.setMailingCountryCode(obj[7]);
			synInvoicePartnerDTO.setMailingState(obj[8]);
			synInvoicePartnerDTO.setMailingZip(obj[9]);
			synInvoicePartnerDTO.setMailingPhone(obj[10]);
			synInvoicePartnerDTO.setMailingFax(obj[11]);
			synInvoicePartnerDTO.setBillingAddress1(obj[12]);
			synInvoicePartnerDTO.setBillingAddress2(obj[13]);
			synInvoicePartnerDTO.setBillingCity(obj[14]);
			synInvoicePartnerDTO.setBillingCountryCode(obj[15]);
			synInvoicePartnerDTO.setBillingState(obj[16]);
			synInvoicePartnerDTO.setBillingZip(obj[17]);
			synInvoicePartnerDTO.setBillingPhone(obj[18]);
			synInvoicePartnerDTO.setBillingFax(obj[19]);
			synInvoicePartnerDTO.setCoordinator(obj[20]);
			synInvoicePartnerDTO.setFirstName(obj[21]);
			synInvoicePartnerDTO.setVatNumber(obj[22]);
			synInvoicePartnerDTO.setClasscode(obj[23]);
			synInvoicePartnerDTO.setAccountManager(obj[24]);
			synInvoicePartnerDTO.setAccountHolder(obj[25]);
			synInvoicePartnerDTO.setBillingEmail(obj[26]);  
			synInvoicePartnerDTO.setAbbreviation(obj[27]);
			synInvoicePartnerDTO.setCreditTerms(obj[28]);
			synInvoicePartnerDTO.setCompanyDivision(obj[29]);
			//synInvoicePartnerDTO.setAccountCrossReference(obj[29]);
			partnerList.add(synInvoicePartnerDTO);
	}
		return partnerList;
	
		
	}
	public int updateBillToExtractAccess(String sessionCorpID,List partnerList,  String userName) {

		 try {
			 String partnerCode = partnerList.toString();
			 partnerCode = partnerCode.replace("[", "");
			 partnerCode = partnerCode.replace("]", "");
			 int i= getHibernateTemplate().bulkUpdate("update PartnerPrivate set billToExtractAccess=false  , updatedBy='"+userName+"',updatedOn = now()    where  corpID='"+sessionCorpID+"'  and partnerCode in ("+partnerCode+") ");
			   return i;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
		}
		return 0;
	 
	}
	
	public int updateVendorExtractAccess(String sessionCorpID,List partnerList,  String userName) {

		 try {
			 String partnerCode = partnerList.toString();
			 partnerCode = partnerCode.replace("[", "");
			 partnerCode = partnerCode.replace("]", "");
			int i= getHibernateTemplate().bulkUpdate("update PartnerPrivate set vendorExtractAccess=false  , updatedBy='"+userName+"',updatedOn = now()    where  corpID='"+sessionCorpID+"'  and partnerCode in ("+partnerCode+") ");
			   return i;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
		}
		return 0;
	 
	}
	
	public Map<String, String> checkBilliToCode(String sessionCorpID){
	 	List list = new ArrayList();
		Map<String, String> partnerMap = new LinkedHashMap<String, String>(); 
	    try{	
			list = getSession() .createSQLQuery("select partnercode,partnertype from partnerpublic where corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"' ,'TSFT') and  partnertype in ('CMM','DMM')" )
			.list();
			String partnercode = "";
		      String partnertype = "";
		    Iterator iterator = list.iterator();
		     while (iterator.hasNext()) {
		      
		      Object[] row = (Object[]) iterator.next();
		     // if (row[0] != null) {
		        if ((row[0] != null) && (!row[0].toString().equals(""))){
		        	partnercode = row[0].toString();
		      }
		      //if (row[1] != null) {
		        if ((row[1] != null) && (!row[1].toString().equals(""))){
		        	partnertype = row[1].toString();
		      }else{
		    	  partnertype = "NAA";
		      } 
		      partnerMap.put(partnercode, partnertype);
		      
		     }
		    if(partnercode.equalsIgnoreCase("T18817"))
		    {
		     partnerMap.put("T18817","DMM");
		    }
		}catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
	return partnerMap;
 }
	 
	public List getOADACode(String partnerCode){
	
		try{
			return this.getSession().createSQLQuery("select concat(if((s.origincountrycode is null or s.origincountrycode =''),'NODATA',s.origincountrycode),'~',if((s.destinationcountrycode is null or s.destinationcountrycode=''),'NODATA',s.destinationcountrycode)) from serviceorder s,billing b where b.id=s.id and s.corpid='cwms'  and (b.billToCode='"+partnerCode+"' or b.billTo2Code='"+partnerCode+"' or b.privatePartyBillingCode='"+partnerCode+"') order by s.id desc limit 1").list();		
		}catch(Exception e){
			
			}
			return null;}

	
}
