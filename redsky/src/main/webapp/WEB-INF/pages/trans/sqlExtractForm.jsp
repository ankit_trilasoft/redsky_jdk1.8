<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title><fmt:message key="sqlExtractDetails.title" /></title>
<meta name="heading" content="<fmt:message key='sqlExtractDetails.heading'/>" />
<script language="JavaScript">
   function  checkLength(){
	var txt = document.forms['extractForm'].elements['sqlExtract.description'].value;
		if(txt.length >252){
			document.forms['extractForm'].elements['sqlExtract.description'].value = txt.substring(0,252);
			return false;
		}
	}
	
	function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==36); 
	} 
	function btntype1(targerelement){
		var a=document.forms['extractForm'].elements['btn'].value=targerelement.value;
		return confirmSubmit(a);
	}
	
function confirmSubmit(b)
{
	var email =  document.forms['extractForm'].elements['sqlExtract.email'].value;
	var emailArray = email.split(",");
	 if(document.forms['extractForm'].elements['sqlExtract.sqlQueryScheduler'].value=="M"){
		if(document.forms['extractForm'].elements['sqlExtract.perMonthScheduler'].value==""){
		alert("Plese select monthly scheduler");
		return false ;
		} 
		}
	 else if(document.forms['extractForm'].elements['sqlExtract.sqlQueryScheduler'].value=="W"){
		if(document.forms['extractForm'].elements['sqlExtract.perWeekScheduler'].value==""){
		alert("Plese select weekly scheduler");
		return false ;
		}}
	var query = document.forms['extractForm'].elements['sqlExtract.sqlText'].value;
			query = query.toLowerCase();
			
			if(query.indexOf("orderby")!=-1)
			{		
			alert('You cant specify orderby Clause in query')
			return false;
			}
			if(query.indexOf("groupby")!=-1)
			{		
			alert('You cant specify groupby Clause in query')
			return false;
			}
			if(query == ''){
				alert('Please insert the select query.');
				return false;
			}
			
			else if(query.substring(0,6) != 'select'){
				alert('Only select query can be executed.');
				return false;
			}
			else if (query.indexOf("Delete ") >=0 || query.indexOf("delete ") >=0){
				alert('Delete query can not be executed.');
				return false;
			}
			else if (query.indexOf("Update ") >=0 || query.indexOf("update ") >=0){
				alert('Update query can not be executed.');
				return false;
			}
			else if (query.indexOf("Insert ") >=0 || query.indexOf("insert ") >=0){
				alert('Insert query can not be executed.');
				return false;
			}
			else if(email!='' && !validatemialId(emailArray)){
		        alert('Please provide a valid email address');
		        return false;				
			}
			if(b=="Test Query"){
			
			document.forms['extractForm'].action = 'saveExtract.html';
			document.forms['extractForm'].submit();
			}
			if(b=="Save"){
			document.forms['extractForm'].action = 'saveExtract.html';
			document.forms['extractForm'].submit();
			}
			//var whereClause2=document.forms['extractForm'].elements['sqlExtract.whereClause2'].value;
			//var sqlText=document.forms['extractForm'].elements['sqlExtract.sqlText'].value;
			//var whereClause3=document.forms['extractForm'].elements['sqlExtract.whereClause3'].value;
			//var whereClause4=document.forms['extractForm'].elements['sqlExtract.whereClause4'].value;
			//var whereClause5=document.forms['extractForm'].elements['sqlExtract.whereClause5'].value;
			//var whereClause1= document.forms['extractForm'].elements['sqlExtract.whereClause1'].value;
			//var orderBy=document.forms['extractForm'].elements['sqlExtract.orderBy'].value;
			//var groupBy= document.forms['extractForm'].elements['sqlExtract.groupBy'].value;
			//var did=document.forms['extractForm'].elements['sqlExtract.id'].value;
			
			//location.href="saveExtract.html?id="+did+"&sqlText="+sqlText+"&whereClause1='"+whereClause1+"'&whereClause2='"+whereClause2+"'&whereClause3='"+whereClause3+"'&whereClause4= '"+whereClause4+"'&whereClause5= '"+whereClause5+"'&groupBy="+groupBy+"&orderBy="+orderBy;
					  		
}
function validatemialId(emailArray){
	var flag=true;
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	for (var i = 0; i < emailArray.length && flag; i++) {
	    var idInArray12 = emailArray[i];
	    if (!filter.test(idInArray12)) {
	    	flag=false;
	    }
	}
	return flag;
}
function  fieldRanking(){

var corpID=document.forms['extractForm'].elements['sqlExtract.whereClause1'].value
corpID=corpID.toLowerCase(); 
if(corpID=='corpid')
{
alert('corpid is not allowed to be in where Clause');
document.forms['extractForm'].elements['sqlExtract.whereClause1'].value=''
document.forms['extractForm'].elements['sqlExtract.whereClause1'].focus();
return false;
} 
checkfillEmail(document.forms['extractForm'].elements['sqlExtract.whereClause1']);
}
function  fieldRanking1()
{
	
	var corpID=document.forms['extractForm'].elements['sqlExtract.whereClause2'].value
	corpID=corpID.toLowerCase(); 
	if(corpID=='corpid')
	{
	alert('corpid is not allowed to be in where Clause');
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value=''
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].focus();
	return false;
	}
	if(document.forms['extractForm'].elements['sqlExtract.whereClause1'].value=='')
	{
	alert('Please select whereCaluse1 first');
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause1'].focus();
	return false;
	}
}
	
function  fieldRanking2()
{
	
	var corpID=document.forms['extractForm'].elements['sqlExtract.whereClause3'].value
	corpID=corpID.toLowerCase(); 
	if(corpID=='corpid')
	{
	alert('corpid is not allowed to be in where Clause');
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value=''
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].focus();
	return false;
	}
	if(document.forms['extractForm'].elements['sqlExtract.whereClause1'].value=='')
	{
	alert('Please select whereCaluse1 first');
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value=""
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause1'].focus();
	return false;
	}
	
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause2'].value=='')
	{
	alert('Please select whereCaluse2 first');
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].focus();
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value="";
	return false;
	}
}
function  fieldRanking3()
{
	
	var corpID=document.forms['extractForm'].elements['sqlExtract.whereClause4'].value
	corpID=corpID.toLowerCase(); 
	if(corpID=='corpid')
	{
	alert('corpid is not allowed to be in where Clause');
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].value=''
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].focus();
	return false;
	}
	if(document.forms['extractForm'].elements['sqlExtract.whereClause1'].value=='')
	{
	alert('Please select whereCaluse1 first');
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause1'].focus();
	return false;
	}
	
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause2'].value=='')
	{
	alert('Please select whereCaluse2 first');
	
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].focus();
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].value="";
	return false;
	}
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause3'].value=='')
	{
	alert('Please select whereCaluse3 first');
	
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].value=''
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].focus();
	
	return false;
	}
}
function  fieldRanking4()
{
	
	var corpID=document.forms['extractForm'].elements['sqlExtract.whereClause5'].value
	corpID=corpID.toLowerCase(); 
	if(corpID=='corpid')
	{
		alert('corpid is not allowed to be in where Clause');
		document.forms['extractForm'].elements['sqlExtract.whereClause5'].value=''
		document.forms['extractForm'].elements['sqlExtract.whereClause5'].focus();
	return false;
	}
	if(document.forms['extractForm'].elements['sqlExtract.whereClause1'].value=='')
	{
	alert('Please select whereCaluse1 first');
	document.forms['extractForm'].elements['sqlExtract.whereClause2'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause3'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause4'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause5'].value="";
	document.forms['extractForm'].elements['sqlExtract.whereClause1'].focus();
	return false;
	}
	
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause2'].value=='')
	{
		alert('Please select whereCaluse2 first');
		document.forms['extractForm'].elements['sqlExtract.whereClause3'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause4'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause5'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause2'].focus();
	return false;
	}
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause3'].value=='')
	{
	alert('Please select whereCaluse3 first');
		
		document.forms['extractForm'].elements['sqlExtract.whereClause4'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause5'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause3'].focus();
	return false;
	}
	
	else if(document.forms['extractForm'].elements['sqlExtract.whereClause4'].value=='')
	{
	alert('Please select whereCaluse4 first');
		document.forms['extractForm'].elements['sqlExtract.whereClause5'].value="";
		document.forms['extractForm'].elements['sqlExtract.whereClause4'].focus();
		
	return false;
	}
	

}
function changeStatus1(){
    
   var one=document.forms['extractForm'].elements['sqlExtract.whereClause1'].value;
   if(one==''){
	   	alert('You can not select isDate Flag for an empty field')
		document.forms['extractForm'].elements['sqlExtract.isdateWhere1'].checked=false;
		return false;
	}
	else{
	document.forms['extractForm'].elements['formStatus'].value='1';
	}
}
function changeStatus2(){
    
   var Two=document.forms['extractForm'].elements['sqlExtract.whereClause2'].value;
   if(Two==''){
	   	alert('You can not select isDate Flag for an empty field')
		document.forms['extractForm'].elements['sqlExtract.isdateWhere2'].checked=false;
		return false;
	}
	else{
	document.forms['extractForm'].elements['formStatus'].value='1';
	}
}
function changeStatus3(){
    
   var Three=document.forms['extractForm'].elements['sqlExtract.whereClause3'].value;
   if(Three==''){
	   	alert('You can not select isDate Flag for an empty field')
		document.forms['extractForm'].elements['sqlExtract.isdateWhere3'].checked=false;
		return false;
	}
	else{
	document.forms['extractForm'].elements['formStatus'].value='1';
	}
}
function changeStatus4(){
    
   var Four=document.forms['extractForm'].elements['sqlExtract.whereClause4'].value;
   if(Four==''){
	   	alert('You can not select isDate Flag for an empty field')
		document.forms['extractForm'].elements['sqlExtract.isdateWhere4'].checked=false;
		return false;
	}
	else{
	document.forms['extractForm'].elements['formStatus'].value='1';
	}
}
function changeStatus5(){
    
   var Five=document.forms['extractForm'].elements['sqlExtract.whereClause5'].value;
   if(Five==''){
	   	alert('You can not select isDate Flag for an empty field')
		document.forms['extractForm'].elements['sqlExtract.isdateWhere5'].checked=false;
		return false;
	}
	else{
	document.forms['extractForm'].elements['formStatus'].value='1';
	}
}

function fillEmail(){
  var whereClause1= document.forms['extractForm'].elements['sqlExtract.whereClause1'].value;
  var displayLabel1= document.forms['extractForm'].elements['sqlExtract.displayLabel1'].value;
  var groupBy= document.forms['extractForm'].elements['sqlExtract.groupBy'].value;
  var orderBy= document.forms['extractForm'].elements['sqlExtract.orderBy'].value;
  whereClause1=whereClause1.trim(); 
  displayLabel1=displayLabel1.trim(); 
  groupBy=groupBy.trim(); 
  orderBy=orderBy.trim(); 
  if(whereClause1=='' && displayLabel1=='' && groupBy=='' && orderBy==''){
  document.forms['extractForm'].elements['sqlExtract.email'].readOnly=false;
  }else{
  document.forms['extractForm'].elements['sqlExtract.email'].readOnly=true;
  }
}
function checkfillEmail(temp){ 
  var whereClause1= document.forms['extractForm'].elements['sqlExtract.whereClause1'].value;
  var displayLabel1= document.forms['extractForm'].elements['sqlExtract.displayLabel1'].value;
  var groupBy= document.forms['extractForm'].elements['sqlExtract.groupBy'].value;
  var orderBy= document.forms['extractForm'].elements['sqlExtract.orderBy'].value;
  var email= document.forms['extractForm'].elements['sqlExtract.email'].value;
  whereClause1=whereClause1.trim(); 
  displayLabel1=displayLabel1.trim(); 
  groupBy=groupBy.trim(); 
  orderBy=orderBy.trim(); 
  email=email.trim(); 
  if(email!=''){
  alert("you cannot fill, Email is filled")
  document.forms['extractForm'].elements[temp.name].value='';
  }else{
  fillEmail();
  }

}
function showScheduler(){
    var queryScheduler=document.forms['extractForm'].elements['sqlExtract.sqlQueryScheduler'].value; 
	if(queryScheduler=="D"){
	}
	if(queryScheduler=="M"){
	document.getElementById("hid3").style.display="block"; 
	document.getElementById("hid2").style.display="none";
	
	} 
	else if(queryScheduler=="W"){
	document.getElementById("hid2").style.display="block";
	document.getElementById("hid3").style.display="none"; 
	
	} 
	else if(queryScheduler=="Q"){
	
	document.getElementById("hid2").style.display="none"; 
	document.getElementById("hid3").style.display="none"; 
	}
	else{
	document.getElementById("hid2").style.display="none"; 
	document.getElementById("hid3").style.display="none"; 
	
	}
	}
</script>
</head>	

<s:form id="extractForm" name="extractForm" action="" method="post" validate="true">
<div id="Layer1" style="width:90%"> 
<s:hidden name="sqlExtract.id"/>
<s:hidden name="btn"/>
	<div id="newmnav">
		  <ul>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>SQL Extract Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<c:if test="${sessionCorpID=='TSFT'}">
		  		<li><a href="sqlExtract.html"><span>SQL Extract List</span></a></li>
		  	 	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${sqlExtract.id}&tableName=sqlextract&decorator=popup&popup=true&corpIds=${sqlExtract.corpID}&companyListTSFT=Y','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		  	</c:if>
			<c:if test="${sessionCorpID!='TSFT'}">
		  		<li><a href="sqlExtractEachUser.html"><span>SQL Extract List</span></a></li>
		 	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${sqlExtract.id}&tableName=sqlextract&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>		  		
		  	</c:if>
		  </ul>
	</div><div class="spn">&nbsp;</div>
	<div style="padding-bottom:1px;"></div>
	<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table cellspacing="1" cellpadding="1" border="0">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2">
					  <tbody>  	
					 	<tr><td height="3px"></td></tr>
						  	<tr>
						  		<td width="30px"></td>
						  		<td width="" align="right">File&nbsp;Name<font color="red" size="2">*</font></td>
						  		<td width="70"><s:textfield name="sqlExtract.fileName"  maxlength="254" size="65px" cssStyle="width:345px;"cssClass="input-text"/></td>
						  		<td width="10px"></td>
						  		<td width="185px" align="right">File&nbsp;Type<font color="red" size="2">*</font></td>
						  		<td><s:select cssClass="list-menu" key="sqlExtract.fileType" list="{'XLS','CSV'}"  cssStyle="width:83px"/></td>
						  	</tr>
						  	<tr>
						  		<td align="left" width="30px"></td>
						  		<td align="right" valign="top">Description<font color="red" size="2">*</font></td>
						  		<td colspan="2" align="left" class="listwhitetext"><s:textarea  name="sqlExtract.description" rows="2" cols="62" cssStyle="height:45px; width:345px;" cssClass="textarea" onchange="return checkLength();" onkeypress="return checkLength();"/></td>
						  		<td width="" align="right">Status</td>
						  		<td><s:textfield name="sqlExtract.tested"  size="11" cssStyle="width:80px;" cssClass="input-textUpper" readonly="true"/></td>					  		
						  	</tr>
					  		<tr>					  						  	
					  		<td width="30px" ></td>
					  		<td width="" align="right">Corp&nbsp;ID<font color="red" size="2">*</font></td>
					  		<td colspan="3">
					  		<table style="margin:0px;padding:0px;" border="0">
					  		<tr>					  		
					  		<td width="132px">
					  		<c:if test="${sessionCorpID=='TSFT'}">
						  		<s:select cssClass="list-menu" key="sqlExtract.corpID" list="%{corpIDList}" cssStyle="width:100px" headerKey="" headerValue=""/>
					  		</c:if>
					  		<c:if test="${sessionCorpID!='TSFT'}">
						  		<s:textfield name="sqlExtract.corpID" cssClass="input-textUpper" readonly="true" cssStyle="width:100px"/>
					  		</c:if>
					  		</td>
					  		<c:if test="${sessionCorpID=='TSFT'}">
					  		<td>Scheduling&nbsp;Criteria<font color="red" size="2">*</font></td>
					  		<td>
					  		<s:select id="sqlQuery" cssClass="list-menu" key="sqlExtract.sqlQueryScheduler" list="%{sqlQuerySchedulerList}" cssStyle="width:110px"  onchange="return showScheduler();"/>					  	
					  		</td>
					  		</c:if>
					  		<td>
		                     <table id="hid2" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0">
		                      <tr> 
			                 <td align="right">Weekly&nbsp;Scheduler<font color="red" size="2">*</font>&nbsp;</td>
			                 <td><s:select cssClass="list-menu" key="sqlExtract.perWeekScheduler" list="%{perWeekSchedulerList}" cssStyle="width:70px" headerKey="" headerValue=""/></td>
							</tr>
							</table>

			  	 			<table id="hid3" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0"> 
		            		<tr>
		            		<td width="" align="right">Monthly&nbsp;Scheduler<font color="red" size="2">*</font>&nbsp;</td>            
		            		<td>
		          			<s:select cssClass="list-menu" key="sqlExtract.perMonthScheduler" list="%{perMonthSchedulerList}" cssStyle="width:70px" headerKey="" headerValue=""/>
				 			</td>
		   					<tr>
		   					</table>
							</td>
							</tr>
							</table>
							</td>					  		
					  		<td><input type="button"  class="cssbuttonA" value="Test Query"  style="width:85px;" onclick="return btntype1(this);"/></td>					  		
					  		</tr>
					  		<tr>
					  		<td align="left" width="30px"></td>
					  		<td align="right" valign="top">SQL Query<font color="red" size="2">*</font></td>
					  		<td colspan="4" align="left" class="listwhitetext"><s:textarea name="sqlExtract.sqlText" rows="15" cols="120" cssStyle="height: 245px; width: 645px;" cssClass="textarea" /></td>
					  		<td valign="top">
							</td>
		   					</tr>		  	
		
							<tr>
							<td width="30px" align="left"></td>
							<td width="30px" align="left"></td>
							<td colspan="10">		
							<table class="detailTabLabel" border="0" cellpadding="3" cellspacing="0" width="650" style="margin:0px;border:2px solid #74b3dc;">					  		
				 		  	<tr>		 		  	
							<td class="subcontenttabChild" style="height:20px;" align="right" width="">Where Clause #</td>
							<td class="subcontenttabChild" style="height:20px;" width="190" colspan="2">Field Name</td>
							<td class="subcontenttabChild" style="height:20px;" width="160">Display Label</td>
							<td class="subcontenttabChild" style="height:20px;" align="left">IsDate</td>
							<td class="subcontenttabChild" style="height:20px;" align="left">Default Condition</td>
							</tr>
											  		
					  		<tr>
					  		<td width="" align="right"> 1: </td>
					  		<td colspan="2"><s:textfield name="sqlExtract.whereClause1"  maxlength="45" size="20px" cssClass="input-text" onchange="return fieldRanking();"/></td>
					  		<td ><s:textfield name="sqlExtract.displayLabel1"  maxlength="45" size="20px" cssClass="input-text"  onchange="checkfillEmail(this);"/></td>
					  		<c:set var="isdateFlagg1" value="false" />
							<c:if test="${sqlExtract.isdateWhere1}">
							<c:set var="isdateFlagg1" value="true" />
							</c:if>
					  		<td align="left"><s:checkbox name="sqlExtract.isdateWhere1" value="${isdateFlagg1}" fieldValue="true" onchange="changeStatus1();"  tabindex="1" /></td>
							<td width="" align="left"><s:select cssClass="list-menu" name="sqlExtract.defaultCondition1" key="condition1" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:95px"/></td>
							</tr>
					  		<tr>
					  		<td width="" align="right"> 2: </td>
					  		<td colspan="2"><s:textfield name="sqlExtract.whereClause2"  maxlength="45" size="20px" cssClass="input-text" onchange="return fieldRanking1();" /></td>
					  		<td ><s:textfield name="sqlExtract.displayLabel2"  maxlength="45" size="20px" cssClass="input-text" /></td>
					  		<c:set var="isdateFlagg2" value="false" />
							<c:if test="${sqlExtract.isdateWhere2}">
							<c:set var="isdateFlagg2" value="true" />
							</c:if>
					  		<td align="left"><s:checkbox name="sqlExtract.isdateWhere2" value="${isdateFlagg2}" fieldValue="true" onchange="changeStatus2();" tabindex="1"/></td>
							<td width="" align="left"><s:select cssClass="list-menu" name="sqlExtract.defaultCondition2" key="condition1" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:95px"/></td>
				
					  		</tr>
					  		<tr>
					  		<td width="" align="right"> 3: </td>
					  		<td colspan="2"><s:textfield name="sqlExtract.whereClause3"  maxlength="45" size="20px" cssClass="input-text" onchange="return fieldRanking2();"/></td>
					  		<td ><s:textfield name="sqlExtract.displayLabel3"  maxlength="45" size="20px" cssClass="input-text" /></td>
					  		<c:set var="isdateFlagg3" value="false" />
							<c:if test="${sqlExtract.isdateWhere3}">
							<c:set var="isdateFlagg3" value="true" />
							</c:if>
					  		<td align="left"><s:checkbox name="sqlExtract.isdateWhere3" value="${isdateFlagg3}" fieldValue="true"  onchange="changeStatus3();" tabindex="1" /></td>
							<td width="" align="left"><s:select cssClass="list-menu" name="sqlExtract.defaultCondition3" key="condition1" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:95px"/></td>
				
					  		</tr>
					  		<tr>
					  		<td width="" align="right"> 4: </td>
					  		<td colspan="2"><s:textfield name="sqlExtract.whereClause4"  maxlength="45" size="20px" cssClass="input-text" onchange="return fieldRanking3();"/></td>
					  		<td ><s:textfield name="sqlExtract.displayLabel4"  maxlength="45" size="20px" cssClass="input-text" /></td>
					  		<c:set var="isdateFlagg4" value="false" />
							<c:if test="${sqlExtract.isdateWhere4}">
							<c:set var="isdateFlagg4" value="true" />
							</c:if>
					  		<td align="left"><s:checkbox name="sqlExtract.isdateWhere4" value="${isdateFlagg4}" fieldValue="true" onchange="changeStatus4();" tabindex="1" /></td>
							<td width="" align="left"><s:select cssClass="list-menu" name="sqlExtract.defaultCondition4" key="condition1" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:95px"/></td>
							</tr>
					  		<tr>
					  		<td width="" align="right"> 5:</td>
					  		<td colspan="2"><s:textfield name="sqlExtract.whereClause5"  maxlength="45" size="20px" cssClass="input-text" onchange="return fieldRanking4();"/></td>
					  		<td ><s:textfield name="sqlExtract.displayLabel5"  maxlength="45" size="20px" cssClass="input-text" /></td>
					  		<c:set var="isdateFlagg5" value="false" />
							<c:if test="${sqlExtract.isdateWhere5}">
							<c:set var="isdateFlagg5" value="true" />
							</c:if>
					  		<td align="left"><s:checkbox name="sqlExtract.isdateWhere5" value="${isdateFlagg5}" fieldValue="true" onchange="changeStatus5();"  tabindex="1" /></td>
							<td width="" align="left"><s:select cssClass="list-menu" name="sqlExtract.defaultCondition5" key="condition1" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:95px"/></td>
							</tr>
					  		
					  		<tr>					  						  		
					  		<td width="" align="right">Group By:</td>
					  		<td align="left" style="width:105px;!width:60px;"><s:textfield name="sqlExtract.groupBy"  maxlength="100" size="20px" cssClass="input-text" onchange="checkfillEmail(this);"/></td>
					  		<td width="69" align="right">Order By:</td>
					  		<td ><s:textfield name="sqlExtract.orderBy"  maxlength="45" size="20px" cssClass="input-text" onchange="checkfillEmail(this);"/></td>
					  		</tr>
					  		<tr>					  					  		
					  		<td width="" align="right">Email:</td>
					  		<td colspan="10"><s:textfield name="sqlExtract.email"  maxlength="500" cssStyle="width:519px;" cssClass="input-text" onkeydown="fillEmail();"/></td>
					  		</tr>
					  		<tr><td align="left" height="1px"></td></tr>
					  		</table>
					  		</td>					  		
					  	</tr>
					  	<tr><td align="left" height="30px">&nbsp;</td></tr>
					  </tbody>
				</table>
			</tr>
		 </tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='sqlExtract.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:155px">
							<fmt:formatDate var="sqlExtractCreatedOnFormattedValue" value="${sqlExtract.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="sqlExtract.createdOn" value="${sqlExtractCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${sqlExtract.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='sqlExtract.createdBy' /></b></td>
							<c:if test="${not empty sqlExtract.id}">
								<s:hidden name="sqlExtract.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{sqlExtract.createdBy}"/></td>
							</c:if>
							<c:if test="${empty sqlExtract.id}">
								<s:hidden name="sqlExtract.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='sqlExtract.updatedOn'/></b></td>
							<fmt:formatDate var="sqlExtractupdatedOnFormattedValue" value="${sqlExtract.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="sqlExtract.updatedOn" value="${sqlExtractupdatedOnFormattedValue}"/>
							<td style="width:155px"><fmt:formatDate value="${sqlExtract.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='sqlExtract.updatedBy' /></b></td>
							<c:if test="${not empty sqlExtract.id}">
								<s:hidden name="sqlExtract.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{sqlExtract.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty sqlExtract.id}">
								<s:hidden name="sqlExtract.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" border="0">
					<tbody> 	
						<tr>
							<td align="left" height="3px"></td>
						</tr>	  	
						<tr>
							<td align="left"><input type="button" Class="cssbutton1"  value="Save" key="button.save" onclick="return btntype1(this);return checkLength();"/></td>
					       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset" onclick=" return resetValues();"/></td>
					       	</tr>		  	
					</tbody>
				</table>
			</div>

</s:form>
<script type="text/javascript">
fillEmail();
<c:if test="${empty sqlExtract.id}">
document.getElementById("hid2").style.display="none"; 
document.getElementById("hid3").style.display="none"; 
document.getElementById("hid4").style.display="none";
</c:if>

showScheduler();
function resetValues()
{ 
var sqlScheduler ='${sqlExtract.sqlQueryScheduler}';
document.forms['extractForm'].elements['sqlExtract.sqlQueryScheduler'].value=sqlScheduler; 
	 showScheduler();

}

</script>
