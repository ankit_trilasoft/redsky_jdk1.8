package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsMoveMgmtDao;
import com.trilasoft.app.model.DsMoveMgmt;


public class DsMoveMgmtDaoHibernate extends GenericDaoHibernate<DsMoveMgmt, Long> implements DsMoveMgmtDao {

	public DsMoveMgmtDaoHibernate() {
		super(DsMoveMgmt.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsMoveMgmt where id = "+id+" ");
	}

}
