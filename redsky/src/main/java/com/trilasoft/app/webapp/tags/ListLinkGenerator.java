package com.trilasoft.app.webapp.tags;

import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;

import com.trilasoft.app.model.ListLinkData;

public class ListLinkGenerator extends TableDecorator {

	/**
	 * FastDateFormat used to format dates in getDate().
	 */
	private FastDateFormat dateFormat;

	/**
	 * DecimalFormat used to format money in getMoney().
	 */
	private DecimalFormat moneyFormat;

	/**
	 * Creates a new Wrapper decorator who's job is to reformat some of the data
	 * located in our TestObject's.
	 */
	public ListLinkGenerator() {
		super();

		// Formats for displaying dates and money.

		this.dateFormat = FastDateFormat.getInstance("MM/dd/yy"); 
		this.moneyFormat = new DecimalFormat("$ #,###,###.00"); 
	}

	/**
	 * Test method which always returns a null value.
	 * 
	 * @return <code>null</code>
	 */
	public String getNullValue() {
		return null;
	}

	/**
	 * Returns the date as a String in MM/dd/yy format.
	 * 
	 * @return formatted date
	 */
	public String getDate() {
		//return this.dateFormat.format(((ListObject) this.getCurrentRowObject())
		//		.getDate());
		return null;
	}

	/**
	 * Returns the money as a String in $ #,###,###.00 format.
	 * 
	 * @return String
	 */
	public String getMoney() {
		//return this.moneyFormat
		//		.format(((ListObject) this.getCurrentRowObject()).getMoney());
		return null;
	}

	public String getListLinkParams() {
		ListLinkData object = (ListLinkData) getCurrentRowObject();
		int index = getListIndex();
		String codeFieldName = getPageContext().getRequest().getParameter("fld_code");
		String descriptionFieldName = getPageContext().getRequest().getParameter("fld_description");
		String secondDescriptionFieldName = getPageContext().getRequest().getParameter("fld_secondDescription");
		String thirdDescriptionFieldName = getPageContext().getRequest().getParameter("fld_thirdDescription");
		String fourthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_fourthDescription");
		String fifthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_fifthDescription");
		String sixthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_sixthDescription");
		String seventhDescriptionFieldName = null;
		String eigthDescriptionFieldName = null;
		String ninthDescriptionFieldName = null;
		String tenthDescriptionFieldName = null;
		try
		{
			seventhDescriptionFieldName = getPageContext().getRequest().getParameter("fld_seventhDescription");  
		}
		catch(Exception ex)
		{
			seventhDescriptionFieldName = ""; 
		}
		try{
			eigthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_eigthDescription");
			ninthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_ninthDescription"); 
			tenthDescriptionFieldName = getPageContext().getRequest().getParameter("fld_tenthDescription");
		}
		catch(Exception ex){
			eigthDescriptionFieldName = "";
			ninthDescriptionFieldName = "";
			tenthDescriptionFieldName=  "";
		}
		/* Code added to check avalibility of some special character in listCode */
		if(object.getListCode().contains("&"))
		{
			return "<a href=\"popupDataReturn.html?decorator=popup&amp;popup=true&amp;listCode=" 
			+ object.getListCode().replace("\"", "~").replace("#", "%23").replaceAll("&", "%26") + "&amp;listDescription=" + object.getListDescription().replace("\"","''").replace("\"", "~").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listSecondDescription=" + object.getListSecondDescription().replace("\"", "~").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listThirdDescription=" + object.getListThirdDescription() + "&amp;listFourthDescription=" + object.getListFourthDescription() + "&amp;listFifthDescription=" + object.getListFifthDescription() + "&amp;listTenthDescription=" + object.getListTenthDescription()+ "&amp;listNinthDescription=" + object.getListNinthDescription()+ "&amp;listEigthDescription=" + object.getListEigthDescription() + "&amp;listSeventhDescription=" + object.getListSeventhDescription()+ "&amp;listSixthDescription=" + object.getListSixthDescription() + 
			"&amp;fld_sixthDescription=" + sixthDescriptionFieldName+"&amp;fld_fifthDescription=" + fifthDescriptionFieldName+"&amp;fld_fourthDescription=" + fourthDescriptionFieldName+"&amp;fld_thirdDescription=" + thirdDescriptionFieldName+"&amp;fld_secondDescription=" + secondDescriptionFieldName+"&amp;fld_description=" + descriptionFieldName+"&amp;fld_code=" +codeFieldName +"&amp;fld_seventhDescription=" +seventhDescriptionFieldName +"&amp;fld_eigthDescription=" +eigthDescriptionFieldName +"&amp;fld_ninthDescription=" +ninthDescriptionFieldName +"&amp;fld_tenthDescription=" +tenthDescriptionFieldName+" \">" 
			+ object.getListCode() + "</a>"; 
		}
		/* Code ends here */
		if(eigthDescriptionFieldName!=null && eigthDescriptionFieldName != "")
		{ 	
			return "<a href=\"popupDataReturn.html?decorator=popup&amp;popup=true&amp;listCode=" 
			+ object.getListCode().replace("\"", "~").replace("#", "%23").replace("&", "%26") + "&amp;listDescription=" + object.getListDescription().replace("\"","''").replace("\"", "~").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listSecondDescription=" + object.getListSecondDescription().replace("\"", "~").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listThirdDescription=" + object.getListThirdDescription() + "&amp;listFourthDescription=" + object.getListFourthDescription() + "&amp;listFifthDescription=" + object.getListFifthDescription() + "&amp;listTenthDescription=" + object.getListTenthDescription()+ "&amp;listNinthDescription=" + object.getListNinthDescription()+ "&amp;listEigthDescription=" + object.getListEigthDescription() + "&amp;listSeventhDescription=" + object.getListSeventhDescription()+ "&amp;listSixthDescription=" + object.getListSixthDescription() + 
			"&amp;fld_sixthDescription=" + sixthDescriptionFieldName+"&amp;fld_fifthDescription=" + fifthDescriptionFieldName+"&amp;fld_fourthDescription=" + fourthDescriptionFieldName+"&amp;fld_thirdDescription=" + thirdDescriptionFieldName+"&amp;fld_secondDescription=" + secondDescriptionFieldName+"&amp;fld_description=" + descriptionFieldName+"&amp;fld_code=" +codeFieldName +"&amp;fld_seventhDescription=" +seventhDescriptionFieldName +"&amp;fld_eigthDescription=" +eigthDescriptionFieldName +"&amp;fld_ninthDescription=" +ninthDescriptionFieldName +"&amp;fld_tenthDescription=" +tenthDescriptionFieldName+" \">" 
			+ object.getListCode() + "</a>"; 
		}
		else if(seventhDescriptionFieldName != null && seventhDescriptionFieldName != "")
		{ 	
			return "<a href=\"popupDataReturn.html?decorator=popup&amp;popup=true&amp;listCode=" 
			+ object.getListCode().replace("\"", "~") + "&amp;listDescription=" + object.getListDescription().replace("\"","''").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listSecondDescription=" + object.getListSecondDescription() + "&amp;listThirdDescription=" + object.getListThirdDescription() + "&amp;listFourthDescription=" + object.getListFourthDescription() + "&amp;listFifthDescription=" + object.getListFifthDescription() + "&amp;listSeventhDescription=" + object.getListSeventhDescription()+ "&amp;listSixthDescription=" + object.getListSixthDescription() + 
			"&amp;fld_sixthDescription=" + sixthDescriptionFieldName+"&amp;fld_fifthDescription=" + fifthDescriptionFieldName+"&amp;fld_fourthDescription=" + fourthDescriptionFieldName+"&amp;fld_thirdDescription=" + thirdDescriptionFieldName+"&amp;fld_secondDescription=" + secondDescriptionFieldName+"&amp;fld_description=" + descriptionFieldName+"&amp;fld_code=" +codeFieldName +"&amp;fld_seventhDescription=" +seventhDescriptionFieldName +"\">" 
			+ object.getListCode() + "</a>"; 
		}
		else
		{ 	if(object.getListFourthDescription()!=null){
			return "<a href=\"popupDataReturn.html?decorator=popup&amp;popup=true&amp;listCode=" 
			+ object.getListCode().replace("\"", "~") + "&amp;listDescription=" + object.getListDescription().replace("\"","''").replace("%", "%25").replace("&", "%26").replace("#", "%23").replace("/", "%2F").replace("@", "%40").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listSecondDescription=" + object.getListSecondDescription() + "&amp;listThirdDescription=" + object.getListThirdDescription() + "&amp;listFourthDescription=" + object.getListFourthDescription().replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listFifthDescription=" + object.getListFifthDescription() + "&amp;listSixthDescription=" + object.getListSixthDescription() + 
			"&amp;fld_sixthDescription=" + sixthDescriptionFieldName+"&amp;fld_fifthDescription=" + fifthDescriptionFieldName+"&amp;fld_fourthDescription=" + fourthDescriptionFieldName+"&amp;fld_thirdDescription=" + thirdDescriptionFieldName+"&amp;fld_secondDescription=" + secondDescriptionFieldName+"&amp;fld_description=" + descriptionFieldName+"&amp;fld_code=" +codeFieldName  +"\">" 
			+ object.getListCode() + "</a>";
		    }else{
		    	return "<a href=\"popupDataReturn.html?decorator=popup&amp;popup=true&amp;listCode=" 
				+ object.getListCode().replace("\"", "~") + "&amp;listDescription=" + object.getListDescription().replace("\"","''").replace("%", "%25").replace("#", "%23").replace("&", "%26").replace("/", "%2F").replace("@", "%40").replace("\\", "%5C").replace("{", "%7B").replace("}", "%7D").replace("[", "%5B").replace("]", "%5D").replace("$", "%24").replace("+", "%2B") + "&amp;listSecondDescription=" + object.getListSecondDescription() + "&amp;listThirdDescription=" + object.getListThirdDescription() + "&amp;listFourthDescription=" + object.getListFourthDescription()+ "&amp;listFifthDescription=" + object.getListFifthDescription() + "&amp;listSixthDescription=" + object.getListSixthDescription() + 
				"&amp;fld_sixthDescription=" + sixthDescriptionFieldName+"&amp;fld_fifthDescription=" + fifthDescriptionFieldName+"&amp;fld_fourthDescription=" + fourthDescriptionFieldName+"&amp;fld_thirdDescription=" + thirdDescriptionFieldName+"&amp;fld_secondDescription=" + secondDescriptionFieldName+"&amp;fld_description=" + descriptionFieldName+"&amp;fld_code=" +codeFieldName  +"\">" 
				+ object.getListCode() + "</a>";
		    	}
			
		}
		
	}
	
}
