package com.trilasoft.qtg.wsclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;

public class QtgClientServiceCall {
	public static ResourceBundle resourceBundle;
	/*private static String url = null;
	static {
		//resourceBundle = ResourceBundle.getBundle("redsky-configuration");
		url = resourceBundle.getString("qtg.url1");
		// username=resourceBundle.getString("rc.username");
		// password=resourceBundle.getString("rc.password");
	}
*/
	public static String callQtgService(String requestXml, String qtgUser,
			String qtgUserPass,String url) throws RemoteException {

		ExternalCreateOpportunityWebServiceServiceStub stub = new ExternalCreateOpportunityWebServiceServiceStub(
				url);

		Options options = stub._getServiceClient().getOptions();

		HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
		auth.setUsername(qtgUser);
		auth.setPassword(qtgUserPass);
		// auth.setUsername("ru00003");
		// auth.setPassword("Tazconn1");
		// set if realm or domain is known

		options.setProperty(
				org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE,
				auth);

		options.setProperty(HTTPConstants.HTTP_PROTOCOL_VERSION,
				HTTPConstants.HEADER_PROTOCOL_10);

		stub._getServiceClient().setOptions(options);

		ExternalCreateOpportunityWebServiceServiceStub.CreateOpportunity creatOp = new ExternalCreateOpportunityWebServiceServiceStub.CreateOpportunity();
		creatOp.setInputCreateOppntyXML(requestXml);

		System.out.println("************ Request XML **************");
		System.out.println(requestXml);
		System.out.println("***************************************");

		ExternalCreateOpportunityWebServiceServiceStub.CreateOpportunityResponse createOpportunityResponse = stub
				.createOpportunity(creatOp);

		String createOpResponse = createOpportunityResponse
				.getCreateOpportunityReturn();

		System.out.println("************ Response XML **************");
		System.out.println(createOpResponse);
		System.out.println("***************************************");

		return createOpResponse;
	}

	private static String getInputXMLAsString() {
		StringBuilder input = new StringBuilder();

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(
					new File("sample_request.xml")));

			String line = null;
			while ((line = fileReader.readLine()) != null) {
				input.append(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("************ Request XML **************");
		System.out.println(input);
		System.out.println("***************************************");

		return input.toString();
	}

}
