<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<s:form id="partnerListForm"   method="post" >  
<s:set name="partners" value="partners" scope="request"/> 
<c:set var="agentClassificationShow" value="N"/>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
<div style="overflow-y:auto;height:215px;">
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>  
<display:table name="partners" class="table pr-f11"  id="partnerList"  style="width:100%;"  >
	<display:column title="Code" >
   	<c:set var="d" value="'"/>
     <c:set var="des" value="${fn:replace(partnerList.lastName,d, '~')}" />
   	<a onclick="copyVendorDetails('${partnerList.partnerCode}','${des}','${partnerNameId}','${paertnerCodeId}','${autocompleteDivId}');">${partnerList.partnerCode}</a>
   	</display:column>   
   	  <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
     <display:column  title="Alias Name" sortable="true" style="width:390px"><c:out value="${partnerList.aliasName}" /></display:column>
    <c:if test="${partnerType == 'AG'}"> 
	    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
    	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'VN'  || partnerType == 'CR' }"> 
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
	    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'PP' || partnerType == 'RSKY' || partnerType == 'AC'}">
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'OO'}">
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
    	<display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if> 
    <c:if test="${agentClassificationShow=='Y'}">
	<display:column title="Agent Classification" style="width:45px;" >
 	<c:if test="${partnerType == 'AG'}">
    	<c:out value="${partnerList.agentClassification}" />
	</c:if>
	</display:column>
	</c:if>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:120px"/>
</display:table> 
</div>
</td>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyDiv('${autocompleteDivId}','${partnerNameId}','${paertnerCodeId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
                                <c:if test="${not empty partners}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="false"/>
								</c:if>
								<c:if test="${empty partners}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="true"/>
								</c:if>
</s:form> 
<script type="text/javascript">
function findUserPermission(name,position) { 
	  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
	  ajax_showTooltip(url,position);	
	  }
</script>

