package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AgentRequest;


public interface AgentRequestDao extends GenericDao<AgentRequest, Long> {
	public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID);	
	public List getPartnerPublicList(String corpID, String lastName,
			String aliasName, String countryCode, String country,
			String stateCode, String status, Boolean isAgt,
			String counter);
	public List getAgentRequestDetailList(String sessionCorpID,String lastName,String aliasName,String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,Long agentPartnerId);
	public String updateAgentRecord(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,String updatedBy,Long id,String createdBy);
	public String  updateAgentRejected(Long id,String updatedBy);
	 public List checkById(Long id) ;
    public List getAgentRequestList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status,Boolean isAgt, Boolean isIgnoreInactive) ;
    public String getUser(String createdBy);
	public AgentRequest getByID(Long id) ;
	public String getUserCoprid(String createdBy);
}