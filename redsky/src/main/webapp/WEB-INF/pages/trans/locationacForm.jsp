<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html:html>
<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.details'/>" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />

<script language="javascript" type="text/javascript">

function checkField(){
 
	if(document.forms['locationacForm'].elements['location.locationId'].value == ''){    	
		alert('Please fill the location.');
		return false;
	}
	if(document.forms['locationacForm'].elements['location.type'].value == ''){    	
		alert('Please select the location type.');
		return false;
	}
	
}

function findCubicFeet()
{
   var cubicMeter = document.forms['locationacForm'].elements['location.cubicMeter'].value;
   if(cubicMeter>0)
   {
   var cubicFeet=(35.3146) * cubicMeter;   
   var cubicFeetValue=cubicFeet.toFixed(2);
   document.forms['locationacForm'].elements['location.cubicFeet'].value=cubicFeetValue;
   document.forms['locationacForm'].elements['location.cubicFeet'].focus(); 
   }
   if(cubicMeter == 0)
   {
	document.forms['locationacForm'].elements['location.cubicFeet'].value=0;
	document.forms['locationacForm'].elements['location.cubicFeet'].focus(); 
   }
}

function findCubicMeter()
{
   var cubicFeet = document.forms['locationacForm'].elements['location.cubicFeet'].value;
   if(cubicFeet>0)
   {
   var cubicMeter =(0.0283168466) * cubicFeet;
   var cubicMeterValue=cubicMeter.toFixed(2);
   document.forms['locationacForm'].elements['location.cubicMeter'].value=cubicMeterValue;
   document.forms['locationacForm'].elements['location.cubicMeter'].focus(); 
   }
   if(cubicFeet == 0)
   {
	document.forms['locationacForm'].elements['location.cubicMeter'].value=0;
	document.forms['locationacForm'].elements['location.cubicMeter'].focus(); 
   }
}

function findUtilizedVolCft()
{
   var cubicMeter = document.forms['locationacForm'].elements['location.utilizedVolCbm'].value;
   if(cubicMeter>0)
   {
   var cubicFeet=(35.3146) * cubicMeter;   
   var cubicFeetValue=cubicFeet.toFixed(2);
   document.forms['locationacForm'].elements['location.utilizedVolCft'].value=cubicFeetValue;
   document.forms['locationacForm'].elements['location.utilizedVolCft'].focus(); 
   }
   if(cubicMeter == 0)
   {
	document.forms['locationacForm'].elements['location.utilizedVolCft'].value=0;
	document.forms['locationacForm'].elements['location.utilizedVolCft'].focus(); 
   }
}

function findUtilizedVolCbm()
{
   var cubicFeet = document.forms['locationacForm'].elements['location.utilizedVolCft'].value;
   if(cubicFeet>0)
   {
   var cubicMeter =(0.0283168466) * cubicFeet;
   var cubicMeterValue=cubicMeter.toFixed(2);
   document.forms['locationacForm'].elements['location.utilizedVolCbm'].value=cubicMeterValue;
   document.forms['locationacForm'].elements['location.utilizedVolCbm'].focus(); 
   }
   if(cubicFeet == 0)
   {
	document.forms['locationacForm'].elements['location.utilizedVolCbm'].value=0;
	document.forms['locationacForm'].elements['location.utilizedVolCbm'].focus(); 
   }
}



function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value=0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}
</script>

</head>
<body>

<s:form id="locationacForm" name="locationacForm" action="saveLocationac.html?from=main" method="post" validate="true" onsubmit="return submit_form();">

	<s:hidden name="location.id" value="%{location.id}" />
	<div id="Layer1" style="width:100%;">
	<div id="newmnav">
     	<ul>
     			<li><a href="locationacs.html"><span>Location List</span></a></li>
        		<li id="newmnav1" style="background:#FFF"><a href="locationacs.html" class="current"><span>Locations <img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
               
               <!--  <li><a onclick="setReturnString('gototab.billing');return locationAutoSave('none');" ><span>Build Locations</span></a></li>-->
      </ul>
  </div><div class="spn">&nbsp;</div>
 
  	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:600px;">
	<tbody>
			<tr>
				<td><!-- <div class="subcontent-tab">&nbsp;Location  Details</div>-->
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">	     
				<tbody>
						<tr>
							<td align="right" width="20px"></td>
							<td align="right" class="listwhitetext">Location<font color="red" size="2">*</font></td>
                            <!--<td class="listwhitetext"><s:textfield name="location.locationId" maxlength="25" required="true" cssClass="input-text" readonly="true" cssStyle="width:200px"/></td>	-->
							<c:if test="${not empty location.id}">
								<td class="listwhitetext"><s:textfield name="location.locationId" maxlength="37" required="true" cssClass="input-text" readonly="true" cssStyle="width:228px"/></td>
							</c:if>
							<c:if test="${empty location.id}">
								<td class="listwhitetext"><s:select name="prifix" list="%{house}"  headerKey=""  headerValue="" cssStyle="width:230px" cssClass="list-menu"/>-&nbsp;<s:textfield name="location.locationId" required="true" cssClass="input-text" maxlength="37" cssStyle="width:225px"/></td>
							</c:if>
							<td></td>
						</tr>
                        <tr>
                        <td></td>
                        <td align="right" class="listwhitetext">Warehouse<font color="red" size="2"></font></td>
						<td><s:select name="location.warehouse" list="%{house}" cssStyle="width:230px" cssClass="list-menu" headerKey=""  headerValue=""/></td>  
												
						</tr>
						<tr></tr><tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Holding Capacity</td>
							<td class="listwhitetext"><s:select cssClass="list-menu" name="location.capacity" cssStyle="width:230px" list="%{capacity}" headerKey=""  headerValue="" tabindex="1"/></td>

							<td></td>
						</tr>
						<tr></tr><tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Type<font color="red" size="2">*</font></td>
							<td>
							<%-- <s:select name="location.type" cssClass="list-menu" cssStyle="width:230px" list="%{locTYPE}" headerKey=""  headerValue="" tabindex="2"/> --%>
							      <configByCorp:customDropDown listType="map" list="${loctype_isactive}" fieldValue="${location.type}" 
   									attribute="id=type class=list-menu name=location.type style=width:230px headerKey='' headerValue='' "/>
							
							</td>
                            
							<td></td>
						</tr>
						<tr></tr><tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Occupied</td>
							<c:if test="${not empty location.id}">
								<c:if test="${occupiedX =='[X]'}">
								     <c:if test="${location.occupied =='X'}"> 
								      <td class="listwhitetext"><s:checkbox name="location.occupied" value="true" cssStyle="margin:0px;" tabindex="3"/></td>
								     </c:if>
								     <c:if test="${location.occupied ==''}"> 
								      <td class="listwhitetext"><s:checkbox name="location.occupied" value="false" cssStyle="margin:0px;"  tabindex="3"/></td>
								     </c:if>
								</c:if>
							</c:if>
							<c:if test="${empty location.id}">
								<td class="listwhitetext"><s:checkbox name="location.occupied" value="false" cssStyle="margin:0px;" tabindex="3"/></td>
							</c:if>
						<td></td>
						</tr>
						<tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext" >Capacity</td>
							<td class="listwhitetext" ><s:textfield name="location.cubicFeet"  required="true" maxlength="10" cssClass="input-text" tabindex="4" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findCubicMeter();" />Cft&nbsp;&nbsp;&nbsp;&nbsp;	
							<s:textfield name="location.cubicMeter" required="true" maxlength="10" cssClass="input-text" tabindex="5" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findCubicFeet();" />Cbm</td>
						    <td></td>
						</tr>
						<tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Utilized</td>
							<td class="listwhitetext"><s:textfield name="location.utilizedVolCft"  required="true" maxlength="10" cssClass="input-text" tabindex="6" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findUtilizedVolCbm();"/>Cft&nbsp;&nbsp;&nbsp;&nbsp;	
							<s:textfield name="location.utilizedVolCbm" required="true" maxlength="10" cssClass="input-text" tabindex="7" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);"onchange="findUtilizedVolCft();" />Cbm</td>
						    <td></td>
						</tr>
						<tr> <td height="15px"></td></tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>

	<div align="left">
		   <s:hidden name="location.corpID"/>				
		<table class="detailTabLabel" border="0" style="width:800px">
		   <tbody>
					<tr>
						<td  align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<fmt:formatDate var="refMasterCreatedOn" value="${location.createdOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="location.createdOn" value="${refMasterCreatedOn}"/>
						<td style="width:130px"><fmt:formatDate value="${location.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
						<c:if test="${not empty location.id}">
										<s:hidden name="location.createdBy" />
										<td style="width:100px"><s:label name="createdBy"
											value="%{location.createdBy}" /></td>
									</c:if>
									<c:if test="${empty location.id}">
										<s:hidden name="location.createdBy"
											value="${pageContext.request.remoteUser}" />
										<td style="width:100px"><s:label name="createdBy"
											value="${pageContext.request.remoteUser}" /></td>
									
									</c:if>
						
						
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<fmt:formatDate var="refMasterUpdatedOn" value="${location.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<s:hidden name="location.updatedOn" value="${refMasterUpdatedOn}"/>
						<td style="width:130px"><fmt:formatDate value="${location.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<s:hidden name="location.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
					<tr><td height="15px"></td></tr>
				</tbody>
			</table>
		</div>
			
		<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px" tabindex="5" onclick="return checkField();"/>
	<!--  
	  <c:if test="${not empty location.id}">
		      <s:submit cssClass="cssbutton" method="delete" key="button.delete" onclick="return confirmDelete('location')" cssStyle="width:75px; height:25px" />
	 </c:if> 
	 -->
	   <s:submit cssClass="cssbutton" method="cancel" key="button.cancel" cssStyle="width:60px; height:25px" tabindex="6"/>
	   <s:reset cssClass="cssbutton" key="button.reset" cssStyle="width:60px; height:25px"/>
	
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
		 <c:when test="${gotoPageString == 'gototab.serviceorder' }">
    		<c:redirect url="/customerServiceOrders.html?id=${location.id}"/>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</c:if>
<script type="text/javascript"> 
 var f = document.getElementById('locationacForm'); 
	f.setAttribute("autocomplete", "off"); 
    Form.focusFirstElement($("locationacForm")); 
   
</script>
</s:form>
</body>
</html:html>