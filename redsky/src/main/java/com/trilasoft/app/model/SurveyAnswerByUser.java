package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="surveyanswerbyuser")
public class SurveyAnswerByUser extends BaseObject{
	
	private Long id;
	private Long serviceOrderId;
	private Long questionId;
	private String answerId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Long customerFileId;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("questionId", questionId)
				.append("answerId", answerId)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("customerFileId", customerFileId)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SurveyAnswerByUser))
			return false;
		SurveyAnswerByUser castOther = (SurveyAnswerByUser) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(questionId, castOther.questionId)
				.append(answerId, castOther.answerId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(customerFileId, castOther.customerFileId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(serviceOrderId)
				.append(questionId).append(answerId)
				.append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn)
				.append(customerFileId)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getAnswerId() {
		return answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	

}
