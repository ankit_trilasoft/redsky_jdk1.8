DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`IsNumeric` $$
CREATE DEFINER=`root`@`122.160.8.101` FUNCTION `IsNumeric`(sIn varchar(1024)) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
RETURN sIn REGEXP '^(-|\\+){0,1}([0-9]+\\.[0-9]*|[0-9]*\\.[0-9]+|[0-9]+)$' $$

DELIMITER ;