package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.jsp.jstl.sql.Result;
import net.sf.navigator.menu.MenuBase;
import net.sf.navigator.menu.MenuComponent;
import net.sf.navigator.menu.MenuRepository;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;
import org.displaytag.properties.SortOrderEnum;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.MenuItem;
import com.trilasoft.app.model.MenuItemPermission;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.MenuItemManager;
import com.trilasoft.app.service.MenuItemPermissionManager;
import com.trilasoft.app.service.RefMasterManager;

public class MenuItemAction extends BaseAction {

	private static final String menuItemId = null;

	public Set menuItems;
    public Set pageItems;
    public Set moduleItems;
	private Long id;

	private Long maxId;

	private List refMasters;

	private RefMasterManager refMasterManager;

	private String sessionCorpID;

	private String requestedMIP;
	private String requestedPIP;

	private MenuItemManager menuItemManager;

	private MenuItem menuItem;

	private MenuItemPermission menuItemPermission;

	private MenuItemPermissionManager menuItemPermissionManager;

	private String perRec;

	private String reqMI;

	private User user;

	private UserManager userManager;

	private String recipient;

	private int MIP;

	private List menuItemPermissions;

	private String requestedMI;

	private List mps;
	private List pps;
	private int mipCount;
    private int pipCount;
	private ServletContext pageContext;

	private List corpList;

	private String corp;

	private List parentList;

	private List ls;
	private String hitFlag;

	private List seqNumList = new ArrayList();

	private String parent;

	private List checkTitleAvailability;

	private String titleN;
	private String userCorpIDs;

	private List findParByCorp = new ArrayList();

	private static Map<String, String> ROLE_ACTION;

	private HibernateFilterInterceptor hibernateFilter = null;
	private CompanyManager companyManager;
	private Company company;
	public MenuItemAction() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	// public void prepare() throws Exception {
	// getComboList(sessionCorpID);
	// findParByCorp = menuItemManager.findParByCorp(corp);
	// }

	public void setMenuItemManager(MenuItemManager menuItemManager) {
		this.menuItemManager = menuItemManager;
	}

	public MenuItemPermission getMenuItemPermission() {
		return menuItemPermission;
	}

	public void setMenuItemPermission(MenuItemPermission menuItemPermission) {
		this.menuItemPermission = menuItemPermission;
	}

	public void setMenuItemPermissionManager(
			MenuItemPermissionManager menuItemPermissionManager) {
		this.menuItemPermissionManager = menuItemPermissionManager;
	}

	public User getUser() {
		return user;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public Set getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(Set menuItems) {
		this.menuItems = menuItems;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRequestedMIP(String requestedMIP) {
		this.requestedMIP = requestedMIP;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getRefMasters() {
		return refMasters;
	}
	private String securityChecked; 
	public String getComboList(String corpId) {
		securityChecked="N";
			try{
				List <Company> compRec=companyManager.findByCorpID(corpId);
				for(Company newCompany:compRec){
					if(newCompany.getSecurityChecked()){
						securityChecked="Y";
					}
				}
			}catch(Exception e){e.printStackTrace();}
		ROLE_ACTION = refMasterManager.findByParameter(corpId, "ROLE_ACTION");
		modulelist=menuItemManager.getModuleList(corpId);
		return SUCCESS;
	}

	public String findCorpId() {
		corpList = menuItemManager.findCorpId();
		return SUCCESS;
	}

	public String findParent() {
		parentList = menuItemManager.findParent();
		return SUCCESS;
	}

	public String findSeqNumList() {
		seqNumList = menuItemManager.findSeqNumList(corp, parent);
		return SUCCESS;
	}

	@SkipValidation
	public String checkTitleAvailability() {
		checkTitleAvailability = menuItemManager.checkTitleAvailability(corp,
				parent, titleN.trim());
		return SUCCESS;
	}

	@SkipValidation
	public String findParByCorp() {
		findParByCorp = menuItemManager.findParByCorp(corp);
		return SUCCESS;
	}

	public Map<String, String> getROLE_ACTION() {
		return ROLE_ACTION;
	}

	private String menuUrl;
	private String menuName;
	private String menuId;
	private String moduleName;
	private String moduleDescr; 
	private String moduleUrl;
	private String sidL;
	private String module1;
	private List modulelist;
	private String menuRight;
	private String oldSidL;
	private String childPageUpdateFlag;
	private String parentN;
	private String menuN;
	private String titleName;
	private String urlN;
	private String pageMenu;
	private String pageDesc;
	private String pageUrl;	
	private String pageId;
	
	private String pageModule;
	private String pageClass;	
	private String pageMethod;
	
	public String pageActionList() {
		getComboList(sessionCorpID);
		if(pageMenu==null){pageMenu="";}
		if(pageDesc==null){pageDesc="";}
		if(pageUrl==null){pageUrl="";}
		
		List mps = menuItemManager.getPageActionListByUrl(menuUrl,menuName,sessionCorpID,pageMenu,pageDesc,pageUrl);
		pageItems = new HashSet(mps);
		pps = menuItemManager.findPagePermissions(menuUrl,menuName,sessionCorpID,pageMenu,pageDesc,pageUrl,perRec);
		pipCount = pps.size();
		getSession().setAttribute("pipCount", pipCount);
		return SUCCESS;
	}
	public String pageUrlList() {
		
		if(pageMenu==null){pageMenu="";}
		if(pageDesc==null){pageDesc="";}
		if(pageUrl==null){pageUrl="";}
		if(menuUrl==null){menuUrl="";}
		if(menuName==null){menuName="";}
		if(pageModule==null){pageModule="";}
		if(pageClass==null){pageClass="";}
		if(pageMethod==null){pageMethod="";}		
		if(corp==null){corp="";}
		List mps = menuItemManager.getPageActionListByUrlNew(menuUrl,menuName,corp,pageMenu,pageDesc,pageUrl);
		pageItems = new HashSet(mps);
		return SUCCESS;
	}
	
	public String editPageAction(){
		if((pageId!=null)&&(!pageId.equalsIgnoreCase(""))){
			String pageRecord=menuItemManager.getPageRecById(pageId);
			String arr[]=pageRecord.split("~");
			pageUrl=arr[0];
			pageClass=arr[1];
			pageMethod=arr[2];
			pageDesc=arr[3];
			pageModule=arr[4];
		}
		return SUCCESS;
	}
	@SkipValidation
	public String savePage(){
		if(pageMenu==null){pageMenu="";}
		if(pageDesc==null){pageDesc="";}
		if(pageUrl==null){pageUrl="";}
		if(menuUrl==null){menuUrl="";}
		if(menuName==null){menuName="";}	
		if(pageModule==null){pageModule="";}
		if(pageClass==null){pageClass="";}
		if(pageMethod==null){pageMethod="";}		
		menuItemManager.updatePageActionUrlList(pageDesc,corp,pageMethod,pageClass,pageModule,pageUrl,menuId,pageId);
		return SUCCESS;
	}
	@SkipValidation
	public String deletePage(){
		if(pageMenu==null){pageMenu="";}
		if(pageDesc==null){pageDesc="";}
		if(pageUrl==null){pageUrl="";}
		if(menuUrl==null){menuUrl="";}
		if(menuName==null){menuName="";}	
		if(pageModule==null){pageModule="";}
		if(pageClass==null){pageClass="";}
		if(pageMethod==null){pageMethod="";}		
		menuItemManager.removePageActionUrlList(pageId);
		return SUCCESS;
	}
	@SkipValidation
	public String menuItemList() {
		getComboList(sessionCorpID);
		if(parentN==null){parentN="";}
		if(menuN==null){menuN="";}
		if(titleName==null){titleName="";}
		if(urlN==null){urlN="";}
		if(pageUrl==null){pageUrl="";}
		menuItems = new HashSet(menuItemManager.listAllMenuItems(sessionCorpID,parentN,menuN,titleName,urlN,pageUrl));
		mps = menuItemManager.findPermissions(perRec,sessionCorpID,parentN,menuN,titleName,urlN);
		// System.out.println(mps.size());
		mipCount = mps.size();
		getSession().setAttribute("mipCount", mipCount);
		// menuItems = menuItemManager.findPermissions(perRec);
		return SUCCESS;
	}
	public String moduleItemList() {
		getComboList(sessionCorpID);
		if(moduleName==null){
			moduleName="";
		}
		if(moduleDescr==null){
			moduleDescr="";
		}
		if(moduleUrl==null){
			moduleUrl="";
		}
		moduleItems=new HashSet(menuItemManager.getModuleActionListByURL(moduleName,perRec,sessionCorpID,moduleDescr,moduleUrl));
		
		return SUCCESS;
	}
	public String allMenuItems() {
		findParByCorp = menuItemManager.findParByCorp(corp);
		corpList = menuItemManager.findCorpId();
		parentList = menuItemManager.findParent();
		menuItems = new HashSet(menuItemManager.allMenuItems());
		return SUCCESS;
	}

	public String delete() {
		menuItemManager.remove(id);
		saveMessage(getText("menuItem.deleted"));
		menuItems = new HashSet(menuItemManager.allMenuItems());
		return SUCCESS;
	}

	@SkipValidation
	public String assignModuleItem() throws Exception {
		if (sidL == null || sidL.equals("")) {
			sidL = "";
		} 
			menuItemManager.updateModuleActionPermission(sidL,sessionCorpID,oldSidL);			
			try{
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				}catch (Exception e) {e.printStackTrace();}	
		getComboList(sessionCorpID);
		if(moduleName==null){
			moduleName="";
		}		
		moduleItems=new HashSet(menuItemManager.getModuleActionListByURL(moduleName,perRec,sessionCorpID,moduleDescr,moduleUrl));
		return SUCCESS;
	}
	
	@SkipValidation
	public String assignPageItem() throws Exception {
		if (requestedPIP == null || requestedPIP.equals("")) {
			requestedPIP = "";
		} else {
			String[] result = requestedPIP.split("\\,");
			String i;
			for (int x = 0; x < result.length; x++) {
				String[] resultP = result[x].split("\\#");
				menuItemManager.updatePageActionPermission(Long.parseLong(resultP[0]),Long.parseLong(menuId),Integer.parseInt(resultP[1]),sessionCorpID,resultP[2].toString(),resultP[3].toString());
			}
		}
		try{
			menuItemManager.updateCurrentUrlPermission(sessionCorpID);
			}catch (Exception e) {e.printStackTrace();}	
		getComboList(sessionCorpID);
		if(pageMenu==null){pageMenu="";}
		if(pageDesc==null){pageDesc="";}
		if(pageUrl==null){pageUrl="";}		
		List mps = menuItemManager.getPageActionListByUrl(menuUrl,menuName,sessionCorpID,pageMenu,pageDesc,pageUrl);
		pageItems = new HashSet(mps);
		pps = menuItemManager.findPagePermissions(menuUrl,menuName,sessionCorpID,pageMenu,pageDesc,pageUrl,perRec);
		pipCount = pps.size();
		getSession().setAttribute("pipCount", pipCount);

		return SUCCESS;
	}	
	
	@SkipValidation
	public String updatePageSecurities() {
		if (requestedPIP == null || requestedPIP.equals("")) {
			requestedPIP = "";
		} else {
			String[] result = requestedPIP.split("\\,");
			String i;
			for (int x = 0; x < result.length; x++) {
				String[] resultP = result[x].split("\\#");
				menuItemManager.updatePageActionPermission(Long.parseLong(resultP[0]),Long.parseLong(menuId),Integer.parseInt(resultP[1]),sessionCorpID,resultP[2].toString(),resultP[3].toString());
			}
			try{
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				}catch (Exception e) {e.printStackTrace();}				
		}
		return SUCCESS;
	}
	@SkipValidation
	public String updateSecurities() {
		getComboList(sessionCorpID);
			if (menuItemManager.permissionExists(
					Long.parseLong(menuId),
					Integer.parseInt(menuRight), perRec,sessionCorpID,menuUrl)) {
				menuItemManager.updateMenuActionPermissionByList(Long.parseLong(menuId),perRec,sessionCorpID,Integer.parseInt(menuRight),menuUrl);			
			}
			
			if (menuItemManager.pagePermissionExists(
					  Long.parseLong(menuId),
					Integer.parseInt(menuRight), perRec,"YES",sessionCorpID)) {
				String idList=menuItemManager.getPageActionIdByUrl(menuItemManager.get(Long.parseLong(menuId)).getUrl(),"", sessionCorpID);
				menuItemManager.updatePageActionPermissionByList(idList,Long.parseLong(menuId),Integer.parseInt(menuRight),perRec,sessionCorpID);
			
			}
			
			try{
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				}catch (Exception e) {e.printStackTrace();}	
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String assignMenuItem() throws Exception {
		if (requestedMIP == null || requestedMIP.equals("")) {
			requestedMIP = "";
		} else {
			String[] result = requestedMIP.split("\\,");

			String i;
			for (int x = 0; x < result.length; x++) {
				String[] resultP = result[x].split("\\#");

					if (menuItemManager.permissionExists(
							Long.parseLong(resultP[0]),
							Integer.parseInt(resultP[1]), perRec,sessionCorpID,resultP[3].toString())) {
						menuItemManager.updateMenuActionPermissionByList(Long.parseLong(resultP[0]),perRec,sessionCorpID,Integer.parseInt(resultP[1]),resultP[3].toString());
					}
					if(!childPageUpdateFlag.equalsIgnoreCase("NO")){
						if (menuItemManager.pagePermissionExists(
								  Long.parseLong(resultP[0]),
								Integer.parseInt(resultP[1]), perRec,"YES",sessionCorpID)) {
							String idList=menuItemManager.getPageActionIdByUrl(menuItemManager.get(Long.parseLong(resultP[0])).getUrl(),"", sessionCorpID);
							menuItemManager.updatePageActionPermissionByList(idList,Long.parseLong(resultP[0]),Integer.parseInt(resultP[1]),perRec,sessionCorpID);
						}
					}
					if(childPageUpdateFlag.equalsIgnoreCase("NO")){
						if (menuItemManager.pagePermissionExists(
								  Long.parseLong(resultP[0]),
								Integer.parseInt(resultP[1]), perRec,"NO",sessionCorpID)) {
							String idList=menuItemManager.getPageActionIdByUrl(menuItemManager.get(Long.parseLong(resultP[0])).getUrl(),"", sessionCorpID);
							menuItemManager.updatePageActionPermissionByList(idList,Long.parseLong(resultP[0]),0,perRec,sessionCorpID);								
						}
					}
			}
			try{
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				}catch (Exception e) {e.printStackTrace();}				
		}
		if(parentN==null){parentN="";}
		if(menuN==null){menuN="";}
		if(titleName==null){titleName="";}
		if(urlN==null){urlN="";}
		if(pageUrl==null){pageUrl="";}
		menuItems = new HashSet(menuItemManager.listAllMenuItems(sessionCorpID,parentN,menuN,titleName,urlN,pageUrl));
		mps = menuItemManager.findPermissions(perRec,sessionCorpID,parentN,menuN,titleName,urlN);
		mipCount = mps.size();
		getSession().setAttribute("mipCount", mipCount);

		return SUCCESS;
	}

	public String edit() {
		if (id != null) {
			menuItem = menuItemManager.getForOtherCorpid(id);
			findParByCorp = menuItemManager.findParByCorp(menuItem.getCorpID());
			menuItem.getParentName();
		} else {
			menuItem = new MenuItem();
			menuItem.setCreatedOn(new Date());
			menuItem.setUpdatedOn(new Date());
			findParByCorp = menuItemManager.findParByCorp(corp);
		}
		corpList = menuItemManager.findCorpId();
		parentList = menuItemManager.findParent();

		// seqNumList = menuItemManager.findSeqNumList(corp,parent);
		return SUCCESS;
	}

	public String save() throws Exception {
		boolean isNew = (menuItem.getId() == null);
		if (isNew) {
			menuItem.setCreatedOn(new Date());
		}
		menuItem.setUpdatedOn(new Date());
		menuItem.setUpdatedBy(getRequest().getRemoteUser());
		menuItem.setCorpID(corp);
		menuItem = menuItemManager.save(menuItem);
		parentList = menuItemManager.findParent();
		findParByCorp = menuItemManager.findParByCorp(corp);
		String key = (isNew) ? "menuItem.added" : "menuItem.updated";
		saveMessage(getText(key));
		// if (!isNew) {
		// return INPUT;
		// } else {
		hitFlag = "1";
		return SUCCESS;
		// }
	}

	@SkipValidation
	public String search() {
		corpList = menuItemManager.findCorpId();
		parentList = menuItemManager.findParent();
		findParByCorp = menuItemManager.findParByCorp(corp);

		String corp = "";
		String parentName = "";
		String menuName = "";
		String title = "";
		if (userCorpIDs != null) {
			corp = userCorpIDs;
		}
		if (menuItem.getParentName() != null) {
			parentName = menuItem.getParentName();
		}
		if (menuItem.getName() != null) {
			menuName = menuItem.getName();
		}
		if (menuItem.getTitle() != null) {
			title = menuItem.getTitle();
		}
		ls = menuItemManager.findMenuItem(corp, parentName, menuName, title);
		menuItems = new HashSet(ls);
		return SUCCESS;
	}

	public static String getMenuItemId() {
		return menuItemId;
	}

	public List getMenuItemPermissions() {
		return menuItemPermissions;
	}

	public void setMenuItemPermissions(List menuItemPermissions) {
		this.menuItemPermissions = menuItemPermissions;
	}

	public String getPerRec() {
		return perRec;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/*
	 * public List getMenuItemPermissions() { return menuItemPermissions; }
	 * 
	 * public String menuItemPermissionList() { //menuItem =
	 * menuItemManager.get(id); menuItemPermissions = new ArrayList(
	 * menuItem.getMenuItemPermissions() ); return SUCCESS; }
	 * 
	 * public void setMenuItemPermissions(List menuItemPermissions) {
	 * this.menuItemPermissions = menuItemPermissions; } public String
	 * getPerRec() { return perRec; }
	 */
	public void setPerRec(String perRec) {
		this.perRec = perRec;
	}

	public MenuItemManager getMenuItemManager() {
		return menuItemManager;
	}

	public MenuItemPermissionManager getMenuItemPermissionManager() {
		return menuItemPermissionManager;
	}

	public String getRequestedMIP() {
		return requestedMIP;
	}

	public String getReqMI() {
		return reqMI;
	}

	public void setReqMI(String reqMI) {
		this.reqMI = reqMI;
	}

	public int getMIP() {
		return MIP;
	}

	public void setMIP(int mip) {
		MIP = mip;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getRequestedMI() {
		return requestedMI;
	}

	public void setRequestedMI(String requestedMI) {
		this.requestedMI = requestedMI;
	}

	public List getMps() {
		return mps;
	}

	public void setMps(List mps) {
		this.mps = mps;
	}

	public int getMipCount() {
		return mipCount;
	}

	public void setMipCount(int mipCount) {
		this.mipCount = mipCount;
	}

	private Set<Role> roles;

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void initializeRepository() throws Exception {

		if (getSession().getAttribute("repository") == null) {

			MenuRepository repository = new MenuRepository();
			ServletContext application = ServletActionContext
					.getServletContext();
			MenuRepository defaultRepository = (MenuRepository) application
					.getAttribute(MenuRepository.MENU_REPOSITORY_KEY);
			
			// System.out.println("defaultRepository.getDisplayers(): " +
			// defaultRepository.getDisplayers());
			repository.setDisplayers(defaultRepository.getDisplayers());

			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			String userName = auth.getName();

			// Set<MenuItem> menuItems = new HashSet(menuItemManager.getAll());
			user = userManager.getUserByUsername(userName);
			roles = user.getRoles();
			Set<MenuItemPermission> menuItemPermissions = new HashSet(
					menuItemPermissionManager.findByUser(user, roles));

			List<MenuItem> menuItemMasterList = menuItemManager
					.getMenuList(user.getCorpID());
			List<Long> menuItemIds = menuItemPermissionManager
					.findMenuIdsByUser(user, roles);

			for (MenuItem menu : menuItemMasterList) {
				if (!menuItemIds.contains(menu.getId()))
					continue;
				MenuComponent mc = new MenuComponent();
				String name = menu.getName();
				mc.setName(name);
				mc.setUrl(getRequest().getContextPath() + menu.getUrl());
				String parentName = menu.getParentName();
				if (parentName != null && !"".equals(parentName.trim())) {
					MenuComponent parentMenu = repository.getMenu(parentName);
					if (parentMenu == null) {
						parentMenu = new MenuComponent();
						parentMenu.setName(parentName);
						repository.addMenu(parentMenu);
					}
					mc.setParent(parentMenu);
				}
				String title = menu.getTitle();
				if(title != null )
					title = title.trim();
				mc.setTitle(title);
				String description = menu.getDescription();
				mc.setDescription(description);
				repository.addMenu(mc);
			}

			getSession().setAttribute("repository", repository);
		} else {

		}

		// getRequest().setAttribute("repository", repository);

	}

	public List getCorpList() {
		return corpList;
	}

	public void setCorpList(List corpList) {
		this.corpList = corpList;
	}

	public String getCorp() {
		return corp;
	}

	public void setCorp(String corp) {
		this.corp = corp;
	}

	public List getParentList() {
		return parentList;
	}

	public void setParentList(List parentList) {
		this.parentList = parentList;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public List getSeqNumList() {
		return seqNumList;
	}

	public void setSeqNumList(List seqNumList) {
		this.seqNumList = seqNumList;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public List getCheckTitleAvailability() {
		return checkTitleAvailability;
	}

	public void setCheckTitleAvailability(List checkTitleAvailability) {
		this.checkTitleAvailability = checkTitleAvailability;
	}

	public String getTitleN() {
		return titleN;
	}

	public void setTitleN(String titleN) {
		this.titleN = titleN;
	}

	public List getFindParByCorp() {
		// return findParByCorp;
		return (findParByCorp != null && !findParByCorp.isEmpty()) ? findParByCorp
				: menuItemManager.findParByCorp(corp);

	}

	public void setFindParByCorp(List findParByCorp) {
		this.findParByCorp = findParByCorp;
	}

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	public String getUserCorpIDs() {
		return userCorpIDs;
	}

	public void setUserCorpIDs(String userCorpIDs) {
		this.userCorpIDs = userCorpIDs;
	}

	public HibernateFilterInterceptor getHibernateFilter() {
		return hibernateFilter;
	}

	public void setHibernateFilter(HibernateFilterInterceptor hibernateFilter) {
		this.hibernateFilter = hibernateFilter;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public Set getPageItems() {
		return pageItems;
	}

	public void setPageItems(Set pageItems) {
		this.pageItems = pageItems;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public int getPipCount() {
		return pipCount;
	}

	public void setPipCount(int pipCount) {
		this.pipCount = pipCount;
	}

	public List getPps() {
		return pps;
	}

	public void setPps(List pps) {
		this.pps = pps;
	}

	public String getRequestedPIP() {
		return requestedPIP;
	}

	public void setRequestedPIP(String requestedPIP) {
		this.requestedPIP = requestedPIP;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public Set getModuleItems() {
		return moduleItems;
	}

	public void setModuleItems(Set moduleItems) {
		this.moduleItems = moduleItems;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getSidL() {
		return sidL;
	}

	public void setSidL(String sidL) {
		this.sidL = sidL;
	}

	public List getModulelist() {
		return modulelist;
	}

	public void setModulelist(List modulelist) {
		this.modulelist = modulelist;
	}

	public String getMenuRight() {
		return menuRight;
	}

	public void setMenuRight(String menuRight) {
		this.menuRight = menuRight;
	}

	public String getOldSidL() {
		return oldSidL;
	}

	public void setOldSidL(String oldSidL) {
		this.oldSidL = oldSidL;
	}

	public String getChildPageUpdateFlag() {
		return childPageUpdateFlag;
	}

	public void setChildPageUpdateFlag(String childPageUpdateFlag) {
		this.childPageUpdateFlag = childPageUpdateFlag;
	}

	public String getParentN() {
		return parentN;
	}

	public void setParentN(String parentN) {
		this.parentN = parentN;
	}

	public String getMenuN() {
		return menuN;
	}

	public void setMenuN(String menuN) {
		this.menuN = menuN;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getUrlN() {
		return urlN;
	}

	public void setUrlN(String urlN) {
		this.urlN = urlN;
	}

	public String getPageMenu() {
		return pageMenu;
	}

	public void setPageMenu(String pageMenu) {
		this.pageMenu = pageMenu;
	}

	public String getPageDesc() {
		return pageDesc;
	}

	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getModuleDescr() {
		return moduleDescr;
	}

	public void setModuleDescr(String moduleDescr) {
		this.moduleDescr = moduleDescr;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getPageModule() {
		return pageModule;
	}

	public void setPageModule(String pageModule) {
		this.pageModule = pageModule;
	}

	public String getPageClass() {
		return pageClass;
	}

	public void setPageClass(String pageClass) {
		this.pageClass = pageClass;
	}

	public String getPageMethod() {
		return pageMethod;
	}

	public void setPageMethod(String pageMethod) {
		this.pageMethod = pageMethod;
	}
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getSecurityChecked() {
		return securityChecked;
	}

	public void setSecurityChecked(String securityChecked) {
		this.securityChecked = securityChecked;
	}
}