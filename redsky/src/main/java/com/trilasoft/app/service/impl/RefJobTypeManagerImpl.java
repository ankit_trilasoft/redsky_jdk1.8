package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.RefJobTypeDao;
import com.trilasoft.app.model.RefJobType;
import com.trilasoft.app.service.RefJobTypeManager;

public class RefJobTypeManagerImpl extends GenericManagerImpl<RefJobType, Long> implements RefJobTypeManager {
	RefJobTypeDao refJobTypeDao;

	public RefJobTypeManagerImpl(RefJobTypeDao refJobTypeDao) {
		super(refJobTypeDao);
		this.refJobTypeDao = refJobTypeDao;
	}

	public List isExisted(String job,String companydivision,String sessionCorpID) {
		return refJobTypeDao.isExisted(job,companydivision,sessionCorpID);
	}
	
	public List searchResult(String job,String companydivision,String surveytool, String sessionCorpID){
		return refJobTypeDao.searchResult(job,companydivision,surveytool,sessionCorpID);
	}
}
