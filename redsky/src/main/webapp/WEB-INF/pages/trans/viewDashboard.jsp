<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>Dashboard</title> 
<meta name="heading" content="Dashboard"/>
<style>
body {padding:0px !important;}
div#inner-content {
margin-left:5px;
}
div#main {width:99% !important;}
.export{padding: 4px; width: 100px; background: none repeat scroll 0px 0px #F4F4F4; border: 1px solid rgb(227, 227, 227);}
</style>
<script language="javascript" type="text/javascript">

function validateJobGraph(){
	var url='viewReportWithParam.html?id=1992&reportName=Revenue for Bill To By Job&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}

function validateModeGraph(){
	var url='viewReportWithParam.html?id=1995&reportName=Revenue for Bill To By Mode&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}


function validateJob(){
	var url='viewReportWithParam.html?id=1992&reportName=Revenue for Bill To By Job&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}

function validateMode(){
	var url='viewReportWithParam.html?id=1995&reportName=Revenue for Bill To By Mode&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}
	
function validateRouting(){
	var url='viewReportWithParam.html?id=1996&reportName=Revenue Bill By Routing&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}
	
function validateCommodity(){
	var url='viewReportWithParam.html?id=1997&reportName=Revenue Bill By Commodity&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}
	
function validateOC(){
	var url='viewReportWithParam.html?id=1999&reportName=Revenue for Bill To By Origin Country&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}
function validateDC(){
	var url='viewReportWithParam.html?id=1998&reportName=Revenue for Bill To By Destination Country&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['viewDashboard'].action =url;
	document.forms['viewDashboard'].submit();
	return true;
	}

</script>

</head> 
<s:form id="viewDashboard" name="viewDashboard" action="viewDashboard" method="post" validate="true">   
 <div id="Layer1" align="center" style="width:100%; margin:0px; padding: 0px;  ">
 <div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content" style="padding-right:0px;padding-left:0px;">
<table cellspacing="2" cellpadding="2" border="0" width="95%" style="margin:0px auto;padding:0px;">
<tbody>	
  <tr>
    <td align="center" style="margin: 0px; padding: 0px"> <iframe id="2008" name="2008" style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2008&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML"  WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
	<td align="center" style="margin: 0px; padding: 0px"><iframe id="2009" name="2009" style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2009&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML"  WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
  </tr>
  <tr>
	 <td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateJob();"/><a href="javascript:void(0)" onClick="return validateJob();"> XLS </a></div></td>
  	 <td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateMode();"/><a href="javascript:void(0)" onClick="return validateMode();"> XLS </a></div></td>
  </tr>
  <tr>
    <td align="center" style="margin: 0px; padding: 0px"> <iframe id="2010" name="2010" style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2010&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML"  WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
	<td align="center" style="margin: 0px; padding: 0px"> <iframe id="2011" name="2011"  style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2011&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML" WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
   </tr>
  <tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateCommodity();"/><a href="javascript:void(0)" onClick="return validateCommodity();"> XLS </a></div></td>
  	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateRouting();"/><a href="javascript:void(0)" onClick="return validateRouting();"> XLS </a></div></td>
  </tr>
  <tr>
    <td align="center" style="margin: 0px; padding: 0px"><iframe id="2012" name="2012" style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2012&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML"  WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
	<td align="center" style="margin: 0px; padding: 0px"><iframe id="2013" name="2013" style="margin: 0px; padding: 0px;" src ="/redsky/viewDashboard.html?id=2013&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=HTML"  WIDTH=485 HEIGHT=400 FRAMEBORDER=0 scrolling="auto"> </iframe></td>
  </tr>
  <tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateOC();"/><a href="javascript:void(0)" onClick="return validateOC();"> XLS </a></div></td>
  	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateDC();"/><a href="javascript:void(0)" onClick="return validateDC();"> XLS </a></div></td>
  </tr> 
  
 <tr>
 </tr>
 <tr>
 </tr>

</tbody></table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div> 
</div>
</div>
</div>      
</s:form>
<script type="text/javascript">
</script>
