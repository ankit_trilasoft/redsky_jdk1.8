<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title></title>   
    <meta name="heading" content="Amount"/> 
     <style>

#ajax_tooltipObj .ajax_tooltip_arrow{	/* Left div for the small arrow */
	background-image:url('../images/tarrow-blue.gif');
	width:13px;
	position:absolute;
	left:6px;
	top:15px;
	background-repeat:no-repeat;
	background-position:center left;
	z-index:1000005;
	height:26px;
}
#ajax_tooltipObj .ajax_tooltip_content{
-moz-border-radius:8px 8px 8px 8px;
-moz-box-shadow:2px 2px #A9A9A9;
background-color:#F2F2F2;
border:1px solid #55a3ec;
color:#585D68;
font-size:0.8em;
left:18px;
overflow:auto;
min-height:40px;
padding:5px;
position:absolute;
top:0;
width:150px;
z-index:1000001;
}
     </style>    
</head>   
<s:form id="AmountForm" method="post" validate="true"> 

<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100px;">
<tr> 
	<td align="left"></td>
</tr>
<tr>
<td style="padding-top:16px;padding-left:8px;">
<b style="font-size:12px;">Balance&nbsp;Amount:&nbsp;&nbsp;</b><b style="font-size:10px;">${totalAmmount}</b>
</td>
</tr>
</table>

</s:form>