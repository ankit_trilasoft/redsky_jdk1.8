// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

CREATE  TABLE `redsky`.`partnerbankinformation` (
 `id` BIGINT(20)   NOT NULL AUTO_INCREMENT ,
 `cropId` VARCHAR(15) DEFAULT  NULL ,
  `updatedBy` VARCHAR(225) DEFAULT NULL ,
  `updatedOn` DATETIME DEFAULT NULL ,
  `createdBy` VARCHAR(225) DEFAULT NULL ,
  `createdOn` DATETIME DEFAULT NULL ,
  `partnerCode`  VARCHAR(8) NOT NULL ,
   `currency` VARCHAR(4)  DEFAULT NULL ,
  `bankAccountNumber` VARCHAR(40) DEFAULT  NULL ,
   PRIMARY KEY (`id`) );
   
   ALTER TABLE vehicle ADD engineNumber varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL;
   
   update networkdatafields set transactionType ='Create'   where fieldName in ('unit1','unit2') and modelName ='Miscellaneous';
   
   ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `updatedBy` VARCHAR(82),
   ADD COLUMN `updatedOn` DATETIME ,
   ADD COLUMN `createdBy` VARCHAR(82) ,
   ADD COLUMN `createdOn` DATETIME;
   
   CREATE TABLE `redsky`.`reportsfieldsection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpId` varchar(15) DEFAULT NULL,
  `reportId` varchar(20) NOT NULL,
  `fieldvalue` varchar(50) DEFAULT NULL,
  `fieldname` varchar(50) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  PRIMARY KEY(`id`)
);

ALTER TABLE `redsky`.`dspdetails`
 MODIFY COLUMN `ONG_dateType1` VARCHAR(150),
 MODIFY COLUMN `ONG_dateType2` VARCHAR(150),
 MODIFY COLUMN `ONG_dateType3` VARCHAR(150);
 
 ALTER TABLE `redsky`.`partnerbankinformation` ADD COLUMN `status` BIT(1) DEFAULT b'1' AFTER `bankAccountNumber`;
 
 ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `VIS_visaExtensionNeeded` VARCHAR(2) AFTER `FLB_displyOtherVendorCode`;
 
 ALTER TABLE `redsky`.`mssgrid` ADD COLUMN `inventoryFlag` VARCHAR(5) AFTER `updatedBy`;
 
Alter table updaterule add ruleNumber bigint(20);
Alter table updateruleresult add ruleNumber bigint(20);
Alter table updateruleresult add orderId bigint(20);
Alter table updateruleresult add shipNumber varchar(20);
Alter table updateruleresult add updatedRuleId bigint(20);

update serviceorder set incExcServiceTypeLanguage='en' where corpId='VOER' and incExcServiceTypeLanguage is null;

update refmaster set flex5='MR3' where code in ('3','1','4','12','14','2','15','Z') and corpid='SSCW' and parameter='HOUSE';
update refmaster set flex5='MR4' where code in ('16','17') and corpid='SSCW' and parameter='HOUSE';