<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 

<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
 <style>
 span.pagelinks {
display:block;font-size:0.85em;margin-bottom:5px;margin-top:-18px;
padding:2px 0;text-align:right;width:106%;
}
.tdheight-footer {
border: 1px solid #e0e0e0; padding: 0;
}
.table th.disrightborder{text-align: left;border-right:medium solid #8A3319;} 
}
</style>
</head>   
<div id="Layer5" style="width:100%"> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:form id="serviceForm1" name="serviceForm1" action="updateSOfromaccountLines"  onsubmit="return submit_form()" method="post">  
<s:hidden name="vanLineAccountView" id ="vanLineAccountView"/>
<s:hidden name="generateMassage" />
<s:hidden name="accountIdCheck"/>
<s:hidden name="activateAccPortal" id="activateAccPortalID"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="accountInterface" value="${accountInterface}"/>
<s:hidden name="multiCurrency" value="${multiCurrency}"/>
<s:hidden name="accountLineStatus"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" /> 
<s:hidden name="serviceOrder.registrationNumber" /> 
<s:hidden name="serviceOrder.id"/>
<s:hidden name="customerFile.id"/>
<s:hidden name="billToCodeForTicket"/>
<s:hidden name="billing.billToAuthority"/>
<s:hidden name="billing.billToCode"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>

<c:if test="${not empty billing.billComplete}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
			 <s:hidden  name="billing.billComplete" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty billing.billComplete}">
		 <s:hidden  name="billing.billComplete"/> 
	 </c:if>

<s:hidden name="emptyList"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>

<s:hidden name="systemDefaultmiscVl" value="%{systemDefaultmiscVl}" />
<s:hidden name="systemDefaultstorage" value="%{systemDefaultstorage}" />
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
<c:set var="billingFlag" value="${billingFlag}" scope="request"/> 
<s:hidden name="billingContractFlag" value="${billingContractFlag}"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>

<div id="newmnav" style="float: left;">
  <ul>
  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
  <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
  <c:set var="relocationServicesKey" value="" />
  <c:set var="relocationServicesValue" value="" /> 
  <c:forEach var="entry" items="${relocationServices}">
         <c:if test="${relocationServicesKey==''}">
         <c:if test="${entry.key==serviceOrder.serviceType}">
         <c:set var="relocationServicesKey" value="${entry.key}" />
         <c:set var="relocationServicesValue" value="${entry.value}" /> 
         </c:if>
         </c:if> 
     </c:forEach>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  <li id="newmnav1" style="background:#FFF "><a href="accountLineList.html?sid=${serviceOrder.id}" class="current" ><span>Accounting<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
   <c:if test="${serviceOrder.job !='RLO'}"> 
   		<c:if test="${forwardingTabVal!='Y'}"> 
			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
		</c:if>
		<c:if test="${forwardingTabVal=='Y'}">
			<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
		</c:if>
  </c:if>
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS' || billing.billToCode =='P4071'}">
  <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	 <c:if test="${serviceOrder.job =='INT'}">
	   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	 </c:if>
  </sec-auth:authComponent> 
  <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
 
 <c:if test="${serviceOrder.job !='RLO'}"> 
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li> 
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </c:if>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  <configByCorp:fieldVisibility componentId="component.field.Alternative.pricingWizardTab">
  <%-- <li><a href="pricingWizardSummary.html?from=Accounting&shipnumber=${serviceOrder.id}" ><span>Pricing</span></a></li> --%>
  </configByCorp:fieldVisibility>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;"><tr>
	<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr></table>
<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!padding-bottom:6px;"></div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
	<tr> 
	<td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0" width="">
		  <tbody> 		  
		  <tr>
               <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">          	  	
		  	<tr><td align="right" width="52">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td>
		  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   size="22"  cssClass="input-textUpper" readonly="true"/>
		  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="14" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="2" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="2" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="4" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.routing"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right"><fmt:message key='customerFile.status'/></td>
            <td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" size="3" readonly="true"/> </td>
		  	</tr>
		  	<tr>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td>
		  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td>
		  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="14" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="2" readonly="true"/></td>
			 <c:if test="${serviceOrder.job!='RLO'}">
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="2" readonly="true"/></td>
		  	</c:if>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td>
		  	<td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="43" readonly="true"/></td>
		  	</tr>
		 	</table>
		 	</td>
		 
		 	</tr>
			<tr>
	            <td align="right" class="listwhitetext" width="56">Bkg. Agent&nbsp;</td>
	            <td><s:textfield cssClass="input-textUpper" name="serviceOrder.bookingAgentCode" size="15" maxlength="8" readonly="true"/></td>
				<td align="right" class="listwhitetext" >Division</td>
				<td><s:textfield cssClass="input-textUpper" name="serviceOrder.companyDivision" size="6" maxlength="3" readonly="true"/></td>
				
				<td align="left" class="listwhitetext" colspan="11">
				<c:if test="${from=='rule'}">
				<c:if test="${field=='serviceorder.revised'}">
				<font color="red">Revised</font>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='serviceorder.revised'}">
				Revised
				</c:if></c:if>
				<c:if test="${from!='rule'}">
				Revised
				</c:if>
				<c:if test="${serviceOrder.revised == true}">
				    <input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)" checked/>
				    </c:if>
					<c:if test="${serviceOrder.revised == false || serviceOrder.revised == null}">
					<input type="checkbox" style="margin-left:0px;vertical-align:bottom;" id="checkboxId" value="${serviceOrder.id}" onclick="checkRevisedId(${serviceOrder.id},this)"/>
					</c:if>
					
				<c:if test="${from=='rule'}">
				<c:if test="${field=='serviceorder.est'}">
				<font color="red">Est</font>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='serviceorder.est'}">
				&nbsp;Est
				</c:if></c:if>
				<c:if test="${from!='rule'}">
				&nbsp;Est
				</c:if>
				<c:if test="${serviceOrder.est == true}">
				    <input type="checkbox" style="margin-left:0px;vertical-align:bottom;" value="${serviceOrder.id}" onclick="checkEstId(${serviceOrder.id},this)" checked/>
				    </c:if>
					<c:if test="${serviceOrder.est == false || serviceOrder.est == null}">
					<input type="checkbox" style="margin-left:0px;vertical-align:bottom;" value="${serviceOrder.id}" onclick="checkEstId(${serviceOrder.id},this)"/>
					</c:if>
					
				&nbsp;Contract&nbsp;<s:textfield cssClass="input-textUpper" name="billing.contract" size="25"  readonly="true" />
				&nbsp;&nbsp;&nbsp;Instruction&nbsp;<s:textfield cssClass="input-textUpper" name="billing.billingInstruction" size="43"  readonly="true" /></td>   
			</tr>
			</table>
			</td>
			</tr>
			<tr>
<td align="left" colspan="18" class="vertlinedata"></td>
</tr>
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="">
<tbody>

	<tr>
	  <c:if test="${serviceOrder.job!='RLO'}">
	<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
                                 <td height="25" align="right" class="listwhitetext">Est&nbsp;Wght</td>
                     
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
                               </c:if>
                               <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="right" class="listwhitetext">Act&nbsp;Wght</td>
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                  <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true"/></td>
                                 </configByCorp:fieldVisibility>
                                 </c:if>
                                   <c:if test="${serviceOrder.job!='RLO'}">
                                 <configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
                                 <td height="25" align="right" class="listwhitetext">Est&nbsp;Vol.</td>
                     				 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                 </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicMtr}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicMtr}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicFeet}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="right" class="listwhitetext">Act&nbsp;Vol.</td>
                                 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicMtr}" size="6" required="true" maxlength="10" readonly="true"/></td>
                                </c:if>
                                  <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicMtr}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                 <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicFeet}" size="6" maxlength="10" required="true" readonly="true"/></td>
                                 </c:if>
                                 
                                 <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true"/></td>
                                 </configByCorp:fieldVisibility>			
                                 </c:if>
      <configByCorp:fieldVisibility componentId="customerFile.survey">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
	<c:if test="${not empty customerFile.survey}">
	<s:text id="customerFileSurveyFormattedValues" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="Survey" value="${customerFileSurveyFormattedValues}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
	</c:if>
	<c:if test="${empty customerFile.survey}">
	<td><s:textfield cssClass="input-textUpper" name="Survey" value="${customerFileSurveyFormattedValues}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
	</c:if>
	</configByCorp:fieldVisibility> 
	 <configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
	<c:if test="${not empty trackingStatus.surveyDate}">
	<s:text id="trackingStatusSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.surveyDate"/></s:text>
	<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.surveyDate}">
	<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	</configByCorp:fieldVisibility> 
	  <c:if test="${serviceOrder.job!='RLO'}">
    <td align="right" class="listwhitetext">Load&nbsp;Date</td>
	<c:if test="${empty trackingStatus.loadA}">
	<c:if test="${empty trackingStatus.beginLoad}"> 
	<td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${not empty trackingStatus.beginLoad}">
	<td width="10px"><font color="red" size="1">(T)</font></td>
	<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>  
	</c:if>
	<c:if test="${not empty trackingStatus.loadA}">
	<td width="10px"><font color="red" size="1">(A)</font></td>
	<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<td align="right" class="listwhitetext">Delivery</td>
	<c:if test="${empty trackingStatus.deliveryA}">
	<c:if test="${empty trackingStatus.deliveryShipper}"> 
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>
	<c:if test="${not empty trackingStatus.deliveryShipper}">
	<td width="10px"><font color="red" size="1">(T)</font></td>
	<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if>  
	</c:if>
	<c:if test="${not empty trackingStatus.deliveryA}">
	<td width="10px"><font color="red" size="1">(A)</font></td>
	<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
	<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
	</c:if> 
	</c:if>
	<td align="right" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
	<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="20"  maxlength="250" readonly="true"/></td>
       <configByCorp:fieldVisibility componentId="component.field.serviceOrder.coordinator">
         <td align="right" class="listwhitetext"><fmt:message key="serviceOrder.coordinator"/></td>
            <td align="left"><s:textfield name="serviceOrder.coordinator" cssClass="input-textUpper" size="18" readonly="true"/></td>
           </configByCorp:fieldVisibility>
	</tr>
										
</tbody>
</table>
</td>
</tr> 
<tr>
<td height="4px" align="left" colspan="18" style="!padding-bottom:4px;" class="vertlinedata"></td>
</tr>
  
                              
                            
</tbody>
</table> 
</div>
<div class="bottom-header" style="margin-top:28px;!margin-top:44px;">
<span style="margin-left:50px; padding-top:5px">
<c:if test="${billing.cod}">
  <div style="position:absolute; width:95%; text-align:center; font-size:14px;padding-top:3px; font-family:Tahoma,Calibri,Verdana,Geneva,sans-serif; font-weight:bold; color:#fe0303;"> COD </div>
<img id="cod1" src="${pageContext.request.contextPath}/images/cod-blue.png" width="95%" />
                              
      </c:if>

</span></div>
</div>
<font color="red" style="position:absolute;text-align:center;width:100%;font:12px arial,verdana;"><b>
<script type="text/javascript"> 
document.forms['serviceForm1'].elements['generateMassage'].value = getCookie('generateMassage');
if(document.forms['serviceForm1'].elements['generateMassage'].value == "invoiceMassage"){ 
	document.write("Invoice has been Generated successfully.");
	document.forms['serviceForm1'].elements['generateMassage'].value="";
}
   document.cookie = 'generateMassage' + "=; expires=" +  new Date().toGMTString();
</script>

</b></font>
</div>

<div style="margin-bottom:15px;">
<c:choose>
<c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
<input type="button" class="cssbuttonA" style="width:45px; height:25px" 
        onclick="editNewAccLine();"  
        value="<fmt:message key="button.add"/>" disabled="disabled"/>
</c:when>
<c:otherwise>
<c:choose> 
<c:when test="${serviceOrder.job !='' && serviceOrder.job!=null}"> 
<c:if test="${billingFlag ==1 && billingContractFlag=='Y'}">
	     <input type="button" class="cssbuttonA" style="width:45px; height:25px" 
        onclick="checkBillingComplete();" 
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<c:if test="${billingFlag ==0 || billingContractFlag=='N'}">
	     <input type="button" class="cssbuttonA" style="width:45px; height:25px" 
	      onclick="showMessage();" value="<fmt:message key="button.add"/>"/>   
	</c:if>
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:45px; height:25px" 
	      onclick="showJobMessage();" value="<fmt:message key="button.add"/>"/>  
</c:otherwise>
</c:choose></c:otherwise></c:choose>
 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
<c:if test="${not empty vanLineAccountCategoryMap}" > 
<c:choose> 
<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
	<c:choose> 
		<c:when test="${billToLength>0}">
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:otherwise>
	</c:choose>
	
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
</c:otherwise>
</c:choose>		
</c:if>
<c:if test="${empty vanLineAccountCategoryMap}" > 
		<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" disabled="disabled"/>
 </c:if>
</sec-auth:authComponent>
</c:if>	 
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
<c:if test="${emptyList!=true}" >
<c:choose> 
<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
<c:choose> 
		<c:when test="${billToLength>0}">
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:otherwise>
	</c:choose>
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:110px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
</c:otherwise>
</c:choose>		
</c:if>
<c:if test="${emptyList==true}" > 
		<input type="button" class="cssbuttonA" style="width:110px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:if>
</sec-auth:authComponent>

</c:if>	
<%--<c:if test="${serviceOrder.defaultAccountLineStatus=='false'}">--%>
<input type="button" class="cssbuttonA" style="width:130px; height:25px" onclick="findDefaultLine();" 	value="Add Default Template" />
<%--  </c:if>
	<c:if test="${serviceOrder.defaultAccountLineStatus=='true'}">
         <input type="button" class="cssbuttonA" style="width:130px; height:25px" onclick="defaultLineMassage()" value="Add Default Template" />
	</c:if> --%>
   <sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
   <c:if test="${accountInterface=='Y'}">
   <c:if test="${emptyList!=true}" >
    <input type="button" class="cssbuttonA" style="width:150px; height:25px"   value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');"/>
	</c:if>
	<c:if test="${emptyList==true}" >
	<input type="button" class="cssbuttonA" style="width:150px; height:25px"   value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');" disabled="disabled"/> 
	</c:if>
	</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	 <c:choose>
    <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
    </c:when>
    <c:otherwise>
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();"/>
   </c:if> 
   <c:if test="${emptyList==true}" >
   <input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
   </c:if>
   </c:otherwise></c:choose>  
    </sec-auth:authComponent>
    
      <td> <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && serviceOrder.job !='' && serviceOrder.job!=null}">
   							 <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="SynchBilling"  value="Synch Billing" onclick="countInvoice();"/>
					      </c:if>
		          </td>
    <sec-auth:authComponent componentId="module.accountLineReset.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	<c:if test="${accountInterface=='Y'}">
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();"/>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();" disabled="disabled"/>
    </c:if>
    </c:if>
    </sec-auth:authComponent> 
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.edit"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
   </c:if>
   <c:if test="${emptyList==true}" >
   <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
   </c:if>
    </configByCorp:fieldVisibility>
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editUGSG"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Added By Kunal For Ticket Number: 6858 -->
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editCWMS"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Modification Closed -->
    <sec-auth:authComponent componentId="module.accountLineDeleteSelectedInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">                                                                                                   
     <c:if test="${emptyList!=true}" >
      <input type="button" class="cssbuttonA" style="width:150px; height:25px"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:150px; height:25px"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();" disabled="disabled"/>
     </c:if>
    </sec-auth:authComponent>
  
    <c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
     <c:if test="${not empty vanLineAccountCategoryMap}"> 
     <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0 }" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0 && serviceOrder.job !='' && serviceOrder.job!=null}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
    
    <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
    </c:if>
    <c:if test="${empty vanLineAccountCategoryMap}"> 
    <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     </c:if>
    </c:if>
     <c:if test="${emptyList!=true}" > 
     <c:choose>
    <c:when test="${!trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup }">
    <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
    </c:when>
    <c:otherwise>
    <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" onclick="javascript:openWindow('copyEstimates.html?sid=${serviceOrder.id}&decorator=popup&popup=true')" />
    </c:otherwise>
    </c:choose> 
     </c:if>
    <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
    </c:if>    
    <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
    		<c:if test="${commissionJobName == 'Yes'}">
    		 <c:if test="${emptyList!=true}" >
    			<%-- <c:if test="${checkCommision == 0 }"> --%>
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission(false)" />
    		<%-- </c:if>
    		<c:if test="${checkCommision > 0}" >
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission()" disabled="disabled"  />
    		</c:if> --%>
    		</c:if>
    		 <c:if test="${emptyList==true}" >
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission(false)" disabled="disabled"  />
    		</c:if>
    	</c:if>
    </configByCorp:fieldVisibility>
       <configByCorp:fieldVisibility componentId="component.button.SSCW.showAdvances">
		<input type="button" class="cssbuttonA" style="width:120px; height:25px; margin-top: 5px;"  name="Show Advances"  value="Show Advances" onclick="getAdvDtls(this)" />
	</configByCorp:fieldVisibility>
    
	<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	 <c:choose>
    <c:when test="${trackingStatus.soNetworkGroup }">
     <input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
    </c:when>
    <c:otherwise>
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();"/>
   </c:if> 
   <c:if test="${emptyList==true}" >
   <input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
   </c:if>
   </c:otherwise></c:choose>  
    </sec-auth:authComponent>
    
    <c:if test="${((!(trackingStatus.accNetworkGroup)) && billingCMMContractType && trackingStatus.soNetworkGroup) || (networkAgent && billingCMMContractType )}">
       
    <input type="button" class="cssbuttonA" style="width:80px; height:25px;margin-top: 5px;"  name="MGMTFees"  value="MGMT Fees" onclick=" calculateMGMTFees()"   />
    
    <%-- <c:if test="${checkGenrateRecInvoiceForMGMT==0 }" > 
    <input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="MGMTFees"  value="MGMT Fees" onclick="  " disabled="disabled"  />
    </c:if>--%>
    </c:if> 
    <c:if test="${((!(trackingStatus.accNetworkGroup)) && (billingDMMContractType) && (trackingStatus.soNetworkGroup))  || (networkAgent && billingDMMContractType ) }">
     <input type="button" class="cssbuttonA" style="width:100px; height:25px;margin-top: 5px;"  name="DiscountFees"  value="Discount Fees" onclick=" calculateDiscountFees()"   />
     </c:if> 
</div>

<c:if test="${serviceOrder.vip}">
		<div style="position: absolute; z-index: 1; top:292px;!top:300px; margin-left:92%; margin-left:93%;">
		<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
		</div>
		</c:if>
<c:if test="${accountLineStatus=='true'}">
  <div id="newmnav" style="margin-left:12px;">
		  <ul>
		  <c:if test="${quotesToValidate=='QTG'}"><li><a onclick="openEstPricing();"><span>Estimate&nbsp;Pricing</span></a></li></c:if>
		    <li><a onclick="openActiveList();"><span>Active List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		    	<li  id="newmnav1" style="background:#FFF"><a class="current"><span>Category List</span></a></li>
		    </c:if>
		    <li><a onclick="openAllList();"><span>All List</span></a></li>
		    
  </ul>
		</div>
		<c:if test="${accountInterface=='Y'}">
		<div id="KeyDiv" class="key_acc" >&nbsp;</div>
		</c:if>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
</c:if>

<c:if test="${accountLineStatus=='allStatus'}">
  <div id="newmnav" style="margin-left:12px;">
		  <ul>
		    <li><a onclick="openActiveList();" ><span>Active List</span></a></li>
		    <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		    	<li><a onclick="openViewList('category','accountCategoryViewList.html');"><span>Category List</span></a></li>
		    </c:if>
		    <li id="newmnav1" style="background:#FFF">
		    <a class="current">
		    <span>All List</span></a></li>
  </ul>
		</div>
		<c:if test="${accountInterface=='Y'}">
		<div id="KeyDiv" class="key_acc" >&nbsp;</div>
		
	    </c:if>
	    
	 
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
</c:if>
     
  <c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
  
  <c:set var="buttons"> 
  <c:choose>
<c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
<input type="button" class="cssbuttonA" style="width:45px; height:25px"  
        onclick="editNewAccLine();"  
        value="<fmt:message key="button.add"/>" disabled="disabled"/>
</c:when>
<c:otherwise> 
 <c:choose> 
<c:when test="${serviceOrder.job !='' && serviceOrder.job!=null}">  
    <c:if test="${billingFlag ==1 && billingContractFlag=='Y'}">
	     <input type="button" class="cssbuttonA" style="width:45px; height:25px"  
        onclick="checkBillingComplete();"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
    <c:if test="${billingFlag ==0 || billingContractFlag=='N'}">
	     <input type="button" class="cssbuttonA" style="width:45px; height:25px" 
	      onclick="showMessage();" value="<fmt:message key="button.add"/>"/>   
	</c:if>
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:45px; height:25px" 
	      onclick="showJobMessage();" value="<fmt:message key="button.add"/>"/>  
</c:otherwise>
</c:choose></c:otherwise></c:choose>     

</c:set>   
</div> 

 <fmt:setLocale value="en-US" />
<s:set name="checkLHF" id="checkLHF"  value="false" scope="request" />
<s:set name="idForCheckLHF" id="idForCheckLHF" value="0" scope="request" />

<div style="width:80%">		
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
<tr>
<td>

<c:forEach var="outerMap" items="${vanLineAccountCategoryMap}" varStatus="rowCounter">
<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;">
<tr>
<td align="left" colspan="10" style="padding: 0px 0px 0px 0px;">
		<div onClick="setAnimatedColCatFocus('cat_${outerMap.key}')" style="margin:0px">
			<table cellpadding="0" cellspacing="0"  border="0" style="width:100%;margin:0px;padding:0px;">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center" ><c:out value="${outerMap.key}"/></td>
					<td width="15" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center" style="cursor:default; " >
						<c:if test="${rowCounter.count == 1 }"> 
							<a href="javascript:collapseFun('block')">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:collapseFun('none')">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> </span>
						</c:if>
					</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>
		<div id="${outerMap.key}" class="switchgroup1" >
		<div id="cat_${outerMap.key}" style="display: block;">
		
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;margin-top:3px;" >	
<tr>
<td width="217" style="border-right:medium solid #8A3319;"></td>
<td class="Estheading-bg" style="margin:0px;padding:0px;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">

<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;width:100%;padding-right:8%;">
<tbody><tr>
<td style="padding-left: 6%;"><span style="line-height:25px;">Actual Receivable</span></td>
<td style="width: 39px;"></td>
<td style="padding-left:15%;">Actual Payable</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td colspan="3">
<s:set name="actualExp" id="actualExp" value="0" scope="request" />
<s:set name="actualRev" id="actualRev" value="0" scope="request" />
<s:set name="distAmt" id="distAmt" value="0" scope="request" />

<display:table name="${outerMap.value}" class="table" requestURI="" id="accountLineList" style="width:1050px;margin-bottom:3px;" pagesize="100" >   

<c:choose> 
     <c:when test="${serviceOrder.job !='' && serviceOrder.job!=null}">  
 		<c:if test="${billingFlag ==1 && billingContractFlag=='Y'}">                                                                 
			<display:column title="#"  style="width:40px;padding:0px;" >
			<a href="#" onclick="goToEditAccountLinePage(${serviceOrder.id},${accountLineList.id})"/>
			<c:out value="${accountLineList.accountLineNumber}" /></a>
			</display:column>
		</c:if>
		<c:if test="${billingFlag ==0 || billingContractFlag=='N'}">
			<display:column title="#"  style="width:40px;padding:0px;"><a href="#" onclick="showMessage();"/><c:out value="${accountLineList.accountLineNumber}" /></a></display:column>
		</c:if>
	</c:when>
	<c:otherwise>
           <display:column title="#"  style="width:40px;padding:0px;"><a href="#" onclick="showJobMessage();"/><c:out value="${accountLineList.accountLineNumber}" /></a></display:column>
    </c:otherwise>
</c:choose>	
<display:column title="Act"   style="width:50px;padding:0px;">
		<c:choose> 
        <c:when test="${((!(trackingStatus.accNetworkGroup)) && (billingCMMContractType) && (trackingStatus.soNetworkGroup)) || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLineList.chargeCode == 'DMMFEE' || accountLineList.chargeCode == 'DMMFXFEE' || accountLineList.createdBy == 'Networking' )) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLineList.chargeCode == 'MGMTFEE') ||(accountLineList.chargeCode == 'MGMTFEE') || (billingDMMContractType && (accountLineList.chargeCode == 'DMMFEE' || accountLineList.chargeCode == 'DMMFXFEE' ))}">
        <c:if test="${accountLineList.status}">
        	<input type="checkbox"   checked="checked" disabled="disabled"   />
        </c:if>
        <c:if test="${accountLineList.status==false}">
		    <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
        </c:when>
        <c:otherwise> 
          <c:if test="${accountLineList.status}">
          <c:if test="${accountInterface!='Y'}">
          <c:if test="${accountLineList.actualRevenue!='0.00' || accountLineList.actualExpense!='0.00'}">
          <input type="checkbox"   checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLineList.id},this)" />
	      </c:if>
	      <c:if test="${accountLineList.actualRevenue=='0.00' && accountLineList.actualExpense=='0.00'}">
          <input type="checkbox"   checked="checked" onclick="inactiveStatusCheck(${accountLineList.id},this)" />
	      </c:if>
	      </c:if>
	      <c:if test="${accountInterface=='Y'}">
          <c:if test="${ not empty accountLineList.accruePayable || not empty accountLineList.accrueRevenue || not empty accountLineList.recAccDate || not empty accountLineList.payAccDate || not empty accountLineList.receivedInvoiceDate }">
          <input type="checkbox"   checked="checked" disabled="disabled" onclick="inactiveStatusCheck(${accountLineList.id},this)" />
	      </c:if>
	      <c:if test="${empty accountLineList.accruePayable && empty accountLineList.accrueRevenue && empty accountLineList.recAccDate && empty accountLineList.payAccDate && empty accountLineList.receivedInvoiceDate}">
          <input type="checkbox"   checked="checked" onclick="inactiveStatusCheck(${accountLineList.id},this)" />
	      </c:if>
	      </c:if>
	      </c:if>
	      <c:if test="${accountLineList.status==false}">
		    <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
		</c:otherwise></c:choose>
	     </display:column> 
	     <display:column title="INV"   style="width:50px;padding:0px;">
         <c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
         <input type="checkbox"   checked="checked" disabled="disabled"   />
         </c:if>
         <c:if test="${accountLineList.recInvoiceNumber == '' || accountLineList.recInvoiceNumber == null}">
         <c:if test="${accountLineList.selectiveInvoice}">
           <input type="checkbox"   checked="checked" onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />
         </c:if>
         <c:if test="${accountLineList.selectiveInvoice==false}">
		   <input type="checkbox"     onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />  
	     </c:if>
	     </c:if>
        </display:column>
<display:column property="chargeCode" title="Charge" maxLength="10" style="width:75px;padding:0px;"></display:column>
<display:column property="companyDivision" title="Div" headerClass="disrightborder containeralign" style="width:40px;padding:0px;border-right:medium solid #8A3319;"></display:column>

<display:column headerClass="containeralign" title="Revenue" style="width:70px; margin: 0px; text-align:right;padding-right:0em;!important ">
       <c:if test="${accountInterface=='Y'}">
       <c:choose> 
       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
	       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
	            <div style="margin:0px; text-align:right; width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesyellowtextUpper" ><fmt:formatNumber type="number"  
	            value="${accountLineList.actualRevenue}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
	           </div>
	        </c:if>
	       <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
	            <div style="margin:0px; text-align:right; width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesredtextUpper" ><fmt:formatNumber type="number"  
	            value="${accountLineList.actualRevenue}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
	           </div>
	        </c:if>
        </c:when>
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
        <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
            <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesgreentextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
        <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
            <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>

        </c:when> 
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
            <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesbluetextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
        </c:when>
        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
        <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
        <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
           </div>
           </c:if>
           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
			<div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
        </c:when>
         <c:otherwise>
          <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
          <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
            </div>
            </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
	          <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
            </div>
            </c:if>
         </c:otherwise>
        </c:choose>
        </c:if> 
        <c:if test="${accountInterface!='Y'}">
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
         <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
           </div>
           </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
         <div style="margin:0px; text-align:right;width:100%;" onmouseover="findToolTipForRevenue('${accountLineList.id}','actRev',this,'${accountLineList.statusDate}');" onmouseout="ajax_hideTooltip();"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
            </c:if>
        </c:if>
        <s:set name="actualRev" value="${actualRev + accountLineList.actualRevenue}"/>
       </display:column> 

       <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
       <c:if test="${!vanlineSettleColourStatus}"> 
       <display:column title="Distribution"  style="text-align:right;width:50px;padding-right:3px;">
       <c:choose> 
       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
        <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
            <div align="right" style="margin: 0px;"><font class="rulesyellowtextUpper" ><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
        </c:if>
        <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
            <div align="right" style="margin: 0px;"><font class="rulesredtextUpper" ><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
        </c:if>
        </c:when>
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
            <div align="right" class=""><font class="rulesgreentextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
            <div align="right" class=""><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
            </c:if>
        </c:when> 
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
            <div align="right" class=""><font class="rulesbluetextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
        </c:when>
        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
        <div align="right"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
           </div>
           </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
        <div align="right"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
            </c:if>
        </c:when>
         <c:otherwise>
          <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
          <div align="right"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
			</div>
			</c:if>
			<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
          	<div align="right"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
			</div>
			</c:if>
         </c:otherwise>
        </c:choose>
        <s:set name="distAmt" value="${distAmt + accountLineList.distributionAmount}"/>
       </display:column>
	</c:if>
	
	<c:if test="${vanlineSettleColourStatus}">
	 <display:column title="Distribution"  style="text-align:right;width:50px;padding-right:3px;">
       <c:choose> 
       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
            <div align="right" style="margin: 0px;"><font class="rulesyellowtextUpper" ><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
		<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
            <div align="right" style="margin: 0px;"><font class="rulesredtextUpper" ><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"   maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
		</c:if>           
        </c:when>
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
            <div align="right" class=""><font class="rulesgreentextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
            <div align="right" class=""><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
        </c:when> 
        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && not empty accountLineList.vanlineSettleDate}">
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
            <div align="right" class=""><font class="rulesbluetextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
           </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}"> 
            <div align="right" class=""><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
            </c:if>
        </c:when>
        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
        <div align="right"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
           </div>
           </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
        <div align="right"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
           </div>
            </c:if> 
        </c:when>
         <c:otherwise>
         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
          <div align="right"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
            </div>
            </c:if>
            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
          <div align="right"><font class="rulesredtextUpper"><fmt:formatNumber type="number"  
            value="${accountLineList.distributionAmount}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/></font>
            </div>
            </c:if> 
         </c:otherwise>
        </c:choose>
       <s:set name="distAmt" value="${distAmt + accountLineList.distributionAmount}"/>
       </display:column>
       </c:if>
	</c:if>
<display:column title="Invoice#"  style="width:90px;padding:0px 0px 0px 5px;">
	<c:out value="${accountLineList.recInvoiceNumber}" />
	<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
        <a><img align="top" title="Forms" onclick="findUserPermission1('${accountLineList.recInvoiceNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
    </c:if>
</display:column>
<display:column property="receivedInvoiceDate" title="Invoice Date" format="{0,date,dd-MMM-yyyy}" style="width:90px;padding:0px 5px 0px 5px;"></display:column>
<display:column title="Bill To" headerClass="disrightborder"  style="width:60px;padding:0px;border-right:medium solid #8A3319;">
	<span title="${accountLineList.billToName}">
		<c:out value="${accountLineList.billToCode}" />
	</span>
</display:column>

<display:column headerClass="containeralign" title="Expense" style="width:70px; text-align:right;padding-right:3px;!padding-right:1.5em;">
<span title="${accountLineList.note}">
		  <c:if test="${accountInterface=='Y'}">
		  <c:if test="${!payablesXferWithApprovalOnly}">
		  <c:choose> 
		  <c:when test="${not empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesyellowtextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesgreentextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesbluetextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" />
            </div>
          </c:when>
          <c:otherwise>
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" />
            </div>
          </c:otherwise>
          </c:choose>
          </c:if>
          <c:if test="${payablesXferWithApprovalOnly}">
		  <c:choose> 
		  <c:when test="${(not empty accountLineList.payPostDate ) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesyellowtextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesgreentextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><font class="rulesbluetextUpper"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" /></font>
            </div>
          </c:when>
          <c:when test="${(accountLineList.payingStatus=='A' || not empty accountLineList.payPostDate) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" />
            </div>
          </c:when>
          <c:otherwise>
		    <div align="right" style="margin:0px; text-align:right;width:100%;" onmouseover="" onmouseout=""><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" />
            </div>
          </c:otherwise>
          </c:choose>
          </c:if>
          </c:if>
          <c:if test="${accountInterface!='Y'}">
		    <div align="right" style="margin:0px; text-align:right;width:100%;"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
            groupingUsed="true" value="${accountLineList.actualExpense}" />
            </div>
          </c:if>
          </span>
          <s:set name="actualExp" value="${actualExp + accountLineList.actualExpense}"/>
        </display:column>
        
        <display:column title="P/S" style="width:10px;" >
	        <c:if test="${accountLineList.payingStatus=='A' || accountLineList.payingStatus=='S'}">
	         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	        </c:if>
	        <c:if test="${accountLineList.payingStatus=='N'}">
	        <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	        </c:if>
	        <c:if test="${accountLineList.payingStatus=='P'}">
	        <img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	        </c:if>
	        <c:if test="${accountLineList.payingStatus=='I'}">
	         <img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	        </c:if>
        </display:column>
        
<display:column property="invoiceNumber" title="Invoice#" maxLength="13"  style="width:90px;padding:0px 0px 0px 5px;"></display:column>
<display:column title="Vendor" maxLength="20"  style="width:90px;padding:0px 0px 0px 3px;">
	<span title="${accountLineList.estimateVendorName}">
		<c:out value="${accountLineList.estimateVendorName}" />
	</span>
</display:column>
<display:column property="note" title="Description"  maxLength="20" style="width:90px;padding:0px 0px 0px 3px;"/>
	<display:footer>
       <tr>
		          <td align="right" width="" colspan="5" style=" border-right:medium solid #878485;padding:0px; " class="tdheight-footer">Sub Total</td>
		  	      
                  <td align="right" class="tdheight-footer" style="!padding-right:5px;padding-right:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${actualRev}" /></div>
                  </td>
                  
                  <td align="right" width="" class="tdheight-footer" style="padding:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${distAmt}" /></div></td>
                  
                  <td colspan="3" class="tdheight-footer"></td>  
                  <td align="right" width="" class="tdheight-footer" style="border-left:medium solid #878485;padding:0px;" >
                  <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${actualExp}" />
                  </div>
                  </td>
                  
                  
                   <c:if test="${multiCurrency=='Y'}">  
                   <td class="tdheight-footer"></td>  
                   </c:if>
		  	      <td colspan="4" style="paddiing:0px;" class="tdheight-footer"></td>
		  	     
		</tr>
	</display:footer>
</display:table>


<s:set name="actualRev" value="0"/>
<s:set name="distAmt" value="0"/>
<s:set name="actualExp" value="0"/>
</td>
</tr>
</table>
</div>
</div>
</td>
</tr>
</table>
</c:forEach>



<c:if test="${empty vanLineAccountCategoryMap}">
<s:set name="accountLineList" value="accountLineList" scope="request"/>

<display:table name="accountLineList" class="table" requestURI="" id="accountLineList" style="width:1050px;margin-bottom:3px;margin-top:3px;" >   

<display:column property="accountLineNumber" title="#"  style="width:40px;padding:0px;"></display:column>
<display:column property="status" title="AC"  style="width:40px;padding:0px;"></display:column>
<display:column property="chargeCode" title="Charge" maxLength="10" style="width:75px;padding:0px;"></display:column>
<display:column property="companyDivision" title="Div" maxLength="10" style="width:50px;padding:0px;"></display:column>
<display:column property="actualRevenue" title="Revenue" maxLength="10" style="width:75px;padding:0px;"></display:column>
<display:column property="distributionAmount" title="Distribution"  style="text-align:right;width:50px;padding:0px;"></display:column>
<display:column property="recInvoiceNumber" title="Invoice#"  style="width:70px;padding:0px;"></display:column>
<display:column property="invoiceDate" title="Invoice Date" format="{0,date,dd-MMM-yyyy}" style="width:80px;padding:0px;"></display:column>

<display:column title="Bill To" ></display:column>
<display:column title="Expense" ></display:column>

<display:column property="invoiceNumber" title="Invoice#"  style="width:70px;padding:0px;"></display:column>
<display:column property="estimateVendorName" title="Vendor"  style="width:70px;padding:0px 0px 0px 3px;"></display:column>

<display:footer>
       <tr>
		          <td align="right" width="" colspan="5" style=" border-right:medium solid #878485;padding:0px; " class="tdheight-footer">Projected Actual Totals</td>
		  	      
                  <td align="right" class="tdheight-footer" style="!padding-right:5px;padding-right:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedActualRevenue}" /></div>
                  </td>
                  
                  <td align="right" width="" class="tdheight-footer" style="padding:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedDistributedTotalAmount}" /></div></td>
                  
                  <td colspan="3" class="tdheight-footer"></td>  
                  <td align="right" width="" class="tdheight-footer" style="border-left:medium solid #878485;padding:0px;" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedActualExpense}" /></div>
                  </td>
                  
                  
                   <c:if test="${multiCurrency=='Y'}">  
                   <td class="tdheight-footer"></td>  
                   </c:if>
		  	      <td colspan="2" style="paddiing:0px;" class="tdheight-footer"></td>
		  	     
		</tr>
		
		<tr>
	          <td colspan="2" align="right" >
	          	<input type="button" class="cssbuttonA" style="width:60px; height:20px; margin-top:-4px;" disabled="disabled" value="Inactivate" />
	          </td>
	          <td colspan="3" align="right"  style=" border-right:medium solid #878485; ">
	          	<b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b>
	          </td>
	          <td align="right" width="" style="padding:0px;">
	          		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.actualRevenue}" /></div>
              </td>
              <td align="right" width="" style="padding-right:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.distributedTotalAmount}" /></div>
              </td>
		  	  <td colspan="3" style="paddiing:0px;" class="tdheight-footer"></td>   
		  	  <td align="right" width="" style="padding:0px;border-left:medium solid #878485;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.actualExpense}" /></div>
              </td>
              <td colspan="2" style="paddiing:0px;" class="tdheight-footer"></td> 
		</tr>
		
		<tr>
	          <td align="right" width="" colspan="5" style=" border-right:medium solid #878485;padding:0px; " class="tdheight-footer">Gross&nbsp;Margin</td>
	          <td align="right" style="padding:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${serviceOrder.actualGrossMargin}" /></div>
              </td>
              <td colspan="4" style="border-right:medium solid #878485;paddiing:0px;" class="tdheight-footer"></td> 
              <td colspan="3" style="paddiing:0px;" class="tdheight-footer"></td> 
		</tr>
		
		<tr>
	         <td align="right" width="" colspan="5" style=" border-right:medium solid #878485;padding:0px; " class="tdheight-footer">Gross&nbsp;Margin%</td>
	         <td align="right" style="padding:0px;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="1"
                  groupingUsed="true" value="${serviceOrder.actualGrossMarginPercentage}" /></div>
             </td> 
             <td colspan="4" style="border-right:medium solid #878485;paddiing:0px;" class="tdheight-footer"></td> 
              <td colspan="3" style="paddiing:0px;" class="tdheight-footer"></td> 
		</tr>
		
		  	</display:footer>

</display:table>
</c:if>
</td>
</tr>
<c:if test="${not empty vanLineAccountCategoryMap}">
<tr>
<td colspan="50">
<table class="table" style="width:1050px;margin-bottom:3px;margin-top:10px;">
<thead>
		
		<tr>
	          <th align="right"  style="width:243px;border-right:medium solid #878485; ">
	          <span style="float:right; margin-top: 4px; font-size:13px;font-weight:bold;" >Projected Actual Totals</span> </th>
	          <th align="right" width="77px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;">
	          		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedActualRevenue}" /></div>
              </th>
              <th align="right" width="73px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedDistributedTotalAmount}" /></div>
              </th>
              <th width="90px">
              </th>
		  	  <th colspan="1" style="paddiing:0px;width:142px;" class="tdheight-footer"></th>   
		  	  <th align="right" width="79px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;border-left:medium solid #878485;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedActualExpense}" /></div>
              </th>
              <th colspan="2">
              </th>
              
		</tr>
		
		<tr>
	          <th align="right"  style="width:207px;border-right:medium solid #878485; ">
	           <input type="button" id="Inactivate"  name="Inactivate"  value="Inactivate" onclick="updateAccInactive();" class="cssbuttonA" style="width:60px; height:20px;" />
	          	<b><span style="float: right; margin-top: 4px;font-size:13px;font-weight:bold;"><fmt:message key="serviceOrder.entitledTotalAmounted"/></span></b>
	          </th>
	          <th align="right" width="77px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;">
	          		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.actualRevenue}" /></div>
              </th>
              <th align="right" width="73px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.distributedTotalAmount}" /></div>
              </th>
              <th width="90px">
              	<img class="openpopup" style=" margin-top:-10px;" height="20" width="17" src="${pageContext.request.contextPath}/images/invoice.png" title="Invoice List With Sub-Total" onclick="findInvoiceTotal(this);"/>
              </th>
		  	  <th colspan="1" style="paddiing:0px;width:142px;" class="tdheight-footer"></th>   
		  	  <th align="right" width="79px" style="border-left:medium solid #878485;padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.actualExpense}" /></div>
              </th>
              <th colspan="2">
              	<img class="openpopup" style=" margin-top:-10px;" height="20" width="17" src="${pageContext.request.contextPath}/images/invoice.png" title="Approved Vendor Invoice List" onclick="findVendorInvoiceTotal(this);"/>
              </th>
		</tr>
		
		<tr>
	          <th align="right"  style="width:207px;border-right:medium solid #878485;">
	          <span style="float:right; margin-top: 4px; font-size:13px;font-weight:bold;" >Gross&nbsp;Margin</span> </th>
	          <th align="right" width="77px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;">
	          		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${serviceOrder.actualGrossMargin}" /></div>
              </th>
              <th align="right" width="73px" style="padding-right:0px;" colspan="3"></th>
		  	  <th align="right" width="79px" style="padding:0px;border-left:medium solid #878485;" colspan="3"></th>
		</tr>
		
		<tr>
	          <th align="right"  style="width:207px;border-right:medium solid #878485; ">
	           <span style="float:right; margin-top: 4px; font-size:13px;font-weight:bold;" >Gross&nbsp;Margin% </span></th>
	          <th align="right" width="77px" style="padding: 0px 2px 0px 0px; font-size: 13px; font-weight: bold;">
	          		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="1"
                  groupingUsed="true" value="${serviceOrder.actualGrossMarginPercentage}" /></div>
              </th>
              <th align="right" width="73px" style="padding-right:0px;" colspan="3"></th>
		  	  <th align="right" width="79px" style="padding:0px;border-left:medium solid #878485;" colspan="3"></th>
		</tr>
       
		  	</thead>
 </table>
</td>
</tr>
</c:if>
</table>
</div>


<s:hidden name="serviceOrder.entitledTotalAmount" />
<s:hidden name="serviceOrder.estimatedTotalExpense" />
<s:hidden  name="serviceOrder.estimatedTotalRevenue" />
<s:hidden name="serviceOrder.distributedTotalAmount" />
<s:hidden name="serviceOrder.revisedTotalExpense" />
<s:hidden name="serviceOrder.revisedTotalRevenue" />
<s:hidden name="serviceOrder.actualExpense" />
<s:hidden name="serviceOrder.actualRevenue" />
<s:hidden name="serviceOrder.estimatedGrossMargin" />
<s:hidden name="serviceOrder.revisedGrossMargin" />
<s:hidden name="serviceOrder.actualGrossMargin" />
<s:hidden name="serviceOrder.estimatedGrossMarginPercentage" />
<s:hidden name="serviceOrder.revisedGrossMarginPercentage" />
<s:hidden name="serviceOrder.actualGrossMarginPercentage" /> 
		<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
		<s:hidden name="serviceOrder.projectedGrossMargin" />

    <s:hidden name="entitledTotal" />
    <s:hidden name="estimateRevenueAmt" />
    <s:hidden name="distributedSum" />
    <s:hidden name="estimateExp" />
    <s:hidden name="grossMrgn" />
    <s:hidden name="gmPercent" />
    <s:hidden name="revisedTotalRev"/>
    <s:hidden name="revisedTotal"/>
    <s:hidden name="revisedGrossMar"/>
    <s:hidden name="revisedGrossMarginPer"/>
    <s:hidden name="actualExpenseTotal" />
    <s:hidden name="actualRevenueTotal" />
    <s:hidden name="actualGrossMar" />
    <s:hidden name="actualGrossMarginPer" />
    <s:hidden name="jobtypeSO" value="${serviceOrder.job}"/>
    <s:hidden name="routingSO" value="${serviceOrder.routing}" />
    <s:hidden name="modeSO" value="${serviceOrder.mode}"/>
    <s:hidden name="billingContract" value="${billing.contract}"/>
    <s:hidden name="buttonType" />
    <s:hidden name="checkLHF" id="checkLHF" value="${checkLHF}" />
    <s:hidden name="idForCheckLHF" id="idForCheckLHF" value="${idForCheckLHF}" />
    <s:hidden name="componentId" value="module.accountLine" /> 
    <c:out value="${buttons}" escapeXml="false" /> 
    <s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
<c:if test="${emptyList!=true}" >
<c:choose> 
<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
<c:choose> 
		<c:when test="${billToLength>0}">
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:otherwise>
	</c:choose>
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
</c:otherwise>
</c:choose>		
</c:if>
<c:if test="${emptyList==true}" >
		<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:if>
</sec-auth:authComponent>
</c:if>	


<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
<c:if test="${emptyList!=true}" >
<c:choose> 
<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
<c:choose> 
		<c:when test="${billToLength>0}">
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbuttonA" style="width:100px; height:25px"
			value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:otherwise>
	</c:choose>
</c:when>
<c:otherwise>
<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
</c:otherwise>
</c:choose>		
</c:if>
 <c:if test="${emptyList==true}" >
		<input type="button" class="cssbuttonA" style="width:100px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
		</c:if>
</sec-auth:authComponent>

</c:if>	
<%--<c:if test="${serviceOrder.defaultAccountLineStatus=='false'}">--%>
<input type="button" class="cssbuttonA" style="width:130px; height:25px" onclick="findDefaultLine();" 	value="Add Default Template" />
<%--</c:if>
	<c:if test="${serviceOrder.defaultAccountLineStatus=='true'}">
         <input type="button" class="cssbuttonA" style="width:130px; height:25px" onclick="defaultLineMassage()" value="Add Default Template" />
	</c:if>--%>
<%--	<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	 <c:if test="${accountInterface=='Y'}">
	<input type="button" class="cssbuttonA" style="width:155px; height:25px"  onclick="findPostDateLine();" value="Apply Recv. Posting Date" />
   </c:if>
   </sec-auth:authComponent>--%>
   <sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
   <c:if test="${accountInterface=='Y'}">
    <c:if test="${emptyList!=true}" >
    <input type="button" class="cssbuttonA" style="width:150px; height:25px"   value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');"/>
	</c:if>
	<c:if test="${emptyList==true}" >
	<input type="button" class="cssbuttonA" style="width:150px; height:25px"   value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');" disabled="disabled"/>
	</c:if>
	</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	<c:choose>
    <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
    </c:when>
    <c:otherwise>
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();"/>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
    </c:if>
    </c:otherwise>
    </c:choose>
    </sec-auth:authComponent>
    		          <td> <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && serviceOrder.job !='' && serviceOrder.job!=null}">
   							 <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="SynchBilling"  value="Synch Billing" onclick="countInvoice();"/>
					      </c:if>
		          </td>
    
    
    <sec-auth:authComponent componentId="module.accountLineReset.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	<c:if test="${accountInterface=='Y'}">
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();"/>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();" disabled="disabled"/>
    </c:if>
    </c:if>
    </sec-auth:authComponent> 
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.edit"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
     </c:if>
    </configByCorp:fieldVisibility>
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editUGSG"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Added By Kunal For Ticket Number: 6858 -->
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editCWMS"> 
     <c:if test="${emptyList!=true}" >                                                                                                   
      <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Modification Closed -->
    <sec-auth:authComponent componentId="module.accountLineDeleteSelectedInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">                                                                                                   
       <c:if test="${emptyList!=true}" >
      <input type="button" class="cssbuttonA" style="width:150px; height:25px"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();"/>
       </c:if>
       <c:if test="${emptyList==true}" > 
       <input type="button" class="cssbuttonA" style="width:150px; height:25px"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();" disabled="disabled"/>
       </c:if>
    </sec-auth:authComponent>
    
    <c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
     <c:if test="${not empty vanLineAccountCategoryMap}">
     <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0 && serviceOrder.job !='' && serviceOrder.job!=null}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled"/>
    </c:if>
    </c:if>
    <c:if test="${empty vanLineAccountCategoryMap}"> 
  <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0}" >
     <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
     <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:95px; height:25px"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled"/>
    </c:if>
     </c:if>
    </c:if>
    <c:if test="${emptyList!=true}" >
    <c:choose>
    <c:when test="${!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup }">
    <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
    </c:when>
    <c:otherwise>
     <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" onclick="javascript:openWindow('copyEstimates.html?sid=${serviceOrder.id}&decorator=popup&popup=true')" />
    </c:otherwise>
    </c:choose>
    </c:if>
    <c:if test="${emptyList==true}" >
     <input type="button" class="cssbuttonA" style="width:100px; height:25px"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
    </c:if>
    
     <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
    		<c:if test="${commissionJobName == 'Yes'}">
    		 <c:if test="${emptyList!=true}" >
    			<%-- <c:if test="${checkCommision == 0 }"> --%>
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission(false)" />
    		<%-- </c:if>
    		<c:if test="${checkCommision > 0}" >
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission()" disabled="disabled"  />
    		</c:if> --%>
    		</c:if>
    		 <c:if test="${emptyList==true}" >
    			<input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="Commission"  value="Commission" onclick="calCommission(false)" disabled="disabled"  />
    		</c:if>
    	</c:if>
    </configByCorp:fieldVisibility>
    
     <configByCorp:fieldVisibility componentId="component.button.SSCW.showAdvances">
		<input type="button" class="cssbuttonA" style="width:120px; height:25px; margin-top: 5px;"  name="Show Advances"  value="Show Advances" onclick="getAdvDtls(this)" />
	</configByCorp:fieldVisibility>
	<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="htmlReplacement:disableKeyBoardInputScript">
	 <c:choose>
    <c:when test="${trackingStatus.soNetworkGroup }">
     <input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
    </c:when>
    <c:otherwise>
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();"/>
   </c:if> 
   <c:if test="${emptyList==true}" >
   <input type="button" class="cssbuttonA" style="width:110px; height:25px;margin-top: 5px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
   </c:if>
   </c:otherwise></c:choose>  
    </sec-auth:authComponent>
    
    <c:if test="${((!(trackingStatus.accNetworkGroup)) && billingCMMContractType && trackingStatus.soNetworkGroup) || (networkAgent && billingCMMContractType )}">
      
    <input type="button" class="cssbuttonA" style="width:80px; height:25px;margin-top: 5px;"  name="MGMTFees"  value="MGMT Fees" onclick=" calculateMGMTFees()"   />
     
    <%--<c:if test="${checkGenrateRecInvoiceForMGMT == 0 }" > 
    <input type="button" class="cssbuttonA" style="width:80px; height:25px"  name="MGMTFees"  value="MGMT Fees" onclick="  " disabled="disabled"  />
    </c:if>  
    --%></c:if> 
     <c:if test="${((!(trackingStatus.accNetworkGroup)) && (billingDMMContractType) && (trackingStatus.soNetworkGroup)) || (networkAgent && billingDMMContractType ) }">
     <input type="button" class="cssbuttonA" style="width:100px; height:25px;margin-top: 5px;"  name="DiscountFees"  value="Discount Fees" onclick=" calculateDiscountFees()"   />
     </c:if> 
    <%--  <input type="button" class="cssbuttonA" style="width:60px; height:25px"   value="Billing" onclick="openBillingWizard();"/>--%>
<%-- <c:if test="${empty sid}">
	<c:redirect url="/accountCategoryViewList.html?vanLineAccountView=${vanLineAccountView}&sid=${serviceOrder.id}&addTemplate=${addTemplate}"></c:redirect>
</c:if>  --%>

<c:if test="${btntype=='yes'}"> 
 <%-- <c:if test="${isPayableRequired=='YES'}"> 
	    <c:redirect url="/accountCategoryViewList.html?vanLineAccountView=${vanLineAccountView}&sid=${serviceOrder.id}&isPayableRequired=${isPayableRequired}"></c:redirect>
</c:if> --%>
 <%-- <c:if test="${isPayableRequired!='YES'}">
	   <c:redirect url="/accountCategoryViewList.html?vanLineAccountView=${vanLineAccountView}&sid=${serviceOrder.id}&addTemplate=${addTemplate}"></c:redirect>
</c:if> --%>
</c:if>
</s:form>

<%-- Script Shifted from Top to Botton on 07-Sep-2012 By Kunal --%>
<SCRIPT LANGUAGE="JavaScript">
function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer,jobType){
	window.open('viewFormParam.html?id='+id+'&invoiceNumber='+invNum+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID=${noteID}&custID=${custID}&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
	//&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&
	//window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
}
function findBookingAgentName(target){ 
     var billToCode=target; 
     var sid = document.forms['serviceForm1'].elements['sid'].value;
     var url="accountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
     //var url="accountList.html?decorator=simple&popup=true&ship=${serviceOrder.shipNumber}&invoiceBillToCode="+encodeURI(target);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
}
function checkEstId(id,targetElement) {
	var estStatus = targetElement.checked;
	var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;	
	var url="updateEstStatus.html?ajax=1&decorator=simple&popup=true&estStatus=" + encodeURI(estStatus)+"&ids=" + encodeURI(serviceOrderId);
	http333.open("GET", url, true);
    http333.onreadystatechange = handleHttpResponse300;
    http333.send(null);
} 
function handleHttpResponse300()
{
	if (http333.readyState == 4){
		var result= http333.responseText         
	}
}	

function checkRevisedId(id,targetElement) {
		var revisedStatus = targetElement.checked;
		var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value; 
		var url="updateRevisedStatus.html?ajax=1&decorator=simple&popup=true&revisedStatus=" + encodeURI(revisedStatus)+"&ids=" + encodeURI(serviceOrderId);
		http222.open("GET", url, true);
        http222.onreadystatechange = handleHttpResponse200;
        http222.send(null);
} 
function handleHttpResponse200(){
    if (http222.readyState == 4){
    	 var result= http222.responseText         
    }
}	
 

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse4() { 
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       results = results.replace('[','');
       results=results.replace(']','');     
       var res = results.split("@");  
       if(res[1].trim()==''){ 
	         var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
	         var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	         if(res[4].trim()!=''){
	       	  	alert("Cannot Invoice, as Bill to Code is missing in line # "+res[4]); 
	         }else{
			   alert("Cannot Invoice, as Bill to Code is missing");
	   		 } 		 
	      }
	      else if(res[2].trim()==''){
				if(res[4].trim()!=''){
	             	  alert("Cannot Invoice, as Charge Code is missing in line # "+res[4]); 
	             }else{
			 		  alert("Cannot Invoice, as Charge Code is missing");
	             }
			 }
		     <c:if test="${accountInterface=='Y'}">
				else if(res[3].trim()==''){
					if(res[4].trim()!=''){
		            	  alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[4]); 
		            }else{
						  alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
		       	    }
			   }
			</c:if>																													
		    else{ 
	        findBillToAuthority();
	           //var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
	           //var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
	        	//var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
	       	//var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	           //document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
	           //if(validateChargeVatExclude())
	           //openWindow('activeWorkTickets.html?buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,300);
	      }
    }
}
        
 function findBillToAuthority(){ 
     var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
     var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
     var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
     var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
     var url="findBillToAuthority.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
     //var url="findBillToAuthority.html?decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&ship=${serviceOrder.shipNumber}";
     http666.open("GET", url, true);
     http666.onreadystatechange = handleHttpResponseBillToAuthority;
     http666.send(null);
}

function handleHttpResponseBillToAuthority() { 
	if (http666.readyState == 4)
    {
       var results = http666.responseText 
       results = results.trim(); 
       results = results.replace('[','');
       results=results.replace(']','');  
      
       if(results==0) { 
           document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
           if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){
            alert("Billing has been marked as complete for this service order.")
            }else{
             var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
            if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
            findBillToAuthState();
            }else{
            validateChargeVatExclude(); 
           }
           }
       } else if(results!=0){
       var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
       var  billToAuthority=document.forms['serviceForm1'].elements['billing.billToAuthority'].value
       if(billToAuthority==''){
       alert("The following account "+billToCodeForTicket+" does not have authorization #, So Invoicing cannot be done.")
       } else if(billToAuthority!=''){ 
           if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){
            alert("Billing has been marked as complete for this service order.")
            }else{
            var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
            if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
            findBillToAuthState();
            }else{
           	 validateChargeVatExclude(); 
           }
           }
       }
       }
    }
        }   
  function findBillToAuthState(){ 
     var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
     var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
     var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
     var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
     var url="findBillToAuthState.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
     http999.open("GET", url, true);
     http999.onreadystatechange = handleHttpResponseBillToAuthState;
     http999.send(null);
}

function handleHttpResponseBillToAuthState() { 
	 if (http999.readyState == 4)
     {
        var results = http999.responseText 
        results = results.trim(); 
        results = results.replace('[','');
        results=results.replace(']','');   
        if(results!='STATE'&& results!='state') { 
        	 validateChargeVatExclude(); 
         } else if(results=='STATE'|| results=='state'){
            alert("WARNING: Confirm that access was not billed on export authorization.")
            validateChargeVatExclude(); 
            }
        }
} 
                
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
 	var http555 = getHTTPObject();
    var http666 = getHTTPObject666();
    var  http999= getHTTPObject999();
    var httpTemplate = getHTTPObject();
    
 function getHTTPObject999()
    {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
   }
   
  function getHTTPObject666()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
 function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();  
    var httpCMMAgent = getHTTPObject1();  
    function getHTTPObject2()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();   
  
 function getHTTPObject3()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject3();    
  
   function getHTTPObject5()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http6 = getHTTPObject5(); 
  var http20 = getHTTPObject21(); 
  
     function getHTTPObject21()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
function getHTTPObject6()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http7 = getHTTPObject6();  
   function getHTTPObject7()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http222 = getHTTPObject77();
    var http333 = getHTTPObject77();
    function getHTTPObject77()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    var http8 = getHTTPObject77();
    var httpVatExclude = getHTTPObject77();  
     var http88= getHTTPObject77();  
     function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http33 = getHTTPObject33(); 
      
</script>
<script type="text/javascript">
function findGenerateInvoice(){
    var billToCode = document.forms['serviceForm1'].elements['sid'].value;
     var url="actualInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(billToCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse14;
     http3.send(null);
}

function handleHttpResponse14() { 
	 if (http3.readyState == 4)
     {
        var results = http3.responseText 
        results = results.trim();  
        
        if(results.length>2){
        <c:choose>
      	<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
      	findCMMAgentBilltoCode()
      	</c:when>
  		<c:otherwise>    
          var res = results.split("@"); 
          if(res.length == 2){  
            document.forms['serviceForm1'].elements['billToCodeForTicket'].value=res[1];
            findBookingAgentName(res[1]); 
          } else {
        	  alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.")
          }
          </c:otherwise>
        	</c:choose> 
			} else  { 
           alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
         }
     }
        }   
        
function findCMMAgentBilltoCode(){
    var sid = document.forms['serviceForm1'].elements['sid'].value;
    var url="findCMMAgentBilltoCode.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
    httpCMMAgent.open("GET", url, true);
    httpCMMAgent.onreadystatechange = CMMAgentBilltoCodeResponse;
    httpCMMAgent.send(null);
}      
function CMMAgentBilltoCodeResponse(){
 if (httpCMMAgent.readyState == 4)
 {
    var results = httpCMMAgent.responseText 
    results = results.trim(); 
    var res = results.split("#"); 
    if(res[0]!='0'){
   	 if(res[0] == '1'){
				if(res[1] == '1'){
	  			document.forms['serviceForm1'].elements['billToCodeForTicket'].value=res[2];
                   findBookingAgentName(res[2]); 
	  		}else{
	  			alert('Cannot generate invoice as Multiple Currency Found.');
	  		}	
			}else{
  			alert('Cannot generate invoice as Multiple Bill to Found.');
			} 
    }else  { 
        alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
    }
 }
}                     
function findCalcDistribution(){
var list = document.forms['serviceForm1'].elements['emptyList'].value;
	if(list=="true"){
		alert('Nothing found to calculate distribution.'); 
		return false;
	}
    var sid = document.forms['serviceForm1'].elements['sid'].value;
    var url="calcDistribution.html?ajax=1&sid=" + encodeURI(sid);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponse444;
    http5.send(null);
}   

function handleHttpResponse444(){
     if (http5.readyState == 4){
         var results = http5.responseText 
         results = results.trim();  
         window.location.reload();                    
    }
}   
              
</script>
<script type="text/javascript">
	function onLoad(){ 
      onEntitledLoad(); 
      onExpenseLoad();
      onRevisionLoad();
      ActualExp();
     document.forms['serviceForm1'].elements['buttonType'].value = "calc";
     return false;
}

function setType(){
document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
}

function ActualExp() {
    var sumActualExpense=0;
	var sumActualRevenue=0;
	var diff=0;
	<c:forEach var="accountLine" items="${accountLines}"> 
	  sumActualExpense+=${accountLine.actualExpense};
	  sumActualRevenue+=${accountLine.actualRevenue}; 
	</c:forEach > 
	document.forms['serviceForm1'].elements['actualExpenseTotal'].value= roundNumber(sumActualExpense*1);
	document.forms['serviceForm1'].elements['actualRevenueTotal'].value= roundNumber(sumActualRevenue*1);
	diff=(sumActualRevenue-sumActualExpense)*1; 
	document.forms['serviceForm1'].elements['actualGrossMar'].value= roundNumber(diff);
	if(diff==""||sumActualRevenue=="")
	{
	  document.forms['serviceForm1'].elements['actualGrossMarginPer'].value=0*1;
	} else {
	   document.forms['serviceForm1'].elements['actualGrossMarginPer'].value=roundNumber(((sumActualRevenue-sumActualExpense)*100)/sumActualRevenue);
	} 
}

function onEntitledLoad() {
    var sumRevenue=0;
   <c:forEach var="accountLine" items="${accountLines}">
     sumRevenue += ${accountLine.entitlementAmount}; 
   </c:forEach >
   document.forms['serviceForm1'].elements['entitledTotal'].value = roundNumber(sumRevenue); 
 }

function onExpenseLoad(){ 
	var sumRevenue=0;
	var sumExp=0;
	var diff=0; 
    <c:forEach var="accountLine" items="${accountLines}">
      sumRevenue += ${accountLine.estimateRevenueAmount};
	  sumExp += ${accountLine.estimateExpense};
    </c:forEach > 
	document.forms['serviceForm1'].elements['estimateRevenueAmt'].value = roundNumber(sumRevenue*1);
	document.forms['serviceForm1'].elements['estimateExp'].value = roundNumber(sumExp*1);
	diff = (sumRevenue - sumExp)*1;
	 document.forms['serviceForm1'].elements['grossMrgn'].value=diff;
	if(diff==""||sumRevenue=="")
	{
	  document.forms['serviceForm1'].elements['gmPercent'].value=0*1;
	} else {
	  document.forms['serviceForm1'].elements['gmPercent'].value = roundNumber(((sumRevenue - sumExp)*100)/sumRevenue)*1;
	} 
}

function onRevisionLoad(){ 
	var sumRevenue=0;
	var sumExp=0;
	var diff=0; 
    <c:forEach var="accountLine" items="${accountLines}">
      sumRevenue += ${accountLine.revisionRevenueAmount};
	  sumExp += ${accountLine.revisionExpense};
    </c:forEach > 
	document.forms['serviceForm1'].elements['revisedTotalRev'].value = roundNumber(sumRevenue*1);
	document.forms['serviceForm1'].elements['revisedTotal'].value = roundNumber(sumExp*1);
	diff = (sumRevenue - sumExp)*1; 
	document.forms['serviceForm1'].elements['revisedGrossMar'].value=roundNumber(diff); 
	if(diff==""||sumRevenue=="")
	{
	  document.forms['serviceForm1'].elements['revisedGrossMarginPer'].value=0*1;
	} else {  
	  document.forms['serviceForm1'].elements['revisedGrossMarginPer'].value = roundNumber(((sumRevenue - sumExp)*100)/sumRevenue);
	 } 
}

function roundNumber(numberToRound) { 
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	var rnum = numberToRound;// numberField.value;
	var rlength = 2; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	} 
	return newnumber;
}

function roundNumberForPercent(numberToRound) { 
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	var rnum = numberToRound;// numberField.value;
	var rlength = 1; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	} 
	return newnumber;
}
function showMessage() {
<c:if test="${billingFlag ==0 }">
	alert("Billing Information has not been created.");
</c:if>
<c:if test="${billingContractFlag=='N'}">
alert("There is no pricing contract in billing: Please select.");
</c:if>
}
function showJobMessage() {
alert("You cannot access Accounting as the Job type is blank, please select job type in the service order detail page.");
}
function openInactiveList() {
	var accountLineStatusInactive = document.forms['serviceForm1'].elements['accountLineStatus'].value='false';
	document.forms['serviceForm1'].action = 'accountLineList.html';
	
	<c:if test="${vanLineAccountView =='category'}">
	 document.forms['serviceForm1'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
	 document.forms['serviceForm1'].action = 'accountCategoryViewList.html';
	</c:if>
	
    document.forms['serviceForm1'].submit(); 
}
function openEstPricing(){
	location.href = "estimatePricingRating.html?sid=${serviceOrder.id}&vanLineAccountView=${vanLineAccountView}";
}
function openActiveList() {
	document.forms['serviceForm1'].elements['vanLineAccountView'].value = '';
	var accountLineStatusInactive = document.forms['serviceForm1'].elements['accountLineStatus'].value='true';
	document.forms['serviceForm1'].action = 'accountLineList.html';
<%--	
	<c:if test="${vanLineAccountView =='category'}">
	 document.forms['serviceForm1'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
	 document.forms['serviceForm1'].action = 'accountCategoryViewList.html';
	</c:if>
--%>	
    document.forms['serviceForm1'].submit(); 
}
function openAllList() {
	var accountLineStatusInactive = document.forms['serviceForm1'].elements['accountLineStatus'].value='allStatus';
	 document.forms['serviceForm1'].action = 'accountLineList.html';
	 
	 <c:if test="${vanLineAccountView =='category'}">
	 document.forms['serviceForm1'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
	 document.forms['serviceForm1'].action = 'accountCategoryViewList.html';
	</c:if>
	
    document.forms['serviceForm1'].submit(); 
}
function findDefaultLine(){
	if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){ 
        var agree = confirm("The billing has been completed, do you still want to add lines?");
         if(agree)
           {
        	    var jobtypeSO=document.forms['serviceForm1'].elements['jobtypeSO'].value;
        		var routingSO=document.forms['serviceForm1'].elements['routingSO'].value;
        		var modeSO=document.forms['serviceForm1'].elements['modeSO'].value;
        		var billingContract=document.forms['serviceForm1'].elements['billingContract'].value;
        		if(billingContract=='')
        		 { 
        		 		alert("Please select Pricing Contract from billing"); 
        		 } else {
        			document.forms['serviceForm1'].elements['vanLineAccountView'].value = '${vanLineAccountView}';
        		  document.forms['serviceForm1'].action = 'findAccountDefaultLine.html?btntype=yes';
        		  showOrHide(1);
        	      document.forms['serviceForm1'].submit();
        		} 
           }else{}
	}else{
	var jobtypeSO=document.forms['serviceForm1'].elements['jobtypeSO'].value;
	var routingSO=document.forms['serviceForm1'].elements['routingSO'].value;
	var modeSO=document.forms['serviceForm1'].elements['modeSO'].value;
	var billingContract=document.forms['serviceForm1'].elements['billingContract'].value;
	if(billingContract=='')
	 { 
	 		alert("Please select Pricing Contract from billing"); 
	 } else {
		document.forms['serviceForm1'].elements['vanLineAccountView'].value = '${vanLineAccountView}';
	  document.forms['serviceForm1'].action = 'findAccountDefaultLine.html?btntype=yes';
	  showOrHide(1);
      document.forms['serviceForm1'].submit();
	} 
	}
}
function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 
function calculateHVY() { 
document.forms['serviceForm1'].action = 'HVYCalculate.html?btntype=yes';
document.forms['serviceForm1'].submit();
}
function calculateMGMTFees() { 
showOrHideAutoSave('1');	
document.forms['serviceForm1'].action = 'MGMTFeesCalculate.html?btntype=yes';
document.forms['serviceForm1'].submit();
}
function calculateDiscountFees(){
showOrHideAutoSave('1');
document.forms['serviceForm1'].action = 'discounteesCalculate.html?btntype=yes';
document.forms['serviceForm1'].submit();
}
function calCommission(isPayableRequiredFlag){
	if(isPayableRequiredFlag){
		document.forms['serviceForm1'].action = 'calculateCommisssion.html?btntype=yes&isPayableRequired=YES';
	}else{
		document.forms['serviceForm1'].action = 'calculateCommisssion.html?btntype=yes';
	}
	document.forms['serviceForm1'].submit();
}
function  massageHVY(){
alert("No Lines to Calculate.");
}
function defaultLineMassage() {
alert("Default accounting template has already been added."); 
}
</script> 
<script type="text/javascript">
function findPostDateLine(){
    var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="postDateList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse114;
     http2.send(null);
}

function handleHttpResponse114() { 
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim(); 
                if(results.length>2)
                { 
                 var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
                	var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value; 
                    openWindow('postAccountLineList.html?buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&decorator=popup&popup=true',900,300);
 				} else {
                   alert("There are no invoice with blank posting date"); 
                 }
             }
        }
 </script>
<script type="text/javascript">
  function findPayPostDate()  {
  var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payPostDateList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse115;
     http2.send(null); 
  }
  function handleHttpResponse115() { 
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>5)
                { 
                 findPayableInvoice(); 
 				} else {
                   alert("No Payable lines found for Posting"); 
                   progressBarAutoSave('0');
                 }
             }
        }
    function findPayableInvoice() { 
     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payableInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse126;
     http3.send(null); 
  }
  
  function handleHttpResponse126() { 
             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>5)
                { 
                 alert("There are payables with missing vendor invoice#");
 				} else {
                   findPayableVendor(); 
                 }
             }
        } 
        
      function findPayableVendor() { 
     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payableVendorList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse136;
     http4.send(null); 
  }
  
  function handleHttpResponse136() { 
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');   
                if(results.length>2)
                { 
                 alert("The vendor code is missing");
 				} else {
                 findPayableCharge();
                 }
             }
        }
        function findbillingCurrency(){
        var sid = document.forms['serviceForm1'].elements['sid'].value;
        var url="findbillingCurrency.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
        http33.open("GET", url, true);
        http33.onreadystatechange = handleHttpResponseCurrency;
        http33.send(null);
       }
        
        function handleHttpResponseCurrency(){ 
         if (http33.readyState == 4)
             {
                var results = http33.responseText 
                results = results.trim();  
                if(results.length>2)
                { 
                  var res = results.split("@");
                  if(res.length == 2)
                  { 
                     <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
                      findGenerateInvoice();
                     </c:if>
                    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
                      findDistAmtGenerateInvoice(); 
                    </c:if> 
                  } else {
                	  <c:choose>
                	  <c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
                	  <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
                         findGenerateInvoice();
                      </c:if>
                      <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
                         findDistAmtGenerateInvoice(); 
                      </c:if> 
                	 </c:when>
	              	<c:otherwise>
                  	 alert("Invoice cannot be generated as the Billing currency of all the account line(s) do not match.")
                  	</c:otherwise>
	                </c:choose>
                  }  
 				} else  { 
                   alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
                 }
             }
        }  
        
         
         function  findAccountCompanyDivision() {
           var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
           var url="findAccountCompanyDivision.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId)+"&billingCMMContractType=${billingCMMContractType}"; 
           http88.open("GET", url, true);
           http88.onreadystatechange = handleHttpResponseAccountCompanyDivision;
           http88.send(null);
         }
       function handleHttpResponseAccountCompanyDivision(){
        if (http88.readyState == 4)
             {
                var results = http88.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');   
                if(results.length>2)
                {
                	var checkCloseCompanyDivision=false;
                	var res = results.split("@"); 
                	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
                	  var closeDivision="";
                      
                      for (i=0;i<res.length;i++){ 
                	  if(!checkCloseCompanyDivision){
                	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
                		  checkCloseCompanyDivision=true;
                		  closeDivision=res[i];
                	  }
                      }
                      }
                    </configByCorp:fieldVisibility>
                  if(checkCloseCompanyDivision){
                	  alert("Cannot generate invoice to accounting as the company division "+closeDivision+" has been closed ");
                	  progressBarAutoSave('0');
                  }else{	
                  var res = results.split("@");  
                  if(res.length == 2)
                  {
                  <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
		           var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1489;
		           http8.send(null); 
		          </c:if>
		          <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
		           var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1499;
		           http8.send(null); 
		          </c:if>
                  }
                  else{
                  var agree = confirm("The new invoice has different company divisions, do you want to still proceed and put these in the same invoice #?");
                      if(agree)
                      {
                          <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
			                  var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
			                  var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
			                  http8.open("GET", url, true);
			                  http8.onreadystatechange = handleHttpResponse1489;
			                  http8.send(null); 
		                  </c:if>
		                  <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
					           var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
					           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
					           http8.open("GET", url, true);
					           http8.onreadystatechange = handleHttpResponse1499;
					           http8.send(null); 
				          </c:if>
                      }else { 
                        }
                  
                  }
                  }
                  } else{
                   alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
                  }
       }
       }
       
 function validateChargeVatExclude(){
    	   
    	   <c:if test="${systemDefaultVatCalculation == 'true'}">
    	     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
             var url="validateChargeVatExclude.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
             httpVatExclude.open("GET", url, true);
             httpVatExclude.onreadystatechange = handleHttpResponseVatExclude;
             httpVatExclude.send(null);
       	  </c:if>
       	<c:if test="${systemDefaultVatCalculation != 'true'}">
       	   var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
           var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
     	   var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
    	   var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
           document.forms['serviceForm1'].elements['buttonType'].value = "invoice"; 
           openWindow('activeWorkTickets.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
        </c:if>
       }
        
       function  handleHttpResponseVatExclude(){
    	   if (httpVatExclude.readyState == 4)
           {
               
              var results = httpVatExclude.responseText 
              results = results.trim();
              results = results.replace('[','');
              results=results.replace(']',''); 
              if(results!='')
              {
            	  alert('Please enter Receivable VAT for the Account line No. '+ results);
            	  progressBarAutoSave('0');
					 
              }else{
            	  var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value
                  var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
            	  var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
           	      var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
                  document.forms['serviceForm1'].elements['buttonType'].value = "invoice"; 
                  openWindow('activeWorkTickets.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
              }
       }
       }
        
        function  findBookingAgent(temp) {
        
        	
	        if(temp=='postingDate'){
		          <c:if test="${accountInterface=='Y'}">
		           <configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
		      	    <c:set var="closedCompanyDivision" value="Y" />
		          </configByCorp:fieldVisibility>
		            <c:if test="${closedCompanyDivision=='Y'}">
		              findAccountCompanyDivisionForPayPost();
		          </c:if> 
	              <c:if test="${closedCompanyDivision!='Y'}">	
		           var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
		          </c:if>
		          </c:if>
		          <c:if test="${accountInterface=='N'||accountInterface==''}">
		            findPayableStatus();
		          </c:if>
		    }
	        if(temp=='invoiceGen'){
		        var CWMSCommission=true; 
	        	<configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
		        CWMSCommission=false;
		        <c:if test="${commissionJobName == 'Yes'}">
	        	 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
	        		<c:if test="${empty compDivForBookingAgentList}"> 
	        		findAccountCompanyDivision();
	        		</c:if>
	        		<c:if test="${not empty compDivForBookingAgentList}">
	        		if('${fn:length(serviceOrder.salesMan)}' == 0){
		           		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
		           		return false;
		           	}else{
		           		findAccountCompanyDivision(); 
			           	}
	        		</c:if>
	        	 </c:if>
	        	 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
	       		if('${fn:length(serviceOrder.salesMan)}' == 0){
	           		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
	           		return false;
	           	}else{
	           		findAccountCompanyDivision(); 
		           	}
	           	</c:if>
	           	</c:if>
	           	<c:if test="${commissionJobName != 'Yes'}">
	           	findAccountCompanyDivision(); 
	           	</c:if>
      		</configByCorp:fieldVisibility>
      		 if(CWMSCommission){ 
		        findAccountCompanyDivision(); 
      		 }
		    }
        }
        function  findAccountCompanyDivisionForPayPost() {
      	 progressBarAutoSave('1');
          var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
         var url="findAccountCompanyDivision.html?ajax=1&companyDivisionPayPostFlag=Y&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
         http88.open("GET", url, true);
         http88.onreadystatechange = handleHttpResponseAccountCompanyDivisionForPayPost;
         http88.send(null);
       }
     function handleHttpResponseAccountCompanyDivisionForPayPost(){
      if (http88.readyState == 4)
           {
              var results = http88.responseText
              results = results.trim();
              results = results.replace('[','');
              results=results.replace(']','');   
              
              if(results.length>2)
              { 
              	var checkCloseCompanyDivision=false;
              	var closeDivision="";
                var res = results.split("@"); 
                for (i=0;i<res.length;i++){ 
              	  if(!checkCloseCompanyDivision){
              	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
              		  checkCloseCompanyDivision=true;
              		  closeDivision=res[i];
              	  }
                }
                }
                if(checkCloseCompanyDivision){
              	  alert("Payable cannot be posted to accounting as the company division "+closeDivision+" has been closed ");
              	  progressBarAutoSave('0');
                }else{
                   var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }
                } else{
                	var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }
     }
     }
        function handleHttpResponse1488() {
             var bookingagentCode = document.forms['serviceForm1'].elements['serviceOrder.bookingAgentCode'].value;
             if (http8.readyState == 4)
             {
                var results = http8.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results=='0')
                {
                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
                } else if(results!='0') {
                findPayableStatus();
                }
             }
        }
        function handleHttpResponse1489() {
             var bookingagentCode = document.forms['serviceForm1'].elements['serviceOrder.bookingAgentCode'].value;
             if (http8.readyState == 4)
             {
                var results = http8.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='0')
                {
                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
                }
                else if(results!='0')
                {
                 <c:if test="${multiCurrency!='Y'}"> 
                   findGenerateInvoice();
                 </c:if> 
                 <c:if test="${multiCurrency=='Y'}"> 
                   findbillingCurrency();
                 </c:if>
                }
             }
        }
        function handleHttpResponse1499() {
             var bookingagentCode = document.forms['serviceForm1'].elements['serviceOrder.bookingAgentCode'].value;
             if (http8.readyState == 4)
             {
                var results = http8.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='0')
                {
                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
                }
                else if(results!='0')
                {
                <c:if test="${multiCurrency!='Y'}"> 
                  findDistAmtGenerateInvoice();
                </c:if> 
                <c:if test="${multiCurrency=='Y'}"> 
                  findbillingCurrency();
                 </c:if> 
                }
             }
        }
         
     function  findPayableStatus()
     { 
     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payableStatusList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
     http7.open("GET", url, true);
     http7.onreadystatechange = handleHttpResponse148;
     http7.send(null); 
     
     }
     function handleHttpResponse148()
        {
             if (http7.readyState == 4)
             {
                var results = http7.responseText
                results = results.trim();
                //alert(results);
                var res = results.split("#");  
                if(res[0]=='No')
                { 
                	if(res[1]=='0')
                	{
                		findPayPostDate();
                	}
                	else if(res[2]=='0')
                	{
                	    findPayPostDate();
                	}
                	else
                	{
                 		alert("The status is not approved ");
                 	}
 				}
                 else if(results=='Yes')
                 { 
                 	findPayPostDate();
                 }
             }
        } 
   	function findPayableCharge() 
    { 
     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payableChargeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse146;
     http5.send(null);
  
  }  
  
  function handleHttpResponse146()
        {

             if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                //alert(results)
                if(results.length>2)
                { 
                 alert("The Charge code is missing");
 				}
                 else
                 { 
                 	findPayableGl();  
                 }
             }
        } 
    function findPayableGl() 
    { 
     var serviceOrderId = document.forms['serviceForm1'].elements['sid'].value;
     var url="payableGLCodeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
     http6.open("GET", url, true);
     http6.onreadystatechange = handleHttpResponse147;
     http6.send(null);
  
  }   
    function handleHttpResponse147()
        {

             if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');   
                if(results.length>2)
                { 
                 alert("The GL code is missing in the 'Payable Detail' of 'Accounting Transfer Information' section");
 				}
                 else
                 { 
                 	var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
                	var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
                    document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
                    if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){
                     var agree = confirm("The billing has been completed, do you want to still post accountlines?");
                      if(agree){
                             openWindow('findPayPostDate.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
                      }
                     }else{
                     openWindow('findPayPostDate.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
                     }
                    
                 }
             }
        }
  function checkBillingComplete() {
	  document.forms['serviceForm1'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
	  
      var sid = document.forms['serviceForm1'].elements['sid'].value;
      var getCheckLHF=document.forms['serviceForm1'].elements['checkLHF'].value;
	  var divisionFlag1='';
	  try{
	  divisionFlag1=document.forms['serviceForm1'].elements['divisionFlag'].value;
	  }catch(e){}      
      if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){ 
          var agree = confirm("The billing has been completed, do you still want to add lines?");
           if(agree){
                  window.location="editAccountLine.html?vanLineAccountView=${vanLineAccountView}&sid="+sid+"&checkLHF="+getCheckLHF+"&divisionFlag="+divisionFlag1;
             }
      }else{
          window.location="editAccountLine.html?sid="+sid+"&checkLHF="+getCheckLHF+"&divisionFlag="+divisionFlag1+"&vanLineAccountView=${vanLineAccountView}";           
      }
  } 
  function editNewAccLine(){
	  var divisionFlag1='';
	  try{
	  divisionFlag1=document.forms['serviceForm1'].elements['divisionFlag'].value;
	  }catch(e){}	  
		  var sid='${serviceOrder.id}';
		  window.location="editAccountLine.html?sid="+sid+"&divisionFlag="+divisionFlag1;  
  }
function countInvoice()
{
   var sid = document.forms['serviceForm1'].elements['sid'].value;
   var url="countInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid); 
     http20.open("GET", url, true);
     http20.onreadystatechange = handleHttpResponse21;
     http20.send(null);
   
}  

function handleHttpResponse21()
{
 		if(http20.readyState == 4)
		{
               var results = http20.responseText
               results = results.trim(); 
               if(parseInt(results)>0)
               {
	               synchBilling();
               }
               else
               {
				alert("No Records found.");
                return false;		               
               }
         }
}
  
function synchBilling() {
   var sid = document.forms['serviceForm1'].elements['sid'].value;
   var billToCode = document.forms['serviceForm1'].elements['billing.billToCode'].value;
      if(sid!=''){
          var agree = confirm("Do you want to convert all active lines billing code to "+billToCode+"?  Press OK to continue.");
          if(agree){
                  window.location="updateAcc.html?vanLineAccountView=${vanLineAccountView}&btntype=yes&sid="+sid;          
          }
  	 }
 }     
 </script>
 <script type="text/javascript">
 function findReverseInvoice() { 
  var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value; 
  openWindow('reverseInvoiceList.html?buttonType=invoice&sid='+sid+'&decorator=popup&popup=true',680,300);
  }
 function findCopyInvoice() { 
	 if(document.forms['serviceForm1'].elements['billing.billComplete'].value!=''){
         alert("Billing has been marked as complete so cannot copy.")
     }else{
	  var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value; 
	  openWindow('copyInvoiceList.html?buttonType=invoice&sid='+sid+'&decorator=popup&popup=true',680,300);
      }
	  }
  function findInvoiceTotal(position) {
  var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
  var divisionFlag="NO"; 
  try{
	  divisionFlag=document.forms['serviceForm1'].elements['divisionFlag'].value;
  }catch(e){} 
  var url="findInvoiceTotal.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&sid=" + encodeURI(sid)+"&divisionFlag="+divisionFlag;
  ajax_showTooltip(url,position);	
  }
  
  function findVendorInvoiceTotal(position) {
  var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value; 
  var url="findVendorInvoiceTotal.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&sid=" + encodeURI(sid);
  ajax_showTooltip(url,position);	
  }
  </script>
  <script type="text/javascript"> 
  
  function findDistAmtGenerateInvoice(){ 
    var sid = document.forms['serviceForm1'].elements['sid'].value;
     var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse1114;
     http3.send(null);
}

  function handleHttpResponse1114()
  { 
   if (http3.readyState == 4)
       {
          var results = http3.responseText 
          results = results.trim();   
          if(results.length>2) {
          	<c:choose>
            	<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
            	findCMMAgentBilltoCode()
            	</c:when>
        		<c:otherwise>  
            var res = results.split("@");
            if(res.length == 2)  {  
              document.forms['serviceForm1'].elements['billToCodeForTicket'].value=res[1]; 
              findDistAmtBookAgent(res[1]); 
            }  else  {
          	  alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.")
            }
            </c:otherwise>
        	  </c:choose>  
			}  else  { 
             alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
           }
       }
  }   
  
  function findDistAmtBookAgent(target){  
     var billToCode=target; 
     var sid = document.forms['serviceForm1'].elements['sid'].value;
     var url="distAmtAccountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4444;
     http2.send(null);
}
function handleHttpResponse4444()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                var res = results.split("@");  
                if(res[1].trim()=='')  { 
                  var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
                  var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
                  if(res[6].trim()!=''){
                	  alert("Cannot Invoice, as Bill to Code is missing in line # "+res[6]);  
                      }else{
 				   alert("Cannot Invoice, as Bill to Code is missing"); 	
                      }	 
 				} else if(res[2].trim()=='')
 				{
 					if(res[6].trim()!=''){
 	                	  alert("Cannot Invoice, as Charge Code is missing in line # "+res[6]);  
 	                      }else{
 				         alert("Cannot Invoice, as Charge Code is missing");
 	                      }
 				}
                <c:if test="${accountInterface=='Y'}">
 				else if(res[3].trim()=='')
 				{
 					if(res[6].trim()!=''){
	                	  alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[6]);  
	                      }else{
 				 alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
	                      }
  				} 
 				</c:if>
 				else if(res[4].trim()!=res[5].trim())
 				{
 				   alert("Actual revenue total ("+ res[4]+") and distribution amount total ("+res[5]+") are not same. So, cannot generate invoice.")
 				}
                else
                 {
                    findBillToAuthority();
                    //var  jobtypeForTicket=document.forms['serviceForm1'].elements['serviceOrder.Job'].value 
                    //var  billToCodeForTicket=document.forms['serviceForm1'].elements['billToCodeForTicket'].value
                 	//var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
                	//var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
                    //document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
                    //if(validateChargeVatExclude())
                    //openWindow('activeWorkTickets.html?buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,300);
                 }
             }
        }
  </script>
  
 <script type="text/javascript">
    function openSendtoDates(){ 
     var sid = document.forms['serviceForm1'].elements['sid'].value; 
     openWindow('resetSendtoDates.html?sid='+sid+'&decorator=popup&popup=true','','300','250');
    }
    
 function openBillingWizard(){
 var sid = document.forms['serviceForm1'].elements['sid'].value;
 window.location ="openBillingWizard.html?sid="+sid;
 }
 
 function findInvoiceTODelete() {
 var sid = document.forms['serviceForm1'].elements['sid'].value;
 openWindow('findInvoiceToDelete.html?ajax=1&sid='+sid+'&decorator=popup&popup=true','','300','250');
 }

 if (window.addEventListener)
	 window.addEventListener("load", function(){
		 var elementValue = document.getElementById('activateAccPortalID').value;
		 var accPortStatus = document.getElementById('accPortalStatus');
		 if((accPortStatus != null && accPortStatus != undefined) && (elementValue == '' || elementValue == undefined || elementValue == null) ){
				document.getElementById('accPortalStatus').disabled = true;
			}
			else if(accPortStatus != null && accPortStatus != undefined){
				document.getElementById('accPortalStatus').disabled = false;
			}
	 }, false)
 
 /* Added By Kunal */
 function activateAccPortalCheck(rowId,element) 
 { 
	 var val = rowId+"#"+element.checked;
	 document.getElementById('activateAccPortalID').value += val + ",";
	 var elementValue = document.getElementById('activateAccPortalID').value;
	 if((accPortStatus != null && accPortStatus != undefined) && (elementValue == '' || elementValue == undefined || elementValue == null)){
			document.getElementById('accPortalStatus').disabled = true;
		}
	 else if(accPortStatus != null && accPortStatus != undefined){
			document.getElementById('accPortalStatus').disabled = false;
		}
 }

 function activateAccPortalSetter(){
	var elementValue = document.getElementById('activateAccPortalID').value;
    document.forms['serviceForm1'].action ='activateAccPortal.html?ajax=1&btntype=yes';
    document.forms['serviceForm1'].submit(); 
 }


 function selectiveInvoiceCheck(rowId,targetElement){
	 progressBarAutoSave('1');
     var selectiveInvoice=true;
	 if(targetElement.checked==false)
     {
		 selectiveInvoice=false;
     }
	 var sid = document.forms['serviceForm1'].elements['sid'].value;
     var url="updateSelectiveInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&id="+rowId+"&selectiveInvoice="+selectiveInvoice;
     httpCMMAgent.open("GET", url, true);
     httpCMMAgent.onreadystatechange = selectiveInvoiceCheckResponse;
     httpCMMAgent.send(null);
 }
 function selectiveInvoiceCheckResponse(){
	  if (httpCMMAgent.readyState == 4)
     {
        var results = httpCMMAgent.responseText  
     }
	  progressBarAutoSave('0');
 }       
	    
 /* End */
 function inactiveStatusCheck(rowId,targetElement) 
   { 
   if(targetElement.checked==false)
     {
      var userCheckStatus = document.forms['serviceForm1'].elements['accountIdCheck'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['serviceForm1'].elements['accountIdCheck'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['serviceForm1'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
      document.forms['serviceForm1'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
      }
    }
   if(targetElement.checked)
    {
     var userCheckStatus = document.forms['serviceForm1'].elements['accountIdCheck'].value;
     var userCheckStatus=document.forms['serviceForm1'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['serviceForm1'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
     } 
     accountIdCheck();
    }
     function accountIdCheck(){
    var accountIdCheck = document.forms['serviceForm1'].elements['accountIdCheck'].value;
    accountIdCheck=accountIdCheck.trim(); 
    if(accountIdCheck=='')
    {
    document.forms['serviceForm1'].elements['Inactivate'].disabled=true;
    }
    else if(accountIdCheck==',')
    {
    document.forms['serviceForm1'].elements['Inactivate'].disabled=true;
    }
    else if(accountIdCheck!='')
    {
      document.forms['serviceForm1'].elements['Inactivate'].disabled=false;
    }
    }
    
    function updateAccInactive() {
            var sid = document.forms['serviceForm1'].elements['sid'].value;
            document.forms['serviceForm1'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
            document.forms['serviceForm1'].action ='updateAccInactive.html?btntype=yes';
            document.forms['serviceForm1'].submit(); 
    }
    	
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http555.open("GET", url, true); 
     http555.onreadystatechange = handleHttpResponseOtherShip; 
     http555.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http555.open("GET", url, true); 
     http555.onreadystatechange = handleHttpResponseOtherShip; 
     http555.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http555.readyState == 4)
             {
               var results = http555.responseText
               results = results.trim();
               location.href = 'accountCategoryViewList.html?vanLineAccountView=${vanLineAccountView}&sid='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceForm1'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrl(id)
	{
		location.href = "accountCategoryViewList.html?vanLineAccountView=${vanLineAccountView}&sid="+id;
	}
	

function findUserPermission(name,position) { 
  var url="reportBySubModuleAccountLineList.html?decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting";
  ajax_showTooltip(url,position);	
  }
  
  function findUserPermission1(name,position) { 
  var url="reportBySubModuleAccountLineList1.html?ajax=1&decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode =${billing.billToCode}&reportModule=serviceOrder&reportSubModule=Accounting";
  ajax_showTooltip(url,position);	
  }
  function findToolTipForExpense(id,name,position){
	  var url="findToolTipForExpense.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&fieldNames="+name;
	  ajax_showTooltip(url,position);
  }

 function findToolTipForRevenue(id,name,position,statusDate){	
	var multiCurr='${multiCurrency}';
	if(!(multiCurr !='Y' && (statusDate==null||statusDate==''))){
		  var url="findToolTipForRevenue.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&fieldNames="+name;
		  ajax_showTooltip(url,position);
	}
  }
 function findToolTipExpRevenue(id,name,position){
	 <c:if test="${multiCurrency =='Y'}"> 
	  var url="findToolTipExpRevenue.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&fieldNames="+name;
	  ajax_showTooltip(url,position);
	 </c:if>
  }
 function changeAccEstmateStatus(id,target){
	 var accEstimate = target.value
		 var url="updateAccountLineField.html?ajax=1&decorator=simple&popup=true&aid="+encodeURI(id)+"&accEstimate="+accEstimate;
		 	httpTemplate.open("GET", url, true);
			httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
			httpTemplate.send(null);
		 }
 function handleHttpResponseForTemplate(){
		if (httpTemplate.readyState == 4){
	        var results= httpTemplate.responseText;
	        alert('Status has been Updated.');
		}
	}

function goToEditAccountLinePage(sid,id){
var ssid=sid;
var iid=id;
var getCheckLHF=document.forms['serviceForm1'].elements['checkLHF'].value;
var idForCheckLHF=document.forms['serviceForm1'].elements['idForCheckLHF'].value;
if(idForCheckLHF==id){
window.location="editAccountLine.html?vanLineAccountView=${vanLineAccountView}&sid="+ssid+"&id="+iid+"&checkLHF=";
}
else{
window.location="editAccountLine.html?vanLineAccountView=${vanLineAccountView}&sid="+ssid+"&id="+iid+"&checkLHF="+getCheckLHF;
}
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
try{
  <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
  	<c:if test="${commissionJobName == 'Yes'}">
    	<c:if test="${emptyList!=true}" >
    	  document.forms['serviceForm1'].elements['buttonType'].value = getCookie('buttonType');
  				if(document.forms['serviceForm1'].elements['buttonType'].value== "invoice"){
					//alert("Invoice Generated, click 'Commission' button to compute relevant commission.");
	 				calCommission(true);
	  				document.forms['serviceForm1'].elements['buttonType'].value="";
 				 }
  		  document.cookie = 'buttonType' + "=; expires=" +  new Date().toGMTString();
  		</c:if>
  	</c:if>
  </configByCorp:fieldVisibility>
}catch(e){
}
try{
  accountIdCheck();
  var fieldName = document.forms['serviceForm1'].elements['field'].value; 
  var fieldName1 = document.forms['serviceForm1'].elements['field1'].value;
  if(fieldName!=''){
  document.forms['serviceForm1'].elements[fieldName].className = 'rules-textUpper';
  }
  if(fieldName1!=''){
  document.forms['serviceForm1'].elements[fieldName1].className = 'rules-textUpper';
  }
}catch(e){
}
</script>  

<script type="text/javascript">
function getAdvDtls(position){
	var url="showAdvances.html?ajax=1&sequenceNum=${serviceOrder.shipNumber}&driver=ACCOUNT&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}
</script>

<script type="text/javascript">
function setAnimatedColCatFocus(id,day){
	toggle_visibility(id);
}
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
    	if(e.style.display == 'block')
    	   e.style.display = 'none';
    	else
    	   e.style.display = 'block';
    }
}
<c:forEach var="outerMap" items="${vanLineAccountCategoryMap}" varStatus="rowCount">
toggle_visibility("cat_${outerMap.key}");
	<%-- <c:if test="${fn:length(vanLineAccountCategoryMap) != rowCount.count }">
		toggle_visibility("cat_${outerMap.key}");
	</c:if> --%>
</c:forEach>
</script>

 <script type="text/javascript"> 
 try{
	 if('${addTemplate}' == "fromTemplate"){
		alert('Default Template has been added.');
	 }
 }catch(e){
	 
 }
 function openViewList(view,url){
		document.forms['serviceForm1'].elements['vanLineAccountView'].value = view;
		document.forms['serviceForm1'].elements['accountLineStatus'].value='true';
		document.forms['serviceForm1'].action = url;
		document.forms['serviceForm1'].submit();
	 }
function collapseFun(action){
	<c:forEach var="outerMap" items="${vanLineAccountCategoryMap}" varStatus="rowCount">
		toggle_visibilityByOrder("cat_${outerMap.key}",action);
	</c:forEach>
}
function toggle_visibilityByOrder(id,action) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
   	   e.style.display = action;
    }
}
</script>

<script type="text/JavaScript">
window.onload = function() {
	
if(screen.width == 1024){
	  var elem = document.getElementById("KeyDiv");
	  elem.style.width = '100%';
	  elem.style.marginLeft = '38%';
	
 }
 else {
	  var elem = document.getElementById("KeyDiv");
	  elem.style.width = '100%';
	  elem.style.marginLeft = '30%';
}
}

function pageReload(){
	document.forms['serviceForm1'].elements['vanLineAccountView'].value = 'category';
	document.forms['serviceForm1'].action = 'accountCategoryViewList.html';
	document.forms['serviceForm1'].submit();	
}

</script>