package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ScreenConfigurationDao;
import com.trilasoft.app.model.ScreenConfiguration;
import com.trilasoft.app.model.SystemConfiguration;

public class ScreenConfigurationDaoHibernate extends GenericDaoHibernate<ScreenConfiguration, Long> implements ScreenConfigurationDao {

	
	public ScreenConfigurationDaoHibernate() {   
        super(ScreenConfiguration.class);   
    }   
 
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from ScreenConfiguration where id=?",id);
    }
    public List<ScreenConfiguration>searchByCorpIdTableAndField(String corpId,String tableName,String fieldName) 
    {
    	
    	return getHibernateTemplate().find("from ScreenConfiguration where corpId like '"+corpId+"%' AND tableName like '"+tableName+"%' AND fieldName like '"+fieldName+"%'");
    }
}