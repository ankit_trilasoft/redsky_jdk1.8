// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`company` ADD COLUMN `cportalBrandingURL` VARCHAR(250) DEFAULT NULL AFTER `accessQuotationFromCustomerFile`;
 
ALTER TABLE `redsky`.`companydivision` ADD COLUMN `cportalBrandingURL` VARCHAR(250) DEFAULT NULL AFTER `lastUpdatedLeave`;
 
ALTER TABLE `redsky`.`companydivision` ADD COLUMN `companyWebsite` VARCHAR(100) AFTER `childAgentCode`;

ALTER TABLE `redsky`.`refjobtype` ADD COLUMN `accAccountPortal` bit(1) DEFAULT b'1' AFTER `personClaims`;

update refjobtype set accAccountPortal=true;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `accountLineAccountPortalFlag` bit(1) DEFAULT b'0' AFTER `soExtractSeq`;

update systemdefault set accountLineAccountPortalFlag=false;

ALTER TABLE `redsky`.`dynamicpricingcalender` ADD COLUMN `noOfMen` INTEGER DEFAULT 0 AFTER `dynamicPricingType`;

//already executed on prod as per sunil sir
alter table billing modify billtoauthority varchar(50);

alter table billing modify billto2authority varchar(50);
//

ALTER TABLE `redsky`.`myfile` ADD COLUMN `accountLineVendorCode` VARCHAR(25) AFTER `documentCategory`,
ADD COLUMN `accountLinePayingStatus` VARCHAR(20) AFTER `accountLineVendorCode`;

ALTER TABLE `redsky`.`accountline` ADD (`myFileFileName` VARCHAR(375) ,`externalIntegrationReference` VARCHAR(10) );

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isvendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isPaymentStatus` BIt(1) DEFAULT b'0';

create table sodashboard as
select
    s.id,
    s.firstName,
    s.lastName,
    s.shipnumber,
trim( both '/' from concat(if(t.originagent is not null and t.originagent<>'','OA/',''),
if(destinationagent is not null and destinationagent <>'','DA/',''),
if(t.originsubagent is not null and t.originsubagent<>'','OSA/',''),
if(t.destinationsubagent is not null and t.destinationsubagent<>'','DSA/',''),
if(t.brokercode is not null and t.brokercode<>'','BR/',''),
if(t.forwardercode is not null and t.forwarder<>'','FR/',''),
if(s.bookingagentcode is not null and s.bookingagentcode<>'','BA/',''),
if(t.networkPartnerCode is not null and t.networkpartnercode<>'','NA/',''),
if(m.haulingAgentCode is not null and m.haulingagentcode<>'','HA/',''))) as 'role',
    s.shipmenttype,
    s.job,
    s.coordinator,
    s.status,
    s.registrationnumber,
    s.statusreason as uvlstatus,
    if(actualSurveyDate is null,'OD',actualSurveyDate) as survey,
    if(sd.weightunit='Kgs',if(m.estimatedNetWeightKilo is null,'OD',m.estimatedNetWeightKilo),if(m.estimatedNetWeight is null,'OD',m.estimatedNetWeight)) as estweight,
    if(sd.volumeunit='Cbm',m.netEstimateCubicMtr,m.netEstimateCubicMtr) as estvolume,
    if(t.loadA is not null,'Actualized','NA') as ldpkticket,
    if(t.loadA is not null,t.loadA,'OD') as loading,
    if(sd.weightunit='Kgs',if(m.actualNetWeightKilo is null,'OD',m.actualNetWeightKilo),if(m.actualNetWeight is null,'OD',m.actualNetWeight)) as actualweight,
    s.statusreason as actVolume,
    m.drivername as driver,
    m.carrier as truck,
    if(t.deliveryA is not null,'Actualized','NA') as deliveryticket,
    if(t.deliveryA is not null,t.deliveryA,'OD') as delivery,
    (select claimnumber from claim where shipnumber=s.shipnumber limit 1) as claims,
    b.auditComplete as qc,
    s.corpid as 'corpID',
    if(c.survey is null,'',c.survey) as estSurvey,
    if(t.beginLoad is not null,t.beginLoad,'') as estLoding,
     if(t.deliveryShipper is not null,t.deliveryShipper,'') as estDelivery
from
    serviceorder s ,trackingstatus t,miscellaneous m,billing b,claim cl,customerfile c
,systemdefault sd
    where s.corpid in ('RSKY') and s.job in ('DOM' , 'UVL', 'MVL') and
t.id=s.id and t.corpid in ('RSKY') and  b.corpid in ('RSKY') and b.id=s.id
  and  s.createdon > '2000-01-01'
and s.id=m.id and m.corpid in ('RSKY')
and c.id=s.customerfileid and c.corpid in ('RSKY')
and cl.serviceOrderId=s.id and cl.corpid in ('RSKY')
and s.registrationnumber <>'' and s.registrationnumber is not null
and sd.corpid=s.corpid
and s.status='CLMS' ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `passwordPolicy` bit(1) DEFAULT '' AFTER `soExtractSeq`;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('TSFT', 'WOP', 'Work Permit', 5, 'SERVICE','en','Relo','dspDetails.WOP_serviceEndDate',now(),'kkumar',now(),'kkumar'),
('TSFT', 'REP', 'Residence Permit', 5, 'SERVICE','en','Relo','dspDetails.REP_serviceEndDate',now(),'kkumar',now(),'kkumar');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('TSFT', 'DsWorkPermit', 'DsWorkPermit', 20, 'NOTESUBTYPE','en','Service Order',now(),'kkumar',now(),'kkumar'),
('TSFT', 'DsResidencePermit', 'DsResidencePermit', 20, 'NOTESUBTYPE','en','Service Order',now(),'kkumar',now(),'kkumar');
 
update refmaster set description='Visa' where parameter='SERVICE' and flex1='Relo' and code='VIS' and corpId='TSFT';
 
ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `VIS_visaStartDate` DATETIME DEFAULT NULL AFTER `VIS_displyOtherVendorCode`;
 
ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `WOP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `VIS_visaStartDate`,
 ADD COLUMN `WOP_vendorName` VARCHAR(225) DEFAULT NULL AFTER `WOP_vendorCode`,
 ADD COLUMN `WOP_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `WOP_vendorName`,
 ADD COLUMN `WOP_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `WOP_vendorContact`,
 ADD COLUMN `WOP_serviceStartDate` DATETIME DEFAULT NULL AFTER `WOP_vendorEmail`,
 ADD COLUMN `WOP_serviceEndDate` DATETIME DEFAULT NULL AFTER `WOP_serviceStartDate`,
 ADD COLUMN `WOP_workPermitHolderName` VARCHAR(100) DEFAULT NULL AFTER `WOP_serviceEndDate`,
 ADD COLUMN `WOP_providerNotificationDate` DATETIME DEFAULT NULL AFTER `WOP_workPermitHolderName`,
 ADD COLUMN `WOP_permitStartDate` DATETIME DEFAULT NULL AFTER `WOP_providerNotificationDate`,
 ADD COLUMN `WOP_visaExpiryDate` DATETIME DEFAULT NULL AFTER `WOP_permitStartDate`,
 ADD COLUMN `WOP_reminder3MosPriorToExpiry` DATETIME DEFAULT NULL AFTER `WOP_visaExpiryDate`,
 ADD COLUMN `WOP_questionnaireSentDate` DATETIME DEFAULT NULL AFTER `WOP_reminder3MosPriorToExpiry`,
 ADD COLUMN `WOP_authorizationLetter` BIT(1) DEFAULT b'0' AFTER `WOP_questionnaireSentDate`,
 ADD COLUMN `WOP_copyResidence` BIT(1) DEFAULT b'0' AFTER `WOP_authorizationLetter`,
 ADD COLUMN `WOP_vendorCodeEXSO` VARCHAR(25) DEFAULT NULL AFTER `WOP_copyResidence`,
 ADD COLUMN `WOP_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `WOP_vendorCodeEXSO`,
 ADD COLUMN `WOP_comment` MEDIUMTEXT DEFAULT NULL AFTER `WOP_displyOtherVendorCode`;
  
ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `REP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `WOP_comment`,
 ADD COLUMN `REP_vendorName` VARCHAR(225) DEFAULT NULL AFTER `REP_vendorCode`,
 ADD COLUMN `REP_vendorContact` VARCHAR(225) DEFAULT NULL AFTER `REP_vendorName`,
 ADD COLUMN `REP_vendorEmail` VARCHAR(65) DEFAULT NULL AFTER `REP_vendorContact`,
 ADD COLUMN `REP_serviceStartDate` DATETIME DEFAULT NULL AFTER `REP_vendorEmail`,
 ADD COLUMN `REP_serviceEndDate` DATETIME DEFAULT NULL AFTER `REP_serviceStartDate`,
 ADD COLUMN `REP_workPermitHolderName` VARCHAR(100) DEFAULT NULL AFTER `REP_serviceEndDate`,
 ADD COLUMN `REP_providerNotificationDate` DATETIME DEFAULT NULL AFTER `REP_workPermitHolderName`,
 ADD COLUMN `REP_permitStartDate` DATETIME DEFAULT NULL AFTER `REP_providerNotificationDate`,
 ADD COLUMN `REP_permitExpiryDate` DATETIME DEFAULT NULL AFTER `WOP_permitStartDate`,
 ADD COLUMN `REP_reminder3MosPriorToExpiry` DATETIME DEFAULT NULL AFTER `REP_permitExpiryDate`,
 ADD COLUMN `REP_questionnaireSentDate` DATETIME DEFAULT NULL AFTER `WOP_reminder3MosPriorToExpiry`,
 ADD COLUMN `REP_authorizationLetter` BIT(1) DEFAULT b'0' AFTER `REP_questionnaireSentDate`,
 ADD COLUMN `REP_copyResidence` BIT(1) DEFAULT b'0' AFTER `REP_authorizationLetter`,
 ADD COLUMN `REP_vendorCodeEXSO` VARCHAR(25) DEFAULT NULL AFTER `REP_copyResidence`,
 ADD COLUMN `REP_displyOtherVendorCode` BIT(1) DEFAULT b'0' AFTER `REP_vendorCodeEXSO`,
 ADD COLUMN `REP_comment` MEDIUMTEXT DEFAULT NULL AFTER `REP_displyOtherVendorCode`;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `WOP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `incExcServiceTypeLanguage`,
ADD COLUMN `REP_vendorCode` VARCHAR(25) DEFAULT NULL AFTER `WOP_vendorCode`;
 
Insert into
refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,bucket)values
('TSFT', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'ashish',now(),'ashish','WOP_vendorCode'),
('TSFT', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'ashish',now(),'ashish','REP_vendorCode');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('CustomerFile','moveType' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('CustomerFile','quotationStatus' ,'Update','amishra',now(),'amishra',now(),false,'','','');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('ServiceOrder','quoteStatus' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`dsfamilydetails` ADD COLUMN `nationality` VARCHAR(82) DEFAULT NULL;

alter table `redsky`.`dsfamilydetails` add column `nativeLanguage` varchar(45) default null;

alter table `redsky`.`dsfamilydetails` add column `passportIssuedDate` datetime;

alter table `redsky`.`dsfamilydetails` add column `grade` varchar(45) default null;

ALTER TABLE `redsky`.`reports` ADD COLUMN `docx` VARCHAR(20) DEFAULT '' AFTER `attachedURL`;
 


 

 

 
