ALTER TABLE `redsky`.`company` ADD COLUMN `certificateAdditionalNumber` BIGINT(20) DEFAULT 20000 AFTER `creditInvoiceSequence`,ADD COLUMN `printInsuranceCertificate` BIT(1) DEFAULT b'0' AFTER `certificateAdditionalNumber`;
 
ALTER TABLE `redsky`.`billing` ADD COLUMN `deductible` VARCHAR(50) AFTER `estMoveCost`,ADD COLUMN `additionalNo` VARCHAR(10) DEFAULT '' AFTER `deductible`;

DROP TABLE IF EXISTS `redsky`.`modulepermissions`;
CREATE TABLE  `redsky`.`modulepermissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(225) DEFAULT NULL,
  `pageaction_id` bigint(20) DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `corpId` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `redsky`.`pageaction`;
CREATE TABLE  `redsky`.`pageaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actionuri` varchar(50) DEFAULT NULL,
  `actionclass` varchar(250) NOT NULL,
  `method` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

DROP TABLE IF EXISTS `redsky`.`pageactionmap`;
CREATE TABLE `pageactionmap` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `pageaction_id` bigint(20) NOT NULL,
  `menuitem_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `redsky`.`pageactionpermission`;
CREATE TABLE `pageactionpermission` (
  `pageaction_id` bigint(20) NOT NULL,
  `permission` int(11) NOT NULL,
  `ROLE` varchar(50) DEFAULT NULL
);

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `ISFConfirmationNo` VARCHAR(20) AFTER `contractType`;

ALTER TABLE `redsky`.`systemdefault` DROP COLUMN `securityChecked`;

ALTER TABLE `redsky`.`company` ADD COLUMN `securityChecked` BIt(1) DEFAULT b'0' AFTER `certificateAdditionalNumber`;

update networkdatafields set fieldname='originAgentEmail' where fieldName='OriginAgentEmail';

