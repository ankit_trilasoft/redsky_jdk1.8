package com.trilasoft.app.dao.hibernate;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ItemsJbkEquipDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.ItemsJbkEquipManager;
  
public class ItemsJbkEquipDaoHibernate extends GenericDaoHibernate<ItemsJbkEquip, Long> implements ItemsJbkEquipDao {   
  
    private List itemsJbkEquips;
    private String newstatus;
    private List totalCartons;
    private int totalCartns;
    private List countCartons;
    private int count; 
    private HibernateUtil hibernateUtil;
    private String check;
    private Long itemsJbkId;
    private ItemsJbkEquipManager itemsJbkEquipManager;
    public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public ItemsJbkEquipDaoHibernate() {   
        super(ItemsJbkEquip.class);   
    }   
   

    public List<ItemsJbkEquip> findByType(String type) {
		return getHibernateTemplate().find("from ItemsJbkEquip where type=?", type);
	} 
    
    public List<ItemsJbkEquip> findById(Long id) {
    	//System.out.println("seq"+id);
		return getHibernateTemplate().find("from ItemsJbkEquip where id=?", id);
	} 
    
    public List<ItemsJbkEquip> findByDescript(String descript) {
		return getHibernateTemplate().find("from ItemsJbkEquip where descript=?", descript);
	}
    
    public List<ItemsJbkEquip> findByDescript(String type, String descript) {
    	//System.out.println(type);
    	//System.out.println(descript);
    	//System.out.println(id);
    	
    	if(type != "" && descript != "") {
    		itemsJbkEquips = getHibernateTemplate().find("from ItemsJbkEquip where type='" + type + "' AND descript='" + descript+"'" );
        	
            }
    	
        if(type != "" && descript.equals("")) {
        	
        	itemsJbkEquips = getHibernateTemplate().find("from ItemsJbkEquip where type='" + type + "'");
        	
        }
        if(type.equals("") && descript != "") {
        	itemsJbkEquips = getHibernateTemplate().find("from ItemsJbkEquip where descript='" + descript + "'");
        	
        }
        return itemsJbkEquips;
    }
    public List<ItemsJbkEquip> findSection(Long ticket, String itemType)
	{
    	//System.out.println(ticket);
    	
		newstatus = "A";
		
		itemsJbkEquips= getHibernateTemplate().find("from ItemsJbkEquip where flag='" + newstatus + "' AND  ticket='" + ticket + "' AND  type='" + itemType + "'");
		 return itemsJbkEquips;
	}
  
    public void genrateInvoiceNumber(Long id3) {
    	//System.out.println("anuj"+id3);
    	getHibernateTemplate().delete("delete ItemsJbkEquip  where id='"+id3+"' ");
    }
    
    public int updateWorkTicketCartons(Long ticket) {
    	String type = "M"; 
    	totalCartns = 0;
    	//System.out.println("Chuk de....");
    	countCartons = getHibernateTemplate().find("Select count(*) from ItemsJbkEquip where ticket='" + ticket + "' AND  type='" + type + "' ");
    	
    	count = Integer.valueOf( countCartons.get(0).toString() ).intValue();
    	//System.out.println(count);
    	totalCartons = getHibernateTemplate().find("Select qty from ItemsJbkEquip where ticket='" + ticket + "' AND  type='" + type + "'");
    	for(int i=0; i<count; i++){
    		totalCartns = totalCartns +  Integer.valueOf(totalCartons.get(i).toString()).intValue();
    		//System.out.println(totalCartns);
    	}
    	//totalCartns = Integer.parseInt(totalCartons.get(0).toString());
    	
    	return getHibernateTemplate().bulkUpdate("update WorkTicket set estimatedCartoons='" +totalCartns+ "' where ticket=?", ticket);
    	
    }
  
    /*
    public int genrateInvoiceNumber(Long ticket) {
    	newstatus = "A";
    	
		// return itemsJbkEquips;
    	
    	 
  return getHibernateTemplate(). createQuery("delete ItemsJbkEquip where ticket="+ticket+" and flag='" + newstatus + "'");
  
    }
     */
    
    public List findByShipNum(String shipNumber,String corpId){
    	String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' AND  corpId='" + corpId + "' and workTicketID is null and day is not null order by day";
    	List list = getHibernateTemplate().find(query);
		return list;
    }
    public List findByShipNumAndTicket(String shipNumber,String ticket){
    	String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' and  ticket=" + ticket;
    	List list = getHibernateTemplate().find(query);
		return list;
    }
    public Map<String, List> findByShipNumAndDay(String shipNumber,String corpId,String dayFocus){
    	Map<String, List> map = new LinkedHashMap<String, List>();
    	int day = findDayTemplate(shipNumber, corpId);
    	for (int i = 1; i <= day; i++) {
    		String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' AND  corpId='" + corpId + "' and ticket is null and day='"+i+"' order by revisionInvoice,type,descript";
        	List list = getHibernateTemplate().find(query);
        	String DAY = String.valueOf(i);
        	if(list == null || list.isEmpty() || list.size() == 0){
        		map.put(DAY, new ArrayList<ItemsJbkEquip>());
        	}else{
        		map.put(DAY, list);
        	}
		}
		return map;
    }
    public Map<String, Map> findByShipNumAndDayMap(String shipNumber,String corpId,String dayFocus){
    	Map<String, Map> outerMap = new LinkedHashMap<String, Map>();
    	int day = findDayTemplate(shipNumber, corpId);
    	
    	/*String query1 = "select distinct r.description from itemsjequip i,refmaster r where type is not null and i.type !='' and i.type=r.code and r.parameter='Resource_Category' order by i.type";
    	List list1 = getSession().createSQLQuery(query1).list();
    	Iterator it = null;*/
    	
    	for (int i = 1; i <= day; i++) {
//    		it = list1.iterator();
    		Map<String, List<ItemsJbkEquip>> innerMap = new LinkedHashMap<String, List<ItemsJbkEquip>>();
    		String query1 = "select distinct i.type,r.description from itemsjbkequip i, refmaster r"+
							" where shipNum='"+shipNumber+"' and ticket is null and day='"+i+"' and type is not null and type !=''"+
							" and r.code = i.type and r.parameter='Resource_Category' order by type";
    		Iterator it = getSession().createSQLQuery(query1).list().iterator();

    		while(it.hasNext()){
    			Object[] obj = (Object[])it.next();
    	        String type = obj[0].toString();
    	        String desc = obj[1].toString();
    	        String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' AND  type='" + type + "' and ticket is null and day='"+i+"' order by revisionInvoice,type,descript";
    	        List<ItemsJbkEquip> list = getHibernateTemplate().find(query);
    	        
    	        if(list == null || list.isEmpty() || list.size() == 0){
    	        	innerMap.put(desc, new ArrayList<ItemsJbkEquip>());
            	}else{
            		innerMap.put(desc, list);
            	}
    		}
    		outerMap.put(String.valueOf(i), innerMap);
		}
		return outerMap;
    }
    
    public Map<String, Map> findByShipNumAndDayMap1(String shipNumber,String corpId,String dayFocus){
    	Map<String, Map> outerMap = new LinkedHashMap<String, Map>();
    	int day = findDayTemplate(shipNumber, corpId);
    	
    	for (int i = 1; i <= day; i++) {
    		Map<String, List<ItemsJbkEquip>> innerMap = new LinkedHashMap<String, List<ItemsJbkEquip>>();
	        String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' AND ticket is null and day is not null and day !='' order by revisionInvoice,type,descript";
	        List<ItemsJbkEquip> list = getHibernateTemplate().find(query);
	        
	        for (ItemsJbkEquip itemsJbkEquip : list) {
	        	if (innerMap.containsKey(itemsJbkEquip.getType())) {
	        		List<ItemsJbkEquip> itemList = innerMap.get(itemsJbkEquip.getType());
	        		itemList.add(itemsJbkEquip);
	        		innerMap.put(itemsJbkEquip.getType(), itemList);
				}else{
					List<ItemsJbkEquip> itemList = new ArrayList<ItemsJbkEquip>(); 
					itemList.add(itemsJbkEquip);
					innerMap.put(itemsJbkEquip.getType(), itemList);
				}
			}
	        if (innerMap.size() > 0) {
	        	String query1 = "select distinct i.type,r.description from itemsjbkequip i, refmaster r"+
								 " where shipNum='"+shipNumber+"' and ticket is null and day is not null and day !='' and type is not null and type !=''"+
								 " and r.code = i.type and r.parameter='Resource_Category' order by type";
	    		Iterator it = getSession().createSQLQuery(query1).list().iterator();
	        	while(it.hasNext()){
	    	        Object[] obj = (Object[])it.next();
	    	        String type = obj[0].toString();
	    	        String description = obj[1].toString();
	    	        if (innerMap.containsKey(type)) {
	    	        	List<ItemsJbkEquip> itemList = innerMap.get(type);
	    	        	innerMap.remove(type);
	    	        	innerMap.put(description, itemList);
	    	        }
	        	}
			}
    		outerMap.put(String.valueOf(i), innerMap);
		}
		return outerMap;
    }
    public List findAllResourcesAjax(String shipNumber,String sessionCorpID)
    {
    	return getHibernateTemplate().find("from ItemsJbkEquip where shipNum='" + shipNumber + "' AND ticket is null and day is not null and day !='' group by type,descript");
    }
    
    public String findDateByShipNum(String shipNumber){
    	String dayAndDate="";
    	String query = "select distinct day,beginDate,endDate from itemsjbkequip where shipNum='"+shipNumber+"'"+
						" and ticket is null and beginDate is not null and beginDate !=''"+
						" and endDate is not null and endDate !=''";
    	List list = getSession().createSQLQuery(query).list();
    	if(list != null && list.size()>0 && !list.isEmpty()){
    		Iterator it = list.iterator();
    		while (it.hasNext()) {
				Object[] ob = (Object[]) it.next();
				if(dayAndDate.length()==0){
					dayAndDate = ob[0].toString().trim()+"*@"+ob[1].toString().trim()+"*@"+ob[2].toString().trim();
				}else{
					dayAndDate = dayAndDate+"_"+ob[0].toString().trim()+"*@"+ob[1].toString().trim()+"*@"+ob[2].toString().trim();
				}
			}
    	}
		return dayAndDate;
    }
    public String updateItemsJbkResource(String resourceListServer,String categoryListServer, String qtyListServer, String returnedListServer, String actQtyListServer, String costListServer, String actualListServer,	String sessionCorpID, WorkTicket workTicket, String fromDate, String toDate, String hubId , String chkLimit,String estListServer,String comListServer, String updatedBy){    	
    	String[] idandValue= new String[2];	
        check="";
    	if(qtyListServer!=null && (!(qtyListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =qtyListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0";
				   }
				   itemsJbkId = Long.parseLong(idandValue[0].trim());
				   ItemsJbkEquip itemsJbkEquip = itemsJbkEquipManager.get(itemsJbkId);
				   if(itemsJbkEquip!=null){
				   int quantity=0;
				   List qtyList = getHibernateTemplate().find("select qty from ItemsJbkEquip where id='"+idandValue[0]+"' ");
				   if(qtyList!=null && (!(qtyList.isEmpty()))){
					   int usedEstQuanitity = 0;
					   List usedEstQuanitityList =  usedEstQuanitityList(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(),fromDate, toDate, hubId, workTicket.getService(), sessionCorpID,workTicket.getTicket());
					   if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty()))){
						   usedEstQuanitity = Integer.parseInt(usedEstQuanitityList.get(0).toString());
					   }
					   quantity = Integer.parseInt(qtyList.get(0).toString());
					   usedEstQuanitity = usedEstQuanitity - quantity;
					   List dailyResourceLimitList = getDailyResourceLimit(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(), fromDate, toDate, hubId, sessionCorpID);
					   int dailyResourceLimit = 0;
					   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty()))){
						   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
						   dailyResourceLimit = dailyQty.intValue();
						
					   }
					   int tempVal = Integer.parseInt(idandValue[1].trim());
					   int totalUsedQty = usedEstQuanitity + tempVal;
					   if((totalUsedQty > dailyResourceLimit) && dailyResourceLimit>0){
						   if(chkLimit.equalsIgnoreCase("chkLimit")){
							   check="Greater";
							   return check;
						   }else{
							   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set qty='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'"); 
							   check="toGoP";
							   //return check;
						   }
					   }else if((totalUsedQty < dailyResourceLimit) && dailyResourceLimit>0){
						      getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set qty='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'"); 
							  if(!(check.equalsIgnoreCase("toGoP"))){
								  check="toGoT";
							  }
					   }else{
						   getHibernateTemplate().flush();
						   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set qty='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
						   
					   }					   
				     }
				   }else{
					   check="NA";
					   return check;
				   }
			  }
		 }
    	
    	if(resourceListServer!=null && (!(resourceListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =resourceListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   getHibernateTemplate().flush();
				       getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set descript='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
				       
				}
		}
    	if(categoryListServer!=null && (!(categoryListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =categoryListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   getHibernateTemplate().flush();
					   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set type='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
					   
				}
		  }
    	
    	if(comListServer!=null && (!(comListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =comListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="";
				   }
				   ItemsJbkEquip itemsJbkEquip = itemsJbkEquipManager.get(Long.valueOf(idandValue[0]));
				   itemsJbkEquip.setWorkTicketID(workTicket.getId());
				   itemsJbkEquip.setTicket(workTicket.getTicket());
				   itemsJbkEquip.setComments(idandValue[1].trim());
				   
				 getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set comments='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"' where id='"+idandValue[0]+"'");
				}
		  }
    	
    	if(estListServer!=null && (!(estListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =estListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0.00";
				   }
				   getHibernateTemplate().flush();
					   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set estHour='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
					   
				}
		  }
    	
    	if(returnedListServer!=null && (!(returnedListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =returnedListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0.00";
				   }
				   getHibernateTemplate().flush();
					   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set returned='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
					   
				}
		  }
    	if(actQtyListServer!=null && (!(actQtyListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =actQtyListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0.00";
				   }
				   getHibernateTemplate().flush();
					   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set actualQty='"+idandValue[1]+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
					  
				}
		  }
    	if(costListServer!=null && (!(costListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =costListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0.00";
				   }
				   getHibernateTemplate().flush();
					   getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set cost='"+idandValue[1]+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
					   
				}
		  }
    	if(actualListServer!=null && (!(actualListServer.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =actualListServer.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   if(idandValue[1].trim().equals("")){
					   idandValue[1]="0.00";
				   }
				   getHibernateTemplate().flush();
				getHibernateTemplate().bulkUpdate("update ItemsJbkEquip set actual='"+idandValue[1].trim()+"',workticketId='"+workTicket.getId()+"',ticket='"+workTicket.getTicket()+"',updatedOn=now(),updatedBy='"+ updatedBy+ "' where id='"+idandValue[0]+"'");
				
				}
		  }
    	updateWorkTicketCartons(workTicket.getTicket());
    	return check;
    }

	public List<ItemsJbkEquip> checkResourceDuplicate(String shipNumber,String corpId, String category, String resource) {
		String query = "from ItemsJbkEquip where shipNum='" + shipNumber + "' AND  corpId='" + corpId + "' and type='"+category+"' and descript='"+resource+"'";
    	List list = getHibernateTemplate().find(query);
		return list;
	}
  
    public String checkIsExist(Long itemsJbkId, String category, String resource, String sessionCorpID){
    	List list = getHibernateTemplate().find("from ItemsJbkEquip where id='"+itemsJbkId+"' and type='"+category+"' and descript='"+resource+"' ");
    	if(list!=null && (!(list.isEmpty()))){
    		check = "Y"; 
    	}else{
    		check = "N";
    	}
    	return check;
    }
    public OperationsResourceLimits getOperationsResourceHub(String warehouse, String sessionCorpID){
    	String hub = "";
    	List opsHubList = new ArrayList();
    	OperationsResourceLimits operationsResourceLimits = new OperationsResourceLimits();
    	List operationMgmtByValue  = this.getSession().createSQLQuery("select operationMgmtBy from systemdefault where corpID = '"+sessionCorpID+"' ").list();
		if(operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse"))
		{		
    	opsHubList = getHibernateTemplate().find("from OperationsResourceLimits where hubId = '"+warehouse+"'");
    	if(!opsHubList.isEmpty()){
    		operationsResourceLimits = (OperationsResourceLimits)opsHubList.get(0);				
    	}
    	else{
    		List hublist =  getSession().createSQLQuery("select flex1 from refmaster where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'HOUSE' and code = '"+warehouse+"'").list();
    		if(!hublist.isEmpty()){
    			hub = hublist.get(0).toString();
    			opsHubList = getHibernateTemplate().find("from OperationsResourceLimits where hubId = '"+hub+"'");
			if(!opsHubList.isEmpty()){
				operationsResourceLimits = (OperationsResourceLimits)opsHubList.get(0);
			}		
		}
    	}
		}else{
    		List hublist =  getSession().createSQLQuery("select flex1 from refmaster where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'HOUSE' and code = '"+warehouse+"'").list();
    		if(!hublist.isEmpty()){
    			hub = hublist.get(0).toString();
    			opsHubList = getHibernateTemplate().find("from OperationsResourceLimits where hubId = '"+hub+"'");
			if(!opsHubList.isEmpty()){
				operationsResourceLimits = (OperationsResourceLimits)opsHubList.get(0);
			}
		}
	 }
		return operationsResourceLimits;
    }
    
    public List serviceTypeList(String corpId, String parameter )
	{
			List typeServiceList;
			typeServiceList = getHibernateTemplate().find(
					  "select code from RefMaster where  parameter= '" + parameter
						+ "'and corpID in ('TSFT','"
									+ hibernateUtil.getParentCorpID(corpId) + "','"
									+ corpId + "')  and bucket='Y' order by code ");
			return typeServiceList;
	}
    
    public String wtServiceList(String corpId){
    	String wttypeService="";
		List wtServiceTypeDyna=serviceTypeList(corpId, "TCKTSERVC");
		Iterator iter1 = wtServiceTypeDyna.iterator();
		while(iter1.hasNext())
		{
			if(wttypeService.equals(""))
			{
				wttypeService="'"+iter1.next().toString()+"'";
			}else{
				wttypeService=wttypeService+",'"+iter1.next().toString()+"'";
			}
			
		}
		if(wttypeService.equals(""))
		{
			wttypeService="''";
		}
		return wttypeService;
    }
    
    public List usedEstQuanitityList(String category, String resource,	String fromDate, String toDate, String hubId, String service, String sessionCorpID,Long ticket){	
    	String wttypeService=wtServiceList(sessionCorpID);
    	List warehouseHubList  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubId+"' ").list();
    	if(warehouseHubList!=null && !warehouseHubList.isEmpty()){
    		String queryString = "select if(sum(qty) is null,'0',sum(qty)) from itemsjbkequip i where (i.ticket in ( Select t.ticket from workticket t, refmaster r where t.targetActual not in ('C','R','P')  " +
				"and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubId+"' and ((t.date1  between '"+fromDate+"' and  '"+toDate+"') or (t.date2 between '"+fromDate+"' and  '"+toDate+"') " +
				"or  (t.date1 <  '"+fromDate+"' and t.date2 > '"+toDate+"'))) or i.ticket = '"+ticket+"') and corpid='"+sessionCorpID+"' and type like '"+category+"' and descript like '"+resource+"'";
    		List list = this.getSession().createSQLQuery(queryString).list();
    		return list;
    	}else{
    	String queryString = "select if(sum(qty) is null,'0',sum(qty)) from itemsjbkequip i where (i.ticket in ( Select t.ticket from workticket t, refmaster r where t.targetActual not in ('C','R','P')  " +
    			"and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code " +
    			"and r.bucket = 'Y' and r.flex1 = '"+hubId+"' and ((t.date1  between '"+fromDate+"' and  '"+toDate+"') or (t.date2 between '"+fromDate+"' and  '"+toDate+"') " +
    			"or  (t.date1 <  '"+fromDate+"' and t.date2 > '"+toDate+"')) and r.parameter = 'HOUSE') or i.ticket = '"+ticket+"') and corpid='"+sessionCorpID+"' and type like '"+category+"' and descript like '"+resource+"'";
	    	List list1 = this.getSession().createSQLQuery(queryString).list();
	    	return list1;
    	}
    }
    public List getDailyResourceLimit(String category, String resource, String fromDate, String toDate, String hubId, String sessionCorpID){
    	List myList = new ArrayList();
    	List addDays = new ArrayList();
    	double totalQty=0;
    	double qty=0;
    	resource=resource.replaceAll("%20", "");
    	List dailyResourceLimitList = new ArrayList();
    	List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+toDate+"', '"+fromDate+"')").list();	
		Long diff = Long.parseLong(getDiff.get(0).toString());
		if(diff >= 0){
			String sendDate="";
			int intValueDiff = diff.intValue();
			for(int i=0;i<=intValueDiff;i++ ){
				qty=0;
				addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+fromDate+"', INTERVAL '"+i+"' DAY) AS CHAR)").list();
				dailyResourceLimitList = this.getSession().createSQLQuery("Select if(resourceLimit is null or resourceLimit='',0,resourceLimit) from resourcegrid where corpID = '"+sessionCorpID+"' and workDate ='"+ addDays.get(0) +"' and hubId = '"+hubId+"' and resource='"+resource+"' and category='"+category+"'").list();
				if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty()))){
					qty = Double.parseDouble(dailyResourceLimitList.get(0).toString());	
				}
				totalQty = totalQty + qty;
			}
			myList.add(totalQty);
		}
		if(totalQty==0){
			myList = this.getSession().createSQLQuery("Select if(resourceLimit is null or resourceLimit='',0,resourceLimit) from operationsresourcelimits where hubId = '"+hubId+"' and resource like '"+resource+"' and category like '"+category+"' ").list();
		}
		
		return myList;
    }
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public Long getItemsJbkId() {
		return itemsJbkId;
	}
	public void setItemsJbkId(Long itemsJbkId) {
		this.itemsJbkId = itemsJbkId;
	}
	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	 public List getResourceAutoComplete(String corpId,String tempDescript,String tempCategory){
		List list = this.getSession().createSQLQuery("select descript from itemsjequip where type='"+tempCategory+"' and corpid='"+corpId+"'and descript like '%"+tempDescript+"%';").list();
	 return list;
	 
	 }

	
	public int findDayTemplate(String shipNumber, String corpID) {
		List list = getSession().createSQLQuery("select distinct max(day+0) from itemsjbkequip where shipNum='"+shipNumber+"' and corpID='" + corpID +"'").list();
		int day=0;
		if(!list.isEmpty() && list.get(0) != null){
			if(list.get(0).toString().indexOf(".")>=0){
				String s = list.get(0).toString().substring(0, list.get(0).toString().indexOf("."));
				day = Integer.parseInt(s);
				return day;
			}
			day = Integer.parseInt(list.get(0).toString());
		}
		return day;
	}
	
	public void deleteAll(String shipNumber, String corpID, List day){
//		List<ItemsJbkEquip> list = getHibernateTemplate().find("select distinct day from ItemsJbkEquip where shipNum='"+shipNumber+"' and corpID='" + corpID +"'");
		
	}
	public void saveDateAjax(String shipNum,String day, String beginDt, String endDt) {
		String SOquery = "update itemsjbkequip "+
		"set beginDate='"+beginDt+"',endDate='"+endDt+
		"' where day='"+day+"' and shipNum='"+shipNum+"'";
		
		getSession().createSQLQuery(SOquery).executeUpdate();
	}
	public List<ItemsJbkEquip> getResource(String corpId, String shipNum,String day) {
		String SOquery = "select * from itemsjbkequip where shipNum='"+shipNum+"' and corpId='"+corpId+"' and day='"+day+"' and ticketTransferStatus is not true";
		List list1 = getSession().createSQLQuery(SOquery).list();
		
		list1= convertObjetToModel(list1);
		
//		String query = "from ItemsJbkEquip where shipNum='"+shipNum+"' and corpId='"+corpId+"' and day='"+day+"' and ticketTransferStatus is not true";
//		List<ItemsJbkEquip> list = getHibernateTemplate().find(query);
		return list1;
	}
	
	private List<ItemsJbkEquip> convertObjetToModel(List list1){
		List<ItemsJbkEquip> itemList = new ArrayList<ItemsJbkEquip>();
		Iterator it=list1.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           Long id = Long.parseLong(row[0].toString());
	           itemList.add(itemsJbkEquipManager.get(id));
		}
		return itemList;
	}
	
	public void updateTicketTransferStatus(String shipNumber,Set set){
		String s = set.toString().replace("[", "").replace("]", "");
		String SOquery = "update itemsjbkequip "+
		"set ticketTransferStatus=true"+
		" where shipNum='"+shipNumber+"' and day in ("+s+")";
		
		getSession().createSQLQuery(SOquery).executeUpdate();
	}
	public void updateAccountLineForCreatePricing(String shipNumber,String category,String resource,String day){
		if(!"".equals(day)){
			String SOquery = "update itemsjbkequip "+
			"set revisionInvoice=true"+
			" where shipNum='"+shipNumber+"' and type='"+category+"' and descript='"+resource+"' and day='"+day+"'";
			getSession().createSQLQuery(SOquery).executeUpdate();
		}else{
			String SOquery = "update itemsjbkequip "+
			"set revisionInvoice=true"+
			" where shipNum='"+shipNumber+"' and type='"+category+"' and descript='"+resource+"'";
			getSession().createSQLQuery(SOquery).executeUpdate();
		}
	}
	
	public void deleteQtyBased(String shipNumber,String corpID){
		String SOquery = "delete from itemsjbkequip "+
		" where shipNum='"+shipNumber+"' and corpId='"+corpID+"' and (qty=0 or estHour=0)";
		
		getSession().createSQLQuery(SOquery).executeUpdate();
	}
	
	public String findByShipNumAndCategoryNull(String shipNumber,String corpID,String day){
		String query ="select id from itemsjbkequip where shipNum='"+shipNumber+"' and day='"+day+"' and type is null";
		String id = "0";
		List list = getSession().createSQLQuery(query).list();
		if(list != null && (!list.isEmpty()))
			id = list.get(0).toString();
//		String temp = getHibernateTemplate().find("select id from ItemsJbkEquip where shipNum='"+shipNumber+"' and day='"+day+"' and type is null").get(0).toString();
		return id;
	}
	public List findByShipNumAndDay(String shipNumber, String day) {
		List<ItemsJbkEquip> list = getHibernateTemplate().find("from ItemsJbkEquip where shipNum='"+shipNumber+"' and day='"+day+"' and beginDate is not null and endDate is not null");
		return list;
	}
	public void deleteItemsJbkResource(ItemsJbkEquip jbkEquip,Boolean isDelete){
		Set<String> set = new HashSet<String>();
		set.add("A"); set.add("C");
		List list1 = getSession().createSQLQuery("select targetActual from workticket where  shipNumber='"+jbkEquip.getShipNum()+"' and id="+jbkEquip.getWorkTicketID()).list();
		if (list1 != null && list1.size()>0 && list1.get(0) != null && !"".equals(list1.get(0)) && !set.contains(list1.get(0).toString())) {
			if(isDelete){
				String query = "delete from itemsjbkequip where shipNum='"+jbkEquip.getShipNum()+"' and descript='"+jbkEquip.getDescript()+"' and day='"+jbkEquip.getDay()+"' and (ticket is not null or ticket !='') and corpId='"+jbkEquip.getCorpID()+"'";
				getSession().createSQLQuery(query).executeUpdate();
			}else{
				List<ItemsJbkEquip> list = getHibernateTemplate().find("from ItemsJbkEquip where shipNum='"+jbkEquip.getShipNum()+"' and descript='"+jbkEquip.getDescript()+"' and day='"+jbkEquip.getDay()+"' and workTicketID="+jbkEquip.getWorkTicketID()+" and (ticket is not null or ticket !='')");
				for (ItemsJbkEquip itemsJbkEquip : list) {
					itemsJbkEquip.setType(jbkEquip.getType());
					itemsJbkEquip.setDescript(jbkEquip.getDescript());
					itemsJbkEquip.setQty(jbkEquip.getQty());
					itemsJbkEquip.setComments(jbkEquip.getComments());
					itemsJbkEquipManager.save(itemsJbkEquip);
					break;
				}
				/*String query = "update itemsjbkequip set type='"+jbkEquip.getType()+"',descript='"+jbkEquip.getDescript()+"',qty="+jbkEquip.getQty()+",comments='"+jbkEquip.getComments()+"'  where shipNum='"+jbkEquip.getShipNum()+"' and descript='"+jbkEquip.getDescript()+"' and day='"+jbkEquip.getDay()+"' and workTicketID="+jbkEquip.getWorkTicketID()+" and (ticket is not null or ticket !='')";
				getSession().createSQLQuery(query).executeUpdate();*/
			}
		}
	}
	
	public List findActualCost(String sessionCorpID, String shipnum, Long ticket, String itemType){
		String query ="select sum(if((actual=0 || actual is null),(if(((actualQty is null) || (actualQty=0)),((if(qty is null,0.00,qty))-(if(returned is null,0.00,returned))),actualQty)*(if(cost is null,0.00,cost))),actual)) from itemsjbkequip where shipNum='"+shipnum+"' and type='"+itemType+"' and ticket="+ticket+" and corpID='" + sessionCorpID +"' ";
		List list = getSession().createSQLQuery(query).list();
		return list;
	}

public String getMaxSeqNumber(String corpID){
	String query ="select max(invoiceSeqNumber) from itemsjbkequip where corpid='"+corpID+"'";
	String id = "000001";
	List list = getSession().createSQLQuery(query).list();
	if(list != null && (!list.isEmpty()) && (list.get(0)!=null) && !(list.get(0).toString().equals("")))
		id = list.get(0).toString();
	return id;
}
public Map<String, String> getItemsjequip(String sessionCorpID) {
	String query ="select descript,cost from itemsjequip where corpid='"+sessionCorpID+"'";
	List list = getSession().createSQLQuery(query).list();
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	if(list != null && (!list.isEmpty()) && (list.get(0)!=null) && !(list.get(0).toString().equals(""))){
		Iterator itr = list.iterator();
		while(itr.hasNext()){
			Object[] row = (Object[]) itr.next();
			parameterMap.put(row[0]+"", row[1]+"");
		}
	}
	return parameterMap;
}
}