delimiter $$
CREATE FUNCTION `GetCarrierInfo`(so_id bigint(20)) RETURNS varchar(270) CHARSET latin1
BEGIN

declare carrierDetal varchar(270);

  select group_concat(concat(carrierCode,',',carrierName)separator ',') into carrierDetal from servicepartner where serviceOrderId=so_id;
  RETURN carrierDetal;

end $$

delimiter;