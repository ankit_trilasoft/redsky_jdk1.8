package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DocumentAccessControl;

public interface DocumentAccessControlDao  extends GenericDao<DocumentAccessControl, Long>{
	
	List isExisted(String fileType, String sessionCorpID);

}
