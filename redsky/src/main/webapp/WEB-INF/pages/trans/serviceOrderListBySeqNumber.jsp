<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>Universal List</title>   
    <meta name="heading" content="Universal List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
<s:form id="universalSoForm" method="post" validate="true">  
<div class="spnblk">&nbsp;</div>
<div class="spnblk">&nbsp;</div>
<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr> 

<c:if test="${controlFlag=='C'}">
<c:choose>
<c:when test="${moveTypeTemp!=null && moveTypeTemp!='' && moveTypeTemp=='Quote'}">
 <td align="left" class="listwhitetext" ><b>Quote List</b></td>
</c:when>
<c:otherwise>
	<td align="left" class="listwhitetext" ><b>Service Order List</b></td>
</c:otherwise>
</c:choose>
</c:if>
<c:if test="${controlFlag=='G'}">	
	<td align="left" class="listwhitetext" ><b>Service Order List</b></td>
</c:if>
 <c:if test="${controlFlag=='Q'}">
 <td align="left" class="listwhitetext" ><b>Quote List</b></td>
 </c:if>
 <c:if test="${controlFlag=='A'}">
 <td align="left" class="listwhitetext" ><b>Order List</b></td>
 </c:if>
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<s:set name="serviceOrderCfList" value="serviceOrderCfList" scope="request"/>   
<display:table name="serviceOrderCfList" class="table" requestURI="" id="serviceOrderCfList" pagesize="100" style="width:100%;" partialList="true" size="100"> 
  <c:if test="${controlFlag=='C'}">
  <display:column title="SO#"  style="width:15%">
  <a href="editServiceOrderUpdate.html?id=${serviceOrderCfList.id}"><c:out value="${serviceOrderCfList.shipNumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${controlFlag=='G'}">
  <display:column title="SO#"  style="width:15%">
  <a href="editServiceOrderUpdate.html?id=${serviceOrderCfList.id}"><c:out value="${serviceOrderCfList.shipNumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${controlFlag=='Q'}">
  <display:column title="Quote#"  style="width:15%">
  <a href="editQuotationServiceOrderUpdate.html?id=${serviceOrderCfList.id}"><c:out value="${serviceOrderCfList.shipNumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${controlFlag=='A'}">
  <display:column title="Order#"  style="width:15%">
  <a href="editOrderInitiation.html?id=${serviceOrderCfList.id}"><c:out value="${serviceOrderCfList.shipNumber}" /></a>
  </display:column>
  </c:if>
  
  <c:if test="${controlFlag=='C'}">
  <display:column property="status" title="Status" style="width:15%"/>
  </c:if>
    <c:if test="${controlFlag=='G'}">
  <display:column property="status" title="Status" style="width:15%"/>
  </c:if>
  <c:if test="${controlFlag=='Q'}">
   	<display:column title="Quote Status" style="width:15%">
   	<c:choose>
   	<c:when test="${serviceOrderCfList.quoteStatus=='PE' }">
  		Pending
  	</c:when>
  	<c:when test="${serviceOrderCfList.quoteStatus=='AC' }">
  		Accepted
  	</c:when>
  	<c:when test="${serviceOrderCfList.quoteStatus=='RE' }">
  		Rejected
  	</c:when>
  	<c:when test="${serviceOrderCfList.quoteStatus=='SU' }">
  		Submitted
  	</c:when>
  	<c:otherwise></c:otherwise> 
  	</c:choose>
  	</display:column>
  </c:if>
  <c:if test="${controlFlag=='A'}">
  <display:column property="status" title="Status" style="width:15%"/>
  </c:if>
  <display:column property="lastName" title="Last Name" style="width:15%"/>
  <display:column property="firstName" title="First Name" style="width:15%"/>  
  <display:column property="job" title="Job"  style="width:15%"/>
  <display:column property="billToName" title="Billing Party"  style="width:15%"/>
  <display:column property="coordinator" title="Coordinator"  style="width:15%"/>
  <display:column property="originCountry" title="Origin Country"  style="width:15%"/>
  <display:column property="originCity" title="Origin city"  style="width:15%"/>
  <display:column property="destinationCountry" title="Destination Country"  style="width:15%"/>
  <display:column property="destinationCity" title="Destination city"  style="width:15%"/>   
  <display:setProperty name="export.excel.filename" value="CustomerFile List.xls"/>   
  <display:setProperty name="export.csv.filename" value="universalSearch List.csv"/>   
  <display:setProperty name="export.pdf.filename" value="universalSearch List.pdf"/>   
</display:table>
</s:form>

<script type="text/javascript">   
    highlightTableRows("serviceOrderCfList");  
</script> 