<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    </head>

<s:form id="searchViewForm" action="" method="post" validate="true">
<div id="newmnav" style="margin-left:20px;">
<ul>
<li><a href="findPartnerProfileList.html?from=view&code=${partnerPublic.partnerCode}&partnerType=AG&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>

			<li><a href="partnerView.html"><span>Partner List</span></a></li>
			<li id="newmnav1" style="background:#FFF;"><a class="current" style="margin-bottom:0px;"><span>Portal Users & Contacts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			
</ul>
</div><div class="spn">&nbsp;</div>
<display:table name="partnerUsersList" requestURI="" defaultsort="2" id="userList" class="table" pagesize="10" style="width:100%;!margin-top:8px;" >

    <display:column  sortable="true" property="username" title="User Name" style="width: 15%">
   </display:column>
    <display:column property="first_name"  sortable="true" title="First Name" style="width: 15%"/>
    <display:column property="last_name"  sortable="true" title="Last Name" style="width: 15%"/>
    <display:column  sortable="true" title="Portal Access Enabled" style="width: 15%">
    <c:if test="${userList.account_enabled==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${userList.account_enabled==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	<c:if test="${partnerType=='AC'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserPermission('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID=='TSFT'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserPermission('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID!='TSFT'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List"  style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List"  style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	 <display:column property="userType"  sortable="true" title="User Type" style="width: 15%"/>
	</display:column>
	<c:if test="${partnerType=='AG' && sessionCorpID=='TSFT' }">
	<display:column title="Email" style="width:45px;font-size: 9px;" >
			<a href="sendEmailToAgents.html?partnerId=${id}&userId=${userList.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Email</a>    
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID!='TSFT' }">
	<display:column title="Email" style="width:45px;font-size: 9px;" >
			<img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Email    
	</display:column>
	</c:if>
    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>
</s:form>