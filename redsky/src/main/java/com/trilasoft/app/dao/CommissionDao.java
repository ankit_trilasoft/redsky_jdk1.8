package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Commission;



public interface CommissionDao extends GenericDao<Commission, Long>{
	Map<Long, Double> getAutoCommisssionMap(String sessionCorpID, String shipNumber);
	BigDecimal findTotalExpenseOfChargeACC(String sessionCorpID, String shipNumber, String tempAccId);
}
