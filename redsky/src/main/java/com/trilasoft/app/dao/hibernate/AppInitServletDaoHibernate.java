package com.trilasoft.app.dao.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessResourceFailureException;

import com.trilasoft.app.dao.AppInitServletDao;
import com.trilasoft.app.model.CorpComponentPermission;
import com.trilasoft.app.model.RoleBasedComponentPermission;

public class AppInitServletDaoHibernate extends GenericDaoHibernate implements
		AppInitServletDao {

	public AppInitServletDaoHibernate() {
		super(AppInitServletDaoHibernate.class);
	}

	public AppInitServletDaoHibernate(Class persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}
	public String getSecurityEnabledCorpId(){
		String strCorpId="";
		List permissionsList = getSession().createSQLQuery("Select corpId from company where securityChecked is true").list();
		if ((!permissionsList.isEmpty()) && (permissionsList != null)&& (permissionsList.get(0) != null)) {
			Iterator itr = permissionsList.iterator();
			while (itr.hasNext()) {
				Object row = (Object) itr.next();
				if(strCorpId.equalsIgnoreCase("")){
					strCorpId="'"+row+"'";
				}else{
					strCorpId=strCorpId+",'"+row+"'";
				}
			}
		} 
		return strCorpId;
	}
	public void populatePermissionCache(Map origPageActionUrls) {
		 String securityEnabledCorpIdList=getSecurityEnabledCorpId();
		if(!securityEnabledCorpIdList.equalsIgnoreCase("")){
		Map pageActionUrls = new HashMap();
		String pageActionQuerySql = "	select pm.corpid, pm.role , p.actionuri from menu_item m, pageactionmap mp, "+
									"	pageaction p, pageactionpermission pm "+
									"	where m.id=mp.menuitem_id "+
									"	and pm.pageaction_id=p.id "+
									"   and mp.pageaction_id=pm.pageaction_id "+
									"   and pm.corpId in ("+securityEnabledCorpIdList+") "+
									"   and m.corpid in ("+securityEnabledCorpIdList+") "+
									"   and pm.permission=2"+
									"	union"+
									"	select mp.corpid, mp.recipient, m.url"+
									"	from menu_item m,menu_item_permission mp"+  
									"	where m.id=mp.menuitemid "+
									"	and mp.menuitemid = m.id"+
									"	and m.corpId in ("+securityEnabledCorpIdList+")"+
									"	and mp.permission<>0"+
									"	union"+
									"	select mp.corpid, mp.role, p.actionuri"+ 
									"	from modulepermissions mp,pageaction p "+
									"	where mp.pageaction_id = p.id  "+
									"	and mp.corpid in ("+securityEnabledCorpIdList+") and mp.permission='2'";
		
		List pageActionsByRole = getSession()
				.createSQLQuery(pageActionQuerySql).list();
		Iterator itr = pageActionsByRole.iterator();

		while (itr.hasNext()) {
			
			Object[] row = (Object[]) itr.next();
			String corpId = (String) row[0];
			String role = (String) row[1];
			String url = (String) row[2];
			if(url.indexOf("?")>-1){
				url=url.substring(0, url.indexOf("?"));
			}			
			List urlList = null;
			
			if ((urlList = (List) pageActionUrls.get(corpId + "-" + role)) == null) {
				urlList = new ArrayList();
				urlList.add(url);
				pageActionUrls.put(corpId + "-" + role, urlList);
			} else {
				urlList.add(url);
				pageActionUrls.put(corpId + "-" + role, urlList);
			}
			
		}
		
		origPageActionUrls.clear();
		origPageActionUrls.putAll(pageActionUrls);
		}
	}

	
	public void populateRolebasedComponentPermissions(Map permissionMap){
		log.warn("Populating the role based permission cache !!!!");
		try {
			List<RoleBasedComponentPermission> rolePermissions = getSession().createQuery("from RoleBasedComponentPermission rb where componentId is not null and componentId !='' and role is not null and role !='' and mask is not null and mask !='' and corpID in (select corpID from Company where status is true) ").list();
			List permList = null;
			
			for(RoleBasedComponentPermission rb: rolePermissions){
				
				String permKey = rb.getCorpID() +"-"+rb.getComponentId();
				if((permList = (List) permissionMap.get(permKey))==null){
					permList = new ArrayList<String>();
					permList.add(rb.getRole()+"~"+rb.getMask());
					permissionMap.put(permKey, permList);
				}else{
					permList.add(rb.getRole()+"~"+rb.getMask());
					permissionMap.put(permKey, permList);
				}
			}
		} catch (Exception e) {
			log.error("Error creating the role based permission cache",e);
		} 
		
		log.warn("Role based permission cache has Populated  !!!!");
		
	}
	public List getListForSurveyCalander(String corpid,String fmdt,String todt,String consult,String surveycity,String job){
		String query="select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag From calendarfile Where (userName like '%"+consult+"' or userName like '"+corpid+"') AND corpID like '"+corpid+"' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"+fmdt+"' and '"+todt+"') and (category = 'Survey' or category = '') UNION (SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag  FROM customerfile where job like '%"+job+"' AND originCity like '%"+surveycity+"%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"+fmdt+"' and '"+todt+"') and estimator like '%"+consult+"' AND corpID like '"+corpid+"') order by surDate, consultants, frmTime";
		System.out.println("select query.........................."+query);
		return this.getSession().createSQLQuery(query).list();
	}
	
	
	public List getAll() {
		return null;
	}

	public Object get(Serializable id) {
		return null;
	}

	public boolean exists(Serializable id) {
		return false;
	}

	public Object save(Object object) {
		return null;
	}

	public void remove(Serializable id) {
	}

	public Object getForOtherCorpid(Serializable id) {
		return null;
	}

	public void removeForOtherCorpid(Serializable id) {

	}

	public void populatePageFieldVisibilityPermissions( Map pageFieldVisibilityComponentMap) {

		log.warn("Populating the Field Visibility permission cache !!!!");
		try {
			List<CorpComponentPermission> componentPermissions = getSession().createQuery("from CorpComponentPermission where componentId is not null and componentId!='' and   mask is not null  and mask!='' and corpID in (select corpID from Company where status is true) ").list();
			//List permList = null;
			
			for(CorpComponentPermission cp: componentPermissions){
				
				String permKey = cp.getCorpID() +"-"+cp.getComponentId();
				pageFieldVisibilityComponentMap.put(permKey, cp.getMask());
			}
		} catch (Exception e) {
			log.error("Error creating the role Field Visibility cache",e);
		} 
		log.warn("Field Visibility permission cache has Populated  !!!!");
		
	
		
	}

}
