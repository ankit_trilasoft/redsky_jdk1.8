package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CustomerSurveyFeedback;

public interface CustomerSurveyFeedbackDao extends GenericDao<CustomerSurveyFeedback, Long> {

}
