package com.trilasoft.app.dao.hibernate.dto;

public class AgentMasterDTO {
	private Object originAgentId;
	private Object originAgent;
	private Object originAgentActivity;
	private Object originAgentFeedback;
	private Object originPort;
	private Object originCost;
	
	private Object freightCost;
	private Object freightCarrier;
	private Object freightId;
	private Object refFreightId;
	
	private Object destinationAgentId;
	private Object destinationAgent;
	private Object destinationAgentActivity;
	private Object destinationAgentFeedback;
	private Object destinationPort;
	private Object destinationCost;
	
	private Object haulingDistance;
	private Object addressDistance;
	
	private Object totalCost;
	private Object originSelection;
	private Object destinationSeletion;
	
	private Object discountvalue;
	private Object netCost;
	
	private Object originPartnerId;
	private Object destinationPartnerId;

	public Object getDestinationAgent() {
		return destinationAgent;
	}

	public void setDestinationAgent(Object destinationAgent) {
		this.destinationAgent = destinationAgent;
	}

	public Object getDestinationAgentActivity() {
		return destinationAgentActivity;
	}

	public void setDestinationAgentActivity(Object destinationAgentActivity) {
		this.destinationAgentActivity = destinationAgentActivity;
	}

	public Object getDestinationAgentFeedback() {
		return destinationAgentFeedback;
	}

	public void setDestinationAgentFeedback(Object destinationAgentFeedback) {
		this.destinationAgentFeedback = destinationAgentFeedback;
	}

	public Object getDestinationAgentId() {
		return destinationAgentId;
	}

	public void setDestinationAgentId(Object destinationAgentId) {
		this.destinationAgentId = destinationAgentId;
	}

	public Object getDestinationCost() {
		return destinationCost;
	}

	public void setDestinationCost(Object destinationCost) {
		this.destinationCost = destinationCost;
	}

	public Object getDestinationPort() {
		return destinationPort;
	}

	public void setDestinationPort(Object destinationPort) {
		this.destinationPort = destinationPort;
	}

	public Object getFreightCarrier() {
		return freightCarrier;
	}

	public void setFreightCarrier(Object freightCarrier) {
		this.freightCarrier = freightCarrier;
	}

	public Object getFreightCost() {
		return freightCost;
	}

	public void setFreightCost(Object freightCost) {
		this.freightCost = freightCost;
	}

	public Object getOriginAgent() {
		return originAgent;
	}

	public void setOriginAgent(Object originAgent) {
		this.originAgent = originAgent;
	}

	public Object getOriginAgentActivity() {
		return originAgentActivity;
	}

	public void setOriginAgentActivity(Object originAgentActivity) {
		this.originAgentActivity = originAgentActivity;
	}

	public Object getOriginAgentFeedback() {
		return originAgentFeedback;
	}

	public void setOriginAgentFeedback(Object originAgentFeedback) {
		this.originAgentFeedback = originAgentFeedback;
	}

	public Object getOriginAgentId() {
		return originAgentId;
	}

	public void setOriginAgentId(Object originAgentId) {
		this.originAgentId = originAgentId;
	}

	public Object getOriginCost() {
		return originCost;
	}

	public void setOriginCost(Object originCost) {
		this.originCost = originCost;
	}

	public Object getOriginPort() {
		return originPort;
	}

	public void setOriginPort(Object originPort) {
		this.originPort = originPort;
	}

	public Object getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Object totalCost) {
		this.totalCost = totalCost;
	}

	public Object getFreightId() {
		return freightId;
	}

	public void setFreightId(Object freightId) {
		this.freightId = freightId;
	}

	public Object getAddressDistance() {
		return addressDistance;
	}

	public void setAddressDistance(Object addressDistance) {
		this.addressDistance = addressDistance;
	}

	public Object getHaulingDistance() {
		return haulingDistance;
	}

	public void setHaulingDistance(Object haulingDistance) {
		this.haulingDistance = haulingDistance;
	}

	public Object getDestinationSeletion() {
		return destinationSeletion;
	}

	public void setDestinationSeletion(Object destinationSeletion) {
		this.destinationSeletion = destinationSeletion;
	}

	public Object getOriginSelection() {
		return originSelection;
	}

	public void setOriginSelection(Object originSelection) {
		this.originSelection = originSelection;
	}

	public Object getDiscountvalue() {
		return discountvalue;
	}

	public void setDiscountvalue(Object discountvalue) {
		this.discountvalue = discountvalue;
	}

	public Object getNetCost() {
		return netCost;
	}

	public void setNetCost(Object netCost) {
		this.netCost = netCost;
	}

	public Object getDestinationPartnerId() {
		return destinationPartnerId;
	}

	public void setDestinationPartnerId(Object destinationPartnerId) {
		this.destinationPartnerId = destinationPartnerId;
	}

	public Object getOriginPartnerId() {
		return originPartnerId;
	}

	public void setOriginPartnerId(Object originPartnerId) {
		this.originPartnerId = originPartnerId;
	}

	public Object getRefFreightId() {
		return refFreightId;
	}

	public void setRefFreightId(Object refFreightId) {
		this.refFreightId = refFreightId;
	}

	
	
}
