package com.trilasoft.app.webapp.action;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable; 
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.Quotation;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerQuoteManager;
import com.trilasoft.app.service.QuotationManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;

import org.apache.log4j.Logger;

public class PartnerQuoteAction extends BaseAction implements Preparable {   

	private PartnerQuoteManager partnerQuoteManager;
    private List partnerQuotes; 
    private List partnerQuotesSubmitted;
    private PartnerQuote partnerQuote;   
    private Long id;
    private RefMasterManager refMasterManager;
    private String shipnumber;
    private String mailtovendor;
    private Long i;
    private String quoteShip;
    private Long autoShip;
    private List maxShip;
    private List maxQuoteShip;
    private CustomerFile customerFile;
    private CustomerFileManager customerFileManager;
	private List customerFiles;
	private Set partnerQuotess;
	private Set partnerQuotessSubmitted;
	
	private List serviceOrders;
	private List maxSequenceNumber;
    private List checkSequenceNumberList;
    private Long autoSequenceNumber;
	private ServiceOrderManager serviceOrderManager;
    private ServiceOrder serviceOrder;
	private List dupPartnerCodeList;
	private List dupVendorEmail;
	private Boolean checkVendorEmail;
	private String ctrSeqNumber;
	private List serviceOrdersList;
	private List serviceOrderId; 
	 // By Vijay
	 
	 private String reqSO;
	 private String shipNum;
	 private String requestedSRVO;
	 private boolean general;
	 private Partner partner;
	 private List partners;
	 private PartnerManager partnerManager;
	 private QuotationManager quotationManager;
	 private Quotation quotations;
	 
	 private static Map<String, String> tcktservc;
	 private static Map<String, String> quoteTypes;
	 
	 private String sessionCorpID;
	private List<Quotation> quotationls;
	private String userType;
	private Company company;
	private CompanyManager companyManager;
	private String oiJobList;
	
	  // private List day;
	    
	  

		public PartnerQuoteAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
	        this.userType= user.getUserType();
	    }
		static final Logger logger = Logger.getLogger(PartnerQuoteAction.class);	
		Date currentdate = new Date();
		private ErrorLogManager errorLogManager;	
		
		public void prepare() throws Exception {
		try{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			company=companyManager.findByCorpID(sessionCorpID).get(0);
	        if(company!=null && company.getOiJob()!=null){
					oiJobList=company.getOiJob();  
				  }
		}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}	
		}
	    
    public Set getPartnerQuotess() {
		return partnerQuotess;
	}
    public Set getPartnerQuotessSubmitted() {
		return partnerQuotessSubmitted;
	}

	public void setPartnerQuoteManager(PartnerQuoteManager partnerQuoteManager) {
        this.partnerQuoteManager = partnerQuoteManager;
    }
	
	public void setPartnerManager(PartnerManager partnerManager) {
        this.partnerManager = partnerManager;
    }
	
    public void setCustomerFileManager(CustomerFileManager customerFileManager) {
        this.customerFileManager = customerFileManager;
    }
    public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    } 
    public List getServiceOrders(){
    	return serviceOrders;
    }
    public List getPartnerQuotes() {   
        return partnerQuotes;   
    }
    public List getPartners() {   
        return partners;   
    }
    public List getPartnerQuotesSubmitted() {   
        return partnerQuotesSubmitted;   
    }
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }

    
    public void setId(Long id) {   
        this.id = id;   
    }
    public void setMailtovendor(String mailtovendor) {   
        this.mailtovendor = mailtovendor;   
    } 
 
    public void setRequestedSRVO(String requestedSRVO) {   
        this.requestedSRVO = requestedSRVO;   
    }
    
    public List getCustomerFiles() {   
        return customerFiles;   
    } 
    
    public CustomerFile getCustomerFile() {   
        return customerFile;   
    }   
      
    public void setCustomerFile(CustomerFile customerFile) {   
        this.customerFile = customerFile;   
    }   
    
    public PartnerQuote getPartnerQuote() {   
        return partnerQuote;   
    }   
      
    public void setPartnerQuote(PartnerQuote partnerQuote) {   
        this.partnerQuote = partnerQuote;   
    }   
    
    public Partner getPartner() {   
        return partner;   
    }   
      
    public void setPartner(Partner partner) {   
        this.partner = partner;   
    }   
    
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    }   
      
    public void setServiceOrder(ServiceOrder serviceOrder) {   
        this.serviceOrder = serviceOrder;   
    }
    
    public String getComboList(String corpId, String jobType){
  	   	tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
  	   	quoteTypes = refMasterManager.findByParameter(corpId,"QUOTETYPE");
  	 	 return SUCCESS;
     }
    public  Map<String, String> getTcktservc() {
		return tcktservc;
	}
    public String delete() {   
        partnerQuoteManager.remove(partnerQuote.getId());   
        saveMessage(getText("partnerQuote.deleted"));   
      
        return listPartnerQuotes();   
    }   
    public String edit() {   
        if (id != null) { 
        	partnerQuote = partnerQuoteManager.get(id);
            customerFile = partnerQuote.getCustomerFile();
        } else {   
            partnerQuote = new PartnerQuote();
            partnerQuote.setCorpID(sessionCorpID);
            partnerQuote.setCreatedOn(new Date());
            partnerQuote.setUpdatedOn(new Date());
            
        }   
        getComboList(sessionCorpID,"");
        return SUCCESS;   
    }   
      
    public synchronized String save() throws Exception {   
    	 if (cancel != null) { 
             return "cancel"; 
         } 
      
         if (delete != null) {
             return delete(); 
         }
         dupVendorEmail = partnerQuoteManager.findByVendorEmail(partnerQuote.getVendorMail(),customerFile.getSequenceNumber());
         if(dupVendorEmail.isEmpty()){
         	checkVendorEmail=true;
         }else {
         	checkVendorEmail=false;
         }
         if(!checkVendorEmail){
        	 errorMessage(getText("Two partner with same email can not be added, Kindly use other email"));
         	return ERROR;
         }
    	partnerQuote.setCustomerFile(customerFile);
        boolean isNew = (partnerQuote.getId() == null);
        dupPartnerCodeList = partnerQuoteManager.findPartnerCode(partnerQuote.getQuote());
       
        if(!(dupPartnerCodeList.isEmpty()) && isNew){
        	
        	 return SUCCESS;   
        }
        
//      @@@@@@@@@@@@@  For Partner Portal By Rajesh @@@@@@@@@@@@@@@@@@@@@@@@@@@@
        partnerQuote.setCorpID(sessionCorpID);
        if(isNew){
        	partnerQuote.setCreatedOn(new Date());
        }
        partnerQuote.setUpdatedOn(new Date());
        partnerQuoteManager.save(partnerQuote);   
        //System.out.println(customerFile);
        String key = (isNew) ? "partnerQuote.added" : "partnerQuote.updated";   
        saveMessage(getText(key));
        getComboList(sessionCorpID,"");
        if (!isNew) {   
            return INPUT;   
        } else {   
        	i = Long.parseLong(partnerQuoteManager.findMaximumId().get(0).toString());
            //System.out.println(i);
                        partnerQuote = partnerQuoteManager.get(i);
                        customerFile = partnerQuote.getCustomerFile();
                        serviceOrders = new ArrayList( customerFile.getServiceOrders() );
                        partnerQuotes = new ArrayList( customerFile.getPartnerQuotes() );
            
            return SUCCESS;   
        } 
        
    }
    @SkipValidation 
    public synchronized String editNewPartnerQuote() {   
        if (id != null) {   
            customerFile = customerFileManager.get(id);
            maxQuoteShip = customerFileManager.findMaximumShip12(customerFile.getSequenceNumber());
            ///System.out.println(maxQuoteShip);
            if ( maxQuoteShip.get(0) == null ) {          
            	quoteShip = "01";
            }else {
	            autoShip = Long.parseLong((maxQuoteShip).get(0).toString()) + 1;
	            //System.out.println(autoShip);
	            if((autoShip.toString()).length() == 1) {
	            	quoteShip = "0"+(autoShip.toString());
	            }else {
	            	quoteShip=autoShip.toString();
	            }
            }
        
	        shipnumber = customerFile.getSequenceNumber() + "Q" +quoteShip;
	        partnerQuote = new PartnerQuote();
	        partnerQuote.setCorpID(sessionCorpID);
	        partnerQuote.setShip(quoteShip);
	        partnerQuote.setQuote(shipnumber);
	        partnerQuote.setQuoteStatus("New");
	        partnerQuote.setOriginCity(customerFile.getOriginCity());
	        partnerQuote.setDestinationCity(customerFile.getDestinationCity());
	        partnerQuote.setUpdatedOn(new Date());
	        partnerQuote.setCreatedOn(new Date());
        }  
        getComboList(sessionCorpID,"");
        return SUCCESS;   
    }       
      
    @SkipValidation
	public synchronized String editAfterSubmitPartnerType() throws Exception {
		//System.out.println(save);
		if ( save != null ) {
			return save();
		}
		getComboList(sessionCorpID,"");
		return INPUT;
	
	}
    @SkipValidation
	public synchronized String editAfterSubmitPartnerTypeedit() throws Exception {
		//System.out.println(save);
		if ( save != null ) {
			return edit();
		}
		getComboList(sessionCorpID,"");
		return SUCCESS;
	
	}
    @SkipValidation
public String updatestatus() throws Exception {   
  	  String[] result = mailtovendor.split("\\,");
  	//System.out.println(requestedSRVO);
  	  	if(requestedSRVO == null){
  			reqSO = "";
	  	}else{
	  		reqSO = requestedSRVO;
	  	}
  		//System.out.println(id);
  	   int i;
  	     for (int x=0; x<result.length; x++)
  	     {
  	    	 //System.out.println(reqSO);
  	    	i = partnerQuoteManager.updateQuoteStatus(result[x],reqSO);
  	     }
  	        
  	   
  	customerFile = customerFileManager.get(id); 
  	serviceOrders = new ArrayList( customerFile.getServiceOrders() );
   	partnerQuotes = new ArrayList( customerFile.getPartnerQuotes() ); 	
  	
   	getComboList(sessionCorpID,"");
  	return SUCCESS;    
  }
   @SkipValidation
public String updatereminder() throws Exception {   
	  String[] result = mailtovendor.split("\\,");
	 // System.out.println(id);
	   int i;
	     for (int x=0; x<result.length; x++)
	     {
	    	 //System.out.println(result[x]);
	    	i = partnerQuoteManager.updateQuoteReminder(result[x]);
	     }
	        
	   
	   customerFile = customerFileManager.get(id); 
	   serviceOrders = new ArrayList( customerFile.getServiceOrders() );
	   partnerQuotes = new ArrayList( customerFile.getPartnerQuotes() ); 	
	
	   getComboList(sessionCorpID,"");
	   return SUCCESS;    
}  
    
    public String listPartnerQuotes() {  
    	partnerQuotes = partnerQuoteManager.getAll();
    	return SUCCESS;   
    }
    
    
//	############## For Portal ##########################################
    @SkipValidation
    public String portalList() {  
    	partnerQuotes= partnerQuoteManager.getAll();
    	String vendorCodeSet="";
    	if(userType.equalsIgnoreCase("AGENT")){
    		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = (User)auth.getPrincipal();
            Map filterMap=user.getFilterMap(); 
        	List userList=new ArrayList();
        	try {
        		List partnerCorpIDList = new ArrayList(); 
        		List list2 = new ArrayList();
        		if (filterMap!=null && (!(filterMap.isEmpty()))) {
        		if(filterMap.containsKey("agentFilter")){
			    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
			    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
			    	Iterator iterator = valueSet.iterator();
			    	while (iterator.hasNext()) {
			    		String  mapkey = (String)iterator.next(); 
					    if(filterMap.containsKey(mapkey)){	
						HashMap valueMap=	(HashMap)filterMap.get(mapkey);
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
							List partnerCorpIDList1 = new ArrayList();
							partnerCorpIDList1= (List)entry.getValue();
							Iterator listIterator= partnerCorpIDList1.iterator();
					    	while (listIterator.hasNext()) {
					    		String value=listIterator.next().toString();
					    		if(value!=null && ((value.trim().equalsIgnoreCase("--NO-FILTER--")))){
					    		partnerCorpIDList.add("'" + listIterator.next().toString() + "'");
					    		}
					    	}
							
							
							}
						}
						}
						}
			    	 }
			    	 Iterator listIterator= partnerCorpIDList.iterator();
				    	while (listIterator.hasNext()) {
				    		list2.add("'" + listIterator.next().toString() + "'");	
				    	}
				    	vendorCodeSet= list2.toString().replace("[","").replace("]", "");
				    	}
			    	 }
        	
        	}catch(Exception e){
        		e.printStackTrace();
        	}
    	}
    	List ls=partnerQuoteManager.findByStatus("New",sessionCorpID,vendorCodeSet);
    	partnerQuotess = new HashSet(ls);
    	return SUCCESS;   
    }
    //==============================================For AccountLine by Raj=================
    @SkipValidation
    public String accountLineVendorPopup() {  
    	if(!general){
	    	partnerQuotes= partnerQuoteManager.getAll();
	    	List ls=partnerQuoteManager.findByStatusSubmitted("Submitted",searchByCode,searchByName,shipNum);
	    	//System.out.println("\n\nQuote Status SinghRaj 2\n\n"+partnerQuoteManager.findByStatusSubmitted("Submitted", searchByCode, searchByName, shipNum));
	    	partnerQuotess = new HashSet(ls);
	    	if(partnerQuotess.isEmpty()){
	    		partners = partnerQuoteManager.getGeneralListVendor(searchByCode,searchByName,searchByCountryCode,searchByState,searchByCity);
	    		general = true;
	    	}
	    	
    	}
    	return SUCCESS;   
    }
    @SkipValidation
    public String accountLineVendor() {  
    	if(!general){
	    	partnerQuotes= partnerQuoteManager.getAll();
	    	List ls=partnerQuoteManager.findByStatusSubmitted("Submitted",searchByCode,searchByName,shipNumber);
	    	partnerQuotess = new HashSet(ls);
	   }else{
    		partners = partnerQuoteManager.getGeneralListVendor(searchByCode,searchByName,searchByCountryCode,searchByState,searchByCity);
    	}
    	return SUCCESS;   
    }
    @SkipValidation
    public String portalReferenceList() {  
    	partnerQuote=partnerQuoteManager.get(id);
    	customerFile=partnerQuote.getCustomerFile();
    	serviceOrders=partnerQuoteManager.findServiceOrders(partnerQuote.getRequestedSO(),sessionCorpID);
    	return SUCCESS;   
    } 
//	 Method to get list of S/O which have proper vaule for Rate Request related to customerfile.
	@SkipValidation
	public String findRateReqSo()
	{
		serviceOrdersList=partnerQuoteManager.findSoList(ctrSeqNumber,sessionCorpID);
		Iterator iterator=serviceOrdersList.iterator();
		while(iterator.hasNext())
		{
		String soNumber =(String)iterator.next();
		 serviceOrderId=partnerQuoteManager.findSoDetail(soNumber,sessionCorpID); 
		} 
		return SUCCESS;	
	}
    
    private Long sid;
    private Long cid;
	// for Quotation Management according to quoteType
    private String quoteType;
    private String searchByCode;
    private String searchByName;
    private String shipNumber;
    private String searchByCountryCode;
    private String searchByState;
    private String searchByCity;
    private String totalPrice;
    
    
    
    public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSearchByCode() {
		return searchByCode;
	}

	public void setSearchByCode(String searchByCode) {
		this.searchByCode = searchByCode;
	}

	public String getSearchByName() {
		return searchByName;
	}

	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}
	public String getSearchByCountryCode() {
		return searchByCountryCode;
	}

	public void setSearchByCountryCode(String searchByCountryCode) {
		this.searchByCountryCode = searchByCountryCode;
	}

	public String getSearchByState() {
		return searchByState;
	}

	public void setSearchByState(String searchByState) {
		this.searchByState = searchByState;
	}
	public String getSearchByCity() {
		return searchByCity;
	}

	public void setSearchByCity(String searchByCity) {
		this.searchByCity = searchByCity;
	}
	public String getQuoteType() {
		return quoteType;
	}

	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
// -----------------------------------------------
	public Long getCid() {
		return cid;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	
	
	public boolean isGeneral() {
		return general;
	}

	public void setGeneral(boolean general) {
		this.general = general;
	}

	@SkipValidation
	public String  portalReferenceDetailsList() {  
    	//System.out.println("\n\nReference Details Page opens\n\n"); 
    	
    	customerFile=customerFileManager.get(cid);
    	serviceOrder=serviceOrderManager.get(sid);
    	partnerQuote=partnerQuoteManager.get(id);
    	
    	//System.out.println("\n\nReference Deatils Page close\n\n"); 
    	return SUCCESS;   
    }
//	###############################################################################
	
	
	
	
// 	@@@@@@@@@@@@@@@@@@@@@ By Madhu - For Quation Management @@@@@@@@@@@@@@@@@@@@@@@@
	@SkipValidation
	public String  quotationList() {
	partnerQuotes = partnerQuoteManager.findBySequenceNumber("9927110");// findByQuoteType("Origin", "9927110");
	serviceOrder=serviceOrderManager.get(sid);
		return SUCCESS;
	}
	
	
	
    @SkipValidation
    public String quotationVendor() {  
    	serviceOrder=serviceOrderManager.get(sid);
    	customerFile=serviceOrder.getCustomerFile();
    	getSession().setAttribute("serviceOrder",serviceOrder );
    	//System.out.println("\n\n CustomerRecord through ServiceOrder is :\t"+customerFile);
    	//System.out.println("\n\n quoteType is :\t"+quoteType);
    	//partnerQuotes
    	List ls=partnerQuoteManager.findByQuoteType(quoteType,customerFile.getSequenceNumber());
    		// partnerQuoteManager.getAll();
    	//List ls = partnerQuoteManager.getAll();
    	partnerQuotess = new HashSet(ls);
    	//partnerQuotes = partnerQuoteManager.getAll();
    	return SUCCESS;   
    }

	public Quotation getQuotations() {
		return quotations;
	}

	public void setQuotations(Quotation quotations) {
		this.quotations = quotations;
	}

	public void setQuotationManager(QuotationManager quotationManager) {
		this.quotationManager = quotationManager;
	}

	public List<Quotation> getQuotationls() {
		return quotationls;
	}

	public void setQuotationls(List<Quotation> quotationls) {
		this.quotationls = quotationls;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getShipNum() {
		return shipNum;
	}

	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}
	
	private String partnerCode;
	private List accountLineList;
	
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	
	
	public List getAccountLineList() {
		return accountLineList;
	}

	public void setAccountLineList(List accountLineList) {
		this.accountLineList = accountLineList;
	}

	@SkipValidation
	public String vendorNamenEmail()
	{   
		if(partnerCode!=null)
        {
        	accountLineList=partnerQuoteManager.vendorNamenEmail(partnerCode);
        }
        else
        {
        	String message="The Vendor Name is not exist";
        	errorMessage(getText(message));
        }
		return SUCCESS; 
	}

	public static Map<String, String> getQuoteTypes() {
		return quoteTypes;
	}

	public static void setQuoteTypes(Map<String, String> quoteTypes) {
		PartnerQuoteAction.quoteTypes = quoteTypes;
	}

	public String getCtrSeqNumber() {
		return ctrSeqNumber;
	}

	public void setCtrSeqNumber(String ctrSeqNumber) {
		this.ctrSeqNumber = ctrSeqNumber;
	}

	public List getServiceOrdersList() {
		return serviceOrdersList;
	}

	public void setServiceOrdersList(List serviceOrdersList) {
		this.serviceOrdersList = serviceOrdersList;
	}

	public List getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(List serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	  public String getUserType() {
			return userType;
		}

		public void setUserType(String userType) {
			this.userType = userType;
		}

		 
        public void setCompanyManager(CompanyManager companyManager) {
			this.companyManager = companyManager;
		}

		public String getOiJobList() {
			return oiJobList;
		}

		public void setOiJobList(String oiJobList) {
			this.oiJobList = oiJobList;
		}

	
		public void setErrorLogManager(ErrorLogManager errorLogManager) {
			this.errorLogManager = errorLogManager;
		}
	
//	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	
}  

