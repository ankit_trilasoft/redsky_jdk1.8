package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.NewsUpdate;
import com.trilasoft.app.service.NewsUpdateManager;
import com.trilasoft.app.service.RefMasterManager;

public class NewsUpdateAction extends BaseAction {
    /**
	 * 
	 */
	//private static final long serialVersionUID = 3631502569872807639L;
	private NewsUpdateManager newsUpdateManager;
	private RefMasterManager refMasterManager;
    private List newsUpdates;
    private List distinctCorpId;
    private List ls;
    private String hitFlag;
    private NewsUpdate newsUpdate;
    private NewsUpdate addNewsUpdateForm;
    private Long id;
    private String corpId;
    private String sessionCorpID;
    private String functionality;
    private String details;
    private String module;
    private String retention;
    private String location;
    private Boolean visible ;
    private Date modifyOn;
    private String createdBy;
    private String modifyBy;
    private Date effectiveDate;
    private Date endDisplayDate;
    private Date publishedDate;
    private Date createdOn;
    private NewsUpdate addCopyNewsUpdate;
    private Long cid;
    private String fileLocation;
    private String fileFileName;
    private File file;
    private String location1;
    private String fileFileName1;
    private String urlValue;
    private String ticketNo;
	private List selectedCorpIdList;
    private Boolean status ;
    private String category;
    private Boolean activeStatus=true;
    private Map<String, String> categoryReport;
    private Map<String, String> moduleReport;
    //Added For #7588
    private UserManager userManager;
    private User user;
    private String updateFlag ; 
    private Boolean addValues = false;
   // String returnFromSaveMethod;
    Date currentdate = new Date();
    
    static final Logger logger = Logger.getLogger(NewsUpdateAction.class);
       public NewsUpdateAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
    
    public String getComboList(String corpId) {
    	moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE");
    	categoryReport=refMasterManager.findByParameter(sessionCorpID, "CATEGORY");
    	distinctCorpId=newsUpdateManager.findDistinct();
    	  	
    	return SUCCESS; 
    	    }
    public String list() {
    	getComboList(sessionCorpID);
		if(addValues== null || addValues== false){
			addValues=false;
		}
	   	if(sessionCorpID.equalsIgnoreCase("TSFT")){
	   		newsUpdates = newsUpdateManager.getAll();
    	}else{
    		newsUpdates = newsUpdateManager.findListOnCorpId(sessionCorpID,addValues);
    	}
	   	   	
	   	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         user = (User)auth.getPrincipal();
         userManager.updateUserNewsUpdateFlag(sessionCorpID, false, user.getUsername());
         user = userManager.getUserByUsername(user.getUsername());
         getRequest().getSession().setAttribute("newsUpdateFlag", user.isNewsUpdateFlag());
         List newsUpdatList = newsUpdateManager.findUpdatedDate(sessionCorpID);
 		//getRequest().getSession().setAttribute("imageSelected", "Updated");
 		//imageSelected="Updated";
 		//getRequest().getSession().setAttribute("imageSelected", "Normal");
 		//imageSelected="Normal";
 		if (newsUpdatList.isEmpty()) {

 			getRequest().getSession().setAttribute("imageSelected", "Normal");
 			//imageSelected = "Normal";
 		} else {
 			getRequest().getSession().setAttribute("imageSelected", "Updated");
 			//imageSelected = "Updated";
 		}
	   	
    	return SUCCESS;
    }
    
    public String distinctCorpId(){
    distinctCorpId=newsUpdateManager.findDistinct();
	//System.out.println("\n\n\n\n\n\n\n\n\n\n distinct corpId"+distinctCorpId);
	return SUCCESS;
}
    @SkipValidation
    public String deleteUpdateDoc() {
    	 	 newsUpdateManager.remove(id);
    	 	 urlValue="true&corpID=${sessionCorpID}";
    	   list();
    	 return SUCCESS;
    	}
    public String delete() {
    	newsUpdateManager.remove(newsUpdate.getId());
        saveMessage(getText("newsUpdate.deleted"));
        moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE");
    	categoryReport=refMasterManager.findByParameter(sessionCorpID, "CATEGORY");
        return SUCCESS;
    }

    public String edit() {
    	distinctCorpId();
        if (id != null) {
        	newsUpdate = newsUpdateManager.get(id);
        	fileLocation = newsUpdate.getLocation();
        } else {
        	newsUpdate = new NewsUpdate();
        	newsUpdate .setCreatedOn(new Date());
        	newsUpdate .setModifyOn(new Date());
        	
        }
        moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE");
    	categoryReport=refMasterManager.findByParameter(sessionCorpID, "CATEGORY");
        return SUCCESS;
    }


    public String save() throws Exception {
    	 
        if (cancel != null) {
            return "cancel";
        }

        if (delete != null) {
            return delete();
        }
        distinctCorpId();
        moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE");
    	categoryReport=refMasterManager.findByParameter(sessionCorpID, "CATEGORY");

        boolean isNew = (newsUpdate.getId() == null);
        if(isNew){
        	newsUpdate.setCreatedOn(new Date());
        	
        }
        String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "newsUpdate" +  "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
				Long kid;
				//if(newsUpdateManager.getMaxId().get(0) == null)
				{
					kid = 1L;
				}
				/*else{
					//kid = Long.parseLong(newsUpdateManager.getMaxId().get(0).toString());
					//kid = kid + 1;
				}*/
				try{					
				InputStream stream = new FileInputStream(file);

				
				//write the file to the file specified
				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				bos.close();
				stream.close();
             	InputStream copyStream = new FileInputStream(file);

				OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];

				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}

				boscopy.close();
				copyStream.close();
				getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
		
		//newsUpdate.setFile(file);
		//FileSizeUtil fileSizeUtil = new FileSizeUtil();
		//newsUpdate.setFileSize(fileSizeUtil.FindFileSize(file));
		//newsUpdate.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName));
		
		newsUpdate.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
		//newsUpdate.setFileType("");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
		if(!fileFileName.equals(""))
					{
						newsUpdate.setFileName(newsUpdate.getFileName());
							newsUpdate.setFileName(fileFileName);
					}
				else{
					newsUpdate.setFileName(fileFileName1);
					newsUpdate.setLocation(location1);
				}
		newsUpdate.setModifyOn(new Date());
        newsUpdate = newsUpdateManager.save(newsUpdate);
        String key = (isNew) ? "newsUpdate.added" : "newsUpdate.updated";
        saveMessage(getText(key));
        if(newsUpdate.getVisible()!=null && newsUpdate.getVisible()){
    	//userManager.updateUserNewsUpdateFlag(newsUpdate.getCorpId(), true,"");
        }
    	//returnFromSaveMethod="true";
        list();
        if (!isNew) {
            return INPUT;
        } else {
            return SUCCESS;
        }
    }
@SkipValidation
    public String addAndCopyNewsUpdateForm() throws Exception {
		if (id == null) {
					getComboList(sessionCorpID);
					//newsUpdate = newsUpdateManager.get(id);
					newsUpdate = new NewsUpdate();
					addCopyNewsUpdate = (NewsUpdate) newsUpdateManager.get(cid);
					//newsUpdate.setCorpId(addCopyNewsUpdate.getCorpId());
					newsUpdate.setDetails(addCopyNewsUpdate.getDetails());
					newsUpdate.setFunctionality(addCopyNewsUpdate.getFunctionality());
					//newsUpdate.setId(addCopyNewsUpdate.getId());
					newsUpdate.setFileName(addCopyNewsUpdate.getFileName());
					newsUpdate.setLocation(addCopyNewsUpdate.getLocation());
					//newsUpdate.setVisible(addCopyNewsUpdate.getVisible());
					newsUpdate.setRetention(addCopyNewsUpdate.getRetention());
					newsUpdate.setModule(addCopyNewsUpdate.getModule());
					newsUpdate.setCreatedBy(addCopyNewsUpdate.getCreatedBy());
					newsUpdate.setCreatedOn(new Date());
					newsUpdate.setEffectiveDate(addCopyNewsUpdate.getEffectiveDate());
					newsUpdate.setEndDisplayDate(addCopyNewsUpdate.getEndDisplayDate());
					newsUpdate.setModifyBy(addCopyNewsUpdate.getModifyBy());
					newsUpdate.setModifyOn(new Date());
					newsUpdate.setPublishedDate(addCopyNewsUpdate.getPublishedDate());
					newsUpdate.setFileName(addCopyNewsUpdate.getFileName());
					newsUpdate.setTicketNo(addCopyNewsUpdate.getTicketNo());
					newsUpdate.setCategory(addCopyNewsUpdate.getCategory());
					//newsUpdateManager.save(newsUpdate);
					distinctCorpId();
					list();
				//save();
					
		}
		return SUCCESS;
    }
    
//  Method to search module
    @SkipValidation
    public String searchNewsUpdate() {
    	getComboList(sessionCorpID);
		if("TSFT".equalsIgnoreCase(sessionCorpID)){
			String condition="";
			if(activeStatus){
				condition="and visible is true";
				newsUpdates = newsUpdateManager.findListByModuleCorp(newsUpdate.getModule(),newsUpdate.getCorpId(),newsUpdate.getPublishedDate(),condition,activeStatus);
			}else{
				condition="and visible is false";
				newsUpdates = newsUpdateManager.findListByModuleCorp(newsUpdate.getModule(),newsUpdate.getCorpId(),newsUpdate.getPublishedDate(),condition,activeStatus);
			}	
		}else{
			String condition = "and visible=true";
			if(!addValues){
				condition = condition+" and datediff(sysdate(),effectiveDate) >=0 and datediff(sysdate(),endDisplayDate) <=0";
			}
			newsUpdates = newsUpdateManager.findListByModuleCorp(newsUpdate.getModule(),sessionCorpID,newsUpdate.getPublishedDate(),condition,activeStatus);
		}
 		return SUCCESS;
	}
    private String newsUpdateDesc;
	@SkipValidation
	public String findToolTipDescriptionForNewsUpdate(){		
		newsUpdateDesc = newsUpdateManager.findNewsUpdateDescForToolTip(id,sessionCorpID);	
		return SUCCESS;
	}
	@SkipValidation
	public String importXlsDataPage() {

		return SUCCESS;
	}
	@SkipValidation
	public String importXlsData() {
		int record = 0;

		hitFlag = "0";
		logger.warn("USER AUDIT: ID: " + currentdate + " ## User: "
				+ getRequest().getRemoteUser() + " Start");

		try {
			String userName = getRequest().getRemoteUser();
			List cellDataList = new ArrayList();

			FileInputStream fileInputStream = new FileInputStream(file);

			POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputStream);

			HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
			HSSFSheet hssfSheet = workBook.getSheetAt(0);

			Iterator rowIterator = hssfSheet.rowIterator();

			while (rowIterator.hasNext()) {
				HSSFRow hssfRow = (HSSFRow) rowIterator.next();
				Iterator iterator = hssfRow.cellIterator();
				List cellTempList = new ArrayList();
				while (iterator.hasNext()) {
					HSSFCell hssfCell = (HSSFCell) iterator.next();
					cellTempList.add(hssfCell);

				}
				cellDataList.add(cellTempList);

			}
			for (int i = 1; i < cellDataList.size(); i++) {
				List cellTempList = (List) cellDataList.get(i);
				NewsUpdate newsUpdate1 = new NewsUpdate();
				newsUpdate1.setCreatedBy(userName);
				newsUpdate1.setCreatedOn(new Date());
				newsUpdate1.setModule(cellTempList.get(0).toString());
				newsUpdate1.setCategory(cellTempList.get(1).toString());
				newsUpdate1.setFunctionality(cellTempList.get(2).toString());
				newsUpdate1.setDetails(cellTempList.get(3).toString());
				String[] result = Pattern.compile("[.]+").split(cellTempList.get(4).toString());
			    newsUpdate1.setTicketNo(result[0]);
				newsUpdate1.setCorpId(cellTempList.get(5).toString());

				newsUpdate1.setPublishedDate(newsUpdate.getPublishedDate());
				newsUpdate1.setRetention(newsUpdate.getRetention());
				newsUpdate1.setEffectiveDate(newsUpdate.getEffectiveDate());
				newsUpdate1.setEndDisplayDate(newsUpdate.getEndDisplayDate());
				newsUpdate1.setModifyBy(userName);
				newsUpdate1.setModifyOn(currentdate);
				if ((cellTempList.get(6).toString().equalsIgnoreCase("1.0"))
						|| (cellTempList.get(6).toString()
								.equalsIgnoreCase("confirmed"))) {
					// newsUpdate1.setStatus(true);
					newsUpdate1.setVisible(true);
				} else {
					newsUpdate1.setStatus(false);
				}
				if (cellTempList.get(7).toString().equalsIgnoreCase("1.0")) {
					newsUpdate1.setStatus(true);
					// newsUpdate1.setVisible(true);
				} else {
					newsUpdate1.setStatus(false);
				}
				NewsUpdate key = newsUpdateManager.save(newsUpdate1);
				if (key == null) {
					saveMessage("Invalid File");
				} else {
					saveMessage("saved successfully");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Call the printToConsole method to print the cell data in the console.
		 */
		// printToConsole(cellDataList);

		logger.warn("USER AUDIT: ID: " + currentdate + " ## User: "
				+ getRequest().getRemoteUser() + " End");
		hitFlag = "1";
		return SUCCESS;
	}
    
	public void setNewsUpdateManager(NewsUpdateManager newsUpdateManager) {
		this.newsUpdateManager = newsUpdateManager;
	}

	public List getNewsUpdates() {
		return newsUpdates;
	}

	public void setNewsUpdates(List newsUpdates) {
		this.newsUpdates = newsUpdates;
	}

	public NewsUpdate getNewsUpdate() {
		return newsUpdate;
	}

	public void setNewsUpdate(NewsUpdate newsUpdate) {
		this.newsUpdate = newsUpdate;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	/**
	 * @return the distinctCorpId
	 */
	public List getDistinctCorpId() {
		return distinctCorpId;
	}

	/**
	 * @param distinctCorpId the distinctCorpId to set
	 */
	public void setDistinctCorpId(List distinctCorpId) {
		this.distinctCorpId = distinctCorpId;
	}

	/**
	 * @return the corpId
	 */
	public String getCorpId() {
		return corpId;
	}

	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	public Map<String, String> getModuleReport() {
		return moduleReport;
	}
	public void setModuleReport(Map<String, String> moduleReport) {
		this.moduleReport = moduleReport;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getFunctionality() {
		return functionality;
	}

	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getRetention() {
		return retention;
	}

	public void setRetention(String retention) {
		this.retention = retention;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Date getModifyOn() {
		return modifyOn;
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getEndDisplayDate() {
		return endDisplayDate;
	}

	public void setEndDisplayDate(Date endDisplayDate) {
		this.endDisplayDate = endDisplayDate;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public NewsUpdate getAddCopyNewsUpdate() {
		return addCopyNewsUpdate;
	}

	public void setAddCopyNewsUpdate(NewsUpdate addCopyNewsUpdate) {
		this.addCopyNewsUpdate = addCopyNewsUpdate;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

		public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getLocation1() {
		return location1;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public String getFileFileName1() {
		return fileFileName1;
	}

	public void setFileFileName1(String fileFileName1) {
		this.fileFileName1 = fileFileName1;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getNewsUpdateDesc() {
		return newsUpdateDesc;
	}

	public void setNewsUpdateDesc(String newsUpdateDesc) {
		this.newsUpdateDesc = newsUpdateDesc;
	}

	public List getSelectedCorpIdList() {
		return selectedCorpIdList;
	}

	public void setSelectedCorpIdList(List selectedCorpIdList) {
		this.selectedCorpIdList = selectedCorpIdList;
	}

	public Boolean getStatus() {
		return status;
	}

	public String getCategory() {
		return category;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Map<String, String> getCategoryReport() {
		return categoryReport;
	}

	public void setCategoryReport(Map<String, String> categoryReport) {
		this.categoryReport = categoryReport;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Boolean getAddValues() {
		return addValues;
	}

	public void setAddValues(Boolean addValues) {
		this.addValues = addValues;
	}

	/*public String getReturnFromSaveMethod() {
		return returnFromSaveMethod;
	}

	public void setReturnFromSaveMethod(String returnFromSaveMethod) {
		this.returnFromSaveMethod = returnFromSaveMethod;
	}*/
	
}
