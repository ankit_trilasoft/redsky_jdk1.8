package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DsHomeRental;

public interface DsHomeRentalDao extends GenericDao<DsHomeRental, Long> {
	List getListById(Long id);
}
