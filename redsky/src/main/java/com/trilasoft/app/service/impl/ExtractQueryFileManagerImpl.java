package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ExtractQueryFileDao;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.service.ExtractQueryFileManager;

public class ExtractQueryFileManagerImpl  extends GenericManagerImpl<ExtractQueryFile, Long>   implements ExtractQueryFileManager{
	
	ExtractQueryFileDao extractQueryFileDao;
	public ExtractQueryFileManagerImpl(ExtractQueryFileDao extractQueryFileDao) {
		super(extractQueryFileDao);
		this.extractQueryFileDao=extractQueryFileDao; 
	} 
	public List oldQueries(String userId) {
		
		return extractQueryFileDao.oldQueries(userId);
	}

	public List fingQueryName(String remoteUser, String sessionCorpID, String queryName, String publicPrivateFlag) {
		
		return extractQueryFileDao.fingQueryName(remoteUser, sessionCorpID, queryName, publicPrivateFlag);
	}
	public List getSchedulerQueryList() {
		
		return extractQueryFileDao.getSchedulerQueryList();
	}
	public void updateSendMailStatus(Long id, String lastEmailSent, String emailStatus, String emailMessage) {
		extractQueryFileDao.updateSendMailStatus(id, lastEmailSent, emailStatus, emailMessage);
		
	}
	public List findDataExtractMailInfo(Long id) {
		return  extractQueryFileDao.findDataExtractMailInfo(id);
	} 
	public List searchExtractQueryFiles(String queryName,String publicPrivateFlag,String modifiedby,String userId){
		return  extractQueryFileDao.searchExtractQueryFiles(queryName, publicPrivateFlag, modifiedby, userId);
	}
	
	
}
