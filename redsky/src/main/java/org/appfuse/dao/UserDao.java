package org.appfuse.dao;

import java.util.Date;
import java.util.List;

import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.appfuse.model.User;

/**
 * User Data Access Object (Dao) interface.
 *
 * <p>
 * <a href="UserDao.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public interface UserDao extends Dao {
    /**
     * Gets users information based on user id.
     * @param userId the user's id
     * @return user populated user object
     */
    public User getUser(Long userId);

    /**
     * Gets users information based on login name.
     * @param username the user's username
     * @return userDetails populated userDetails object
     */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    
    /**
     * Gets a list of users based on parameters passed in.
     *
     * @return List populated list of users
     */
    public List getUsers(User user);

    /**
     * Saves a user's information
     * @param user the object to be saved
     */
    public void saveUser(User user);

    /**
     * Removes a user from the database by id
     * @param userId the user's id
     */
    public void removeUser(Long userId);
	public List findUserRoles(String corpID);
	public List findUserRoles(String corpID,String category);
	public List findUserPassword(String corpID);
	public List findUserDetails(String userName);
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName);
	public void findPwordHintQ(String pwdHintQues,String userName);
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName);
	public void removeUserPhotograph(String location,String userName);
	
	public List getDateSecuritySet();
	
	public List getPermissionList();
	
	public List getPermission(String permissionName);
	public List getSuperVisor(String corpID);
	
	public List getPartnerExpirationDate(String sessionCorpID, String username);
	public void updatePasswordExpiryDate(String username, String sessionCorpID, Date compareWithDate);
	public void setExpirePassword(String username, String sessionCorpID);
	public void updatePassword(String passwordNew, Long id);
	//Added For #7588
	public void updateUserNewsUpdateFlag(String corpid,Boolean flag,String user);
	public List findDateDiff(Date pwdexpiryDate, Date compareWithDate);
	public List getFromEmailAddress(String corpID);
	public List findUserByUsername(String coordinator);
	public List getAllUserList(String corpID);
	public List getAllUserRole(String userList,String sessionCorpID,String activeStatus);
	public List pickAllUser(String roleList,String sessionCorpID,String userList,String activeStatus);
	public int updateUserWithRoles(String transferFrom,String roleTransfer,String transferTo,String sessionCorpID);
	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID);
	/* Modified by Kunal Kumar Sharma for ticket number: 6838 */
	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId);
	public List getContactListByPartnerCode(String partnerCode,String corpID);
	public List getSearchContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId);
	/* Modification ends */
	public List findCoordinatorByUsername(String coordinator,String sessionCorpID);
	public List findCoordinatorByNetworkCoordinator(String coordinator, String corpID);
	public String doNotEmailFlag(String email);
	public String findToolTip(String username, String corpID);
	public void updateUserLoginFailureReason(String userName,String chng,String cnh1);
	public List findUserDetail(String coordinator, String sessionCorpID);
	public void updateUserInfo(String updateBy,String userID,String datasecurityID);
	public void updateUserRoleInfo(String updateBy,String userID,String roleID);
	public int getDefaultContactPersonCount(String partnerCode, String corpID);
	public List getUsersByParentAgent(String parentAgent);
	public List getAllInactiveUserList(String corpID);
	/*sonu */
	public List getContactList(String partnerCode,String corpID,String name, String emailId,String phone_number);
	
	public List getagentRequestList(String partnerCode,String corpID,String name, String emailId,String phone_number,String status);
	public List getUserRequestDetailList(String email);
	public List findUserUsername(String coordinator);
	
}
