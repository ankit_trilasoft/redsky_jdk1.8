<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Job Status Log</title> 
<meta name="heading" content="Job Status Log"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}</style>

<script language="javascript" type="text/javascript">

</script>
</head>


	<s:form id="jobStatusLog" name="jobStatusLog" action="" method="post" validate="true">   
	<div id="Layer1" style="width:85%;">
	
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Job Log List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="jobStatusList" class="table" requestURI="" id="jobStatusListId" export="true" pagesize="10" style="width:100%" >   
	<display:column property="id"  href="" paramId="id" paramProperty="id" sortable="true" titleKey="dataSecuritySetList.Id"/>
   	<display:column property="userName"  sortable="true" title="Run By"/>
   	<display:column property="totalCount"  sortable="true" title="TotalCount"/>
   	<display:column property="presentCount"  sortable="true" title="Present Count"/>
   	<display:column property="createdOn"  sortable="true" title="CreatedOn"/>
   	</display:table>
	
	</div>
</s:form> 

<script type="text/javascript"> 

highlightTableRows("jobStatusListId"); 

</script> 