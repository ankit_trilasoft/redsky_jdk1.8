package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.StorageBillingMonitorDao;
import com.trilasoft.app.model.StorageBillingMonitor;

public class StorageBillingMonitorDaoHibernate extends GenericDaoHibernate<StorageBillingMonitor, Long> implements StorageBillingMonitorDao{
	public StorageBillingMonitorDaoHibernate(){
		super(StorageBillingMonitor.class);
	}
	public List checkBillingFlag(String corpId){
		return getHibernateTemplate().find("from StorageBillingMonitor where billingFlag is true AND corpID = '"+ corpId +"'");
	}
	public List getStorageBillingMonitorList(String corpId){
		return getHibernateTemplate().find("from StorageBillingMonitor where corpID = '"+ corpId +"' order by createdOn desc");
	}	

}
