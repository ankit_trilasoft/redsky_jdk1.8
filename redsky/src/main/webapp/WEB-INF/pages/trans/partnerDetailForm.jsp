<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    <style type="text/css">
	fieldset {
			  padding: 0.0em;	
			  position: relative;
			  margin-right:75em
			  width: 90%;  
			  }
	</style>
	<script language="javascript" type="text/javascript">
		function onLoad() {
			document.forms['partnerDetailForm'].elements['billingInstructionCode'].value=document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingInstruction'].value;
		}
	</script>
	<script language="javascript" type="text/javascript">
		function onLoad() {
			var i;
			for(i=0;i<=80;i++)
			{
					document.forms['partnerDetailForm'].elements[i].disabled = true;
			}
		}
		
		function autoPopulate_partner_billingInstruction(targetElement) {
			var billingInstructionCode=targetElement.options[targetElement.selectedIndex].value;
		    document.forms['partnerDetailForm'].elements['partner.billingInstructionCode'].value=billingInstructionCode.substring(0,billingInstructionCode.indexOf(":")-1);
			targetElement.form.elements['partner.billingInstruction'].value=billingInstructionCode.substring(billingInstructionCode.indexOf(":")+2,billingInstructionCode.length);
		}
	</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
</script>
</head> 

<s:form id="partnerDetailForm" action="savePartner" method="post" validate="true">   
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partner.id" value="%{partner.id}"/>  
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" ><span>List</span></a></li>
			  	    <li><a href="editPartner.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Address</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Details</span></a></li>
			  		<li><a href="editPartnerRemarks.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" ><span>Remarks</span></a></li>
			  		<c:if test="${partnerType == 'AC'}">
			  			<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  			<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li>
			  			<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  	  	</c:if>
			  	  </c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div><br> 
<div id="Layer1">

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext"></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.firstName'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.lastName'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/></td>
						</tr>
						<tr>
							<td width="4" align="left" class="listwhitetext"></td>
							<td align="left" class="listwhitetext"> <s:textfield name="partner.partnerPrefix" size="4" required="true"
							cssClass="text medium" maxlength="10"  onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)"/></td>
							<td align="left" class="listwhitetext"> <s:textfield name="partner.firstName" required="true"
							cssClass="text medium" maxlength="25" onfocus="checkDate();" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.middleInitial"
							required="true" cssClass="text medium" maxlength="1" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true"
							cssClass="text medium" maxlength="40" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" size="4" required="true"
							cssClass="text small" maxlength="10" onkeydown="return onlyCharsAllowed(event)"/></td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
							required="true" cssClass="text medium" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)"/> </td>
						</tr>
					</tbody>
				</table>
				<fieldset>
					<table width="98%">
					<c:set var="isAccountFlag" value="false"/>
					<c:set var="isAgentFlag" value="false"/>
					<c:set var="isBrokerFlag" value="false"/>
					<c:set var="isVendorFlag" value="false"/>
					<c:set var="isCarrierFlag" value="false"/>
					<c:set var="qcFlag" value="false"/>
					<c:set var="fidiFlag" value="false"/>
					<c:if test="${partner.isAccount}">
						 <c:set var="isAccountFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isAgent}">
						 <c:set var="isAgentFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isBroker}">
						 <c:set var="isBrokerFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isVendor}">
						 <c:set var="isVendorFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isCarrier}">
						 <c:set var="isCarrierFlag" value="true"/>
					</c:if>
					<c:if test="${partner.qc}">
						 <c:set var="qcFlag" value="true"/>
					</c:if>
					
					<tr>Participates As
					<td rowspan="9">
					<table>
						<tbody>
							<tr>
								<td><s:checkbox key="partner.isAccount" value="${isAccountFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.isAccount'/></td>
							</tr>
							<tr>
								<td><s:checkbox key="partner.isAgent" value="${isAgentFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.isAgent'/></td>
							</tr>
							<tr>
								<td><s:checkbox key="partner.isBroker" value="${isBrokerFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.isBroker'/></td>
							</tr>
							<tr>
								<td><s:checkbox key="partner.isVendor" value="${isVendorFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.isVendor'/></td>
							</tr>
							<tr>
								<td><s:checkbox key="partner.isCarrier" value="${isCarrierFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.isCarrier'/></td>
							</tr>
							<tr>
								<td><s:checkbox key="partner.qc" value="${qcFlag}" fieldValue="true"/></td>
								<td><fmt:message key='partner.qc'/></td>
							</tr>
							<tr>
								<td><s:select key="partner.driver" list="%{isdriver}" cssStyle="width:35px" /></td>
								<td><fmt:message key='partner.isDriver'/></td>
							</tr>
						</tbody>
					</table>
					</td>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.doNotUse'/></td>
						<s:text id="partnerDoNotUseFormattedValue" name="FormDateValue"><s:param name="value" value="partner.doNotUse"/></s:text>
						<td><s:textfield cssClass="input-text" id="doNotUse" name="partner.doNotUse" value="%{partnerDoNotUseFormattedValue}" size="10" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerAddForm'].doNotUse,'calender','MM/dd/yyyy'); return false;"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate'/></td>
						<c:if test="${not empty partner.effectiveDate}">
						<s:text id="partnerEffectiveDateFormattedValue" name="FormDateValue"><s:param name="value" value="partner.effectiveDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="10" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerAddForm'].effectiveDate,'calender','MM/dd/yyyy'); return false;"/></td>
					</c:if>
					<c:if test="${empty partner.effectiveDate}">
					<td><s:textfield cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="10" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerAddForm'].effectiveDate,'calender','MM/dd/yyyy'); return false;"/></td>
					</c:if>
					<td align="right" class="listwhitetext"><fmt:message key='partner.zone'/></td><td><s:textfield cssClass="input-text" name="partner.zone"  size="4" maxlength="2"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.rank'/></td><td><s:select name="partner.rank"  list="%{rank}" cssStyle="width:70px" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayType'/></td><td><s:select name="partner.billPayType" list="%{paytype}" cssStyle="width:70px" headerKey="" headerValue=": Leave Empty" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayOption'/></td><td><s:select name="partner.billPayOption"  list="%{payopt}" cssStyle="width:55px" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.abbreviation'/></td><td><s:textfield cssClass="input-text" name="partner.abbreviation"  size="10" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.invoicePercentage'/></td><td><s:textfield cssClass="input-text" name="partner.invoicePercentage"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.warehouse'/></td><td><s:select name="partner.warehouse" list="%{house}" cssStyle="width:55px" headerKey=""  headerValue=""/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billToGroup'/></td><td><s:select name="partner.billToGroup"  list="%{billgrp}" cssStyle="width:70px" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.scheduleCode'/></td><td><s:select name="partner.scheduleCode" list="%{schedule}" cssStyle="width:70px" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.commitionType'/></td><td><s:select name="partner.commitionType" list="%{psh}" cssStyle="width:55px"  /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.coordinator'/></td><td><s:select name="partner.coordinator"  list="%{coord}" cssStyle="width:110px" headerKey="" headerValue=": Leave Empty" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.salesMan'/></td><td><s:select name="partner.salesMan"  list="%{sale}" cssStyle="width:110px" headerKey="" headerValue=": Leave Empty" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billingInstructionCode'/></td><td><s:select name="billingInstructionCode"  list="%{billinst}" cssStyle="width:55px" onchange="autoPopulate_partner_billingInstruction(this);"/></td><s:hidden key="partner.billingInstructionCode" />
						</tr>
					<tr>
					<td colspan="6" ><s:textfield cssClass="input-text" name="partner.billingInstruction" size="90" maxlength="65" /></td>
					</tr>
					</table>
				</fieldset>
				<fieldset>
					<table>
					<tr><td>Driver</td></tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.driverAgency'/></td><td><s:textfield cssClass="input-text" name="partner.driverAgency"  size="10" maxlength="8"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.shortPercentage'/></td><td><s:textfield cssClass="input-text" name="partner.shortPercentage"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.longPercentage'/></td><td><s:textfield cssClass="input-text" name="partner.longPercentage"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.localPercentage'/></td><td><s:textfield cssClass="input-text" name="partner.localPercentage"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
					</tr>
					</table>
					</fieldset>
					<fieldset>
					<table>
					<tr>
						<td colspan="3">Cross-reference....</td>
						<td><s:textfield cssClass="input-text" name="partner.client4"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.validNationalCode'/></td><td><s:textfield cssClass="input-text" name="partner.validNationalCode"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client5'/></td><td><s:textfield cssClass="input-text" name="partner.client5"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.idCode'/></td><td><s:textfield cssClass="input-text" name="partner.idCode"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client6'/></td><td><s:textfield cssClass="input-text" name="partner.client6"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						
						<td align="right" class="listwhitetext"><fmt:message key='partner.client7'/></td><td><s:textfield cssClass="input-text" name="partner.client7"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client1'/></td><td><s:textfield cssClass="input-text" name="partner.client1"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client8'/></td><td><s:textfield cssClass="input-text" name="partner.client8"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client2'/></td><td><s:textfield cssClass="input-text" name="partner.client2"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client9'/></td><td><s:textfield cssClass="input-text" name="partner.client9"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="40px"></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client3'/></td><td><s:textfield cssClass="input-text" name="partner.client3"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client10'/></td><td><s:textfield cssClass="input-text" name="partner.client10"  size="25" maxlength="15"/></td>
					</tr>
					</table>
				</fieldset>
				
				</td>
			</tr>
		</tbody>
	</table>
</div>
<table style="width:700px">
<tr>
<td align="center">
<table>
<tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdOn'/></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedOn'/></td>
						<s:hidden name="partner.updatedOn"/>
						<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedBy' /></td>
						<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"  />
						<td style="width:100px"><s:label name="partner.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
</tbody>
</table>  



</td>
</tr>
</table>
<s:hidden key="partner.perDiemFreeDays"  />
<s:hidden key="partner.perDiemCost1"  />
<s:hidden key="partner.perDiemCost2"  />
<s:hidden key="partner.remarks1"  />
<s:hidden key="partner.remarks2"  />
<s:hidden key="partner.remarks3"  />
<s:hidden key="partner.remarks4"  />
<s:hidden key="partner.remarks5"  />

<s:hidden name="partner.mailingAddress1"/>
<s:hidden name="partner.mailingCity"/>
<s:hidden name="partner.mailingFax"/>
<s:hidden name="partner.mailingAddress2"/>
<s:hidden name="partner.mailingState" />
<s:hidden name="partner.mailingPhone"/>
<s:hidden name="partner.mailingAddress3"/>
<s:hidden name="partner.mailingZip"/>
<s:hidden name="partner.mailingTelex"/>
<s:hidden name="partner.mailingAddress4"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.mailingCountry"/>
<s:hidden name="partner.mailingEmail" />
<s:hidden name="partner.terminalAddress1"/>
<s:hidden name="partner.terminalCity"/>
<s:hidden name="partner.terminalFax"/>
<s:hidden name="partner.terminalAddress2"/>
<s:hidden name="partner.terminalState" />
<s:hidden name="partner.terminalPhone"/>
<s:hidden name="partner.terminalAddress3"/>
<s:hidden name="partner.terminalZip"/>
<s:hidden name="partner.terminalTelex"/>
<s:hidden name="partner.terminalAddress4"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.terminalCountry"/>
<s:hidden name="partner.terminalEmail" />
<s:hidden name="partner.billingAddress1"/>
<s:hidden name="partner.billingCity"/>
<s:hidden name="partner.billingFax"/>
<s:hidden name="partner.billingAddress2"/>
<s:hidden name="partner.billingState" />
<s:hidden name="partner.billingPhone"/>
<s:hidden name="partner.billingAddress3"/>
<s:hidden name="partner.billingZip"/>
<s:hidden name="partner.billingTelex"/>
<s:hidden name="partner.billingAddress4"/>
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.billingCountry"/>
<s:hidden name="partner.billingEmail" />
<c:if test="${ empty param.popup}" >
            
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="save" key="button.save"/>   
        <c:if test="${not empty partner.id}">    
            <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="delete" key="button.delete" onclick="return confirmDelete('partner')"/>   
        </c:if>   
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="cancel" key="button.cancel"/>   

</c:if>
</s:form>
<script type="text/javascript"> 
try{
<c:if test="${not empty param.popup}" >
	for(i=0;i<=80;i++)
			{
					document.forms['partnerDetailForm'].elements[i].disabled = true;
			}
</c:if>  
}
catch(e){}
	Form.focusFirstElement($("partnerDetailForm"));   
</script> 