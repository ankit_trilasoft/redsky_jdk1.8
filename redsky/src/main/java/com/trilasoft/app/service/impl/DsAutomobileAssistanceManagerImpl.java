package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsAutomobileAssistanceDao;
import com.trilasoft.app.model.DsAutomobileAssistance;
import com.trilasoft.app.service.DsAutomobileAssistanceManager;


public class DsAutomobileAssistanceManagerImpl extends GenericManagerImpl<DsAutomobileAssistance, Long> implements DsAutomobileAssistanceManager { 
	DsAutomobileAssistanceDao dsAutomobileAssistanceDao;
	public DsAutomobileAssistanceManagerImpl(DsAutomobileAssistanceDao dsAutomobileAssistanceDao) {
		super(dsAutomobileAssistanceDao); 
        this.dsAutomobileAssistanceDao = dsAutomobileAssistanceDao; 	
	}
	
	public List getListById(Long id) {
		return dsAutomobileAssistanceDao.getListById(id);
	}

}

