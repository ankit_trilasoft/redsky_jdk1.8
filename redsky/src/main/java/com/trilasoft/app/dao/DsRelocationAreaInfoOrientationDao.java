package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;

public interface DsRelocationAreaInfoOrientationDao extends GenericDao<DsRelocationAreaInfoOrientation, Long>{
	List getListById(Long id);
}
