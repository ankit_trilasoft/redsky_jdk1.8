package com.trilasoft.qtg.wsclient.driver;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.QuotesToGoDTO;
import com.trilasoft.qtg.wsclient.request.model.ESOpportunity;
import com.trilasoft.qtg.wsclient.request.model.OpportunityData;
import com.trilasoft.qtg.wsclient.request.model.RoutingHeader;
import com.trilasoft.qtg.wsclient.response.model.CreateOpportunityReturn;

public class XMLDto {

	public String creatXMLRequest(List<QuotesToGoDTO> obj) throws JAXBException {
		
		OpportunityData opportunityData = new OpportunityData();
		StringWriter writer = new StringWriter();
		
		ESOpportunity esOpportunity = new ESOpportunity();		
		esOpportunity.setSourceType(obj.get(0).getSource().toString());
		esOpportunity.setTrackingCode(obj.get(0).getSequenceNumber().toString());
		esOpportunity.setCustomerNameFirst(obj.get(0).getFirstName().toString());
		esOpportunity.setCustomerNameLast(obj.get(0).getLastName().toString());
		esOpportunity.setCustomerMiddleInitial(obj.get(0).getMiddleInitial().toString());
		esOpportunity.setCustomerTitle(obj.get(0).getPrefix().toString());
		esOpportunity.setAgentAssignmentCode(obj.get(0).getVanlinecode().toString());
		esOpportunity.setCustomerPhone(obj.get(0).getOriginHomePhone().toString());
		esOpportunity.setCustomerAlternatePhone(obj.get(0).getOriginMobile().toString());
		esOpportunity.setCustomerFaxPhone(obj.get(0).getOriginFax().toString());
		esOpportunity.setCustomerEmail(obj.get(0).getEmail().toString());
		esOpportunity.setSurveyAddressLine1(obj.get(0).getOriginAddress1().toString());
		esOpportunity.setSurveyAddressLine2(obj.get(0).getOriginAddress2().toString());
		esOpportunity.setSurveyCity(obj.get(0).getOriginCity().toString());
		esOpportunity.setSurveyStateAbv(obj.get(0).getOriginState().toString());
		esOpportunity.setSurveyPostalCode(obj.get(0).getOriginZip().toString());
		esOpportunity.setDestinationAddressLine1(obj.get(0).getDestinationAddress1().toString());
		esOpportunity.setDestinationAddressLine2(obj.get(0).getDestinationAddress2().toString());
		esOpportunity.setDestinationCity(obj.get(0).getDestinationCity().toString());
		esOpportunity.setDestinationStateAbv(obj.get(0).getDestinationState().toString());
		esOpportunity.setDestinationPostalCode(obj.get(0).getDestinationZip().toString());
		esOpportunity.setCustomerPhoneExt(obj.get(0).getCustomerPhoneExt().toString());
		esOpportunity.setCustomerAlternatePhoneExt(obj.get(0).getCustomerAlternatePhoneExt().toString());
		GregorianCalendar c = new GregorianCalendar();
		if(obj.get(0).getMoveDate()!=null){
		c.setTime((Date)obj.get(0).getMoveDate());
		try {
			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			esOpportunity.setApproximateMoveDate(date2);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else{
			esOpportunity.setApproximateMoveDate(null);
		}
		esOpportunity.setMoveTypeTC(obj.get(0).getBillToCode().toString());
		
		opportunityData.setOpportunity(esOpportunity);
		
		RoutingHeader header= new RoutingHeader();
		header.setRoutingUuid(obj.get(0).getSequenceBookingAgentCode().toString());
		header.setRoutingReturnToUuid("");
		opportunityData.setRoutingHeaders(header);
		//create request
		final JAXBContext context =  JAXBContext.newInstance(com.trilasoft.qtg.wsclient.request.model.OpportunityData.class);
		context.createMarshaller().marshal(opportunityData, writer);
		
		return writer.toString();
		
	}
	
	
	public CreateOpportunityReturn readXmlResponse(String responseXml) throws JAXBException {
		
		final JAXBContext context =  JAXBContext.newInstance(com.trilasoft.qtg.wsclient.response.model.CreateOpportunityReturn.class);
		StringReader stringReader = new StringReader(responseXml);
		final CreateOpportunityReturn createOpportunityReturn = (CreateOpportunityReturn) context.createUnmarshaller().unmarshal(stringReader);
		return createOpportunityReturn;
		
	}
	
	
	
}
