
delimiter $$

CREATE trigger redsky.trigger_add_history_companydivision
BEFORE UPDATE on redsky.companydivision
for each row

BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
     



  IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'companydivision.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;



  IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'companydivision.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;
 
 
   



   
  IF (OLD.companyCode <> NEW.companyCode ) THEN
      select CONCAT(fieldNameList,'companydivision.companyCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyCode,'~') into newValueList;
  END IF;
  
  IF (OLD.GSTRegnNo <> NEW.GSTRegnNo ) THEN
      select CONCAT(fieldNameList,'companydivision.GSTRegnNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.GSTRegnNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.GSTRegnNo,'~') into newValueList;
  END IF;
 





  IF (OLD.bookingAgentCode <> NEW.bookingAgentCode ) THEN
      select CONCAT(fieldNameList,'companydivision.bookingAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentCode,'~') into newValueList;
  END IF;
 





  IF (OLD.accountingCode <> NEW.accountingCode ) THEN
      select CONCAT(fieldNameList,'companydivision.accountingCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountingCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountingCode,'~') into newValueList;
  END IF;
 





  IF (OLD.vanLineCode <> NEW.vanLineCode ) THEN
      select CONCAT(fieldNameList,'companydivision.vanLineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineCode,'~') into newValueList;
  END IF;
 





  IF (OLD.vanlinedefaultJobtype <> NEW.vanlinedefaultJobtype ) THEN
      select CONCAT(fieldNameList,'companydivision.vanlinedefaultJobtype~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanlinedefaultJobtype,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanlinedefaultJobtype,'~') into newValueList;
  END IF;
 




  IF (OLD.billingAddress1 <> NEW.billingAddress1 ) THEN
      select CONCAT(fieldNameList,'companydivision.billingAddress1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress1,'~') into newValueList;
  END IF;
 





  IF (OLD.billingAddress2 <> NEW.billingAddress2 ) THEN
      select CONCAT(fieldNameList,'companydivision.billingAddress2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress2,'~') into newValueList;
  END IF;
 





  IF (OLD.billingAddress3 <> NEW.billingAddress3 ) THEN
      select CONCAT(fieldNameList,'companydivision.billingAddress3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress3,'~') into newValueList;
  END IF;
 




  IF (OLD.billingAddress4 <> NEW.billingAddress4 ) THEN
      select CONCAT(fieldNameList,'companydivision.billingAddress4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress4,'~') into newValueList;
  END IF;
 




  IF (OLD.billingCity <> NEW.billingCity ) THEN
      select CONCAT(fieldNameList,'companydivision.billingCity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCity,'~') into newValueList;
  END IF;
 





  IF (OLD.billingCountry <> NEW.billingCountry ) THEN
      select CONCAT(fieldNameList,'companydivision.billingCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCountry,'~') into newValueList;
  END IF;
 





  IF (OLD.billingCountryCode <> NEW.billingCountryCode ) THEN
      select CONCAT(fieldNameList,'companydivision.billingCountryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCountryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCountryCode,'~') into newValueList;
  END IF;
 





  IF (OLD.billingEmail <> NEW.billingEmail ) THEN
      select CONCAT(fieldNameList,'companydivision.billingEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingEmail,'~') into newValueList;
  END IF;
 




  IF (OLD.billingFax <> NEW.billingFax ) THEN
      select CONCAT(fieldNameList,'companydivision.billingFax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingFax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingFax,'~') into newValueList;
  END IF;
 





  IF (OLD.billingPhone <> NEW.billingPhone ) THEN
      select CONCAT(fieldNameList,'companydivision.billingPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingPhone,'~') into newValueList;
  END IF;
 



  IF (OLD.billingState <> NEW.billingState ) THEN
      select CONCAT(fieldNameList,'companydivision.billingState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingState,'~') into newValueList;
  END IF;
 



  IF (OLD.billingTelex <> NEW.billingTelex ) THEN
      select CONCAT(fieldNameList,'companydivision.billingTelex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingTelex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingTelex,'~') into newValueList;
  END IF;
 




  IF (OLD.billingZip <> NEW.billingZip ) THEN
      select CONCAT(fieldNameList,'companydivision.billingZip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingZip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingZip,'~') into newValueList;
  END IF;
 




  IF (OLD.logoName <> NEW.logoName ) THEN
      select CONCAT(fieldNameList,'companydivision.logoName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.logoName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.logoName,'~') into newValueList;
  END IF;
 



  IF (OLD.bankName <> NEW.bankName ) THEN
      select CONCAT(fieldNameList,'companydivision.bankName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankName,'~') into newValueList;
  END IF;
 



  IF (OLD.bankAccountNumber <> NEW.bankAccountNumber ) THEN
      select CONCAT(fieldNameList,'companydivision.bankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankAccountNumber,'~') into newValueList;
  END IF;
 




  IF (OLD.wireTransferAccountNumber <> NEW.wireTransferAccountNumber ) THEN
      select CONCAT(fieldNameList,'companydivision.wireTransferAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.wireTransferAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.wireTransferAccountNumber,'~') into newValueList;
  END IF;
 



  IF (OLD.vanlineseq <> NEW.vanlineseq ) THEN
      select CONCAT(fieldNameList,'companydivision.vanlineseq~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanlineseq,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanlineseq,'~') into newValueList;
  END IF;
 




  IF (OLD.vanlineCoordinator <> NEW.vanlineCoordinator ) THEN
      select CONCAT(fieldNameList,'companydivision.vanlineCoordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanlineCoordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanlineCoordinator,'~') into newValueList;
  END IF;
 



  IF (OLD.vanLineCodeGen <> NEW.vanLineCodeGen ) THEN
      select CONCAT(fieldNameList,'companydivision.vanLineCodeGen~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineCodeGen,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineCodeGen,'~') into newValueList;
  END IF;
 




  IF (OLD.EIN <> NEW.EIN ) THEN
      select CONCAT(fieldNameList,'companydivision.EIN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EIN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EIN,'~') into newValueList;
  END IF;
 




  IF (OLD.SCAC <> NEW.SCAC ) THEN
      select CONCAT(fieldNameList,'companydivision.SCAC~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCAC,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCAC,'~') into newValueList;
  END IF;
 




  IF (OLD.internalCostVendorCode <> NEW.internalCostVendorCode ) THEN
      select CONCAT(fieldNameList,'companydivision.internalCostVendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.internalCostVendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.internalCostVendorCode,'~') into newValueList;
  END IF;
 




  IF (OLD.operationsHub <> NEW.operationsHub ) THEN
      select CONCAT(fieldNameList,'companydivision.operationsHub~') into fieldNameList;
      select CONCAT(oldValueList,OLD.operationsHub,'~') into oldValueList;
      select CONCAT(newValueList,NEW.operationsHub,'~') into newValueList;
  END IF;
 



  IF (OLD.baseCurrency <> NEW.baseCurrency ) THEN
      select CONCAT(fieldNameList,'companydivision.baseCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.baseCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.baseCurrency,'~') into newValueList;
  END IF;
 




  IF (OLD.resourceID <> NEW.resourceID ) THEN
      select CONCAT(fieldNameList,'companydivision.resourceID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.resourceID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.resourceID,'~') into newValueList;
  END IF;
 




  IF (OLD.resourcePassword <> NEW.resourcePassword ) THEN
      select CONCAT(fieldNameList,'companydivision.resourcePassword~') into fieldNameList;
      select CONCAT(oldValueList,OLD.resourcePassword,'~') into oldValueList;
      select CONCAT(newValueList,NEW.resourcePassword,'~') into newValueList;
  END IF;
 




  IF (OLD.textMessage <> NEW.textMessage ) THEN
      select CONCAT(fieldNameList,'companydivision.textMessage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.textMessage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.textMessage,'~') into newValueList;
  END IF;
 



  IF (OLD.vanlineJob <> NEW.vanlineJob ) THEN
      select CONCAT(fieldNameList,'companydivision.vanlineJob~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanlineJob,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanlineJob,'~') into newValueList;
  END IF;
 




  IF (OLD.swiftcode <> NEW.swiftcode ) THEN
      select CONCAT(fieldNameList,'companydivision.swiftcode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.swiftcode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.swiftcode,'~') into newValueList;
  END IF;
 




  IF (OLD.beneficiaryName <> NEW.beneficiaryName ) THEN
      select CONCAT(fieldNameList,'companydivision.beneficiaryName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.beneficiaryName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.beneficiaryName,'~') into newValueList;
  END IF;
 




  IF (OLD.bankAddress <> NEW.bankAddress ) THEN
      select CONCAT(fieldNameList,'companydivision.bankAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankAddress,'~') into newValueList;
  END IF;
 




  IF (OLD.mssCustomerID <> NEW.mssCustomerID ) THEN
      select CONCAT(fieldNameList,'companydivision.mssCustomerID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mssCustomerID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mssCustomerID,'~') into newValueList;
  END IF;
 




  IF (OLD.mssPassword <> NEW.mssPassword ) THEN
      select CONCAT(fieldNameList,'companydivision.mssPassword~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mssPassword,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mssPassword,'~') into newValueList;
  END IF;
 




  IF (OLD.ugwwAgentCodes <> NEW.ugwwAgentCodes ) THEN
      select CONCAT(fieldNameList,'companydivision.ugwwAgentCodes~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwwAgentCodes,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwwAgentCodes,'~') into newValueList;
  END IF;
 


  
  
 
  
  
   IF ((date_format(OLD.lastUpdatedLeave,'%Y-%m-%d') <> date_format(NEW.lastUpdatedLeave,'%Y-%m-%d')) or (date_format(OLD.lastUpdatedLeave,'%Y-%m-%d') is null and date_format(NEW.lastUpdatedLeave,'%Y-%m-%d') is not null)
    or (date_format(OLD.lastUpdatedLeave,'%Y-%m-%d') is not null and date_format(NEW.lastUpdatedLeave,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.lastUpdatedLeave~') into fieldNameList;
      IF(OLD.lastUpdatedLeave is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.lastUpdatedLeave is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.lastUpdatedLeave is not null) THEN
          select CONCAT(oldValueList,OLD.lastUpdatedLeave,'~') into oldValueList;
       END IF;
        IF(NEW.lastUpdatedLeave is not null) THEN
          select CONCAT(newValueList,NEW.lastUpdatedLeave,'~') into newValueList;
        END IF;
  END IF;
  
  
  
  
  IF (OLD.childAgentCode <> NEW.childAgentCode ) THEN
      select CONCAT(fieldNameList,'companydivision.childAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.childAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.childAgentCode,'~') into newValueList;
  END IF;
   
   
   
   
  
  IF (OLD.cportalBrandingURL <> NEW.cportalBrandingURL ) THEN
      select CONCAT(fieldNameList,'companydivision.cportalBrandingURL~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cportalBrandingURL,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cportalBrandingURL,'~') into newValueList;
  END IF;
  
  
  IF (OLD.ugwwAgentCodes <> NEW.ugwwAgentCodes ) THEN
      select CONCAT(fieldNameList,'companydivision.ugwwAgentCodes~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwwAgentCodes,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwwAgentCodes,'~') into newValueList;
  END IF;
   
   
   
  IF (OLD.companyWebsite <> NEW.companyWebsite ) THEN
      select CONCAT(fieldNameList,'companydivision.companyWebsite~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyWebsite,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyWebsite,'~') into newValueList;
  END IF;
  
  
  
  
  IF (OLD.eoriNo <> NEW.eoriNo ) THEN
      select CONCAT(fieldNameList,'companydivision.eoriNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.eoriNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.eoriNo,'~') into newValueList;
  END IF;
  
  IF (OLD.lastInvoiceNumber <> NEW.lastInvoiceNumber ) THEN
      select CONCAT(fieldNameList,'companydivision.lastInvoiceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastInvoiceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastInvoiceNumber,'~') into newValueList;
  END IF;
  
  IF (OLD.lastCreditInvoice <> NEW.lastCreditInvoice ) THEN
      select CONCAT(fieldNameList,'companydivision.lastCreditInvoice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastCreditInvoice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastCreditInvoice,'~') into newValueList;
  END IF;
  
  
  IF ((date_format(OLD.inactiveEffectiveDate,'%Y-%m-%d') <> date_format(NEW.inactiveEffectiveDate,'%Y-%m-%d')) or (date_format(OLD.inactiveEffectiveDate,'%Y-%m-%d') is null and date_format(NEW.inactiveEffectiveDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.inactiveEffectiveDate,'%Y-%m-%d') is not null and date_format(NEW.inactiveEffectiveDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'companydivision.inactiveEffectiveDate~') into fieldNameList;
      IF(OLD.inactiveEffectiveDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.inactiveEffectiveDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.inactiveEffectiveDate is not null) THEN
          select CONCAT(oldValueList,OLD.inactiveEffectiveDate,'~') into oldValueList;
       END IF;
        IF(NEW.inactiveEffectiveDate is not null) THEN
          select CONCAT(newValueList,NEW.inactiveEffectiveDate,'~') into newValueList;
        END IF;
  END IF;
  
  
   IF (OLD.closedDivision <> NEW.closedDivision ) THEN
       select CONCAT(fieldNameList,'companydivision.closedDivision~') into fieldNameList;
       IF(OLD.closedDivision = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.closedDivision = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.closedDivision = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.closedDivision = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
CALL add_tblHistory (OLD.id,"companydivision", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$

delimiter;