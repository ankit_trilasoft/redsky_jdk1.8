<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
	    <title><fmt:message key="reportsDetail.title"/></title>   
	    <meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
	    <c:if test="${param.popup}"> 
	    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
	    </c:if>
	    <style type="text/css">
		h2 {background-color: #CCCCCC}
		</style>
		
		<script language="javascript" type="text/javascript">
		function validatefields(val){
			document.forms['reportForm'].elements['fileType'].value = val;
			document.forms['reportForm'].submit();
		 return true;
		}
		
		function refreshParent() {
			  window.opener.location.href = window.opener.location.href;
				if (window.opener.progressWindow)
					{
			    		window.opener.progressWindow.close()
			  		}
			  window.close();
		}
		
		window.opener.location.href = window.opener.location.href;
		if (window.opener.progressWindow)
			{
			 	window.opener.progressWindow.close()
			}
		
	</script>
	<style type="text/css">
		/* collapse */
		
		img {
		cursor:pointer;
		}
	</style>
</head>  
<body style="background-color:#444444;">
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<s:form id="reportForm" action='${empty param.popup?"viewReport.html":"viewReport.html?decorator=popup&popup=true"}' method="post" validate="true">
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>

<table>
		<tbody>
			<tr>
				<td><img src="<c:url value='/images/form_ico.gif'/>"/></td>
				<td>
					<table>
					<tr><td>&nbsp;</td></tr> 
					<tr><td><b><font style="font-size:16px; font-family:Verdana, Geneva, sans-serif; font-style:italic; color: #EB8234;">Report Manager</font></b></td></tr> 
					<tr style="height: 5px;"><td></td></tr> 
					<tr><td><b><font style="color: #5D5D5D;">Report : <%=request.getParameter("reportName") %></font></b></td></tr> 
					</table>
				</td>
			</tr>	
		</tbody>
		
	</table>   

	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px; height:250px; ">
		<tbody>
			<tr>
				<td valign="top">
		 			<div class="subcontent-tab">Select Output format for Report</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
		    				<tbody>	
						  	<tr><td class="listwhitetext" style="height:100px"></td></tr>  
							<tr><td><b>&nbsp;&nbsp;Output Format : </b></td></tr> 
							<tr><td class="listwhitetext" style="height:20px"></td></tr>  
							<!--  <tr><td >&nbsp;&nbsp;<img alt="CSV File" src="<c:url value='/images/csv.gif'/>" onclick="return validatefields('CSV');"> <strong> <a href="#" onClick="return validatefields('CSV');"> <font style="color: #0066CB;">CSV</font> </a> </strong>| <img alt="XLS File" src="<c:url value='/images/xls.gif'/>" onclick="return validatefields('XLS');"/> <strong> <a href="#" onClick="return validatefields('XLS');"> <font style="color: #0066CB;">XLS</font> </a> </strong> | <img alt="HTML File" src="<c:url value='/images/xml.gif'/>" onclick="return validatefields('HTML');"/> <strong> <a href="#" onClick="return validatefields('HTML');"><font style="color: #0066CB;"> HTML </font></a> </strong> | <img alt="PDF File" src="<c:url value='/images/PDF_icon2.jpg'/>" onclick="return validatefields('PDF');"/> <strong> <a href="#" onClick="return validatefields('PDF');"><font style="color: #0066CB;"> PDF </font></a> </strong></td></tr> -->
							<tr><td colspan="2">&nbsp;&nbsp;<img alt="CSV File" src="<c:url value='/images/csv1.gif'/>" onclick="return validatefields('CSV');"/><a href="#" onClick="return validatefields('CSV');"> <font style="color: #0000FE;">CSV</font> </a>&nbsp;&nbsp;<img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validatefields('XLS');"/><a href="#" onClick="return validatefields('XLS');"> <font style="color: #0000FE;">XLS</font> </a>&nbsp;&nbsp;<img alt="HTML File" src="<c:url value='/images/html1.gif'/>" onclick="return validatefields('HTML');"/><a href="#" onClick="return validatefields('HTML');"><font style="color: #0000FE;"> HTML </font></a>&nbsp;&nbsp;<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="return validatefields('PDF');"/> <a href="#" onClick="return validatefields('PDF');"><font style="color: #0000FE;"> PDF </font></a></td></tr>
							<tr><td class="listwhitetext" style="height:10px"></td></tr>  
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>						
	<table> 
		<tbody>
			<tr height="20px;">
			  <td align="right" style="padding-left: 385px;"><strong><a href="#" onClick="return refreshParent();"><font style="color: #EB8234;font-size:11px;"><u>Close this window</u></font></a></strong></td>
			</tr>  		
	  	</tbody>
	</table>		
<s:hidden name="fileType" />
</s:form> 
</body>
<script type="text/javascript">   
    Form.focusFirstElement($("reportForm"));   
</script>