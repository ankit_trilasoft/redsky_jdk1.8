/**
 * WSSkipLogic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSSkipLogic  extends com.keysurvey.api.v68.form.design.WSLogic  implements java.io.Serializable {
    private long answerId;

    private long questionId;

    public WSSkipLogic() {
    }

    public WSSkipLogic(
           long versionId,
           long answerId,
           long questionId) {
        super(
            versionId);
        this.answerId = answerId;
        this.questionId = questionId;
    }


    /**
     * Gets the answerId value for this WSSkipLogic.
     * 
     * @return answerId
     */
    public long getAnswerId() {
        return answerId;
    }


    /**
     * Sets the answerId value for this WSSkipLogic.
     * 
     * @param answerId
     */
    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }


    /**
     * Gets the questionId value for this WSSkipLogic.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSSkipLogic.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSSkipLogic)) return false;
        WSSkipLogic other = (WSSkipLogic) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.answerId == other.getAnswerId() &&
            this.questionId == other.getQuestionId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getAnswerId()).hashCode();
        _hashCode += new Long(getQuestionId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSSkipLogic.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSkipLogic"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "answerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
