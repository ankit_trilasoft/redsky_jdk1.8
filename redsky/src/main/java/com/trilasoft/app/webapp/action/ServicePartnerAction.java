/**
 * @Class Name  Service Partner Action
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.Port;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PortManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class ServicePartnerAction extends BaseAction implements Preparable{
   
    private ServiceOrder serviceOrder;
    private CustomerFile customerFile;
    private ServicePartner servicePartner;
    private  TrackingStatus trackingStatus;
    private AccountLine accountLine;
    private   Miscellaneous  miscellaneous;
    private MiscellaneousManager miscellaneousManager ;
    private TrackingStatusManager trackingStatusManager;
    private ServiceOrderManager serviceOrderManager;
    private NotesManager notesManager;
    private AccountLineManager accountLineManager;
    private RefMasterManager refMasterManager;
    private ServicePartnerManager servicePartnerManager; 
    private  IntegrationLogInfoManager  integrationLogInfoManager;
    private String carrierNumber;
    private Long autoCarrier;
    private String accountLineNumber;
    private Long autoLineNumber;
    private String portCode;
    private String countServiceNotes;
    private String countCarrierNotes;
    private String userName;
    private String sessionCorpID;
    private String accountInterface; 
    private String gotoPageString;
    private String validateFormNav;
    private String hitFlag="";
    private String shipNumber;
    private Long maxId;
    private Long id;
    private Long soId;
    private Long sid; 
    private List maxCarrierNumber;
    private List portLineList;
    private Set servicePartners;  
    private List servicePartnerList;
    private  List containerNumberList;
    private List refMasters;
    private List serviceOrders;
    private List servicePartnerss;
    private List maxLineNumber; 
    private  Map<String, String> SITOUTTA;
    private  Map<String, String> omni;
    private  Map<String, String> poe;
    private  Map<String, String> pol;
    private  Map<String, String> partnerType;
    private Date carrierATD;
    private Date carrierETD;
    private Date carrierATA;
    private Date carrierETA;
    private String shipSize;
	private String minShip;
	private String countShip;
	private CustomerFileManager customerFileManager;
	private List customerSO;
	
	private CustomManager customManager;
	private String countBondedGoods;
	
	private String maxChild;
	private String countChild;
	private String minChild;
	private Long soIdNum;
	private Long sidNum;
	private long tempservicePartner;
	private List servicePartnerSO;
	private int totalClone;
	private int copyCarrierAtd;
	private int copyCarrierAta;
	private Long customerFileId;
	private CompanyDivisionManager companyDivisionManager;
	private  ExchangeRateManager exchangeRateManager;
	private ToDoRuleManager toDoRuleManager;
	private  Map<String,String>euVatPercentList;
	private List polList;
	private PortManager portManager;
	private Port port;
	private String mode;
	private String userType;
	private boolean accountLineAccountPortalFlag=false;
	private List<SystemDefault> sysDefaultDetail;
	private String  usertype;
	private Boolean surveyTab = new Boolean(false);
	private String routingStatus;
	private SystemDefault systemDefault;
	private String code;
	private String Name;
	private  Map<String, String> omni_isactive;
    private String oiJobList;
	 
//  A Method to Authenticate User, calling from main menu.
    
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ServicePartnerAction.class);
	private Company company;
	private CompanyManager companyManager;
	private String voxmeIntergartionFlag;
	private boolean futureDateFlag=false;
	private Set linkedshipNumber=new HashSet();
    public ServicePartnerAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        this.userType=user.getUserType();
        usertype=user.getUserType();
	}
    
//  End of Method.
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();	
		company=companyManager.findByCorpID(sessionCorpID).get(0);

         if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
         if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				}
			if(company.getAllowFutureActualDateEntry()!=null){
				futureDateFlag = company.getAllowFutureActualDateEntry();
			}
		}
		try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
				 Long soId = null;
				 try {
					 soId = new Long(getRequest().getParameter("sid").toString());
					} catch (Exception e) {
						soId = new Long(getRequest().getParameter("id").toString());
						e.printStackTrace();
					}
			 if(soId!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(soId);
				 String corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(soId);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
		sysDefaultDetail = serviceOrderManager.findSysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
					accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
				}
			} 
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
//  Method for getting the values for all comboboxes in the dropdown list.
    
   public String getComboList(String corpID){
    	SITOUTTA = refMasterManager.findByParameter(corpID, "SITOUTTA");
    	omni = refMasterManager.findByParameter(corpID, "omni"); 
    	poe = refMasterManager.findByParameter(corpID, "poe");
    	pol = refMasterManager.findByParameter(corpID, "pol");
    	partnerType = refMasterManager.findByParameter(corpID, "partnerType");
    	euVatPercentList = refMasterManager.findVatPercentList(corpID, "EUVAT");

    	 String parameters="'omni','groupagestatus'";
    	LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
    	   
    	omni_isactive  = new LinkedHashMap<String, String>();
    	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpID,parameters);
    	   for (RefMasterDTO  refObj : allParamValue) {
    	    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("omni")){
    	    	    		 	omni_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());}}
 	   return SUCCESS;
    }
   
// End of Method.
   
// A Method to get the number of container.
   
   public List containerNumberList() {
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
   	containerNumberList=servicePartnerManager.getContainerNumber(serviceOrder.getShipNumber());
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   	return containerNumberList;
   }
   
// End of Method.  
   
//  A Method to get port list.
    
  
    public String portName()
	{
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        if(portCode!=null)
        {
        	portLineList=servicePartnerManager.portName(portCode,sessionCorpID);
        }
        else
        {	
        	String message="The Port Name is not exist";
        	errorMessage(getText(message));
        }
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
    
//  End of Method.
    
//  Method used to save through tabs.
        
    public String saveOnTabChange() throws Exception {
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    		validateFormNav = "OK"; 
            String save = save(); 
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return gotoPageString;
    }
    
//  End of Method.
    
    @SkipValidation 
    public String servicePartnerNext(){
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	tempservicePartner=Long.parseLong((servicePartnerManager.goNextSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    @SkipValidation 
    public String servicePartnerPrev(){
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	tempservicePartner=Long.parseLong((servicePartnerManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    } 
      
    @SkipValidation 
    public String servicePartnerSO(){
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	servicePartnerSO = servicePartnerManager.goSOChild(sidNum,sessionCorpID,soIdNum);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    
    
//  Method for editing and adding the record.
    Boolean alreadyExists=false;
    List fieldsInfo=new ArrayList();
    public String edit() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    
    	long time1=System.currentTimeMillis();
    	//getComboList(sessionCorpID);
    	long time2=System.currentTimeMillis();
    	long step1Time=(time2-time1)/1000;
    	System.out.println("\n\n\n\n\n editTime for step1  "+step1Time +" in seconds");
    	long time3=System.currentTimeMillis();
    	accountInterface="";
    	if(serviceOrderManager.findAccountInterface(sessionCorpID).get(0)!=null){
    	accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
    	}
    	totalClone=0;
    	if (id != null){  
    		servicePartner = servicePartnerManager.get(id);
    		if(refMasterManager.getDescription(servicePartner.getOmni(),"omni")!=null && !refMasterManager.getDescription(servicePartner.getOmni(),"omni").isEmpty()
    				&& refMasterManager.getDescription(servicePartner.getOmni(),"omni").get(0)!=null){
    			omni.put(servicePartner.getOmni(), refMasterManager.getDescription(servicePartner.getOmni(),"omni").get(0).toString());    			
    		}
    		serviceOrder = servicePartner.getServiceOrder();
    		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
        	trackingStatus=trackingStatusManager.get(serviceOrder.getId());
        	try {
     			 if((usertype.trim().equalsIgnoreCase("AGENT"))){
     				 
     			 if(serviceOrder!=null){
     				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(serviceOrder.getCorpID()); 
     				 if(serviceOrder.getIsNetworkRecord() && (trackingStatus.getSoNetworkGroup() || chkCompanySurveyFlag)){
     						surveyTab= true;
     				}
     				}
     			 }
     		 } catch (Exception e) {
     			e.printStackTrace();
     		 }
        	 billing = billingManager.get(serviceOrder.getId());
        	customerFile = serviceOrder.getCustomerFile();
        	List maxChildList=servicePartnerManager.findMaximumChild(serviceOrder.getShipNumber());
        	 if(maxChildList!=null && !(maxChildList.isEmpty()) && (maxChildList.get(0)!=null)){
        	 maxChild = maxChildList.get(0).toString();
        	 }
        	 List minChildList=servicePartnerManager.findMinimumChild(serviceOrder.getShipNumber());
        	 if(minChildList!=null && !(minChildList.isEmpty()) && (minChildList.get(0)!=null)){
             minChild =  minChildList.get(0).toString();
        	 }
        	 List countChildList=servicePartnerManager.findCountChild(serviceOrder.getShipNumber());
        	 if(countChildList!=null && !(countChildList.isEmpty()) && (countChildList.get(0)!=null)){
              countChild = countChildList.get(0).toString();
        	 }
          
          
        String errorMsg="";
      	Long routingId=0L;
      	routingId=servicePartner.getId();
      	ServicePartner tempoo=servicePartnerManager.get(routingId);
          if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
        	  Date cal = new Date();
              logger.warn("system Date : "+cal);
              DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
              formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
              String ss =formatter.format(cal);
              logger.warn("system Date After formate : "+ss);
  			try {
  				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
  				Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
  				 logger.warn("system Date After parse : "+currentTimeZoneWise);
     			if(servicePartner.getAtDepart()!=null){
     			if(servicePartner.getAtDepart().after(currentTimeZoneWise)){
     				servicePartner.setAtDepart(tempoo.getAtDepart());
     				String key = "Actual Departure Date.";
     				errorMsg+=key+"<BR>";
     			}
     			}if(servicePartner.getAtArrival()!=null){
     			if(servicePartner.getAtArrival().after(currentTimeZoneWise)){
     				servicePartner.setAtArrival(tempoo.getAtArrival());
     				String key = "Actual Arrival Date.";
     				errorMsg+=key+"<BR>";
					}
					}
				}catch (ParseException e) {
					e.printStackTrace();
				}
			}
          if(!errorMsg.equalsIgnoreCase("")){
				String key1 ="System cannot accept actualizing future dates, Please edit following fields:";
	 				validateFormNav = "NOTOK";
	 				errorMessage(getText(key1+"<BR>"+errorMsg));
	 	            return SUCCESS;
				
			}
    	}else{
    		    servicePartner = new ServicePartner();
	    		serviceOrder = serviceOrderManager.get(sid);
	    		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	    		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
	    		miscellaneous = miscellaneousManager.get(sid); 
	    		billing = billingManager.get(sid);
	        	customerFile = serviceOrder.getCustomerFile();
	    		
	            List list =servicePartnerManager.findTranshipped(serviceOrder.getShipNumber());
	            
	            if(!list.isEmpty()&& list!=null ){
	            	int size=list.size();
	            	Object[] obj=(Object[])list.get(size-1);
	            	if(obj[1] != null){
	            		if(obj[1].toString().equalsIgnoreCase("true")){
	            			if(obj[4]!=null && !obj[4].toString().equals("")){
			            	servicePartner.setCarrierCode(obj[4].toString());
	            			}
	            			if(obj[5]!=null && !obj[5].toString().equals("")){
			            	servicePartner.setCarrierName(obj[5].toString());
	            			}
	            			if(obj[7]!=null && !obj[7].toString().equals("")){
			            	servicePartner.setCntnrNumber(obj[7].toString());
	            			}
			            	if(obj[3]!=null && !obj[3].toString().equals("")){
			            	   	servicePartner.setPolCode(obj[3].toString());
			            	}
			            	if(obj[6]!=null && !obj[6].toString().equals("")){
			            	   	servicePartner.setCarrierDeparture(obj[6].toString());
			            	}
			            }
	            	}
		        }
	            
	            List listClone =servicePartnerManager.findClone(serviceOrder.getShipNumber());
	            
	            if(!listClone.isEmpty() && listClone!=null ){
	            	int size = listClone.size();
	            	Object[] obj = (Object[])listClone.get(size-1);
	            	if(obj[0] != null){
	            		if(obj[0].toString().equalsIgnoreCase("true")){
	            			if(obj[1]!=null && !obj[1].toString().equals("")){
	            			servicePartner.setEtDepart((Date)obj[1]);
	            			}
	            			if(obj[2]!=null && !obj[2].toString().equals("")){
			            	servicePartner.setEtArrival((Date)obj[2]);
	            			}
	            			if(obj[3]!=null && !obj[3].toString().equals("")){
			            	servicePartner.setAtDepart((Date)obj[3]);
	            			}
	            			if(obj[4]!=null && !obj[4].toString().equals("")){
			            	servicePartner.setAtArrival((Date)obj[4]);
	            			}
	            			if(obj[5]!=null && !obj[5].toString().equals("")){
			            	servicePartner.setCarrierCode(obj[5].toString());
	            			}
	            			if(obj[6]!=null && !obj[6].toString().equals("")){
			            	servicePartner.setCarrierName(obj[6].toString());
	            			}
	            			if(obj[7]!=null && !obj[7].toString().equals("")){
			            	servicePartner.setAesNumber(obj[7].toString());
	            			}
	            			if(obj[8]!=null && !obj[8].toString().equals("")){
				            	servicePartner.setRevisedETA((Date)obj[8]);
		            			}
	            			if(obj[9]!=null && !obj[9].toString().equals("")){
				            	servicePartner.setRevisedETD((Date)obj[9]);
		            			}
			            }
	            	}
		        }
	            
	            servicePartner.setSkids("0");
	            servicePartner.setCreatedOn(new Date());
	            servicePartner.setUpdatedOn(new Date());
	            servicePartner.setCorpID(serviceOrder.getCorpID());
	            if(listClone.isEmpty()){
	          	  servicePartner.setBlNumber(trackingStatus.getBillLading());
	            }
	    	}
    	long time4=System.currentTimeMillis();
    	long step2Time=(time4-time3)/1000;
    	System.out.println("\n\n\n\n\n editTime for step2  "+step2Time +" in seconds");
    	long time5=System.currentTimeMillis();
    	List shipSizeList=customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID());
    	if(shipSizeList!=null && !(shipSizeList.isEmpty()) && (shipSizeList.get(0)!=null)){
    	shipSize = shipSizeList.get(0).toString();
    	}
    	List minShipList=customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID());
    	 if(minShipList!=null && !(minShipList.isEmpty()) && (minShipList.get(0)!=null)){
		 minShip =  minShipList.get(0).toString();
    	 }
    	List countShipList=customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID());
    	 if(countShipList!=null && !(countShipList.isEmpty()) && (countShipList.get(0)!=null)){
          countShip =  countShipList.get(0).toString();
    	 }
        if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
        }
    	getNotesForIconChange();
    	containerNumberList= containerNumberList();
    	if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
    		
    		containerNumberList= new ArrayList();
    	}
    	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){
    		fieldsInfo=servicePartnerManager.findRoutingLines(serviceOrder.getId(), sessionCorpID);
    		if(fieldsInfo!=null && !(fieldsInfo.isEmpty())){
    			alreadyExists=true;
    		}
    		else{
    			alreadyExists=false;
    		}
    	}
    	long time6=System.currentTimeMillis();
    	long step3Time=(time6-time5)/1000;
    	System.out.println("\n\n\n\n\n editTime for step3  "+step3Time +" in seconds");
    	long totalTime=(time6-time1)/1000;
    	System.out.println("\n\n\n\n\n total edit time    "+totalTime +" in seconds");
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
    
    
    
    
//  End of Method.
    
    
//  A Method to get the servicePartner list.
	private Billing billing;
	private BillingManager billingManager;
    public String servicePartnerlist() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
  	//getComboList(sessionCorpID);
		serviceOrder = serviceOrderManager.get(id);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		trackingStatus=trackingStatusManager.get(id);
		miscellaneous = miscellaneousManager.get(id); 
		 billing = billingManager.get(id);
		customerFile=serviceOrder.getCustomerFile(); 
		servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));
    	getSession().setAttribute("servicePartnerss", servicePartnerss);

		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	    minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
       countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
       if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
           countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
       }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
  
	
//End of Method.
    
    
//  Method getting the count of notes according to which the icon color changes in form
 	

	@SkipValidation 
    public String getNotesForIconChange() {  
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List m = notesManager.countForServiceNotes(servicePartner.getId());
    	List md = notesManager.countForCarrierNotes(servicePartner.getId());
    	if( m.isEmpty() ){
			countServiceNotes = "0";
		}else {
			countServiceNotes="";
			if((m).get(0)!=null){
			countServiceNotes = ((m).get(0)).toString() ;
			}
		}
		if( md.isEmpty() ){
			countCarrierNotes = "0";
		}else {
			countCarrierNotes ="";
			if((md).get(0)!=null){
			countCarrierNotes = ((md).get(0)).toString() ;
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }   
	
//  End of Method.
    
//  Method used to save and update the record.
    private String divisionFlag;
	private String updateRecords;
	private List listByGrpId=new ArrayList();
	private List listByGrpId1=new ArrayList();
	private List listWithOutLine=new ArrayList();
	private List<ServiceOrder> listByGrpId2=new ArrayList();
	private Long masterId;
	private CustomerFileAction customerFileAction;
    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	long time1=System.currentTimeMillis();
    	if(!servicePartner.getCarrierCode().equals("")){
    	if(carrierCodeValid().equals("input")){
    		String key = "carrierCode is not valid";
    		errorMessage(getText(key));
    		validateFormNav="NOTOK";
            return INPUT;
    	}
    }
    	else{
    		containerNumberList= containerNumberList();
        	if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
        		
        		containerNumberList= new ArrayList();
        	}
    		String key = "Carrier Code is required field.";
    		errorMessage(getText(key));
    		validateFormNav="NOTOK";
            return INPUT;
    	}
    	
    	if((servicePartner.getCarrierCode()!=null) && !(servicePartner.getCarrierCode().toString().trim().equals(""))){
			try{
				List	a1 =servicePartnerManager.getCarrierName(servicePartner.getCarrierCode(),sessionCorpID);
			if(a1.size()>0 && !a1.isEmpty()){
				servicePartner.setCarrierName(a1.get(0).toString());
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
    	
    	if((servicePartner.getSubcarrierCode()!=null) && !(servicePartner.getSubcarrierCode().toString().trim().equals(""))){
			try{
				List	a2 =servicePartnerManager.getCarrierName(servicePartner.getSubcarrierCode(),sessionCorpID);
			if(a2.size()>0 && !a2.isEmpty()){
				servicePartner.setSubcarrierName(a2.get(0).toString());
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
    	
    	if((servicePartner.getSubcarrierCode()==null) || (servicePartner.getSubcarrierCode().toString().trim().equals(""))){
			try{
				servicePartner.setSubcarrierName("");
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
    	
    	
    	long time2=System.currentTimeMillis();
    	long step1Time=(time2-time1)/1000;
    	System.out.println("\n\n\n\n\n saveTime for step1  "+step1Time +" in seconds");
    	long time3=System.currentTimeMillis();
    	boolean isNew = (servicePartner.getId() == null); 
    	if(isNew){
    		serviceOrder = serviceOrderManager.get(sid);
    		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    		
        }else{
        	serviceOrder = serviceOrderManager.get(serviceOrder.getId());
        	sid = serviceOrder.getId();
        	id = servicePartner.getId();
        }
    	
    	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
    	trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
    	billing=billingManager.get(serviceOrder.getId());
    	customerFile=serviceOrder.getCustomerFile();
    	containerNumberList=servicePartnerManager.getContainerNumber(serviceOrder.getShipNumber());
    	String errorMsg="";
    	if(servicePartner.getId()!=null && !servicePartner.getId().equals("")){
    	ServicePartner tempoo=servicePartnerManager.get(servicePartner.getId());    	
    	if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
    		Date cal = new Date();
            logger.warn("system Date : "+cal);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
            formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
            String ss =formatter.format(cal);
            logger.warn("system Date After formate : "+ss);
			try {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
				Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
				 logger.warn("system Date After parse : "+currentTimeZoneWise);
   			if(servicePartner.getAtDepart()!=null){
   			if(servicePartner.getAtDepart().after(currentTimeZoneWise)){
   				servicePartner.setAtDepart(tempoo.getAtDepart());
   				String key = "Actual Departure Date.";
   				errorMsg+=key+"<BR>";
   			}
   			}if(servicePartner.getAtArrival()!=null){
   			if(servicePartner.getAtArrival().after(currentTimeZoneWise)){
   				servicePartner.setAtArrival(tempoo.getAtArrival());
   				String key = "Actual Arrival Date.";
   				errorMsg+=key+"<BR>";
   				}
   			}
   			logger.warn("Error Message : "+errorMsg);
			 } catch (ParseException e) {
					e.printStackTrace();
				}}
    	if(!errorMsg.equalsIgnoreCase("")){
			String key1 ="System cannot accept actualizing future dates, Please edit following fields:";
 				validateFormNav = "NOTOK";
 				errorMessage(getText(key1+"<BR>"+errorMsg));
 				return INPUT;
			
		}
    	}else{
    		if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
    			Date cal = new Date();
                logger.warn("system Date : "+cal);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
                String ss =formatter.format(cal);
                logger.warn("system Date After formate : "+ss);
    			try {
    				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
    				Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
    				 logger.warn("system Date After parse : "+currentTimeZoneWise);
       			if(servicePartner.getAtDepart()!=null){
       			if(servicePartner.getAtDepart().after(currentTimeZoneWise)){
       				String key = "Actual Departure Date.";
       				errorMsg+=key+"<BR>";
       				servicePartner.setAtDepart(null);
       			}
       			}
       			if(servicePartner.getAtArrival()!=null){
       			if(servicePartner.getAtArrival().after(currentTimeZoneWise)){
       				String key = "Actual Arrival Date.";
       				errorMsg+=key+"<BR>";
       				servicePartner.setAtArrival(null);
       				}
       			}
       			if(!errorMsg.equalsIgnoreCase("")){
    				String key1 ="System cannot accept actualizing future dates, Please edit following fields:";
    	 				validateFormNav = "NOTOK";
    	 				errorMessage(getText(key1+"<BR>"+errorMsg));
    	 				return INPUT;
    				
    			}
       		logger.warn("Error Message : "+errorMsg);
			 } catch (ParseException e) {
					e.printStackTrace();
				}}
    	}
    	 
    	servicePartner.setServiceOrder(serviceOrder);
    	  
        //servicePartner.setCorpID(sessionCorpID);
        servicePartner.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	servicePartner.setCreatedOn(new Date());
        }
       
        servicePartner.setUpdatedOn(new Date());
        maxCarrierNumber = serviceOrderManager.findMaximumCarrierNumber(serviceOrder.getShipNumber());
        if ( maxCarrierNumber.get(0) == null ) {          
        	carrierNumber = "01";
        }else {
            autoCarrier = Long.parseLong((maxCarrierNumber).get(0).toString()) + 1;
            if((autoCarrier.toString()).length() == 1) {
            	carrierNumber = "0"+(autoCarrier.toString());
            }else {
            	carrierNumber=autoCarrier.toString();
            }
        }
        if(isNew){
        	servicePartner.setCarrierNumber(carrierNumber);
        }
        else{
        	servicePartner.setCarrierNumber(servicePartner.getCarrierNumber());
        }
        userName=getRequest().getRemoteUser();
        if(isNew){
	        if(!(servicePartner.getCarrierCode() == null || servicePartner.getCarrierCode().equalsIgnoreCase(""))){
	        	String baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
	    		String baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
	    		if(baseCurrencyCompanyDivision.equals("")||baseCurrencyCompanyDivision==null)
	    		{
	    			baseCurrencyCompanyDivision=baseCurrency;
	    		}
	        	Boolean defaultVendorAccLine = false;
				defaultVendorAccLine = trackingStatusManager.getdefaultVendorAccLine(sessionCorpID);
	        	if(defaultVendorAccLine == true && userType!=null && userType.equalsIgnoreCase("USER")){	
	        		List insurarAcc = servicePartnerManager.findCarrierFromAccLine("Freight", servicePartner.getCarrierCode(), servicePartner.getShipNumber());
		        	if(insurarAcc == null || insurarAcc.isEmpty() || insurarAcc.get(0) == null || insurarAcc.get(0).toString().equalsIgnoreCase("")){
		        		accountLine = new AccountLine();
		        		boolean activateAccPortal =true;
		        		try{
		        		if(accountLineAccountPortalFlag){	
		        		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
		        		accountLine.setActivateAccPortal(activateAccPortal);
		        		}
		        		}catch(Exception e){
		        			
		        		}
		        		accountLine.setServiceOrderId(serviceOrder.getId());
		        		accountLine.setCorpID(sessionCorpID);
		        		accountLine.setShipNumber(servicePartner.getShipNumber());
		        		accountLine.setCategory("Freight");
		        		accountLine.setVendorCode(servicePartner.getCarrierCode());
		        		accountLine.setActgCode("");
		        		accountLine.setEstimateVendorName(servicePartner.getCarrierName());
		        		accountLine.setStatus(true);
		        		accountLine.setCreatedBy(servicePartner.getCreatedBy());
		        		accountLine.setBasis("");
		        		accountLine.setBillToCode(billing.getBillToCode());
						accountLine.setBillToName(billing.getBillToName());
						accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
						accountLine.setNetworkBillToName(billing.getNetworkBillToName());
						if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
			    				accountLine.setRecVatPercent(recVatPercent);
					    		}
				    	}
						///Code for AccountLine division 
						if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
							String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
							try{  
								String roles[] = agentRoleValue.split("~");
							  if(roles[3].toString().equals("hauler")){
								   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
									   accountLine.setDivision("01");
								   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
									   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
										   accountLine.setDivision("01");
									   }else{ 
										   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine.setDivision(divisionTemp);
												}else{
													accountLine.setDivision(null);
												}
									     
									   }
								   }else{
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								   }
							  }else{
								  String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							  }
							}catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						///Code for AccountLine division 		
		           	 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
		                if ( maxLineNumber.get(0) == null ) {          
		                	accountLineNumber = "001";
		                }else {
		                	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		                    if((autoLineNumber.toString()).length() == 2) {
		                    	accountLineNumber = "0"+(autoLineNumber.toString());
		                    }
		                    else if((autoLineNumber.toString()).length() == 1) {
		                    	accountLineNumber = "00"+(autoLineNumber.toString());
		                    } 
		                    else {
		                    	accountLineNumber=autoLineNumber.toString();
		                    }
		                }
			            accountLine.setAccountLineNumber(accountLineNumber);
			    		accountLine.setCreatedOn(new Date());
			    		accountLine.setUpdatedOn(new Date());
			    		userName=getRequest().getRemoteUser();
			    		accountLine.setUpdatedBy(userName);
			    		accountLine.setServiceOrder(serviceOrder);			    		
			    		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			    		accountLine.setContract(billing.getContract());
			    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
		    			accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
		    			accountLine.setCountry(baseCurrencyCompanyDivision);
		    			accountLine.setEstValueDate(new Date());
		    			accountLine.setRevisionValueDate(new Date());
		    			accountLine.setValueDate(new Date()); 
		    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
		    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
		    				try{
		    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
		    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
		    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
		    				}catch(Exception e){
		    					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setExchangeRate(new Double(1.0));	
		    				}
		    			}else{
		    				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
		    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
		    				accountLine.setExchangeRate(new Double(1.0));
		    			}
			    		accountLineManager.save(accountLine);
		        	}else{
		        		accountLine = (AccountLine) insurarAcc.get(0);
		        		accountLine.setVendorCode(servicePartner.getCarrierCode());
		        		accountLine.setEstimateVendorName(servicePartner.getCarrierName());
		        		accountLine.setActgCode("");
		        		accountLine.setBasis("");
		        		userName=getRequest().getRemoteUser();
		        		accountLine.setUpdatedBy(userName);
		        		accountLine.setUpdatedOn(new Date());
		        		accountLine.setServiceOrder(serviceOrder);
		        		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
		        		accountLine.setContract(billing.getContract());
		        		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
		    			accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
		        		accountLine.setValueDate(new Date());
		        		accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
	    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
	    				accountLine.setExchangeRate(new Double(1.0));
		        		accountLineManager.save(accountLine);
		        	}
	        	}
	        }
        }else{
        	
        }
        
        try{
    	    if((serviceOrder.getGrpPref()!=null)&&(!serviceOrder.getGrpPref().equals(""))&&(serviceOrder.getGrpPref()==true)){
            	if(serviceOrder.getGrpStatus()!=null){
            		if((serviceOrder.getGrpStatus().indexOf("Finalized")>-1)){
            			List<ServiceOrder> listByGrpId29=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);		
                       	if(listByGrpId29!=null && !(listByGrpId29.isEmpty())){
                       		for (ServiceOrder serviceOrder29 : listByGrpId29) {
                       			List<ServicePartner> serviceParterList = servicePartnerManager.routingList(serviceOrder29.getId());
                       			if(serviceParterList!=null && !(serviceParterList.isEmpty())){
	                       			for (ServicePartner servicePartner1 : serviceParterList) {
	                       				if (servicePartner.getId().toString().equals(servicePartner1.getServicePartnerId().toString())) {
	                       					servicePartner1.setAtDepart(servicePartner.getAtDepart());
		                       				servicePartner1.setAtArrival(servicePartner.getAtArrival());
		                       				servicePartnerManager.save(servicePartner1);
										}
									}}}}}}}
     }catch(Exception e){
    	 e.printStackTrace();
     }
        servicePartner= servicePartnerManager.save(servicePartner);  
        if(isNew && sessionCorpID.equalsIgnoreCase("UGWW")){
        	servicePartner.setUgwIntId(servicePartner.getId().toString());
        	 servicePartner= servicePartnerManager.save(servicePartner);
       	}
        long time4=System.currentTimeMillis();
        long step2Time=(time4-time3)/1000;
    	System.out.println("\n\n\n\n\n saveTime for step2  "+step2Time +" in seconds");
    	 long time5=System.currentTimeMillis();
        serviceOrder=servicePartner.getServiceOrder();
        masterId=servicePartner.getId();
        
        if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
           	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){
           		if(isNew){
           			listByGrpId2=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);		
           		}
           		else{
           			listByGrpId1=servicePartnerManager.routingListByGrpId(servicePartner.getId());
           			listWithOutLine=servicePartnerManager.routingWithNoLine(sessionCorpID,serviceOrder.getId(),servicePartner.getId());
           		}
           	 
           	}
           	
           	if(isNew){
           	if(listByGrpId2!=null && !(listByGrpId2.isEmpty())){
           		Iterator it=listByGrpId2.iterator();
           		while(it.hasNext()){
           			ServiceOrder tempOrder =(ServiceOrder)it.next();
           			ServicePartner tempRouting=new ServicePartner();
           			TrackingStatus tempStatus=trackingStatusManager.get(tempOrder.getId()); 
           			tempRouting.setActgCode(servicePartner.getActgCode());
           			tempRouting.setAesNumber(servicePartner.getAesNumber());
           			tempRouting.setAtArrival(servicePartner.getAtArrival());
           			tempRouting.setAtDepart(servicePartner.getAtDepart());
           			tempRouting.setBlNumber(servicePartner.getBlNumber());
         			tempRouting.setHblNumber(servicePartner.getHblNumber());
           			tempRouting.setBookNumber(servicePartner.getBookNumber());
           			tempRouting.setCarrierArrival(servicePartner.getCarrierArrival());
           			tempRouting.setCarrierClicd(servicePartner.getCarrierClicd());
           			tempRouting.setCarrierCode(servicePartner.getCarrierCode());
           			tempRouting.setCarrierDeparture(servicePartner.getCarrierDeparture());
           			tempRouting.setCarrierName(servicePartner.getCarrierName());
           			tempRouting.setCarrierPhone(servicePartner.getCarrierPhone());
           			tempRouting.setCarrierVessels(servicePartner.getCarrierVessels());
           			tempRouting.setCity(servicePartner.getCity());
           			tempRouting.setClone(servicePartner.getClone());
           			tempRouting.setCntnrNumber(servicePartner.getCntnrNumber());
           			tempRouting.setCoord(servicePartner.getCoord());
           			
           			tempRouting.setDropPick(servicePartner.getDropPick());
           			tempRouting.setEmail(servicePartner.getEmail());
           			tempRouting.setEtArrival(servicePartner.getEtArrival());
           			tempRouting.setETATA(servicePartner.getETATA());
           			tempRouting.setEtDepart(servicePartner.getEtDepart());
           			tempRouting.setETDTA(servicePartner.getETDTA());
           			tempRouting.setHouseBillIssued(servicePartner.getHouseBillIssued());
           			tempRouting.setLiveLoad(servicePartner.getLiveLoad());
           			tempRouting.setMiles(servicePartner.getMiles());
           			tempRouting.setOmni(servicePartner.getOmni());
           			tempRouting.setPartnerType(servicePartner.getPartnerType());
           			tempRouting.setPdCost1(servicePartner.getPdCost1());
           			tempRouting.setPdCost2(servicePartner.getPdCost2());
           			tempRouting.setPdDays2(servicePartner.getPdDays2());
           			tempRouting.setPdFreedays(servicePartner.getPdFreedays());
           			tempRouting.setPoeCode(servicePartner.getPoeCode());
           			tempRouting.setPolCode(servicePartner.getPolCode());
           			tempRouting.setSkids(servicePartner.getSkids());
           			tempRouting.setTranshipped(servicePartner.getTranshipped());
           			tempRouting.setUpdatedBy(servicePartner.getUpdatedBy());
           			tempRouting.setUpdatedOn(servicePartner.getUpdatedOn());
           			tempRouting.setServicePartnerId(servicePartner.getId());
           			tempRouting.setServiceOrder(tempOrder);
           			tempRouting.setServiceOrderId(tempOrder.getId());
           			tempRouting.setSequenceNumber(tempOrder.getSequenceNumber());
           			tempRouting.setShip(tempOrder.getShip());
           			tempRouting.setShipNumber(tempOrder.getShipNumber());
           			tempRouting.setCorpID(servicePartner.getCorpID());
           			tempRouting.setRevisedETA(servicePartner.getRevisedETA());
           			tempRouting.setRevisedETD(servicePartner.getRevisedETD());
           			
           			tempRouting.setCreatedOn(new Date());

           		  maxCarrierNumber = serviceOrderManager.findMaximumCarrierNumber(tempOrder.getShipNumber());
           		        if ( maxCarrierNumber.get(0) == null ) {          
           		        	carrierNumber = "01";
           		        }else {
           		            autoCarrier = Long.parseLong((maxCarrierNumber).get(0).toString()) + 1;
           		            if((autoCarrier.toString()).length() == 1) {
           		            	carrierNumber = "0"+(autoCarrier.toString());
           		            }else {
           		            	carrierNumber=autoCarrier.toString();
           		            }
           		        }

           		     tempRouting.setCarrierNumber(carrierNumber);
           		  tempRouting=servicePartnerManager.save(tempRouting);
           			try{
           	            if(tempOrder.getIsNetworkRecord()){
           	            	List linkedShipNumber=findLinkedShipNumber(tempOrder);
           	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
           	    			synchornizeRouting(serviceOrderRecords,tempRouting,isNew,tempOrder,tempStatus); 

           	    		}
           	          
           	            }catch(Exception ex){
           	    			System.out.println("\n\n\n\n error------->"+ex);
           	    			 ex.printStackTrace();
           	    		}
    			
           		}
           		
           	}
           	
           	}
           	else{
              	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
               		Iterator it=listByGrpId1.iterator();
               		while(it.hasNext()){
               		//	Long tempId=new Long(it.next().toString());
               			ServicePartner tempRouting=(ServicePartner)it.next();
               			ServiceOrder tempOrder =serviceOrderManager.get(tempRouting.getServiceOrderId());
               			TrackingStatus tempStatus=trackingStatusManager.get(tempOrder.getId()); 
               			tempRouting.setActgCode(servicePartner.getActgCode());
               			tempRouting.setAesNumber(servicePartner.getAesNumber());
               			tempRouting.setAtArrival(servicePartner.getAtArrival());
               			tempRouting.setAtDepart(servicePartner.getAtDepart());
               			tempRouting.setBlNumber(servicePartner.getBlNumber());
               			tempRouting.setHblNumber(servicePartner.getHblNumber());
               			tempRouting.setBookNumber(servicePartner.getBookNumber());
               			tempRouting.setCarrierArrival(servicePartner.getCarrierArrival());
               			tempRouting.setCarrierClicd(servicePartner.getCarrierClicd());
               			tempRouting.setCarrierCode(servicePartner.getCarrierCode());
               			tempRouting.setCarrierDeparture(servicePartner.getCarrierDeparture());
               			tempRouting.setCarrierName(servicePartner.getCarrierName());
               			tempRouting.setCarrierPhone(servicePartner.getCarrierPhone());
               			tempRouting.setCarrierVessels(servicePartner.getCarrierVessels());
               			tempRouting.setCity(servicePartner.getCity());
               			tempRouting.setClone(servicePartner.getClone());
               			tempRouting.setCntnrNumber(servicePartner.getCntnrNumber());
               			tempRouting.setCoord(servicePartner.getCoord());
               			
               			tempRouting.setDropPick(servicePartner.getDropPick());
               			tempRouting.setEmail(servicePartner.getEmail());
               			tempRouting.setEtArrival(servicePartner.getEtArrival());
               			tempRouting.setETATA(servicePartner.getETATA());
               			tempRouting.setEtDepart(servicePartner.getEtDepart());
               			tempRouting.setETDTA(servicePartner.getETDTA());
               			tempRouting.setHouseBillIssued(servicePartner.getHouseBillIssued());
               			tempRouting.setLiveLoad(servicePartner.getLiveLoad());
               			tempRouting.setMiles(servicePartner.getMiles());
               			tempRouting.setOmni(servicePartner.getOmni());
               			tempRouting.setPartnerType(servicePartner.getPartnerType());
               			tempRouting.setPdCost1(servicePartner.getPdCost1());
               			tempRouting.setPdCost2(servicePartner.getPdCost2());
               			tempRouting.setPdDays2(servicePartner.getPdDays2());
               			tempRouting.setPdFreedays(servicePartner.getPdFreedays());
               			tempRouting.setPoeCode(servicePartner.getPoeCode());
               			tempRouting.setPolCode(servicePartner.getPolCode());
               			tempRouting.setSkids(servicePartner.getSkids());
               			tempRouting.setTranshipped(servicePartner.getTranshipped());
               			tempRouting.setUpdatedBy(servicePartner.getUpdatedBy());
               			tempRouting.setUpdatedOn(servicePartner.getUpdatedOn());
               			tempRouting.setRevisedETA(servicePartner.getRevisedETA());
               			tempRouting.setRevisedETD(servicePartner.getRevisedETD());
               			tempRouting.setStatus(servicePartner.isStatus());
               			tempRouting=servicePartnerManager.save(tempRouting);
               			try{
               	            if(tempOrder.getIsNetworkRecord()){
               	            	List linkedShipNumber=findLinkedShipNumber(tempOrder);
               	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
               	    			synchornizeRouting(serviceOrderRecords,tempRouting,false,tempOrder,tempStatus);
               	    		}
               	          
               	            }catch(Exception ex){
               	    			System.out.println("\n\n\n\n error------->"+ex);
               	    			 ex.printStackTrace();
               	    		}
               		}
               		
               	}
           		
           	}
           	if(!isNew){
            	if(listWithOutLine!=null && !(listWithOutLine.isEmpty())){
               		Iterator it=listWithOutLine.iterator();
               		while(it.hasNext()){
               			ServiceOrder tempOrder =serviceOrderManager.get(new Long((it.next().toString())));
               			ServicePartner tempRouting=new ServicePartner();
               			TrackingStatus tempStatus=trackingStatusManager.get(tempOrder.getId()); 
               			tempRouting.setActgCode(servicePartner.getActgCode());
               			tempRouting.setAesNumber(servicePartner.getAesNumber());
               			tempRouting.setAtArrival(servicePartner.getAtArrival());
               			tempRouting.setAtDepart(servicePartner.getAtDepart());
               			tempRouting.setBlNumber(servicePartner.getBlNumber());
             			tempRouting.setHblNumber(servicePartner.getHblNumber());
               			tempRouting.setBookNumber(servicePartner.getBookNumber());
               			tempRouting.setCarrierArrival(servicePartner.getCarrierArrival());
               			tempRouting.setCarrierClicd(servicePartner.getCarrierClicd());
               			tempRouting.setCarrierCode(servicePartner.getCarrierCode());
               			tempRouting.setCarrierDeparture(servicePartner.getCarrierDeparture());
               			tempRouting.setCarrierName(servicePartner.getCarrierName());
               			tempRouting.setCarrierPhone(servicePartner.getCarrierPhone());
               			tempRouting.setCarrierVessels(servicePartner.getCarrierVessels());
               			tempRouting.setCity(servicePartner.getCity());
               			tempRouting.setClone(servicePartner.getClone());
               			tempRouting.setCntnrNumber(servicePartner.getCntnrNumber());
               			tempRouting.setCoord(servicePartner.getCoord());
               			
               			tempRouting.setDropPick(servicePartner.getDropPick());
               			tempRouting.setEmail(servicePartner.getEmail());
               			tempRouting.setEtArrival(servicePartner.getEtArrival());
               			tempRouting.setETATA(servicePartner.getETATA());
               			tempRouting.setEtDepart(servicePartner.getEtDepart());
               			tempRouting.setETDTA(servicePartner.getETDTA());
               			tempRouting.setHouseBillIssued(servicePartner.getHouseBillIssued());
               			tempRouting.setLiveLoad(servicePartner.getLiveLoad());
               			tempRouting.setMiles(servicePartner.getMiles());
               			tempRouting.setOmni(servicePartner.getOmni());
               			tempRouting.setPartnerType(servicePartner.getPartnerType());
               			tempRouting.setPdCost1(servicePartner.getPdCost1());
               			tempRouting.setPdCost2(servicePartner.getPdCost2());
               			tempRouting.setPdDays2(servicePartner.getPdDays2());
               			tempRouting.setPdFreedays(servicePartner.getPdFreedays());
               			tempRouting.setPoeCode(servicePartner.getPoeCode());
               			tempRouting.setPolCode(servicePartner.getPolCode());
               			tempRouting.setSkids(servicePartner.getSkids());
               			tempRouting.setTranshipped(servicePartner.getTranshipped());
               			tempRouting.setUpdatedBy(servicePartner.getUpdatedBy());
               			tempRouting.setUpdatedOn(servicePartner.getUpdatedOn());
               			tempRouting.setServicePartnerId(servicePartner.getId());
               			tempRouting.setServiceOrder(tempOrder);
               			tempRouting.setServiceOrderId(tempOrder.getId());
               			tempRouting.setSequenceNumber(tempOrder.getSequenceNumber());
               			tempRouting.setShip(tempOrder.getShip());
               			tempRouting.setShipNumber(tempOrder.getShipNumber());
               			tempRouting.setCorpID(servicePartner.getCorpID());
               			tempRouting.setRevisedETA(servicePartner.getRevisedETA());
               			tempRouting.setRevisedETD(servicePartner.getRevisedETD());
               			
               			tempRouting.setCreatedOn(new Date());

               		  maxCarrierNumber = serviceOrderManager.findMaximumCarrierNumber(tempOrder.getShipNumber());
               		        if ( maxCarrierNumber.get(0) == null ) {          
               		        	carrierNumber = "01";
               		        }else {
               		            autoCarrier = Long.parseLong((maxCarrierNumber).get(0).toString()) + 1;
               		            if((autoCarrier.toString()).length() == 1) {
               		            	carrierNumber = "0"+(autoCarrier.toString());
               		            }else {
               		            	carrierNumber=autoCarrier.toString();
               		            }
               		        }

               		     tempRouting.setCarrierNumber(carrierNumber);
               			servicePartnerManager.save(tempRouting);
               			try{
               	            if(tempOrder.getIsNetworkRecord()){
               	            	List linkedShipNumber=findLinkedShipNumber(tempOrder);
               	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
               	    			synchornizeRouting(serviceOrderRecords,tempRouting,isNew,tempOrder,tempStatus);
               	    		}
               	          
               	            }catch(Exception ex){
               	    			System.out.println("\n\n\n\n error------->"+ex);
               	    			 ex.printStackTrace();
               	    		}
        			
               		}
               		
               	}
           		
           	}
           }
        long time6=System.currentTimeMillis(); 
        long step3Time=(time6-time5)/1000;
    	System.out.println("\n\n\n\n\n saveTime for step3  "+step3Time +" in seconds");
    	long time7=System.currentTimeMillis(); 
        
    	// COMMENTED .... PLACED IT DOWN 
    	/*try{
        if(serviceOrder.getIsNetworkRecord()){
        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeRouting(serviceOrderRecords,servicePartner,isNew,serviceOrder);
		}
        Long StartTime = System.currentTimeMillis();
		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
			if(agentShipNumber!=null && !agentShipNumber.equals(""))
				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
   			
   			}
			
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");
   
        }catch(Exception ex){
			System.out.println("\n\n\n\n error------->"+ex);
			 ex.printStackTrace();
		}*/
    	
        List companyDetailsList=companyDivisionManager.findCompanyCodeByBookingAgent(serviceOrder.getBookingAgentCode(), sessionCorpID); 
        if(serviceOrder.getJob().toString().trim().equalsIgnoreCase("INT") && serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("HOLL") 
	    		&& companyDetailsList!=null && !companyDetailsList.isEmpty() && companyDetailsList.get(0)!=null){
			Long StartTime = System.currentTimeMillis();
			customerFileAction.createIntegrationLogInfo(serviceOrder.getShipNumber(),"CREATE","SP");
			Long timeTaken =  System.currentTimeMillis() - StartTime;
    		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");
        }
    	
        if(servicePartner.getClone() != null && servicePartner.getClone() && totalClone > 0){
     	   serviceCloneSave(servicePartner, serviceOrder);
        }
        long time8=System.currentTimeMillis(); 
        long step4Time=(time8-time7)/1000;
    	System.out.println("\n\n\n\n\n saveTime for step4  "+step4Time +" in seconds");
    	long time9=System.currentTimeMillis(); 
    	/*servicePartnerManager.updateTrack(servicePartner.getShipNumber(),servicePartner.getBlNumber(),userName);
        servicePartnerManager.updateServiceOrder(servicePartner.getCarrierDeparture(), servicePartner.getCarrierArrival(), servicePartner.getShipNumber(),userName);
    	servicePartnerManager.updateServiceOrderDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),userName );
        servicePartnerManager.updateServiceOrderActualDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),serviceOrder.getCustomerFileId(),userName);*/
//      *************************************Buisness logic for status field********************************
        if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
        {  
        	serviceOrder=serviceOrderManager.get(serviceOrder.getId()); 
        	if((serviceOrder.getStatusNumber().intValue()<30)&& (Integer.parseInt((serviceOrderManager.getATDDate(serviceOrder.getId())).get(0).toString())>0)){
        		  String status="TRNS";
	     		  int statusNumber=30;
	     		//Bug 11839 - Possibility to modify Current status to Prior status
	     		  //serviceOrder.setStatus("TRNS");
	     		 // serviceOrder.setStatusNumber(30);
	     		  //serviceOrder.setStatusDate(new Date());
	     		  int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser()); 
          }
     	   if((serviceOrder.getStatusNumber().intValue()<40)&& (Integer.parseInt((serviceOrderManager.getATADate(serviceOrder.getId())).get(0).toString())>0)){
     		      String status="DSRV";
     		      int statusNumber=40;
     		      //serviceOrder.setStatus("DSRV");
	     		  //serviceOrder.setStatusNumber(40);
	     		  //serviceOrder.setStatusDate(new Date());
     		      int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser()); 
    		 } 
     	   
     	   
           try{
        	   if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
		       	    if((serviceOrder.getGrpPref()!=null)&&(!serviceOrder.getGrpPref().equals(""))&&(serviceOrder.getGrpPref()==true)){
		               	if(serviceOrder.getGrpStatus()!=null){
		               		if((serviceOrder.getGrpStatus().indexOf("Finalized")>-1)){/*
		               			List<ServiceOrder> listByGrpId29=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);		
		                          	if(listByGrpId29!=null && !(listByGrpId29.isEmpty())){
		                          		Iterator it4=listByGrpId29.iterator();
		                          		while(it4.hasNext()){
		                          			ServiceOrder tempOrder =(ServiceOrder)it4.next();
		                          		//Bug 11839 - Possibility to modify Current status to Prior status
		                          			//tempOrder.setStatus(serviceOrder.getStatus());
		                          			serviceOrderManager.save(tempOrder);
		                          		}}*/}}}
        	   }
           }catch(Exception e){
        	   e.printStackTrace();
           }     	   
        }
        
       
		   // **************** AutoPopulate trackingStatus deliveryA Start ***********************************************************************
		try{
			List al=new ArrayList();
			al.add("FOTDP");
			al.add("D/DP");
			al.add("Warehouse to Destination Port");
			al.add("OP/DP");
			al.add("D/P");
			al.add("W/P");	
				List temp=servicePartnerManager.findTranshipChild(serviceOrder.getShipNumber());
				if((temp!=null)&&(!temp.isEmpty())&&(temp.get(0)!=null)&&(!temp.get(0).toString().equalsIgnoreCase(""))){					
					Long spid=Long.parseLong(temp.get(0).toString());
					ServicePartner newService=servicePartnerManager.get(spid);
					if((!newService.getTranshipped())&&(serviceOrder.getServiceType()!=null)&&(!serviceOrder.getServiceType().equalsIgnoreCase(""))&&(al.contains(serviceOrder.getServiceType()))&&(trackingStatus.getDeliveryA()==null)&&(!serviceOrder.getIsNetworkRecord())){					
							if(newService.getAtArrival()!=null){
							trackingStatus.setDeliveryA(newService.getAtArrival());
							trackingStatus.setUpdatedBy(newService.getUpdatedBy());
							trackingStatus.setUpdatedOn(new Date());
							trackingStatus=trackingStatusManager.save(trackingStatus);
							//Bug 11839 - Possibility to modify Current status to Prior status
							//serviceOrder.setStatus("DLVR");
							//serviceOrder.setStatusNumber(60);
							serviceOrder.setUpdatedBy(newService.getUpdatedBy());
							serviceOrder.setUpdatedOn(new Date());
							//serviceOrder=serviceOrderManager.save(serviceOrder);
							 int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),"DLVR",60,sessionCorpID,getRequest().getRemoteUser()); 
						}
				    }
			    }	        	  
		}catch(Exception e){
			 e.printStackTrace();
		}
		if((sessionCorpID.equals("VOER") || sessionCorpID.equals("VORU") || sessionCorpID.equals("VOCZ") || sessionCorpID.equals("VOAO")|| sessionCorpID.equals("BONG")) && serviceOrder.getServiceType()!=null &&(!serviceOrder.getServiceType().equalsIgnoreCase("")) && (serviceOrder.getServiceType().equals("D/DP")|| serviceOrder.getServiceType().equalsIgnoreCase("Free on Truck to Destination Port")|| serviceOrder.getServiceType().equalsIgnoreCase("Warehouse to Destination Port") || serviceOrder.getServiceType().equalsIgnoreCase("OP/DP"))){
			List maxAtArrival=servicePartnerManager.getMaxAtArrivalDate(serviceOrder.getId(),sessionCorpID);
			if(maxAtArrival!=null && !maxAtArrival.isEmpty() && maxAtArrival.get(0)!=null){
				ServicePartner  maxServicePartner = servicePartnerManager.get(Long.parseLong(maxAtArrival.get(0).toString()));
				trackingStatus.setServiceCompleteDate(maxServicePartner.getAtArrival());
				trackingStatus.setUpdatedBy(maxServicePartner.getUpdatedBy());
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus=trackingStatusManager.save(trackingStatus);	
			}
			
		}
		
		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){
			if((serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(serviceOrder.getBookingAgentShipNumber());
			}
			if((trackingStatus.getBookingAgentExSO()!=null) && (!trackingStatus.getBookingAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(trackingStatus.getBookingAgentExSO());
			}
			if((trackingStatus.getNetworkAgentExSO()!=null) && (!trackingStatus.getNetworkAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(trackingStatus.getNetworkAgentExSO());
			}	
			if(linkedshipNumber!=null && !linkedshipNumber.isEmpty() && linkedshipNumber.size()>0 && trackingStatus.getServiceCompleteDate()!=null){
				trackingStatusManager.updateLinkedShipNumberforCompletedDate(linkedshipNumber,trackingStatus.getServiceCompleteDate(),sessionCorpID,getRequest().getRemoteUser());
			}
		}
       // **************** AutoPopulate trackingStatus deliveryA End ***********************************************************************
        
//      *************************************End of Buisness logic for status field********************************   
		// **********************************Syncing routing ***************************************************** 
        try{
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			synchornizeRouting(serviceOrderRecords,servicePartner,isNew,serviceOrder,trackingStatus);
    		}
            /*Long StartTime = System.currentTimeMillis();
    		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
    			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getSequenceNumber(),sessionCorpID);
    			if(agentShipNumber!=null && !agentShipNumber.equals(""))
    				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
       			
       			}
    			
    		Long timeTaken =  System.currentTimeMillis() - StartTime;
    		logger.warn("\n\nTime taken for creating line in integration log info table : "+timeTaken+"\n\n");*/
       
            }catch(Exception ex){
    			System.out.println("\n\n\n\n error------->"+ex);
    			 ex.printStackTrace();
    		}
        //************************************End Of syncing routing *******************************************
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){ 
        String  key = (isNew) ? "servicePartner.added" : "servicePartner.updated";   
        saveMessage(getText(key));   
        }
        long time10=System.currentTimeMillis(); 
        long step5Time=(time10-time9)/1000;
    	System.out.println("\n\n\n\n\n saveTime for step5  "+step5Time +" in seconds");
    	long time11=System.currentTimeMillis(); 
    	userName=getRequest().getRemoteUser();
    	servicePartnerManager.updateTrack(servicePartner.getShipNumber(),servicePartner.getBlNumber(),servicePartner.getHblNumber(),userName);
        servicePartnerManager.updateServiceOrder(servicePartner.getCarrierDeparture(), servicePartner.getCarrierArrival(), servicePartner.getShipNumber(),userName);
    	servicePartnerManager.updateServiceOrderDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),userName );
        servicePartnerManager.updateServiceOrderActualDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),serviceOrder.getCustomerFileId(),userName);
    	/*servicePartnerManager.updateTrack(servicePartner.getShipNumber(),servicePartner.getBlNumber(),userName);
        servicePartnerManager.updateServiceOrder(servicePartner.getCarrierDeparture(), servicePartner.getCarrierArrival(), servicePartner.getShipNumber(),userName);
        servicePartnerManager.updateServiceOrderDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),userName );
        servicePartnerManager.updateServiceOrderActualDate(servicePartner.getServiceOrderId(),servicePartner.getCorpID(),serviceOrder.getCustomerFileId(),userName);*/
        if (!isNew) {   
        	serviceOrder = serviceOrderManager.get(serviceOrder.getId());
        	servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));
            getSession().setAttribute("servicePartners", servicePartners);
            long time12=System.currentTimeMillis(); 
            long step6Time=(time12-time11)/1000;
        	System.out.println("\n\n\n\n\n saveTime for step6  "+step6Time +" in seconds");
            long totaltime=(time12-time1)/1000;
        	System.out.println("\n\n\n\n\n total Save Time    "+totaltime +" in seconds");
            hitFlag="1";
            return SUCCESS;   
        } else {   
        //	maxId = Long.parseLong(servicePartnerManager.findMaxId().get(0).toString());
            servicePartner = servicePartnerManager.get(masterId);
            serviceOrder = servicePartner.getServiceOrder();
        }
        
        serviceOrder = serviceOrderManager.get(serviceOrder.getId());
        servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));
        getSession().setAttribute("servicePartners", servicePartners); 
        hitFlag="1";
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;  
    }
    
//  End of Method.
    
    private void synchornizeRouting(List<Object> serviceOrderRecords,ServicePartner servicePartner, boolean isNew,ServiceOrder serviceOrderold,TrackingStatus trackingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		TrackingStatus trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	
    			if(isNew){
    				ServicePartner servicePartnerNew= new ServicePartner();
    				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(servicePartnerNew, servicePartner);
    				
    				servicePartnerNew.setId(null);
    				servicePartnerNew.setCarrierCode("T10000");
    				servicePartnerNew.setCorpID(serviceOrder.getCorpID());
    				servicePartnerNew.setShipNumber(serviceOrder.getShipNumber());
    				servicePartnerNew.setSequenceNumber(serviceOrder.getSequenceNumber());
    				servicePartnerNew.setServiceOrder(serviceOrder);
    				servicePartnerNew.setServiceOrderId(serviceOrder.getId());
    				servicePartnerNew.setCreatedBy("Networking");
    				servicePartnerNew.setUpdatedBy("Networking");
    				servicePartnerNew.setCreatedOn(new Date());
    				servicePartnerNew.setUpdatedOn(new Date()); 
    				servicePartnerNew = servicePartnerManager.save(servicePartnerNew); 
    				
    				 if(servicePartnerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 servicePartnerNew.setUgwIntId(servicePartnerNew.getId().toString());
						 }
					 servicePartnerNew= servicePartnerManager.save(servicePartnerNew);
					 userName=getRequest().getRemoteUser();
					 servicePartnerManager.updateTrack(servicePartnerNew.getShipNumber(),servicePartnerNew.getBlNumber(),servicePartnerNew.getHblNumber(),userName);
	    	    	 servicePartnerManager.updateServiceOrder(servicePartnerNew.getCarrierDeparture(), servicePartnerNew.getCarrierArrival(), servicePartnerNew.getShipNumber(),"Networking");
	    	    	 servicePartnerManager.updateServiceOrderDate(servicePartnerNew.getServiceOrderId(),serviceOrder.getCorpID(),"Networking" );
	    	    	 servicePartnerManager.updateServiceOrderActualDate(servicePartnerNew.getServiceOrderId(),serviceOrder.getCorpID(),serviceOrder.getCustomerFileId(),"Networking");
	    	    	
    			}else{
    				ServicePartner servicePartnerTo= servicePartnerManager.getForOtherCorpid(servicePartnerManager.findRemoteServicePartner(servicePartner.getCarrierNumber(),serviceOrder.getId()));  
        			Long id=servicePartnerTo.getId();
        			String createdBy=servicePartnerTo.getCreatedBy();
        			Date createdOn=servicePartnerTo.getCreatedOn();
        			String carrierCode=servicePartnerTo.getCarrierCode();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(servicePartnerTo, servicePartner);
    				servicePartnerTo.setCarrierCode(carrierCode); 
    				servicePartnerTo.setId(id);
    				servicePartnerTo.setCreatedBy(createdBy);
    				servicePartnerTo.setCreatedOn(createdOn);
    				servicePartnerTo.setCorpID(serviceOrder.getCorpID());
    				servicePartnerTo.setShipNumber(serviceOrder.getShipNumber());
    				servicePartnerTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				servicePartnerTo.setServiceOrder(serviceOrder);
    				servicePartnerTo.setServiceOrderId(serviceOrder.getId());
    				servicePartnerTo.setUpdatedBy(servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				servicePartnerTo.setUpdatedOn(new Date()); 
    				servicePartnerTo = servicePartnerManager.save(servicePartnerTo);
    				
    				 if(servicePartnerTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					 servicePartnerTo.setUgwIntId(servicePartnerTo.getId().toString());
						 }
    				 servicePartnerTo= servicePartnerManager.save(servicePartnerTo);
    				userName=getRequest().getRemoteUser();
    				servicePartnerManager.updateTrack(servicePartnerTo.getShipNumber(),servicePartnerTo.getBlNumber(),servicePartnerTo.getHblNumber(),userName);
    	    	    servicePartnerManager.updateServiceOrder(servicePartnerTo.getCarrierDeparture(), servicePartnerTo.getCarrierArrival(), servicePartnerTo.getShipNumber(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    	    	    servicePartnerManager.updateServiceOrderDate(servicePartnerTo.getServiceOrderId(),serviceOrder.getCorpID(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser() );
    	    	    servicePartnerManager.updateServiceOrderActualDate(servicePartnerTo.getServiceOrderId(),serviceOrder.getCorpID(),serviceOrder.getCustomerFileId(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    	    	
    			}
    			if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
    	        {
    				if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecod.getSoNetworkGroup()){     		     		  
    				   /*serviceOrder.setStatus(serviceOrderold.getStatus());
    				   serviceOrder.setStatusNumber(serviceOrderold.getStatusNumber());
    				   serviceOrder.setStatusDate(serviceOrderold.getStatusDate());
    				   serviceOrder.setUpdatedBy(servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				   serviceOrder.setUpdatedOn(new Date());
    		     	   serviceOrderManager.save(serviceOrder);*/
    				   serviceOrderManager.updateStorageJob(serviceOrder.getId(),serviceOrderold.getStatus(),serviceOrderold.getStatusNumber(),serviceOrder.getCorpID(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    				}
    	    		}
    			}
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing servicePartner"+ex);	
    			 ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		List<Object> recordList= new ArrayList();
		Iterator it =linkedShipNumber.iterator();
		while(it.hasNext()){
			String shipNumber= it.next().toString();
			if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
    			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    			recordList.add(serviceOrderRemote);
    		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return recordList;
	}

	private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		List linkedShipNumberList= new ArrayList();
		if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		}else{
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return linkedShipNumberList;
	}

	public String carrierCodeValid()throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
     String carrier=	servicePartner.getCarrierCode();
     carrier=carrier.trim();
     if(servicePartnerManager.findByCarrierCode(carrier).isEmpty()){
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return INPUT;
     }
     else{
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return SUCCESS;
     }
    }
    
    @SkipValidation
    public String copyCarrierAtd(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
    	copyCarrierAtd=servicePartnerManager.copyAtd(carrierATD, shipNumber);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    @SkipValidation
    public String copyCarrierAta(){
     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
    	copyCarrierAta=servicePartnerManager.copyAta(carrierATA, shipNumber);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    private String fieldName;
    private String fieldValue;
    private String fieldName1;
    private String fieldValue1;
    private String tableName;
    @SkipValidation
   	public String updateServicePartnerDetailsAjax(){
       		userName=getRequest().getRemoteUser();
       		servicePartnerManager.updateServicePartnerDetails(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,sessionCorpID,userName);
			servicePartner =servicePartnerManager.get(id);
			serviceOrder=servicePartner.getServiceOrder();
			trackingStatus=trackingStatusManager.get(serviceOrder.getId());
			boolean isNew = (servicePartner.getId() == null);
            if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
               	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){              
               			listByGrpId1=servicePartnerManager.routingListByGrpId(servicePartner.getId());
               		}               	 
               	}
            
            if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
           		Iterator it=listByGrpId1.iterator();
           		while(it.hasNext()){
           			ServicePartner tempRouting=(ServicePartner)it.next();
           			ServiceOrder tempOrder =serviceOrderManager.get(tempRouting.getServiceOrderId());
        			TrackingStatus trackingStatus=trackingStatusManager.get(tempOrder.getId());

           			tempRouting.setCarrierArrival(servicePartner.getCarrierArrival());
           			tempRouting.setCarrierCode(servicePartner.getCarrierCode());
           			tempRouting.setCarrierDeparture(servicePartner.getCarrierDeparture());
           			tempRouting.setCarrierName(servicePartner.getCarrierName());          
           			tempRouting.setPoeCode(servicePartner.getPoeCode());
           			tempRouting.setPolCode(servicePartner.getPolCode());
           			tempRouting.setEtArrival(servicePartner.getEtArrival());
           			tempRouting.setEtDepart(servicePartner.getEtDepart());
           			tempRouting.setAtArrival(servicePartner.getAtArrival());
           			tempRouting.setAtDepart(servicePartner.getAtDepart());
           			
           			tempRouting.setUpdatedBy(servicePartner.getUpdatedBy());
           			tempRouting.setUpdatedOn(servicePartner.getUpdatedOn());
           			tempRouting=servicePartnerManager.save(tempRouting);
           			try{
           				if(tempOrder.getIsNetworkRecord()){
           	            	List linkedShipNumber=findLinkedShipNumber(tempOrder);
           	    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
           	    			synchornizeRouting(serviceOrderRecords,tempRouting,false,tempOrder,trackingStatus);
           	    		} 
               	}catch(Exception e){
               		e.printStackTrace();
               	}	       
           		}
           		
           	}
            servicePartnerManager.updateServiceOrder(servicePartner.getCarrierDeparture(), servicePartner.getCarrierArrival(), servicePartner.getShipNumber(),userName);
            servicePartnerManager.updateServiceOrderDate(servicePartner.getServiceOrderId(),sessionCorpID,userName );
            servicePartnerManager.updateServiceOrderActualDate(servicePartner.getServiceOrderId(),sessionCorpID,serviceOrder.getCustomerFileId(),userName);
            if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
            {  
            	serviceOrder=serviceOrderManager.get(serviceOrder.getId()); 
            	if((serviceOrder.getStatusNumber().intValue()<30)&& (Integer.parseInt((serviceOrderManager.getATDDate(serviceOrder.getId())).get(0).toString())>0)){
            		  String status="TRNS";
    	     		  int statusNumber=30;
    	     		//Bug 11839 - Possibility to modify Current status to Prior status
    	     		 // serviceOrder.setStatus("TRNS");
    	     		  //serviceOrder.setStatusNumber(30);
    	     		 // serviceOrder.setStatusDate(new Date());
    	     		  int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser()); 
              }
         	   if((serviceOrder.getStatusNumber().intValue()<40)&& (Integer.parseInt((serviceOrderManager.getATADate(serviceOrder.getId())).get(0).toString())>0)){
         		      String status="DSRV";
         		      int statusNumber=40;
         		   //Bug 11839 - Possibility to modify Current status to Prior status
         		      //serviceOrder.setStatus("DSRV");
    	     		  //serviceOrder.setStatusNumber(40);
    	     		 // serviceOrder.setStatusDate(new Date());
         		      int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser()); 
        		 } 
         	   
         	   
               try{
            	   if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
    		       	    if((serviceOrder.getGrpPref()!=null)&&(!serviceOrder.getGrpPref().equals(""))&&(serviceOrder.getGrpPref()==true)){
    		               	if(serviceOrder.getGrpStatus()!=null){
    		               		if((serviceOrder.getGrpStatus().indexOf("Finalized")>-1)){/*
    		               			List<ServiceOrder> listByGrpId29=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);		
    		                          	if(listByGrpId29!=null && !(listByGrpId29.isEmpty())){
    		                          		Iterator it4=listByGrpId29.iterator();
    		                          		while(it4.hasNext()){
    		                          			ServiceOrder tempOrder =(ServiceOrder)it4.next();
    		                          		//Bug 11839 - Possibility to modify Current status to Prior status
    		                          			//tempOrder.setStatus(serviceOrder.getStatus());
    		                          			serviceOrderManager.save(tempOrder);
    		                          		}}*/}}}
            	   }
               }catch(Exception e){
            	   e.printStackTrace();
               }     	   
            }
    	// **************** AutoPopulate trackingStatus deliveryA Start ***********************************************************************
    		try{
    			List al=new ArrayList();
    			al.add("FOTDP");
    			al.add("D/DP");
    			al.add("Warehouse to Destination Port");
    			al.add("OP/DP");
    			al.add("D/P");
    			al.add("W/P");	
    				List temp=servicePartnerManager.findTranshipChild(serviceOrder.getShipNumber());
    				if((temp!=null)&&(!temp.isEmpty())&&(temp.get(0)!=null)&&(!temp.get(0).toString().equalsIgnoreCase(""))){					
    					Long spid=Long.parseLong(temp.get(0).toString());
    					ServicePartner newService=servicePartnerManager.get(spid);
    					if((!newService.getTranshipped())&&(serviceOrder.getServiceType()!=null)&&(!serviceOrder.getServiceType().equalsIgnoreCase(""))&&(al.contains(serviceOrder.getServiceType()))&&(trackingStatus.getDeliveryA()==null)&&(!serviceOrder.getIsNetworkRecord())){					
    							if(newService.getAtArrival()!=null){
    							trackingStatus.setDeliveryA(newService.getAtArrival());
    							trackingStatus.setUpdatedBy(newService.getUpdatedBy());
    							trackingStatus.setUpdatedOn(new Date());
    							trackingStatus=trackingStatusManager.save(trackingStatus);
    							//Bug 11839 - Possibility to modify Current status to Prior status
    							//serviceOrder.setStatus("DLVR");
    							//serviceOrder.setStatusNumber(60);
    							serviceOrder.setUpdatedBy(newService.getUpdatedBy());
    							serviceOrder.setUpdatedOn(new Date());
    							//serviceOrder=serviceOrderManager.save(serviceOrder);
    							int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),"DLVR",60,sessionCorpID,getRequest().getRemoteUser()); 
    						}
    				    }
    			    }	        	  
    		}catch(Exception e){
    			 e.printStackTrace();
    		}
    		if(serviceOrder.getIsNetworkRecord()!=null){
    		if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			synchornizeRouting(serviceOrderRecords,servicePartner,isNew,serviceOrder,trackingStatus);
    		}
    		}
            return SUCCESS;
    }

    public String updateServicePartnerStatus()
	{
     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
    	String key = "Routing has been deleted successfully.";   
        saveMessage(getText(key));
        try{
        	servicePartner=servicePartnerManager.get(id);
            serviceOrder  =servicePartner.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteServicePartner(serviceOrderRecords,servicePartner.getCarrierNumber(),routingStatus);
          }
          }catch(Exception ex){
        	  System.out.println("\n\n\n\n error while removing container--->"+ex);
        	  ex.printStackTrace();
          }
        servicePartnerManager.updateDeleteStatus(id);
        id=sid;
        serviceOrder=serviceOrderManager.get(sid);
        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
        userName=getRequest().getRemoteUser();
        servicePartnerManager.updateServiceOrderDate(sid, sessionCorpID,userName);
        servicePartnerManager.updateServiceOrderActualDate(sid,sessionCorpID,serviceOrder.getCustomerFileId(),userName);
    	if((sessionCorpID.equals("VOER") || sessionCorpID.equals("VORU") || sessionCorpID.equals("VOCZ") || sessionCorpID.equals("VOAO")|| sessionCorpID.equals("BONG")) && serviceOrder.getServiceType()!=null &&(!serviceOrder.getServiceType().equalsIgnoreCase("")) && (serviceOrder.getServiceType().equals("D/DP")|| serviceOrder.getServiceType().equalsIgnoreCase("Free on Truck to Destination Port")|| serviceOrder.getServiceType().equalsIgnoreCase("Warehouse to Destination Port") || serviceOrder.getServiceType().equalsIgnoreCase("OP/DP"))){
    		trackingStatus=trackingStatusManager.get(sid);
    		List maxAtArrival=servicePartnerManager.getMaxAtArrivalDate(serviceOrder.getId(),sessionCorpID);
			if(maxAtArrival!=null && !maxAtArrival.isEmpty() && maxAtArrival.get(0)!=null){
				ServicePartner  maxServicePartner = servicePartnerManager.get(Long.parseLong(maxAtArrival.get(0).toString()));
				trackingStatus.setServiceCompleteDate(maxServicePartner.getAtArrival());
				trackingStatus.setUpdatedBy(getRequest().getRemoteUser());
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus=trackingStatusManager.save(trackingStatus);	
			}else{
				trackingStatus.setServiceCompleteDate(null);
				trackingStatus.setUpdatedBy(getRequest().getRemoteUser());
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus=trackingStatusManager.save(trackingStatus);
			}
			
		}
    	if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){
			if((serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(serviceOrder.getBookingAgentShipNumber());
			}
			if((trackingStatus.getBookingAgentExSO()!=null) && (!trackingStatus.getBookingAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(trackingStatus.getBookingAgentExSO());
			}
			if((trackingStatus.getNetworkAgentExSO()!=null) && (!trackingStatus.getNetworkAgentExSO().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
				linkedshipNumber.add(trackingStatus.getNetworkAgentExSO());
			}	
			if(linkedshipNumber!=null && !linkedshipNumber.isEmpty() && linkedshipNumber.size()>0 && trackingStatus.getServiceCompleteDate()!=null){
				trackingStatusManager.updateLinkedShipNumberforCompletedDate(linkedshipNumber,trackingStatus.getServiceCompleteDate(),sessionCorpID,getRequest().getRemoteUser());
			}
		}
        servicePartnerlist();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
    
    private void deleteServicePartner(List<Object> serviceOrderRecords,	String carrierNumber2,String  routingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		servicePartnerManager.deleteNetworkServicePartner(serviceOrder.getId(),carrierNumber2,routingStatus);
    		servicePartnerManager.updateServiceOrderDate(serviceOrder.getId(), serviceOrder.getCorpID(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
            servicePartnerManager.updateServiceOrderActualDate(serviceOrder.getId(),serviceOrder.getCorpID(),serviceOrder.getCustomerFileId(),servicePartner.getCorpID()+":"+getRequest().getRemoteUser());
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	}

	public String serviceCloneSave(ServicePartner servicePartner, ServiceOrder serviceOrder){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
	    for(int i = 0; i<totalClone; i++){
		   ServicePartner servicePartnerNew = new ServicePartner();
		   billing=billingManager.get(serviceOrder.getId());
		   maxCarrierNumber = serviceOrderManager.findMaximumCarrierNumber(serviceOrder.getShipNumber());
		   if (maxCarrierNumber.get(0) == null ) {          
		        	carrierNumber = "01";
	        }else {
	            autoCarrier = Long.parseLong((maxCarrierNumber).get(0).toString()) + 1;
	            if((autoCarrier.toString()).length() == 1) {
	            	carrierNumber = "0"+(autoCarrier.toString());
	            }else {
	            	carrierNumber=autoCarrier.toString();
	            }
	        }
		   

	          if(!(servicePartner.getCarrierCode() == null || servicePartner.getCarrierCode().equalsIgnoreCase(""))){
	        	Boolean defaultVendorAccLine = false;
			defaultVendorAccLine = trackingStatusManager.getdefaultVendorAccLine(sessionCorpID);
	        	if(defaultVendorAccLine == true && userType!=null && userType.equalsIgnoreCase("USER")){	
	        		List insurarAcc = servicePartnerManager.findCarrierFromAccLine("Freight", servicePartner.getCarrierCode(), servicePartner.getShipNumber());
		        	if(insurarAcc == null || insurarAcc.isEmpty() || insurarAcc.get(0) == null || insurarAcc.get(0).toString().equalsIgnoreCase("")){
		        		accountLine = new AccountLine();
		        		boolean activateAccPortal =true;
		        		try{
		        		if(accountLineAccountPortalFlag){	
		        		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
		        		accountLine.setActivateAccPortal(activateAccPortal);
		        		}
		        		}catch(Exception e){
		        			
		        		}
		        		accountLine.setServiceOrderId(serviceOrder.getId());
		        		accountLine.setCorpID(sessionCorpID);
		        		accountLine.setShipNumber(servicePartner.getShipNumber());
		        		accountLine.setCategory("Freight");
		        		accountLine.setVendorCode(servicePartner.getCarrierCode());
		        		accountLine.setActgCode(servicePartner.getActgCode());
		        		accountLine.setEstimateVendorName(servicePartner.getCarrierName());
		        		accountLine.setStatus(true);
		        		accountLine.setCreatedBy(servicePartner.getCreatedBy());
		        		accountLine.setBasis("");
		        		accountLine.setBillToCode(billing.getBillToCode());
						accountLine.setBillToName(billing.getBillToName());
						accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
						accountLine.setNetworkBillToName(billing.getNetworkBillToName());
						if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
			    				accountLine.setRecVatPercent(recVatPercent);
					    		}
				    	}
		
		           	 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
		                if ( maxLineNumber.get(0) == null ) {          
		                	accountLineNumber = "001";
		                }else {
		                	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		                    if((autoLineNumber.toString()).length() == 2) {
		                    	accountLineNumber = "0"+(autoLineNumber.toString());
		                    }
		                    else if((autoLineNumber.toString()).length() == 1) {
		                    	accountLineNumber = "00"+(autoLineNumber.toString());
		                    } 
		                    else {
		                    	accountLineNumber=autoLineNumber.toString();
		                    }
		                }
			          accountLine.setAccountLineNumber(accountLineNumber);
			    		accountLine.setCreatedOn(new Date());
			    		accountLine.setUpdatedOn(new Date());
			    		userName=getRequest().getRemoteUser();
			    		accountLine.setUpdatedBy(userName);
			    		accountLine.setServiceOrder(serviceOrder);
			    		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			    		accountLine.setContract(billing.getContract());
			    		accountLineManager.save(accountLine);
		        	}else{
		        		accountLine = (AccountLine) insurarAcc.get(0);
		        		accountLine.setVendorCode(servicePartner.getCarrierCode());
		        		accountLine.setEstimateVendorName(servicePartner.getCarrierName());
		        		accountLine.setActgCode(servicePartner.getActgCode());
		        		accountLine.setBasis("");
		        		userName=getRequest().getRemoteUser();
		        		accountLine.setUpdatedBy(userName);
		        		accountLine.setUpdatedOn(new Date());
		        		accountLine.setServiceOrder(serviceOrder);
		        		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
		        		accountLine.setContract(billing.getContract());
		        		accountLineManager.save(accountLine);
		        	}
	        	}
	        } 
	          servicePartnerNew.setServiceOrder(serviceOrder);
	          
	          servicePartnerNew.setCarrierNumber(carrierNumber);
	          servicePartnerNew.setCorpID(sessionCorpID);
	          servicePartnerNew.setShipNumber(servicePartner.getShipNumber());
	          servicePartnerNew.setBookNumber(servicePartner.getBookNumber());
	          servicePartnerNew.setCarrierClicd(servicePartner.getCarrierClicd()!= null ? servicePartner.getCarrierClicd() : "");
	          servicePartnerNew.setCarrierCode(servicePartner.getCarrierCode());
	          servicePartnerNew.setCarrierName(servicePartner.getCarrierName());
	          servicePartnerNew.setCarrierPhone(servicePartner.getCarrierPhone()!= null ? servicePartner.getCarrierPhone() : "");
	          servicePartnerNew.setCarrierVessels(servicePartner.getCarrierVessels()!= null ? servicePartner.getCarrierVessels() : "");
	          servicePartnerNew.setETATA(servicePartner.getETATA() != null ? servicePartner.getETATA() : "");
	          servicePartnerNew.setETDTA(servicePartner.getETDTA() != null ? servicePartner.getETDTA() : "");
	          servicePartnerNew.setOmni(servicePartner.getOmni() != null ? servicePartner.getOmni() : "");
	          servicePartnerNew.setPdCost1(servicePartner.getPdCost1() != null ? servicePartner.getPdCost1() : new BigDecimal(0));
	          servicePartnerNew.setPdCost2(servicePartner.getPdCost2() != null ? servicePartner.getPdCost2() : new BigDecimal(0));
	          servicePartnerNew.setPdDays2(servicePartner.getPdDays2() != 0 ? servicePartner.getPdDays2() : 0);
	          servicePartnerNew.setPdFreedays(servicePartner.getPdFreedays() != 0 ? servicePartner.getPdFreedays() : 0);
	          servicePartnerNew.setCarrierArrival(servicePartner.getCarrierArrival()!= null ? servicePartner.getCarrierArrival() : "");
	          servicePartnerNew.setCarrierDeparture(servicePartner.getCarrierDeparture()!= null ? servicePartner.getCarrierDeparture() : "");
	          servicePartnerNew.setAtArrival(servicePartner.getAtArrival());
	          servicePartnerNew.setAtDepart(servicePartner.getAtDepart());
	          servicePartnerNew.setEtArrival(servicePartner.getEtArrival());
	          servicePartnerNew.setEtDepart(servicePartner.getEtDepart());
	          servicePartnerNew.setCoord(servicePartner.getCoord()!= null ? servicePartner.getCoord() : "");
	          servicePartnerNew.setPartnerType(servicePartner.getPartnerType()!= null ? servicePartner.getPartnerType() : "");
	          servicePartnerNew.setDropPick(servicePartner.getDropPick()!= null ? servicePartner.getDropPick() : false);
	          servicePartnerNew.setLiveLoad(servicePartner.getLiveLoad()!= null ? servicePartner.getLiveLoad() : false);
	          servicePartnerNew.setBlNumber(servicePartner.getBlNumber()!= null ? servicePartner.getBlNumber() : "");
	          servicePartnerNew.setHblNumber(servicePartner.getHblNumber()!=null ? servicePartner.getHblNumber() :"");
	          servicePartnerNew.setTranshipped(servicePartner.getTranshipped()!= null ? servicePartner.getTranshipped() : false);
	          servicePartnerNew.setPolCode(servicePartner.getPolCode()!= null ? servicePartner.getPolCode() : "");
	          servicePartnerNew.setPoeCode(servicePartner.getPoeCode()!= null ? servicePartner.getPoeCode() : "");
	          servicePartnerNew.setActgCode(servicePartner.getActgCode()!= null ? servicePartner.getActgCode() : "");
	          servicePartnerNew.setServiceOrderId(servicePartner.getServiceOrderId());
	          servicePartnerNew.setHouseBillIssued(servicePartner.getHouseBillIssued());
	          servicePartnerNew.setCntnrNumber(servicePartner.getCntnrNumber()!= null ? servicePartner.getCntnrNumber() : "");
	          servicePartnerNew.setAesNumber(servicePartner.getAesNumber()!= null ? servicePartner.getAesNumber() : "");
	          servicePartnerNew.setSkids(servicePartner.getSkids()!= null ? servicePartner.getSkids() : "");
	          servicePartnerNew.setClone(false);
	          servicePartnerNew.setEmail(servicePartner.getEmail()!= null ? servicePartner.getEmail() : "");
	          servicePartnerNew.setSequenceNumber(servicePartner.getSequenceNumber()!= null ? servicePartner.getSequenceNumber() : "");
	          servicePartnerNew.setShip(servicePartner.getShip()!= null ? servicePartner.getShip() : "");
	          servicePartnerNew.setCity(servicePartner.getCity()!= null ? servicePartner.getCity() : "");
	          servicePartnerNew.setMiles(servicePartner.getMiles()!= null ? servicePartner.getMiles() : 0);
	          
	          servicePartnerNew.setUpdatedBy(getRequest().getRemoteUser());
	          servicePartnerNew.setCreatedBy(getRequest().getRemoteUser());
	          servicePartnerNew.setCreatedOn(new Date());
	          servicePartnerNew.setUpdatedOn(new Date());
	          servicePartnerManager.save(servicePartnerNew);   
 	   }
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;
    }
	
	private String carrierDeparture;
	private String autocompletePortDivId;
	private String nameId;
	private String serviceMode;
	@SkipValidation
	public String polAutocomplete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(!carrierDeparture.equals("")){
			polList=portManager.getPOLAutoComplete(sessionCorpID,carrierDeparture,serviceMode);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}   
	
	@SkipValidation
	public String poeAutocomplete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(!servicePartner.getCarrierArrival().equals("")){
			polList=portManager.getPOEAutoComplete(sessionCorpID,servicePartner.getCarrierArrival(),mode);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}  
  private  List partnerList;
  private String partnerCode="";
  private String partnerlastName="";
  private String partneraliasName="";
  private String  partnerbillingCountryCode="";
  private String  partnerbillingState="";
  private String soCorpID;
    @SkipValidation
    public String servicePartnerAgentPopUp() {
    	 partnerList =servicePartnerManager.getCarrierListvalue(partnerCode,partnerlastName,partneraliasName,partnerbillingCountryCode,partnerbillingState,soCorpID);
		/*for (PartnerPublic partnerPublic : partnerPublicList) {
			partnerList.add(partnerPublic);
		}
	*/
    	return SUCCESS;   
	      }
    private List accountLineList;
    @SkipValidation
    public String checkVendorNameForAgent(){
    	accountLineList =servicePartnerManager.getCarrierNameForAgent(partnerCode,soCorpID);
		/*for (PartnerPublic partnerPublic : partnerPublicList) {
			partnerList.add(partnerPublic);
		}
	*/
    	return SUCCESS;   
	  }
    private List containerNumberListRouting;
    @SkipValidation
	public String getRoutingDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			containerNumberListRouting=servicePartnerManager.getContainerNumber(serviceOrder.getShipNumber());
			servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));
			userName=getRequest().getRemoteUser();
			servicePartnerManager.updateServiceOrderDate(id, sessionCorpID,userName);
	        servicePartnerManager.updateServiceOrderActualDate(id,sessionCorpID,serviceOrder.getCustomerFileId(),userName);
		} catch (Exception e) {			
	    	 return CANCEL;
		}		
		return SUCCESS;
	}
    
    @SkipValidation
   	public String deleteRoutingDetailsAjax(){
   		try {
   			servicePartner=servicePartnerManager.get(id);
   			//servicePartnerManager.remove(id);
   			servicePartnerManager.updateStatus(id,routingStatus,updateRecords);
            serviceOrder  =servicePartner.getServiceOrder();
            if(serviceOrder.getIsNetworkRecord()){
            	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			deleteServicePartner(serviceOrderRecords,servicePartner.getCarrierNumber(),routingStatus);
          }
            if((updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll"))){
               	if(serviceOrder.getControlFlag().equalsIgnoreCase("G")){
               		listByGrpId1=servicePartnerManager.routingListByGrpId(servicePartner.getId());
               		}
               	}
          	if(listByGrpId1!=null && !(listByGrpId1.isEmpty())){
           		Iterator it=listByGrpId1.iterator();
           		while(it.hasNext()){
           			try {
           			ServicePartner tempRouting=(ServicePartner)it.next();
           			ServiceOrder tempOrder =serviceOrderManager.get(tempRouting.getServiceOrderId());
           			List linkedShipNumber=findLinkedShipNumber(tempOrder);
        			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,tempOrder);
        			deleteServicePartner(serviceOrderRecords,servicePartner.getCarrierNumber(),routingStatus);
           			}
           			catch(Exception e)
           			{return CANCEL;}
           		}
           		
           	}
   		} catch (Exception e) {			
   	    	 return CANCEL;
   		}		
   		return SUCCESS;
   	}
    
public String getCountCarrierNotes() {
	return countCarrierNotes;
}
public String getCountServiceNotes() {
	return countServiceNotes;
}
public List getServicePartnerss() {
	return servicePartnerss;
}
public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
	this.trackingStatusManager = trackingStatusManager;
}
public TrackingStatus getTrackingStatus() {
	return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
}
public void setTrackingStatus(TrackingStatus trackingStatus) {
	this.trackingStatus = trackingStatus;
}
public String getPortCode() {
	return portCode;
}
public void setPortCode(String portCode) {
	this.portCode = portCode;
}
public List getPortLineList() {
	return portLineList;
}
public void setPortLineList(List portLineList) {
	this.portLineList = portLineList;
}
public AccountLine getAccountLine() {
	return accountLine;
}
public void setAccountLine(AccountLine accountLine) {
	this.accountLine = accountLine;
}
public void setAccountLineManager(AccountLineManager accountLineManager) {
	this.accountLineManager = accountLineManager;
}
public CustomerFile getCustomerFile() {
	return (customerFile!=null ?customerFile:customerFileManager.get(customerFileId));
}
public void setCustomerFile(CustomerFile customerFile) {
	this.customerFile = customerFile;
}
public Miscellaneous getMiscellaneous() {
	return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
}
public void setMiscellaneous(Miscellaneous miscellaneous) {
	this.miscellaneous = miscellaneous;
}
public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
	this.miscellaneousManager = miscellaneousManager;
}
public String getAccountInterface() {
	return accountInterface;
}
public void setAccountInterface(String accountInterface) {
	this.accountInterface = accountInterface;
} 
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
    this.servicePartnerManager = servicePartnerManager;
}
public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
    this.serviceOrderManager = serviceOrderManager;
}
public void setRefMasterManager(RefMasterManager refMasterManager) {
    this.refMasterManager = refMasterManager;
}
public void setNotesManager(NotesManager notesManager) {
    this.notesManager = notesManager;
}
public List getRefMasters(){
	return refMasters;
}
public List getServiceOrders() {   
    return serviceOrders;   
}   
public  Map<String, String> getSITOUTTA() {
	return SITOUTTA;
}
public  Map<String, String> getOmni() {
	if(servicePartner !=null && servicePartner.getId()!=null && servicePartner.getOmni()!=null && !servicePartner.getOmni().equals(""))
	{
	if(!omni.containsValue(servicePartner.getOmni()) && !omni.containsKey(servicePartner.getOmni()))
	{omni.put(servicePartner.getOmni(), servicePartner.getOmni());
	}
	}
	return omni;
}
public  Map<String, String> getPoe() {
	return poe;
}
public  Map<String, String> getPol() {
	return pol;
}
public  Map<String, String> getPartnerType() {
	return partnerType;
}
public Set getServicePartners() {   
    return servicePartners;   
}   
public void setId(Long id) {   
    this.id = id;   
} 
public ServicePartner getServicePartner() {   
    return servicePartner;   
} 
public void setServicePartner(ServicePartner servicePartner) {   
    this.servicePartner = servicePartner;   
}
public ServiceOrder getServiceOrder() {   
    return serviceOrder;   
} 
public void setServiceOrder(ServiceOrder serviceOrder) {   
    this.serviceOrder = serviceOrder;   
}
public String getValidateFormNav() 
{
  return validateFormNav;
 }
 public void setValidateFormNav(String validateFormNav) 
 {
  this.validateFormNav = validateFormNav;
  }
public String getGotoPageString() 
{
return gotoPageString;
}
public void setGotoPageString(String gotoPageString) 
{
this.gotoPageString = gotoPageString;
}
public Long getSid() {
	return sid;
}
public void setSid(Long sid) {
	this.sid = sid;
}

/**
 * @return the hitFlag
 */
public String getHitFlag() {
	return hitFlag;
}

/**
 * @param hitFlag the hitFlag to set
 */
public void setHitFlag(String hitFlag) {
	this.hitFlag = hitFlag;
}

/**
 * @return the carrierATD
 */
public Date getCarrierATD() {
	return carrierATD;
}

/**
 * @param carrierATD the carrierATD to set
 */
public void setCarrierATD(Date carrierATD) {
	this.carrierATD = carrierATD;
}

/**
 * @return the carrierETD
 */
public Date getCarrierETD() {
	return carrierETD;
}

/**
 * @param carrierETD the carrierETD to set
 */
public void setCarrierETD(Date carrierETD) {
	this.carrierETD = carrierETD;
}

/**
 * @return the carrierATA
 */
public Date getCarrierATA() {
	return carrierATA;
}

/**
 * @param carrierATA the carrierATA to set
 */
public void setCarrierATA(Date carrierATA) {
	this.carrierATA = carrierATA;
}

/**
 * @return the carrierETA
 */
public Date getCarrierETA() {
	return carrierETA;
}

/**
 * @param carrierETA the carrierETA to set
 */
public void setCarrierETA(Date carrierETA) {
	this.carrierETA = carrierETA;
}

public Long getSoId() {
	return soId;
}

public void setSoId(Long soId) {
	this.soId = soId;
}

public String getCountShip() {
	return countShip;
}

public void setCountShip(String countShip) {
	this.countShip = countShip;
}

public String getMinShip() {
	return minShip;
}

public void setMinShip(String minShip) {
	this.minShip = minShip;
}

public String getShipSize() {
	return shipSize;
}

public void setShipSize(String shipSize) {
	this.shipSize = shipSize;
}

public void setCustomerFileManager(CustomerFileManager customerFileManager) {
	this.customerFileManager = customerFileManager;
}

public List getCustomerSO() {
	return customerSO;
}

public void setCustomerSO(List customerSO) {
	this.customerSO = customerSO;
} 
public List getContainerNumberList() {
	return (containerNumberList!=null && !containerNumberList.isEmpty())?containerNumberList:servicePartnerManager.getContainerNumber(shipNumber);
 }

public void setContainerNumberList(List containerNumberList) {
	this.containerNumberList = containerNumberList;
}

/**
 * @return the shipNumber
 */
public String getShipNumber() {
	return shipNumber;
}

/**
 * @param shipNumber the shipNumber to set
 */
public void setShipNumber(String shipNumber) {
	this.shipNumber = shipNumber;
}

public String getCountChild() {
	return countChild;
}

public void setCountChild(String countChild) {
	this.countChild = countChild;
}

public String getMaxChild() {
	return maxChild;
}

public void setMaxChild(String maxChild) {
	this.maxChild = maxChild;
}

public String getMinChild() {
	return minChild;
}

public void setMinChild(String minChild) {
	this.minChild = minChild;
}

public Long getSidNum() {
	return sidNum;
}

public void setSidNum(Long sidNum) {
	this.sidNum = sidNum;
}

public Long getSoIdNum() {
	return soIdNum;
}

public void setSoIdNum(Long soIdNum) {
	this.soIdNum = soIdNum;
}

public List getServicePartnerSO() {
	return servicePartnerSO;
}

public void setServicePartnerSO(List servicePartnerSO) {
	this.servicePartnerSO = servicePartnerSO;
}

public long getTempservicePartner() {
	return tempservicePartner;
}

public void setTempservicePartner(long tempservicePartner) {
	this.tempservicePartner = tempservicePartner;
}

public int getTotalClone() {
	return totalClone;
}

public void setTotalClone(int totalClone) {
	this.totalClone = totalClone;
}

/**
 * @return the copyCarrierAtd
 */
public int getCopyCarrierAtd() {
	return copyCarrierAtd;
}

/**
 * @param copyCarrierAtd the copyCarrierAtd to set
 */
public void setCopyCarrierAtd(int copyCarrierAtd) {
	this.copyCarrierAtd = copyCarrierAtd;
}

/**
 * @return the copyCarrierAta
 */
public int getCopyCarrierAta() {
	return copyCarrierAta;
}

/**
 * @param copyCarrierAta the copyCarrierAta to set
 */
public void setCopyCarrierAta(int copyCarrierAta) {
	this.copyCarrierAta = copyCarrierAta;
}

public Long getCustomerFileId() {
	return customerFileId;
}

public void setCustomerFileId(Long customerFileId) {
	this.customerFileId = customerFileId;
}

public String getCountBondedGoods() {
	return countBondedGoods;
}

public void setCountBondedGoods(String countBondedGoods) {
	this.countBondedGoods = countBondedGoods;
}

public void setCustomManager(CustomManager customManager) {
	this.customManager = customManager;
}

public Boolean getAlreadyExists() {
	return alreadyExists;
}

public void setAlreadyExists(Boolean alreadyExists) {
	this.alreadyExists = alreadyExists;
}

public String getUpdateRecords() {
	return updateRecords;
}

public void setUpdateRecords(String updateRecords) {
	this.updateRecords = updateRecords;
}

public void setCompanyDivisionManager(
		CompanyDivisionManager companyDivisionManager) {
	this.companyDivisionManager = companyDivisionManager;
}

public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
	this.exchangeRateManager = exchangeRateManager;
}

public Billing getBilling() {
	return billing;
}

public void setBilling(Billing billing) {
	this.billing = billing;
}

public void setBillingManager(BillingManager billingManager) {
	this.billingManager = billingManager;
}

public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
	this.toDoRuleManager = toDoRuleManager;
}


public void setCustomerFileAction(CustomerFileAction customerFileAction) {
	this.customerFileAction = customerFileAction;
}

public void setIntegrationLogInfoManager(
		IntegrationLogInfoManager integrationLogInfoManager) {
	this.integrationLogInfoManager = integrationLogInfoManager;
}

public String getDivisionFlag() {
	return divisionFlag;
}

public void setDivisionFlag(String divisionFlag) {
	this.divisionFlag = divisionFlag;
}

public Map<String, String> getEuVatPercentList() {
	return euVatPercentList;
}

public void setEuVatPercentList(Map<String, String> euVatPercentList) {
	this.euVatPercentList = euVatPercentList;
}

public List getPolList() {
	return polList;
}

public void setPolList(List polList) {
	this.polList = polList;
}

public PortManager getPortManager() {
	return portManager;
}

public void setPortManager(PortManager portManager) {
	this.portManager = portManager;
}

public Port getPort() {
	return port;
}

public void setPort(Port port) {
	this.port = port;
}

public String getMode() {
	return mode;
}

public void setMode(String mode) {
	this.mode = mode;
}

public Company getCompany() {
	return company;
}

public CompanyManager getCompanyManager() {
	return companyManager;
}

public String getVoxmeIntergartionFlag() {
	return voxmeIntergartionFlag;
}

public void setCompany(Company company) {
	this.company = company;
}

public void setCompanyManager(CompanyManager companyManager) {
	this.companyManager = companyManager;
}

public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
	this.voxmeIntergartionFlag = voxmeIntergartionFlag;
}

public String getPartnerCode() {
	return partnerCode;
}

public void setPartnerCode(String partnerCode) {
	this.partnerCode = partnerCode;
}

public String getPartnerlastName() {
	return partnerlastName;
}

public void setPartnerlastName(String partnerlastName) {
	this.partnerlastName = partnerlastName;
}

public String getPartneraliasName() {
	return partneraliasName;
}

public void setPartneraliasName(String partneraliasName) {
	this.partneraliasName = partneraliasName;
}

public String getPartnerbillingCountryCode() {
	return partnerbillingCountryCode;
}

public void setPartnerbillingCountryCode(String partnerbillingCountryCode) {
	this.partnerbillingCountryCode = partnerbillingCountryCode;
}

public String getPartnerbillingState() {
	return partnerbillingState;
}

public void setPartnerbillingState(String partnerbillingState) {
	this.partnerbillingState = partnerbillingState;
}

public List<PartnerPublic> getPartnerList() {
	return partnerList;
}

public void setPartnerList(List<PartnerPublic> partnerList) {
	this.partnerList = partnerList;
}

public String getSoCorpID() {
	return soCorpID;
}

public void setSoCorpID(String soCorpID) {
	this.soCorpID = soCorpID;
}

public List getAccountLineList() {
	return accountLineList;
}

public void setAccountLineList(List accountLineList) {
	this.accountLineList = accountLineList;
}

public boolean isAccountLineAccountPortalFlag() {
	return accountLineAccountPortalFlag;
}

public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
	this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
}

public List<SystemDefault> getSysDefaultDetail() {
	return sysDefaultDetail;
}

public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
	this.sysDefaultDetail = sysDefaultDetail;
}

public String getCarrierDeparture() {
	return carrierDeparture;
}

public void setCarrierDeparture(String carrierDeparture) {
	this.carrierDeparture = carrierDeparture;
}

public String getAutocompletePortDivId() {
	return autocompletePortDivId;
}

public void setAutocompletePortDivId(String autocompletePortDivId) {
	this.autocompletePortDivId = autocompletePortDivId;
}

public String getNameId() {
	return nameId;
}

public void setNameId(String nameId) {
	this.nameId = nameId;
}

public String getServiceMode() {
	return serviceMode;
}

public void setServiceMode(String serviceMode) {
	this.serviceMode = serviceMode;
}

public String getUsertype() {
	return usertype;
}

public void setUsertype(String usertype) {
	this.usertype = usertype;
}

public Boolean getSurveyTab() {
	return surveyTab;
}

public void setSurveyTab(Boolean surveyTab) {
	this.surveyTab = surveyTab;
}

public List getContainerNumberListRouting() {
	return containerNumberListRouting;
}

public void setContainerNumberListRouting(List containerNumberListRouting) {
	this.containerNumberListRouting = containerNumberListRouting;
}

public boolean isFutureDateFlag() {
	return futureDateFlag;
}

public void setFutureDateFlag(boolean futureDateFlag) {
	this.futureDateFlag = futureDateFlag;
}

public String getFieldName() {
	return fieldName;
}

public void setFieldName(String fieldName) {
	this.fieldName = fieldName;
}

public String getFieldValue() {
	return fieldValue;
}

public void setFieldValue(String fieldValue) {
	this.fieldValue = fieldValue;
}

public String getFieldName1() {
	return fieldName1;
}

public void setFieldName1(String fieldName1) {
	this.fieldName1 = fieldName1;
}

public String getFieldValue1() {
	return fieldValue1;
}

public void setFieldValue1(String fieldValue1) {
	this.fieldValue1 = fieldValue1;
}

public List getListByGrpId1() {
	return listByGrpId1;
}

public void setListByGrpId1(List listByGrpId1) {
	this.listByGrpId1 = listByGrpId1;
}

public String getTableName() {
	return tableName;
}

public void setTableName(String tableName) {
	this.tableName = tableName;
}

public String getRoutingStatus() {
	return routingStatus;
}

public void setRoutingStatus(String routingStatus) {
	this.routingStatus = routingStatus;
}

public SystemDefault getSystemDefault() {
	return systemDefault;
}

public void setSystemDefault(SystemDefault systemDefault) {
	this.systemDefault = systemDefault;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getName() {
	return Name;
}

public void setName(String name) {
	Name = name;
}

public Map<String, String> getOmni_isactive() {
	return omni_isactive;
}

public void setOmni_isactive(Map<String, String> omni_isactive) {
	this.omni_isactive = omni_isactive;
}

public String getOiJobList() {
	return oiJobList;
}

public void setOiJobList(String oiJobList) {
	this.oiJobList = oiJobList;
}
}

//End of Class.   
 
 