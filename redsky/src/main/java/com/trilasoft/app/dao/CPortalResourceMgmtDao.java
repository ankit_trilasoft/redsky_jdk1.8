package com.trilasoft.app.dao;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.CPortalResourceMgmt;

public interface CPortalResourceMgmtDao extends GenericDao<CPortalResourceMgmt, Long>{
	public List cPortalResourceMgmtList(String corpId);
	public List getMaxId();
	public List getListByDesc(Long id);
	public List vendorName(String partnerCode,String corpId);
	public List getBysearchCriteria(String documentType,String documentName,String fileFileName,String originCountry,String destinationCountry,String billToCode,String billToName,String corpID);
	public List findCPortalDocument(String partnerCode,String documentType ,String documentName,String fileFileName,String language,String infoPackage,String corpId );
	public List accountportalResourceMgmtList(String partnerCode,String corpId);
	public List FindsearchCriteria(String documentType,String documentName,String fileFileName,String originCountry,String destinationCountry,String billToCode,String corpID);
	public List<CPortalResourceMgmt> resourceExtracts(String documentType, String sessionCorpID);
	public List getChildList(String partnerCode,String corpId);
	public List getDocumentLocation(String corpID,String language,String jobType);
	
}