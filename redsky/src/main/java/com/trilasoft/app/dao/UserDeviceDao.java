package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.UserDevice;

public interface UserDeviceDao extends GenericDao<UserDevice, Long>{
	public List findRecordByLoginUser(String loginUser, String deviceIdVal);
}
