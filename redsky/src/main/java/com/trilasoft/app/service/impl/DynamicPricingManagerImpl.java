package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DynamicPricingDao;
import com.trilasoft.app.model.DynamicPricing;
import com.trilasoft.app.service.DynamicPricingManager;

public class DynamicPricingManagerImpl extends GenericManagerImpl<DynamicPricing, Long> implements DynamicPricingManager{
	DynamicPricingDao dynamicPricingDao;
	public DynamicPricingManagerImpl(DynamicPricingDao dynamicPricingDao) {
		super(dynamicPricingDao);
		this.dynamicPricingDao=dynamicPricingDao;
	}
	public DynamicPricing getDynamicPricingByType(String type,String sessionCorpID){
		return dynamicPricingDao.getDynamicPricingByType(type, sessionCorpID);
	}
	public List getDynamicPricingCalenderByType(String type,String sessionCorpID){
		return dynamicPricingDao.getDynamicPricingCalenderByType(type, sessionCorpID);
	}
}
