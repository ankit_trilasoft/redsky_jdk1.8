<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title><fmt:message key="payableCompanyCodeForm.title" /></title>
<meta name="heading" content="<fmt:message key='payableCompanyCodeForm.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:488px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}

</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">
function checkCompanyCode()
{ 
 var companyCode = document.forms['payableCompanyCodeForm'].elements['payCompanyCode'].value; 
 companyCode=companyCode.trim(); 
 if(companyCode=='' )
 {
  document.forms['payableCompanyCodeForm'].elements['Extract'].disabled=true;
 }
else if(companyCode!='' )
{
document.forms['payableCompanyCodeForm'].elements['Extract'].disabled=false; 
}
} 

</script>



</script>
</head>
<s:form id="payableCompanyCodeForm" name="payableCompanyCodeForm" action="showPayablePostDate" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="Layer1" style="width:85%;">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Payable Data Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table  border="0" class="" style="width:80%;" cellspacing="0" cellpadding="2">
	<tr><td height="5"></td></tr>
	<tr> 
	    <td width="5px"></td>
		<td align="right" class="listwhitebox" style="width:200px; !width:210px;">Please enter Company Group Code</td> 
		<td align="left"  width="470px"><s:select list="%{payCompanyCodeList}" cssClass="list-menu" cssStyle="width:120px" headerKey=" " headerValue=" " id="payCompanyCode"  name="payCompanyCode"  onchange="checkCompanyCode();"/></td>
		</tr> 
		
		<tr><td height="10"></td></tr>
		
		<%-- <tr>
		<td width="5px"></td>
		<td align="right"  class="listwhitebox">Posting Date</td> 
		<td width="280px"><s:textfield cssClass="input-textUpper" id="payPostingDate" name="payPostingDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkCompanyCode();"/><img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['payableCompanyCodeForm'].payPostingDate,'calender',document.forms['payableCompanyCodeForm'].dateFormat.value);document.forms['payableCompanyCodeForm'].elements['payPostingDate'].select(); return false;"/></td>
		</tr> --%>	
		
		<tr>
		<td width="5px"></td>
		<td></td> 
        <td align="left"><s:submit type="button"  cssClass="cssbutton1"  value="Check Post Date" name="Extract"  cssStyle="width:120px;"/>  
        </td>
		</tr> 
		<tr><td height="15"></td></tr>	
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
</s:form>
<script type="text/javascript"> 
try{
checkCompanyCode(); 
}
catch(e){}
</script>