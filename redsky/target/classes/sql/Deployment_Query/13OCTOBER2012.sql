ALTER TABLE `redsky`.`customerfile` ADD COLUMN `emailSentDate` VARCHAR(50) AFTER `ugwIntId`;

ALTER TABLE `redsky`.`redskybilling` ADD COLUMN `packingA` DATETIME AFTER `redskyBillDate`,ADD COLUMN `loadA` DATETIME AFTER `packingA`,ADD COLUMN `deliveryA` DATETIME AFTER `loadA`;

ALTER TABLE `redsky`.`networkcontrol` ADD COLUMN `childAgentCode` VARCHAR(20) DEFAULT NULL AFTER `createdBy`,  ADD COLUMN `childAgentType` VARCHAR(5) DEFAULT NULL AFTER `childAgentCode`,  ADD COLUMN `companyDiv` VARCHAR(10) DEFAULT NULL AFTER `childAgentType`;

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values ('TSFT', 'ForwarderAgent', 'ForwarderAgent', 20, 'PRULEROLE','en',now(),'RS_SETUP',now(),'RS_SETUP'),('TSFT', 'BrokerAgent', 'BrokerAgent', 20, 'PRULEROLE','en',now(),'RS_SETUP',now(),'RS_SETUP');

ALTER TABLE `redsky`.`partnerpublic` MODIFY COLUMN `billingState` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, MODIFY COLUMN `mailingState` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, MODIFY COLUMN `terminalState` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`partnerprivate` MODIFY COLUMN `licenseState` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;
 
