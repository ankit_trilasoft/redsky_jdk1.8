package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="companytodoruleexecutionupdate")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class CompanyToDoRuleExecutionUpdate {
	private Long id;
	private String corpID;	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Boolean rulesRunning;
	private Date lastRunDate;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("rulesRunning", rulesRunning)
				.append("lastRunDate", lastRunDate).toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CompanyToDoRuleExecutionUpdate))
			return false;
		CompanyToDoRuleExecutionUpdate castOther = (CompanyToDoRuleExecutionUpdate) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(rulesRunning, castOther.rulesRunning)
				.append(lastRunDate, castOther.lastRunDate).isEquals();
				
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id)
				.append(corpID).append(createdBy).append(updatedBy).append(createdOn).append(updatedOn).append(rulesRunning).append(lastRunDate).toHashCode();
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Boolean getRulesRunning() {
		return rulesRunning;
	}
	public void setRulesRunning(Boolean rulesRunning) {
		this.rulesRunning = rulesRunning;
	}
	@Column
	public Date getLastRunDate() {
		return lastRunDate;
	}
	public void setLastRunDate(Date lastRunDate) {
		this.lastRunDate = lastRunDate;
	}
}
