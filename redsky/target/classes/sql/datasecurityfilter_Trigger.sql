delimiter $$

CREATE trigger redsky.trigger_add_history_datasecurityfilter
BEFORE UPDATE on redsky.datasecurityfilter
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.name <> NEW.name ) THEN
      select CONCAT(fieldNameList,'datasecurityfilter.name~') into fieldNameList;
      select CONCAT(oldValueList,OLD.name,'~') into oldValueList;
      select CONCAT(newValueList,NEW.name,'~') into newValueList;
  END IF;




   IF (OLD.fieldName <> NEW.fieldName ) THEN
      select CONCAT(fieldNameList,'datasecurityfilter.fieldName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldName,'~') into newValueList;
  END IF;









   IF (OLD.corpid <> NEW.corpid ) THEN
      select CONCAT(fieldNameList,'datasecurityfilter.corpid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
  END IF;




   IF (OLD.filterValues <> NEW.filterValues ) THEN
      select CONCAT(fieldNameList,'datasecurityfilter.filterValues~') into fieldNameList;
      select CONCAT(oldValueList,OLD.filterValues,'~') into oldValueList;
      select CONCAT(newValueList,NEW.filterValues,'~') into newValueList;
  END IF;




   IF (OLD.tableName <> NEW.tableName ) THEN
      select CONCAT(fieldNameList,'datasecurityfilter.tableName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tableName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tableName,'~') into newValueList;
  END IF;






  CALL add_tblHistory (OLD.id,"datasecurityfilter", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;