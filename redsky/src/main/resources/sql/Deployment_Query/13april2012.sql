ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `emailRecvd` DATETIME AFTER `shipmentType`;

ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `fileSize` VARCHAR(25) DEFAULT NULL AFTER `infoPackage`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludePublicReloSvcsParentUpdate` BIT(1) DEFAULT b'0' AFTER `excludePublicFromParentUpdate`;

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `language` VARCHAR(5) DEFAULT NULL AFTER `label4`;

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values('TSFT', 'M', 'Merge',1, 'QUOACCE',now(),'gverma',now(),'gverma');

ALTER TABLE `itemsjbkequip` ADD COLUMN `revisionInvoice` bit(1) DEFAULT b'0' AFTER `ticketTransferStatus`;

CREATE TABLE `redsky`.`resourcegrid` (`id` BIGINT(20) NOT NULL AUTO_INCREMENT,`category` VARCHAR(1),`resource` VARCHAR(50),`resourceLimit` INTEGER,`hubID` VARCHAR(25),`workDate` DATETIME,`corpId` VARCHAR(10),`createdBy` VARCHAR(45),`createdOn` DATETIME,`updatedBy` VARCHAR(45),`updatedOn` DATETIME,PRIMARY KEY(`id`))

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `estHour` DOUBLE DEFAULT 1 AFTER `revisionInvoice`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `actualSurveyDate`  DateTime  DEFAULT NULL AFTER `survey`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `estMoveCost` DECIMAL(19,4) DEFAULT 0.0000 AFTER `valueDate`;

update networkdatafields set fieldName='estimateContractCurrency' where fieldName='estiamteContractCurrency';

update networkdatafields set toFieldName='estimateContractCurrency' where toFieldName='estiamteContractCurrency';

update networkdatafields set fieldName='estimateContractRate' where fieldName='estiamteContractRate';

update networkdatafields set toFieldName='estimateContractRate' where toFieldName='estiamteContractRate';

update networkdatafields set fieldName='estimateContractExchangeRate' where fieldName='estiamteContractExchangeRate';

update networkdatafields set toFieldName='estimateContractExchangeRate' where toFieldName='estiamteContractExchangeRate';

update networkdatafields set fieldName='estimateContractRateAmmount' where fieldName='estiamteContractRateAmmount';

update networkdatafields set toFieldName='estimateContractRateAmmount' where toFieldName='estiamteContractRateAmmount';

update networkdatafields set fieldName='estimateContractValueDate' where fieldName='estiamteContractValueDate';

update networkdatafields set toFieldName='estimateContractValueDate' where toFieldName='estiamteContractValueDate';

update networkdatafields set fieldName='estimatePayableContractCurrency' where fieldName='estiamtePayableContractCurrency';

update networkdatafields set toFieldName='estimatePayableContractCurrency' where toFieldName='estiamtePayableContractCurrency';

update networkdatafields set fieldName='estimatePayableContractRate' where fieldName='estiamtePayableContractRate';

update networkdatafields set toFieldName='estimatePayableContractRate' where toFieldName='estiamtePayableContractRate';

update networkdatafields set fieldName='estimatePayableContractExchangeRate' where fieldName='estiamtePayableContractExchangeRate';

update networkdatafields set toFieldName='estimatePayableContractExchangeRate' where toFieldName='estiamtePayableContractExchangeRate';

update networkdatafields set fieldName='estimatePayableContractRateAmmount' where fieldName='estiamtePayableContractRateAmmount';

update networkdatafields set toFieldName='estimatePayableContractRateAmmount' where toFieldName='estiamtePayableContractRateAmmount';

update networkdatafields set fieldName='estimatePayableContractValueDate' where fieldName='estiamtePayableContractValueDate';

update networkdatafields set toFieldName='estimatePayableContractValueDate' where toFieldName='estiamtePayableContractValueDate';

ALTER TABLE `redsky`.`accountline` CHANGE COLUMN `estiamteContractCurrency` `estimateContractCurrency` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, CHANGE COLUMN `estiamteContractRate` `estimateContractRate` DECIMAL(19,2) DEFAULT '0.00', CHANGE COLUMN `estiamteContractExchangeRate` `estimateContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000', CHANGE COLUMN `estiamteContractRateAmmount` `estimateContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00', CHANGE COLUMN `estiamteContractValueDate` `estimateContractValueDate` DATETIME DEFAULT NULL, CHANGE COLUMN `estiamtePayableContractCurrency` `estimatePayableContractCurrency` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, CHANGE COLUMN `estiamtePayableContractRate` `estimatePayableContractRate` DECIMAL(19,2) DEFAULT '0.00', CHANGE COLUMN `estiamtePayableContractExchangeRate` `estimatePayableContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000', CHANGE COLUMN `estiamtePayableContractRateAmmount` `estimatePayableContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00', CHANGE COLUMN `estiamtePayableContractValueDate` `estimatePayableContractValueDate` DATETIME DEFAULT NULL;

ALTER TABLE `redsky`.`company` ADD COLUMN `creditInvoiceSequence` bit(1) DEFAULT false ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `commissionable` VARCHAR(55) AFTER `mvlBillToName`;

ALTER TABLE `redsky`.`resourcegrid` ADD UNIQUE `Index_2` USING BTREE(`resource`, `hubID`, `category`);

update  company set creditInvoiceSequence = true where corpid='STAR';


