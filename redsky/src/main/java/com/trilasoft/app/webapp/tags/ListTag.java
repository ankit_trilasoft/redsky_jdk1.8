package com.trilasoft.app.webapp.tags;

import java.io.IOException;
import java.io.StringWriter;

import java.util.Iterator;

import javax.servlet.ServletRequest;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

// <trail:list/>
// <trail:list type="full"/>
public class ListTag extends TagSupport {

    private String var;
    private String delimiter = " -> ";
    private String type;

    public ListTag() {
    }

    public String getVar() { return this.var; }
    public void setVar(String var) { this.var = var; }

    public String getType() { return this.type; }
    public void setType(String type) { this.type = type; }

    public String getDelimiter() { return this.delimiter; }
    public void setDelimiter(String delimiter) { this.delimiter = delimiter; }

    public int doEndTag() throws JspException {
        BreadCrumbs breadcrumbs = JspUtils.getBreadCrumbs(pageContext);

        StringBuffer buffer = new StringBuffer();
        Iterator itr = null;
        // fix
        if(this.type == null || "normalized".equals(this.type) ) {
            itr = breadcrumbs.iterateNormalizedTrail();
        	//itr = breadcrumbs.iterateTrail();
        } else {
            // "full" or anything else does this
            //itr = breadcrumbs.iterateTrail();
        	itr = breadcrumbs.iterateNormalizedTrail();
        }
        while( itr.hasNext() ) {
        	BreadCrumb breadcrumb = (BreadCrumb) itr.next();
            Object label = breadcrumb.getLabel();
            Object url = breadcrumb.getUrl();
            // By Vijay for bread crumbs decorations //
            if(! itr.hasNext()){
            	buffer.append("<font color=\"black\" >");
            	buffer.append(label);
            	buffer.append("</font>");
            }else{
            // End //
            	if(!((url.toString()).indexOf("save") > -1 || (url.toString()).indexOf("search") > -1|| (url.toString()).indexOf("absent") > -1|| (url.toString()).indexOf("remove") > -1|| (url.toString()).indexOf("build") > -1 || (buffer.toString()).indexOf(url.toString()) > -1)){ //  Condition By Vijay just to ignore error for urls of save and search
            		buffer.append("<a href=\"");
                    buffer.append(url);
                    buffer.append("\">");
                    buffer.append("<u>");
                    buffer.append(label);
                    buffer.append("</u>");
                    buffer.append("</a>");
                    buffer.append(this.delimiter);
                    }else{
                    }
        		
        	}
            
        }
        String txt = buffer.toString();
        //System.out.println(txt);
        String s= "<u>Page Not Found</u></a>&nbsp;&nbsp;&gt;&gt;";
        //String s1= "&nbsp;&nbsp;&gt;&gt; <a href=\"#\"><u>Page Not Found</u></a>";
        String s2= "&nbsp;&nbsp;&gt;&gt; <font color=\"black\" >Page Not Found</font>";
        //String s3= "&nbsp;&nbsp;&gt;&gt; <u>Page Not Found</u>";
        //String s = "&nbsp;&nbsp;&gt;&gt; <font color=\"black\" >Page Not Found</font>";
        txt = txt.replaceAll(s, "</a>"); // Jusrt To Remove Page not found
        //txt = txt.replaceAll(s1, ""); // Jusrt To Remove Page not found
        txt = txt.replaceAll(s2, ""); // Jusrt To Remove Page not found
        //txt = txt.replaceAll(s3, ""); // Jusrt To Remove Page not found
        //System.out.println(txt);
        if(this.var == null) {
            JspWriter writer = pageContext.getOut();
            try {
                writer.print(txt);
            } catch(IOException ioe) {
                throw new JspException(ioe.toString());
            }
        } else {
            pageContext.setAttribute(this.var, txt);
        }
        return EVAL_PAGE;
    }

}
