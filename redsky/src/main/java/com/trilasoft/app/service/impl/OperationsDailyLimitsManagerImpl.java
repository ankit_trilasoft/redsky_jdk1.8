package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.OperationsDailyLimitsDao;
import com.trilasoft.app.model.OperationsDailyLimits;
import com.trilasoft.app.service.OperationsDailyLimitsManager;

public class OperationsDailyLimitsManagerImpl extends GenericManagerImpl<OperationsDailyLimits, Long> implements OperationsDailyLimitsManager {
	OperationsDailyLimitsDao operationsDailyLimitsDao;

	public OperationsDailyLimitsManagerImpl(OperationsDailyLimitsDao operationsDailyLimitsDao) {
		super(operationsDailyLimitsDao);
		this.operationsDailyLimitsDao = operationsDailyLimitsDao;
	}

	public List isExisted(Date workDate, String hub, String sessionCorpID) {
		return operationsDailyLimitsDao.isExisted(workDate, hub, sessionCorpID);
	}
	
	public List getDailyLimitsByHub(String hub, Date newWorkDate, String sessionCorpID, String category, String resource){
		return operationsDailyLimitsDao.getDailyLimitsByHub(hub, newWorkDate, sessionCorpID,category,resource);
	}
	public void updateWorkDate(String hub, String newWorkDate,Long id, String sessionCorpID){
	operationsDailyLimitsDao.updateWorkDate(hub, newWorkDate,id, sessionCorpID);
	}
	public List getListById(Long id, String sessionCorpID){
		return operationsDailyLimitsDao.getListById(id, sessionCorpID);
	}
	public void updateDailyLimitParent(Integer tempDailyLimit,Integer tempDailyLimitNew,Integer tempDailyLimitOld, String sessionCorpID,String hubID,Date workDate){
		operationsDailyLimitsDao.updateDailyLimitParent(tempDailyLimit, tempDailyLimitNew,tempDailyLimitOld, sessionCorpID,hubID,workDate);
	}
	public void updateDailyMaxEstWtParent(BigDecimal tempDailymaxWt,BigDecimal tempDailyMaxWtNew,BigDecimal tempDailyMaxWtOld, String sessionCorpID,String hubID,Date workDate){
		operationsDailyLimitsDao.updateDailyMaxEstWtParent(tempDailymaxWt, tempDailyMaxWtNew,tempDailyMaxWtOld, sessionCorpID,hubID,workDate);
	}
	
	public void updateDailyMaxEstVolParent(BigDecimal tempDailyMaxEstVol,BigDecimal tempDailyMaxEstVolNew,BigDecimal tempDailyMaxEstVolOld, String sessionCorpID,String hubID,Date workDate){
		operationsDailyLimitsDao.updateDailyMaxEstVolParent(tempDailyMaxEstVol, tempDailyMaxEstVolNew,tempDailyMaxEstVolOld, sessionCorpID,hubID,workDate);
	}
	public void updateDailyCrewLimitParent(Integer tempDailyCrewLimit,Integer tempDailyCrewLimitNew,Integer tempDailyCrewLimitOld, String sessionCorpID,String hubID,Date workDate){
		operationsDailyLimitsDao.updateDailyCrewLimitParent(tempDailyCrewLimit, tempDailyCrewLimitNew,tempDailyCrewLimitOld, sessionCorpID,hubID,workDate);
	}

}
