<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Update Account Details</title>   
    <meta name="heading" content="Update Account Details"/> 
<style>
.table{margin:0 0 0.5em;}
.table td, .table th, .tableHeaderTable td {padding:0.2em;}
 </style>
 
 </head>
 <DIV ID="layerH" style="display:none">
 <table border="0" width="100%" align="middle">
 <tr>
 <td align="center">
	<font size="2" color="#1666C9"><b><blink>Child Account Updating...</blink></b></font>
	</td>
	</tr>
	</table>
</DIV>
 <s:form name="updateChildForm" action="" method="post" validate="true">
    <c:set var="agentParent" value="<%= request.getParameter("agentParent")%>"/>
    <s:hidden name="agentParent" value="<%= request.getParameter("agentParent")%>"/>
<c:set var="buttons">   
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="updateChildDefaultParent()" value="Update"/>
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="closeWindow();" value="Cancel"/>
</c:set> 
<table class="table">

<tr><td class="listwhitetext" width="10px"><input type="checkbox"  value="Check All" onclick="this.value=check(this.form)"/></td><td><div id="CheckAll" class="listwhitetext">
Check All
</div>
<div id="UncheckAll" style="display:none" class="listwhitetext" >
Uncheck All
</div></td></tr>


<tr><td class="subcontenttabChild" colspan="2">Update The Following Sections In  Account Detail.</td></tr>
<configByCorp:fieldVisibility componentId="component.field.partner.partnerTypeDropDown">
<tr><td class="listwhitetext" width="10px"><s:checkbox id="Type" name="Type"/></td><td  align="left" class="listwhitetext" >Type</td></tr>
</configByCorp:fieldVisibility>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="logoPhotographs" name="logoPhotographs"/></td><td  align="left" class="listwhitetext" >Logo/Photographs</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="bankCode" name="bankCode"/></td><td  align="left" class="listwhitetext" >Bank Code</td></tr>

<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingCurrency">
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultBillingCurrency" name="defaultBillingCurrency"/></td><td  align="left" class="listwhitetext" >Default Billing Currency</td></tr>
</configByCorp:fieldVisibility>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="partnerPortalDetail" name="partnerPortalDetail"/></td><td  align="left" class="listwhitetext" >Partner Portal Detail</td></tr>
<tr><td colspan="2"></td></tr>   


<tr><td class="subcontenttabChild" colspan="2">Update The Following Sections In Child Accounts.</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="controlInfo" name="controlInfo"/></td><td  align="left" class="listwhitetext" >Control Info</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultRoles" name="defaultRoles"/></td><td  align="left" class="listwhitetext" >Default Roles</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultRLORoles" name="defaultRLORoles"/></td><td  align="left" class="listwhitetext" >Default&nbsp;Roles&nbsp;for&nbsp;Relocation</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="paymentMethod" name="paymentMethod"/></td><td  align="left" class="listwhitetext" >Payment Method</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="billingInstruction" name="billingInstruction"/></td><td  align="left" class="listwhitetext" >Billing Instruction</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insurance" name="insurance"/></td><td  align="left" class="listwhitetext" >Insurance</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="creditTerms" name="creditTerms"/></td><td  align="left" class="listwhitetext" >Credit Terms</td></tr>
<configByCorp:fieldVisibility componentId="component.field.partner.QualityMeasurementUTSI">
<c:if test="${company.qualitySurvey==true}">  
<tr><td class="listwhitetext" width="10px"><s:checkbox id="qualityMeasurement" name="qualityMeasurement"/></td><td class="listwhitetext"  align="left" >Quality Measurements</td></tr>
</c:if>
</configByCorp:fieldVisibility>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="entitlement" name="entitlement"/></td><td  align="left" class="listwhitetext" >Entitlement</td></tr>
<!--<tr><td class="listwhitetext" width="10px"><s:checkbox id="accountAssignmentType" name="accountAssignmentType"/></td><td  align="left" class="listwhitetext" >Assignment&nbsp;Type</td></tr>
-->
<tr><td class="listwhitetext" width="10px"><s:checkbox id="storageBillingGroup" name="storageBillingGroup"/></td><td  align="left" class="listwhitetext" >Storage Billing Group</td></tr>

<tr><td class="listwhitetext" width="10px"><s:checkbox id="billingCycle" name="billingCycle"/></td><td  align="left" class="listwhitetext" >Billing Cycle</td></tr>

<tr><td class="listwhitetext" width="10px"><s:checkbox id="storageEmailType" name="storageEmailType"/></td><td  align="left" class="listwhitetext" >Storage Email Type</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="accountAssignmentType" name="accountAssignmentType"/></td><td  align="left" class="listwhitetext" >Assignment&nbsp;Type</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insuranceSet" name="insuranceSet"/></td><td  align="left" class="listwhitetext" >Insurance Setup</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="contract" name="contract"/></td><td  align="left" class="listwhitetext" >Default Contract</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="networkPartnerCode" name="networkPartnerCode"/></td><td  align="left" class="listwhitetext" >Default Network Code</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="vendorCode" name="vendorCode"/></td><td  align="left" class="listwhitetext" >Default Vendor Code</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="source" name="source"/></td><td  align="left" class="listwhitetext" >Default Source</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insuranceHas" name="insuranceHas"/></td><td  align="left" class="listwhitetext" >Default Has Valuation</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="insuranceOption" name="insuranceOption"/></td><td  align="left" class="listwhitetext" >Default Insurance Option</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultVat" name="defaultVat"/></td><td  align="left" class="listwhitetext" >Default Vat Desc</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="rddBased" name="rddBased"/></td><td  align="left" class="listwhitetext" >Default RDD Based</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="rddDays" name="rddDays"/></td><td  align="left" class="listwhitetext" >Default # Of Days</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="billToAuthorization" name="billToAuthorization"/></td><td  align="left" class="listwhitetext" >Default Account PO#/SAP Number</td></tr>


<tr><td class="subcontenttabChild" colspan="2">Update The Policy Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="policyInfo" name="policyInfo" /></td><td  align="left" class="listwhitetext" >Policy</td></tr>

<tr><td class="subcontenttabChild" colspan="2">Update The FAQ Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="faqInfo" name="faqInfo"/></td><td  align="left" class="listwhitetext" >FAQ</td></tr>

<tr><td class="subcontenttabChild" colspan="2">Update The Services Sections </td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="servicesInfo" name="servicesInfo"/></td><td  align="left" class="listwhitetext" >Services</td></tr>
</table>
<c:out value="${buttons}" escapeXml="false" />
</s:form>
<script type="text/javascript">
function updateChildDefaultParent(){
	 try{
		 //partner Public start...
	 	 var logoPhotographs = document.getElementById("logoPhotographs");	
	 	 var bankCode = document.getElementById("bankCode");
	 	 var partnerPortalDetail = document.getElementById("partnerPortalDetail");
	 	 var defaultBillingCurrency = document.getElementById("defaultBillingCurrency");
	 	 try{
	 		var Type = document.getElementById("Type");
	 	 }catch(e){
	 	 }
	 	 //end
	 	 //partner private start...
	 	 var controlInfo = document.getElementById("controlInfo");
	 	 var defaultRoles = document.getElementById("defaultRoles");
	 	 var paymentMethod = document.getElementById("paymentMethod");
	 	 var billingInstruction = document.getElementById("billingInstruction");
	 	 var insurance = document.getElementById("insurance");	
	 	 var creditTerms = document.getElementById("creditTerms");	
	 	 var qualityMeasurement = document.getElementById("qualityMeasurement");	
	 	 var entitlement = document.getElementById("entitlement");
	 	 var storageBillingGroup = document.getElementById("storageBillingGroup");
		 var billingCycle = document.getElementById("billingCycle"); 
	 	 var storageEmailType = document.getElementById("storageEmailType"); 
	 	 var defaultRLORoles = document.getElementById("defaultRLORoles");
	 	 var accountAssignmentType = document.getElementById("accountAssignmentType");	
	 	 var insuranceSet=document.getElementById("insuranceSet");
		 	var contract = document.getElementById("contract");
		 	var networkPartnerCode = document.getElementById("networkPartnerCode");
		 	var vendorCode = document.getElementById("vendorCode");
		 	var source = document.getElementById("source");
		 	var insuranceHas = document.getElementById("insuranceHas");
		 	var insuranceOption = document.getElementById("insuranceOption");
		 	var defaultVat = document.getElementById("defaultVat");
		 	var rddBased = document.getElementById("rddBased");
		 	var rddDays = document.getElementById("rddDays");
		 	var billToAuthorization = document.getElementById("billToAuthorization");

	 	// var Type = document.getElementById("Type");
	 	 //end	
	 	 //policy
	 	 var CPFInfoType = document.getElementById("policyInfo");
	 	 //end
	 	 //services
	 	 var servicesInfo = document.getElementById("servicesInfo");
	 	 //End
	 	//services
	 	 var faqInfo= document.getElementById("faqInfo");
	 	 //End
	 	 }catch(e){}	
	 	 var checkedOption="";
	 	 var checkedPrivateOption="";
	 	 var pageListType="";
	 	 var pagePrivateListType="";
	 	 var pagePrsListType="";
	 	 var pageCPFListType="";
	 	 var pageFaqInfo="";
	 	 var count=0;
		 try{
			 ///partner public...
			 try{
		  if(Type.checked==true) {
	 	  checkedOption=checkedOption+"Type";
	 	 }
		}catch(e){}
	 	 if(bankCode.checked==true) {
	 		checkedOption=checkedOption+"bankCode";
	 	 }
		 if(logoPhotographs.checked==true) {
		 checkedOption=checkedOption+"logoPhotographs";
		 }
		 try{
	 	 if(defaultBillingCurrency.checked==true) {
		 checkedOption=checkedOption+"defaultBillingCurrency";
	 	 }
		 }catch(e){}
		 if(partnerPortalDetail.checked==true) {
			 checkedOption=checkedOption+"partnerPortalDetail";
			 }
	 	 ///end
	 	 ///private start..
		     if(controlInfo.checked==true) {
		    	 checkedPrivateOption=checkedPrivateOption+"controlInfo";
		 	 }
		 	 if(defaultRoles.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"defaultRoles";
		 	 }
		 	 if(paymentMethod.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"paymentMethod";
		 	 }
		 	 if(defaultRLORoles.checked==true) {
			 	checkedPrivateOption=checkedPrivateOption+"defaultRLORoles";
			 	 }
		 	 if(billingInstruction.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"billingInstruction";
		 	 }
		 	 if(insurance.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"insurance";
		 	 }
		 	 if(creditTerms.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"creditTerms";
		 	 }
		 	 try{
		 		if(qualityMeasurement.checked==true) {
				 checkedPrivateOption=checkedPrivateOption+"qualityMeasurement";
				 	 }
		 	 }catch(e){}
		 	
		 	 if(entitlement.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"entitlement";
			 }
		 	if(storageBillingGroup.checked==true){
		 		checkedPrivateOption=checkedPrivateOption+"storageBillingGroup";
		 	}
		 	if(billingCycle.checked==true){
		 		checkedPrivateOption=checkedPrivateOption+"billingCycle";
		 	}
		 	if(storageEmailType.checked==true){
		 		checkedPrivateOption=checkedPrivateOption+"storageEmailType";
		 	}
		 	if(accountAssignmentType.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"accountAssignmentType";
	 	 	}
		 	try{
			 	if(insuranceSet.checked==true){
			 		checkedPrivateOption=checkedPrivateOption+"InsuranceSetUp";
			 	}
			 	
			 	}catch(e){
			 		
			 	}
			 	if(contract.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"contract";
		 	 }
			 	if(networkPartnerCode.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"networkPartnerCode";
		 	 }
			 	 if(vendorCode.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"vendorCode";
			 	 }
			 	 if(source.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"source";
			 	 }
			 	 if(insuranceHas.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"insuranceHas";
			 	 }
			 	if(insuranceOption.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"insuranceOption";
		 	 }
			 	 if(defaultVat.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"defaultVat";
			 	 }
			 	if(rddBased.checked==true) {
			 		checkedPrivateOption=checkedPrivateOption+"rddBased";
		 	 }
			 if(rddDays.checked==true) {
				 checkedPrivateOption=checkedPrivateOption+"rddDays";
			 		
		 	 }
			 if(billToAuthorization.checked==true) {
				 checkedPrivateOption=checkedPrivateOption+"billToAuthorization";
		 	 }
		 	/*if(accountAssignmentType.checked==true) {
		 		checkedPrivateOption=checkedPrivateOption+"accountAssignmentType";
	 	 	}*/
		 }catch(e){}
		 //alert(checkedPrivateOption); 
		 if(checkedOption.trim()!=""){
			 pageListType='PUB';
			 count++;
		 }
		 if(checkedPrivateOption.trim()!=""){
			 pagePrivateListType='AIS';
			 count++;
		 }
		 if(servicesInfo.checked==true){
			 pagePrsListType='PRS';
			 count++;
		 }
		 if(CPFInfoType.checked==true){
			 pageCPFListType='CPF';
			 count++;
		 }
		 if(faqInfo.checked==true){
			 pageFaqInfo='FAQ';
			 count++;
		 }
	 	 if(count>0) {	
	 		showUploadMsg();	 	 
			  var url="updateDefaultChildPartner.html?ajax=1&decorator=simple&popup=true&checkedOption="+checkedOption+"&checkedPrivateOption="+checkedPrivateOption+"&id=${partnerPrivate.id}&agentParent=${agentParent}&pageListType="+pageListType+"&pagePrivateListType="+pagePrivateListType+"&pagePrsListType="+pagePrsListType+"&pageCPFListType="+pageCPFListType+"&pageFaqInfo="+pageFaqInfo+"&partnerCode=${partnerPrivate.partnerCode}";
			  http10.open("GET", url, true); 
			  http10.onreadystatechange = handleHttpResponse; 
		      http10.send(null); 
	 	 }else{
	 		 alert("Please Select One Option.");
	 	 }
	 	 
}
function handleHttpResponse(){
    if (http10.readyState == 4)
    {
    			  var results = http10.responseText
	               results = results.trim();					               
				if(results!="")	{
				document.getElementById('layerH').style.display = "none";	
				alert(results);	
				window.opener.location.href = window.opener.location.href;
				//parent.window.opener.document.location.reload(); 
				self.close();			
				} 
    } 
}

var http10 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function showUploadMsg()
{
 var mymsg = document.getElementById('layerH').style.display = "block";
}
function closeWindow()
{
	self.close();
}
var checkflag = "false";
function check(field) {
	 if (checkflag == "false") {
    for (i = 0; i < field.length; i++) {
      field[i].checked = true;
    }
    checkflag = "true";
     document.getElementById('CheckAll').style.display = 'none';
     document.getElementById('UncheckAll').style.display = 'block';
    return "Uncheck All";
  } else {
    for (i = 0; i < field.length; i++) {
      field[i].checked = false;
    }
    checkflag = "false";
     document.getElementById('UncheckAll').style.display = "none";
    document.getElementById('CheckAll').style.display = "block";
    return "Check All";
  }
}
</script>
