<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Operation Resource Limit</title>   
    <meta name="heading" content="Operation Resource Limit"/>  
    
<style>
.tab{
border:1px solid #74B3DC;
}
.containeralign{width:90px;}
.table th.order1 a {	
	padding:0px 8px 0px 0px;
}

.table th.order2 a {
	padding:0px 8px 0px 0px;
}

.table th.sortable a {
	padding:0px 8px 0px 0px;
}

</style> 
<script language="javascript" type="text/javascript">
function searchResourceList(){
	document.forms['resourceLimitList'].action = 'searchResourceLimit.html?hub=${hub}';
	document.forms['resourceLimitList'].submit();
}
		
</script>
</head>
<s:form cssClass="form_magn" name="resourceLimitList" action="searchResourceLimit.html?hub=${hub}" method="post" validate="true">	

<div id="newmnav" class="nav_tabs"><!-- sandeep -->
	  <ul>
	  	<li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
	  	<li><a href="editHubLimitOps.html?hubID=${hub}"><span>Operational&nbsp;/&nbsp;WH Management</span></a></li>
	    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Resource Limit List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	    <li><a href="dailyControlList.html?hub=${hub}"><span>Daily Limit List</span></a></li>
	  </ul>
</div>
<div style="width: 100%" >
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom: 3px"></div>
</div>
		
<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;"></div>
<div id="content" align="center" style="width: 100%" >
<div id="liquid-round">
   	<div class="top"><span></span></div>
   	<div class="center-content">
		<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  		<tbody>
		  		<tr>
				  	<td align="left" class="listwhitetext">
					  	<table class="detailTabLabel" border="0" cellpadding="2"  style="width:950px">
							  <tbody>  	
							  	<tr>
							  		<td align="left" width="5px"></td>
							  		<td align="left" colspan="2">Hub&nbsp;/&nbsp;WH Selected : <strong>
							  		<c:forEach var="entry" items="${distinctHubDescription}">
									<c:if test="${hub==entry.key}">
									<c:out value="${entry.value}" />
									</c:if>
									</c:forEach>							  		
							  		</strong></td>
								</tr>
								<tr>
									<td align="left" width="125px"></td>
									<td align="left" width="15px">Category</td>
							    	<td class="listwhitetext"><s:select cssClass="list-menu" name="category" list="%{resourceCategory}" cssStyle="width:180px" headerKey="" headerValue="" />
							    	<input type="button" class="cssbutton" style="width:70px; height:25px" onclick="searchResourceList();" value="Search"/></td>
								</tr>
							  </tbody>
						</table>
					 </td>
		    	</tr>		  	
		 	</tbody>
		</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
		
<div id="Layer1" style="width:100%;">
<c:if test="${hub!='0'}">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editResourceLimit.html?hub=${hub}"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  
</c:if>

<table  width="100%" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="resourceLimits" value="resourceLimits" scope="request"/>			  
				
				<display:table name="resourceLimits" class="table" requestURI="" export="true" id="resourceLimits" defaultsort="1" pagesize="50" style="width: 100%;" >   
					<display:column sortable="true" sortProperty="category" style="width: 250px;" href="editResourceLimit.html?hub=${hub}" paramId="id" paramProperty="id" title="Category">
						<c:if test="${resourceLimits.category=='C'}">Crew</c:if>
						<c:if test="${resourceLimits.category=='E'}">Equipment</c:if>
						<c:if test="${resourceLimits.category=='M'}">Material</c:if>
						<c:if test="${resourceLimits.category=='V'}">Vehicle</c:if>
						<c:if test="${resourceLimits.category=='T'}">Third Party</c:if>
					</display:column>
					<display:column property="resource" sortable="true" style="width: 250px;" title="Resource"/>								
					<display:column style="width:55px; text-align:right;" headerClass="containeralign" title="Limit"><div align="right">
					<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceLimits.resourceLimit}" /></div>
					</display:column>
									
				    <display:setProperty name="paging.banner.item_name" value="Operations Resource Limits"/>   
				    <display:setProperty name="paging.banner.items_name" value="Operations Resource Limits"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Operations Resource Limits List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Operations Resource Limits List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Operations Resource Limits List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
	<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr></table>
</div>
</s:form>
<script type="text/javascript"> 
    highlightTableRows("resourceLimitList"); 
</script>