package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.PasswordOTPDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.PasswordOTP;


public class PasswordOTPDaoHibernate extends GenericDaoHibernate<PasswordOTP, Long> implements PasswordOTPDao{
	
	public PasswordOTPDaoHibernate() {
		super(PasswordOTP.class);
	}
	
    private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	
	 public List checkById(Long id) { 
	    	try {
				return getHibernateTemplate().find("from PasswordOTP where expiryDate >=now() and  userId=?",id);
			} catch (DataAccessException e) {
				
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	 e.printStackTrace();
			}
			return null;
	    }
	 
	 public List <User> loadUserByUsername(String username) {
		 try {
				return getHibernateTemplate().find("from User where username=?", username);
			} catch (DataAccessException e) {
				
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	 e.printStackTrace();
			}
			return null;
		 
	 }


	public List checkUserOTP(Long id, String verificationCode) { 
    	try {
			return getHibernateTemplate().find("from PasswordOTP where (verifiedOTPCode ='' or verifiedOTPCode is null ) and otpCode ='"+verificationCode+"' and expiryDate >=now() and  userId=?",id);
		} catch (DataAccessException e) {
			
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
		}
		return null;
    }
	   

}
