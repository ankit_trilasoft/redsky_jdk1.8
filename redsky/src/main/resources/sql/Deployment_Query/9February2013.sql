ALTER TABLE `redsky`.`billing` MODIFY COLUMN `postGrate` DECIMAL(19,5) DEFAULT NULL, MODIFY COLUMN `payableRate` DECIMAL(19,5) DEFAULT NULL;

CREATE TABLE `redsky`.`sentsologinfo` (
   `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
   `soNumber` VARCHAR(20),
   `sentDate` DATETIME,
   PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`sentsologinfo` ADD COLUMN `corpid` VARCHAR(45) DEFAULT NULL AFTER `sentDate`,  ADD COLUMN `soId` BIGINT(20) UNSIGNED AFTER `corpid`;

ALTER TABLE `redsky`.`company` ADD COLUMN `autoFxUpdater` BIT(1) DEFAULT b'0' AFTER `cmmdmmAgent`;
