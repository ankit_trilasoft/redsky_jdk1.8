<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataSecurityFilterList.title"/></title> 
<meta name="heading" content="<fmt:message key='dataSecurityFilterList.heading'/>"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=3;i++)
			{
					document.forms['dataSecurityFilterList'].elements[i].value = "";
			}
}

</script>



</head>



<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/dataSecurityFilterForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set>

	<s:form id="dataSecurityFilterList" name="dataSecurityFilterList" action="" method="post" validate="true">   
	<div id="Layer1" style="width:85%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			
	<table class="table">
	<thead>
	<tr>
	<th><fmt:message key="dataSecurityFilterList.name"/></th>
	<th><fmt:message key="dataSecurityFilterList.tableName"/></th>
	<th><fmt:message key="dataSecurityFilterList.fieldName"/></th>
	<th><fmt:message key="dataSecurityFilterList.filterValues"/></th>
	
	
	</tr></thead>
	
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="dataSecurityFilter.name" required="true" cssClass="input-text" size="40"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.tableName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.fieldName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.filterValues" required="true" cssClass="input-text" size="20"/></td>
			</tr>
			 <tr>
			 <td colspan="3"></td>
			 <td style="border-left: hidden;">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
       
   			</td>
		</tr>
		</tbody>
	</table>
	
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Tab Permission List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="" class="table" requestURI="" id="" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property=""  href="dataSecurityFilterForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="dataSecurityFilterList.id"/>
   	<display:column property=""  sortable="true" title="Name"/>
   	<display:column property=""  sortable="true" title="TableName"/>
   	<display:column property=""  sortable="true" title="FieldName"/>
   	<display:column property=""  sortable="true" title="Filter Values"/>
   	<display:column property=""  sortable="true" title="CorpID"/>
   	
   	</display:table>
	
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 

<script type="text/javascript"> 

highlightTableRows("dataSecurityFilterListId"); 

</script> 