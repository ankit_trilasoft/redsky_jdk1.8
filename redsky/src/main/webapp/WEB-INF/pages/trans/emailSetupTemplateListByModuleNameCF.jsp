<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<head>   
    <title>Email Setup Template List</title>   
    <meta name="heading" content="Email Setup Template List"/>  
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tinymce.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtable-email.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtooltip.css'/>" />
 <style>
 span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
</style>
</head>
<s:hidden name="noteFor" value="CustomerFile" />
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="idOfWhom" value="${customerFile.id}" />
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
<c:set var="tableName" value="customerfile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<div id="layer1" style="width:100%"> 
	<div id="newmnav">		  
		  <ul>
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("cid") %>" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
				<li><a href="customerServiceOrders.html?id=<%=request.getParameter("cid") %> " ><span>Service Orders</span></a></li>
			</c:if>
			<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
				<li><a href="customerServiceOrders.html?id=<%=request.getParameter("cid") %> " ><span>Quotes</span></a></li>
			</c:if>
			<sec-auth:authComponent componentId="module.tab.customerFile.accountPolicyTab">
		    	<configByCorp:fieldVisibility componentId="component.tab.customerFile.AccountPolicy">		    	
		    		<li><a href="showAccountPolicy.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li>
		    	</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.customerFile.reportTab">
		    <configByCorp:fieldVisibility componentId="component.tab.customerFile.Reports">
		    <c:if test="${customerFile.moveType=='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=quotation&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=quotation&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		   	<c:if test="${customerFile.moveType!='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=customerFile&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		    </configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">				
		    	<li><a onclick="window.open('auditList.html?id=<%=request.getParameter("cid") %>&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		   	</sec-auth:authComponent>
		   <li id="newmnav1" style="background:#FFF "><a class="current"><span>View Emails<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</ul>
		</div><div class="spn">&nbsp;</div>

	<display:table name="emailSetupTemplateList" class="table" requestURI="" id="emailSetupTemplateList" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >
	
		<display:column property="emailTo" title="To" sortable="true" maxLength="35" style="width:50px;"></display:column>
		<display:column property="emailFrom" title="From" sortable="true" maxLength="35" style="width:50px;"></display:column>
		<display:column title="Description" sortable="true" style="width:150px;">
			<div id="emailTemplateDetails${emailSetupTemplateList.id}" class="modal fade"></div>
			<a onclick="openEmailTemplatePopUp('${emailSetupTemplateList.id}','<%=request.getParameter("cid") %>')" />${emailSetupTemplateList.saveAs}</a>
			<div class="modal fade" id="ajaxRunningDiv${emailSetupTemplateList.id}" aria-hidden="true">
				<div class="loading-indicator">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"" style="font-size:54px;color:grey;" id="loading-indicator${emailSetupTemplateList.id}"></i>
				</div>
			</div>
		</display:column>
		<display:column property="emailSubject" title="Subject" sortable="true" style="width:250px;"></display:column>
		<display:column title="Last Sent On" style="width:50px;" >
			<c:choose> 
				<c:when test="${emailSetupTemplateList.emailStatus=='SaveForEmail'}">
					<div ALIGN="center">
						<img  src="${pageContext.request.contextPath}/images/ques-small.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP title="Ready For Sending"/>
					</div>
				</c:when>
				<c:when test="${fn:indexOf(emailSetupTemplateList.emailStatus,'SaveForEmail')>-1}">
					<div ALIGN="center">
						<img  src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP title="Fail"/>
					</div>
				</c:when> 							
				<c:otherwise>
					<fmt:parseDate pattern="MM/dd/yyyy HH:mm" value="${emailSetupTemplateList.emailStatus}" var="parsedEmailStatusDate" />
					<fmt:formatDate pattern="dd-MMM-yy HH:mm" value="${parsedEmailStatusDate}" var="formattedEmailStatusDate" />
						<c:out value="${formattedEmailStatusDate}"></c:out>
				</c:otherwise>			
			</c:choose>
		</display:column>
		<display:column title="Action" headerClass="centeralign" style="text-align:center;width:100px;">
			<button type="button" style="width:4.6em;" class="btn btn-success btn-blue btn-xs" id="load${emailSetupTemplateList.id}" onclick="sendMailFromTemplate('${customerFile.id}','${emailSetupTemplateList.id}')"  data-loading-text="<i class='fa fa-spinner fa-spin ' style='font-size:17px;'></i>"><i class='fa fa-paper-plane-o'></i>&nbsp;Send</button>
			<div id="emailTemplateDetailsEdit${emailSetupTemplateList.id}" class="modal fade"></div>
			<button type="button" style="width:4em;" class="btn btn-primary btn-xs" id="editLoad${emailSetupTemplateList.id}" onclick="editMailFromTemplate('${customerFile.id}','${emailSetupTemplateList.id}','edit${emailSetupTemplateList.id}')" ><i class='fa fa-pencil'></i>&nbsp;Edit</button>
			<div class="modal fade" id="sendMail${emailSetupTemplateList.id}" aria-hidden="true">
				<div class="modal-dialog" style="width:35%;">
					<div class="modal-content">
						<div class="modal-body" id="modalAlert">
				      		<div class="alert alert-success" id="getSuccessMsg${emailSetupTemplateList.id}">
				      			<strong>${returnAjaxStringValue}</strong>
				    		</div>
				    		<div class="alert alert-danger" id="getErrorMsg${emailSetupTemplateList.id}">
							  <strong>${returnAjaxStringValue}</strong>
							</div>
				    	</div>
				    	<div class="modal-footer">
					        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Close</button>
					    </div>
					</div>
				</div>
			</div>
		</display:column>
	</display:table>
	<div class="modal fade" id="showFileErrorModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				    <div class="modal-body" style="font-weight:600;">
					    <div class="alert alert-danger" id="getFileErrorMsg">
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-sm btn-danger" onclick="redirectModal();" data-dismiss="modal">Close</button>
				    </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function openEmailTemplatePopUp(parentId,cid){
	  var $j = jQuery.noConflict();
	  	$j("#loading-indicator"+parentId).show();
		$j("#ajaxRunningDiv"+parentId).modal({show:true,backdrop: 'static',keyboard: false});
		  	$j.ajax({
	            url: 'editEmailSetupTemplatePopUpAjax.html?id='+parentId+'&cid='+cid+'&decorator=modal&popup=true',
	            success: function(data){
	            	$j("#loading-indicator"+parentId).hide();
				    $j("#ajaxRunningDiv"+parentId).modal("hide");
				    $j("#emailTemplateDetails"+parentId).modal({show:true,backdrop: 'static',keyboard: false});
					$j("#emailTemplateDetails"+parentId).html(data);
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#emailTemplateDetails"+parentId).modal("hide");
	                $j("#loading-indicator"+parentId).hide();
				    $j("#ajaxRunningDiv"+parentId).modal("hide");
	             }   
    	});  
}
function closeModal(parentId){
	tinymce.remove('#emailBody'+parentId);
}
function closeModalEdit(parentId){
	tinymce.remove('#emailBody'+parentId);
}
function closeErrorModal(parentId){
	var $j = jQuery.noConflict();
    $j("#showErrorModal"+parentId).modal("hide");
}
function sendMailFromTemplate(cid,emailId){
	var $j = jQuery.noConflict();
  	$j('#load'+emailId).button('loading');
	new Ajax.Request('/redsky/sendEmailFromEmailTemplateAjax.html?ajax=1&cid='+cid+'&id='+emailId+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    	var response = transport.responseText || "no response text";
			    	$j("#sendMail"+emailId).modal('show');
			    	var check = response.trim().indexOf("succesfully");
					if(check >= 0){
						$j("#getSuccessMsg"+emailId).html(response.trim());
						$j("#getSuccessMsg"+emailId).show();
						$j("#getErrorMsg"+emailId).hide();
					}else{
						$j("#getErrorMsg"+emailId).html(response.trim());
						$j("#getErrorMsg"+emailId).show();
						$j("#getSuccessMsg"+emailId).hide();
					}
			    	$j('#load'+emailId).button('reset');
			    },
			    onFailure: function(){ 
				}
			  });
}
function editMailFromTemplate(cid,emailId,editModalId){
	var $j = jQuery.noConflict();
	$j("#loading-indicator"+emailId).show();
	$j("#ajaxRunningDiv"+emailId).modal({show:true,backdrop: 'static',keyboard: false});
	  	$j.ajax({
            url: 'editEmailPopupFromEmailTemplateAjax.html?decorator=modal&popup=true&cid='+cid+'&id='+emailId+'&editModalId='+editModalId,
            success: function(data){
            	$j("#loading-indicator"+emailId).hide();
			    $j("#ajaxRunningDiv"+emailId).modal("hide");
			    $j("#emailTemplateDetailsEdit"+emailId).modal({show:true,backdrop: 'static',keyboard: false});
				$j("#emailTemplateDetailsEdit"+emailId).html(data);
            },
            error: function () {
                alert('Some error occurs');
                $j("#emailTemplateDetailsEdit"+emailId).modal("hide");
                $j("#loading-indicator"+emailId).hide();
			    $j("#ajaxRunningDiv"+emailId).modal("hide");
             }   
	});
}
function redirectModal(){
	var cid = "<%=request.getAttribute("cid")%>";
	var replaceURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"";
	window.location.replace(replaceURL);
}
try{
    if('${fileError}' == 'NoFile'){
     	var $j = jQuery.noConflict();
 		$j('#getFileErrorMsg').html("<p>This file is currently not available.</p>");
 		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
    }
    //$j("#loading-indicator").hide();
}catch(e){}
</script>