package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.TableCatalogDao;
import com.trilasoft.app.model.TableCatalog;
import com.trilasoft.app.service.TableCatalogManager;


public class TableCatalogManagerImpl extends GenericManagerImpl<TableCatalog, Long> implements TableCatalogManager { 
	TableCatalogDao tableCatalogDao;
	public TableCatalogManagerImpl(TableCatalogDao tableCatalogDao) {
		super(tableCatalogDao); 
        this.tableCatalogDao = tableCatalogDao; 
		// TODO Auto-generated constructor stub
	}
	
	public List getTableListValue(String tableN,String tableD){
		 return tableCatalogDao.getTableListValue(tableN, tableD);
	}
	public List getData(String tableName,String sessionCorpID){
		return tableCatalogDao.getData(tableName,sessionCorpID);
	}
}
