package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.Quotation;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PartnerQuoteManager;
import com.trilasoft.app.service.QuotationManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.webapp.util.EncodingUtil;

public class QuotationAction extends BaseAction {

	private QuotationManager quotationManager;
	private EmailSetupManager emailSetupManager;

	private Quotation quotation;

	private List quotations;

	private CustomerFileManager customerFileManager;

	private CustomerFile customerFile;
	
	private MiscellaneousManager miscellaneousManager;
	private Miscellaneous miscellaneous;

	private ServiceOrderManager serviceOrderManager;

	private ServiceOrder serviceOrder;

	private PartnerQuoteManager partnerQuoteManager;

	private List partnerQuotes;

	private PartnerQuote partnerQuote;

	private Long id;

	private Long quoteID;

	private String quotationToatalPrice;

	private long IdMax;
	private List companyDivis;
	private String bookCode;

	private String sessionCorpID;
	private String partnerCode;
	private List BookingAgentList;
	private Map<String, String> bookingAgencyMap;
	private String email;
	private String sequenceNumber;
	private String MOLmail=(String)AppInitServlet.redskyConfigMap.get("Mol_MailId");
	private String lastName;
	private String body;
	private String subject;
	private String filter;
	private String userType;
	private Company company;
	private CompanyManager companyManager;
	private Boolean cmmDmmFlag=false;
	
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(QuotationAction.class);

	public QuotationAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.userType= user.getUserType();
	}

	public List getQuotations() {
		return quotations;
	}

	public void setQuotations(List quotations) {
		this.quotations = quotations;
	}

	public void setQuotationManager(QuotationManager quotationManager) {
		this.quotationManager = quotationManager;
	}

	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PartnerQuote getPartnerQuote() {
		return partnerQuote;
	}

	public void setPartnerQuote(PartnerQuote partnerQuote) {
		this.partnerQuote = partnerQuote;
	}

	public List getPartnerQuotes() {
		return partnerQuotes;
	}

	public void setPartnerQuotes(List partnerQuotes) {
		this.partnerQuotes = partnerQuotes;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setPartnerQuoteManager(PartnerQuoteManager partnerQuoteManager) {
		this.partnerQuoteManager = partnerQuoteManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (quotation.getId()== null);
		if(isNew)
		{
			quotation.setCreatedOn(new Date());
			quotation.setCreatedBy(getRequest().getRemoteUser());
			quotation.setUpdatedOn(new Date());
			quotation.setUpdatedBy(getRequest().getRemoteUser());
		}
		if(!isNew)
		{
			quotation.setUpdatedOn(new Date());
			quotation.setUpdatedBy(getRequest().getRemoteUser());
		}
		quotation.setQuoteStatus("Processing");
		quotation.setPqId(quoteID);
		quotationManager.processQuotation(quoteID);
		quotationManager.save(quotation);
		List maxmimum=quotationManager.findMaximumId();
		if(maxmimum!=null && !maxmimum.isEmpty() && maxmimum.get(0)!=null)
			quotation.setId(Long.parseLong(maxmimum.get(0).toString()));
		getComboList(sessionCorpID);
		String key = (isNew) ? "quotation.added" : "quotation.updated";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	// for Save and Submit
	@SkipValidation
	public String saveSubmit() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		quotation.setQuoteStatus("Submitted");
		quotationManager.submitQuotation(quoteID);
		getComboList(sessionCorpID);
		//String key = "Quotation has been submitted";
		saveMessage("Quotation has been submitted");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private Long sid;
	private Long cid;
	private String btntype;

	public String getBtntype() {
		return btntype;
	}

	public void setBtntype(String btntype) {
		this.btntype = btntype;
	}

	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (quoteID != null) {
			customerFile = customerFileManager.get(cid);
			partnerQuote = partnerQuoteManager.get(quoteID);
			if(btntype == null || btntype.equalsIgnoreCase("next")){
				List nextSidTemp =quotationManager.nextServiceOrder(partnerQuote.getRequestedSO(), sid);
				Long nextSid = new Long(0) ;
				if(nextSidTemp!=null && !nextSidTemp.isEmpty() && nextSidTemp.get(0)!=null)
					nextSid=Long.parseLong(nextSidTemp.get(0).toString());
				serviceOrder = serviceOrderManager.get(nextSid);
				sid = nextSid;
			}else if(btntype.equalsIgnoreCase("previous")){
				Long previousSid = new Long(0);
				List previousSidTemp = quotationManager.previousServiceOrder(partnerQuote.getRequestedSO(), sid);
				if(previousSidTemp!=null && !previousSidTemp.isEmpty() && previousSidTemp.get(0)!=null)
					previousSid=Long.parseLong(previousSidTemp.get(0).toString());
				serviceOrder = serviceOrderManager.get(previousSid);
				sid = previousSid;
			}
			
			miscellaneous = miscellaneousManager.get(sid);
						
			getComboList(sessionCorpID);
			List portalRefList = quotationManager.checkQuotation2(sid, quoteID);
			if (portalRefList.isEmpty()) {
				quotation = new Quotation();
				quotation.setShipmentNumber(serviceOrder.getShipNumber());
				quotation.setCreatedOn(new Date());
				quotation.setCreatedBy(getRequest().getRemoteUser());
				quotation.setUpdatedOn(new Date());
				quotation.setUpdatedBy(getRequest().getRemoteUser());
				if (miscellaneous.getUnit1().equals("Lbs"))
				{
					if(miscellaneous.getEstimateGrossWeight()==null || miscellaneous.getEstimateGrossWeight().equals(new BigDecimal("0.00")))
					{
						quotation.setActualNetWeight(new BigDecimal("0.00"));
						quotation.setNetWeightkg(new BigDecimal("0.00"));
	     			}else{
	     				quotation.setActualNetWeight(miscellaneous.getEstimateGrossWeight());
	     				double actualNetWeight=Double.parseDouble(miscellaneous.getEstimateGrossWeight().multiply(new BigDecimal(0.45359237)).toString());
	     				double actualNetWeight1=Math.round((actualNetWeight*100)/100);
	     				BigDecimal actualNetWeightRnd=new BigDecimal(actualNetWeight1);
	    				quotation.setNetWeightkg(actualNetWeightRnd);
	     			}
				

				} else if (miscellaneous.getUnit1().equals("Kgs"))
				{
					if(miscellaneous.getEstimateGrossWeight()==null || miscellaneous.getEstimateGrossWeight().equals(new BigDecimal("0.00")))
					{
						quotation.setNetWeightkg(new BigDecimal("0.00"));
						quotation.setActualNetWeight(new BigDecimal("0.00"));
	     			}else{
	     				quotation.setNetWeightkg(miscellaneous.getEstimateGrossWeight());
	     				double actualNetWeight=Double.parseDouble(miscellaneous.getEstimateGrossWeight().multiply(new BigDecimal(2.20462262)).toString());
	     				double actualNetWeight1=Math.round((actualNetWeight*100)/100);
	     				BigDecimal actualNetWeightRnd=new BigDecimal(actualNetWeight1);
	     				quotation.setActualNetWeight(actualNetWeightRnd);
	     			}
					
				

				}

				if (miscellaneous.getUnit2().equals("Cft"))
				{
					if(miscellaneous.getEstimateCubicFeet()== null || miscellaneous.getEstimateCubicFeet().equals(new BigDecimal("0.00")))
					{
						quotation.setActualCubicFeet(new BigDecimal("0.00"));
						quotation.setNetWeightcbm(new BigDecimal("0.00"));
	     			}
					else{
						quotation.setActualCubicFeet(miscellaneous.getEstimateCubicFeet());
						double  netWeightcbm=Double.parseDouble(miscellaneous.getEstimateCubicFeet().multiply(new BigDecimal(0.0283168466)).toString()) ;
						double netWeightcbm1=Math.round((netWeightcbm*100)/100);
						BigDecimal netWeightcbmRnd=new BigDecimal(netWeightcbm1);
						quotation.setNetWeightcbm(netWeightcbmRnd);
					}
					
				
				} else if (miscellaneous.getUnit2().equals("Cbm")) 
				{
					if(miscellaneous.getEstimateCubicFeet()==null || miscellaneous.getEstimateCubicFeet().equals(new BigDecimal("0.00")))
					{
						quotation.setNetWeightcbm(new BigDecimal("0.00"));
						quotation.setActualCubicFeet(new BigDecimal("0.00"));
	     			}
					else{
						quotation.setNetWeightcbm(miscellaneous.getEstimateCubicFeet());
						double netWeightcbm=Double.parseDouble(miscellaneous.getEstimateCubicFeet().multiply(new BigDecimal(35.3146667)).toString());
						double netWeightcbm1=Math.round((netWeightcbm*100)/100);
						BigDecimal netWeightcbmRnd =new BigDecimal(netWeightcbm1);
						quotation.setActualCubicFeet(netWeightcbmRnd);
						}
					}
				
				
				quotation.setBillToName(customerFile.getBillToName());
				quotation.setCommodity(serviceOrder.getCommodity());
				quotation.setCorpID(serviceOrder.getCorpID());
				quotation.setEquipment(miscellaneous.getEquipment());
				quotation.setMode(serviceOrder.getMode());
				quotation.setOriginCity(serviceOrder.getOriginCity());
				quotation.setPackingMode(serviceOrder.getPackingMode());
				quotation.setVendorName(partnerQuote.getVendorName());
				quotation.setQuote(partnerQuote.getQuote());
				quotation.setQuoteStatus(partnerQuote.getQuoteStatus());
				quotation.setQuoteType(partnerQuote.getQuoteType());
				quotation.setCorpID(customerFile.getCorpID());
				quotation.setCid(cid);
				quotation.setSid(sid);
				quotation.setPqId(quoteID);
			} else {
				 List  quotationTemp = quotationManager.getQuotation(sid, quoteID);
				 if(quotationTemp!=null && !quotationTemp.isEmpty() && quotationTemp.get(0)!=null)
					 quotation = (Quotation)quotationTemp.get(0);
				//quotation.setCreatedBy(getRequest().getRemoteUser());
				quotation.setCreatedOn(new Date());
			}
			Long maximumId = Long.parseLong((quotationManager.maxId(partnerQuote.getRequestedSO()).get(0).toString()));
			quotation.setMaxId(maximumId);
			Long minimumId = Long.parseLong((quotationManager.minId(partnerQuote.getRequestedSO()).get(0).toString()));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: vanlineSettleColourStatus "+minimumId);
			quotation.setMinId(minimumId);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	  public String BookingAgentName(){   
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		company = companyManager.findByCorpID(sessionCorpID).get(0);
        if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent()) || (company.getUTSI()!=null  && company.getUTSI())){
    		cmmDmmFlag = true;
    		}
		if(partnerCode!=null){
			BookingAgentList=quotationManager.BookingAgentName(partnerCode, sessionCorpID, cmmDmmFlag);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: vanlineSettleColourStatus "+BookingAgentList);
		}else{
	        	String message="The Vendor Name is not exist";
	        	errorMessage(getText(message));
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS; 
	  }
	@SkipValidation
	public String findBookCodeStatus(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		bookCode=quotationManager.findBookCodeStatusAjax(sessionCorpID, bookCode);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findBookAgentCodeStatus() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		companyDivis = quotationManager.findBookCodeStatus(sessionCorpID, bookCode);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findQuotesBookAgentStatus() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		bookingAgencyMap = quotationManager.findBookCodeQuotes(sessionCorpID, id);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	private RefMasterManager refMasterManager;

	private Map<String, String> pbasis;

	private Map<String, String> pcurrency;

	private HashSet partnerQuotess;

	public HashSet getPartnerQuotess() {
		return partnerQuotess;
	}

	public void setPartnerQuotess(HashSet partnerQuotess) {
		this.partnerQuotess = partnerQuotess;
	}

	public Map<String, String> getPbasis() {
		return pbasis;
	}

	public void setPbasis(Map<String, String> pbasis) {
		this.pbasis = pbasis;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getComboList(String corpId) {
		pbasis = refMasterManager.findByParameter(corpId, "PBASIS");
		pcurrency = refMasterManager.findCodeOnleByParameter(corpId, "PCURRENCY");
		return SUCCESS;
	}

	public Map<String, String> getPcurrency() {
		return pcurrency;
	}

	public void setPcurrency(Map<String, String> pcurrency) {
		this.pcurrency = pcurrency;
	}

	public long getIdMax() {
		return IdMax;
	}

	public void setIdMax(long idMax) {
		IdMax = idMax;
	}

	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean quote = partnerQuote.getQuote() == null;
		boolean quoteStatus = partnerQuote.getQuoteStatus() == null;
		String vendorCodeSet="";
		if(userType.equalsIgnoreCase("AGENT")){
    		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = (User)auth.getPrincipal();
            Map filterMap=user.getFilterMap(); 
        	List userList=new ArrayList();
        	try {
        		List partnerCorpIDList = new ArrayList(); 
        		List list2 = new ArrayList();
        		if (filterMap!=null && (!(filterMap.isEmpty()))) {
        		if(filterMap.containsKey("agentFilter")){
			    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
			    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
			    	Iterator iterator = valueSet.iterator();
			    	while (iterator.hasNext()) {
			    		String  mapkey = (String)iterator.next(); 
					    if(filterMap.containsKey(mapkey)){	
						HashMap valueMap=	(HashMap)filterMap.get(mapkey);
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
							List partnerCorpIDList1 = new ArrayList();
							partnerCorpIDList1= (List)entry.getValue();
							Iterator listIterator= partnerCorpIDList1.iterator();
					    	while (listIterator.hasNext()) {
					    		String value=listIterator.next().toString();
					    		if(value!=null && ((value.trim().equalsIgnoreCase("--NO-FILTER--")))){
					    		partnerCorpIDList.add("'" + listIterator.next().toString() + "'");
					    		}
					    	}
							
							}
						}
						}
						}
			    	 }
			    	 Iterator listIterator= partnerCorpIDList.iterator();
				    	while (listIterator.hasNext()) {
				    		list2.add("'" + listIterator.next().toString() + "'");	
				    	}
				    	vendorCodeSet= list2.toString().replace("[","").replace("]", "");
				    	}
			    	 }
        	
        	}catch(Exception e){
        		
        	}
    	}
		if (!quote || !quoteStatus) {
			List ls = quotationManager.findByQuotation(partnerQuote.getQuote(), partnerQuote.getQuoteStatus(),sessionCorpID,vendorCodeSet);
			partnerQuotess = new HashSet(ls);
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String sendMOLMailMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String tempLink = "";
		if(filter.equals("Starline")){
			tempLink = "http://starline.movesonline.com/step1.aspx?PIN="+sequenceNumber+"&LNAME="+EncodingUtil.encodeURIComponent(lastName) ;
		}else{
			tempLink = "http://highland.movesonline.com/step1.aspx?PIN="+sequenceNumber+"&LNAME="+EncodingUtil.encodeURIComponent(lastName) ;
		}
		body = body + "<a href="+tempLink+">http://highland.movesonline.com/step1.aspx?PIN="+sequenceNumber+"&LNAME="+lastName+"</a>";
		// for testing
		body = body +"<br/><br/> For testing <br/><br/>" +
				"<a href = http://redsky.movesonline.com/step1.aspx?PIN="+sequenceNumber+"&LNAME="+EncodingUtil.encodeURIComponent(lastName)+">" +
						"http://redsky.movesonline.com/step1.aspx?PIN="+sequenceNumber+"&LNAME="+lastName+"</a>";
		EmailSetup emailSetup= new EmailSetup();
		emailSetup.setAttchedFileLocation("");
		emailSetup.setBody(body);
		emailSetup.setSignaturePart("");
		emailSetup.setCorpId(sessionCorpID);
		emailSetup.setCreatedBy(getRequest().getRemoteUser());
		emailSetup.setCreatedOn(new Date());
		emailSetup.setDateSent(new Date());
		emailSetup.setEmailStatus("SaveForEmail");
		emailSetup.setRecipientBcc("");
		emailSetup.setRecipientCc("");
		emailSetup.setRecipientTo(email);
		emailSetup.setRetryCount(0);
		emailSetup.setSignature(MOLmail);
		emailSetup.setSubject(subject);
		emailSetup.setUpdatedBy(getRequest().getRemoteUser());
		emailSetup.setUpdatedOn(new Date());
		emailSetup.setFileNumber(sequenceNumber);
		emailSetup=emailSetupManager.save(emailSetup);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getQuotationToatalPrice() {
		return quotationToatalPrice;
	}

	public void setQuotationToatalPrice(String quotationToatalPrice) {
		this.quotationToatalPrice = quotationToatalPrice;
	}

	public Long getQuoteID() {
		return quoteID;
	}

	public void setQuoteID(Long quoteID) {
		this.quoteID = quoteID;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public List getBookingAgentList() {
		return BookingAgentList;
	}

	public void setBookingAgentList(List bookingAgentList) {
		BookingAgentList = bookingAgentList;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public Map<String, String> getBookingAgencyMap() {
		return bookingAgencyMap;
	}

	public void setBookingAgencyMap(Map<String, String> bookingAgencyMap) {
		this.bookingAgencyMap = bookingAgencyMap;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	} 

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Boolean getCmmDmmFlag() {
		return cmmDmmFlag;
	}

	public void setCmmDmmFlag(Boolean cmmDmmFlag) {
		this.cmmDmmFlag = cmmDmmFlag;
	}


}
