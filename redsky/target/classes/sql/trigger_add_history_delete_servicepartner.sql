DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_servicepartner BEFORE DELETE ON servicepartner
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

   IF (OLD.sequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;
   IF (OLD.ship <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;
   IF (OLD.createdOn <> '' or OLD.createdOn is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.createdOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdOn,'~') into oldValueList;
   END IF;
   IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
   IF (OLD.createdBy <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.createdBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdBy,'~') into oldValueList;
   END IF;
   IF (OLD.updatedOn <> '' or OLD.updatedOn is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.updatedOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedOn,'~') into oldValueList;
   END IF;
   IF (OLD.updatedBy <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.updatedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedBy,'~') into oldValueList;
   END IF;
   IF (OLD.actgCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.actgCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
   END IF;
   IF (OLD.polCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.polCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.polCode,'~') into oldValueList;
   END IF;
   IF (OLD.poeCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.poeCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.poeCode,'~') into oldValueList;
   END IF;
   IF (OLD.blNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.blNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.blNumber,'~') into oldValueList;
   END IF;
   IF (OLD.partnerType <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.partnerType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerType,'~') into oldValueList;
   END IF;
   IF (OLD.coord <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.coord~') into fieldNameList;
       select CONCAT(oldValueList,OLD.coord,'~') into oldValueList;
   END IF;
   IF (OLD.email <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.email~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
   END IF;
   IF (OLD.city <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
   END IF;
   IF (OLD.bookNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.bookNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bookNumber,'~') into oldValueList;
   END IF;
   IF (OLD.carrierArrival <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierArrival,'~') into oldValueList;
   END IF;
   IF (OLD.carrierDeparture <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierDeparture~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierDeparture,'~') into oldValueList;
   END IF;
   IF (OLD.eTATA <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.eTATA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.eTATA,'~') into oldValueList;
   END IF;
   IF (OLD.eTDTA <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.eTDTA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.eTDTA,'~') into oldValueList;
   END IF;
   IF (OLD.carrierPhone <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierPhone,'~') into oldValueList;
   END IF;
   IF (OLD.carrierClicd <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierClicd~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierClicd,'~') into oldValueList;
   END IF;
   IF (OLD.omni <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.omni~') into fieldNameList;
       select CONCAT(oldValueList,OLD.omni,'~') into oldValueList;
   END IF;
   IF (OLD.carrierNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierNumber,'~') into oldValueList;
   END IF;
   IF (OLD.carrierCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierCode,'~') into oldValueList;
   END IF;
   IF (OLD.carrierName <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierName,'~') into oldValueList;
   END IF;
   IF (OLD.carrierVessels <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierVessels~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierVessels,'~') into oldValueList;
   END IF;
   IF (OLD.etDepart <> '' or OLD.etDepart is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.etDepart~') into fieldNameList;
       select CONCAT(oldValueList,OLD.etDepart,'~') into oldValueList;
   END IF;
   IF (OLD.atDepart <> '' or OLD.atDepart is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.atDepart~') into fieldNameList;
       select CONCAT(oldValueList,OLD.atDepart,'~') into oldValueList;
   END IF;
   IF (OLD.etArrival <> '' or OLD.etArrival is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.etArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.etArrival,'~') into oldValueList;
   END IF;
   IF (OLD.houseBillIssued <> '' or OLD.houseBillIssued is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.houseBillIssued~') into fieldNameList;
       select CONCAT(oldValueList,OLD.houseBillIssued,'~') into oldValueList;
   END IF;
   IF (OLD.atArrival <> '' or OLD.atArrival is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.atArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.atArrival,'~') into oldValueList;
   END IF;
   IF (OLD.miles <> '' or OLD.miles is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.miles~') into fieldNameList;
       select CONCAT(oldValueList,OLD.miles,'~') into oldValueList;
   END IF;
   IF (OLD.pdCost1 <> '' or OLD.pdCost1 is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.pdCost1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdCost1,'~') into oldValueList;
   END IF;
   IF (OLD.pdCost2 <> '' or OLD.pdCost2 is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.pdCost2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdCost2,'~') into oldValueList;
   END IF;
   IF (OLD.pdFreedays <> '' or OLD.pdFreedays is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.pdFreedays~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdFreedays,'~') into oldValueList;
   END IF;
   IF (OLD.pdDays2 <> '' or OLD.pdDays2 is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.pdDays2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdDays2,'~') into oldValueList;
   END IF;
   IF (OLD.serviceOrderId <> '' or OLD.serviceOrderId is not null) THEN
       select CONCAT(fieldNameList,'servicepartner.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;

   CALL add_tblHistory (OLD.serviceOrderId,"servicepartner", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$