<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Freight MarkUp</title> 
<meta name="heading" content="Freight MarkUp"/>
<script type="text/javascript">
function checkFields(){
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.mode'].value==''){
		alert('Please select mode');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.lowCost'].value.trim()==''){
		alert('Please enter Cost Low value');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.highCost'].value.trim()==''){
		alert('Please enter Cost High value');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.currency'].value==''){
		alert('Please select currency');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.profitFlat'].value.trim()==''){
		alert('Please enter flat value');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.profitPercent'].value.trim()==''){
		alert('Please enter percentage value');
		return false;
	}
	if(document.forms['freightMarkUpForm'].elements['freightMarkUpControl.contract'].value==''){
		alert('Please select contract');
		return false;
	}
}

</script>

 
</head>

 <s:form id="freightMarkUpForm" name="freightMarkUpForm" action="savefreightMarkUp" method="post" validate="true">
 <s:hidden name="freightMarkUpControl.id"/>
 
 <div id="Layer1" style="width:100%"> 

		<div id="newmnav">
		  <ul>
		  	
		  	<li><a href="freightMarkUpList.html"><span>FreightMarkUp List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>FreightMarkUp Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:5px; "><span></span></div>
   <div class="center-content">		

  	<table class="detailTabLabel" border="0" width="600px">
		  <tbody>  	
		  	
		  	<tr class="listwhitetext">
		  		<td align="right">Mode</td>
		  		<td align="left"><s:select cssClass="list-menu" name="freightMarkUpControl.mode" list="{'Air','Sea'}" headerKey="" headerValue="" cssStyle="width:84px" /></td>
		  	</tr>
		  	<tr class="listwhitetext">
		  		<td align="right">Cost Low</td>
		  		<td align="left"><s:textfield name="freightMarkUpControl.lowCost" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Cost High</td>
		  		<td align="left"><s:textfield name="freightMarkUpControl.highCost" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Currency</td>
		  		<td align="left"><s:select cssClass="list-menu" name="freightMarkUpControl.currency" list="%{currencyList}" headerKey="" headerValue="" cssStyle="width:150px" /></td>
		  	</tr>
		  	<tr class="listwhitetext">
		  		<td align="right">Flat</td>
		  		<td align="left"><s:textfield name="freightMarkUpControl.profitFlat" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Percentage</td>
		  		<td align="left"><s:textfield name="freightMarkUpControl.profitPercent" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		 		<td align="right">Contract</td>
		  		<td align="left"><s:select cssClass="list-menu" name="freightMarkUpControl.contract" list="%{contract}" headerKey="" headerValue="" cssStyle="width:150px"/></td>
		 	</tr>
		 
	  		 <tr><td align="left" class="listwhite" height="20"></td></tr>
		</tbody>
	</table>

</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
		  <table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td>
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="freightMarkUpControlCreatedOnFormattedValue" value="${freightMarkUpControl.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="freightMarkUpControl.createdOn" value="${freightMarkUpControlCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${freightMarkUpControl.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty freightMarkUpControl.id}">
								<s:hidden name="freightMarkUpControl.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{freightMarkUpControl.createdBy}"/></td>
							</c:if>
							<c:if test="${empty freightMarkUpControl.id}">
								<s:hidden name="freightMarkUpControl.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="freightMarkUpControlupdatedOnFormattedValue" value="${freightMarkUpControl.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="freightMarkUpControl.updatedOn" value="${freightMarkUpControlupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${freightMarkUpControl.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty freightMarkUpControl.id}">
								<s:hidden name="freightMarkUpControl.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{freightMarkUpControl.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty freightMarkUpControl.id}">
								<s:hidden name="freightMarkUpControl.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		  
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" onclick="return checkFields()"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty freightMarkUpControl.id}">
		       <input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/freightMarkUpForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table></div>

</s:form>