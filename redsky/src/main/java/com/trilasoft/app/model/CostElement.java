package com.trilasoft.app.model;

import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table ( name="costelement") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class CostElement extends BaseObject{
	
	
	private Long id;
	private String corpId;
	private String costElement;
	private String description;
	private String recGl;
	private String payGl;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String reportingAlias; 
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("costElement", costElement)
				.append("description", description).append("recGl", recGl)
				.append("payGl", payGl).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("reportingAlias", reportingAlias).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CostElement))
			return false;
		CostElement castOther = (CostElement) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(costElement, castOther.costElement)
				.append(description, castOther.description)
				.append(recGl, castOther.recGl).append(payGl, castOther.payGl)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(reportingAlias, castOther.reportingAlias).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(costElement).append(description).append(recGl)
				.append(payGl).append(createdBy).append(createdOn)
				.append(updatedBy).append(updatedOn).append(reportingAlias)
				.toHashCode();
	}
	@Column(length = 25)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
   @Column(nullable=false,length=45,unique=true)
	public String getCostElement() {
		return costElement;
	}
	public void setCostElement(String costElement) {
		this.costElement = costElement;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length = 30)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 20)
	public String getPayGl() {
		return payGl;
	}
	public void setPayGl(String payGl) {
		this.payGl = payGl;
	}
	@Column(length = 20)
	public String getRecGl() {
		return recGl;
	}
	public void setRecGl(String recGl) {
		this.recGl = recGl;
	}
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 50)
	public String getReportingAlias() {
		return reportingAlias;
	}
	public void setReportingAlias(String reportingAlias) {
		this.reportingAlias = reportingAlias;
	}

	
}
