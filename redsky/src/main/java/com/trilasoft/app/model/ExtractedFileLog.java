package com.trilasoft.app.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="extractedfilelog")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class ExtractedFileLog extends BaseObject implements Serializable{
	private String corpID;
	private Long id;
	private String fileName;
    private String location;
    private String createdBy;
    private Date createdOn;
    private String module;
    private String messageLog;
    
    @Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id)
				.append("fileName", fileName)
				.append("location", location)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("module", module)
				.append("messageLog", messageLog)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ExtractedFileLog))
			return false;
		ExtractedFileLog castOther = (ExtractedFileLog) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID) 
				.append(id, castOther.id) 
				.append(location, castOther.location)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(module, castOther.module)
				.append(messageLog, castOther.messageLog)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID)
				.append(id).append(fileName).append(location)
				.append(createdBy).append(createdOn).append(module)
				.append(messageLog)
				.toHashCode();
	} 
    @Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getMessageLog() {
		return messageLog;
	}

	public void setMessageLog(String messageLog) {
		this.messageLog = messageLog;
	}
}
