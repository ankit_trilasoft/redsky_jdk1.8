/**
 * @Class Name  Carton Dao Hibernate
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CartonDao;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;

public class CartonDaoHibernate extends GenericDaoHibernate<Carton, Long> implements CartonDao
{   
  
    public CartonDaoHibernate() 
    {   
        super(Carton.class);   
    
    }
    public List<Carton> findByLastName(String lastName) {   
        return getHibernateTemplate().find("from Carton where lastName=?", lastName);   
    }
        public List findMaxId() {
    		
    		return getHibernateTemplate().find("select max(id) from Carton");
    	}
        public List checkById(Long id) {
        	return getHibernateTemplate().find("select id from Carton where id=?",id);
        }
        
        public List findMaximumIdNumber(String shipNum) {
        	List list=  this.getSession().createSQLQuery("select max(idNumber) from carton where shipNumber='"+shipNum+"'").list();
        	return list;
        }
        public List getGross(String shipNumber){
        	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(grossWeight, 0)) is null, 0.00, sum(coalesce(grossWeight, 0))) from carton where status = true and  shipNumber='"+shipNumber+"'").list();
        	return L1;
        }
        public List getTare(String shipNumber)
        {
        	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(emptyContWeight, 0)) is null, 0.00, sum(coalesce(emptyContWeight, 0))) from carton where  status = true and   shipNumber='"+shipNumber+"'").list();
        	return L1;
        }
        public List getNet(String shipNumber)
        {
        	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(netWeight, 0)) is null , 0.00 , sum(coalesce(netWeight, 0))) from carton where  status = true and  shipNumber='"+shipNumber+"'").list();
        	return L1;
        }
        public List getVolume(String shipNumber){
        	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(volume, 0)) is null, 0.00, sum(coalesce(volume, 0)) ) from carton where status = true and   shipNumber='"+shipNumber+"'").list();
        	return L1;
        }
        public List getPieces(String shipNumber){
        	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(pieces, 0)) is null, 0, sum(coalesce(pieces, 0))) from carton where  status = true and  shipNumber='"+shipNumber+"'").list();
        	return L1;
        }
        public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
        	String unitQuery="";
        	String unit ="";
    		List l =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+sessionCorpID+"'").list();
    		if(l!=null && !l.isEmpty() && l.get(0)!=null && (!(l.get(0).toString().trim().equals("")))){
    			unit = l.get(0).toString();
    		}
    		if(unit !=null && unit.trim().equalsIgnoreCase("YES")){
    			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
    		}
        	 
        	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualGrossWeight='" +actualGrossWeight+ "' , actualNetWeight='" +actualNetWeight+ "' "+unitQuery+" , actualTareWeight='" +actualTareWeight+"' ,updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
        	
        	ServiceOrder serviceOrder = new ServiceOrder();
        	List linkedShipNumberList= new ArrayList();
        	List soList =new ArrayList();
        	
    		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
    		try{
    			soList = getHibernateTemplate().find(query1);
    			if(!soList.isEmpty()){
    				 serviceOrder = (ServiceOrder)soList.get(0);
    			}
    		}catch(Exception e){
    			e.getStackTrace();
    		}
    		
    	   	try{
    	        if(serviceOrder.getIsNetworkRecord()){		        	
    	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	        	}else{
    	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	        	}		        	
    	        	
    				Iterator  it=linkedShipNumberList.iterator();
    		    	while(it.hasNext()){			    			
    		    		String shipNum=(String)it.next();
    		    		String corpId = shipNum.substring(0, 4);
    		    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
    		    			unitQuery="";
    		            	String unit1 ="";
    		        		List list =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+corpId+"'").list();
    		        		if(list!=null && !list.isEmpty() && list.get(0)!=null && (!(list.get(0).toString().trim().equals("")))){
    		        			unit1 = list.get(0).toString();
    		        		}
    		        		if(unit1 !=null && unit1.trim().equalsIgnoreCase("YES")){
    		        			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
    		        		}
    		    			String cartonContainerUpdateQuery1="update miscellaneous set actualGrossWeight='" +actualGrossWeight+ "' , actualNetWeight='" +actualNetWeight+ "' , actualTareWeight='" +actualTareWeight+"' " + unitQuery +" ,updatedBy='"+sessionCorpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
    		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
    		    		}
    		    	}
    	        }
    	   	}catch(Exception ex){
    			System.out.println("\n\n\n\n error while syn old data"+ex);
    			ex.printStackTrace();
    		}
    	   	return 1;
        	
        }
        public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy){
        	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualCubicFeet='" +actualCubicFeet+ "',netActualCubicFeet='" +actualCubicFeet+ "',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
	    	List al=this.getSession().createSQLQuery("select concat(if(grpPref is true,1,0),'#',if(grpStatus is null or grpStatus='',' ',grpStatus),'#',if(grpId is null or grpId='',' ',grpId)) as rec from serviceorder where shipNumber='"+shipNumber+"' and corpId='"+corpID+"'").list();
	    	if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
	    	{
	    		String[] temp=al.get(0).toString().split("#");
		            if((temp[0]!=null)&&(!temp[0].equals(""))&&(temp[0].equalsIgnoreCase("1")))
		            {
	            		if((temp[2].trim()!=null)&&(!temp[2].trim().equals(""))&&(!temp[1].trim().equalsIgnoreCase("Finalized")))		            		
		            	{
		            		try{
		            			String childIdList=getAllChildId(temp[2].trim().toString(),corpID);
		            			if(!childIdList.equalsIgnoreCase(""))
		            			{
		            				List newordersList=weightOfOrdersToGrouped(childIdList,corpID);
		            				if(newordersList!=null && !(newordersList.isEmpty()) && newordersList.get(0)!=null){
		            					 String tempWeight=newordersList.get(0).toString();
		            					 String[] str=tempWeight.split("#");
		            					 if(!str[1].equalsIgnoreCase("0")){		            						 
		            				    	this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr='"+str[1]+"' where id='"+temp[2].toString()+"'").executeUpdate();
		            					 }else{
		            						this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr=0 where id='"+temp[2].toString()+"'").executeUpdate();            						 
		            					 }
		            					 if(!str[0].equalsIgnoreCase("0")){
		            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet='" +str[0]+ "' where id='"+temp[2].toString()+"'").executeUpdate();
		            					 }else{
		            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet=0 where id='"+temp[2].toString()+"'").executeUpdate();				 
		            					 }
		            				}
		            			}
		            			
		            		}catch(Exception e){}
		            	}
		            }            
	    	}
	    	
	    	ServiceOrder serviceOrder = new ServiceOrder();
	    	List linkedShipNumberList= new ArrayList();
	    	List soList =new ArrayList();
	    	
			String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
			try{
				soList = getHibernateTemplate().find(query1);
				if(!soList.isEmpty()){
					 serviceOrder = (ServiceOrder)soList.get(0);
				}
			}catch(Exception e){
				e.getStackTrace();
			}
			
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){		        	
		        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
		        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		        	}else{
		        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		        	}		        	
		        	
					Iterator  it=linkedShipNumberList.iterator();
			    	while(it.hasNext()){			    			
			    		String shipNum=(String)it.next();
			    		String corpId = shipNum.substring(0, 4);
			    		if(!corpId.equalsIgnoreCase(corpID)){
			    			String cartonContainerUpdateQuery1="update miscellaneous set actualCubicFeet='" +actualCubicFeet+ "',netActualCubicFeet='" +actualCubicFeet+ "',updatedBy='"+corpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
			    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
			    		}
			    	}
		        }
		   	}catch(Exception ex){
				System.out.println("\n\n\n\n error while syn old data"+ex);
				ex.printStackTrace();
			}
	    	
        	return 1;
        }
		public List getContainerNumber(String shipNumber) {
			return getHibernateTemplate().find("select containerNumber from Container where status = true and  shipNumber=? and containerNumber is not null",shipNumber);
		}
		public List getVolumeUnit(String shipNumber) {
			
			List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(coalesce(unit2,' ') is null ,'',coalesce(unit2,' ')) from carton where  status = true and shipNumber='"+shipNumber+"'").list();
        	return L1;
		}
		public List getWeightUnit(String shipNumber) {
			
			List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(coalesce(unit1,' ') is null,'',coalesce(unit1,' ')) from carton where  status = true and  shipNumber='"+shipNumber+"'").list();
        	return L1;
		}
		public int updateDeleteStatus(Long ids){
			return getHibernateTemplate().bulkUpdate("delete Carton where id='"+ids+"'");
	}
		public List getGrossKilo(String shipNumber){
			List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(grossWeightKilo, 0)) is null, 0.00 , sum(coalesce(grossWeightKilo, 0)) )  from carton where status = true and   shipNumber='"+shipNumber+"'").list();
        	return L1;
		}
	    public List getTareKilo(String shipNumber)
	    {
	    	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(emptyContWeightKilo, 0)) is null, 0.00 , sum(coalesce(emptyContWeightKilo, 0)) )  from carton where status = true and   shipNumber='"+shipNumber+"'").list();
        	return L1;
	    }
	    public List getNetKilo(String shipNumber){
	    	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(netWeightKilo, 0)) is null , 0.00 , sum(coalesce(netWeightKilo, 0)))  from carton where status = true and   shipNumber='"+shipNumber+"'").list();
        	return L1;
	    }
	    public List getVolumeCbm(String shipNumber){
	    	List L1 = new ArrayList();
        	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(volumeCbm, 0)) is null , 0.00 ,sum(coalesce(volumeCbm, 0)) )  from carton where status = true and  shipNumber='"+shipNumber+"'").list();
        	return L1;
	    }
	    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
	    	String unitQuery="";
        	String unit ="";
    		List l =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+sessionCorpID+"'").list();
    		if(l!=null && !l.isEmpty() && l.get(0)!=null && (!(l.get(0).toString().trim().equals("")))){
    			unit = l.get(0).toString();
    		}
    		if(unit !=null && unit.trim().equalsIgnoreCase("YES")){
    			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
    		}
        	 
        	
	    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualGrossWeightKilo='" +actualGrossWeightKilo+ "' , actualNetWeightKilo='" +actualNetWeightKilo+ "' , actualTareWeightKilo='"+actualTareWeightKilo+"' " +unitQuery+" ,updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
	    
	    	 ServiceOrder serviceOrder = new ServiceOrder();
	      	List linkedShipNumberList= new ArrayList();
	      	List soList =new ArrayList();
	      	
	  		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
	  		try{
	  			soList = getHibernateTemplate().find(query1);
	  			if(!soList.isEmpty()){
	  				 serviceOrder = (ServiceOrder)soList.get(0);
	  			}
	  		}catch(Exception e){
	  			e.getStackTrace();
	  		}
	  		
	  	   	try{
	  	        if(serviceOrder.getIsNetworkRecord()){		        	
	  	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	  	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	  	        	}else{
	  	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	  	        	}		        	
	  	        	
	  				Iterator  it=linkedShipNumberList.iterator();
	  		    	while(it.hasNext()){			    			
	  		    		String shipNum=(String)it.next();
	  		    		String corpId = shipNum.substring(0, 4);
	  		    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
	  		    			unitQuery="";
	  		    			String unit1 ="";
    		        		List list =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+corpId+"'").list();
    		        		if(list!=null && !list.isEmpty() && list.get(0)!=null && (!(list.get(0).toString().trim().equals("")))){
    		        			unit1 = list.get(0).toString();
    		        		}
    		        		if(unit1 !=null && unit1.trim().equalsIgnoreCase("YES")){
    		        			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
    		        		}
    		    			
	  		    			
	  		    			String cartonContainerUpdateQuery1="update miscellaneous set actualGrossWeightKilo='" +actualGrossWeightKilo+ "' , actualNetWeightKilo='" +actualNetWeightKilo+ "' , actualTareWeightKilo='"+actualTareWeightKilo+"' " +unitQuery+ " ,updatedBy='"+sessionCorpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
	  		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
	  		    		}
	  		    	}
	  	        }
	  	   	}catch(Exception ex){
	  			System.out.println("\n\n\n\n error while syn old data"+ex);
	  			ex.printStackTrace();
	  		}
	  	   	return 1;
	    
	    }
	    
		String getAllChildId(String grpId, String sessionCorpId){
			String query="select id from serviceorder where grpId= "+grpId+" and corpID='"+sessionCorpId+"'";
			List list=this.getSession().createSQLQuery(query).list();
			String childList="";
			Iterator it =list.iterator();
			while(it.hasNext()){
				String childId=it.next().toString();
				if(childList.equalsIgnoreCase(""))
				{
					childList=childId.trim();
				}else{
					childList=childList+","+childId.trim();
				}
			}
			return childList;
		}
		List weightOfOrdersToGrouped(String idList,String corpid){
			List temp=new ArrayList();
			String query="select"+
			" concat"+
			"("+
			" sum(if(actualCubicFeet is null or actualCubicFeet=0,"+
			" if(netActualCubicFeet is null or netActualCubicFeet=0,"+
			" if(estimateCubicFeet is null or estimateCubicFeet=0,"+
			" if(netEstimateCubicFeet is null or netEstimateCubicFeet=0,"+
			" '0',"+
			" netEstimateCubicFeet),"+
			" estimateCubicFeet),"+
			" netActualCubicFeet),"+
			" actualCubicFeet)),"+
			" '#',"+
			" sum(if(actualCubicMtr is null or actualCubicMtr=0,"+
			" if(netActualCubicMtr is null or netActualCubicMtr=0,"+
			" if(estimateCubicMtr is null or estimateCubicMtr=0,"+
			" if(netEstimateCubicMtr is null or netEstimateCubicMtr=0,"+
			" '0',"+
			" netEstimateCubicMtr),"+
			" estimateCubicMtr),"+
			" netActualCubicMtr),"+
			" actualCubicMtr))"+
			" )"+
			" from miscellaneous where id in("+idList+")";		
			try{
				if(!idList.equalsIgnoreCase(""))
				{
					temp=this.getSession().createSQLQuery(query).list();
				}else{
					temp=null;
				}
			}
			catch(Exception e){
			}
			return temp;
		}		
	    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy)
	    {
	    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualCubicMtr='" +actualCubicMtr+ "',netActualCubicMtr='" +actualCubicMtr+ "',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
	    	List al=this.getSession().createSQLQuery("select concat(if(grpPref is true,1,0),'#',if(grpStatus is null or grpStatus='',' ',grpStatus),'#',if(grpId is null or grpId='',' ',grpId)) as rec from serviceorder where shipNumber='"+shipNumber+"' and corpId='"+corpID+"'").list();
	    	if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
	    	{
	    		String[] temp=al.get(0).toString().split("#");
		            if((temp[0]!=null)&&(!temp[0].equals(""))&&(temp[0].equalsIgnoreCase("1")))
		            {
		            	if((temp[2].trim()!=null)&&(!temp[2].trim().equals(""))&&(!temp[1].trim().equalsIgnoreCase("Finalized")))		            		
		            	{
		            		try{
		            			String childIdList=getAllChildId(temp[2].trim().toString(),corpID);
		            			if(!childIdList.equalsIgnoreCase(""))
		            			{
		            				List newordersList=weightOfOrdersToGrouped(childIdList,corpID);
		            				if(newordersList!=null && !(newordersList.isEmpty()) && newordersList.get(0)!=null){
		            					 String tempWeight=newordersList.get(0).toString();
		            					 String[] str=tempWeight.split("#");
		            					 if(!str[1].equalsIgnoreCase("0")){
		            				    	
		            				    	this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr='"+str[1]+"' where id='"+temp[2].toString()+"'").executeUpdate();
		            					 }else{
		            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr=0 where id='"+temp[2].toString()+"'").executeUpdate();           						 
		            					 }
		            					 if(!str[0].equalsIgnoreCase("0")){
		            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet='" +str[0]+ "' where id='"+temp[2].toString()+"'").executeUpdate();
		            					 }else{
		            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet=0 where id='"+temp[2].toString()+"'").executeUpdate();           						 
		            					 }
		            				}
		            			}
		            			
		            		}catch(Exception e){}
		            	}
		            }            
	    	}
	    	
	    	ServiceOrder serviceOrder = new ServiceOrder();
	     	List linkedShipNumberList= new ArrayList();
	     	List soList =new ArrayList();
	     	
	 		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
	 		try{
	 			soList = getHibernateTemplate().find(query1);
	 			if(!soList.isEmpty()){
	 				 serviceOrder = (ServiceOrder)soList.get(0);
	 			}
	 		}catch(Exception e){
	 			e.getStackTrace();
	 		}
	 		
	 	   	try{
	 	        if(serviceOrder.getIsNetworkRecord()){		        	
	 	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	 	        	}else{
	 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	 	        	}		        	
	 	        	
	 				Iterator  it=linkedShipNumberList.iterator();
	 		    	while(it.hasNext()){			    			
	 		    		String shipNum=(String)it.next();
	 		    		String corpId = shipNum.substring(0, 4);
	 		    		if(!corpId.equalsIgnoreCase(corpID)){
	 		    			String cartonContainerUpdateQuery1="update miscellaneous set actualCubicMtr='" +actualCubicMtr+ "',netActualCubicMtr='" +actualCubicMtr+ "',updatedBy='"+corpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
	 		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
	 		    		}
	 		    	}
	 	        }
	 	   	}catch(Exception ex){
	 			System.out.println("\n\n\n\n error while syn old data"+ex);
	 			ex.printStackTrace();
	 		}
	    	
	    	return 1;
	    }
	    public List goSOChild(Long serviceOrderId, String corpID, Long id) {
			return getHibernateTemplate().find("from  Carton where status = true and  serviceOrderId='"+serviceOrderId+"' and id !='"+id+"' and corpID='"+corpID+"'");
		}
		public List goNextSOChild(Long serviceOrderId, String corpID, Long id) {
			return getHibernateTemplate().find("select id from  Carton where status = true and  serviceOrderId='"+serviceOrderId+"' and id >'"+id+"' and corpID='"+corpID+"' order by 1 ");
		}
		public List goPrevSOChild(Long serviceOrderId, String corpID, Long id) {
			return getHibernateTemplate().find("select id from  Carton where status = true and  serviceOrderId='"+serviceOrderId+"' and id <'"+id+"' and corpID='"+corpID+"' order by 1 desc");
		}
		 public List findMaximumChild(String shipNm) {
		    	return getHibernateTemplate().find("select max(id) from Carton where shipNumber=?", shipNm);
		    }
		    public List findMinimumChild(String shipNm){
		    	return getHibernateTemplate().find("select min(id) from Carton where shipNumber=?", shipNm);
		    }
		    public List findCountChild(String shipNm){
		    	return getHibernateTemplate().find("select count(*) from Carton where  shipNumber=?", shipNm);
		    }
			public List cartonList(Long serviceOrderId) {
				return getHibernateTemplate().find("from  Carton where serviceOrderId='"+serviceOrderId+"' ");
			}
			public List cartonListotherCorpId(Long sid){
				return getSession().createSQLQuery("select id from carton where  serviceOrderId='"+sid+"'").list();
			}
			public Long findRemoteCarton(String idNumber, Long serviceOrderId) {
				return Long.parseLong(this.getSession().createSQLQuery("select id from carton where  idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderId+"").list().get(0).toString());
			}
			public void deleteNetworkCarton(Long serviceOrderId, String idNumber, String cartonStatus) {
				this.getSession().createSQLQuery("update carton set status = "+cartonStatus+"  where idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderId+"").executeUpdate();				
			}
			public List findCartonBySid(Long sofid, String sessionCorpID){
				return this.getSession().createSQLQuery("select id from carton where   serviceOrderId='"+sofid+"'and corpID='"+sessionCorpID+"' ").list();	
			}
			public List getGrossNetwork(String shipNumber, String corpID) {
				return this.getSession().createSQLQuery("select if(sum(coalesce(grossWeight, 0)) is null , 0.00 , sum(coalesce(grossWeight, 0)) ) from carton   where status = true and  shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
			 }
			public List getNetNetwork(String shipNumber, String corpID) { 
				return this.getSession().createSQLQuery("select if(sum(coalesce(netWeight, 0)) is null, 0.00 , sum(coalesce(netWeight, 0)) )  from carton  where status = true and  shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
			}
			public List getVolumeNetwork(String shipNumber, String corpID) { 
				return this.getSession().createSQLQuery("select if(sum(coalesce(volume, 0)) is null, 0.00 , sum(coalesce(volume, 0)) ) from carton where status = true and   shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
			}
			public List getPiecesNetwork(String shipNumber, String corpID) { 
				return this.getSession().createSQLQuery("select if(sum(coalesce(pieces, 0)) is null , 0 , sum(coalesce(pieces, 0)) ) from carton where status = true and  shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
			}
			public List getTareNetwork(String shipNumber, String corpID) { 
				return this.getSession().createSQLQuery("select if(sum(coalesce(emptyContWeight, 0)) is null , 0.00 , sum(coalesce(emptyContWeight, 0)) ) from carton where status = true and   shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
			}
			public List findCartonTypeValue(String sessionCorpID){
				String query="select cartonType from refcrate where corpID='"+sessionCorpID+"' order by cartonType";
				return getSession().createSQLQuery(query).list();
			}
			public String findUnitValuesByCarton(String cartonVal,String sessionCorpID){
				String accValue="";
				try {
					String query="select concat(if(emptyContWeight is null or emptyContWeight='','NA',emptyContWeight),'~',if(unit1 is null or unit1='','NA',unit1),'~',if(length is null or length='','NA',length),"
							+ "'~',if(width is null or width='','NA',width),'~',if(height is null or height='','NA',height),'~',if(unit2 is null or unit2='','NA',unit2),"
							+ "'~',if(volume is null or volume='','NA',volume),'~',if(unit3 is null or unit3='','NA',unit3)) "
							+" from refcrate where cartonType='"+ cartonVal+ "' and corpid ='"+sessionCorpID+"'" ;
					List temp =  this.getSession().createSQLQuery(query).list();
					if ((temp != null) && (!temp.isEmpty()) && (temp.get(0) != null)) {
						accValue = temp.get(0).toString();
					}								
				} catch (Exception e) {					
				}
				return accValue;
			}
			
			public void updateCartonNetWeightDetails(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName){
				String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"' ,"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"',"+fieldGrossWeight+"='"+fieldGrossWeightVal+"',"+fieldGrossWeightKilo+"='"+fieldGrossWeightKiloVal+"',"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
				getHibernateTemplate().bulkUpdate(query);
			}
			public void updateCartonDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName){
				String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"' ,"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"',"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
				getHibernateTemplate().bulkUpdate(query);
			}
			public void updateCartonDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldTypeName,String fieldTypeVal,String tableName,String sessionCorpID,String userName){
				String query="update "+tableName+" set "+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"' ,"+fieldTypeName+"='"+fieldTypeVal+"',updatedOn=now(),updatedby='"+userName+"'  where id ='"+id+"' ";
				getHibernateTemplate().bulkUpdate(query);
			}
			public void updateValueByCartonType(Long id,String fieldUnit1,String fieldUnit1Val,String fieldUnit2,String fieldUnit2Val,String fieldUnit3,String fieldUnit3Val,String fieldLength,String fieldLengthVal,String fieldWidth,String fieldWidthVal,String fieldHeight,String fieldHeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldValueCartonVal,String tableName,String sessionCorpID,String userName){
				String query="update "+tableName+" set "+fieldUnit1+"='"+fieldUnit1Val+"' ,"+fieldUnit2+"='"+fieldUnit2Val+"',"+fieldUnit3+"='"+fieldUnit3Val+"',"+fieldLength+"='"+fieldLengthVal+"',"+fieldWidth+"='"+fieldWidthVal+"',"+fieldHeight+"='"+fieldHeightVal+"' ,"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',cartonType='"+fieldValueCartonVal+"' ,description='"+fieldValueCartonVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
				getHibernateTemplate().bulkUpdate(query);
			}
			
			public List getLinkedShipNumber(String shipNumber, String recordType) {
				try {
					List linkedShipNumberList= new ArrayList();
					if(recordType.equalsIgnoreCase("Primary")){
						List linkedList= this.getSession().createSQLQuery("select concat(IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber='"+shipNumber+"'").list();
						List reloLinkedList=this.getSession().createSQLQuery("select concat(IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,'') ,'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber='"+shipNumber+"'").list();
						if(linkedList!=null && !linkedList.isEmpty() && linkedList.get(0)!= null){
							String listString=linkedList.get(0).toString();
							String [] listArray=listString.split("#");
							for(String sNumber:listArray){
								if(!sNumber.trim().equalsIgnoreCase("")){
									linkedShipNumberList.add(sNumber);	
								}
							
							}  
						}
						if(reloLinkedList!=null && !reloLinkedList.isEmpty() && reloLinkedList.get(0)!= null){
							String listReloString=reloLinkedList.get(0).toString();
							String [] listReloArray=listReloString.split("#");
							for(String sNumber:listReloArray){
								if(!sNumber.trim().equalsIgnoreCase("")){
									linkedShipNumberList.add(sNumber);	
								}
							}
						}
						if(linkedShipNumberList != null && (!linkedShipNumberList.isEmpty())){
							String linkShipNumLst = "'"+linkedShipNumberList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "','")+"'";
							List linkedListSub=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber in ("+linkShipNumLst+")").list();
							List reloLinkedListSub=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,''),'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber in ("+linkShipNumLst+")").list();
							if(linkedListSub!=null && !linkedListSub.isEmpty() && linkedListSub.get(0)!= null){
								String listStringSub=linkedListSub.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
								String [] listArraySub=listStringSub.split("#");
								for(String sNumber:listArraySub){
									if(!sNumber.trim().equalsIgnoreCase("")){
										linkedShipNumberList.add(sNumber);	
									} 
								}  
							}
							
							if(reloLinkedListSub!=null && !reloLinkedListSub.isEmpty() && reloLinkedListSub.get(0)!= null){
								String listReloStringSub=reloLinkedListSub.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
								String [] listReloArraySub=listReloStringSub.split("#");
								for(String sNumber:listReloArraySub){
									if(!sNumber.trim().equalsIgnoreCase("")){
										linkedShipNumberList.add(sNumber);	
									}
								}
							}
						}
						
					}if(recordType.equalsIgnoreCase("Secondary")){
						List linkedShipNumber= this.getSession().createSQLQuery("select bookingAgentShipNumber from serviceorder where shipNumber='"+shipNumber+"'").list();
						List linkedShipNumber1= this.getSession().createSQLQuery("select bookingAgentShipNumber from serviceorder where shipNumber='"+linkedShipNumber.get(0).toString()+"'").list();
						List linkedList=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber in ('"+linkedShipNumber.get(0).toString()+"','"+shipNumber+"')").list();
						List reloLinkedList=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,''),'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber in ('"+linkedShipNumber.get(0).toString()+"','"+shipNumber+"')").list();
						
						if(linkedList!=null && !linkedList.isEmpty() && linkedList.get(0)!= null){
							String listString=linkedList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
							//String listString=linkedList.get(0).toString();
							
							String [] listArray=listString.split("#");
							if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && linkedShipNumber.get(0)!= null)
								linkedShipNumberList.add(linkedShipNumber.get(0).toString());
						
							if(linkedShipNumber1!=null && !linkedShipNumber1.isEmpty() && linkedShipNumber1.get(0)!= null)
								linkedShipNumberList.add(linkedShipNumber1.get(0).toString());	
							
								for(String sNumber:listArray){
									if(!sNumber.trim().equalsIgnoreCase("")){
										linkedShipNumberList.add(sNumber);	
									}
								}
							
						}
						if(reloLinkedList!=null && !reloLinkedList.isEmpty() && reloLinkedList.get(0)!= null){
							String listReloString=reloLinkedList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
							// String listReloString=reloLinkedList.get(0).toString();
							String [] listReloArray=listReloString.split("#");
							for(String sNumber:listReloArray){
								if(!sNumber.trim().equalsIgnoreCase("")){
									linkedShipNumberList.add(sNumber);	
								}
								
							} 
						}
					}
					HashSet hs = new HashSet();
					hs.addAll(linkedShipNumberList);
					linkedShipNumberList.clear();
					linkedShipNumberList.addAll(hs);
					linkedShipNumberList.remove("");
					linkedShipNumberList.remove("OrderDecline");
					return linkedShipNumberList;
				} catch (Exception e) {
					logger.error("Error executing query"+ e,e);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();	    	
				}
			return null;
			}
			public void updateStatus(Long id, String cartonStatus) {
				getHibernateTemplate().bulkUpdate("update Carton set status ="+cartonStatus+" where id="+id+"");   
				
			}
			
	}


//End of Class.   