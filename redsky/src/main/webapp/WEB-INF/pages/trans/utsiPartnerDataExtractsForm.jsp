<%@page import="java.util.List"%>
<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <meta name="heading" content="Partner Extract"/>  
     <title>Extraction Details</title>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
		<%@ include file="/common/calenderStyle.css"%>
	</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	
	<script language="javascript" type="text/javascript">
		function getSelectedValue(){
			var childParents='';
			<c:forEach var="childMap" items="${childMap}">
				if(document.getElementById("${childMap.key}").checked == true){
					childParents = "${childMap.value}"+','+childParents;
				}
			</c:forEach>
			if(childParents.length > 0){
			 childParents = childParents.substring(0, childParents.length-1);

			 document.getElementById("utsiPartnerExtractForm_childParentIdList").value=childParents;
			 	return validateBlankDt();
			}else{
				alert('Pick at least one child');
				return false;
			}
		}

		function getSelectAll(str){
			if(str == 'selectAll'){
				document.getElementById("deselectAll").checked=false;
				<c:forEach var="childMap" items="${childMap}">
					document.getElementById("${childMap.key}").checked=true;
				</c:forEach>
			}else if(str == 'deselectAll'){
				document.getElementById("selectAll").checked=false;
				<c:forEach var="childMap" items="${childMap}">
					document.getElementById("${childMap.key}").checked=false;
				</c:forEach>
			}else if(str == 'CHECKALL'){
				document.getElementById("selectAll").checked=false;
				document.getElementById("deselectAll").checked=false;
			}
		}

		function validateReportType(str){
			if(str == 'flatFile'){
				document.getElementById("claim_Report_from").value='';
				document.getElementById("claim_Report_to").value='';
			}else if(str == 'claimFile'){
				document.getElementById("flat_Report_from").value='';
				document.getElementById("flat_Report_to").value='';
			}
		}

		function checkDt(fromDt, ToDt){

			   var str1  = 	fromDt;		// document.forms['workTicketDataExtractsForm'].elements['beginDateW'].value;
			   var str2  = 	ToDt;		//document.forms['workTicketDataExtractsForm'].elements['endDateW'].value;
			   var mySplitResult = str1.split("-");
   				var day = mySplitResult[0];
   				var month = mySplitResult[1];
  				 var year = mySplitResult[2];
		  if(month == 'Jan') {
		       month = "01";
		   } else if(month == 'Feb') {
		       month = "02";
		   } else if(month == 'Mar')
		   {
		       month = "03"
		   } else if(month == 'Apr') {
		       month = "04"
		   } else if(month == 'May') {
		       month = "05"
		   } else if(month == 'Jun') {
		       month = "06"
		   }else if(month == 'Jul'){
		       month = "07"
		   }else if(month == 'Aug'){
		       month = "08"
		   } else if(month == 'Sep'){
		       month = "09"
		   }else if(month == 'Oct'){
		       month = "10"
		   } else if(month == 'Nov'){
		       month = "11"
		   } else if(month == 'Dec'){
		       month = "12";
		   }
		   
		   var finalDate = month+"-"+day+"-"+year;
		   
		   var mySplitResult2 = str2.split("-");
		   var day2 = mySplitResult2[0];
		   var month2 = mySplitResult2[1];
		   var year2 = mySplitResult2[2];
		   if(month2 == 'Jan'){
		       month2 = "01";
		   } else if(month2 == 'Feb') {
		       month2 = "02";
		   } else if(month2 == 'Mar') {
		       month2 = "03"
		   } else if(month2 == 'Apr'){
		       month2 = "04"
		   }else if(month2 == 'May'){
		       month2 = "05"
		   }else if(month2 == 'Jun'){
		       month2 = "06"
		   }else if(month2 == 'Jul'){
		       month2 = "07"
		   }else if(month2 == 'Aug'){
		       month2 = "08"
		   }else if(month2 == 'Sep'){
		       month2 = "09"
		   } else if(month2 == 'Oct') {
		       month2 = "10"
		   }else if(month2 == 'Nov'){
		       month2 = "11"
		   }else if(month2 == 'Dec'){
		       month2 = "12";
		   }
  			var finalDate2 = month2+"-"+day2+"-"+year2;
 			 date1 = finalDate.split("-");
  
  			date2 = finalDate2.split("-");
   
  			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
 			 var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
    		var daysApart = Math.round((sDate-eDate)/86400000);
			   
		   if(daysApart>0) {
		      alert("From date cannot be greater than To date");
		      return false;
		   }
		}

		function validateBlankDt(){
			for (var i=0; i < document.utsiPartnerExtractForm.reportType.length; i++){
			   if (document.utsiPartnerExtractForm.reportType[i].checked){
			      var rad_val = document.utsiPartnerExtractForm.reportType[i].value;
			      if(rad_val == 'claimFile'){
			    	 var claimFrom = document.getElementById("claim_Report_from").value;
					 var claimTo =	document.getElementById("claim_Report_to").value;
					 if(claimFrom.length == 0 && claimTo.length == 0 ){
						 alert('Enter From & To date for Claim Overview');
						 return false;
					 }else if(claimFrom.length == 0 && claimTo.length > 0){
						 alert('Enter From date for Claim Overview');
						 return false;
					 }else if(claimFrom.length > 0 && claimTo.length == 0){
						 alert('Enter To date for Claim Overview');
						 return false;
					 }else{
						 return checkDt(claimFrom, claimTo);
					 }
						 
			      }else{
			    	  var flatFrom = document.getElementById("flat_Report_from").value;
					  var flatTo =	document.getElementById("flat_Report_to").value;
			    	  if(flatFrom.length == 0 && flatTo.length == 0 ){
							 alert('Enter From & To date for Flat file');
							 return false;
						 }else if(flatFrom.length == 0 && flatTo.length > 0){
							 alert('Enter From date for Flat file');
							 return false;
						 }else if(flatFrom.length > 0 && flatTo.length == 0){
							 alert('Enter To date for Flat file');
							 return false;
						 }else{
							 return checkDt(flatFrom, flatTo);
						 }
			      }
			      }
			   }
		}
	</script>
	<script language="javascript" type="text/javascript">

	</script>

</head>

<c:set var="buttons"> 
    <input type="submit"" class="cssbutton1" onclick=" return getSelectedValue();" value="View Reports" style="width:120px;" tabindex="83"/> 
	<input type="reset" class="cssbutton1" onclick="getValue()" value="Reset" style="width:60px;" tabindex="83"/> 
</c:set>

<s:form name="utsiPartnerExtractForm" id="utsiPartnerExtractForm" action="viewUTSIPartnerExtract" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<s:hidden name="childParentIdList"/>


      
<div id="Layer5" style="width:100%">
<table class="" cellspacing="0" cellpadding="0" border="0" width="95%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
   
<table style="margin-bottom:0px"  border="0">
	<tr><td height="5px"></td></tr>	  
	<tr><td  class="bgblue" >Extract Parameters</td></tr>
	<tr><td height="2px"></td></tr>
	<tr>
		<td align="left" class="listwhitetext" style="padding-left:10px;">
				
		  <fieldset>
		          <legend>Available List</legend>
		          <c:if test="${fn:length(childMap) > 0 }">
		          <table class="pickList11" style="margin:0px">
		          	<tr>
		   				<td  width="22%"><s:checkbox name="selectAll" id="selectAll" value="selectAll" fieldValue="true" tabindex="3" onclick="getSelectAll('selectAll')" /> Select All</td>
		   				<td><s:checkbox name="deselectAll" id="deselectAll" value="deselectAll" fieldValue="true" tabindex="3" onclick="getSelectAll('deselectAll')" /> Deselect All</td>
		          	</tr>
		          	<tr>
		          	<td></td>
		          	<td></td>
		          	</tr>
		          	<tr>
		          	<td colspan="2">
		          	<div id="teste" style="overflow:auto;height:60px;width:400px;">
		          	<c:forEach var="childMap" items="${childMap}">								
					<s:checkbox name="childMap.key" id="${childMap.key}" value="${childMap.value}" fieldValue="true" tabindex="3" onclick="getSelectAll('CHECKALL')" />
	                <s:label title="${childMap.value}" value="${childMap.key}"></s:label><br>
					</c:forEach>					
					</div>
					</td>
					</tr>
		          </table>
		          </c:if>
		          <c:if test="${fn:length(childMap) == 0 }">
			          <table> 
				          <tr>
					          <td class="listwhitetext">Data not available.</td>
				          </tr>
			          </table>
		          </c:if>
		  </fieldset>
		  <fieldset>
		          <legend>Report Type</legend>
		          <table class="pickList11"  border="0">
		          	  <tr>
		          	  	  <td></td>
		                  <td width="30px"></td>
		                  <td></td>
		                  <td align="center"" class="listwhitetext">From</td>
		                  <td></td>
		                  <td align="center" class="listwhitetext">To</td>
		           		  <td></td>
		                  
		          	  </tr>
		              <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="flatFile" checked="checked" onclick="validateReportType('flatFile')"/> Flat File</td>
		                  <td width="30px"></td>
		                  <td class="listwhitetext">Customer File Created</td>
		                  <td><s:textfield cssClass="input-text" name="flat_Report_from" id="flat_Report_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td><img id="flat_Report_from-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  <td><s:textfield cssClass="input-text" name="flat_Report_to"  id="flat_Report_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td><img id="flat_Report_to-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr>
		             
		              <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="claimFile" onclick="validateReportType('claimFile')"/> Claim Overview</td>
		                   <td width="30px"></td>
		                  <td class="listwhitetext">Claim Notification Date</td>
		                  <td><s:textfield cssClass="input-text" name="claim_Report_from"  id="claim_Report_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td><img id="claim_Report_from-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  <td><s:textfield cssClass="input-text"  name="claim_Report_to" id="claim_Report_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td><img id="claim_Report_to-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr>
		          </table>
		  </fieldset>
		</td>
	</tr>
	<c:if test="${fn:length(childMap) > 0 }">
	<tr>
		<td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
	</tr>
	</c:if>
</table>

<c:if test="${fn:length(qltyServeyResMap) > 0 }">
	<table style="margin-bottom:0px"  border="0" id="qserveyRes">
		<tr><td height="5px"></td></tr>	  
		<tr><td  class="bgblue" >Quality Survey Results</td></tr>
		<tr height="2px"></tr>
		<tr>
			<td align="left" colspan="0" width="">
				<c:forEach var="qltyServeyResMap" items="${qltyServeyResMap}">
					<input type="button" class="cssbutton1" onclick="window.open('http://${qltyServeyResMap.value}')" value="${qltyServeyResMap.key}" style="width:120px; " tabindex="83"/> 
				</c:forEach>
			</td>
		</tr>
	</table>
</c:if>

</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 

</td>
</tr>
</tbody>
</table> 

</div> 

</s:form> 
<div id="mydiv" style="position:absolute;top:0px;margin-top:-47px;"></div>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>