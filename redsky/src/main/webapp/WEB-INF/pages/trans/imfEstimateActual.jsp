<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="<fmt:message key='imfExtract.title'/>"/> 
	<title><fmt:message key="imfExtract.title"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>

		
		

	
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; } 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>

</head>
<body style="background-color:#444444;">
<s:form id="imfEstimateNew" name="imfEstimateNew" action="imfEstimateData" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" >
 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >IMF Extract Costs</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px">
<tr>
<td width="20px"></td>
<td><s:submit cssClass="cssbutton" cssStyle="width:145px; height:25px " align="top" method="imfEstimateFileActual" value="Actual Extract (sscosts)" /></td>
<td width="10px"></td>
<td>
<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/ssDownActual.html"/>'"  
        			value="SS Down (ssdown)"/> 
        			</td>
</tr>
<tr>
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/imfEstimateData.html"/>'"  
        			value="IMF Entitlement (enti237)"/></td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/imfEstimateForm.html"/>'"  
        			value="IMF Estimate (esti237)"/> </td>
</tr>

</table>
</td>
</tr>


<tr>
<td width="10px;"></td>
<td class="bgblue" >IMF Extract Reference Table</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px">
<tr>
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/ssAgentsExtract.html"/>'"  
        			value="SSAgents"/> </td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/sscar.html"/>'"  
        			value="SSCARR"/></td>
</tr>

</table></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >IMF Extract Storage Files</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px">

<tr>
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/tbkstg.html"/>'"  
        			value="TBK STG"/> </td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/tsto.html"/>'"  
        			value="TSTO"/></td>
</tr>

</table>
</td>
</tr>
<tr><td width="10px;"></td><td class="bgblue" >IMF Extract Audit Trail</td></tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px">

<tr>
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/sshist.html"/>'"  
        			value="SSHIST"/></td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/tstg.html"/>'"  
        			value="TSTG Hist"/> </td>
</tr>

</table>
</td>
</tr>



<tr><td width="10px;"></td><td class="bgblue" >IMF Extract Office Doc Move</td></tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px">
<tr>
<td width="20px"></td>
<td><button type="button" class="cssbutton" style="width:150px; height:25px" 
        			onclick="location.href='<c:url value="/officeDocMove.html"/>'"  
        			value="">
        			<span style="color:black;">Office Doc move</span></button></td>
<td width="10px"></td>
<td><button type="button" class="cssbutton" style="width:150px; height:25px" 
        			onclick="location.href='<c:url value="/officeDocMoveNew.html"/>'"  
        			value="">
        			<span style="color:black;">IT Shipments</span></button></td>

</tr>
</table>
</td>
</tr>
<tr><td height="15px"></td></tr>
</table>
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

</div>
 
</s:form>		