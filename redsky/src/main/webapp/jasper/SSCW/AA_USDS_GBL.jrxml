<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="AA_USDS_GBL"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="504"
		 columnSpacing="0"
		 leftMargin="72"
		 rightMargin="36"
		 topMargin="36"
		 bottomMargin="18"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String">
		<parameterDescription><![CDATA[Enter Customer File#]]></parameterDescription>
	</parameter>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT 'AA_USDS_GBL-F-PDF-SSCW.jrxml',
	serviceorder.`mode` AS serviceorder_mode,
	carton.`pieces` AS carton_pieces,	 
	carton.`volume` AS carton_volume,
	carton.`idNumber` AS carton_idNumber,
	if(serviceorder.`mode`="Air",carton.`grossWeight`,carton.`netWeight`)AS WgtShow,
	CONCAT(serviceorder.`prefix`,'  ',serviceorder.`firstName`,'  ',serviceorder.`lastName`) AS Shipper,
	serviceorder.`shipNumber` AS serviceorder_shipNumber,
	max(carton.`idnumber`)AS piececount,
	workticket.`warehouse` AS workticket_warehouse,
	refmaster.`address1` AS refmaster_address1,
	sum(if(carton.grossWeight is null || trim(carton.grossWeight)='',0,carton.grossWeight)) AS carton_totalGrossWeight,
	sum(if(carton.netWeight is null || trim(carton.netWeight)='',0,carton.netWeight)) AS carton_totalNetWeight,
	CONCAT(serviceorder.`destinationCity`,"  ",serviceorder.`destinationState`," ",serviceorder.`destinationCountry`)AS Destn,
	CONCAT(app_user.`first_name`,' ',app_user.`last_name`)AS Coord,
	billing.`billToAuthority` AS billing_billToAuthority,
	concat(refmaster.`city`,'  ',refmaster.`state`,'  ',refmaster.`zip`) AS WHctyzp
FROM serviceorder serviceorder
	INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` and billing.corpID = $P{Corporate ID}
	LEFT OUTER JOIN `carton` carton ON serviceorder.`shipNumber` = carton.`shipNumber` and carton.status is true and carton.corpID = $P{Corporate ID}
	LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` and app_user.corpID = $P{Corporate ID}
	LEFT OUTER JOIN `workticket` workticket ON serviceorder.`id` = workticket.`serviceorderid` and workticket.corpID = $P{Corporate ID}
	LEFT OUTER JOIN `refmaster` refmaster ON workticket.`warehouse` = refmaster.`code` and refmaster.`parameter` = 'HOUSE' and refmaster.corpID IN ('TSFT',$P{Corporate ID})
WHERE serviceorder.corpID = $P{Corporate ID}
	AND serviceorder.`shipNumber` = $P{Service Order Number}
GROUP BY serviceorder.`shipnumber`]]></queryString>

	<field name="AA_USDS_GBL-F-PDF-SSCW.jrxml" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="carton_pieces" class="java.lang.Integer"/>
	<field name="carton_volume" class="java.math.BigDecimal"/>
	<field name="carton_idNumber" class="java.lang.String"/>
	<field name="WgtShow" class="java.math.BigDecimal"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="piececount" class="java.lang.String"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="refmaster_address1" class="java.lang.String"/>
	<field name="carton_totalGrossWeight" class="java.math.BigDecimal"/>
	<field name="carton_totalNetWeight" class="java.math.BigDecimal"/>
	<field name="Destn" class="java.lang.String"/>
	<field name="Coord" class="java.lang.String"/>
	<field name="billing_billToAuthority" class="java.lang.String"/>
	<field name="WHctyzp" class="java.lang.String"/>

	<sortField name="serviceorder_shipNumber" />

	<variable name="ref" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{serviceorder_mode}== "Sea" ?"Ref Household effects shipment for" :($F{serviceorder_mode}== "Air" ? "Ref Personal effects shipment for": "")]]></variableExpression>
	</variable>
	<variable name="piece_count" class="java.lang.Integer" resetType="Group" resetGroup="shipnumber" calculation="Highest">
		<variableExpression><![CDATA[$F{carton_pieces}]]></variableExpression>
	</variable>
	<variable name="cubic_feet" class="java.math.BigDecimal" resetType="Group" resetGroup="shipnumber" calculation="Sum">
		<variableExpression><![CDATA[$F{carton_idNumber} == null ? new BigDecimal(0)   :$F{carton_volume}]]></variableExpression>
	</variable>
	<variable name="WgtShow" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{WgtShow}]]></variableExpression>
	</variable>

		<group  name="shipnumber" >
			<groupExpression><![CDATA[$F{serviceorder_shipNumber}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="491"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="9"
						y="181"
						width="133"
						height="18"
						key="staticText-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[This shipment is located at :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="471"
						width="148"
						height="18"
						key="staticText-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Global Service Team]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="425"
						width="148"
						height="18"
						key="staticText-6"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Sincerely.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="37"
						y="122"
						width="113"
						height="18"
						key="staticText-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[NBR OF PIECES :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="37"
						y="148"
						width="113"
						height="18"
						key="staticText-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[CUBIC FEET :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="#,##0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="171"
						y="147"
						width="55"
						height="19"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{cubic_feet}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="38"
						y="56"
						width="113"
						height="18"
						key="staticText-24"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[CUSTOMER :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="38"
						y="98"
						width="113"
						height="18"
						key="staticText-25"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[DESTINATION :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="162"
						y="56"
						width="323"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="163"
						y="98"
						width="246"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destn}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="263"
						width="351"
						height="18"
						key="staticText-26"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Please send the GBL and Dock Receipts to,]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="165"
						y="289"
						width="148"
						height="14"
						key="staticText-27"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Security Storage Company]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="165"
						y="304"
						width="197"
						height="12"
						key="staticText-29"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[8515 Rainswood Dr]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="165"
						y="316"
						width="197"
						height="14"
						key="staticText-30"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Landover, MD 20785]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="396"
						width="351"
						height="18"
						key="staticText-31"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[We look forward to receiving your shipping instructions.]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="449"
						width="203"
						height="17"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Coord}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="27"
						width="485"
						height="18"
						key="staticText-38"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Attached is a copy of our itemized packing list for the following shipment: ]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="134"
						height="18"
						key="staticText-39"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[US Despatch Agent:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="141"
						y="198"
						width="84"
						height="15"
						key="staticText-40"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Warehouse #  ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="161"
						y="78"
						width="135"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToAuthority}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="1"
						y="78"
						width="150"
						height="18"
						key="staticText-41"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[TRAVEL AUTHORIZATION :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="159"
						y="180"
						width="148"
						height="18"
						key="staticText-42"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Security Storage Company]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="122"
						width="49"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{piececount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="229"
						y="198"
						width="34"
						height="14"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="160"
						y="213"
						width="172"
						height="14"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{refmaster_address1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="160"
						y="227"
						width="172"
						height="13"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{WHctyzp}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="390"
						y="144"
						width="69"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{carton_totalGrossWeight}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="390"
						y="123"
						width="69"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{carton_totalNetWeight}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="249"
						y="122"
						width="113"
						height="18"
						key="staticText-43"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[Net Weight :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="249"
						y="143"
						width="113"
						height="18"
						key="staticText-44"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[Gross Weight :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="165"
						y="329"
						width="197"
						height="14"
						key="staticText-45"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[Fax:  (301) 386 - 5633]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="165"
						y="343"
						width="197"
						height="14"
						key="staticText-46"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[gst@sscw.com]]></text>
				</staticText>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="110"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="2"
						y="38"
						width="148"
						height="14"
						key="staticText-7"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[US DESPATCH AGENT]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="MMMMM dd, yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="2"
						y="7"
						width="110"
						height="15"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="2"
						y="54"
						width="148"
						height="14"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[SUITE 125]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="2"
						y="68"
						width="148"
						height="14"
						key="staticText-22"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[2200 BROENING HWY]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="2"
						y="82"
						width="148"
						height="14"
						key="staticText-23"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[BALTIMORE, MD 21224]]></text>
				</staticText>
				<image  onErrorType="Icon" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="321"
						y="0"
						width="183"
						height="52"
						key="image-1"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/SSCW_NEW.bmp"]]></imageExpression>
				</image>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="47"  isSplitAllowed="false" >
				<staticText>
					<reportElement
						x="0"
						y="24"
						width="41"
						height="18"
						key="staticText-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[CC :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="5"
						width="41"
						height="18"
						key="staticText-32"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[ENC]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="49"
						y="25"
						width="138"
						height="18"
						key="staticText-35"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[DEPARTMENT OF STATE]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
