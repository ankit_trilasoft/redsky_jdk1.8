<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
</style>

<SCRIPT LANGUAGE="JavaScript">
function totalWeightAndTotalVolume()
{
	var totalWeight = '0';
	var totalVolume = '0';
	var totalQuantity='0';
	
	var totalEstimatedPieces='0';
	var totalEstimatedVolume='0';
	var totalEstimatedWeight='0';
	
	<c:forEach var="entry" items="${dataItemList}"> 
	var weight = '${entry.weightReceived}';
	var volume = '${entry.volumeReceived}';
	var quantity = '${entry.quantityReceived}';
	var workTicketId = '${entry.workTicketId}';
	
	var estimatedPieces='${entry.qtyExpected}';
	var estimatedVolume='${entry.volume}';
    var estimatedWeight='${entry.weight}';
	
	totalWeight=totalWeight*1+weight*1;
	totalVolume=totalVolume*1+volume*1;
	totalQuantity = totalQuantity*1+quantity*1;
	
	 totalEstimatedPieces=totalEstimatedPieces*1+estimatedPieces*1;
     totalEstimatedVolume=totalEstimatedVolume*1+estimatedVolume*1;
     totalEstimatedWeight=totalEstimatedWeight*1+estimatedWeight*1;
    
	</c:forEach>
	 if(totalWeight !='0'){
		    document.getElementById('totalWeight').value=totalWeight.toFixed(2);
		    }else{
		    	document.getElementById('totalWeight').value='0.00';
		    }
		    if(totalVolume !='0'){
		    document.getElementById('totalVolume').value=totalVolume.toFixed(2);
		    }else{
		    	document.getElementById('totalVolume').value='0.00'	
		    }
		    
		    if(totalEstimatedPieces !='0'){
	            document.getElementById('totalEstPieces').value=totalEstimatedPieces;
	            document.getElementById('estimatedPieces').value=totalEstimatedPieces;
	            }else{
	                document.getElementById('totalEstPieces').value='0';
	                document.getElementById('estimatedPieces').value='0';
	            }
		    
		    if(totalEstimatedVolume !='0'){
	            document.getElementById('totalEstVolume').value=totalEstimatedVolume.toFixed(2);
	            document.getElementById('estimatedCubicFeet').value=totalEstimatedVolume.toFixed(2);
	            }else{
	                document.getElementById('totalEstVolume').value='0.00';
	                document.getElementById('estimatedCubicFeet').value='0.00';
	            }
		    
		    if(totalEstimatedWeight !='0'){
	            document.getElementById('totalEstWeight').value=totalEstimatedWeight.toFixed(2);
	            document.getElementById('estimatedWeight').value=totalEstimatedWeight.toFixed(2);
	            }else{
	                document.getElementById('totalEstWeight').value='0.00';
	                document.getElementById('estimatedWeight').value='0.00';
	            }
		    
		    if(totalQuantity !='0'){
	            document.getElementById('actQuantity').value=totalQuantity.toFixed(2);
	            }else{
	            document.getElementById('actQuantity').value='0.00';
	            }
		    
		    if(totalWeight !='0' && totalVolume !='0' || estimatedPieces  !='0'){
    $.get("updateItemDataWeightVolume.html?ajax=1&decorator=simple&popup=true", 
            {totalWeight:totalWeight,totalVolume:totalVolume,workTicketId:workTicketId,totalEstimatedPieces:totalEstimatedPieces,totalEstimatedVolume:totalEstimatedVolume,totalEstimatedWeight:totalEstimatedWeight},
            function(data){ 
            	
        });
}}
totalWeightAndTotalVolume();
function findAllResource1()
{
	var st2=document.getElementById('itemData');
	st2.style.display = 'none';
	setTimeout(function(){findAllResource();}, 1000); 
	st2.style.display = 'block';
}
</script>

    <s:set name="dataItemList" value="dataItemList" scope="request" />
	<display:table name="dataItemList" class="table" style="margin:3px 0px 5px 0px" requestURI="" id="dataItemList">
	<display:column title="Line" property="line" headerClass="containeralign" style="width:50px;text-align:right;">
        </display:column>				
		<display:column title="Item List" style="width:300px;">
		  <a onclick="editDateItem('${dataItemList.id}','${dataItemList.serviceOrderId}','${dataItemList.workTicketId}','${dataItemList.shipNumber}')">${dataItemList.itemDesc}</a>
		</display:column>	   	
		<%-- <display:column property="itemUomid" title="Item UomID" style="width:60px;text-align:right;"></display:column>	   --%> 	
		<display:column style="width:250px;" title="Warehouse">
		
			<c:forEach var="chrms" items="${house}" varStatus="loopStatus">
			<c:choose>
			<c:when test="${chrms.key == dataItemList.warehouse}">
			<c:set var="selectedInd" value=" selected"></c:set>
			<c:out value="${chrms.value}"></c:out>
			</c:when>
			</c:choose>
    </option>
	</c:forEach> 
		</display:column>	
		
		  <display:column property="qtyExpected" title="Estimated&nbsp;Pieces" headerClass="containeralign" style="text-align:right;width:60px;"></display:column>     
        <display:column title="Estimated&nbsp;Volume" headerClass="containeralign" style="text-align:right;width:60px;"> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${dataItemList.volume}" /></display:column> 
        <display:column title="Estimated&nbsp;Weight"    headerClass="containeralign" style="text-align:right;width:60px;"> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${dataItemList.weight}" /></display:column>     
		
		<display:column property="quantityReceived" title="Actual Quantity" headerClass="containeralign" style="text-align:right;width:80px;"></display:column>	   	
		<display:column title="Actual Volume" headerClass="containeralign" style="text-align:right;width:80px;"> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${dataItemList.volumeReceived}" /></display:column>	
		<display:column title="Actual Weight" headerClass="containeralign" style="text-align:right;width:80px;"> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${dataItemList.weightReceived}" /></display:column>	   	
		<%-- <display:column property="totalWeight" title="Total Weight" headerClass="containeralign" style="text-align:right;"></display:column> --%>
		<display:column property="notes" title="Notes" style="width:250px;" maxLength="30"></display:column>	
		 <c:if test="${ticketAssignedStatus!='Assigned'}"><display:column title="Remove" style="width:5px;text-align:center;">
		<a><img align="middle" onclick="deleteItemData('${dataItemList.id}','${dataItemList.workTicketId}')" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
		</display:column>
		</c:if>						
		<display:footer>		
		 <tr> 
              <td align="right" colspan="2"></td>
              
              <td style="text-align: right ! important; padding-right: 2px;"><div style="float: right;line-height: 18px;padding-right: 2px;">Total:</div></td>
              
               <td style="width: 85px; text-align: right ! important; padding-right: 2px;">
               <div style="float:right">
               <div style="float:left;padding-left:2px;"><input type="text" id='totalEstPieces' style="text-align:right;width:60px;" readonly="true" size="10" class="input-textUpper myclass"/></div>
               </div>
               </td> 
               
                <td style="width: 85px; text-align: right ! important; padding-right: 2px;">
               <div style="float:right">
               <div style="float:left;padding-left:2px;"><input type="text" id='totalEstVolume' style="text-align:right;width:60px;" readonly="true" size="10" class="input-textUpper myclass"/></div>
               </div>
               </td> 
               
                <td style="width: 85px; text-align: right ! important; padding-right: 2px;">
               <div style="float:right">
               <div style="float:left;padding-left:2px;"><input type="text" id='totalEstWeight' style="text-align:right;width:60px;" readonly="true" size="10" class="input-textUpper myclass"/></div>
               </div>
               </td> 
              
              
              <td style="text-align: right ! important; padding-right: 2px;"><div style="float: right;line-height: 18px;padding-right: 2px;"><input type="text" id='actQuantity' style="text-align:right;width:60px;" readonly="true" size="10" class="input-textUpper myclass"/></div></td>
               <td style="width: 85px; text-align: right ! important; padding-right: 2px;">
               <div style="float:right">
               <div style="float:left;padding-left:2px;"><input type="text" id='totalVolume' style="text-align:right;width:60px;" readonly="true" size="10" class="input-textUpper myclass"/></div>
               </div>
               </td>   
				<td style="text-align: right ! important; padding-right: 2px; width:85px;">
               <div style="float:right">
               <div style="float: left;line-height: 18px;"></div><div style="float:left;padding-right:2px;"><input type="text" style="text-align:right;width:60px;" readonly="true" id='totalWeight' size="10" class="input-textUpper myclass"/></div>
               </div>
               </td>   
               <td></td> 
                <td></td> 
          </tr>		
		</display:footer>		
	</display:table>
		<c:if test="${ticketAssignedStatus!='Assigned'}">
		<input type="button" class="cssbutton1" id="addForm" name="addForm" style="width:70px; margin-bottom:5px;" value="Add" onclick="dataListForm('');" />
		</c:if>	
		
	     </div>
		</td>
	</tr>