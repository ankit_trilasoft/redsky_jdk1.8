<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Payable Processing</title>
<STYLE type=text/css>
/* CSS Reset   
----------------------------------------------------------*/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, font, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td 
{
	margin: 0;	
	border: 0;
	outline: 0;
	background: transparent;
}
table 
{
	border-collapse: collapse;
	border-spacing: 0;
}
/* h1 style 
----------------------------------------------------------*/
h1
{
	font-size: 20px;
	text-align: center;
	background-color: #ffffdd;
	border: solid 1px #888;
	margin: 0 0 10px 0;
	padding: 5px;
}

/* Table style  
----------------------------------------------------------*/
table.tbl1
{
	width:2000px
}

table.tbl1 thead
{
	background: #BCD2EF url('../images/tbl_head_bg.png') repeat-x scroll 0 0;
	color: #333;
width: 1400px;
}

table.tbl1 tbody
{
	width: 2000px;
}

table.tbl1 tr td,
table.tbl1 tr th
{	
	border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    text-decoration: none;
    height:25px;
  
  padding: 0 !important;
  margin: 0 !important;

}

table.tbl1 tr td
{
	text-align: center;
	background-color: #fff;
	color: #444;
}

table.tbl1 tbody tr.alt td
{
	background-color: #e4ecf7;
}

table.tbl1 tr.over td,
table.tbl1 tbody tr.alt.over td
{
	background-color: #bcd4ec;
	color: #000;
}

table.tbl1 a
{
	color:#15428B
}

table.tbl1 a:active
{
	color:#15428B
}

table.tbl1 a:visited
{
	color:#15428B
}


table.tbl1 a:hover
{
	text-decoration: none;
}

table.tbl1 a:visited
{
	
}

/* Define column widths 
----------------------------------------------------------*/
table.tbl1 td.id
{
	
}
table.tbl1 td.SO
{
	
}
table.tbl1 td.Shipper
{
	text-align: left;
padding-left:5px !important;
}
table.tbl1 td.Category
{
	
}
table.tbl1 td.Line
{
	
}
table.tbl1 td.cCode
{
	
}
table.tbl1 td.cDivision
{
	
}
table.tbl1 td.Note
{
	text-align: left;
padding-left:5px !important;
}
table.tbl1 td.Basis
{
	
}
table.tbl1 td.estPayAmount
{
	
}
table.tbl1 td.revPayAmount
{
	
}
table.tbl1 td.actExpense
{
	
}
table.tbl1 td.locAmount
{
	
}
table.tbl1 td.Currency
{
	
}
table.tbl1 td.Invoice
{
	
}
table.tbl1 td.Status
{
	
}
table.tbl1 td.actgCode
{
	
}
table.tbl1 td.actualExpense
{
	
}
table.tbl1 td.VATDesc
{
	
}
table.tbl1 td.VATPercent
{
	
}
table.tbl1 td.VAT
{
	
}



</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
    <script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
	<!--<script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/scrolltable.js"></script>-->
    
<!-- Modification closed here -->
<script type="text/javascript">

	    $(document).ready(function() {
			/* zebra stripe the tables (not necessary for scrolling) */
			
	        var tbl = $("table.tbl1");
	        addZebraStripe(tbl);
	        addMouseOver(tbl);

	        
	        var tblcell = document.getElementById('dataTable').rows[0].cells.length ;

	      
			
			/* make the table scrollable with a fixed header */	        
	        if(tblcell == '20')
	        { 
	        	$("#tablewrap").width('2600');
				$("#bodywrap").width('2600');

		        }
			if(tblcell == '16')
	        { 
				$("#tablewrap").width('2200');
				$("#bodywrap").width('2200');
		            //height: '400px'
		        

		    }
			var rowCount = $('#dataTable >tbody >tr').length;			
			
			 var theWidth = $('#bodywrap table td.id').width(); 
			   var sowidth = $('#bodywrap table td.SO').width();			     
			   var shiprwidth = $('#bodywrap table td.Shipper').width();
			   var catwidth = $('#bodywrap table td.Category').width(); 
			   var linewidth = $('#bodywrap table td.Line').width();
			   var cCodewidth = $('#bodywrap table td.cCode').width();
			   var cDivwidth = $('#bodywrap table td.cDivision').width();
			   var notewidth = $('#bodywrap table td.Note').width();
			   var basiswidth = $('#bodywrap table td.Basis').width();
			   var estPaywidth = $('#bodywrap table td.estPayAmount').width();
			   var revPaywidth = $('#bodywrap table td.revPayAmount').width();
			   var actExpwidth = $('#bodywrap table td.actExpense').width();
			   var locAmtwidth = $('#bodywrap table td.locAmount').width();
			   var curwidth = $('#bodywrap table td.Currency').width();
			   var invwidth = $('#bodywrap table td.Invoice').width();
			   var statwidth = $('#bodywrap table td.Status').width();
			   var actcodwidth = $('#bodywrap table td.actgCode').width();
			 
			  
			  $('#headwrap table th.thid').attr('style', 'width:'+theWidth+'px !important; text-align:center'); 
			  $('#headwrap table th.thso').attr('style', 'width:'+sowidth+'px !important; text-align:center');
			  $('#headwrap table th.thship').attr('style', 'width:'+shiprwidth+'px !important; text-align:left !important ;padding-left:5px !important;'); 
			  $('#headwrap table th.thcat').attr('style', 'width:'+catwidth+'px !important; text-align:center');
			  $('#headwrap table th.thline').attr('style', 'width:'+linewidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thccode').attr('style', 'width:'+cCodewidth+'px !important; text-align:center'); 
			  $('#headwrap table th.thcdiv').attr('style', 'width:'+cDivwidth+'px !important; text-align:center'); 
			  $('#headwrap table th.thnote').attr('style', 'width:'+notewidth+'px !important; text-align:left !important ;padding-left:5px !important;');
			  $('#headwrap table th.thbasis').attr('style', 'width:'+basiswidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thestpay').attr('style', 'width:'+estPaywidth+'px !important;text-align:center'); 
			  $('#headwrap table th.threvpay').attr('style', 'width:'+revPaywidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thactexp').attr('style', 'width:'+actExpwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thlocamt').attr('style', 'width:'+locAmtwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thcur').attr('style', 'width:'+curwidth+'px !important; text-align:center'); 
			  $('#headwrap table th.thinv').attr('style', 'width:'+invwidth+'px !important; text-align:center');
			  $('#headwrap table th.thstat').attr('style', 'width:'+statwidth+'px !important; text-align:center'); 
			  $('#headwrap table th.thactcod').attr('style', 'width:'+actcodwidth+'px !important; text-align:center');  


	         var tableheight = $('#bodywrap table').height();
            // $('#tablewrap').attr('style','height:'+tableheight+'px; overflow-x:scroll ');
			  
			
				 $("#addLine").live("click",function(){				 

					 var theWidth = $('#bodywrap table td.id').width(); 
					   var sowidth = $('#bodywrap table td.SO').width();
					   var shiprwidth = $('#bodywrap table td.Shipper').width();
					   var catwidth = $('#bodywrap table td.Category').width(); 
					   var linewidth = $('#bodywrap table td.Line').width();
					   var cCodewidth = $('#bodywrap table td.cCode').width();
					   var cDivwidth = $('#bodywrap table td.cDivision').width();
					   var notewidth = $('#bodywrap table td.Note').width();
					   var basiswidth = $('#bodywrap table td.Basis').width();
					   var estPaywidth = $('#bodywrap table td.estPayAmount').width();
					   var revPaywidth = $('#bodywrap table td.revPayAmount').width();
					   var actExpwidth = $('#bodywrap table td.actExpense').width();
					   var locAmtwidth = $('#bodywrap table td.locAmount').width();
					   var curwidth = $('#bodywrap table td.Currency').width();
					   var invwidth = $('#bodywrap table td.Invoice').width();
					   var statwidth = $('#bodywrap table td.Status').width();
					   var actcodwidth = $('#bodywrap table td.actgCode').width();					 
					  
					   $('#headwrap table th.thid').attr('style', 'width:'+theWidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thso').attr('style', 'width:'+sowidth+'px !important; text-align:center');
						  $('#headwrap table th.thship').attr('style', 'width:'+shiprwidth+'px !important; text-align:left !important ;padding-left:5px !important;'); 
						  $('#headwrap table th.thcat').attr('style', 'width:'+catwidth+'px !important; text-align:center');
						  $('#headwrap table th.thline').attr('style', 'width:'+linewidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thccode').attr('style', 'width:'+cCodewidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thcdiv').attr('style', 'width:'+cDivwidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thnote').attr('style', 'width:'+notewidth+'px !important; text-align:left !important ;padding-left:5px !important;'); 
						  $('#headwrap table th.thbasis').attr('style', 'width:'+basiswidth+'px !important ;text-align:center'); 
						  $('#headwrap table th.thestpay').attr('style', 'width:'+estPaywidth+'px !important;text-align:center'); 
						  $('#headwrap table th.threvpay').attr('style', 'width:'+revPaywidth+'px !important;text-align:center'); 
						  $('#headwrap table th.thactexp').attr('style', 'width:'+actExpwidth+'px !important;text-align:center'); 
						  $('#headwrap table th.thlocamt').attr('style', 'width:'+locAmtwidth+'px !important;text-align:center'); 
						  $('#headwrap table th.thcur').attr('style', 'width:'+curwidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thinv').attr('style', 'width:'+invwidth+'px !important; text-align:center');
						  $('#headwrap table th.thstat').attr('style', 'width:'+statwidth+'px !important; text-align:center'); 
						  $('#headwrap table th.thactcod').attr('style', 'width:'+actcodwidth+'px !important; text-align:center');  
				  });
			
			if( rowCount > 10)
			{			  


				   $('#bodywrap').attr('style', 'width:2017px; height: 350px !important; overflow-y: scroll !important');
			   			   
			   var theWidth = $('#bodywrap table td.id').width(); 
			   var sowidth = $('#bodywrap table td.SO').width();
			   var shiprwidth = $('#bodywrap table td.Shipper').width();
			   var catwidth = $('#bodywrap table td.Category').width(); 
			   var linewidth = $('#bodywrap table td.Line').width();
			   var cCodewidth = $('#bodywrap table td.cCode').width();
			   var cDivwidth = $('#bodywrap table td.cDivision').width();
			   var notewidth = $('#bodywrap table td.Note').width();
			   var basiswidth = $('#bodywrap table td.Basis').width();
			   var estPaywidth = $('#bodywrap table td.estPayAmount').width();
			   var revPaywidth = $('#bodywrap table td.revPayAmount').width();
			   var actExpwidth = $('#bodywrap table td.actExpense').width();
			   var locAmtwidth = $('#bodywrap table td.locAmount').width();
			   var curwidth = $('#bodywrap table td.Currency').width();
			   var invwidth = $('#bodywrap table td.Invoice').width();
			   var statwidth = $('#bodywrap table td.Status').width();
			   var actcodwidth = $('#bodywrap table td.actgCode').width(); 

			 
			  
			  $('#headwrap table th.thid').attr('style', 'width:'+theWidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thso').attr('style', 'width:'+sowidth+'px !important;text-align:center');
			  $('#headwrap table th.thship').attr('style', 'width:'+shiprwidth+'px !important; text-align:left !important ;padding-left:5px !important'); 
			  $('#headwrap table th.thcat').attr('style', 'width:'+catwidth+'px !important;text-align:center');
			  $('#headwrap table th.thline').attr('style', 'width:'+linewidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thccode').attr('style', 'width:'+cCodewidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thcdiv').attr('style', 'width:'+cDivwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thnote').attr('style', 'width:'+notewidth+'px !important; text-align:left !important ;padding-left:5px !important;'); 
			  $('#headwrap table th.thbasis').attr('style', 'width:'+basiswidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thestpay').attr('style', 'width:'+estPaywidth+'px !important;text-align:center'); 
			  $('#headwrap table th.threvpay').attr('style', 'width:'+revPaywidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thactexp').attr('style', 'width:'+actExpwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thlocamt').attr('style', 'width:'+locAmtwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thcur').attr('style', 'width:'+curwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thinv').attr('style', 'width:'+invwidth+'px !important;text-align:center');
			  $('#headwrap table th.thstat').attr('style', 'width:'+statwidth+'px !important;text-align:center'); 
			  $('#headwrap table th.thactcod').attr('style', 'width:'+actcodwidth+'px !important;text-align:center');  
			  
			   			   
			}  

	    });

	    function addZebraStripe(table) {
            table.find("tbody tr:odd").addClass("alt");
        }

        function addMouseOver(table) {
            table.find("tbody tr").hover(
                    function() {
                        $(this).addClass("over");
                    },
                    function() {
                        $(this).removeClass("over");
                    }
                );
        }

	</script>
<script type="text/JavaScript">

  window.onload = function() {
	  if (screen.width > 1600 )
		{
				 var elem = document.getElementById("para1");				
				 elem.style.overflow = 'none';
				 elem.style.width = '100%';
				
		 } 

	  if (screen.width == 1600 )
		{
				 var elem = document.getElementById("para1");				
				 elem.style.overflow = 'none';
				 elem.style.width = '100%';
				
		 }

	  if (screen.width == 1440 )
		{
				 var elem = document.getElementById("para1");				
				 elem.style.overflow = 'auto';
				 elem.style.width = '1320px';
				
		 }

	  if (screen.width == 1360 )
		{
		 var elem = document.getElementById("para1");				
		 elem.style.overflow = 'auto';
		 elem.style.width = '1250px';
		
}
	  if(screen.width == 1280)
    {
  	  var elem = document.getElementById("para1");
  	  elem.style.overflow = 'auto';
  	  elem.style.width = '1160px';
        }
    if(screen.width == 1152)
    {
  	  var elem = document.getElementById("para1");
  	  elem.style.overflow = 'auto';
  	  elem.style.width = '1035px';
        }
    if(screen.width == 1024)
    {
  	  var elem = document.getElementById("para1");
  	  elem.style.overflow = 'auto';
  	  elem.style.width = '970px';
        }
		 

}
  
  
</script>

<script type="text/javascript">
function checkFloatNew(temp) {
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.id;  
	var result2=fieldName.replace(fieldName.substring(0,3),''); 
	var count = 0;
	var countArth = 0;

    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{       	  
          document.getElementById(fieldName).select();
          document.getElementById(fieldName).value=''; 
       	   
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  }  
    if(check=='Invalid'){     
    document.getElementById(fieldName).select();
    document.getElementById(fieldName).value=''; 
     
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.getElementById(fieldName).value=value; 
    }  
}
function checkFloatVatNew(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.id;  
	var result2=fieldName.replace(fieldName.substring(0,3),''); 
	var count = 0;
	var countArth = 0;
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.getElementById(fieldName).select();
    document.getElementById(fieldName).value='0.00';
    calculatePayVatAmtNew(result2) 
	}else{
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data." ); 
          document.getElementById(fieldName).select();
          document.getElementById(fieldName).value=''; 
       	   
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  }  
    if(check=='Invalid'){ 
    alert("Invalid data." ); 
    document.getElementById(fieldName).select();
    document.getElementById(fieldName).value=''; 
     
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.getElementById(fieldName).value=value; 
    }  }}
function checkFloatVat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.id;
	var result2=fieldName.replace(fieldName.substring(0,3),'');  
	var count = 0;
	var countArth = 0;
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.getElementById(fieldName).value='0.00'; 
	document.getElementById(fieldName).select(); 
    calculatePayVatAmt(result2);
	}else{
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data." ); 
          document.getElementById(fieldName).select();
          document.getElementById(fieldName).value=''; 
       	   
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  }  
    if(check=='Invalid'){ 
    alert("Invalid data ." ); 
    document.getElementById(fieldName).select();
    document.getElementById(fieldName).value=''; 
     
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.getElementById(fieldName).value=value; 
    }  } }
function checkFloat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.id;  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data." ); 
          document.getElementById(fieldName).select();
          document.getElementById(fieldName).value=''; 
       	   
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  }  
    if(check=='Invalid'){ 
    alert("Invalid data ." ); 
    document.getElementById(fieldName).select();
    document.getElementById(fieldName).value=''; 
     
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.getElementById(fieldName).value=value; 
    }  } 
function getcount(result){
var count= document.getElementById('name').value;
document.getElementById(result).value=document.getElementById('name').value;
document.getElementById('name').value=Number(count)+ Number(1);
}
function setInvoiceNumber(id,param){	
	var actualExpense=0;
	if(param.value!=''){
	actualExpense=eval(param.value);
	} 
	if((actualExpense<=0)&&(actualExpense==''))
	{		
		document.getElementById('inv'+id).value='';
	}else{		
		 document.getElementById('inv'+id).value=document.getElementById('vendorInvoice').value;
		 
		
	}
	var currency =document.forms['payableProcessing'].elements['currency'].value;
	var exchangeRate =document.forms['payableProcessing'].elements['exchangeRate'].value;
	document.getElementById('cur'+id).value=currency;
	document.getElementById('exr'+id).value=exchangeRate;
	calculatePayVatAmt(id);
}
function checkPayGlCode(id,payGl)
{	
	 var result2=id;
	 payGl=payGl.trim();
	 if(payGl=='')
	 {
		 alert("Payable GL Code missing for this accountline");		 
		 document.getElementById('exp'+result2).value='0.00';
			 
	 }
}

function checkActgCode1(id,param){   
	 if(param.value!=''){	    
	      var result2=id;
        <c:if test="${accountInterface=='Y'}"> 
    var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
	 var companyDivision=document.getElementById('com'+result2).value;   
    vendorId=vendorId.trim();
    companyDivision=companyDivision.trim();    
   if(companyDivision!='')
   {  
   if(vendorId!=''){       
	     var url=""
	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		    try{
		    	actgCodeFlag=document.forms['payableProcessing'].elements['actgCodeFlag'].value;
		    }catch(e){} 
	     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);
	    </c:if>
	    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
	     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
	    </c:if>
       httpvandor1.open("GET", url, true);
       httpvandor1.onreadystatechange =function(){ handleHttpResponse999(result2);};
	    httpvandor1.send(null);
   }else{
       alert("Please enter vendor code first.");
       document.forms['payableProcessing'].elements['vendorCode'].focus();
       document.getElementById('exp'+temp).value='';
       calculatetotalActualExpense();
       return false;
   }
   }
   </c:if>
	 } 
	 return true;              
 }
   function handleHttpResponse999(temp){
		if (httpvandor1.readyState == 4){
               var results = httpvandor1.responseText
               results = results.trim();  
               var res = results.split("#"); 
          	 var companyDivision=document.getElementById('com'+temp).value;   
               if(res.length >= 2){ 
		                if(res[3]=='Approved'){		                     
		                   if(res[2] != "null" && res[2] !=''){ 
		                	   document.getElementById('act'+temp).value=res[2];  
		                	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		                	    try{
			                	if(res[8] != "null" && res[8].trim() !=''){
			                		document.getElementById('com'+temp).value=res[8].trim();
			                	}}catch(e){} 
	 	                	   </c:if>
				            } else if (res[2] == "null" || res[2] =='') {
				            	document.getElementById('exp'+temp).value='0.00';
					            document.getElementById('act'+temp).value='';				            	
				              <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
				              alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing for company division "+companyDivision+", please contact your accounting dept.");
				              </c:if>
				              <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}">
				              alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept."); 
				              </c:if>
				              calculatetotalActualExpense();
				              return false;					  
				            } 		              	
				        }else{
				            alert("Partner code is not approved" );
			            	document.getElementById('exp'+temp).value='0.00';
				            document.getElementById('act'+temp).value='';  
				            calculatetotalActualExpense();
				            return false; 						 
				        }
		            }else{
		                alert("This Partner Code cannot be processed as the Accounting code for the partner is missing for company division "+companyDivision+", please contact your accounting dept." ); 						
		            	document.getElementById('exp'+temp).value='0.00';
			            document.getElementById('act'+temp).value=''; 
		                calculatetotalActualExpense();
		                return false;
					}  } }

function setInvoiceNumberNew(id,param){
	var actualExpense=0;
	if(param.value!=''){
	actualExpense=eval(param.value);
	} 
	if((actualExpense<=0)&&(actualExpense==''))
	{
		document.getElementById('inv'+id).value=''; 
	}else{
		 document.getElementById('inv'+id).value=document.getElementById('vendorInvoice').value;
		
	}
	var currency =document.forms['payableProcessing'].elements['currency'].value;
	var exchangeRate =document.forms['payableProcessing'].elements['exchangeRate'].value;
	document.getElementById('cur'+id).value=currency;
	document.getElementById('exr'+id).value=exchangeRate;
	calculatePayVatAmtNew(id);
}

function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http33 = getHTTPObject();
var http5=getHTTPObject();
  var httpvandor=getHTTPObject();
  var httpvandor1=getHTTPObject();
function toGetSOinfo(result){
if(result.value.trim()!=''){
	var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
    var url="getSOInfoForBatchPAyProcessing.html?ajax=1&decorator=simple&popup=true&sequenceNumber="+result.value+"&defaultVatVendorCode="+vendorId;
    http33.onreadystatechange=function () { handleHttpResponse(result.id); };
    http33.open("GET",url,true);
    http33.send(null);
    }else{
	     var resultvalue=result.id; 
         var result2=resultvalue.replace(resultvalue.substring(0,3),'');
         document.getElementById('nam'+result2).value='';
         document.getElementById('con'+result2).value='';
         document.getElementById('cha'+result2).value='';
         document.getElementById('not'+result2).value='';
         document.getElementById('com'+result2).value='';
         document.getElementById('cnt'+result2).value='';
         <c:if test="${systemDefaultVatCalculation=='true'}">
         document.getElementById('des'+result2).value='';
         document.getElementById('pvp'+result2).value='0.00';
         </c:if>         
    }
}
 function handleHttpResponse(id){
  if(http33.readyState == 4){
     var response=http33.responseText;     
       response=response.trim();
       if(response!=''){
       id=id.replace(id.substring(0,3),'');
       var splitresponse=response.split('#');
         document.getElementById('nam'+id).value=splitresponse[0];
         document.getElementById('con'+id).value=splitresponse[1];
         document.getElementById('cnt'+id).value=splitresponse[3];
         <c:if test="${systemDefaultVatCalculation=='true'}">
         document.getElementById('des'+id).value=splitresponse[4].trim();
         document.getElementById('pvp'+id).value=splitresponse[5].trim();
         </c:if>
		var compDiv='${findCompanyDivisionByCorpId}';
		compDiv=compDiv.replace('[','').replace(']','');
		var compDivArray=compDiv.split(",");
		var cont=1;  
		for(var x=0;x<compDivArray.length;x++){
			var val1=compDivArray[x]+'';
			var val2=splitresponse[2]+'';
		  if(val1.trim()==val2.trim())
		  { break;
		  }
		  cont++;		   
		}
			document.forms['payableProcessing'].elements['com'+id].options[cont].value=splitresponse[2];
			document.forms['payableProcessing'].elements['com'+id].options[cont].text=splitresponse[2];
			document.forms['payableProcessing'].elements['com'+id].options[cont].selected=true;
         }else{
         alert("Selected Service Order is not Active. Please select a active Service Order");
         id=id.replace(id.substring(0,3),'');
         document.getElementById('seq'+id).value='';
         document.getElementById('nam'+id).value='';
         <c:if test="${systemDefaultVatCalculation=='true'}">
         document.getElementById('des'+id).value='';
         document.getElementById('pvp'+id).value='0.00';
         </c:if>        
         document.forms['payableProcessing'].elements['seq'+id].focus();
         }
    }
  }
 
function chk(result){
  var resultvalue=result; 
  var result2=resultvalue.replace(resultvalue.substring(0,3),'');
      var soNo=document.getElementById('seq'+result2).value;
      if(soNo.trim()!=''){
      contract=document.getElementById('con'+result2).value;
      if(contract.trim()!=''){
      javascript:openWindow('chargess.html?contract='+contract+'&originCountry=&destinationCountry=&serviceMode=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=rgl'+result2+'&fld_description=not'+result2+'&fld_thirdDescription=pgl'+result2+'&fld_code='+result);
      }else{
          alert("Selected Service Order has no Contract Selected - Please Select Contract First");
          document.forms['payableProcessing'].elements[result].focus(); 
      } 
      }else{
    	  alert("Please Select Service Order First.");
    	  document.forms['payableProcessing'].elements['cha'+result2].value='';
    	  document.forms['payableProcessing'].elements['seq'+result2].focus();
      }
         
}  

//function toaddAutocompletetoOnload(textId,divid,contractId){
  //window.onload=function(){
     //alert(contractId);
     // url="/getSOInfoForBatchPAyProcessing.html?decorator=simple&popup=true&sequenceNumber="+document.getElementById(contractId).value;
     //new Ajax.Autocompleter(textId,divid,url,{callback:function(ele,entry){alert(ele.value+",,,,"+entry); return "/getSOInfoForBatchPAyProcessing.html?decorator=simple&popup=true&sequenceNumber="+document.getElementById(contractId).value; }});
  //}
 // }

</script>

<script type="text/javascript">
function addRow(tableID) {
	var tot=parseFloat(document.getElementById('totalExpense').value);
	var amt=parseFloat(document.getElementById('amount').value);		        
	if(amt<tot)
	{
		alert("Payable Invoice amount is not equal to entered amount.Please correct and try again.");
		return false;	
	}else{
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
            var rowCountSSCW = rowCount;
            <configByCorp:fieldVisibility componentId="component.field.Alternative.showFrzeeHeader">
            rowCountSSCW=rowCountSSCW+1
            if(rowCount > 10)
            {
            	$.fn.scrollTo = function( target, options, callback ){
            		  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
            		  var settings = $.extend({
            		    scrollTarget  : target,
            		    offsetTop     : 50,
            		    duration      : 500,
            		    easing        : 'swing'
            		  }, options);
            		  return this.each(function(){
            		    var scrollPane = $(this);
            		    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
            		    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
            		    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
            		      if (typeof callback == 'function') { callback.call(this); }
            		    });
            		  });
            		}
            	var lastRow = $('#bodywrap table tr:last')
        		           	
            	$('#bodywrap').scrollTo(lastRow);            	              	 
               }
            </configByCorp:fieldVisibility>
            var actualExpense;
            var previousrow=Number(rowCountSSCW)-1;
            if(document.getElementById('exp'+previousrow)==null){
              actualExpense=123;
            }else{
            actualExpense=document.getElementById('exp'+previousrow).value;
            }          
            if(actualExpense=='' || Math.round(actualExpense)==0){
              alert("Actual expense is required field and can not be zero");
            }else{
            var newlineid=document.getElementById('newlineid').value;              
            if(newlineid==''){
                newlineid=rowCountSSCW;
              }else{
              newlineid=newlineid+"#"+rowCountSSCW;
              }
            document.getElementById('newlineid').value=newlineid; 
            var row = table.insertRow(rowCount);
            
            var cell2 = row.insertCell(0);
            cell2.innerHTML = rowCountSSCW;
            cell2.setAttribute("class", "listwhitetext" );
            var element999 = document.createElement("input");
            element999.type = "hidden";
            element999.id='num'+rowCountSSCW;
            element999.value=rowCount;
            element999.name='accountlineNumberList';
            cell2.appendChild(element999);
 
            var cell1 = row.insertCell(1);
            var element1 = document.createElement("input");
            element1.type = "text";
            element1.style.width="80px";
            element1.setAttribute("onChange", "toGetSOinfo(this);" );
            element1.setAttribute("class", "input-text" );
            element1.id='seq'+rowCountSSCW
            cell1.appendChild(element1);
           
            var cell3 = row.insertCell(2);
            var element2 = document.createElement("input");
            element2.type = "text";
            element2.style.width="80px"
             element2.setAttribute("class", "input-text" );
            element2.id='nam'+rowCountSSCW
            cell3.appendChild(element2);
            
             var cell15 = row.insertCell(3);
             var element3 = document.createElement("select");
                 element3.setAttribute("class", "list-menu" );
                 element3.id="cat"+rowCountSSCW;
                 element3.style.width="60px"
            var catogeryList='${category}';
            catogeryList=catogeryList.replace('{','').replace('}','');
            var catogeryarray=catogeryList.split(",");
                var optioneleBlankCategory= document.createElement("option");
                optioneleBlankCategory.value="";
                optioneleBlankCategory.text=""; 
                element3.options.add(optioneleBlankCategory);  
            for(var i=0;i<catogeryarray.length;i++){
               var value=catogeryarray[i].split("=");
                var optionele= document.createElement("option");
                optionele.value=value[0];
                optionele.text=value[1]; 
                element3.options.add(optionele);             
             }
             cell15.appendChild(element3);
              
             var cell4 = row.insertCell(4);
           // var element4 = document.createElement("input");
           // element4.type = "text";
           // element4.setAttribute("class", "input-text" );
           // element4.style.width="50px";
           // element4.setAttribute("readonly","readonly");
           // cell4.appendChild(element4);
           
             
             var cell5 = row.insertCell(5);
              //cell5.setAttribute("style","width:70px;");
            var element5 = document.createElement("input");
            element5.type = "text";
            element5.setAttribute("onfocus","checkingpay(this);");
            element5.setAttribute("onchange","checkChargeCode(this);");
            element5.setAttribute("class", "input-text" );
            element5.style.width="50px"
            element5.id="cha"+rowCountSSCW;
            element5.name="cha"+rowCountSSCW;         
            cell5.appendChild(element5);
           
             var elementimg=document.createElement("img");
              elementimg.setAttribute("onclick","chk(\"cha"+rowCountSSCW+"\")");
              elementimg.setAttribute("class","openpopup");
              elementimg.setAttribute("width","15");
              elementimg.setAttribute("height","17");
              elementimg.setAttribute("style","vertical-align:top;padding-left:2px");
              elementimg.setAttribute("src","<c:url value='/images/open-popup.gif'/>");
              cell5.appendChild(elementimg);
             var elementpgl = document.createElement("input");
            elementpgl.type = "hidden";
            elementpgl.name="pgl"+rowCountSSCW;
            elementpgl.id="pgl"+rowCountSSCW;
            elementpgl.value='';
            cell5.appendChild(elementpgl);
             var elementrgl = document.createElement("input");
            elementrgl.type = "hidden";
            elementrgl.name="rgl"+rowCountSSCW;
            elementrgl.id="rgl"+rowCountSSCW;
             elementrgl.value='';
            cell5.appendChild(elementrgl);
            

            var cell26 = row.insertCell(6);
            var element26 = document.createElement("select");
                element26.setAttribute("class", "list-menu" );
                element26.setAttribute("onchange","checkActgCode(this);");
                element26.style.width="80px"
                element26.id="com"+rowCountSSCW;
                element26.name="com"+rowCountSSCW;
 
           var compDiv='${findCompanyDivisionByCorpId}';
        
           compDiv=compDiv.replace('[','').replace(']','');
           var compDivArray=compDiv.split(",");  
            var optioneleblnkcomp= document.createElement("option");
               optioneleblnkcomp.value="";
               optioneleblnkcomp.text="";            
               element26.options.add(optioneleblnkcomp);  
           for(var x=0;x<compDivArray.length;x++){
              var value26=compDivArray[x];
               var optionele26= document.createElement("option");
               optionele26.value=value26;
               optionele26.text=value26; 
               element26.options.add(optionele26);             
            }
            cell26.appendChild(element26);
            
            
             var cell20 = row.insertCell(7);
            var element20 = document.createElement("input");
            element20.type = "text";
            element20.style.width="70px";
            element20.setAttribute("class", "input-text" );
            element20.id='not'+rowCountSSCW
            cell20.appendChild(element20);
            
            
             var cell16 = row.insertCell(8);
             var element16 = document.createElement("select");
                 element16.setAttribute("class", "list-menu" );
                 element16.style.width="50px"
                 element16.id="bas"+rowCountSSCW;
            var basis='${basis}';
            basis=basis.replace('{','').replace('}','');
            var basisarray=basis.split(",");  
             var optioneleblnk= document.createElement("option");
                optioneleblnk.value="";
                optioneleblnk.text="";            
                element16.options.add(optioneleblnk);  
            for(var k=0;k<basisarray.length;k++){
               var value2=basisarray[k].split("=");
                var optionele2= document.createElement("option");
                optionele2.value=value2[0];
                optionele2.text=value2[1]; 
                element16.options.add(optionele2);             
             }
             cell16.appendChild(element16);
            

             var cell6 = row.insertCell(9);
            var element6 = document.createElement("input");
            element6.type = "text";
             element6.setAttribute("class", "input-text" );
            element6.style.width="40px"
            element6.id='est'+rowCountSSCW;
            element6.setAttribute("onchange","checkFloatNew(this);checkFloat(this);");
            cell6.appendChild(element6);
           
            
             var cell7 = row.insertCell(10);
            var element7 = document.createElement("input");
            element7.type = "text";
             element7.setAttribute("class", "input-text" );
            element7.style.width="40px"
            element7.id='rev'+rowCountSSCW;
            element7.setAttribute("onchange","checkFloatNew(this);checkFloat(this);");
            cell7.appendChild(element7);
            
                        
             var cell9 = row.insertCell(11);
            var element9 = document.createElement("input");
            element9.type = "text";
            element9.setAttribute("class", "input-text" );
            element9.style.width="50px";
            element9.id='exp'+rowCountSSCW;
            element9.name='actualExpenseList';
            element9.setAttribute("onchange","checkFloatNew(this);checkFloat(this);checkActgCode(this);setInvoiceNumberNew(\'"+rowCountSSCW+"\',this);calculatetotalActualExpense();");
            element9.setAttribute("onkeypress","return numbersonly(this,event);");
            element9.setAttribute("maxlength","12");
            cell9.appendChild(element9);            

           var cell27 = row.insertCell(12);
           var element27 = document.createElement("select");
                element27.setAttribute("class", "list-menu" );
                element27.setAttribute("onchange","checkCurrency(this);");
                element27.style.width="80px"
                element27.id="cur"+rowCountSSCW;
                element27.name='currencyList';
                var optioneleblnkcurr= document.createElement("option");
                optioneleblnkcurr.value="";
                optioneleblnkcurr.text="";            
                element27.options.add(optioneleblnkcurr);                    
                <c:forEach var="entry" items="${country}">
               	 var optionele27= document.createElement("option");
               	 optionele27.value='${entry.key}';
               	 optionele27.text='${entry.value}'; 
              	  element27.options.add(optionele27);             
                </c:forEach>
            cell27.appendChild(element27);
                        

            var element91 = document.createElement("input");
            element91.type = "hidden";
            element91.id='exr'+rowCountSSCW;
            element91.value='';
            element91.name='exchangeRateList';
            cell27.appendChild(element91);
            var element92 = document.createElement("input");
            element92.type = "hidden";
            element92.id='ocr'+rowCountSSCW;
            element92.value='';
            element92.name='oldCurrencyList';
            cell27.appendChild(element92);
            var element93 = document.createElement("input");
            element93.type = "hidden";
            element93.id='oxr'+rowCountSSCW;
            element93.value='';
            element93.name='oldExchangeRateList';
            cell27.appendChild(element93);

                            
            
            var cell8 = row.insertCell(13);
            var element8 = document.createElement("input");
            element8.type = "text";
            element8.setAttribute("class", "input-text" );
            element8.setAttribute("maxlength","17");
            element8.style.width="60px"
            element8.id='inv'+rowCountSSCW;
            cell8.appendChild(element8);
            

            var cell10 = row.insertCell(14);
             var element10 = document.createElement("select");
                 element10.setAttribute("class", "list-menu" );
                 element10.style.width="80px"
                 element10.id="sta"+rowCountSSCW;
            var payingStatus='${payingStatus}';
            payingStatus=payingStatus.replace('{','').replace('}','');
            var payingStatusarray=payingStatus.split(",");
            for(var j=0;j<payingStatusarray.length;j++){
               var value2=payingStatusarray[j].split("=");
                var optionele2= document.createElement("option");
                optionele2.value=value2[0];
                optionele2.text=value2[1]; 
                element10.options.add(optionele2);             
             }
             cell10.appendChild(element10);
             <c:if test="${accountInterface=='Y'}">
             var cell888 = row.insertCell(15);
             var element888 = document.createElement("input");
             element888.type = "text";
             element888.setAttribute("class", "input-text" );
             element888.setAttribute("maxlength","17");
             element888.setAttribute("readonly","true");             
             element888.style.width="60px"
             element888.id='act'+rowCountSSCW;
             cell888.appendChild(element888);
             </c:if>
                          
  <c:if test="${systemDefaultVatCalculation=='true'}">
           var cell222 = row.insertCell(16);
            var element222 = document.createElement("input");
            element222.type = "text";
            element222.setAttribute("class", "input-text" );
            element222.style.width="50px";
            element222.id='aea'+rowCount;
            element222.setAttribute("readonly","true");   
            cell222.appendChild(element222);  
              
            var cell127 = row.insertCell(17);
           var element127 = document.createElement("select");
                element127.setAttribute("class", "list-menu" );
                element127.setAttribute("onchange","getPayPercentNew(this);");
                element127.style.width="80px"
                element127.id="des"+rowCount; 
                var optioneleblnkvat= document.createElement("option");
                optioneleblnkvat.value="";
                optioneleblnkvat.text="";            
                element127.options.add(optioneleblnkvat);                    
                <c:forEach var="entry1" items="${payVatList}">
               	 var optionele127= document.createElement("option");
               	 optionele127.value='${entry1.key}';
               	 optionele127.text='${entry1.value}'; 
              	  element127.options.add(optionele127);             
                </c:forEach>
            cell127.appendChild(element127);
            
            var cell119 = row.insertCell(18);
            var element119 = document.createElement("input");
            element119.type = "text";
            element119.setAttribute("class", "input-text" );
            element119.style.width="50px";
            element119.id='pvp'+rowCount; 
            element119.setAttribute("maxlength","6");
            element119.setAttribute("onchange","onlyFloat1(this);checkFloatVatNew(this);calculatePayVatAmtNew(\'"+rowCount+"\');");
            element119.setAttribute("onkeypress","return numbersonly(this,event);");
            cell119.appendChild(element119);  
            
            var cell120 = row.insertCell(19);
            var element120 = document.createElement("input");
            element120.type = "text";
            element120.setAttribute("class", "input-text" );
            element120.style.width="50px";
            element120.id='pva'+rowCount; 
            element120.setAttribute("readonly","true"); 
            cell120.appendChild(element120);    
       </c:if>
             
            var element11 = document.createElement("input");
            element11.type = "hidden";
            element11.name="con"+rowCountSSCW;
            element11.id="con"+rowCountSSCW;
            cell10.appendChild(element11);

            var element1984 = document.createElement("input");
            element1984.type = "hidden";
            element1984.name="cnt"+rowCountSSCW;
            element1984.id="cnt"+rowCountSSCW;
            cell10.appendChild(element1984);            
            }
	}
            
        }



</script>
<script type="text/javascript">
function checkingpay(result){
  if(result.value.trim()!=''){
  var resultvalue=result.name; 
  var result2=resultvalue.replace(resultvalue.substring(0,3),'');
      if(document.getElementById('pgl'+result2).value.trim()==''){
      alert("Payable GL Code missing. Please select another Charge code");
      document.getElementById('cha'+result2).value='';
      document.getElementById('rgl'+result2).value='';
   }
  } 
}

function setTotalExpense(total,currency){
	 var currentCurrency=document.forms['payableProcessing'].elements['currency'].value;	
	 if(currentCurrency==currency)
	 {
		 var tot=0.00;
		 tot=Number(document.getElementById('totalexpensebase').value) + Number(total);
  		 document.getElementById('totalexpensebase').value=Math.round(tot*100)/100;
	 }  
  }
function setTotalExpense2(){	
  document.getElementById('totalExpense').value=document.getElementById('totalexpensebase').value; 
  document.forms['totalExpenseForm'].elements['totExpValue'].value=document.getElementById('totalexpensebase').value; 
  }
function calculatetotalActualExpense(){
  var totalExpense=document.forms['payableProcessing'].elements['actualExpenseList'];
  var curencyArrayList=document.forms['payableProcessing'].elements['currencyList'];
  var currentCurrency=document.forms['payableProcessing'].elements['currency'].value;	
  var total="0.00";
  if(totalExpense.length!=null){
    for(var i=0;i<totalExpense.length;i++){
       if(totalExpense[i].value!=''){          
           if(curencyArrayList[i].value==currentCurrency)
           {
       			total= parseFloat(totalExpense[i].value)+parseFloat(total);
           }
       }
       }
   }else{
        if(totalExpense.value!=''){
       total=parseFloat(totalExpense.value);
       }
      }
      document.getElementById('totalExpense').value=Math.round(total*100)/100;
}  
function checkChargeCode(result){ 
    var resultvalue=result.id; 
    var result2=resultvalue.replace(resultvalue.substring(0,3),'');
	var soNo=document.getElementById('seq'+result2).value;
     if(soNo.trim()!=''){
		if(result.value!=''){
      		contract=document.getElementById('con'+result2).value;    
      		var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(contract)+"&chargeCode="+encodeURIComponent(result.value); 
     			http5.open("GET", url, true);
     			http5.onreadystatechange =function(){ handleHttpResponse112(result2);};
     			http5.send(null);   
    		}else{
	     		var resultvalue=result.id; 
         		var result2=resultvalue.replace(resultvalue.substring(0,3),'');
         		document.getElementById('not'+result2).value='';
    		}
     }else{
   	  alert("Please Select Service Order First.");
   		document.forms['payableProcessing'].elements['cha'+result2].value='';
	  document.forms['payableProcessing'].elements['seq'+result2].focus();
  }  
}

function handleHttpResponse112(temp){
             if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']',''); 
                var res = results.split("#"); 
                var description1 = "";
                var note = "";  
                if(results.length>6){
	                document.getElementById('rgl'+temp).value = res[1];
	                document.getElementById('pgl'+temp).value = res[2];
	                document.getElementById('not'+temp).value = res[4];
	                if(res[2].trim()==''){
	                alert("Payable GL Code missing. Please select another Charge code");
                     document.getElementById('cha'+temp).value='';
                     document.getElementById('rgl'+temp).value='';
                     document.getElementById('not'+temp).value ='';
	                }
              }else{                   
                    alert("Charge code does not exist for this contract, Please select another" );
                    document.getElementById('cha'+temp).value = '';
                    document.getElementById('rgl'+temp).value = '';
	                document.getElementById('pgl'+temp).value = '';
	                document.getElementById('not'+temp).value ='';

				}  }  }


function save(){

	var tot=parseFloat(document.getElementById('totalExpense').value);
	var amt=parseFloat(document.getElementById('amount').value); 
	if(amt<tot)
	{
		alert("Payable Invoice amount is not equal to entered amount.Please correct and try again.");
	}else{ 
    var flag=checkMandatoryAddLineField(); 
   if(flag==true){ 
   var runningTotal=document.getElementById('totalExpense').value;
   var enteredTotal=Math.round(document.getElementById('amount').value); 
   var newlineid=document.getElementById('newlineid').value; 
     var arrayLine=newlineid.split("#");        
        for(var i=0;i<arrayLine.length;i++){ 
        if(document.getElementById('exp'+arrayLine[i])!=null){
            if(document.getElementById('exp'+arrayLine[i]).value.trim()=="" || (Math.round(document.getElementById('exp'+arrayLine[i]).value)==0)){
               alert("Please enter actual expense for line # "+arrayLine[i]+" as actual expense is required field and can not be zero.");
               return "";
            }
            }
        }
        
    var systemDefaultVat="";
	var idacc="";
	if(document.forms['payableProcessing'].payVatDescrList!=undefined){
    if(document.forms['payableProcessing'].payVatDescrList.size!=0){
	      for (i=0; i<document.forms['payableProcessing'].payVatDescrList.length; i++){		           
	    	  idacc=document.forms['payableProcessing'].payVatDescrList[i].id;
	    	  idacc=idacc.replace(idacc.substring(0,3),'');
	        var payVatDescr=document.getElementById('des'+idacc).value;
	        var expenceVat=0;
	        var idVat=""
	        try{
	        idVat=document.getElementById(idacc).value;
	        }catch(e){
	        idVat='No'
	        } 
	        if(idVat=='No'){
	         expenceVat =document.getElementById('exp'+idacc).value;
	        }else{
	        expenceVat =document.getElementById(idacc).value;
	        }
	        var numberVat=document.getElementById('num'+idacc).value;
		        if(payVatDescr=="" && expenceVat>0){
			    	 alert("Please select VAT Desc for line # "+numberVat); 
			    	 systemDefaultVat="notok"
		        }
	    }
	    }}

	if(systemDefaultVat==""){      
     var payingStatusListServer="";
     var invoiceNumberListServer="";
     var actualExpenseListServer="";
     var payVatDescrListServer ="";
     var payVatPerListServer ="";
     var payVatAmtListServer ="";
     var actgCodeListServer ="";     
     var id=""
    if(document.forms['payableProcessing'].payingStatusList!=undefined){
       if(document.forms['payableProcessing'].payingStatusList.size!=0){
	      for (i=0; i<document.forms['payableProcessing'].payingStatusList.length; i++){
	    	  id=document.forms['payableProcessing'].payingStatusList[i].id;
          	    id=id.replace(id.substring(0,3),'');
                 if(payingStatusListServer==''){
	            payingStatusListServer=id+": "+document.forms['payableProcessing'].payingStatusList[i].value;
	             }else{
	                  payingStatusListServer= payingStatusListServer+"#"+id+": "+document.forms['payableProcessing'].payingStatusList[i].value;
	             }
	       }	
	     }else{	       
	           payingStatusListServer=document.forms['payableProcessing'].payingStatusList.id+": "+document.forms['payableProcessing'].payingStatusList.value;
	         }	        
	     }
     if(document.forms['payableProcessing'].actgCodeList!=undefined){
         if(document.forms['payableProcessing'].actgCodeList.length!=undefined){
  	      for (i=0; i<document.forms['payableProcessing'].actgCodeList.length; i++){	        	           
  	             	   id=document.forms['payableProcessing'].actgCodeList[i].id;
  	                   id=id.replace(id.substring(0,3),'');
  	             if(actgCodeListServer==''){
  	            	 actgCodeListServer=id+": "+document.forms['payableProcessing'].actgCodeList[i].value;
  	                 }else{
  	                	 actgCodeListServer= actgCodeListServer+"#"+id+": "+document.forms['payableProcessing'].actgCodeList[i].value;
  	                   }
  	       }	
  	     }else{	   
  	           id=document.forms['payableProcessing'].actgCodeList.id;
  	           id=id.replace(id.substring(0,3),'');     
  	           actgCodeListServer=id+": "+document.forms['payableProcessing'].actgCodeList.value;
  	         }	        
  	     } 
	   
	  if(document.forms['payableProcessing'].invoiceNumberList!=undefined){
       if(document.forms['payableProcessing'].invoiceNumberList.length!=undefined){
	      for (i=0; i<document.forms['payableProcessing'].invoiceNumberList.length; i++){	        	           
	             	   id=document.forms['payableProcessing'].invoiceNumberList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             if(invoiceNumberListServer==''){
	                   invoiceNumberListServer=id+": "+document.forms['payableProcessing'].invoiceNumberList[i].value;
	                 }else{
	                  invoiceNumberListServer= invoiceNumberListServer+"#"+id+": "+document.forms['payableProcessing'].invoiceNumberList[i].value;
	                   }
	       }	
	     }else{	   
	           id=document.forms['payableProcessing'].invoiceNumberList.id;
	          id=id.replace(id.substring(0,3),'');     
	           invoiceNumberListServer=id+": "+document.forms['payableProcessing'].invoiceNumberList.value;
	         }	        
	     } 
	     
	     if(document.forms['payableProcessing'].actualExpenseList!=undefined){
	    	 var currencyArrayLine=document.forms['payableProcessing'].currencyList;
	    	 var exchangeRateArrayLine=document.forms['payableProcessing'].exchangeRateList; 
       if(document.forms['payableProcessing'].actualExpenseList.length!=undefined){
	      for (i=0; i<document.forms['payableProcessing'].actualExpenseList.length; i++){		           
	             	   id=document.forms['payableProcessing'].actualExpenseList[i].id;
		  	           id=id.replace(id.substring(0,3),'');
		  	          var currency="NO";
		  	          if(currencyArrayLine[i].value.trim()!=''){currency=currencyArrayLine[i].value}
	                 if(actualExpenseListServer==''){
	                	 actualExpenseListServer=id+": "+document.forms['payableProcessing'].actualExpenseList[i].value+": "+exchangeRateArrayLine[i].value+": "+currency;
	                 }else{
	                  actualExpenseListServer= actualExpenseListServer+"#"+id+": "+document.forms['payableProcessing'].actualExpenseList[i].value+": "+exchangeRateArrayLine[i].value+": "+currency;
	                   }
	       }	
	     }else{	   
	           id=document.forms['payableProcessing'].actualExpenseList.id;
	           id=id.replace(id.substring(0,3),'');        
	           actualExpenseListServer=id+": "+document.forms['payableProcessing'].actualExpenseList.value+": "+document.forms['payableProcessing'].exchangeRateList.value+": "+document.forms['payableProcessing'].currencyList.value;
	         }	        
	     }


	     
	 <c:if test="${systemDefaultVatCalculation=='true'}"> 
       if(document.forms['payableProcessing'].payVatDescrList!=undefined){
       if(document.forms['payableProcessing'].payVatDescrList.size!=0){
	         	      for (i=0; i<document.forms['payableProcessing'].payVatDescrList.length; i++){		           
	         	    	 id=document.forms['payableProcessing'].payVatDescrList[i].id;
	        	    	  id=id.replace(id.substring(0,3),'');
	             if(payVatDescrListServer==''){
	                   payVatDescrListServer=id+": "+document.forms['payableProcessing'].payVatDescrList[i].value;
	                 }else{
	                  payVatDescrListServer= payVatDescrListServer+"#"+id+": "+document.forms['payableProcessing'].payVatDescrList[i].value;
	                   }
	       }	
	     }else{	   
	           id=document.forms['payableProcessing'].payVatDescrList.id;
	           id=id.replace(id.substring(0,3),'');     
	           payVatDescrListServer=id+": "+document.forms['payableProcessing'].payVatDescrList.value;
	         }	        
	     } 
	   if(document.forms['payableProcessing'].payVatPerList!=undefined){
       if(document.forms['payableProcessing'].payVatPerList.length!=undefined){
	      for (i=0; i<document.forms['payableProcessing'].payVatPerList.length; i++){	        	           
	             	   id=document.forms['payableProcessing'].payVatPerList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             if(payVatPerListServer==''){
	                   payVatPerListServer=id+": "+document.forms['payableProcessing'].payVatPerList[i].value;
	                 }else{
	                  payVatPerListServer= payVatPerListServer+"#"+id+": "+document.forms['payableProcessing'].payVatPerList[i].value;
	                   }
	       }	
	     }else{	   
	           id=document.forms['payableProcessing'].payVatPerList.id;
	           id=id.replace(id.substring(0,3),'');     
	           payVatPerListServer=id+": "+document.forms['payableProcessing'].payVatPerList.value;
	         }	        
	     } 
	   if(document.forms['payableProcessing'].payVatAmtList!=undefined){
       if(document.forms['payableProcessing'].payVatAmtList.length!=undefined){
	      for (i=0; i<document.forms['payableProcessing'].payVatAmtList.length; i++){	        	           
	             	   id=document.forms['payableProcessing'].payVatAmtList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             if(payVatAmtListServer==''){
	                   payVatAmtListServer=id+": "+document.forms['payableProcessing'].payVatAmtList[i].value;
	                 }else{
	                  payVatAmtListServer= payVatAmtListServer+"#"+id+": "+document.forms['payableProcessing'].payVatAmtList[i].value;
	                   }
	       }	
	     }else{	   
	           id=document.forms['payableProcessing'].payVatAmtList.id;
	           id=id.replace(id.substring(0,3),'');     
	           payVatAmtListServer=id+": "+document.forms['payableProcessing'].payVatAmtList.value;
	         }	        
	     } 
	 </c:if>
	     
	 document.getElementById('payVatDescrListServer').value=payVatDescrListServer;
	 document.getElementById('payVatAmtListServer').value=payVatAmtListServer;  
	 document.getElementById('payVatPerListServer').value=payVatPerListServer;  
     document.getElementById('payingStatusListServer').value=payingStatusListServer;
     document.getElementById('invoiceNumberListServer').value=invoiceNumberListServer;    
     document.getElementById('actualExpenseListServer').value=actualExpenseListServer;
     document.getElementById('actgCodeListServer').value=actgCodeListServer;
     
     
     <c:if test="${systemDefaultVatCalculation!='true'}">
     var accountlinelist="";
     if(newlineid!=''){
            var arrayLine=newlineid.split("#");
            for(var i=0;i<arrayLine.length;i++){              
                if(accountlinelist==''){
                  accountlinelist=document.getElementById('seq'+arrayLine[i]).value+" :"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('cha'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('rev'+arrayLine[i]).value+" :"+document.getElementById('inv'+arrayLine[i]).value+" :"+document.getElementById('exp'+arrayLine[i]).value+" :"+document.getElementById('sta'+arrayLine[i]).value+" :"+document.getElementById('pgl'+arrayLine[i]).value+" :"+document.getElementById('rgl'+arrayLine[i]).value+" :"+document.getElementById('bas'+arrayLine[i]).value+": "+document.getElementById('not'+arrayLine[i]).value+": "+document.getElementById('com'+arrayLine[i]).value+": "+document.getElementById('exchangeRate').value+": "+document.getElementById('cur'+arrayLine[i]).value;
                }else{
                  accountlinelist=accountlinelist+"#"+document.getElementById('seq'+arrayLine[i]).value+" :"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('cha'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('rev'+arrayLine[i]).value+" :"+document.getElementById('inv'+arrayLine[i]).value+" :"+document.getElementById('exp'+arrayLine[i]).value+" :"+document.getElementById('sta'+arrayLine[i]).value+" :"+document.getElementById('pgl'+arrayLine[i]).value+" :"+document.getElementById('rgl'+arrayLine[i]).value+" :"+document.getElementById('bas'+arrayLine[i]).value+": "+document.getElementById('not'+arrayLine[i]).value+": "+document.getElementById('com'+arrayLine[i]).value+": "+document.getElementById('exchangeRate').value+": "+document.getElementById('cur'+arrayLine[i]).value;
                }
              }
         }
     </c:if>
     <c:if test="${systemDefaultVatCalculation=='true'}">
      var accountlinelist="";
     if(newlineid!=''){
            var arrayLine=newlineid.split("#");
            for(var i=0;i<arrayLine.length;i++){              
                if(accountlinelist==''){
                  accountlinelist=document.getElementById('seq'+arrayLine[i]).value+" :"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('cha'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('rev'+arrayLine[i]).value+" :"+document.getElementById('inv'+arrayLine[i]).value+" :"+document.getElementById('exp'+arrayLine[i]).value+" :"+document.getElementById('sta'+arrayLine[i]).value+" :"+document.getElementById('pgl'+arrayLine[i]).value+" :"+document.getElementById('rgl'+arrayLine[i]).value+" :"+document.getElementById('bas'+arrayLine[i]).value+": "+document.getElementById('not'+arrayLine[i]).value+": "+document.getElementById('com'+arrayLine[i]).value+": "+document.getElementById('exchangeRate').value+": "+document.getElementById('cur'+arrayLine[i]).value +": "+document.getElementById('des'+arrayLine[i]).value +": "+document.getElementById('pvp'+arrayLine[i]).value +": "+document.getElementById('pva'+arrayLine[i]).value;
                }else{
                  accountlinelist=accountlinelist+"#"+document.getElementById('seq'+arrayLine[i]).value+" :"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('cha'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('rev'+arrayLine[i]).value+" :"+document.getElementById('inv'+arrayLine[i]).value+" :"+document.getElementById('exp'+arrayLine[i]).value+" :"+document.getElementById('sta'+arrayLine[i]).value+" :"+document.getElementById('pgl'+arrayLine[i]).value+" :"+document.getElementById('rgl'+arrayLine[i]).value+" :"+document.getElementById('bas'+arrayLine[i]).value+": "+document.getElementById('not'+arrayLine[i]).value+": "+document.getElementById('com'+arrayLine[i]).value+": "+document.getElementById('exchangeRate').value+": "+document.getElementById('cur'+arrayLine[i]).value +": "+document.getElementById('des'+arrayLine[i]).value +": "+document.getElementById('pvp'+arrayLine[i]).value +": "+document.getElementById('pva'+arrayLine[i]).value;
                }
              }
         }
     </c:if>   
       document.getElementById('accountlinelist').value =accountlinelist;
       document.forms['payableProcessing'].action="payableProcessingSave.html?savePayableProcessingFlag=yes";
       document.forms['payableProcessing'].submit();
   }
  }} }
  
function clearhiddenfields(){
      document.getElementById('newlineid').value="";      
  }
function cleartotalexpensebase(){
      document.getElementById('totalexpensebase').value="";
      document.getElementById('totalExpense').value="";
}  

function payPosting(){
	
	var tot=parseFloat(document.getElementById('totalExpense').value);
	var amt=parseFloat(document.getElementById('amount').value);		        
	if(amt<tot)
	{
		alert("Payable Invoice amount is not equal to entered amount.Please correct and try again.");
		return false;	
	}else{
		var checkdata=""
	  for (i=0; i<document.forms['payableProcessing'].actualExpenseList.length; i++){	        	           
         	   id=document.forms['payableProcessing'].actualExpenseList[i].id;
         	   var trunid=id.replace(id.substring(0,3),'');
         	   var expcheck=0; 
         	  expcheck=eval(document.forms['payableProcessing'].actualExpenseList[i].value); 
         	  if(expcheck!=0 && expcheck!=''&& document.getElementById('inv'+trunid).value==''){
       	     document.getElementById('inv'+trunid).value=document.getElementById('vendorInvoice').value;
         	    checkdata="notOk" 
         	  } 
       }
	   if(checkdata=="notOk" ){
       alert("Please save the data first before applying payable posting.");
	   }else{
		   if(document.forms['payableProcessing'].elements['saveCheck1'].value=='OK'){
			    var comDiv='Y';
			    try{
			    comDiv=document.forms['payableProcessing'].elements['companyDivision'].value;
			    document.forms['payableProcessing'].elements['cDivision'].value=comDiv;
			    }catch(e){   
			     }
			    if(comDiv=='Y'){
			     document.forms['payableProcessing'].elements['cDivision'].value='N';
			    }
			   var cDivision = document.forms['payableProcessing'].elements['cDivision'].value;
			    shipNumber=document.getElementById('shipNo').value;    
			       vendorCode=document.getElementById('vendorCode').value;
			       vendorInvoice=document.getElementById('vendorInvoice').value;
			       currency=document.getElementById('currency').value;      
			       openWindow('batchPayablePostDate.html?vendorCode='+vendorCode+'&vendorInvoice='+vendorInvoice+'&currency='+currency+'&shipNo='+shipNumber+'&cDivision='+cDivision+'&decorator=popup&popup=true',900,300);
			    }else{
			   alert("Please save the data first before applying payable posting.");
			     }
	   }
   	} 
   }
function enableOrdisablePaybutton(){
		var expence=parseFloat(document.getElementById('totalExpense').value);
		var amount=parseFloat(document.getElementById('amount').value);		 
 if(expence==amount){
	 document.forms['payableProcessing'].elements['payposting'].disabled=false;
	 document.forms['payableProcessing'].elements['addLine'].disabled=true;
	 //document.forms['payableProcessing'].elements['saveLine'].disabled=true;
    }else{
    	document.forms['payableProcessing'].elements['payposting'].disabled=true;
	   	document.forms['payableProcessing'].elements['addLine'].disabled=false;
	//	document.forms['payableProcessing'].elements['saveLine'].disabled=false;
    	
    }
}   
</script>

<script type="text/javascript">
 function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
  var http77 = getHTTPObject77();
  var http2=getHTTPObject77();
function winOpen(){
	//javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=defaultAccountLine.billToName&fld_code=vendorCode');
    javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=vendorCode');
 }
function openBookingAgentPopWindow(){
	openWindow("openPartnerPayableProcessing.html?partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=vendorCode",1024,500);
	}
function findExchangeEstRate(){
        var country =document.forms['payableProcessing'].elements['currency'].value; 
        var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
	    http77.open("GET", url, true);
	    http77.onreadystatechange = handleHttpResponseEstRate;
	    http77.send(null);
  }
 function handleHttpResponseEstRate() { 
             if (http77.readyState == 4) {
                var results = http77.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>1) {
                  document.forms['payableProcessing'].elements['exchangeRate'].value=results 
                  } else {
                  document.forms['payableProcessing'].elements['exchangeRate'].value=1;
                  document.forms['payableProcessing'].elements['exchangeRate'].readOnly=false; 
                  }                
             }   }

 function findBookingAgentCode(){    
	    var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
	    vendorId=vendorId.trim();  
		if(vendorId!='')
		{
	    var url="vendorCodeAndName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse4;
	    http2.send(null);
		}else{
			document.forms['payableProcessing'].elements['vendorDescription'].value='';
		}
	  }
	  function handleHttpResponse4()        {
	           if (http2.readyState == 4)             {
	                var results = http2.responseText
	                results = results.trim();
	                if(results.length>=2)                {
	                	  if(results!="NotAct")
	                	  {
		                	document.forms['payableProcessing'].elements['vendorDescription'].value=results;
	                	  }else{
	                           alert("This Vendor Code cannot be processed as the Actg Code for the partner is missing , please contact your accounting dept.");
	                           document.forms['payableProcessing'].elements['vendorCode'].value='';
	                           document.forms['payableProcessing'].elements['vendorDescription'].value='';
	                          }
	 				}
	                 else                 {
	                     alert("Invalid vendor code, please select another");
	                     document.forms['payableProcessing'].elements['vendorCode'].value='';
	                     document.forms['payableProcessing'].elements['vendorDescription'].value='';
	                 }             }    }  
 

	function checkActgCode(result){   
	 if(result.value!=''){
	     var resultvalue=result.id; 
	      var result2=resultvalue.replace(resultvalue.substring(0,3),'');
         <c:if test="${accountInterface=='Y'}"> 
     var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
	 var companyDivision=document.getElementById('com'+result2).value;   
     vendorId=vendorId.trim();
     companyDivision=companyDivision.trim();    
    if(companyDivision!='')
    {  
    if(vendorId!=''){       
	     var url=""
	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		    try{
		    	actgCodeFlag=document.forms['payableProcessing'].elements['actgCodeFlag'].value;
		    }catch(e){} 
	     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);
	    </c:if>
	    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
	     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
	    </c:if>
        httpvandor.open("GET", url, true);
        httpvandor.onreadystatechange =function(){ handleHttpResponse222(result2);};
	    httpvandor.send(null);
    }else{
        alert("Please enter vendor code first.");
        document.forms['payableProcessing'].elements['vendorCode'].focus();
        document.getElementById('exp'+temp).value='';
        calculatetotalActualExpense();
        return false;
    }
    }
    </c:if>
	 } 
	 return true;              
  }
    function handleHttpResponse222(temp){
		if (httpvandor.readyState == 4){
                var results = httpvandor.responseText
                results = results.trim();  
                var res = results.split("#"); 
           	 var companyDivision=document.getElementById('com'+temp).value;   
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){		                     
		                   if(res[2] != "null" && res[2] !=''){ 
		                	   document.getElementById('act'+temp).value=res[2];  
		                	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		                	    try{
		                	   if(res[8] != "null" && res[8].trim() !=''){
			                		document.getElementById('com'+temp).value=res[8].trim();
			                	}}catch(e){} 
			                	</c:if>
				            } else if (res[2] == "null" || res[2] =='') {
				            	document.getElementById('exp'+temp).value='0.00';
					            document.getElementById('act'+temp).value='';
				              <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
				              alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing for company division "+companyDivision+", please contact your accounting dept.");
				              </c:if>
				              <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}">
				              alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept."); 
				              </c:if>				            		
				              calculatetotalActualExpense();
				              return false;					  
				            } 		              	
				        }else{
				            alert("Partner code is not approved" );
			            	document.getElementById('exp'+temp).value='0.00';
				            document.getElementById('act'+temp).value=''; 
				            calculatetotalActualExpense();
				            return false; 						 
				        }
		            }else{
		                alert("This Partner Code cannot be processed as the Accounting code for the partner is missing for company division "+companyDivision+", please contact your accounting dept." ); 						
		            	document.getElementById('exp'+temp).value='0.00';
			            document.getElementById('act'+temp).value=''; 
		                calculatetotalActualExpense();
		                return false;
					}  } }
    function checkMandatoryAddLineField(){
	   var newlineid=document.getElementById('newlineid').value;
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("#"); 
	        for(var i=0;i<arrayLine.length;i++){
	   		 var category=document.getElementById('cat'+arrayLine[i]).value;
			 var chargeCode=document.getElementById('cha'+arrayLine[i]).value;																
			 var companyDivision=document.getElementById('com'+arrayLine[i]).value;
			 var payGLCode=document.getElementById('pgl'+arrayLine[i]).value;
		       if(category==""){
			    	 alert("Please enter catagory for line # "+arrayLine[i]);
			    	 return false;
		        }
		       if(chargeCode==""){
			    	 alert("Please enter charge code for line # "+arrayLine[i]);
			    	 return false;
		        }
			   if(companyDivision==""){
			    	 alert("Please select company division for line # "+arrayLine[i]);
			    	 return false;
		        }
				if(payGLCode.trim()=="") {
		        	 alert("Payable GL Code missing. Please select another Charge code for line # "+arrayLine[i]);
		        	 document.getElementById('cha'+arrayLine[i]).value="";
		        	 document.getElementById('cha'+arrayLine[i]).focus();
		        	 return false;
		        }
		       <c:if test="${systemDefaultVatCalculation=='true'}">
		        var payVatDescr=document.getElementById('des'+arrayLine[i]).value;
		        if(payVatDescr==""){
			    	 alert("Please select VAT Desc for line # "+arrayLine[i]);
			    	 return false;
		        }
		        </c:if>
          }
		        return true;
    }else{
    	 return true;
    }
	    }
	        
   function checkMandatoryField(){
	     var mandatoryField=""
	     var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
	     var amount=document.forms['payableProcessing'].elements['amount'].value;
	     var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
	     var invoiceDate=document.forms['payableProcessing'].elements['invoiceDate'].value;	     
			if(vandorCode.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		    	 document.forms['payableProcessing'].elements['vendorCode'].focus();
		    	 return false;
	       }else if(vendorInvoice.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		       	 document.forms['payableProcessing'].elements['vendorInvoice'].focus();
		    	 return false;
		     }else if(invoiceDate.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		          	 document.forms['payableProcessing'].elements['invoiceDate'].focus();
		        	 return false;
		     }else if(amount.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		            document.forms['payableProcessing'].elements['amount'].focus();
		            return false;
		     }else {
		           return feedCompanyDivision();
		      }
	  } 
   function feedCompanyDivision(){
		  var comDiv='Y';
		  try{
		  comDiv=document.forms['payableProcessing'].elements['companyDivision'].value;
		  document.forms['payableProcessing'].elements['cDivision'].value=comDiv;
		  }catch(e){			
			  }
		  if(comDiv=='Y'){
			  document.forms['payableProcessing'].elements['cDivision'].value='N';
		  }
		return true;		  
	  } 
  function previewInvoicelines(){
	  document.forms['payableProcessing'].elements['fieldName'].value="";
	  document.forms['payableProcessing'].elements['sortType'].value="";
      document.forms['payableProcessing'].action='previewInvoicelines.html';
      var flag=checkMandatoryField();
      if(flag==true){
          document.forms['payableProcessing'].submit();
       }
  }  
  function checkEmptyValue(result){
    if(result.value.trim()==''){
     alert();
    }
  }
  function clear_fields(){
		document.forms['payableProcessing'].elements['vendorCode'].value = "";
		document.forms['payableProcessing'].elements['vendorDescription'].value = "";		
		document.forms['payableProcessing'].elements['vendorInvoice'].value = "";
		document.forms['payableProcessing'].elements['invoiceDate'].value = "";
		document.forms['payableProcessing'].elements['recievedDate'].value = "";
		document.forms['payableProcessing'].elements['amount'].value = "";
}
	function numbersonly(myfield, e)
	{
		var key;
		var keychar;

		var val = myfield.value;
				if(val.indexOf(".")<0)
				{
				count=0;
				}
	    
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
	
			keychar = String.fromCharCode(key);
	
		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		   return true;
		else if ((("0123456789").indexOf(keychar) > -1))
		   return true;
		else if ((count<1)&&(keychar == "."))
		{
		   count++;
		   return true;
		}
		else if ((count>0)&&(keychar == "."))
		{
		   return false;
		}else
		   return false;
	}
	function onlyDelNoQuotes(evt,targetElement) 
	{
   		var keyCode = evt.which ? evt.which : evt.keyCode;
	  if(keyCode==46 || keyCode==8){
	  		targetElement.value = '';
	  }else{
	 return false;
	  	
	  }
   }

	   function poulateActualExpense(){		   
		   document.forms['payableProcessing'].elements['totalExpense'].value=document.forms['totalExpenseForm'].elements['totExpValue'].value;
		   }
  
</script>
<script type="text/javascript">
    function sortPayableProcessingList(temp) { 
    	var fieldName=  document.forms['payableProcessing'].elements['fieldName'].value;
    	var sortType=document.forms['payableProcessing'].elements['sortType'].value;
    	if(fieldName.trim()=='') {
    		document.forms['payableProcessing'].elements['sortType'].value='asc';
    	}else{
    		if(fieldName.trim()==temp )
    		{
        	if(sortType.trim()=='asc'){
    			document.forms['payableProcessing'].elements['sortType'].value='desc';
        	}else{
    			document.forms['payableProcessing'].elements['sortType'].value='asc';        		
    		}
    		}
    		else{
    			document.forms['payableProcessing'].elements['sortType'].value='asc';        		
    		}
    	}
    document.forms['payableProcessing'].elements['fieldName'].value=temp;  	
    document.forms['payableProcessing'].action ='previewInvoicelines.html';
    var check =saveValidation();
 	if(check){
    document.forms['payableProcessing'].submit();
    } 
    }
    function saveValidation(){
	     var mandatoryField=""
		     var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;		   
		     var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
		     var currency=document.forms['payableProcessing'].elements['currency'].value;	     
				if(vandorCode.trim()==''){
			    	 alert("Please enter all the mandatory fields.");
			    	 document.forms['payableProcessing'].elements['vendorCode'].focus();
			    	 return false;
		       }else if(vendorInvoice.trim()==''){
			    	 alert("Please enter all the mandatory fields.");
			       	 document.forms['payableProcessing'].elements['vendorInvoice'].focus();
			    	 return false;
			     }else if(currency.trim()==''){
			    	 alert("Please enter all the mandatory fields.");
			          	 document.forms['payableProcessing'].elements['currency'].focus();
			        	 return false;
			     }else {
			           return true;
			      }
    	 }     
</script>
<script type="text/javascript">
function autoPopulate_recievedDate() {
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);	
	document.forms['payableProcessing'].elements['recievedDate'].value=datam;
}
function checkCurrency(result){
    var resultvalue=result.id; 
    var result2=resultvalue.replace(resultvalue.substring(0,3),'');
var selectCurrency =document.getElementById('cur'+result2).value;
var oldCurrency =document.getElementById('ocr'+result2).value;
var currency =document.forms['payableProcessing'].elements['currency'].value;
var currencyValue = document.forms['payableProcessing'].elements['currency'].options[document.forms['payableProcessing'].elements['currency'].selectedIndex].text;
//Old or new update by above exchangeRate on action onChange
var exchangeRate =document.forms['payableProcessing'].elements['exchangeRate'].value;
var oldexchangeRate =document.getElementById('oxr'+result2).value;
document.getElementById('exr'+result2).value=exchangeRate;
if(currency!=selectCurrency)
{	
	 alert('You can select only '+currencyValue+' currency.');
	 document.getElementById('cur'+result2).value=oldCurrency;
	 document.getElementById('exr'+result2).value=oldexchangeRate;
	 return false;
}
if(currency==selectCurrency)
{
calculatePayVatAmt(result2)
}
return true; 
}
function getPayPercent(result){ 
<c:if test="${systemDefaultVatCalculation=='true'}">
    var resultvalue=result.id; 
    var result2=resultvalue.replace(resultvalue.substring(0,3),'');  
    var relo='';
	<c:forEach var="entry" items="${payVatPercentList}" > 	
	if(relo==''){
	  if(document.getElementById(resultvalue).value==${entry.key}){
	      document.getElementById('pvp'+result2).value='${entry.value}';
	      calculatePayVatAmt(result2);
	      relo="yes";
	      }   }
	     </c:forEach>	     
	     if(document.getElementById(resultvalue).value==''){	    	 
            document.getElementById('pvp'+result2).value='0.00';
            calculatePayVatAmt(result2);
      }  
  
</c:if>
}

function calculatePayVatAmt(result){
   <c:if test="${systemDefaultVatCalculation=='true'}"> 
    var result2= result;
    var payVatAmt=0;       
    var actualExpenseCheck= document.getElementById('exp'+result2).value; 
    actualExpenseCheck=actualExpenseCheck.trim();
    if(actualExpenseCheck!=""){
    var actualExpense= eval(document.getElementById('exp'+result2).value); 
	var exchangeRate=1.00;
	var amount=0;
	exchangeRate= eval(document.getElementById('exr'+result2).value); 
	amount=actualExpense/exchangeRate;
	amount=Math.round(amount*100)/100; 
	document.getElementById('aea'+result2).value=amount;
	if(document.getElementById('pvp'+result2).value!=''){
	var payVatPercent=eval(document.getElementById('pvp'+result2).value);
	var contractType= document.getElementById('cnt'+result2).value;
	if(contractType.trim()=='Y'){ 
		var amountCon=0;
		amountCon=eval(document.getElementById('exp'+result2).value);
		payVatAmt=(amountCon*payVatPercent)/100;
		payVatAmt=Math.round(payVatAmt*100)/100;	
	}else{
		payVatAmt=(amount*payVatPercent)/100;
		payVatAmt=Math.round(payVatAmt*100)/100;		
	}
	document.getElementById('pva'+result2).value=payVatAmt;
	}}
	</c:if>
	}
	
function getPayPercentNew(result){ 
   <c:if test="${systemDefaultVatCalculation=='true'}">
    var resultvalue=result.id; 
    var result2=resultvalue.replace(resultvalue.substring(0,3),''); 
    var relo='';
	<c:forEach var="entry" items="${payVatPercentList}" > 
	if(relo==''){
	  if(document.getElementById(resultvalue).value==${entry.key}){
	      document.getElementById('pvp'+result2).value='${entry.value}';
	      calculatePayVatAmtNew(result2);
	      relo="yes";
	      }   }
	     </c:forEach>
	     if(document.getElementById(resultvalue).value==''){
            document.getElementById('pvp'+result2).value='0.00';
            calculatePayVatAmtNew(result2);
      }  
  
</c:if>
}
	
	function calculatePayVatAmtNew(result){
   <c:if test="${systemDefaultVatCalculation=='true'}"> 
    var result2= result;
    var payVatAmt=0;
    var actualExpenseCheck= document.getElementById('exp'+result2).value;
    actualExpenseCheck=actualExpenseCheck.trim();
    if(actualExpenseCheck!=''){
    var actualExpense= eval(document.getElementById('exp'+result2).value);
	var exchangeRate=1.00;
	var amount=0;
	exchangeRate= eval(document.getElementById('exr'+result2).value);
	amount=actualExpense/exchangeRate;
	amount=Math.round(amount*100)/100; 
	document.getElementById('aea'+result2).value=amount;
	if(document.getElementById('pvp'+result2).value!=''){
	var payVatPercent=eval(document.getElementById('pvp'+result2).value); 
	var contractType= document.getElementById('cnt'+result2).value;
	if(contractType.trim()=='Y'){ 
		var amountCon=0;
		amountCon=eval(document.getElementById('exp'+result2).value);
		payVatAmt=(amountCon*payVatPercent)/100;
		payVatAmt=Math.round(payVatAmt*100)/100;	
	}else{
		payVatAmt=(amount*payVatPercent)/100;
		payVatAmt=Math.round(payVatAmt*100)/100;		
	}
	document.getElementById('pva'+result2).value=payVatAmt;
	}}
	</c:if>
	}


function onlyFloat1(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value="0.00";
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}
</script>
<meta name="heading" content="Payable Processing" />
</head>
<s:form name="payableProcessing" id="payableProcessing" action="payableProcessingSave.html" method="post">
	<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
		<s:hidden name="actgCodeFlag" value="YES" />
	</configByCorp:fieldVisibility>
 <input type="hidden" name="name" value="1" id="name"/>
 <s:hidden name="fieldName" value="${fieldName}"/>
 <s:hidden name="sortType" value="${sortType}"/>
  <s:hidden name="secondDescription" />
  <s:hidden name="description" />
   <s:hidden name="savePayableProcessingFlag1" value="<%=request.getParameter("savePayableProcessingFlag")%>"/> 
  <c:set var="savePayableProcessingFlag1" value="<%=request.getParameter("savePayableProcessingFlag")%>"></c:set>
   <s:hidden name="saveCheck1" value="<%=request.getParameter("saveCheck")%>"/> 
  <c:set var="saveCheck1" value="<%=request.getParameter("saveCheck")%>"></c:set>  
  
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
	<s:hidden name="cDivision"/>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
 <div id="Layer1" style="width:100%;">
 <div id="otabs">
		  <ul>		  
		    <li><a class="current"><span>Payable Processing</span></a></li>
		  </ul>
		</div>
	<div class="spnblk" >&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
      <table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" width="900px">
				<tbody>
					<tr>
					 <td align="right" class="listwhitebox">Vendor Code<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorCode" id="vendorCode"  cssClass="input-text"  size="10" onchange="findBookingAgentCode();"/>
					  <img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="javascript:openBookingAgentPopWindow()" src="<c:url value='/images/open-popup.gif'/>" /></td>
					 <td align="right" class="listwhitebox">Descripton</td>					 
					 <td><s:textfield name="vendorDescription" cssClass="input-textUpper"  size="35" onchange="" readonly="true"/></td>					 
					 <td align="right" class="listwhitebox">Vendor Invoice#<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorInvoice"  id="vendorInvoice" cssClass="input-text"  size="10" maxlength="17"/></td>
					  <td align="right" class="listwhitebox">Invoice Date<font color="red" size="2">*</font></td>
					  <td>
					   <c:if test="${not empty invoiceDate}">
								<s:text id="invoiceDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="invoiceDate"/></s:text>												
								<s:textfield id="invoiceDateText" name="invoiceDate" value="%{invoiceDateFormattedValue}" cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					            <img id="invoiceDateText-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
						</c:if>
					  
					  <c:if test="${empty invoiceDate}">
					  <s:textfield id="invoiceDateText" name="invoiceDate" cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					  <img id="invoiceDateText-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
					  </c:if>
					  </td>
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Currency</td>
					 <td><s:select name="currency" list="%{country}" id="currency" cssClass="list-menu"  cssStyle="width:74px" onchange="findExchangeEstRate()" headerKey="" headerValue=""/></td>
					 <td align="right" class="listwhitebox">Exchange Rate</td>
					 <td><s:textfield name="exchangeRate" cssClass="input-text" id="exchangeRate"  size="10" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event,this)" cssStyle="text-align:right;" /></td>
					  <td align="right" class="listwhitebox">Received Date</td>
					 <td>
					                            <c:if test="${not empty recievedDate}">
												<s:text id="recievedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="recievedDate"/></s:text>												
												<s:textfield id="recievedDateText" name="recievedDate"  value="%{recievedDateFormattedValue}" cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					                            <img id="recievedDateText-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
												</c:if>
					    <c:if test="${empty recievedDate}">
					    <s:textfield id="recievedDateText" name="recievedDate" cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					    <img id="recievedDateText-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
					    </c:if>
					  </td>
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Amount<font color="red" size="2">*</font></td>
					 <td><s:textfield name="amount"  id="amount" cssClass="input-text"  maxlength="12" size="10" onchange="return onlyFloat(this);" onblur="checkFloat(this);"/></td>					
					 <td align="right" class="listwhitebox">Service Order</td>
					 <td><s:textfield name="shipNo"  id="shipNo"  cssClass="input-text" cssStyle="width:104px" maxlength="25" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>					
					<configByCorp:fieldVisibility componentId="component.field.batchPayable.companyDivision">
					<td align="right" class="listwhitetext" style="">Company&nbsp;Division</td>
					<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="companyDivision" list="%{companyDivis}"  cssStyle="width:80px" headerKey="" headerValue=""  /></td>
					</configByCorp:fieldVisibility>
					</tr>
					<tr>
					 <td align="right"  height="10"></td>				
					</tr>
				</tbody>
	  </table>    
    </div>
<div class="bottom-header" style="margin-top:31px;"><span></span></div>
</div>
<input type="button"  Class="cssbutton" Style="width:155px; height:25px;margin-left:35px;margin-top:10px;" value="Preview Invoice Lines"  onclick="previewInvoicelines();"/>
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();poulateActualExpense();"/>
</div>
	</div>
	
	
 
 <!--
 <s:hidden name="vendorInvoice" id="vendorInvoice" value="${vendorInvoice}"/>
 <s:hidden name="amount" id="amount" value='${amount}'/>
 <s:hidden name="vendorCode" id="vendorCode" value='${vendorCode}'/>
 <s:hidden name="currency" id="currency" value='${currency}'/>
 <s:hidden name="exchangeRate" id="exchangeRate" value='${exchangeRate}'/>
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <c:if test="${not empty invoiceDate}">
  <s:text id="invoiceDateFormattedValue" name="${FormDateValue}">
 <s:param name="value" value="invoiceDate" /></s:text>
 <s:hidden  id="invoiceDate" name="invoiceDate" value="%{invoiceDateFormattedValue}" />
 </c:if>
 <c:if test="${ empty invoiceDate}">
 <s:hidden  id="invoiceDate" name="invoiceDate"  />
 </c:if>
 <c:if test="${not empty recievedDate}">
 <s:text id="recievedDateFormattedValue" name="${FormDateValue}">
 <s:param name="value" value="recievedDate" /></s:text>
 <s:hidden   id="recievedDate" name="recievedDate" value="%{recievedDateFormattedValue}"  onkeydown="return onlyDel(event,this)"/>
 </c:if>
 
 <c:if test="${empty recievedDate}">
  <s:hidden   id="recievedDate" name="recievedDate" />
 </c:if> 
  
  
  -->
  
  <s:hidden name="invoiceNumberListServer" id="invoiceNumberListServer" value=""/>
  <s:hidden name="actualExpenseListServer" id="actualExpenseListServer" value=""/>
  <s:hidden name="payingStatusListServer" id="payingStatusListServer" value=""/>
  <s:hidden name="payVatDescrListServer" id="payVatDescrListServer" value=""/>
  <s:hidden name="payVatPerListServer" id="payVatPerListServer" value=""/>
  <s:hidden name="payVatAmtListServer" id="payVatAmtListServer" value=""/>
  <s:hidden name="actgCodeListServer" id="actgCodeListServer" value=""/>
  
  
  <s:hidden name="newlineid" id="newlineid" value=""/>
  <s:hidden name="accountlinelist" id="accountlinelist" value=""/>
  <s:hidden name="totalexpensebase" id="totalexpensebase"/>
     <s:hidden name="secondDescription" />
     <s:hidden name="description" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
	<script type="text/javascript">
		    cleartotalexpensebase();
		 </script>
		 
      <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Payable Processing</span></a></li>		    
		  </ul>
		</div>
		<div class="spn" style="line-height:0px;">&nbsp;</div>
		<div id="para1" style="clear:both;">
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showFrzeeHeader">	
		
		<div id="tablewrap" style="overflow-x:auto; height:auto">
		<div id="headwrap" style="width:100%; height:auto">
		 <table class="tbl1" style="">
		 <thead>
		 <tr>
		 <th width="" class="thid">#</th>		 		 
		 <c:if test="${fieldName!='a.shipNumber' || fieldName==''}">
		 <th width="" class="thso"><a href="#" onclick="sortPayableProcessingList('a.shipNumber');">SO#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.shipNumber'}">
		 <th width="" class="thso"><a href="#" onclick="sortPayableProcessingList('a.shipNumber');"><font color="black">SO#</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='s.firstName,s.lastName' || fieldName==''}">
		 <th width="" class="thship"><a href="#" onclick="sortPayableProcessingList('s.firstName');">Shipper</a></th>
		 </c:if>
		 <c:if test="${fieldName=='s.firstName,s.lastName'}">		 
		 <th width="" class="thship"><a href="#" onclick="sortPayableProcessingList('s.firstName');"><font color="black">Shipper</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.category' || fieldName==''}">
		<th width="" class="thcat"><a href="#" onclick="sortPayableProcessingList('a.category');">Category<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.category'}">
		 <th width="" class="thcat"><a href="#" onclick="sortPayableProcessingList('a.category');"><font color="black">Category</font><font color="red" size="2">*</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.accountLineNumber' || fieldName==''}">
		 <th width="" class="thline"><a href="#" onclick="sortPayableProcessingList('a.accountLineNumber');">Line<br>#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.accountLineNumber'}">
		 <th width="" class="thline"><a href="#" onclick="sortPayableProcessingList('a.accountLineNumber');"><font color="black">Line<br>#</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.chargeCode' || fieldName==''}">
		 <th width="" class="thccode"><a href="#" onclick="sortPayableProcessingList('a.chargeCode');">Charge Code<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.chargeCode'}">
		 <th width="" class="thccode"><a href="#" onclick="sortPayableProcessingList('a.chargeCode');"><font color="black">Charge Code</font><font color="red" size="2">*</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.companyDivision' || fieldName==''}">
		 <th width="" class="thcdiv"><a href="#" onclick="sortPayableProcessingList('a.companyDivision');">Company Division<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.companyDivision'}">		 
		 <th width="" class="thcdiv"><a href="#" onclick="sortPayableProcessingList('a.companyDivision');"><font color="black">Company Division</font><font color="red" size="2">*</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.note' || fieldName==''}">
		 <th width="" class="thnote"><a href="#" onclick="sortPayableProcessingList('a.note');">Note</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.note'}">
		 <th width="" class="thnote"><a href="#" onclick="sortPayableProcessingList('a.note');"><font color="black">Note</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.basis' || fieldName==''}">
		 <th width="" class="thbasis"><a href="#" onclick="sortPayableProcessingList('a.basis');">Basis</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.basis'}">
		 <th width="" class="thbasis"><a href="#" onclick="sortPayableProcessingList('a.basis');"><font color="black">Basis</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.estimateExpense' || fieldName==''}">
	     <th width="" class="thestpay"><a href="#" onclick="sortPayableProcessingList('a.estimateExpense');">Est Pay Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.estimateExpense'}">
		 <th width="" class="thestpay"><a href="#" onclick="sortPayableProcessingList('a.estimateExpense');"><font color="black">Est Pay Amount</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.revisionExpense' || fieldName==''}">
		 <th width="" class="threvpay"><a href="#" onclick="sortPayableProcessingList('a.revisionExpense');">Revised Pay Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.revisionExpense'}">		 
		 <th width="" class="threvpay"><a href="#" onclick="sortPayableProcessingList('a.revisionExpense');"><font color="black">Revised Pay Amount</font></a></th>		 		 
		 </c:if>
 
       <c:if test="${systemDefaultVatCalculation!='true'}">
		 <c:if test="${fieldName!='a.localAmount' || fieldName==''}">
		 <th width="" class="thactexp"><a href="#" onclick="sortPayableProcessingList('a.localAmount');" >Actual Expense</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.localAmount'}">
		 <th width="" class="thactexp"><a href="#" onclick="sortPayableProcessingList('a.localAmount');"><font color="black">Actual Expense</font></a></th>		 
		 </c:if>
		 </c:if>
		<c:if test="${systemDefaultVatCalculation=='true'}">
		<c:if test="${fieldName!='a.localAmount' || fieldName==''}">
		 <th width="" class="thlocamt"><a href="#" onclick="sortPayableProcessingList('a.localAmount');" >Local Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.localAmount'}">
		 <th width="" class="thlocamt"><a href="#" onclick="sortPayableProcessingList('a.localAmount');"><font color="black">Local Amount</font></a></th>		 
		 </c:if>
		</c:if>
		 
		 <c:if test="${fieldName!='a.country' || fieldName==''}">
		 <th width="" class="thcur"><a href="#" onclick="sortPayableProcessingList('a.country');">Currency</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.country'}">		 
		 <th width="" class="thcur"><a href="#" onclick="sortPayableProcessingList('a.country');"><font color="black">Currency</font></a></th>		 
		 </c:if>
		 

		 <c:if test="${fieldName!='a.invoiceNumber' || fieldName==''}">
		 <th width="" class="thinv"><a href="#" onclick="sortPayableProcessingList('a.invoiceNumber');">Invoice#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.invoiceNumber'}">
		 <th width="" class="thinv"><a href="#" onclick="sortPayableProcessingList('a.invoiceNumber');"><font color="black">Invoice#</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.payingStatus' || fieldName==''}">
	     <th width="" class="thstat"><a href="#" onclick="sortPayableProcessingList('a.payingStatus');">Status</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payingStatus'}">
		 <th width="" class="thstat"><a href="#" onclick="sortPayableProcessingList('a.payingStatus');"><font color="black">Status</font></a></th>
		 </c:if>
         <c:if test="${accountInterface=='Y'}">
		 <c:if test="${fieldName!='a.actgCode' || fieldName==''}">
		 <th width="" class="thactcod"><a href="#" onclick="sortPayableProcessingList('a.actgCode');">Actg Code</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.actgCode'}">		 
		 <th width="" class="thactcod"><a href="#" onclick="sortPayableProcessingList('a.actgCode');"><font color="black">Accounting Cross Reference</font></a></th>		 		 
		 </c:if>
	     </c:if>			 
		 <c:if test="${systemDefaultVatCalculation=='true'}">
		 <c:if test="${fieldName!='a.actualExpense' || fieldName==''}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.actualExpense');" >Actual Expense</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.actualExpense'}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.actualExpense');"><font color="black">Actual Expense</font></a></th>		 
		 </c:if>
		 <c:if test="${fieldName!='a.payVatDescr' || fieldName==''}">
		 <th width="" id="vatDesc"><a href="#" onclick="sortPayableProcessingList('a.payVatDescr');" >VAT Desc</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatDescr'}">
		 <th width="" id="vatDesc"><a href="#" onclick="sortPayableProcessingList('a.payVatDescr');"><font color="black">VAT Desc</font></a></th>		 
		 </c:if>
		 <c:if test="${fieldName!='a.payVatPercent' || fieldName==''}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.payVatPercent');" >VAT %</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatPercent'}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.payVatPercent');"><font color="black">VAT %</font></a></th>		 
		 </c:if>
		  <c:if test="${fieldName!='a.payVatAmt' || fieldName==''}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.payVatAmt');" >VAT</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatAmt'}">
		 <th width=""><a href="#" onclick="sortPayableProcessingList('a.payVatAmt');"><font color="black">VAT</font></a></th>		 
		 </c:if>		 
		 </c:if>
		 
		 </tr>
		 </thead>
		 
		 </table>
		 </div>
		 
		<div id="bodywrap" style="width:2017px; overflow-y:scroll;  "> 
		
		<table id="dataTable" class="tbl1" >
		 <tbody>
		 <c:forEach var="individualItem" items="${previewAccountLines}" >
		 <tr>
		 <td  class="id" align="right" width=20px>${individualItem.count}</td>
		 <s:hidden name="accountlineNumberList" value="${individualItem.count}" id="num${individualItem.id}" />
	     <s:hidden name="contractTypeList" value="${individualItem.contractType}" id="cnt${individualItem.id}" />
		 <td  class="SO" align="right" width="100px">${individualItem.shipNumber}</td>
		 <td  class="Shipper" align="left" width="150px">		 
		 <c:if test="${individualItem.name==null || fn:trim(individualItem.name)==''}">
			<img class="openpopup" height="20" width="50px" align="top" src="images/blank.png">
		 </c:if>
		 ${individualItem.name}
		 </td>
		 <td  class="Category" align="right" width=80px>${individualItem.category}</td>
		 <td  class="Line" align="right" width="24px">${individualItem.accountLineNumber}</td>
		 <td  class="cCode" align="right" width=100px>${individualItem.chargeCode}</td>
		  <c:if test="${individualItem.payGL!='' && individualItem.payGL!=null}">
		 <td class="cDivision" width=80px><s:select cssClass="list-menu" list="%{findCompanyDivisionByCorpId}"  value="'${individualItem.companyDivision}'"  name="companyDivisionList" id="com${individualItem.id}" headerKey="" headerValue="" disabled="true" /></td>		 
		 </c:if>		 
		  <c:if test="${individualItem.payGL=='' || individualItem.payGL==null}">
		 <td class="cDivision"><s:select cssClass="list-menu" list="%{findCompanyDivisionByCorpId}"  value="'${individualItem.companyDivision}'" headerKey="" headerValue=""  disabled="true" /></td>
		 </c:if>		 

		 <td  class="Note" align="left" width="250px">${individualItem.description}</td>
		 <td  class="Basis" align="right" width=40px>
		 <c:if test="${individualItem.basis==null || fn:trim(individualItem.basis)==''}">
			<img class="openpopup" height="20" width="24px" align="top" src="images/blank.png">
		 </c:if>
		 ${individualItem.basis}
		 </td>
		  <td  class="estPayAmount" align="right" width=80px>${individualItem.estimateExpense}</td>
		  <td  class="revPayAmount" align="right" width=80px>${individualItem.revisionExpense}</td>
		  <c:if test="${individualItem.payGL!='' && individualItem.payGL!=null}">
           <td  class="actExpense" align="right" width=80px>
            <input type="text" value="${individualItem.actualExpense}" maxlength="12" class="input-text" style="width:50px" name="actualExpenseList" id="exp${individualItem.id}" onkeypress="return numbersonly(this,event);"  onchange="checkFloatNew(this);checkFloat(this);checkPayGlCode('${individualItem.id}','${individualItem.payGL}');checkActgCode1('${individualItem.id}',this);setInvoiceNumber('${individualItem.id}',this);calculatetotalActualExpense();"/>            																																					                                                        
           </td>
           <c:if test="${individualItem.currency!=currency}">
		 <td class="Currency"><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;"/></td>
		 <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		 <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		 <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />		 
		 </c:if>
           <c:if test="${individualItem.currency==currency}">
		 <td class="Currency" width=100px><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" onchange="return checkCurrency(this);"  disabled="true" cssStyle="width:80px;"/></td>
		 <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		 <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		 <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />		 
		 </c:if>
		 
           <td  class="Invoice" align="right" width=150px>
           <input type="text"  value="${individualItem.invoiceNumber}" name="invoiceNumberList" class="input-text" readonly="readonly" id="inv${individualItem.id}" />
           </td>
                
            <td class="Status" width=100px>
            <s:select cssClass="list-menu" list="%{payingStatus}"  value="'${individualItem.payingStatus}'"  name="payingStatusList" id="pay${individualItem.id}" headerKey="" headerValue="" disabled="true" cssStyle="width:80px;"/>
             </td>
           </c:if>
            <c:if test="${individualItem.payGL=='' || individualItem.payGL==null}">
           <td  class="actExpense" align="right">
            <input type="text" value="${individualItem.actualExpense}" id="exp${individualItem.id}" name="actualExpenseList" class="input-textUpper" readonly="readonly" style="width:50px" />
           </td> 
            <c:if test="${individualItem.currency!=currency}">
           <td class="Currency"><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'" name="currencyList" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;"/></td>
           <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		   <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		   <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />
		   </c:if>
		   <c:if test="${individualItem.currency==currency}">
           <td class="Currency"><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'" name="currencyList" headerKey="" headerValue="" onchange="return checkCurrency(this);"  disabled="true" cssStyle="width:80px;"/></td>
           <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		   <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		   <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />
		   </c:if>
            <td  class="Invoice" align="right" >
           <input type="text" value="${individualItem.invoiceNumber}" readonly="readonly" class="input-textUpper"  />
           </td>
            <!--  <td  class="listwhitetext" align="right"  >${individualItem.status}</td> -->
            <td >
            <s:select cssClass="list-menu" list="%{payingStatus}" value="'${individualItem.payingStatus}'" headerKey="" headerValue="" disabled="true"  cssStyle="width:80px;" />          
             </td>
           </c:if>
          <c:if test="${accountInterface=='Y'}">
           <td  class="actgCode" align="right" width=80px>
            <input type="text" value="${individualItem.actgCode}" class="input-text" name="actgCodeList" id="act${individualItem.id}" onkeypress="" maxlength="17" style="width:60px;" readonly="true"/>
           </td>
           </c:if>
           <c:if test="${systemDefaultVatCalculation=='true'}">
           <td  class="actualExpense" align="right" >
            <input type="text" value="${individualItem.expenseActual}" class="input-text" name="expenseActualList" id="aea${individualItem.id}"  style="width:50px;" readonly="true"/>
           </td>
		   <td ><s:select cssClass="list-menu" list="%{payVatList}"  value="'${individualItem.payVatDescr}'"  name="payVatDescrList" id="des${individualItem.id}" headerKey="" headerValue="" onchange="return getPayPercent(this);" cssStyle="width:50px;"/></td>
		   <td  class="VATPercent" align="right" >
            <input type="text" value="${individualItem.payVatPercent}" class="input-text" name="payVatPerList" id="pvp${individualItem.id}" onkeypress="return numbersonly(this,event);" style="width:50px;" onchange="onlyFloat1(this);checkFloatVat(this);calculatePayVatAmt('${individualItem.id}');" maxlength="6"/>
           </td>
           <td  class="VAT" align="right" >
            <input type="text" value="${individualItem.payVatAmt}" class="input-text" name="payVatAmtList" id="pva${individualItem.id}" onkeypress="return numbersonly(this,event);" style="width:50px;" readonly="true"/>
           </td>
		   </c:if>
		 </tr>
		 <script type="text/javascript">
		    setTotalExpense('${individualItem.actualExpense}','${individualItem.currency}');
		 </script>
		 </c:forEach>
		 </tbody>		 		  
		 </table>
		 </div>
		 </div>
		 </configByCorp:fieldVisibility>
		 <configByCorp:fieldVisibility componentId="component.field.Alternative.showNoFrzeeHeader">
		  <table class="table" id="dataTable" style="width:1650px;!margin-top:-6px;">
		 <thead>
		 <tr>
		 <th width="20px">#</th>		 		 
		 <c:if test="${fieldName!='a.shipNumber' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.shipNumber');">SO#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.shipNumber'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.shipNumber');"><font color="black">SO#</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='s.firstName,s.lastName' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('s.firstName');">Shipper</a></th>
		 </c:if>
		 <c:if test="${fieldName=='s.firstName,s.lastName'}">		 
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('s.firstName');"><font color="black">Shipper</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.category' || fieldName==''}">
		<th width="50px"><a href="#" onclick="sortPayableProcessingList('a.category');">Category<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.category'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.category');"><font color="black">Category</font><font color="red" size="2">*</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.accountLineNumber' || fieldName==''}">
		 <th width="30px"><a href="#" onclick="sortPayableProcessingList('a.accountLineNumber');">Line#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.accountLineNumber'}">
		 <th width="30px"><a href="#" onclick="sortPayableProcessingList('a.accountLineNumber');"><font color="black">Line#</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.chargeCode' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.chargeCode');">Charge Code<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.chargeCode'}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.chargeCode');"><font color="black">Charge Code</font><font color="red" size="2">*</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.companyDivision' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.companyDivision');">Company Division<font color="red" size="2">*</font></a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.companyDivision'}">		 
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.companyDivision');"><font color="black">Company Division</font><font color="red" size="2">*</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.note' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.note');">Note</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.note'}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.note');"><font color="black">Note</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.basis' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.basis');">Basis</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.basis'}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.basis');"><font color="black">Basis</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.estimateExpense' || fieldName==''}">
	     <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.estimateExpense');">Est Pay Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.estimateExpense'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.estimateExpense');"><font color="black">Est Pay Amount</font></a></th>
		 </c:if>

		 <c:if test="${fieldName!='a.revisionExpense' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.revisionExpense');">Revised Pay Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.revisionExpense'}">		 
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.revisionExpense');"><font color="black">Revised Pay Amount</font></a></th>		 		 
		 </c:if>
 
       <c:if test="${systemDefaultVatCalculation!='true'}">
		 <c:if test="${fieldName!='a.localAmount' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.localAmount');" >Actual Expense</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.localAmount'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.localAmount');"><font color="black">Actual Expense</font></a></th>		 
		 </c:if>
		 </c:if>
		<c:if test="${systemDefaultVatCalculation=='true'}">
		<c:if test="${fieldName!='a.localAmount' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.localAmount');" >Local Amount</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.localAmount'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.localAmount');"><font color="black">Local Amount</font></a></th>		 
		 </c:if>
		</c:if>
		 
		 <c:if test="${fieldName!='a.country' || fieldName==''}">
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.country');">Currency</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.country'}">		 
		 <th width="70px"><a href="#" onclick="sortPayableProcessingList('a.country');"><font color="black">Currency</font></a></th>		 
		 </c:if>
		 

		 <c:if test="${fieldName!='a.invoiceNumber' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.invoiceNumber');">Invoice#</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.invoiceNumber'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.invoiceNumber');"><font color="black">Invoice#</font></a></th>		 
		 </c:if>

		 <c:if test="${fieldName!='a.payingStatus' || fieldName==''}">
	     <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payingStatus');">Status</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payingStatus'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payingStatus');"><font color="black">Status</font></a></th>
		 </c:if>
         <c:if test="${accountInterface=='Y'}">
		 <c:if test="${fieldName!='a.actgCode' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.actgCode');">Actg Code</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.actgCode'}">		 
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.actgCode');"><font color="black">Accounting Cross Reference</font></a></th>		 		 
		 </c:if>
	     </c:if>			 
		 <c:if test="${systemDefaultVatCalculation=='true'}">
		 <c:if test="${fieldName!='a.actualExpense' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.actualExpense');" >Actual Expense</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.actualExpense'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.actualExpense');"><font color="black">Actual Expense</font></a></th>		 
		 </c:if>
		 <c:if test="${fieldName!='a.payVatDescr' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatDescr');" >VAT Desc</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatDescr'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatDescr');"><font color="black">VAT Desc</font></a></th>		 
		 </c:if>
		 <c:if test="${fieldName!='a.payVatPercent' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatPercent');" >VAT %</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatPercent'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatPercent');"><font color="black">VAT %</font></a></th>		 
		 </c:if>
		  <c:if test="${fieldName!='a.payVatAmt' || fieldName==''}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatAmt');" >VAT</a></th>
		 </c:if>
		 <c:if test="${fieldName=='a.payVatAmt'}">
		 <th width="50px"><a href="#" onclick="sortPayableProcessingList('a.payVatAmt');"><font color="black">VAT</font></a></th>		 
		 </c:if>
		 
		 </c:if>
		 </tr>
		 </thead>
		 <tbody>
		 <c:forEach var="individualItem" items="${previewAccountLines}" >
		 <tr>
		 <td  class="listwhitetext" align="right" >${individualItem.count}</td>
		 <s:hidden name="accountlineNumberList" value="${individualItem.count}" id="num${individualItem.id}" />
	     <s:hidden name="contractTypeList" value="${individualItem.contractType}" id="cnt${individualItem.id}" />
		 <td  class="listwhitetext" align="right" >${individualItem.shipNumber}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.name}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.category}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.accountLineNumber}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.chargeCode}</td>
		  <c:if test="${individualItem.payGL!='' && individualItem.payGL!=null}">
		 <td ><s:select cssClass="list-menu" list="%{findCompanyDivisionByCorpId}"  value="'${individualItem.companyDivision}'"  name="companyDivisionList" id="com${individualItem.id}" headerKey="" headerValue="" disabled="true" cssStyle="width:80px;"/></td>		 
		 </c:if>		 
		  <c:if test="${individualItem.payGL=='' || individualItem.payGL==null}">
		 <td ><s:select cssClass="list-menu" list="%{findCompanyDivisionByCorpId}"  value="'${individualItem.companyDivision}'" headerKey="" headerValue=""  disabled="true" cssStyle="width:80px;"/></td>
		 </c:if>		 

		 <td  class="listwhitetext" align="right" >${individualItem.description}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.basis}</td>
		  <td  class="listwhitetext" align="right" >${individualItem.estimateExpense}</td>
		  <td  class="listwhitetext" align="right" >${individualItem.revisionExpense}</td>
		  <c:if test="${individualItem.payGL!='' && individualItem.payGL!=null}">
           <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.actualExpense}" maxlength="12" class="input-text" name="actualExpenseList" id="exp${individualItem.id}" onkeypress="return numbersonly(this,event);" style="width:50px;" onchange="checkFloatNew(this);checkFloat(this);checkPayGlCode('${individualItem.id}','${individualItem.payGL}');checkActgCode1('${individualItem.id}',this);setInvoiceNumber('${individualItem.id}',this);calculatetotalActualExpense();"/>            																																					                                                        
           </td>
           <c:if test="${individualItem.currency!=currency}">
		 <td ><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;"/></td>
		 <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		 <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		 <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />		 
		 </c:if>
           <c:if test="${individualItem.currency==currency}">
		 <td ><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;" disabled="true"/></td>
		 <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		 <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		 <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />		 
		 </c:if>
		 

           <td  class="listwhitetext" align="right" >
           <input type="text"  value="${individualItem.invoiceNumber}" name="invoiceNumberList" class="input-text" readonly="readonly" id="inv${individualItem.id}" style="width:60px;" />
           </td>
                
            <td >
            <s:select cssClass="list-menu" list="%{payingStatus}"  value="'${individualItem.payingStatus}'"  name="payingStatusList" id="pay${individualItem.id}" headerKey="" headerValue="" disabled="true" cssStyle="width:80px;"/>
             </td>
           </c:if>
            <c:if test="${individualItem.payGL=='' || individualItem.payGL==null}">
           <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.actualExpense}" id="exp${individualItem.id}" name="actualExpenseList" style="width:50px;" class="input-textUpper" readonly="readonly"/>
           </td> 
            <c:if test="${individualItem.currency!=currency}">
           <td ><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'" name="currencyList" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;"/></td>
           <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		   <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		   <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />
		   </c:if>
		   <c:if test="${individualItem.currency==currency}">
           <td ><s:select cssClass="list-menu" list="%{country}"  value="'${individualItem.currency}'" name="currencyList" headerKey="" headerValue="" onchange="return checkCurrency(this);" cssStyle="width:80px;" disabled="true"/></td>
           <s:hidden name="oldCurrencyList" value="${individualItem.currency}" id="ocr${individualItem.id}" />
		   <s:hidden name="exchangeRateList" value="${individualItem.exchangeRate}" id="exr${individualItem.id}" />
		   <s:hidden name="oldExchangeRateList" value="${individualItem.exchangeRate}" id="oxr${individualItem.id}" />
		   </c:if>
            <td  class="listwhitetext" align="right" >
           <input type="text" value="${individualItem.invoiceNumber}" readonly="readonly" class="input-textUpper" style="width:60px;" />
           </td>
            <!--  <td  class="listwhitetext" align="right"  >${individualItem.status}</td> -->
            <td >
            <s:select cssClass="list-menu" list="%{payingStatus}" value="'${individualItem.payingStatus}'" headerKey="" headerValue=""   cssStyle="width:80px;" />          
             </td>
           </c:if>
          <c:if test="${accountInterface=='Y'}">
           <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.actgCode}" class="input-text" name="actgCodeList" id="act${individualItem.id}" onkeypress="" maxlength="17" style="width:60px;" readonly="true"/>
           </td>
           </c:if>
           <c:if test="${systemDefaultVatCalculation=='true'}">
           <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.expenseActual}" class="input-text" name="expenseActualList" id="aea${individualItem.id}"  style="width:50px;" readonly="true"/>
           </td>
		   <td ><s:select cssClass="list-menu" list="%{payVatList}"  value="'${individualItem.payVatDescr}'"  name="payVatDescrList" id="des${individualItem.id}" headerKey="" headerValue="" onchange="return getPayPercent(this);" cssStyle="width:80px;"/></td>
		   <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.payVatPercent}" class="input-text" name="payVatPerList" id="pvp${individualItem.id}" onkeypress="return numbersonly(this,event);" style="width:50px;" onchange="onlyFloat1(this);checkFloatVat(this);calculatePayVatAmt('${individualItem.id}');" maxlength="6"/>
           </td>
           <td  class="listwhitetext" align="right" >
            <input type="text" value="${individualItem.payVatAmt}" class="input-text" name="payVatAmtList" id="pva${individualItem.id}" onkeypress="return numbersonly(this,event);" style="width:50px;" readonly="true"/>
           </td>
		   </c:if>
		 </tr>
		 <script type="text/javascript">
		    setTotalExpense('${individualItem.actualExpense}','${individualItem.currency}');
		 </script>
		 </c:forEach>
		 </tbody>		 		  
		 </table>
		 	</configByCorp:fieldVisibility>
		 </div> 
          <div align="right"  style="width:76%;margin-top:5px;">
		 <span class="listwhitetext" style="width:200px;"><b>Total Expense :&nbsp;</b><input type="text" name="totalExpense" style="text-align:right" size="6" id="totalExpense" value="" class="input-textUpper" readonly="readonly"></span>
		 </div> 
      <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:80px; height:25px;margin-left:38px;" value="Add Lines" onclick="addRow('dataTable');" />
      <input type="button" class="cssbutton1" id="saveLine" name="saveLine" style="width:55px; height:25px;margin-left:5px;" value="Save" onclick="save();" />
      <c:if test="${payPostingFlag=='y'}">
      <input type="button" class="cssbutton1" style="width:150px; height:25px;margin-left:5px;" id="payposting" name="payposting" value="Apply Payable Posting" onclick="payPosting();" />
      </c:if>
      <c:if test="${payPostingFlag!='y'}">
            <input type="button" class="cssbutton1" style="width:150px; height:25px;margin-left:5px;" id="payposting" name="payposting" value="Apply Payable Posting" onclick="payPosting();" />
      </c:if>
      </s:form>
      <s:form name="totalExpenseForm">
      <s:hidden name="totExpValue"></s:hidden>
      </s:form>
     <script type="text/javascript">
     try{
    	 if(document.forms['payableProcessing'].elements['savePayableProcessingFlag1'].value=='yes'){
    		 document.forms['payableProcessing'].elements['fieldName'].value="";
    		 document.forms['payableProcessing'].elements['sortType'].value="";        	  
    		 document.forms['payableProcessing'].action='previewInvoicelines.html?saveCheck=OK'; 
    	     document.forms['payableProcessing'].submit(); 
    	 }
    	 }
    	 catch(e){}
     clearhiddenfields();
     setTotalExpense2();
     enableOrdisablePaybutton();
     </script>
     <script type="text/javascript">
try{
	var recievedDate=document.forms['payableProcessing'].elements['recievedDate'].value;
	if(recievedDate.trim()=='')
	{
	autoPopulate_recievedDate();
	}
	   }
	   catch(e){}
</script> 
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();	
</script>  