<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="com.trilasoft.app.model.PricingControlDetails"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agent Pricing Wizard | Red Sky</title>



<link rel="stylesheet" href="styles/list.css" media="screen" type="text/css">
<link rel="stylesheet" href="styles/forms.css" media="screen" type="text/css">
</head>

<s:form id="agentPricing" name="agentPricing" action='locatePartners1.html' method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy hh:mm:ss}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isDestination"/>
<s:hidden key="pricingControl.isFreight"/>

<div id="newmnav" style="!margin-bottom:5px;float:left;">
		  <ul>
		  <c:if test="${!param.popup}">
		 	 	<li><a href="agentPricingBasicInfo.html?id=${pricingControl.id}"><span>Basic Info</span></a></li>
			  	<li><a onclick="return checkOriginScope();" href="agentPricingOriginInfo.html?id=${pricingControl.id}"><span>Origin Options</span></a></li>
			  	<li><a onclick="return checkFreightScope();" href="agentPricingFreightInfo.html?id=${pricingControl.id}"><span>Freight Options</span></a></li>
			  	<li><a onclick="return checkDestinationScope();" href="agentPricingDestinationInfo.html?id=${pricingControl.id}"><span>Destination Options</span></a></li>
			  	<li id="newmnav1" ><a class="current"><span>Pricing Summary<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  	<li><a href="agentPricingAllPricingInfo.html?id=${pricingControl.id}"><span>Pricing Options</span></a></li>
			  	<li><a href="agentPricingList.html"><span>Quote List</span></a></li>
			<sec-auth:authComponent componentId="module.tab.pricingWizard.accountingTab">
			 <c:if test="${not empty pricingControl.serviceOrderId}">
			  	<li><a onclick="return checkControlFlag();"><span>Accounting</span></a></li>
		   	</c:if>	
		  </sec-auth:authComponent> 
		  </c:if>	
		   <c:if test="${param.popup}">
		  	<li><a href="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"><span>Pricing Engine List</span></a></li>
		  </c:if>
		  	
		  </ul>
		</div>
		<!--<div style="float: left; font-size: 13px; margin-left: -10px;margin-top:-3px;">
		<a href="http://www.sscw.com/movingservices/international_move.html" target="_blank" style="text-decoration:underline;">
		
		<a href="#"  style="color:#330000;" onclick="javascript:window.open('http://www.sscw.com/redsky/agentportal/','help','width=1004,height=500,scrollbars=yes,left=100,top=50');">
		<img src="images/wizard-button.png" border="0"/></a></div>
		--><div class="spn">&nbsp;</div>

    </td>
  </tr>
</table>
</td>
</tr>
</table>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width:90%;margin:0px;padding:0px;" >
  <tbody>
    <tr>
      <td width="100%" valign="top" style="margin:0px;padding: 0px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0px;padding: 0px;">
          <tbody>
            <tr>
            <tr>
              <td  colspan="8" width="100%" style="margin:0px;padding:0px;"><span onclick="swapImg(img1);">
                <div onClick="javascript:animatedcollapse.toggle('info');" style="margin:0px;">
                 <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img1" src="images/tab_open.png" border="0" /></a></div><div class="price_tright">&nbsp;Shipment Information</div></div>
                </div>
                </span>
                <div id="info" class="summary-tab">                
                <table width="100%"  cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" class="listblacktext">
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Expected Load Date:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Mode:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Weight:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" style="background-color:#EEEEEE" width="180">
					<table style="margin:0px;padding:0px;">
					<tr>
					<fmt:formatDate var="expectedDateFormattedValue" value="${pricingControl.expectedLoadDate}" pattern="${displayDateTimeEditFormat}"/>
					<td align="left"  valign="top" class="listblacktext"><fmt:formatDate value="${pricingControl.expectedLoadDate}" pattern="${displayDateFormat}"/></td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.mode}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.weight} (lbs)&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td width="90">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Packing Mode:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Container Size:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Volume:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.packing}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.containerSize}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.volume} (cft)&nbsp;</td>
					</tr>
					</table>					
					</td>
					
					<td width="90" valign="top" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Pricing Id:</b></td>
					</tr>
					</table>			
					</td>
					
					<td class="listblacktext" style="background-color:#EEEEEE" width="190" valign="top">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.pricingID}&nbsp;</td>
					</tr>
					
					</table>					
					</td>
					
					</tr>
                	</table>
                	
                	</td>
                	</tr>
                	</table>
                
					                  
                	</div>
                        </td>
                    </tr>
                  </table>
                </div></td>
            </tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img2);">
                <div onClick="javascript:animatedcollapse.toggle('origin');" style="margin:0px;">
                <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img2" src="images/tab_close.png" border="0"/></a></div><div class="price_tright">&nbsp;Origin Address</div></div>
                </div>
                </span>
                <div id="origin" class="summary-tab">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Address:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="180">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.originAddress1}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Latitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Longitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Port Of Entry:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.originLatitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originLongitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.originPOEName}&nbsp;</td>
					</tr>
					</table>					
					</td>					
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                	
                	</div></td>
            </tr>
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img3);">
                <div onClick="javascript:animatedcollapse.toggle('destination');" style="margin:0px;">
                 <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img3" src="images/tab_close.png" border="0"/></a></div><div class="price_tright">&nbsp;Destination Address</div></div>
                </div>
                </div>
                </span>
                <div id="destination" class="summary-tab">
                
                 <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Address:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="180">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.destinationAddress1}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Latitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Longitude:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Port Of Entry:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControl.destinationLatitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationLongitude}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${pricingControl.destinationPOEName}&nbsp;</td>
					</tr>
					</table>					
					</td>					
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                </div></td>
            </tr>
            
            <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img4);">
                <div onClick="javascript:animatedcollapse.toggle('originoption');" style="margin:0px;">
                 <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img4" src="images/tab_open.png" border="0"/></a></div><div class="price_tright">&nbsp;Origin Agent<span style="padding-left:300px;"><a onclick="return checkOriginScope()" href="agentPricingOriginInfo.html?id=${pricingControl.id}">(Please see options in Origin tab)</a></span></div></div>
                </div>
                </div>
                </span>
                <div id="originoption" class="summary-tab">
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Name:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="180">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${originPartner.lastName }&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Fax:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Phone:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Telex:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Email:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${originPartner.terminalFax}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalPhone}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${originPartner.terminalTelex}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext"><a href="#">${originPartner.terminalEmail}</a>&nbsp;</td>
					</tr>
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Orgin Price:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="182">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsOrigin.baseCurrency} 
					<script type="text/javascript">
					var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
									
					totalAmount=originPrice*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
					 </script>
					
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table></div></td>
            </tr>
            
             <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img6);">
                <div onClick="javascript:animatedcollapse.toggle('freight');" style="margin:0px;">
                 <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"><img id="img6" src="images/tab_open.png" border="0"/></a></div><div class="price_tright">&nbsp;Freight<span style="padding-left:332px;"><a onclick="return checkFreightScope()" href="agentPricingFreightInfo.html?id=${pricingControl.id}">(Please see options in Freight tab)</a></span></div></div>
                </div>
                </div>
                </span>
                <div id="freight" class="summary-tab">
                
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Carrier:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Origin Port:</b></td>
					</tr>					
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="180">
					<table border="0" style="margin:0px;padding:0px;">					
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.carrier}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.originPortCity},${refFreightRates.originPortCountry}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">					
					<tr><td align="right"  valign="top" class="listblacktext"><b>Origin:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Destination:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${refFreightRates.originCity}, ${refFreightRates.originCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${refFreightRates.destinationCity}, ${refFreightRates.destinationCountry}&nbsp;</td>
					</tr>					
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Freight Quote:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Doc Fee:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="182">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext"> 
					<script type="text/javascript">
					var originPrice='${refFreightRates.totalPrice}';
					var exchangeRate=1;
					totalAmount=originPrice*1.00*exchangeRate*1;
					document.write(totalAmount.toFixed(2));
				    </script>
					&nbsp; 
					
					</td>
					</tr>
					
					<tr>					
					<td align="left"  valign="top" class="listblacktext"> 
					<script type="text/javascript">
					var frieghtMarkup='${pricingFreight.freightMarkup}';
					<c:if test="${pricingControl.freightMarkUpControl == 'false'}" >
						frieghtMarkup=0.0;
					</c:if>				
					totalAmount=frieghtMarkup*1.00;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					&nbsp; 
					
					</td>
					</tr>
										
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                
                
                
                </div></td>
            </tr>
            
             <tr>
              <td  colspan="8" width="100%" style="margin: 0px;"><span onclick="swapImg(img5);">
                <div onClick="javascript:animatedcollapse.toggle('destinationoption');" style="margin:0px;"><div class="pricetab_bg">&nbsp;<div class="price_tleft"><img id="img5" src="images/tab_open.png" border="0"/></a></div> <div  class="price_tright">&nbsp;Destination Agent<span style="padding-left:270px;"><a onclick="return checkDestinationScope();" href="agentPricingDestinationInfo.html?id=${pricingControl.id}">(Please see options in Destination tab)</a></span></div></div>
                </div>
                </div>
                </span>
                <div id="destinationoption" style="margin:0px;padding:0px;">
                
                
                <table width="100%"  border="0" cellpadding="0" cellspacing="0" style="padding-left:10px; margin-bottom:0px;" >
                
                	<tr>
                	<td>
                
                	<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
                	<tr>
                	
					<td width="117" valign="top" align="right">
					<table border="0" style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Name:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Country:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>State:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>City:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Postal:</b></td>
					</tr>
					</table>
					</td>
					
					<td style="background-color:#EEEEEE" valign="top" width="180">
					<table border="0" style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${destinationPartner.lastName}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalCountry}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalState}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalCity}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalZip}&nbsp;</td>
					</tr>
					</table>
					</td>
					
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Fax:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Phone:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Telex:</b></td>
					</tr>
					<tr><td align="right"  valign="top" class="listblacktext"><b>Email:</b></td>
					</tr>
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="190">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalFax}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalPhone}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext">${destinationPartner.terminalTelex}&nbsp;</td>
					</tr>
					<tr><td align="left"  valign="top" class="listblacktext"><a href="#">${destinationPartner.terminalEmail}</a>&nbsp;</td>
					</tr>
					</table>					
					</td>
										
					<td valign="top" width="90" align="right">
					<table style="margin:0px;padding:0px;">
					<tr><td align="right"  valign="top" class="listblacktext"><b>Dest Price:</b></td>
					</tr>					
					</table>
					</td>
					
					<td class="listblacktext" valign="top" style="background-color:#EEEEEE" width="182">
					<table style="margin:0px;padding:0px;">
					<tr>					
					<td align="left"  valign="top" class="listblacktext">${pricingControlDetailsDestination.baseCurrency} 
					<script type="text/javascript">
					var originPrice='${pricingControlDetailsDestination.rateMarkUp}';
					var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
					var chargeMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
					var quoteMarkupWithoutTHC='${pricingControlDetailsDestination.quoteMarkupWithoutTHC}';
					var chargeArrayMarkUp=new Array();
					chargeArrayMarkUp = chargeMarkup.split(",");
					var dthcFlag='${pricingControlDetailsDestination.dthcFlag}';
					if(dthcFlag=='true'){
							originPrice= quoteMarkupWithoutTHC*1*exchangeRate;
					}
					totalAmount=originPrice*1.00*exchangeRate*1;
					
					document.write(totalAmount.toFixed(2));
				    </script>
					</td>
					</tr>					
					</table>					
					</td>				
					</tr>
                	</table>                	
                	</td>
                	</tr>
                	</table>
                
                </div></td>
            </tr>
            
         
		 <tr>
		  <table class="detailTabLabel" style="margin:0px;padding:0px;width:90%;" border="0">
		 	<td align="right"  valign="middle" class="listblacktext" width="79%"><b>Total Price:</b></td>
			<td align="left" valign="top" class="listblacktext" style="width:100px">
		<c:choose>
			<c:when test="${pricingControl.isOrigin==true}" >
				${pricingControlDetailsOrigin.baseCurrency} 
			</c:when>
			<c:when test="${pricingControl.isOrigin==true}" >
					${pricingControlDetailsDestination.baseCurrency} 
			</c:when>
			<c:otherwise>
				${pricingControlDetailsFreight.baseCurrency}
			</c:otherwise>
		</c:choose>
		
			<script type="text/javascript">
				
			
			var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
			var freightPrice='${refFreightRates.totalPrice}';
			var destinationPrice='${pricingControlDetailsDestination.rateMarkUp}';
			var chargeMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
			var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
			var quoteMarkupWithoutTHC='${pricingControlDetailsDestination.quoteMarkupWithoutTHC}';
			var chargeArrayMarkUp=new Array();
			chargeArrayMarkUp = chargeMarkup.split(",");
			var dthcFlag='${pricingControlDetailsDestination.dthcFlag}';
			if(dthcFlag=='true'){
					destinationPrice= quoteMarkupWithoutTHC*1*exchangeRate;
				}
			var originExchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
			var destinationExchangeRate='${pricingControlDetailsDestination.exchangeRate}';
			var freightExchangeRate= 1;	
			var frieghtMarkup='${pricingFreight.freightMarkup}';
			<c:if test="${pricingControl.freightMarkUpControl == 'false'}" >
				frieghtMarkup=0.0;
			</c:if>					
			totalAmount=originPrice*1.00*originExchangeRate+freightPrice*1.00*freightExchangeRate+destinationPrice*1.00*destinationExchangeRate +  frieghtMarkup*1.00;
			
			document.write(totalAmount.toFixed(2));
			</script>
			</td>					
					

          </tbody>
       </table> 
       </tr>
       
       <tr>
		  <table class="detailTabLabel" style="margin:0px;padding:0px;width:90%;" border="0">
		 	<td align="right"  valign="middle" class="listblacktext" width="79%"><b>Discount/Premium:</b></td>
			<td align="left" valign="top" class="listblacktext" style="width:100px">
		<c:choose>
			<c:when test="${pricingControl.isOrigin==true}" >
				${pricingControlDetailsOrigin.baseCurrency} 
			</c:when>
			<c:when test="${pricingControl.isOrigin==true}" >
					${pricingControlDetailsDestination.baseCurrency} 
			</c:when>
			<c:otherwise>
				${pricingControlDetailsFreight.baseCurrency}
			</c:otherwise>
		</c:choose>
		
			<script type="text/javascript">
				
			
			var originDiscount='${pricingControlDetailsOrigin.discountValue}';
			var destinationDiscount='${pricingControlDetailsDestination.discountValue}';
						
			totalDiscount=originDiscount*1.00 + destinationDiscount*1;
			
			document.write(totalDiscount.toFixed(2));
			</script>
			</td>					
					

          </tbody>
       </table> 
       </tr>
       
       <tr>
		  <table class="detailTabLabel" style="margin:0px;padding:0px;width:90%;" border="0">
		 	<td align="right"  valign="middle" class="listblacktext" width="79%"><b>Net Price:</b></td>
			<td align="left" valign="top" class="listblacktext" style="width:100px">
		<c:choose>
			<c:when test="${pricingControl.isOrigin==true}" >
				${pricingControlDetailsOrigin.baseCurrency} 
			</c:when>
			<c:when test="${pricingControl.isOrigin==true}" >
					${pricingControlDetailsDestination.baseCurrency} 
			</c:when>
			<c:otherwise>
				${pricingControlDetailsFreight.baseCurrency}
			</c:otherwise>
		</c:choose>
		
			<script type="text/javascript">
			
			var originPrice='${pricingControlDetailsOrigin.rateMarkUp}';
			var freightPrice='${refFreightRates.totalPrice}';
			var destinationPrice='${pricingControlDetailsDestination.rateMarkUp}';
			var chargeMarkup='${pricingControlDetailsDestination.chargeDetailMarkUp}';
			var exchangeRate='${pricingControlDetailsDestination.exchangeRate}';
			var quoteMarkupWithoutTHC='${pricingControlDetailsDestination.quoteMarkupWithoutTHC}';
			var chargeArrayMarkUp=new Array();
			chargeArrayMarkUp = chargeMarkup.split(",");
			var dthcFlag='${pricingControlDetailsDestination.dthcFlag}';
			if(dthcFlag=='true'){
		
					destinationPrice= quoteMarkupWithoutTHC*1*exchangeRate;
				}
			var originExchangeRate='${pricingControlDetailsOrigin.exchangeRate}';
			var destinationExchangeRate='${pricingControlDetailsDestination.exchangeRate}';
			var frieghtMarkup='${pricingFreight.freightMarkup}';
			<c:if test="${pricingControl.freightMarkUpControl == 'false'}" >
						frieghtMarkup=0.0;
			</c:if>				
			totalAmount=originPrice*1.00*originExchangeRate+freightPrice*1.00*freightExchangeRate+destinationPrice*1.00*destinationExchangeRate +  frieghtMarkup*1.00;
			
			
				
			
			var originDiscount='${pricingControlDetailsOrigin.discountValue}';
			var destinationDiscount='${pricingControlDetailsDestination.discountValue}';
						
			totalDiscount=originDiscount*1.00 + destinationDiscount*1;
			
			netAmount= totalAmount*1 + totalDiscount*1;
			document.write(netAmount.toFixed(2));
			</script>
			</td>					
					

          </tbody>
       </table> 
       </tr>
            
          </tbody>
        </table>
        
    
	
 <table class="detailTabLabel" style="margin:0px;padding:0px;width:90%; border:1px solid #ccc; background-color:#efefef">
 <tr>
	<td align="left" style="width:210px" class="listblacktext"><b>For missing quotes and clarification:</b></td>
	<td align="left"  ><input type="button" id="submittedQuotes" class="cssbutton1" style="margin-right:35px;width:215px; height:25px"  
         onclick="window.open('emailPage.html?id=${pricingControl.id}&decorator=popup&popup=true','surveysList','height=300,width=700,top=0, left=610, scrollbars=yes,resizable=yes').focus();" 
        value="Contact Rate Department"/></td>
        <td align="left" class="listblacktext" style="width:100px"></td>
        <td align="left" class="listblacktext" style="width:px"><input type="button" class="cssbuttonA" style="width:50px; height:25px;margin-left:100px; "  name="Print"  value="Print" onclick="printPricingReport();"/></td>
</tr>
 </table>				
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 11-Sep-2012 By Kunal --%>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=0')
	animatedcollapse.addDiv('origin', 'fade=1,persist=0,hide=1')
	animatedcollapse.addDiv('destination', 'fade=0,persist=0,hide=1')
	animatedcollapse.addDiv('originoption', 'fade=0,persist=0,hide=0')
	animatedcollapse.addDiv('destinationoption', 'fade=0,persist=0,hide=0')
	animatedcollapse.addDiv('freight', 'fade=0,persist=0,hide=0')
	animatedcollapse.addDiv('masterList', 'fade=0,persist=0,hide=0')
	animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("tab_open.png", "tab_close.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
}

function calculateTotal()
{
	var originPrice='${pricingControlDetailsOrigin.rate}';
	var freightPrice='${refFreightRates.totalPrice}';
	var destinationPrice='${pricingControlDetailsDestination.rate}';
	var currency='${pricingControlDetailsOrigin.currency}';
	var totalAmount=originPrice*1+freightPrice*1+destinationPrice*1;
	document.forms['agentPricing'].elements['totalPrice'].value=totalAmount;
}

function checkOriginScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
if(destinationScope!='true')
{
	alert('Origin information not provided.');
	return false;
}
}

function checkDestinationScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isDestination'].value;
if(destinationScope!='true')
{
	alert('Destination information not provided.');
	return false;
}
}

function checkFreightScope(){
var freightScope=document.forms['agentPricing'].elements['pricingControl.isFreight'].value;
if(freightScope!='true')
{
	alert('Origin and/or Destination Ports information not provided.');
	return false;
}
}

function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
	
}

function handleHttpResponseCheckControlFlag(){
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                var sid='${pricingControl.serviceOrderId}';
                	location.href='accountLineList.html?sid='+sid;
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();
    
    function printPricingReport(){
	    var id = '${pricingControl.id}';
		window.open("printAgentPricingSummaryReport.html?id="+id+"&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=1000, height=680,top=0, left=0,menubar=no")
    }
    
    function selectRespectiveRecords(originAgentId, destinationAgentId, freightId){
    // alert(originAgentId+' --- '+destinationAgentId+' ---- '+freightId);
	   	var pricingControlID = '${pricingControl.id}';
	    var url="updateRespectiveRecords.html?ajax=1&decorator=simple&popup=true&originAgentRecordId="+originAgentId+"&destinationAgentRecordId="+destinationAgentId+"&freightRecordId="+freightId+"&pricingControlID="+pricingControlID;
		http2.open("GET", url, true);
	  	http2.send(null);
    }
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
	<c:if test="${hitFlag == 33}" >
		<c:redirect url="/agentPricingBasicInfo.html"  />
	</c:if>
</script>
