<%@ include file="/common/taglibs.jsp"%> 

<head>
	<meta name="heading" content="Executable Query Result"/> 
	<title>Executable Query Result</title>
</head>
<script type="text/javascript"> 
</script>	
<body style="background-color:#444444;">
<s:form id="executeBulkQuery" name="executeBulkQuery" action="executeBulkQuery" method="post" validate="true">
<s:hidden name="id" value="%{dataMaintainence.id}"/>
<div id="Layer1"  style="width:750px" >
<div id="newmnav">
		  <ul>
		  	
		  	
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Output<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
</div><div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:0px;!margin-top:0px;"><span></span></div>
<div class="center-content" style="overflow:auto;" >
<table class="detailTabLabel" border="0" width="100%">
<c:forEach items="${executedResult}" var="item" >
<tr><td align="left" class="listwhitetext" style="font-size:12px"><c:out value="${item}"/></td></tr>
</c:forEach>
<tr><td align="center" class="vertlinedata" colspan="15"></td>
</tr>
<tr>
<td>
<s:submit cssClass="cssbutton1" cssStyle="width:65px;" type="button" method="bulkUpdate" label="Execute"  /></td> 
</tr>
<tr>
<td align="left" height="20px"></td>
</tr>
</table>
<div style=""> </div>
</div>
<div class="bottom-header">
<span>&nbsp;</span>
</div>
</div>
</s:form>

