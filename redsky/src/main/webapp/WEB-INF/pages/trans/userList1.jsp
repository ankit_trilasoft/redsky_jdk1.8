<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>
</head>

<c:set var="buttons">
    <input type="button" style="margin-right: 5px"
        onclick="location.href='<c:url value="/editTrilaUser.html?method=Add&from=list"/>'"
        value="<fmt:message key="button.add"/>"/>
    
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"
        value="<fmt:message key="button.done"/>"/>
</c:set>



<display:table name="userss" cellspacing="0" cellpadding="0" requestURI="" 
    defaultsort="1" id="userss" pagesize="25" class="table" export="true">
    <display:column property="username" escapeXml="true" sortable="true" titleKey="userss.username" style="width: 25%"
        url="/editTrilaUser.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="first_name" titleKey="users.first_name" />
    <display:column property="last_name" titleKey="users.last_name" />
    <display:column property="email" sortable="true" titleKey="users.email" style="width: 25%" autolink="true" media="html"/>
    <display:column property="email" titleKey="users.email" media="csv xml excel pdf"/>
    <!-- 
    <display:column sortProperty="enabled" sortable="true" titleKey="users.enabled" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${users.enabled}">checked="checked"</c:if>/>
    </display:column>
    <display:column property="enabled" titleKey="users.enabled" media="csv xml excel pdf"/>
	 -->
    <display:setProperty name="paging.banner.item_name" value="users"/>
    <display:setProperty name="paging.banner.items_name" value="userss"/>

    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>

<c:out value="${buttons}" escapeXml="false" />

<script type="text/javascript">
    highlightTableRows("userss");
</script>
