<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>
 		<title>Driver Event Details</title>   
    	<meta name="heading" content="Driver Event Details"/>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script LANGUAGE="JavaScript">
function checkDate(temp)
{
 var date1 = document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value;	 
<c:if test="${not empty calendarFile.id}"> 
 var date2 = document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value; 
</c:if>
<c:if test="${empty calendarFile.id}"> 
 var date2 = document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value; 
</c:if>
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')   {       month = "01";   }
   else if(month == 'Feb')   {       month = "02";   }
   else if(month == 'Mar')   {       month = "03"   }
   else if(month == 'Apr')   {       month = "04"   }
   else if(month == 'May')   {       month = "05"   }
   else if(month == 'Jun')   {       month = "06"   }
   else if(month == 'Jul')   {       month = "07"   }
   else if(month == 'Aug')   {       month = "08"   }
   else if(month == 'Sep')   {       month = "09"   }
   else if(month == 'Oct')   {       month = "10"   }
   else if(month == 'Nov')   {       month = "11"   }
   else if(month == 'Dec')   {       month = "12";   }   
   var finalDate = month+"-"+day+"-"+year;
   
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')   {       month2 = "01";   }
   else if(month2 == 'Feb')   {       month2 = "02";   }
   else if(month2 == 'Mar')   {       month2 = "03"   }
   else if(month2 == 'Apr')   {       month2 = "04"   }
   else if(month2 == 'May')   {       month2 = "05"   }
   else if(month2 == 'Jun')   {       month2 = "06"   }
   else if(month2 == 'Jul')   {       month2 = "07"   }
   else if(month2 == 'Aug')   {       month2 = "08"   }
   else if(month2 == 'Sep')   {       month2 = "09"   }
   else if(month2 == 'Oct')   {       month2 = "10"   }
   else if(month2 == 'Nov')   {       month2 = "11"   }
   else if(month2 == 'Dec')   {       month2 = "12";   }
   var finalDate2 = month2+"-"+day2+"-"+year2;

  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  document.forms['myDriverEventForm'].elements['calendarFile.surveyDays'].value=daysApart+"";
  if(daysApart<0)
  {
      alert("From Date should be less than To Date");
    document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value='';
<c:if test="${empty calendarFile.id}"> 
    document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value='';
</c:if>
    document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].focus();
    return false;
  }else	if(temp=='autoSave')
 	{
		autoSaveFunc('none');
	}
}
function calcDays()
{
 var date2 = document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value;	 
 var date1 = document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['myDriverEventForm'].elements['calendarFile.surveyDays'].value = daysApart; 
  if(daysApart<0)
  {
    alert("From Date should be less than To Date");
    document.forms['myDriverEventForm'].elements['calendarFile.surveyDays'].value='';
    document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value='';
    return false;
  }
  if(document.forms['myDriverEventForm'].elements['calendarFile.surveyDays'].value=='NaN')
   {
     document.forms['myDriverEventForm'].elements['calendarFile.surveyDays'].value = '';
   } 
 document.forms['myDriverEventForm'].elements['checkSurveyDaysClick'].value = '';
 }
 function fillSurveyToDate(){
	var surveyFromDate = document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value;
	var surveyToDate = document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value;
  if(surveyToDate =="")
	{
	document.forms['myDriverEventForm'].elements['calendarFile.surveyTillDate'].value = surveyFromDate;
	}
} 
function openBookingAgentPopWindow(){
window.open("openDriverAgentPopup.html?decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=800, height=480,top=0, left=0,menubar=no")
}

function IsValidTime(temp){
	if(document.forms['myDriverEventForm'].elements['calendarFile.activityName'].value == ""){
 		alert("Enter Event Name to continue.....");
 		return false;
 		}else{
 		return checkDriverField(temp);
 	}
}

function checkDriverField(temp){
	var driverName=document.forms['myDriverEventForm'].elements['calendarFile.personId'].value;
	if(driverName == ""){
 		alert("Select Driver to continue.....");
 		document.forms['myDriverEventForm'].elements['calendarFile.personId'].select();
 		return false;
 	}else{
 	return checkDateField(temp)
 	}

}

function checkDateField(temp){
	var firstDate=document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].value;
	if(firstDate == ""){
 		alert("Select date to continue.....");
 		document.forms['myDriverEventForm'].elements['calendarFile.surveyDate'].select();
 		return false;
 	}else{
 	return checkDate(temp)
 	}

}
function autoSaveFunc(clickType){

	if ('${autoSavePrompt}' == 'No'){
		var noSaveAction = '<c:out value="${calendarFile.id}"/>';
      	     		
		if(document.forms['myDriverEventForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 			noSaveAction = 'searchEventsList.html?from=search';
 				noSaveAction = 'searchEventsList.html?from=search&eventFrom=${eventFrom}&eventTo=${eventTo}';
        }
        processAutoSave(document.forms['myDriverEventForm'], 'saveMyDriver.html', noSaveAction);
	
	
	}else{
			if (formIsDirty(document.forms['myDriverEventForm']))
			{
			var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='myCalendarDetail.heading'/>");
       			if(agree){
		           if(document.forms['myDriverEventForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 				noSaveAction = 'searchEventsList.html?from=search&eventFrom=${eventFrom}&eventTo=${eventTo}';
       				 }
       				 processAutoSave(document.forms['myDriverEventForm'], 'saveMyDriver.html', noSaveAction);
        		}else
        		{
        		if(document.forms['myDriverEventForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 				noSaveAction = 'searchEventsList.html?from=search&eventFrom=${eventFrom}&eventTo=${eventTo}';
       				 }
       				 processAutoSave(document.forms['myDriverEventForm'], noSaveAction, noSaveAction);
        		
        		}
    		
  		}else{
  			if(document.forms['myDriverEventForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 				noSaveAction = 'searchEventsList.html?from=search&eventFrom=${eventFrom}&eventTo=${eventTo}';
 				                
       		}
       			processAutoSave(document.forms['myDriverEventForm'], noSaveAction, noSaveAction);
  		
  		}
  	}
  //getEventList();
}
function forDays(){
 document.forms['myDriverEventForm'].elements['checkSurveyDaysClick'].value = '1';
}

function makePopulateDriverName(){
	var driverCode=document.forms['myDriverEventForm'].elements['calendarFile.personId'].value;
	var url="driverDetailCode.html?ajax=1&decorator=simple&popup=true&myDriverCode=" + encodeURI(driverCode);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpPortDriverCode;
    http4.send(null);
}

function httpPortDriverCode()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>2)
               	{
               	    var dArr=results.split("~");
 					document.forms['myDriverEventForm'].elements['calendarFile.firstName'].value = dArr[0];
 					document.forms['myDriverEventForm'].elements['calendarFile.lastName'].value = dArr[1];
                 }
                 else
                 {
                   alert("Please enter valid driver code");
                   document.forms['myDriverEventForm'].elements['calendarFile.personId'].value="";
				   document.forms['myDriverEventForm'].elements['calendarFile.firstName'].value = "";
 				   document.forms['myDriverEventForm'].elements['calendarFile.lastName'].value = "";
                   document.forms['myDriverEventForm'].elements['calendarFile.personId'].focus();                 
                 }
             }
        }

function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject4();



</script>  	
</head>
<s:form id="myDriverEventForm"   name="myDriverEventForm" action="saveMyDriver.html" method="post" validate="true"> 
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="calendarFile.id" />
    <s:hidden name="driverName" value="" />
    <s:hidden id="checkSurveyDaysClick" name="checkSurveyDaysClick" />
    <s:hidden id="surDays" name="calendarFile.surveyDays"/>
    <s:hidden id="eventFrom" name="eventFrom"/>
    <s:hidden id="eventTo" name="eventTo"/>    
<s:hidden name="calendarFile.corpID" />
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<c:choose>
<c:when test="${gotoPageString == 'gototab.calendarFile' }">
   <c:redirect url="/eventList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<div id="Layer1" onkeydown="changeStatus();" style="width:750px;">
	<div id="newmnav">
			<ul>
             <li id="newmnav1"  style="cursor:pointer" style="background:#FFF "><a class="current"  ><span  style="cursor:pointer">Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
             
			 <li><a onclick="setReturnString('gototab.calendarFile');return IsValidTime('autoSave');"><span>Event List</span></a></li>			
<!--			 <li><a href="eventList.html?from=driver" ><span>Event List</span></a></li>-->
			 
</ul>
    </div><div class="spn">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style=""><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="2" border="0">
	<tbody>
		<tr>
		<td class="listwhitetext" align="right">Driver<font color="red" >*</font></td>
		<td class="listwhitetext" align="left"><s:textfield cssClass="input-text" size="7" maxlength="20" name="calendarFile.personId" onchange="makePopulateDriverName()"/></td>
		<td width="22"><img id="calendarFile.driverPopup" class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow();document.forms['myDriverEventForm'].elements['calendarFile.personId'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>		
		<td colspan="5">
		<table class="detailTabLabel" border="0">
		<tr>
		<td class="listwhitetext" align="right">First&nbsp;Name</td>
		<td class="listwhitetext" align="left"><s:textfield cssClass="input-textUpper" size="10" maxlength="25" name="calendarFile.firstName" readonly="true" /></td>
		<td class="listwhitetext" align="right">Last&nbsp;Name</td>
		<td class="listwhitetext" align="left"><s:textfield cssClass="input-textUpper" size="13" maxlength="25" name="calendarFile.lastName" readonly="true" /></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
<c:if test="${not empty calendarFile.id}">
<td align="right" class="listwhitetext" width="">Date<font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyDate}">
<s:text id="calendarFileSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyDate"/></s:text>
<td width=""><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" value="%{calendarFileSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="fillSurveyToDate();calcDays();findSurveyDay();"/></td>
<td width="20"><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyDate}">
<td width=""><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="fillSurveyToDate();calcDays();findSurveyDay();" /></td>
<td width="20"><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>
<s:hidden name="calendarFile.surveyTillDate" value="%{calendarFile.surveyTillDate}"/>
</c:if>

<c:if test="${empty calendarFile.id}">		
<td align="right" class="listwhitetext" width=""><fmt:message key='calendarFile.surveyDate'/><font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyDate}">
<s:text id="calendarFileSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyDate"/></s:text>
<td width=""><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" value="%{calendarFileSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="fillSurveyToDate();calcDays();findSurveyDay();"/></td>
<td width="20"><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyDate}">
<td width=""><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="fillSurveyToDate();calcDays();findSurveyDay();" /></td>
<td width="20"><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>
<td align="right" class="listwhitetext" width=""><fmt:message key='calendarFile.surveyTillDate'/><font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyTillDate}">
<s:text id="calendarFileSurveyTillDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyTillDate"/></s:text>
<td width="50"><s:textfield cssClass="input-text" id="surveyTillDate" name="calendarFile.surveyTillDate" value="%{calendarFileSurveyTillDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="calcDays();findSurveyDay();"/></td>
<td align="left"><img id="surveyTillDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyTillDate}">
<td width="50"><s:textfield cssClass="input-text" id="surveyTillDate" name="calendarFile.surveyTillDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="calcDays();findSurveyDay();"/></td>
<td align="left"><img id="surveyTillDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
</c:if>  
</c:if>
        </tr>
        <tr>
        <td class="listwhitetext" align="right" width="100">Event Name<font color="red" >*</font></td>
        <td align="left" colspan="6"><s:textarea name="calendarFile.activityName" cols="50" rows="3" cssClass="textarea"/></td>
        </tr>
        
    </tbody>
 </table>  
 </div>  
 <div class="bottom-header"><span></span></div>
 </div>
     </div>
     </div>
			<table>
				<tbody>
					<tr>
					<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<fmt:formatDate var="calendarFileCreatedOnFormattedValue" value="${calendarFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="calendarFile.createdOn" value="${calendarFileCreatedOnFormattedValue}" />
							<td><fmt:formatDate value="${calendarFile.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty calendarFile.id}">
								<s:hidden name="calendarFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{calendarFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty calendarFile.id}">
								<s:hidden name="calendarFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="calendarFileUpdatedOnFormattedValue" value="${calendarFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="calendarFile.updatedOn" value="${calendarFileUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${calendarFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
						
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty calendarFile.id}">
								<s:hidden name="calendarFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{calendarFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty calendarFile.id}">
								<s:hidden name="calendarFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table> 
<s:submit cssClass="cssbutton" id="once" cssStyle="width:55px; height:25px; margin-right:3px;" key="button.save" onclick="fillSurveyToDate(); return IsValidTime('noSave');"/>  
<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset"/>
<input type="button" class="cssbutton1" style="width:55px; height:25px" value="Cancel" style="width:87px; height:26px" onclick="location.href='<c:url value="eventList.html?from=driver"/>'" />
    
 </s:form>
 
 <script type="text/javascript">
 	setOnSelectBasedMethods(["forDays(),calcDays()"]);
	setCalendarFunctionality();
 </script>