DELIMITER $$
create trigger redsky.trigger_add_history_refcrate
BEFORE UPDATE on redsky.refcrate
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

      
      IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'refcrate.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;

      
      IF (OLD.pieces <> NEW.pieces ) THEN
       select CONCAT(fieldNameList,'refcrate.pieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pieces,'~') into newValueList;
   END IF;

      
      IF (OLD.cartonType <> NEW.cartonType ) THEN
       select CONCAT(fieldNameList,'refcrate.cartonType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cartonType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cartonType,'~') into newValueList;
   END IF;

      
      IF (OLD.unit1 <> NEW.unit1 ) THEN
       select CONCAT(fieldNameList,'refcrate.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
   END IF;

      
      IF (OLD.unit2 <> NEW.unit2 ) THEN
       select CONCAT(fieldNameList,'refcrate.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
   END IF;



      IF (OLD.unit3 <> NEW.unit3 ) THEN
       select CONCAT(fieldNameList,'refcrate.unit3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit3,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit3,'~') into newValueList;
   END IF;


 
   IF ((OLD.volume <> NEW.volume) or (OLD.volume is null and NEW.volume is not null)
      or (OLD.volume is not null and NEW.volume is null)) THEN
        select CONCAT(fieldNameList,'refcrate.volume~') into fieldNameList;
        IF(OLD.volume is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.volume is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.volume is not null) then
            select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
        END IF;
        IF(NEW.volume is not null) then
            select CONCAT(newValueList,NEW.volume,'~') into newValueList;
        END IF;
    END IF;


 
   IF ((OLD.emptyContWeight <> NEW.emptyContWeight) or (OLD.emptyContWeight is null and NEW.emptyContWeight is not null)
      or (OLD.emptyContWeight is not null and NEW.emptyContWeight is null)) THEN
        select CONCAT(fieldNameList,'refcrate.emptyContWeight~') into fieldNameList;
        IF(OLD.emptyContWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.emptyContWeight is not null) then
            select CONCAT(oldValueList,OLD.emptyContWeight,'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeight is not null) then
            select CONCAT(newValueList,NEW.emptyContWeight,'~') into newValueList;
        END IF;
    END IF;



 
   IF ((OLD.length <> NEW.length) or (OLD.length is null and NEW.length is not null)
      or (OLD.length is not null and NEW.length is null)) THEN
        select CONCAT(fieldNameList,'refcrate.length~') into fieldNameList;
        IF(OLD.length is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.length is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.length is not null) then
            select CONCAT(oldValueList,OLD.length,'~') into oldValueList;
        END IF;
        IF(NEW.length is not null) then
            select CONCAT(newValueList,NEW.length,'~') into newValueList;
        END IF;
    END IF;


 
   IF ((OLD.height <> NEW.height) or (OLD.height is null and NEW.height is not null)
      or (OLD.height is not null and NEW.height is null)) THEN
        select CONCAT(fieldNameList,'refcrate.height~') into fieldNameList;
        IF(OLD.height is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.height is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.height is not null) then
            select CONCAT(oldValueList,OLD.height,'~') into oldValueList;
        END IF;
        IF(NEW.height is not null) then
            select CONCAT(newValueList,NEW.height,'~') into newValueList;
        END IF;
    END IF;



 
   IF ((OLD.width <> NEW.width) or (OLD.width is null and NEW.width is not null)
      or (OLD.width is not null and NEW.width is null)) THEN
        select CONCAT(fieldNameList,'refcrate.width~') into fieldNameList;
        IF(OLD.width is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.width is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.width is not null) then
            select CONCAT(oldValueList,OLD.width,'~') into oldValueList;
        END IF;
        IF(NEW.width is not null) then
            select CONCAT(newValueList,NEW.width,'~') into newValueList;
        END IF;
    END IF;



   CALL add_tblHistory (OLD.id,"refcrate", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$

