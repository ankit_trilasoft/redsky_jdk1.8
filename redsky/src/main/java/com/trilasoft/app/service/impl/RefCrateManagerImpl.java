package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.RefCrateDao;
import com.trilasoft.app.model.RefCrate;
import com.trilasoft.app.service.RefCrateManager;

public class RefCrateManagerImpl extends GenericManagerImpl<RefCrate, Long> implements RefCrateManager {
	RefCrateDao refCrateDao;

	public RefCrateManagerImpl(RefCrateDao refCrateDao) {
		super(refCrateDao);
		this.refCrateDao = refCrateDao;
	}

	public List<RefCrate> findByLastName(String lastName) {
		return refCrateDao.findByLastName(lastName);
	}
	public List refCrateList(String corpID){
		return refCrateDao.refCrateList(corpID);
	}
}
