package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DataCatalogDao;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.service.DataCatalogManager;

public class DataCatalogManagerImpl extends GenericManagerImpl<DataCatalog, Long> implements DataCatalogManager{
	
	DataCatalogDao dataCatalogDao;
	

    public DataCatalogManagerImpl(DataCatalogDao dataCatalogDao) { 
        super(dataCatalogDao); 
        this.dataCatalogDao = dataCatalogDao; 
    }
    
	 public List findMaximumId(){
	    	return dataCatalogDao.findMaximumId(); 
	}

	public List findById(Long id) {
		return dataCatalogDao.findById(id);
	} 
	
	public List<DataCatalog> searchdataCatalog(String tableName, String fieldName, String auditable, String description, String usDomestic, String defineByToDoRule,String visible, String charge){   
        return dataCatalogDao.searchdataCatalog(tableName, fieldName, auditable, description, usDomestic, defineByToDoRule, visible, charge);   
    }
	
	public List configurableTable(boolean b){
		return dataCatalogDao.configurableTable(b);
	}
	
    public List configurableField(String tableName,boolean b){
    	return dataCatalogDao.configurableField(tableName, b);
    }

    public List getList() {
		return dataCatalogDao.getList();
	}
    
    public List getCorpAuthoritiesForComponent(String userCorpID, String componentId) {
		return dataCatalogDao.getCorpAuthoritiesForComponent();
	}

	public boolean getFieldVisibilityAttrbute(String userCorpID, String componentId) {
		return dataCatalogDao.getFieldVisibilityAttrbute(userCorpID, componentId);
	}
	public List checkData(String tableName, String fieldName, String sessionCorpID) {
		return dataCatalogDao.checkData(tableName, fieldName, sessionCorpID);
	}
	public void mergeDataCatalog(DataCatalog dc){
		dataCatalogDao.mergeDataCatalog( dc);
	}
}
