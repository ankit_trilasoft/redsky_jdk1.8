package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.PartnerRefHaulingGrid;

public interface PartnerRefHaulingGridManager  extends GenericManager<PartnerRefHaulingGrid, Long> {
	public List getHaulingGridList(String lowDistance, String highDistance, String rateMile, String rateFlat, String grid, String unit, String countryCode,String corpID);
}
