<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="itemsJbkEquipList.title"/></title> 
    <meta name="heading" content="<fmt:message key='itemsJbkEquipList.heading'/>"/> 
</head>

<s:set name="itemsJbkEquips" value="itemsJbkEquips" scope="request"/> 
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      
	<td width="130" class="detailActiveTabLabel content-tab"><a href="javascript:history.go(-1)">Assigned Crate Items</td>
	<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
	</tr>
</tbody></table>
    
    <display:table name="itemsJbkEquips" class="table" requestURI="" id="itemsJbkEquipList" export="true" pagesize="10"> 
    
    <display:column property="type" sortable="true" titleKey="itemsJbkEquip.type" paramId="id" paramProperty="id"/> 
    <display:column property="actual" sortable="true" titleKey="itemsJbkEquip.actual"/> 
    <display:column property="returned" sortable="true" titleKey="itemsJbkEquip.returned"/> 
    <display:column property="descript" sortable="true" titleKey="itemsJbkEquip.descript"/>
    <display:column property="charge" sortable="true" titleKey="itemsJbkEquip.charge"/> 
    <display:column property="otCharge" sortable="true" titleKey="itemsJbkEquip.otCharge"/>
    <display:column property="dtCharge" sortable="true" titleKey="itemsJbkEquip.dtCharge"/>
    <display:column property="cost" sortable="true" titleKey="itemsJbkEquip.cost"/> 
    <display:column property="packLabour" sortable="true" titleKey="itemsJbkEquip.packLabour"/>
    
    <display:setProperty name="paging.banner.item_name" value="itemsJbkEquip"/> 
    <display:setProperty name="paging.banner.items_name" value="itemsJbkEquip"/>
    
    <display:setProperty name="export.excel.filename" value="ItemsJbkEquip List.xls"/> 
    <display:setProperty name="export.csv.filename" value="ItemsJbkEquip List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="ItemsJbkEquip List.pdf"/> 
</display:table>
 
<c:out value="${buttons}" escapeXml="false" /> 

<script type="text/javascript">
    highlightTableRows("searchitemsJbkEquip"); 
</script>

<script language="JavaScript1.2"> 
try{
if (document.all){ 
document.onkeydown = function (){ 
var key_f5 = 116; // 116 = F5 

if (key_f5==event.keyCode){ 
event.keyCode = 27; 

return false; 
} 
} 
}
}
catch(e){}
</script>  