package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="contractpolicy")
public class ContractPolicy  extends BaseObject{
	
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String policy;
	private String partnerCode;
	private String sectionName;
	private String language;
	private String fileName;
	private String fileSize;
	private String documentType;
	private String documentName;
	private Long parentId;
	private Long docSequenceNumber ;
	private String jobType;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("policy", policy)
				.append("partnerCode", partnerCode)
				.append("sectionName", sectionName)
				.append("language", language).append("fileName", fileName)
				.append("fileSize", fileSize)
				.append("documentType", documentType)
				.append("documentName", documentName)
				.append("parentId", parentId)
				.append("docSequenceNumber", docSequenceNumber)
				.append("jobType", jobType).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ContractPolicy))
			return false;
		ContractPolicy castOther = (ContractPolicy) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(policy, castOther.policy)
				.append(partnerCode, castOther.partnerCode)
				.append(sectionName, castOther.sectionName)
				.append(language, castOther.language)
				.append(fileName, castOther.fileName)
				.append(fileSize, castOther.fileSize)
				.append(documentType, castOther.documentType)
				.append(documentName, castOther.documentName)
				.append(parentId, castOther.parentId)
				.append(docSequenceNumber, castOther.docSequenceNumber)
				.append(jobType, castOther.jobType).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(policy).append(partnerCode)
				.append(sectionName).append(language).append(fileName)
				.append(fileSize).append(documentType).append(documentName)
				.append(parentId).append(docSequenceNumber).append(jobType)
				.toHashCode();
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=5000)
	public String getPolicy() {
		return policy;
	}
	public void setPolicy(String policy) {
		this.policy = policy;
	}
	@Column(length=15)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	@Column
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	@Column
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Column
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	@Column
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	@Column
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	@Column
	public Long getDocSequenceNumber() {
		return docSequenceNumber;
	}
	public void setDocSequenceNumber(Long docSequenceNumber) {
		this.docSequenceNumber = docSequenceNumber;
	}
	@Column
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
}
