<%@ include file="/common/taglibs.jsp"%>  

 <head>    
    <title><fmt:message key="customerFileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='customerFileList.heading'/>"/>   
      <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
     
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-15px;
!margin-top:-10px;
}

div#main {
margin:-5px 0 0;
}

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
 <c:set var="res" value=""/> 
 <c:set var="res1" value=""/> 
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;"  
        onclick="location.href='<c:url value="/editCustomerFile.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px;"  method="search" key="button.search" onclick="return goToSearchCustomerDetail();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>   

<s:hidden name="fileID"  id= "fileID" value=""/> 
<c:set var="fileID"  value=""/>   
<s:form id="searchForm" action="searchCustomerFiles" method="post" validate="true">  
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="csoSortOrder"   value="${csoSortOrder}"/>
<c:set var="csoSortOrder"  value="${csoSortOrder}"/>
<s:hidden name="orderForCso"   value="${orderForCso}"/>
<c:set var="orderForCso"  value="${orderForCso}"/>
<c:if test="${!accessQuotationFromCustomerFile }">
<s:hidden name="customerFile.moveType" />
</c:if>
<s:hidden name="id" />
<s:hidden name="actStatus" />
<c:set var="coordinatr" value="<%=request.getParameter("coordinatr") %>" />
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
<table class="table" style="width:100%;">
<thead>
<tr>
<th style="width:120px" ><fmt:message key="customerFile.sequenceNumber"/></th>
<th style="width:120px"><fmt:message key="customerFile.lastName"/></th>
<th style="width:120px"><fmt:message key="customerFile.firstName"/></th>
<th style="width:120px"><fmt:message key="customerFile.customerEmployer"/></th>
<th style="width:120px"><fmt:message key="customerFile.job"/></th>
<th style="width:120px"><fmt:message key="customerFile.status"/></th>
<c:if test="${accessQuotationFromCustomerFile }">
<th style="width:120px">Move Type</th>
</c:if>
<th style="width:120px"><fmt:message key="customerFile.coordinator"/></th>
<c:if test="${accessQuotationFromCustomerFile }">
<th style="width:131px">Sales Person</th>
</c:if>
<c:if test="${!accessQuotationFromCustomerFile }">
<th style="width:222px">Sales Person</th>
</c:if>



</tr></thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
			    <s:textfield name="customerFile.sequenceNumber" required="true" cssClass="input-text" cssStyle="width:125px" size="15" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="customerFile.lastName" required="true" cssClass="input-text" size="16" cssStyle="width:125px" />
			</td>
			<td width="20" align="left">
			    <s:textfield name="customerFile.firstName" required="true" cssClass="input-text" size="10" cssStyle="width:125px" />
			</td>
			<td width="20" align="left">
			    <s:textfield name="customerFile.billToName" required="true" cssClass="input-text" cssStyle="width:125px" size="20"/>
			</td>
			<td width="20" align="left">
			    <!--<s:textfield name="customerFile.job" required="true" cssClass="input-text" size="8" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    --><s:select name="customerFile.job" list="%{jobs}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue=""/>
			</td>
				<td width="20px" align="left"><!--
			    <s:textfield name="customerFile.status" required="true" cssClass="input-text" size="10" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			--><s:select name="customerFile.status" list="%{custStatus}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue=""/>
			
			</td>
			<c:if test="${accessQuotationFromCustomerFile }">
			</td>
				<td width="20px" align="left"> 
				<s:select id="moveType" name="customerFile.moveType" list="%{moveTypeList}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue=""/>
			 </td>
			</c:if>
			<td width="130px" align="left">
				<c:if test="${coordinatr == null || coordinatr == ''}" >
			    	<!--<s:textfield name="customerFile.coordinator" required="true" cssClass="input-text" size="10" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    	--><s:select name="customerFile.coordinator" list="%{coord}"  cssClass="list-menu"  cssStyle="width:125px"  headerKey="" headerValue=""/>
			    </c:if>
			    <c:if test="${coordinatr != '' && coordinatr != null}" >
			    	<!--<s:textfield name="customerFile.coordinator" required="true" cssClass="input-text" size="10" value="${coordinatr}" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    	--><s:select name="customerFile.coordinator" list="%{coord}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue="" />
			    </c:if>
			</td>
		   <td width="130px" align="left" colspan="5" >
		   <s:select cssClass="list-menu" name="customerFile.estimator" id="estimator" list="%{sale}"   cssStyle="width:125px" headerKey="" headerValue=""  />
		   </td>
			</tr>
			<tr><td style="text-align:left;border-right: 2px solid #74B3DC; margin:0px;padding:0px;" class="listwhitetext" colspan="10">
			<table style="margin:0px;padding:0px;border:none; float:left;" class="listwhitetext">
				<tr>
			<c:if  test="${companies == 'Yes'}">
			<td style="border:none;" class="listwhitetext">
			Company Division<br>
			<s:select name="customerFile.companyDivision" list="%{companyDivis}" cssClass="list-menu" cssStyle="width:129px;margin-right:18px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</c:if>
			<c:if  test="${companies != 'Yes'}">
			<s:hidden name="customerFile.companyDivision"/>
			</c:if>
			<td style="border:none;" class="listwhitetext">
			<fmt:message key='serviceOrder.registrationNumber' />
			&nbsp;<br><s:textfield cssClass="input-text" name="customerFile.registrationNumber" onchange="validateRegNumber(this)" onkeypress="valid(this,'special')" size="16" cssStyle="margin-right:14px;margin-top:2px;" maxlength="20"  />
			</td>
			
			<configByCorp:fieldVisibility componentId="component.field.customerFile.payable">			
			</configByCorp:fieldVisibility>
			<td style="border:none;" class="listwhitetext">
			O/D Options&nbsp;<br>
			<s:select name="cityCountryZipOption" id="cityCountryZipOption" list="%{cityCountryZipSearchOption}" cssClass="list-menu" cssStyle="width:85px;margin-right:5px;margin-top:2px;" onchange="showHideCityCountry();"/>
			</td>
			<td style="border:none;">
			<table style="margin:0px;padding:0px;border:none; float:left;" id="showHideCityOrZip">
				<tr>
					<td style="border:none;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:textfield name="originCityOrZip" cssClass="input-text" cssStyle="width:125px;margin-right:5px;margin-top:2px;"/>
					</td>
					<td style="border:none;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:textfield name="destinationCityOrZip" cssClass="input-text" cssStyle="width:125px;margin-right:0px;margin-top:2px;"/>
					</td>
				</tr>
			</table>
			</td>
			<td style="border:none;">
			<table style="margin:0px;padding:0px;border:none; float:left;" id="showHideCountry">
				<tr>
					<td style="border:none;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:select name="customerFile.originCountry" list="%{ocountry}" cssClass="list-menu" cssStyle="width:125px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
					<td style="border:none;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:select name="customerFile.destinationCountry" list="%{dcountry}" cssClass="list-menu" cssStyle="width:125px;margin-right:0px;margin-top:2px;" headerKey="" headerValue="" /></td>
				</tr>
			</table>
			</td>
			<td style="border:none;" class="listwhitetext">
			Account&nbsp;Name<br>
			<s:textfield name="customerFile.accountName" cssClass="input-text" cssStyle="width:98px;margin-right:0px;margin-top:2px;"/>
			</td>
			
			<td style="border:none;" class="listwhitetext">
			Created&nbsp;On<br>
			<c:if test="${not empty customerFile.createdOn}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.createdOn" /></s:text>
			<s:textfield cssClass="input-text" id="statusDate" name="customerFile.createdOn" value="%{customerFiledate1FormattedValue}" size="7" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/>
			<img id="statusDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			
			</c:if>
			<c:if test="${empty customerFile.createdOn}">
			<s:textfield cssClass="input-text" id="statusDate" name="customerFile.createdOn" size="7" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/>
			<img id="statusDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			</c:if> 
			<td style="border:none;" class="listwhitetext">
			Active Status<br><s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 33px 3px 25px;"/>
			</td>
			<td style="border:none; width:508px;" class="listwhitetext">		
			Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;width:127px;"/>
			</td>
			<td style="border:none;vertical-align:bottom;line-height:37px;">						
			    <c:out value="${searchbuttons}" escapeXml="false" />
			</td>
			</tr>
			</table>  
			       
			</td>
			
		</tr>

		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Customer File List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="customerFileExt" value="customerFiles" scope="request"/>   
<display:table name="customerFileExt" class="table" requestURI="" id="customerFileList" export="true"  pagesize="25" style="width:100%;" >   
  
    <c:if test="${accessQuotationFromCustomerFile}">
       <display:column title="Type" style="width:20px" sortable="true">
	<c:choose>
     <c:when test="${customerFileList.moveType=='Quote'}">
    	 <c:out value="QF" />
     </c:when>
     <c:otherwise>
     <c:out value="CF" />
     </c:otherwise>
     </c:choose>
     </display:column>   
  <display:column  sortable="true" style="width:18px" >
  <img align="top" onclick="serviceOrderBySeqNumber1('${customerFileList.sequenceNumber}','C',this,'${customerFileList.moveType}');" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  	</c:if>
    <display:column property="sequenceNumber" sortable="true" titleKey="customerFile.sequenceNumber" url="/editCustomerFile.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:90px" maxLength="20"/>
    <display:column property="lastName" sortable="true" titleKey="customerFile.lastName" style="width:90px" maxLength="20"/>
    <display:column property="firstName" sortable="true" titleKey="customerFile.firstName" maxLength="20"/>
    <display:column property="billToName" sortable="true" title="Billing Party" style="width:150px" maxLength="19"/> 
     <display:column property="accountName" sortable="true" title="Account&nbsp;Name" style="width:150px" maxLength="19"/> 
     <display:column property="job" sortable="true" titleKey="customerFile.job"/>
    <display:column property="coordinator" style="text-transform: uppercase;" sortable="true" titleKey="customerFile.coordinator" />
    <display:column property="statusDate" sortable="true" titleKey="customerFile.statusDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
    
    <display:column  sortable="true" title="Status">    
   <c:choose>
   <c:when test="${accessQuotationFromCustomerFile && customerFileList.moveType=='Quote'}">
    <c:out value="${customerFileList.quotationStatus}"></c:out>
    </c:when>  
    <c:otherwise>
   <c:out value="${customerFileList.status}"></c:out>
   </c:otherwise>
   </c:choose>
   </display:column>
    <display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>	    
	<%--<configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.notshowcity">
<display:column property="originCity" sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"></display:column> 
</configByCorp:fieldVisibility>--%>
<%--<configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.city">--%>
<display:column  sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"><c:choose><c:when test="${customerFileList.originState!='' && customerFileList.originState!=null }"><c:set var="res1" value="${customerFileList.originCity }, ${customerFileList.originState}"/></c:when><c:otherwise><c:set var="res1" value="${customerFileList.originCity }"/></c:otherwise></c:choose><c:out value="${res1}"></c:out> </display:column> 
<%--</configByCorp:fieldVisibility>--%>
	<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"/> 
	   
    <%--<configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.notshowcity">
<display:column property="destinationCity" sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"></display:column> 
</configByCorp:fieldVisibility>--%>
<%--<configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.city">--%>
<display:column  sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"><c:choose><c:when test="${customerFileList.destinationState!='' && customerFileList.destinationState!=null}"><c:set var="res" value="${customerFileList.destinationCity }, ${customerFileList.destinationState}"/></c:when><c:otherwise><c:set var="res" value="${customerFileList.destinationCity }"/></c:otherwise></c:choose><c:out value="${res}"></c:out> </display:column> 
<%--</configByCorp:fieldVisibility> --%>
    <display:column property="createdOn" sortable="true" titleKey="customerFile.createdOn" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
       
   
    <display:setProperty name="paging.banner.item_name" value="customerfile"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="CustomerFile List.xls"/>   
    <display:setProperty name="export.csv.filename" value="CustomerFile List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="CustomerFile List.pdf"/>   
</display:table>   
<s:hidden name="componentId" value="customerFile" /> 
<sec-auth:authComponent componentId="module.customerFileList.edit"> 
<c:out value="${buttons}" escapeXml="false" /> 
</sec-auth:authComponent>
<c:set var="isTrue" value="false" scope="session"/>
</s:form>

<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['searchForm'].elements['activeStatus'].checked = false;
		document.forms['searchForm'].elements['customerFile.job'].value  = "";
		document.forms['searchForm'].elements['customerFile.coordinator'].value  = "";
		document.forms['searchForm'].elements['customerFile.status'].value  = "";
		document.forms['searchForm'].elements['customerFile.sequenceNumber'].value  = ""; 
		document.forms['searchForm'].elements['customerFile.lastName'].value  = "";
		document.forms['searchForm'].elements['customerFile.firstName'].value  = "";
		document.forms['searchForm'].elements['customerFile.billToName'].value  = "";
		document.forms['searchForm'].elements['customerFile.accountName'].value ="";
		document.forms['searchForm'].elements['customerFile.companyDivision'].value ="";
		document.forms['searchForm'].elements['customerFile.registrationNumber'].value ="";
		document.forms['searchForm'].elements['customerFile.estimator'].value ="";
		document.forms['searchForm'].elements['customerFile.moveType'].value ="";
		document.forms['searchForm'].elements['customerFile.createdOn'].value ="";
		try{
			document.forms['searchForm'].elements['originCityOrZip'].value ="";
			document.forms['searchForm'].elements['destinationCityOrZip'].value ="";
			document.forms['searchForm'].elements['customerFile.originCountry'].value ="";
			document.forms['searchForm'].elements['customerFile.destinationCountry'].value ="";
			document.forms['searchForm'].elements['cityCountryZipOption'].value ="City";
		}catch(e){}
		
}

function goToCustomerDetail(targetValue){
		document.forms['searchForm'].elements['id'].value = targetValue;
		document.forms['searchForm'].action = 'editCustomerFile.html?from=list';
		document.forms['searchForm'].submit();
}
function goToSearchCustomerDetail(){
		return selectSearchField();
		document.forms['searchForm'].action = 'searchCustomerFiles.html';
		document.forms['searchForm'].submit();
}

function validateRegNumber(abc){
var reg=abc.value;
var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
     reg =reg.trim();
     job =job.trim();
      if(job=='UVL' || job=='MVL'){
       if(reg!=''){
        var firstIndex=reg.indexOf("-"); 
        var lastIndex=reg.lastIndexOf("-");
        if(firstIndex!=5||lastIndex!=11||reg.length!=13){
       alert('Registration Number format is incorrect');
       document.forms['customerFileForm'].elements['customerFile.registrationNumber'].value='';
       }
      }
     } 
}

function selectSearchField(){
	    var originCityCountryOrZip="";
	    var destinationCityCountryOrZip="";
		var activeStatus=document.forms['searchForm'].elements['activeStatus'].checked;
		var job=document.forms['searchForm'].elements['customerFile.job'].value;
		var coordinator=document.forms['searchForm'].elements['customerFile.coordinator'].value;
		var status=document.forms['searchForm'].elements['customerFile.status'].value;
		var sequenceNumber=document.forms['searchForm'].elements['customerFile.sequenceNumber'].value; 
		var lastName=document.forms['searchForm'].elements['customerFile.lastName'].value;
		var firstName=document.forms['searchForm'].elements['customerFile.firstName'].value;
		var billToName=document.forms['searchForm'].elements['customerFile.billToName'].value;
		var accountName=document.forms['searchForm'].elements['customerFile.accountName'].value;
		var companyDivis=document.forms['searchForm'].elements['customerFile.companyDivision'].value;
		var reg = document.forms['searchForm'].elements['customerFile.registrationNumber'].value;
		var moveType = document.forms['searchForm'].elements['customerFile.moveType'].value;
		var estimator = document.forms['searchForm'].elements['customerFile.estimator'].value;
		var cityCountryZipVal = document.forms['searchForm'].elements['cityCountryZipOption'].value;
		  if(cityCountryZipVal== 'City' || cityCountryZipVal == 'Zip'){
		   originCityCountryOrZip = document.forms['searchForm'].elements['originCityOrZip'].value;
		   destinationCityCountryOrZip = document.forms['searchForm'].elements['destinationCityOrZip'].value;
		  }else{
		   originCityCountryOrZip = document.forms['searchForm'].elements['customerFile.originCountry'].value;
		   destinationCityCountryOrZip = document.forms['searchForm'].elements['customerFile.destinationCountry'].value;
		  }
		  var createdOn=document.forms['searchForm'].elements['customerFile.createdOn'].value;
		if(activeStatus==false)
		if( reg=='' && job=='' &&  coordinator=='' && status=='' &&  sequenceNumber=='' && lastName=='' &&  firstName=='' && billToName=='' && accountName=='' && companyDivis=='' && moveType=='' && estimator=='' && originCityCountryOrZip=='' && destinationCityCountryOrZip=='' && createdOn=='')
			{
				alert('Please select any one of the search criteria!');	
				return false;	
			}
	}
function serviceOrderBySeqNumber1(sequenceNumber,controlFlag,position,moveType){
	var url="serviceOrderBySeqNumber.html?sequenceNumber="+sequenceNumber+"&controlFlag="+controlFlag+"&decorator=simple&popup=true&moveTypeTemp="+moveType;
	ajax_SoTooltip(url,position);	
}
function showHideCityCountry(){
	var cityCountryZipVal = document.forms['searchForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
	document.forms['searchForm'].elements['originCityOrZip'].value='';
	document.forms['searchForm'].elements['destinationCityOrZip'].value='';
	document.forms['searchForm'].elements['customerFile.originCountry'].value ="";
	document.forms['searchForm'].elements['customerFile.destinationCountry'].value ="";
}
</script>

<script type="text/javascript">   
   try{
    document.forms['searchForm'].elements['customerFile.sequenceNumber'].focus();  
  }
  catch(e){}
   try{
    document.forms['searchForm'].elements['customerFile.coordinator'].value='${coordinatr}';
    }
    catch(e){}
   try{
    <c:if test="${detailPage == true}" >
    	<c:redirect url="/editCustomerFile.html?from=list&id=${customerFileList.id}"  />
	</c:if>
	}
	catch(e){}
</script>	
<script type="text/javascript">   
    highlightTableRows("customerFileList");  
    Form.focusFirstElement($("searchForm"));
	try{
   	if('${activeStatus}'=='true')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = true;
   	}
   	}
   	catch(e){}
   try{
   if('${activeStatus}'=='false')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = false;
   	}
   	}
   	catch(e){}
</script> 
<script type="text/javascript">  

try{
	var companyDivision='${defaultCompanyCode}';
	if(companyDivision!=''){
	 document.forms['searchForm'].elements['customerFile.companyDivision'].value='${companyCode}';
	}
}catch(e){}
try{
	var cityCountryZipVal = document.forms['searchForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
}catch(e){}
</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
 