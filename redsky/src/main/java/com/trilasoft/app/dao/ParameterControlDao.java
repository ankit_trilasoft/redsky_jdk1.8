package com.trilasoft.app.dao;
import com.trilasoft.app.model.ParameterControl;
import java.util.List;
import org.appfuse.dao.GenericDao;
public interface ParameterControlDao extends GenericDao<ParameterControl,Long>{
	public List findUseCorpId();
	public Boolean checkTsftFlag(String parameter2, String sessionCorpID2);
	public Boolean checkHybridFlag(String parameter2, String sessionCorpID2);
	public List<ParameterControl> findByFields(String parameter,String customType,Boolean active,Boolean tsftFlag,Boolean hybridFlag);
	List getAllParameter(String paramater);
	public String getDescription(String parameter,String corpId);

}
