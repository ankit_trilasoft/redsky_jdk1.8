package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.MonthlySalesGoalDao;
import com.trilasoft.app.model.MonthlySalesGoal;


public class MonthlySalesGoalDaoHibernate extends GenericDaoHibernate<MonthlySalesGoal, Long> implements MonthlySalesGoalDao{

	public MonthlySalesGoalDaoHibernate() {
		super(MonthlySalesGoal.class);
	}
	
	public MonthlySalesGoal getMonthlySalesGoalDetails(String partnerCode,String sessionCorpID,String yearVal){
		List monthlySalesGoalList = new ArrayList();
		MonthlySalesGoal monthlySalesGoal = new MonthlySalesGoal();
		try {
			if(yearVal==null || yearVal.equalsIgnoreCase("")){
				monthlySalesGoalList= getHibernateTemplate().find("from MonthlySalesGoal where partnerCode='"+partnerCode+"' and corpId='"+sessionCorpID+"'");
			}else{
				monthlySalesGoalList= getHibernateTemplate().find("from MonthlySalesGoal where partnerCode='"+partnerCode+"' and corpId='"+sessionCorpID+"' and year='"+yearVal+"' ");
			}
		if(!monthlySalesGoalList.isEmpty()){
			monthlySalesGoal = (MonthlySalesGoal)monthlySalesGoalList.get(0);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return monthlySalesGoal;
	}
}
