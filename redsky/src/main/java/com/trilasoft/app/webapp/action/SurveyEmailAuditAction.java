package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.SurveyEmailAudit;
import com.trilasoft.app.service.SurveyEmailAuditManager;

public class SurveyEmailAuditAction extends BaseAction{
	
private SurveyEmailAuditManager surveyEmailAuditManager;
private SurveyEmailAudit surveyEmailAudit;
private List surveyEmailAuditList;
private String sessionCorpID;
private Long id;
static final Logger logger = Logger.getLogger(SurveyEmailAudit.class);
	public SurveyEmailAuditAction()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID=user.getCorpID();	
	}
	public SurveyEmailAudit getSurveyEmailAudit() {
		return surveyEmailAudit;
	}
	public void setSurveyEmailAudit(SurveyEmailAudit surveyEmailAudit) {
		this.surveyEmailAudit = surveyEmailAudit;
	}
	public List getSurveyEmailAuditList() {
		return surveyEmailAuditList;
	}
	public void setSurveyEmailAuditList(List surveyEmailAuditList) {
		this.surveyEmailAuditList = surveyEmailAuditList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setSurveyEmailAuditManager(
			SurveyEmailAuditManager surveyEmailAuditManager) {
		this.surveyEmailAuditManager = surveyEmailAuditManager;
	}
}
