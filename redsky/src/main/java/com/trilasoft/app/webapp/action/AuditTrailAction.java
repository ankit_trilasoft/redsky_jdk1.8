package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.SystemDefaultManager;

public class AuditTrailAction extends BaseAction {

	private Long id;

	private String sessionCorpID;

	private String tableName;

	private List auditTrailList;
	private SystemDefaultManager systemDefaultManager;
	private SystemDefault systemDefault;

	private AuditTrailManager auditTrailManager;
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(AuditTrailAction.class);
	 

	public AuditTrailAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	private String corpIds;
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String ss="";
		if((tableName!=null) && (!tableName.equalsIgnoreCase(""))){
			String tempTableName[] = tableName.split(",");
			for(String str:tempTableName){
				if(ss.equalsIgnoreCase("")){
					ss="'"+str+"'";
				}else{
					ss=ss+",'"+str+"'";
				}
			}
			tableName=ss;
		}
		String from = getRequest().getParameter("from");
		if (from == null) {
			from = "";
		}
		String companyListTSFT = getRequest().getParameter("companyListTSFT");
		if (companyListTSFT == null) {
			companyListTSFT = "";
		}
		String partnerCode = getRequest().getParameter("partnerCode");
		if (partnerCode == null) {
			partnerCode = "";
		}
		String secondID = getRequest().getParameter("secondID");
		if (secondID == null) {
			secondID = "";
		}
		
		if(id==null){
			if(partnerCode!=null && !partnerCode.equalsIgnoreCase("")){
			auditTrailList=auditTrailManager.getAuditAccountContactList(sessionCorpID, partnerCode,tableName);
			}
			
		}else{
		if (companyListTSFT.equalsIgnoreCase("")) {
			auditTrailList = auditTrailManager.getAuditTrailList(id, tableName,
					sessionCorpID,from,secondID);
		} else {
			auditTrailList = auditTrailManager.getAuditTrailListNew(id,
					tableName,
					(sessionCorpID.equalsIgnoreCase("TSFT") ? corpIds
							: sessionCorpID));
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String alertHistoryList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		id = Long.parseLong(systemDefaultManager.findIdByCorpID(sessionCorpID).get(0).toString());
		if (id != null) {
			systemDefault = systemDefaultManager.get(id);
		}
		int daystoManageAlerts=systemDefault.getDaystoManageAlert();
		auditTrailList = auditTrailManager.getAlertHistoryList(getRequest().getRemoteUser().toUpperCase(),daystoManageAlerts);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String containerCartonVehicleRoutingConsigneeList(){
		auditTrailList = auditTrailManager.findContainerCartonVehicleRoutingConsigneeList(id,tableName,sessionCorpID);
		return SUCCESS;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List getAuditTrailList() {
		return auditTrailList;
	}

	public void setAuditTrailList(List auditTrailList) {
		this.auditTrailList = auditTrailList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
		this.auditTrailManager = auditTrailManager;
	}

	public String getCorpIds() {
		return corpIds;
	}
	public void setCorpIds(String corpIds) {
		this.corpIds = corpIds;
	}

	public SystemDefaultManager getSystemDefaultManager() {
		return systemDefaultManager;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

}
