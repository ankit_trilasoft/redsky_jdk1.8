package com.trilasoft.app.webapp.action; 
 
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AuditTrail;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EquipMaterialsCost;
import com.trilasoft.app.model.EquipMaterialsLimits;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.MaterialDto;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.EquipMaterialsCostManager;
import com.trilasoft.app.service.EquipMaterialsLimitsManager;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.app.webapp.util.GridDataProcessor;
public class ItemsJbkEquipAction extends BaseAction implements Preparable{
	
  //  private GenericManager<ItemsJEquip, Long> itemsJEquipManager; 
	private ItemsJbkEquipManager itemsJbkEquipManager;

	private ItemsJEquip itemsJEquip=new ItemsJEquip(); 
    private List itemsJbkEquips;
    private User user;
	private Map<Long,String> invDocMap;
	private Set<Role> roles;
	private Map<String, String> house;
	private String chngMap;
    //Changes done on 5th Sept
	private ItemsJEquipManager itemsJEquipManager;
	private Map<String, String> comapnyDivisionCodeDescp;

	private List itemsJEquips;
	private List materialsList;
	//private ItemsJEquip workTicketManager;
	private String sessionCorpID;
	private static TrackingStatus trackingStatus;
	private Map<String, String> invCategMap=new HashMap<String, String>();
	private Map<String, String> comapnyDivisionCodeDescpCS=new HashMap<String, String>();
	private TrackingStatusManager trackingStatusManager;
	private static Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private String wrehouse;
	private String wrehouseDesc;
	private static CustomerFile customerFile;
	private  Map<String, String> paytype;
	private String backUpOnSave;
	private Billing billing;
	private BillingManager billingManager;    
    private ItemsJbkEquip itemsJbkEquip; 
    private Long id;
    private Long wid;
    private Long cticket;
    private int uticket;
    private String popup;
    private Long ticketW;
    private Long ticket;
    private String valFlag;
    //
    private String retQty;
	private String actQty;
    private String reffBy;   
	private String comment;
    //
	private String listData;
	private String listFieldNames;
    private String listFieldEditability;   
	private String listFieldTypes; 
	private String listIdField; 
	private String itemType;
	private WorkTicket workTicket;
	private WorkTicketManager workTicketManager;
	private Long sid;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private RefMasterManager refMasterManager;
	private Map<String, String> resourceCategory;
	
	private String shipNum;
	private String resourceURL;
	private Long serviceOrderId;
	private String btntype;
	private List resourceList;
	private String type;
	private String resource;
	private String category;
	private String resourceListServer;
	private String categoryListServer;
	private String qtyListServer;
	private String estListServer;
	private String comListServer;
	private String returnedListServer;
	private String actQtyListServer;
	private String costListServer;
	private String actualListServer;
	private String btnType;
	private String hitFlag;
	private String resourceslist;
	private String fromDate;
	private String toDate;
	private String checkFlag;
	private List resourcList;
	private String tempCategory;
	private String tempresource;
	private String checkHubResourceLimit;


	private OperationsResourceLimits operationsResourceLimits;
	private String materialDescription;
	private List material=new ArrayList();
	private String contract;
	private String shipNumber;
	private String day;
	private String tempId;
	private String cost;
	private String comments;
	private String beginDt;
	private String endDt;
	private List jEquipList;
	private int  materialsListSize;
	private List<ItemsJbkEquip> checkDuplicateList;
	private AuditTrailManager auditTrailManager;
	private String usertype;
	private EquipMaterialsLimitsManager equipMaterialsLimitsManager;
	private Long resourceId;
	private EquipMaterialsCostManager equipMaterialsCostManager;
	private EquipMaterialsCost equipMaterialsCost;
	Date currentdate = new Date();
	private EquipMaterialsLimits eqipMaterialsLimits;
	static final Logger logger = Logger.getLogger(ItemsJbkEquipAction.class);
    private String voxmeIntergartionFlag;
    private String branch;
	private String division;
	private String ship;
	private Boolean isActQtyChng;
	private Boolean isRetQtyChng;
	private List<EquipMaterialsLimits> equipMaterialsLimitsList;
	private List<EquipMaterialsCost> equipMaterialsList;
	private Date beginDate;
	private Date endDate;
	private String customerName;
	private String payMethod;
	private String whouse;
	private String wtkDivision;
	private String rowNum;
	private OperationsIntelligenceManager operationsIntelligenceManager ;
	private String oiJobList;
    public String getCategoryResource() throws Exception {
//	   	  String type =  itemsJbkEquip.getType();
//	   	  String resource = itemsJbkEquip.getDescript();
		/* resourceList = new ArrayList();
		 if(resource == null){
			 resource="";
		 }*/
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	      resourceList = itemsJEquipManager.getResource(sessionCorpID,type,resource);
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	      return SUCCESS;
    }
	 
	 private String dayFocus;
	 private String categoryType;
	 public String addRowToResourceTemplateGrid() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 ResourceCommonUtil.addRowToResourceTemplateGrid(itemsJbkEquipManager, shipNumber, sessionCorpID, getRequest().getRemoteUser(), day, type);
		 	/*List<ItemsJbkEquip> jbk = itemsJbkEquipManager.findByShipNumAndDay(shipNumber, day);
		 	
		 	ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
		 	jbkEquip.setType(type);
		 	jbkEquip.setShipNum(shipNumber);
    		jbkEquip.setCorpID(sessionCorpID);
		 	jbkEquip.setCost(Double.valueOf(0));		 	
		 	jbkEquip.setReturned(Double.valueOf(0));
		 	jbkEquip.setActualQty(Integer.valueOf(0));
		 	jbkEquip.setActual(Double.valueOf(0));		 	
		 	jbkEquip.setQty(0);
		 	jbkEquip.setDay(day);
		 	jbkEquip.setCreatedBy(getRequest().getRemoteUser());
    		jbkEquip.setCreatedOn(new Date());
    		jbkEquip.setUpdatedBy(getRequest().getRemoteUser());
    		jbkEquip.setUpdatedOn(new Date());
    		jbkEquip.setTicketTransferStatus(false);
    		jbkEquip.setRevisionInvoice(false);
    		jbkEquip.setEstHour(Double.valueOf(1));
    		
    		for (ItemsJbkEquip itemsJbkEquip : jbk) {
    			jbkEquip.setBeginDate(itemsJbkEquip.getBeginDate());
    			jbkEquip.setEndDate(itemsJbkEquip.getEndDate());
    			break;
			}
		 	
	    	itemsJbkEquipManager.save(jbkEquip);*/ 
	    	resourceURL = "?id="+tempId+"&dayFocus=Day"+day+"&day="+day+"&categoryType="+categoryType;
	    	
	    	saveMessage(getText("Row added for the Day"+day));
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    }  
    public String delete() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	itemsJbkEquipManager.remove(itemsJbkEquip.getId()); 
        saveMessage(getText("itemsJbkEquip.deleted")); 
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
     
        return SUCCESS; 
    } 
    
	 public ItemsJbkEquipAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
	        this.usertype=user.getUserType();
		}
	 public Map<String, String> itemsJequipMap;
	 public void prepare() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getComboList(sessionCorpID);				
				company=companyManager.findByCorpID(sessionCorpID).get(0);
                 if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
				if(company!=null){
					if(company.getVoxmeIntegration()!=null){
						voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
						}
					}
				itemsJequipMap=itemsJbkEquipManager.getItemsjequip(sessionCorpID);
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} 
	 
	 private String estHour;
	 private String workTicketStatus;
	 private String workTicketID;
	 @SkipValidation
	public String autoSaveResouceTemplateAjax(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 	
		checkDuplicateList = itemsJEquipManager.checkDuplicateResourceDayBasis(resource, sessionCorpID, day,shipNumber);
		if (checkDuplicateList != null && checkDuplicateList.size() > 0 && checkDuplicateList.get(0) != null  ) {
			for (ItemsJbkEquip itemsjbk : checkDuplicateList) {
				jEquipList = new ArrayList();
				if(itemsjbk.getId().toString().equals(tempId) && (itemsjbk.getDescript().toString().equals(resource))){
					jEquipList.add("YES");
				}else {
					return SUCCESS; 
				}
			}
		}else{
			jEquipList = new ArrayList();
			jEquipList.add("YES");
		}
//		jEquipList = itemsJEquipManager.checkDuplicateResource(resource, sessionCorpID);
			
		 ItemsJbkEquip jbkEquip = itemsJbkEquipManager.get(Long.valueOf(tempId));
			jbkEquip.setType(category);
			jbkEquip.setDescript(resource);
			try {
				double d = Double.valueOf(cost);
				jbkEquip.setQty((int)d);
				jbkEquip.setEstHour(Double.valueOf(estHour));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
			jbkEquip.setComments(comments);
			//jbkEquip.setComments(StringUtils.escape(comments));
			
			jbkEquip.setRevisionInvoice(false);
			/*jbkEquip.setTicket(null);
			jbkEquip.setWorkTicketID(null);
			jbkEquip.setTicketTransferStatus(false);*/
			if(beginDt !=null && endDt != null){
				DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					jbkEquip.setBeginDate(f.parse(beginDt));
					jbkEquip.setEndDate(f.parse(endDt));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
/*			if (jEquipList != null && jEquipList.size() > 0 && jEquipList.get(0) != null  ) {
				itemsJbkEquipManager.save(jbkEquip);
			}*/
			
			itemsJbkEquipManager.save(jbkEquip);
			if(jbkEquip.getWorkTicketID() != null && !"".equals(jbkEquip.getWorkTicketID().toString().trim()) && jbkEquip.getTicketTransferStatus() == true){
				itemsJbkEquipManager.deleteItemsJbkResource(jbkEquip,false);
			}
			if(workTicketID != null && !"".equalsIgnoreCase(workTicketID) && workTicketID.trim().length() > 0){
				Long id = Long.valueOf(workTicketID);
				if("PENDING".equals(workTicketStatus)){
					WorkTicket wt = workTicketManager.get(id);
					if(!"P".equals(wt.getTargetActual().toString())){
						wt.setTargetActual("P");
						workTicketManager.save(wt);
					}
				}else{
					WorkTicket wt = workTicketManager.get(id);
					if("P".equals(wt.getTargetActual().toString())){
						List<MaterialDto> list = itemsJEquipManager.searchResourceList("",wt.getTicket().toString(),"", sessionCorpID,"");
						OperationsHubLimits operationsHubLimits  = workTicketManager.getOperationsHub(wt.getWarehouse(), sessionCorpID);
						Company company= companyManager.findByCorpID(sessionCorpID).get(0);
						String ticketPendingStatus=null;
						String chkDate1="";
	       		    	String chkDate2="";
	       		    	if (!(wt.getDate1() == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(wt.getDate1()));
	       					chkDate1 = nowYYYYMMDD1.toString();
	       				}

	       				if (!(wt.getDate2() == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(wt.getDate2()));
	       					chkDate2 = nowYYYYMMDD2.toString();
	       				}
	       				
						for (MaterialDto materialDto: list) {
							 List dailyResourceLimitList = itemsJbkEquipManager.getDailyResourceLimit(materialDto.getCheckType(), materialDto.getDescript(), chkDate1, chkDate2, operationsHubLimits.getHubID(), sessionCorpID);
		   					   int dailyResourceLimit = 0;
							   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && (dailyResourceLimitList.get(0)!=null)){
								   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
								   dailyResourceLimit = dailyQty.intValue();							
							   }
							   if(Integer.parseInt(materialDto.getQty()) > dailyResourceLimit && dailyResourceLimit>0 && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
								   ticketPendingStatus="Y";
							   }
						}
						if(!"Y".equals(ticketPendingStatus)){
							wt.setTargetActual("T");
							workTicketManager.save(wt);
						}
					}
				}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	 
	public String autoSaveDateAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		itemsJbkEquipManager.saveDateAjax(shipNum,day,beginDt,endDt);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	    public String list() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	getComboList(sessionCorpID);
	    	itemsJbkEquips = itemsJbkEquipManager.getAll();
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    } 
	    
	    @SkipValidation 
	    public String resouceTemplate() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    		
	    	resourceURL = "?id="+tempId+"&template=YES";
	    	operationsIntelligenceManager.getDeleteWorkOrder(shipNumber,sessionCorpID);
	    	ResourceCommonUtil.addResourceTemplate(itemsJEquipManager, itemsJbkEquipManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser());
	    	//String maxOrder=ResourceCommonUtil.findMaxWorkOrder(operationsIntelligenceManager,shipNumber,sessionCorpID);
	    	String maxWorkOrder = operationsIntelligenceManager.FindMaxWorkOrder(shipNumber);
	    	ResourceCommonUtil.addResourceTemplate1(itemsJEquipManager, operationsIntelligenceManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser(),maxWorkOrder);
	    	operationsIntelligenceManager.updateExpenseRevenueConsumables(shipNumber);
	    	operationsIntelligenceManager.updateExpenseRevenueAB5Surcharge(shipNumber);
	    	/*List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
	    	int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
	    	if(!resourceList.isEmpty()){
	    		Iterator it=resourceList.iterator();
	    		while(it.hasNext()){
		    	    Object []row= (Object [])it.next();
			    		ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
			    		jbkEquip.setShipNum(shipNumber);
			    		jbkEquip.setCorpID(sessionCorpID);
			    		jbkEquip.setType(row[0].toString());
			    		jbkEquip.setDescript(row[1].toString());
			    		jbkEquip.setCost(Double.valueOf(row[2].toString()));
			    		try {
			    			double d = Double.valueOf(row[2].toString());
							jbkEquip.setQty((int)d);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						jbkEquip.setEstHour(Double.valueOf(1));
			    		jbkEquip.setCreatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setCreatedOn(new Date());
			    		jbkEquip.setUpdatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setUpdatedOn(new Date());
			    		jbkEquip.setDay(String.valueOf(day+1));
			    		jbkEquip.setTicketTransferStatus(false);
			    		jbkEquip.setRevisionInvoice(false);
			    		itemsJbkEquipManager.save(jbkEquip);
	    		}
	    	}*/
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    } 
	    
	    @SkipValidation 
	    public String resouceTemplateForAccPortal() {
	    	
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    		
	    	resourceURL = "?id="+tempId+"&template=YES";
	    	operationsIntelligenceManager.getDeleteWorkOrder(shipNumber,sessionCorpID);
	    	//ResourceCommonUtil.findResourceTemplateForAccPortal(itemsJEquipManager, itemsJbkEquipManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser());
	    	String maxOrder=ResourceCommonUtil.findMaxWorkOrder(operationsIntelligenceManager,shipNumber,sessionCorpID);
	    	ResourceCommonUtil.findResourceTemplateForAccPortal(itemsJEquipManager, operationsIntelligenceManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser(),maxOrder,tempId);
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    } 
	    
	    @SkipValidation 
	    public String resouceTemplateSO() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    		
	    	resourceURL = "?id="+tempId+"&template=YES";
	    	
	    	List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
	    	int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
	    	if(!resourceList.isEmpty()){
	    		Iterator it=resourceList.iterator();
	    		while(it.hasNext()){
		    	    Object []row= (Object [])it.next();
			    		ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
			    		jbkEquip.setShipNum(shipNumber);
			    		jbkEquip.setCorpID(sessionCorpID);
			    		jbkEquip.setType(row[0].toString());
			    		jbkEquip.setDescript(row[1].toString());
			    		jbkEquip.setCost(Double.valueOf(row[2].toString()));
			    		try {
			    			double d = Double.valueOf(row[2].toString());
							jbkEquip.setQty((int)d);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						jbkEquip.setEstHour(Double.valueOf(1));
			    		jbkEquip.setCreatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setCreatedOn(new Date());
			    		jbkEquip.setUpdatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setUpdatedOn(new Date());
			    		jbkEquip.setDay(String.valueOf(day+1));
			    		jbkEquip.setTicketTransferStatus(false);
			    		jbkEquip.setRevisionInvoice(false);
			    		itemsJbkEquipManager.save(jbkEquip);
	    		}
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    } 
	  @SkipValidation 
	  public String itemsJbkResourceList() {
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	    	serviceOrder=serviceOrderManager.get(sid);
	    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	    	trackingStatus=trackingStatusManager.get(sid);
	   		miscellaneous = miscellaneousManager.get(sid);
	   		billing = billingManager.get(sid);
	   		customerFile=serviceOrder.getCustomerFile(); 
	    	materialsList=new ArrayList();
	    	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	    	String contract="";
	    	workTicket = workTicketManager.get(id);
	  	    wrehouse=workTicket.getWarehouse();
	   	    if(wrehouse!=null && wrehouse!=""){
	   	    	wrehouseDesc= itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID).get(0).toString();
	   	    }
	   	  	String ticket=workTicket.getTicket().toString();
	   	  	materialsList = itemsJEquipManager.searchResourceList(resource,ticket,contract, sessionCorpID,category);
	   	   if(materialsList!=null && !materialsList.isEmpty()){
	   		materialsListSize=materialsList.size();  
	   	   }
	   	  	material = itemsJEquipManager.findMaterialByCategory(category,sessionCorpID);
	   	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	   	  	return SUCCESS;   
	  }
	  private String returnAjaxStringValue;
	  @SkipValidation
		public String getResourceCategoryValueAjax(){
			try {
				returnAjaxStringValue = itemsJEquipManager.getResourceCategoryValue(sessionCorpID,ticket,type,resource);
			} catch (Exception e) {
		    	 e.printStackTrace();
			}
			return SUCCESS;
		}
	  @SkipValidation 
	  public String checkResourcesLimit() {
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 	
	    	String contract="";
	    	workTicket = workTicketManager.get(id);
	    	company= companyManager.findByCorpID(sessionCorpID).get(0);
	    	operationsResourceLimits  = itemsJbkEquipManager.getOperationsResourceHub(workTicket.getWarehouse(), sessionCorpID);
	    	if (!(workTicket.getDate1() == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
				fromDate = nowYYYYMMDD1.toString();
			}

			if (!(workTicket.getDate2() == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
				toDate = nowYYYYMMDD2.toString();
			}
			String checkLimit = itemsJbkEquipManager.updateItemsJbkResource(resourceListServer,categoryListServer, qtyListServer, returnedListServer, actQtyListServer, costListServer, actualListServer, sessionCorpID, workTicket, fromDate, toDate,operationsResourceLimits.getHubId(), "chkLimit", estListServer,comListServer,getRequest().getRemoteUser());
			if(checkLimit!=null && checkLimit.equalsIgnoreCase("Greater") && !checkHubResourceLimit.equals("Y")&& (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
					checkFlag="Y";	
					return SUCCESS; 
			}
			String[] str= new String[25];		
			String[] strRes= new String[25];
			double dailyResourceLimit=0;
			double usedEstQty=0;
			if(resourceslist!=null && (!(resourceslist.trim().equals("")))){
			str = resourceslist.split("~");
			for (String string : str) {
				strRes=string.split(":");
				Boolean wareHouseCheck = false;
				wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
				if(wareHouseCheck == true){
				  if(!(workTicket.getTargetActual().equalsIgnoreCase("C")) && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
					if(operationsResourceLimits!=null){
					List dailyResourceLimitList  = itemsJbkEquipManager.getDailyResourceLimit(strRes[0].toString().trim(),strRes[1].toString().trim(),fromDate, toDate,operationsResourceLimits.getHubId(), sessionCorpID);
					if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && (dailyResourceLimitList.get(0)!=null)){
						dailyResourceLimit = Double.parseDouble(dailyResourceLimitList.get(0).toString());
					}
					List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(strRes[0].toString().trim(),strRes[1].toString().trim(),fromDate, toDate,operationsResourceLimits.getHubId(),workTicket.getService(), sessionCorpID,workTicket.getTicket());
					if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && (usedEstQuanitityList.get(0)!=null)){
						usedEstQty = Double.parseDouble(usedEstQuanitityList.get(0).toString());
					}
					double tempVal = 0.00;
					if(strRes[2]!=null && !strRes[2].toString().trim().equalsIgnoreCase("")){
						tempVal=Double.parseDouble(strRes[2].toString().trim());
					}
				   /*	String checkValid = itemsJbkEquipManager.checkIsExist(itemsJbkId, category, resource, sessionCorpID);*/
					if(dailyResourceLimit>0){
						availableQty=dailyResourceLimit-usedEstQty;
  					}
					usedEstQty = usedEstQty + tempVal;
					if((usedEstQty > dailyResourceLimit) && dailyResourceLimit>0 && !checkHubResourceLimit.equals("Y")&& (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
					//if(usedEstQty > dailyResourceLimit){
						checkFlag = "Y";
					}			
				   }
				 }
				}
			  }	
			}
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   		return SUCCESS;   
	  } 
	  private double availableQty;
	  private String avqty;
	  	  @SkipValidation 
	  	  public String checkResourcesAvailable() {
	  		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	  			
	  	    	workTicket = workTicketManager.get(id);			
	  	    	operationsResourceLimits  = itemsJbkEquipManager.getOperationsResourceHub(workTicket.getWarehouse(), sessionCorpID);
	  	    	if (!(workTicket.getDate1() == null)) {
	  				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	  				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
	  				fromDate = nowYYYYMMDD1.toString();
	  			}

	  			if (!(workTicket.getDate2() == null)) {
	  				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	  				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
	  				toDate = nowYYYYMMDD2.toString();
	  			}
	  			double dailyResourceLimit=0;
	  			double usedEstQty=0;
	  			Boolean wareHouseCheck = false;
	  				wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
	  				if(wareHouseCheck == true){
	  				  if(!(workTicket.getTargetActual().equalsIgnoreCase("C")) && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
	  					if(operationsResourceLimits!=null){
	  					List dailyResourceLimitList  = itemsJbkEquipManager.getDailyResourceLimit(category,resource,fromDate, toDate,operationsResourceLimits.getHubId(), sessionCorpID);
	  					if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && (dailyResourceLimitList.get(0)!=null)){
	  						dailyResourceLimit = Double.parseDouble(dailyResourceLimitList.get(0).toString());
	  					}else{
	  						avqty="Available";
	  					}
	  					List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(category,resource,fromDate, toDate,operationsResourceLimits.getHubId(),workTicket.getService(), sessionCorpID,workTicket.getTicket());
	  					if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && (usedEstQuanitityList.get(0)!=null)){
	  						usedEstQty = Double.parseDouble(usedEstQuanitityList.get(0).toString());
	  					}
	  					if(dailyResourceLimit>0)
	  					{availableQty=dailyResourceLimit-usedEstQty;
	  					avqty=availableQty+"";}
	  				   }else{
	  					   avqty="Available";
	  				   }
	  			  }	
	  			}
	  		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  	   		return SUCCESS;   
	  	  } 

	  
	  @SkipValidation 
	  public String saveItemsJbkResourceList() {
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	    	serviceOrder=serviceOrderManager.get(sid);
	    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	    	trackingStatus=trackingStatusManager.get(sid);
	   		miscellaneous = miscellaneousManager.get(sid);
	   		billing = billingManager.get(sid);
	   		customerFile=serviceOrder.getCustomerFile();
	   		company= companyManager.findByCorpID(sessionCorpID).get(0);
	    	materialsList=new ArrayList();
	    	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	    	String contract="";
	    	workTicket = workTicketManager.get(id);
	  	    wrehouse=workTicket.getWarehouse();
	   	    if(wrehouse!=null && wrehouse!=""){
	   	    	wrehouseDesc= itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID).get(0).toString();
	   	    }				
	    	operationsResourceLimits  = itemsJbkEquipManager.getOperationsResourceHub(workTicket.getWarehouse(), sessionCorpID);
	    	if (!(workTicket.getDate1() == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
				fromDate = nowYYYYMMDD1.toString();
			}

			if (!(workTicket.getDate2() == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
				toDate = nowYYYYMMDD2.toString();
			}
			if(operationsResourceLimits!=null){
	    	String checkLimit = itemsJbkEquipManager.updateItemsJbkResource(resourceListServer,categoryListServer, qtyListServer, returnedListServer, actQtyListServer, costListServer, actualListServer, sessionCorpID, workTicket, fromDate, toDate,operationsResourceLimits.getHubId(), "", estListServer,comListServer,getRequest().getRemoteUser());
	    	if(checkLimit!=null && checkLimit.equalsIgnoreCase("toGoP") && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
	    		if(!(workTicket.getTargetActual().toString().equalsIgnoreCase("P"))&& (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
	    			AuditTrail auditTrail = new AuditTrail();
	    			if(workTicket.getTargetActual().equalsIgnoreCase("A")){
						auditTrail.setOldValue("Actual");
					}else if(workTicket.getTargetActual().equalsIgnoreCase("R")){
						auditTrail.setOldValue("Rejected");
					}else{
						auditTrail.setOldValue("Target");
					}
	    			workTicket.setTargetActual("P");
					workTicket = workTicketManager.save(workTicket);
					serviceOrder = workTicket.getServiceOrder();
					String warehouseManager = refMasterManager.findByParameterHouse(sessionCorpID, workTicket.getWarehouse(), "HOUSE");
					auditTrail.setCorpID(workTicket.getCorpID());
					auditTrail.setXtranId(workTicket.getId().toString());
					auditTrail.setTableName("workticket");
					auditTrail.setFieldName(workTicket.getTargetActual());
					auditTrail.setDescription("Warehouse Manager");
				
					auditTrail.setNewValue("Pending");
					auditTrail.setUser(getRequest().getRemoteUser());
					auditTrail.setDated(new Date());
					auditTrail.setAlertRole("Warehouse Manager");
					auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
					auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
					if(warehouseManager!=null){
						auditTrail.setAlertUser(warehouseManager);
					}else{
						auditTrail.setAlertUser("");
					}
					auditTrail.setIsViewed(false);			
					auditTrailManager.save(auditTrail);
				}
	    	}
	    	if(checkLimit.equalsIgnoreCase("toGoT") && !checkHubResourceLimit.equals("Y") && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
	    		if((workTicket.getTargetActual().toString().equalsIgnoreCase("P")) || (workTicket.getTargetActual().toString().equalsIgnoreCase("R"))){
	    			AuditTrail auditTrail = new AuditTrail();
	    			if(workTicket.getTargetActual().equalsIgnoreCase("R")){
						auditTrail.setOldValue("Rejected");
					}else{
						auditTrail.setOldValue("Pending");
					}
	    			workTicket.setTargetActual("T");
					workTicket = workTicketManager.save(workTicket);
					serviceOrder = workTicket.getServiceOrder();
					auditTrail.setCorpID(workTicket.getCorpID());
					auditTrail.setXtranId(workTicket.getId().toString());
					auditTrail.setTableName("workticket");
					auditTrail.setFieldName(workTicket.getTargetActual());
					auditTrail.setDescription("Coordinator");
					auditTrail.setOldValue("Pending");
					auditTrail.setNewValue("Target");
					auditTrail.setUser(getRequest().getRemoteUser());
					auditTrail.setDated(new Date());
					auditTrail.setAlertRole("Coordinator");
					auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
					auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
				    auditTrail.setAlertUser(serviceOrder.getCoordinator());
					auditTrail.setIsViewed(false);			
					auditTrailManager.save(auditTrail);
				}
	    	}
	    	if(checkLimit.equalsIgnoreCase("toGoT") && checkHubResourceLimit.equals("Y") && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
	    		if((workTicket.getTargetActual().toString().equalsIgnoreCase("R"))){
					workTicket.setTargetActual("P");
					workTicket = workTicketManager.save(workTicket);
					serviceOrder = workTicket.getServiceOrder();
					String warehouseManager = refMasterManager.findByParameterHouse(sessionCorpID, workTicket.getWarehouse(), "HOUSE");
					AuditTrail auditTrail = new AuditTrail();
					auditTrail.setCorpID(workTicket.getCorpID());
					auditTrail.setXtranId(workTicket.getId().toString());
					auditTrail.setTableName("workticket");
					auditTrail.setFieldName(workTicket.getTargetActual());
					auditTrail.setDescription("Warehouse Manager");
					auditTrail.setOldValue("Rejected");
					auditTrail.setNewValue("Pending");
					auditTrail.setUser(getRequest().getRemoteUser());
					auditTrail.setDated(new Date());
					auditTrail.setAlertRole("Warehouse Manager");
					auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
					auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
					if(warehouseManager!=null){
						auditTrail.setAlertUser(warehouseManager);
					}else{
						auditTrail.setAlertUser("");
					}
					auditTrail.setIsViewed(false);			
					auditTrailManager.save(auditTrail);
				}
	    	}
	  }
	    	String[] str= new String[25];		
			String[] strRes= new String[25];
			double dailyResourceLimit=0;
			double usedEstQty=0;
			if(resourceslist!=null && (!(resourceslist.trim().equals("")))){
			str = resourceslist.split("~");
			for (String string : str) {
				strRes=string.split(":");
				
				Boolean wareHouseCheck = false;
				wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
				String tempCat = strRes[0].toString().trim();
				String tempRes = strRes[1].toString().trim();
				
				int tempQty = (strRes[2]==null || "".equals(strRes[2].toString().trim()))?0 : Integer.parseInt(strRes[2].toString().trim());
				String tempCom = (strRes[3]==null)?"" : strRes[3].toString().trim();
				double tempEst = (strRes[4]==null && strRes[4].equals(""))?0.0 : Double.parseDouble(strRes[4].toString().trim());		
				double tempRet = (strRes[5]==null && strRes[5].equals(""))?0.0 : Double.parseDouble(strRes[5].toString().trim());
				int tempAcq = (strRes[6]==null && strRes[6].equals(""))? 0 : Integer.parseInt(strRes[6].toString().trim());
				double tempCos = (strRes[7]==null && strRes[7].equals(""))? 0.0 : Double.parseDouble(strRes[7].toString().trim());
				double tempAct = (strRes[8]==null && strRes[8].equals(""))? 0.0 : Double.parseDouble(strRes[8].toString().trim());
				
				/*int tempQty=0;
				if(strRes[2]==null && strRes[2].equals("")){
					tempQty=0;
				}else{
					tempQty = Integer.parseInt(strRes[2].toString().trim());
				}
				
				double tempEst=0.0;
				if(strRes[4]==null && strRes[4].equals("")){
					tempEst=0.0;
				}else{
					tempEst = Double.parseDouble(strRes[4].toString().trim());
				}
				double tempRet=0.0;
				if(strRes[5]==null && strRes[5].equals("")){
					tempRet=0.0;
				}else{
					tempRet = Double.parseDouble(strRes[5].toString().trim());
				}
				int tempAcq=0;
				if(strRes[6]==null && strRes[6].equals("")){
					tempAcq=0;
				}else{
					tempAcq = Integer.parseInt(strRes[6].toString().trim());
				}
				double tempCos=0.0;
				if(strRes[7]==null && strRes[7].equals("")){
					tempCos=0.0;
				}else{
					tempCos = Double.parseDouble(strRes[7].toString().trim());
				}
				double tempAct=0.0;
				if(strRes[8]==null && strRes[8].equals("")){
					tempAct=0.0;
				}else{
					tempAct = Double.parseDouble(strRes[8].toString().trim());
				}*/
				
				if(wareHouseCheck == true && (!(workTicket.getTargetActual().equalsIgnoreCase("C"))) && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
					if(operationsResourceLimits!=null){
				
					List dailyResourceLimitList  = itemsJbkEquipManager.getDailyResourceLimit(tempCat,tempRes,fromDate, toDate,operationsResourceLimits.getHubId(), sessionCorpID);
					if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && (dailyResourceLimitList.get(0)!=null)){
						dailyResourceLimit = Double.parseDouble(dailyResourceLimitList.get(0).toString());
					}
					List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(tempCat,tempRes,fromDate, toDate,operationsResourceLimits.getHubId(),workTicket.getService(), sessionCorpID,workTicket.getTicket());
					if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && usedEstQuanitityList.get(0)!=null && (!(usedEstQuanitityList.get(0).toString().trim().equals("")))){
						usedEstQty = Double.parseDouble(usedEstQuanitityList.get(0).toString());
					}
					double availableQuantity=0;
					if(dailyResourceLimit>0){
						availableQuantity=dailyResourceLimit-usedEstQty;
  					}
					usedEstQty = usedEstQty + tempQty;
					if((usedEstQty > dailyResourceLimit) && dailyResourceLimit>0 && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
						if(!(workTicket.getTargetActual().toString().equalsIgnoreCase("P"))){
							workTicket.setTargetActual("P");
							workTicketManager.save(workTicket);
						}
					}
					if((usedEstQty < dailyResourceLimit) && dailyResourceLimit>0 && !checkHubResourceLimit.equals("Y") && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
						if((workTicket.getTargetActual().toString().equalsIgnoreCase("P") && availableQuantity<0) || (workTicket.getTargetActual().toString().equalsIgnoreCase("R"))){
							workTicket.setTargetActual("T");
							workTicketManager.save(workTicket);
						}
					}
					if((usedEstQty < dailyResourceLimit) && dailyResourceLimit>0 && checkHubResourceLimit.equals("Y") && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
						if(workTicket.getTargetActual().toString().equalsIgnoreCase("R")){
							workTicket.setTargetActual("P");
							workTicketManager.save(workTicket);
						}
					}
					ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
				    itemsJbkEquip.setCorpID(workTicket.getCorpID());
	        		itemsJbkEquip.setWorkTicketID(workTicket.getId());
	        		itemsJbkEquip.setFlag("T");
	        		itemsJbkEquip.setType(tempCat);
	        		itemsJbkEquip.setDescript(tempRes);
	        		itemsJbkEquip.setQty(tempQty);
	        		itemsJbkEquip.setEstHour(tempEst);
	        		itemsJbkEquip.setComments(tempCom);
	        		itemsJbkEquip.setReturned(tempRet);
	        		itemsJbkEquip.setActualQty(tempAcq);
	        		itemsJbkEquip.setCost(tempCos);
	        		itemsJbkEquip.setActual(tempAct);
	
	        		itemsJbkEquip.setTicket(workTicket.getTicket());
	        		itemsJbkEquip.setShipNum(workTicket.getShipNumber());
	        		itemsJbkEquip.setGl("");
	        		//itemsJbkEquip.setSeqNum(workTicket.getSequenceNumber());
	        		itemsJbkEquip.setBillCrew("");
	        		itemsJbkEquip.setDtCharge(new Double(0.00));
	        		itemsJbkEquip.setCharge(new Double(0.00));
	        		itemsJbkEquip.setIdNum(0);
	        		itemsJbkEquip.setOtCharge(new Double(0.00));
	        		itemsJbkEquip.setPackLabour(new Double(0.00));
	        		itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
	        		itemsJbkEquip.setCreatedOn(new Date());
	        		itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
	        		itemsJbkEquip.setUpdatedOn(new Date());
	        		itemsJbkEquipManager.save(itemsJbkEquip); 				
				   }
				 }else{					 	
					 	ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
					    itemsJbkEquip.setCorpID(workTicket.getCorpID());
		        		itemsJbkEquip.setWorkTicketID(workTicket.getId());
		        		itemsJbkEquip.setFlag("T");
		        		itemsJbkEquip.setType(tempCat);
		        		itemsJbkEquip.setDescript(tempRes);
		        		itemsJbkEquip.setQty(tempQty);
		        		itemsJbkEquip.setReturned(tempRet);
		        		itemsJbkEquip.setActualQty(tempAcq);
		        		itemsJbkEquip.setCost(tempCos);
		        		itemsJbkEquip.setActual(tempAct);
		        		itemsJbkEquip.setTicket(workTicket.getTicket());
		        		itemsJbkEquip.setShipNum(workTicket.getShipNumber());
		        		itemsJbkEquip.setGl("");
		        		//itemsJbkEquip.setSeqNum(workTicket.getSequenceNumber());
		        		itemsJbkEquip.setBillCrew("");
		        		itemsJbkEquip.setDtCharge(new Double(0.00));
		        		itemsJbkEquip.setCharge(new Double(0.00));
		        		itemsJbkEquip.setIdNum(0);
		        		itemsJbkEquip.setOtCharge(new Double(0.00));
		        		itemsJbkEquip.setPackLabour(new Double(0.00));
		        		itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
		        		itemsJbkEquip.setCreatedOn(new Date());
		        		itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
		        		itemsJbkEquip.setUpdatedOn(new Date());
		        		itemsJbkEquipManager.save(itemsJbkEquip);
				 }
				}
			  }	    	
	   	  	String ticket=workTicket.getTicket().toString();
	   	    materialsList = itemsJEquipManager.searchResourceList(resource,ticket,contract, sessionCorpID,category);
	   	 if(materialsList!=null && !materialsList.isEmpty()){
	   	    materialsListSize=materialsList.size();
	   	 }
	   	    material = itemsJEquipManager.findMaterialByCategory(category,sessionCorpID);
	   	  	hitFlag="1";	   	 
	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	   		return SUCCESS;   
	  }
	  
	  @SkipValidation	 	  
	  public String findInvDescriptionList(){		  
		  invDocMap=itemsJEquipManager.findInvDescriptionList(branch,division,category,sessionCorpID);		  
		  return SUCCESS;
	  }
private String countCheck,checkLimit;
private OperationsHubLimits operationsHubLimits;
private Company company;
private String exceedDate = "";
private CompanyManager companyManager;

private BigDecimal actualcost;

	public BigDecimal getActualcost() {
	return actualcost;
	}
	public void setActualcost(BigDecimal actualcost) {
	this.actualcost = actualcost;
	}

@SkipValidation	  
	  public String checkHubLimit() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			boolean isNew = (id == null);
			String key = "";
			countCheck="";
			checkLimit="N";
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			Boolean wareHouseCheck = false;
			workTicket= workTicketManager.get(id);
			wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
			if(wareHouseCheck == true){
				if(!(workTicket.getTargetActual().equalsIgnoreCase("C")) && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
					user = userManager.getUserByUsername(getRequest().getRemoteUser());
					roles = user.getRoles();				
					operationsHubLimits  = workTicketManager.getOperationsHub(workTicket.getWarehouse(), sessionCorpID);
					if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
			    		
					}else{
	    	   		/*Boolean checkDate1 = false;
	    	   		if(!isNew){
	    			   checkDate1 = workTicketManager.checkDate1(workTicket.getWarehouse(), workTicket.getService(), workTicket.getDate1(), id, sessionCorpID, workTicket.getEstimatedCubicFeet(), workTicket.getEstimatedWeight(), workTicket.getDate2());
	    	   		}*/    		      		   		 
	    		    //if(isNew || (!isNew && checkDate1== true)){
	    			   if (operationsHubLimits != null){
	    				   Long dailyCount = Long.parseLong(operationsHubLimits.getDailyOpHubLimit());
	    				   Long dailyCountPC = Long.parseLong(operationsHubLimits.getDailyOpHubLimitPC());
	    				   Double dailyMaxEstWt = 0.00;
	    				   Double dailyMaxEstVol = 0.00;
	    				   if(operationsHubLimits.getDailyMaxEstWt() != null){
	    				        dailyMaxEstWt = Double.parseDouble(operationsHubLimits.getDailyMaxEstWt().toString());
	    				   }
	    				   if(operationsHubLimits.getDailyMaxEstVol() != null){
	    				        dailyMaxEstVol = Double.parseDouble(operationsHubLimits.getDailyMaxEstVol().toString()); 
	    				   }
	    				   countCheck = workTicketManager.getDailyOpsLimit(workTicket.getDate1(), workTicket.getDate2(), dailyCount, dailyCountPC, sessionCorpID, operationsHubLimits.getHubID(), workTicket.getEstimatedWeight(), workTicket.getEstimatedCubicFeet(), dailyMaxEstWt, dailyMaxEstVol,id);
		    			   if(countCheck.length()>15){	    				   
		    				   exceedDate = countCheck.substring(countCheck.indexOf(":")+1, countCheck.length());
			    			   countCheck = countCheck.substring(0, countCheck.indexOf(":"));
			    			   if(countCheck.equalsIgnoreCase("GreaterWeightVolume")){
			    				   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
			    					   checkLimit="Y"; 
				    			   }
			    				}
				    			if(countCheck.equalsIgnoreCase("Greater")){
				    				if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
				    					checkLimit="Y"; 
					    			}
				    			}	    				   
		    			   }
		    			   
		    		   }
	    		  //}
	    		  
	    	           }
					}				 
			    }
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}

	  
	  
	 @SkipValidation 
	 public String itemsJbkEquipList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	serviceOrder=serviceOrderManager.get(sid);
	 	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			billing = billingManager.get(sid);
			customerFile=serviceOrder.getCustomerFile(); 
	 	materialsList=new ArrayList();
	 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	 	String contract="";
	 	workTicket = workTicketManager.get(id);
		  	Long ticket=workTicket.getTicket();
		    wrehouse=workTicket.getWarehouse();
		    if(wrehouse!=null || wrehouse!=""){
		    wrehouseDesc= itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID).get(0).toString();
		    }
		      /* if((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()){
			  		contract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0).toString();
			  	}else{
			  		contract=itemsJEquipManager.findContractFromBilling(workTicket.getShipNumber()).get(0).toString();
			  	}*/
	
		  	
		  	if(itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterials(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);
			}
		  	if(itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterials(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);
			}
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}		  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 //Master Info
	 public String deleteInventory() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			    itemsJEquipManager.deleteInventory(id);
			    itemsJbkEquipExtraMasterInfoList();
			 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
	 
	 @SkipValidation 
	 public String itemsJbkEquipExtraMasterInfoList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	//serviceOrder=serviceOrderManager.get(sid);
	 	//getRequest().setAttribute("soLastName",serviceOrder.getLastName());	
		 /*if(itemType==null || itemType.equals("")){
				itemType="E";
			}*/
		materialsList = itemsJEquipManager.findMaterialExtraMasterInfo(sessionCorpID,itemType,"","","");
		contract="";		
	  	material = itemsJEquipManager.findMaterialExtraMasterInfoByContract(contract,sessionCorpID,itemType);
		      
		  	
		  	/*if(itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}	*/	  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 @SkipValidation 
	 public String findMaterialExtraMasterInfo() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	//serviceOrder=serviceOrderManager.get(sid);
	 	//getRequest().setAttribute("soLastName",serviceOrder.getLastName());	
		 if(itemType==null || itemType.equals("")){
				itemType="E";
			}
		materialsList = itemsJEquipManager.getMaterialExtraMasterInfo(sessionCorpID,itemType,"","");
		contract="";		
	  	material = itemsJEquipManager.findMaterialExtraMasterInfoByContract(contract,sessionCorpID,itemType);
		      
		  	
		  	/*if(itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}	*/	  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 @SkipValidation 
	 public String searchItemsJbkEquipsMasterInfo() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	//serviceOrder=serviceOrderManager.get(sid);
	 	//getRequest().setAttribute("soLastName",serviceOrder.getLastName());	
		/* if(itemType==null || itemType.equals("")){
				itemType="E";
			}*/
		 if(branch==null || branch.equals("")){
			 branch="";
			}
		 if(division==null || division.equals("")){
			 division="";
			}
		 if(resource==null || resource.equals("")){
			 resource="";
			}
		materialsList = itemsJEquipManager.findMaterialExtraMasterInfo(sessionCorpID,itemType,branch,division,resource);
		contract="";		
	  	material = itemsJEquipManager.findMaterialExtraMasterInfoByContract(contract,sessionCorpID,itemType);
		      
		  	
		  	/*if(itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(id,ticket.toString(), "", sessionCorpID, itemType);
			  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			}
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}	*/	  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 //
	 //
	 @SkipValidation 
	 public String miscellaneousRequestSearchList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
		 	//serviceOrder=serviceOrderManager.get(sid);
		 	//getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	//trackingStatus=trackingStatusManager.get(sid);
			//	miscellaneous = miscellaneousManager.get(sid);
				//billing = billingManager.get(sid);
				//customerFile=serviceOrder.getCustomerFile(); 
		 	materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";
		 	//workTicket = workTicketManager.get(id);
			  	//Long ticket=workTicket.getTicket();
			   // wrehouse=workTicket.getWarehouse();
			    /*if(wrehouse!=null || wrehouse!=""){
			    wrehouseDesc= itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID).get(0).toString();
			    }*/
			      /* if((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()){
				  		contract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0).toString();
				  	}else{
				  		contract=itemsJEquipManager.findContractFromBilling(workTicket.getShipNumber()).get(0).toString();
				  	}*/
		
			  	
			  	if(itemType.equalsIgnoreCase("M")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType,"","");
				  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID,"","");
				}
			  	if(itemType.equalsIgnoreCase("E")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType,"","");
				  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID,"","");
				}else{
					materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType,"","");
				  	material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID,division,resource);
				}
			  	/*if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
		    	{
			  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		    	}*/		  	
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS; 
	 }
	 @SkipValidation 
	 public String itemsJbkEquipExtraInfoList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	serviceOrder=serviceOrderManager.get(sid);
	 	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			billing = billingManager.get(sid);
			customerFile=serviceOrder.getCustomerFile(); 
	 	materialsList=new ArrayList();
	 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	 	String contract="";
	 	workTicket = workTicketManager.get(wid);
	 	wtkDivision=workTicket.getCompanyDivision();
		  	Long ticket=workTicket.getTicket();
		    wrehouse=workTicket.getWarehouse();
		    if(wrehouse!=null || wrehouse!=""){
		    	List listiii=itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID);
		    	if(listiii!=null && !(listiii.isEmpty()) && (listiii.get(0)!=null)){
		    wrehouseDesc= listiii.get(0).toString();
		    	}
		    }else{
		    	wrehouse="";	
		    }
		    materialsList=new ArrayList();
		    //materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, wrehouse);
		  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,wrehouse,sessionCorpID,division,resource);
		  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
		  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
		  	}
		      /* if((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()){
			  		contract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0).toString();
			  	}else{
			  		contract=itemsJEquipManager.findContractFromBilling(workTicket.getShipNumber()).get(0).toString();
			  	}*/
	
		  	
		  	/*if(itemType!=null && itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, itemType);
			  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
		  	}else if(itemType!=null && itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, itemType);
			  	List invDocMaps = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
			}else{
				materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, "");
				List invDocMaps= itemsJEquipManager.findMaterialExtraInfoByContract(contract,"",sessionCorpID);
				if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
			}*/
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}		  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 @SkipValidation 
	 public String workTicketReturnInfoList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 
	 	serviceOrder=serviceOrderManager.get(sid);
	 	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			billing = billingManager.get(sid);
			customerFile=serviceOrder.getCustomerFile(); 
	 	materialsList=new ArrayList();
	 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	 	String contract="";
	 	workTicket = workTicketManager.get(wid);
	 	wtkDivision=workTicket.getCompanyDivision();
		  	Long ticket=workTicket.getTicket();
		    wrehouse=workTicket.getWarehouse();
		    if(wrehouse!=null || wrehouse!=""){
		    	List listiii=itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID);
		    	if(listiii!=null && !(listiii.isEmpty()) && (listiii.get(0)!=null)){
		    wrehouseDesc= listiii.get(0).toString();
		    	}
		    }else{
		    	wrehouse="";	
		    }
		    
		    materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, wrehouse);
		  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,wrehouse,sessionCorpID,division,resource);
		  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
		  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
		  	}
		      /* if((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()){
			  		contract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0).toString();
			  	}else{
			  		contract=itemsJEquipManager.findContractFromBilling(workTicket.getShipNumber()).get(0).toString();
			  	}*/
	
		  	
		  	/*if(itemType!=null && itemType.equalsIgnoreCase("M")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, itemType);
			  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
		  	}else if(itemType!=null && itemType.equalsIgnoreCase("E")){
			  	materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, itemType);
			  	List invDocMaps = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
			}else{
				materialsList = itemsJEquipManager.findMaterialExtraInfo(wid,ticket.toString(), "", sessionCorpID, "");
				List invDocMaps= itemsJEquipManager.findMaterialExtraInfoByContract(contract,"",sessionCorpID);
				if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
			}*/
		  	if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}		  	
		  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		  	return SUCCESS;   
	 }
	 //
     
	    public String edit() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	 
	    	getComboList(sessionCorpID);
	        if (id != null) { 
	        	itemsJbkEquip = itemsJbkEquipManager.get(id);
	        	material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);
	        	//workTicket = itemsJbkEquip.getWorkTicket();
	        } else { 
	        	itemsJbkEquip = new ItemsJbkEquip(); 
	        	material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);
	        } 
	       /* if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	} */
	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	        return SUCCESS; 
	    } 
	     
	    public String save() throws Exception {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    		
	    	getComboList(sessionCorpID);
	        if (cancel != null) { 
	            return "cancel"; 
	        } 
	     
	        if (delete != null) { 
	            return delete(); 
	        } 
	        serviceOrder=serviceOrderManager.get(sid);
	        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	        workTicket=workTicketManager.get(id);
	        //itemsJbkEquip.setWorkTicket(workTicket);    
	        itemsJbkEquip.setCorpID(sessionCorpID);
	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
	        boolean isNew = (itemsJbkEquip.getId() == null);
	        itemsJbkEquip.setWorkTicketID(workTicket.getId());
	        itemsJbkEquip.setTicket(ticket);
	        itemsJbkEquip.setType(itemType);
	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
	        if (isNew) {
				itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
				itemsJbkEquip.setCreatedOn(new Date());
			}
			itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
	        itemsJbkEquip.setUpdatedOn(new Date());
	        resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
	         if(!(itemsJbkEquip.getQty().equals(0))|| !(itemsJbkEquip.getActualQty().equals(0))){
	        	 int a=itemsJbkEquip.getQty();
	        	 int c=itemsJbkEquip.getReturned().intValue();
	        	 int b=itemsJbkEquip.getActualQty();
	        	 if(b!=0 && b==a-c){	        		 
	        	 }else if(b!=0 && b!=a-c){
	        	 }else{
	        		 itemsJbkEquip.setActualQty(a-c);
	        	 }
	        	 if(itemsJbkEquip.getActualQty()!=null){
	        		 itemsJbkEquip.setActual(itemsJbkEquip.getActualQty()*itemsJbkEquip.getCost());
	        	 }/*else{
	        		 itemsJbkEquip.setActual(itemsJbkEquip.getQty()*itemsJbkEquip.getCost());
	        	 }*/
	        	 itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip
	        }
	        itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
	       /* if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
	    	{
		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	    	}
	        */
	        if(popup == null){
	        	String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
	        	saveMessage(getText(key)); 
	        }
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        if (!isNew) { 
	            return SUCCESS; 
	        } else { 
	            return SUCCESS; 
	        }
	    } 
	    
	    public String search(){ 
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    		
	    	getComboList(sessionCorpID);
	    	boolean myType = (itemsJbkEquip.getType() == null);
	    	boolean myDescription = (itemsJbkEquip.getDescript() == null);
	    	
	    	/*if(myDescription) {
	    		itemsJEquips = itemsJEquipManager.findByDescript(itemsJEquip.getDescript());
	    	}*/
	    	//itemsJEquips = itemsJEquipManager.findByType(itemsJEquip.getType());
	    	//itemsJEquips= itemsJEquipManager.findByDescript(itemsJEquip.getDescript());
	    	//itemsJEquips = new HashSet(ls);
	    	if(!myType || !myDescription) {
	    		itemsJbkEquips = itemsJbkEquipManager.findByDescript(itemsJbkEquip.getType(),itemsJbkEquip.getDescript());
	  }
	    	//itemsJEquips = new HashSet(ls);
	        //workTickets = ls;	    	
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	    	return SUCCESS;     
	    	//itemsJbkEquips = itemsJbkEquipManager.findByType(itemsJbkEquip.getType());   
	        //return SUCCESS;   
	    }
	    
	    
	    public String getMaterialDescription() {
			return materialDescription;
		}
	
		public void setMaterialDescription(String materialDescription) {
			this.materialDescription = materialDescription;
		}
	
		@SkipValidation 
	    public String searchMaterials() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
	    	String contract="";
	    	
		      materialsList=new ArrayList();
		  	  workTicket = workTicketManager.get(id);
		  	  serviceOrder=serviceOrderManager.get(sid);
		  	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		  	  trackingStatus=trackingStatusManager.get(sid);
			  miscellaneous = miscellaneousManager.get(sid); 
			  customerFile=serviceOrder.getCustomerFile(); 
			  resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			  /*
			  if((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()){
				contract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0).toString();
			  }else{
				contract=itemsJEquipManager.findContractFromBilling(workTicket.getShipNumber()).get(0).toString();
			  }*/
			  wrehouse=workTicket.getWarehouse();
		  	    if(wrehouse!=null || wrehouse!=""){
		  	    wrehouseDesc= itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID).get(0).toString();
		  	    }
		  	  boolean myDescription = (itemsJbkEquip.getDescript()== null);
		  	  if( !myDescription){
					materialsList = itemsJEquipManager.searchMaterials(itemsJbkEquip.getDescript(),workTicket.getTicket().toString(),contract, sessionCorpID,itemType);
					material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);
			  }
			  getRequest().setAttribute("itemId", id);
			  getRequest().setAttribute("itemType", itemType);			 
			  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  return SUCCESS;
		}
		
		public String getComboList(String corpID){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
			material = itemsJEquipManager.findMaterialByContract(contract,itemType,sessionCorpID);	
			resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			paytype = refMasterManager.findByParameter(corpID, "PAYTYPE");
			 invCategMap.put("E","Equipment");
		  	 invCategMap.put("M","Material");
			house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
			comapnyDivisionCodeDescp = equipMaterialsLimitsManager.getCompanyDivisionMap(corpID);
			comapnyDivisionCodeDescpCS.put("OMD","Highland Office Moving");
			equipMaterialsLimitsList=equipMaterialsLimitsManager.getAll();
			equipMaterialsList=equipMaterialsCostManager.getAll();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
	    
		/*@SkipValidation 
	    public String saveList() throws Exception {
	    	List<Map> rowValues =  GridDataProcessor.getRowData(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	
	    	System.out.println("\n\n\n rowValues it..."+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {
				Long id = (Long) row.get(listIdField);
				
				try{
					    itemsJbkEquip = itemsJbkEquipManager.get(id);
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							//System.out.println("\n\n\n itemType it..."+ key+"---->\t\t"+value);
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
						}
						save();
				}
				catch(Exception ex)
				{
					itemsJbkEquip=new ItemsJbkEquip();
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
					}
					save();
				}
			}
	    	serviceOrder=serviceOrderManager.get(sid);
	    	getRequest().setAttribute("sid", sid);
			getRequest().setAttribute("itemId", id);
			getRequest().setAttribute("itemType", itemType);
			getRequest().getSession().setAttribute("messages", null);
	        saveMessage(getText("itemsJbkEquip.updated")); 
			return SUCCESS;
		}*/
		
		public String resourceAutocomplete(){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
			if(tempresource!=null&& !tempresource.equals("")){
				resourcList=itemsJbkEquipManager.getResourceAutoComplete(sessionCorpID,tempresource,tempCategory);	
			}			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;	
		}
		
		
		
		
		public String saveList() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
	    	List<Map> rowValues =  GridDataProcessor.getRowData(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {
				Long id = (Long) row.get(listIdField);
				try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
					itemsJbkEquip.setUpdatedOn(new Date());
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
						}
						save();
				}
				catch(Exception ex)
				{
					itemsJbkEquip=new ItemsJbkEquip();
					itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
					itemsJbkEquip.setCreatedOn(new Date());
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
					}
					save();
					ex.printStackTrace();
				}
			}
			getRequest().setAttribute("itemId", id);
			getRequest().setAttribute("itemType", itemType);
			getRequest().getSession().setAttribute("messages", null);
	        saveMessage(getText("itemsJbkEquip.updated")); 	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		//
		 @SkipValidation 
		 public String materialList() {
			 List invDocMaps =  itemsJEquipManager.materialByCategory(branch,sessionCorpID);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}
			 return SUCCESS;  
		 }
		//customized save for #8612
		 @SkipValidation 
		 public String miscellaneousSearchRequestList() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 	 
		 	 materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";		 
		 	if(ship==null || ship.equals("")){
				 ship="MS"; 
			 }
			 if(branch==null || branch.equals("")){
				 branch=""; 
			 }
			 if(division==null || division.equals("")){
				 division=""; 
			 }
			 if(resource==null || resource.equals("")){
				 resource=""; 
			 }
			 materialsList=new ArrayList();
			materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, branch,division,resource);			 
			 List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,branch,sessionCorpID,division,resource);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}	  	
			  
			  	String msg=(String)getRequest().getSession().getAttribute("keyMsg");
			  	Boolean msg1=(Boolean)getRequest().getSession().getAttribute("isError");
			  	if(msg!=null && !msg.equals("") && !msg1){
			  	saveMessage(msg);
			  	getRequest().getSession().setAttribute("keyMsg","");
			  	}
			  	String succmsg=(String)getRequest().getAttribute("messages");
			  	if(succmsg!=null && !succmsg.equals("") ){
				  	saveMessage(succmsg);
				  	getRequest().setAttribute("succmsg","");
				  	}
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }
		 @SkipValidation 
		 public String miscellaneousRequestList() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		 	 
		 	 materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";		 
		 	if(ship==null || ship.equals("")){
				 ship="MS"; 
			 }
			 if(branch==null || branch.equals("")){
				 branch=""; 
			 }
			 if(division==null || division.equals("")){
				 division=""; 
			 }
			 if(resource==null || resource.equals("")){
				 resource=""; 
			 }
			 materialsList=new ArrayList();
			//materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, branch,division,resource);			 
			 List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,branch,sessionCorpID,division,resource);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}	  	
			  
			  	String msg=(String)getRequest().getSession().getAttribute("keyMsg");
			  	Boolean msg1=(Boolean)getRequest().getSession().getAttribute("isError");
			  	if(msg!=null && !msg.equals("") && !msg1){
			  	saveMessage(msg);
			  	getRequest().getSession().setAttribute("keyMsg","");
			  	}
			  	String succmsg=(String)getRequest().getAttribute("messages");
			  	if(succmsg!=null && !succmsg.equals("") ){
				  	saveMessage(succmsg);
				  	getRequest().setAttribute("succmsg","");
				  	}
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }
		 @SkipValidation 
		 public String miscellaneousRequestListReturn() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 	 materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";		 	
		 	if(ship==null || ship.equals("")){
				 ship="MS"; 
			 }
			 if(branch==null || branch.equals("")){
				 branch=""; 
			 }
			 if(division==null || division.equals("")){
				 division=""; 
			 }
			 if(resource==null || resource.equals("")){
				 resource=""; 
			 }
			 materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, branch,division,resource);			 
			 List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContract(contract,branch,sessionCorpID,division,resource);
			  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
			  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
			  	}			  	
			  	String msg=(String)getRequest().getSession().getAttribute("keyMsg");
			  	Boolean msg1=(Boolean)getRequest().getSession().getAttribute("isError");
			  	if(msg!=null && !msg.equals("") && !msg1){
			  	saveMessage(msg);
			  	getRequest().getSession().setAttribute("keyMsg","");
			  	}
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }
		 @SkipValidation 
		 public String itemsJbkEquipCustomerSalesList() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			 if(ship==null || ship.equals("")){
				 ship="CS"; 
			 }
			 if(whouse==null || whouse.equals("")){
				 whouse=""; 
			 }
		 	materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";
		 	materialsList=new ArrayList();
		 	//materialsList = itemsJEquipManager.salesRequestList(id,ship, sessionCorpID, branch);
		  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,whouse,sessionCorpID);
		  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
		  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
		  	}
			  	
			  	/*if(itemType!=null && itemType.equalsIgnoreCase("M")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}
			  	if(itemType!=null && itemType.equalsIgnoreCase("E")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}else{
					materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
					List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}*/
			  	/*if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
		    	{
			  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		    	}*/	
			  	getRequest().setAttribute("whouse", whouse);
			  	getRequest().setAttribute("ship", ship);
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }	
		 //
		 @SkipValidation 
		 public String overallItemsJbkEquipCustomerSalesList() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			 if(ship==null || ship.equals("")){
				 ship=""; 
			 }
			 if(branch==null || branch.equals("")){
				 branch=""; 
			 }
			 SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder beginDates = new StringBuilder(formats.format( beginDate ));
		        StringBuilder endDates = new StringBuilder(formats.format( endDate ));
		 	materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";
		 	materialsList=new ArrayList();
		 	materialsList = itemsJEquipManager.overallSalesRequestList(id,ship, sessionCorpID, branch,beginDates.toString(),endDates.toString());
		  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,branch,sessionCorpID);
		  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
		  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
		  	}
			  	
			  	/*if(itemType!=null && itemType.equalsIgnoreCase("M")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}
			  	if(itemType!=null && itemType.equalsIgnoreCase("E")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}else{
					materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
					List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}*/
			  	/*if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
		    	{
			  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		    	}*/	
			  	getRequest().setAttribute("ship", ship);
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }	
		 //
		 @SkipValidation 
		 public String overallItemsJbkEquipCustomerSales() {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			 if(ship==null || ship.equals("")){
				 ship=""; 
			 }
			 if(branch==null || branch.equals("")){
				 branch=""; 
			 }
		 	materialsList=new ArrayList();
		 	resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		 	String contract="";	
		 	materialsList = itemsJEquipManager.salesRequestList(id,ship, sessionCorpID, branch);
		  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
			List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,branch,sessionCorpID);
		  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
		  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
		  	}
			  	
			  	/*if(itemType!=null && itemType.equalsIgnoreCase("M")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}
			  	if(itemType!=null && itemType.equalsIgnoreCase("E")){
				  	materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
				  	List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}else{
					materialsList = itemsJEquipManager.miscellaneousRequestList(id,ship, sessionCorpID, itemType);
				  	//material = itemsJEquipManager.findMaterialExtraInfoByContract(contract,itemType,sessionCorpID);
					List invDocMaps =  itemsJEquipManager.findMaterialExtraInfoByContractForSales(contract,itemType,sessionCorpID);
				  	if(invDocMaps!=null && !(invDocMaps.isEmpty()) && (invDocMaps.get(0)!=null)){
				  		invDocMap=(HashMap<Long, String>) invDocMaps.get(0);
				  	}
				}*/
			  	/*if(!((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty()))
		    	{
			  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		    	}*/	
			  	getRequest().setAttribute("ship", ship);
			  	String msg=(String)getRequest().getSession().getAttribute("Sucmessages");
			  	if(msg!=null && (!msg.equals(""))){
			  	saveMessage(msg);
			  	getRequest().getSession().setAttribute("Sucmessages","");
			  	}
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			  	return SUCCESS;   
		 }	
		 
		 @SkipValidation 
		 public String editEquipMaterialMiscLimitDetails(){
			 
			 itemsJbkEquip = itemsJbkEquipManager.get(id);
			 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
			 category=eqipMaterialsLimits.getCategory();
			 division=eqipMaterialsLimits.getDivision();
			 branch=eqipMaterialsLimits.getBranch();
			 resource=itemsJbkEquip.getDescript();
			 if(itemsJbkEquip.getActualQty()!=null){
			 actQty=itemsJbkEquip.getActualQty().toString();
			 }
			 if(itemsJbkEquip.getReturned()!=null){
			 retQty=itemsJbkEquip.getReturned().toString();
			 }else{
				 
			 }
			 reffBy=itemsJbkEquip.getRefferedBy();
			 comment=itemsJbkEquip.getComments();
			 return SUCCESS;
		 }
		 @SkipValidation 
		 public String saveMiscellaneousInvRequestList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				if(category==null || category.equals("")){
					String key="Category is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(branch==null || branch.equals("")){
					String key="Warehouse is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(division==null || division.equals("")){
					String key="Division is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(resource==null || resource.equals("")){
					String key="Description is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				
				/**if(comment==null || comment.equals("")){
					String key="Comment is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(reffBy==null || reffBy.equals("")){
					String key="Reffered By is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}*/
				
		    		boolean isNew=true;				
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					isNew=false;
						}catch(Exception e){		 	         
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);		 	       
			 	        itemsJbkEquip.setType(category);
			            itemsJbkEquip.setShip(ship);	
			            e.printStackTrace();
						
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String custName="";
						String emailId="";
						String custCell="";
						String refferedBy="";
						boolean custNamechk=false;
						boolean emailIdchk=false;
						boolean custCellchk=false;
						boolean refferedBychk=false;	
								
			eqipMaterialsLimits=equipMaterialsLimitsManager.get(resourceId);
	        equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
	        itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
	        desc=eqipMaterialsLimits.getResource();	
	        if(retQty!=null && !(retQty.equals(""))){
		    returnqtyy=new BigDecimal(retQty);	
		    if(itemsJbkEquip.getReturned()!=null){
		    returnqtyy= new BigDecimal(itemsJbkEquip.getReturned().toString()).subtract(returnqtyy);
		    }
	        }else{
	        	returnqtyy=new BigDecimal("0.0");
	        	 itemsJbkEquip.setReturned(new Double("0.0"));
	        }
	        if(actQty!=null && !(actQty.equals(""))){
		    qtyy=new BigDecimal(actQty);
	        }else{
	        	qtyy=new BigDecimal("0.0");	
	        }
			qtyyMaster=qtyy;									
		    refferedBy=reffBy;
			refferedBychk=true;			
									
						
							
							
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isActQtyChng!=null && isActQtyChng){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
								remainqty=remainqty.subtract(returnqtyy);
							}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
								remainqty=remainqty.add(returnqtyy);
							}
							/*else{
								remainqty=(actqty.subtract(qtyy));	
							}*/
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isActQtyChng || isRetQtyChng)){
								//max chk
								String key="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isActQtyChng|| isRetQtyChng)){
								//min chk
								String key="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setCreatedOn(new Date());	
								itemsJbkEquip.setType(category);
								itemsJbkEquip.setDescript(desc);
								itemsJbkEquip.setComments(comment);
								itemsJbkEquip.setRefferedBy(refferedBy);
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								
								if(isActQtyChng || isRetQtyChng){
								eqipMaterialsLimits.setQty(remainqty.intValue());
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(remainqty.intValue());
								equipMaterialsCostManager.save(equipMaterialsCost);						
								
															
								if(isActQtyChng){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}else{
										
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isRetQtyChng){								
									//Double diff=itemsJbkEquip.getActualQty()-itemsJbkEquip.getReturned();
									/**if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
										Double diff=new Double(itemsJbkEquip.getActualQty().toString())+(new Double(returnqtyy.toString()));
										itemsJbkEquip.setActualQty(diff.intValue());
									}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
										Double diff=new Double(itemsJbkEquip.getActualQty().toString())-(new Double(returnqtyy.toString()));
										itemsJbkEquip.setActualQty(diff.intValue());
									}*/
									itemsJbkEquip.setReturned(new Double(retQty.toString()));
									
									}								
					 	        
								}else{
									//itemsJbkEquip.setActualQty(0);
									//itemsJbkEquip.setReturned(new Double("0.0"));									
								}
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								itemsJbkEquipManager.save(itemsJbkEquip);
							}
							
					}
					catch(Exception ex)
					{
						ex.getStackTrace();
					}
				
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("ship", ship);
				getRequest().getSession().setAttribute("messages", null);
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //
		 
		 @SkipValidation 
		 public String removeMiscAssignmentRecord() throws Exception {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			     itemsJbkEquip=itemsJbkEquipManager.get(id);
			     eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
			     if(itemsJbkEquip.getActualQty()!=null){
			    	 if(itemsJbkEquip.getReturned()!=null){
			     eqipMaterialsLimits.setQty(eqipMaterialsLimits.getQty()+(itemsJbkEquip.getActualQty()-(itemsJbkEquip.getReturned().intValue())));
			    	 }else{
			    		 eqipMaterialsLimits.setQty(eqipMaterialsLimits.getQty()+(itemsJbkEquip.getActualQty()));	 
			    	 }
			     equipMaterialsLimitsManager.save(eqipMaterialsLimits);
			     }
			     itemsJbkEquipManager.remove(id);			    
			    //saveMessage("Record has been deleted ");
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("ship", ship);
				getRequest().setAttribute("messages", "Record has been deleted ");
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			 return SUCCESS;
		 }
		 @SkipValidation 
		 public String removeWorkTicketAssignment() throws Exception {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			     itemsJbkEquip=itemsJbkEquipManager.get(id);
			     eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
			     if(itemsJbkEquip.getActualQty()!=null){
			    	 if(itemsJbkEquip.getReturned()!=null){
			     eqipMaterialsLimits.setQty(eqipMaterialsLimits.getQty()+(itemsJbkEquip.getActualQty()-(itemsJbkEquip.getReturned().intValue())));
			    	 }else{
			    		 eqipMaterialsLimits.setQty(eqipMaterialsLimits.getQty()+(itemsJbkEquip.getActualQty()));	 
			    	 }
			     equipMaterialsLimitsManager.save(eqipMaterialsLimits);
			     }
			     itemsJbkEquipManager.remove(id);			    
			    //saveMessage("Record has been deleted ");
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("wid", wid);
				getRequest().setAttribute("messages", "Record has been deleted ");
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			 return SUCCESS;
		 }
		 
		 
		 @SkipValidation 
		 public String saveCustomerSalesList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				if(category==null || category.equals("")){
					String key="Category is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(branch==null || branch.equals("")){
					String key="Warehouse is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(division==null || division.equals("")){
					String key="Division is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(resource==null || resource.equals("")){
					String key="Description is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				
				
				
				
		    		boolean isNew=true;				
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					isNew=false;
						}catch(Exception e){		 	         
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);		 	       
			 	        itemsJbkEquip.setType(category);
			            itemsJbkEquip.setShip(ship);			           
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String custName="";
						String emailId="";
						String custCell="";
						String refferedBy="";
						boolean custNamechk=false;
						boolean emailIdchk=false;
						boolean custCellchk=false;
						boolean refferedBychk=false;	
								
			eqipMaterialsLimits=equipMaterialsLimitsManager.get(resourceId);
	        equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
	        itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
	        desc=eqipMaterialsLimits.getResource();	
	        if(retQty!=null && !(retQty.equals(""))){
		    returnqtyy=new BigDecimal(retQty);	
		    if(itemsJbkEquip.getReturned()!=null){
		    returnqtyy= new BigDecimal(itemsJbkEquip.getReturned().toString()).subtract(returnqtyy);
		    }
	        }else{
	        	returnqtyy=new BigDecimal("0.0");
	        	 itemsJbkEquip.setReturned(new Double("0.0"));
	        }
	        if(actQty!=null && !(actQty.equals(""))){
		    qtyy=new BigDecimal(actQty);
	        }else{
	        	qtyy=new BigDecimal("0.0");	
	        }
			qtyyMaster=qtyy;									
		    //refferedBy=reffBy;
			refferedBychk=true;			
									
						
							
							
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isActQtyChng!=null && isActQtyChng){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
								remainqty=remainqty.subtract(returnqtyy);
							}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
								remainqty=remainqty.add(returnqtyy);
							}/*else{
							}
								remainqty=(actqty.subtract(qtyy));	
							}*/
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isActQtyChng || isRetQtyChng)){
								//max chk
								String key="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isActQtyChng|| isRetQtyChng)){
								//min chk
								String key="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setCreatedOn(new Date());	
								itemsJbkEquip.setType(category);
								itemsJbkEquip.setDescript(desc);
								itemsJbkEquip.setComments(comment);
								//itemsJbkEquip.setRefferedBy(refferedBy);
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								
								if(isActQtyChng || isRetQtyChng){
								eqipMaterialsLimits.setQty(remainqty.intValue());
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(remainqty.intValue());
								equipMaterialsCostManager.save(equipMaterialsCost);						
								
															
								if(isActQtyChng){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}else{
										
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isRetQtyChng){								
								//Double diff=itemsJbkEquip.getActualQty()-itemsJbkEquip.getReturned();
								/**if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
									Double diff=new Double(itemsJbkEquip.getActualQty().toString())+(new Double(returnqtyy.toString()));
									itemsJbkEquip.setActualQty(diff.intValue());
								}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
									Double diff=new Double(itemsJbkEquip.getActualQty().toString())-(new Double(returnqtyy.toString()));
									itemsJbkEquip.setActualQty(diff.intValue());
								}*/
								itemsJbkEquip.setReturned(new Double(retQty.toString()));
								
								}
					 	        
								}else{
									itemsJbkEquip.setActualQty(0);
									itemsJbkEquip.setReturned(new Double("0.0"));									
								}
								ship=itemsJbkEquip.getShip();
								if(ship==null){
									ship="";
								}

                                if(ship.equals("CS")){
							itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getSalestCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								}else if(ship.equals("OS")){
									itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getOwnerOpCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								}
                                ship="";
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								itemsJbkEquipManager.save(itemsJbkEquip);
							}
							
					}
					catch(Exception ex)
					{
						ex.getStackTrace();
					}
				
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("ship", "");
				getRequest().getSession().setAttribute("messages", null);
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //
		 
		/* @SkipValidation 
		 public String saveMiscellaneousInvRequestList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		    	  
		    		boolean isNew=true;				
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					isNew=false;
						}catch(Exception e){		 	         
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);		 	       
			 	        itemsJbkEquip.setType(category);
			            itemsJbkEquip.setShip(ship);			           
						
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String custName="";
						String emailId="";
						String custCell="";
						String refferedBy="";
						boolean custNamechk=false;
						boolean emailIdchk=false;
						boolean custCellchk=false;
						boolean refferedBychk=false;	
								
			eqipMaterialsLimits=equipMaterialsLimitsManager.get(Long.parseLong(resource));
	        equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
	        itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
	        desc=eqipMaterialsLimits.getResource();	
	        if(retQty!=null && !(retQty.equals(""))){
		    returnqtyy=new BigDecimal(retQty);	
	        }else{
	        	returnqtyy=null;
	        	 itemsJbkEquip.setReturned(new Double("0.0"));
	        }
	        if(actQty!=null && !(actQty.equals(""))){
		    qtyy=new BigDecimal(actQty);
	        }else{
	        	qtyy=new BigDecimal("0.0");	
	        }
			qtyyMaster=qtyy;									
		    refferedBy=reffBy;
			refferedBychk=true;								
									
							//checking row whether change or not so that update master quantity data
							boolean isactqtychange=true;
							boolean isreturnqtychange=true;
							if(!(isNew)){
								if(qtyy.toString().equals(itemsJbkEquip.getActualQty().toString())){
									System.out.println("actual act qty remains same no need to save row again........");
									isactqtychange=false;
								}else{
									System.out.println("actual  qty changed need to save row again........");
									isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}
								if(returnqtyy.doubleValue()==itemsJbkEquip.getReturned().doubleValue()){
									System.out.println("actual return qty remains same no need to save row again........");
									isreturnqtychange=false;
								
								}else{
									isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								String key="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								String key="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setCreatedOn(new Date());	
								itemsJbkEquip.setType(category);
								itemsJbkEquip.setDescript(desc);
								itemsJbkEquip.setComments(comment);
								itemsJbkEquip.setRefferedBy(refferedBy);
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								
								if(isactqtychange || isreturnqtychange){
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);							
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(qtyy.toString()))));
															
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}else{
										
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null){
								itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
								Double diff=itemsJbkEquip.getActualQty()-itemsJbkEquip.getReturned();
								itemsJbkEquip.setActualQty(diff.intValue());
								}
					 	        
								}							
								itemsJbkEquipManager.save(itemsJbkEquip);
							}
							
					}
					catch(Exception ex)
					{
						ex.getStackTrace();
					}
				
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("ship", ship);
				getRequest().getSession().setAttribute("messages", null);
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}*/
			
		 
		public String saveMiscellaneousRequestList() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
	    	List<Map> rowValues =  GridDataProcessor.getRowDataMiscMap(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {	
	    		boolean isNew=true;
	    		//serviceOrder=serviceOrderManager.get(sid);
	 	        //getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	       // workTicket=workTicketManager.get(id);
	    		//item jequip	    		 
	 	       // boolean isNew = (itemsJbkEquip.getId() == null);
	 	            
	 	        
	 	       /* if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
	 	    	{
	 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	 	    	}
	 	        */
	 	        if(popup == null){
	 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
	 	        	//saveMessage(getText(key)); 
	 	        }
	    		//
				Long id = (Long) row.get(listIdField);
				try{
					try{
				itemsJbkEquip = itemsJbkEquipManager.get(id);
				isNew=false;
					}catch(Exception e){									
		 	        //itemsJbkEquip.setWorkTicket(workTicket); 
		 	       itemsJbkEquip=new ItemsJbkEquip();
		 	        itemsJbkEquip.setCorpID(sessionCorpID);
		 	       // itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
		 	       //itemsJbkEquip.setWorkTicketID(workTicket.getId());
		 	        //itemsJbkEquip.setTicket(ticket);
		 	        itemsJbkEquip.setType(itemType);
		            itemsJbkEquip.setShip(ship);	 
					e.printStackTrace();
				}
					
					BigDecimal qtyy=null;
					BigDecimal qtyyMaster=null;
					BigDecimal returnqtyy=null;
					String desc="";
					String custName="";
					String emailId="";
					String custCell="";
					String refferedBy="";
					boolean custNamechk=false;
					boolean emailIdchk=false;
					boolean custCellchk=false;
					boolean refferedBychk=false;
						for (Object key : row.keySet()) {							
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								//eqipMaterialsLimits.setResource(value.toString());
								String str=value.toString();
								if(str!=null && !str.equals("")){
									String ss[]=str.split("-");
									if(ss!=null && ss.length>=1){
										 desc=ss[0];
						eqipMaterialsLimits=equipMaterialsLimitsManager.getByDesc(desc,sessionCorpID);
									}
								}
							}
							if(key.toString().equals("returned")){
								returnqtyy=new BigDecimal(value.toString());
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("actualQty")){
								qtyy=new BigDecimal(value.toString());
								qtyyMaster=qtyy;
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("custName")){
								custName=value.toString();
								custNamechk=true;
								
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("emailId")){
								emailId=value.toString();
								emailIdchk=true;
							
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("custCell")){
								custCell=value.toString();
								custCellchk=true;
								
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("refferedBy")){
								refferedBy=value.toString();
								refferedBychk=true;
								
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
							//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
						}
						//
						//checking row whether change or not so that update master quantity data
						boolean isactqtychange=true;
						boolean isreturnqtychange=true;
						if(!(isNew)){
							if(qtyy.toString().equals(itemsJbkEquip.getActualQty().toString())){
								System.out.println("actual act qty remains same no need to save row again........");
								isactqtychange=false;
							}else{
								System.out.println("actual  qty changed need to save row again........");
								isactqtychange=true;
								qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
							}
							if(returnqtyy.doubleValue()==itemsJbkEquip.getReturned().doubleValue()){
								System.out.println("actual return qty remains same no need to save row again........");
								isreturnqtychange=false;
							
							}else{
								isreturnqtychange=true;
								System.out.println("actual return qty changed need to save row again........");
							}
						}
						//
						BigDecimal tqty;
						BigDecimal remainqty=new BigDecimal("0.0");
						BigDecimal actqty=new BigDecimal("0.0");
						if(eqipMaterialsLimits.getQty()!=null){
						 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
						}
						BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
						if(isactqtychange){
							remainqty=(actqty.subtract(qtyy));
						}else{
							remainqty=actqty;
						}
						if(returnqtyy!=null && isreturnqtychange){
							remainqty=remainqty.add(returnqtyy);
						}/*else{
							remainqty=(actqty.subtract(qtyy));	
						}*/
						BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
						if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
							//max chk
							String key="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
							errorMessage(getText(key));
							//return INPUT;
						}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
							//min chk
							String key="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
							errorMessage(getText(key));
							//return INPUT;
						}else {
							String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
							saveMessage(key);
							//
							if(isactqtychange || isreturnqtychange){
							eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
							equipMaterialsLimitsManager.save(eqipMaterialsLimits);
							equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
							equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
							equipMaterialsCostManager.save(equipMaterialsCost);
							//
							//jeqip save							
							itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
							itemsJbkEquip.setType(itemType);
							itemsJbkEquip.setDescript(desc);
							itemsJbkEquip.setComments(custName+"-"+emailId+"-"+custCell+"-"+refferedBy);
							itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(qtyy.toString()))));
							itemsJbkEquip.setCreatedOn(new Date());
							itemsJbkEquip.setUpdatedOn(new Date());
							itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
							if(isactqtychange){
								if(!isNew){
									itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
								}else if(isNew){
									itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
								}
							
							}else{
								itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
							}
							if(returnqtyy!=null){
							itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
							}
				 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
				 	        //itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
							}else if((custNamechk || emailIdchk||custCellchk|| refferedBychk) && !(isNew)){
								//only cust info changed.....
								itemsJbkEquip.setComments(custName+"-"+emailId+"-"+custCell+"-"+refferedBy);
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								itemsJbkEquipManager.save(itemsJbkEquip);
							}
							//
						}
						//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
						//save();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					/*itemsJbkEquip=new ItemsJbkEquip();
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						if(key.toString().equals("descript")){
							eqipMaterialsLimits.setResource(value.toString());	
						}
						if(key.toString().equals("minLevel")){
							eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
						}
						if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
						PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
						}
					}
					equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
					//save();
				}
			}
			getRequest().setAttribute("itemId", id);
			getRequest().setAttribute("itemType", itemType);
			getRequest().setAttribute("ship", ship);
			getRequest().getSession().setAttribute("messages", null);
	       // saveMessage(getText("itemsJbkEquip.updated")); 	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		//work tkt save
		 @SkipValidation 
		 public String saveMaterialEquipInfoList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				getComboList(sessionCorpID);
				if(category==null || category.equals("")){
					String key="Category is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(branch==null || branch.equals("")){
					String key="Warehouse is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(division==null || division.equals("")){
					String key="Division is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				if(resource==null || resource.equals("")){
					String key="Description is Required";
					valFlag="openSaveBlock";
					errorMessage(key);
					return INPUT;
				}
				serviceOrder=serviceOrderManager.get(sid);
	 	        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	        workTicket=workTicketManager.get(wid);
				boolean isNew=true;				
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					isNew=false;
						}catch(Exception e){		 	         
							itemsJbkEquip=new ItemsJbkEquip();
				 	        itemsJbkEquip.setCorpID(sessionCorpID);
				 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
				 	       itemsJbkEquip.setWorkTicketID(workTicket.getId());
				 	        itemsJbkEquip.setTicket(ticket);
				 	        itemsJbkEquip.setType(itemType);
				 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());			           
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String custName="";
						String emailId="";
						String custCell="";
						String refferedBy="";
						boolean custNamechk=false;
						boolean emailIdchk=false;
						boolean custCellchk=false;
						boolean refferedBychk=false;	
								
			eqipMaterialsLimits=equipMaterialsLimitsManager.get(resourceId);
	        equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
	        itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
	        desc=eqipMaterialsLimits.getResource();	
	        if(retQty!=null && !(retQty.equals(""))){
		    returnqtyy=new BigDecimal(retQty);
		    if(itemsJbkEquip.getReturned()!=null){
		    returnqtyy= new BigDecimal(itemsJbkEquip.getReturned().toString()).subtract(returnqtyy);
		    }
	        }else{
	        	returnqtyy=new BigDecimal("0.0");
	        	 itemsJbkEquip.setReturned(new Double("0.0"));
	        }
	        if(actQty!=null && !(actQty.equals(""))){
		    qtyy=new BigDecimal(actQty);
	        }else{
	        	qtyy=new BigDecimal("0.0");	
	        }
	        qtyyMaster=qtyy;	
	       /** if(!isNew && isActQtyChng){
				if(qtyy.compareTo(new BigDecimal (itemsJbkEquip.getActualQty().toString()))==1){				
				qtyy=qtyy.subtract(new BigDecimal (itemsJbkEquip.getActualQty().toString()));
				}else{				
					qtyy=(new BigDecimal (itemsJbkEquip.getActualQty().toString())).subtract(qtyy);
				}
	        }*/
											
		    refferedBy=reffBy;
			refferedBychk=true;			
									
						
							
							
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isActQtyChng!=null && isActQtyChng){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
								remainqty=remainqty.subtract(returnqtyy);
							}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
								remainqty=remainqty.add(returnqtyy);
							}
							/*else{
								remainqty=(actqty.subtract(qtyy));	
							}*/
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isActQtyChng || isRetQtyChng)){
								//max chk
								String key="Assigned Quantity Exceeds Maximum Limit By "+(qtyy.subtract(maxqty)).intValue()+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isActQtyChng|| isRetQtyChng)){
								//min chk
								String key="Assigned Quantity Decreases Min Limit By "+(minqty.subtract(qtyy)).intValue()+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setCreatedOn(new Date());	
								itemsJbkEquip.setType(category);
								itemsJbkEquip.setDescript(desc);
								//itemsJbkEquip.setComments(comment);
								//itemsJbkEquip.setRefferedBy(refferedBy);
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								
								if(isActQtyChng || isRetQtyChng){
								eqipMaterialsLimits.setQty(remainqty.intValue());
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(remainqty.intValue());
								equipMaterialsCostManager.save(equipMaterialsCost);						
								
															
								if(isActQtyChng){
									if(!isNew){
										itemsJbkEquip.setActualQty(new Integer(qtyyMaster.toString())+itemsJbkEquip.getActualQty());
									}else if(isNew){
										itemsJbkEquip.setActualQty(new Integer(qtyyMaster.toString()));
									}else{
										
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyyMaster.toString()));	
								}
								if(returnqtyy!=null && isRetQtyChng){								
									//Double diff=itemsJbkEquip.getActualQty()-itemsJbkEquip.getReturned();
									/**if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()!=null){
										Double diff=new Double(itemsJbkEquip.getActualQty().toString())+(new Double(returnqtyy.toString()));
										itemsJbkEquip.setActualQty(diff.intValue());
									}else if(isRetQtyChng!=null && isRetQtyChng && itemsJbkEquip.getReturned()==null){
										Double diff=new Double(itemsJbkEquip.getActualQty().toString())-(new Double(returnqtyy.toString()));
										itemsJbkEquip.setActualQty(diff.intValue());
									}*/
									itemsJbkEquip.setReturned(new Double(retQty.toString()));
									
									}								
					 	        
								}else{
									itemsJbkEquip.setActualQty(0);
									itemsJbkEquip.setReturned(new Double("0.0"));									
								}
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								itemsJbkEquipManager.save(itemsJbkEquip);
								workTicket.setWarehouse(branch);
								workTicketManager.save(workTicket);
							}
							
					}
					catch(Exception ex)
					{
						ex.getStackTrace();
					}
				
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", category);
				getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("wid", wid);
				getRequest().getSession().setAttribute("messages", null);
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		//end
		 //
		/* public String multipleAssignmentList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");					
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForSave(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;
		    		serviceOrder=serviceOrderManager.get(sid);
		 	        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	        workTicket=workTicketManager.get(wid);
		    		//item jequip	    		 
		 	       // boolean isNew = (itemsJbkEquip.getId() == null);
		 	            
		 	        
		 	        if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
		 	    	{
		 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		 	    	}
		 	        
		 	        if(popup == null){
		 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
		 	        	//saveMessage(getText(key)); 
		 	        }
		    		//
					Long id = (Long) row.get(listIdField);
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);					
					isNew=false;
						}catch(Exception e){									
			 	        //itemsJbkEquip.setWorkTicket(workTicket); 
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);
			 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 	        itemsJbkEquip.setTicket(ticket);
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());	 
						
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){
									//eqipMaterialsLimits.setResource(value.toString());
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split(":");
										if(ss!=null && ss.length>=1){
											 desc=ss[0];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("actualQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							boolean isactqtychange=true;
							boolean isreturnqtychange=true;
							if(!(isNew)){
								if(qtyy.toString().equals(itemsJbkEquip.getActualQty().toString())){
									System.out.println("actual act qty remains same no need to save row again........");
									isactqtychange=false;
								}else{
									System.out.println("actual  qty changed need to save row again........");
									isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}
								if(returnqtyy.doubleValue()==itemsJbkEquip.getReturned().doubleValue()){
									System.out.println("actual return qty remains same no need to save row again........");
									isreturnqtychange=false;
								
								}else{
									isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								String key="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								String key="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								if(isactqtychange || isreturnqtychange){
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);
								//
								//jeqip save							
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
								itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(qtyy.toString()))));
								itemsJbkEquip.setCreatedOn(new Date());
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isreturnqtychange){
								itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
								}
					 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
					 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
								}
								//
							}
							//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
							//save();
					}
					catch(Exception ex)
					{
						itemsJbkEquip=new ItemsJbkEquip();
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
							}
						}
						equipMaterialsLimitsManager.save(eqipMaterialsLimits);
						//save();
					}
				}
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", itemType);
				getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("wid", wid);
				getRequest().getSession().setAttribute("messages", null);
				//chngMap="";
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}*/
		 //
		//
	//
		 //val start
		 public String validateMultipleMiscllaneousList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				String key1="";
				Boolean b=new Boolean(false);
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMiscMap(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	int rowCountNum=1;
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;		    		
		 	        if(popup == null){		 	        	
		 	        }
		    		
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("3")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){
							isNew=true;
						 itemsJbkEquip=new ItemsJbkEquip();
			 	       isactqtychange=true;
						 isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String refferedBy="";
						String comment="";						
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){									
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split(":");
										if(ss!=null && ss.length>=1){
											 desc=ss[0];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("actualQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("comments")){
									comment=value.toString();
									if(comment!=null && !comment.equals("0")){
									itemsJbkEquip.setComments(comment);
									}else{
										itemsJbkEquip.setComments("");
									}
									//custCellchk=true;
									
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("refferedBy")){
									refferedBy=value.toString();
									if(refferedBy!=null && !refferedBy.equals("0")){
									itemsJbkEquip.setRefferedBy(refferedBy);
									}else{
										itemsJbkEquip.setRefferedBy("");	
									}
									//refferedBychk=true;
									
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
									qtyy=qtyy.abs();
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");
									//isreturnqtychange=false;
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							//qtyy=new BigDecimal(itemsJbkEquip.getActualQty().toString());
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(isreturnqtychange){
								BigDecimal actreturnqtyy=returnqtyy;
								if(itemsJbkEquip.getReturned()!=null){
									Double newRet=new Double(returnqtyy.toString());
									Double oldRet=itemsJbkEquip.getReturned();
									Double diff=new Double("0.0d");
									diff=newRet-oldRet;
									/**if(oldRet>newRet){
										diff=oldRet-newRet;
									}else if(oldRet<newRet){
										diff=newRet-oldRet;
									}else{
										
									}*/
									returnqtyy=new BigDecimal(diff);
								}
								remainqty=(actqty.add(returnqtyy));
								if(!(actreturnqtyy.compareTo(new BigDecimal(itemsJbkEquip.getActualQty()))==1)){
								eqipMaterialsLimits.setQty(remainqty.intValue());
								//itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty().intValue()-(returnqtyy.intValue()));
								itemsJbkEquip.setReturned(new Double(actreturnqtyy.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								itemsJbkEquipManager.save(itemsJbkEquip);
							}else{
								errorMessage("Returned Quantity should not exceed Actual Quantity ");
							}
							}else{
								//remainqty=actqty;
							}
							
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){								
								 key1="Quantity Assigned exceeds set Minimum Levels, please input valid amount.";
								 if(rowNum!=null && !rowNum.equals("")){
								 rowNum=rowNum+", "+String.valueOf(rowCountNum);
								 }else{
									 rowNum=String.valueOf(rowCountNum); 
								 }
								 b=new Boolean(true);								 
								
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){								
								 if(!rowNum.equals("")){
									 rowNum=rowNum+", "+String.valueOf(rowCountNum);
									 }else{
										 rowNum=String.valueOf(rowCountNum); 
									 }
								 key1="Quantity Assigned decreases set Minimum Levels, please input valid amount.";
								 b=new Boolean(true);
								 
							}else {
								 
							}							
					}
					catch(Exception ex)
					{ex.printStackTrace();
						
					}
					rowCountNum=rowCountNum+1;
				}
				       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //val end
		 public String multipleMiscllaneousList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				String key1="";
				Boolean b=new Boolean(false);
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMiscMap(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;
		    		//serviceOrder=serviceOrderManager.get(sid);
		 	       // getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	       // workTicket=workTicketManager.get(wid);
		    		//item jequip	    		 
		 	       // boolean isNew = (itemsJbkEquip.getId() == null);
		 	            
		 	        
		 	        /*if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
		 	    	{
		 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		 	    	}
		 	        */
		 	        if(popup == null){
		 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
		 	        	//saveMessage(getText(key)); 
		 	        }
		    		//
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("3")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){									
			 	        //itemsJbkEquip.setWorkTicket(workTicket); 
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       //itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 	        //itemsJbkEquip.setTicket(ticket);
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShip(ship);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       isactqtychange=true;
						 isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String desc="";
						String refferedBy="";
						String comment="";						
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){
									//eqipMaterialsLimits.setResource(value.toString());
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split(":");
										if(ss!=null && ss.length>=1){
											 desc=ss[0];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("actualQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("comments")){
									comment=value.toString();
									if(comment!=null && !(comment.equals("0"))){
									itemsJbkEquip.setComments(comment);
									}else{
										itemsJbkEquip.setComments("");	
									}
									//custCellchk=true;
									
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("refferedBy")){
									refferedBy=value.toString();
									if(refferedBy!=null && !(refferedBy.equals("0"))){
									itemsJbkEquip.setRefferedBy(refferedBy);
									}else{
										itemsJbkEquip.setRefferedBy("");	
									}
									//refferedBychk=true;
									
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
									qtyy=qtyy.abs();
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");
									//isreturnqtychange=false;
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							/*if(returnqtyy!=null && isreturnqtychange){
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}*/
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								 key1="Quantity Assigned exceeds set Minimum Levels, please input valid amount.";
								 b=new Boolean(true);
								 
								errorMessage(getText(key1));
								continue;
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								 key1="Quantity Assigned decreases set Minimum Levels, please input valid amount.";
								 b=new Boolean(true);
								 //getRequest().getSession().setAttribute("isError", new Boolean(true));
								errorMessage(getText(key1));
								continue;
								//return INPUT;
							}else {
								 key1="Item has been successfully assigned";
								 //getRequest().getSession().setAttribute("isError", new Boolean(false));
								//saveMessage(key1);
								//
								if(isactqtychange || isreturnqtychange){
								/*eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);*/
								//
								//jeqip save							
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
								itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());							
								itemsJbkEquip.setCreatedOn(new Date());
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyyMaster.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isreturnqtychange && ((itemsJbkEquip.getActualQty().compareTo(Integer.parseInt((returnqtyy.toString())))==1) || (itemsJbkEquip.getActualQty().compareTo(Integer.parseInt((returnqtyy.toString())))==0))){
									remainqty=remainqty.add(returnqtyy);
								itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
								if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty()-Integer.parseInt((returnqtyy.toString())));
								
								}else{
									if(isactqtychange){
									remainqty=(actqty.subtract(new BigDecimal(qtyyMaster.toString())));	
									}
								}								
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);
								if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
								itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
					 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
								}
								//
							}
							//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
							//save();
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
						/*itemsJbkEquip=new ItemsJbkEquip();
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
							}
						}
						equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
						//save();
					}
				}
				//getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", itemType);
				//getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("ship", ship);
				getRequest().getSession().setAttribute("keyMsg", key1);
				getRequest().getSession().setAttribute("isError", b);
				chngMap="";
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
	
	//update
		 public String updateMultipleAssignmentCsOsList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForUpdate(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;
		    		//serviceOrder=serviceOrderManager.get(sid);
		 	       // getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	       // workTicket=workTicketManager.get(wid);
		    		//item jequip	    		 
		 	       // boolean isNew = (itemsJbkEquip.getId() == null);
		 	            
		 	        
		 	        /*if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
		 	    	{
		 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		 	    	}
		 	        */
		 	        if(popup == null){
		 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
		 	        	//saveMessage(getText(key)); 
		 	        }
		    		//
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("7")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){									
			 	        //itemsJbkEquip.setWorkTicket(workTicket); 
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       //itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 	        //itemsJbkEquip.setTicket(ticket);
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShip(ship);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       isactqtychange=true;
						 isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						BigDecimal masterreturnqtyy=new BigDecimal("0");
						String custName="";						
						String desc="";
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){
									//eqipMaterialsLimits.setResource(value.toString());
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split("-");
										if(ss!=null && ss.length>=1){
											 desc=ss[ss.length-1];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									masterreturnqtyy=returnqtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("soldQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								
								if(key.toString().equals("customerName")){
									custName=value.toString();
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("payMethod")){
									payMethod=value.toString();
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");
									//isreturnqtychange=false;
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(itemsJbkEquip.getReturned()!=null){
								Double newRet=new Double(returnqtyy.toString());
								Double oldRet=itemsJbkEquip.getReturned();
								Double diff=new Double("0.0d");
								diff=newRet-oldRet;
								/**if(oldRet>newRet){
									diff=oldRet-newRet;
								}else if(oldRet<newRet){
									diff=newRet-oldRet;
								}else{
									
								}*/
								returnqtyy=new BigDecimal(diff);
							}
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								String key="Quantity to sale exceeds set Minimum Levels, please input valid amount.";
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								String key="Quantity to sale decreases set Minimum Levels, please input valid amount.";
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								saveMessage(key);
								//
								if(isactqtychange || isreturnqtychange){
									
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);
								//
								//jeqip save							
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
								itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());							
								itemsJbkEquip.setCreatedOn(new Date());
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setCustomerName(custName);
								itemsJbkEquip.setPayMethod(payMethod);
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isreturnqtychange){
								itemsJbkEquip.setReturned(new Double(masterreturnqtyy.toString()));
								/**if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty()-Integer.parseInt((returnqtyy.toString())));*/
								
								}
								ship=itemsJbkEquip.getShip();
								if(ship==null){
									ship="";
								}
								if(itemsJbkEquip.getActualQty()!=null){
								//itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								if(ship.equals("CS")){
							itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getSalestCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								}else if(ship.equals("OS")){
									itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getOwnerOpCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
								}
								}
					 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
					 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
								}
								//
							}
							//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
							//save();
					}
					catch(Exception ex)
					{ex.printStackTrace();
						/*itemsJbkEquip=new ItemsJbkEquip();
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
							}
						}
						equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
						//save();
					}
				}
				//getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", itemType);
				//getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("ship", "");
				getRequest().getSession().setAttribute("messages", null);
				chngMap="";
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //end update
		 
		 
		 
		 
		 //	 
		 //validation
		 public String validateMultipleAssignmentCsOsList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				String invoiceSeqNum=itemsJbkEquipManager.getMaxSeqNumber(sessionCorpID);
				Long invoiceSeqUpdate=Long.valueOf(invoiceSeqNum); 
				invoiceSeqUpdate=invoiceSeqUpdate+1;
		 		String val2 = "00000";
		 		String key1="";
		 		val2 = val2.substring(invoiceSeqUpdate.toString().length()-1); 
		 		invoiceSeqNum=val2+invoiceSeqUpdate.toString();		 		
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForSave(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	int rowCountNum=1;
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;		    		    		
		 	        if(popup == null){		 	        	
		 	        }		    		
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("3")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){						
							isNew=true;
			 	        itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);			 	        
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShip(ship);			 	      
			 	       isactqtychange=true;
						isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String custName=customerName;						
						String desc="";
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){									
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split("-");
										if(ss!=null && ss.length>=1){
											 desc=ss[ss.length-1];
											 if(isNew){
							                  eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){													
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {													 
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());									
								}
								
								if(key.toString().equals("customerName")){
									custName=value.toString();									
								}
								/**if(key.toString().equals("payMethod")){
									payMethod=value.toString();									
								}*/
								if(key.toString().equals("soldQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
										
								}
														
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");									
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								if(rowNum!=null && !rowNum.equals("")){
									 rowNum=rowNum+", "+String.valueOf(rowCountNum);
									 }else{
										 rowNum=String.valueOf(rowCountNum); 
									 }
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								if(rowNum!=null && !rowNum.equals("")){
									 rowNum=rowNum+", "+String.valueOf(rowCountNum);
									 }else{
										 rowNum=String.valueOf(rowCountNum); 
									 }
								
							}else {								
								
							}
							
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
					rowCountNum=rowCountNum+1;
				}
					       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //end validation
		 
		 public String multipleAssignmentCsOsList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				String invoiceSeqNum=itemsJbkEquipManager.getMaxSeqNumber(sessionCorpID);
				Long invoiceSeqUpdate=Long.valueOf(invoiceSeqNum); 
				invoiceSeqUpdate=invoiceSeqUpdate+1;
		 		String val2 = "00000";
		 		String key1="";
		 		val2 = val2.substring(invoiceSeqUpdate.toString().length()-1); 
		 		invoiceSeqNum=val2+invoiceSeqUpdate.toString();		 		
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForSave(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;
		    		    		//serviceOrder=serviceOrderManager.get(sid);
		 	       // getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	       // workTicket=workTicketManager.get(wid);
		    		//item jequip	    		 
		 	       // boolean isNew = (itemsJbkEquip.getId() == null);
		 	            
		 	        
		 	        /*if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
		 	    	{
		 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		 	    	}
		 	        */
		 	        if(popup == null){
		 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
		 	        	//saveMessage(getText(key)); 
		 	        }
		    		//
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("3")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){									
			 	        //itemsJbkEquip.setWorkTicket(workTicket); 
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       //itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 	        //itemsJbkEquip.setTicket(ticket);
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShip(ship);
			 	        //itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       isactqtychange=true;
						 isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						String custName=customerName;
						//String payMethod="";
						String desc="";
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){
									//eqipMaterialsLimits.setResource(value.toString());
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split("-");
										if(ss!=null && ss.length>=1){
											 desc=ss[ss.length-1];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								
								/**if(key.toString().equals("customerName")){
									custName=value.toString();
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}*/
								/**if(key.toString().equals("payMethod")){
									payMethod=value.toString();
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}*/
								if(key.toString().equals("soldQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");
									//isreturnqtychange=false;
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								 key1="Assigned Quantity "+qtyy+"  Exceeds Maximum Limit "+maxqty+" For "+desc;
								errorMessage(getText(key1));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								 key1="Assigned Quantity "+qtyy+" is Decreases Min Limit "+minqty+" For "+desc;
								errorMessage(getText(key1));
								//return INPUT;
							}else {
								 //key1="Quantity "+qtyy+" is Successfull Assigned to  "+desc;
								key1="Quantity is Successfully Assigned for Sales";
								saveMessage(key1);
								//
								if(isactqtychange || isreturnqtychange){
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);
								//
								//jeqip save							
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
								itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());							
								itemsJbkEquip.setCreatedOn(new Date());
								itemsJbkEquip.setUpdatedOn(new Date());
								//
								itemsJbkEquip.setCustomerName(custName);
								itemsJbkEquip.setPayMethod(payMethod);
								//
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isreturnqtychange  && (qtyyMaster.compareTo(returnqtyy)==1)){
									System.out.println("greater.................................................................");
								itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
								if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty()-Integer.parseInt((returnqtyy.toString())));
								
								}
								if(itemsJbkEquip.getActualQty()!=null){
									itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
									if(ship.equals("CS")){
										if(equipMaterialsCost.getSalestCost()!=null && !(equipMaterialsCost.getSalestCost().toString().equals(""))){
								itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getSalestCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
										}else{
											itemsJbkEquip.setSalePrice(((new Double("0.0d"))*(new Double(itemsJbkEquip.getActualQty()))));
										}
									}else if(ship.equals("OS")){
										if(equipMaterialsCost.getOwnerOpCost()!=null && !(equipMaterialsCost.getOwnerOpCost().toString().equals(""))){
										itemsJbkEquip.setSalePrice(((new Double(equipMaterialsCost.getOwnerOpCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
										}else{
											itemsJbkEquip.setSalePrice(((new Double("0.0d"))*(new Double(itemsJbkEquip.getActualQty()))));	
										}
									}
								}
								itemsJbkEquip.setInvoiceSeqNumber(invoiceSeqNum);
					 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
					 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
								}
								//
							}
							//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
							//save();
					}
					catch(Exception ex)
					{ex.printStackTrace();
						/*itemsJbkEquip=new ItemsJbkEquip();
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
							}
						}
						equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
						//save();
					}
				}
				//getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", itemType);
				//getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("ship", "");
				getRequest().setAttribute("Sucmessages", key1);
				chngMap="";
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 //
		 
		 public String returnMultipleAssignmentList() throws Exception {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
				TreeMap<Long,String> idString=new TreeMap<Long,String>();
				if(chngMap!=null && !(chngMap.equals(""))){
					String s1[]=chngMap.split(",");
					if(s1!=null && (s1.length>0)){
						for(int i=0;i<s1.length;i++){
						String s2[]=s1[i].split(":");
						if(s2!=null && (s2.length==2)){
							String id=s2[0];
							String ff=s2[1];
							if(idString.containsKey(new Long(id))){
								System.out.println(idString.get(new Long(id)));
								if(String.valueOf(idString.get(new Long(id)))!=null)
								idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
							}else{
								idString.put(new Long(id),ff);
							}
						}
						}
					}
				}
				
		    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForSave(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
		    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
		    	for (Map row : rowValues) {	
		    		boolean isNew=true;
		    		serviceOrder=serviceOrderManager.get(sid);
		 	        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 	        workTicket=workTicketManager.get(wid);
		    		//item jequip	    		 
		 	       // boolean isNew = (itemsJbkEquip.getId() == null);
		 	            
		 	        
		 	        /*if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
		 	    	{
		 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
		 	    	}
		 	        */
		 	        if(popup == null){
		 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
		 	        	//saveMessage(getText(key)); 
		 	        }
		    		//
					Long id = (Long) row.get(listIdField);
					boolean isactqtychange=false;
					boolean isreturnqtychange=false;
					try{
						try{
					itemsJbkEquip = itemsJbkEquipManager.get(id);
					 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
					if(idString!=null && idString.size()>0){
					if(idString.containsKey(new Long(id))){
						String str=idString.get(id);
						if(str!=null && !str.equals("")){
							String strc[]=str.split("~");
							for(int i=0;i<strc.length;i++){
								String ii=strc[i];
								if(ii.equals("2")){
									isactqtychange=true;
									
								}else if(ii.equals("3")){
									isreturnqtychange=true;
									
								}else{
									
								}
							}
						}
					}
					}
					isNew=false;
						}catch(Exception e){									
			 	        //itemsJbkEquip.setWorkTicket(workTicket); 
			 	       itemsJbkEquip=new ItemsJbkEquip();
			 	        itemsJbkEquip.setCorpID(sessionCorpID);
			 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 	        itemsJbkEquip.setTicket(ticket);
			 	        itemsJbkEquip.setType(itemType);
			 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
			 	       isactqtychange=true;
						 isreturnqtychange=true;
						e.printStackTrace();
					}
						
						BigDecimal qtyy=null;
						BigDecimal qtyyMaster=null;
						BigDecimal returnqtyy=null;
						BigDecimal masterreturnqtyy=new BigDecimal("0");
						String desc="";
						String descText="";
							for (Object key : row.keySet()) {							
								Object value = row.get(key);
								if(key.toString().equals("descript")){
									//eqipMaterialsLimits.setResource(value.toString());
									String str=value.toString();
									if(str!=null && !str.equals("")){
										String ss[]=str.split("-");
										if(ss!=null && ss.length>=1){
											 desc=ss[ss.length-1];
											 if(isNew){
							eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
											 }else if(!isNew){
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
												 if(desc.equals(eqipMaterialsLimits.getResource())){
													//des chng during not edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
												 }else {
													 //des chng during edit
													 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
												 }
											 }else{
												 
											 }
							itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
										}
									}
								}
								if(key.toString().equals("returned")){
									returnqtyy=new BigDecimal(value.toString());
									masterreturnqtyy=returnqtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								if(key.toString().equals("actualQty")){
									qtyy=new BigDecimal(value.toString());
									qtyyMaster=qtyy;
									//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
								}
								//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
								//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
							}
							//
							//checking row whether change or not so that update master quantity data
							
							if(!(isNew)){
								if(!isactqtychange){
									System.out.println("actual act qty remains same no need to save row again........");
									//isactqtychange=false;
								}else if(isactqtychange){
									System.out.println("actual  qty changed need to save row again........");
									//isactqtychange=true;
									//qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
								}else{
									
								}
								if(!isreturnqtychange){
									System.out.println("actual return qty remains same no need to save row again........");
									//isreturnqtychange=false;
								
								}else if(isreturnqtychange){
									//isreturnqtychange=true;
									System.out.println("actual return qty changed need to save row again........");
								}else{
									
								}
							}
							//
							 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
							descText=eqipMaterialsLimits.getResource();
							BigDecimal tqty;
							BigDecimal remainqty=new BigDecimal("0.0");
							BigDecimal actqty=new BigDecimal("0.0");
							if(itemsJbkEquip.getReturned()!=null){
								Double newRet=new Double(returnqtyy.toString());
								Double oldRet=itemsJbkEquip.getReturned();
								Double diff=new Double("0.0d");
								diff=newRet-oldRet;
								/**if(oldRet>newRet){
									diff=oldRet-newRet;
								}else if(oldRet<newRet){
									diff=newRet-oldRet;
								}else{
									
								}*/
								returnqtyy=new BigDecimal(diff);
							}
							if(eqipMaterialsLimits.getQty()!=null){
							 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
							}
							BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
							if(isactqtychange){
								remainqty=(actqty.subtract(qtyy));
							}else{
								remainqty=actqty;
							}
							if(returnqtyy!=null && isreturnqtychange){
								remainqty=remainqty.add(returnqtyy);
							}else{
								remainqty=(actqty.subtract(qtyy));	
							}
							BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
							if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
								//max chk
								String key="Assigned Quantity Exceeds Maximum Limit By "+(qtyy.subtract(maxqty)).intValue()+" For "+descText;
								errorMessage(getText(key));
								//return INPUT;
							}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
								//min chk
								String key="Assigned Quantity  Decreases Min Limit By "+(minqty.subtract(qtyy)).intValue()+" For "+descText;
								errorMessage(getText(key));
								//return INPUT;
							}else {
								String key="Quantity "+qtyy+" is Successfull Assigned to  "+descText;
								saveMessage(key);
								//
								if(isactqtychange || isreturnqtychange){
								eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
								equipMaterialsLimitsManager.save(eqipMaterialsLimits);
								equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
								equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
								equipMaterialsCostManager.save(equipMaterialsCost);
								//
								//jeqip save							
								itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
								itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
								itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());							
								itemsJbkEquip.setCreatedOn(new Date());
								itemsJbkEquip.setUpdatedOn(new Date());
								itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
								if(isactqtychange){
									if(!isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
									}else if(isNew){
										itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
									}
								
								}else{
									itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
								}
								if(returnqtyy!=null && isreturnqtychange){
								itemsJbkEquip.setReturned(new Double(masterreturnqtyy.toString()));
								/**if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty()-Integer.parseInt((returnqtyy.toString())));*/
								
								}
								if(itemsJbkEquip.getActualQty()!=null)
								itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*((new Double(itemsJbkEquip.getActualQty()))-itemsJbkEquip.getReturned())));
					 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
					 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
								}
								//
							}
							//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
							//save();
					}
					catch(Exception ex)
					{ex.printStackTrace();
						/*itemsJbkEquip=new ItemsJbkEquip();
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
							PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
							}
						}
						equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
						//save();
					}
				}
				getRequest().setAttribute("itemId", id);
				getRequest().setAttribute("itemType", itemType);
				getRequest().setAttribute("sid", sid);
				getRequest().setAttribute("wid", wid);
				getRequest().getSession().setAttribute("messages", null);
				chngMap="";
		       // saveMessage(getText("itemsJbkEquip.updated")); 	       
		        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
				return SUCCESS;
			}
		 
		 
		public String multipleAssignmentList() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
			TreeMap<Long,String> idString=new TreeMap<Long,String>();
			if(chngMap!=null && !(chngMap.equals(""))){
				String s1[]=chngMap.split(",");
				if(s1!=null && (s1.length>0)){
					for(int i=0;i<s1.length;i++){
					String s2[]=s1[i].split(":");
					if(s2!=null && (s2.length==2)){
						String id=s2[0];
						String ff=s2[1];
						if(idString.containsKey(new Long(id))){
							System.out.println(idString.get(new Long(id)));
							if(String.valueOf(idString.get(new Long(id)))!=null)
							idString.put(new Long(id),String.valueOf(idString.get(new Long(id)))+"~"+ff);
						}else{
							idString.put(new Long(id),ff);
						}
					}
					}
				}
			}
			
	    	List<Map> rowValues =  GridDataProcessor.getRowDataMapForSave(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {	
	    		boolean isNew=true;
	    		serviceOrder=serviceOrderManager.get(sid);
	 	        getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	 	        workTicket=workTicketManager.get(wid);
	    		//item jequip	    		 
	 	       // boolean isNew = (itemsJbkEquip.getId() == null);
	 	            
	 	        
	 	        /*if((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0)== null || (itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).isEmpty())
	 	    	{
	 		  	actualcost=new BigDecimal((itemsJbkEquipManager.findActualCost(sessionCorpID,serviceOrder.getShipNumber(),workTicket.getTicket(),itemType)).get(0).toString());
	 	    	}
	 	        */
	 	        if(popup == null){
	 	        	//String key = (isNew) ? "itemsJbkEquip.added" : "itemsJbkEquip.updated"; 
	 	        	//saveMessage(getText(key)); 
	 	        }
	    		//
				Long id = (Long) row.get(listIdField);
				boolean isactqtychange=false;
				boolean isreturnqtychange=false;
				try{
					try{
				itemsJbkEquip = itemsJbkEquipManager.get(id);
				if(idString!=null && idString.size()>0){
				if(idString.containsKey(new Long(id))){
					String str=idString.get(id);
					if(str!=null && !str.equals("")){
						String strc[]=str.split("~");
						for(int i=0;i<strc.length;i++){
							String ii=strc[i];
							if(ii.equals("2")){
								isactqtychange=true;
								
							}else if(ii.equals("3")){
								isreturnqtychange=true;
								
							}else{
								
							}
						}
					}
				}
				}
				isNew=false;
					}catch(Exception e){									
		 	        //itemsJbkEquip.setWorkTicket(workTicket); 
		 	       itemsJbkEquip=new ItemsJbkEquip();
		 	        itemsJbkEquip.setCorpID(sessionCorpID);
		 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
		 	       itemsJbkEquip.setWorkTicketID(workTicket.getId());
		 	        itemsJbkEquip.setTicket(ticket);
		 	        itemsJbkEquip.setType(itemType);
		 	        itemsJbkEquip.setShipNum(serviceOrder.getShipNumber());
		 	       isactqtychange=true;
					 isreturnqtychange=true;
					e.printStackTrace();
				}
					
					BigDecimal qtyy=null;
					BigDecimal qtyyMaster=null;
					BigDecimal returnqtyy=null;
					String desc="";
					String descText="";
						for (Object key : row.keySet()) {							
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								//eqipMaterialsLimits.setResource(value.toString());
								String str=value.toString();
								if(str!=null && !str.equals("")){
									String ss[]=str.split("-");
									if(ss!=null && ss.length>=1){
										 desc=ss[ss.length-1];
										 if(isNew){
						eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc));
										 }else if(!isNew){
											 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId()); 
											 if(desc.equals(eqipMaterialsLimits.getResource())){
												//des chng during not edit
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(itemsJbkEquip.getEquipMaterialsId());
											 }else {
												 //des chng during edit
												 eqipMaterialsLimits=equipMaterialsLimitsManager.get(new Long(desc)); 
											 }
										 }else{
											 
										 }
						itemsJbkEquip.setEquipMaterialsId(eqipMaterialsLimits.getId());
									}
								}
							}
							if(key.toString().equals("returned")){
								returnqtyy=new BigDecimal(value.toString());
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("actualQty")){
								qtyy=new BigDecimal(value.toString());
								qtyyMaster=qtyy;
								//eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							//if(!(key.toString().equals("qty")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))))
							//PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);							
						}
						//
						//checking row whether change or not so that update master quantity data
						
						if(!(isNew)){
							if(!isactqtychange){
								System.out.println("actual act qty remains same no need to save row again........");
								//isactqtychange=false;
							}else if(isactqtychange){
								System.out.println("actual  qty changed need to save row again........");
								//isactqtychange=true;
								//qtyy=qtyy.subtract(BigDecimal.valueOf(itemsJbkEquip.getActualQty().intValue()));
							}else{
								
							}
							if(!isreturnqtychange){
								System.out.println("actual return qty remains same no need to save row again........");
								//isreturnqtychange=false;
							
							}else if(isreturnqtychange){
								//isreturnqtychange=true;
								System.out.println("actual return qty changed need to save row again........");
							}else{
								
							}
						}
						//
						descText=eqipMaterialsLimits.getResource();
						BigDecimal tqty;
						BigDecimal remainqty=new BigDecimal("0.0");
						BigDecimal actqty=new BigDecimal("0.0");
						if(eqipMaterialsLimits.getQty()!=null){
						 actqty=new BigDecimal(eqipMaterialsLimits.getQty());
						}
						BigDecimal minqty=eqipMaterialsLimits.getMinResourceLimit();
						if(isactqtychange){
							remainqty=(actqty.subtract(qtyy));
						}else{
							remainqty=actqty;
						}
						if(returnqtyy!=null && isreturnqtychange){
							remainqty=remainqty.add(returnqtyy);
						}else{
							remainqty=(actqty.subtract(qtyy));	
						}
						BigDecimal maxqty=eqipMaterialsLimits.getMaxResourceLimit();
						if( (qtyy.compareTo(maxqty))==1 && (isactqtychange || isreturnqtychange)){
							//max chk
							String key="Assigned Quantity Exceeds Maximum Limit By "+(qtyy.subtract(maxqty)).intValue()+" For "+descText;
							errorMessage(getText(key));
							//return INPUT;
						}else if(minqty.compareTo(remainqty)==1 && (isactqtychange || isreturnqtychange)){
							//min chk
							String key="Assigned Quantity  Decreases Min Limit By "+(minqty.subtract(qtyy)).intValue()+" For "+descText;
							errorMessage(getText(key));
							//return INPUT;
						}else {
							String key="Quantity "+qtyy+" is Successfull Assigned to  "+descText;
							saveMessage(key);
							//
							if(isactqtychange || isreturnqtychange){
							eqipMaterialsLimits.setQty( Integer.parseInt(remainqty.toString()));
							equipMaterialsLimitsManager.save(eqipMaterialsLimits);
							equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(eqipMaterialsLimits.getId());
							equipMaterialsCost.setAvailableQty(new Integer(remainqty.toString()));
							equipMaterialsCostManager.save(equipMaterialsCost);
							//
							//jeqip save							
							itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
							itemsJbkEquip.setType(eqipMaterialsLimits.getCategory());
							itemsJbkEquip.setDescript(eqipMaterialsLimits.getResource());							
							itemsJbkEquip.setCreatedOn(new Date());
							itemsJbkEquip.setUpdatedOn(new Date());
							itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
							if(isactqtychange){
								if(!isNew){
									itemsJbkEquip.setActualQty((new Integer(qtyy.toString()))+itemsJbkEquip.getActualQty());	
								}else if(isNew){
									itemsJbkEquip.setActualQty((new Integer(qtyy.toString())));
								}
							
							}else{
								itemsJbkEquip.setActualQty(new Integer(qtyy.toString()));	
							}
							if(returnqtyy!=null && isreturnqtychange){
							itemsJbkEquip.setReturned(new Double(returnqtyy.toString()));
							if(itemsJbkEquip.getActualQty()!=null)
							itemsJbkEquip.setActualQty(itemsJbkEquip.getActualQty()-Integer.parseInt((returnqtyy.toString())));
							
							}
							if(itemsJbkEquip.getActualQty()!=null)
							itemsJbkEquip.setCost(((new Double(equipMaterialsCost.getUnitCost().toString()))*(new Double(itemsJbkEquip.getActualQty()))));
				 	        itemsJbkEquipManager.save(itemsJbkEquip); //itemsjbkequip,itemsjequip				 	        
				 	       // itemsJbkEquipManager.updateWorkTicketCartons(itemsJbkEquip.getTicket());
							}
							//
						}
						//xzxsAequipMaterialsLimitsManager.save(eqipMaterialsLimits);
						//save();
				}
				catch(Exception ex)
				{ex.printStackTrace();
					/*itemsJbkEquip=new ItemsJbkEquip();
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						if(key.toString().equals("descript")){
							eqipMaterialsLimits.setResource(value.toString());	
						}
						if(key.toString().equals("minLevel")){
							eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
						}
						if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel")))){
						PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
						}
					}
					equipMaterialsLimitsManager.save(eqipMaterialsLimits);*/
					//save();
				}
			}
			getRequest().setAttribute("itemId", id);
			getRequest().setAttribute("itemType", itemType);
			getRequest().setAttribute("sid", sid);
			getRequest().setAttribute("wid", wid);
			getRequest().getSession().setAttribute("messages", null);
			chngMap="";
	       // saveMessage(getText("itemsJbkEquip.updated")); 	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		
		public String saveMaterialEquipMasterInfoList() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
	    	List<Map> rowValues =  GridDataProcessor.getRowDataMap(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {
	    		eqipMaterialsLimits=new EquipMaterialsLimits();
	    		eqipMaterialsLimits.setCategory(itemType);
	    		eqipMaterialsLimits.setCorpId(sessionCorpID);
	    		eqipMaterialsLimits.setCreatedBy(getRequest().getRemoteUser());
	    		eqipMaterialsLimits.setCreatedOn(new Date());
	    		eqipMaterialsLimits.setUpdatedOn(new Date());
	    		eqipMaterialsLimits.setUpdatedBy(getRequest().getRemoteUser());
	    		eqipMaterialsLimits.setBranch("");
	    		eqipMaterialsLimits.setDivision("");
	    		
	    		equipMaterialsCost=new EquipMaterialsCost();
	    		equipMaterialsCost.setCorpID(sessionCorpID);
	    		equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
	    		equipMaterialsCost.setCreatedOn(new Date());
	    		equipMaterialsCost.setUpdatedOn(new Date());
	    		equipMaterialsCost.setUpdatedBy(getRequest().getRemoteUser());
	    		
				Long id = (Long) row.get(listIdField);
				try{					
					
						for (Object key : row.keySet()) {							
							Object value = row.get(key);
							if(key.toString().equals("descript")){
								eqipMaterialsLimits.setResource(value.toString());	
							}
							if(key.toString().equals("minLevel")){
								eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
							}
							if(key.toString().equals("maxResourceLimit")){
								eqipMaterialsLimits.setMaxResourceLimit(new BigDecimal(value.toString()));
								//eqipMaterialsLimits.setQty(new Integer(eqipMaterialsLimits.getMaxResourceLimit().toString()));
							}
							if(key.toString().equals("freeQty")){
								
								if(value!=null && !value.toString().equals("0")){
									eqipMaterialsLimits.setQty(new Integer(value.toString()));
									equipMaterialsCost.setAvailableQty(new Integer(value.toString()));
								}
							}
							if(key.toString().equals("cost")){
								equipMaterialsCost.setUnitCost(new Double(value.toString()));	
							}
							if(key.toString().equals("salesPrice")){
								equipMaterialsCost.setSalestCost(new Double(value.toString()));	
							}
							if(key.toString().equals("ooprice")){
								equipMaterialsCost.setOwnerOpCost(new Double(value.toString()));	
							}
								
						}
						if(eqipMaterialsLimits.getQty()==null){
						eqipMaterialsLimits.setQty(new Integer(eqipMaterialsLimits.getMaxResourceLimit().intValueExact()));
						}
						if(equipMaterialsCost.getAvailableQty()==null){
						equipMaterialsCost.setAvailableQty(new Integer(eqipMaterialsLimits.getMaxResourceLimit().intValueExact()));
						}
						equipMaterialsLimitsManager.saveEquipMaterialObject(eqipMaterialsLimits);
						equipMaterialsCost.setEquipMaterialsId(eqipMaterialsLimits.getId());
						equipMaterialsCostManager.save(equipMaterialsCost);
						//save();
				}
				catch(Exception ex)
				{
					itemsJbkEquip=new ItemsJbkEquip();
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						if(key.toString().equals("descript")){
							//eqipMaterialsLimits.setResource(value.toString());	
						}
						if(key.toString().equals("minLevel")){
							eqipMaterialsLimits.setMinResourceLimit(new BigDecimal(value.toString()));	
						}
						if(key.toString().equals("maxResourceLimit")){
							eqipMaterialsLimits.setMaxResourceLimit(new BigDecimal(value.toString()));	
						}
						if(key.toString().equals("freeQty")){
							equipMaterialsCost.setAvailableQty(new Integer(value.toString()));	
						}
						if(key.toString().equals("cost")){
							equipMaterialsCost.setUnitCost(new Double(value.toString()));	
						}
						if(key.toString().equals("salesPrice")){
							equipMaterialsCost.setSalestCost(new Double(value.toString()));	
						}
						if(key.toString().equals("ooprice")){
							equipMaterialsCost.setOwnerOpCost(new Double(value.toString()));	
						}
						/*if(!(key.toString().equals("minLevel")) && !(key.toString().equals("freeQty")) && !(key.toString().equals("salesPrice")) && (!(key.toString().equals("minLevel"))) && (!(key.toString().equals("ooprice")))){
						PropertyUtils.setProperty(itemsJbkEquip, key.toString(), value);
						}*/
					}
					eqipMaterialsLimits=equipMaterialsLimitsManager.save(eqipMaterialsLimits);
					equipMaterialsCost.setEquipMaterialsId(eqipMaterialsLimits.getId());
					equipMaterialsCostManager.save(equipMaterialsCost);
					//save();
				}
			}
			getRequest().setAttribute("itemId", id);
			//getRequest().setAttribute("itemType", itemType);
			getRequest().getSession().setAttribute("messages", null);
	        saveMessage(getText("itemsJbkEquip.updated")); 	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		
	//update price list
		public String saveMaterialEquipMasterPriceList() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
	    	List<Map> rowValues =  GridDataProcessor.getRowDataMap(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
	    	//System.out.println("\n\n\n rowValues it...11111111111111"+"---->\t\t"+rowValues);
	    	for (Map row : rowValues) {     		
	    		
	    		
				Long id = (Long) row.get(listIdField);
				if(id!=null){
					equipMaterialsCost=equipMaterialsCostManager.getCostById(id);
					System.out.println("cost idddddddd::::::::::::::::"+id);
				}
				try{	
					
						for (Object key : row.keySet()) {							
							Object value = row.get(key);			
							
							
							if(key.toString().equals("cost")){
								equipMaterialsCost.setUnitCost(new Double(value.toString()));	
							}
							if(key.toString().equals("salesPrice")){
								equipMaterialsCost.setSalestCost(new Double(value.toString()));	
							}
							if(key.toString().equals("ooprice")){
								equipMaterialsCost.setOwnerOpCost(new Double(value.toString()));	
							}
								
						}
						
						equipMaterialsCostManager.save(equipMaterialsCost);
						//save();
				}
				catch(Exception ex)
				{
					for (Object key : row.keySet()) {							
						Object value = row.get(key);			
						
						
						if(key.toString().equals("cost")){
							equipMaterialsCost.setUnitCost(new Double(value.toString()));	
						}
						if(key.toString().equals("salesPrice")){
							equipMaterialsCost.setSalestCost(new Double(value.toString()));	
						}
						if(key.toString().equals("ooprice")){
							equipMaterialsCost.setOwnerOpCost(new Double(value.toString()));	
						}
							
					}
					
					equipMaterialsCostManager.save(equipMaterialsCost);
				}
			}
			getRequest().setAttribute("itemId", id);
			//getRequest().setAttribute("itemType", itemType);
			getRequest().getSession().setAttribute("messages", null);
	        saveMessage(getText("itemsJbkEquip.updated")); 	       
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		//
		
		
		
		
		//
		
		public String editResource() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
			if (id != null) {
				itemsJbkEquip = itemsJbkEquipManager.get(id);
				
			} else {
				itemsJbkEquip = new ItemsJbkEquip(); 
				itemsJbkEquip.setCreatedOn(new Date());
				itemsJbkEquip.setUpdatedOn(new Date());
				itemsJbkEquip.setCost(Double.valueOf(0));
				itemsJbkEquip.setShipNum(shipNum);
			} 
			resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			resourceList = itemsJEquipManager.findResource(sessionCorpID);		
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		
		public String saveResource() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
			ItemsJbkEquip jbkEquip = null;
			boolean isNew = (itemsJbkEquip.getId() == null);
			resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			
			List<ItemsJbkEquip> resourceList = itemsJbkEquipManager.checkResourceDuplicate(itemsJbkEquip.getShipNum(),sessionCorpID,itemsJbkEquip.getType(),itemsJbkEquip.getDescript());
			if(resourceList.size() > 0 && itemsJbkEquip.getId() == null){
				 btntype = "DUPCHECK";
				return SUCCESS;
			}
			if (isNew) { 
				itemsJbkEquip.setCreatedOn(new Date());
				itemsJbkEquip.setCorpID(sessionCorpID);
				itemsJbkEquip.setUpdatedOn(new Date());
				itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser()); 
				if(itemsJbkEquip.getCost() == null){
					itemsJbkEquip.setCost(Double.valueOf(0));
				}
				itemsJbkEquipManager.save(itemsJbkEquip);  
	        }else {
	        	jbkEquip = itemsJbkEquipManager.get(id);
	        	jbkEquip.setType(itemsJbkEquip.getType());
	        	jbkEquip.setDescript(itemsJbkEquip.getDescript());
	        	jbkEquip.setCost(itemsJbkEquip.getCost());
	        	jbkEquip.setComments(itemsJbkEquip.getComments());
	        	
	        	jbkEquip.setCorpID(sessionCorpID);
	    		jbkEquip.setUpdatedOn(new Date());
	    		jbkEquip.setUpdatedBy(getRequest().getRemoteUser()); 
	    		itemsJbkEquipManager.save(jbkEquip);  
	        }
			
		    String key = (isNew) ?"Resource details have been saved." :"Resource details have been saved." ;
		    saveMessage(getText(key)); 
		    //resourceURL = "?btntype=YES";
		    btntype = "YES";		   
		    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
		}
		
		public String deleteResource() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
			ItemsJbkEquip item = itemsJbkEquipManager.get(id);
	    	itemsJbkEquipManager.remove(id);
	    	resourceURL="?id="+serviceOrderId+"&day="+day+"&categoryType="+categoryType;
	    	itemsJbkEquipManager.deleteItemsJbkResource(item,true);	    	
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	        return SUCCESS;
	    }
		private String distinctWorkOrder;
		private String action;
	
		@SkipValidation 
	    public String resouceCWMSTemplate() {
			
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	    	
	    	resourceURL = "?id="+tempId+"&template=YES";
	    	operationsIntelligenceManager.getDeleteWorkOrder(shipNumber,sessionCorpID);
	    	ResourceCommonUtil.addResourceTemplate(itemsJEquipManager, itemsJbkEquipManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser());
	    	//String maxOrder=ResourceCommonUtil.findMaxWorkOrder(operationsIntelligenceManager,shipNumber,sessionCorpID);
	    	
			List distinctList = operationsIntelligenceManager.distinctWOForInsert(action,shipNumber, distinctWorkOrder);
			Iterator it2 = distinctList.iterator();
			while(it2.hasNext()){
			OperationsIntelligence operationsIntelligence =(OperationsIntelligence) it2.next();
			String wo = operationsIntelligence.getWorkorder();
			Integer woNumber = Integer.parseInt(wo.split("_")[1]);
			if(usertype.equalsIgnoreCase("ACCOUNT")){
				usertype = "AC_"+getRequest().getRemoteUser();
			}else if(usertype.equalsIgnoreCase("DRIVER")){
				usertype = "DR_"+getRequest().getRemoteUser();
			}else{
				usertype = getRequest().getRemoteUser();
			}
		    operationsIntelligenceManager.updateWorkOrder(wo, shipNumber,usertype);
			}
	    	ResourceCommonUtil.addResourceCWMSTemplate1(itemsJEquipManager, operationsIntelligenceManager, contract, shipNumber, sessionCorpID, getRequest().getRemoteUser(),distinctWorkOrder,action,usertype);
	    	
			operationsIntelligenceManager.updateExpenseRevenueConsumables(shipNumber);
	    	/*List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
	    	int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
	    	if(!resourceList.isEmpty()){
	    		Iterator it=resourceList.iterator();
	    		while(it.hasNext()){
		    	    Object []row= (Object [])it.next();
			    		ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
			    		jbkEquip.setShipNum(shipNumber);
			    		jbkEquip.setCorpID(sessionCorpID);
			    		jbkEquip.setType(row[0].toString());
			    		jbkEquip.setDescript(row[1].toString());
			    		jbkEquip.setCost(Double.valueOf(row[2].toString()));
			    		try {
			    			double d = Double.valueOf(row[2].toString());
							jbkEquip.setQty((int)d);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						jbkEquip.setEstHour(Double.valueOf(1));
			    		jbkEquip.setCreatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setCreatedOn(new Date());
			    		jbkEquip.setUpdatedBy(getRequest().getRemoteUser());
			    		jbkEquip.setUpdatedOn(new Date());
			    		jbkEquip.setDay(String.valueOf(day+1));
			    		jbkEquip.setTicketTransferStatus(false);
			    		jbkEquip.setRevisionInvoice(false);
			    		itemsJbkEquipManager.save(jbkEquip);
	    		}
	    	}*/
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS; 
	    } 
		
		public String getListData() {
			return listData;
		}
	
		public void setListData(String listData) {
			this.listData = listData;
		}
	
		public String getListFieldEditability() {
			return listFieldEditability;
		}
	
		public void setListFieldEditability(String listFieldEditability) {
			this.listFieldEditability = listFieldEditability;
		}
	
		public String getListFieldNames() {
			return listFieldNames;
		}
	
		public void setListFieldNames(String listFieldNames) {
			this.listFieldNames = listFieldNames;
		}
	
		public String getListFieldTypes() {
			return listFieldTypes;
		}
	
		public void setListFieldTypes(String listFieldTypes) {
			this.listFieldTypes = listFieldTypes;
		}
	
		public String getListIdField() {
			return listIdField;
		}
	
		public void setListIdField(String listIdField) {
			this.listIdField = listIdField;
		}
	
		public String getItemType() {
			return itemType;
		}
	
		public void setItemType(String itemType) {
			this.itemType = itemType;
		}

		public Long getTicket() {
			return ticket;
		}
	
		public void setTicket(Long ticket) {
			this.ticket = ticket;
		}
	
		public Long getTicketW() {
			return ticketW;
		}
	
		public void setTicketW(Long ticketW) {
			this.ticketW = ticketW;
		}
	
		public List getMaterialsList() {
			return materialsList;
		}
	
		public void setMaterialsList(List materialsList) {
			this.materialsList = materialsList;
		}
	
		public WorkTicket getWorkTicket() {
			return workTicket;
		}
	
		public void setWorkTicket(WorkTicket workTicket) {
			this.workTicket = workTicket;
		}
	
		public Long getId() {
			return id;
		}
	
		public Long getWid() {
			return wid;
		}
	
		public void setWid(Long wid) {
			this.wid = wid;
		}
	
		public ServiceOrder getServiceOrder() {
			return serviceOrder;
		}
	
		public void setServiceOrder(ServiceOrder serviceOrder) {
			this.serviceOrder = serviceOrder;
		}
	
		public Long getSid() {
			return sid;
		}
	
		public void setSid(Long sid) {
			this.sid = sid;
		}
	
		public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
			this.serviceOrderManager = serviceOrderManager;
		}
	
		public static CustomerFile getCustomerFile() {
			return customerFile;
		}
	
		public static void setCustomerFile(CustomerFile customerFile) {
			ItemsJbkEquipAction.customerFile = customerFile;
		}
	
		public static Miscellaneous getMiscellaneous() {
			return miscellaneous;
		}
	
		public static void setMiscellaneous(Miscellaneous miscellaneous) {
			ItemsJbkEquipAction.miscellaneous = miscellaneous;
		}
	
		public static TrackingStatus getTrackingStatus() {
			return trackingStatus;
		}
	
		public static void setTrackingStatus(TrackingStatus trackingStatus) {
			ItemsJbkEquipAction.trackingStatus = trackingStatus;
		}
	
		public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
			this.miscellaneousManager = miscellaneousManager;
		}
	
		public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
			this.trackingStatusManager = trackingStatusManager;
		}
	
		public List getMaterial() {
			return material;
		}
	
		public void setMaterial(List material) {
			this.material = material;
		}
	
		public String getContract() {
			return contract;
		}
	
		public void setContract(String contract) {
			this.contract = contract;
		}
	
		public String getWrehouse() {
			return wrehouse;
		}
	
		public void setWrehouse(String wrehouse) {
			this.wrehouse = wrehouse;
		}
	
		public String getWrehouseDesc() {
			return wrehouseDesc;
		}
	
		public void setWrehouseDesc(String wrehouseDesc) {
			this.wrehouseDesc = wrehouseDesc;
		}
	
		public Billing getBilling() {
			return billing;
		}
	
		public void setBilling(Billing billing) {
			this.billing = billing;
		}
	
		public void setBillingManager(BillingManager billingManager) {
			this.billingManager = billingManager;
		}
	
		public Map<String, String> getResourceCategory() {
			return resourceCategory;
		}
	
		public void setResourceCategory(Map<String, String> resourceCategory) {
			this.resourceCategory = resourceCategory;
		}
	
		public void setRefMasterManager(RefMasterManager refMasterManager) {
			this.refMasterManager = refMasterManager;
		}
		
	//private Long workTickets;
	    
	    public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
	        this.itemsJbkEquipManager = itemsJbkEquipManager;
	    }
	    //Changes done on 5th Sept
	    public void setItemsJEquipManager(ItemsJEquipManager itemsJEquipManager) {
	        this.itemsJEquipManager = itemsJEquipManager;
	    }
	
	/*
	    public void setWorkTicketManager(GenericManager<WorkTicket, Long> workTicketManager) { 
	        this.workTicketManager = workTicketManager; 
	    } 
	*/
	    public List getItemsJbkEquips() { 
	        return itemsJbkEquips; 
	    } 
	    public List getItemsJEquips() { 
	        return itemsJEquips; 
	    }
	    
	    public void setWorkTicketManager(WorkTicketManager workTicketManager) {
	    	this.workTicketManager = workTicketManager;
	    }
	
	    	
	        public void setId(Long id) { 
	            this.id = id; 
	        }
	        public void setPopup(String popup) {   
	            this.popup = popup;   
	        }
	        
	       // public void setLastName(String lastName) { 
	         //   this.lastName = lastName; 
	       // } 
	         
	        public ItemsJbkEquip getItemsJbkEquip() { 
	            return itemsJbkEquip;
	        } 
	         
	        public void setItemsJbkEquip(ItemsJbkEquip itemsJbkEquip) { 
	            this.itemsJbkEquip = itemsJbkEquip; 
	        } 
	        
	        public ItemsJEquip getItemsJEquip() { 
	            return itemsJEquip;
	        } 
	         
	        public void setItemsJEquip(ItemsJEquip itemsJEquip) { 
	            this.itemsJEquip = itemsJEquip; 
	        }
	
			public String getShipNum() {
				return shipNum;
			}
	
			public void setShipNum(String shipNum) {
				this.shipNum = shipNum;
			}
	
			public String getResourceURL() {
				return resourceURL;
			}
	
			public void setResourceURL(String resourceURL) {
				this.resourceURL = resourceURL;
			}
	
			public Long getServiceOrderId() {
				return serviceOrderId;
			}
	
			public void setServiceOrderId(Long serviceOrderId) {
				this.serviceOrderId = serviceOrderId;
			}
	
			public String getBtntype() {
				return btntype;
			}
	
			public void setBtntype(String btntype) {
				this.btntype = btntype;
			}

			public String getResource() {
				return resource;
			}

			public void setResource(String resource) {
				this.resource = resource;
			}

			public String getCategory() {
				return category;
			}

			public void setCategory(String category) {
				this.category = category;
			}

			public String getResourceListServer() {
				return resourceListServer;
			}

			public void setResourceListServer(String resourceListServer) {
				this.resourceListServer = resourceListServer;
			}

			public String getCategoryListServer() {
				return categoryListServer;
			}

			public void setCategoryListServer(String categoryListServer) {
				this.categoryListServer = categoryListServer;
			}

			public String getQtyListServer() {
				return qtyListServer;
			}

			public void setQtyListServer(String qtyListServer) {
				this.qtyListServer = qtyListServer;
			}

			public String getReturnedListServer() {
				return returnedListServer;
			}

			public void setReturnedListServer(String returnedListServer) {
				this.returnedListServer = returnedListServer;
			}

			public String getActQtyListServer() {
				return actQtyListServer;
			}

			public void setActQtyListServer(String actQtyListServer) {
				this.actQtyListServer = actQtyListServer;
			}

			public String getCostListServer() {
				return costListServer;
			}

			public void setCostListServer(String costListServer) {
				this.costListServer = costListServer;
			}

			public String getActualListServer() {
				return actualListServer;
			}

			public void setActualListServer(String actualListServer) {
				this.actualListServer = actualListServer;
			}

			public String getBtnType() {
				return btnType;
			}

			public void setBtnType(String btnType) {
				this.btnType = btnType;
			}

			public String getHitFlag() {
				return hitFlag;
			}

			public void setHitFlag(String hitFlag) {
				this.hitFlag = hitFlag;
			}

			public String getResourceslist() {
				return resourceslist;
			}



		/*
		    public void setWorkTicketManager(GenericManager<WorkTicket, Long> workTicketManager) { 
		        this.workTicketManager = workTicketManager; 
		    } 
		*/

    	public void setCticket(Long cticket) { 
            this.cticket = cticket; 
        }
   
       // public void setLastName(String lastName) { 
         //   this.lastName = lastName; 
       // } 

		public List getResourceList() {
			return resourceList;
		}

		public void setResourceList(List resourceList) {
			this.resourceList = resourceList;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		
		public void setResourceslist(String resourceslist) {
			this.resourceslist = resourceslist;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		}

		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}

		public int getUticket() {
			return uticket;
		}

		public void setUticket(int uticket) {
			this.uticket = uticket;
		}

		public Long getCticket() {
			return cticket;
		}

		public String getPopup() {
			return popup;
		}

		public void setItemsJbkEquips(List itemsJbkEquips) {
			this.itemsJbkEquips = itemsJbkEquips;
		}

		public void setItemsJEquips(List itemsJEquips) {
			this.itemsJEquips = itemsJEquips;
		}


		public OperationsResourceLimits getOperationsResourceLimits() {
			return operationsResourceLimits;
		}


		public void setOperationsResourceLimits(
				OperationsResourceLimits operationsResourceLimits) {
			this.operationsResourceLimits = operationsResourceLimits;
		}


		public String getFromDate() {
			return fromDate;
		}


		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}


		public String getToDate() {
			return toDate;
		}


		public void setToDate(String toDate) {
			this.toDate = toDate;
		}


		public String getCheckFlag() {
			return checkFlag;
		}


		public void setCheckFlag(String checkFlag) {
			this.checkFlag = checkFlag;
		}

		public String getShipNumber() {
			return shipNumber;
		}


		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}

		public String getDay() {
			return day;
		}

		public void setDay(String day) {
			this.day = day;
		}

		public String getTempId() {
			return tempId;
		}

		public void setTempId(String tempId) {
			this.tempId = tempId;
		}


		public double getAvailableQty() {
			return availableQty;
		}


		public void setAvailableQty(double availableQty) {
			this.availableQty = availableQty;
		}

		public String getCost() {
			return cost;
		}

		public void setCost(String cost) {
			this.cost = cost;
		}

		public String getComments() {
			return comments;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}

		public String getAvqty() {
			return avqty;
		}

		public void setAvqty(String avqty) {
			this.avqty = avqty;
		}

		public String getBeginDt() {
			return beginDt;
		}

		public void setBeginDt(String beginDt) {
			this.beginDt = beginDt;
		}

		public String getEndDt() {
			return endDt;
		}

		public void setEndDt(String endDt) {
			this.endDt = endDt;
		}

		public String getTempCategory() {
			return tempCategory;
		}


		public void setTempCategory(String tempCategory) {
			this.tempCategory = tempCategory;
		}


		public List getResourcList() {
			return resourcList;
		}


		public void setResourcList(List resourcList) {
			this.resourcList = resourcList;
		}


		public String getTempresource() {
			return tempresource;
		}


		public void setTempresource(String tempresource) {
			this.tempresource = tempresource;
		}

		public String getCountCheck() {
			return countCheck;
		}

		public void setCountCheck(String countCheck) {
			this.countCheck = countCheck;
		}

		public String getCheckLimit() {
			return checkLimit;
		}

		public void setCheckLimit(String checkLimit) {
			this.checkLimit = checkLimit;
		}

		public String getExceedDate() {
			return exceedDate;
		}

		public void setExceedDate(String exceedDate) {
			this.exceedDate = exceedDate;
		}

		public String getCheckHubResourceLimit() {
			return checkHubResourceLimit;
		}

		public void setCheckHubResourceLimit(String checkHubResourceLimit) {
			this.checkHubResourceLimit = checkHubResourceLimit;
		}

		public void setCompanyManager(CompanyManager companyManager) {
			this.companyManager = companyManager;
		}

		public List getjEquipList() {
			return jEquipList;
		}

		public void setjEquipList(List jEquipList) {
			this.jEquipList = jEquipList;
		}

		public int getMaterialsListSize() {
			return materialsListSize;
		}

		public void setMaterialsListSize(int materialsListSize) {
			this.materialsListSize = materialsListSize;
		}

		public List<ItemsJbkEquip> getCheckDuplicateList() {
			return checkDuplicateList;
		}

		public void setCheckDuplicateList(List<ItemsJbkEquip> checkDuplicateList) {
			this.checkDuplicateList = checkDuplicateList;
		}

		public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
			this.auditTrailManager = auditTrailManager;
		}

		public String getEstHour() {
			return estHour;
		}

		public void setEstHour(String estHour) {
			this.estHour = estHour;
		}
		public String getDayFocus() {
			return dayFocus;
		}
		public void setDayFocus(String dayFocus) {
			this.dayFocus = dayFocus;
		}
		public String getEstListServer() {
			return estListServer;
		}
		public void setEstListServer(String estListServer) {
			this.estListServer = estListServer;
		}
		public String getComListServer() {
			return comListServer;
		}
		public void setComListServer(String comListServer) {
			this.comListServer = comListServer;
		}
		public String getCategoryType() {
			return categoryType;
		}
		public void setCategoryType(String categoryType) {
			this.categoryType = categoryType;
		}
		public String getWorkTicketStatus() {
			return workTicketStatus;
		}
		public void setWorkTicketStatus(String workTicketStatus) {
			this.workTicketStatus = workTicketStatus;
		}
		public String getWorkTicketID() {
			return workTicketID;
		}
		public void setWorkTicketID(String workTicketID) {
			this.workTicketID = workTicketID;
		}
		public String getUsertype() {
			return usertype;
		}
		public void setUsertype(String usertype) {
			this.usertype = usertype;
		}

		public String getVoxmeIntergartionFlag() {
			return voxmeIntergartionFlag;
		}

		public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
			this.voxmeIntergartionFlag = voxmeIntergartionFlag;
		}
		public void setEquipMaterialsLimitsManager(
				EquipMaterialsLimitsManager equipMaterialsLimitsManager) {
			this.equipMaterialsLimitsManager = equipMaterialsLimitsManager;
		}
		public String getBranch() {
			return branch;
		}
		public void setBranch(String branch) {
			this.branch = branch;
		}
		public String getDivision() {
			return division;
		}
		public void setDivision(String division) {
			this.division = division;
		}
		public void setEquipMaterialsCostManager(
				EquipMaterialsCostManager equipMaterialsCostManager) {
			this.equipMaterialsCostManager = equipMaterialsCostManager;
		}
		public EquipMaterialsCost getEquipMaterialsCost() {
			return equipMaterialsCost;
		}
		public void setEquipMaterialsCost(EquipMaterialsCost equipMaterialsCost) {
			this.equipMaterialsCost = equipMaterialsCost;
		}
		public String getShip() {
			return ship;
		}
		public void setShip(String ship) {
			this.ship = ship;
		}
		public Map<Long, String> getInvDocMap() {
			return invDocMap;
		}
		public void setInvDocMap(Map<Long, String> invDocMap) {
			this.invDocMap = invDocMap;
		}
		public String getRetQty() {
			return retQty;
		}
		public void setRetQty(String retQty) {
			this.retQty = retQty;
		}
		public String getActQty() {
			return actQty;
		}
		public void setActQty(String actQty) {
			this.actQty = actQty;
		}
		public String getReffBy() {
			return reffBy;
		}
		public void setReffBy(String reffBy) {
			this.reffBy = reffBy;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		public Boolean getIsActQtyChng() {
			return isActQtyChng;
		}
		public void setIsActQtyChng(Boolean isActQtyChng) {
			this.isActQtyChng = isActQtyChng;
		}
		public Boolean getIsRetQtyChng() {
			return isRetQtyChng;
		}
		public void setIsRetQtyChng(Boolean isRetQtyChng) {
			this.isRetQtyChng = isRetQtyChng;
		}
		public Map<String, String> getHouse() {
			return house;
		}
		public void setHouse(Map<String, String> house) {
			this.house = house;
		}
		public List<EquipMaterialsLimits> getEquipMaterialsLimitsList() {
			return equipMaterialsLimitsList;
		}
		public void setEquipMaterialsLimitsList(
				List<EquipMaterialsLimits> equipMaterialsLimitsList) {
			this.equipMaterialsLimitsList = equipMaterialsLimitsList;
		}
		public String getValFlag() {
			return valFlag;
		}
		public void setValFlag(String valFlag) {
			this.valFlag = valFlag;
		}
		public List<EquipMaterialsCost> getEquipMaterialsList() {
			return equipMaterialsList;
		}
		public void setEquipMaterialsList(List<EquipMaterialsCost> equipMaterialsList) {
			this.equipMaterialsList = equipMaterialsList;
		}
		public Map<String, String> getInvCategMap() {
			return invCategMap;
		}
		public void setInvCategMap(Map<String, String> invCategMap) {
			this.invCategMap = invCategMap;
		}
		public String getChngMap() {
			return chngMap;
		}
		public void setChngMap(String chngMap) {
			this.chngMap = chngMap;
		}
		public Map<String, String> getComapnyDivisionCodeDescp() {
			return comapnyDivisionCodeDescp;
		}
		public void setComapnyDivisionCodeDescp(
				Map<String, String> comapnyDivisionCodeDescp) {
			this.comapnyDivisionCodeDescp = comapnyDivisionCodeDescp;
		}
		public Map<String, String> getComapnyDivisionCodeDescpCS() {
			return comapnyDivisionCodeDescpCS;
		}
		public void setComapnyDivisionCodeDescpCS(
				Map<String, String> comapnyDivisionCodeDescpCS) {
			this.comapnyDivisionCodeDescpCS = comapnyDivisionCodeDescpCS;
		}
		public Date getBeginDate() {
			return beginDate;
		}
		public void setBeginDate(Date beginDate) {
			this.beginDate = beginDate;
		}
		public Date getEndDate() {
			return endDate;
		}
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		public Map<String, String> getPaytype() {
			return paytype;
		}
		public void setPaytype(Map<String, String> paytype) {
			this.paytype = paytype;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getPayMethod() {
			return payMethod;
		}
		public void setPayMethod(String payMethod) {
			this.payMethod = payMethod;
		}
		public String getWhouse() {
			return whouse;
		}
		public void setWhouse(String whouse) {
			this.whouse = whouse;
		}
		public String getWtkDivision() {
			return wtkDivision;
		}
		public void setWtkDivision(String wtkDivision) {
			this.wtkDivision = wtkDivision;
		}
		public String getBackUpOnSave() {
			return backUpOnSave;
		}
		public void setBackUpOnSave(String backUpOnSave) {
			this.backUpOnSave = backUpOnSave;
		}
		public Long getResourceId() {
			return resourceId;
		}
		public void setResourceId(Long resourceId) {
			this.resourceId = resourceId;
		}
		public String getRowNum() {
			return rowNum;
		}
		public void setRowNum(String rowNum) {
			this.rowNum = rowNum;
		}
		public void setOperationsIntelligenceManager(
				OperationsIntelligenceManager operationsIntelligenceManager) {
			this.operationsIntelligenceManager = operationsIntelligenceManager;
		}
		public Map<String, String> getItemsJequipMap() {
			return itemsJequipMap;
		}
		public void setItemsJequipMap(Map<String, String> itemsJequipMap) {
			this.itemsJequipMap = itemsJequipMap;
		}
		public String getReturnAjaxStringValue() {
			return returnAjaxStringValue;
		}
		public void setReturnAjaxStringValue(String returnAjaxStringValue) {
			this.returnAjaxStringValue = returnAjaxStringValue;
		}
		public String getOiJobList() {
			return oiJobList;
		}
		public void setOiJobList(String oiJobList) {
			this.oiJobList = oiJobList;
		}
		public String getDistinctWorkOrder() {
			return distinctWorkOrder;
		}
		public void setDistinctWorkOrder(String distinctWorkOrder) {
			this.distinctWorkOrder = distinctWorkOrder;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
			
} 
