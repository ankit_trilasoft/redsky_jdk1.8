package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;   

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.model.Custom;   
import com.trilasoft.app.dao.CustomDao;   
  
public class CustomDaoHibernate extends GenericDaoHibernate<Custom, Long> implements CustomDao {    
  
    public CustomDaoHibernate() {   
        super(Custom.class);   
    }   
    public List findBySO(Long shipNumber, String corpId) {
	return getHibernateTemplate().find("from Custom where serviceOrderId=?",shipNumber);
	}
    public List countBondedGoods(Long soId, String sessionCorpID){
    	return getHibernateTemplate().find("select count(*) from WorkTicket where bondedGoods='Y' and corpId='"+sessionCorpID+"' and serviceorderid="+soId+" group by serviceorderid ");
	}
    public List countForTicket(Long ticket, Long serviceOrderId, String movement, String sessionCorpID) {
		return getHibernateTemplate().find("select count(*) from Custom where movement='"+movement+"' and corpId='"+sessionCorpID+"' and ticket='"+ticket+"' and serviceOrderId='"+serviceOrderId+"'");
	}
	public List customsInList(Long sid, String movement) {
		List list = new ArrayList();
		list = getHibernateTemplate().find("select id from Custom where serviceOrderId='"+sid+"'  order by id asc");
		list.add(0,"");
		return list;
	}
	public int checkCustomsId(Long transactionId, String movement, String sessionCorpID){
		String temp = "0";
		List list = this.getSession().createSQLQuery("select count(*) from custom where corpId='"+sessionCorpID+"' and transactionId='"+transactionId+"'").list();
		if(list!=null && (!(list.isEmpty()))){
			temp = list.get(0).toString();
		}
		int countValue = Integer.parseInt(temp);
		return countValue;
	}
	public Long findRemoteCustom(Long networkId, Long serviceOrderId){
		return Long.parseLong(this.getSession().createSQLQuery("select id from custom where  networkId="+networkId+" and  serviceOrderId="+serviceOrderId+"").list().get(0).toString());
	}
	public void deleteNetworkCustom(Long serviceOrderId, Long networkId) {
		this.getSession().createSQLQuery("delete from custom where networkId='"+networkId+"' and serviceOrderId="+serviceOrderId+"").executeUpdate();
	}
	public List getTransactionIdCount(String sessionCorpID,Long customId){
		List list = this.getSession().createSQLQuery("select count(*) from custom where corpId='"+sessionCorpID+"' and transactionId ='"+customId+"'").list();
		return list;
	}
}
