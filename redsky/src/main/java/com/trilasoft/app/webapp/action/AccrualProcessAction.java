package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import org.apache.log4j.Logger;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;


import com.trilasoft.app.dao.hibernate.AccrualProcessDaoHibernate.AccrualProcessDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.service.AccrualProcessManager;
import com.trilasoft.app.service.VanLineGLTypeManager;


public class AccrualProcessAction extends BaseAction {
	private Long id;	 
	private VanLineGLTypeManager vanLineGLTypeManager;
	private AccrualProcessManager accrualProcessManager;
	private String sessionCorpID;
	private static Map<String, String> glCode;	
	private static Map<String, String> glType;	
	private String gotoPageString;
	private Date endDate;
    private List accrualList;
    Date currentdate = new Date();
	private String validateFormNav;	
    static final Logger logger = Logger.getLogger(AccrualProcessAction.class);

	public AccrualProcessAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		glType = vanLineGLTypeManager.findByParameter(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String getGotoPageString() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return gotoPageString;
	}
	
	@SkipValidation
	public String accrualProcess(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
		
	public void payableReverseAccrued() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.payableReverseAccruedList(endDate, sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("PayableReverseAccrued");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Expense \t".getBytes());
	    	outputStream.write("Actual Expense \t".getBytes());
	    	outputStream.write("ChargeCode \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("ACT Status \t".getBytes());
	    	outputStream.write("Status \t".getBytes());
	    	outputStream.write("PayGl \t".getBytes());
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActStatus()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActStatus().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getStatus()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getStatus().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		if(((AccrualProcessDTO) extracts).getPayGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPayGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}

	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}
	    	else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	public void payableNewAccruals() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.payableNewAccruals(endDate, sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("PayableNewAccruals");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Expense \t".getBytes());
	    	outputStream.write("Actual Expense \t".getBytes());
	    	outputStream.write("ChargeCode \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("PayGl \t".getBytes());
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPayGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPayGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}
	    	else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void payableBalance() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.payableBalance(sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("PayableBalance");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Expense \t".getBytes());
	    	outputStream.write("Actual Expense \t".getBytes());
	    	outputStream.write("Charge Code \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("PayGl \t".getBytes());	    	
	    	outputStream.write("Bill To Code \t".getBytes());
	    	outputStream.write("Bill To Name \t".getBytes());
	    	outputStream.write("Vendor Code \t".getBytes());
	    	outputStream.write("Vendor Name \t".getBytes());
	    	outputStream.write("Invoice Date \t".getBytes());
	    	outputStream.write("Paying Status \t".getBytes());
	    	outputStream.write("PayPostDate \t".getBytes());
	    	outputStream.write("AccruePayable \t".getBytes());
	    	outputStream.write("Actual Delivery \t".getBytes());
	    	outputStream.write("Billers Name \t".getBytes());
	    	outputStream.write("Payable Name \t".getBytes());
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualExpense()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualExpense().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    			//outputStream.write((""+(((AccrualProcessDTO) extracts).getRevRecog().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPayGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPayGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillToCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getBillToCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillToName()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getBillToName().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		if(((AccrualProcessDTO) extracts).getVendorCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getVendorCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getVendorName()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getVendorName().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		if(((AccrualProcessDTO) extracts).getInvoiceDate()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getInvoiceDate()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPayingStatus()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPayingStatus().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPayPostDate()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getPayPostDate()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
		    	if(((AccrualProcessDTO) extracts).getAccruePayable()!=null){
		    		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getAccruePayable()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	
		    	if(((AccrualProcessDTO) extracts).getActualDelivery()!=null){
		    		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getActualDelivery()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPersonBilling()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPersonBilling().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPersonPayable()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPersonPayable().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void receivableReverseAccrued() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.receivableReverseAccruedList(endDate, sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("ReceivableReverseAccrued");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Revenue Amt \t".getBytes());
	    	outputStream.write("Actual Revenue \t".getBytes());
	    	outputStream.write("ChargeCode \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("ACT Status \t".getBytes());
	    	outputStream.write("Status \t".getBytes());
	    	outputStream.write("Rec Gl \t".getBytes());	
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionRevenueAmount()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionRevenueAmount().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualRevenue()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualRevenue().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		if(((AccrualProcessDTO) extracts).getActStatus()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActStatus().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getStatus()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getStatus().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRecGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRecGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}
	    	else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void receivableNewAccruals() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.receivableNewAccruals(endDate, sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("ReceivableNewAccruals");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Revenue Amt \t".getBytes());
	    	outputStream.write("Actual Revenue \t".getBytes());
	    	outputStream.write("ChargeCode \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("RecGl \t".getBytes());
	    	outputStream.write("Accrue Revenue \t".getBytes());
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionRevenueAmount()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionRevenueAmount().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualRevenue()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualRevenue().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRecGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRecGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccrueRevenue()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getAccrueRevenue()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}
	    	else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void receivableBalance() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accrualList = accrualProcessManager.receivableBalance(sessionCorpID);
		if(!accrualList.isEmpty()){
	    	HttpServletResponse response = getResponse();
	    	response.setContentType("application/vnd.ms-excel");
	    	ServletOutputStream outputStream = response.getOutputStream();
	    	File file = new File("ReceivableBalance");
	    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
	    	response.setHeader("Pragma", "public");
	    	response.setHeader("Cache-Control", "max-age=0");
	    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + accrualList.size() + "\t is extracted");
	    	outputStream.write("CompanyDivision \t".getBytes());
	    	outputStream.write("Job \t".getBytes());
	    	outputStream.write("ShipNumber \t".getBytes());
	    	outputStream.write("Revision Revenue Amt \t".getBytes());
	    	outputStream.write("Actual Revenue \t".getBytes());
	    	outputStream.write("Charge Code \t".getBytes());
	    	outputStream.write("Bill Complete \t".getBytes());
	    	outputStream.write("Rev Recog \t".getBytes());
	    	outputStream.write("RecGl \t".getBytes());    	
	    	outputStream.write("Bill To Code \t".getBytes());
	    	outputStream.write("Bill To Name \t".getBytes());
	    	outputStream.write("Accrue Revenue \t".getBytes());
	    	outputStream.write("Actual Delivery \t".getBytes());
	    	outputStream.write("Billers Name \t".getBytes());
	    	outputStream.write("AccountLine ID \t".getBytes());
	    	outputStream.write("\r\n".getBytes());
	    	Iterator it = accrualList.iterator();
	    	while(it.hasNext()){
	    		Object extracts = it.next();
	    		if(((AccrualProcessDTO) extracts).getCompanyDivision()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getCompanyDivision().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getJob()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getJob().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getShipNumber()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getShipNumber().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevisionRevenueAmount()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRevisionRevenueAmount().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getActualRevenue()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getActualRevenue().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getChargeCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getChargeCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillComplete()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getBillComplete()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRevRecog()!=null){
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getRevRecog()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getRecGl()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getRecGl().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillToCode()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getBillToCode().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getBillToName()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getBillToName().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
		    	if(((AccrualProcessDTO) extracts).getAccrueRevenue()!=null){
		    		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getAccrueRevenue()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
		    	if(((AccrualProcessDTO) extracts).getActualDelivery()!=null){
		    		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
					StringBuilder date = new StringBuilder(format.format(((AccrualProcessDTO) extracts).getActualDelivery()));
					outputStream.write((date.toString() + "\t").getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getPersonBilling()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getPersonBilling().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}
	    		if(((AccrualProcessDTO) extracts).getAccountLineId()!=null){
	    			outputStream.write((""+(((AccrualProcessDTO) extracts).getAccountLineId().toString() + "\t")).getBytes());
	    		}else{
	    			outputStream.write("\t".getBytes());
	    		}	    		
	    		outputStream.write("\r\n".getBytes());
	    		}
	    	}else{
	    		HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("NO RECORD FOUND");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				String header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
	    	}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public void setGotoPageString(String gotoPageString) {
	   this.gotoPageString = gotoPageString;
	}

    public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public Map<String, String> getGlType() {
		return glType;
	}

	public void setGlType(Map<String, String> glType) {
		this.glType = glType;
	}

	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public void setAccrualProcessManager(AccrualProcessManager accrualProcessManager) {
		this.accrualProcessManager = accrualProcessManager;
	}

	public List getAccrualList() {
		return accrualList;
	}

	public void setAccrualList(List accrualList) {
		this.accrualList = accrualList;
	}

	public void setVanLineGLTypeManager(VanLineGLTypeManager vanLineGLTypeManager) {
		this.vanLineGLTypeManager = vanLineGLTypeManager;
	}
	
}
