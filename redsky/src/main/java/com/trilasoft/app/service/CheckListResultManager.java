package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CheckListResult;

public interface CheckListResultManager extends GenericManager<CheckListResult,Long> {
	public List getById();
}
