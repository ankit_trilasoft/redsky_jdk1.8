<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="agentRequest.title"/></title>   
    <meta name="heading" content="<fmt:message key="agentRequest.title"/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">

</script>
<style>

span.pagelinks {display:block;margin-bottom:0px;!margin-bottom:2px;margin-top:-10px;!margin-top:-17px;padding:2px 0px;text-align:right;width:99%;!width:98%;font-size:0.85em;
}
.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;}
.radiobtn {margin:0px 0px 2px 0px;!padding-bottom:5px;!margin-bottom:5px;}
form {margin-top:-10px;!margin-top:0px;}

div#main {margin:-5px 0 0;}
input[type="checkbox"] {vertical-align:middle;}
.br-n-ctnr {border:none !important;text-align:center !important;}
.br-n-rgt {border:none !important;text-align:right !important;}
#overlay190 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
</head>
<c:set var="buttons">     
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/agentRequestFinal.html"/>'" value="<fmt:message key="button.add"/>"/>         
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
<c:if test="${viewAgentLists!='[]'}">				  
    <div class="message" id="successMessages">
    <img src="<c:url value="/images/iconInformation.gif"/>"
            alt="<fmt:message key="icon.information"/>" class="icon" />&nbsp;"Similar Agent records are listed..If you want to APPROVED Your newly created agent then select APPROVED or Else select radio buton it will reject your Request";
         	</div>
    </c:if>
    <c:if test="${viewAgentLists=='[]'}">				  
    <div class="message" id="failureMessages">
    <img src="<c:url value="/images/iconInformation.gif"/>"
            alt="<fmt:message key="icon.information"/>" class="icon" />&nbsp;"Records are not found

            ";
         	</div>
    </c:if>

<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
	<ul>
		<li><a class="current"><span>Agent List</span></a></li>
	</ul>
</div><div class="spnblk">&nbsp;</div> 

 <s:form id="agentListForm" action="searchAgentRequest.html" method="post" > 
 <s:hidden name="agentRequestId" value="${agentRequest.id}" />
<s:set name="viewAgentLists" value="viewAgentLists" scope="request"/> 
<s:hidden name="radioId" />

<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="createdBy" value="<%=request.getParameter("createdBy") %>"/>
<s:hidden name="updatedBy" value="<%=request.getParameter("updatedBy") %>"/>
<display:table name="viewAgentList" id="viewAgentLists" class="table" requestURI="" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;">  		
	
    <display:column><input id="rad" type="radio" name="dd"   value="${viewAgentLists.id}" onclick="check(this.value,'${viewAgentLists.lastName}','${viewAgentLists.partnerCode}','${viewAgentLists.corpID}')"/></display:column>
	
	<display:column property="lastName" sortable="true" title="Alias Name" style="width:65px"/>
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/> 
	<display:column property="add1" sortable="true" title="Adddress" style="width:150px"/> 
	<display:column property="email" sortable="true" title="Email" style="width:150px"/> 
	<display:column property="zip" sortable="true" title="Zip" style="width:150px"/> 
	<display:column property="status" sortable="true" title="status" style="width:150px"/> 
    <display:column  sortable="true" title="Created By" style="width:50px;"  value="<%=request.getParameter("createdBy") %>" ></display:column>

	<display:column  sortable="true" title="Updated By" style="width:50px;"  value="<%=request.getParameter("updatedBy") %>" ></display:column>

	
   
    
</display:table>
<tr>
  <c:if test="${viewAgentLists!='[]' }">		
    <s:select id="status" cssClass="list-menu" name="status" list="{'Approved'}" onchange="statusValue(this);" headerKey="" headerValue="" cssStyle="width:75px" />
</c:if> 
<c:if test="${viewAgentLists=='[]' }">		
    <s:select id="status" cssClass="list-menu" name="status" list="{'Approved'}" onchange="statusValue(this);" headerKey="" headerValue="" cssStyle="width:75px" />
</c:if> 
</tr>

<div id="overlay190">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing .......Please wait<br></font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
			
</s:form>
<tr>

</div> 
<c:set var="isTrue" value="false" scope="session"/>

<script type="text/javascript">

function check(temp,name,partnercode,corpId)
{    
	
	var rejected='Rejected';
	    var createdBy='<%=request.getParameter("createdBy") %>';
	     document.getElementById("status").style.display="none";
	     var uid = '${pageContext.request.remoteUser}';
	     var agentid=document.forms['agentListForm'].elements['id'].value;
	     var postDate = valButton(document.forms['agentListForm'].elements['dd']); 
	    
		 
		 var url="updatedAgentData.html?ajax=1&decorator=simple&popup=true&lastName="+name+"&partnerCode="+partnercode+"&agentId="+agentid+"&radioId="+encodeURI(postDate)+"&agentStatus="+rejected+"&createdBy="+createdBy+"&corpId="+corpId;
	 	   
	     http2.open("GET", url, true);
	     http2.onreadystatechange = function(){handleHttpResponse10(name,partnercode,agentid,uid,corpId);} ;
	     http2.send(null);
	     window.location.reload();
	}
	
	function statusValue(target)
	{
		 
		var agentid=document.forms['agentListForm'].elements['id'].value;
		showOrHide(1);
	    var url="updatedAgentData.html?ajax=1&decorator=simple&popup=true&agentStatus=" + encodeURI(target.value)+"&agentId="+agentid;
	
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse9;
	     http2.send(null);
	
}
    
	  function valButton(btn) {
		
	    var cnt = -1; 
	    var len = btn.length; 
	    if(len >1)
	    {
	    for (var i=btn.length-1; i > -1; i--) {
		        if (btn[i].checked) {cnt = i; i = -1;}
		    }
		    if (cnt > -1) return btn[cnt].value;
		    else return null;
	    	
	    }
	    else
	    { 
	    	return document.forms['agentListForm'].elements['dd'].value; 
	    } 
	  }
	  
function handleHttpResponse9()
       {
            if (http2.readyState == 4)
            {
            	showOrHide(0); 
               var results = http2.responseText
               results = results.trim();
               var res = results.split("#");
               showOrHide(0);
               alert("The newly created Agent has been Approved. Hence Email has been sent to your email id  that your Request has been Approved");
              
                  
              } 
       }
function handleHttpResponse10(name,partnercode,agentid,uid,corpid)
{
	
     if (http2.readyState == 4)
     {
    	
        var results = http2.responseText
        results = results.trim();
        var res = results.split(" ");
       
        if(results.length>=1)
        {
        	
        alert("The selected Agent is already Exist Hence Email has been sent to your email id  that your Request has been rejected ") 	
        var createdBy=document.forms['agentListForm'].elements['createdBy'].value;
        var agree = confirm("Do you want to Reject the Request Of Agent ?click OK to proceed or Cancel.");
		if(agree){
		   
		   /* window.open('editAgentRequestReason.html?&createdBy='+createdBy+'&corpId='+corpid+'&decorator=popup&popup=true','forms','height=600,width=600,top=1, left=120, scrollbars=yes,resizable=yes');
		    */
		    var url = "editAgentRequestReason.html?createdBy='+createdBy+'&corpId='+corpid";	 
			document.forms['agentListForm'].action= url;	 
			document.forms['agentListForm'].submit();
			return true;
			return true;	 
		    } 
		
     	/* var originEmail = res[0];
		
		subject = "Request Has been Rejected";
         var body_message = "The Selected agent "+name+" is already existing in our system RedSky. "+partnercode+" is the agent code for the same.'"; 
		
		var mailto_link = 'mailto:'+res[0]+'?subject='+subject+'&body='+body_message;
		
	alert(mailto_link)
	win = window.open(mailto_link,'emailWindow'); 
	if (win && win.open &&!win.closed) win.close(); 
	return true; */
       }  }
}
var http2 = getHTTPObject();
function getHTTPObject6(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function showOrHide(value) {
        if (value==0) {
           if (document.layers)
               document.layers["overlay190"].visibility='hide';
            else
               document.getElementById("overlay190").style.visibility='hidden';
       }
       else if (value==1) {
          if (document.layers)
              document.layers["overlay190"].visibility='show';
           else
              document.getElementById("overlay190").style.visibility='visible';
       }
    }
/* function sendEmail1(target){
	The below agent “Star International Movers” is already existing in our system RedSky. T164581 is the agent code for the same.
 	var originEmail = target;
		var subject = 'C/F# ${customerFile.sequenceNumber}';
		subject = subject+" ${customerFile.firstName}";
		subject = subject+" ${customerFile.lastName}";
		
	var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
	win = window.open(mailto_link,'emailWindow'); 
	if (win && win.open &&!win.closed) win.close(); 
}  */
showOrHide(0);

</script>
