<%@ include file="/common/taglibs.jsp"%> 

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:15px;
margin-top:-40px;
padding:2px 0px;
text-align:right;
width:97%;
}
</style>
<head> 

<title><fmt:message key="portalList.title"/></title> 

<meta name="heading" content="<fmt:message key='portalList.heading'/>"/> 

<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['searchForm'].elements['searchByCode'].value = "";
			document.forms['searchForm'].elements['searchByName'].value = "";
			document.forms['searchForm'].elements['searchByCountryCode'].value = "";
			document.forms['searchForm'].elements['searchByState'].value = "";
			document.forms['searchForm'].elements['searchByCity'].value = "";
}

function generalList(){
document.forms['searchForm'].elements['general'].value=true;
document.forms['searchForm'].elements['searchByName'].value="";
document.forms['searchForm'].elements['searchByCode'].value="";
document.forms['searchForm'].elements['searchByCountryCode'].value="";
document.forms['searchForm'].elements['searchByState'].value="";
document.forms['searchForm'].elements['searchByCity'].value="";
document.forms['searchForm'].submit();

}

function submittedList(){
document.forms['searchForm'].elements['general'].value=false;
document.forms['searchForm'].elements['searchByName'].value="";
document.forms['searchForm'].elements['searchByCode'].value="";
document.forms['searchForm'].elements['searchByCountryCode'].value="";
document.forms['searchForm'].elements['searchByState'].value="";
document.forms['searchForm'].elements['searchByCity'].value="";
document.forms['searchForm'].submit();

}

</script>

</head> 

<s:form id="searchForm" action="accountLineVendor.html?decorator=popup&popup=true" method="post" validate="true">  
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>

<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
</c:if>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="quoteType" value="<%=request.getParameter("quoteType")%>"/>

<s:hidden name="general" />

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
	 
 <div id="Layer1">
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<table class="table" style="width:850px;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th><fmt:message key="partner.billingCountryCode"/></th>
<th><fmt:message key="partner.billingState"/></th>
<th><fmt:message key="partner.billingCity"/></th>
<th style="background:none;border:none;"></th>
<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
<c:set var="id" value="<%=request.getParameter("id")%>" scope="session"/>
</tr>
</thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="searchByCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="searchByName" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="searchByCountryCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="searchByState" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="searchByCity" cssClass="text medium"/>
			</td>
			<td width="130px">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		
		</tbody>
	</table>

<!--  
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
  <ul id="newmnav">
  <td><input type="button" class="cssbutton" style="width:75px; height:25px" onclick="submittedList();" value="Quotations"/></td>
  <td><input type="button" class="cssbutton" style="width:75px; height:25px" onclick="generalList();" value="All Vendors"/></td>
  
  
</ul>
</td></tr>
</tbody></table>
-->
<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
<c:if test="${general}">


<div id="newmnav">
		  <ul>
		  <li><a onclick="submittedList();"><span>Quotations</span></a></li>
		  <li id="newmnav1" style="background:#FFF "><a  onclick="generalList();" class="current"><span>All Vendors<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		    </ul>
</div><div class="spn">&nbsp;</div><br>
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="1" pagesize="10" 
decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
 
	<display:column titleKey="partner.name"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column property="rank" sortable="true" titleKey="partner.rank" style="width:15px"/>
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:35px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:110px"/>
    <display:column property="doNotUse" sortable="true" titleKey="partner.doNotUse" style="width:65px" format="{0,date,dd-MMM-yyyy}"/>
   
</display:table> 
</c:if>
<c:if test="${!general}">


<div id="newmnav">
		  <ul>
		  <li id="newmnav1" style="background:#FFF "><a onclick="submittedList();" class="current"><span>Quotations<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  <li><a  onclick="generalList();"><span>All Vendors</span></a></li>
		   </ul>
</div><div class="spn">&nbsp;</div><br>

<display:table name="partnerQuotess" class="table" requestURI="" id="portalList" export="${empty param.popup}" defaultsort="1" pagesize="10" 
decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="vendorCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerQuote.html" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
 
<display:column property="vendorName" sortable="true" titleKey="partner.name"/> 
<display:column property="totalPrice" sortable="true" titleKey="partnerQuote.QuotationTotals"/>
</display:table> 
</c:if>

 </div>
 <c:if test="${empty param.popup}"> 
<c:out value="${buttons}" escapeXml="false" /> 
</c:if>
</s:form>
<script type="text/javascript"> 

highlightTableRows("portalList"); 

</script> 
