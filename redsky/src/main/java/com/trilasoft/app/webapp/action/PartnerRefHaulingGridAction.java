package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.PartnerRefHaulingGrid;
import com.trilasoft.app.service.PartnerRefHaulingGridManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerRefHaulingGridAction extends BaseAction {

	private Long id;
	
	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;
	
	private PartnerRefHaulingGrid partnerRefHaulingGrid;
	
	private PartnerRefHaulingGridManager partnerRefHaulingGridManager;
	
	private List partnerRefHaulingGridList;
	
	private RefMasterManager refMasterManager;
	
	private Map<String, String> country;
	
	private Map<String, String> grid;

	private String hitflag;

	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}
	
	@SkipValidation
	public String getComboList(String corpId) {
		country = refMasterManager.findByParameter(corpId, "COUNTRY");
		grid = refMasterManager.findByParameter(corpId, "GRID");
		return SUCCESS;
	}
	
	@SkipValidation
	public String list() {
		getComboList(sessionCorpID);
		partnerRefHaulingGridList = partnerRefHaulingGridManager.getAll();
		return SUCCESS;
	}

	public PartnerRefHaulingGridAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			partnerRefHaulingGrid = partnerRefHaulingGridManager.get(id);
		} else {
			partnerRefHaulingGrid = new PartnerRefHaulingGrid();
			partnerRefHaulingGrid.setCorpID(sessionCorpID);
			
			partnerRefHaulingGrid.setCreatedOn(new Date());
			partnerRefHaulingGrid.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}

	public String save() throws Exception {
		getComboList(sessionCorpID);
		if (cancel != null) {
			return "cancel";
		}

		partnerRefHaulingGrid.setCorpID(sessionCorpID);
		boolean isNew = (partnerRefHaulingGrid.getId() == null);
		
		if (isNew) {
			partnerRefHaulingGrid.setCreatedOn(new Date());
			partnerRefHaulingGrid.setCreatedBy(getRequest().getRemoteUser());
		}
		partnerRefHaulingGrid.setUpdatedOn(new Date());
		partnerRefHaulingGrid.setUpdatedBy(getRequest().getRemoteUser());
		partnerRefHaulingGridManager.save(partnerRefHaulingGrid);
		hitflag = "1";
		if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
			String key = (isNew) ? "partnerRefHaulingGrid.added" : "partnerRefHaulingGrid.updated";
			saveMessage(getText(key));
		}
		return SUCCESS;
	}
	public String searchHaulingGridList(){
		getComboList(sessionCorpID);
		String lowDistance;
		String highDistance;
		String rateFlat;
		String rateMile;
			if(partnerRefHaulingGrid.getLowDistance()==null){
				lowDistance="";
			}
			else
			{
				lowDistance=partnerRefHaulingGrid.getLowDistance().toString();
			}
			if(partnerRefHaulingGrid.getHighDistance()==null){
				highDistance="";
			}
			else
			{
				highDistance=partnerRefHaulingGrid.getHighDistance().toString();
			}
			if(partnerRefHaulingGrid.getRateFlat()==null){
				rateFlat="";
			}
			else
			{
				rateFlat=partnerRefHaulingGrid.getRateFlat().toString();
			}
			if(partnerRefHaulingGrid.getRateMile()==null){
				rateMile="";
			}
			else
			{
				rateMile=partnerRefHaulingGrid.getRateMile().toString();
			}
	partnerRefHaulingGridList =partnerRefHaulingGridManager.getHaulingGridList(lowDistance,highDistance,rateMile,rateFlat,partnerRefHaulingGrid.getGrid(),partnerRefHaulingGrid.getUnit(),partnerRefHaulingGrid.getCountryCode(),sessionCorpID);
		return SUCCESS;
}
	public String delete() {
		partnerRefHaulingGridManager.remove(id);
		saveMessage(getText("Partner Hauling Grid Item is deleted sucessfully"));
		
		return SUCCESS;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public PartnerRefHaulingGrid getPartnerRefHaulingGrid() {
		return partnerRefHaulingGrid;
	}

	public void setPartnerRefHaulingGrid(PartnerRefHaulingGrid partnerRefHaulingGrid) {
		this.partnerRefHaulingGrid = partnerRefHaulingGrid;
	}

	public void setPartnerRefHaulingGridManager(
			PartnerRefHaulingGridManager partnerRefHaulingGridManager) {
		this.partnerRefHaulingGridManager = partnerRefHaulingGridManager;
	}

	public List getPartnerRefHaulingGridList() {
		return partnerRefHaulingGridList;
	}

	public void setPartnerRefHaulingGridList(List partnerRefHaulingGridList) {
		this.partnerRefHaulingGridList = partnerRefHaulingGridList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public Map<String, String> getGrid() {
		return grid;
	}

	public void setGrid(Map<String, String> grid) {
		this.grid = grid;
	}

}