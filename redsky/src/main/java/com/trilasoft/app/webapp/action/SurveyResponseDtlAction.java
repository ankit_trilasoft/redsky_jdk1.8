package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;



import com.trilasoft.app.model.SurveyResponseDtl;
import com.trilasoft.app.service.SurveyResponseDtlManager;
public class SurveyResponseDtlAction extends BaseAction{
	
	private SurveyResponseDtlManager surveyResponseDtlManager;
	private SurveyResponseDtl surveyResponseDtl;
	private List surveyResponseDtlList;
	private String sessionCorpID;
	private Long id;
	static final Logger logger = Logger.getLogger(SurveyResponseDtl.class);
	public SurveyResponseDtlAction()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID=user.getCorpID();	
	}
	
	public SurveyResponseDtl getSurveyResponseDtl() {
		return surveyResponseDtl;
	}
	public void setSurveyResponseDtl(SurveyResponseDtl surveyResponseDtl) {
		this.surveyResponseDtl = surveyResponseDtl;
	}
	public List getSurveyResponseDtlList() {
		return surveyResponseDtlList;
	}
	public void setSurveyResponseDtlList(List surveyResponseDtlList) {
		this.surveyResponseDtlList = surveyResponseDtlList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setSurveyResponseDtlManager(
			SurveyResponseDtlManager surveyResponseDtlManager) {
		this.surveyResponseDtlManager = surveyResponseDtlManager;
	}	
}
