package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.GlCodeRateGridDao;
import com.trilasoft.app.model.GlCodeRateGrid;
import com.trilasoft.app.service.GlCodeRateGridManager;

public class GlCodeRateGridManagerImpl  extends GenericManagerImpl<GlCodeRateGrid, Long> implements GlCodeRateGridManager{
	GlCodeRateGridDao glCodeRateGridDao;
	public GlCodeRateGridManagerImpl(GlCodeRateGridDao glCodeRateGridDao) {
		super(glCodeRateGridDao); 
        this.glCodeRateGridDao = glCodeRateGridDao; 
		
	}
	public List getAllRateGridList(String sessionCorpID,String costId,String jobType,String routing1,String recGl,String payGl){
		return glCodeRateGridDao.getAllRateGridList(sessionCorpID, costId,jobType,routing1,recGl,payGl);
	}
}
