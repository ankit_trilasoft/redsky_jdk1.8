<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineGLTypeList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineGLTypeList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=3;i++){
			document.forms['searchForm'].elements[i].value = "";
		}
}
</script>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-16px;
!margin-top:-15px;
padding:0px 0;
text-align:right;
width:99%;
}
form{margin-top:-40px;}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVanLineGLType.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;!margin-bottom: 10px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>   

     
<s:form id="searchForm" action="searchVanLineGLType" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
<div class="center-content">
<table class="table" style="width:100%;"  >
	<thead>
		<tr>
			<th><fmt:message key="vanLineGLType.glType"/></th>
			<th><fmt:message key="vanLineGLType.description"/></th>
			<th><fmt:message key="vanLineGLType.glCode"/></th>
			
		</tr>
	</thead>	
		<tbody>
		<tr>
			<td width="" align="left"> <s:textfield name="vanLineGLType.glType" required="true" cssClass="input-text" size="30"/> </td>
			<td width="" align="left"> <s:textfield name="vanLineGLType.description" required="true" cssClass="input-text" size="30"/> </td>
			<td width="" align="left"> <s:textfield name="vanLineGLType.glCode" required="true" cssClass="input-text" size="30"/> </td>
			
		</tr>
		<tr>
		<td colspan="2"></td>
		<td  align="center"  style="border-left:hidden;"> <c:out value="${searchbuttons}" escapeXml="false" /> </td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>GL Type List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="vanLineGLTypeList" value="vanLineGLTypeList" scope="request"/>   
<display:table name="vanLineGLTypeList" class="table" requestURI="" id="vanLineGLTypeList" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
    <display:column property="glType" sortable="true" titleKey="vanLineGLType.glType" url="/editVanLineGLType.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="description" sortable="true" titleKey="vanLineGLType.description"/>
    <display:column property="glCode" sortable="true" titleKey="vanLineGLType.glCode"/>
    <display:column property="discrepancy" headerClass="containeralign" style="text-align: right" sortable="true" titleKey="vanLineGLType.discrepancy"/>
    <display:column property="shortHaul" sortable="true" titleKey="vanLineGLType.shortHaul"/> 
    <display:column property="bucket" sortable="true" titleKey="vanLineGLType.bucket" />
      
    <display:setProperty name="paging.banner.item_name" value="vanLineGLType"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLineGLTypes"/>   
  
    <display:setProperty name="export.excel.filename" value="Van Line GL Type List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line GL Type List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line GL Type List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 
<script type="text/javascript">   
    highlightTableRows("vanLineGLTypeList");  
    Form.focusFirstElement($("searchForm"));    
</script> 