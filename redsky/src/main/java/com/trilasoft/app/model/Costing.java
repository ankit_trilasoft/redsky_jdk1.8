package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;



import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="costing")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class Costing extends BaseObject{
	
	private String corpID;
	private Long id;
	private String shipNumber;
	private String invoiceNumber;
	private String expInvoiceNumber;
	private Date invoiceDate;
	private Date expInvoiceDate;
	private String expPayeeName;
	private BigDecimal expTotalInvoice;
	private String expPayeeCode;
	private String expAccountId;
	private String expAction;
	private Date expActionDate;
	private String expComment;
	private String expPayeeType;
	private Date expPaidThem;
	private BigDecimal expExchangeRate;
	private BigDecimal expAmountFx;
	private Date expValInvoiceDate;
	private String expCurrencyCode;
	private BigDecimal expEstimateExpenses;
	private String expPer2post;
	private String expAccountXrefference;
	private Date expSentAccount;
	private Date expPostDate;
	private String expSentFile;
	private String expGl;
	private String invoiceCheckNumber;
	private BigDecimal expRevisedAmount;
	private Date expRevisedDate;
	private Date expEstimateDate;
	
	
	   //New Field 
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	
	//For Header
	private String contract;
	private String poNumber;
	private String type;
	private String branch;
	private String driver;
	private Long line;
	private Long ticketNumber;
	private String recPay;
	
	
	//receivable field
	private Boolean passthrough;
    private BigDecimal rate;
    private BigDecimal driverAmtount;
    private BigDecimal discount;
    private String billToCode;
    private String billToName;
    private BigDecimal amount;
    private BigDecimal quantity;
    private BigDecimal estimate;
    private Date storageBegin;
    private Date estimateRevisedDate;
    private BigDecimal estimateRevisedAmount;
    private Date paid;
    private Date storageEnd;
    private String comment1;
    private String comment2;
    private String gl;
    private String warehouse;
    private Date sentWb;
    private Date estimateDate;
    private BigDecimal entitledAmount;
    private Date estimateSent;
    private BigDecimal per;
    private String perItem;
    private int days;
    private int ticket;
    private String item;
    private String divMult;
    private BigDecimal intTicket;
    private int rIdNumber;
    private int storageBill;
    private String postPer;
    private String bucket;
    //private String driver;
    private String commodity;
    private Date sendEntitled;
    private Date entitledDate;
    private String description;
    private Date doNotSent;
    private Date sentAccount;
    
    private Date postDate;
    private String sentFile;
        
    //For Mapping 
	private ServiceOrder serviceOrder;
	
	
	

	


	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("shipNumber", shipNumber).append("invoiceNumber",
				invoiceNumber).append("expInvoiceNumber", expInvoiceNumber)
				.append("invoiceDate", invoiceDate).append("expInvoiceDate",
						expInvoiceDate).append("expPayeeName", expPayeeName)
				.append("expTotalInvoice", expTotalInvoice).append(
						"expPayeeCode", expPayeeCode).append("expAccountId",
						expAccountId).append("expAction", expAction).append(
						"expActionDate", expActionDate).append("expComment",
						expComment).append("expPayeeType", expPayeeType)
				.append("expPaidThem", expPaidThem).append("expExchangeRate",
						expExchangeRate).append("expAmountFx", expAmountFx)
				.append("expValInvoiceDate", expValInvoiceDate).append(
						"expCurrencyCode", expCurrencyCode).append(
						"expEstimateExpenses", expEstimateExpenses).append(
						"expPer2post", expPer2post).append(
						"expAccountXrefference", expAccountXrefference).append(
						"expSentAccount", expSentAccount).append("expPostDate",
						expPostDate).append("expSentFile", expSentFile).append(
						"expGl", expGl).append("invoiceCheckNumber",
						invoiceCheckNumber).append("expRevisedAmount",
						expRevisedAmount).append("expRevisedDate",
						expRevisedDate).append("expEstimateDate",
						expEstimateDate).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("valueDate", valueDate)
				.append("contract", contract).append("poNumber", poNumber)
				.append("type", type).append("branch", branch).append("driver",
						driver).append("line", line).append("ticketNumber",
						ticketNumber).append("recPay", recPay).append(
						"passthrough", passthrough).append("rate", rate)
				.append("driverAmtount", driverAmtount).append("discount",
						discount).append("billToCode", billToCode).append(
						"billToName", billToName).append("amount", amount)
				.append("quantity", quantity).append("estimate", estimate)
				.append("storageBegin", storageBegin).append(
						"estimateRevisedDate", estimateRevisedDate).append(
						"estimateRevisedAmount", estimateRevisedAmount).append(
						"paid", paid).append("storageEnd", storageEnd).append(
						"comment1", comment1).append("comment2", comment2)
				.append("gl", gl).append("warehouse", warehouse).append(
						"sentWb", sentWb).append("estimateDate", estimateDate)
				.append("entitledAmount", entitledAmount).append(
						"estimateSent", estimateSent).append("per", per)
				.append("perItem", perItem).append("days", days).append(
						"ticket", ticket).append("item", item).append(
						"divMult", divMult).append("intTicket", intTicket)
				.append("rIdNumber", rIdNumber).append("storageBill",
						storageBill).append("postPer", postPer).append(
						"bucket", bucket).append("commodity", commodity)
				.append("sendEntitled", sendEntitled).append("entitledDate",
						entitledDate).append("description", description)
				.append("doNotSent", doNotSent).append("sentAccount",
						sentAccount).append("postDate", postDate).append(
						"sentFile", sentFile).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Costing))
			return false;
		Costing castOther = (Costing) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(shipNumber, castOther.shipNumber).append(
				invoiceNumber, castOther.invoiceNumber).append(
				expInvoiceNumber, castOther.expInvoiceNumber).append(
				invoiceDate, castOther.invoiceDate).append(expInvoiceDate,
				castOther.expInvoiceDate).append(expPayeeName,
				castOther.expPayeeName).append(expTotalInvoice,
				castOther.expTotalInvoice).append(expPayeeCode,
				castOther.expPayeeCode).append(expAccountId,
				castOther.expAccountId).append(expAction, castOther.expAction)
				.append(expActionDate, castOther.expActionDate).append(
						expComment, castOther.expComment).append(expPayeeType,
						castOther.expPayeeType).append(expPaidThem,
						castOther.expPaidThem).append(expExchangeRate,
						castOther.expExchangeRate).append(expAmountFx,
						castOther.expAmountFx).append(expValInvoiceDate,
						castOther.expValInvoiceDate).append(expCurrencyCode,
						castOther.expCurrencyCode).append(expEstimateExpenses,
						castOther.expEstimateExpenses).append(expPer2post,
						castOther.expPer2post).append(expAccountXrefference,
						castOther.expAccountXrefference).append(expSentAccount,
						castOther.expSentAccount).append(expPostDate,
						castOther.expPostDate).append(expSentFile,
						castOther.expSentFile).append(expGl, castOther.expGl)
				.append(invoiceCheckNumber, castOther.invoiceCheckNumber)
				.append(expRevisedAmount, castOther.expRevisedAmount).append(
						expRevisedDate, castOther.expRevisedDate).append(
						expEstimateDate, castOther.expEstimateDate).append(
						createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(valueDate,
						castOther.valueDate).append(contract,
						castOther.contract)
				.append(poNumber, castOther.poNumber).append(type,
						castOther.type).append(branch, castOther.branch)
				.append(driver, castOther.driver).append(line, castOther.line)
				.append(ticketNumber, castOther.ticketNumber).append(recPay,
						castOther.recPay).append(passthrough,
						castOther.passthrough).append(rate, castOther.rate)
				.append(driverAmtount, castOther.driverAmtount).append(
						discount, castOther.discount).append(billToCode,
						castOther.billToCode).append(billToName,
						castOther.billToName).append(amount, castOther.amount)
				.append(quantity, castOther.quantity).append(estimate,
						castOther.estimate).append(storageBegin,
						castOther.storageBegin).append(estimateRevisedDate,
						castOther.estimateRevisedDate).append(
						estimateRevisedAmount, castOther.estimateRevisedAmount)
				.append(paid, castOther.paid).append(storageEnd,
						castOther.storageEnd).append(comment1,
						castOther.comment1)
				.append(comment2, castOther.comment2).append(gl, castOther.gl)
				.append(warehouse, castOther.warehouse).append(sentWb,
						castOther.sentWb).append(estimateDate,
						castOther.estimateDate).append(entitledAmount,
						castOther.entitledAmount).append(estimateSent,
						castOther.estimateSent).append(per, castOther.per)
				.append(perItem, castOther.perItem)
				.append(days, castOther.days).append(ticket, castOther.ticket)
				.append(item, castOther.item)
				.append(divMult, castOther.divMult).append(intTicket,
						castOther.intTicket).append(rIdNumber,
						castOther.rIdNumber).append(storageBill,
						castOther.storageBill).append(postPer,
						castOther.postPer).append(bucket, castOther.bucket)
				.append(commodity, castOther.commodity).append(sendEntitled,
						castOther.sendEntitled).append(entitledDate,
						castOther.entitledDate).append(description,
						castOther.description).append(doNotSent,
						castOther.doNotSent).append(sentAccount,
						castOther.sentAccount).append(postDate,
						castOther.postDate)
				.append(sentFile, castOther.sentFile).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				shipNumber).append(invoiceNumber).append(expInvoiceNumber)
				.append(invoiceDate).append(expInvoiceDate)
				.append(expPayeeName).append(expTotalInvoice).append(
						expPayeeCode).append(expAccountId).append(expAction)
				.append(expActionDate).append(expComment).append(expPayeeType)
				.append(expPaidThem).append(expExchangeRate)
				.append(expAmountFx).append(expValInvoiceDate).append(
						expCurrencyCode).append(expEstimateExpenses).append(
						expPer2post).append(expAccountXrefference).append(
						expSentAccount).append(expPostDate).append(expSentFile)
				.append(expGl).append(invoiceCheckNumber).append(
						expRevisedAmount).append(expRevisedDate).append(
						expEstimateDate).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(valueDate).append(
						contract).append(poNumber).append(type).append(branch)
				.append(driver).append(line).append(ticketNumber)
				.append(recPay).append(passthrough).append(rate).append(
						driverAmtount).append(discount).append(billToCode)
				.append(billToName).append(amount).append(quantity).append(
						estimate).append(storageBegin).append(
						estimateRevisedDate).append(estimateRevisedAmount)
				.append(paid).append(storageEnd).append(comment1).append(
						comment2).append(gl).append(warehouse).append(sentWb)
				.append(estimateDate).append(entitledAmount).append(
						estimateSent).append(per).append(perItem).append(days)
				.append(ticket).append(item).append(divMult).append(intTicket)
				.append(rIdNumber).append(storageBill).append(postPer).append(
						bucket).append(commodity).append(sendEntitled).append(
						entitledDate).append(description).append(doNotSent)
				.append(sentAccount).append(postDate).append(sentFile)
				.toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="shipNumber", nullable=false, updatable=false,
	referencedColumnName="shipNumber")
	
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	
	@Column(length=8)
	public String getExpAccountId() {
		return expAccountId;
	}
	public void setExpAccountId(String expAccountId) {
		this.expAccountId = expAccountId;
	}
	
	@Column(length=8)
	public String getExpAccountXrefference() {
		return expAccountXrefference;
	}
	public void setExpAccountXrefference(String expAccountXrefference) {
		this.expAccountXrefference = expAccountXrefference;
	}
	
	@Column(length=1)
	public String getExpAction() {
		return expAction;
	}
	public void setExpAction(String expAction) {
		this.expAction = expAction;
	}
	
	@Column
	public Date getExpActionDate() {
		return expActionDate;
	}
	public void setExpActionDate(Date expActionDate) {
		this.expActionDate = expActionDate;
	}
	
	@Column(length=20)
	public BigDecimal getExpAmountFx() {
		return expAmountFx;
	}
	public void setExpAmountFx(BigDecimal expAmountFx) {
		this.expAmountFx = expAmountFx;
	}
	
	@Column(length=100)
	public String getExpComment() {
		return expComment;
	}
	public void setExpComment(String expComment) {
		this.expComment = expComment;
	}
	
	@Column(length=65)
	public String getExpCurrencyCode() {
		return expCurrencyCode;
	}
	public void setExpCurrencyCode(String expCurrencyCode) {
		this.expCurrencyCode = expCurrencyCode;
	}
	
	@Column(length=20)
	public BigDecimal getExpEstimateExpenses() {
		return expEstimateExpenses;
	}
	public void setExpEstimateExpenses(BigDecimal expEstimateExpenses) {
		this.expEstimateExpenses = expEstimateExpenses;
	}
	
	@Column(length=20)
	public BigDecimal getExpExchangeRate() {
		return expExchangeRate;
	}
	public void setExpExchangeRate(BigDecimal expExchangeRate) {
		this.expExchangeRate = expExchangeRate;
	}
	
	@Column
	public Date getExpPaidThem() {
		return expPaidThem;
	}
	public void setExpPaidThem(Date expPaidThem) {
		this.expPaidThem = expPaidThem;
	}
	
	@Column(length=8)
	public String getExpPayeeCode() {
		return expPayeeCode;
	}
	public void setExpPayeeCode(String expPayeeCode) {
		this.expPayeeCode = expPayeeCode;
	}
	
	@Column(length=30)
	public String getExpPayeeName() {
		return expPayeeName;
	}
	public void setExpPayeeName(String expPayeeName) {
		this.expPayeeName = expPayeeName;
	}
	
	@Column(length=2)
	public String getExpPayeeType() {
		return expPayeeType;
	}
	public void setExpPayeeType(String expPayeeType) {
		this.expPayeeType = expPayeeType;
	}
	
	@Column(length=6)
	public String getExpPer2post() {
		return expPer2post;
	}
	public void setExpPer2post(String expPer2post) {
		this.expPer2post = expPer2post;
	}
	
	@Column
	public Date getExpSentAccount() {
		return expSentAccount;
	}
	public void setExpSentAccount(Date expSentAccount) {
		this.expSentAccount = expSentAccount;
	}
	
	@Column(length=20)
	public BigDecimal getExpTotalInvoice() {
		return expTotalInvoice;
	}
	public void setExpTotalInvoice(BigDecimal expTotalInvoice) {
		this.expTotalInvoice = expTotalInvoice;
	}
	
	@Column
	public Date getExpValInvoiceDate() {
		return expValInvoiceDate;
	}
	public void setExpValInvoiceDate(Date expValInvoiceDate) {
		this.expValInvoiceDate = expValInvoiceDate;
	}
	

	
	
	@Column(length=20)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	@Column(length=8)
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	
	@Column(length=30)
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	
	@Column(length=45)
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	
	@Column(length=80)
	public String getComment1() {
		return comment1;
	}
	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}
	
	@Column(length=80)
	public String getComment2() {
		return comment2;
	}
	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}
	
	@Column(length=5)
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	
	@Column(length=5)
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	
	@Column(length=45)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(length=20)
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	@Column(length=15)
	public String getDivMult() {
		return divMult;
	}
	public void setDivMult(String divMult) {
		this.divMult = divMult;
	}
	
	@Column
	public Date getDoNotSent() {
		return doNotSent;
	}
	public void setDoNotSent(Date doNotSent) {
		this.doNotSent = doNotSent;
	}
	
	@Column(length=20)
	public BigDecimal getDriverAmtount() {
		return driverAmtount;
	}
	public void setDriverAmtount(BigDecimal driverAmtount) {
		this.driverAmtount = driverAmtount;
	}
	
	@Column(length=20)
	public BigDecimal getEntitledAmount() {
		return entitledAmount;
	}
	public void setEntitledAmount(BigDecimal entitledAmount) {
		this.entitledAmount = entitledAmount;
	}
	
	@Column
	public Date getEntitledDate() {
		return entitledDate;
	}
	public void setEntitledDate(Date entitledDate) {
		this.entitledDate = entitledDate;
	}
	
	@Column(length=20)
	public BigDecimal getEstimate() {
		return estimate;
	}
	public void setEstimate(BigDecimal estimate) {
		this.estimate = estimate;
	}
	
	@Column
	public Date getEstimateDate() {
		return estimateDate;
	}
	public void setEstimateDate(Date estimateDate) {
		this.estimateDate = estimateDate;
	}
	
	@Column(length=20)
	public BigDecimal getEstimateRevisedAmount() {
		return estimateRevisedAmount;
	}
	public void setEstimateRevisedAmount(BigDecimal estimateRevisedAmount) {
		this.estimateRevisedAmount = estimateRevisedAmount;
	}
	
	@Column
	public Date getEstimateRevisedDate() {
		return estimateRevisedDate;
	}
	public void setEstimateRevisedDate(Date estimateRevisedDate) {
		this.estimateRevisedDate = estimateRevisedDate;
	}
	
	@Column
	public Date getEstimateSent() {
		return estimateSent;
	}
	public void setEstimateSent(Date estimateSent) {
		this.estimateSent = estimateSent;
	}
	
	@Column(length=20)
	public String getExpGl() {
		return expGl;
	}
	public void setExpGl(String expGl) {
		this.expGl = expGl;
	}
	
	@Column(length=20)
	public BigDecimal getIntTicket() {
		return intTicket;
	}
	public void setIntTicket(BigDecimal intTicket) {
		this.intTicket = intTicket;
	}
	
	@Column(length=15)
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	
	@Column
	public Date getPaid() {
		return paid;
	}
	public void setPaid(Date paid) {
		this.paid = paid;
	}
	
	@Column(length=1)
	public Boolean getPassthrough() {
		return passthrough;
	}
	public void setPassthrough(Boolean passthrough) {
		this.passthrough = passthrough;
	}
	
	@Column(length=20)
	public BigDecimal getPer() {
		return per;
	}
	public void setPer(BigDecimal per) {
		this.per = per;
	}
	
	@Column(length=15)
	public String getPerItem() {
		return perItem;
	}
	public void setPerItem(String perItem) {
		this.perItem = perItem;
	}
	
	@Column(length=6)
	public String getPostPer() {
		return postPer;
	}
	public void setPostPer(String postPer) {
		this.postPer = postPer;
	}
	
	@Column(length=20)
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	@Column(length=20)
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	@Column(length=10)
	public int getRIdNumber() {
		return rIdNumber;
	}
	public void setRIdNumber(int idNumber) {
		rIdNumber = idNumber;
	}
	
	@Column
	public Date getSendEntitled() {
		return sendEntitled;
	}
	public void setSendEntitled(Date sendEntitled) {
		this.sendEntitled = sendEntitled;
	}
	
	@Column
	public Date getSentWb() {
		return sentWb;
	}
	public void setSentWb(Date sentWb) {
		this.sentWb = sentWb;
	}
	
	@Column
	public Date getStorageBegin() {
		return storageBegin;
	}
	public void setStorageBegin(Date storageBegin) {
		this.storageBegin = storageBegin;
	}
	
	@Column(length=4)
	public int getStorageBill() {
		return storageBill;
	}
	public void setStorageBill(int storageBill) {
		this.storageBill = storageBill;
	}
	
	@Column
	public Date getStorageEnd() {
		return storageEnd;
	}
	public void setStorageEnd(Date storageEnd) {
		this.storageEnd = storageEnd;
	}
	
	@Column(length=10)
	public int getTicket() {
		return ticket;
	}
	public void setTicket(int ticket) {
		this.ticket = ticket;
	}
	
	@Column(length=2)
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	
		
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=20)
	public BigDecimal getExpRevisedAmount() {
		return expRevisedAmount;
	}
	public void setExpRevisedAmount(BigDecimal expRevisedAmount) {
		this.expRevisedAmount = expRevisedAmount;
	}
	@Column
	public Date getExpRevisedDate() {
		return expRevisedDate;
	}
	public void setExpRevisedDate(Date expRevisedDate) {
		this.expRevisedDate = expRevisedDate;
	}


	@Column(length=20)
	public String getGl() {
		return gl;
	}
	public void setGl(String gl) {
		this.gl = gl;
	}
	
	
	@Column(length=20)
	public String getInvoiceCheckNumber() {
		return invoiceCheckNumber;
	}
	public void setInvoiceCheckNumber(String invoiceCheckNumber) {
		this.invoiceCheckNumber = invoiceCheckNumber;
	}
	
	@Column
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	@Column
	public Date getExpInvoiceDate() {
		return expInvoiceDate;
	}
	public void setExpInvoiceDate(Date expInvoiceDate) {
		this.expInvoiceDate = expInvoiceDate;
	}
	
	@Column(length=15)
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	@Column(length=15)
	public String getExpInvoiceNumber() {
		return expInvoiceNumber;
	}
	public void setExpInvoiceNumber(String expInvoiceNumber) {
		this.expInvoiceNumber = expInvoiceNumber;
	}
	
	@Column(length=6)
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	
	
	@Column(length=20)
	public String getSentFile() {
		return sentFile;
	}
	public void setSentFile(String sentFile) {
		this.sentFile = sentFile;
	}
	
	@Column(length=9, insertable=false, updatable=false)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	//New Field
	
	
	
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	
	@Column(length=8)
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	
	@Column(length=8)
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	
	@Column(length=9)
	public Long getLine() {
		return line;
	}
	public void setLine(Long line) {
		this.line = line;
	}
	
	@Column(length=12)
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	
	@Column(length=9)
	public Long getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(Long ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	@Column(length=7)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(length=7)
	public String getRecPay() {
		return recPay;
	}
	public void setRecPay(String recPay) {
		this.recPay = recPay;
	}
	
	@Column
	public Date getExpPostDate() {
		return expPostDate;
	}
	public void setExpPostDate(Date expPostDate) {
		this.expPostDate = expPostDate;
	}
	
	@Column(length=20)
	public String getExpSentFile() {
		return expSentFile;
	}
	public void setExpSentFile(String expSentFile) {
		this.expSentFile = expSentFile;
	}
	
	@Column
	public Date getExpEstimateDate() {
		return expEstimateDate;
	}
	public void setExpEstimateDate(Date expEstimateDate) {
		this.expEstimateDate = expEstimateDate;
	}
	
	@Column
	public Date getSentAccount() {
		return sentAccount;
	}
	public void setSentAccount(Date sentAccount) {
		this.sentAccount = sentAccount;
	}

}