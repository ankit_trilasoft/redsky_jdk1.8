<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="OriginCrewInstructions" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="b25432a5-c8a4-4a73-9a94-be2faa7fdefd">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT "OriginCrewInstructions-SSCW-pdf.jrxml",
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     date_format(trackingstatus.`packA`,'%d-%b-%Y') AS trackingstatus_packA,
     date_format(trackingstatus.`loadA`,'%d-%b-%Y') AS trackingstatus_loadA,
     date_format(trackingstatus.`deliveryA`,'%d-%b-%Y') AS trackingstatus_deliveryA,
     billing.`billtoname` AS billing_billtoname,
     app_user.phone_number AS app_user_phone_number,
     CONCAT(if(trim(app_user.first_name)="" || app_user.first_name is null,"",trim(app_user.first_name)),if(trim(app_user.last_name)="" || app_user.last_name is null,"",if((trim(app_user.first_name)="" || app_user.first_name is null),trim(app_user.last_name),concat(" ",trim(app_user.last_name))))) AS serviceorder_coordinator,
     notes.note AS notes_note
FROM serviceorder serviceorder
	INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` and trackingstatus.corpID=$P{Corporate ID}
	INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` and billing.corpID=$P{Corporate ID}
	LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN notes notes  on  notes.noteskeyid = serviceorder.id and notes.corpid = $P{Corporate ID} and notetype='Service Order' and notesubtype='Critical to Quality Note'
WHERE serviceorder.corpID = $P{Corporate ID}
AND serviceorder.`shipNumber` = $P{Service Order Number}]]>
	</queryString>
	<field name="OriginCrewInstructions-SSCW-pdf.jrxml" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="trackingstatus_packA" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.lang.String"/>
	<field name="trackingstatus_deliveryA" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="app_user_phone_number" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="notes_note" class="java.lang.String"/>
	<group name="Instruction">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="315" splitType="Stretch">
				<rectangle>
					<reportElement key="rectangle-1" x="5" y="204" width="530" height="50" forecolor="#0019A9" uuid="40867a1e-1b6b-470a-99a1-d7d846c9bf03"/>
				</rectangle>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-8" x="5" y="1" width="530" height="34" uuid="beeb95b6-759a-4775-84e3-f53bb9c2b673"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" size="18" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["General instructions:"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-1" x="5" y="35" width="530" height="187" uuid="dba08cbe-e21a-40ee-8f1a-0c04ba53110e"/>
					<text><![CDATA[• If you are going to be late (outside start time window) you must call the client and notify the Counselor listed below.
• Minimum of two crew members must be assigned to every shipment.
• Full packing is required.
• On Air shipments, goods must be packed into boxes before loading inside air containers.
• Detailed packing list of all items in the shipment. No PBO’s (Packed by Owner) are allowed.
• All electrical items must be on packing list with make, model and serial number.
• Client must sign all pages of packing list.
• Final walk-through with transferee to verify nothing left behind unintentionally.
• If client requests any additional services not previously authorized, call the Counselor listed below from residence
  before carrying out any additional services; otherwise you may not get paid for this service.
• Bring a portable scale to residence anytime the client’s allowance is 1,000 lbs or under.
• For full container shipments container must be loaded floor to ceiling and a bulkhead created with appropriate
  materials.]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-2" x="5" y="204" width="530" height="58" forecolor="#0019A9" uuid="0c7b8db0-8d97-4575-80b7-7ee115322ac7"/>
					<box topPadding="5" leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[• A 7-Point inspection must be done prior to loading of container. Please see instructions and form attached.
• Container must be loaded floor to ceiling and a bulkhead built with proper materials.
• A C-TPAT/ISO Compliant High-Security SEAL (Cable lock, Cable seal or Bolt Seal) must be used.]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-9" x="5" y="256" width="530" height="59" uuid="01ec06b1-d34b-44cd-b716-44b8fa93cec6"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["IF AT ANY TIME YOU HAVE ANY QUESTIONS ABOUT WHAT GOES WHERE, OR HOW SOMETHING SHOULD BE HANDLED, PLEASE CALL "+($F{serviceorder_coordinator}==null ? "" : $F{serviceorder_coordinator})+" at "+($F{app_user_phone_number}==null ? "" : $F{app_user_phone_number})]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="225" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" x="255" y="7" width="274" height="34" uuid="f7d5bcb6-1c8c-4ca3-a92e-b5ed234424d8"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="18" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["Origin Crew Instructions"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-2" x="255" y="42" width="274" height="34" uuid="190543c0-b946-495d-adcb-50834037d274"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["Reg # "+($F{serviceorder_shipNumber}==null ? "" : $F{serviceorder_shipNumber})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-3" x="0" y="53" width="250" height="34" uuid="49159860-6118-434e-8abd-12e8e0017369"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA["815 South Main Street, 4th Floor Jacksonville, Florida 32207"+"\n"+
"Phone 800-874-3833 Fax 904-858-1201"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-4" x="5" y="127" width="530" height="56" uuid="724a6ed5-7134-4a7a-93b7-684c846e2e48"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[("Suddath Reg # "+($F{serviceorder_shipNumber}==null ? "" : $F{serviceorder_shipNumber}))+"\n"+
("Transferee Name: "+($F{serviceorder_firstName}==null ? "" : ($F{serviceorder_firstName}+" "))+($F{serviceorder_lastName}==null ? "" : $F{serviceorder_lastName}))+"\n"+
("Account Name: "+($F{billing_billtoname}==null ? "" : $F{billing_billtoname}))+"\n"+
("Pack Date: "+($F{trackingstatus_packA}==null ? "" : ($F{trackingstatus_packA}+", ")))+("Load Date: "+($F{trackingstatus_loadA}==null ? "" : $F{trackingstatus_loadA}))]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-5" x="5" y="191" width="274" height="34" forecolor="#800000" uuid="968f55dc-d55d-4b18-b9d5-d57bd23ce59f"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="18" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Critical Items"]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" onErrorType="Blank">
				<reportElement key="image-1" x="1" y="1" width="230" height="50" uuid="21717240-7c45-458c-b716-4e98c3630674"/>
				<imageExpression><![CDATA["../../images/SuddathLogo.jpg"]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-10" stretchType="RelativeToTallestObject" x="5" y="0" width="530" height="18" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" forecolor="#800000" uuid="895e2fe7-563a-40d3-abaf-e374d4a2409a"/>
				<box topPadding="1" leftPadding="3" bottomPadding="1">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{notes_note}==null ? "" : ("* "+ $F{notes_note}.trim()))]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
