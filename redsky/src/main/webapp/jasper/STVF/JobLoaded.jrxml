<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="JobLoaded" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="a6a84bd9-953f-4bc5-aff2-a3f5699bd157">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"JobLoaded-R-PDF-STVF.jrxml",
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.status AS serviceorder_Status,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=""),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS Shipper_Name,
	billing.billtocode AS billing_Billtocode,
	billing.billtoname AS billing_Billtoname,
	serviceorder.job AS serviceorder_Job,
	serviceorder.companydivision AS serviceorder_companydivision,
	DATE_FORMAT(trackingstatus.loadA,'%d-%b-%y') AS trackingstatus_LoadA,
	DATE_FORMAT(trackingstatus.deliveryA,'%d-%b-%y') AS trackingstatus_DeliveryA,
	sum(if(accountline.actualRevenue is null || accountline.actualRevenue='',0.0,accountline.actualRevenue)) AS accountline_ActualRevenue,
	sum(if(accountline.revisionRevenueAmount is null || accountline.revisionRevenueAmount='',0.0,accountline.revisionRevenueAmount)) AS accountline_RevisionRevenueAmount,
	sum(if(accountline.estimateRevenueAmount is null || accountline.estimateRevenueAmount='',0.0,accountline.estimateRevenueAmount)) AS accountline_EstimateRevenueAmount
From serviceorder serviceorder
	inner join billing billing on serviceorder.id=billing.id AND billing.corpId =$P{Corporate ID}
	inner join trackingstatus trackingstatus on serviceorder.id=trackingstatus.id AND trackingstatus.corpId =$P{Corporate ID}
	inner join accountline accountline on serviceorder.id=accountline.serviceorderId AND accountline.corpId =$P{Corporate ID}
where serviceorder.corpId =$P{Corporate ID}
	AND (trackingstatus.loadA <= sysdate() or trackingstatus.deliveryA <= sysdate())
	AND accountline.status = true
	AND serviceorder.status not in ('CNCL','CANCEL','DWNLD','DWN','HOLD','CLSD','2CLS')
	AND serviceorder.job not in ('DWN')
	AND billing.billComplete is null
group by accountline.shipNumber
having sum(accountline.actualRevenue) <> 0 and
sum(accountline.actualRevenue) < if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))
	AND ((if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))) -
  (sum(accountline.actualRevenue)))/(if(sum(accountline.revisionRevenueAmount) >0, sum(accountline.revisionRevenueAmount),sum(accountline.estimateRevenueAmount))) > .15
order by serviceorder.companydivision, accountline.shipNumber,trackingstatus.loadA desc]]>
	</queryString>
	<field name="JobLoaded-R-PDF-STVF.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_Status" class="java.lang.String"/>
	<field name="Shipper_Name" class="java.lang.String"/>
	<field name="billing_Billtocode" class="java.lang.String"/>
	<field name="billing_Billtoname" class="java.lang.String"/>
	<field name="serviceorder_Job" class="java.lang.String"/>
	<field name="serviceorder_companydivision" class="java.lang.String"/>
	<field name="trackingstatus_LoadA" class="java.lang.String"/>
	<field name="trackingstatus_DeliveryA" class="java.lang.String"/>
	<field name="accountline_ActualRevenue" class="java.math.BigDecimal"/>
	<field name="accountline_RevisionRevenueAmount" class="java.math.BigDecimal"/>
	<field name="accountline_EstimateRevenueAmount" class="java.math.BigDecimal"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companydivision}]]></groupExpression>
		<groupHeader>
			<band height="16" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="115" height="16" uuid="f31fb233-eab5-416c-a201-005ec5e70f82"/>
					<textElement>
						<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companydivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-14" x="0" y="0" width="102" height="16" uuid="7e5dd7a9-8d55-4826-ae70-f163acdd51e1"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="29" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="3" width="802" height="26" uuid="25ca4e7c-9706-4db1-93b2-8b24cd215378"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job Loaded Or Delivered And More Than 15% Of Revised/Estimated Amt Not Invoiced]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="32" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="0" y="8" width="79" height="24" uuid="7fabd8b8-b8d6-42e4-959c-6a1d512e61a2"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SO #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="79" y="8" width="57" height="24" uuid="1ab8a5b6-e389-451e-969f-4fd04929f153"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="136" y="8" width="93" height="24" uuid="42556fa0-ddf2-4f7f-ae34-a24358c6989f"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="229" y="8" width="70" height="24" uuid="8b129d95-3240-4b9b-9cdb-72af2d205aaa"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="299" y="8" width="116" height="24" uuid="300b4ba6-025b-4e99-9be3-c8a8308f0968"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="415" y="8" width="39" height="24" uuid="3516c583-8946-45b1-9357-7cb25cca8d87"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="454" y="8" width="60" height="24" uuid="1d918cdb-5e32-49f8-a144-1410261b396f"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Load Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="514" y="8" width="60" height="24" uuid="44f1d227-62b0-49ce-8326-5afb763d3fc5"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="574" y="8" width="76" height="24" uuid="7868c2eb-3f6e-496c-ba26-b0111898a24e"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Inv Amt.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="650" y="8" width="76" height="24" uuid="2d83a29c-d618-4db0-b8d5-d6e7a54d6b5e"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Rev Amt.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="726" y="8" width="76" height="24" uuid="cd4824ca-482e-41b1-b370-7f8ff4c6033a"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Est Amt.]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="0" width="79" height="18" uuid="746588b3-a73f-4b56-bc13-518773c9e8ce"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="79" y="0" width="57" height="18" uuid="41372dea-4640-4373-9f56-3b38e450b91d"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_Status}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="136" y="0" width="93" height="18" uuid="d8de2020-73de-4957-848c-a468de0b7e1e"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper_Name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="229" y="0" width="70" height="18" uuid="27d6b8da-889a-4714-a29f-637c1d91bfae"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_Billtocode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="299" y="0" width="116" height="18" uuid="077c3790-3a73-41a3-a7db-1187cbf08599"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_Billtoname}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="415" y="0" width="39" height="18" uuid="bca633f1-94c3-4bef-8403-13dc6e1cf2af"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_Job}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="454" y="0" width="60" height="18" uuid="ea3c83ee-fdee-4588-816a-cc75af159f88"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_LoadA}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="514" y="0" width="60" height="18" uuid="77e50927-674c-4118-9381-465657e1f12b"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_DeliveryA}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="574" y="0" width="76" height="18" uuid="6d4583d2-4134-42ac-9e72-38f1c30d52b2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_ActualRevenue}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="650" y="0" width="76" height="18" uuid="9de13dff-1dd3-4976-81a6-990697b7022f"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_RevisionRevenueAmount}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-4" x="726" y="0" width="76" height="18" uuid="7ab08765-1d26-412f-86e3-5174743b64d0"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountline_EstimateRevenueAmount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="18" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="668" y="4" width="71" height="14" uuid="a69def6a-dc8c-41d1-ac3b-ee38b9251e01"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Helvetica" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="739" y="4" width="44" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="c4c7a2b1-dfcc-471c-b518-8685885b4b91"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-13" x="149" y="4" width="485" height="14" uuid="04f2be2f-8129-40d2-b0bd-9ac4dba89f6b"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[JobLoaded.jrxml]]></text>
			</staticText>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-5" x="1" y="4" width="105" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="61f1636b-44fd-4dc8-a763-3757d51b2a7a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Helvetica" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="1" width="782" height="1" uuid="93282f93-0b07-4528-b23a-552d22a40c8c"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
