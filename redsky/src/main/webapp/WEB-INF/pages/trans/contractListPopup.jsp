 <%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
</head> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style>

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" onclick="getContract();" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set> 

<s:form id="contractListForm" method="post" >
<s:hidden name="contractType" value="${contractType}"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>

<th><fmt:message key="contract.contract"/></th>

<th><fmt:message key="contract.description"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="cContract" size="25" required="true" cssClass="text medium" onfocus="onFormLoad();"/></td>
			    <td><s:textfield name="cDesc" size="25" required="true" cssClass="text medium"/>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<div id="layer1" style="width:100%">
<div id="otabs">
	<ul>
		<li><a class="current"><span>Price</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div> 

<div id="newmnav">   
</div><div class="spnblk" style="width:800px">&nbsp;</div>
 
<s:set name="itemsJEquip.contract" value="${param.contract}"/>
<s:hidden name="itemsJEquip.contract" id="itemsJEquip.contract" value="${param.contract}"/>

<s:set name="objContract" value="contractList"/>   
<display:table name="objContract" class="table" requestURI="" id="contractList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%"
				decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >
	<c:if test="${empty param.popup}">  
		<display:column property="contract" sortable="true" title="Contract"
		href="addMatEquip.html" paramId="contract" paramProperty="contract" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" title="Contract"/>   
    </c:if> 
    <display:column property="description" sortable="true" title="Description"/>
</display:table>
</div>
</div> 
</s:form>
	
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
	
	<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['contractListForm'].elements['cContract'].value = "";
	document.forms['contractListForm'].elements['cDesc'].value = "";
	}

	function getContract(){		
		url = "searchResourceContract.html?decorator=popup&popup=true";
		document.forms['contractListForm'].action = url;
		document.forms['contractListForm'].submit();
	}
</script> 	
	
