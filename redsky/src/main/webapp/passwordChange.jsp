<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="passwordChange.title"/></title>
    <meta name="heading" content="<fmt:message key='passwordChange.heading'/>"/>
    <meta name="menu" content="passwordChange"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-1col.css'/>" />
  
    <script language="javascript" type="text/javascript">
       function clear_fileds(){
    var i;
			for(i=0;i<=5;i++)
 	{
       	document.forms['passwordChangeForm'].elements['user.password'].value = "";
       	document.forms['passwordChangeForm'].elements['user.confirmPassword'].value = "";
    	document.forms['passwordChangeForm'].elements['j_passwordHint'].value = "";
    	document.forms['passwordChangeForm'].elements['user.email'].value = "";
    	document.forms['passwordChangeForm'].elements['j_passwordHintQues'].value = "";
    }
    }
    </script>
</head>
<body id="passwordChange"/>

<form method="post" id="passwordChangeForm" name="passwordChangeForm" action="<c:url value='/j_security_check'/>"
    onsubmit="saveUsername(this);return validateForm(this)">
<s:hidden name="perRec" value="<%=request.getParameter("perRec") %>" />
<c:if test="${cookieLogin == 'true'}">
	    <s:hidden key="user.password"/>
	    <s:hidden key="user.confirmPassword"/>
</c:if>
<s:if test="user.version == null">
    	<input type="hidden" name="encryptPass" value="true" />
</s:if>
<input type="hidden" name="from" value="${param.from}"/>

	 	  	<tr><td align="left" class="listwhitetext">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="24%">&nbsp;</td>
    <td width="52%"><table width="400" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a2c9f8">
      <tr>
        <td height="350" valign="top" style="background:url(${pageContext.request.contextPath}/images/loginbg.jpg) top repeat-x #2955b1; "><table width="365" border="0" cellspacing="0" cellpadding="4" style="font:normal 12px arial,verdana; color:#FFFFFF ">
          <tr align="center">
            <td colspan="2"><img src="${pageContext.request.contextPath}/images/rsloginlogo.gif" width="133" height="64"></td>
            </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_username" class="required desc">
            	<fmt:message key="label.username"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><input type="text" class="text medium" name="j_username" id="j_username" tabindex="1" /></td>
          </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_password" class="required desc">
            	<fmt:message key="label.passwordNew"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><s:password key="user.password" showPassword="true" required="true" cssClass="input-text" onchange="passwordChanged(this)"/></td>
          </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_confirmPassword" class="required desc">
            	<fmt:message key="label.confirmPassword"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><s:password key="user.confirmPassword"  required="true" showPassword="true" cssClass="input-text" onchange="passwordChanged(this)" /></td>
          </tr>
          <tr>
            <td width="400px" height="30" align="right"><label for="j_passwordHintQues" class="required desc">
            	<fmt:message key="label.passwordHintQues"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td><input type="passwordHintQues" class="text medium" name="j_passwordHintQues" id="j_passwordHintQues" tabindex="2" size="40"/></td>
          </tr>
          <tr>
            <td width="172" height="30" align="right"><label for="j_passwordHint" class="required desc">
            	<fmt:message key="label.passwordHint"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td><input type="passwordHint" class="text medium" name="j_passwordHint" id="j_passwordHint" tabindex="3" /></td>
          </tr>
          <tr>
			 <td align="right"><fmt:message key="user.email"/><font color="red" size="2">*</font></td>
			 <td align="left"><s:textfield name="user.email" cssClass="input-text" size="40"/></td>
		 </tr>
		 <tr>
	       <td>&nbsp;</td>
          <td><input type="button" name="submit" onclick="location.href='<c:url value="/mainMenu.html"/>'"  value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
          </tr>
          </table></td>
      </tr>
    </table></td>
    <td width="24%">&nbsp;</td>
  </tr>
</table>

</td>
</tr>
</form>
