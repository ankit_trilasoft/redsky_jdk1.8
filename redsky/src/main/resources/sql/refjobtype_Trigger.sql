

delimiter $$

CREATE trigger redsky.trigger_add_history_refjobtype
BEFORE UPDATE on redsky.refjobtype
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'refjobtype.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;

   IF (OLD.job <> NEW.job ) THEN
      select CONCAT(fieldNameList,'refjobtype.job~') into fieldNameList;
      select CONCAT(oldValueList,OLD.job,'~') into oldValueList;
      select CONCAT(newValueList,NEW.job,'~') into newValueList;
  END IF;

   IF (OLD.personBilling <> NEW.personBilling ) THEN
      select CONCAT(fieldNameList,'refjobtype.personBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personBilling,'~') into newValueList;
  END IF;

   IF (OLD.personPricing <> NEW.personPricing ) THEN
      select CONCAT(fieldNameList,'refjobtype.personPricing~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personPricing,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personPricing,'~') into newValueList;
  END IF;

   IF (OLD.personPayable <> NEW.personPayable ) THEN
      select CONCAT(fieldNameList,'refjobtype.personPayable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personPayable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personPayable,'~') into newValueList;
  END IF;

   IF (OLD.coordinator <> NEW.coordinator ) THEN
      select CONCAT(fieldNameList,'refjobtype.coordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
  END IF;

   IF (OLD.estimator <> NEW.estimator ) THEN
      select CONCAT(fieldNameList,'refjobtype.estimator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimator,'~') into newValueList;
  END IF;

   IF (OLD.personAuditor <> NEW.personAuditor ) THEN
      select CONCAT(fieldNameList,'refjobtype.personAuditor~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personAuditor,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personAuditor,'~') into newValueList;
  END IF;

   IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'refjobtype.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;

   IF (OLD.country <> NEW.country ) THEN
      select CONCAT(fieldNameList,'refjobtype.country~') into fieldNameList;
      select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
      select CONCAT(newValueList,NEW.country,'~') into newValueList;
  END IF;

   IF (OLD.countryCode <> NEW.countryCode ) THEN
      select CONCAT(fieldNameList,'refjobtype.countryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.countryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.countryCode,'~') into newValueList;
  END IF;

   IF (OLD.surveyTool <> NEW.surveyTool ) THEN
      select CONCAT(fieldNameList,'refjobtype.surveyTool~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyTool,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyTool,'~') into newValueList;
  END IF;

   IF (OLD.personClaims <> NEW.personClaims ) THEN
      select CONCAT(fieldNameList,'refjobtype.personClaims~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personClaims,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personClaims,'~') into newValueList;
  END IF;
       
   IF (OLD.accAccountPortal <> NEW.accAccountPortal ) THEN
       select CONCAT(fieldNameList,'partnerpublic.accAccountPortal~') into fieldNameList;
       IF(OLD.accAccountPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accAccountPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accAccountPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accAccountPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
    
   IF (OLD.internalBillingPerson <> NEW.internalBillingPerson ) THEN
      select CONCAT(fieldNameList,'refjobtype.internalBillingPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.internalBillingPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.internalBillingPerson,'~') into newValueList;
  END IF;

      IF (OLD.opsPerson <> NEW.opsPerson ) THEN
      select CONCAT(fieldNameList,'refjobtype.opsPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.opsPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.opsPerson,'~') into newValueList;
  END IF; 

        IF (OLD.personForwarder <> NEW.personForwarder ) THEN
      select CONCAT(fieldNameList,'refjobtype.personForwarder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personForwarder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personForwarder,'~') into newValueList;
  END IF; 

CALL add_tblHistory (OLD.id,"refjobtype", fieldNameList, oldValueList, newValueList, NEW.updatedBy, OLD.corpID, now());

END $$


delimiter;

