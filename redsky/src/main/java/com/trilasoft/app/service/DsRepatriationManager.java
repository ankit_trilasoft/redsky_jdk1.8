package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.DsRepatriation;

public interface DsRepatriationManager extends GenericManager<DsRepatriation, Long>{
	List getListById(Long id);
}
