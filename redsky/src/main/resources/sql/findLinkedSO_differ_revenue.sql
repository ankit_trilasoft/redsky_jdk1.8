DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findLinkedSO_differ_revenue` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `findLinkedSO_differ_revenue`(pid varchar(500)) RETURNS varchar(100) CHARSET utf8
BEGIN
DECLARE revenue varchar(100) DEFAULT 0;


select group_concat(actualrevenue) into revenue from accountline a where a.id in (pid);


RETURN revenue;

END $$

DELIMITER ;