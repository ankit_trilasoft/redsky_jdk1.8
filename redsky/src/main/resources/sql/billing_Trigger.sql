delimiter $$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `redsky`.`trigger_add_history_billing`
AFTER UPDATE ON `redsky`.`billing`
FOR EACH ROW
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  
  
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  
  IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
      select CONCAT(fieldNameList,'billing.sequenceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
  END IF;
  IF (OLD.billCompleteA <> NEW.billCompleteA ) THEN
      select CONCAT(fieldNameList,'billing.billCompleteA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billCompleteA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billCompleteA,'~') into newValueList;
  END IF;
   IF ((date_format(OLD.billComplete,'%Y-%m-%d') <> date_format(NEW.billComplete,'%Y-%m-%d')) or (date_format(OLD.billComplete,'%Y-%m-%d') is null and date_format(NEW.billComplete,'%Y-%m-%d') is not null)
    or (date_format(OLD.billComplete,'%Y-%m-%d') is not null and date_format(NEW.billComplete,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.billComplete~') into fieldNameList;
      IF(OLD.billComplete is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.billComplete is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.billComplete is not null) THEN
          select CONCAT(oldValueList,OLD.billComplete,'~') into oldValueList;
       END IF;
        IF(NEW.billComplete is not null) THEN
          select CONCAT(newValueList,NEW.billComplete,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.billingId <> NEW.billingId ) THEN
      select CONCAT(fieldNameList,'billing.billingId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingId,'~') into newValueList;
  END IF;
  IF (OLD.billToAuthority <> NEW.billToAuthority ) THEN
      select CONCAT(fieldNameList,'billing.billToAuthority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToAuthority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToAuthority,'~') into newValueList;
  END IF;
  IF (OLD.billTo2Reference <> NEW.billTo2Reference ) THEN
      select CONCAT(fieldNameList,'billing.billTo2Reference~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo2Reference,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo2Reference,'~') into newValueList;
  END IF;
  IF (OLD.billTo2Point <> NEW.billTo2Point ) THEN
      select CONCAT(fieldNameList,'billing.billTo2Point~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo2Point,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo2Point,'~') into newValueList;
  END IF;
  IF (OLD.billTo2Name <> NEW.billTo2Name ) THEN
      select CONCAT(fieldNameList,'billing.billTo2Name~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo2Name,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo2Name,'~') into newValueList;
  END IF;
  IF (OLD.billTo2Code <> NEW.billTo2Code ) THEN
      select CONCAT(fieldNameList,'billing.billTo2Code~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo2Code,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo2Code,'~') into newValueList;
  END IF;
  IF (OLD.billTo2Authority <> NEW.billTo2Authority ) THEN
      select CONCAT(fieldNameList,'billing.billTo2Authority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo2Authority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo2Authority,'~') into newValueList;
  END IF;
  IF (OLD.billTo1Point <> NEW.billTo1Point ) THEN
      select CONCAT(fieldNameList,'billing.billTo1Point~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo1Point,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo1Point,'~') into newValueList;
  END IF;
   IF ((date_format(OLD.billThrough,'%Y-%m-%d') <> date_format(NEW.billThrough,'%Y-%m-%d')) or (date_format(OLD.billThrough,'%Y-%m-%d') is null and date_format(NEW.billThrough,'%Y-%m-%d') is not null)
    or (date_format(OLD.billThrough,'%Y-%m-%d') is not null and date_format(NEW.billThrough,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.billThrough~') into fieldNameList;
      IF(OLD.billThrough is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.billThrough is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.billThrough is not null) THEN
          select CONCAT(oldValueList,OLD.billThrough,'~') into oldValueList;
       END IF;
        IF(NEW.billThrough is not null) THEN
          select CONCAT(newValueList,NEW.billThrough,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.billingInstruction <> NEW.billingInstruction ) THEN
      select CONCAT(fieldNameList,'billing.billingInstruction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstruction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstruction,'~') into newValueList;
  END IF;
  IF (OLD.authorityType <> NEW.authorityType ) THEN
      select CONCAT(fieldNameList,'billing.authorityType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.authorityType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.authorityType,'~') into newValueList;
  END IF;
  
  IF (OLD.creditTerms <> NEW.creditTerms ) THEN
      select CONCAT(fieldNameList,'billing.creditTerms~') into fieldNameList;
      select CONCAT(oldValueList,OLD.creditTerms,'~') into oldValueList;
      select CONCAT(newValueList,NEW.creditTerms,'~') into newValueList;
  END IF;

  IF (OLD.shipNumber <> NEW.shipNumber ) THEN
      select CONCAT(fieldNameList,'billing.shipNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
  END IF;
    IF (OLD.billToCode <> NEW.billToCode ) THEN
      select CONCAT(fieldNameList,'billing.billToCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
  END IF;

  IF (OLD.billToName <> NEW.billToName ) THEN
      select CONCAT(fieldNameList,'billing.billToName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
  END IF;
  IF (OLD.billToReference <> NEW.billToReference ) THEN
      select CONCAT(fieldNameList,'billing.billToReference~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToReference,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToReference,'~') into newValueList;
  END IF;
  IF (OLD.charge <> NEW.charge ) THEN
      select CONCAT(fieldNameList,'billing.charge~') into fieldNameList;
      select CONCAT(oldValueList,OLD.charge,'~') into oldValueList;
      select CONCAT(newValueList,NEW.charge,'~') into newValueList;
  END IF;
  IF (OLD.certnum <> NEW.certnum ) THEN
      select CONCAT(fieldNameList,'billing.certnum~') into fieldNameList;
      select CONCAT(oldValueList,OLD.certnum,'~') into oldValueList;
      select CONCAT(newValueList,NEW.certnum,'~') into newValueList;
  END IF;
  IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'billing.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;
    IF (OLD.cycle <> NEW.cycle ) THEN
      select CONCAT(fieldNameList,'billing.cycle~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cycle,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cycle,'~') into newValueList;
  END IF;
  IF ((OLD.deposit <> NEW.deposit) or (OLD.deposit is null and NEW.deposit is not null)
    or (OLD.deposit is not null and NEW.deposit is null)) THEN
    select CONCAT(fieldNameList,'billing.deposit~') into fieldNameList;
    IF(OLD.deposit is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.deposit is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.deposit is not null) then
      select CONCAT(oldValueList,OLD.deposit,'~') into oldValueList;
    END IF;
    IF(NEW.deposit is not null) then
      select CONCAT(newValueList,NEW.deposit,'~') into newValueList;
    END IF; 
  END IF;
  IF ((OLD.discount <> NEW.discount) or (OLD.discount is null and NEW.discount is not null)
    or (OLD.discount is not null and NEW.discount is null)) THEN
    select CONCAT(fieldNameList,'billing.discount~') into fieldNameList;
    IF(OLD.discount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.discount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.discount is not null) then
      select CONCAT(oldValueList,OLD.discount,'~') into oldValueList;
    END IF;
    IF(NEW.discount is not null) then
      select CONCAT(newValueList,NEW.discount,'~') into newValueList;
    END IF; 
  END IF;
  IF ((OLD.insurancePerMonth <> NEW.insurancePerMonth) or (OLD.insurancePerMonth is null and NEW.insurancePerMonth is not null)
    or (OLD.insurancePerMonth is not null and NEW.insurancePerMonth is null)) THEN
    select CONCAT(fieldNameList,'billing.insurancePerMonth~') into fieldNameList;
    IF(OLD.insurancePerMonth is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.insurancePerMonth is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.insurancePerMonth is not null) then
      select CONCAT(oldValueList,OLD.insurancePerMonth,'~') into oldValueList;
    END IF;
    IF(NEW.insurancePerMonth is not null) then
      select CONCAT(newValueList,NEW.insurancePerMonth,'~') into newValueList;
    END IF;
  END IF;
  IF (OLD.insuranceOptionCode <> NEW.insuranceOptionCode ) THEN
      select CONCAT(fieldNameList,'billing.insuranceOptionCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceOptionCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceOptionCode,'~') into newValueList;
  END IF;
  IF (OLD.insuranceOption <> NEW.insuranceOption ) THEN
      select CONCAT(fieldNameList,'billing.insuranceOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceOption,'~') into newValueList;
  END IF;
  IF (OLD.insurancePolicy <> NEW.insurancePolicy ) THEN
      select CONCAT(fieldNameList,'billing.insurancePolicy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insurancePolicy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insurancePolicy,'~') into newValueList;
  END IF;
  IF ((OLD.insuranceValueActual <> NEW.insuranceValueActual) or (OLD.insuranceValueActual is null and NEW.insuranceValueActual is not null)
    or (OLD.insuranceValueActual is not null and NEW.insuranceValueActual is null)) THEN
    select CONCAT(fieldNameList,'billing.insuranceValueActual~') into fieldNameList;
    IF(OLD.insuranceValueActual is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.insuranceValueActual is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.insuranceValueActual is not null) then
      select CONCAT(oldValueList,OLD.insuranceValueActual,'~') into oldValueList;
    END IF;
    IF(NEW.insuranceValueActual is not null) then
      select CONCAT(newValueList,NEW.insuranceValueActual,'~') into newValueList;
    END IF;
  END IF;
  IF ((OLD.insuranceValueEntitle <> NEW.insuranceValueEntitle) or (OLD.insuranceValueEntitle is null and NEW.insuranceValueEntitle is not null) 
    or (OLD.insuranceValueEntitle is not null and NEW.insuranceValueEntitle is null)) THEN
    select CONCAT(fieldNameList,'billing.insuranceValueEntitle~') into fieldNameList;
    IF(OLD.insuranceValueEntitle is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.insuranceValueEntitle is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.insuranceValueEntitle is not null) then
      select CONCAT(oldValueList,OLD.insuranceValueEntitle,'~') into oldValueList;
    END IF;
    IF(NEW.insuranceValueEntitle is not null) then
      select CONCAT(newValueList,NEW.insuranceValueEntitle,'~') into newValueList;
    END IF; 
  END IF;
  IF ((OLD.insuranceRate <> NEW.insuranceRate) or (OLD.insuranceRate is null and NEW.insuranceRate is not null) 
    or (OLD.insuranceRate is not null and NEW.insuranceRate is null)) THEN
    select CONCAT(fieldNameList,'billing.insuranceRate~') into fieldNameList;
    IF(OLD.insuranceRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.insuranceRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.insuranceRate is not null) then
      select CONCAT(oldValueList,OLD.insuranceRate,'~') into oldValueList;
    END IF;
    IF(NEW.insuranceRate is not null) then
      select CONCAT(newValueList,NEW.insuranceRate,'~') into newValueList;
    END IF; 
  END IF;

  IF (OLD.insuranceThrough <> NEW.insuranceThrough ) THEN
      select CONCAT(fieldNameList,'billing.insuranceThrough~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceThrough,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceThrough,'~') into newValueList;
  END IF;
  
  IF (OLD.insuranceHas <> NEW.insuranceHas ) THEN
      select CONCAT(fieldNameList,'billing.insuranceHas~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceHas,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceHas,'~') into newValueList;
  END IF;
  IF ((OLD.onHand <> NEW.onHand) or (OLD.onHand is null and NEW.onHand is not null) 
    or (OLD.onHand is not null and NEW.onHand is null)) THEN
    select CONCAT(fieldNameList,'billing.onHand~') into fieldNameList;
    IF(OLD.onHand is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.onHand is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.onHand is not null) then
      select CONCAT(oldValueList,OLD.onHand,'~') into oldValueList;
    END IF;
    IF(NEW.onHand is not null) then
      select CONCAT(newValueList,NEW.onHand,'~') into newValueList;
    END IF; 
  END IF;

  IF ((OLD.otherDiscount <> NEW.otherDiscount) or (OLD.otherDiscount is null and NEW.otherDiscount is not null) 
    or (OLD.otherDiscount is not null and NEW.otherDiscount is null)) THEN
    select CONCAT(fieldNameList,'billing.otherDiscount~') into fieldNameList;
    IF(OLD.otherDiscount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.otherDiscount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.otherDiscount is not null) then
      select CONCAT(oldValueList,OLD.otherDiscount,'~') into oldValueList;
    END IF;
    IF(NEW.otherDiscount is not null) then
      select CONCAT(newValueList,NEW.otherDiscount,'~') into newValueList;
    END IF; 
  END IF;
  IF ((OLD.packDiscount <> NEW.packDiscount) or (OLD.packDiscount is null and NEW.packDiscount is not null) 
    or (OLD.packDiscount is not null and NEW.packDiscount is null)) THEN
    select CONCAT(fieldNameList,'billing.packDiscount~') into fieldNameList;
    IF(OLD.packDiscount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.packDiscount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.packDiscount is not null) then
      select CONCAT(oldValueList,OLD.packDiscount,'~') into oldValueList;
    END IF;
    IF(NEW.packDiscount is not null) then
      select CONCAT(newValueList,NEW.packDiscount,'~') into newValueList;
    END IF; 
  END IF;
  IF ((OLD.postGrate <> NEW.postGrate) or (OLD.postGrate is null and NEW.postGrate is not null)
    or (OLD.postGrate is not null and NEW.postGrate is null)) THEN
    select CONCAT(fieldNameList,'billing.postGrate~') into fieldNameList;
    IF(OLD.postGrate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.postGrate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.postGrate is not null) then
      select CONCAT(oldValueList,OLD.postGrate,'~') into oldValueList;
    END IF;
    IF(NEW.postGrate is not null) then
      select CONCAT(newValueList,NEW.postGrate,'~') into newValueList;
    END IF; 
  END IF;

  IF ((OLD.sitDiscount <> NEW.sitDiscount) or (OLD.sitDiscount is null and NEW.sitDiscount is not null) 
    or (OLD.sitDiscount is not null and NEW.sitDiscount is null)) THEN
    select CONCAT(fieldNameList,'billing.sitDiscount~') into fieldNameList;
    IF(OLD.sitDiscount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.sitDiscount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.sitDiscount is not null) then
      select CONCAT(oldValueList,OLD.sitDiscount,'~') into oldValueList;
    END IF;
    IF(NEW.sitDiscount is not null) then
      select CONCAT(newValueList,NEW.sitDiscount,'~') into newValueList;
    END IF;
  END IF;

  IF ((OLD.storagePerMonth <> NEW.storagePerMonth) or (OLD.storagePerMonth is null and NEW.storagePerMonth is not null) 
    or (OLD.storagePerMonth is not null and NEW.storagePerMonth is null)) THEN
    select CONCAT(fieldNameList,'billing.storagePerMonth~') into fieldNameList;
    IF(OLD.storagePerMonth is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.storagePerMonth is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.storagePerMonth is not null) then
      select CONCAT(oldValueList,OLD.storagePerMonth,'~') into oldValueList;
    END IF;
    IF(NEW.storagePerMonth is not null) then
      select CONCAT(newValueList,NEW.storagePerMonth,'~') into newValueList;
    END IF;
  END IF;

  IF ((OLD.transportationDiscount <> NEW.transportationDiscount) or (OLD.transportationDiscount is null and NEW.transportationDiscount is not null)
    or (OLD.transportationDiscount is not null and NEW.transportationDiscount is null)) THEN
    select CONCAT(fieldNameList,'billing.transportationDiscount~') into fieldNameList;
    IF(OLD.transportationDiscount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.transportationDiscount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.transportationDiscount is not null) then
      select CONCAT(oldValueList,OLD.transportationDiscount,'~') into oldValueList;
    END IF;
    IF(NEW.transportationDiscount is not null) then
      select CONCAT(newValueList,NEW.transportationDiscount,'~') into newValueList;
    END IF;
  END IF;
  IF ((date_format(OLD.taExpires,'%Y-%m-%d') <> date_format(NEW.taExpires,'%Y-%m-%d')) or (date_format(OLD.taExpires,'%Y-%m-%d') is null and date_format(NEW.taExpires,'%Y-%m-%d') is not null)
    or (date_format(OLD.taExpires,'%Y-%m-%d') is not null and date_format(NEW.taExpires,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.taExpires~') into fieldNameList;
      IF(OLD.taExpires is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.taExpires is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
       END IF;
      IF(OLD.taExpires is not null) THEN
          select CONCAT(oldValueList,OLD.taExpires,'~') into oldValueList;
       END IF;
        IF(NEW.taExpires is not null) THEN
          select CONCAT(newValueList,NEW.taExpires,'~') into newValueList;
        END IF;
  END IF;
   IF ((date_format(OLD.readyForInvoiceCheck,'%Y-%m-%d') <> date_format(NEW.readyForInvoiceCheck,'%Y-%m-%d')) or (date_format(OLD.readyForInvoiceCheck,'%Y-%m-%d') is null and date_format(NEW.readyForInvoiceCheck,'%Y-%m-%d') is not null)
    or (date_format(OLD.readyForInvoiceCheck,'%Y-%m-%d') is not null and date_format(NEW.readyForInvoiceCheck,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.readyForInvoiceCheck~') into fieldNameList;
      IF(OLD.readyForInvoiceCheck is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.readyForInvoiceCheck is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
       END IF;
      IF(OLD.readyForInvoiceCheck is not null) THEN
          select CONCAT(oldValueList,OLD.readyForInvoiceCheck,'~') into oldValueList;
       END IF;
        IF(NEW.readyForInvoiceCheck is not null) THEN
          select CONCAT(newValueList,NEW.readyForInvoiceCheck,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.invoiceRemarks1 <> NEW.invoiceRemarks1 ) THEN
      select CONCAT(fieldNameList,'billing.invoiceRemarks1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoiceRemarks1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoiceRemarks1,'~') into newValueList;
  END IF;
  IF (OLD.invoiceRemarks2 <> NEW.invoiceRemarks2 ) THEN
      select CONCAT(fieldNameList,'billing.invoiceRemarks2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoiceRemarks2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoiceRemarks2,'~') into newValueList;
  END IF;
  IF (OLD.invoiceRemarks3 <> NEW.invoiceRemarks3 ) THEN
      select CONCAT(fieldNameList,'billing.invoiceRemarks3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoiceRemarks3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoiceRemarks3,'~') into newValueList;
  END IF;
  IF (OLD.invoiceRemarks4 <> NEW.invoiceRemarks4 ) THEN
      select CONCAT(fieldNameList,'billing.invoiceRemarks4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoiceRemarks4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoiceRemarks4,'~') into newValueList;
  END IF;
  IF (OLD.matchShip <> NEW.matchShip ) THEN
      select CONCAT(fieldNameList,'billing.matchShip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.matchShip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.matchShip,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.noMoreWork,'%Y-%m-%d') <> date_format(NEW.noMoreWork,'%Y-%m-%d')) or (date_format(OLD.noMoreWork,'%Y-%m-%d') is null and date_format(NEW.noMoreWork,'%Y-%m-%d') is not null)
    or (date_format(OLD.noMoreWork,'%Y-%m-%d') is not null and date_format(NEW.noMoreWork,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.noMoreWork~') into fieldNameList;
      IF(OLD.noMoreWork is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.noMoreWork is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.noMoreWork is not null) THEN
          select CONCAT(oldValueList,OLD.noMoreWork,'~') into oldValueList;
       END IF;
        IF(NEW.noMoreWork is not null) THEN
          select CONCAT(newValueList,NEW.noMoreWork,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.OrderForService <> NEW.OrderForService ) THEN
      select CONCAT(fieldNameList,'billing.OrderForService~') into fieldNameList;
      select CONCAT(oldValueList,OLD.OrderForService,'~') into oldValueList;
      select CONCAT(newValueList,NEW.OrderForService,'~') into newValueList;
  END IF;
  IF (OLD.securityAccount <> NEW.securityAccount ) THEN
      select CONCAT(fieldNameList,'billing.securityAccount~') into fieldNameList;
      select CONCAT(oldValueList,OLD.securityAccount,'~') into oldValueList;
      select CONCAT(newValueList,NEW.securityAccount,'~') into newValueList;
  END IF;
  IF (OLD.serial <> NEW.serial ) THEN
      select CONCAT(fieldNameList,'billing.serial~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serial,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serial,'~') into newValueList;
  END IF;
  IF ((OLD.sharedDiscount <> NEW.sharedDiscount) or (OLD.sharedDiscount is null and NEW.sharedDiscount is not null) 
    or (OLD.sharedDiscount is not null and NEW.sharedDiscount is null)) THEN
    select CONCAT(fieldNameList,'billing.sharedDiscount~') into fieldNameList;
    IF(OLD.sharedDiscount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.sharedDiscount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.sharedDiscount is not null) then
      select CONCAT(oldValueList,OLD.sharedDiscount,'~') into oldValueList;
    END IF;
    IF(NEW.sharedDiscount is not null) then
      select CONCAT(newValueList,NEW.sharedDiscount,'~') into newValueList;
    END IF;
  END IF;
    IF (OLD.storageMeasurement <> NEW.storageMeasurement ) THEN
      select CONCAT(fieldNameList,'billing.storageMeasurement~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageMeasurement,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageMeasurement,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.storageOut,'%Y-%m-%d') <> date_format(NEW.storageOut,'%Y-%m-%d')) or (date_format(OLD.storageOut,'%Y-%m-%d') is null and date_format(NEW.storageOut,'%Y-%m-%d') is not null)
    or (date_format(OLD.storageOut,'%Y-%m-%d') is not null and date_format(NEW.storageOut,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.storageOut~') into fieldNameList;
      IF(OLD.storageOut is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.storageOut is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.storageOut is not null) THEN
          select CONCAT(oldValueList,OLD.storageOut,'~') into oldValueList;
       END IF;
        IF(NEW.storageOut is not null) THEN
          select CONCAT(newValueList,NEW.storageOut,'~') into newValueList;
        END IF;
  END IF;

 IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'billing.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;

  IF (OLD.contractSystem <> NEW.contractSystem ) THEN
      select CONCAT(fieldNameList,'billing.contractSystem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractSystem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractSystem,'~') into newValueList;
  END IF;
  
  IF (OLD.warehouse <> NEW.warehouse ) THEN
      select CONCAT(fieldNameList,'billing.warehouse~') into fieldNameList;
      select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
      select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
  END IF;
 
  IF (OLD.scheduleCode <> NEW.scheduleCode ) THEN
      select CONCAT(fieldNameList,'billing.scheduleCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.scheduleCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.scheduleCode,'~') into newValueList;
  END IF;
  IF (OLD.specialInstruction <> NEW.specialInstruction ) THEN
      select CONCAT(fieldNameList,'billing.specialInstruction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.specialInstruction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.specialInstruction,'~') into newValueList;
  END IF;

    IF (OLD.billingInstructionCode <> NEW.billingInstructionCode ) THEN
      select CONCAT(fieldNameList,'billing.billingInstructionCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstructionCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstructionCode,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.systemDate,'%Y-%m-%d') <> date_format(NEW.systemDate,'%Y-%m-%d')) or (date_format(OLD.systemDate,'%Y-%m-%d') is null and date_format(NEW.systemDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.systemDate,'%Y-%m-%d') is not null and date_format(NEW.systemDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.systemDate~') into fieldNameList;
      IF(OLD.systemDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.systemDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.systemDate is not null) THEN
          select CONCAT(oldValueList,OLD.systemDate,'~') into oldValueList;
       END IF;
        IF(NEW.systemDate is not null) THEN
          select CONCAT(newValueList,NEW.systemDate,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.approvedBy <> NEW.approvedBy ) THEN
      select CONCAT(fieldNameList,'billing.approvedBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.approvedBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.approvedBy,'~') into newValueList;
  END IF;
  IF (OLD.noCharge <> NEW.noCharge ) THEN
      select CONCAT(fieldNameList,'billing.noCharge~') into fieldNameList;
      IF(OLD.noCharge = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.noCharge = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.noCharge = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.noCharge = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;
  IF (OLD.isAutomatedStorageBillingProcessing <> NEW.isAutomatedStorageBillingProcessing ) THEN
      select CONCAT(fieldNameList,'billing.isAutomatedStorageBillingProcessing~') into fieldNameList;
      IF(OLD.isAutomatedStorageBillingProcessing = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.isAutomatedStorageBillingProcessing = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.isAutomatedStorageBillingProcessing = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.isAutomatedStorageBillingProcessing = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;

 IF ((date_format(OLD.dateApproved,'%Y-%m-%d') <> date_format(NEW.dateApproved,'%Y-%m-%d')) or (date_format(OLD.dateApproved,'%Y-%m-%d') is null and date_format(NEW.dateApproved,'%Y-%m-%d') is not null)
    or (date_format(OLD.dateApproved,'%Y-%m-%d') is not null and date_format(NEW.dateApproved,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.dateApproved~') into fieldNameList;
      IF(OLD.dateApproved is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.dateApproved is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.dateApproved is not null) THEN
          select CONCAT(oldValueList,OLD.dateApproved,'~') into oldValueList;
       END IF;
        IF(NEW.dateApproved is not null) THEN
          select CONCAT(newValueList,NEW.dateApproved,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.insuranceOptionCodeWithDesc <> NEW.insuranceOptionCodeWithDesc ) THEN
      select CONCAT(fieldNameList,'billing.insuranceOptionCodeWithDesc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceOptionCodeWithDesc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceOptionCodeWithDesc,'~') into newValueList;
  END IF;
  IF (OLD.billingInstructionCodeWithDesc <> NEW.billingInstructionCodeWithDesc ) THEN
      select CONCAT(fieldNameList,'billing.billingInstructionCodeWithDesc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstructionCodeWithDesc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstructionCodeWithDesc,'~') into newValueList;
  END IF;
  IF (OLD.imfJob <> NEW.imfJob ) THEN
      select CONCAT(fieldNameList,'billing.imfJob~') into fieldNameList;
      select CONCAT(oldValueList,OLD.imfJob,'~') into oldValueList;
      select CONCAT(newValueList,NEW.imfJob,'~') into newValueList;
  END IF;
  IF (OLD.isCreditCard <> NEW.isCreditCard ) THEN
      select CONCAT(fieldNameList,'billing.isCreditCard~') into fieldNameList;
      select CONCAT(oldValueList,OLD.isCreditCard,'~') into oldValueList;
      select CONCAT(newValueList,NEW.isCreditCard,'~') into newValueList;
  END IF;
  IF (OLD.storageEmailType <> NEW.storageEmailType ) THEN
      select CONCAT(fieldNameList,'billing.storageEmailType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmailType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmailType,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.gstVATrefund,'%Y-%m-%d') <> date_format(NEW.gstVATrefund,'%Y-%m-%d')) or (date_format(OLD.gstVATrefund,'%Y-%m-%d') is null and date_format(NEW.gstVATrefund,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstVATrefund,'%Y-%m-%d') is not null and date_format(NEW.gstVATrefund,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstVATrefund~') into fieldNameList;
      IF(OLD.gstVATrefund is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstVATrefund is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstVATrefund is not null) THEN
          select CONCAT(oldValueList,OLD.gstVATrefund,'~') into oldValueList;
       END IF;
        IF(NEW.gstVATrefund is not null) THEN
          select CONCAT(newValueList,NEW.gstVATrefund,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.privatePartyBillingCode <> NEW.privatePartyBillingCode ) THEN
      select CONCAT(fieldNameList,'billing.privatePartyBillingCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privatePartyBillingCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privatePartyBillingCode,'~') into newValueList;
  END IF;
  IF (OLD.privatePartyBillingName <> NEW.privatePartyBillingName ) THEN
      select CONCAT(fieldNameList,'billing.privatePartyBillingName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privatePartyBillingName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privatePartyBillingName,'~') into newValueList;
  END IF;
  IF (OLD.signedDisclaimer <> NEW.signedDisclaimer ) THEN
      select CONCAT(fieldNameList,'billing.signedDisclaimer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.signedDisclaimer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.signedDisclaimer,'~') into newValueList;
  END IF;
  IF (OLD.vendorCode <> NEW.vendorCode ) THEN
      select CONCAT(fieldNameList,'billing.vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
  END IF;
  IF (OLD.vendorName <> NEW.vendorName ) THEN
      select CONCAT(fieldNameList,'billing.vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorName,'~') into newValueList;
  END IF;
  IF (OLD.totalInsrVal <> NEW.totalInsrVal ) THEN
      select CONCAT(fieldNameList,'billing.totalInsrVal~') into fieldNameList;
      select CONCAT(oldValueList,OLD.totalInsrVal,'~') into oldValueList;
      select CONCAT(newValueList,NEW.totalInsrVal,'~') into newValueList;
  END IF;
  IF (OLD.currency <> NEW.currency ) THEN
      select CONCAT(fieldNameList,'billing.currency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currency,'~') into newValueList;
  END IF;
  IF (OLD.storageBillingParty <> NEW.storageBillingParty ) THEN
      select CONCAT(fieldNameList,'billing.storageBillingParty~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageBillingParty,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageBillingParty,'~') into newValueList;
  END IF;
  
    IF (OLD.insuranceCharge <> NEW.insuranceCharge ) THEN
      select CONCAT(fieldNameList,'billing.insuranceCharge~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceCharge,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceCharge,'~') into newValueList;
  END IF;
  IF (OLD.personBilling <> NEW.personBilling ) THEN
      select CONCAT(fieldNameList,'billing.personBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personBilling,'~') into newValueList;
  END IF;

  IF ((date_format(OLD.issueDate,'%Y-%m-%d') <> date_format(NEW.issueDate,'%Y-%m-%d')) or (date_format(OLD.issueDate,'%Y-%m-%d') is null and date_format(NEW.issueDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.issueDate,'%Y-%m-%d') is not null and date_format(NEW.issueDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.issueDate~') into fieldNameList;
      IF(OLD.issueDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.issueDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.issueDate is not null) THEN
          select CONCAT(oldValueList,OLD.issueDate,'~') into oldValueList;
       END IF;
        IF(NEW.issueDate is not null) THEN
          select CONCAT(newValueList,NEW.issueDate,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.revenueRecognition,'%Y-%m-%d') <> date_format(NEW.revenueRecognition,'%Y-%m-%d')) or (date_format(OLD.revenueRecognition,'%Y-%m-%d') is null and date_format(NEW.revenueRecognition,'%Y-%m-%d') is not null)
    or (date_format(OLD.revenueRecognition,'%Y-%m-%d') is not null and date_format(NEW.revenueRecognition,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.revenueRecognition~') into fieldNameList;
      IF(OLD.revenueRecognition is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.revenueRecognition is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.revenueRecognition is not null) THEN
          select CONCAT(oldValueList,OLD.revenueRecognition,'~') into oldValueList;
       END IF;
        IF(NEW.revenueRecognition is not null) THEN
          select CONCAT(newValueList,NEW.revenueRecognition,'~') into newValueList;
        END IF;
  END IF;
IF (OLD.actgCode <> NEW.actgCode ) THEN
      select CONCAT(fieldNameList,'billing.actgCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.actgCode,'~') into newValueList;
  END IF;
  IF (OLD.payMethod <> NEW.payMethod ) THEN
      select CONCAT(fieldNameList,'billing.payMethod~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payMethod,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payMethod,'~') into newValueList;
  END IF;
   IF (OLD.personPricing <> NEW.personPricing ) THEN
      select CONCAT(fieldNameList,'billing.personPricing~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personPricing,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personPricing,'~') into newValueList;
  END IF;

   IF (OLD.personPayable <> NEW.personPayable ) THEN
      select CONCAT(fieldNameList,'billing.personPayable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personPayable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personPayable,'~') into newValueList;
  END IF;


   IF (OLD.personReceivable <> NEW.personReceivable ) THEN
      select CONCAT(fieldNameList,'billing.personReceivable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personReceivable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personReceivable,'~') into newValueList;
  END IF;

   IF (OLD.invoiceByEmail <> NEW.invoiceByEmail ) THEN
      select CONCAT(fieldNameList,'billing.invoiceByEmail~') into fieldNameList;
      IF(OLD.invoiceByEmail = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.invoiceByEmail = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.invoiceByEmail = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.invoiceByEmail = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;

IF ((date_format(OLD.auditComplete,'%Y-%m-%d') <> date_format(NEW.auditComplete,'%Y-%m-%d')) or (date_format(OLD.auditComplete,'%Y-%m-%d') is null and date_format(NEW.auditComplete,'%Y-%m-%d') is not null)
    or (date_format(OLD.auditComplete,'%Y-%m-%d') is not null and date_format(NEW.auditComplete,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.auditComplete~') into fieldNameList;
      IF(OLD.auditComplete is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.auditComplete is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.auditComplete is not null) THEN
          select CONCAT(oldValueList,OLD.auditComplete,'~') into oldValueList;
       END IF;
        IF(NEW.auditComplete is not null) THEN
          select CONCAT(newValueList,NEW.auditComplete,'~') into newValueList;
        END IF;
  update sodashboard set qc=NEW.auditComplete where serviceorderid=NEW.id;

  END IF;


 IF (OLD.billingIdOut <> NEW.billingIdOut ) THEN
     select CONCAT(fieldNameList,'billing.billingIdOut~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingIdOut,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingIdOut,'~') into newValueList;
  END IF;

 IF (OLD.authorizationNo2 <> NEW.authorizationNo2 ) THEN
      select CONCAT(fieldNameList,'billing.authorizationNo2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.authorizationNo2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.authorizationNo2,'~') into newValueList;
  END IF;
IF ((date_format(OLD.authUpdated,'%Y-%m-%d') <> date_format(NEW.authUpdated,'%Y-%m-%d')) or (date_format(OLD.authUpdated,'%Y-%m-%d') is null and date_format(NEW.authUpdated,'%Y-%m-%d') is not null)
    or (date_format(OLD.authUpdated,'%Y-%m-%d') is not null and date_format(NEW.authUpdated,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.authUpdated~') into fieldNameList;
      IF(OLD.authUpdated is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.authUpdated is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.authUpdated is not null) THEN
          select CONCAT(oldValueList,OLD.authUpdated,'~') into oldValueList;
       END IF;
        IF(NEW.authUpdated is not null) THEN
          select CONCAT(newValueList,NEW.authUpdated,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstThirdPartyOrigin,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyOrigin,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyOrigin,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyOrigin,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyOrigin,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyOrigin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyOrigin~') into fieldNameList;
      IF(OLD.gstThirdPartyOrigin is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyOrigin is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyOrigin is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyOrigin,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyOrigin is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyOrigin,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstThirdPartyRequestOrigin,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyRequestOrigin,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyRequestOrigin,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyRequestOrigin,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyRequestOrigin,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyRequestOrigin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyRequestOrigin~') into fieldNameList;
      IF(OLD.gstThirdPartyRequestOrigin is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyRequestOrigin is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyRequestOrigin is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyRequestOrigin,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyRequestOrigin is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyRequestOrigin,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyAuthReceivedOrigin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyAuthReceivedOrigin~') into fieldNameList;
      IF(OLD.gstThirdPartyAuthReceivedOrigin is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyAuthReceivedOrigin is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyAuthReceivedOrigin is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyAuthReceivedOrigin,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyAuthReceivedOrigin is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyAuthReceivedOrigin,'~') into newValueList;
        END IF;
  END IF;
   IF (OLD.gstThirdPartyAuthNoOrigin <> NEW.gstThirdPartyAuthNoOrigin ) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyAuthNoOrigin~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstThirdPartyAuthNoOrigin,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstThirdPartyAuthNoOrigin,'~') into newValueList;
  END IF;
IF ((date_format(OLD.gstShuttleOriginService,'%Y-%m-%d') <> date_format(NEW.gstShuttleOriginService,'%Y-%m-%d')) or (date_format(OLD.gstShuttleOriginService,'%Y-%m-%d') is null and date_format(NEW.gstShuttleOriginService,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleOriginService,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleOriginService,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleOriginService~') into fieldNameList;
      IF(OLD.gstShuttleOriginService is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleOriginService is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleOriginService is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleOriginService,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleOriginService is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleOriginService,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstShuttleOriginRequest,'%Y-%m-%d') <> date_format(NEW.gstShuttleOriginRequest,'%Y-%m-%d')) or (date_format(OLD.gstShuttleOriginRequest,'%Y-%m-%d') is null and date_format(NEW.gstShuttleOriginRequest,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleOriginRequest,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleOriginRequest,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleOriginRequest~') into fieldNameList;
      IF(OLD.gstShuttleOriginRequest is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleOriginRequest is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleOriginRequest is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleOriginRequest,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleOriginRequest is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleOriginRequest,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstShuttleOriginAuthReceived,'%Y-%m-%d') <> date_format(NEW.gstShuttleOriginAuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstShuttleOriginAuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstShuttleOriginAuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleOriginAuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleOriginAuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleOriginAuthReceived~') into fieldNameList;
      IF(OLD.gstShuttleOriginAuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleOriginAuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleOriginAuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleOriginAuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleOriginAuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleOriginAuthReceived,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.gstShuttleOriginAuthNo <> NEW.gstShuttleOriginAuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleOriginAuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstShuttleOriginAuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstShuttleOriginAuthNo,'~') into newValueList;
  END IF;
IF ((date_format(OLD.gstShuttleDestService,'%Y-%m-%d') <> date_format(NEW.gstShuttleDestService,'%Y-%m-%d')) or (date_format(OLD.gstShuttleDestService,'%Y-%m-%d') is null and date_format(NEW.gstShuttleDestService,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleDestService,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleDestService,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleDestService~') into fieldNameList;
      IF(OLD.gstShuttleDestService is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleDestService is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleDestService is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleDestService,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleDestService is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleDestService,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstShuttleDestRequest,'%Y-%m-%d') <> date_format(NEW.gstShuttleDestRequest,'%Y-%m-%d')) or (date_format(OLD.gstShuttleDestRequest,'%Y-%m-%d') is null and date_format(NEW.gstShuttleDestRequest,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleDestRequest,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleDestRequest,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleDestRequest~') into fieldNameList;
      IF(OLD.gstShuttleDestRequest is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleDestRequest is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleDestRequest is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleDestRequest,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleDestRequest is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleDestRequest,'~') into newValueList;
        END IF;
  END IF;

 IF ((date_format(OLD.gstShuttleDestAuthReceived,'%Y-%m-%d') <> date_format(NEW.gstShuttleDestAuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstShuttleDestAuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstShuttleDestAuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstShuttleDestAuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstShuttleDestAuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleDestAuthReceived~') into fieldNameList;
      IF(OLD.gstShuttleDestAuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstShuttleDestAuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstShuttleDestAuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstShuttleDestAuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstShuttleDestAuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstShuttleDestAuthReceived,'~') into newValueList;
        END IF;
  END IF;


 IF (OLD.gstShuttleDestAuthNo <> NEW.gstShuttleDestAuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstShuttleDestAuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstShuttleDestAuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstShuttleDestAuthNo,'~') into newValueList;
  END IF;

 IF ((date_format(OLD.gstAccess1Service,'%Y-%m-%d') <> date_format(NEW.gstAccess1Service,'%Y-%m-%d')) or (date_format(OLD.gstAccess1Service,'%Y-%m-%d') is null and date_format(NEW.gstAccess1Service,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess1Service,'%Y-%m-%d') is not null and date_format(NEW.gstAccess1Service,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess1Service~') into fieldNameList;
      IF(OLD.gstAccess1Service is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess1Service is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess1Service is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess1Service,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess1Service is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess1Service,'~') into newValueList;
        END IF;
  END IF;


 IF ((date_format(OLD.gstAccess1Request,'%Y-%m-%d') <> date_format(NEW.gstAccess1Request,'%Y-%m-%d')) or (date_format(OLD.gstAccess1Request,'%Y-%m-%d') is null and date_format(NEW.gstAccess1Request,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess1Request,'%Y-%m-%d') is not null and date_format(NEW.gstAccess1Request,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess1Request~') into fieldNameList;
      IF(OLD.gstAccess1Request is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess1Request is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess1Request is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess1Request,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess1Request is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess1Request,'~') into newValueList;
        END IF;
  END IF;

 IF ((date_format(OLD.gstAccess1AuthReceived,'%Y-%m-%d') <> date_format(NEW.gstAccess1AuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstAccess1AuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstAccess1AuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess1AuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstAccess1AuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess1AuthReceived~') into fieldNameList;
      IF(OLD.gstAccess1AuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess1AuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess1AuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess1AuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess1AuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess1AuthReceived,'~') into newValueList;
        END IF;
  END IF;

 IF (OLD.gstAccess1AuthNo <> NEW.gstAccess1AuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstAccess1AuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstAccess1AuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstAccess1AuthNo,'~') into newValueList;
  END IF;


  IF ((date_format(OLD.gstAccess2Service,'%Y-%m-%d') <> date_format(NEW.gstAccess2Service,'%Y-%m-%d')) or (date_format(OLD.gstAccess2Service,'%Y-%m-%d') is null and date_format(NEW.gstAccess2Service,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess2Service,'%Y-%m-%d') is not null and date_format(NEW.gstAccess2Service,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess2Service~') into fieldNameList;
      IF(OLD.gstAccess2Service is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess2Service is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess2Service is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess2Service,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess2Service is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess2Service,'~') into newValueList;
        END IF;
  END IF;


   IF ((date_format(OLD.gstAccess2Request,'%Y-%m-%d') <> date_format(NEW.gstAccess2Request,'%Y-%m-%d')) or (date_format(OLD.gstAccess2Request,'%Y-%m-%d') is null and date_format(NEW.gstAccess2Request,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess2Request,'%Y-%m-%d') is not null and date_format(NEW.gstAccess2Request,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess2Request~') into fieldNameList;
      IF(OLD.gstAccess2Request is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess2Request is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess2Request is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess2Request,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess2Request is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess2Request,'~') into newValueList;
        END IF;
  END IF;
   IF ((date_format(OLD.gstAccess2AuthReceived,'%Y-%m-%d') <> date_format(NEW.gstAccess2AuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstAccess2AuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstAccess2AuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess2AuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstAccess2AuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess2AuthReceived~') into fieldNameList;
      IF(OLD.gstAccess2AuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess2AuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess2AuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess2AuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess2AuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess2AuthReceived,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.gstAccess2AuthNo <> NEW.gstAccess2AuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstAccess2AuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstAccess2AuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstAccess2AuthNo,'~') into newValueList;
  END IF;

   IF ((date_format(OLD.gstAccess3Service,'%Y-%m-%d') <> date_format(NEW.gstAccess3Service,'%Y-%m-%d')) or (date_format(OLD.gstAccess3Service,'%Y-%m-%d') is null and date_format(NEW.gstAccess3Service,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess3Service,'%Y-%m-%d') is not null and date_format(NEW.gstAccess3Service,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess3Service~') into fieldNameList;
      IF(OLD.gstAccess3Service is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess3Service is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess3Service is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess3Service,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess3Service is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess3Service,'~') into newValueList;
        END IF;
  END IF;

  IF ((date_format(OLD.gstAccess3Request,'%Y-%m-%d') <> date_format(NEW.gstAccess3Request,'%Y-%m-%d')) or (date_format(OLD.gstAccess3Request,'%Y-%m-%d') is null and date_format(NEW.gstAccess3Request,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess3Request,'%Y-%m-%d') is not null and date_format(NEW.gstAccess3Request,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess3Request~') into fieldNameList;
      IF(OLD.gstAccess3Request is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess3Request is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess3Request is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess3Request,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess3Request is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess3Request,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstAccess3AuthReceived,'%Y-%m-%d') <> date_format(NEW.gstAccess3AuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstAccess3AuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstAccess3AuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstAccess3AuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstAccess3AuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstAccess3AuthReceived~') into fieldNameList;
      IF(OLD.gstAccess3AuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstAccess3AuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstAccess3AuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstAccess3AuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstAccess3AuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstAccess3AuthReceived,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.gstAccess3AuthNo <> NEW.gstAccess3AuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstAccess3AuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstAccess3AuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstAccess3AuthNo,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.gstExtraStopService,'%Y-%m-%d') <> date_format(NEW.gstExtraStopService,'%Y-%m-%d')) or (date_format(OLD.gstExtraStopService,'%Y-%m-%d') is null and date_format(NEW.gstExtraStopService,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstExtraStopService,'%Y-%m-%d') is not null and date_format(NEW.gstExtraStopService,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstExtraStopService~') into fieldNameList;
      IF(OLD.gstExtraStopService is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstExtraStopService is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstExtraStopService is not null) THEN
          select CONCAT(oldValueList,OLD.gstExtraStopService,'~') into oldValueList;
       END IF;
        IF(NEW.gstExtraStopService is not null) THEN
          select CONCAT(newValueList,NEW.gstExtraStopService,'~') into newValueList;
        END IF;
  END IF;
 IF ((date_format(OLD.gstExtraStopRequest,'%Y-%m-%d') <> date_format(NEW.gstExtraStopRequest,'%Y-%m-%d')) or (date_format(OLD.gstExtraStopRequest,'%Y-%m-%d') is null and date_format(NEW.gstExtraStopRequest,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstExtraStopRequest,'%Y-%m-%d') is not null and date_format(NEW.gstExtraStopRequest,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstExtraStopRequest~') into fieldNameList;
      IF(OLD.gstExtraStopRequest is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstExtraStopRequest is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstExtraStopRequest is not null) THEN
          select CONCAT(oldValueList,OLD.gstExtraStopRequest,'~') into oldValueList;
       END IF;
        IF(NEW.gstExtraStopRequest is not null) THEN
          select CONCAT(newValueList,NEW.gstExtraStopRequest,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstExtraStopAuthReceived,'%Y-%m-%d') <> date_format(NEW.gstExtraStopAuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstExtraStopAuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstExtraStopAuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstExtraStopAuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstExtraStopAuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstExtraStopAuthReceived~') into fieldNameList;
      IF(OLD.gstExtraStopAuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstExtraStopAuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstExtraStopAuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstExtraStopAuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstExtraStopAuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstExtraStopAuthReceived,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.gstExtraStopAuthNo <> NEW.gstExtraStopAuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstExtraStopAuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstExtraStopAuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstExtraStopAuthNo,'~') into newValueList;
  END IF;


IF ((date_format(OLD.gstThirdPartyDestination,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyDestination,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyDestination,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyDestination,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyDestination,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyDestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyDestination~') into fieldNameList;
      IF(OLD.gstThirdPartyDestination is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyDestination is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyDestination is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyDestination,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyDestination is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyDestination,'~') into newValueList;
        END IF;
  END IF;


IF ((date_format(OLD.gstThirdPartyRequestDestination,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyRequestDestination,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyRequestDestination,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyRequestDestination,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyRequestDestination,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyRequestDestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyRequestDestination~') into fieldNameList;
      IF(OLD.gstThirdPartyRequestDestination is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyRequestDestination is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyRequestDestination is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyRequestDestination,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyRequestDestination is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyRequestDestination,'~') into newValueList;
        END IF;
  END IF;
IF ((date_format(OLD.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d') <> date_format(NEW.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d')) or (date_format(OLD.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d') is null and date_format(NEW.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d') is not null and date_format(NEW.gstThirdPartyAuthReceivedDestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyAuthReceivedDestination~') into fieldNameList;
      IF(OLD.gstThirdPartyAuthReceivedDestination is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstThirdPartyAuthReceivedDestination is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstThirdPartyAuthReceivedDestination is not null) THEN
          select CONCAT(oldValueList,OLD.gstThirdPartyAuthReceivedDestination,'~') into oldValueList;
       END IF;
        IF(NEW.gstThirdPartyAuthReceivedDestination is not null) THEN
          select CONCAT(newValueList,NEW.gstThirdPartyAuthReceivedDestination,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstPartialDelivery,'%Y-%m-%d') <> date_format(NEW.gstPartialDelivery,'%Y-%m-%d')) or (date_format(OLD.gstPartialDelivery,'%Y-%m-%d') is null and date_format(NEW.gstPartialDelivery,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstPartialDelivery,'%Y-%m-%d') is not null and date_format(NEW.gstPartialDelivery,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstPartialDelivery~') into fieldNameList;
      IF(OLD.gstPartialDelivery is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstPartialDelivery is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstPartialDelivery is not null) THEN
          select CONCAT(oldValueList,OLD.gstPartialDelivery,'~') into oldValueList;
       END IF;
        IF(NEW.gstPartialDelivery is not null) THEN
          select CONCAT(newValueList,NEW.gstPartialDelivery,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstPartialDeliveryRequest,'%Y-%m-%d') <> date_format(NEW.gstPartialDeliveryRequest,'%Y-%m-%d')) or (date_format(OLD.gstPartialDeliveryRequest,'%Y-%m-%d') is null and date_format(NEW.gstPartialDeliveryRequest,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstPartialDeliveryRequest,'%Y-%m-%d') is not null and date_format(NEW.gstPartialDeliveryRequest,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstPartialDeliveryRequest~') into fieldNameList;
      IF(OLD.gstPartialDeliveryRequest is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstPartialDeliveryRequest is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstPartialDeliveryRequest is not null) THEN
          select CONCAT(oldValueList,OLD.gstPartialDeliveryRequest,'~') into oldValueList;
       END IF;
        IF(NEW.gstPartialDeliveryRequest is not null) THEN
          select CONCAT(newValueList,NEW.gstPartialDeliveryRequest,'~') into newValueList;
        END IF;
  END IF;

IF ((date_format(OLD.gstPartialDeliveryAuthReceived,'%Y-%m-%d') <> date_format(NEW.gstPartialDeliveryAuthReceived,'%Y-%m-%d')) or (date_format(OLD.gstPartialDeliveryAuthReceived,'%Y-%m-%d') is null and date_format(NEW.gstPartialDeliveryAuthReceived,'%Y-%m-%d') is not null)
    or (date_format(OLD.gstPartialDeliveryAuthReceived,'%Y-%m-%d') is not null and date_format(NEW.gstPartialDeliveryAuthReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.gstPartialDeliveryAuthReceived~') into fieldNameList;
      IF(OLD.gstPartialDeliveryAuthReceived is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.gstPartialDeliveryAuthReceived is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.gstPartialDeliveryAuthReceived is not null) THEN
          select CONCAT(oldValueList,OLD.gstPartialDeliveryAuthReceived,'~') into oldValueList;
       END IF;
        IF(NEW.gstPartialDeliveryAuthReceived is not null) THEN
          select CONCAT(newValueList,NEW.gstPartialDeliveryAuthReceived,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.gstThirdPartyAuthNoDestination <> NEW.gstThirdPartyAuthNoDestination ) THEN
      select CONCAT(fieldNameList,'billing.gstThirdPartyAuthNoDestination~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstThirdPartyAuthNoDestination,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstThirdPartyAuthNoDestination,'~') into newValueList;
  END IF;

IF (OLD.gstPartialDeliveryAuthNo <> NEW.gstPartialDeliveryAuthNo ) THEN
      select CONCAT(fieldNameList,'billing.gstPartialDeliveryAuthNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gstPartialDeliveryAuthNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gstPartialDeliveryAuthNo,'~') into newValueList;
  END IF;
IF (OLD.attnBilling <> NEW.attnBilling ) THEN
      select CONCAT(fieldNameList,'billing.attnBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.attnBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.attnBilling,'~') into newValueList;
  END IF;
IF (OLD.auditor <> NEW.auditor ) THEN
      select CONCAT(fieldNameList,'billing.auditor~') into fieldNameList;
      select CONCAT(oldValueList,OLD.auditor,'~') into oldValueList;
      select CONCAT(newValueList,NEW.auditor,'~') into newValueList;
  END IF;

IF (OLD.storageVatPercent <> NEW.storageVatPercent ) THEN
      select CONCAT(fieldNameList,'billing.storageVatPercent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageVatPercent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageVatPercent,'~') into newValueList;
  END IF;


IF (OLD.storageVatDescr <> NEW.storageVatDescr ) THEN
      select CONCAT(fieldNameList,'billing.storageVatDescr~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageVatDescr,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageVatDescr,'~') into newValueList;
  END IF;


IF (OLD.insuranceVatPercent <> NEW.insuranceVatPercent ) THEN
      select CONCAT(fieldNameList,'billing.insuranceVatPercent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceVatPercent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceVatPercent,'~') into newValueList;
  END IF;
IF (OLD.insuranceVatDescr <> NEW.insuranceVatDescr ) THEN
      select CONCAT(fieldNameList,'billing.insuranceVatDescr~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceVatDescr,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceVatDescr,'~') into newValueList;
  END IF;

 IF (OLD.historicalContracts <> NEW.historicalContracts ) THEN
      select CONCAT(fieldNameList,'billing.historicalContracts~') into fieldNameList;
      IF(OLD.historicalContracts = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.historicalContracts = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.historicalContracts = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.historicalContracts = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;
IF (OLD.billingFrequency <> NEW.billingFrequency ) THEN
      select CONCAT(fieldNameList,'billing.billingFrequency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingFrequency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingFrequency,'~') into newValueList;
  END IF;
IF ((date_format(OLD.taxableOn,'%Y-%m-%d') <> date_format(NEW.taxableOn,'%Y-%m-%d')) or (date_format(OLD.taxableOn,'%Y-%m-%d') is null and date_format(NEW.taxableOn,'%Y-%m-%d') is not null)
    or (date_format(OLD.taxableOn,'%Y-%m-%d') is not null and date_format(NEW.taxableOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.taxableOn~') into fieldNameList;
      IF(OLD.taxableOn is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.taxableOn is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;
      IF(OLD.taxableOn is not null) THEN
          select CONCAT(oldValueList,OLD.taxableOn,'~') into oldValueList;
       END IF;
        IF(NEW.taxableOn is not null) THEN
          select CONCAT(newValueList,NEW.taxableOn,'~') into newValueList;
        END IF;
  END IF;

 IF (OLD.cod <> NEW.cod ) THEN
      select CONCAT(fieldNameList,'billing.cod~') into fieldNameList;
      IF(OLD.cod = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.cod = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.cod = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.cod = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;


 IF ((OLD.codAmount <> NEW.codAmount) or (OLD.codAmount is null and NEW.codAmount is not null) 
    or (OLD.codAmount is not null and NEW.codAmount is null)) THEN
    select CONCAT(fieldNameList,'billing.codAmount~') into fieldNameList;
    IF(OLD.codAmount is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.codAmount is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.codAmount is not null) then
      select CONCAT(oldValueList,OLD.codAmount,'~') into oldValueList;
    END IF;
    IF(NEW.codAmount is not null) then
      select CONCAT(newValueList,NEW.codAmount,'~') into newValueList;
    END IF; 
  END IF;



IF (OLD.billingEmail <> NEW.billingEmail ) THEN
      select CONCAT(fieldNameList,'billing.billingEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingEmail,'~') into newValueList;
  END IF;


IF (OLD.insuranceValueEntitleCurrency <> NEW.insuranceValueEntitleCurrency ) THEN
      select CONCAT(fieldNameList,'billing.insuranceValueEntitleCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceValueEntitleCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceValueEntitleCurrency,'~') into newValueList;
  END IF;


 IF ((OLD.baseInsuranceValue <> NEW.baseInsuranceValue) or (OLD.baseInsuranceValue is null and NEW.baseInsuranceValue is not null)
    or (OLD.baseInsuranceValue is not null and NEW.baseInsuranceValue is null)) THEN
    select CONCAT(fieldNameList,'billing.baseInsuranceValue~') into fieldNameList;
    IF(OLD.baseInsuranceValue is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.baseInsuranceValue is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.baseInsuranceValue is not null) then
      select CONCAT(oldValueList,OLD.baseInsuranceValue,'~') into oldValueList;
    END IF;
    IF(NEW.baseInsuranceValue is not null) then
      select CONCAT(newValueList,NEW.baseInsuranceValue,'~') into newValueList;
    END IF; 
  END IF;


 IF ((OLD.insuranceBuyRate <> NEW.insuranceBuyRate) or (OLD.insuranceBuyRate is null and NEW.insuranceBuyRate is not null) 
    or (OLD.insuranceBuyRate is not null and NEW.insuranceBuyRate is null)) THEN
    select CONCAT(fieldNameList,'billing.insuranceBuyRate~') into fieldNameList;
    IF(OLD.insuranceBuyRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.insuranceBuyRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.insuranceBuyRate is not null) then
      select CONCAT(oldValueList,OLD.insuranceBuyRate,'~') into oldValueList;
    END IF;
    IF(NEW.insuranceBuyRate is not null) then
      select CONCAT(newValueList,NEW.insuranceBuyRate,'~') into newValueList;
    END IF; 
  END IF;


IF (OLD.baseInsuranceTotal <> NEW.baseInsuranceTotal ) THEN
      select CONCAT(fieldNameList,'billing.baseInsuranceTotal~') into fieldNameList;
      select CONCAT(oldValueList,OLD.baseInsuranceTotal,'~') into oldValueList;
      select CONCAT(newValueList,NEW.baseInsuranceTotal,'~') into newValueList;
  END IF;

IF ((OLD.exchangeRate <> NEW.exchangeRate) or (OLD.exchangeRate is null and NEW.exchangeRate is not null) 
    or (OLD.exchangeRate is not null and NEW.exchangeRate is null)) THEN
    select CONCAT(fieldNameList,'billing.exchangeRate~') into fieldNameList;
    IF(OLD.exchangeRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.exchangeRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.exchangeRate is not null) then
      select CONCAT(oldValueList,OLD.exchangeRate,'~') into oldValueList;
    END IF;
    IF(NEW.exchangeRate is not null) then
      select CONCAT(newValueList,NEW.exchangeRate,'~') into newValueList;
    END IF;
  END IF;
IF ((date_format(OLD.valueDate,'%Y-%m-%d') <> date_format(NEW.valueDate,'%Y-%m-%d')) or (date_format(OLD.valueDate,'%Y-%m-%d') is null and date_format(NEW.valueDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.valueDate,'%Y-%m-%d') is not null and date_format(NEW.valueDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.valueDate~') into fieldNameList;
      IF(OLD.valueDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.valueDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.valueDate is not null) THEN
          select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
       END IF;
        IF(NEW.valueDate is not null) THEN
          select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
        END IF;
  END IF;


IF ((OLD.estMoveCost <> NEW.estMoveCost) or (OLD.estMoveCost is null and NEW.estMoveCost is not null) 
    or (OLD.estMoveCost is not null and NEW.estMoveCost is null)) THEN
    select CONCAT(fieldNameList,'billing.estMoveCost~') into fieldNameList;
    IF(OLD.estMoveCost is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.estMoveCost is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.estMoveCost is not null) then
      select CONCAT(oldValueList,OLD.estMoveCost,'~') into oldValueList;
    END IF;
    IF(NEW.estMoveCost is not null) then
      select CONCAT(newValueList,NEW.estMoveCost,'~') into newValueList;
    END IF;
  END IF;


IF (OLD.deductible <> NEW.deductible ) THEN
      select CONCAT(fieldNameList,'billing.deductible~') into fieldNameList;
      select CONCAT(oldValueList,OLD.deductible,'~') into oldValueList;
      select CONCAT(newValueList,NEW.deductible,'~') into newValueList;
  END IF;
IF (OLD.additionalNo <> NEW.additionalNo ) THEN
      select CONCAT(fieldNameList,'billing.additionalNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.additionalNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.additionalNo,'~') into newValueList;
  END IF;
  IF (OLD.vendorCode1 <> NEW.vendorCode1 ) THEN
      select CONCAT(fieldNameList,'billing.vendorCode1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCode1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCode1,'~') into newValueList;
  END IF;
    IF (OLD.vendorName1 <> NEW.vendorName1 ) THEN
      select CONCAT(fieldNameList,'billing.vendorName1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorName1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorName1,'~') into newValueList;
  END IF;
 
    IF (OLD.insuranceCharge1 <> NEW.insuranceCharge1 ) THEN
      select CONCAT(fieldNameList,'billing.insuranceCharge1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceCharge1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceCharge1,'~') into newValueList;
  END IF;
   IF (OLD.billingCurrency <> NEW.billingCurrency ) THEN
      select CONCAT(fieldNameList,'billing.billingCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCurrency,'~') into newValueList;
  END IF;
    IF (OLD.vendorStoragePerMonth <> NEW.vendorStoragePerMonth ) THEN
      select CONCAT(fieldNameList,'billing.vendorStoragePerMonth~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorStoragePerMonth,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorStoragePerMonth,'~') into newValueList;
  END IF;
  IF (OLD.vendorStorageVatDescr <> NEW.vendorStorageVatDescr ) THEN
      select CONCAT(fieldNameList,'billing.vendorStorageVatDescr~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorStorageVatDescr,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorStorageVatDescr,'~') into newValueList;
  END IF;
IF ((OLD.vendorStorageVatPercent <> NEW.vendorStorageVatPercent) or (OLD.vendorStorageVatPercent is null and NEW.vendorStorageVatPercent is not null)
    or (OLD.vendorStorageVatPercent is not null and NEW.vendorStorageVatPercent is null)) THEN
    select CONCAT(fieldNameList,'billing.vendorStorageVatPercent~') into fieldNameList;
    IF(OLD.vendorStorageVatPercent is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.vendorStorageVatPercent is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;
    IF(OLD.vendorStorageVatPercent is not null) then
      select CONCAT(oldValueList,OLD.vendorStorageVatPercent,'~') into oldValueList;
    END IF;
    IF(NEW.vendorStorageVatPercent is not null) then
      select CONCAT(newValueList,NEW.vendorStorageVatPercent,'~') into newValueList;
    END IF; 
  END IF;

 IF (OLD.vendorBillingCurency <> NEW.vendorBillingCurency ) THEN
      select CONCAT(fieldNameList,'billing.vendorBillingCurency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorBillingCurency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorBillingCurency,'~') into newValueList;
  END IF;

 IF (OLD.readyForInvoicing <> NEW.readyForInvoicing ) THEN
      select CONCAT(fieldNameList,'billing.readyForInvoicing~') into fieldNameList;
      IF(OLD.readyForInvoicing = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.readyForInvoicing = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.readyForInvoicing = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.readyForInvoicing = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;

 IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
      select CONCAT(fieldNameList,'billing.ugwIntId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
  END IF;
   IF (OLD.storageEmail <> NEW.storageEmail ) THEN
      select CONCAT(fieldNameList,'billing.storageEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmail,'~') into newValueList;
  END IF;

   IF (OLD.primaryVatCode <> NEW.primaryVatCode ) THEN
      select CONCAT(fieldNameList,'billing.primaryVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.primaryVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.primaryVatCode,'~') into newValueList;
  END IF;


   IF (OLD.secondaryVatCode <> NEW.secondaryVatCode ) THEN
      select CONCAT(fieldNameList,'billing.secondaryVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.secondaryVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.secondaryVatCode,'~') into newValueList;
  END IF;

    IF (OLD.privatePartyVatCode <> NEW.privatePartyVatCode ) THEN
      select CONCAT(fieldNameList,'billing.privatePartyVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privatePartyVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privatePartyVatCode,'~') into newValueList;
  END IF;


     IF (OLD.bookingAgentVatCode <> NEW.bookingAgentVatCode ) THEN
      select CONCAT(fieldNameList,'billing.bookingAgentVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentVatCode,'~') into newValueList;
  END IF;

    IF (OLD.networkPartnerVatCode <> NEW.networkPartnerVatCode ) THEN
      select CONCAT(fieldNameList,'billing.networkPartnerVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerVatCode,'~') into newValueList;
  END IF;


  IF (OLD.originAgentVatCode <> NEW.originAgentVatCode ) THEN
      select CONCAT(fieldNameList,'billing.originAgentVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentVatCode,'~') into newValueList;
  END IF;


  IF (OLD.destinationAgentVatCode <> NEW.destinationAgentVatCode ) THEN
      select CONCAT(fieldNameList,'billing.destinationAgentVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentVatCode,'~') into newValueList;
  END IF;


   IF (OLD.noInsurance <> NEW.noInsurance ) THEN
      select CONCAT(fieldNameList,'billing.noInsurance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.noInsurance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.noInsurance,'~') into newValueList;
  END IF;

    IF (OLD.claimHandler <> NEW.claimHandler ) THEN
      select CONCAT(fieldNameList,'billing.claimHandler~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimHandler,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimHandler,'~') into newValueList;
  END IF;
  
  IF (OLD.networkBillToCode <> NEW.networkBillToCode ) THEN
       select CONCAT(fieldNameList,'billing.networkBillToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkBillToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkBillToCode,'~') into newValueList;
   END IF;
   
   IF (OLD.networkBillToName <> NEW.networkBillToName) THEN
       select CONCAT(fieldNameList,'billing.networkBillToName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkBillToName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkBillToName,'~') into newValueList;
   END IF;
    
   IF (OLD.contractCurrency <> NEW.contractCurrency ) THEN
      select CONCAT(fieldNameList,'billing.contractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractCurrency,'~') into newValueList;
  END IF;

IF (OLD.payableContractCurrency<> NEW.payableContractCurrency) THEN
      select CONCAT(fieldNameList,'billing.payableContractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableContractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableContractCurrency,'~') into newValueList;
  END IF;

IF (OLD.originSubAgentVatCode<> NEW.originSubAgentVatCode) THEN
      select CONCAT(fieldNameList,'billing.originSubAgentVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originSubAgentVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originSubAgentVatCode,'~') into newValueList;
  END IF;

IF (OLD.destinationSubAgentVatCode<> NEW.destinationSubAgentVatCode) THEN
      select CONCAT(fieldNameList,'billing.destinationSubAgentVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationSubAgentVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationSubAgentVatCode,'~') into newValueList;
  END IF;

IF (OLD.forwarderVatCode<> NEW.forwarderVatCode) THEN
      select CONCAT(fieldNameList,'billing.forwarderVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forwarderVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forwarderVatCode,'~') into newValueList;
  END IF;

IF (OLD.brokerVatCode<> NEW.brokerVatCode) THEN
      select CONCAT(fieldNameList,'billing.brokerVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.brokerVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.brokerVatCode,'~') into newValueList;
  END IF;

IF (OLD.vendorCodeVatCode<> NEW.vendorCodeVatCode) THEN
      select CONCAT(fieldNameList,'billing.vendorCodeVatCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCodeVatCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCodeVatCode,'~') into newValueList;
  END IF;

IF (OLD.vendorCodeVatCode1<> NEW.vendorCodeVatCode1) THEN
      select CONCAT(fieldNameList,'billing.vendorCodeVatCode1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCodeVatCode1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCodeVatCode1,'~') into newValueList;
  END IF;

IF (OLD.personForwarder<> NEW.personForwarder) THEN
      select CONCAT(fieldNameList,'billing.personForwarder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personForwarder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personForwarder,'~') into newValueList;
  END IF;

 IF ((OLD.payableRate<> NEW.payableRate) or (OLD.payableRate is null and NEW.payableRate is not null) 
    or (OLD.payableRate is not null and NEW.payableRate is null)) THEN
    select CONCAT(fieldNameList,'billing.payableRate~') into fieldNameList;
    IF(OLD.payableRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.payableRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.payableRate is not null) then
      select CONCAT(oldValueList,OLD.payableRate,'~') into oldValueList;
    END IF;
    IF(NEW.payableRate is not null) then
      select CONCAT(newValueList,NEW.payableRate,'~') into newValueList;
    END IF; 
  END IF;

IF ((date_format(OLD.insSubmitDate,'%Y-%m-%d') <> date_format(NEW.insSubmitDate,'%Y-%m-%d')) or (date_format(OLD.insSubmitDate,'%Y-%m-%d') is null and date_format(NEW.insSubmitDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.insSubmitDate,'%Y-%m-%d') is not null and date_format(NEW.insSubmitDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.insSubmitDate~') into fieldNameList;
      IF(OLD.insSubmitDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.insSubmitDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.insSubmitDate is not null) THEN
          select CONCAT(oldValueList,OLD.insSubmitDate,'~') into oldValueList;
       END IF;
        IF(NEW.insSubmitDate is not null) THEN
          select CONCAT(newValueList,NEW.insSubmitDate,'~') into newValueList;
        END IF;
  END IF;


IF ((date_format(OLD.wareHousereceiptDate,'%Y-%m-%d') <> date_format(NEW.wareHousereceiptDate,'%Y-%m-%d')) or (date_format(OLD.wareHousereceiptDate,'%Y-%m-%d') is null and date_format(NEW.wareHousereceiptDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.wareHousereceiptDate,'%Y-%m-%d') is not null and date_format(NEW.wareHousereceiptDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.wareHousereceiptDate~') into fieldNameList;
      IF(OLD.wareHousereceiptDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.wareHousereceiptDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.wareHousereceiptDate is not null) THEN
          select CONCAT(oldValueList,OLD.wareHousereceiptDate,'~') into oldValueList;
       END IF;
        IF(NEW.wareHousereceiptDate is not null) THEN
          select CONCAT(newValueList,NEW.wareHousereceiptDate,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.billingInsurancePayableCurrency<> NEW.billingInsurancePayableCurrency) THEN
      select CONCAT(fieldNameList,'billing.billingInsurancePayableCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInsurancePayableCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInsurancePayableCurrency,'~') into newValueList;
  END IF;

IF (OLD.billName<> NEW.billName) THEN
      select CONCAT(fieldNameList,'billing.billName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billName,'~') into newValueList;
  END IF;


IF (OLD.locationAgentCode<> NEW.locationAgentCode) THEN
      select CONCAT(fieldNameList,'billing.locationAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.locationAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.locationAgentCode,'~') into newValueList;
  END IF;

IF (OLD.locationAgentName<> NEW.locationAgentName) THEN
      select CONCAT(fieldNameList,'billing.locationAgentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.locationAgentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.locationAgentName,'~') into newValueList;
  END IF;


IF ((date_format(OLD.recSignedDisclaimerDate,'%Y-%m-%d') <> date_format(NEW.recSignedDisclaimerDate,'%Y-%m-%d')) or (date_format(OLD.recSignedDisclaimerDate,'%Y-%m-%d') is null and date_format(NEW.recSignedDisclaimerDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.recSignedDisclaimerDate,'%Y-%m-%d') is not null and date_format(NEW.recSignedDisclaimerDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.recSignedDisclaimerDate~') into fieldNameList;
      IF(OLD.recSignedDisclaimerDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.recSignedDisclaimerDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.recSignedDisclaimerDate is not null) THEN
          select CONCAT(oldValueList,OLD.recSignedDisclaimerDate,'~') into oldValueList;
       END IF;
        IF(NEW.recSignedDisclaimerDate is not null) THEN
          select CONCAT(newValueList,NEW.recSignedDisclaimerDate,'~') into newValueList;
        END IF;
  END IF;


IF (OLD.fXRateOnActualizationDate<> NEW.fXRateOnActualizationDate) THEN
      select CONCAT(fieldNameList,'billing.fXRateOnActualizationDate~') into fieldNameList;
      IF(OLD.fXRateOnActualizationDate= false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.fXRateOnActualizationDate= true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.fXRateOnActualizationDate= false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.fXRateOnActualizationDate= true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;


IF (OLD.storageVatExclude<> NEW.storageVatExclude) THEN
      select CONCAT(fieldNameList,'billing.storageVatExclude~') into fieldNameList;
      IF(OLD.storageVatExclude= false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.storageVatExclude= true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.storageVatExclude= false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.storageVatExclude= true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;

IF (OLD.insuranceVatExclude<> NEW.insuranceVatExclude) THEN
      select CONCAT(fieldNameList,'billing.insuranceVatExclude~') into fieldNameList;
      IF(OLD.insuranceVatExclude= false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.insuranceVatExclude= true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.insuranceVatExclude= false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.insuranceVatExclude= true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;

IF ((date_format(OLD.contractReceivedDate,'%Y-%m-%d') <> date_format(NEW.contractReceivedDate,'%Y-%m-%d')) or (date_format(OLD.contractReceivedDate,'%Y-%m-%d') is null and date_format(NEW.contractReceivedDate,'%Y-%m-%d') is not null)
    or (date_format(OLD.contractReceivedDate,'%Y-%m-%d') is not null and date_format(NEW.contractReceivedDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.contractReceivedDate~') into fieldNameList;
      IF(OLD.contractReceivedDate is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.contractReceivedDate is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.contractReceivedDate is not null) THEN
          select CONCAT(oldValueList,OLD.contractReceivedDate,'~') into oldValueList;
       END IF;
        IF(NEW.contractReceivedDate is not null) THEN
          select CONCAT(newValueList,NEW.contractReceivedDate,'~') into newValueList;
        END IF;
  END IF;

IF (OLD.internalBillingPerson<> NEW.internalBillingPerson) THEN
      select CONCAT(fieldNameList,'billing.internalBillingPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.internalBillingPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.internalBillingPerson,'~') into newValueList;
  END IF;
  IF (OLD.totalLossCoverageIndicator <> NEW.totalLossCoverageIndicator ) THEN
     select CONCAT(fieldNameList,'billing.totalLossCoverageIndicator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.totalLossCoverageIndicator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.totalLossCoverageIndicator,'~') into newValueList;
  END IF;
  IF (OLD.mechanicalMalfunctionIndicator <> NEW.mechanicalMalfunctionIndicator ) THEN
     select CONCAT(fieldNameList,'billing.mechanicalMalfunctionIndicator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mechanicalMalfunctionIndicator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mechanicalMalfunctionIndicator,'~') into newValueList;
  END IF;
  IF (OLD.pairsOrSetsIndicator <> NEW.pairsOrSetsIndicator ) THEN
     select CONCAT(fieldNameList,'billing.pairsOrSetsIndicator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pairsOrSetsIndicator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pairsOrSetsIndicator,'~') into newValueList;
  END IF;
  IF (OLD.moldOrMildewIndicator <> NEW.moldOrMildewIndicator ) THEN
     select CONCAT(fieldNameList,'billing.moldOrMildewIndicator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.moldOrMildewIndicator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.moldOrMildewIndicator,'~') into newValueList;
  END IF;
  IF (OLD.valuation <> NEW.valuation ) THEN
     select CONCAT(fieldNameList,'billing.valuation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.valuation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.valuation,'~') into newValueList;
  END IF;
  IF (OLD.billingCycle <> NEW.billingCycle ) THEN
     select CONCAT(fieldNameList,'billing.billingCycle~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCycle,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCycle,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.FIDIISOAudit,'%Y-%m-%d') <> date_format(NEW.FIDIISOAudit,'%Y-%m-%d')) or (date_format(OLD.FIDIISOAudit,'%Y-%m-%d') is null and date_format(NEW.FIDIISOAudit,'%Y-%m-%d') is not null)
    or (date_format(OLD.FIDIISOAudit,'%Y-%m-%d') is not null and date_format(NEW.FIDIISOAudit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.FIDIISOAudit~') into fieldNameList;
      IF(OLD.FIDIISOAudit is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.FIDIISOAudit is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.FIDIISOAudit is not null) THEN
          select CONCAT(oldValueList,OLD.FIDIISOAudit,'~') into oldValueList;
       END IF;
        IF(NEW.FIDIISOAudit is not null) THEN
          select CONCAT(newValueList,NEW.FIDIISOAudit,'~') into newValueList;
        END IF;
  END IF;
  IF ((date_format(OLD.financialsComplete,'%Y-%m-%d') <> date_format(NEW.financialsComplete,'%Y-%m-%d')) or (date_format(OLD.financialsComplete,'%Y-%m-%d') is null and date_format(NEW.financialsComplete,'%Y-%m-%d') is not null)
    or (date_format(OLD.financialsComplete,'%Y-%m-%d') is not null and date_format(NEW.financialsComplete,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'billing.financialsComplete~') into fieldNameList;
      IF(OLD.financialsComplete is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.financialsComplete is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.financialsComplete is not null) THEN
          select CONCAT(oldValueList,OLD.financialsComplete,'~') into oldValueList;
       END IF;
        IF(NEW.financialsComplete is not null) THEN
          select CONCAT(newValueList,NEW.financialsComplete,'~') into newValueList;
        END IF;
  END IF;
  IF (OLD.paymentReceived <> NEW.paymentReceived ) THEN
      select CONCAT(fieldNameList,'billing.paymentReceived~') into fieldNameList;
      IF(OLD.paymentReceived = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.paymentReceived = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.paymentReceived = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.paymentReceived = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
  END IF;
  IF ((OLD.autoBilledRate <> NEW.autoBilledRate) or (OLD.autoBilledRate is null and NEW.autoBilledRate is not null)
    or (OLD.autoBilledRate is not null and NEW.autoBilledRate is null)) THEN
    select CONCAT(fieldNameList,'billing.autoBilledRate~') into fieldNameList;
    IF(OLD.autoBilledRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.autoBilledRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.autoBilledRate is not null) then
      select CONCAT(oldValueList,OLD.autoBilledRate,'~') into oldValueList;
    END IF;
    IF(NEW.autoBilledRate is not null) then
      select CONCAT(newValueList,NEW.autoBilledRate,'~') into newValueList;
    END IF;
  END IF;
  IF ((OLD.autoInsuredValue <> NEW.autoInsuredValue) or (OLD.autoInsuredValue is null and NEW.autoInsuredValue is not null)
    or (OLD.autoInsuredValue is not null and NEW.autoInsuredValue is null)) THEN
    select CONCAT(fieldNameList,'billing.autoInsuredValue~') into fieldNameList;
    IF(OLD.autoInsuredValue is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.autoInsuredValue is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.autoInsuredValue is not null) then
      select CONCAT(oldValueList,OLD.autoInsuredValue,'~') into oldValueList;
    END IF;
    IF(NEW.autoInsuredValue is not null) then
      select CONCAT(newValueList,NEW.autoInsuredValue,'~') into newValueList;
    END IF;
  END IF;
  IF ((OLD.autoInternalRate <> NEW.autoInternalRate) or (OLD.autoInternalRate is null and NEW.autoInternalRate is not null)
    or (OLD.autoInternalRate is not null and NEW.autoInternalRate is null)) THEN
    select CONCAT(fieldNameList,'billing.autoInternalRate~') into fieldNameList;
    IF(OLD.autoInternalRate is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.autoInternalRate is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.autoInternalRate is not null) then
      select CONCAT(oldValueList,OLD.autoInternalRate,'~') into oldValueList;
    END IF;
    IF(NEW.autoInternalRate is not null) then
      select CONCAT(newValueList,NEW.autoInternalRate,'~') into newValueList;
    END IF;
  END IF;


  
CALL add_tblHistory (OLD.id,"billing", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
IF (OLD.billComplete<> NEW.billComplete) THEN
call ServiceOrder_UpdateStatus(OLD.shipnumber,NEW.updatedBy);
end if;
END
$$