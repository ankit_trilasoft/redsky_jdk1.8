<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 

<head>   
    <title><fmt:message key="cartonList.title"/></title>   
    <meta name="heading" content="<fmt:message key='cartonList.heading'/>"/>
<style type="text/css">

/* collapse */
	@media screen and (-webkit-min-device-pixel-ratio:0) {
		.table td, .table th, .tableHeaderTable td{ height:5px }
	}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
<script>
<sec-auth:authComponent componentId="module.script.form.agentScript">
 	window.onload = function() { 
		trap();
		<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm2" transIdList='${serviceOrder.shipNumber}'>
		</sec-auth:authScript>
	}
  </sec-auth:authComponent>
  
  <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	window.onload = function() { 
		//trap();
	}
</sec-auth:authComponent>

function trap(){
  	if(document.images){
   		for(i=0;i<document.images.length;i++){
       		if(document.images[i].src.indexOf('nav')>0){
				document.images[i].onclick= right; 
      			document.images[i].src = 'images/navarrow.gif';  
			}
     	}
    }
}
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="mode" value="%{serviceOrder.mode}" />
<c:set var="buttons">  

 <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
 
     <input type="button" class="cssbutton1" onclick="window.open('editCartonAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=550,width=950,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Add" style="width:60px; height:25px;margin-bottom:5px;" />   
         
  
</c:set>
<div id="Layer1" style="width:100%">   

 
 <s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>

<s:set name="cartons" value="cartons" scope="request"/>   

    <display:table name="cartons" class="table" requestURI="" id="cartonList" export="false" defaultsort="1" style="width:100%; margin-left:5px;margin:2px 0 10px;;">   
   
   
	<display:column  titleKey="carton.idNumber" style="width:20px" >
	<sec-auth:authComponent componentId="module.link.carton.id">
	  <a href="javascript:openWindow('editCartonAjax.html?id=${cartonList.id}&decorator=popup&popup=true',900,550)" style="cursor:pointer">
	</sec-auth:authComponent>
	<c:out value="${cartonList.idNumber}" /></a></display:column>
    
	<display:column  titleKey="carton.cartonType"  style="width:50px">
	    <select name ="carton.cartonType" style="width:80px" onchange="populateUnitValuesByCarton('${cartonList.id}');" id="cartonType${cartonList.id}" class="list-menu">
						<option value="<c:out value='' />">
							<c:out value=""></c:out>
							</option> 		      
				      <c:forEach var="chrms" items="${cartonTypeValue}" varStatus="loopStatus">
				           <c:choose>
			                        <c:when test="${chrms == cartonList.cartonType}">
			                        <c:set var="selectedInd" value=" selected"></c:set>
			                        </c:when>
			                        <c:otherwise>
			                        <c:set var="selectedInd" value=""></c:set>
			                        </c:otherwise>
		                            </c:choose>
				      <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
				                    <c:out value="${chrms}"></c:out>
				                    </option>
					</c:forEach>		      
			</select>    
    </display:column>
	
    <display:column  titleKey="carton.cntnrNumber" style="width:50px;text-align: left;">
	    <select name ="carton.cntnrNumber" style="width:120px" onchange="updateCartonDetails('${cartonList.id}','cntnrNumber',this,'Carton');" class="list-menu">
						<option value="<c:out value='' />">
							<c:out value=""></c:out>
							</option> 		      
				      <c:forEach var="chrms" items="${containerNumberListCarton}" varStatus="loopStatus">
				           <c:choose>
			                        <c:when test="${chrms == cartonList.cntnrNumber}">
			                        <c:set var="selectedInd" value=" selected"></c:set>
			                        </c:when>
			                        <c:otherwise>
			                        <c:set var="selectedInd" value=""></c:set>
			                        </c:otherwise>
		                            </c:choose>
				      <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
				                    <c:out value="${chrms}"></c:out>
				                    </option>
					</c:forEach>		      
			</select>    
    </display:column>
  	<display:column  title="Pieces" headerClass="containeralign" style="width:70px;text-align: right;padding-right: 0.3em;">
  		<input type="text" class="input-text" style="text-align:right;width:70px" name="carton.pieces" maxlength="4" onchange="updateCartonDetails('${cartonList.id}','pieces',this,'Carton');" value="<c:out value="${cartonList.pieces}"/>"
    </display:column>
    <c:if test="${UnitType==true}">
    <display:column  headerClass="containeralign"  title="Gross&nbsp;Weight" style="width:70px;text-align: right;padding-right: 0.3em;">    
    	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.grossWeight" id="grossWeight${cartonList.id}" onchange="onlyFloat(this);calcNetWeight1(${cartonList.id});" value="<c:out value="${cartonList.grossWeight}"/>"
    </display:column>
    <display:column headerClass="hidden"   title="Gross Wt Kilo" style="width:50px" class="hidden">
  		<input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="carton.grossWeightKilo" id="grossWeightKilo${cartonList.id}" value="<c:out value="${cartonList.grossWeightKilo}"/>" 
 	</display:column>   
     <display:column  headerClass="containeralign"  titleKey="carton.emptyContWeight" style="width:70px;text-align: right;padding-right: 0.3em;">
       	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.emptyContWeight" id="emptyContWeight${cartonList.id}" onchange="onlyFloat(this);calcNetWeight1(${cartonList.id});" value="<c:out value="${cartonList.emptyContWeight}"/>"    
     </display:column>
     <display:column headerClass="hidden"  title="Tare Wt Kilo" style="width:50px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="carton.emptyContWeightKilo" id="emptyContWeightKilo${cartonList.id}" value="<c:out value="${cartonList.emptyContWeightKilo}"/>"
	</display:column>
     <display:column  headerClass="containeralign"  titleKey="carton.netWeight" style="width:70px;text-align: right;padding-right: 0.3em;">
         <input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.netWeight" id="netWeight${cartonList.id}" onchange="onlyFloat(this);calculateTare(${cartonList.id});" value="<c:out value="${cartonList.netWeight}"/>"
    </display:column>
    <display:column headerClass="hidden"  title="Net Wt Kilo" style="width:50px" class="hidden" >
	 	<input type="hidden" class="input-text" style="text-align:right;width:50px" maxlength="10" name="carton.netWeightKilo" id="netWeightKilo${cartonList.id}" value="<c:out value="${cartonList.netWeightKilo}"/>" 
 	</display:column>
    </c:if>
        
    <c:if test="${UnitType==false}">
     <display:column  headerClass="containeralign"  title="Gross&nbsp;Weight" style="width:70px;text-align: right;padding-right: 0.3em;">
        <input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.grossWeightKilo" id="grossWeightKilo${cartonList.id}" onchange="onlyFloat(this);calcNetWeightKilo(${cartonList.id});" value="<c:out value="${cartonList.grossWeightKilo}"/>"          
      </display:column>
      <display:column headerClass="hidden"   title="Gross Wt" style="width:80px" class="hidden">
  		<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="carton.grossWeight" id="grossWeight${cartonList.id}" value="<c:out value="${cartonList.grossWeight}"/>" 
 	</display:column>   
     <display:column  headerClass="containeralign"  titleKey="carton.emptyContWeight" style="width:70px;text-align: right;padding-right: 0.3em;">
          <input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.emptyContWeightKilo" id="emptyContWeightKilo${cartonList.id}" onchange="onlyFloat(this);calcNetWeightKilo(${cartonList.id});" value="<c:out value="${cartonList.emptyContWeightKilo}"/>"
      </display:column>
      <display:column headerClass="hidden"  title="Tare Wt" style="width:70px" class="hidden"> 
  		<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="carton.emptyContWeight" id="emptyContWeight${cartonList.id}" value="<c:out value="${cartonList.emptyContWeight}"/>"
 	</display:column>
     <display:column  headerClass="containeralign"  titleKey="carton.netWeight" style="width:70px;text-align: right;padding-right: 0.3em;">
     	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="carton.netWeightKilo" id="netWeightKilo${cartonList.id}" onchange="onlyFloat(this);calcTareKilo(${cartonList.id});" value="<c:out value="${cartonList.netWeightKilo}"/>"
     </display:column>
     <display:column headerClass="hidden"  title="Net Wt" style="width:70px" class="hidden">
 		<input type="hidden" class="input-text" style="text-align:right;width:100px" maxlength="10" name="carton.netWeight" id="netWeight${cartonList.id}" value="<c:out value="${cartonList.netWeight}"/>" 
 	</display:column>
     </c:if>
     
     <display:column  headerClass="containeralign"  titleKey="carton.length" style="width:70px;text-align: right;padding-right: 0.3em;">
    	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="5" name="carton.length" id="length${cartonList.id}" onchange="onlyFloat(this);calculateCartonDensityVolumn('${cartonList.id}','length');" value="<c:out value="${cartonList.length}"/>"
    </display:column>
    <display:column  headerClass="containeralign"  titleKey="carton.width" style="width:70px;text-align: right;padding-right: 0.3em;">
        <input type="text" class="input-text" style="text-align:right;width:90px" maxlength="5" name="carton.width" id="width${cartonList.id}" onchange="onlyFloat(this);calculateCartonDensityVolumn('${cartonList.id}','width');" value="<c:out value="${cartonList.width}"/>"
     </display:column>
    <display:column  headerClass="containeralign"  titleKey="carton.height" style="width:70px;text-align: right;padding-right: 0.3em;">
     	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="5" name="carton.height" id="height${cartonList.id}" onchange="onlyFloat(this);calculateCartonDensityVolumn('${cartonList.id}','height');" value="<c:out value="${cartonList.height}"/>"
     </display:column>
   
    <c:if test="${VolumeType==true}">
    <display:column  headerClass="containeralign"   titleKey="carton.volume" style="width:70px;text-align: right;padding-right: 0.3em;">
    	<input type="text" class="input-textUpper" style="text-align:right;width:90px" readonly="true" name="carton.volume" id="volume${cartonList.id}" onchange="" value="<c:out value="${cartonList.volume}"/>"
    </display:column>
     <display:column  headerClass="hidden"  title="Volumn Cbm" style="width:40px" class="hidden">
		<input type="hidden" class="input-textUpper" style="text-align:right;width:40px" readonly="true" name="carton.volumeCbm" id="volumeCbm${cartonList.id}" onchange="" value="<c:out value="${cartonList.volumeCbm}"/>"                  
	</display:column>
    <display:column  headerClass="containeralign" style="text-align: right;padding-right: 0.3em;width:40px;"  titleKey="carton.density">
    	<input type="text" class="input-textUpper" style="text-align:right;width:90px" readonly="true" name="carton.density" id="density${cartonList.id}" onchange="" value="<c:out value="${cartonList.density}"/>"
    </display:column>
     <display:column headerClass="hidden"  title="Density Metric" style="width:30px" class="hidden">
 		<input type="hidden" class="input-textUpper" style="text-align:right;width:30px" readonly="true" name="carton.densityMetric" id="densityMetric${cartonList.id}" onchange="" value="<c:out value="${cartonList.densityMetric}"/>"
 	</display:column>
    </c:if>
   
    <c:if test="${VolumeType==false}">
    <display:column  headerClass="containeralign"   titleKey="carton.volume" style="width:70px;text-align: right;padding-right: 0.3em;">
     	<input type="text" class="input-textUpper" style="text-align:right;width:90px" readonly="true" name="carton.volumeCbm" id="volumeCbm${cartonList.id}" onchange="" value="<c:out value="${cartonList.volumeCbm}"/>"             
    </display:column>
     <display:column  headerClass="hidden"  titleKey="Carton.volume" style="width:70px" class="hidden">
 		<input type="hidden" class="input-textUpper" style="text-align:right;width:100px" readonly="true" name="carton.volume" id="volume${cartonList.id}" onchange="" value="<c:out value="${cartonList.volume}"/>"
	</display:column>
    <display:column  headerClass="containeralign" style="text-align: right;width:40px;padding-right: 0.3em;"  titleKey="carton.density">
     	<input type="text" class="input-textUpper" style="text-align:right;width:90px" readonly="true" name="carton.densityMetric" id="densityMetric${cartonList.id}" onchange="" value="<c:out value="${cartonList.densityMetric}"/>"
     </display:column>
      <display:column headerClass="hidden"  titleKey="Carton.density" style="width:70px;text-align: right;" class="hidden">
		<input type="hidden" class="input-textUpper" style="text-align:right;width:100px" readonly="true" name="carton.density" id="density${cartonList.id}" onchange="" value="<c:out value="${cartonList.density}"/>"
	</display:column>
    </c:if>
    
     <display:column headerClass="hidden"  title="Unit1" style="width:70px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:100px" name="carton.unit1" id="unit1${cartonList.id}" value="<c:out value="${cartonList.unit1}"/>"
	</display:column>
     <display:column headerClass="hidden"  title="Unit2" style="width:70px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:100px" name="carton.unit2" id="unit2${cartonList.id}" value="<c:out value="${cartonList.unit2}"/>"
	</display:column>
	 <display:column headerClass="hidden"  title="Unit3" style="width:70px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:100px" name="carton.unit3" id="unit3${cartonList.id}" value="<c:out value="${cartonList.unit3}"/>"
	</display:column>
	<sec-auth:authComponent componentId="module.tab.container.auditTab">
	    <display:column title="Audit" style="width:25px; text-align: center;">
	    	<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${cartonList.id}&tableName=carton&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
	    </display:column>
    </sec-auth:authComponent> 
    	<display:column title="Status" style="width:45px; text-align: center;">
		<c:if test="${cartonList.status}">
          <input type="checkbox"  name="status${cartonList.id}" id="status${cartonList.id}" checked="checked" onclick="deleteCartonDetails(${cartonList.id},this);" />
        </c:if>
       <c:if test="${cartonList.status==false}">
		<input type="checkbox"  name="status${cartonList.id}" id="status${cartonList.id}"   onclick="deleteCartonDetails(${cartonList.id},this);" />
       </c:if> 
		</display:column> 
    <display:setProperty name="paging.banner.item_name" value="carton"/>  
    <display:setProperty name="paging.banner.items_name" value="carton"/>
  
    <display:setProperty name="export.excel.filename" value="Carton List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Carton List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Carton List.pdf"/>
    
    <display:footer>
<tr>
 	  	          <td></td>
 	  	          <td align="right" colspan="2"><b><div align="right"><fmt:message key="carton.total"/></div></b></td>
 	  	          
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${carton.totalPieces}" /></div></td>
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${carton.totalGrossWeight}" /></div></td>
                  <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${carton.totalTareWeight}" /></div></td>
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${carton.totalNetWeight}" /></div></td>
                  <td></td>
                  <td></td>
                  <td></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${carton.totalVolume}" /></div></td>
		  	     <td></td> 
		  	      <td ></td> 
		  	      <td style="border-right:2px solid #74B3DC;"></td>
		  	</tr>
		  	</display:footer>   
</display:table>   

<s:hidden name="id"></s:hidden>
<sec-auth:authComponent componentId="module.button.carton.addButton">
  <c:out value="${buttons}" escapeXml="false" />
  </sec-auth:authComponent>
  </div>
  <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">		  

function goToCustomerDetail(targetValue){
    document.forms['serviceForm2'].elements['id'].value = targetValue;
    document.forms['serviceForm2'].action = 'editCarton.html?from=list';
    document.forms['serviceForm2'].submit();
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	     location.href = "updateStatusList.html?id="+encodeURI(targetElement)+"&sid=${serviceOrder.id}";	    
	     location.href = 'containers.html?id='+${serviceOrder.id};	 	
     }else{
		return false;
	}
}

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function updateCartonDetails(id,fieldName,target,tableName){
	showOrHide(1);
	var fieldValue ="";		
		fieldValue = target.value;		
		if(fieldValue==undefined){
			fieldValue = target;
		}
	var url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
	  http2.open("GET", url, true); 
	  http2.onreadystatechange = function(){ handleHttpResponse2(id,fieldName,fieldValue,tableName)};
	  http2.send(null);	 
}
function handleHttpResponse2(id,fieldName,fieldValue,tableName){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();             
            if(fieldName=='pieces' || fieldName=='cntnrNumber'){
            	getCartonDetails();            	
            }
            showOrHide(0);
	}
}
var http2 = getHTTPObject();

function calcNetWeight1(id){
	   var Q1 = eval(document.getElementById('grossWeight'+id).value);
	   var Q2 = eval(document.getElementById('emptyContWeight'+id).value);
	   var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getCartonDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateCartonNetWeightDetails(id,Q1,Q2);
		}else if((Q1>Q2) || Q2 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getCartonDetails();
		}
	}
	
function calculateTare(id){	
	   var Q1 = eval(document.getElementById('grossWeight'+id).value);
	   var Q2 = eval(document.getElementById('netWeight'+id).value);
	   var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Net weight");
			getCartonDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateCartonTareWeight(id,Q1,Q2);
		}else if((Q1>Q2) ||Q2 == undefined){
			alert("Gross weight should be greater than Net weight");
			getCartonDetails();
		}
		 
	}
var fieldTypeName = '';
var fieldTypeVal = '';
function calculateCartonDensityVolumn(id,fieldType){
	showOrHide(1);
	var weightUnitKgs = document.getElementById('unit2'+id).value;
	var volumeUnitCbm = document.getElementById('unit3'+id).value ;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('grossWeight'+id).value);
	var netWeight = eval(document.getElementById('netWeight'+id).value);
	var volume = eval(document.getElementById('volume'+id).value);
	var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
	var width= eval (document.getElementById('width'+id).value);
	var height= eval (document.getElementById('height'+id).value);
	var length= eval (document.getElementById('length'+id).value);
	
	if(fieldType =='length'){
		fieldTypeName = fieldType;
		fieldTypeVal = length;
	}else if(fieldType =='width'){
		fieldTypeName = fieldType;
		fieldTypeVal = width;
	}else{
		fieldTypeName = fieldType;
		fieldTypeVal = height;
	}
	var density=0;
	var densityMetric=0;
	var factor = 0;
	var factor1 = 0;
	
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cft'){
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cft'){
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 1;
		}
	}
	var density = width*length*height;
	if(weightUnitKgs == 'Cbm'){
			document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}
		if(weightUnitKgs == 'Cft'){
			document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}

	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{ 
			density=grossWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{			
			densityMetric=grossWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
		}
	}else{
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{
			density=netWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{
			densityMetric=netWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
		}
	}
	
	var url = "updateCartonDetailsDensityAjax.html?ajax=1&id="+id+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1
				+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&fieldTypeName="+fieldTypeName+"&fieldTypeVal="+fieldTypeVal+"&tableName1="+tableName1;
	
	  httpCartonDensity.open("GET", url, true); 
	  httpCartonDensity.onreadystatechange = function(){handleHttpResponseCartonDensity(id)};
	  httpCartonDensity.send(null);
}

function handleHttpResponseCartonDensity(id){
	if (httpCartonDensity.readyState == 4){		
            var results = httpCartonDensity.responseText
            results = results.trim();
            getCartonDetails();
            showOrHide(0);
	}
}
var httpCartonDensity = getHTTPObject();

function calcNetWeightKilo(id){
	var Q1 = eval(document.getElementById('grossWeightKilo'+id).value);
	var Q2 = eval(document.getElementById('emptyContWeightKilo'+id).value);
	var E1='';
	   if((Q2>Q1) || Q1 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getCartonDetails();
		}else if(Q1 != undefined && Q2 != undefined){			
			updateCartonNetWtKiloDetails(id,Q1,Q2);
		}else if((Q1>Q2) ||Q2 == undefined){
			alert("Gross weight should be greater than Tare weight");
			getCartonDetails();
		}
}

function calcTareKilo(id){
	var Q1 = eval(document.getElementById('grossWeightKilo'+id).value);
	var Q2 = eval(document.getElementById('netWeightKilo'+id).value);
	var E1='';
	if((Q2>Q1) || Q1 == undefined){
		alert("Gross weight should be greater than Net weight");
		getCartonDetails();
	}else if(Q1 != undefined && Q2 != undefined){		
		updateCartonTareKiloDetails(id,Q1,Q2);
	}else if((Q1>Q2) ||Q2 == undefined){
		alert("Gross weight should be greater than Net weight");
		getCartonDetails();
	}
}

var tableName1 = 'Carton';
var fieldNetWeight1 = 'netWeight';
var fieldGrossWeight1 = 'grossWeight';
var fieldEmptyConWeight1 ='emptyContWeight';
var fieldNetWeightKilo1 ='netWeightKilo';
var fieldGrossWeightKilo1 = 'grossWeightKilo';
var fieldemptyContWeightKilo1 = 'emptyContWeightKilo';
var fieldVolume1 = 'volume';
var fieldVolumeCbm1 = 'volumeCbm';
var fieldDensity1 = 'density';
var fieldDensityMetric1 = 'densityMetric';

function updateCartonNetWeightDetails(id,Q1,Q2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	var fieldNetWeightVal1 = document.getElementById('netWeight'+id).value=E2;
	var fieldGrossWeightVal1 = Q1;
	var fieldEmptyConWeightVal1 = Q2;
	
	var E3 = E1*0.4536;
	var E4=Math.round(E3*10000)/10000;
	var fieldNetWeightKiloVal1 = document.getElementById('netWeightKilo'+id).value=E4;
	var Q3 = Q1*0.4536;
	var Q4=Math.round(Q3*10000)/10000;
	var fieldGrossWeightKiloVal1 = document.getElementById('grossWeightKilo'+id).value=Q4;
	var Q5 = Q2*0.4536;
	var Q6=Math.round(Q5*10000)/10000;
	var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value=Q6;
	
	var weightUnitKgs = document.getElementById('unit2'+id).value;
	var volumeUnitCbm = document.getElementById('unit3'+id).value ;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('grossWeight'+id).value);
	var netWeight = eval(document.getElementById('netWeight'+id).value);
	var volume = eval(document.getElementById('volume'+id).value);
	var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
	var width= eval (document.getElementById('width'+id).value);
	var height= eval (document.getElementById('height'+id).value);
	var length= eval (document.getElementById('length'+id).value);
	
	var density=0;
	var densityMetric=0;
	var factor = 0;
	var factor1 = 0;
	
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cft'){
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cft'){
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 1;
		}
	}
	var density = width*length*height;
	if(weightUnitKgs == 'Cbm'){
			document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}
		if(weightUnitKgs == 'Cft'){
			document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}

	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{ 
			density=grossWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{			
			densityMetric=grossWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
		}
	}else{
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{
			density=netWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{
			densityMetric=netWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
		}
	}	
	
	var url = "updateCartonNetWeightDetailsAjax.html?ajax=1&id="+id+"&fieldNetWeight1="+fieldNetWeight1+"&fieldNetWeightVal1="+fieldNetWeightVal1+"&fieldGrossWeight1="+fieldGrossWeight1+"&fieldGrossWeightVal1="+fieldGrossWeightVal1+"&fieldEmptyConWeight1="+fieldEmptyConWeight1+"&fieldEmptyConWeightVal1="+fieldEmptyConWeightVal1
				+"&fieldNetWeightKilo1="+fieldNetWeightKilo1+"&fieldNetWeightKiloVal1="+fieldNetWeightKiloVal1+"&fieldGrossWeightKilo1="+fieldGrossWeightKilo1+"&fieldGrossWeightKiloVal1="+fieldGrossWeightKiloVal1+"&fieldemptyContWeightKilo1="+fieldemptyContWeightKilo1+"&fieldemptyContWeightKiloVal1="+fieldemptyContWeightKiloVal1
				+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&tableName1="+tableName1;
	  httpCartonNetWt.open("GET", url, true); 
	  httpCartonNetWt.onreadystatechange = handleHttpResponseCartonNetWt;
	  httpCartonNetWt.send(null);
}
function handleHttpResponseCartonNetWt(){
	if (httpCartonNetWt.readyState == 4){		
            var results = httpCartonNetWt.responseText
            results = results.trim();
            getCartonDetails();
            showOrHide(0);            
	}
}
var httpCartonNetWt = getHTTPObject();

function updateCartonTareWeight(id,Q1,Q2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	var fieldEmptyConWeightVal1 = document.getElementById('emptyContWeight'+id).value=E2;
	var fieldNetWeightVal1 = Q2;
	var E3 = E1*0.4536;
	var E4=Math.round(E3*10000)/10000;
	var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value=E4;
	var E5 = Q2*0.4536;
	var E6=Math.round(E5*10000)/10000;
	var fieldNetWeightKiloVal1 = document.getElementById('netWeightKilo'+id).value=E6;
	
	var weightUnitKgs = document.getElementById('unit2'+id).value;
	var volumeUnitCbm = document.getElementById('unit3'+id).value ;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('grossWeight'+id).value);
	var netWeight = eval(document.getElementById('netWeight'+id).value);
	var volume = eval(document.getElementById('volume'+id).value);
	var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
	var width= eval (document.getElementById('width'+id).value);
	var height= eval (document.getElementById('height'+id).value);
	var length= eval (document.getElementById('length'+id).value);
	
	var density=0;
	var densityMetric=0;
	var factor = 0;
	var factor1 = 0;
	
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cft'){
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cft'){
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 1;
		}
	}
	var density = width*length*height;
	if(weightUnitKgs == 'Cbm'){
			document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}
		if(weightUnitKgs == 'Cft'){
			document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}

	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{ 
			density=grossWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{			
			densityMetric=grossWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
		}
	}else{
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{
			density=netWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{
			densityMetric=netWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
		}
	}
	var url = "updateCartonDetailsTareWtAjax.html?ajax=1&id="+id+"&fieldNetWeight1="+fieldNetWeight1+"&fieldNetWeightVal1="+fieldNetWeightVal1+"&fieldNetWeightKilo1="+fieldNetWeightKilo1+"&fieldNetWeightKiloVal1="+fieldNetWeightKiloVal1
			+"&fieldEmptyConWeight1="+fieldEmptyConWeight1+"&fieldEmptyConWeightVal1="+fieldEmptyConWeightVal1+"&fieldemptyContWeightKilo1="+fieldemptyContWeightKilo1+"&fieldemptyContWeightKiloVal1="+fieldemptyContWeightKiloVal1
			+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&tableName1="+tableName1;
		httpCartonTareWt.open("GET", url, true); 
		httpCartonTareWt.onreadystatechange = handleHttpResponseCartonTareWt;
		httpCartonTareWt.send(null);	
}
function handleHttpResponseCartonTareWt(){
	if (httpCartonTareWt.readyState == 4){		
            var results = httpCartonTareWt.responseText
            results = results.trim();
            getCartonDetails();
            showOrHide(0);
	}
}
var httpCartonTareWt = getHTTPObject();


function updateCartonNetWtKiloDetails(id,Q1,Q2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	var fieldNetWeightKiloVal1 = document.getElementById('netWeightKilo'+id).value=E2;
	var E3 = E1*2.2046;
	var E4=Math.round(E3*10000)/10000;
	var fieldNetWeightVal1 = document.getElementById('netWeight'+id).value=E4;
	var Q3 = Q1*2.2046;
	var Q4=Math.round(Q3*10000)/10000;
	var fieldGrossWeightVal1 = document.getElementById('grossWeight'+id).value=Q4;
	var Q5 = Q2*2.2046;
	var Q6=Math.round(Q5*10000)/10000;
	var fieldEmptyConWeightVal1 = document.getElementById('emptyContWeight'+id).value=Q6;
	
	var fieldGrossWeightKiloVal1 = document.getElementById('grossWeightKilo'+id).value=Q1;
	var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value=Q2;
	
	var weightUnitKgs = document.getElementById('unit2'+id).value;
	var volumeUnitCbm = document.getElementById('unit3'+id).value ;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('grossWeight'+id).value);
	var netWeight = eval(document.getElementById('netWeight'+id).value);
	var volume = eval(document.getElementById('volume'+id).value);
	var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
	var width= eval (document.getElementById('width'+id).value);
	var height= eval (document.getElementById('height'+id).value);
	var length= eval (document.getElementById('length'+id).value);	

	var density=0;
	var densityMetric=0;
	var factor = 0;
	var factor1 = 0;
	
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cft'){
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cft'){
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 1;
		}
	}
	var density = width*length*height;
	if(weightUnitKgs == 'Cbm'){
			document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}
		if(weightUnitKgs == 'Cft'){
			document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}

	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{ 
			density=grossWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{			
			densityMetric=grossWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
		}
	}else{
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{
			density=netWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{
			densityMetric=netWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
		}
	}
	
	var url = "updateCartonNetWeightDetailsAjax.html?ajax=1&id="+id+"&fieldNetWeight1="+fieldNetWeight1+"&fieldNetWeightVal1="+fieldNetWeightVal1+"&fieldGrossWeight1="+fieldGrossWeight1+"&fieldGrossWeightVal1="+fieldGrossWeightVal1+"&fieldEmptyConWeight1="+fieldEmptyConWeight1+"&fieldEmptyConWeightVal1="+fieldEmptyConWeightVal1
				+"&fieldNetWeightKilo1="+fieldNetWeightKilo1+"&fieldNetWeightKiloVal1="+fieldNetWeightKiloVal1+"&fieldGrossWeightKilo1="+fieldGrossWeightKilo1+"&fieldGrossWeightKiloVal1="+fieldGrossWeightKiloVal1+"&fieldemptyContWeightKilo1="+fieldemptyContWeightKilo1+"&fieldemptyContWeightKiloVal1="+fieldemptyContWeightKiloVal1
				+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&tableName1="+tableName1;
		httpCartonNetWtKilo.open("GET", url, true); 
		httpCartonNetWtKilo.onreadystatechange = handleHttpResponseCartonNetWtKilo;
		httpCartonNetWtKilo.send(null);
}
function handleHttpResponseCartonNetWtKilo(){
		if (httpCartonNetWtKilo.readyState == 4){		
		var results = httpCartonNetWtKilo.responseText
		results = results.trim();
		getCartonDetails();
		showOrHide(0);            
		}
}
var httpCartonNetWtKilo = getHTTPObject();

function updateCartonTareKiloDetails(id,Q1,Q2){
	showOrHide(1);
	var E1='';
	E1=Q1-Q2;
	var E2=Math.round(E1*10000)/10000;
	var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value=E2;
	var E3 = E1*2.2046;
	var E4=Math.round(E3*10000)/10000;		
	var fieldEmptyConWeightVal1 = document.getElementById('emptyContWeight'+id).value=E4;
	var Q3 = Q1*2.2046;
	var Q4=Math.round(Q3*10000)/10000;
	//document.getElementById('grossWeight'+id).value=Q4;
	var E5 = Q3*2.2046;
	var E6=Math.round(E5*10000)/10000;
	var fieldNetWeightVal1 = document.getElementById('netWeight'+id).value=E6;
	var fieldNetWeightKiloVal1 = document.getElementById('netWeightKilo'+id).value=Q2;
	
	var weightUnitKgs = document.getElementById('unit2'+id).value;
	var volumeUnitCbm = document.getElementById('unit3'+id).value ;
	var mode='${serviceOrder.mode}';;
	var grossWeight = eval(document.getElementById('grossWeight'+id).value);
	var netWeight = eval(document.getElementById('netWeight'+id).value);
	var volume = eval(document.getElementById('volume'+id).value);
	var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
	var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
	var width= eval (document.getElementById('width'+id).value);
	var height= eval (document.getElementById('height'+id).value);
	var length= eval (document.getElementById('length'+id).value);	

	var density=0;
	var densityMetric=0;
	var factor = 0;
	var factor1 = 0;
	
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0005787;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cft'){
			factor = 1;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cft'){
			factor = 0.0000353;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cft'){
			factor = 35.3147;
		}
	}
	if(volumeUnitCbm == 'Inches'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr'){
		if(weightUnitKgs == 'Cbm'){
			factor1 = 1;
		}
	}
	var density = width*length*height;
	if(weightUnitKgs == 'Cbm'){
			document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}
		if(weightUnitKgs == 'Cft'){
			document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
			var volume = document.getElementById('volume'+id).value;
			var fieldVolumeVal1 = volume ;
			document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
			var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
			var fieldVolumeCbmVal1 = volumeConvertedCbm;
		}

	if(mode=='Air'){
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{ 
			density=grossWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{			
			densityMetric=grossWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
		}
	}else{
		if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
			document.getElementById('density'+id).value=0;
			var fieldDensityVal1 = document.getElementById('density'+id).value;
		}else{
			density=netWeight/volume;
			var densityConverted = Math.round(density*100)/100;
			var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
		}
		if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
			document.getElementById('densityMetric'+id).value=0;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
		}else{
			densityMetric=netWeightKilo/volumeConvertedCbm;
			var densityMetricConverted = Math.round(densityMetric*100)/100;
			var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
		}
	}
	
	var url = "updateCartonDetailsTareWtAjax.html?ajax=1&id="+id+"&fieldNetWeight1="+fieldNetWeight1+"&fieldNetWeightVal1="+fieldNetWeightVal1+"&fieldNetWeightKilo1="+fieldNetWeightKilo1+"&fieldNetWeightKiloVal1="+fieldNetWeightKiloVal1
				+"&fieldEmptyConWeight1="+fieldEmptyConWeight1+"&fieldEmptyConWeightVal1="+fieldEmptyConWeightVal1+"&fieldemptyContWeightKilo1="+fieldemptyContWeightKilo1+"&fieldemptyContWeightKiloVal1="+fieldemptyContWeightKiloVal1
				+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&tableName1="+tableName1;
		httpCartonTareWtKilo.open("GET", url, true); 
		httpCartonTareWtKilo.onreadystatechange = handleHttpResponseCartonTareWtKilo;
		httpCartonTareWtKilo.send(null);	
}
function handleHttpResponseCartonTareWtKilo(){
		if (httpCartonTareWtKilo.readyState == 4){		
		    var results = httpCartonTareWtKilo.responseText
		    results = results.trim();
		    getCartonDetails();
		    showOrHide(0);
		}
}
var httpCartonTareWtKilo = getHTTPObject();

var fieldUnit1 = 'unit1';
var fieldUnit2 = 'unit2';
var fieldUnit3 = 'unit3';
var fieldLength = 'length';
var fieldWidth = 'width';
var fieldHeight = 'height';

function populateUnitValuesByCarton(id){
	showOrHide(1);
	var fieldValueCartonVal = document.getElementById('cartonType'+id).value
	if(fieldValueCartonVal!=''){
		var url = "findUnitValuesByCartonAjax.html?ajax=1&decorator=simple&popup=true&cartonVal="+encodeURI(fieldValueCartonVal);
		  http1112.open("GET", url, true); 
		  http1112.onreadystatechange = function(){handleHttpResponse1112(id,fieldValueCartonVal)};
		  http1112.send(null);
	}
}
function handleHttpResponse1112(id,fieldValueCartonVal){
	if (http1112.readyState == 4){
            var results = http1112.responseText
            results = results.trim();
        	if(results!=''){
    	    	var	res = results.split("~");    	    	
    	    	if(res[0]!='NA'){
    	    		var fieldEmptyConWeightVal1 = document.getElementById('emptyContWeight'+id).value=res[0];
	    		}else{
	    			var ET = "0.00";
	    			var fieldEmptyConWeightVal1 = document.getElementById('emptyContWeight'+id).value=ET;
	    		}
    	    	if(res[1]!='NA'){
    	    		var res1 = res[1];
    	    		document.getElementById('unit1'+id).value=res1;
	    		}
    	    	if(res[2]!='NA'){
    	    		var fieldLengthVal = document.getElementById('length'+id).value=res[2];
	    		}else{
	    			var L = "0.00";
	    			var fieldLengthVal = document.getElementById('length'+id).value=L;
	    		}
    	    	if(res[3]!='NA'){
    	    		var fieldWidthVal = document.getElementById('width'+id).value=res[3];
	    		}else{
	    			var W = "0.00";
	    			var fieldWidthVal = document.getElementById('width'+id).value=W;
	    		}
    	    	if(res[4]!='NA'){
    	    		var fieldHeightVal = document.getElementById('height'+id).value=res[4];
	    		}else{
	    			var H = "0.00";
	    			var fieldHeightVal = document.getElementById('height'+id).value=H;
	    		}
    	    	if(res[5]!='NA'){
    	    		var fieldUnit3Val = document.getElementById('unit3'+id).value.value=res[5];
	    		}
    	    	if(res[6]!='NA'){
    	    		var fieldVolumeVal1 = document.getElementById('volume'+id).value=res[6];
	    		}else{
	    			var VL = "0.00";
	    			var fieldVolumeVal1 = document.getElementById('volume'+id).value=VL;
	    		}
    	    	if(res[7]!='NA'){
    	    		var res7 = res[7];
    	    		document.getElementById('unit2'+id).value=res7;
	    		}    	    	
    	    	
    	    	  var t = document.getElementById('unit2'+id).value;
    	    	  var volume = document.getElementById('volume'+id).value;
    	    	  var emptyContWeight = document.getElementById('emptyContWeight'+id).value;
    	    	  if(t == 'Cbm'){    	    		  	
    	    		  	var V1 = Math.round(volume*35.3147*1000)/1000;
    	    		  	var fieldVolumeCbmVal1 = document.getElementById('volumeCbm'+id).value = V1;
    	    		  
    	    			var E1 = Math.round(emptyContWeight*2.2046*1000)/1000;
    	    			var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value=E1;
    	    			}
    	    			if(t == 'Cft'){
    	    			var V2 = Math.round(volume*0.0283*1000)/1000;
    	    			var fieldVolumeCbmVal1 = document.getElementById('volumeCbm'+id).value = V2;
    	    			var E2 = Math.round(emptyContWeight*0.4536*1000)/1000;
    	    			var fieldemptyContWeightKiloVal1 = document.getElementById('emptyContWeightKilo'+id).value= E2;
    	    			}
    	    	  if(t=='Cbm'){
    	    		  var fieldUnit1Val = document.getElementById('unit1'+id).value='Kgs';
	    	    	  var fieldUnit2Val = document.getElementById('unit2'+id).value='Cbm';
    	    	  } else{
    	    		  var lbs = 'Lbs';
    	    		  var fieldUnit1Val = document.getElementById('unit1'+id).value=lbs;
	    	    	  var cft = 'Cft';
	    	    	  var fieldUnit2Val = document.getElementById('unit2'+id).value=cft;
    	    	  }
    	    	  
    	    		var weightUnitKgs = document.getElementById('unit2'+id).value;
    	    		var volumeUnitCbm = document.getElementById('unit3'+id).value ;
    	    		var mode='${serviceOrder.mode}';;
    	    		var grossWeight = eval(document.getElementById('grossWeight'+id).value);
    	    		var netWeight = eval(document.getElementById('netWeight'+id).value);
    	    		var volume = eval(document.getElementById('volume'+id).value);
    	    		var netWeightKilo= eval (document.getElementById('netWeightKilo'+id).value);
    	    		var grossWeightKilo= eval (document.getElementById('grossWeightKilo'+id).value);
    	    		var width= eval (document.getElementById('width'+id).value);
    	    		var height= eval (document.getElementById('height'+id).value);
    	    		var length= eval (document.getElementById('length'+id).value);
    	    		
    	    		
    	    		var density=0;
    	    		var densityMetric=0;
    	    		var factor = 0;
    	    		var factor1 = 0;
    	    		
    	    		if(volumeUnitCbm == 'Inches'){
    	    			if(weightUnitKgs == 'Cft'){
    	    				factor = 0.0005787;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Ft'){
    	    			if(weightUnitKgs == 'Cft'){
    	    				factor = 1;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Cm'){
    	    			if(weightUnitKgs == 'Cft'){
    	    				factor = 0.0000353;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Mtr'){
    	    			if(weightUnitKgs == 'Cft'){
    	    				factor = 35.3147;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Inches'){
    	    			if(weightUnitKgs == 'Cbm'){
    	    				factor1 = 0.0000164;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Ft'){
    	    			if(weightUnitKgs == 'Cbm'){
    	    				factor1 = 0.02834;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Cm'){
    	    			if(weightUnitKgs == 'Cbm'){
    	    				factor1 = 0.000001;
    	    			}
    	    		}
    	    		if(volumeUnitCbm == 'Mtr'){
    	    			if(weightUnitKgs == 'Cbm'){
    	    				factor1 = 1;
    	    			}
    	    		}
    	    		var density = width*length*height;
    	    		if(weightUnitKgs == 'Cbm'){
    	    				document.getElementById('volume'+id).value = Math.round(density*factor1*35.3147*1000)/1000;
    	    				var volume = document.getElementById('volume'+id).value;
    	    				var fieldVolumeVal1 = volume ;
    	    				document.getElementById('volumeCbm'+id).value = Math.round(density*factor1*1000)/1000;
    	    				var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
    	    				var fieldVolumeCbmVal1 = volumeConvertedCbm;
    	    			}
    	    			if(weightUnitKgs == 'Cft'){
    	    				document.getElementById('volume'+id).value = Math.round(density*factor*1000)/1000;
    	    				var volume = document.getElementById('volume'+id).value;
    	    				var fieldVolumeVal1 = volume ;
    	    				document.getElementById('volumeCbm'+id).value = Math.round(density*factor*0.0283168*1000)/1000;
    	    				var volumeConvertedCbm = document.getElementById('volumeCbm'+id).value;
    	    				var fieldVolumeCbmVal1 = volumeConvertedCbm;
    	    			}

    	    		if(mode=='Air'){
    	    			if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
    	    				document.getElementById('density'+id).value=0;
    	    				var fieldDensityVal1 = document.getElementById('density'+id).value;
    	    			}else{ 
    	    				density=grossWeight/volume;
    	    				var densityConverted = Math.round(density*100)/100;
    	    				var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
    	    			}
    	    			if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
    	    				document.getElementById('densityMetric'+id).value=0;
    	    				var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
    	    			}else{			
    	    				densityMetric=grossWeightKilo/volumeConvertedCbm;
    	    				var densityMetricConverted = Math.round(densityMetric*100)/100;
    	    				var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value=densityMetricConverted;
    	    			}
    	    		}else{
    	    			if(netWeight==undefined||netWeight==0||netWeight==0.00||grossWeight==undefined||grossWeight==0||grossWeight==0.00||volume==undefined||volume==0||volume==0.00 ){
    	    				document.getElementById('density'+id).value=0;
    	    				var fieldDensityVal1 = document.getElementById('density'+id).value;
    	    			}else{
    	    				density=netWeight/volume;
    	    				var densityConverted = Math.round(density*100)/100;
    	    				var fieldDensityVal1 = document.getElementById('density'+id).value = densityConverted;
    	    			}
    	    			if(netWeightKilo==undefined || netWeightKilo==0 || netWeightKilo==0.00|| grossWeightKilo==undefined || grossWeightKilo==0|| grossWeightKilo==0.00 || volumeConvertedCbm==undefined||volumeConvertedCbm==0||volumeConvertedCbm==0.00){
    	    				document.getElementById('densityMetric'+id).value=0;
    	    				var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value;
    	    			}else{
    	    				densityMetric=netWeightKilo/volumeConvertedCbm;
    	    				var densityMetricConverted = Math.round(densityMetric*100)/100;
    	    				var fieldDensityMetricVal1 = document.getElementById('densityMetric'+id).value = densityMetricConverted;
    	    			}
    	    		}
    	    	  
    	    updateValueByCartonType(id,fieldEmptyConWeightVal1,fieldUnit1Val,fieldLengthVal,fieldWidthVal,fieldHeightVal,fieldUnit3Val,fieldVolumeVal1,fieldUnit2Val,fieldVolumeCbmVal1,fieldemptyContWeightKiloVal1,fieldValueCartonVal,fieldDensityVal1,fieldDensityMetricVal1 );
        }  
	}
}
var http1112 = getHTTPObject();

function updateValueByCartonType(id,fieldEmptyConWeightVal1,fieldUnit1Val,fieldLengthVal,fieldWidthVal,fieldHeightVal,fieldUnit3Val,fieldVolumeVal1,fieldUnit2Val,fieldVolumeCbmVal1,fieldemptyContWeightKiloVal1,fieldValueCartonVal,fieldDensityVal1,fieldDensityMetricVal1){
	
	var url = "updateValueByCartonTypeAjax.html?ajax=1&id="+id+"&fieldUnit1="+fieldUnit1+"&fieldUnit1Val="+fieldUnit1Val+"&fieldUnit2="+fieldUnit2+"&fieldUnit2Val="+fieldUnit2Val+"&fieldValueCartonVal="+fieldValueCartonVal
			+"&fieldUnit3="+fieldUnit3+"&fieldUnit3Val="+fieldUnit3Val+"&fieldLength="+fieldLength+"&fieldLengthVal="+fieldLengthVal+"&fieldWidth="+fieldWidth+"&fieldWidthVal="+fieldWidthVal
			+"&fieldHeight="+fieldHeight+"&fieldHeightVal="+fieldHeightVal+"&fieldVolume1="+fieldVolume1+"&fieldVolumeVal1="+fieldVolumeVal1+"&fieldVolumeCbm1="+fieldVolumeCbm1+"&fieldVolumeCbmVal1="+fieldVolumeCbmVal1
			+"&fieldEmptyConWeight1="+fieldEmptyConWeight1+"&fieldEmptyConWeightVal1="+fieldEmptyConWeightVal1+"&fieldemptyContWeightKilo1="+fieldemptyContWeightKilo1+"&fieldemptyContWeightKiloVal1="+fieldemptyContWeightKiloVal1
			+"&fieldDensity1="+fieldDensity1+"&fieldDensityVal1="+fieldDensityVal1+"&fieldDensityMetric1="+fieldDensityMetric1+"&fieldDensityMetricVal1="+fieldDensityMetricVal1+"&tableName1="+tableName1;
	httpCartonType.open("GET", url, true); 
	httpCartonType.onreadystatechange = handleHttpResponseCartonType;
	httpCartonType.send(null);	
}
function handleHttpResponseCartonType(){
		if (httpCartonType.readyState == 4){		
		var results = httpCartonType.responseText
		results = results.trim();
		getCartonDetails();
		showOrHide(0);
		}
}
var httpCartonType = getHTTPObject();

function deleteCartonDetails(targetElement,targetElementStatus){
	var massage="";
	if(targetElementStatus.checked==false){
		massage="Are you sure you wish to deactivate this row?"
	}else{
		massage="Are you sure you wish to activate this row?"
	}  
	var agree=confirm(massage);
		if (agree){
			showOrHide(1);
			var cartonStatus=false;
			if(targetElementStatus.checked==false){
				cartonStatus=false;	
			}else{
				cartonStatus=true;
			}
	   	var url = "deleteCartonDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&cartonStatus="+cartonStatus;
	 	  http12.open("GET", url, true); 
	 	  http12.onreadystatechange = handleHttpResponse12;
	 	  http12.send(null);
		}else{
			if(targetElementStatus.checked==false){
				document.getElementById('status'+targetElement).checked=true;
				}else{
					document.getElementById('status'+targetElement).checked=false;
				}
		}
}
function handleHttpResponse12(){
	if (http12.readyState == 4){
            var results = http12.responseText
            getCartonDetails();
            showOrHide(0);
	}
}
var http12 = getHTTPObject();

</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">   
	
</script>  
