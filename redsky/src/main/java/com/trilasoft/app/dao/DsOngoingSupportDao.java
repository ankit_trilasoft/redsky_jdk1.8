package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsOngoingSupport;

public interface DsOngoingSupportDao extends GenericDao<DsOngoingSupport, Long>{
	
	List getListById(Long id);

}

