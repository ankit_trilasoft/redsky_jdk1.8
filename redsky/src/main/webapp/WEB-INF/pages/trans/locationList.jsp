<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
		for(i=0;i<=3;i++){
			document.forms['locationformList'].elements['locationId'].value = "";
			document.forms['locationformList'].elements['warehouse'].value = "";
			document.forms['locationformList'].elements['type'].value = "";
			document.forms['locationformList'].elements['occupied'].checked = false;
			
		}
}
</script>
<script language="javascript" type="text/javascript">
	function check1(value,value1,capacity){
			if( value1 == 'X' && (capacity == '' || capacity == '1' || capacity == '2')){
					document.forms['assignItemsForm'].elements['forwardBtn'].disabled=true;
					alert('This location is already occupied. Please select any other.');
				  return false;  
				}else{
					document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
				}
			return true;
		}
		
function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
!margin-bottom:2px;
margin-top:-5px;
!margin-top:-28px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>

</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
 <configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<s:form id="locationformList"  action="searchLocates" method="post">
<div id="Layer1" style="width:100%">
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="serviceOrder.id" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
	<s:hidden name="noteID" value="%{workTicket.ticket}"/>
	<c:set var="noteID" value="%{workTicket.ticket}" />
		<div id="newmnav" style="!margin-bottom:7px;">
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent> 
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
  	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>	      
		       <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  	<c:if test="${forwardingTabVal!='Y'}"> 
	   				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			  			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">	
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  </sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 2px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>


<!-- Edited by Siddhartha -->

<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
				
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id") %>"><span>Work Ticket</span></a></li>
					  	<li><a href="bookStorages.html?id=<%=request.getParameter("id") %>"><span>Location List</span></a></li>
						<li id="newmnav1" style="background:#FFF"><a class="current"><span>Add Item To Location</span></a></li>
					  	<li><a href="storages.html?id=<%=request.getParameter("id") %>"><span>Access/ Release Items</span></a></li>
					  	<li><a href="storagesMove.html?id=${workTicket.id}"><span>Move Items</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div>
		<div class="spn">&nbsp;</div>
	
<table width="100%" cellspacing="1" cellpadding="0" border="0">
		<tbody>
			<tr>
			  <td>	
			  <div id="content" align="center" style="!margin-top:7px;">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							
							<td class="listwhitetext">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhitetext">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

	<table class="table" width="100%">
		<thead>
			<tr>
				<th><fmt:message key="location.locationId" /></th>
				<th><fmt:message key="location.warehouse" /></th>
				<th><fmt:message key="location.type" /></th>
				<th><fmt:message key="location.occupied" /></th>
				<th ></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield name="locationId" required="true" cssClass="input-text" cssStyle="width:165px" onkeydown="return quotesNotAllowed(event);"/></td>
				<td><s:select name="warehouse" list="%{house}" cssStyle="width:165px" cssClass="list-menu" headerKey=""  headerValue=""/></td>  
				<td>
				<configByCorp:customDropDown listType="map" list="${loctype_isactive}" fieldValue="${location.type}" 
   									attribute="id=type class=list-menu name=location.type style=width:230px headerKey='' headerValue='' "/>
							
				<%-- <s:select name="type" cssClass="list-menu" cssStyle="width:162px" list="%{locTYPE}" headerKey=""  headerValue=""/> --%>
				</td>
				<td><s:checkbox key="occupied" /></td>				
				<td width="130px" align="center"><c:out value="${searchbuttons}" escapeXml="false" /></td>
					
					
			</tr>
		</tbody>
	</table>

	<div id="otabs" style="margin-bottom:0px">
		  <ul>
		    <li><a class="current"><span>Location List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<!--  Till here -->	
	<s:set name="locations" value="locations" scope="request" />
	<display:table name="locations" class="table" requestURI="" defaultsort="2" id="locationList" export="true" pagesize="50">
		<display:column><input type="radio" name="dd" onclick="check(this); check1('${locationList.id}','${locationList.occupied}','${locationList.capacity}')"	value="${locationList.id}" /></display:column>
		<display:column property="locationId" sortable="true" titleKey="location.locationId" />
	 	<display:column property="capacity" headerClass="containeralign" style="text-align: right;" sortable="true"	title="Holding Capacity" />
		<display:column property="cubicFeet" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Capacity(Cft)" />
		<display:column property="cubicMeter" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Capacity(Cbm)" />
	    <display:column property="utilizedVolCft" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Used(Cft)" />
	    <display:column property="utilizedVolCbm" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Used(Cbm)" />
	    <display:column headerClass="containeralign" style="text-align: right;"  sortable="true" title="Available(Cft)">
	    <c:out value="${locationList.cubicFeet-locationList.utilizedVolCft}"></c:out>
	    </display:column>
	   <display:column headerClass="containeralign" style="text-align: right;"  sortable="true" title="Available(Cbm)">
	    <c:out value="${locationList.cubicMeter-locationList.utilizedVolCbm}"></c:out>
	    </display:column>
		<display:column property="type" sortable="true" titleKey="location.type"/>
		<display:column property="occupied" sortable="true" titleKey="location.occupied" />
		
		<display:setProperty name="paging.banner.item_name" value="location" />
		<display:setProperty name="paging.banner.items_name" value="location" />
		<display:setProperty name="paging.banner.sort" value="list" />
		
		<display:setProperty name="export.excel.filename" value="Location List.xls" />
		<display:setProperty name="export.csv.filename"	value="Location List.csv" />
		<display:setProperty name="export.pdf.filename" value="Location List.pdf" />
	</display:table>
</td>
</tr>
</tbody>
</table>

</div> 
 
</s:form>
<s:form id="assignItemsForm" action="editLocation" method="post">
	<s:hidden name="id" /> 
	<s:hidden name="locate"  />
	<s:hidden name="id1"  value="<%=request.getParameter("id") %>"/>
	<s:hidden name="ticket" value="%{workTicket.ticket}"/>
	<s:hidden name="shipNumber" value="%{workTicket.shipNumber}"/>
	<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
	<input type="submit" name="forwardBtn"  disabled value="Add" class="cssbutton" style="width:55px; height:25px" />
    </c:if>
    <c:if test='${serviceOrder.status == "CNCL"}'>
     <input type="button" name="forwardBtn"  class="cssbutton" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before adding location.')" value="Add" />
    </c:if>
    <c:if test='${serviceOrder.status == "HOLD"}'>
     <input type="button" name="forwardBtn"  class="cssbutton" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before adding location.')" value="Add" />
    </c:if> 
</s:form>  


<script type="text/javascript"> 
    highlightTableRows("locationList");
</script>
<script type="text/javascript">
		function check(targetElement) {
		try{
		  	document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
			document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
			//document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
		
		  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		}
		catch(e){}
		}
</script>

