<%@ include file="/common/taglibs.jsp"%> 
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head> 
<title><fmt:message key="emailSetUpPageList.title"/></title>   
<meta name="heading" content="<fmt:message key='emailSetUpPageList.heading'/>"/> 

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:1px 0;
text-align:right;
width:100%;
}
.table th.sorted, .table th.order2 {
	color:#15428B !important;
} 
</style>

 <script language="javascript" type="text/javascript">
 <%@ include file="/common/formCalender.js"%></script> 	   
	    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
		<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />    	
  
 </head> 
<s:form id="emailSetUpList" name="emailSetUpsList" action="searchEmailTracker" method="post" >
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <div id="Layer1" style="width:100%"> 
		<div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
		<table class="table" width="99%" >
		<thead>
		<tr>
		<th>From</th>
		<th>To</th>
		<th>File Number</th>
		<th>Subject</th>
		<th>Sent&nbsp;Date</th>
		<th>Email&nbsp;Status</th>
		<th></th>
		</thead>
		</tr>
		<tr>
		<td><s:textfield name="emailSetup.signature" cssStyle="width:150px;" cssClass="input-text" /></td>
		<td><s:textfield name="emailSetup.recipientTo" cssStyle="width:150px;" cssClass="input-text" /></td>
		<td><s:textfield name="emailSetup.fileNumber" cssStyle="width:150px;" cssClass="input-text" /></td>
		<td><s:textfield name="emailSetup.subject" cssStyle="width:150px;" cssClass="input-text" /></td>
		<td><c:if test="${not empty emailSetup.dateSent}">
			<s:text id="customerFileMoveDateFormattedValue"
				name="${FormDateValue}">
				<s:param name="value" value="emailSetup.dateSent" />
			</s:text>
			<s:textfield cssClass="input-text" id="dateSent"
				name="emailSetup.dateSent"
				value="%{customerFileMoveDateFormattedValue}" size="7"
				maxlength="11" onkeydown="return onlyDel(event,this)"/>
			<img id="dateSent_trigger" style="vertical-align: bottom"
				src="${pageContext.request.contextPath}/images/calender.png"
				HEIGHT=20 WIDTH=20 />

		</c:if> <c:if test="${empty emailSetup.dateSent}">
			<s:textfield cssClass="input-text" id="dateSent"
				name="emailSetup.dateSent" size="7" maxlength="11"
				onkeydown="return onlyDel(event,this)"  cssStyle="width:120px;"/>
			<img id="dateSent_trigger" style="vertical-align: bottom"
				src="${pageContext.request.contextPath}/images/calender.png"
				HEIGHT=20 WIDTH=20 />
			</c:if>
			</td><td>
			<s:select title="" cssClass="list-menu" name="emailStatusType" list="{'','All','Sent','Pending','Failed'}" cssStyle="width:150px"/>
			</td>
				<td width="130px">
			    <s:submit cssClass="cssbutton" cssStyle="width:57px;" method='searchEmailSetUp' key="button.search" />
		       <input type="button" class="cssbutton" value="Clear" style="width:57px;" onclick="clear_fields();"/>   
			   </td>			
				</tr>
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
	<div id="newmnav">
		  <ul>
		     <li><a><span>Email&nbsp;Tracker</span></a></li>		  
		  </ul>
		</div>
		<div class="spn"></div>
		
	<s:hidden name="id" value="<%=request.getParameter("id")%>" />
 	<s:set name="emailSetUp" value="emailSetUp" scope="request"/> 
	<display:table name="emailSetUpList" class="table" requestURI="" id="emailSetUpList" export="false" pagesize="25" style="width:100%;margin-top:0px;!margin-top:8px; "> 
 	<display:column property="signature" url="/emailtrackerview.html" paramId="id" paramProperty="id" sortable="true" title="From"  style="width:40px;"/>
 	<display:column title="To" sortable="true" style="width:40px;">
 	<c:set var="recipientToList" value="${fn:split(emailSetUpList.recipientTo, ',')}" />
 	<c:forEach var="tmp" items="${recipientToList}">
 	<c:set var="recipientTO" value="${tmp}" />
 	<c:out value="${recipientTO}"/>
 	</c:forEach>
 	</display:column>
 	<display:column title="CC" sortable="true" style="width:40px;">
 	<c:set var="recipientCcList" value="${fn:split(emailSetUpList.recipientCc, ',')}" />
 	<c:forEach var="tmp" items="${recipientCcList}">
 	<c:set var="recipientCC" value="${tmp}" />
 	<c:out value="${recipientCC}"/>
 	</c:forEach>
 	</display:column>
 	<display:column title="BCC" sortable="true" style="width:40px;">
 	<c:set var="recipientBccList" value="${fn:split(emailSetUpList.recipientBcc, ',')}" />
 	<c:forEach var="tmp" items="${recipientBccList}">
 	<c:set var="recipientBCC" value="${tmp}" />
 	<c:out value="${recipientBCC}"/>
 	</c:forEach>
 	</display:column>
	<display:column property="fileNumber" title="File Number" sortable="true"  maxLength="30" style="width:100px;"/>
	<display:column property="subject" title="Subject"  sortable="true" maxLength="35" style="width:200px;"/>
	<display:column title="Email&nbsp;Status"   style="width:10px;" sortable="true" sortProperty="emailStatus"  paramId="id" paramProperty="id">	
		<c:choose> 
			<c:when test="${emailSetUpList.emailStatus=='SaveForEmail'}">
					<img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			</c:when>
			<c:when test="${fn:indexOf(emailSetUpList.emailStatus,'SaveForEmail')>-1}">
					<img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetUpList.emailStatus,'SaveForEmail','')}"/>
			</c:when> 							
			<c:otherwise>
				<img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetUpList.emailStatus}"/>
			</c:otherwise>			
		</c:choose>		
	</display:column>
		<display:column title="Sent&nbsp;Date" sortable="true"   style="width:150px;">
       		<fmt:timeZone value="${sessionTimeZone}" >
				<fmt:formatDate type="both"  value="${emailSetUpList.dateSent}" pattern="yyyy-MM-dd HH:mm:ss"/>
			</fmt:timeZone>
        </display:column>
</display:table>
</s:form>
 <script type="text/javascript"> 
 function clear_fields()
 {
	  	document.forms['emailSetUpList'].elements['emailSetup.recipientTo'].value = "";
		document.forms['emailSetUpList'].elements['emailSetup.signature'].value = "";
		document.forms['emailSetUpList'].elements['emailSetup.dateSent'].value = "";
		document.forms['emailSetUpList'].elements['emailSetup.subject'].value = "";
		document.forms['emailSetUpList'].elements['emailSetup.fileNumber'].value = "";
		document.forms['emailSetUpList'].elements['emailStatusType'].value = "";
 }
</script> 
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
