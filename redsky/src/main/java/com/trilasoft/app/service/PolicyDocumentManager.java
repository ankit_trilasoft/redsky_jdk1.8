package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PolicyDocument;

public interface PolicyDocumentManager extends GenericManager<PolicyDocument,Long>  {
	public List checkPolicyId(Long fid);
	public List getMaxId();
	public int checkPolicyDocumentName(String documentName,String sectionName,String language,String partnerCode,String sessionCorpID);
}
