package com.trilasoft.app.model;

import java.util.Date;

public class CheckListResultDTO
   {
	private Long checkListId;
	private Long resultId;
	private String corpID;
	private Long duration;
	private Long toDoResultId;
	private String owner;
	private String messageDisplayed;
	private String url;
	private String shipper;
	private String resultNumber;	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String docType;
	private String ruleNumber;
	private Long id;

	public Long getCheckListId() {
		return checkListId;
	}
	public void setCheckListId(Long checkListId) {
		this.checkListId = checkListId;
	}
	public Long getResultId() {
		return resultId;
	}
	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getMessageDisplayed() {
		return messageDisplayed;
	}
	public void setMessageDisplayed(String messageDisplayed) {
		this.messageDisplayed = messageDisplayed;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getResultNumber() {
		return resultNumber;
	}
	public void setResultNumber(String resultNumber) {
		this.resultNumber = resultNumber;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getToDoResultId() {
		return toDoResultId;
	}
	public void setToDoResultId(Long toDoResultId) {
		this.toDoResultId = toDoResultId;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
   }