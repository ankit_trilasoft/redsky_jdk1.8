package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.AgentDao;
import com.trilasoft.app.model.Agent;
import com.trilasoft.app.service.AgentManager;

public class AgentManagerImpl extends GenericManagerImpl<Agent, Long> implements AgentManager { 
	AgentDao agentDao; 
    
 
    public AgentManagerImpl(AgentDao agentDao) { 
        super(agentDao); 
        this.agentDao = agentDao; 
    } 
 
    public List<Agent> findByLastName(String lastName) { 
        return agentDao.findByLastName(lastName); 
    } 
    public List checkById(Long id) { 
        return agentDao.checkById(id);
        
    }

    public List findMaximumCarrierNumber(String shipNum) {
    	return agentDao.findMaximumCarrierNumber(shipNum);
    }
  
    
}
