delimiter $$
CREATE trigger redsky.trigger_delete_history_userdatasecurity
before delete on redsky.userdatasecurity
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE oldCorpid LONGTEXT;
  DECLARE newUser LONGTEXT;
  DECLARE newRoleName LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET oldCorpid = " ";
  SET newUser = " ";
  SET newRoleName = " ";

select corpid into oldCorpid from datasecurityset where id=OLD.userdatasecurity_id limit 1;

IF (OLD.userdatasecurity_id <> '' or OLD.userdatasecurity_id is not null) THEN
    select name into newRoleName from datasecurityset where id=OLD.userdatasecurity_id;
       select CONCAT(fieldNameList,'userdatasecurity.userdatasecurity_id~') into fieldNameList;
       select CONCAT(newValueList,newRoleName,'~') into oldValueList;
   END IF;
CALL add_tblHistory (OLD.user_id,"userdatasecurity", fieldNameList, oldValueList, newValueList, OLD.updatedby, oldCorpid, now());
END $$
delimiter;