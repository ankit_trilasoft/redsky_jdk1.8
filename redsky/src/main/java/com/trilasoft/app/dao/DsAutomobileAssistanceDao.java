package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsAutomobileAssistance;

public interface DsAutomobileAssistanceDao extends GenericDao<DsAutomobileAssistance, Long>{
	
	List getListById(Long id);

}

