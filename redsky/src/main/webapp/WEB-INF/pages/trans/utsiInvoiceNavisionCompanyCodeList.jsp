<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title>Sales Invoice Extract</title>
<meta name="heading" content=" Invoice Extract" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="">
.bgblue-left {
background:url("images/blue_band.jpg") no-repeat scroll 0 0 transparent;
color:#007A8D;
font-family:Arial,Helvetica,sans-serif;
font-size:12px;
font-weight:bold;
height:30px;
padding-left:40px;
padding-top:8px;

}
</style>

<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">
function checkCompanyCode()
{  
 var companyCode = document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value; 
 var recPostingDate = document.forms['utsiInvoiceCompanyCodeForm'].elements['recPostingDate'].value; 
 companyCode=companyCode.trim();
 recPostingDate=recPostingDate.trim();   
 if(companyCode=='')
 { 
  document.forms['utsiInvoiceCompanyCodeForm'].elements['Extract'].disabled=true;
//  document.forms['utsiInvoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=true;
 }
 else if(companyCode==''|| recPostingDate=='')
 {
 // document.forms['utsiInvoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=true;
 }
if(companyCode!='')
{
document.forms['utsiInvoiceCompanyCodeForm'].elements['Extract'].disabled=false; 
}
}


function fillReverseInvoice(){ 
       var postDate = valButton(document.forms['utsiInvoiceCompanyCodeForm'].elements['radiobilling']);   
	    if (postDate == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       { 
	        postDate = postDate.replace('/','');  
	        document.forms['utsiInvoiceCompanyCodeForm'].elements['recPostingDate'].value=postDate;
	        var companyCode = document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value; 
            var recPostingDate = document.forms['utsiInvoiceCompanyCodeForm'].elements['recPostingDate'].value; 
            companyCode=companyCode.trim();
            recPostingDate=recPostingDate.trim(); 
            if(companyCode!='' && recPostingDate!='')
            { 
  	        var agree = confirm("Invoice data for company " +companyCode+" for posting date " +recPostingDate+ " is under processing.");
            if(agree)
             {
	          document.forms['utsiInvoiceCompanyCodeForm'].action = 'utsiInvoiceNavisionExtracts.html';
              document.forms['utsiInvoiceCompanyCodeForm'].submit();
             }
             else {
             return false; 
             }
             //document.forms['utsiInvoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=false; 
           }  
      }
      }
      
  function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['utsiInvoiceCompanyCodeForm'].elements['radiobilling'].value; 
    } 
  }
  
  function refreshPostDate()
 {
 document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value='';
 document.forms['utsiInvoiceCompanyCodeForm'].action = 'utsiInvoiceNavisionCompanyCodeList.html';
 document.forms['utsiInvoiceCompanyCodeForm'].submit();
 }
  function setCompanydiv(companydivision)
  {
  document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value= companydivision 
  checkCompanyCode();
  }
  
</script>
</head>
<s:form id="utsiInvoiceCompanyCodeForm" name="utsiInvoiceCompanyCodeForm" action="utsiInvoiceNavisionExtracts" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:hidden cssClass="input-textUpper" id="recPostingDate" name="recPostingDate"  />  
<div id="Layer1" style="width:90%;">
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
		<div class="bgblue-left">Sales Invoice Extract</div>
		<div>
		<table border="0" style="margin-bottom: 3px">
			<tr> <td height="1px"></td></tr>
	<tr> 
	   <%--  <td width="5px"></td>
		<td align="right"  class="listwhitebox" width="150px">Company Group Code</td> 
		<td align="left" ><s:select list="%{invCompanyCodeList}" cssClass="list-menu" cssStyle="width:120px" headerKey=" " headerValue=" " id="invoiceCompanyCode"  name="invoiceCompanyCode"  onchange="checkCompanyCode();"/></td>
		--%>
		<td width="5px"></td>
		<s:hidden  id="invoiceCompanyCode"  name="invoiceCompanyCode"  />
	    <td align="left"><input type="button"  class="cssbutton1"  value="Refresh Date List for All Companies" name="refresh"  style="width:230px;" onclick="refreshPostDate();" />
		</td>
		</tr>
		</table>
		</div>
	</div>

       <div class="bottom-header" style="margin-top:40px;"><span></span></div>
       </div>
       </div>
 

<table  border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td></td>
  </tr>
  <c:if test="${InvoicePostDateList!='[]'}"> 
  <tr>
    <td>
	<table class="detailTabLabel" cellpadding="0" cellspacing="2" ><tr>
	<td align="right" class="listwhitetext"><b>The following posting dates are available, please select one to process</b></td> 
	</tr></table>
	</td>
  </tr>
  </c:if>
  <tr>
    <td>
	<table border="0" width="100%" style="margin:0px;">
		<tr><td>
		<s:set name="InvoicePostDateList" value="InvoicePostDateList" scope="request"/> 
        <display:table name="InvoicePostDateList" class="table" requestURI="" id="InvoicePostDateList" style="width:100%" defaultsort="1" pagesize="100" >   
 		<display:column property="companyDivision" sortable="true" title="Company Division" style="width:170px" /> 
		<display:column property="recPostDate" sortable="true" title="Posting Date" format="{0,date,dd-MMM-yyyy}"  style="width:70px" />
		 <display:column property="recPostDateCount" sortable="true" title="Count" style="width:170px" />
	 <display:column   title="Select" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${InvoicePostDateList.recPostDate} value=${InvoicePostDateList.recPostDate} onclick="setCompanydiv('${InvoicePostDateList.companyDivision}');"/>
 		<s:hidden id="invoiceCompanyCode1"  name="invoiceCompanyCode1"  value="${InvoicePostDateList.companyDivision}"/>
 		</display:column>
	    
</display:table> 
</td>
</tr>
</table>
	</td>
  </tr>
  <tr>
    <td>
	<table><tbody> 
		<tr>
		
        <td align="left"><input type="button"  class="cssbutton1"  value="Extract" name="Extract"  style="width:70px;" onclick="return fillReverseInvoice();" />  
        </td>
        
      
       <!--<td align="left" ><input type="button" class="cssbutton1" name="ExtractCustomer"  style="width:150px; " onclick="location.href='<c:url value="/customerNavisionExtracts.html"/>'"  
        value="Extract Customer Data"/>
        </td>
		--></tr> 
	</tbody></table>
	</td>
  </tr>
  
</table>
</div>
</s:form>
<script type="text/javascript">  
try{
document.forms['utsiInvoiceCompanyCodeForm'].elements['radiobilling'].checked=true; 
if(document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode1'].value.trim()!='undefined'){
document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value= document.forms['utsiInvoiceCompanyCodeForm'].elements['invoiceCompanyCode1'].value;
document.forms['utsiInvoiceCompanyCodeForm'].elements['recPostingDate'].value='';
}
}
catch(e){}
try{
checkCompanyCode(); 
}
catch(e){}
</script>