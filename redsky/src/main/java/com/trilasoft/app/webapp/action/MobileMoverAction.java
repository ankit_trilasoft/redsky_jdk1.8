package com.trilasoft.app.webapp.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.OperationContext;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryLocation;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.model.WorkTicketInventoryData;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.InventoryLocationManager;
import com.trilasoft.app.service.InventoryPathManager;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.RoomManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketInventoryDataManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.mobilemover.service.SyncServiceStub;
import com.trilasoft.mobilemover.service.SyncServiceStub.Agents;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfEmail;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfImage;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfInventoryActivity;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfInventoryItem;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfInventoryLoad;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfInventoryUnload;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfItemCartonContents;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfItemDescriptiveEntry;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfLocation;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfPhone;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfRoomCondition;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfWeightTicket;
import com.trilasoft.mobilemover.service.SyncServiceStub.ArrayOfstring;
import com.trilasoft.mobilemover.service.SyncServiceStub.ConditionDamageEntry;
import com.trilasoft.mobilemover.service.SyncServiceStub.ConditionLocationEntry;
import com.trilasoft.mobilemover.service.SyncServiceStub.CustomerCareRep;
import com.trilasoft.mobilemover.service.SyncServiceStub.Dates;
import com.trilasoft.mobilemover.service.SyncServiceStub.Driver;
import com.trilasoft.mobilemover.service.SyncServiceStub.Email;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetInventoryItemImages;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetInventoryItemImagesResponse;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetLocationImages;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetLocationImagesResponse;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetOrderById;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetOrderByIdResponse;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetRoomImages;
import com.trilasoft.mobilemover.service.SyncServiceStub.GetRoomImagesResponse;
import com.trilasoft.mobilemover.service.SyncServiceStub.Image;
import com.trilasoft.mobilemover.service.SyncServiceStub.Inventory;
import com.trilasoft.mobilemover.service.SyncServiceStub.InventoryActivity;
import com.trilasoft.mobilemover.service.SyncServiceStub.InventoryItem;
import com.trilasoft.mobilemover.service.SyncServiceStub.InventoryLoad;
import com.trilasoft.mobilemover.service.SyncServiceStub.InventoryStatus;
import com.trilasoft.mobilemover.service.SyncServiceStub.InventoryUnload;
import com.trilasoft.mobilemover.service.SyncServiceStub.Item;
import com.trilasoft.mobilemover.service.SyncServiceStub.ItemCartonContents;
import com.trilasoft.mobilemover.service.SyncServiceStub.ItemConditionEntry;
import com.trilasoft.mobilemover.service.SyncServiceStub.ItemDescriptiveEntry;
import com.trilasoft.mobilemover.service.SyncServiceStub.LoadType;
import com.trilasoft.mobilemover.service.SyncServiceStub.Location;
import com.trilasoft.mobilemover.service.SyncServiceStub.LocationType;
import com.trilasoft.mobilemover.service.SyncServiceStub.Order;
import com.trilasoft.mobilemover.service.SyncServiceStub.OrderStatus;
import com.trilasoft.mobilemover.service.SyncServiceStub.Phone;
import com.trilasoft.mobilemover.service.SyncServiceStub.PhoneType;
import com.trilasoft.mobilemover.service.SyncServiceStub.PricingModes;
import com.trilasoft.mobilemover.service.SyncServiceStub.Room;
import com.trilasoft.mobilemover.service.SyncServiceStub.RoomCondition;
import com.trilasoft.mobilemover.service.SyncServiceStub.WeightTicket;
import com.trilasoft.mobilemover.service.SyncServiceStub.WeightTicketType;

public class MobileMoverAction extends BaseAction implements Preparable{

	// variable declaration 
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(MobileMoverAction.class);
	private CustomerFileManager customerFileManager;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager;
	private WorkTicketManager workTicketManager;
	private UserManager userManager;
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private BillingManager billingManager;
	private ItemsJEquipManager itemsJEquipManager ;
	private NotesManager notesManager;
	private InventoryDataManager  inventoryDataManager;
	private Long id ;
	private Long sid;
	private Long wktId;
	private String idURL;
	private String buttonType;
	private WorkTicketInventoryDataManager workTicketInventoryDataManager;
	private InventoryLocationManager inventoryLocationManager;
	private RoomManager roomManager;
	private InventoryPathManager inventoryPathManager;
	private Company company;
	private CompanyManager companyManager;
	private String sessionCorpID;
	private String mmMemberID;
	private String requestedCW;
	private String requestedWT;
	private PayrollManager payrollManager;
	private Payroll payroll;
	private String ticketNumber="";
	private ReportAction reportAction;
	private RefMasterManager refMasterManager;
	private String fileLoc ="";
	private  MyFileManager myFileManager;
	
	public MobileMoverAction(){
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal();
	this.sessionCorpID = user.getCorpID();
 	
	}
	
	public void prepare() throws Exception {
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			mmMemberID = company.getMmClientID();	
		}
	}
	
	@SkipValidation
	public String getMobMoverImg(){
		System.out.println("========================================== in getMobMoverImg =================================================");
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String getMobMoverServiceMethod() throws Exception  {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		//Populating models objects
		WorkTicket wkt = workTicketManager.get(wktId);
		logger.warn("WorkTicket Id :----->"+wktId);
		if(sid==null){
			sid = wkt.getServiceOrderId();
		}
		ServiceOrder so = serviceOrderManager.get(sid);
		CustomerFile cf = so.getCustomerFile();
		Miscellaneous misc = miscellaneousManager.get(sid);
		TrackingStatus ts = trackingStatusManager.get(sid);
		
		//Getting attached doc in workticket 
		try{
		Long StartTime = System.currentTimeMillis();
		String myFileIds = wkt.getAttachedFile();
		if(myFileIds!=null && !myFileIds.equals("")){
			if(myFileIds.contains(",")){
				String [] myFileIdArr = myFileIds.split(",");
				for (String tempId : myFileIdArr) {
					Long myDocId = Long.parseLong(tempId);
					MyFile myFile = myFileManager.get(myDocId) ;
					if(fileLoc.equals("")){
						fileLoc = myFile.getLocation();
					}else{
						fileLoc = fileLoc +","+myFile.getLocation();
					}
				}
			}else{
				Long myDocId = Long.parseLong(myFileIds);
				MyFile myFile = myFileManager.get(myDocId) ;
				if(fileLoc.equals("")){
					fileLoc = myFile.getLocation();
				}else{
					fileLoc = fileLoc +","+myFile.getLocation();
				}
			}
		}
		Long timeTaken =  System.currentTimeMillis() - StartTime;
	 logger.warn("\n\nTime taken to get Attached files location : "+timeTaken+"\n\n");
		}catch(Exception e){
			e.printStackTrace();
			
		}
		// End
		User user = userManager.getUserByUsername(cf.getCoordinator());
		logger.warn("Starting =========== Calling Service");
		SyncServiceStub syncServiceStub = new SyncServiceStub();
		logger.warn("End Point Address : "+syncServiceStub._getServiceClient().getTargetEPR());
		//EndpointReference epr = new EndpointReference("http://216.185.53.31:8080/AISyncService/SyncService.svc");
		EndpointReference epr = new EndpointReference("https://aisync1.mobilemover.com/AISyncService/SyncService.svc");
		syncServiceStub._getServiceClient().setTargetEPR(epr);
		logger.warn("End Point Address : "+syncServiceStub._getServiceClient().getTargetEPR());
		try{
			Order order = new Order();
			Agents agent = new Agents();
			CustomerCareRep customerCareRep  = new CustomerCareRep();
			Dates dates= new Dates();
			ArrayOfLocation arrLoc = new ArrayOfLocation();
			Location locO  = new Location();
			Location locD  = new Location();
			ArrayOfEmail  oArrEmail = new ArrayOfEmail();
			Email oEmail1 = new Email();
			Email oEmail2 = new Email();
			ArrayOfPhone oArrPhone = new ArrayOfPhone();
			Phone oPhn = new Phone();
			Phone oPhn1 = new Phone();
			Phone oPhn2 = new Phone();
			ArrayOfEmail  dArrEmail = new ArrayOfEmail();
			Email dEmail1 = new Email();
			Email dEmail2 = new Email();
			ArrayOfPhone ArrPhoneDestination = new ArrayOfPhone();
			Phone dPhn = new Phone();
			Phone dPhn1 = new Phone();
			Phone dPhn2 = new Phone();
			Driver driver = new Driver();
			WeightTicket weightTicket = new WeightTicket();
			ArrayOfWeightTicket  arrayOfWeightTicket = new ArrayOfWeightTicket();
			if(wkt.getOrderId()!=null && !wkt.getOrderId().equals("") && !wkt.getOrderId().equals("0") ){
				System.out.println("\n\nwork ticket order id : "+wkt.getOrderId());
				 GetOrderById getOrderById = new GetOrderById();
				 getOrderById.setOrderID(Integer.parseInt(wkt.getOrderId()));
				 GetOrderByIdResponse order1 = syncServiceStub.getOrderById(getOrderById);
				 order = order1.getGetOrderByIdResult();
				 if(order.getAgents()!=null)
					 agent = order.getAgents();
				 if(order.getCustomerCareRep()!=null)
					 customerCareRep = order.getCustomerCareRep();
				 if(order.getDates()!=null)
					 dates = order.getDates();
				
				
			}
			
			// getting agent and set the value in his property setter
			agent.setBookingAgentCode(mmMemberID);
			agent.setDestinationAgentCode(ts.getDestinationAgentCode());
			agent.setHaulingAgentCode(misc.getHaulingAgentCode());
			agent.setOriginAgentCode(ts.getOriginAgentCode());
			
			// getting CustomerCareRep  and set the value in his property setter
			
			//CustomerCareRep customerCareRep  = new CustomerCareRep();
			customerCareRep.setDirectDial(user.getPhoneNumber());
			customerCareRep.setEmailAddress(user.getEmail());
			customerCareRep.setFax(user.getFaxNumber());
			customerCareRep.setRepName(cf.getCoordinator() == null ? "" : cf.getCoordinator());
			customerCareRep.setUserIdPrimary(user.getUsername());
		
			
			//getting Dates class and setting value in his property setter
			// Dates dates= new Dates();
			if(wkt.getService().equals("DL")){
				dates.setDeliverFrom(DateToCalendar(wkt.getDate1()));
				dates.setDeliverTo(DateToCalendar(wkt.getDate2()));
				dates.setActualDelivery(DateToCalendar(wkt.getDate2()));
					
			}else if(wkt.getService().equals("PK")){
				dates.setPackFrom(DateToCalendar(wkt.getDate1()));
				dates.setPackTo(DateToCalendar(wkt.getDate2()));
			}else if (wkt.getService().equals("PAD")){
				dates.setDeliverFrom(DateToCalendar(wkt.getDate1()));
				dates.setDeliverTo(DateToCalendar(wkt.getDate2()));
				dates.setActualDelivery(DateToCalendar(wkt.getDate2()));
				dates.setPackFrom(DateToCalendar(wkt.getDate1()));
				dates.setPackTo(DateToCalendar(wkt.getDate2()));
			}else if(wkt.getService().equals("LOD")){
				dates.setLoadFrom(DateToCalendar(wkt.getDate1()));
				dates.setLoadTo(DateToCalendar(wkt.getDate2()));
			}else if(wkt.getService().equals("PD")){
				dates.setActualPickup(DateToCalendar(wkt.getDate2()));
			}
			dates.setRegistered(DateToCalendar(misc.getShipmentRegistrationDate()));
			dates.setRequestedDelivery(DateToCalendar(ts.getRequiredDeliveryDate()));
			dates.setSurvey(DateToCalendar(cf.getSurvey()));
			dates.setWillAdvise(false);
			
			
			// Email array 
			
			//Email oEmail1 = new Email();
			oEmail1.setEmailAddress(cf.getEmail());
			oEmail1.setEmailID(1);
			oEmail1.setIsPrimary(true);
			oEmail1.setNote("CF Mail ID");
			
			//Email oEmail2 = new Email();
			oEmail2.setEmailAddress(cf.getEmail2());
			oEmail2.setEmailID(2);
			oEmail2.setIsPrimary(false);
			oEmail2.setNote("CF 2nd Mail  ID");
			
			
			Email emailArrayOfOrigin[] = {oEmail1,oEmail2};
			
			
			// ArrayOfEmail  oArrEmail = new ArrayOfEmail();
			oArrEmail.setEmail(emailArrayOfOrigin);
			
			
			//Phone array
			
			
			
			//Phone oPhn = new Phone();
			oPhn.setNumber(wkt.getOriginContactPhone());
			oPhn.setPhoneID(1);
			oPhn.setPhoneType(PhoneType.Home);
			
			
			//Phone oPhn1 = new Phone();
			oPhn1.setNumber(wkt.getOriginMobile());
			oPhn1.setPhoneID(2);
			oPhn1.setPhoneType(PhoneType.Mobile);
			
			//Phone oPhn2 = new Phone();
			oPhn2.setNumber(wkt.getOriginContactWeekend());
			oPhn2.setPhoneID(3);
			oPhn2.setPhoneType(PhoneType.Work);
			
			
			Phone phoneArrayOrigin [] = {oPhn,oPhn1,oPhn2} ;
			
			//ArrayOfPhone oArrPhone = new ArrayOfPhone();
			oArrPhone.setPhone(phoneArrayOrigin);
			// setting location Origin and destination
			
			//Location locO  = new Location();
			locO.setAddress1(wkt.getAddress1());
			locO.setAddress2(wkt.getAddress2());
			locO.setCity(wkt.getCity());
			locO.setCountry(wkt.getOriginCountryCode());
			locO.setFirstName(wkt.getFirstName()== null ? "" : wkt.getFirstName());
			locO.setLastName(wkt.getLastName()== null ? "" : wkt.getLastName());
			locO.setState(wkt.getState());
			locO.setZip(wkt.getZip());
			locO.setEmails(oArrEmail);
			locO.setPhones(oArrPhone);
			locO.setLocationType(LocationType.Origin);
			
			
			//Email dEmail1 = new Email();
			dEmail1.setEmailAddress(cf.getDestinationEmail());
			dEmail1.setEmailID(1);
			dEmail1.setIsPrimary(true);
			dEmail1.setNote("CF Mail ID");
			
			//Email dEmail2 = new Email();
			dEmail2.setEmailAddress(cf.getDestinationEmail2());
			dEmail2.setEmailID(2);
			dEmail2.setIsPrimary(false);
			dEmail2.setNote("CF 2nd Mail  ID");
			
			
			Email emailArrayOfDest[] = {dEmail1,dEmail2};
			
			
			//ArrayOfEmail  dArrEmail = new ArrayOfEmail();
			dArrEmail.setEmail(emailArrayOfDest);
			
			
//Phone array
			
			
			//Phone dPhn = new Phone();
			dPhn.setNumber(wkt.getContactFax());
			dPhn.setPhoneID(1);
			dPhn.setPhoneType(PhoneType.Home);
			
		//	Phone dPhn1 = new Phone();
			dPhn1.setNumber(wkt.getDestinationMobile());
			dPhn1.setPhoneID(2);
			dPhn1.setPhoneType(PhoneType.Mobile);
			
		//	Phone dPhn2 = new Phone();
			dPhn2.setNumber(wkt.getContactPhone());
			dPhn2.setPhoneID(3);
			dPhn2.setPhoneType(PhoneType.Work);
			
			
			Phone dArrPhone [] = {dPhn,dPhn1,dPhn2} ;
			
			//ArrayOfPhone ArrPhoneDestination = new ArrayOfPhone();
			ArrPhoneDestination.setPhone(dArrPhone);
			
			
			//Location locD  = new Location();
			locD.setAddress1(wkt.getDestinationAddress1());
			locD.setAddress2(wkt.getDestinationAddress2());
			locD.setCity(wkt.getDestinationCity());
			locD.setCountry(wkt.getDestinationCountryCode());
			locD.setFirstName(wkt.getFirstName()== null ? "" : wkt.getFirstName());
			locD.setLastName(wkt.getLastName()== null ? "" : wkt.getLastName());
			locD.setState(wkt.getDestinationState());
			locD.setZip(wkt.getDestinationZip());
			locD.setEmails(dArrEmail);
			locD.setPhones(ArrPhoneDestination);
			locD.setLocationType(LocationType.Destination);
			
			// setting location Origin and destination Array
			Location  locArray [] = {locO,locD};
			
			//ArrayOfLocation arrLoc = new ArrayOfLocation();
			arrLoc.setLocation(locArray);
			
			
			
			
			
			// setting Weight Ticket 
			weightTicket.setDescription("Ticket Actual Weight");
			try{
			if(wkt.getActualWeight()!=null){
				weightTicket.setGrossWeight(wkt.getActualWeight().intValue());
			}
			}catch (Exception e) {
				e.printStackTrace();
				
			}
			weightTicket.setTicketDate(DateToCalendar(wkt.getCreatedOn()));
			weightTicket.setWeightType(WeightTicketType.Gross);
			
			
			WeightTicket [] weightTicketArr = {weightTicket};
			
			arrayOfWeightTicket.setWeightTicket(weightTicketArr);
			
			
			// getting order and set the value in his setter
			
			order.setAccountNumber(123);
			
			order.setAgents(agent);
			order.setCustomerCareRep(customerCareRep);
			order.setDates(dates);
			order.setPricingMode(PricingModes.Local);
			order.setArchived(true);
			order.setBillAsWeight(123);
			order.setCarrierID(150);
			order.setComments(wkt.getInstructions());
			order.setGBLNumber(ts.getGbl());
			order.setAuthorityNumber("RedSky1");
			order.setRegistrationNumber(wkt.getTicket().toString());
			order.setLocations(arrLoc);
			order.setWeightTickets(arrayOfWeightTicket);
			try {
				order.setEstimatedWeight(wkt.getEstimatedWeight().intValue());
				order.setBillAsWeight(wkt.getActualWeight().intValue());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(wkt.getActualWeight()!=null && !wkt.getActualWeight().toString().equals("")){
				order.setActualWeight(wkt.getActualWeight().intValue());
				order.setWeightActualGross(wkt.getActualWeight().intValue());
				order.setWeightActualNet(wkt.getActualWeight().intValue());
				order.setWeightActualTare(wkt.getActualWeight().intValue());
			}else{
				order.setActualWeight(misc.getActualGrossWeight()!=null ? misc.getActualGrossWeight().intValue() : 0);
			}
			
			order.setOrderNumber(wkt.getTicket().toString());
			if(so.getStatus().equals("CNCL"))
				order.setOrderStatus(OrderStatus.Cancelled);
			else if(so.getStatus().equals("DLVR"))
				order.setOrderStatus(OrderStatus.Delivered);
			else if(so.getStatus().equals("NEW"))
				order.setOrderStatus(OrderStatus.Estimated);
			
			if(buttonType!=null && buttonType.equalsIgnoreCase("MMInv")){
				
				// InventoryActivity
				InventoryActivity inAct =  new InventoryActivity();
				inAct.setActivityDate(DateToCalendar(new Date()));
				inAct.setAgencyCode(wkt.getAccount());
				inAct.setCompanyName(wkt.getCompanyDivision());
				inAct.setDriverNumber("");
				inAct.setStatus(InventoryStatus.Warehouse);
				inAct.setWarehouse(wkt.getWarehouse());
				
				InventoryActivity invActivity[] ={inAct};
				
				
				ArrayOfInventoryActivity invActivityArray = new ArrayOfInventoryActivity();
				invActivityArray.setInventoryActivity(invActivity);
				
				//Inventory Items setting
				List <InventoryData>InventoryList = inventoryDataManager.getInventoryListForSO(so.getId(), cf.getId());
				InventoryItem inItem[] = new InventoryItem[InventoryList.size()];
				int count=0;
				for (InventoryData inventoryData : InventoryList) {
					InventoryItem  invItem = new InventoryItem();
					invItem.setComments(inventoryData.getComment());
					
					// setting item
					Item itm  = new Item();
					itm.setItemName(inventoryData.getAtricle());
					
					invItem.setItem(itm);
					invItem.setBarcode(inventoryData.getBarCode());
					try{
						invItem.setQuantity(Integer.parseInt(inventoryData.getQuantity()));
					}catch(Exception e){
						e.printStackTrace();
						logger.warn("got an error in setting quantity. "+e.getStackTrace()[0]);
						invItem.setQuantity(1);
					}
					// setting room
					Room rm = new Room();
					rm.setRoomName(inventoryData.getRoom());
					
					invItem.setRoom(rm);
					inItem[count] = invItem;
					count++;
				}
				ArrayOfInventoryItem invItemArr = new ArrayOfInventoryItem();
				invItemArr.setInventoryItem(inItem);
				
				Inventory invOrder =  new Inventory();
				invOrder.setActivity(invActivityArray);
				invOrder.setItems(invItemArr);
				invOrder.setLoadType(LoadType.International);
				ArrayOfInventoryLoad arrInvLoad = new  ArrayOfInventoryLoad();
				invOrder.setLoads(arrInvLoad);
				invOrder.setStatus(InventoryStatus.Warehouse);
				ArrayOfInventoryUnload arrInvUnload = new ArrayOfInventoryUnload();
				invOrder.setUnloads(arrInvUnload);
				order.setInventory(invOrder);
			}
			// Sending for schedule resources 
			String driverName = "Driver23";
			if(payroll!=null){
				//driver.setFirstName(payroll.getFirstName());
				//driver.setLastName(payroll.getLastName());
				//driver.setHomePhone(payroll.getOfficePhone());
				//driver.setType(payroll.getTypeofWork());
				//order.setDriver(driver);
				driverName = payroll.getUserName();
			}
			
			com.trilasoft.mobilemover.service.SyncServiceStub.SaveOrder saveOrder = new com.trilasoft.mobilemover.service.SyncServiceStub.SaveOrder();
			saveOrder.setOrder(order);
			saveOrder.setAgentCode(wkt.getVendorCode());
			saveOrder.setDriverNumber(driverName);
			saveOrder.setCompany(wkt.getCompanyDivision());
			saveOrder.setWarehouse(wkt.getWarehouse());
			
			
			Long StartTime = System.currentTimeMillis();
			com.trilasoft.mobilemover.service.SyncServiceStub.SaveOrderResponse saveOrderResponse = syncServiceStub.saveOrder(saveOrder);
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken to Save order on sync server : "+timeTaken+"\n\n");	
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n Order Save Successfully  and response is : "+saveOrderResponse);
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getAuthorityNumber());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getCustomerID());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getGBLNumber());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getOrderNumber());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getComments());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+saveOrderResponse.getSaveOrderResult().getRegistrationNumber());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\norder id "+saveOrderResponse.getSaveOrderResult().getOrderID());
			// Creating pdf for work ticket
			try {
				 StartTime = System.currentTimeMillis();
				reportAction.setJasperName("WorkTicketByTicket.jrxml");
				reportAction.setTicket(saveOrderResponse.getSaveOrderResult().getRegistrationNumber());
				reportAction.setCompanyManager(companyManager);
				reportAction.setCompany(company);
				String tempLoc = reportAction.saveWorkTicketMobileMoverPDF();
				if(fileLoc.equals("")){
					fileLoc = tempLoc;
				}else{
					fileLoc = fileLoc +","+tempLoc;
				}
				 timeTaken =  System.currentTimeMillis() - StartTime;
				logger.warn("\n\nTime taken to Creating work ticket report : "+timeTaken+"\n\n");
				System.out.println(tempLoc);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// End
			if(ticketNumber!=null && !ticketNumber.equals("")){
				ticketNumber = ticketNumber+", "+saveOrderResponse.getSaveOrderResult().getRegistrationNumber();
			}else{
				ticketNumber = saveOrderResponse.getSaveOrderResult().getRegistrationNumber();
			}
			Integer temp = new Integer(saveOrderResponse.getSaveOrderResult().getOrderID());
			if(wkt.getOrderId()==null || wkt.getOrderId().equals("")  ){
				wkt.setOrderId(temp.toString());
				wkt = workTicketManager.save(wkt);
			}
			saveMessage(getText("Ticket has been Sent to Mobile Mover."));
		}catch (Exception e) {
			errorMessage(getText("Error in sending Ticket to Mobile Mover."));
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n Error in Save Order : "+e.getMessage());
			logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			e.printStackTrace();
		}finally{
			try {
				OperationContext operationContext = syncServiceStub
						._getServiceClient().getLastOperationContext();
				System.out.println("Printing Messages");
				if (operationContext != null) {
					MessageContext outMessageContext = operationContext
							.getMessageContext("Out");
					if (outMessageContext != null) {
						System.out.println("OUT SOAP: "
								+ outMessageContext.getEnvelope().toString());
					}
					MessageContext inMessageContext = operationContext
							.getMessageContext("In");
					if (inMessageContext != null) {
						System.out.println("IN SOAP: "
								+ inMessageContext.getEnvelope().toString());
					}
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
        }
  

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("End =========== Calling Service");	
		
		idURL = "?id="+wktId;
		return SUCCESS;
	}
	
	@SkipValidation
	public String sendToMobileMoverFromResources(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("Start =========== send to Mobile mover from resources");
		try {
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				mmMemberID = company.getMmClientID();	
			}
			 Long StartTime = System.currentTimeMillis();
			Map <String, String> mmDeviceMap = refMasterManager.getFlex1MapByParameter(sessionCorpID, "MM-DEVICE");
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			 logger.warn("\n\nTime taken to getting MM-Device Map from refmaster : "+timeTaken+"\n\n");
			System.out.println("\n\n\n\n\nwork ticket list:"+requestedWT+"\n\n\n\n\n crew list :"+requestedCW);
			String [] crewList = requestedCW.split(",");
			for (String crewId : crewList) {
				Payroll crewLocal = payrollManager.get(Long.parseLong(crewId));
				if(crewLocal.getIntegrationTool()!=null && !crewLocal.getIntegrationTool().equals("") && crewLocal.getIntegrationTool().equals("MM") ){
					this.setPayroll(crewLocal); 
					break;
				}
			}
			String [] workticketList = requestedWT.split(",");
			for (String wrkTcktId : workticketList) {
				this.setWktId(Long.parseLong(wrkTcktId));
				getMobMoverServiceMethod();
			}
			if(ticketNumber!=null && !ticketNumber.equals("") && payroll!=null ){
				StartTime = System.currentTimeMillis();
				String to  = payroll.getDeviceNumber();
				String bcc = "";
				String cc = "";
				String location = "";
				if(!fileLoc.equals("")){
					location = fileLoc;
				}
				String subject = "Mobile mover order";
				String body ="";
				if(ticketNumber.contains(",")){
					body = "Orders " + ticketNumber +" have been  submitted to mobile mover. Please download.";
				}else{
					body = "Order " + ticketNumber +" has been  submitted to mobile mover. Please download.";
				}
				
				if(to!=null && !to.equals("")){
					logger.warn("Hi visitor, sending mail to "+to+". Please wait........");
					EmailSetUpUtil.sendMail(to, bcc, cc, subject, body, location, "support@redskymobility.com");
					logger.warn("Mail send to "+to);
					saveMessage(getText("Sent dispatch email to Mobile Mover device -  "+mmDeviceMap.get(to)+"("+to+")."));
				}
				 timeTaken =  System.currentTimeMillis() - StartTime;
				 logger.warn("\n\nTime taken to sending MM mail  : "+timeTaken+"\n\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		logger.warn("End =========== send to Mobile mover from resources");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		return SUCCESS;
	}
	

	
	@SkipValidation
	public String updateRedSkyByMobileMover() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("Start =========== Mobile mover sync");
		 SyncServiceStub syncServiceStub = new SyncServiceStub();
		try{
		WorkTicket wtObj =  workTicketManager.get(wktId);
		ServiceOrder soObj = serviceOrderManager.get(sid);
		CustomerFile cfObj =  soObj.getCustomerFile();
		  
	
			if(wtObj.getOrderId()!=null && !wtObj.getOrderId().equals("") && !wtObj.getOrderId().equals("0") ){
					System.out.println("\n\nwork ticket order id : "+wtObj.getOrderId());
					Agents agent = new Agents();
					CustomerCareRep customerCareRep  = new CustomerCareRep();
					Dates dates= new Dates();
					ArrayOfLocation arrLoc = new ArrayOfLocation();
					Location locO  = new Location();
					Location locD  = new Location();
					ArrayOfEmail  oArrEmail = new ArrayOfEmail();
					Email oEmail1 = new Email();
					Email oEmail2 = new Email();
					ArrayOfPhone oArrPhone = new ArrayOfPhone();
					Phone oPhn = new Phone();
					Phone oPhn1 = new Phone();
					Phone oPhn2 = new Phone();
					ArrayOfEmail  dArrEmail = new ArrayOfEmail();
					Email dEmail1 = new Email();
					Email dEmail2 = new Email();
					ArrayOfPhone ArrPhoneDestination = new ArrayOfPhone();
					Phone dPhn = new Phone();
					Phone dPhn1 = new Phone();
					Phone dPhn2 = new Phone();
					WeightTicket weightTicket = new WeightTicket();
					ArrayOfWeightTicket  arrayOfWeightTicket = new ArrayOfWeightTicket();
					GetOrderById getOrderById = new GetOrderById();
					getOrderById.setOrderID(Integer.parseInt(wtObj.getOrderId()));
					Long StartTime = System.currentTimeMillis();
					GetOrderByIdResponse order1 = syncServiceStub.getOrderById(getOrderById);
					Long timeTaken =  System.currentTimeMillis() - StartTime;
					logger.warn("\n\nTime taken to getting order by sync server : "+timeTaken+"\n\n");
					Order order = order1.getGetOrderByIdResult();
					order.getAccountNumber();
					order.getActualWeight();
					System.out.println("\n\n\n\n\n\norder  order.getAccountNumber()"+ order.getAccountNumber()); 
					System.out.println("\n\n\n\n\n\norder order.getActualWeight()"+order.getActualWeight());
					System.out.println("\n\n\n\n\n\norder order.getOrderNumber()"+order.getOrderNumber());
					System.out.println("\n\n\n\n\n\norder order.getComments()"+order.getComments());
					System.out.println("\n\n\n\n\n\norder order.getCustomerID()"+order.getCustomerID());

					System.out.println("\n\nwork ticket order id : "+wtObj.getOrderId());
					 agent = order.getAgents();
					 customerCareRep = order.getCustomerCareRep();
					 dates = order.getDates();
					 arrLoc = order.getLocations();
					 HashMap <String,Long> locationMap = new HashMap<String, Long>();
					 Location loc[] = arrLoc.getLocation();
					  for (Location location : loc) {
						  StartTime = System.currentTimeMillis();
						  InventoryLocation invLoc = new InventoryLocation();
						  List tempObjLoc  = inventoryLocationManager.checkInventoryLocation(wtObj.getCorpID(),location.getLocationType().toString(),wtObj.getTicket().toString()); 
						  if(tempObjLoc!=null && !tempObjLoc.isEmpty() && tempObjLoc.get(0)!=null){
							  invLoc = (InventoryLocation) tempObjLoc.get(0);
						  }else{
							  invLoc.setCorpId(wtObj.getCorpID());
							  invLoc.setCreatedOn(new Date());
							  invLoc.setCreatedBy(getRequest().getRemoteUser()); 
						  }
						  
						  invLoc.setWorkTicket(wtObj.getTicket().toString());
						  invLoc.setShipNumber(soObj.getShipNumber());
						  invLoc.setSequenceNumber(cfObj.getSequenceNumber());
						  invLoc.setUpdatedOn(new Date());
						  invLoc.setUpdatedBy(getRequest().getRemoteUser());
						  invLoc.setLocationType(location.getLocationType().toString());
						  invLoc = inventoryLocationManager.save(invLoc);
						  locationMap.put(location.getLocationType().toString(), invLoc.getId());
						  timeTaken =  System.currentTimeMillis() - StartTime;
						  logger.warn("\n\nTime taken to get and save location : "+timeTaken+"of type "+location.getLocationType().toString()+"\n\n");
						  
						  StartTime = System.currentTimeMillis();
						  inventoryPathManager.deleteAllWorkTicketInventory("locationId",wtObj.getCorpID(),invLoc.getId());
						  timeTaken =  System.currentTimeMillis() - StartTime;
						  logger.warn("\n\nTime taken to delete location images from inventory path: "+timeTaken+"\n\n");
						  
						  GetLocationImages getLocationImages = new GetLocationImages();
						  getLocationImages.setLocationID(location.getLocationID());
						  GetLocationImagesResponse locImage  = syncServiceStub.getLocationImages(getLocationImages);
						   Image[] locImgArr = locImage.getGetLocationImagesResult().getImage();
						  if(locImgArr!=null){
							  StartTime = System.currentTimeMillis();
							  for (Image image : locImgArr) {
								  DataHandler  img = image.getImageData();
								  ByteArrayOutputStream output = new ByteArrayOutputStream();
								  img.writeTo(output);
								  BufferedImage imag=ImageIO.read(new ByteArrayInputStream(output.toByteArray()));
								  String path = "/usr/local/redskydoc/MobileMover/MM-"+image.getImageID()+".jpg";
								  ImageIO.write(imag, "jpg", new File("/usr/local/redskydoc/MobileMover","MM-"+image.getImageID()+".jpg"));
								  InventoryPath invPath =  new InventoryPath();
								  invPath.setCorpID(wtObj.getCorpID());
								  invPath.setPath(path);
								  invPath.setLocationId(invLoc.getId());
								  inventoryPathManager.save(invPath);
							}
							  timeTaken =  System.currentTimeMillis() - StartTime;
							  logger.warn("\n\nTime taken to get and save location images : "+timeTaken+"of type "+location.getLocationType().toString()+"\n\n");
						}  
						  
					}
					 Inventory invOrder = order.getInventory();
					 if(invOrder!=null){
						 StartTime = System.currentTimeMillis();
					InventoryLoad[] invLoadArr=invOrder.getLoads().getInventoryLoad();
					HashMap<String, String> roomConditionMap = new HashMap<String, String>();
					HashMap<String, String> itemConditionMap = new HashMap<String, String>();
					HashMap<String, String> itemUnloadConditionMap = new HashMap<String, String>();
					HashMap<String, String> deliveredItemMap = new HashMap<String, String>();
					HashMap<String, String> locationConditionMap = new HashMap<String, String>();
					HashMap<String, String> conditionMap = new HashMap<String, String>();
					HashMap<String, String> inventoryConditionMap = new HashMap<String, String>();
					HashMap<String, String> itemConditionUnloadMap = new HashMap<String, String>();
					HashMap<String, String> conditionUnloadMap = new HashMap<String, String>();
					HashMap<String, String> locationConditionUnloadMap = new HashMap<String, String>();
					
					
					try {
						if(invLoadArr!=null){
						for (InventoryLoad inventoryLoad : invLoadArr) {
							ArrayOfRoomCondition roomConditionArr = inventoryLoad.getRoomConditions();
							RoomCondition[] roomConditionArr1 = roomConditionArr.getRoomCondition() ;
							if(roomConditionArr1 != null){
							for (RoomCondition roomCondition : roomConditionArr1) {
								String tempRoomVal1 =   roomCondition.getHasDamage()?"true":"false" ;
								String tempRoomVal2 =	roomCondition.getFloorType()==null?"NA":roomCondition.getFloorType().toString();
								String tempRoomVal3 =	roomCondition.getDamageDetail()==null?"NA":roomCondition.getDamageDetail().toString();
								String tempRoomVal = tempRoomVal1+"~"+tempRoomVal2+"~"+tempRoomVal3+"~"+roomCondition.getRoomConditionID();;
								roomConditionMap.put(""+roomCondition.getRoom().getRoomCode(),tempRoomVal);
							}
						}
							ItemConditionEntry[] ArrayOfItemConditionEntry = inventoryLoad.getItemConditions().getItemConditionEntry();
							if(ArrayOfItemConditionEntry!=null){
							for (ItemConditionEntry itemConditionEntry : ArrayOfItemConditionEntry) {
								ConditionDamageEntry[] tempItemCondition  = itemConditionEntry.getDamages().getConditionDamageEntry();
								for (ConditionDamageEntry conditionDamageEntry : tempItemCondition) {
									if(itemConditionMap.containsKey(""+itemConditionEntry.getConditionEntryID())){
										String tempCond = itemConditionMap.get(""+itemConditionEntry.getConditionEntryID());
										tempCond  = tempCond+" "+conditionDamageEntry.getDescription();
										itemConditionMap.put(""+itemConditionEntry.getConditionEntryID(), tempCond);
		    	    		 		}else{
		    	    		 			itemConditionMap.put(""+itemConditionEntry.getConditionEntryID(), conditionDamageEntry.getDescription());
		    	    		 		}
									conditionMap.put(""+itemConditionEntry.getConditionEntryID(),""+itemConditionEntry.getItemBarcode());
								}
								ConditionLocationEntry[] locationConditionArr = itemConditionEntry.getLocations().getConditionLocationEntry();
								for (ConditionLocationEntry conditionLocationEntry : locationConditionArr) {
									if(locationConditionMap.containsKey(""+itemConditionEntry.getConditionEntryID())){
										String tempCond = locationConditionMap.get(""+itemConditionEntry.getConditionEntryID());
										if(tempCond!=null){
											tempCond  = tempCond+"-"+conditionLocationEntry.getDescription();
										}else{
											tempCond  = conditionLocationEntry.getDescription();
										}
										
										locationConditionMap.put(""+itemConditionEntry.getConditionEntryID(), tempCond);
		    	    		 		}else{
		    	    		 			locationConditionMap.put(""+itemConditionEntry.getConditionEntryID(), (conditionLocationEntry.getDescription()!= null ?conditionLocationEntry.getDescription():""));
		    	    		 		}
									conditionMap.put(""+itemConditionEntry.getConditionEntryID(),""+itemConditionEntry.getItemBarcode());
								}
							}
						}
							
						}
					}
						for (String key : conditionMap.keySet()) {
							if(inventoryConditionMap.containsKey(conditionMap.get(key))){
								String tempCond = inventoryConditionMap.get(conditionMap.get(key));
								if(locationConditionMap.get(key)!=null){
									if(itemConditionMap.get(key)!=null){
										tempCond  = tempCond+",<br/>"+locationConditionMap.get(key)+" "+itemConditionMap.get(key);
									}
								}else{
									if(itemConditionMap.get(key)!=null){
										tempCond  = tempCond+",<br/>"+itemConditionMap.get(key);
									}
								}
								inventoryConditionMap.put(conditionMap.get(key), tempCond);
    	    		 		}else{
    	    		 			if(locationConditionMap.get(key)!=null){
    	    		 				if(itemConditionMap.get(key)!=null){
    	    		 					inventoryConditionMap.put(conditionMap.get(key), locationConditionMap.get(key)+" "+itemConditionMap.get(key));
    	    		 				}
    	    		 			}else{
    	    		 				if(itemConditionMap.get(key)!=null){
    	    		 					inventoryConditionMap.put(conditionMap.get(key), itemConditionMap.get(key));
    	    		 				}
    	    		 			}
    	    		 		}
						}
						
						
						InventoryUnload[] invUnloadArr = invOrder.getUnloads().getInventoryUnload();
						if(invUnloadArr != null ){
						for (InventoryUnload inventoryUnload : invUnloadArr) {
							if(inventoryUnload.getItemBarcodes()!=null){
								ArrayOfstring deliveredItemsArray = inventoryUnload.getItemBarcodes();
								String [] tempDeliverList = deliveredItemsArray.getString();
								for (String temp : tempDeliverList) {
									deliveredItemMap.put(temp, "Y");
								}
							}
							ItemConditionEntry[] ArrayOfItemConditionEntry = inventoryUnload.getItemConditions().getItemConditionEntry();
							if(ArrayOfItemConditionEntry!=null){
							for (ItemConditionEntry itemConditionEntry : ArrayOfItemConditionEntry) {
								ConditionDamageEntry[] tempItemCondition  = itemConditionEntry.getDamages().getConditionDamageEntry();
								for (ConditionDamageEntry conditionDamageEntry : tempItemCondition) {
									if(itemConditionUnloadMap.containsKey(""+itemConditionEntry.getConditionEntryID())){
										String tempCond = itemConditionUnloadMap.get(""+itemConditionEntry.getConditionEntryID());
										tempCond  = tempCond+" "+conditionDamageEntry.getDescription();
										itemConditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(), tempCond);
		    	    		 		}else{
		    	    		 			itemConditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(), conditionDamageEntry.getDescription());
		    	    		 		}
									conditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(),""+itemConditionEntry.getItemBarcode());
								}
								ConditionLocationEntry[] locationConditionArr = itemConditionEntry.getLocations().getConditionLocationEntry();
								for (ConditionLocationEntry conditionLocationEntry : locationConditionArr) {
									if(locationConditionUnloadMap.containsKey(""+itemConditionEntry.getConditionEntryID())){
										String tempCond = locationConditionUnloadMap.get(""+itemConditionEntry.getConditionEntryID());
										if(tempCond!=null){
											tempCond  = tempCond+"-"+conditionLocationEntry.getDescription();
										}else{
											tempCond  = conditionLocationEntry.getDescription();
										}
										
										locationConditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(), tempCond);
		    	    		 		}else{
		    	    		 			locationConditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(), (conditionLocationEntry.getDescription()!= null ?conditionLocationEntry.getDescription():""));
		    	    		 		}
									conditionUnloadMap.put(""+itemConditionEntry.getConditionEntryID(),""+itemConditionEntry.getItemBarcode());
								}
							}
							}
							
							for (String key : conditionUnloadMap.keySet()) {
								if(itemUnloadConditionMap.containsKey(conditionUnloadMap.get(key))){
									String tempCond = itemUnloadConditionMap.get(conditionUnloadMap.get(key));
									if(locationConditionUnloadMap.get(key)!=null){
										if(itemConditionUnloadMap.get(key)!=null){
											tempCond  = tempCond+",<br/>"+locationConditionUnloadMap.get(key)+" "+itemConditionUnloadMap.get(key);
										}
									}else{
										if(itemConditionUnloadMap.get(key)!=null){
											tempCond  = tempCond+",<br/>"+itemConditionUnloadMap.get(key);
										}
									}
									itemUnloadConditionMap.put(conditionUnloadMap.get(key), tempCond);
	    	    		 		}else{
	    	    		 			if(locationConditionUnloadMap.get(key)!=null){
	    	    		 				if(itemConditionUnloadMap.get(key)!=null){
	    	    		 					itemUnloadConditionMap.put(conditionUnloadMap.get(key), locationConditionUnloadMap.get(key)+" "+itemConditionUnloadMap.get(key));
	    	    		 				}
	    	    		 			}else{
	    	    		 				if(itemConditionUnloadMap.get(key)!=null){
	    	    		 					itemUnloadConditionMap.put(conditionUnloadMap.get(key), itemConditionUnloadMap.get(key));
	    	    		 				}
	    	    		 			}
	    	    		 		}
							}
						}
					}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			
					 ArrayOfInventoryItem arrItem = invOrder.getItems();
					 InventoryItem[] items = arrItem.getInventoryItem();
					 
					 if(items != null){
					 for (InventoryItem inventoryItem : items) {
						 logger.warn("\nStarts entry for inventory No. :"+inventoryItem.getBarcode());
						 String cartonContent  = "";
						 ArrayOfItemCartonContents tempCartonContain = inventoryItem.getCartonContents();
						 if(tempCartonContain!=null){
							 ItemCartonContents[] tempCartonContainArr  = tempCartonContain.getItemCartonContents();
								 if(tempCartonContainArr!=null){
									 for (ItemCartonContents itemCartonContents : tempCartonContainArr) {
										 if(cartonContent.equals("")){
											 cartonContent = itemCartonContents.getDescription();
										 }else{
											 cartonContent = cartonContent+", "+itemCartonContents.getDescription();
										 }
								}
							}
						 }
						 
						 String packingCode = "";
						 Item item = inventoryItem.getItem();
						boolean cpCheck = item.getIsCP();
						if(cpCheck){
							packingCode="CP";
						}
						 ArrayOfItemDescriptiveEntry descriptionEntry = inventoryItem.getDescriptions();
						 if(descriptionEntry!=null){
							 ItemDescriptiveEntry[] itemDescriptiveEntry  = descriptionEntry.getItemDescriptiveEntry();
							 if(itemDescriptiveEntry!=null){
								 for (ItemDescriptiveEntry itemDescriptiveEntryObj : itemDescriptiveEntry) {
									 if(packingCode.equals("")){
										 packingCode = itemDescriptiveEntryObj.getCode();
									 }else{
										 packingCode = packingCode+", "+itemDescriptiveEntryObj.getCode();
									 }
								}
							 }
						 }
							 
							 
						 String tempInvetoryUnloadData = "";
						WorkTicketInventoryData wktInv = new WorkTicketInventoryData();
						
						
						List tempObjInv  = workTicketInventoryDataManager.checkInventoryData(wtObj.getCorpID(),inventoryItem.getBarcode(),soObj.getShipNumber());
						if(tempObjInv!=null && !tempObjInv.isEmpty() && tempObjInv.get(0)!=null){
							wktInv = (WorkTicketInventoryData) tempObjInv.get(0);
						  }else{
							  wktInv.setCorpId(wtObj.getCorpID());
							  wktInv.setCreatedOn(new Date());
							  wktInv.setCreatedBy(getRequest().getRemoteUser());
						 }
						if(deliveredItemMap!= null){
							tempInvetoryUnloadData = deliveredItemMap.get(""+inventoryItem.getBarcode());
						}
						if(tempInvetoryUnloadData!=null && tempInvetoryUnloadData.equals("Y")){
							wktInv.setDelivered("Y");
						}else{
							wktInv.setDelivered("N");
						}
						
						wktInv.setPackingCode(packingCode);
						wktInv.setCartonContent(cartonContent);
						wktInv.setItem(""+item.getItemCode());
						wktInv.setItemDesc(item.getItemName());
						wktInv.setBarCode(inventoryItem.getBarcode());
						wktInv.setQuantity(inventoryItem.getQuantity());
						wktInv.setVolume(new BigDecimal("0.00"));
						wktInv.setWeight(new BigDecimal("0.00"));
						wktInv.setUpdatedOn(new Date());
						wktInv.setUpdatedBy(getRequest().getRemoteUser());
						Room orderRoom = inventoryItem.getRoom();
						com.trilasoft.app.model.Room  room = new com.trilasoft.app.model.Room();
						List tempObjRoom  = roomManager.checkRoom(wtObj.getCorpID(),orderRoom.getRoomCode(),soObj.getShipNumber()); 
						if(tempObjRoom!=null && !tempObjRoom.isEmpty() && tempObjRoom.get(0)!=null){
							room = (com.trilasoft.app.model.Room) tempObjRoom.get(0);
						  }else{
							room.setCorpId(wtObj.getCorpID());
							room.setCreatedOn(new Date());
							room.setCreatedBy(getRequest().getRemoteUser());
						  }
						String roomConditionId="";
						try {
							String tempConditionVal = roomConditionMap.get(""+orderRoom.getRoomCode());
							String[] arrRoomVal = tempConditionVal.split("~");
							if(arrRoomVal[0]!=null && !arrRoomVal[0].equals("") && arrRoomVal[0].equals("true") ){
								room.setHasDamage(true);
							}else{
								room.setHasDamage(false);
							}
							if(arrRoomVal[1]!=null && !arrRoomVal[1].equals("NA")){
								room.setFloor(arrRoomVal[1]);
							}else{
								room.setFloor("");
							}
							if(arrRoomVal[2]!=null && !arrRoomVal[2].equals("NA")){
								room.setComment(arrRoomVal[2]);
							}else{
								room.setComment("");
							}
							if(arrRoomVal[3]!=null && !arrRoomVal[3].equals("") ){
								if(roomConditionId.equals("")){
									roomConditionId =arrRoomVal[3]; 
								}else{
									roomConditionId =roomConditionId+"~"+arrRoomVal[3];
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						room.setRoom(""+orderRoom.getRoomCode());
						room.setRoomDesc(orderRoom.getRoomName());
						room.setUpdatedOn(new Date());
						room.setUpdatedBy(getRequest().getRemoteUser());
						room.setShipNumber(soObj.getShipNumber());
						if(invLoadArr!=null){
							room.setLocationId(locationMap.get("Origin"));
						}else{
							room.setLocationId(locationMap.get("Destination"));
						}
						try{
							room = roomManager.save(room);
						}catch(Exception e){
							logger.warn("Error in saving Room od inventory item "+inventoryItem.getBarcode());
						}
						StartTime = System.currentTimeMillis();
						inventoryPathManager.deleteAllWorkTicketInventory("roomId",wtObj.getCorpID(),room.getId());
						timeTaken =  System.currentTimeMillis() - StartTime;
						logger.warn("\n\ntime taken to delete room images from inventory path : "+timeTaken+"\n\n");
						String[] arrRoomVal = roomConditionId.split("~");
						for (String tempCondition : arrRoomVal) {
							StartTime = System.currentTimeMillis();
							GetRoomImages getRoomImg = new GetRoomImages();
							getRoomImg.setRoomConditionID(Integer.parseInt(tempCondition));
							 GetRoomImagesResponse roomImage  = syncServiceStub.getRoomImages(getRoomImg);
							  Image[] ImgArr = roomImage.getGetRoomImagesResult().getImage();
							  if(ImgArr!=null){
								  for (Image image : ImgArr) {
									  try {
										DataHandler  img = image.getImageData();
										  ByteArrayOutputStream output = new ByteArrayOutputStream();
										  img.writeTo(output);
										  BufferedImage imag=ImageIO.read(new ByteArrayInputStream(output.toByteArray()));
										  String path = "/usr/local/redskydoc/MobileMover/MM-"+image.getImageID()+".jpg";
										  ImageIO.write(imag, "jpg", new File("/usr/local/redskydoc/MobileMover","MM-"+image.getImageID()+".jpg"));
										  InventoryPath invPath =  new InventoryPath();
										  invPath.setCorpID(wtObj.getCorpID());
										  invPath.setPath(path);
										  invPath.setRoomId(room.getId());
										  inventoryPathManager.save(invPath);
									} catch (Exception e) {
										logger.warn("Exception in saving Room Image in inventory patha table.");
										e.printStackTrace();
									}
								}
							}
							 timeTaken =  System.currentTimeMillis() - StartTime;
							logger.warn("\n\ntime taken to save room images from inventory path : "+timeTaken+"\n\n");
						}
						
						if(invLoadArr!=null){
							wktInv.setoItemRoomId(room.getId());
						}else{
							wktInv.setdItemRoomId(room.getId());
						}
						wktInv.setoItemCondition(inventoryConditionMap.get(inventoryItem.getBarcode()));
						wktInv.setLocationId(room.getLocationId());
						wktInv.setShipNumber(soObj.getShipNumber());
						wktInv.setdItemCondition(itemUnloadConditionMap.get(inventoryItem.getBarcode()));
						wktInv.setoItemComment(inventoryItem.getComments());
						try {
							wktInv = workTicketInventoryDataManager.save(wktInv);
						} catch (Exception e) {
							logger.warn("Error in saving work ticket inventory "+inventoryItem.getBarcode());
							e.printStackTrace();
						}
						
						// Getting inventory images 
						StartTime = System.currentTimeMillis();
						inventoryPathManager.deleteAllWorkTicketInventory("workticketInventoryDataId",wtObj.getCorpID(),wktInv.getId());
						 timeTaken =  System.currentTimeMillis() - StartTime;
							logger.warn("\n\ntime taken to delete work ticket inventory images from inventory path : "+timeTaken+"\n\n");
						GetInventoryItemImages getInventoryItemImages = new GetInventoryItemImages();
						getInventoryItemImages.setInventoryItemID(inventoryItem.getInventoryItemID());
						GetInventoryItemImagesResponse invImgs = syncServiceStub.getInventoryItemImages(getInventoryItemImages) ;
						Image[] ImgArr = invImgs.getGetInventoryItemImagesResult().getImage();
						if(ImgArr!=null){
							StartTime = System.currentTimeMillis();
							for (Image image : ImgArr) {
								  try {
									DataHandler  img = image.getImageData();
									  ByteArrayOutputStream output = new ByteArrayOutputStream();
									  img.writeTo(output);
									  BufferedImage imag=ImageIO.read(new ByteArrayInputStream(output.toByteArray()));
									  String path = "/usr/local/redskydoc/MobileMover/MM-"+image.getImageID()+".jpg";
									  ImageIO.write(imag, "jpg", new File("/usr/local/redskydoc/MobileMover","MM-"+image.getImageID()+".jpg"));
									  InventoryPath invPath =  new InventoryPath();
									  invPath.setCorpID(wtObj.getCorpID());
									  invPath.setPath(path);
									  invPath.setWorkticketInventoryDataId(wktInv.getId());
									  inventoryPathManager.save(invPath);
								} catch (Exception e) {
									logger.warn("Error in saving inventory image of bar code "+inventoryItem.getBarcode());
									e.printStackTrace();
								}
							}
							timeTaken =  System.currentTimeMillis() - StartTime;
							logger.warn("\n\ntime taken to Save work ticket inventory images from inventory path : "+timeTaken+"\n\n");
						}
					}
			} 
					 timeTaken =  System.currentTimeMillis() - StartTime;
					  logger.warn("\n\nTime taken to get and save inventory and room : "+timeTaken+"\n\n");
				
			}		
				
		}
			saveMessage(getText("Ticket has been synced with Mobile Mover"));
 	}catch (Exception e) {
 		errorMessage(getText("Getting Error : "+e.getMessage()+" in Syncing With  Mobile Mover.Please contact to support@redskymobility.com"));
		logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n Error in Save Order : "+e.getMessage());
		logger.warn("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		e.printStackTrace();
	}finally{
		try {
			OperationContext operationContext = syncServiceStub
					._getServiceClient().getLastOperationContext();
			System.out.println("Printing Messages");
			if (operationContext != null) {
				MessageContext outMessageContext = operationContext
						.getMessageContext("Out");
				if (outMessageContext != null) {
					System.out.println("OUT SOAP: "
							+ outMessageContext.getEnvelope().toString());
				}
				MessageContext inMessageContext = operationContext
						.getMessageContext("In");
				if (inMessageContext != null) {
					System.out.println("IN SOAP: "
							+ inMessageContext.getEnvelope().toString());
				}
			}
		} catch (Throwable e) {
e.printStackTrace();
}
    }
        
	idURL = "?id="+wktId;
	logger.warn("End =========== Mobile mover sync");
	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	return SUCCESS;
  }
	
	
	// method for converting date object to calender object
	public Calendar DateToCalendar(Date date){ 
		Company cmp =companyManager.findByCorpID(sessionCorpID).get(0);
		 TimeZone timeZone = TimeZone.getTimeZone(cmp.getTimeZone());
		 // Calendar cal = Calendar.getInstance();
		 Calendar cal =  new GregorianCalendar(timeZone);
		  if(date== null){
			  return null;
		  }else{
			  cal.setTime(date);
			  return cal;
		  }
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getSid() {
		return sid;
	}


	public void setSid(Long sid) {
		this.sid = sid;
	}


	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}


	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}


	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}


	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}


	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}


	public Long getWktId() {
		return wktId;
	}


	public void setWktId(Long wktId) {
		this.wktId = wktId;
	}


	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}


	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}


	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}


	public void setItemsJEquipManager(ItemsJEquipManager itemsJEquipManager) {
		this.itemsJEquipManager = itemsJEquipManager;
	}


	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}


	public String getIdURL() {
		return idURL;
	}


	public void setIdURL(String idURL) {
		this.idURL = idURL;
	}


	public String getButtonType() {
		return buttonType;
	}


	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}


	public void setInventoryDataManager(InventoryDataManager inventoryDataManager) {
		this.inventoryDataManager = inventoryDataManager;
	}


	public void setUpalodedImagesManager(WorkTicketInventoryDataManager workTicketInventoryDataManager) {
		this.workTicketInventoryDataManager = workTicketInventoryDataManager;
	}

	/**
	 * @param workTicketInventoryDataManager the workTicketInventoryDataManager to set
	 */
	public void setWorkTicketInventoryDataManager(
			WorkTicketInventoryDataManager workTicketInventoryDataManager) {
		this.workTicketInventoryDataManager = workTicketInventoryDataManager;
	}

	/**
	 * @param inventoryLocationManager the inventoryLocationManager to set
	 */
	public void setInventoryLocationManager(
			InventoryLocationManager inventoryLocationManager) {
		this.inventoryLocationManager = inventoryLocationManager;
	}

	/**
	 * @param roomManager the roomManager to set
	 */
	public void setRoomManager(RoomManager roomManager) {
		this.roomManager = roomManager;
	}

	public void setInventoryPathManager(InventoryPathManager inventoryPathManager) {
		this.inventoryPathManager = inventoryPathManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getMmMemberID() {
		return mmMemberID;
	}

	public void setMmMemberID(String mmMemberID) {
		this.mmMemberID = mmMemberID;
	}

	public String getRequestedCW() {
		return requestedCW;
	}

	public void setRequestedCW(String requestedCW) {
		this.requestedCW = requestedCW;
	}

	public String getRequestedWT() {
		return requestedWT;
	}

	public void setRequestedWT(String requestedWT) {
		this.requestedWT = requestedWT;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}

	public Payroll getPayroll() {
		return payroll;
	}

	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public void setReportAction(ReportAction reportAction) {
		this.reportAction = reportAction;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getFileLoc() {
		return fileLoc;
	}

	public void setFileLoc(String fileLoc) {
		this.fileLoc = fileLoc;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	
	
}
