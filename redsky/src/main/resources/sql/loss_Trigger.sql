
DELIMITER $$
create trigger redsky.trigger_add_history_loss BEFORE UPDATE on redsky.loss 
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'loss.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;

 


   IF (OLD.ship <> NEW.ship ) THEN
       select CONCAT(fieldNameList,'loss.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ship,'~') into newValueList;
   END IF;

   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'loss.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;

   IF ((OLD.claimNumber <> NEW.claimNumber) or (OLD.claimNumber is null and NEW.claimNumber is not null) 
	  or (OLD.claimNumber is not null and NEW.claimNumber is null)) THEN
		select CONCAT(fieldNameList,'loss.claimNumber~') into fieldNameList;
		IF(OLD.claimNumber is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.claimNumber is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.claimNumber is not null) then
			select CONCAT(oldValueList,OLD.claimNumber,'~') into oldValueList;
		END IF;
		IF(NEW.claimNumber is not null) then
			select CONCAT(newValueList,NEW.claimNumber,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.idNumber1 <> NEW.idNumber1) or (OLD.idNumber1 is null and NEW.idNumber1 is not null) 
	  or (OLD.idNumber1 is not null and NEW.idNumber1 is null)) THEN
		select CONCAT(fieldNameList,'loss.idNumber1~') into fieldNameList;
		IF(OLD.idNumber1 is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.idNumber1 is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.idNumber1 is not null) then
			select CONCAT(oldValueList,OLD.idNumber1,'~') into oldValueList;
		END IF;
		IF(NEW.idNumber1 is not null) then
			select CONCAT(newValueList,NEW.idNumber1,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.lossNumber <> NEW.lossNumber ) THEN
       select CONCAT(fieldNameList,'loss.lossNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lossNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lossNumber,'~') into newValueList;
   END IF;

   IF (OLD.lossType <> NEW.lossType ) THEN
       select CONCAT(fieldNameList,'loss.lossType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lossType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lossType,'~') into newValueList;
   END IF;

   IF (OLD.damageByAction <> NEW.damageByAction ) THEN
       select CONCAT(fieldNameList,'loss.damageByAction~') into fieldNameList;
       select CONCAT(oldValueList,OLD.damageByAction,'~') into oldValueList;
       select CONCAT(newValueList,NEW.damageByAction,'~') into newValueList;
   END IF;

   IF (OLD.lossAction <> NEW.lossAction ) THEN
       select CONCAT(fieldNameList,'loss.lossAction~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lossAction,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lossAction,'~') into newValueList;
   END IF;

   IF ((OLD.paidCompensationCustomer <> NEW.paidCompensationCustomer) or (OLD.paidCompensationCustomer is null and NEW.paidCompensationCustomer is not null) 
	  or (OLD.paidCompensationCustomer is not null and NEW.paidCompensationCustomer is null)) THEN
		select CONCAT(fieldNameList,'loss.paidCompensationCustomer~') into fieldNameList;
		IF(OLD.paidCompensationCustomer is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.paidCompensationCustomer is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.paidCompensationCustomer is not null) then
			select CONCAT(oldValueList,OLD.paidCompensationCustomer,'~') into oldValueList;
		END IF;
		IF(NEW.paidCompensationCustomer is not null) then
			select CONCAT(newValueList,NEW.paidCompensationCustomer,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.paidCompensation3Party <> NEW.paidCompensation3Party) or (OLD.paidCompensation3Party is null and NEW.paidCompensation3Party is not null) 
	  or (OLD.paidCompensation3Party is not null and NEW.paidCompensation3Party is null)) THEN
		select CONCAT(fieldNameList,'loss.paidCompensation3Party~') into fieldNameList;
		IF(OLD.paidCompensation3Party is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.paidCompensation3Party is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.paidCompensation3Party is not null) then
			select CONCAT(oldValueList,OLD.paidCompensation3Party,'~') into oldValueList;
		END IF;
		IF(NEW.paidCompensation3Party is not null) then
			select CONCAT(newValueList,NEW.paidCompensation3Party,'~') into newValueList;
		END IF; 
	END IF;
	
      IF (OLD.lossComment <> NEW.lossComment ) THEN
       select CONCAT(fieldNameList,'loss.lossComment~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lossComment,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lossComment,'~') into newValueList;
   END IF;

   IF (OLD.warehouse <> NEW.warehouse ) THEN
       select CONCAT(fieldNameList,'loss.warehouse~') into fieldNameList;
       select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
       select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
   END IF;
   
   IF ((OLD.ticket <> NEW.ticket) or (OLD.ticket is null and NEW.ticket is not null) 
	  or (OLD.ticket is not null and NEW.ticket is null)) THEN
		select CONCAT(fieldNameList,'loss.ticket~') into fieldNameList;
		IF(OLD.ticket is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.ticket is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.ticket is not null) then
			select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
		END IF;
		IF(NEW.ticket is not null) then
			select CONCAT(newValueList,NEW.ticket,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.ticket2 <> NEW.ticket2) or (OLD.ticket2 is null and NEW.ticket2 is not null) 
	  or (OLD.ticket2 is not null and NEW.ticket2 is null)) THEN
		select CONCAT(fieldNameList,'loss.ticket2~') into fieldNameList;
		IF(OLD.ticket2 is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.ticket2 is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.ticket2 is not null) then
			select CONCAT(oldValueList,OLD.ticket2,'~') into oldValueList;
		END IF;
		IF(NEW.ticket2 is not null) then
			select CONCAT(newValueList,NEW.ticket2,'~') into newValueList;
		END IF; 
	END IF;


   IF ((OLD.ticket3 <> NEW.ticket3) or (OLD.ticket3 is null and NEW.ticket3 is not null) 
	  or (OLD.ticket3 is not null and NEW.ticket3 is null)) THEN
		select CONCAT(fieldNameList,'loss.ticket3~') into fieldNameList;
		IF(OLD.ticket3 is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.ticket3 is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.ticket3 is not null) then
			select CONCAT(oldValueList,OLD.ticket3,'~') into oldValueList;
		END IF;
		IF(NEW.ticket3 is not null) then
			select CONCAT(newValueList,NEW.ticket3,'~') into newValueList;
		END IF; 
	END IF;


   IF (OLD.itemDescription <> NEW.itemDescription ) THEN
       select CONCAT(fieldNameList,'loss.itemDescription~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itemDescription,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itemDescription,'~') into newValueList;
   END IF;

   IF ((OLD.weightDamagedItem <> NEW.weightDamagedItem) or (OLD.weightDamagedItem is null and NEW.weightDamagedItem is not null) 
	  or (OLD.weightDamagedItem is not null and NEW.weightDamagedItem is null)) THEN
		select CONCAT(fieldNameList,'loss.weightDamagedItem~') into fieldNameList;
		IF(OLD.weightDamagedItem is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.weightDamagedItem is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.weightDamagedItem is not null) then
			select CONCAT(oldValueList,OLD.weightDamagedItem,'~') into oldValueList;
		END IF;
		IF(NEW.weightDamagedItem is not null) then
			select CONCAT(newValueList,NEW.weightDamagedItem,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.inventory <> NEW.inventory ) THEN
       select CONCAT(fieldNameList,'loss.inventory~') into fieldNameList;
       select CONCAT(oldValueList,OLD.inventory,'~') into oldValueList;
       select CONCAT(newValueList,NEW.inventory,'~') into newValueList;
   END IF;

   IF (OLD.chargeBack <> NEW.chargeBack ) THEN
       select CONCAT(fieldNameList,'loss.chargeBack~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeBack,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeBack,'~') into newValueList;
   END IF;

   IF (OLD.chargeBackTo <> NEW.chargeBackTo ) THEN
       select CONCAT(fieldNameList,'loss.chargeBackTo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeBackTo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeBackTo,'~') into newValueList;
   END IF;

   IF ((OLD.chargeBackAmount <> NEW.chargeBackAmount) or (OLD.chargeBackAmount is null and NEW.chargeBackAmount is not null) 
	  or (OLD.chargeBackAmount is not null and NEW.chargeBackAmount is null)) THEN
		select CONCAT(fieldNameList,'loss.chargeBackAmount~') into fieldNameList;
		IF(OLD.chargeBackAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.chargeBackAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.chargeBackAmount is not null) then
			select CONCAT(oldValueList,OLD.chargeBackAmount,'~') into oldValueList;
		END IF;
		IF(NEW.chargeBackAmount is not null) then
			select CONCAT(newValueList,NEW.chargeBackAmount,'~') into newValueList;
		END IF; 
	END IF;
	
    IF ((OLD.requestedAmount <> NEW.requestedAmount) or (OLD.requestedAmount is null and NEW.requestedAmount is not null) 
	  or (OLD.requestedAmount is not null and NEW.requestedAmount is null)) THEN
		select CONCAT(fieldNameList,'loss.requestedAmount~') into fieldNameList;
		IF(OLD.requestedAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.requestedAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.requestedAmount is not null) then
			select CONCAT(oldValueList,OLD.requestedAmount,'~') into oldValueList;
		END IF;
		IF(NEW.requestedAmount is not null) then
			select CONCAT(newValueList,NEW.requestedAmount,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.whoWorkCDE <> NEW.whoWorkCDE ) THEN
       select CONCAT(fieldNameList,'loss.whoWorkCDE~') into fieldNameList;
       select CONCAT(oldValueList,OLD.whoWorkCDE,'~') into oldValueList;
       select CONCAT(newValueList,NEW.whoWorkCDE,'~') into newValueList;
   END IF;



   IF (OLD.whoWorkName <> NEW.whoWorkName ) THEN
       select CONCAT(fieldNameList,'loss.whoWorkName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.whoWorkName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.whoWorkName,'~') into newValueList;
   END IF;

   IF (OLD.notesLoss <> NEW.notesLoss ) THEN
       select CONCAT(fieldNameList,'loss.notesLoss~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notesLoss,'~') into oldValueList;
       select CONCAT(newValueList,NEW.notesLoss,'~') into newValueList;
   END IF;

   IF (OLD.chequeNumberCustmer <> NEW.chequeNumberCustmer ) THEN
       select CONCAT(fieldNameList,'loss.chequeNumberCustmer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chequeNumberCustmer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chequeNumberCustmer,'~') into newValueList;
   END IF;


   IF (OLD.chequeNumber3Party <> NEW.chequeNumber3Party ) THEN
       select CONCAT(fieldNameList,'loss.chequeNumber3Party~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chequeNumber3Party,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chequeNumber3Party,'~') into newValueList;
   END IF;
   
   IF (OLD.claimId <> NEW.claimId ) THEN
       select CONCAT(fieldNameList,'loss.claimId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.claimId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.claimId,'~') into newValueList;
   END IF;
   
   IF (OLD.jobNumber <> NEW.jobNumber ) THEN
       select CONCAT(fieldNameList,'loss.jobNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.jobNumber,'~') into newValueList;
   END IF;
   
   IF ((OLD.paidCompensationCustomerDate <> NEW.paidCompensationCustomerDate) or (OLD.paidCompensationCustomerDate is null and NEW.paidCompensationCustomerDate is not null)
	  or (OLD.paidCompensationCustomerDate is not null and NEW.paidCompensationCustomerDate is null)) THEN
       select CONCAT(fieldNameList,'loss.paidCompensationCustomerDate~') into fieldNameList;
       IF(OLD.paidCompensationCustomerDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.paidCompensationCustomerDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.paidCompensationCustomerDate is not null) THEN
        	select CONCAT(oldValueList,OLD.paidCompensationCustomerDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.paidCompensationCustomerDate is not null) THEN
        	select CONCAT(newValueList,NEW.paidCompensationCustomerDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF ((OLD.paidCompensation3PartyDate <> NEW.paidCompensation3PartyDate) or (OLD.paidCompensation3PartyDate is null and NEW.paidCompensation3PartyDate is not null)
	  or (OLD.paidCompensation3PartyDate is not null and NEW.paidCompensation3PartyDate is null)) THEN
       select CONCAT(fieldNameList,'loss.paidCompensation3PartyDate~') into fieldNameList;
       IF(OLD.paidCompensation3PartyDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.paidCompensation3PartyDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.paidCompensation3PartyDate is not null) THEN
        	select CONCAT(oldValueList,OLD.paidCompensation3PartyDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.paidCompensation3PartyDate is not null) THEN
        	select CONCAT(newValueList,NEW.paidCompensation3PartyDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.suffix <> NEW.suffix ) THEN
       select CONCAT(fieldNameList,'loss.suffix~') into fieldNameList;
       select CONCAT(oldValueList,OLD.suffix,'~') into oldValueList;
       select CONCAT(newValueList,NEW.suffix,'~') into newValueList;
   END IF;
   
   IF ((OLD.reserve <> NEW.reserve) or (OLD.reserve is null and NEW.reserve is not null) 
	  or (OLD.reserve is not null and NEW.reserve is null)) THEN
		select CONCAT(fieldNameList,'loss.reserve~') into fieldNameList;
		IF(OLD.reserve is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.reserve is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.reserve is not null) then
			select CONCAT(oldValueList,OLD.reserve,'~') into oldValueList;
		END IF;
		IF(NEW.reserve is not null) then
			select CONCAT(newValueList,NEW.reserve,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.tk1pc <> NEW.tk1pc) or (OLD.tk1pc is null and NEW.tk1pc is not null) 
	  or (OLD.tk1pc is not null and NEW.tk1pc is null)) THEN
		select CONCAT(fieldNameList,'loss.tk1pc~') into fieldNameList;
		IF(OLD.tk1pc is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.tk1pc is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.tk1pc is not null) then
			select CONCAT(oldValueList,OLD.tk1pc,'~') into oldValueList;
		END IF;
		IF(NEW.tk1pc is not null) then
			select CONCAT(newValueList,NEW.tk1pc,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.tk2pc <> NEW.tk2pc) or (OLD.tk2pc is null and NEW.tk2pc is not null) 
	  or (OLD.tk2pc is not null and NEW.tk2pc is null)) THEN
		select CONCAT(fieldNameList,'loss.tk2pc~') into fieldNameList;
		IF(OLD.tk2pc is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.tk2pc is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.tk2pc is not null) then
			select CONCAT(oldValueList,OLD.tk2pc,'~') into oldValueList;
		END IF;
		IF(NEW.tk2pc is not null) then
			select CONCAT(newValueList,NEW.tk2pc,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.tk3pc <> NEW.tk3pc) or (OLD.tk3pc is null and NEW.tk3pc is not null) 
	  or (OLD.tk3pc is not null and NEW.tk3pc is null)) THEN
		select CONCAT(fieldNameList,'loss.tk3pc~') into fieldNameList;
		IF(OLD.tk3pc is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.tk3pc is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.tk3pc is not null) then
			select CONCAT(oldValueList,OLD.tk3pc,'~') into oldValueList;
		END IF;
		IF(NEW.tk3pc is not null) then
			select CONCAT(newValueList,NEW.tk3pc,'~') into newValueList;
		END IF; 
	END IF;

	
	
	
	
	 IF ((OLD.datePurchased <> NEW.datePurchased ) or (OLD.datePurchased is null and NEW.datePurchased is not null)
	  or (OLD.datePurchased is not null and NEW.datePurchased is null)) THEN
       select CONCAT(fieldNameList,'loss.datePurchased~') into fieldNameList;
       IF(OLD.datePurchased is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.datePurchased is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.datePurchased is not null) THEN
        	select CONCAT(oldValueList,OLD.datePurchased,'~') into oldValueList;
     	END IF;
      	IF(NEW.datePurchased is not null) THEN
        	select CONCAT(newValueList,NEW.datePurchased,'~') into newValueList;
      	END IF;
   END IF;
  
  
   IF ((OLD.originalCost <> NEW.originalCost) or (OLD.originalCost is null and NEW.originalCost is not null) 
	  or (OLD.originalCost is not null and NEW.originalCost is null)) THEN
		select CONCAT(fieldNameList,'loss.originalCost~') into fieldNameList;
		IF(OLD.originalCost is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.originalCost is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.originalCost is not null) then
			select CONCAT(oldValueList,OLD.originalCost,'~') into oldValueList;
		END IF;
		IF(NEW.originalCost is not null) then
			select CONCAT(newValueList,NEW.originalCost,'~') into newValueList;
		END IF; 
	END IF;
  
  
  
  
  IF (OLD.cartonDamaged <> NEW.cartonDamaged ) THEN
       select CONCAT(fieldNameList,'itemsjequip.cartonDamaged~') into fieldNameList;
       IF(OLD.cartonDamaged = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.cartonDamaged = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.cartonDamaged = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.cartonDamaged = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
   IF (OLD.originalCostCurrency <> NEW.originalCostCurrency ) THEN
       select CONCAT(fieldNameList,'loss.originalCostCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originalCostCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originalCostCurrency,'~') into newValueList;
   END IF;

  
   IF (OLD.requestedAmountCurrency <> NEW.requestedAmountCurrency ) THEN
       select CONCAT(fieldNameList,'loss.requestedAmountCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.requestedAmountCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.requestedAmountCurrency,'~') into newValueList;
   END IF;
 IF (OLD.paidCompensationCustomerCurrency <> NEW.paidCompensationCustomerCurrency ) THEN
       select CONCAT(fieldNameList,'loss.paidCompensationCustomerCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paidCompensationCustomerCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paidCompensationCustomerCurrency,'~') into newValueList;
   END IF;

  
   IF (OLD.paidCompensation3PartyCurrency <> NEW.paidCompensation3PartyCurrency ) THEN
       select CONCAT(fieldNameList,'loss.paidCompensation3PartyCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paidCompensation3PartyCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paidCompensation3PartyCurrency,'~') into newValueList;
   END IF;
 IF (OLD.chargeBackCurrency <> NEW.chargeBackCurrency ) THEN
       select CONCAT(fieldNameList,'loss.chargeBackCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeBackCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeBackCurrency,'~') into newValueList;
   END IF;
 IF (OLD.selfBlame <> NEW.selfBlame ) THEN
       select CONCAT(fieldNameList,'loss.selfBlame~') into fieldNameList;
       select CONCAT(oldValueList,OLD.selfBlame,'~') into oldValueList;
       select CONCAT(newValueList,NEW.selfBlame,'~') into newValueList;
   END IF;
 IF (OLD.itmItemCode <> NEW.itmItemCode ) THEN
       select CONCAT(fieldNameList,'loss.itmItemCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itmItemCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itmItemCode,'~') into newValueList;
   END IF;

   IF (OLD.itmClmsItemSeqNbr <> NEW.itmClmsItemSeqNbr ) THEN
       select CONCAT(fieldNameList,'loss.itmClmsItemSeqNbr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itmClmsItemSeqNbr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itmClmsItemSeqNbr,'~') into newValueList;
   END IF;
  IF (OLD.itmDnlRsnCode <> NEW.itmDnlRsnCode ) THEN
       select CONCAT(fieldNameList,'loss.itmDnlRsnCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itmDnlRsnCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itmDnlRsnCode,'~') into newValueList;
   END IF;
  
  
   IF ((OLD.itrPmntId <> NEW.itrPmntId ) or (OLD.itrPmntId is null and NEW.itrPmntId is not null) 
	  or (OLD.itrPmntId is not null and NEW.itrPmntId is null)) THEN
		select CONCAT(fieldNameList,'loss.itrPmntId~') into fieldNameList;
		IF(OLD.itrPmntId is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.itrPmntId is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.itrPmntId is not null) then
			select CONCAT(oldValueList,OLD.itrPmntId,'~') into oldValueList;
		END IF;
		IF(NEW.itrPmntId is not null) then
			select CONCAT(newValueList,NEW.itrPmntId,'~') into newValueList;
		END IF; 
	END IF;
  
  IF (OLD.itmAltItemDesc <> NEW.itmAltItemDesc ) THEN
       select CONCAT(fieldNameList,'loss.itmAltItemDesc~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itmAltItemDesc,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itmAltItemDesc,'~') into newValueList;
   END IF;
  
  IF (OLD.itrRespExcpCode <> NEW.itrRespExcpCode ) THEN
       select CONCAT(fieldNameList,'loss.itrRespExcpCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itrRespExcpCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itrRespExcpCode,'~') into newValueList;
   END IF;
  
   IF ((OLD.itrAssessdLiabilityAmt <> NEW.itrAssessdLiabilityAmt) or (OLD.itrAssessdLiabilityAmt is null and NEW.itrAssessdLiabilityAmt is not null)
	  or (OLD.itrAssessdLiabilityAmt is not null and NEW.itrAssessdLiabilityAmt is null)) THEN
       select CONCAT(fieldNameList,'loss.itrAssessdLiabilityAmt~') into fieldNameList;
       IF(OLD.itrAssessdLiabilityAmt is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.itrAssessdLiabilityAmt is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.itrAssessdLiabilityAmt is not null) THEN
        	select CONCAT(oldValueList,OLD.itrAssessdLiabilityAmt,'~') into oldValueList;
     	END IF;
      	IF(NEW.itrAssessdLiabilityAmt is not null) THEN
        	select CONCAT(newValueList,NEW.itrAssessdLiabilityAmt,'~') into newValueList;
      	END IF;
   END IF;
  
  
   IF ((OLD.itrTotalLiabilityAmt <> NEW.itrTotalLiabilityAmt) or (OLD.itrTotalLiabilityAmt is null and NEW.itrTotalLiabilityAmt is not null)
	  or (OLD.itrTotalLiabilityAmt is not null and NEW.itrTotalLiabilityAmt is null)) THEN
       select CONCAT(fieldNameList,'loss.itrTotalLiabilityAmt~') into fieldNameList;
       IF(OLD.itrTotalLiabilityAmt is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.itrTotalLiabilityAmt is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.itrTotalLiabilityAmt is not null) THEN
        	select CONCAT(oldValueList,OLD.itrTotalLiabilityAmt,'~') into oldValueList;
     	END IF;
      	IF(NEW.itrTotalLiabilityAmt is not null) THEN
        	select CONCAT(newValueList,NEW.itrTotalLiabilityAmt,'~') into newValueList;
      	END IF;
   END IF;
	
	
	
   
   CALL add_tblHistory (OLD.id,"loss", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$

