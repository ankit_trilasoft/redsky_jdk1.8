package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ProfileInfoDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ProfileInfo;

public class ProfileInfoDaoHibernate extends GenericDaoHibernate<ProfileInfo, Long> implements ProfileInfoDao {
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public ProfileInfoDaoHibernate() {
		super(ProfileInfo.class);
	} 
	
	public List getProfileInfoListByPartnerCode(String corpID, String partnerCode) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("FROM ProfileInfo WHERE partnerCode = '"+ partnerCode +"' ");
	}

}
