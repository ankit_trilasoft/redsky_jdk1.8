<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="storageList2.title"/></title> 
    <meta name="heading" content="<fmt:message key='storageList2.heading'/>"/> 
    
    <script language="javascript" type="text/javascript">

	function clear_fields(){
		var i;
		for(i=0;i<=7;i++){
			document.forms['searchForm'].elements[i].value = "";
		}
	}
	
	function chkSelect(){
		if (checkNumber('searchForm','storage.ticket','Please enter only numeric data in Ticket!') == false){
              document.forms['searchForm'].elements['storage.ticket'].focus();
              return false
           }
      }
  function onlyNumsAllowed(evt, strList, bAllow){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110); 
	}
	function onlyNumeric(targetElement)
	{   
	var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Please enter only numeric data in Ticket!");
	        targetElement.value="";
	        return false;
	        }
	    }
	    return true;
	}

   function findStorageLibraryVolume1(storageId,position){
		var url="findStorageLibraryVolume.html?storageId="+storageId+"&decorator=simple&popup=true";
		ajax_showTooltip(url,position);		
	}
function goToSearch(){
		return selectSearchField();
        document.forms['searchForm'].action = 'searchStorages2.html';
        document.forms['searchForm'].submit();
}	

function selectSearchField()

{	
		var locationId=document.forms['searchForm'].elements['storage.locationId'].value; 
		var storageId= document.forms['searchForm'].elements['storage.storageId'].value;
		var firstname=document.forms['searchForm'].elements['firstname'].value;
		var lastName=document.forms['searchForm'].elements['lastName'].value;
		var shipNumber=document.forms['searchForm'].elements['storage.shipNumber'].value;
		var ticket=document.forms['searchForm'].elements['storage.ticket'].value;
		var description=document.forms['searchForm'].elements['storage.description'].value;
		var itemTag=document.forms['searchForm'].elements['storage.itemTag'].value;
		
		if(locationId=='' && storageId=='' &&  firstname=='' && lastName=='' && shipNumber=='' && ticket=='' && description=='' && itemTag=='')
		{
			alert('Please select any one of the search criteria!');	
			return false;	
		}
		
}
	
	</script>
<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:9px;
!margin-bottom:2px;
margin-top:-25px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
}
div.error, span.error, li.error, div.message {
background:#BCD2EF none repeat scroll 0%;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:bold;
margin:10px auto;
padding:3px;
text-align:center;
vertical-align:bottom;
width:450px;
}

</style>	
</head>

<c:set var="buttons">
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/workTickets.html"/>'" 
        value="<fmt:message key="button.done"/>"/>
</c:set> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>

     
<s:form cssClass="form_magn" id="searchForm" action="searchStorages2" method="post" validate="true">   

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
   <div class="center-content">
	
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="storage.locationId"/></th>
<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdVOER">
<th>Storage Id</th>
</configByCorp:fieldVisibility>
<th>First Name</th>
<th>Last Name</th>
<th><fmt:message key="storage.shipNumber"/></th>
<th><fmt:message key="storage.ticket"/></th>
<th><fmt:message key="storage.description"/></th>
<th><fmt:message key="storage.itemTag"/></th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <s:textfield name="storage.locationId" required="true" cssClass="input-text" size="15"/>
			</td>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdVOER">
			<td width="" align="left">
			    <s:textfield name="storage.storageId" required="true" cssClass="input-text" size="15"/>
			</td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdSSCW">
			<s:hidden name="storage.storageId" />
			</configByCorp:fieldVisibility>
			<td width="" align="left">
			    <s:textfield name="firstname" required="true" cssClass="input-text" size="15"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="lastName" required="true" cssClass="input-text" size="15"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="storage.shipNumber" required="true" cssClass="input-text" size="15"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="storage.ticket" required="true" cssClass="input-text" size="15" onchange="return onlyNumeric(this)"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="storage.description" required="true" cssClass="input-text" size="15"/>
			</td>
			<td class="rbrSF"  style="border-bottom:1px solid #E0E0E0;" align="left">
			    <s:textfield name="storage.itemTag" required="true" cssClass="input-text" size="15"/>
			</td>
			
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td colspan="8" align="center" style="border-left: hidden;text-align:right;" class="rbrSF">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />
<s:set name="storages" value="storages" scope="request"/>   
<div style="width:100%;overflow-x:auto;overflow-y:auto;">
<div id="otabs" style="margin-bottom:26px;!margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Storage List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<display:table name="storages" style="margin-top:-2px;!margin-top:5px;" class="table" requestURI="" id="storageList2" export="${empty param.popup}" pagesize="10" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>
 	<display:column property="locationId" sortable="true" titleKey="storage.locationId" url="/editStorage2.html?from=list" paramId="id" paramProperty="id"/>
 	<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdVOER">
 	<display:column property="storageId" sortable="true" title="Storage Id"/>
 	</configByCorp:fieldVisibility>
 	<display:column sortable="true" title="Name"> 	
 	<c:out value="${storageList2.firstname}"/>&nbsp;<c:out value="${storageList2.lastName}"/> 	
    </display:column>
 	<%-- <display:column property="itemNumber" titleKey="storage.itemNumber" /> 
    <display:column property="jobNumber" sortable="true" titleKey="workTicket.jobNumber" /> --%>
    <display:column property="shipNumber" sortable="true" titleKey="storage.shipNumber"/>
 	<display:column property="releaseDate" titleKey="storage.releaseDate" sortable="true" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="ticket" headerClass="containeralign" style="text-align: right" sortable="true" titleKey="workTicket.ticket" />
 	<display:column property="toRelease" sortable="true" title="Release&nbsp;Ticket"/>
	<display:column property="description" sortable="true" titleKey="storage.description"/>
	<display:column property="containerId" sortable="true" titleKey="storage.containerId"/>
	<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdVOER">
	<display:column sortable="true" title="Volume">
	<c:out value="${storageList2.volume}"/>
	<c:if test="${storageList2.volume != '' && storageList2.volume != null}">
	<img align="top" onclick="findStorageLibraryVolume1('${storageList2.storageId}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/>
	</c:if>
	</display:column>
	</configByCorp:fieldVisibility>
	<display:column property="itemTag" sortable="true" titleKey="storage.itemTag"/>
	<display:column headerClass="containeralign" sortable="true" property="pieces" style="text-align: right" titleKey="storage.pieces"/>
	<display:column headerClass="containeralign" sortable="true" property="price" style="text-align: right" titleKey="storage.price"/>
	
		
	<display:setProperty name="paging.banner.item_name" value="storage"/>
    <display:setProperty name="paging.banner.items_name" value="storage"/>
    
    <display:setProperty name="export.excel.filename" value="Storage List.xls"/>
    <display:setProperty name="export.csv.filename" value="Storage List.csv"/>
    <display:setProperty name="export.pdf.filename" value="Storage List.pdf"/>
</display:table>
</div>
</s:form>  

<script type="text/javascript"> 
    //highlightTableRows("storageList2");
</script> 