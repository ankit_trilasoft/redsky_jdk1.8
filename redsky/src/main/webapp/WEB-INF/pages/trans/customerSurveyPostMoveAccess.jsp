<%@ include file="/common/taglibs.jsp"%>
<html><head>
	<title>RedSky Post Move Survey</title>
	<meta name="heading" content="Customer Request Access"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style>
div#page {text-align: left;}
form ul li{list-style: outside none disc;line-height:13px;margin: 0; padding: 1px 1px 2px 0px;}    
form{font-size:12px;}
html,body{background:#fff none repeat scroll 0 0;border:0 none;color:#000;margin:0;padding:0}a{border:0 none;color:blue}a img{border:medium none} h1,h2,h3,h4,h5,h6,p,ul,ol,li,table,form{margin:0;padding:0} 
h1{color:#083d87;font-family:Times,serif;font-size:24px !important;font-weight:700} 
h2{color:#083d87;font-size:16px;font-weight:400}h3{font-size:12px;font-weight:400} 
h4{color:#083d87;font-size:10px;font-weight:400} 
div.round_box{background:#fed847 none repeat scroll 0 0;border:1px solid #feae01;border-radius:6px;color:#186ebe;font:bold 16px arial;padding:5px 10px;text-align:center} 
div.round_box:hover{background:#fec901 none repeat scroll 0 0;color:#085eae} 
a.round_box{color:#186ebe;font:bold 16px arial;padding:6px 11px;text-decoration:none} 
a.round_box:hover{background:#fed847 none repeat scroll 0 0;border:1px solid #feae01;border-radius:6px;padding:5px 10px} 
table.plain{border-collapse:collapse} 
table.plain td{border:2px solid #219dd1 !important;font-family:verdana,arial !important;font-size:11px;font-weight:400;padding:3px 2px;text-align:center} 
table.plain th{border:2px solid #219dd1 !important;font-family:verdana,arial !important;font-size:11px;font-weight:700;padding:3px 3px 3px 2px;text-align:center} 
.customHead{background-color:#e4edf6;border-bottom:1px solid grey;font:bold 18px Arial;margin-top:30px} 
.suddpreFooter{width:100%;height:16px;margin-top:20px;background:#186EBE;text-align:right;color:#F6D009;padding-top:2px;font:10px arial}
</style>
</head>
<body>

<script type="text/javascript">
function fix_width(id) //set width of id to 85% if >=1150px, 95% 900-1149px, 100%<900px
{
  if (window.innerWidth>=1150)
    document.getElementById(id).style.width="85%";
	if ((window.innerWidth>=900)&&(window.innerWidth<1150))
    document.getElementById(id).style.width="95%";
  if (window.innerWidth<900)
    document.getElementById(id).style.width="100%";	
}	

function adjust_for_width()
{
  fix_width('page');
	if (window.innerWidth<900)
	  document.getElementById('logo').src="images/SuddathLogo_2012_small.jpg";
}

function get_radio_value(ele)
{
  var rad_val = null;
	
  for (var i=0; i < ele.length; i++)
  {
    if (ele[i].checked)
    {
      rad_val = ele[i].value;
    }
  }
	return rad_val;
}

function verify_comments(b)
{
  var oForm = document.forms["customerPostMoveRequestAccess"]; 
	
	//this also sets the forms hidden inputs with radio button values
	oForm.elements["ADMIN"].value = get_radio_value(oForm.elements["custService"]);
	oForm.elements["ORIGIN"].value = get_radio_value(oForm.elements["originAgent"]);
	oForm.elements["DEST"].value = get_radio_value(oForm.elements["destinAgent"]);
	oForm.elements["OVERALL"].value = get_radio_value(oForm.elements["overall"]);
	oForm.elements["RECOMMEND"].value = get_radio_value(oForm.elements["recommend"]);
  if ((oForm.elements["ADMIN"].value==1)&&(oForm.elements["comment1"].value.length<5))
	{
	  alert('If you gave a Customer Service score of Poor, please leave us a comment to explain.');
		oForm.elements["comment1"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["ORIGIN"].value==1)&&(oForm.elements["comment2"].value.length<5))
	{
	  alert('If you gave an Origin score of Poor, please leave us a comment to explain.');
		oForm.elements["comment2"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["DEST"].value==1)&&(oForm.elements["comment3"].value.length<5))
	{
	  alert('If you gave a Destination score of Poor, please leave us a comment to explain.');
		oForm.elements["comment3"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["OVERALL"].value==1)&&(oForm.elements["comment4"].value.length<5))
	{
	  alert('If you gave an Overall score of Poor, please leave us a comment to explain.');
		oForm.elements["comment4"].style.borderColor="red";
		return false;
	}	
	if((oForm.elements["CONTACT_YES"].checked==true)){
		if(oForm.elements["contactPhone"].value=='')
		{
			alert('Please fill your phone.');
			oForm.elements["contactPhone"].style.borderColor="red";
			return false;
		}
		if(oForm.elements["contactEmail"].value=='')
		{
			alert('Please fill your email.');
			oForm.elements["contactEmail"].style.borderColor="red";
			return false;
		}
		if(oForm.elements["contactEmail"].value!='')
		{
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!filter.test(email.value)) {
				alert('Please provide a valid email address');
				oForm.elements["contactEmail"].style.borderColor="red";
				return false;
			}
		}
	}
	
  b.value = 'Submitting...';
  b.disabled = true;
  b.form.submit();
	
	return true;
}

</script>


<div id="container" style="width:85%; margin:0 auto;">
<div style="float:right; margin-top:15px;margin-right:65px;"><a href="http://www.suddath.com"><img id="logo" src="images/SUDD_logo.png"></a></div>
<div class="round_box" style="float:left; margin-top:39px;margin-left:60px;">Suddath Service Evaluation</div>
<div style="clear:both; margin-bottom:16px;"></div>

<!---main bordered div--->	
<div id="page" style="border:2px solid #219dd1; padding:0px 20px 20px 20px; margin:0px auto; width:85%; color:#7f7f7f;"> 

<s:form id="customerPostMoveRequestAccess" name="customerPostMoveRequestAccess" action="saveCustomerPostMoveSurvey.html?decorator=popup&popup=true&bypass_sessionfilter=YES&soId=${soId}&sessionCorpID=${sessionCorpID}" method="post" validate="true">
<input type=hidden name=SOURCE value="SURVEY_GLOBAL">
<input type=hidden name=ACCOUNT value="295867">
<input type=hidden name=BRANCH_CODE value="05-016-00">
<input type=hidden name=MOVES_ID value="251631">
<input type=hidden name=REG value="251631"><input type=hidden name=ORDER value="">
<input type=hidden name=MM_NAME value="Katie Schiffers">
<input type=hidden name=TYPE_OF_MOVE value="D/D - DOOR/DOOR">
<input type=hidden name=DATE_SUBMITTED value="28-Apr-2016">
<input type=hidden name=TRANSFEREE value="Willy Chen">
<input type=hidden name=WWID value="">
<input type=hidden name=shipNumber value="${shipNumber}">
<c:if test="${postFeedBack=='Y'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Your Feedback has already been submitted!!<br>Please click our Logo on the right to return to our home page.</b></h3>
    </td>
  </tr>
</c:if>
<c:if test="${postFeedBack=='N'}">
<div style="text-align:left; font:700 13px arial!important; color:#003366;">
<div style="width:40%; float:left;">Transferee: ${customerName}<br>Move Counselor: ${counselor}</div>
<div style="width:40%; float:right; text-align:right;">Registration #: ${shipNumber}<br>Evaluation Date: ${evaluationDate}</div>
<div style="clear:both;"></div>
</div>

<input type="hidden" name="ADMIN">
<input type="hidden" name="ORIGIN">
<input type="hidden" name="DEST">
<input type="hidden" name="OVERALL">
<input type="hidden" name="RECOMMEND">

<div style="font:normal 12px Verdana !important; margin-top:20px;">
<span style="font:bold 19px Verdana !important;">How was your experience?</span><br>
It was our pleasure to serve you during your recent move which we trust was a success. 
Understanding the quality of our service delivery through our customer's eyes is extremely important to us and your feedback is essential in that process. 
Please take a moment to share your experience which will be used as part of our continued focus aimed at providing personalized services that keep our customers best interest at heart. 
Thank you for your valuable time. We wish you much success in your new location.
<br>

<div class="customHead">
Customer Service
</div>
<div style="font:normal 16px Arial; margin-bottom:20px;">
Counselor: ${counselor}</div>

<span style="font-weight:bold;">
How would you rate your experience with our Customer Service Team in terms of?
<ul style="margin:24px 30px;">
<li>Knowledge</li>
<li>Friendliness</li>
<li>Helpfulness</li>
<li>Responsiveness</li>
<li>Communication</li>
</ul>
<table class="plain" style="border:1px solid darkblue; width:100%;">
<tr>
  <td><input type="radio" name="custService" value="5"/></td>
  <td><input type="radio" name="custService" value="4"/></td>
	<td><input type="radio" name="custService" value="3"/></td>
	<td><input type="radio" name="custService" value="2"/></td>
	<td><input type="radio" name="custService" value="1"/></td>
</tr>
<tr>
  <td>Exceptional</td><td>Very Good</td><td>Average</td><td>Needs Improvement</td><td>Poor</td>
</tr>
</table>
<br>
Please note:  If you scored less than Average, please specify the issue and location:<br><br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment1 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>

</div>


<input type=hidden name=OA_NAME value="Asian  Tigers  K.C. Dat ( Chin"><input type=hidden name=OA_ID value="OM1452"><!-------------------------->
<!-------- ORIGIN  --------->
<!-------------------------->
<div class="customHead">
Origin Experience&#8212;(Pre-move survey, Packing &amp; Loading Services)
</div>
<div style="font:normal 16px Arial; margin-bottom:20px;">
Agent: ${oaName}</div>

<span style="font-weight:bold;">
How would you rate the services provided by our Supervisor &amp; Crew in terms of:</span>
<ul style="margin:24px 30px;">
<li>Punctuality</li>
<li>Efficiency/Skill</li>
<li>Care and consideration of your belongings</li>
<li>Appearance/Neatness</li>
<li>Overall Attitude/Friendliness</li>
<li>Communication</li>
</ul>
<table class="plain" style="border:1px solid darkblue; width:100%;">
<tr>
	<td><input type="radio" name="originAgent" value="5"/></td>
	<td><input type="radio" name="originAgent" value="4"/></td>
	<td><input type="radio" name="originAgent" value="3"/></td>
	<td><input type="radio" name="originAgent" value="2"/></td>
	<td><input type="radio" name="originAgent" value="1"/></td>
</tr>
<tr>
  <td>Exceptional</td><td>Very Good</td><td>Average</td><td>Needs Improvement</td><td>Poor</td>
</tr>
</table>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment2 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>

<input type=hidden name=DA_NAME value="Fry Wagner Moving & Storage Co"><input type=hidden name=DA_ID value="3344"><!------------------------>
<!--dest agent section  -->
<!------------------------>
<div class="customHead">
Destination Experience&#8212;(Unloading/Delivery, Damage Claim, Unpacking and Debris Removal)
</div>
<div style="font:normal 16px Arial; margin-bottom:20px;">
Agent: ${daName}</div>

<span style="font-weight:bold;">
How would you rate the services provided by our Supervisor &amp; Crew in terms of:</span>
<ul style="margin:24px 30px;">
<li>Punctuality</li>
<li>Efficiency/Skill</li>
<li>Care and consideration of your belongings</li>
<li>Appearance/Neatness</li>
<li>Overall Attitude/Friendliness</li>
<li>Communication</li>
</ul>
<table class="plain" style="border:1px solid darkblue; width:100%;">
<tr>
	<td><input type="radio" name="destinAgent" value="5"/></td>
	<td><input type="radio" name="destinAgent" value="4"/></td>
	<td><input type="radio" name="destinAgent" value="3"/></td>
	<td><input type="radio" name="destinAgent" value="2"/></td>
	<td><input type="radio" name="destinAgent" value="1"/></td>
</tr>
<tr>
  <td>Exceptional</td><td>Very Good</td><td>Average</td><td>Needs Improvement</td><td>Poor</td>
</tr>
</table>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment3 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>


<!------------------------>
<!----overall section ---->
<!------------------------>
<div class="customHead">
Overall Experience
</div>

<div style="font-weight:bold; margin:20px 0;">
Considering your entire move experience, how would you rate our services overall?
</div>

<table class="plain" style="border:1px solid darkblue; width:100%;">
<tr>
	<td><input type="radio" name="overall" value="5"/></td>
	<td><input type="radio" name="overall" value="4"/></td>
	<td><input type="radio" name="overall" value="3"/></td>
	<td><input type="radio" name="overall" value="2"/></td>
	<td><input type="radio" name="overall" value="1"/></td>
</tr>
<tr>
  <td>Exceptional</td><td>Very Good</td><td>Average</td><td>Needs Improvement</td><td>Poor</td>
</tr>
</table>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment4 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>

<!--------------------------->
<!--recommendation section--->
<!--------------------------->
<div class="customHead">
Recommendation
</div>

<div style="font-weight:bold; margin:20px 0;">
Would you recommend our services to a family member, friend or colleague?
</div>

<table class="plain" style="border:1px solid darkblue; width:100%;">
<tr>
	<td width="20%"><input type="radio" name="recommend" value="5"/></td>
	<td width="20%"><input type="radio" name="recommend" value="4"/></td>
	<td width="20%"><input type="radio" name="recommend" value="3"/></td>
	<td width="20%"><input type="radio" name="recommend" value="2"/></td>
	<td width="20%"><input type="radio" name="recommend" value="1"/></td>
</tr>
<tr>
  <td>Definitely Would</td><td>Would</td><td>Might</td><td>Might Not</td><td>Would Not</td>
</tr>
</table>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment5 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>

<!--------------------->
<!--feedback section--->
<!--------------------->
<div class="customHead">
Additional Feedback & Recognition
</div>

<div style="font-weight:bold; margin:20px 0;">
Please share any additional comments regarding your recent move experience.
</div>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Comments</div>
  <textarea name=comment6 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>

<div style="font-weight:bold; margin:20px 0;">
Please list the names and/or positions of any individual you believe should be recognized for exceptional performance along with any additional comments you wish to be shared.
</div>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;">Names<br>and<br>Comments</div>
  <textarea name=comment7 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>


<!--closing -->

<div style="text-align:center;">
<span style="font-style:italic; color:#003366;">
We truly appreciate your time and evaluation. If you would like a member of our team to contact you for additional assistance, please indicate below. Thank you.
</span><br><br>
I would like to be contacted for additional assistance: 
<input type=checkbox name=CONTACT_YES id=CONTACT_YES onclick="document.getElementById('CONTACT_NO').checked=false;document.getElementById('contact').style.display='block';"> Yes &nbsp;
<input type=checkbox name=CONTACT_NO id=CONTACT_NO onclick="document.getElementById('CONTACT_YES').checked=false;document.getElementById('contact').style.display='none';" checked> No<br>
<br>
<div id=contact style="display:none;">
Please indicate your preferred contact method below:<br>
<br>
Phone: <input type="text" name="contactPhone" id=phone size=20 maxlength="20"> &nbsp; &nbsp; Email: <input type=text name="contactEmail" id=email size=45><br>
</div>
</div>


<center>
<button type=submit style="margin:15px auto 5px auto; width:300px; height:30px;" onclick="return(verify_comments(this));" default>Submit Feedback</button>
</center>
</c:if>
</s:form>
<br>
</div>
</div>
<div class="suddpreFooter">
&copy;2016 <a style="text-decoration:none; color:#F6D009;" href="http://www.suddath.com">SUDDATH</a> &#8226;  1-888-799-5033 &#8226;  1-904-352-2577  &nbsp;
</div>
</body>
</html> 
