<%@ include file="/common/taglibs.jsp"%>  
<head>   
    <title><fmt:message key="contractList.title"/></title>   
    <meta name="heading" content="<fmt:message key='contractList.heading'/>"/>  

<style>
 span.pagelinks {
display:block; font-size:0.95em; margin-bottom:2px;!margin-bottom:0px; margin-top:-18px; !margin-top:-18px;padding:2px 0px; text-align:right; width:100%; !width:100%;}
form {margin-top:-40px;!margin-top:0px;}
div#main {margin:-5px 0 0;!margin:0px;}
/*jitendra*/

#content{margin-bottom:26px !important;}
/*end*/
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px;"  onclick="location.href='<c:url value="/editContract.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>   

      
<s:form id="contractForm" action='${empty param.popup?"searchContracts.html":"searchContracts.html?decorator=popup&popup=true"}' method="post" validate="true">
<div id="Layer1" style="width: 100%">  
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden name="truck.truckStatus" value="true"/>
	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	
</c:if>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-1px; "><span></span></div>
<div class="center-content">
<table class="table" width="100%" >
<thead>
<tr>
<th><fmt:message key="contract.contract"/></th>
<th><fmt:message key="contract.description"/></th>
<th><fmt:message key="contract.jobType"/></th>
<th><fmt:message key="partnerAccountRef.companyDivision"/></th>
<th>Active</th>
</tr></thead>	
<tbody>
<tr>
<td>
	<s:textfield name="contract.contract" required="true" cssClass="input-text" cssStyle="width:150px" />
</td>
<td>
	<s:textfield name="contract.description" required="true" cssClass="input-text" cssStyle="width:230px" />
</td>
<td>
	<s:select cssClass="list-menu" name="contract.jobType" list="%{job}" headerKey="" headerValue="" cssStyle="width:200px" />
</td>
<td>
	<s:select cssClass="list-menu" name="contract.companyDivision" list="%{companyDivis}" headerKey="" headerValue="" cssStyle="width:80px" />
</td>

<td>
<s:checkbox key="activeCheck" />
	
</td>
</tr>
<tr>
<td colspan="4"></td>
<td style="border-left: hidden;">
<s:submit cssClass="cssbutton" cssStyle="width:58px; !margin-bottom:10px;" method="searchContract" key="button.search"/>
<input type="button" class="cssbutton" value="Clear" style="width:58px;!margin-bottom: 10px;" onclick="clear_fields();"/>   
</td>
</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" /> 
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>Contract List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
</tr>
</tbody></table>
<s:set name="contracts" value="contracts" scope="request"/>
<display:table name="contracts" defaultsort="1" class="table" requestURI="" id="contractList" export="${empty param.popup}" pagesize="10" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
<c:if test="${empty param.popup}">
<display:column property="contract" sortable="true" titleKey="contract.contract" href="editContract.html" paramId="id" paramProperty="id"/>   
</c:if>
<c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="contract.contract"/>
</c:if>
<display:column property="description" sortable="true" titleKey="contract.description"/> 
<display:column property="jobType" sortable="true" titleKey="contract.jobType"/> 
<display:column property="companyDivision" sortable="true" title="Divn"/>
<display:column property="noOfCharge" sortable="true" title="#&nbsp;Charges" style="text-align:right" />  
<display:column property="begin" sortable="true" titleKey="contract.begin" format="{0,date,dd-MMM-yyyy}"/> 
<display:column property="ending" sortable="true" titleKey="contract.ending" format="{0,date,dd-MMM-yyyy}"/> 
<display:column property="updatedOn" sortable="true" titleKey="contract.updatedOn" format="{0,date,dd-MMM-yyyy}"/> 

<display:setProperty name="export.excel.filename" value="Contract List.xls"/>   
<display:setProperty name="export.csv.filename" value="Contract List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Contract List.pdf"/>  
</display:table>
<c:if test="${empty param.popup}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
</div>
</s:form>

<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['contractForm'].elements['contract.contract'].value = "";
		document.forms['contractForm'].elements['contract.description'].value = "";
		document.forms['contractForm'].elements['contract.jobType'].value = "";
		document.forms['contractForm'].elements['contract.companyDivision'].value = "";				
		document.forms['contractForm'].elements['activeCheck'].checked = false;
}
</script> 

<c:if test="${empty param.popup}"> 
<script type="text/javascript">   
    highlightTableRows("contractList");  
    Form.focusFirstElement($("contractForm"));
  try{
    if('<%=session.getAttribute("contractTypeSession")%>'=='null')
   	{
   		document.forms['contractForm'].elements['contract.contract'].value='';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("contractTypeSession")%>'!='null')
   	{
   		document.forms['contractForm'].elements['contract.contract'].value='<%=session.getAttribute("contractTypeSession")%>';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("descriptionSession")%>'=='null')
   	{
   		
   		document.forms['contractForm'].elements['contract.description'].value='';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("descriptionSession")%>'!='null')
   	{	
   		
   		document.forms['contractForm'].elements['contract.description'].value='<%=session.getAttribute("descriptionSession")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("jobTypeSession")%>'=='null')
   	{
   		document.forms['contractForm'].elements['contract.jobType'].value='';
   	}
   	}
   	catch(e){}
	try{
	if('<%=session.getAttribute("jobTypeSession")%>'!='null')
   	{
   		document.forms['contractForm'].elements['contract.jobType'].value='<%=session.getAttribute("jobTypeSession")%>';
   	} 
   	}
   	
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("companyDivisionSession")%>'=='null')
   	{
   		document.forms['contractForm'].elements['contract.companyDivision'].value='';
   	}
   	}
   	catch(e){}
	try{
	if('<%=session.getAttribute("companyDivisionSession")%>'!='null')
   	{
   		document.forms['contractForm'].elements['contract.companyDivision'].value='<%=session.getAttribute("companyDivisionSession")%>';
   	} 
   	}
   	
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("activeCheckSession")%>'=='null')
   	{
   		document.forms['contractForm'].elements['activeCheck'].checked = false;
   	}
	}
	catch(e){}
	try{
	if('<%=session.getAttribute("activeCheckSession")%>'=='true')
   	{
   		document.forms['contractForm'].elements['activeCheck'].checked = true;
   	} 
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("activeCheckSession")%>'=='false')
   	{
   		document.forms['contractForm'].elements['activeCheck'].checked = false;
   	}
   	}
   	catch(e){}
   try{
   	if('${activeCheck}'=='true')
   	{
   		document.forms['contractForm'].elements['activeCheck'].checked = true;
   	}
   	}
   	catch(e){}
</script>  
</c:if>
<script type="text/javascript">  
try{
	var companyDivision=document.forms['contractForm'].elements['contract.companyDivision'].value;
	if(companyDivision==''){
	 document.forms['contractForm'].elements['contract.companyDivision'].value='${companyCode}';
	}
}catch(e){}
</script> 