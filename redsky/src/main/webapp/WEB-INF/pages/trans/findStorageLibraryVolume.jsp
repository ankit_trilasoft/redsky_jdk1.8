<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>Volume List</title>   
    <meta name="heading" content="Volume List"/> 
     <style>
      #ajax_tooltipObj .ajax_tooltip_content  {
      width:auto;
      }
     </style>    
</head>   
<s:form id="VolumeForm" method="post" validate="true">  
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr> 
	<td align="left"><b>Volume List</b></td>
	<img align="right" class="openpopup" onclick="ajax_hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
<s:set name="storageLibraryVolumeList" value="storageLibraryVolumeList" scope="request"/>   
<display:table name="storageLibraryVolumeList" class="table" requestURI="" id="storageLibraryVolumeList" pagesize="5" style="width:200px;"> 
<display:column headerClass="containeralign" title="Volume<br>Cbm" style="width:50px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibraryVolumeList.volumeCbm}" /></div>
	</display:column>
	<display:column headerClass="containeralign" title="Used&nbsp;Vol<br>Cbm" style="width:55px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibraryVolumeList.usedVolumeCbm}" /></div>
	</display:column>
	<display:column headerClass="containeralign" title="Avail&nbsp;Vol<br>Cbm" style="width:55px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibraryVolumeList.availVolumeCbm}" /></div>
	</display:column>
</display:table>
</s:form>