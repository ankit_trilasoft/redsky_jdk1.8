
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerMobileAffiliatesResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSCustomerAffiliate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerMobileAffiliatesResult"
})
@XmlRootElement(name = "GetCustomerMobileAffiliatesResponse")
public class GetCustomerMobileAffiliatesResponse {

    @XmlElement(name = "GetCustomerMobileAffiliatesResult")
    protected ArrayOfMSSCustomerAffiliate getCustomerMobileAffiliatesResult;

    /**
     * Gets the value of the getCustomerMobileAffiliatesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSCustomerAffiliate }
     *     
     */
    public ArrayOfMSSCustomerAffiliate getGetCustomerMobileAffiliatesResult() {
        return getCustomerMobileAffiliatesResult;
    }

    /**
     * Sets the value of the getCustomerMobileAffiliatesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSCustomerAffiliate }
     *     
     */
    public void setGetCustomerMobileAffiliatesResult(ArrayOfMSSCustomerAffiliate value) {
        this.getCustomerMobileAffiliatesResult = value;
    }

}
