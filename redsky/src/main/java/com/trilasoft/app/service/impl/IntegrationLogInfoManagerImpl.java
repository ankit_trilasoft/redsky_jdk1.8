package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.IntegrationLogInfoDao;
import com.trilasoft.app.model.IntegrationLogInfo;
import com.trilasoft.app.service.IntegrationLogInfoManager;

public class IntegrationLogInfoManagerImpl  extends GenericManagerImpl <IntegrationLogInfo, Long> implements IntegrationLogInfoManager {
	IntegrationLogInfoDao integrationLogInfoDao;
	public IntegrationLogInfoManagerImpl(IntegrationLogInfoDao integrationLogInfoDao){
		super(integrationLogInfoDao);   
        this.integrationLogInfoDao = integrationLogInfoDao;
	}
	public String findSendMoveCloudStatus(String sessionCorpID ,String fileNumber){
		return integrationLogInfoDao.findSendMoveCloudStatus(sessionCorpID, fileNumber);
	}
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber){
		return integrationLogInfoDao.findSendMoveCloudStatusCrew(sessionCorpID, fileNumber);
	}
	public String findSendMoveCloudStatusClaim(String sessionCorpID ,String fileNumber){
		return integrationLogInfoDao.findSendMoveCloudStatusClaim(sessionCorpID, fileNumber);
	}
	public String findSendMoveCloudGlobalStatus(String sessionCorpID ,String fileNumber,String taskType){
		return integrationLogInfoDao.findSendMoveCloudGlobalStatus(sessionCorpID, fileNumber, taskType);
	}
}
