package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.HolidayMaintenance;

public interface HolidayMaintenanceDao extends GenericDao<HolidayMaintenance, Long>{
public List checkHoliDayList(String type,Date hDayDate,String corpID);
public List getHoliDayMaintenanceList(String corpID);
}
