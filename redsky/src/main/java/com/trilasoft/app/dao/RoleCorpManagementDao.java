package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.Role;

public interface RoleCorpManagementDao extends GenericDao<Role, Long> {
	public List<String> onlyRoles(String corpIDList);
	public List<Role> corpRoles(String corpIdList);
	public Role fetchRoleDetail(Long id,String corpID);
	public List<Role> searchRole(String name,String corpID,String category);
	public boolean checkRoleForExistance(String name, String corpID, String category);
}
