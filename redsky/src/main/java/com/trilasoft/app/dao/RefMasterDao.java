package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.BillingEmailDTO;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.UserDTO;

import java.util.*;

public interface RefMasterDao extends GenericDao<RefMaster, Long> {
	public List<RefMaster> findByParameter(String parameter, String searchCode, String searchDescription, String corpId, String status);

	public List<RefMaster> getList();

	public List<RefMaster> searchList(String parameter, String corpId,String nameonform);

	public Map<String, String> findByParameter(String corpId, String parameter);
	public Map<String, String> findByParameterWithBucket2(String corpId, String parameter);
	public String findDocCategoryByDocType(String sessionCorpID, String docType);

	// public Map<String, String> findUser(String corpId, String parameter);
	public List<RefMaster> findByParameterlist(String parameter, String corpId);

	public Map<String, String> findUser(String corpId, String parameter);
	public Map<String, String> findUserUGCORP(String parameter);
	

	public Map<String, String> findUserNotes(String corpId, String parameter);
	public List findClaimPerson(String corpID, String jobType);
	

	// public List findRole(String corpId);
	public List findRole(String corpId, String parameter);

	public List findByRoles(String corpID);

	public List searchUser(String userType, String username, String firstName, String lastName, String email, String supervisor,String enableUser,String parentAgent);

	public List findUserDetails(String corpId, String parameter);

	public List searchByCodeAndDescription(String searchCode, String searchDescription, String corpId);

	public List findMaximumId();

	public List<RefMaster> deletedByParameterAndCode(String parameter, String code, String corpId);

	public List testJoin();

	public Map<String, String> findDocumentList(String corpID, String Param);
	
	public Map<String, String> findDocumentListByCategory(String corpID, String Param,String cat);

	public List findAll(String corpID);

	public List findAccordingCorpId();

	public List<RefMaster> searchListWithoutState(String parameter, String corpId,String nameonform);

	public List<RefMaster> findByParameterlistWithoutState(String parameter, String corpId);

	public List<RefMaster> findByParameterWithoutState(String parameter, String searchCode, String searchDescription, String corpId, String status);

	public List test();

	public List<RefMaster> findByParameters(String corpId, String parameter);
	
	public Map<String,String> findByParameterCodeDesc(String corpId, String parameter);
	
	public List findCompanyList( String sessionCorpID);
	
	
	
	///gaurav 18/2010
	public List<RefMaster> findByParameters_status(String corpId, String parameter);

	public List getBucket(String code, String corpId);

	public List findOnlyCode(String code, String corpId);

	public Map<String, String> findByParameterPortCountry(String corpId, String parameter);

	public Map<String, String> findByParameterStatus(String parameter, String corpId, int statusNumber);

	public List isExisted(String code, String parameter, String corpId,String language);

	public List occupied(String parameter, String corpId);

	public List findCountryCode(String countryName, String sessionCorpID);
	public List findStateCode(String stateName, String sessionCorpID);

	public Map<String, String> findCountry(String corpId, String parameter);
	public Map<String, String> findContinentList(String corpId, String parameter);
	public List findCountryList(String countryName,String sessionCorpID);
	public List findContinent(String countryName,String sessionCorpID);
	public Map<String, String> findCountryForPartner(String corpId, String parameter);
	public List findCountryCodeList(String countryName,String sessionCorpID);
	public List militaryDesc(String code, String corpId);

	public Map<String, String> findByParameterOrderByCode(String corpId, String parameter);
	public Map<String, String> findByParameterOrderByFlex1(String corpId, String parameter);

	public void updateRefmasterStopDate(Date stopDate, int fieldLength, String bucket2, String bucket, Double number, String address1, String address2, String city, String state, String zip, String branch, String sessionCorpID, String code, Date updatedOn, String updatedBy);

	public Map<String, String> findState(String sessionCorpID);

	public Map<String, String> findByParameterWhareHouse(String corpId, String parameter);

	public Map<String, String> findGlType(String corpId);

	public List getSuperVisor(String corpId);

	public Map<String, String> findNoteSubType(String corpId, String noteType, String parameter);

	public List findNoteSubTypeList(String corpId, String noteType, String parameter);

	public List findRolesUser(Long id, String sessionCorpID);

	public List findUserRoles(String sessionCorpID, String name);

	public Map<String, String> findSaleConsultant(String corpId);

	public Map<String, String> findMapList(String corpID, String Param);
	
	public List getDescription(String originCountry, String parameter);
	public List<String>  getCustomerServiceAnsList(String corpid, String parameter);
	public Map<String, String> getCustomerServiceQuesList(String corpID, String parameter);
	public List<String> getCustomerServiceQuesFlex2List(String corpID, String parameter);
	public String findDivisionInBucket2(String corpId, String jobType);
	public Map<String, String> findCordinaor(String job, String parameter, String sessionCorpID);
	
	public List findCordinaorList(String custJobType, String parameter, String sessionCorpID);
	
	public List findJobByCompanyDivision(String compDivision, String sessionCorpID);
	
	public Map<String, String> findByParameterRelo(String corpId, String parameter);
    public List findJobByCompanyDivisionRelo(String compDivision, String sessionCorpID);
	
	public List findJobByCompanyDivisionReloServiceOrder(String compDivision, String sessionCorpID);  
	
	public Map<String, String> findByParameterJob(String corpId, String parameter);

	public Map<String, String> findByParameterJobRelo(String corpId, String parameter);
	public String findReloJob(String job, String sessionCorpID, String parameter);
	public Map<String, String> findByRelocationServices(String corpId, String parameter);
	public Map<String, String> findBySurveyQualityParameter(String corpId, String parameter);
	public Map<String, String> findServiceByJob(String job, String sessionCorpID, String parameter);
	public List findServiceByJob(String jobRelo, String sessionCorpID);
	public Map<String, String> findVatPercentList(String corpID, String parameter);
	public Map<String, String> findByflexValue(String corpId, String parameter);
	public String findCodeByParameter(String sessionCorpID, String param,String storageLibrayType);


	public Map<String, String> findByParameterCustomDoc(String corpId, String parameter, String county);
	
	public String findCountryCodeEU(String originCountryCode, String destinationCountryCode, String sessionCorpID);
	public List findByParameterFlex3(String corpId, String parameter);
	public Map<String, String> findCountryIso2(String corpId, String parameter);
	public Map<String, String> findByParameterFlex4(String corpId, String parameter);
	public Map<String, String> findByParameterService(String sessionCorpID,String parameter, String serviceGroup);
	public Map<String, String> findByParameter(String corpId, String parameter, String job);
	public Map<String, String> findByParameterReloParentAgent(String sessionCorpID, String parameter, String parentAgent);
	public Map<String, String> findByParameterASML(String corpId, String parameter);
	public Map<String, String> findByParameterWithOutASML(String corpId, String parameter);
	public Map<String, String> findDocumentListAccountPortal(String sessionCorpID, String Param);
	public Map<String, String> findByParameterWithoutParent(String corpID, String parameter);
	public Map<String, String> findByParameterNotCF(String sessionCorpID,String parameter);
	public List findAvaliableVolume(String defaulVolUnit,String containerSize,String sessionCorpID);
	public List typeOfWorkFlex1(String typeofWork, String parameter,String sessionCorpID);
    public Boolean getCostElementFlag(String corpID);
    public List findByParameterRadius(String sessionCorpID,String parameter);
    public Map<String, String> findDescByParameter(String sessionCorpID,String parameter);
    public List getTimetypeFlex1List(String actionCode, String parameter, String sessionCorpID);
    public Map<String, String>findWithDescriptionWithBucket2(String parameter, String sessionCorpID);
    public Map<String, String> findCodeOnleByParameter(String corpID, String parameter);
    public String getVatDescrCode(String externalCorpID, String parameter, String VatPercent);
	public String findByParameterHouse(String sessionCorpID, String warehouse, String parameter);
	public String getBucket2(String code, String corpId);
	public Map<String, String> findByParameteFlex2(String sessionCorpID, String parameter);
	public String getVatCodeDetail(String vatCode, String sessionCorpID,String vatParameter);
	public List findByRefParameterlist(String parameter, String sessionCorpID);
	public List<String> findUserType(String sessionCorpID, String parameter);
	public List findBillingEmail(String roleName, String sessionCorpID);
	public List findReportEmailList(String partnerCode, String sessionCorpID);
	public List findReportEmailViaCompanyDivision(String corpId,String bookingAgentCode);
	public Map<String, Map<String, String>> findByMultipleParameter(String corpId, String parameters);
	public Map<String, String> findByCategoryCode (String sessionCorpID, String parameter);
	public Map<String, String> findByCategoryCodeByFlex(String sessionCorpID, String extTaskCode,String parameter);
	public Map<String, String> findByParameterUTSIPayVat(String sessionCorpID, String parameters, String externalCorpID);
	public Map<String, String> findByParameterAgetntPayVat(String sessionCorpID, String parameters, String externalCorpID);
	public String findDivisionForReconcile(String corpID,String chargeCode,String regNum);
	public String checkInvoiceDateForWithReconcile(String corpID,String chargeCode,String RegNum);
	public Map<String, String> findByParameterUserGuideModule(String corpId, String parameter);
	public Map<String, String> findByProperty(String corpId, String... parameter);
	public List<UserDTO> getAllUser(String corpId, String roles);
	public Map<String, String> findByParameterAjax(String sessionCorpID,String parameter);
	public Map<String, String> findServiceByJobForlinkUpNotes(String job, String sessionCorpID, String parameter);
	public List<RefMasterDTO> getAllParameterValue(String corpId,String parameters);	
	public Map<String, String> findByAnswerParamSurveyQualityParameter(String corpId, String parameter);
	public Map<String, String> findByParameterVatRecGL(String sessionCorpID, String parameter);
	public Map<String, Map<String, String>> findByParameterQstVatGL(String sessionCorpID);
	public Map<String, Map<String, String>> findByParameterQstVatAmtGL(String sessionCorpID);
	public Map<String, String> findByParameterVatPayGL(String sessionCorpID, String parameter);
	public List findByParameterFlex5(String corpId, String parameter);
	public void deletedByParameterAndCodeList(String parameter, String code, String corpId);
	public Map<String, String> getFlex1MapByParameter(String corpId,String parameter);
	public Map<String, List> findReloServiceFieldMap(String serviceType, String corpId);
	public List customsDocList(String sessionCorpID);
	public List searchCustomDoc(String code , String description , String sessionCorpID);
	public List findCodeList();
	public Map<String, Map> findByParameterUTSIPayVatMap(String parameters, String sessionCorpID);
	public Map<String, Map> findByParameterAgetntPayVatMap(String parameters, String sessionCorpID);
	public Map<String, Map> findVatPercentListMap(String parameters);
	public List serviceTypeList(String corpId, String parameter );
	public String getSDHubWarehouseOperationalLimit(String sessionCorpID);
	public String findByCrTerms(String sessionCorpID, String jobNumber);
	public String surveyEmailListForRelo(String sessionCorpID,String parameters);
	public Map<String, String> findRefMasterFlex8Map(String sessionCorpID, String parameter);
	public Map<String, String> findRefMasterFlex7Map(String sessionCorpID, String parameter);
	public Map<String, String> findRefMasterFlex6Map(String sessionCorpID, String parameter);
	public Map<String, Map> findVatBillingGroupMap(String sessionCorpID, String parameter);
	public Map<String, String> findByParameterforVat(String corpId, String parameter);
	public Map<String, String> findByParameterHubWithWarehouse(String corpId, String parameter,Map<String, String> hub);
	public Map<String, String> findDocumentByJobList(String corpID, String Param,String job);
	public Map<String, String> findMapByJobList(String corpID, String Param,String job);
	public Map<String, String> findByParameterWithCurrency(String corpID, String parameter);
	public Map<String, Set<String>> getMandatoryFileMap(String sessionCorpID,String parameter);
	public Map<String, String> findDefaultStateIsActiveList(String bucket2, String corpId);
	public Map<String, String> findCordinaorForCmmDmm(String job, String parameter, String corpID);
	public Map<String, String> findCordinaorForCorpId(String job, String parameter, String corpID,String checkCorpId);
	public Map<String, String> findByParameterOnlyDescription(String sessionCorpID, String parameter);
	public List findByParameterFlex1(String sessionCorpID, String parameter);
	public Map<String, String> findEuvateDescWithFlex(String sessionCorpID,	String parameter);
	public Map<String, String> findByParentParameterValues(String sessionCorpID,	String parameter);
	public String getValueFromParameterControl(String sessionCorpID,String parameter);
	public List getParentParameterList(String sessionCorpID,String parameter,String instructionCode);
	public List getParentParameterListAjax(String sessionCorpID,String parameter,String serviceCode,String instructionCode);
	public List findOnlyCodeByOpenperiod(String parameter, String corpId);
	public String findBucket2Value(String code, String parameter);
	public Map<String, String> getCodeWithDecription(String sessionCorpID);
	public Map<String, String> findByParameterFromServiceorder(String corpId, String parameter, String job);
	public List findMaxIdByParameter(String parameter,String sessionCorpID);
	public List findFlex5ByDescription(String countryName,String sessionCorpID,String parameterName);
	public List getDescriptionBylanguage(String parameter,String language,String sessionCorpID);
	public Map<String, String> findByParameter_isactive(String corpId, String parameter, String job);
	public List getServiceTypeListByWarehouse(String corpId, String parameter, String wareHouse);
	public List getTaskPlannerListAjax(String parameter,String extTaskCode);
	public List getAssigneeListAjax(String parameter,String assignee);
	public String getCompanyUUID(String code,String parameter);
	public String findDescriptionByFlex5(String code,String parameter);
	public Map findCodeOnleByParameterCountry(String corpID, String parameter);
	//my code
	public String findPartnerVatBillingFlexCode(String corpId, String vatdescription);
	public String findPartnerVatBillingDescription(String corpId,String vatdescription);
	public String findPartnerVatBillingCode(String code, String corpId,String compDivis);
	public Map<String, String> findVatDescriptionList(String corpID, String parameter,String flexcode,String defaultVatDec,String partnerVatDec,String flagb);
	public List findAccountLineRecVatCodeDescription(String flexcode,String defaultVatDec,String partnerVatDec, String corpId,String flag);
	public List findAccountLinePayVatCodeDescription(String flexcode,String defaultVatDec, String corpId,String flag);
	public String findAccountLinePayVatBillingCode(String code, String corpId,String compDivis);
	public Map<String, String> findPayVatDescriptionList(String corpID, String parameter,String flexcode,String defaultVatDec,String flag);
	public List findPartnerVatCodeDescription(String flexcode,String defaultVatDec, String corpId,String flag);
	public Map<String, String> findPartnerVatDescriptionList(String corpID, String parameter,String flexcode,String defaultVatDec,String flagb);
	public Map<String, String> findPartnerVatDescription(String corpID, String parameter,String flexcode,String defaultVatDec,String flagb);
	public Map<String, String> findVatDescriptionListForPricing(String corpID, String parameter,String flexcode,String defaultVatDec,String partnerVatDec,String flagb);
	public Map<String, String> findPayVatDescription(String corpID, String parameter, String flexcode,String defaultVatDec,String flag);
	public Map<String, String> findPartnerRecVatDescriptionList(String corpID, String parameter);
	public Map<String, String> findRecVatDescriptionData(String corpID, String parameter);
	public Map<String, String> findPayVatDescriptionData(String corpID, String parameter);
	

}