package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.T20VisionLogInfo;

public interface T20VisionLogInfoDao extends GenericDao<T20VisionLogInfo, Long> {
	List findT20VisionLogList(String sessionCorpID);
	List searcht20VisionLogInfoList(String entryNumber,String invoiceNumber,String orderNumber,String orderNumberLine,String status,String sessionCorpID);
}
