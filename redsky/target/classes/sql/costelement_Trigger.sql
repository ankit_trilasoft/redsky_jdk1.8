delimiter $$

CREATE trigger redsky.trigger_add_history_costelement
BEFORE UPDATE on redsky.costelement
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'costelement.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;

    IF (OLD.costElement <> NEW.costElement ) THEN
      select CONCAT(fieldNameList,'costelement.costElement~') into fieldNameList;
      select CONCAT(oldValueList,OLD.costElement,'~') into oldValueList;
      select CONCAT(newValueList,NEW.costElement,'~') into newValueList;
  END IF;

   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'costelement.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;

   IF (OLD.recGl <> NEW.recGl ) THEN
      select CONCAT(fieldNameList,'costelement.recGl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.recGl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.recGl,'~') into newValueList;
  END IF;

   IF (OLD.payGl <> NEW.payGl ) THEN
      select CONCAT(fieldNameList,'costelement.payGl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payGl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payGl,'~') into newValueList;
  END IF;

  
   IF (OLD.reportingAlias <> NEW.reportingAlias ) THEN
      select CONCAT(fieldNameList,'costelement.reportingAlias~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportingAlias,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportingAlias,'~') into newValueList;
  END IF;


 CALL add_tblHistory (OLD.id,"costelement", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;