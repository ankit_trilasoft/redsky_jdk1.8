package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   
import com.trilasoft.app.model.Custom;   
  
import java.util.List;   
  
public interface CustomDao  extends GenericDao<Custom, Long> {
	public List findBySO(Long shipNumber, String corpId);
	public List countBondedGoods(Long soId, String sessionCorpID);
	public List countForTicket(Long ticket, Long serviceOrderId, String movement, String sessionCorpID);
	public List customsInList(Long sid, String movement);
	public int checkCustomsId(Long transactionId, String movement, String sessionCorpID);
	public Long findRemoteCustom(Long networkId, Long serviceOrderId);
	public void deleteNetworkCustom(Long serviceOrderId, Long networkId);
	public List getTransactionIdCount(String sessionCorpID,Long customId);
}  
