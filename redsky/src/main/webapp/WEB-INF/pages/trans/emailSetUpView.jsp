<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Email&nbsp;Tracker&nbsp;Detail</title>   
    <meta name="heading" content="Email&nbsp;Tracker&nbsp;Detail"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
	.textareaUpper {background-color: #E3F1FE; border:1px solid #ABADB3;color:#000000;font-family: arial,verdana;font-size: 12px;text-decoration:none;}	
	</style>
 		<script language="javascript" type="text/javascript">
			<%@ include file="/common/formCalender.js"%>
		</script> 
	   
	    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
		<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	</head>
	<div id="newmnav">
		  <ul>
		     <li id="newmnav1" style="background:#FFF"><a class="current"><span>Email&nbsp;Tracker&nbsp;Detail</span></a></li>
		     <li><a href="emailtracker.html?true&corpID=${sessionCorpID}"><span>Email&nbsp;Tracker</span></a></li>
	     </ul>
	</div>
	<div class="spn"></div>
	<s:form id="emailSetUpForm" action="saveEmailTracker" onsubmit="" method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="emailSetup.id" />
	<s:hidden name="id" value="${emailSetup.id}"/> 
	<s:hidden name="emailSetup.retryCount" />
	<s:hidden name="emailSetup.attchedFileLocation" />
	<s:hidden name="emailSetup.corpId" />
	<s:hidden name="emailSetup.signaturePart" />
	<s:hidden name="emailSetup.fileNumber" />
	<s:hidden name="emailSetup.module" />
	<s:hidden name="emailSetup.saveAs" />
	<s:hidden name="emailSetup.lastSentMailBody" />
	
	<c:if test="${not empty emailSetup.emailUploadedOn}">
	<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="emailSetup.emailUploadedOn"/></s:text>
	<td><s:hidden cssClass="input-text" id="emailUploadedOn" name="emailSetup.emailUploadedOn" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:162px"  onfocus="changeStatus();"/>
	</c:if>
	<c:if test="${empty emailSetup.emailUploadedOn}">
	<td><s:hidden cssClass="input-text" id="emailUploadedOn" name="emailSetup.emailUploadedOn" cssStyle="width:65px" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
	</c:if>
	
	<div id="content" align="center">
	<div id="liquid-round">
    <div class="top" style="!margin-top:2px;"><span></span></div>
    <div class="center-content">    	
	<table cellspacing="1" cellpadding="2" border="0" class="no-mp">
	<tr>
	<td class="listwhitetext" align="right">Recipient To</td><td><s:textfield  cssClass="input-text" name="emailSetup.recipientTo" cssStyle="width:185px;" /></td>
	<td class="listwhitetext" align="right" width="113">Recipient CC</td><td><s:textfield  cssClass="input-text" name="emailSetup.recipientCc" cssStyle="width:185px;" /></td>
	</tr>
	<tr>
	<td class="listwhitetext" align="right">Recipient BCC</td><td><s:textfield  cssClass="input-text" name="emailSetup.recipientBcc" cssStyle="width:185px;" /></td>
	<td class="listwhitetext" align="right">Date Sent</td><c:if test="${not empty emailSetup.dateSent}">
	<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="emailSetup.dateSent"/></s:text>
	<td><s:textfield cssClass="input-text" id="dateSent" name="emailSetup.dateSent" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:162px" maxlength="11"   onfocus="changeStatus();"/>
	<c:if test="${emailSetup.emailStatus=='SaveForEmail'}">
	<img id="dateSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
 	</c:if>
	<c:if test="${empty emailSetup.dateSent}">
	<td><s:textfield cssClass="input-text" id="dateSent" name="emailSetup.dateSent" cssStyle="width:65px" maxlength="11"  onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
	<img id="dateSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	</tr>
	<tr><td class="listwhitetext" align="right">Subject</td><td colspan="3"><s:textfield  cssClass="input-text" name="emailSetup.subject" cssStyle="width:500px;" /></td></tr>	
	<tr><td class="listwhitetext" align="right">Body</td><td colspan="3"><s:textarea  cssClass="textarea" name="emailSetup.body" cssStyle="width:500px;height:115px"/></td></tr>
	<tr><td class="listwhitetext" align="right">Signature</td><td colspan="3"><s:textfield  cssClass="input-text" name="emailSetup.signature" cssStyle="width:500px;" /></td></tr>
	<tr><td class="listwhitetext" align="right">Email&nbsp;Status</td><td colspan="3"><s:textfield  cssClass="input-text" name="emailSetup.emailStatus" cssStyle="width:500px;" /></td></tr>
    </table>
    
	<table style="margin:0px;padding:0px;width:528px;margin-top:-2px;margin-left:56px;"  cellspacing="3" cellpadding="1" border="0">	
			<tr>					
			<td valign="top" style="border:none;padding:0px 0px 0px 17px;">
			<div style="max-height:250px;overflow-y:auto;border-bottom:1px solid #e0e0e0;">							
			<table class="table" style="width:100%; margin:0px;">
				<thead>
					<tr>
						<th width="15" style="height:18px !important;">#</th>
						<th style="min-width: 150px;height:18px !important;">Attached File's</th>
					</tr>
				</thead>
				<tbody>
						<c:if test="${not empty attachedFileNameList}">
							<c:set var = "replacedAttachedFileList" value = "${attachedFileNameList}"/>
							<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
							<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
							<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem" varStatus="rowCounter">
								<tr>
								<td>${rowCounter.count}</td>
									<td>
									<span style="font-family: arial,verdana;font-size: 12px;height:15px;text-decoration: none;"><c:out value="${attachedFileItem}"></c:out></span>
									</td>
								</tr>
							</c:forTokens>
						</c:if>
				</tbody>
			</table>
			</div>
			</td>
			</tr>
	</table>	
	</div><div class="bottom-header" style="margin-top:40px;"><span></span></div></div></div>
	<table>
	<tr>
	<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='proposalManagement.createdOn' /></b></td>
					<td style="width:130px"><fmt:formatDate
					var="customerFileCreatedOnFormattedValue"
					value="${emailSetup.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="emailSetup.createdOn"
					value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate
					value="${emailSetup.createdOn}" pattern="${displayDateTimeFormat}" /></td>
					
					<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message
					key='proposalManagement.createdBy' /></b></td>
				<c:if test="${not empty emailSetup.id}">
					<s:hidden name="emailSetup.createdBy" />
					<td style="width:65px"><s:label name="createdBy"
						value="%{emailSetup.createdBy}" /></td>
				</c:if>
				
				<c:if test="${empty emailSetup.id}">
					<s:hidden name="emailSetup.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>
				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='proposalManagement.updatedOn' /></b></td>
					<td style="width:130px"><fmt:formatDate
					var="customerFileCreatedOnFormattedValue"
					value="${emailSetup.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="emailSetup.updatedOn"
					value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate
					value="${emailSetup.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
					
					<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message
					key='proposalManagement.updatedBy' /></b></td>
				<c:if test="${not empty emailSetup.id}">
					<s:hidden name="emailSetup.updatedBy" />
					<td style="width:65px"><s:label name="createdBy"
						value="%{emailSetup.updatedBy}" /></td>
				</c:if>
				
				<c:if test="${empty emailSetup.id}">
					<s:hidden name="emailSetup.updatedBy"
						value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="updatedBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>
					
			</tr>
			</table>
			<table>
			<tr>
			<c:if test="${emailSetup.emailStatus=='SaveForEmail'}">
			<td><s:submit cssClass="cssbuttonA" method="save" key="button.save" theme="simple"/></td>
			<td><s:reset cssClass="cssbutton1" key="Reset" /></td>
			</c:if>	
			</tr>
			</table>
	</s:form>
	<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">
<c:if test="${emailSetup.emailStatus!='SaveForEmail'}">
var elementsLen=document.forms['emailSetUpForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['emailSetUpForm'].elements[i].type=='text'){
				document.forms['emailSetUpForm'].elements[i].readOnly =true;
				document.forms['emailSetUpForm'].elements[i].className = 'input-textUpper';
				
			}else if(document.forms['emailSetUpForm'].elements[i].type=='textarea'){
				document.forms['emailSetUpForm'].elements[i].readOnly =true;
				document.forms['emailSetUpForm'].elements[i].className = 'textareaUpper';
			}
			
			else
			{
				document.forms['emailSetUpForm'].elements[i].disabled=true;
			}
	}
</c:if>
</script>
