package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.appfuse.model.User;
import org.appfuse.model.Role;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;
import java.text.SimpleDateFormat;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Costing;
import com.trilasoft.app.model.CreditCard;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;

import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CreditCardManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class CreditCardAction extends BaseAction implements Preparable {

	
	 	private CreditCardManager creditCardManager;
		
	    private CreditCard creditCard;
	    private List creditCards;
	    private Long id;
	    private List ls;
	    private Billing billing;
	    private BillingManager billingManager;

		private ServiceOrder serviceOrder;
		private  TrackingStatus trackingStatus;
		private TrackingStatusManager trackingStatusManager;
		private  Miscellaneous  miscellaneous;
		private MiscellaneousManager miscellaneousManager ;
	    private String sessionCorpID;
	    private  String validate;
	    private  String userRole =new String();;
	    private String replacedString = "XXXX-XXXX-XXXX-";
	    private   String firstSub = new String();
	    private   String lastNumber = new String();
	    private   String number = new String();
	    private  StringBuffer   builders = new StringBuffer ();
	    private String numberPattern = "(\\d{4}-)?(\\d{3}-)?\\d{4}";
	    private Map<String, String> relocationServices;
	    private Company company;
		private CompanyManager companyManager;
		private String voxmeIntergartionFlag;
		private String oiJobList;
		public Company getCompany() {
			return company;
		}


		public CompanyManager getCompanyManager() {
			return companyManager;
		}


		public String getVoxmeIntergartionFlag() {
			return voxmeIntergartionFlag;
		}


		public void setCompany(Company company) {
			this.company = company;
		}


		public void setCompanyManager(CompanyManager companyManager) {
			this.companyManager = companyManager;
		}


		public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
			this.voxmeIntergartionFlag = voxmeIntergartionFlag;
		}
		private List primaryFlagStatus = new ArrayList();
		Date currentdate = new Date();
		static final Logger logger = Logger.getLogger(CreditCardAction.class);

		   
	     
	    public CreditCardAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
	        Set<Role> roles  = user.getRoles();
	        
	        Role role=new Role();
	        Iterator it = roles.iterator();
	        

	        while(it.hasNext()) {
	        	role=(Role)it.next();
	        	userRole = role.getName();
	        	if(userRole.equalsIgnoreCase("ROLE_FINANCE") || userRole.equalsIgnoreCase("ROLE_BILLING")){
	        		userRole=role.getName();
	        		break;
	        	}
	        	
	        }
		}

	    
	    public void prepare() throws Exception {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	getComboList(sessionCorpID);
	    	company=companyManager.findByCorpID(sessionCorpID).get(0);
            if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }

			if(company!=null){
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
					}
				}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			///primaryFlagStatus = new ArrayList();
			}
		private long IdMax;
	    
	    
		public long getIdMax() {
			return IdMax;
		}

		public void setIdMax(long idMax) {
			IdMax = idMax;
		}

		public ServiceOrder getServiceOrder() {
			return serviceOrder;
		}

		public void setServiceOrder(ServiceOrder serviceOrder) {
			this.serviceOrder = serviceOrder;
		}

		public void setBillingManager(BillingManager billingManager) {
			this.billingManager = billingManager;
		}

		public void setBilling(Billing billing) {
			this.billing = billing;
		}
		
		public CreditCard getCreditCard() {
			return creditCard;
		}
		public void setCreditCard(CreditCard creditCard) {
			this.creditCard = creditCard;
		}
		public CreditCardManager getCreditCardManager() {
			return creditCardManager;
		}
		public void setCreditCardManager(CreditCardManager creditCardManager) {
			this.creditCardManager = creditCardManager;
		}

		
		public List getCreditCards() {
			return creditCards;
		}
		public void setCreditCards(List creditCards) {
			this.creditCards = creditCards;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public List getLs() {
			return ls;
		}
		public void setLs(List ls) {
			this.ls = ls;
		}

		@SkipValidation
	    public String List() { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder=serviceOrderManager.get(id);
	    	trackingStatus=trackingStatusManager.get(id);
    		miscellaneous = miscellaneousManager.get(id);  
    		  customerFile=serviceOrder.getCustomerFile();
	    	String shipNumber=serviceOrder.getShipNumber();
			creditCards=creditCardManager.findByShipNumber(shipNumber);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
	        return SUCCESS;   
	    }
		
		@SkipValidation
	    public String cancel() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS; 
		}
		
	    
	    public int flag;

		private Long sid;

		private ServiceOrderManager serviceOrderManager;

		private String primaryFlags;

		private String primaryFlagId;

		private  CustomerFile customerFile;

		private CustomerFileManager customerFileManager;

		private Long customerFileId;


	    public CustomerFile getCustomerFile() {
			return (customerFile!=null ?customerFile:customerFileManager.get(customerFileId));
		}

		public void setCustomerFile(CustomerFile customerFile) {
			this.customerFile = customerFile;
		}

		public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
			this.serviceOrderManager = serviceOrderManager;
		}

		public Long getSid() {
			return sid;
		}

		public void setSid(Long sid) {
			this.sid = sid;
		}
		
		public String edit() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//getComboList(sessionCorpID);
			
			if (id != null)
	          { 
				creditCard = creditCardManager.get(id);
				billing = creditCard.getBilling();
	  			serviceOrder=serviceOrderManager.get(sid);
	  			trackingStatus=trackingStatusManager.get(sid);
	    		miscellaneous = miscellaneousManager.get(sid);  
	    		customerFile=serviceOrder.getCustomerFile();
	    		 getNotesForIconChange(creditCard.getId());
	          }
	         else 
	           {   
	        	    creditCard = new CreditCard();
	        	 	creditCard.setCreatedOn(new Date());
	        	 	creditCard.setUpdatedOn(new Date());
			   		serviceOrder=serviceOrderManager.get(sid);
			   		billing=billingManager.get(sid);
			   		trackingStatus=trackingStatusManager.get(sid);
		    		miscellaneous = miscellaneousManager.get(sid);  
		    		customerFile=serviceOrder.getCustomerFile();
		    		//creditCard.setShipNumber(billing.getShipNumber());
			   		creditCard.setCcType(billing.getBillTo1Point());
			   		
		  	 	}   
			  creditCard.setCorpID(sessionCorpID);
			 
			  
			primaryFlagStatus=creditCardManager.getPrimaryFlagStatus(serviceOrder.getShipNumber(), sessionCorpID);
			  if(!primaryFlagStatus.isEmpty()){
				  Iterator it= primaryFlagStatus.iterator();
					while(it.hasNext()){
						Object [] row=(Object[])it.next();
						primaryFlagId=row[0].toString();
						primaryFlags=row[1].toString();
					}
			}
			  if(primaryFlagStatus.isEmpty()){
				  primaryFlags="show";
			  }
			  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	       
	      }
		
		
		public String save() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			billing = billingManager.get(sid);
  			serviceOrder=serviceOrderManager.get(sid);
  			trackingStatus=trackingStatusManager.get(sid);
    		miscellaneous = miscellaneousManager.get(sid); 
			creditCard.setBilling(billing);
			customerFile=serviceOrder.getCustomerFile();
			String month=creditCard.getExpireMonth();
			String lastDateOfMonth=new String();
			/*if(month!=null && (month.equals("01") || month.equals("03") || month.equals("05") 
					|| month.equals("07") || month.equals("08") || month.equals("10") || month.equals("12")
					)){
				lastDateOfMonth="31";
			}if(month!=null && (month.equals("04") || month.equals("06") || month.equals("09") || month.equals("11"))){
				lastDateOfMonth="30";
			}
			if(month!=null && month.equals("02")){
				lastDateOfMonth="28";
			}*/
			String year=creditCard.getExpireYear();						
			String temp=year+"-"+month+"-"+"01";			
			SimpleDateFormat df =  new SimpleDateFormat("yyyy-MM-dd");
			Date expireDate = df.parse(temp);
			creditCard.setExpiryDate(expireDate);
			Date currentDate=creditCard.getCcApproved();
			boolean isNew = (creditCard.getId() == null);
	        if(isNew)
	        {
	  		   	creditCard.setCreatedOn(new Date());
	        	
	        }
		     creditCard.setCcNumber1("XXXXXXXXXXXX-"+creditCard.getCcNumber().substring(creditCard.getCcNumber().length()-4, creditCard.getCcNumber().length()));
	        //creditCard.setCcNumber1(creditCard.getCcNumber());
	        creditCard.setUpdatedOn(new Date());
	        creditCard.setUpdatedBy(getRequest().getRemoteUser());
	        creditCard=creditCardManager.save(creditCard);
	       // creditCard = creditCardManager.get(creditCard.getId());
	       primaryFlagStatus=creditCardManager.getPrimaryFlagStatus(serviceOrder.getShipNumber(), sessionCorpID);
        	if(!primaryFlagStatus.isEmpty()){
				  Iterator it= primaryFlagStatus.iterator();
					while(it.hasNext()){
						Object [] row=(Object[])it.next();
						primaryFlagId=row[0].toString();
						primaryFlags=row[1].toString();
					}
			}
			  if(primaryFlagStatus.isEmpty()){
				  primaryFlags="show";
			  }
	        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
	        String key = (isNew) ? "creditCard.added" : "creditCard.updated";
	        saveMessage(getText(key));
	        }
	 	   //getComboList(sessionCorpID);
	 	    
	 		  
	        	getNotesForIconChange(creditCard.getId());
	 	  validate = "OK";
	        	/*serviceOrder=serviceOrderManager.get(sid);
		    	trackingStatus=trackingStatusManager.get(sid);
	    		miscellaneous = miscellaneousManager.get(sid);  
	    		  customerFile=serviceOrder.getCustomerFile();
		    	String shipNumber=serviceOrder.getShipNumber();*/
				creditCards=creditCardManager.findByShipNumber(serviceOrder.getShipNumber());
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        	return SUCCESS;   
	        }
			
			
	    //} 
	    
		
	    // For getcombobox
	    
		private RefMasterManager refMasterManager;

		private NotesManager notesManager;

		private String countCreditCardNotes;
		
	    private  Map<String, String> paytype =new LinkedHashMap<String, String>();;
	    
	    public String getComboList(String corpID){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	relocationServices=refMasterManager.findByRelocationServices(corpID, "RELOCATIONSERVICES");
	    	Map<String,String>map = refMasterManager.findByParameter(corpID, "PAYTYPE");
	    	Set set=map.entrySet();
	    	for(Object obj:set)
	    	{
	    		Map.Entry<String, String> entry=(Map.Entry<String, String>)obj;
	    		
	    		if(entry.getKey().equalsIgnoreCase("VC")|| entry.getKey().equalsIgnoreCase("AE")||entry.getKey().equalsIgnoreCase("MC")||entry.getKey().equalsIgnoreCase("DD"))
	    		{
	    			paytype.put(entry.getKey(), entry.getValue());
	    		}
	    		else
	    		{
	    			
	    		}
	    	}
	  	   //paytype = refMasterManager.findByParameter(corpID, "PAYTYPE");
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	 	   return SUCCESS;
	     }
	     

	 	// ----------------------------
	      public  Map<String, String> getPaytype() {
	 		return paytype;
	 	}

		public void setRefMasterManager(RefMasterManager refMasterManager) {
			this.refMasterManager = refMasterManager;
		}
		public void setNotesManager(NotesManager notesManager) {
	        this.notesManager = notesManager;
	    }
		
		private Long creditCardId;
		@SkipValidation 
	    public String getNotesForIconChange(Long id) { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//System.out.println("\n\n\n\n\n\n\n creditCardId---->"+id);
	    	List ma = notesManager.countCreditCardNotes(id); 	
	    	if( ma.isEmpty() ){
	    		countCreditCardNotes = "0";
			}else{
				countCreditCardNotes = ((ma).get(0)).toString() ;
			}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;
          }

		public String getCountCreditCardNotes() {
			return countCreditCardNotes;
		}

		public void setCountCreditCardNotes(String countCreditCardNotes) {
			this.countCreditCardNotes = countCreditCardNotes;
		}
		
		

		public  String getFirstSub() {
			return firstSub;
		}

		public  void setFirstSub(String firstSub) {
			this.firstSub = firstSub;
		}

		public  String getLastNumber() {
			return lastNumber;
		}

		public  void setLastNumber(String lastNumber) {
			this.lastNumber = lastNumber;
		}

		

		public String getNumberPattern() {
			return numberPattern;
		}

		public void setNumberPattern(String numberPattern) {
			this.numberPattern = numberPattern;
		}

		public String getReplacedString() {
			return replacedString;
		}

		public void setReplacedString(String replacedString) {
			this.replacedString = replacedString;
		}

		public  String getNumber() {
			return number;
		}

		public  void setNumber(String number) {
			this.number = number;
		}

		public  StringBuffer getBuilders() {
			return builders;
		}

		public  void setBuilders(StringBuffer builders) {
			this.builders = builders;
		}
		public String maskNumber()
		{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String numbers;
			String fNumber;
			String sNumber;
			String tNumber;
			String foNumber;
			String sub=creditCard.getCcNumber();
			String fSub;
			String sSub;
			String tSub;
			String foSub;
			//numbers=getNumber();
			fNumber=number.substring(0, number.length()-14);
			sNumber=number.substring(number.length()-14, number.length()-9);
			tNumber=number.substring(number.length()-9, number.length()-4);
			foNumber=number.substring(number.length()-4,number.length());
			fSub=sub.substring(0, sub.length()-14);
			sSub=sub.substring(sub.length()-14, sub.length()-9);
			tSub=sub.substring(sub.length()-9, sub.length()-4);
			foSub=sub.substring(sub.length()-4, sub.length());
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			if(fNumber.equalsIgnoreCase(fSub)){
				if(sNumber.equalsIgnoreCase(sSub))
				{
					if(tNumber.equalsIgnoreCase(tSub))
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fNumber+sNumber+tNumber+foNumber);
							return fNumber+sNumber+tNumber+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fNumber+sNumber+tNumber+foSub);
							return fNumber+sNumber+tNumber+foSub;
						}
						
					}
					else
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fNumber+sNumber+tSub+foNumber);
							return fNumber+sNumber+tSub+foNumber;
							
						}
						else
						{
							//creditCard.setCcNumber(fNumber+sNumber+tSub+foSub);
							return fNumber+sNumber+tSub+foSub;
						}
					}
				}
				else
				{
					if(tNumber.equalsIgnoreCase(tSub))
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fNumber+sSub+tNumber+foNumber);
							return fNumber+sSub+tNumber+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fNumber+sSub+tNumber+foSub);
							return fNumber+sSub+tNumber+foSub;
						}
						
					}
					else
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fNumber+sSub+tSub+foNumber);
							return fNumber+sSub+tSub+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fNumber+sSub+tSub+foSub);
							return fNumber+sSub+tSub+foSub;
						}
					}
				}
				
			}
			else
			{
				if(sNumber.equalsIgnoreCase(sSub))
				{
					if(tNumber.equalsIgnoreCase(tSub))
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fSub+sNumber+tNumber+foNumber);
							return fSub+sNumber+tNumber+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fSub+sNumber+tNumber+foSub);
							return fSub+sNumber+tNumber+foSub;
						}
						
					}
					else
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fSub+sNumber+tSub+foNumber);
							return fSub+sNumber+tSub+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fSub+sNumber+tSub+foSub);
							return fSub+sNumber+tSub+foSub;
						}
					}
				}
				else
				{
					if(tNumber.equalsIgnoreCase(tSub))
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fSub+sSub+tNumber+foNumber);
							return fSub+sSub+tNumber+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fSub+sSub+tNumber+foSub);
							return fSub+sSub+tNumber+foSub;
						}
						
					}
					else
					{
						if(foNumber.equalsIgnoreCase(foSub))
						{
							//creditCard.setCcNumber(fSub+sSub+tSub+foNumber);
							return fSub+sSub+tSub+foNumber;
						}
						else
						{
							//creditCard.setCcNumber(fSub+sSub+tSub+foSub);
							return fSub+sSub+tSub+foSub;
						}
					}
				} 	
			}
	}
		
	public String replaceString(String replases)
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		firstSub=replases.substring(0, replases.length()-4);
	   

	    lastNumber=replases.substring( replases.length()-4,replases.length());
	 
	    builders.append(number.replaceAll(firstSub, replacedString));
	    //builders.append(lastNumber);
	    
	    //creditCard.setCcNumber(replases.replaceAll(firstSub, replacedString));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return replases.replaceAll(firstSub, replacedString);
	}

	public  String getUserRole() {
		return userRole;
	}

	public  void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
	private String gotoPageString;
	private String validateFormNav;
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getGotoPageString() {

	return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	this.gotoPageString = gotoPageString;

	}
	 
	public String saveOnTabChange() throws Exception {
	    //if (option enabled for this company in the system parameters table then call save) {
			validateFormNav = "OK";
	        String s = save();    // else simply navigate to the requested page)
	        return gotoPageString;
	}

	public  void setPaytype(Map<String, String> paytype) {
		this.paytype = paytype;
	}

	public  Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(sid);
	}

	public  void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public  TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(sid);
	}

	public  void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getPrimaryFlagId() {
		return primaryFlagId;
	}

	public void setPrimaryFlagId(String primaryFlagId) {
		this.primaryFlagId = primaryFlagId;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}


	public List getPrimaryFlagStatus() {
		return (primaryFlagStatus!=null && !primaryFlagStatus.isEmpty())?primaryFlagStatus:creditCardManager.getPrimaryFlagStatus(serviceOrder.getShipNumber(), sessionCorpID);
		}


	public void setPrimaryFlagStatus(List primaryFlagStatus) {
		this.primaryFlagStatus = primaryFlagStatus;
	}


	public String getPrimaryFlags() {
		return primaryFlags;
	}


	public void setPrimaryFlags(String primaryFlags) {
		this.primaryFlags = primaryFlags;
	}


	public Long getCreditCardId() {
		return creditCardId;
	}


	public void setCreditCardId(Long creditCardId) {
		this.creditCardId = creditCardId;
	}


	public Long getCustomerFileId() {
		return customerFileId;
	}


	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}


	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}


	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}


	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}


	public String getOiJobList() {
		return oiJobList;
	}


	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	
}


