delimiter $$

CREATE trigger redsky.trigger_add_history_partneraccountref
BEFORE UPDATE on redsky.partneraccountref
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";




   IF (OLD.partnercode <> NEW.partnercode ) THEN
      select CONCAT(fieldNameList,'partneraccountref.partnercode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnercode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnercode,'~') into newValueList;
  END IF;


     IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'partneraccountref.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;


     IF (OLD.accountCrossReference <> NEW.accountCrossReference ) THEN
      select CONCAT(fieldNameList,'partneraccountref.accountCrossReference~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountCrossReference,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountCrossReference,'~') into newValueList;
  END IF;


     IF (OLD.corpid <> NEW.corpid ) THEN
      select CONCAT(fieldNameList,'partneraccountref.corpid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
  END IF;


   

     IF (OLD.refType <> NEW.refType ) THEN
      select CONCAT(fieldNameList,'partneraccountref.refType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.refType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.refType,'~') into newValueList;
  END IF;






CALL add_tblHistory (OLD.id,"partneraccountref", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;