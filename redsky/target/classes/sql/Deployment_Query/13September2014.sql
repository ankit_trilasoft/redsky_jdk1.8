// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `MMG_originAgentCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `TAC_timeAuthorized` , 
ADD COLUMN `MMG_originAgentName` VARCHAR(225) NULL DEFAULT NULL  AFTER `MMG_originAgentCode` , 
ADD COLUMN `MMG_originAgentPhone` VARCHAR(65) NULL DEFAULT NULL  AFTER `MMG_originAgentName` ,
ADD COLUMN `MMG_originAgentEmail` VARCHAR(65) NULL DEFAULT NULL  AFTER `MMG_originAgentPhone`,
ADD COLUMN `MMG_destinationAgentCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `MMG_originAgentEmail` , 
ADD COLUMN `MMG_destinationAgentName` VARCHAR(225) NULL DEFAULT NULL  AFTER `MMG_destinationAgentCode` , 
ADD COLUMN `MMG_destinationAgentPhone` VARCHAR(45) NULL DEFAULT NULL  AFTER `MMG_destinationAgentName` , 
ADD COLUMN `MMG_destinationAgentEmail` VARCHAR(65) NULL DEFAULT NULL  AFTER `MMG_destinationAgentPhone`;

ALTER TABLE `redsky`.`sqlextract` MODIFY COLUMN `email` VARCHAR(500);

ALTER TABLE `redsky`.`app_user` 
ADD COLUMN `followUpEmailAlert` bit(1) DEFAULT  b'0' AFTER `sortOrderForFileCabinet`;

//As per discussion with ashish sir
delete from parametercontrol where parameter='UTS_Company_Type';

delete from refmaster where parameter='UTS_COMPANY_TYPE';
//

//As per mamta mail

select concat("insert into redsky.corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId) values ('2','component.standard.claimTab','",corpid,"','",concat(corpid,'_SETUP'),"',now(),'",concat(corpid,'_SETUP'),"',now(),'component.standard.claimTab');")
FROM company ;

select concat("insert into redsky.corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId) values ('2','component.standard.cPortalActivation','",corpid,"','",concat(corpid,'_SETUP'),"',now(),'",concat(corpid,'_SETUP'),"',now(),'component.standard.cPortalActivation');")
FROM company ;


select concat("insert into redsky.corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId) values ('2','component.standard.accountContactTab','",corpid,"','",concat(corpid,'_SETUP'),"',now(),'",concat(corpid,'_SETUP'),"',now(),'component.standard.accountContactTab');")
FROM company ;



select concat("insert into redsky.corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId) values ('2','component.standard.surveyTool','",corpid,"','",concat(corpid,'_SETUP'),"',now(),'",concat(corpid,'_SETUP'),"',now(),'component.standard.surveyTool');")
FROM company ;


select concat("insert into redsky.corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId) values ('2','component.standard.accountPotralActivation','",corpid,"','",concat(corpid,'_SETUP'),"',now(),'",concat(corpid,'_SETUP'),"',now(),'component.standard.accountPotralActivation');")
FROM company ;


SELECT substring_index(group_concat(id order by id asc),',',1),count(*),group_concat(corp_id),group_concat(createdby),group_concat(createdon),group_concat(componentId)
FROM corp_comp_permission
where componentid in ('component.standard.claimTab','component.standard.cPortalActivation','component.standard.accountContactTab',
'component.standard.surveyTool','component.standard.accountPotralActivation')
 group by corp_id,componentId having count(componentId)>1;

select concat("delete from corp_comp_permission where id in ('",substring_index(group_concat(id order by id asc),',',1),"');")
FROM corp_comp_permission
where componentid in ('component.standard.claimTab','component.standard.cPortalActivation','component.standard.accountContactTab',
'component.standard.surveyTool','component.standard.accountPotralActivation')
 group by corp_id,componentId having count(componentId)>1;
//


ALTER TABLE `redsky`.`company` ADD COLUMN `storageBillingStatus` bit(1) DEFAULT false ;

update company set storageBillingStatus =false ;

ALTER TABLE `redsky`.`proposalmanagement` 
ADD COLUMN `billTillDate` DECIMAL(19,2) NULL DEFAULT NULL  AFTER `approvalFileLocation` ;

//#9618 as per abhishek
select corpid ,code,description,bucket,bucket2 from refmaster where parameter='TCKTSERVC'
and code in ('PK','LD','DU','DL','UP','PL','LAB','PAD');

update refmaster set bucket='Y'  where parameter='TCKTSERVC'
and code in ('PK','LD','DU','DL','UP','PL','LAB','PAD');

/*
not run on prod
select corpid ,code,description,bucket,bucket2 from refmaster where parameter='TCKTSERVC'
and corpid='HOLL';

update refmaster set bucket='Y'  where parameter='TCKTSERVC'
and corpid='HOLL';
*/
//

//#9716
select s.customerfileid,so.customerfileid,so.id,so.serviceorderid
from serviceorder s,sodashboard so
where s.id=so.serviceorderid;

select concat("update sodashboard set customerfileid='",s.customerfileid,"' where id='",so.id,"' and serviceorderid='",so.serviceorderid,"';")
from sodashboard so,
serviceorder s
where s.id=so.serviceorderid;


select concat("update sodashboard set actualSurvey='",cf.actualsurveydate,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.actualsurveydate is not null;



select concat("update sodashboard set estimateSurvey='",cf.survey,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
 s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.survey is not null;

select concat("update sodashboard set actualLoading='",t.loadA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.loadA is not null;

//

ALTER TABLE miscellaneous ADD actualLinehaul decimal(7,2);

