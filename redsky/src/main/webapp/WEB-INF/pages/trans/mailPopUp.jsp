<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<style>
.blue{color:#15428B;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}
.green{color:#04b915;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}
.red{color:#b90404;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}

div#content {left:0;margin:0 auto 0px 0px; padding:0;position:relative;text-align:left;}
</style>
</head>
<body>
<s:hidden name="from" value="<%=request.getParameter("from") %>" />
<c:set var="custID" value="<%= request.getParameter("custID")%>" />
<s:hidden name="custID" value="<%= request.getParameter("custID")%>" />

<c:set var="from" value="<%=request.getParameter("from") %>" />
<s:form id="mailForm" target="_parent" action="sendMail.html?id=${custID}&decorator=popup&popup=true" method="post" validate="true" enctype="multipart/form-data" >
	
	<s:hidden name="emailTo" value="<%=request.getParameter("emailTo") %>" />
	<c:set var="emailTo" value="<%=request.getParameter("emailTo") %>" />
	
	<s:hidden name="userID" value="<%=request.getParameter("userID") %>" />
	<c:set var="userID" value="<%=request.getParameter("userID") %>" />

    <div id="layer1" style="border: solid; border-color: #aacefe; padding: 0; margin: 0">
	<table border="0" width="100%" cellpadding="3" cellspacing="1"  height="100%" bgcolor="#e0edff" style="margin-bottom:0px">
		<tbody>
			<tr>
				<td height="2px"></td>
				<td></td>
			</tr>
		
			<c:if test="${mailStatus == 'sent'}">
				<tr>
					<td width="70px"><img src="${pageContext.request.contextPath}/images/mail_sent.gif" HEIGHT=70 WIDTH=70 style="cursor: default" onclick="notExists();"/></td>
					<td class="green">Your mail has been successfully sent to ${emailTo}</td>
					<script>
						//parent.window.opener.document.location.reload();
					</script>
				</tr>
				<tr>
				<td height="2px"></td>
				<td></td>
			  </tr>
				<tr>
				<td><input type="button"  value="Close" onclick="window.close();"/></td>
				</tr>
			</c:if>
			<c:if test="${mailStatus == 'notSent'}">
				<tr>
					<td width="70px"><img src="${pageContext.request.contextPath}/images/mail_ntsent.gif" HEIGHT=70 WIDTH=70 style="cursor: default" onclick="notExists();"/></td>
					<td class="red">Error occured while sending mail to ${emailTo}</td>
				</tr>
				<tr>
				<td height="2px"></td>
				<td></td>
			  </tr>
				<tr>
				<td><input type="button"  value="Close" onclick="window.close();"/></td>
				</tr>
			</c:if>
			<tr>
				<td height="2px"></td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
	</div>
</s:form>
</body>