package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name = "forwardingmarkupcontrol")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class ForwardingMarkUpControl extends BaseObject{
	private Long id;
	private String corpID;
	private BigDecimal lowCost;
	private BigDecimal highCost;
	private String  currency;
	private BigDecimal profitFlat;
	private BigDecimal profitPercent;
	private String contract;
		
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("lowCost", lowCost).append("highCost", highCost)
				.append("currency", currency).append("profitFlat", profitFlat)
				.append("profitPercent", profitPercent).append("contract",
						contract).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ForwardingMarkUpControl))
			return false;
		ForwardingMarkUpControl castOther = (ForwardingMarkUpControl) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(lowCost, castOther.lowCost).append(
				highCost, castOther.highCost).append(currency,
				castOther.currency).append(profitFlat, castOther.profitFlat)
				.append(profitPercent, castOther.profitPercent).append(
						contract, castOther.contract).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(lowCost)
				.append(highCost).append(currency).append(profitFlat).append(
						profitPercent).append(contract).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column(length = 10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length = 10)
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column(length = 10)
	public BigDecimal getHighCost() {
		return highCost;
	}
	public void setHighCost(BigDecimal highCost) {
		this.highCost = highCost;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 10)
	public BigDecimal getLowCost() {
		return lowCost;
	}
	public void setLowCost(BigDecimal lowCost) {
		this.lowCost = lowCost;
	}
	@Column(length = 10)
	public BigDecimal getProfitFlat() {
		return profitFlat;
	}
	public void setProfitFlat(BigDecimal profitFlat) {
		this.profitFlat = profitFlat;
	}
	@Column(length = 10)
	public BigDecimal getProfitPercent() {
		return profitPercent;
	}
	public void setProfitPercent(BigDecimal profitPercent) {
		this.profitPercent = profitPercent;
	}
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
}
