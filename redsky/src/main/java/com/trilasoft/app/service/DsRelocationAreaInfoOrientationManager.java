package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;



public interface DsRelocationAreaInfoOrientationManager extends GenericManager<DsRelocationAreaInfoOrientation, Long>{
	List getListById(Long id);
}
