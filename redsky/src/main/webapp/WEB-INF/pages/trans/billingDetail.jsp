<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
 <title>
  <fmt:message key="BillingDetail.title"/>
  </title> 
</head>

<c:if test="${empty totalOfSo}">
 <div>Company Name: ${companyName}     Year: ${year}    ServiceOrder Generated: NIL</div>
 </c:if>
 <c:if test="${not empty totalOfSo}">
 <div>Company Name: ${companyName}    Year: ${year}   ServiceOrder Generated:<fmt:formatNumber type="number" maxFractionDigits="0" minFractionDigits="0" groupingUsed="true" value="${totalOfSo}" /></div>
</c:if>
<s:form id="billingDetailList" name="billingDetailList" action="" method="post" validate="true">
<s:hidden name="corpID" value="%{corpID}"/>
<s:hidden name="month" />
<s:hidden name="year" value="%{year}"/>

<display:table name="billingDetailList" requestURI=""  id="billingDetailListID"  class="table" pagesize="12" style="width:80%;margin-top:5px;" >  
    <display:column property="corpID" escapeXml="true" sortable="true"   titleKey="BillingDetail.corpid" style="width: 10%; cursor: default;"/>
    <display:column property="monthName" escapeXml="true" sortable="true" titleKey="BillingDetail.monthName" style="width: 10%; cursor: default;"/>
   <display:column property="soCount" escapeXml="true"  sortable="true" titleKey="BillingDetail.soCount" style="width: 10%; cursor: default;"/>
   <display:column style="width: 10%; cursor: default;" titleKey="BillingDetail.Details"><a a href="#" onclick ="linkToDetail(${billingDetailListID.month})" >Details</a></display:column>
 </display:table>  
</s:form>

<script language="javascript">
	function linkToDetail(val){
   		document.forms['billingDetailList'].elements['month'].value=val;
		document.forms['billingDetailList'].action="billingList.html";
		document.forms['billingDetailList'].submit();
	}
</script>