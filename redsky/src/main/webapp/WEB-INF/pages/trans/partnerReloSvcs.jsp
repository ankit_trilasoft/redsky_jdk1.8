<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Services</title> 
    <meta name="heading"
	content="Services" /> 
     
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script language="JavaScript">
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
	}
</script>
<script language="JavaScript">
animatedcollapse.addDiv('rel', 'fade=1,hide=0')
animatedcollapse.addDiv('tra', 'fade=1,hide=1')
animatedcollapse.init()
</script>
<script>
function checkdate(clickType){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${agentBase.id}"/>';
      		var id = document.forms['agentBaseForm'].elements['agentBase.id'].value;
			var id1 = document.forms['agentBaseForm'].elements['partner.id'].value;
    		
            if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				noSaveAction = 'editPartnerPublic.html?id='+id1+'&partnerType=AG';
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					noSaveAction = 'searchPartnerView.html';
				}else{
					noSaveAction = 'partnerPublics.html?partnerType=AG';
				}
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.scacLOIlist'){
				noSaveAction = 'baseSCACList.html?id='+id;
			}
			processAutoSave(document.forms['agentBaseForm'], 'saveBase!saveOnTabChange.html', noSaveAction);
	}else{
			var id = document.forms['agentBaseForm'].elements['agentBase.id'].value;
			var id1 = document.forms['agentBaseForm'].elements['partner.id'].value;
    		
            if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AG';
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'partnerPublics.html?partnerType=AG';
				}
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.scacLOIlist'){
				location.href = 'baseSCACList.html?id='+id;
			}
	}	
}

function changeStatus(){
	document.forms['agentBaseForm'].elements['formStatus'].value = '1';
}

function openAgentBasePopWindow(){
	javascript:openWindow('baseAgentPopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentBase.description&fld_code=agentBase.baseCode');
}

function findAgentBaseName(){
	var baseCode = document.forms['agentBaseForm'].elements['agentBase.baseCode'].value;
    var url="baseAgentName.html?ajax=1&decorator=simple&popup=true&baseCode=" + encodeURI(baseCode);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse4;
    http2.send(null);
}

function handleHttpResponse4(){
		if (http2.readyState == 4){
         var results = http2.responseText
         results = results.trim();
         var res = results.split("#"); 
             if(res.size() >= 2){
 					document.forms['agentBaseForm'].elements['agentBase.description'].value = res[1];
 				}else{
                     alert("Invalid Base Code, please select another");
                    
					 document.forms['agentBaseForm'].elements['agentBase.description'].value="";
					 document.forms['agentBaseForm'].elements['agentBase.baseCode'].value="";
					 document.forms['agentBaseForm'].elements['agentBase.baseCode'].select();
               }
   		}
}

var http2 = getHTTPObject();

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function openAgentBaseSCAC(){
	javascript:openWindow('baseSCACList.html?id=${agentBase.id}&decorator=popup&popup=true',755,500);
}

function selectColumn(targetElement) 
   {  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = userCheckStatus + '#' + rowId;
      document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value;
     var userCheckStatus=document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = userCheckStatus.replace( rowId , '' );
     document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = userCheckStatus.replace( '##' , '#' );
     } 
    }
function ashish(){
var len = document.forms['partnerReloSvcsForm'].elements['checkV'].length;
var check=true;
for (i = 0; i < len; i++){ 
		if(document.forms['partnerReloSvcsForm'].elements['checkV'][i].checked == false ){ 
		check=false; 
		i=len+1
		} 
	} 
	if(check){
	document.forms['partnerReloSvcsForm'].elements['checkAllRelo'].checked=true;
	}else{
	document.forms['partnerReloSvcsForm'].elements['checkAllRelo'].checked=false;
	}
}
function checkAllReloBox(temp){
	document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = ""; 
	var len = document.forms['partnerReloSvcsForm'].elements['checkV'].length;
	if(temp.checked){
	for (i = 0; i < len; i++){
	
		document.forms['partnerReloSvcsForm'].elements['checkV'][i].checked = true ; 
		selectColumn(document.forms['partnerReloSvcsForm'].elements['checkV'][i]);
	}
	}
	else{
	for (i = 0; i < len; i++){
	
		document.forms['partnerReloSvcsForm'].elements['checkV'][i].checked = false ; 
		selectColumn(document.forms['partnerReloSvcsForm'].elements['checkV'][i]);
	}
	}
}

function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value; 
if(userCheckStatus!=""){ 
document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value="";
for(j = 0; j<50 ; j++){
userCheckStatus=userCheckStatus.replace('#',',');
}
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value;  
     var userCheckStatus=	document.forms['partnerReloSvcsForm'].elements['partnerPublic.reloServices'].value = userCheckStatus + '#' +column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}





function selectColumn1(targetElement) 
{  
  var rowId=targetElement.value;  
  if(targetElement.checked)
    {
	  var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value;
     if(userCheckStatus == '')
     {
	  	document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = rowId;
     }
     else
     {
      var userCheckStatus=	document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = userCheckStatus + '#' + rowId;
     document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = userCheckStatus.replace( '##' , '#' );
     }
   }
  if(targetElement.checked==false)
   {
    var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value;
    var userCheckStatus=document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = userCheckStatus.replace( rowId , '' );
    document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = userCheckStatus.replace( '##' , '#' );
    } 
 }
function checkColumn1(){
	var userCheckStatus ="";
	userCheckStatus = '${partnerPublic.transPort}';		  
	if(userCheckStatus!="")
	{
		document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value="";
		var column = userCheckStatus.split(",");  
		for(i = 0; i<column.length ; i++)
		{    
		     var userCheckStatus = document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value;  
	    	 var userCheckStatus=	document.forms['partnerReloSvcsForm'].elements['tempTransPort'].value = userCheckStatus + '#' +column[i];
	        	if(column[i]!="")
	        	{
	     				document.getElementById(column[i]).checked=true;
	        	}
		} 
	}
	if(userCheckStatus==""){	}
}
function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=PRS&excludeFPU=TRUE&partnerCode=${partnerPublic.partnerCode}";
		  http10.open("GET", url, true); 
		  http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=PRS&excludeFPU=FALSE&partnerCode=${partnerPublic.partnerCode}";
		  http10.open("GET", url, true); 
	      http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }
}
function handleHttpResponse(){
   if (http10.readyState == 4)
   {
   			  var results = http10.responseText
	               results = results.trim();					               
				if(results=="")	{
				
				}
				else{
				
				} 
   } 
}
function updateChildFromParent()
{
		  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&id=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&pageListType=PRS";
		  http11.open("GET", url, true); 
		  http11.onreadystatechange = handleHttpResponse1; 
	      http11.send(null); 
}
function handleHttpResponse1(){
    if (http11.readyState == 4)
    {
    			  var results = http11.responseText
	               results = results.trim();					               
				if(results!="")	{
				alert(results);				
				} 
    } 
}
var http11 = getHTTPObject();
var http10 = getHTTPObject();
</script>
</head> 
<style>
.mainDetailTable {
	width:650px;
}
</style>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partnerPublic.id}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>

<s:form id="partnerReloSvcsForm" action="savePartnerReloSvcs.html" method="post" validate="true">  
<s:hidden name="id" value="${partnerPublic.id}"/> 
<s:hidden name="partnerType" value="${partnerType}" />
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px"  
        onclick="location.href='<c:url value="/editPartnerRatesForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	 <c:if test="${partnerType == 'AG' || partnerType == 'AC' || partnerType == 'VN'}">
	 <s:hidden name="tempTransPort" value="${partnerPublic.transPort}" />
	 </c:if>
	 <c:if test="${partnerType != 'AG' && partnerType != 'AC' && partnerType != 'VN'}">
	 <s:hidden  name="tempTransPort" value="" />
	 </c:if>    
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.partnerlist' }">
	<c:redirect url="/partnerPublics.html?partnerType=AG"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AG"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.scacLOIlist' }">
	<c:redirect url="/baseSCACList.html?id=${agentBase.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>
			<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				<c:if test="${partnerType == 'AG'}">
					<c:if test="${not empty partnerPublic.id}">
					<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
					<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
                    <c:if test="${sessionCorpID!='TSFT' }">
                    <li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
					</c:if>
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>	
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					<c:if test="${not empty partnerPublic.id}">
						 <c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
									<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
						  </c:if>
						  <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
									<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
						  </c:if>
					 </c:if>
					<c:if test="${not empty partnerPublic.id}">						
					  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Services<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					</c:if>
					<c:if test="${partnerPublic.partnerPortalActive == true}">
					<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
					<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=AG&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					</configByCorp:fieldVisibility>
					</c:if>	
					<c:if test="${partnerType == 'AG'}">
			
			       <li id="AGR"><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			
			 </c:if>
					</c:if>
				</c:if>
				<c:if test="${partnerType == 'VN'}">
					<c:if test="${not empty partnerPublic.id}">
					<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=${partnerType}" ><span>Vendor Detail</span></a></li>
                    <c:if test="${sessionCorpID!='TSFT' }">
                    <li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
					</c:if>
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Services<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    </c:if>
				</c:if>
				<c:if test="${partnerType == 'AC'}">
					<c:if test="${not empty partnerPublic.id}">
					<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=${partnerType}" ><span>Account Detail</span></a></li>
                    <li><a href="editNewAccountProfile.html?id=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
                    <c:if test="${sessionCorpID!='TSFT' }">
                    <li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
                    </c:if>
                    <configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
                    <li><a href="editProfileInfo.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
                    </configByCorp:fieldVisibility>
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<configByCorp:fieldVisibility componentId="component.standard.accountContactTab"><li><a href="accountContactList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
					<c:if test="${checkTransfereeInfopackage==true}">
						<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
					</c:if>
					<c:if test="${partnerPublic.partnerPortalActive == true}">
					 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation"><li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
					</c:if>
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Services<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${partnerType == 'AC'}">
				 	<c:if test="${not empty partnerPublic.id}">
					<c:url value="frequentlyAskedQuestionsList.html" var="url">
					<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
					<c:param name="partnerType" value="AC"/>
					<c:param name="partnerId" value="${partnerPublic.id}"/>
					<c:param name="lastName" value="${partnerPublic.lastName}"/>
					<c:param name="status" value="${partnerPublic.status}"/>
					</c:url>				 	
					<li><a href="${url}"><span>FAQ</span></a></li>
					<c:if test="${partnerPublic.partnerPortalActive == true}">
						<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
					</c:if>
				  	</c:if>
				</c:if>	
				</c:if>				
				</c:if>
				
			</sec-auth:authComponent> 
				   
			<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
					   	<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
					   	 <li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
					   	 <li><a href="partnerAgent.html"><span>Agent List</span></a></li>
    	 </sec-auth:authComponent>
    	 <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${partnerPublic.id}&tableName=partnerpublic&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
	</div>
	
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table>
					<tbody>					
						<tr>
							<td align="right" class="listwhitetext" width="100">Name</td>
							<td colspan="3"><s:textfield cssClass="input-textUpper" name="partnerPublic.lastName" required="true" cssStyle="width:330px;" readonly="true"/></td>
							<td align="right" class="listwhitetext" width="60px"><fmt:message key='partner.partnerCode' /></td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPublic.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
							<td align="right" class="listwhitetext" width="50px">Status</td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPublic.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
			 			</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>			 			
						<tr>
							<td height="0" width="100%" align="left" >		
								<div  onClick="javascript:animatedcollapse.toggle('rel')" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left">
						</td>
						<td class="headtab_center" >&nbsp;Relocation Services
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>								
						  	<div id="rel">
						  	<table>
						  	<tbody>
			 			<tr>
							<td height="5px"></td>
						</tr>
						
						<tr>
							<td align="right" class="listwhitetext" width="100px">Relo Contact Name</td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPublic.reloContact" required="true" cssClass="input-text" maxlength="100" size="50"/></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext" width="">Email Address</td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPublic.reloContactEmail" required="true" cssClass="input-text" maxlength="100" size="50"/></td>
							
						</tr>
						<tr>
						<td style="margin:0px;padding:0px; height:25px"></td>
					<td align="left"  class="listwhitetext" valign="bottom"><table width="80%" cellspacing="0" cellpadding="0" class="detailTabLabel"><tr><td width="66%">Relocation&nbsp;Services</td><td><input type="checkbox" name="checkAllRelo" onclick="checkAllReloBox(this)" /></td><td width="25%">Check all</td></tr></table></td>
                      <s:hidden  name="partnerPublic.reloServices"/>
                     <td class="listwhitetext" valign="top" align="left"></td>
                     </tr>
                     <tr>
                     <td></td>
                     <td colspan="5" valign="top">
                     <fieldset >                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                      <td width="" valign="top">
                        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${serviceRelos1}">
                           <tr>
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" /> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                           
                          </tr>
                          </c:forEach>
                          </table> 
                       </td>
                       <td valign="top">
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${serviceRelos2}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" /> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:forEach>
                          </table>                    
                       </td>
                       </tr>
                       </table>
                          </fieldset>
                       </td>
                        
                       </tr>	
						  	
						  	</tbody>
						  	</table>
						  	</div>
						  	</td></tr>
						<tr>
							<td height="0" width="100%" align="left" >		
								<div  onClick="javascript:animatedcollapse.toggle('tra')" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left">
						</td>
						<td class="headtab_center" >&nbsp;Transportation Services
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>								
						  	<div id="tra">
						  	<table class="listblacktext">
						  	<tbody>
						  	<tr><td><input type="checkbox" id="sea" value="sea" onclick="selectColumn1(this)"/></td><td class="listwhitetext" >Sea</td></tr>
						  	<tr><td><input type="checkbox" id="air" value="air" onclick="selectColumn1(this)"/></td><td class="listwhitetext" >Air</td></tr>						  	
						  	<tr><td><input type="checkbox" id="road" value="road" onclick="selectColumn1(this)"/></td><td class="listwhitetext" >Road</td></tr>
						  	<tr><td><input type="checkbox" id="long" value="long" onclick="selectColumn1(this)"/></td><td class="listwhitetext" >Long Term Storage</td></tr>
						  	</tbody></table></div></td></tr>
			 			<tr>
							<td>
<c:if test="${not empty partnerPublic.id}">
	<c:if test="${partnerType == 'AC'}">
	<table width="100%" border="0" style="margin:0px;padding:0px;">
		<tr>
		<td align="center" colspan="15" class="vertlinedata"></td>
		</tr>	     	
	<tr align="right">
	<td align="right" colspan="2"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="updateChildFromParent();"/></td>
	</tr>
	<tr align="right">
	<c:set var="isExcludePublicReloSvcsParentUpdate" value="false" />
	<c:if test="${partnerPrivate.excludePublicReloSvcsParentUpdate}">
	<c:set var="isExcludePublicReloSvcsParentUpdate" value="true" />
	</c:if>
	<td align="right" class="listwhitetext" colspan="2">Exclude from Parent updates&nbsp;
	<s:checkbox key="partnerPrivate.excludePublicReloSvcsParentUpdate" id="excludeFPU" value="${isExcludePublicReloSvcsParentUpdate}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>
	</tr>											
	</table>
	</c:if>	
	</c:if>								
							</td>
						</tr>
							
<tr>
<td height="80">&nbsp;</td>
</tr>

</div>

<div class="bottom-header" style="margin-top:40px;"><span></span></div>

</div>
</div>
</div>	
  
<table>
		<tbody>
					<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:120px">
								<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${partnerPublic.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="partnerPublic.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${partnerPublic.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdBy' /></b></td>
							<c:if test="${not empty partnerPublic.id}">
								<s:hidden name="partnerPublic.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{partnerPublic.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partnerPublic.id}">
								<s:hidden name="partnerPublic.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedOn'/></b></td>
							<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${partnerPublic.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="partnerPublic.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${partnerPublic.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedBy' /></b></td>
							<c:if test="${not empty partnerPublic.id}">
								<s:hidden name="partnerPublic.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{partnerPublic.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partnerPublic.id}">
								<s:hidden name="partnerPublic.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
		</tbody>
</table>               
        <s:submit cssClass="cssbutton" method="" key="button.save" cssStyle="width:70px; height:25px"/>   
       
  
  <table><tr><td height="80px"></td></tr></table>
</s:form>
<script type="text/javascript">
checkColumn(); 
checkColumn1();
ashish();
<c:if test="${hitflag == 1}" >
   	<c:redirect url="/partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}" ></c:redirect>
</c:if>  

</script>
<script type="text/javascript">
		  	<c:if test="${excludeFPU==true}">
							document.getElementById("excludeFPU").checked=true;
			</c:if>
		  	<c:if test="${excludeFPU==false}">
			document.getElementById("excludeFPU").checked=false;
			</c:if>
</script>
<script type="text/javascript">
try{
	<c:if test="${partnerType=='AG'}">
	var checkRoleAG = 0;
	//Modified By subrat BUG #6471 
	<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
		checkRoleAG  = 14;
	</sec-auth:authComponent>
	if(checkRoleAG < 14){
		document.getElementById('AGR').style.display = 'block';
	}else{
		document.getElementById('AGR').style.display = 'none';
	}
	</c:if>
}catch(e){}

</script>