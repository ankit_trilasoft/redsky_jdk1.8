<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <table width="100%" border="0" cellpadding="4" cellspacing="0" class="detailTabLabel">
 <c:set var="mandatorySource" value="N" />
<configByCorp:fieldVisibility componentId="component.customerfile.field.source">
<c:set var="mandatorySource" value="Y"/>
</configByCorp:fieldVisibility> 
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
  <tr>
  <s:hidden name="isPrivateParty"/>
    <td width="90">&nbsp;</td>
    		<td align="left">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="" >
					<tbody>
						<tr>
							<td width="131" align="left" valign="bottom" class="listwhitetext">
								<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sequenceNumber',this);return false"><fmt:message key='customerFile.sequenceNumber'/></a>
							</td>
							<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">		
							<td width="124" align="left" valign="bottom" class="listwhitetext">
								<a href="#"  style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.status',this);return false"><fmt:message key='customerFile.status'/></a>
							</td>	
							</c:if>
							<c:if test="${checkAccessQuotation}">
							<c:if test="${moveTypeFlag != 'Y'}"> 
							<td width=""></td> 
							<c:if test="${not empty customerFile.id}">
							<td width="" ></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
							<td width="" colspan="3"></td>
							</c:if>
						    </c:if>
						    <c:if test="${moveTypeFlag == 'Y'}">
						    <td width="" ></td>
						    </c:if>
						    <td align="left" class="listwhitetext" id="quotationStatusLabel" valign="bottom"><fmt:message key='customerFile.quotationStatus'/></td>						    
						     </c:if>
						    <td align="left" class="listwhitetext" valign="bottom"  width="85"><fmt:message key='customerFile.statusDate'/></td>
						  <c:choose>
							<c:when test="${checkAccessQuotation && customerFile.moveType=='BookedMove'}">
							<td  width="90" align="left" valign="bottom" class="listwhitetext" id="hidStatusReg"> 
								<fmt:message key='customerFile.statusReason'/>&nbsp;&nbsp;
						    </td>
							</c:when>
							<c:when test="${empty checkAccessQuotation || checkAccessQuotation== null || checkAccessQuotation==false}">
							<td  width="90" align="left" valign="bottom" class="listwhitetext" id="hidStatusReg"> 
								<fmt:message key='customerFile.statusReason'/>&nbsp;&nbsp;
						    </td>
							</c:when>
							<c:otherwise>
							</c:otherwise>
							</c:choose>
						 	<td class="listwhitetext" align="left" valign="bottom"   width="90"  >&nbsp;<fmt:message key='serviceOrder.registrationNumber' /></td>							 		
							 <configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">	
							<td align="left" class="listwhitetext" id="statusReasonId1" style="display:none;">Status&nbsp;Reason<font color="red" size="2">*</font></td>
						    </configByCorp:fieldVisibility>
						  <td style="width:0px"></td>						   					  
						 <td class="listwhitetext" align="left" valign="bottom"   width="120"  >
						 <configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
						 <c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null}">
						 	<c:if test="${not empty customerFile.id}">Quotes To-Go Tracking #</c:if>
						 </c:if>
						 </configByCorp:fieldVisibility></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" valign="top" width="116"> <s:textfield  cssClass="input-textUpper" key="customerFile.sequenceNumber"
							required="true" cssStyle="width:118px" maxlength="15" readonly="true" onfocus = "myDate();" /> </td>
							<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">							
							<td align="left" class="listwhitetext" valign="top" > 
								<configByCorp:customDropDown listType="map" list="${custStatus_isactive}" fieldValue="${customerFile.status}" 
   									attribute="class=list-menu id=status1 name=customerFile.status headerKey='' headerValue='' cssStyle=width:110px onchange=checkStatus();changeStatus(); onfocus = myDate();"/>							
							</td> 
							</c:if>
							<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">							
							<s:hidden name="customerFile.status"/>
							</c:if>
							  <c:if test="${checkAccessQuotation}">							
					            <c:if test="${empty customerFile.id || empty customerFile.moveType}">
					            <td colspan="4" valign="top" style="padding-right: 3px;">
					             <table cellspacing="1" style="background-color: #fafafa; margin: 0px; padding: 0px; border: 1px solid rgb(33, 157, 209); border-radius: 2px;" cellpading="0">
					            <tr>
					            <td class="listwhitetext" width=""><input type="radio" style="margin:0px;" id="Quote" name="moves" value="Quote" onclick="desableCustomerFileStatus(this);showQuotationStatus(this,'F');changeQuoteBookingDate(this),changeComptetive(this);"></td>
					            <td align="right" class="listwhitetext" style="color:#01a301;">Quote&nbsp;</td>	
					            <td class="listwhitetext" width=""><input type="radio" style="margin:0px;" id="BookedMove" name="moves" value="BookedMove" onclick="desableCustomerFileStatus(this);showQuotationStatus(this,'F');changeQuoteBookingDate(this),changeComptetive(this);"></td>				          
					             <td align="right" class="listwhitetext" style="color:#01a301;">Booked Move&nbsp;&nbsp;</td>
					            </tr>
					             </table>
					             </td>					             
					            </c:if>
					              <c:if test="${not empty customerFile.id &&  not empty customerFile.moveType}">
					              <c:if test='${moveTypeFlag != "Y" }'>
					               <c:if test="${customerFile.moveType=='Quote'}"> 
					             <td class="listwhitetext" width=""><input type="radio" style="margin:0px;" name="moves" value="Quote" checked="checked" onclick="showQuotationStatus(this,'F');changeQuoteBookingDate(this);changeComptetive(this);"></td>
					            <td align="right" class="listwhitetext" width="">Quote&nbsp;&nbsp;</td>	
					               </c:if>
					             <c:if test="${customerFile.moveType=='BookedMove'}">
					                    <td class="listwhitetext" width=""><input type="radio" style="margin:0px;" name="moves" value="BookedMove" checked="checked" onclick="showQuotationStatus(this,'F');changeQuoteBookingDate(this);changeComptetive(this);"></td>				          
					             <td align="right" class="listwhitetext" width="">Booked Move&nbsp;&nbsp;</td>
					             </c:if>
					             </c:if>
					             <c:if test='${moveTypeFlag == "Y" }'>					               
					             <td class="listwhitetext" valign="top">
					             <s:radio id="" name="moves" list="%{moveTypeValue}" onclick="desableCustomerFileStatus(this);showQuotationStatus(this,'F');changeQuoteBookingDate(this);changeComptetive(this);" />&nbsp;</td>					             
					             </c:if>
					             </c:if>
					       	 <td align="left" class="listwhitetext" id="quotationStatusShow" valign="top">
							<c:if test="${not empty customerFile.id}">
							<s:select cssClass="list-menu"   name="customerFile.quotationStatus" list="%{orderStatus1}" cssStyle="width:85px" onchange="showStatusReason(this);checkQuotation();" />
							</c:if>
							<c:if test="${empty customerFile.id}">
							<s:select cssClass="list-menu"   name="customerFile.quotationStatus" list="{'New'}" cssStyle="width:85px" onchange="showStatusReason(this);checkQuotation();" />
							</c:if>
							&nbsp;&nbsp;</td>
							</c:if>
							<c:if test="${empty checkAccessQuotation || checkAccessQuotation== null || checkAccessQuotation==false}">
							<s:hidden name="customerFile.quotationStatus" />
							</c:if>	
							<c:if test="${not empty customerFile.statusDate}">
							<s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
							<td width="100"  valign="top"><s:textfield cssClass="input-textUpper" name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" cssStyle="width:80px" maxlength="11" readonly="true"  /></td>
							</c:if>
							<c:if test="${empty customerFile.statusDate}">
							<td width="100" valign="top"><s:textfield cssClass="input-textUpper"  name="customerFile.statusDate" cssStyle="width:80px" readonly="true"  /></td>
							</c:if>	
							 <c:choose>
							<c:when test="${checkAccessQuotation && customerFile.moveType=='BookedMove'}">
							<td id="hidStatusReg1" >
								<configByCorp:customDropDown listType="map" list="${custStatusReason_isactive}" fieldValue="${customerFile.statusReason}" 
   									attribute="class=list-menu name=customerFile.statusReason headerKey='' headerValue='' style=width:105px"/>&nbsp;&nbsp;							
							</td>
							</c:when>
							<c:when test="${empty checkAccessQuotation || checkAccessQuotation== null || checkAccessQuotation==false}">
							<td id="hidStatusReg1" >
								<configByCorp:customDropDown listType="map" list="${custStatusReason_isactive}" fieldValue="${customerFile.statusReason}" 
   									attribute="class=list-menu name=customerFile.statusReason headerKey='' headerValue='' style=width:105px"/>&nbsp;&nbsp;														
							</td>
							</c:when>
							<c:otherwise>
							</c:otherwise>
							</c:choose>
							 <td align="left" class="listwhitetext" valign="top" width="113" style="padding-left:3px;">
									<s:textfield cssClass="input-text"  name="customerFile.registrationNumber" onchange="checkRegUpdate()" cssStyle="width:97px" maxlength="20"  />
							</td>							 														
							 <configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">							
							<td align="left" class="listwhitetext" id="statusReasonId" valign="top" style="display:none;">
								<configByCorp:customDropDown listType="map" list="${qfReason_isactive}" fieldValue="${customerFile.qfStatusReason}" 
   									attribute="class=list-menu   name=customerFile.qfStatusReason headerKey='' headerValue='' style=width:120px"/>														
							</td>
							</configByCorp:fieldVisibility>
							 <td style="width:0px"></td>							
									<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
									<td align="left" class="listwhitetext" valign="top" width="">
									<c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null }">
									<c:if test="${not empty customerFile.id}">
										<s:textfield cssClass="input-text"  name="customerFile.estimateNumber"  size="18" maxlength="20"  />
									</c:if></c:if></configByCorp:fieldVisibility>	
							</td>
							<c:if test="${not empty customerFile.id}">
							<c:if test="${jobTypes !='UVL' && jobTypes !='MVL' && jobTypes !='ULL' && jobTypes !='MLL' && jobTypes !='DOM' && jobTypes !='LOC' && customerFile.originCountryCode != customerFile.destinationCountryCode}">
								<td><a><img src="images/viewcustom2.jpg" onclick="findCustomInfo(this);" alt="Customs" title="Customs" /></a></td>
						    </c:if>
						    </c:if>						    
						    <c:if test="${not empty customerFile.id}">
							<td><a><img src="images/viewdoc2.jpg" onclick="findDocInfo(this);" alt="Docs" title="Docs" /></a></td>
						    </c:if>
						</tr>  
					</tbody>
</table>				</td>
  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td align="left">
				  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
				  <tr>
				  <td>				  
				<table width="" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
					<tbody>
						<tr>
							<td align="left" width="60" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/></td>
							<td width=""></td>
							<td align="left" width="72" class="listwhitetext" valign="bottom">Title / Rank</td>
							<td width=""></td>
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /></td>
							<td width=""></td>
							<td align="left" width="23" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
							<td width=""></td>
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/><font color="red" size="2">*</font></td>
							<td width="10" ></td>
							<td align="left" width="" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.socialSecurityNumber',this);return false" >${labelChangeValue}</a></configByCorp:fieldVisibility></td>
							<td align="right" width="" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.vip',this);return false" ><fmt:message key='serviceOrder.vip'/></a></td>
							<td align="right" width="8" valign="middle" class="listwhitetext" rowspan="2">
							<c:set var="isVipFlag" value="false"/>
							<c:if test="${customerFile.vip}">
								<c:set var="isVipFlag" value="true"/>
							</c:if>
							<div style="position:absolute;top:70px;margin-left:115px;">
								<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
							</div>
							</td>
							<td align="left" class="listwhitetext">Language&nbsp;Preferred</td>
						</tr>
						<tr>						
							<td align="left" class="listwhitetext" valign="top" > 
							<configByCorp:customDropDown listType="map" list="${preffix_isactive}" fieldValue="${customerFile.prefix}" 
   									attribute="class=list-menu name=customerFile.prefix style=width:50px headerKey='' headerValue='' onchange=changeStatus(); onfocus=myDate();"/>
							</td>
							<td align="left" class="" ></td>
							<td align="left" class="listwhitetext" valign="top" ><s:textfield cssClass="input-text" key="customerFile.rank" cssStyle="width:57px;" maxlength="7" onkeyup="valid(this,'special')" onblur="valid(this,'special')" /> </td>
							<td align="left" class="" ></td>
							<td align="left" class="listwhitetext" valign="top" width="203"> <s:textfield cssClass="input-text upper-case" name="customerFile.firstName"  onchange="noQuote(this);isUpdaterFlag();setFirstNameFlag();" onblur="titleCase(this)" onkeypress=""  required="true"
							cssStyle="width:193px;" maxlength="80"  onfocus = "myDate();" /> </td>
							<td></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.middleInitial"
							required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" /> </td>
							<td></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text upper-case"  key="customerFile.lastName" onchange="isUpdaterFlag();lastNameValidation();setLastNameFlag();noQuote(this);" onblur="titleCase(this)" onkeypress="" required="true"
							cssStyle="width:190px" maxlength="80"  onfocus = "myDate();" /> </td>
							<td width="10" ></td>
					        <td align="left" class="listwhitetext" width=""><configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
							<s:textfield cssClass="input-text" key="customerFile.socialSecurityNumber" cssStyle="width:81px;" maxlength="30" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" />
				        	</configByCorp:fieldVisibility> </td>
							<td align="right" width="23" valign="top" ><s:checkbox id="customerFile.vip" key="customerFile.vip" value="${isVipFlag}" fieldValue="true" onclick="changeStatus(),setVipImage()" /></td>
							<td align="left" class="listwhitetext" valign="top" >
							<configByCorp:customDropDown listType="map" list="${languageList_isactive}" fieldValue="${customerFile.customerLanguagePreference}" 
   									attribute="name=customerFile.customerLanguagePreference style=width:95px; class=list-menu onchange=changeLanguage();" />
							</td>	
					  </tr>
					</tbody>
				</table>
				</td>
				<c:if test="${empty customerFile.id}">
							<td align="left" style="width:75px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countVipNotes == '0' || countVipNotes == '' || countVipNotes == null}">
								<td align="left" style="width:75px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="left" style="width:75px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
						</c:choose> 
					</c:if>
				</tr>
				</table>		
				</td>				
			    </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td align="left">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
					<tbody>						
						<tr>
						<td width="132" align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.originCountry',this);return false" >Origin</a></td>
						<td width="40" >&nbsp;</td>
						<td width="4"></td>
						<td width=""></td>
						<td width="114" align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.destinationCountry',this);return false" >Destination</a></td>
						<td width=""></td>
						<td width="69"></td>
						<td></td>
						<td width="" align="left" class="listwhitetext">
						<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.customerEmployer',this);return false" >
						<fmt:message key='customerFile.customerEmployer'/>
						</a></td>
						
						</tr>
						<tr>
							<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.originCityCode" cssStyle="width:117px" maxlength="30" readonly="true" /></td>
							<td align="left"><s:textfield cssClass="input-textUpper"  name="customerFile.originCountryCode" cssStyle="width:35px" maxlength="30" readonly="true" /></td>
							<td></td>
							<td></td>
							<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.destinationCityCode" cssStyle="width:105px" maxlength="30" readonly="true" /></td>
							<td></td>
							<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.destinationCountryCode" cssStyle="width:35px" maxlength="30" readonly="true" /></td>
							<td></td>
							<td align="left">
							<s:textfield cssClass="input-textUpper" key="customerFile.customerEmployer" cssStyle="width:305px;" readonly="true" /></td>
						</tr>
					</tbody>
				</table></td>
		    </tr>
		    <tr>
		    	<td align="right" valign="middle" class="listwhitetext" style="width:90px;padding-top:4px;">Booking Agent:</td>
				<td align="left">
		    		<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  width="">
					<tbody>	
						<tr>
							<td align="left" class="listwhitetext" >Code<font color="red" size="2">*</font></td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:100px">Name</td>
							<td style="width:5px"></td>
							<td style="width:5px"></td>
							<c:if  test="${companies == 'Yes'}">
							<td align="left" class="listwhitetext" width="95px">Company&nbsp;Division<font color="red" size="2">*</font></td>							
							<td style="width:5px"></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.bookingDate',this);return false" >Booking&nbsp;Date</a></td>
							<td width="30px"></td>
							<td align="left"></td>
							<td style="width:2px"></td>
							<td align="left" class="listwhitetext" width="20" colspan="2"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.initialContactDate',this);return false" >Initial&nbsp;Contact</a></td>
							<td></td>
							    <configByCorp:fieldVisibility componentId="component.customerFile.introCall">
							<td style="width:2px"></td>
							<td align="left" class="listwhitetext" width="20" colspan="2">Intro Call</a></td>
							<td></td>
							<td align="left"></td>
							</configByCorp:fieldVisibility>
						</tr>
						<tr>
						<c:choose>
						<c:when test="${hasBookingAgentDataSecuritySet}">
						    <td width="73px"><s:textfield cssClass="input-text" id="customerFileBookingAgentCode" name="customerFile.bookingAgentCode" cssStyle="width:106px" maxlength="8" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidBookAgent');findBookingAgentName();changeStatus();findCompanyDivisionByBookAg();findCoordinatorByBA();"  onblur="findCompanyDivisionByBookAg();"/></td>
						    <td width="22"><img id="customerfile.bookingAgentPopUp" class="openpopup" width="17" height="20" onclick="openBookingAgentBookingAgentSetPopWindow();document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						    <td align="left" style="width:100px;">
                          <c:if test="${agentSearchValidation}">
						    <s:textfield cssClass="input-text" id="customerFileBookingAgentName" name="customerFile.bookingAgentName" cssStyle="width:193px;" maxlength="250" onchange="findPartnerDetailsByName('customerFileBookingAgentCode','customerFileBookingAgentName');fillAccountFlag();" onkeyup="findPartnerDetails('customerFileBookingAgentName','customerFileBookingAgentCode','bookingAgentNameDiv','and (isAgent=true) ','',event);"   onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'hidBookAgent');"  />
						    </c:if>
						       <c:if test="${!agentSearchValidation}">
						    <s:textfield cssClass="input-text" id="customerFileBookingAgentName" name="customerFile.bookingAgentName" cssStyle="width:193px;" maxlength="250" onchange="findPartnerDetailsByName('customerFileBookingAgentCode','customerFileBookingAgentName');fillAccountFlag();" onkeyup="findPartnerDetails('customerFileBookingAgentName','customerFileBookingAgentCode','bookingAgentNameDiv','and ((isAccount=true or isAgent=true) or (isVendor=true and typeOfVendor=\\'Agent Broker\\'))','',event);"  onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'hidBookAgent');"  />
						    </c:if>
							<div id="bookingAgentNameDiv" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
						</c:when>
						<c:otherwise>
							<td width="73px"><s:textfield cssClass="input-text" id="customerFileBookingAgentCode" name="customerFile.bookingAgentCode" cssStyle="width:106px" maxlength="8" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidBookAgent');findBookingAgentName();changeStatus();findCompanyDivisionByBookAg();findCoordinatorByBA();"  onblur="findCompanyDivisionByBookAg();"/></td>
						    <td width="22"><img id="customerfile.bookingAgentPopUp" class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow();document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						    <td align="left" style="width:100px;">
						 <c:if test="${agentSearchValidation}">
						<s:textfield cssClass="input-text" id="customerFileBookingAgentName" name="customerFile.bookingAgentName" cssStyle="width:193px;" maxlength="250" onchange="findPartnerDetailsByName('customerFileBookingAgentCode','customerFileBookingAgentName');fillAccountFlag();" onkeyup="findPartnerDetails('customerFileBookingAgentName','customerFileBookingAgentCode','bookingAgentNameDiv','and (isAgent=true) ','',event);"  onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'hidBookAgent');"  />
						    </c:if>
						       <c:if test="${!agentSearchValidation}">
						 <s:textfield cssClass="input-text" id="customerFileBookingAgentName" name="customerFile.bookingAgentName" cssStyle="width:193px;" maxlength="250" onchange="findPartnerDetailsByName('customerFileBookingAgentCode','customerFileBookingAgentName');fillAccountFlag();" onkeyup="findPartnerDetails('customerFileBookingAgentName','customerFileBookingAgentCode','bookingAgentNameDiv','and ((isAccount=true or isAgent=true) or (isVendor=true and typeOfVendor=\\'Agent Broker\\'))','',event);"  onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'hidBookAgent');"  />
						    </c:if>
							<div id="bookingAgentNameDiv" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
						</c:otherwise>
						</c:choose> 
							<td align="left" width="25">
								<div id="hidBookAgent" style="vertical-align:middle;"><a><img class="openpopup"  style="width:25px;height:19px;" id="noteCustBANavigations" onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'noteCustBANavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
							</td>
							<script type="text/javascript">
							showOnLoadPartnerAlert('onload','${customerFile.bookingAgentCode}','hidBookAgent');
							</script>
							<td align="left" style="width:5px"></td>
							<c:if  test="${companies == 'Yes'}">
							<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  headerKey="" headerValue="" cssStyle="width:100px" onchange="getJobList();changeStatus();getContract();getNewCoordAndSale('change');"  /></td>
							<!--<td align="left"><configByCorp:fieldVisibility componentId="customerFile.companyDivision"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  cssStyle="width:100px" onchange="changeStatus();" headerKey="" headerValue="" /></configByCorp:fieldVisibility></td>-->					
							<td align="left" style="width:16px"></td>
							</c:if>
							<c:if  test="${companies != 'Yes'}">
							<s:hidden name="customerFile.companyDivision"/>							
							</c:if>	
							<c:if test="${not empty customerFile.bookingDate}">
								<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
								<td style="width:60px;"><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td>&nbsp;<img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty customerFile.bookingDate}">
								<td style="width:60px;"><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td>&nbsp;<img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<td align="left" ></td>
							<td align="left" style="width:2px"></td>
							<c:if test="${not empty customerFile.initialContactDate}">
							<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
							<td style="width:60px;"><s:textfield id="initialContactDate" cssClass="input-text" name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" cssStyle="width:65px" maxlength="11" onfocus="changeStatus();" readonly="true" onkeydown="onlyDel(event,this)" /></td><td>&nbsp;<img id="initialContactDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty customerFile.initialContactDate}">
							<td style="width:60px;"><s:textfield id="initialContactDate" cssClass="input-text" name="customerFile.initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" onkeydown="onlyDel(event,this)"/></td><td>&nbsp;<img id="initialContactDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<td align="left"></td>
								<td align="left" style="width:2px"></td>
							    <configByCorp:fieldVisibility componentId="component.customerFile.introCall">
											 <td align="left" style="padding-right:10px;"><s:select cssStyle="width:60px" cssClass="list-menu" name="customerFile.introCall" onchange="changeStatus();" list="{'','N','Y'}"  /></td>
						</configByCorp:fieldVisibility>
						</tr>
					</tbody>
				</table>
		    	</td>
		    </tr>	
		    <tr>
			  <td>&nbsp;</td>
			  <td align="left">	
				<table width="" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
					<tbody>
						<tr>
						  <td width="" align="left" class="listwhitetext"><fmt:message key='customerFile.job'/><font color="red" size="2">*</font></td>
						  <td align="left" class="listwhitetext" style="width:29px"></td>						  
						  <td width="105" align="left" id="coordRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.coordinator'/></td>
						  <td width="105" align="left" id="coordRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.coordinator'/><font color="red" size="2">*</font></td>
						   <td align="left" class="listwhitetext" style="width:15px"></td>
						   <c:if test="${not empty customerFile.id}"><td align="left" class="listwhitetext" style="width:15px"></td></c:if>
						  <td width="" align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="customerFile.salesMan"><fmt:message key='customerFile.salesMan'/></configByCorp:fieldVisibility></td>
						</tr>
						<tr>
							<td align="left">
							<configByCorp:customDropDown listType="map" list="${job_isactive}" fieldValue="${customerFile.job}" 
   									attribute="id=job class=list-menu name=customerFile.job style=width:330px headerKey='' headerValue='' onchange=setCountry();setStatas();changeStatus();getContract();getNewCoordAndSale('change');coordinatorRequiredStatusJob();"/>
							</td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" width="105"><s:select cssClass="list-menu" cssStyle="width:175px" id="coordinator" name="customerFile.coordinator" list="%{coordinatorList}"  headerKey="" headerValue="" onchange="changeStatus();" /></td>
							
							<c:if test="${not empty customerFile.id}">
							<td align="left">
							<img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="updateAllSo()"/></td>
							</c:if>
							<td align="left"></td>
							<td align="left"><configByCorp:fieldVisibility componentId="customerFile.salesMan"><s:select cssClass="list-menu" name="customerFile.salesMan" list="%{sale}"  cssStyle="width:103px" headerKey="" headerValue="" onchange="changeStatus();" /></configByCorp:fieldVisibility></td>
						</tr>
					</tbody>
				</table>				
			  </td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tbody>
						<tr>
							<td colspan="25">
							<table style="margin:0px;" cellspacing="0" cellpadding="0" border="0" width="">
							<tr>
							<td  align="left" class="listwhitetext" id="competitiveRequiredFalse">
							<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.comptetive',this);return false" >
							<fmt:message key='customerFile.comptetive'/></a></td>
							<td  align="left" class="listwhitetext" id="competitiveRequiredTrue">
							<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.comptetive',this);return false" >
							<fmt:message key='customerFile.comptetive'/><font color="red" size="2">*</font></a></td>
							<td width="" align="left"></td>
							<c:choose>
								<c:when test="${mandatorySource == 'Y'}">
									<td  align="left" class="listwhitetext" width=""><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sourceCode',this);return false" ><fmt:message key='customerFile.sourceCode'/></a><font color="red" size="2">*</font></td>
								</c:when>
								<c:otherwise>
									<td  align="left" class="listwhitetext" width=""><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sourceCode',this);return false" ><fmt:message key='customerFile.sourceCode'/></a></td>
								</c:otherwise>
							</c:choose>
							<td width="" align="left"></td>							
							<td width=""  align="left" class="listwhitetext" >							
							<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.moveDate',this);return false" ><fmt:message key='customerFile.moveDate'/></a>							
							</td>						
							<td width="" colspan="2" align="left"></td>
							<td align="left" class="listwhitetext" width=""><fmt:message key='customerFile.salesStatus'/></td>
						  	<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
						  	<td align="left" class="listwhitetext">ILEAD #</td>
						  	</configByCorp:fieldVisibility>
						</tr>
						<tr>
							<td align="left" style="padding-right:10px;"><s:select cssStyle="width:60px" cssClass="list-menu" name="customerFile.comptetive" onchange="changeStatus();" list="{'','Y','N'}"  /></td>
							<td align="left"></td>
							<td style="padding-right:27px;">
								<configByCorp:customDropDown listType="map" list="${lead_isactive}" fieldValue="${customerFile.source}" 
   									attribute="class=list-menu name=customerFile.source style=width:262px headerKey='' headerValue='' onchange=changeStatus();"/>							
							</td>
							<td align="left"></td>
							<td align="left" class="listwhitetext"  colspan="2">
							<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
							<tr>			
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="onlyDel(event,this)" /></td>
							<td style="padding-right:10px;">&nbsp;<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="onlyDel(event,this)" /></td>
							<td style="padding-right:10px;">&nbsp;<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							</table>							
							</td>
							<td align="left" class="listwhitetext">
							<configByCorp:fieldVisibility componentId="component.field.Alternative.showForUTSIOnly">
							<c:if test="${from=='rule'}">
				<c:if test="${field=='customerFile.doNotSendQRequest'}">
				<font color="red">&nbsp;&nbsp;Do&nbsp;not&nbsp;send&nbsp;quality&nbsp;request</font>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='customerFile.doNotSendQRequest'}">
				&nbsp;&nbsp;&nbsp;Do&nbsp;not&nbsp;send&nbsp;quality&nbsp;request
				</c:if></c:if>
				<c:if test="${from!='rule'}">
				&nbsp;&nbsp;&nbsp;Do&nbsp;not&nbsp;send&nbsp;quality&nbsp;request
				</c:if>
							<s:checkbox name="customerFile.doNotSendQRequest" cssStyle="vertical-align:middle;" fieldValue="true" />&nbsp;&nbsp;
							</configByCorp:fieldVisibility>
							</td>
							<td align="left" style="padding-right:11px;"><s:textfield cssClass="input-textUpper" name="customerFile.salesStatus" value="${customerFile.salesStatus}" cssStyle="width:89px;" readonly="true" /></td> 
							<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
							<td align="left"><s:textfield cssClass="input-text" name="customerFile.iLeadNumber" cssStyle="width:101px;" maxlength="14" /></td> 
							</configByCorp:fieldVisibility>
							</tr>
							</table>
							</td> 
							<c:if test="${empty customerFile.id}">
							<td align="left" width="75"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countNotes == '0' || countNotes == '' || countNotes == null}">
									<td align="left"  width="75"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
									<td align="left"  width="75"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>						
					</tbody>
				</table></td>
			    </tr></table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tbody>
						<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
					</tbody>
				</table>	
			    <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tbody>
						<tr><td style="width:100px; height:10px;" colspan="12"></td></tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
							<c:if test="${customerFile.job !='RLO'}"><td align="left" class="listwhitetext"><fmt:message key='customerFile.estimator'/></td>
							<td align="left" class="listwhitetext" style="width:10px"></td></c:if>
							 <c:if test="${customerFile.job !='RLO'}"> 
							 <td align="left" class="listwhitetext" colspan="3"><fmt:message key='customerFile.survey'/></td>
							<td align="left"></td>							
							<td align="left" class="listwhitetext" style="width:10px" colspan="8"></td>
							<td style="width:50px;" align="left" class="listwhitetext" >Add to Cal.</td>
							<%-- <td style="width:30px;"></td>  --%>
							<c:if test="${surveyTool=='MoveCloud' || customerFile.corpID =='JKMS'}">	
							<td align="left" class="listwhitetext">Survey&nbsp;Type</td>
							</c:if>
							</c:if>
						<td align="left" class="listwhitetext" style="width:134px">Time&nbsp;Zone</td>
							<c:if test="${customerFile.corpID !='JKMS'}"><td align="left" style="width:10px"></td></c:if>
							 <c:if test="${customerFile.job !='RLO'}">
							 							<td align="left" style="width:10px"></td>
							 	 <c:if test="${moveCount > 0}">
														<td align="left" class="listwhitetext" style="width:10px"></td>
													<td align="left" class="listwhitetext" style="width:10px"  colspan="1"></td>
									</c:if>	
  							  	<c:if test="${surveyTool=='Voxme' && count > 0}">
											<td align="left" class="listwhitetext" style="width:10px"></td>
													<td align="left" class="listwhitetext" style="width:10px"  colspan="1"></td>
									</c:if>	
									
 							<td align="left"  class="listwhitetext" colspan="2">Actual&nbsp;Survey&nbsp;Date</td>
						 	</c:if>
							<configByCorp:fieldVisibility componentId="component.tab.billing.contractSystem">
							<td align="left"></td>	
							<td align="left" class="listwhitetext" colspan="2">Survey&nbsp;Observation</td>
							
							</configByCorp:fieldVisibility>
						</tr>
						<tr>
							<c:if test="${customerFile.job !='RLO'}"><td align="right" class="listwhitetext" style="width:100px">Pre Move Survey:&nbsp;</td>
							<td style="width:131px"><s:select cssClass="list-menu" name="customerFile.estimator" id="estimator" list="%{sale}"   cssStyle="width:130px" headerKey="" headerValue="" onchange="changeStatus();" /></td></c:if>
							<td align="left" style="width:10px"></td>
							<c:if test="${customerFile.job !='RLO'}">							
							<c:if test="${not empty customerFile.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
							<td width="71px"><s:textfield  cssClass="input-text" id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td>
							<td width="40px"><img onclick="surveyDateChng();" id="survey_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.survey}">
							<td width="71px"><s:textfield  cssClass="input-text" id="survey" name="customerFile.survey" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
 							<td width="40px"><img id="survey_trigger" onclick="surveyDateChng();" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							 <!--  <td>
								 <c:if test="${voxmeIntergartionFlag=='true'}">
								 	<c:if test="${surveyTool=='Voxme' }">
								 		<input type="button" value="Send To Voxme" style="width:106px;font-size:11px;margin-right:10px; margin-left:15px;" class="cssbuttonB" onclick="sendMailFromVoxme('Voxme')" />
								 	</c:if>
								 	<c:if test="${surveyTool=='MoveCloud'}">
								 		<input type="button" value="Send To MoveCloud" style="width:114px;font-size:11px;margin-right:5px; margin-left:15px;" class="cssbuttonB" onclick="sendMailFromVoxme('MoveToCloud')" />
								 	</c:if>
								 </c:if>
							</td>
							
							-->
							
 							
							<!--<c:if test="${surveyTool=='Voxme' }">	
							<c:if test="${count > 0}">
						<td align="left" width="15px"></td>
						<td align="left" class="listwhitetext">Email Status <img class="openpopup" id="openpopup.img1" style="vertical-align:bottom;"  src="<c:url value='/images/email_small.gif'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='SaveForEmail'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'SaveForEmail')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'SaveForEmail','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							</c:if>-->
							 <td align="left" class="listwhitetext" style="width:19px"></td>
							 <td colspan="9" width="">
							 
							
							
							 
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
							<tr>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td align="left" style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.surveyTime" id="surveyTime" cssStyle="width:35px;" maxlength="5"  onchange = "changeStatus();completeTimeString();" /> </td>
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td align="left" style="width:3px"></td>
							<td style="width:105px;"><s:textfield cssClass="input-text" name="customerFile.surveyTime2" id="surveyTime2" cssStyle="width:35px;" maxlength="5" onchange = "changeStatus();completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td>
							</tr>
							</table>
							</td>
							<td class="listwhitetext" width="60" align="center">
							<img src="${pageContext.request.contextPath}/images/event_calendar.png"  align="top" onclick="calendarICSFunction();" ></td>
							<c:if test="${surveyTool=='MoveCloud' || customerFile.corpID =='JKMS'}">
							<td style="width:131px"><s:select cssClass="list-menu" name="customerFile.surveyType" id="surveyType" list="%{surveytype}"   cssStyle="width:100px" onchange="changeStatus();" /></td>
							</c:if>			
							</c:if>
							<c:if test="${customerFile.job =='RLO'}">							
							<s:hidden cssClass="input-text" name="customerFile.surveyTime" id="surveyTime" />							
							<s:hidden cssClass="input-text" name="customerFile.surveyTime2" id="surveyTime2"/>
							</c:if>
							<td><s:textfield name="timeZone" value="${sessionTimeZone}" readonly="true" cssStyle="width:103px;" cssClass="input-textUpper" /></td>
							<c:if test="${surveyTool=='MoveCloud'}">	
							<c:if test="${moveCount > 0}">						
							<td align="left" class="listwhitetext" width="50"><img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;margin-left:17px; margin-top:1px;"  title="Move Cloud" src="<c:url value='/images/move4u.png'/>"  />
							</td>
							<c:if test="${customerFile.corpID !='JKMS'}">
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img style="float:right;" src="<c:url value='/images/ques-small.gif'/>" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img style="float:right;" src="<c:url value='/images/cancel001.gif'/>" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img style="float:right;" src="<c:url value='/images/tick01.gif'/>" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							</c:if>
							</c:if>
							
							 
							<c:if test="${surveyTool=='Voxme' }">	
							<s:hidden cssClass="input-text" name="customerFile.surveyType"/>
							<c:if test="${count > 0}">
						<!-- <td align="left" width="15px"></td> -->
						<td align="left" class="listwhitetext" style="width:88px;"><span>Email Status</span> <img class="openpopup" id="openpopup.img1" style="vertical-align:sub;"  src="<c:url value='/images/email_small.gif'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='SaveForEmail'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'SaveForEmail')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'SaveForEmail','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							</c:if>
							<c:if test="${customerFile.corpID !='JKMS'}">
 							<td>
								 <c:if test="${voxmeIntergartionFlag=='true'}">
								 	<c:if test="${surveyTool=='Voxme' }">
								 		<input type="button" value="Send To Voxme" style="width:106px;font-size:11px;margin-right:10px; margin-left:15px;" class="cssbuttonB" onclick="sendMailFromVoxme('Voxme')" />
								 	</c:if>
								 	<configByCorp:fieldVisibility componentId="component.tab.task.cftaskplanning">
								 	<c:if test="${surveyTool=='MoveCloud'}">
								 		<input type="button" value="Send To MoveCloud" style="width:114px;font-size:11px;margin-right:5px; margin-left:15px;" class="cssbuttonB" onclick="sendMailFromVoxme('MoveToCloud')" />
								 	</c:if>
								 	</configByCorp:fieldVisibility>
								 </c:if>
							</td>
							</c:if>
 							<c:if test="${customerFile.job !='RLO'}">
							<td align="left" class="listwhitetext" style="width:10px"></td>
							<c:if test="${not empty customerFile.actualSurveyDate}">
							<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.actualSurveyDate"/></s:text>
							<td width="96"><s:textfield cssClass="input-text" id="actualSurveyDate" name="customerFile.actualSurveyDate" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/>
							<img id="actualSurveyDate_trigger" onclick="surveyDateChng();" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							
							</td>
							<!-- <td><img id="actualSurveyDate_trigger" onclick="surveyDateChng();" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> -->
							</c:if>
							<c:if test="${empty customerFile.actualSurveyDate}">
							<td width="96"><s:textfield cssClass="input-text" id="actualSurveyDate" name="customerFile.actualSurveyDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/>
							<img id="actualSurveyDate_trigger" style="vertical-align:bottom" onclick="surveyDateChng();" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							</td>
							<!-- <td style="width:45px;"><img id="actualSurveyDate_trigger" style="vertical-align:bottom" onclick="surveyDateChng();" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> -->
							</c:if> 
							</c:if>
							<configByCorp:fieldVisibility componentId="component.tab.billing.contractSystem">
							<td style="width: 20px;"></td>
				           <td><configByCorp:customDropDown listType="map" list="${surveyObservationList}" fieldValue="${customerFile.surveyObservation}"
		                      attribute="id=customerFileForm_customerFile_surveyObservation class=list-menu name=customerFile.surveyObservation   headerKey='' headerValue='' tabindex='' style='width: 179px;' "/></td>
					</configByCorp:fieldVisibility>
 						</tr>
 						<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">
						<tr><td style="width:100px; height:10px;" colspan="12"></td></tr>
						<tr>
						<td>
						</td>
						<td colspan="20">
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
						<tr>
						<td align="left" class="listwhitetext" style="width:131px">&nbsp;</td>
						<td align="left" class="listwhitetext" style="width:10px"></td>
							<c:if test="${customerFile.job !='RLO'}">
							<td align="left" class="listwhitetext" colspan="13"><fmt:message key='customerFile.prefSurveyDate1'/></td>							 
							<td align="left" class="listwhitetext" colspan="3"><fmt:message key='customerFile.prefSurveyDate2'/></td>
								<td align="left" class="listwhitetext"></td>
						</c:if>
				        </tr>
						<tr>
						<td align="left" style="width:100px">&nbsp;</td>
						<td align="left" style="width:10px"></td>
							<c:if test="${customerFile.job !='RLO'}">
						<c:if test="${not empty customerFile.prefSurveyDate1}">
							<s:text id="customerFilePrefSurveyFormattedValue1" name="${FormDateValue}"><s:param name="value" value="customerFile.prefSurveyDate1"/></s:text>
							<td><s:textfield cssClass="input-text" id="prefSurveyDate1" name="customerFile.prefSurveyDate1" value="%{customerFilePrefSurveyFormattedValue1}" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>&nbsp;<img id="prefSurveyDate1_trigger" style="vertical-align:bottom;padding-right:9px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.prefSurveyDate1}">
							<td><s:textfield cssClass="input-text" id="prefSurveyDate1" name="customerFile.prefSurveyDate1" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>&nbsp;<img id="prefSurveyDate1_trigger" style="vertical-align:bottom;padding-right:9px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left" style="width:20px"></td>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td align="left" style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime1" id="prefSurveyTime1" size="3" maxlength="5"  onchange = "changeStatus();completeTimeString();"  /> </td>
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td align="left" style="width:3px"></td>
							<td width="90"><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime2" id="prefSurveyTime2" size="3" maxlength="5"  onchange = "changeStatus();completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td>
							<c:if test="${not empty customerFile.prefSurveyDate2}">
							<s:text id="customerFilePrefSurveyFormattedValue2" name="${FormDateValue}"><s:param name="value" value="customerFile.prefSurveyDate2"/></s:text>
							<td><s:textfield cssClass="input-text" id="prefSurveyDate2" name="customerFile.prefSurveyDate2" value="%{customerFilePrefSurveyFormattedValue2}" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>&nbsp;<img id="prefSurveyDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.prefSurveyDate2}">
							<td><s:textfield cssClass="input-text" id="prefSurveyDate2" name="customerFile.prefSurveyDate2" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>&nbsp;<img id="prefSurveyDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							 <td align="left" style="width:10px"></td>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td align="left" style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime3" id="prefSurveyTime3" size="3" maxlength="5"   onchange = "changeStatus();completeTimeString();" /> </td>
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime4" id="prefSurveyTime4" size="3" maxlength="5"  onchange = "changeStatus();completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td> 
						 </c:if> 
						</tr>
						</table>
						</td>
						</tr>						
						</configByCorp:fieldVisibility>						
						 </tr>						
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
				<tr>
							<c:if test="${customerFile.job !='RLO'}">
							<td align="right" class="listwhitetext" style="padding-top:15px;width:99px;">Origin Agent:&nbsp;</td>
							<td align="left" style="padding-top:5px;" colspan="20" >
							</c:if>
							<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">
							<tr>
							<c:if test="${customerFile.job !='RLO'}">
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext">Name</td>
							<td></td>
							<td align="left" class="listwhitetext">Contact</td>
							<td ></td>	
							<td></td>		
							<td align="left" class="listwhitetext">Email</td>
							<td align="left" class="listwhitetext">Contact Number</td>
							</c:if>
							<c:if test="${count > 0}">
							<td width="5px" colspan="3"></td>	
							</c:if>
							</tr>
							<tr>
							<c:if test="${customerFile.job !='RLO'}">
							<td><s:textfield cssClass="input-text" key="" name="customerFile.originAgentCode" id="originAgentCode" cssStyle="width:106px;" maxlength="10" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findOriginAgentName();changeStatus();resetContactEmail('OA');"  onblur="showAddressImage();" onfocus="showPartnerAlert('onchange',this.value,'hidTSOriginAgent');"/>
							</td>
							<td align="left" width="28px"><img id="openpopup.img" style="vertical-align:top;" class="openpopup" width="15" height="20" onclick="javascript:winOpenOrigin();document.forms['customerFileForm'].elements['customerFile.originAgentCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left">
							<c:if test="${agentSearchValidation}">
							<s:textfield cssClass="input-text" name="customerFile.originAgentName" id="originAgentName" cssStyle="width:187px;" maxlength="250" onkeyup="findPartnerDetails('originAgentName','originAgentCode','originAgentNameDiv',' and (isAgent=true)','',event);" onchange="valid(this,'special');resetContactEmail('OA');findOriginAgentName();showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findPartnerDetailsByName('originAgentCode','originAgentName');" onblur="showAddressImage();" />
							</c:if>
							<c:if test="${!agentSearchValidation}">
							<s:textfield cssClass="input-text" name="customerFile.originAgentName" id="originAgentName" cssStyle="width:187px;" maxlength="250" onkeyup="findPartnerDetails('originAgentName','originAgentCode','originAgentNameDiv',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onchange="valid(this,'special');resetContactEmail('OA');findOriginAgentName();showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findPartnerDetailsByName('originAgentCode','originAgentName');" onblur="showAddressImage();" />
							</c:if>
							<div id="originAgentNameDiv" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td align="left" width="10">
								<div id="hidTSOriginAgent" style="vertical-align:middle;width:21px;"><a><img  style="width:25px;height:19px;" class="openpopup" id="noteCustOriginAgentCode" onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value,'noteCustOriginAgentCode');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
							</td>
							 <td align="left" >	
					        <s:textfield id="origincontact" cssClass="input-text" key="customerFile.originAgentContact" onchange="reseEmails('OA',this.value);" cssStyle="width:180px;" tabindex="" />
				            </td>
					        <td><div id="hidImagePlus" style="vertical-align:middle;float:left; "><img class="openpopup" width="17" height="20" onclick="getContactInfo('OA',document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value);" src="<c:url value='/images/plus-small.png'/>" /></div>
					        <td align="left"></td>
					        <td style="padding-right:5px;"><s:textfield cssClass="input-text" id="originAgentEmail" name="customerFile.originAgentEmail" cssStyle="width:150px;" maxlength="65" onchange="checkEmail('customerFile.originAgentEmail');"  tabindex="" /></td>
							 <td>
					<s:textfield  cssClass="input-text" key="customerFile.originAgentPhoneNumber" maxlength="30" cssStyle="width:118px;"  onchange="reseEmails('OA',this.value);"  tabindex="" />
					</td>
					</c:if>
		<%-- 					<td>
								 <c:if test="${voxmeIntergartionFlag=='true'}">
								 	<c:if test="${surveyTool=='Voxme' }">
								 		<input type="button" value="Send To Voxme" style="width:106px;font-size:11px;margin-right:10px;" class="cssbuttonB" onclick="sendMailFromVoxme('Voxme')" />
								 	</c:if>
								 	<c:if test="${surveyTool=='MoveCloud'}">
								 		<input type="button" value="Send To MoveCloud" style="width:114px;font-size:11px;margin-right:5px;" class="cssbuttonB" onclick="sendMailFromVoxme('MoveToCloud')" />
								 	</c:if>
								 </c:if>
							</td>   --%>
							<script type="text/javascript">
							showOnLoadPartnerAlert('onload','${customerFile.originAgentCode}','hidTSOriginAgent');
							</script>
						<%-- <c:if test="${surveyTool=='Voxme' }">	
							<c:if test="${count > 0}">
						<td align="left" width="15px"></td>
						<td align="left" class="listwhitetext">Email Status2 <img class="openpopup" id="openpopup.img1" style="vertical-align:bottom;"  src="<c:url value='/images/email_small.gif'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='SaveForEmail'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'SaveForEmail')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'SaveForEmail','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							</c:if> --%>
							<%-- <c:if test="${surveyTool=='MoveCloud'}">	
							<c:if test="${moveCount > 0}">						
							<td align="left" class="listwhitetext" width="50">Status <img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;"  title="Move Cloud" src="<c:url value='/images/move_cloud.png'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							</c:if> --%>
							</tr>
							</table>
							</td>							
							</tr>
				</table>
				<c:if test="${customerFile.job !='RLO'}">
				<table cellspacing="0" cellpadding="0" border="0" style="position: absolute; margin-top: -6px; text-align: right; float: right; width:96%;">
				<tr>
				<td align="right" colspan="20">
				<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
				<tr>
						<c:if test="${empty customerFile.id}">
							<td align="right" colspan="20" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>						
						</tr>
				</table>
				</td>
				</tr>
				</table>
				</c:if>				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
					    <tr><td align="left" style="height:5px;"></td></tr>
						<tr>
						 	<td align="left" class="listwhitetext" colspan="20" style="">
						 	  <c:if test="${customerFile.job !='RLO'}"> 
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
							<tr>
							<td align="left" style="width:99px">&nbsp;</td>
							<td align="left" class="listwhitetext" style="width:90px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToAccDate',this);return false" >To&nbsp;Account</a></td>
							<td></td>
							<td align="left" ></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" width="90"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToTranfDate',this);return false" >To&nbsp;Transferee</a></td>
							<td></td>
							<td align="left"></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" colspan="3" width="90"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToBookerDate',this);return false" >To&nbsp;Booker</a></td>
							<td></td>
							<td align="left"></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" colspan="3" width="81"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.quoteAcceptenceDate',this);return false" >Quote&nbsp;Acceptance</a></td>
							 </tr>
							</table>
							</c:if>
							<td align="left"></td>
						</tr>
						<tr>	
						    <td align="left" class="listwhitetext" colspan="10">
						 	  <c:if test="${customerFile.job !='RLO'}"> 
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
							<tr>
							<td align="right" class="listwhitetext" style="width:100px"> Price&nbsp;Submission:&nbsp;</td>
							<c:choose>
							<c:when test="${checkAccessQuotation && not empty customerFile.moveType && customerFile.moveType=='Quote' && customerFile.controlFlag=='C' && not empty customerFile.priceSubmissionToAccDate }">
							<c:if test="${customerFile.quotationStatus=='Submitted'}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true"  onkeydown="" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${customerFile.quotationStatus!='Submitted'}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>							
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>							
							</c:when>
							<c:otherwise>
							<c:if test="${not empty customerFile.priceSubmissionToAccDate}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true"  onkeydown="onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToAccDate}">
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>							
							</c:otherwise>
							</c:choose>
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							<c:choose>
							<c:when test="${checkAccessQuotation && not empty customerFile.moveType && customerFile.moveType=='Quote' && customerFile.controlFlag=='C' && not empty customerFile.priceSubmissionToTranfDate}">
							<c:if test="${customerFile.quotationStatus=='Submitted'}">
						    <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:57px" maxlength="11"  readonly="true" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${customerFile.quotationStatus!='Submitted'}">
							 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:when>
							<c:otherwise>
							<c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
							<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:57px" maxlength="11"  readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToTranfDate}">
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:otherwise>
							</c:choose>
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							<c:if test="${not empty customerFile.priceSubmissionToBookerDate}">
							<s:text id="customerFilepriceSubmissionToBookerFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToBookerDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="customerFile.priceSubmissionToBookerDate" value="%{customerFilepriceSubmissionToBookerFormattedValue}" cssStyle="width:58px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToBookerDate}">
							<td width="62px"><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="customerFile.priceSubmissionToBookerDate" cssStyle="width:58px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							<c:if test="${not empty customerFile.quoteAcceptenceDate}">
							<s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
							<td width="62px"><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.quoteAcceptenceDate}">
							<td width="62px"><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							</table>
							</c:if>
							</td>
							<td align="left" style="width:10px"></td>
							<c:if test="${customerFile.job !='RLO'}"> 
							 <c:if test="${not empty customerFile.id}">
							<td><input type="button" value="Schedule Survey" style="!width:120px;width:112px;font-size:11px;" class="cssbuttonB" onclick="scheduleSurvey();" /></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<td  align="left" ><input type="button" disabled value="Schedule Survey" style="!width:120px;width:112px;font-size:11px;"  class="cssbuttonB" onclick="scheduleSurvey();"/></td>
							</c:if>						
							<c:if test="${customerFile.surveyEmailLanguage==null || customerFile.surveyEmailLanguage==''}">
							<c:set var="ischecked" value="true"/>
							<c:set var="ischecked13" value="false"/>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileGerman">
							<c:set var="ischecked" value="false"/>
							<c:set var="ischecked12" value="true"/>
							</configByCorp:fieldVisibility>
							</c:if>
							<c:if test="${customerFile.surveyEmailLanguage!=null && customerFile.surveyEmailLanguage!=''}">
								<c:set var="ischecked13" value="false"/>
								<c:set var="ischecked" value="false"/>
								<c:set var="ischecked12" value="false"/>
							<c:set var="splittedString" value="${fn:split(customerFile.surveyEmailLanguage, ',')}" />
								<c:forEach var="lang" items="${splittedString}">
									<c:if test="${fn:indexOf(lang,'ENGLISH')>=0}" >
										<c:set var="ischecked" value="true"/>
									</c:if>
									<c:if test="${fn:indexOf(lang,'GERMAN')>=0}" >
										<c:set var="ischecked12" value="true"/>
									</c:if>
									<c:if test="${fn:indexOf(lang,'DUTCH')>=0}" >
										<c:set var="ischecked13" value="true"/>
									</c:if>
								</c:forEach>
							</c:if>					
							<td align="left" style="width:31px"></td>
							<c:choose>
								<c:when test="${usertype=='ACCOUNT' || usertype=='AGENT'}">	
								</c:when>
								<c:otherwise>
									<td align="left" class="listwhitetext" style="width:10px">English</td>
									<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.showForSelectedCorpId"><s:checkbox name="checkEnglish" value="${ischecked}" fieldValue="true" onclick="changeStatus()" /></configByCorp:fieldVisibility></td>
									<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.showForBourOnly"><s:checkbox name="checkBourEnglish" value="${ischecked}" fieldValue="true" onclick="changeStatus()" /></configByCorp:fieldVisibility></td>
								</c:otherwise>
							</c:choose>
							<td align="left" style="width:9px"></td>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileDutch">
							<td align="left" class="listwhitetext" style="width:10px">Dutch</td>
							</configByCorp:fieldVisibility>
							<td align="left">
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileDutch">
							<s:checkbox name="checkDutch" value="${ischecked13}" fieldValue="true" onclick="changeStatus()" />
							</configByCorp:fieldVisibility>
							</td>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileGerman">
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext" style="width:10px">German</td>
							<td align="left">
							<s:checkbox name="checkGerman" value="${ischecked12}" fieldValue="true" onclick="changeStatus()" />
							</td>
							</configByCorp:fieldVisibility>
							<c:if test="${not empty customerFile.id}">
								<td><input type="button" name="Button1" value="Send Survey Email" style="width:108px;font-size:11px;" class="cssbuttonB" onclick="sendSurveyEmail();" /></td>
							</c:if>
							<c:if test="${empty customerFile.id}">	
								<td><input type="button" name="Button1" disabled value="Send Survey Email" style="width:108px;font-size:11px;" class="cssbuttonB" onclick="sendSurveyEmail();"  /></td>
							</c:if>
							</c:if>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
							<c:if test="${quotesToValidate=='QTG' && (customerFile.estimateNumber=='' || customerFile.estimateNumber == null)}">
							<s:hidden name="customerFile.quotesToGoFlag"/>
							<s:hidden  name="customerFile.estimateNumber" />
							<c:if test="${not empty customerFile.id}">
								<td class="listwhitetext">
								<img src="${pageContext.request.contextPath}/images/quotes-to-go.png" onclick="quotesToGoMethod();"/>
								</td>
							</c:if>
							</c:if>
							</configByCorp:fieldVisibility>
							<c:if test="${quotesToValidate!='QTG'}">
							<s:hidden name="customerFile.quotesToGoFlag" />							
							<s:hidden  name="customerFile.estimateNumber" />									
							</c:if>
					<c:set var="ischeckedQtgFlag" value="false"/>
					<c:if test="${customerFile.quotesToGoFlag}">
						<c:set var="ischeckedQtgFlag" value="true"/>
					</c:if>
					<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
					<c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null}">
					<c:if test="${not empty customerFile.id}">
					<td align="right" class="listwhitetext" style="width:10px"><s:hidden name="customerFile.quotesToGoFlag"/></td>							
					<td align="left" class="listwhitetext" style="width:140px">Sent&nbsp;to&nbsp;Quotes-To-Go</td>
					</c:if></c:if>
					</configByCorp:fieldVisibility>					
						</tr>
						<tr><td style="width:10px; height:12px;" colspan="7"></td></tr>
					</tbody>
				</table>