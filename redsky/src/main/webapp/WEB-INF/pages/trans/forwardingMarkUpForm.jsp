<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Forwarding MarkUp</title> 
<meta name="heading" content="Forwarding MarkUp"/>
<script type="text/javascript">
function checkFields(){
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.lowCost'].value.trim()==''){
		alert('Please enter Cost Low value');
		return false;
	}
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.highCost'].value.trim()==''){
		alert('Please enter Cost High value');
		return false;
	}
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.currency'].value==''){
		alert('Please select currency');
		return false;
	}
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.profitFlat'].value.trim()==''){
		alert('Please enter flat value');
		return false;
	}
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.profitPercent'].value.trim()==''){
		alert('Please enter percentage value');
		return false;
	}
	if(document.forms['forwardingMarkUpForm'].elements['forwardingMarkUpControl.contract'].value==''){
		alert('Please select contract');
		return false;
	}

}


</script>

 
</head>

 <s:form id="forwardingMarkUpForm" name="forwardingMarkUpForm" action="saveforwardingMarkUp" method="post" validate="true">
 <s:hidden name="forwardingMarkUpControl.id"/>
 
 <div id="Layer1" style="width:100%"> 
		<div id="newmnav">
		  <ul>
		  	<li><a href="forwardingMarkUpList.html"><span>ForwardingMarkUp List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>ForwardingMarkUp Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:5px; "><span></span></div>
   <div class="center-content">
		
<table class="" cellspacing="1" cellpadding="1" border="0" style="width:650px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" width="600px" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right">Cost Low</td>
		  		<td align="left"><s:textfield name="forwardingMarkUpControl.lowCost" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Cost High</td>
		  		<td align="left"><s:textfield name="forwardingMarkUpControl.highCost" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Currency</td>
		  		<td align="left"><s:select cssClass="list-menu" name="forwardingMarkUpControl.currency" list="%{currencyList}" headerKey="" headerValue="" cssStyle="width:150px" /></td>
		  	</tr>
		  	<tr>
		  		<td align="right">Flat</td>
		  		<td align="left"><s:textfield name="forwardingMarkUpControl.profitFlat" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		  		<td align="right">Percentage</td>
		  		<td align="left"><s:textfield name="forwardingMarkUpControl.profitPercent" onchange="onlyFloat(this);" maxlength="7" cssStyle="width:80px" cssClass="input-text" readonly="false"/></td>
		 		<td align="right">Contract</td>
		  		<td align="left"><s:select cssClass="list-menu" name="forwardingMarkUpControl.contract" list="%{contract}" headerKey="" headerValue="" cssStyle="width:150px"/></td>
		 	</tr>		 
	  	    <tr><td align="left" class="listwhite" colspan="7"></td></tr>
		</tbody>
	</table>
		  
		    </td>
       	  </tr>	
       	  
       	   	
	 </tbody>
</table>

</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>

<table border="0" style="margin:0px; padding:0px;">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="forwardingMarkUpControlCreatedOnFormattedValue" value="${forwardingMarkUpControl.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="forwardingMarkUpControl.createdOn" value="${forwardingMarkUpControlCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${forwardingMarkUpControl.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty forwardingMarkUpControl.id}">
								<s:hidden name="forwardingMarkUpControl.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{forwardingMarkUpControl.createdBy}"/></td>
							</c:if>
							<c:if test="${empty forwardingMarkUpControl.id}">
								<s:hidden name="forwardingMarkUpControl.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="forwardingMarkUpControlupdatedOnFormattedValue" value="${forwardingMarkUpControl.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="forwardingMarkUpControl.updatedOn" value="${forwardingMarkUpControlupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${forwardingMarkUpControl.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty forwardingMarkUpControl.id}">
								<s:hidden name="forwardingMarkUpControl.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{forwardingMarkUpControl.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty forwardingMarkUpControl.id}">
								<s:hidden name="forwardingMarkUpControl.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>


		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" onclick="return checkFields()"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty forwardingMarkUpControl.id}">
		       <input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/forwardingMarkUpForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table></div>

</s:form>