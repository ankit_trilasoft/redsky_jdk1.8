ALTER TABLE redsky.app_user ADD COLUMN mobileNumber VARCHAR(30) DEFAULT NULL AFTER contact;

Insert into refmaster (corpid, code, description, fieldlength,parameter) values ('UTSI', 'OriginAgent', 'OriginAgent', 10, 'PRULEROLE'),('UTSI', 'DestinationAgent', 'DestinationAgent', 10, 'PRULEROLE'),('UTSI', 'BookingAgent', 'BookingAgent', 10, 'PRULEROLE'),('UTSI', 'OriginSubAgent', 'OriginSubAgent', 10, 'PRULEROLE'),('UTSI', 'DestinationSubAgent', 'DestinationSubAgent', 10, 'PRULEROLE');
 
ALTER TABLE `redsky`.`todoresult` ADD COLUMN `agentCode` VARCHAR(8) AFTER `manualEmail`;

ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `resourceCategory` VARCHAR(20); 
 
ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `controlled` VARCHAR(5) DEFAULT 'false' AFTER `resourceCategory`;
 
Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', '', '', 20, 'Resource_Category',now(),'rchandra',now(),'rchandra');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','Resource_Category',20,true,now(),'rchandra',now(),'rchandra',1,1,'');

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', '', '', 20, 'TAXID_TYPE',now(),'rchandra',now(),'rchandra');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','TAXID_TYPE',20,true,now(),'rchandra',now(),'rchandra',1,1,'');

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `soAllDrivers` bit(1) DEFAULT b'0' AFTER `excludeFromParentFaqUpdate`;

update networkdatafields set type='' where modelName='AccountLine' and fieldName in ('estiamteContractCurrency','estiamteContractValueDate','estSellCurrency','estSellValueDate','estiamteContractRate','estiamteContractRateAmmount','revisionContractCurrency','revisionContractValueDate','revisionSellCurrency','revisionSellValueDate','revisionContractRate','revisionContractRateAmmount','contractCurrency','contractValueDate','contractRateAmmount');

update networkdatafields set type='CMM' where modelName='AccountLine' and fieldName in ('estimateSellRate','estimateRevenueAmount','revisionSellRate','revisionRevenueAmount');

ALTER TABLE `redsky`.`workticket` ADD COLUMN `resourceReqdAm` bit(1) AFTER `changes`,ADD COLUMN `resourceReqdPm` bit(1) AFTER `resourceReqdAm`;

update workticket set resourceReqdAm=true,resourceReqdPm=true;

CREATE TABLE `redsky`.`operationsresourcelimits` (
  `id` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(25),
  `resource` VARCHAR(45),
  `corpId` VARCHAR(11),
  `limit` DECIMAL(19,2),
  `createdBy` VARCHAR(45),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(45),
  `updatedOn` DATETIME,
  `hubId` VARCHAR(20),
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;
 
ALTER TABLE `redsky`.`operationsresourcelimits` CHANGE COLUMN `limit` `resourceLimit` DECIMAL(19,2);
 
ALTER TABLE `redsky`.`operationsdailylimits` ADD COLUMN `category` VARCHAR(50) AFTER `comment`,ADD COLUMN `resource` VARCHAR(65) AFTER `category`,ADD COLUMN `resourceLimit` DECIMAL(19,2) AFTER `resource;

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `tripNumber` VARCHAR(15) AFTER `vanID`;

update networkdatafields set type='' where modelName='AccountLine' and fieldName ='recVatDescr' and toFieldName='payVatDescr';

update networkdatafields set type='' where modelName='AccountLine' and fieldName ='recVatPercent' and toFieldName='payVatPercent';

ALTER TABLE `redsky`.`keysurveyquestionmap` MODIFY COLUMN `questionText` VARCHAR(200);
 
ALTER TABLE `redsky`.`refsurveyemailbody` MODIFY COLUMN `emailBody` VARCHAR(200), MODIFY COLUMN `description` VARCHAR(1000);

ALTER TABLE `redsky`.`qualitysurveysettings` CHANGE COLUMN `shipNumber` `surveyFrom` VARCHAR(200);

INSERT into refmaster (corpid,description,code,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values('UTSI', 'BuZa', 'http://www.keysurvey.co.uk/votingmodule/s180/survey/407666/9d0e/??', 200, 'SURVEYFROM',now(),'dkumar',now(),'dkumar'),('UTSI', 'Continental', 'http://www.keysurvey.co.uk/votingmodule/s180/survey/407666/9d0e/??', 200, 'SURVEYFROM',now(),'dkumar',now(),'dkumar'),('UTSI', 'Standard', 'http://www.keysurvey.co.uk/votingmodule/s180/survey/407666/9d0e/??', 200, 'SURVEYFROM',now(),'dkumar',now(),'dkumar'),('UTSI', 'Schaeffler', 'http://www.keysurvey.co.uk/votingmodule/s180/survey/407666/9d0e/??', 200, 'SURVEYFROM',now(),'dkumar',now(),'dkumar'),('UTSI', 'Continental (RELO)', 'http://www.keysurvey.co.uk/votingmodule/s180/survey/407666/9d0e/??', 200, 'SURVEYFROM',now(),'dkumar',now(),'dkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('UTSI','SURVEYFROM',200,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`app_user` ADD COLUMN `digitalSignature` VARCHAR(255) AFTER `mobileNumber`;

ALTER TABLE redsky.claim ADD(reimbursementCurrency varchar(6) DEFAULT NULL, reciveReimbursementCurrency varchar(6) DEFAULT NULL);

ALTER TABLE `redsky`.`itemsjequip` MODIFY COLUMN `controlled` bit(1)  DEFAULT b'0'

ALTER TABLE redsky.claim ADD COLUMN insuranceUser bit(1) DEFAULT false AFTER reciveReimbursementCurrency;

insert into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName) values('AccountLine','accountLineNumber' ,'Create','amishra',now(),'amishra',now(),false,'',''),('AccountLine','accountLineNumber' ,'Update','amishra',now(),'amishra',now(),false,'','');

ALTER TABLE `redsky`.`contractaccount` ADD COLUMN `vatDesc` VARCHAR(30), ADD COLUMN `vatPercent` VARCHAR(7) AFTER `vatDesc`;

ALTER TABLE `redsky`.`charges` MODIFY COLUMN `charge` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`charges` ADD COLUMN `payablePreset` DOUBLE DEFAULT 0.0 AFTER `payableContractCurrency`;

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `g11` bit(1) DEFAULT false ;

ALTER TABLE `redsky`.`refjobtype` ADD COLUMN `personClaims` VARCHAR(82) AFTER `surveyTool`;

ALTER TABLE redsky.partnerprivate ADD COLUMN claimsUser VARCHAR(45) DEFAULT NULL AFTER soAllDrivers;

ALTER TABLE redsky.claim ADD COLUMN claimPerson VARCHAR(45) DEFAULT NULL AFTER insuranceUser;

INSERT INTO redsky.role(name, description, corpID) VALUES('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','CWMS'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','VOER'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','VOAO'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','UGCN'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','JOHN'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','BOUR'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','UGHK'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','UTSI'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','ADAM'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','VORU'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','VOCZ'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','STAR'),('ROLE_CLAIM_HANDLER','responsible for the communication with the Transfere','SSCW');

ALTER TABLE `redsky`.`accountline` ADD COLUMN `costTransferred` bit(1) DEFAULT false ;
 
ALTER TABLE `redsky`.`company` ADD COLUMN `useCostTransferred` bit(1) DEFAULT false ;

CREATE TABLE  `redsky`.`resourcecontractcharges` (
`id` bigint(20) NOT NULL auto_increment,
`contract` varchar(45) default NULL,
`charge` varchar(45) default NULL,
`corpId` varchar(10) default NULL,
`createdBy` varchar(45) default NULL,
`createdOn` datetime default NULL,
`updatedBy` varchar(45) default NULL,
`updatedOn` datetime default NULL,
`parentId` bigint(20) default NULL,
PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `comments` VARCHAR(200) DEFAULT NULL;

ALTER TABLE `redsky`.`charges` ADD COLUMN `commissionable` bit(1) DEFAULT false;

ALTER TABLE `redsky`.`company` ADD COLUMN `lastTripNumber` VARCHAR(15) AFTER `useCostTransferred`;
 
ALTER TABLE `redsky`.`accountline` ADD COLUMN `categoryResource` VARCHAR(10) DEFAULT NULL AFTER `costTransferred`;

CREATE TABLE `redsky`.`glcoderategrid` (
  `id` BIGINT(20) AUTO_INCREMENT,
  `corpID` VARCHAR(30) DEFAULT '',
  `createdBy` VARCHAR(82),
  `updatedBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedOn` DATETIME,
  `costElementId` BIGINT(20),
  `costElement` VARCHAR(7),
  `gridRecGl` VARCHAR(20),
  `gridPayGl` VARCHAR(20),
  `gridJob` VARCHAR(3),
  `gridRouting` VARCHAR(5),
  PRIMARY KEY(`id`)
);
 
ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `vanLineCompanyDivision` VARCHAR(10) AFTER `currentPay`, ADD COLUMN `uvlContract` VARCHAR(65) AFTER `vanLineCompanyDivision`, ADD COLUMN `uvlBillToCode` VARCHAR(8) AFTER `uvlContract`, 
ADD COLUMN `uvlBillToName` VARCHAR(225) AFTER `uvlBillToCode`, ADD COLUMN `mvlContract` VARCHAR(65) AFTER `uvlBillToName`, ADD COLUMN `mvlBillToCode` VARCHAR(8) AFTER `mvlContract`, ADD COLUMN `mvlBillToName` VARCHAR(225) AFTER `mvlBillToCode`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `categoryResourceName` VARCHAR(45) DEFAULT NULL AFTER `categoryResource`;

ALTER TABLE `redsky`.`charges` MODIFY COLUMN `charge` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci;





 
