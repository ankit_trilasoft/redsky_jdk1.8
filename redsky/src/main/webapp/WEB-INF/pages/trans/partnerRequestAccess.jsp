<%@ include file="/common/taglibs.jsp"%>
<head>
	<title>Partner Request Access</title>
	<meta name="heading" content="Partner Request Access"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
</head>
<script language="javascript" type="text/javascript">

function checkFields(){
	selectAll('userJobType');
	var countryCode= document.forms['partnerRequestAccess'].elements['userCountry'].value;
	var ListBoxOptions = document.getElementsByName('userJobType')[0].options;
	
	if(document.forms['partnerRequestAccess'].elements['userCountry'].value==''){
  			alert("Please select the country");
  			document.forms['partnerRequestAccess'].elements['userCountry'].focus();
  			return false;
  	}
  	if(countryCode=='USA' ||countryCode=='IND'||countryCode=='CAN'){
  		if(document.forms['partnerRequestAccess'].elements['userState'].value==''){
  			alert("Please select the state");
  			document.forms['partnerRequestAccess'].elements['userState'].focus();
  			return false;
  		}
  	}
  	if(document.forms['partnerRequestAccess'].elements['userBranch'].value==''){
  			alert("Please select the company name");
  			document.forms['partnerRequestAccess'].elements['userBranch'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['userLastName'].value==''){
  			alert("Please enter the last name");
  			document.forms['partnerRequestAccess'].elements['userLastName'].focus();
  			return false;
  	}
	if(document.forms['partnerRequestAccess'].elements['userTitle'].value==''){
			alert("Please enter the Job Title");
			document.forms['partnerRequestAccess'].elements['userTitle'].focus();
			return false;
	}
  	if(document.forms['partnerRequestAccess'].elements['userFirstName'].value==''){
  			alert("Please enter the first name");
  			document.forms['partnerRequestAccess'].elements['userFirstName'].focus();
  			return false;
  	}
  	
  	
  	if(document.forms['partnerRequestAccess'].elements['userPhoneNumber'].value==''){
  			alert("Please enter the phone1");
  			document.forms['partnerRequestAccess'].elements['userPhoneNumber'].focus();
  			return false;
  	}

  	if(document.forms['partnerRequestAccess'].elements['userEmail'].value==''){
  			alert("Please enter the emailID");
  			document.forms['partnerRequestAccess'].elements['userEmail'].focus();
  			return false;
  	}
  	var total = '0';
         for(var i = 0; i < ListBoxOptions.length; i++) {
                
                	total = total+1;
            
        }
        if(total == 0)
        {
        	alert("Please select job function.");
        	return false;
        }
  
	return echeck();
}


    
 function getCompanyName(){
 		var countryCode= document.forms['partnerRequestAccess'].elements['userCountry'].value;
 		var stateCode= document.forms['partnerRequestAccess'].elements['userState'].value;
 		var sessionCorpID= document.forms['partnerRequestAccess'].elements['sessionCorpID'].value;
 		if(countryCode=='')
 		{
 			alert('Please select the country');
 			return false;
 		}else{
 		if(countryCode=='USA')
 		{
 			if(stateCode=='')
 			{
 				alert('Please select the state');
 			}else{
 			javascript:openWindow("agentPopUp.html?bypass_sessionfilter=YES&partnerType=AG&userCountry="+countryCode+"&sessionCorpID="+sessionCorpID+"&stateCode="+stateCode+"&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=userBranch&fld_code=partnerCode");
 			}
 		}else{
 		
 		javascript:openWindow("agentPopUp.html?bypass_sessionfilter=YES&partnerType=AG&userCountry="+countryCode+"&sessionCorpID="+sessionCorpID+"&stateCode="+stateCode+"&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=userBranch&fld_code=partnerCode");
 		}
		}
	
	}

function copyToAllias(){
	var firstName= document.forms['partnerRequestAccess'].elements['userFirstName'].value;
	document.forms['partnerRequestAccess'].elements['userAlias'].value=firstName;
}
function onlyPhoneNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
	

function echeck() {
var str=document.forms['partnerRequestAccess'].elements['userEmail'].value;
		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID");
		   document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		   return false;
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		   return false;
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID");
		     document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID");
		     document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }
			
 		 return true;					
	}
	
function displayState()
{
		var country=document.forms['partnerRequestAccess'].elements['userCountry'].value;
	  	var e2 = document.getElementById('hid1');
	  	var enbState = '${enbState}';
		var index = (enbState.indexOf(country)> -1);
		if(index != '' && country!=''){
		e2.style.visibility = 'visible';
		}else{
		e2.style.visibility = 'hidden';
		}
		

}

function copyJobFunction(target){
	document.forms['partnerRequestAccess'].elements['selectedJobFunction'].value= document.forms['partnerRequestAccess'].elements['selectedJobFunction'].value +"," +target.value;
}

function openEmailForm(){
	window.open('agentEmailForm.html?id=${pricingControl.id}&bypass_sessionfilter=YES&sessionCorpID=${sessionCorpID}&decorator=popup&popup=true','surveysList','height=520,width=950,top=200, left=100, scrollbars=yes,resizable=yes');
}

function onlyAlphaNumericAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==190) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
}
function getState(targetElement) {
	var country = targetElement.value;
	var enbState = '${enbState}';
	 var index = (enbState.indexOf(country)> -1);
	 if(index != ''){
     var url="findStateService.html?bypass_sessionfilter=YES&sessionCorpID=${sessionCorpID}&decorator=simple&popup=true&bucket2="+encodeURI(country); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
	 }
}
function handleHttpResponse5(){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['partnerRequestAccess'].elements['userState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['partnerRequestAccess'].elements['userState'].options[i].text = '';
			document.forms['partnerRequestAccess'].elements['userState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['partnerRequestAccess'].elements['userState'].options[i].text = stateVal[1];
			document.forms['partnerRequestAccess'].elements['userState'].options[i].value = stateVal[0]; 
    }
} 
    }
}
   var httpState = getHTTPObjectState()
  function getHTTPObjectState()
  {
      var xmlhttp;
      if(window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else if (window.ActiveXObject)
      {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          if (!xmlhttp)
          {
              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
          }
      }
      return xmlhttp;
  } 
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46)||(keyCode==189)||(keyCode==173); 
	}
	
	function requiredState(){
	    var originCountry = document.forms['partnerRequestAccess'].elements['userCountry'].value;
		var r2=document.getElementById('redHiddenFalse');
		var r1=document.getElementById('redHidden');
		if(originCountry == 'USA' || originCountry == 'CAN' || originCountry == 'IND'){	
		    var enbState = '${enbState}';
			var index = (enbState.indexOf(originCountry)> -1);
			if(index != ''){
			r1.style.display = 'block';
			r2.style.display = 'none';
			}else{	
				r1.style.display = 'none';
				r2.style.display = 'block';		
			}
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}
</script>
<style type="text/css">
p{line-height:0px;padding:0px;margin:0px;}

div.error, span.error, li.error, div.message {
width:450px;
}

table.pickList11 td select {
    width: 150px;
}
form .info {
text-align:left;
padding-left:15px;
}

input.button, button {
padding:2px 2px 0 0;
width:4em;
}

form {
margin-top:0px;
!margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}

#mainPopup {padding:0px;}

#main-top {
background-image:url(images/bg-band.gif);
background-position:left bottom;
background-repeat:repeat-x;
border-bottom:1px solid #D7E4EC;
height:63px;
margin:0;
padding:0;
}
</style>



 <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>

<s:form id="partnerRequestAccess" name="partnerRequestAccess" action="savePartnerRequestAccess.html?decorator=popup&popup=true&bypass_sessionfilter=YES" method="post" validate="true">
<s:hidden key="user.id"/>
<s:hidden name="partnerCode"/>
<c:set var="bypass_sessionfilter" value="<%=request.getParameter("bypass_sessionfilter") %>" />
<s:hidden name="bypass_sessionfilter" value="<%=request.getParameter("bypass_sessionfilter") %>" />
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden key="user.version"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<div id="branding">
<!--<s:textfield name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
<c:set var="fileID" value="<%=request.getParameter("id") %>"/> -->

<div id="main-top">    
          <table cellpadding="0" cellspacing="0" border="0" >
          <tr>
          <td width="19">&nbsp;</td>
          <td width=""><img src="<c:url value='/images/logo_redsky.png'/>" width="112" height="63" style="margin-top:5px;"/></td>		                     
          </tr>
          </table>
          </div>
 
</div>

<div id="Layer1" style="width:70%;margin-left:10px;margin-top:10px;clear:both;"> 

		<div id="newmnav">
		  <ul>
		  	<li id="newmnav1" style="margin-top:30px;"><a class="current"><span>Partner Portal Id Request Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top" style="!margin-top:7px;">
   <div class="top"><span></span></div>
   <div class="center-content">
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
   <tr>
 	 	<td align="left" class="listwhitetext" height="30px" colspan="4" style="padding-top:8px;"><font color="red">Please enter data in all the mandatory fields and then submit your request for access to redsky.</font></td>
 		<td align="left" height="15px"></td>
 		
 		<td align="right" height="15px">
 		<div id="privacyText">
   	<%-- View our <a href="javascript: void(0)" onclick="popup2('${pageContext.request.contextPath}/privacypolicy.jsp')" style="text-decoration:underline;">Privacy Policy</a>. --%>
    View our <a href="https://redskymobility.com/privacy-policy" target="_blank" style="margin-top: 15px !important; display: inline-block;">Privacy Policy</a>.
  </div>
 		
 		</td>
   </tr>
   <tr><td colspan="7">
	<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
		<tr>
			<td class="probg_left"></td>
			<td NOWRAP class="probg_center">My Company Details</td>
			<td class="probg_right"></td>
		</tr>
	</table>
	</td>
	</tr>	
	</table>										
  	<table border="0" cellpadding="2" cellspacing="1" width="100%">
		  <tbody>  	
		 
		  	<tr>
		  		<td align="right" width="120px" class="listwhitetext"><fmt:message key="user.address.country"/><font color="red" size="2">*</font></td>
		  	  	<td align="left"><s:select cssClass="list-menu" id="userCountry" name="userCountry" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:170px"  onchange="requiredState();displayState();getState(this);"/>
		  	  	<td colspan="2" align="left" style="margin: 0px;padding:0px; ">
				<div id="hid1" style="margin: 0px;padding:0px;">
				 	<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0"  style="margin: 0px;padding:0px;">
					<tbody>
					<tr>
					
						<td align="right" id="redHiddenFalse" class="listwhitetext">State</td>
											<td align="right"  id="redHidden"  class="listwhitetext">State<font color="red" size="2">*</font></td>
		  	  		<td align="left"><s:select cssClass="list-menu" id="userState" name="userState" list="%{states}" headerKey="" headerValue="" cssStyle="width:170px"/>
					</tr>
					</tbody>
					</table>
				</div>																													
				</td>	
		  	  	
		  	
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Company Name<font color="red" size="2">*</font></td>
		  		<td align="left" colspan="5"><s:textfield name="userBranch"  maxlength="100" size="28" cssClass="input-textUpper" readonly="true" /><img align="top" class="openpopup" width="17" height="20" onclick="return getCompanyName();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
				<i>(Click to select your company)</i></td>
				
			</tr>
			<tr>
			<td align="right" class="listwhitetext" colspan="2"><font color="red" size="2">*</font>If your company is not listed, click <a  onClick="openEmailForm()" style="text-decoration: underline; cursor: pointer;">here</a> </td>
			</tr>
			<tr><td colspan="7">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center">My Details</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										</td>
										</tr>
			
		  	<tr>
		  		<td align="right" class="listwhitetext">First Name<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userFirstName"  maxlength="100" size="28" cssClass="input-text" readonly="false" onchange="copyToAllias();" onkeydown="return onlyCharsAllowed(event)" /></td>
		  	
		  		<td align="right" class="listwhitetext">Last Name<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userLastName"  maxlength="100" size="28" cssClass="input-text" readonly="false" onkeydown="return onlyCharsAllowed(event)"/></td>
		  		
		    </tr>
		  	<tr>
<%-- 		  		<td align="right" class="listwhitetext">Alias Name<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userAlias"  maxlength="100" size="28" cssClass="input-text" readonly="false"/></td> --%>
		  		
		  	    <td align="right" class="listwhitetext" width="90px">Job Title<font color="red" size="2">*</font></td>
			 	<td align="left"><s:textfield name="userTitle" cssClass="input-text" size="28" maxlength="50" onkeydown="return onlyAlphaNumericAllowed(event)"/></td>
		  	
		  			<td align="right" class="listwhitetext">Office Phone:<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userPhoneNumber"  maxlength="100" size="28" cssClass="input-text" readonly="false" onkeypress="return isNumberKey(event)"/></td>
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Email<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield id="userEmail" name="userEmail"  maxlength="100" size="28" cssClass="input-text" readonly="false" onchange="return echeck()"/></td>
		  		  		<td align="right" class="listwhitetext">Mobile Phone:<font color="red" size="2"></font></td>
		  		<td align="left"><s:textfield name="mobileNumber"  maxlength="100" size="28" cssClass="input-text" readonly="false" onkeypress="return isNumberKey(event)"/></td>
		  	
		  	</tr>
		  <tr>
		  	
		  	<td align="left" class="colored_bg"  colspan="7">
		  	 
       	 
			        <fieldset >
			            <legend>Job Functions<font color="red" size="2">*</font></legend>
			            <table class="pickList" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Function List</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Selected Functions</label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${availableJobTypes}" scope="request"/>
			                <s:set name="rightList" value="userJobType1" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList1.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availableJobTypes && !userJobType"/>
			                    <c:param name="rightId" value="userJobType"/>
			                </c:import>
			            </table>
			        </fieldset>
			  
			   
			    	
			    </td>
		  	
		  	
		  
		  	</tbody>
	</table>
  
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

		   
		  <table class="detailTabLabel" border="0" style="margin: 0px;padding: 0px;" width="100%">
		  <tbody> 	
		   	
		   <tr>
		  		<td align="right">
        		<s:submit cssClass="cssbutton1" type="button"   value="Submit" onclick="return checkFields();"/>  
        		</td>
       
        		<td align="left">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        	
       	  	</tr>		  	
		  </tbody>
		  </table>
		  
		  </div>

</s:form>

<script type="text/javascript">
<c:if test="${accountMessage=='User already exist with this emailID. Please change the emailID.'}">
document.forms['partnerRequestAccess'].elements['userEmail'].select();
</c:if>
	try{
		var country=document.forms['partnerRequestAccess'].elements['userCountry'].value;
		}
		catch(e){}
		try{
		var e2 = document.getElementById('hid1');
		}
		catch(e){}
		try{
		if(country == 'USA'){
		e2.style.visibility = 'visible';
		}else{
		e2.style.visibility = 'hidden';
		}
		}
		catch(e){}
		function isNumberKey(evt)
			      {
			         var charCode = (evt.which) ? evt.which : event.keyCode
			        		 if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
			        		        return false;
			        		    return true;
			      }
</script>


<script type="text/javascript">
<!--
function popup2(url) 
{
 var width  = 850;
 var height = 800;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}
// -->
</script>