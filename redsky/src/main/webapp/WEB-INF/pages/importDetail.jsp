<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="importDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='importDetail.heading'/>"/>   
</head> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<s:form id="internationalForm" action="saveInternational" method="post" validate="true">   
<s:hidden name="international.id" value="%{international.id}"/>
<table width="100%" border="0">

<tr>
<td><input type="checkbox" name="checkbox" value="checkbox"></td>
<td align="left" width="2"><input type="button" name="Free Entry" value="Free Entry"></td>
<td><input type="checkbox" name="checkbox" value="checkbox"></td>
<td align="left" width="2"><input type="button" name="3299" value="3299"></td>
<td align="left" width="5%"><s:label label="Clear Customs" /></td>
<td width="18%"><s:select name="international.clearCustom" list="{':','T','A'}"/></td>
<td align="left" width="12%"><s:textfield name="international.clearCustom" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Original OBL" /></td>
<td align="left" width="12%"><s:textfield name="international.originBL" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Sent 3299" /></td>
<td align="left" width="12%"><s:textfield name="international.recived3299" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Clear at WH or Pier" /></td>
<td width="18%"><s:select name="international.clearCustomTA" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Forwardereighted OBL" /></td>
<td align="left" width="12%"><s:textfield name="international.freightOBLTA" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Received 3299" /></td>
<td align="left" width="12%"><s:textfield name="international.mail3299" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Pickup pier/Arrv WH" /></td>
<td align="left" width="18%"><s:select name="international.arriveWH" list="{':','T','A'}"/></td>
<td align="left" width="12%"><s:textfield name="international.arriveWH" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="OMNI" /></td>
<td align="left" width="12%"><s:textfield name="transportcargo.omni" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="3299 to Broker" /></td>
<td align="left" width="12%"><s:textfield name="international.sent3299ToBroker" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Return Container" /></td>
<td width="18%"><s:select name="international.returnContainer" list="{':','T','A'}"/></td>
<td align="left" width="12%"><s:textfield name="international.returnContainer" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Broker Delivery Order" /></td>
<td align="left" width="12%"><s:textfield name="international.deliveryInstruction" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Rqstd Free Entry" /></td>
<td align="left" width="12%"><s:textfield name="international.freeEntryRequest" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Location" /></td>
<td align="left" width="12%"><s:textfield name="international.returnLocation" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Last Free Day" /></td>
<td align="left" width="12%"><s:textfield name="international.lastFree" required="true" cssClass="text medium"/></td>
</tr>


<tr>
<td align="left" width="5%"><s:label label="Requested by" /></td>
<td align="left" width="12%"><s:textfield name="international.freeEntryBy" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Inspector" /></td>
<td align="left" width="12%"><s:textfield name="international.inspectorYN" required="true" cssClass="text medium"/></td>
<td align="left" width="12%"><s:textfield name="international.inspectorName" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="FE Location" /></td>
<td align="left" width="12%"><s:textfield name="international.freeEntryWhere" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Email" /></td>
<td align="left" width="12%"><s:textfield name="international.email" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Demurrage?" /></td>
<td width="18%"><s:select name="international.demurrageYN" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Per Diem?" /></td>
<td width="18%"><s:select name="international.perdiumYN" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Cstms Xray?" /></td>
<td width="18%"><s:select name="international.xrayYN" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Cstms Inspctn?" /></td>
<td width="18%"><s:select name="international.inspectorYN" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="USDA?" /></td>
<td width="18%"><s:select name="international.usdaYN" list="{':','T','A'}"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Payment By" /></td>
<td width="18%"><s:select name="international.demurragePayment" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Payment By" /></td>
<td width="18%"><s:select name="international.perdiumPayment" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Payment By" /></td>
<td width="18%"><s:select name="international.xrayPaymentTrack" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Payment By" /></td>
<td width="18%"><s:select name="international.inspectorPaymentTrack;
" list="{':','T','A'}"/></td>
<td align="left" width="5%"><s:label label="Payment By" /></td>
<td width="18%"><s:select name="international.usdaPaymentTrack" list="{':','T','A'}"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Cost/Day" /></td>
<td align="left" width="12%"><s:textfield name="international.demurrageCostPerDay1" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Cost/Day" /></td>
<td align="left" width="12%"><s:textfield name="international.perdiumCostPerDay" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Cost" /></td>
<td align="left" width="12%"><s:textfield name="international.xrayCost" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Cost" /></td>
<td align="left" width="12%"><s:textfield name="international.inspectorCost" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Cost" /></td>
<td align="left" width="12%"><s:textfield name="international.usdaCost" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Cost/Day2" /></td>
<td align="left" width="12%"><s:textfield name="international.demurrageCostPerDay2" required="true" cssClass="text medium"/></td>
</tr>

</table>
</s:form>
  
<script type="text/javascript">   
    Form.focusFirstElement($("internationalForm"));   
</script>
   
