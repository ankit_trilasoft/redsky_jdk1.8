delimiter $$

CREATE  trigger redsky.trigger_add_history_glcommission
BEFORE UPDATE on redsky.glcommission
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





IF (OLD.glType <> NEW.glType ) THEN
      select CONCAT(fieldNameList,'glcommission.glType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glType,'~') into newValueList;
  END IF;



IF (OLD.code <> NEW.code ) THEN
      select CONCAT(fieldNameList,'glcommission.code~') into fieldNameList;
      select CONCAT(oldValueList,OLD.code,'~') into oldValueList;
      select CONCAT(newValueList,NEW.code,'~') into newValueList;
  END IF;




IF (OLD.type <> NEW.type ) THEN
      select CONCAT(fieldNameList,'glcommission.type~') into fieldNameList;
      select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
      select CONCAT(newValueList,NEW.type,'~') into newValueList;
  END IF;




IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'glcommission.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;



IF (OLD.charge <> NEW.charge ) THEN
      select CONCAT(fieldNameList,'glcommission.charge~') into fieldNameList;
      select CONCAT(oldValueList,OLD.charge,'~') into oldValueList;
      select CONCAT(newValueList,NEW.charge,'~') into newValueList;
  END IF;




IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'glcommission.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;




IF (OLD.glCode <> NEW.glCode ) THEN
      select CONCAT(fieldNameList,'glcommission.glCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glCode,'~') into newValueList;
  END IF;





  CALL add_tblHistory (OLD.id,"glcommission", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;