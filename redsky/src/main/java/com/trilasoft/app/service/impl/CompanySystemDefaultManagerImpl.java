package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CompanySystemDefaultDao;
import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.model.CompanySystemDefault;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanySystemDefaultManager;
import com.trilasoft.app.service.SystemDefaultManager;

public class CompanySystemDefaultManagerImpl  extends GenericManagerImpl<CompanySystemDefault, Long> implements CompanySystemDefaultManager {
	CompanySystemDefaultDao companySystemDefaultDao;

	public CompanySystemDefaultManagerImpl(CompanySystemDefaultDao companySystemDefaultDao) {
		super(companySystemDefaultDao);
		this.companySystemDefaultDao = companySystemDefaultDao;
	}

	/*public List<CompanySystemDefault> findByCorpID(String sessionCorpID) {
		
		return companySystemDefaultDao.findByCorpID(sessionCorpID);
	} */
	 
}
