<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="lockPayroll.title"/></title> 
<meta name="heading" content="<fmt:message key='lockPayroll.heading'/>"/> 
</head> 

<s:form id="lockPayrollList" name="lockPayrollList" action="" method="post" validate="true">
<div id="Layer1" style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Lock Payroll List</span></a></li>
		  </ul>
</div>
		<div class="spnblk">&nbsp;</div>
		
<s:set name="lockPayrollList" value="lockPayrollList" scope="request"/>
<display:table name="lockPayrollList" class="table" requestURI="" id="lockPayrollListId"  defaultsort="1" export="true" pagesize="30"> 
<display:column property="code" sortable="true" href="lockPayrollForm.html" paramId="id" paramProperty="id" titleKey="lockPayroll.code"/> 
<display:column property="description" sortable="true"  titleKey="lockPayroll.description"/> 
<display:column property="stopDate" sortable="true" titleKey="lockPayroll.stopDate" format="{0,date,dd-MMM-yyyy}" style="width:100px"/> 
<display:setProperty name="export.excel.filename" value="Lock Payroll List.xls"/>
<display:setProperty name="export.csv.filename" value="Lock Payroll List.csv"/>
</display:table>
</div>
</s:form>
<script type="text/javascript"> 
<c:if test="${hitFlag1=='1'}">
<c:redirect url="/lockPayroll.html"/>
</c:if>
highlightTableRows("lockPayrollListId"); 
</script>
