
// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


Alreday on prod :-

ALTER TABLE `redsky`.`company` ADD COLUMN `generateInvoiceBy` VARCHAR(100) DEFAULT '';
update company set generateInvoiceBy='Company';
update company set generateInvoiceBy='CompanyDivision' where corpid='STAR';
ALTER TABLE `redsky`.`companydivision` ADD COLUMN `lastInvoiceNumber` INTEGER DEFAULT 0 ,
ADD COLUMN `lastCreditInvoice` INTEGER DEFAULT 0 ;

-->

ALTER TABLE `redsky`.`claim` ADD COLUMN `propertyDamageAmount` DECIMAL(19,2) NULL  AFTER `claimEmailSent` ,
ADD COLUMN `customerSvcAmount` DECIMAL(19,2) NULL  AFTER `propertyDamageAmount` ,
ADD COLUMN `lostDamageAmount` DECIMAL(19,2) NULL  AFTER `customerSvcAmount` ,
ADD COLUMN `totalPaidToCustomerAmount` DECIMAL(19,2) NULL  AFTER `lostDamageAmount` ;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `taskId` VARCHAR(100) NULL;

alter table redsky.company add status boolean ;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `welcomeEmailOn` DATETIME NULL  AFTER `taskId` ;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `proNumber` VARCHAR(45) NULL  AFTER `targetdeliveryShipper` , ADD COLUMN `packingWeightOn` DATETIME NULL  AFTER `proNumber` , ADD COLUMN `confirmationOn` DATETIME NULL  AFTER `packingWeightOn` ;