package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.PersonDao;   
import com.trilasoft.app.model.Person;   
import com.trilasoft.app.service.PersonManager;   
import org.appfuse.service.impl.GenericManagerImpl;   
  
import java.util.List;   
  
public class PersonManagerImpl extends GenericManagerImpl<Person, Long> implements PersonManager {   
    PersonDao personDao;   
  
    public PersonManagerImpl(PersonDao personDao) {   
        super(personDao);   
        this.personDao = personDao;   
    }   
  
    public List<Person> findByLastName(String lastName) {   
        return personDao.findByLastName(lastName);   
    }   
}  
