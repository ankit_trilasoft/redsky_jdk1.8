package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PartnerVanLineRef;
import com.trilasoft.app.service.PartnerVanLineRefManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerVanLineRefAction extends BaseAction implements Preparable {

	private String sessionCorpID;

	private List partnerVanlineRefs;

	private Long id;

	private String partnerCodeForRef;

	private PartnerVanLineRef partnerVanLineRef;

	private PartnerVanLineRefManager partnerVanLineRefManager;

	private PartnerPublic partner;

	private List companyCodeList = new ArrayList();

	private String partnerCode;

	private String hitFlag = "";
	
	private Map<String, String> job;
	
	private RefMasterManager refMasterManager; 
	
	private String jobType;

	public PartnerVanLineRefAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public void prepare() throws Exception {
		getComboList(sessionCorpID);
	}

	public String getComboList(String corpId) {
		job = refMasterManager.findByParameter(corpId, "JOB");
		return SUCCESS;
	}

	public String list() {
		partner = (PartnerPublic) partnerVanLineRefManager.getpartner(partnerCodeForRef, sessionCorpID).get(0);
		partnerVanlineRefs = partnerVanLineRefManager.getPartnerVanLineReferenceList(sessionCorpID, partnerCodeForRef);
		return SUCCESS;
	}

	public String edit() {
		//getComboList(sessionCorpID);
		if (id != null) {
			partnerVanLineRef = partnerVanLineRefManager.get(id);
			partner = (PartnerPublic) partnerVanLineRefManager.getpartner(partnerVanLineRef.getPartnerCode(), sessionCorpID).get(0);
			
			

		} else {
			partner = (PartnerPublic) partnerVanLineRefManager.getpartner(partnerCodeForRef, sessionCorpID).get(0);
			partnerVanLineRef = new PartnerVanLineRef();
			partnerVanLineRef.setCorpID(sessionCorpID);
			partnerVanLineRef.setPartnerCode(partnerCodeForRef);
			partnerVanLineRef.setUpdatedOn(new Date());
			partnerVanLineRef.setCreatedOn(new Date());
			

		}
		return SUCCESS;
	}

	@SkipValidation
	public String deleteVanlineRef() {
		String key = "Vanline Ref has been deleted successfully.";
		saveMessage(getText(key));
		partnerVanLineRefManager.updateDeleteStatus(id);
		hitFlag = "1";
		list();
		return SUCCESS;
	}

	public String save() throws Exception {
		
		
		
		//getComboList(sessionCorpID);
		if (cancel != null) {
			return "cancel";
		}
		boolean isNew = (partnerVanLineRef.getId() == null);
		if(isNew){
			List vanLineCodeList = partnerVanLineRefManager.checkJobType(partnerVanLineRef.getVanLineCode(), sessionCorpID, partnerVanLineRef.getJobType());
			if(!vanLineCodeList.isEmpty()){
				String key = "Vanline code for this job type already exist.";
				errorMessage(getText(key));
				return SUCCESS;
			}
			
		}
		partnerVanLineRef.setUpdatedOn(new Date());
		partnerVanLineRef.setUpdatedBy(getRequest().getRemoteUser());
		partnerVanLineRef.setCorpID("TSFT");
		partnerVanLineRef = partnerVanLineRefManager.save(partnerVanLineRef);
		hitFlag = "1";
		partner = (PartnerPublic) partnerVanLineRefManager.getpartner(partnerVanLineRef.getPartnerCode(), sessionCorpID).get(0);
		String key = (isNew) ? "partnerVanLineRef.added" : "partnerVanLineRef.updated";
		saveMessage(getText(key));
		if (!isNew) {
			return INPUT;
		} else {
			return SUCCESS;
		}
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartnerCodeForRef() {
		return partnerCodeForRef;
	}

	public void setPartnerCodeForRef(String partnerCodeForRef) {
		this.partnerCodeForRef = partnerCodeForRef;
	}

	public PartnerPublic getPartner() {
		return partner;
	}

	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}

	public List getCompanyCodeList() {
		return companyCodeList;
	}

	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public List getPartnerVanlineRefs() {
		return partnerVanlineRefs;
	}

	public void setPartnerVanlineRefs(List partnerVanlineRefs) {
		this.partnerVanlineRefs = partnerVanlineRefs;
	}

	public void setPartnerVanLineRefManager(PartnerVanLineRefManager partnerVanLineRefManager) {
		this.partnerVanLineRefManager = partnerVanLineRefManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public PartnerVanLineRef getPartnerVanLineRef() {
		return partnerVanLineRef;
	}

	public void setPartnerVanLineRef(PartnerVanLineRef partnerVanLineRef) {
		this.partnerVanLineRef = partnerVanLineRef;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

}
