package com.trilasoft.app.dao.hibernate;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.AgentRequestDao;
import com.trilasoft.app.dao.AgentRequestReasonDao;
import com.trilasoft.app.dao.hibernate.PartnerPublicDaoHibernate.PartnerDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AgentRequest;
import com.trilasoft.app.model.AgentRequestReason;
import com.trilasoft.app.model.PartnerPublic;


public class AgentRequestReasonDaoHibernate extends GenericDaoHibernate<AgentRequestReason, Long> implements AgentRequestReasonDao {
	
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public AgentRequestReasonDaoHibernate() {
		super(AgentRequestReason.class);
	}

	public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID){			
		List <String>list  = this.getSession().createSQLQuery("Select concat(if(description is null,' ',description),'~',if((branch is null or branch='' or branch='No'),'No','Yes'),'~',if((bucket2 is null or bucket2=''),'NoBucket',bucket2)) from refmaster where parameter='"+parameter+"' and (language='en' or language='' or language is null )").list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for(String pageL: list){
			 String arr[]=pageL.split("~");
			 if((arr[0]!=null)&&(!arr[0].toString().equalsIgnoreCase(""))){
			 parameterMap.put(arr[0].toString(), arr[1].toString()+"#"+arr[2].toString());
			 }
		}
		return parameterMap;		
	}

	public List getPartnerPublicList(String corpID, String lastName,
			String aliasName, String countryCode, String country,
			String stateCode, String status, Boolean isAgt,
			Boolean isIgnoreInactive, String fidiNumber, String OMNINumber,
			String AMSANumber, String WERCNumber, String IAMNumber,
			String utsNumber, String eurovanNetwork, String PAIMA, String LACMA,String counter) {		List <Object> partnerList = new ArrayList<Object>();
		List <Object> popupList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(500);
		String tempExtRef = "";
		
		String query1 = "";
		String query="";
		 query = "select distinct p.id, p.lastName,p.aliasName, isAgent, " +
							"if((isAgent=true ),terminalCountryCode,billingCountryCode) as country, " +
							"if((isAgent=true ),terminalCity,billingCity) as city, " +
							"if((isAgent=true ),terminalState,billingState) as state,'', p.corpID,p.agentGroup, p.status,p.createdBy,p.updatedBy,p.createdOn,p.updatedOn,p.partnerId,p.counter " +
							//",ref.description " +
							"FROM agentRequest p  " +
							//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
							//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
							"WHERE  (p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%')  AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
							
						
							"AND p.status like '"+ status +"%' ";
		
		
		String additionalSearch=""; 
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.fidiNumber ='"+fidiNumber+"' or p.fidiNumber is null or p.fidiNumber='')  ";
		}
		if(fidiNumber!=null && (!(fidiNumber.trim().equals(""))) && fidiNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.fidiNumber ='"+fidiNumber+"'   ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.OMNINumber is null or p.OMNINumber='') ";
		}
		if(OMNINumber!=null && (!(OMNINumber.trim().equals(""))) && OMNINumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.OMNINumber !='' and p.OMNINumber is not null  ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.AMSANumber ='"+AMSANumber+"'  or p.AMSANumber is null or p.AMSANumber='') ";
		}
		if(AMSANumber!=null && (!(AMSANumber.trim().equals(""))) && AMSANumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.AMSANumber ='"+AMSANumber+"' ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.WERCNumber ='"+WERCNumber+"'  or p.WERCNumber is null or p.WERCNumber='') ";
		}
		if(WERCNumber!=null && (!(WERCNumber.trim().equals(""))) && WERCNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.WERCNumber ='"+WERCNumber+"' ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.IAMNumber ='"+IAMNumber+"'  or p.IAMNumber is null or p.IAMNumber='') ";
		}
		if(IAMNumber!=null && (!(IAMNumber.trim().equals(""))) && IAMNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.IAMNumber ='"+IAMNumber+"'  ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.utsNumber ='"+utsNumber+"'  or p.utsNumber is null or p.utsNumber='') ";
		}
		if(utsNumber!=null && (!(utsNumber.trim().equals(""))) && utsNumber.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.utsNumber ='"+utsNumber+"' ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.eurovanNetwork ='"+eurovanNetwork+"'  or p.eurovanNetwork is null or p.eurovanNetwork='') ";
		}
		if(eurovanNetwork!=null && (!(eurovanNetwork.trim().equals(""))) && eurovanNetwork.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.eurovanNetwork ='"+eurovanNetwork+"'  ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.PAIMA ='"+PAIMA+"'  or p.PAIMA is null or p.PAIMA='') ";
		}
		if(PAIMA!=null && (!(PAIMA.trim().equals(""))) && PAIMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.PAIMA ='"+PAIMA+"' ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("N")){
			additionalSearch=additionalSearch+" and (p.LACMA ='"+LACMA+"'  or p.LACMA is null or p.LACMA='') ";
		}
		if(LACMA!=null && (!(LACMA.trim().equals(""))) && LACMA.trim().equalsIgnoreCase("Y")){
			additionalSearch=additionalSearch+" and p.LACMA ='"+LACMA+"' ";
		}
		System.out.println("\n\n\n\n additionalSearch "+additionalSearch);
		query=query+additionalSearch;
		if((status!=null)&&(!status.equalsIgnoreCase(""))&&(!status.equalsIgnoreCase("New"))){
			query=query.replaceAll("left outer", "inner");
		}
		if(isIgnoreInactive){
			query += "AND p.status <> 'Inactive' ";
		}
		
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		
		
		
		
		if( isAgt ){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND isAgent = false ";
		}
		query += " limit 500 ";
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list(); 
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			if(isIgnoreInactive)
			{
				
				dto.setId(row[0]);
				
				dto.setLastName(row[1]);
				if(row[2] == null){
					dto.setAliasName("");
				}else{
					dto.setAliasName(row[2]);
				}
				if(row[3]==null){
					dto.setIsAgent("");
				}else{
					dto.setIsAgent(row[3]);
				}
				
				
				
				if(row[4] == null){
					dto.setCountryName("");
				}else {
					dto.setCountryName(row[4]);
				}
				if(row[5] == null){
					dto.setCityName("");
				}else{
					dto.setCityName(row[5]);
				}
				if(row[6] == null){
					dto.setStateName("");
				}else{
					List descName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+row[6]+"' and parameter='STATE' and bucket2='"+row[4]+"' ").addScalar("r.description", Hibernate.STRING).list();
						if(descName!=null && !descName.isEmpty() && descName.get(0)!=null ){
							dto.setStateName(descName.get(0).toString());	
						}else{
							dto.setStateName("");
						}
				} 
				
				
				if(row[8]==null){
					dto.setAgentGroup("");
				}else{
					dto.setAgentGroup(row[8]);
				}
				
				if(row[10]==null){
					dto.setStatus("");
				}else{
					dto.setStatus(row[10]);
				}

				if(row[11]==null){
					dto.setCreatedBy("");
				}else{
					dto.setCreatedBy(row[11]);
				}

				if(row[12]==null){
					dto.setUpdatedBy("");
				}else{
					dto.setUpdatedBy(row[12]);
				}
				if(row[13]==null){
					dto.setCreatedOn("");
				}else{
					dto.setCreatedOn(row[13]);
				}
				if(row[14]==null){
					dto.setUpdatedOn("");
				}else{
					dto.setUpdatedOn(row[14]);
				}
				if(row[15]==null){
					dto.setPartnerId("");
				}else{
					dto.setPartnerId(row[15]);
				}
				if(row[16]==null){
					dto.setCounter("");
				}else{
					dto.setCounter(row[16]);
				}
				partnerList.add(dto);
				
			}
		}
		
		return partnerList;
	}
	public List findAgentFeedbackList(Long id)
	{ 
	    
		return   getHibernateTemplate().find("from AgentRequestReason WHERE  agentRequestId='"+id+"' order by updatedOn asc"); 
	
	}
	public class PartnerDTO {
		private Object id;
		private Object partnerCode;
		private Object lastName;
		private Object firstName;
		private Object aliasName;
		private Object isAgent;
		private Object isAccount;
		private Object isPrivateParty;
		private Object isCarrier;
		private Object isVendor;
		private Object isOwnerOp;
		private Object status;
		private Object countryName;
		private Object stateName;
		private Object cityName;
		private Object stage;
		private Object vanLineCode;
		private Object add1;
		private Object add2;
		private Object zip;
		private Object companyDiv;
		private Object corpID;
		private Object mergedCode;
		private Object doNotMerge;
		private Object extReference;
		private Object publicStatus;
		private Object agentGroup;
		private Object agentClassification;
		private Object email;
		private Object createdBy;
		private Object updatedBy;
		private Object createdOn;
		private Object updatedOn;
		private Object partnerId;
	    private Object counter; 
	    private Object reason;
	    private Object comment;
		
		

		public Object getPartnerId() {
			return partnerId;
		}
		public void setPartnerId(Object partnerId) {
			this.partnerId = partnerId;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getIsAccount() {
			return isAccount;
		}
		public void setIsAccount(Object isAccount) {
			this.isAccount = isAccount;
		}
		public Object getIsAgent() {
			return isAgent;
		}
		public void setIsAgent(Object isAgent) {
			this.isAgent = isAgent;
		}
		public Object getIsCarrier() {
			return isCarrier;
		}
		public void setIsCarrier(Object isCarrier) {
			this.isCarrier = isCarrier;
		}
		public Object getIsOwnerOp() {
			return isOwnerOp;
		}
		public void setIsOwnerOp(Object isOwnerOp) {
			this.isOwnerOp = isOwnerOp;
		}
		public Object getIsPrivateParty() {
			return isPrivateParty;
		}
		public void setIsPrivateParty(Object isPrivateParty) {
			this.isPrivateParty = isPrivateParty;
		}
		public Object getIsVendor() {
			return isVendor;
		}
		public void setIsVendor(Object isVendor) {
			this.isVendor = isVendor;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getCityName() {
			return cityName;
		}
		public void setCityName(Object cityName) {
			this.cityName = cityName;
		}
		public Object getCountryName() {
			return countryName;
		}
		public void setCountryName(Object countryName) {
			this.countryName = countryName;
		}
		public Object getStateName() {
			return stateName;
		}
		public void setStateName(Object stateName) {
			this.stateName = stateName;
		}
		public Object getStage() {
			return stage;
		}
		public void setStage(Object stage) {
			this.stage = stage;
		}
		public Object getVanLineCode() {
			return vanLineCode;
		}
		public void setVanLineCode(Object vanLineCode) {
			this.vanLineCode = vanLineCode;
		}

		public Object getAdd1() {
			return add1;
		}

		public void setAdd1(Object add1) {
			this.add1 = add1;
		}

		public Object getAdd2() {
			return add2;
		}

		public void setAdd2(Object add2) {
			this.add2 = add2;
		}

		public Object getZip() {
			return zip;
		}

		public void setZip(Object zip) {
			this.zip = zip;
		}

		public Object getCompanyDiv() {
			return companyDiv;
		}

		public void setCompanyDiv(Object companyDiv) {
			this.companyDiv = companyDiv;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getMergedCode() {
			return mergedCode;
		}
		public void setMergedCode(Object mergedCode) {
			this.mergedCode = mergedCode;
		}
		public Object getDoNotMerge() {
			return doNotMerge;
		}
		public void setDoNotMerge(Object doNotMerge) {
			this.doNotMerge = doNotMerge;
		}
	
		public Object getExtReference() {
			return extReference;
		}
	
		public void setExtReference(Object extReference) {
			this.extReference = extReference;
		}
		public Object getAliasName() {
			return aliasName;
		}
		public void setAliasName(Object aliasName) {
			this.aliasName = aliasName;
		}
		public Object getPublicStatus() {
			return publicStatus;
		}
		public void setPublicStatus(Object publicStatus) {
			this.publicStatus = publicStatus;
		}
		public Object getAgentGroup() {
			return agentGroup;
		}
		public void setAgentGroup(Object agentGroup) {
			this.agentGroup = agentGroup;
		}
		public Object getAgentClassification() {
			return agentClassification;
		}
		public void setAgentClassification(Object agentClassification) {
			this.agentClassification = agentClassification;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Object createdOn) {
			this.createdOn = createdOn;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Object getCounter() {
			return counter;
		}
		public void setCounter(Object counter) {
			this.counter = counter;
		}
		public Object getReason() {
			return reason;
		}
		public void setReason(Object reason) {
			this.reason = reason;
		}
		public Object getComment() {
			return comment;
		}
		public void setComment(Object comment) {
			this.comment = comment;
		}
	}
	public String updateAgentRecord(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,String updatedBy,Long id,String createdBy)
	{
		 String email="";
		String query="";
		String actualDate="";
	
		try{
			if (status.equals("Approved")) {
	        getSession().createSQLQuery("update agentRequest set status='Approved',updatedOn=now(),updatedby='"+updatedBy+"' where id ='"+id+"' ").executeUpdate();
	    	query="select email from app_user where username='"+createdBy+"' ";
			List createdByList = this.getSession().createSQLQuery(query).list();
				if((createdByList!=null)&&(!createdByList.isEmpty())){
					email=createdByList.get(0).toString();
				}
			}
			else
			{
			getSession().createSQLQuery("update agentRequest set status='Rejected', updatedOn=now(),updatedby='"+updatedBy+"' where id ='"+id+"' ").executeUpdate();
			query="select email from app_user where username='"+createdBy+"' ";
			List createdByList = this.getSession().createSQLQuery(query).list();
				if((createdByList!=null)&&(!createdByList.isEmpty())){
					email=createdByList.get(0).toString();
				}
			
				}
		}catch(Exception e){
			e.printStackTrace();
		}
		return email;
}
	public String  updateAgentRejected(Long id,String updatedBy){
		 String check="Updated";
		 String query="";
		 getSession().createSQLQuery("update agentRequest set status='Rejected', updatedOn=now(),updatedby='"+updatedBy+"' where id ='"+id+"' ").executeUpdate();
       return check;
}
	
	 public List checkById(Long id) {
	    	return getHibernateTemplate().find("from AgentRequest where partnerId=?", id);
	    }
		public List getAgentRequestList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status,Boolean isAgt, Boolean isIgnoreInactive) {
		List <Object> popupList = new ArrayList<Object>();
		getHibernateTemplate().setMaxResults(500);
		String tempExtRef = "";
		List <Object> partnerList = new ArrayList<Object>();
		String query1 = "";
		String query="";
		 query = "select distinct p.id, p.lastName,p.aliasName, isAgent, " +
							"if((isAgent=true ),terminalCountryCode,billingCountryCode) as country, " +
							"if((isAgent=true ),terminalCity,billingCity) as city, " +
							"if((isAgent=true ),terminalState,billingState) as state,'', p.corpID,p.agentGroup, p.status,p.createdBy,p.updatedBy,p.createdOn,p.updatedOn,p.partnerId " +
							//",ref.description " +
							"FROM agentRequest p  " +
							//"left outer join refmaster ref on ((if(isAgent=true or isVendor=true or isCarrier=true, terminalState =ref.code, billingState =ref.code))) and ref.corpid='TSFT' and ref.parameter='STATE' " +
							//"and (if(isAgent=true or isVendor=true or isCarrier=true, terminalCountryCode =ref.bucket2, billingCountryCode =ref.bucket2)) " +
							"WHERE  (p.aliasName like '%" + aliasName.replaceAll("'", "''") +"%' or p.lastName like '%" + aliasName.replaceAll("'", "''") + "%')  AND(p.lastName like '%" + lastName.replaceAll("'", "''") + "%' or p.aliasName like '%" + lastName.replaceAll("'", "''") +"%') " +
							
						
							"AND p.status like '"+ status +"%' and p.corpId='"+corpID+"'";
		
		
				
		query=query;
		if((status!=null)&&(!status.equalsIgnoreCase(""))&&(!status.equalsIgnoreCase("New"))){
			query=query.replaceAll("left outer", "inner");
		}
		if(isIgnoreInactive){
			query += "AND p.status <> 'Inactive' ";
		}
		
		if(isAgt){
			query1 = query1+",isAgent=true";
		}
		
		
		
		
		if( isAgt ){
			query1 = query1.substring(1, query1.length());
			String tempString[] = query1.split(",");
			int totalLength = tempString.length;
			
			if(totalLength == 1){
				query +=  " AND "+query1;
			}
			if(totalLength > 1){
				String tmpStart = " AND (";
	  		   	String tmpEnd = ")";
	  		   	String tempQuery = "";
				for (int i = 0 ; i < totalLength ; i++) {
			     	   
			     	  tempQuery += tempString[i]+" or ";
					}
				tempQuery = tempQuery.substring(0,tempQuery.length()-3);
				query += tmpStart.concat(tempQuery).concat(tmpEnd);
			}	
		}else{
			query += " AND isAgent = false ";
		}
		query += " limit 500 ";
		//System.out.println("----------->"+query);
		popupList = this.getSession().createSQLQuery(query).list(); 
		Iterator it = popupList.iterator();
		PartnerDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PartnerDTO();
			if(isIgnoreInactive)
			{
				
				dto.setId(row[0]);
				
				dto.setLastName(row[1]);
				if(row[2] == null){
					dto.setAliasName("");
				}else{
					dto.setAliasName(row[2]);
				}
				if(row[3]==null){
					dto.setIsAgent("");
				}else{
					dto.setIsAgent(row[3]);
				}
				
				
				
				if(row[4] == null){
					dto.setCountryName("");
				}else {
					dto.setCountryName(row[4]);
				}
				if(row[5] == null){
					dto.setCityName("");
				}else{
					dto.setCityName(row[5]);
				}
				if(row[6] == null){
					dto.setStateName("");
				}else{
					List descName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+row[6]+"' and parameter='STATE' and bucket2='"+row[4]+"' ").addScalar("r.description", Hibernate.STRING).list();
						if(descName!=null && !descName.isEmpty() && descName.get(0)!=null ){
							dto.setStateName(descName.get(0).toString());	
						}else{
							dto.setStateName("");
						}
				} 
				
				
				if(row[8]==null){
					dto.setAgentGroup("");
				}else{
					dto.setAgentGroup(row[8]);
				}
				
				if(row[10]==null){
					dto.setStatus("");
				}else{
					dto.setStatus(row[10]);
				}

				if(row[11]==null){
					dto.setCreatedBy("");
				}else{
					dto.setCreatedBy(row[11]);
				}

				if(row[12]==null){
					dto.setUpdatedBy("");
				}else{
					dto.setUpdatedBy(row[12]);
				}
				if(row[13]==null){
					dto.setCreatedOn("");
				}else{
					dto.setCreatedOn(row[13]);
				}
				if(row[14]==null){
					dto.setUpdatedOn("");
				}else{
					dto.setUpdatedOn(row[14]);
				}
				if(row[15]==null){
					dto.setPartnerId("");
				}else{
					dto.setPartnerId(row[15]);
				}
				partnerList.add(dto);
				
			}
		}
		
		return partnerList;	
		}
		
		public String getUser(String createdBy)
		{String query="";
		String name="";
		
		query="select concat(first_name,'',last_name) from app_user where username='"+createdBy+"' ";
		List createdByList = this.getSession().createSQLQuery(query).list();
			if((createdByList!=null)&&(!createdByList.isEmpty())){
				name=createdByList.get(0).toString();
			}
			return name;
			}
		
		
		public AgentRequestReason getByID(Long id) {
			List agentRequestReasonList=new ArrayList();
			AgentRequestReason agentR = new AgentRequestReason();
			try {
				String query ="from AgentRequestReason where agentRequestId='"+id+"'";
				agentRequestReasonList = getHibernateTemplate().find(query);
				 if(agentRequestReasonList!=null && (!agentRequestReasonList.isEmpty())){
					 agentR = (AgentRequestReason)agentRequestReasonList.get(0);
				}
				 return agentR;
	        } catch (Exception e) {
	        	 e.printStackTrace();
		    	
	        }
			return null;	
		}
		public List getAgentRequestDetailList(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1)
		{	
			List <Object> partnerList = new ArrayList<Object>();
			List <Object> popupList = new ArrayList<Object>();
			getHibernateTemplate().setMaxResults(500);
			String tempExtRef = "";
			
			String query1 = "";
			String query="";
		    query = "select distinct id, lastName,aliasName, isAgent,billingCountry, billingCity,billingState, corpID,agentGroup,status,billingEmail,billingZip,billingAddress1,partnerCode  from partnerpublic WHERE  aliasName='"+aliasName+"' and lastName='"+lastName+"'  AND  billingCountry='"+billingCountry+"' AND billingCity='"+billingCity+"'  AND billingAddress1='"+billingAddress1+"' ";	;	
		
			System.out.println("----------->"+query);
			popupList = this.getSession().createSQLQuery(query).list(); 
			Iterator it = popupList.iterator();
			PartnerDTO dto = null;
			while(it.hasNext()){
				Object [] row = (Object[])it.next();
				dto = new PartnerDTO();
				 dto.setId(row[0]);
				 dto.setLastName(row[1]);
					if(row[2] == null){
						dto.setAliasName("");
					}else{
						dto.setAliasName(row[2]);
					}
					if(row[3]==null){
						dto.setIsAgent("");
					}else{
						dto.setIsAgent(row[3]);
					} 
					if(row[4] == null){
						dto.setCountryName("");
					}else {
						dto.setCountryName(row[4]);
					}
					if(row[5] == null){
						dto.setCityName("");
					}else{
						dto.setCityName(row[5]);
					}
					if(row[6] == null){
						dto.setStateName("");
					}else{
						List descName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+row[6]+"' and parameter='STATE' and bucket2='"+row[4]+"' ").addScalar("r.description", Hibernate.STRING).list();
							if(descName!=null && !descName.isEmpty() && descName.get(0)!=null ){
								dto.setStateName(descName.get(0).toString());	
							}else{
								dto.setStateName("");
							}
					}  
					if(row[8]==null){
						dto.setAgentGroup("");
					}else{
						dto.setAgentGroup(row[8]);
					}
					
					if(row[9]==null){
						dto.setStatus("");
					}else{
						dto.setStatus(row[9]);
					} 
					if(row[10]==null){
						dto.setEmail("");
					}else{
						dto.setEmail(row[10]);
					} 
					if(row[11]==null){
						dto.setZip("");
					}else{
						dto.setZip(row[11]);
					} 
					if(row[12]==null){
						dto.setAdd1("");
					}else{
						dto.setAdd1(row[12]);
					} 
					if(row[13]==null){
						dto.setPartnerCode("");
					}else{
						dto.setPartnerCode(row[13]);
					} 
					
					partnerList.add(dto);
					
				
			}
			
			return partnerList;
		
		}
}
	
	


