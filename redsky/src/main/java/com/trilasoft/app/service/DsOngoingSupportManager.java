package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsOngoingSupport;


public interface DsOngoingSupportManager extends GenericManager<DsOngoingSupport, Long>{
	List getListById(Long id);
}

