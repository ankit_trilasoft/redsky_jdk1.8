<%@ include file="/common/taglibs.jsp"%> 


<head> 

<title><fmt:message key="updateRuleList.title"/></title> 

<meta name="heading" content="<fmt:message key='updateRuleList.heading'/>"/> 

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=2;i++)
			{
					document.forms['updateRuleList'].elements[i].value = "";
			}
			//document.forms['toDoList'].elements['isAgentTdr'].checked = false;
}

function showOrHide(value) {
    if (value==0) {
        if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'\_'&'\-']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head> 
<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="editUpdateRule.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set> 

<s:form id="updateRuleList" name="updateRuleList" action="searchUpdateRule" method="post" validate="true">   
<div id="Layer1" style="width:100%">
<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" defaultsort="1" export="true" pagesize="10" style="margin-top:1px;">
<thead>
<tr>
<!-- <th>ID</th>
<th>Field  update</th> -->
<th><fmt:message key="updateRuleList.fieldToUpdate"/></th>
<th><fmt:message key="updateRuleList.validationContion"/></th>
<th><fmt:message key="updateRuleList.ruleStatus"/></th>
<th></th>
</tr></thead> 
<tbody>
  <tr>
    <td>
       <s:textfield name="updateRule.fieldToUpdate" required="true" cssClass="input-text" size="10"/>
   </td>
   <td>
       <s:textfield name="updateRule.validationContion" required="true" cssClass="input-text" size="50"/>
   </td>
   
    <td>
    	<s:select cssClass="list-menu" name="updateRule.ruleStatus" list="{'Tested','Error'}" headerKey="All" headerValue="All" cssStyle="width:70px" />
       
   </td>
   <td style="border-left: hidden;">
       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" method="" key="button.search"/>  
       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
       
   </td>
   
  </tr>
  </tbody>
 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" /> 
<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>UpdateRules</span></a></li>
		    
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<s:set name="updateRulesList" value="updateRulesList" scope="request"/>
<c:if test="${updateRulesList!='[]'}">
<display:table name="updateRulesList" class="table" requestURI="" id="updateRulesList"  defaultsort="1" export="true" pagesize="10" style="margin-top:1px;"> 
<display:column property="ruleNumber" sortable="true" paramId="ruleNumber" paramProperty="ruleNumber" title="Rule ID"/>
<display:column property="fieldToUpdate" href="editUpdateRule.html" sortable="true"  paramId="id" paramProperty="id" titleKey="updateRuleList.fieldToUpdate"/>
<display:column property="ruleName" sortable="true" paramId="ruleName" paramProperty="ruleName" titleKey="updateRuleList.validationContion"/>
<c:if test="${updateRulesList.ruleStatus=='Tested' }">
<display:column titleKey="updateRuleList.ruleStatus" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
</c:if>
<c:if test="${updateRulesList.ruleStatus!='Tested' }">
<display:column titleKey="updateRuleList.ruleStatus" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
</c:if>
<c:if test="${updateRulesList.checkEnable=='true' }">
<display:column  titleKey="updateRuleList.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
</c:if>
<c:if test="${updateRulesList.checkEnable!='true' }">
<display:column  titleKey="updateRuleList.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
</c:if>
</display:table> 
</c:if>
<c:if test="${updateRulesList=='[]'}">
<display:table name="updateRulesList" class="table" requestURI="" id="updateRulesList"  defaultsort="1" export="true" pagesize="10" style="margin-top:1px;"> 
<display:column property="fieldToUpdate" href="editUpdateRule.html" sortable="true"  paramId="id" paramProperty="id" titleKey="updateRuleList.fieldToUpdate"/>
<display:column property="ruleName" sortable="true" paramId="ruleName" paramProperty="ruleName" titleKey="updateRuleList.validationContion"/>
<display:column property="ruleStatus" sortable="true"  paramId="ruleStatus" paramProperty="ruleStatus" titleKey="updateRuleList.ruleStatus"/>
</display:table> 
</c:if>


<c:out value="${buttons}" escapeXml="false" /> 
<td align="right">
</td>
</div>

</s:form>

<script type="text/javascript"> 
try{
showOrHide(0);
}
catch(e){}
highlightTableRows("updateRuleList"); 
</script> 
