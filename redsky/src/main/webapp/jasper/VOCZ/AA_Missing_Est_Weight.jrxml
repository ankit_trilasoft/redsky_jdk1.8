<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="AA_Missing_Est_Weight" pageWidth="792" pageHeight="612" orientation="Landscape" columnWidth="756" leftMargin="18" rightMargin="18" topMargin="18" bottomMargin="18" uuid="ee7f8412-a520-47ad-8c4c-362b5ed946b4">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="begindate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Corporate ID" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT
     'AA_Missing_Est_Weight',
     serviceorder.`corpID` AS serviceorder_corpID,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`status` AS serviceorder_status,
     serviceorder.`routing` AS serviceorder_routing,
     serviceorder.`packingMode` AS serviceorder_packingMode,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`job` AS serviceorder_job,
     serviceorder.`firstName` AS serviceorder_firstName,
     CONCAT(serviceorder.`lastName`,", ",serviceorder.`firstName`)AS Shipper,
     serviceorder.`email2` AS serviceorder_email2,
     serviceorder.`email` AS serviceorder_email,
     serviceorder.`registrationNumber` AS serviceorder_registrationNumber,
     workticket.`city` AS workticket_city,
     workticket.`confirm` AS workticket_confirm,
     workticket.`confirmDate` AS workticket_confirmDate,
     workticket.`corpID` AS workticket_corpID,
     workticket.`crews` AS workticket_crews,
     workticket.`date1` AS workticket_date1,
     workticket.`date2` AS workticket_date2,
     workticket.`estimatedCartoons` AS workticket_estimatedCartoons,
     workticket.`estimatedCubicFeet` AS workticket_estimatedCubicFeet,
     workticket.`estimatedWeight` AS workticket_estimatedWeight,
     workticket.`homePhone` AS workticket_homePhone,
     workticket.`instructions` AS workticket_instructions,
     workticket.`instructionCode` AS workticket_instructionCode,
     workticket.`leaveWareHouse` AS workticket_leaveWareHouse,
     workticket.`originCountry` AS workticket_originCountry,
     workticket.`originMobile` AS workticket_originMobile,
     workticket.`originFax` AS workticket_originFax,
     workticket.`phone` AS workticket_phone,
     workticket.`scheduled1` AS workticket_scheduled1,
     workticket.`scheduled2` AS workticket_scheduled2,
     workticket.`scanned` AS workticket_scanned,
     workticket.`shipNumber` AS workticket_shipNumber,
     workticket.`service` AS workticket_service,
     workticket.`ticket` AS workticket_ticket,
     workticket.`targetActual` AS workticket_targetActual,
     workticket.`warehouse` AS workticket_warehouse,
     workticket.`zip` AS workticket_zip,
     workticket.`coordinator` AS workticket_coordinator,
     serviceorder.`coordinator` AS serviceorder_coordinator,
     workticket.`serviceOrderId` AS workticket_serviceOrderId,
     serviceorder.`id` AS serviceorder_id,
     customerfile.`id` AS customerfile_id,
     customerfile.`survey` AS customerfile_survey,
     serviceorder.`commodity` AS serviceorder_commodity,
     customerfile.`salesMan` AS customerfile_salesMan,
     customerfile.`estimator` AS customerfile_estimator,
     customerfile.`estimatorName` AS customerfile_estimatorName,
     serviceorder.`customerFileId` AS serviceorder_customerFileId
FROM
     `workticket` workticket LEFT OUTER JOIN `serviceorder` serviceorder ON workticket.`serviceOrderId` = serviceorder.`id`
     LEFT OUTER JOIN `customerfile` customerfile ON serviceorder.`customerFileId` = customerfile.`id`
WHERE
     workticket.`corpID` = $P{Corporate ID}
 and serviceorder.`job` in ("UVL","MVL","STO","INT","LOC","DOM","GST","VAI")
 AND workticket.`targetActual` <> 'C'
 AND workticket.`service` IN ("PK","LD","DL","DU","UP")
 AND (workticket.`estimatedWeight` = 0
     or workticket.`estimatedWeight` IS null)
AND (
       (workticket.`date1` between $P{begindate}  AND  $P{enddate})
    OR (workticket.`date2` between $P{begindate}  AND $P{enddate})
    OR (workticket.`date1` < $P{begindate} AND workticket.`date2` > $P{enddate})
)]]>
	</queryString>
	<field name="AA_Missing_Est_Weight" class="java.lang.String"/>
	<field name="serviceorder_corpID" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="serviceorder_packingMode" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="serviceorder_email2" class="java.lang.String"/>
	<field name="serviceorder_email" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="workticket_city" class="java.lang.String"/>
	<field name="workticket_confirm" class="java.lang.String"/>
	<field name="workticket_confirmDate" class="java.sql.Timestamp"/>
	<field name="workticket_corpID" class="java.lang.String"/>
	<field name="workticket_crews" class="java.lang.Integer"/>
	<field name="workticket_date1" class="java.sql.Timestamp"/>
	<field name="workticket_date2" class="java.sql.Timestamp"/>
	<field name="workticket_estimatedCartoons" class="java.lang.Integer"/>
	<field name="workticket_estimatedCubicFeet" class="java.lang.Double"/>
	<field name="workticket_estimatedWeight" class="java.lang.Double"/>
	<field name="workticket_homePhone" class="java.lang.String"/>
	<field name="workticket_instructions" class="java.lang.String"/>
	<field name="workticket_instructionCode" class="java.lang.String"/>
	<field name="workticket_leaveWareHouse" class="java.lang.String"/>
	<field name="workticket_originCountry" class="java.lang.String"/>
	<field name="workticket_originMobile" class="java.lang.String"/>
	<field name="workticket_originFax" class="java.lang.String"/>
	<field name="workticket_phone" class="java.lang.String"/>
	<field name="workticket_scheduled1" class="java.sql.Timestamp"/>
	<field name="workticket_scheduled2" class="java.sql.Timestamp"/>
	<field name="workticket_scanned" class="java.sql.Timestamp"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_targetActual" class="java.lang.String"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="workticket_zip" class="java.lang.String"/>
	<field name="workticket_coordinator" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="workticket_serviceOrderId" class="java.lang.Long"/>
	<field name="serviceorder_id" class="java.lang.Long"/>
	<field name="customerfile_id" class="java.lang.Long"/>
	<field name="customerfile_survey" class="java.sql.Timestamp"/>
	<field name="serviceorder_commodity" class="java.lang.String"/>
	<field name="customerfile_salesMan" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<field name="customerfile_estimatorName" class="java.lang.String"/>
	<field name="serviceorder_customerFileId" class="java.math.BigInteger"/>
	<sortField name="workticket_date1"/>
	<sortField name="Shipper"/>
	<sortField name="serviceorder_shipNumber"/>
	<variable name="tktcount" class="java.lang.Integer" resetType="Group" resetGroup="date1" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{workticket_ticket}]]></variableExpression>
	</variable>
	<group name="date1">
		<groupExpression><![CDATA[$F{workticket_date1}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="35" splitType="Stretch">
				<textField pattern="#,##0" isBlankWhenNull="true">
					<reportElement key="textField" x="257" y="10" width="64" height="18" forecolor="#0000CC" uuid="71ebde24-cc3b-48ff-87c7-435e6a08553b"/>
					<textElement>
						<font isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{tktcount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-22" x="33" y="10" width="148" height="18" forecolor="#0000CC" uuid="eaf30401-55db-4f1e-add2-12d5048c6af5"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<text><![CDATA[Number of tickets for ]]></text>
				</staticText>
				<textField pattern="MM/dd/yy" isBlankWhenNull="true">
					<reportElement key="textField-4" x="184" y="10" width="56" height="18" forecolor="#0000CC" uuid="963f6828-cde0-44cb-adf8-c5ae354ba029"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{workticket_date1}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement key="rectangle-1" x="0" y="7" width="756" height="0" uuid="d0a530fc-24f0-489d-83bd-5ac3ac703c0f"/>
				</rectangle>
				<line>
					<reportElement key="line-2" x="0" y="4" width="756" height="1" uuid="0fc3e536-e8e5-49ff-b44c-ca033b0aa143"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="48" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="185" y="0" width="412" height="23" forecolor="#663300" uuid="08d1a59d-c689-447c-b23b-563c4b2c83c5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Workticket Missing Estimated Weight]]></text>
			</staticText>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement key="textField-2" x="483" y="23" width="107" height="20" uuid="80d38a26-8480-41e8-aef8-327ad78df682"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{enddate}]]></textFieldExpression>
			</textField>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement key="textField-3" x="340" y="25" width="98" height="18" uuid="7f409969-1e89-4a0c-8267-2c272ac42880"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{begindate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" x="437" y="23" width="36" height="21" uuid="0949d04f-4388-4202-a791-e4b1eb8200da"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[  to]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="201" y="23" width="136" height="21" uuid="554c6e93-f742-4eaa-9175-20ba4c86b4e1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[Workticket Date  ]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="34" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-4" x="1" y="16" width="92" height="16" uuid="f5b256e6-d35b-4ec0-9cde-2b5f353bb42e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="539" y="16" width="31" height="16" uuid="77d6002d-4ce5-4a5d-aba7-0998320b8f2c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Svc]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="268" y="16" width="52" height="14" uuid="f93fd952-6ddb-48b2-ae0d-ddb969ccb639"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[S/O #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="387" y="16" width="61" height="16" uuid="1d1eefac-96ad-40b5-8a81-72b860ce0737"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Tcket #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="451" y="16" width="43" height="16" uuid="d7dbf957-f7a7-4f95-b373-3ebda67781ea"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[WH #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="500" y="16" width="34" height="16" uuid="f57f6389-79f3-4151-930f-d52ac9b81cc2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="600" y="16" width="64" height="16" uuid="5dfcff3e-d5e2-49ae-ae2c-1fc08ee98745"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Coordinator]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" x="681" y="16" width="73" height="16" uuid="75e32e34-346f-4809-be02-c971306e8689"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Sales Rep]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" x="188" y="16" width="69" height="15" uuid="3e77ef81-e6ab-4e80-8e42-d5dc12329ac0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Survey Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="19" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="539" y="0" width="31" height="18" uuid="e9418137-0a78-4cb3-9600-f3d912e631df"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_service}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="267" y="0" width="102" height="18" uuid="7220e1e4-ac3b-48d9-bcd0-30fddba55204"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="387" y="0" width="61" height="18" uuid="31b6114b-ab63-4f34-a421-6447076164aa"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_ticket}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="466" y="0" width="27" height="18" uuid="32c8db8a-8181-4a78-b8d8-1eb0e14ac639"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="0" width="171" height="18" uuid="6bf0270d-adb3-4bb8-8729-86097fa45365"/>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="500" y="0" width="33" height="18" uuid="2aa9ad7a-af44-439d-b2b4-947b2a0a7be4"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yy" isBlankWhenNull="true">
				<reportElement key="textField" x="193" y="0" width="66" height="18" uuid="7b3c5daf-431e-46d7-bc82-5a2a52348d0a"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="674" y="0" width="78" height="18" uuid="f48b41c9-031e-463e-9fc8-9c6226c16681"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_estimator}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="585" y="0" width="78" height="18" uuid="ff6a6483-5a27-4b8c-be00-97b63fefe71f"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="27" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="574" y="8" width="134" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="08c9e9a0-9b97-4d09-b66c-5665deedd6c7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Helvetica" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="715" y="8" width="36" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="55bb26e4-c7e8-4d65-8d84-cbb2955395fd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Helvetica" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="1" y="8" width="209" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="1324609d-d6c0-42fa-9d14-ac6b62d73242"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Helvetica" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="6" width="756" height="1" forecolor="#00FFCC" uuid="de5ce538-5818-4e86-8567-55a24e258a4b"/>
				<graphicElement>
					<pen lineWidth="2.0" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-20" x="302" y="8" width="224" height="13" uuid="85505f41-33d1-4cd6-ab1e-201c463c24fa"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[AA_Missing_Est_Weight.jrxml]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
