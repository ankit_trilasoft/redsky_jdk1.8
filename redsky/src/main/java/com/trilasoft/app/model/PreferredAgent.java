package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="preferredagentforaccount")
public class PreferredAgent extends BaseObject {

	private Long id;

	private String corpID;
	
	private String preferredAgentCode;

	private String preferredAgentName;

	private String partnerCode;

	private Boolean status;

	private String updatedBy;
	
	private String createdBy;
	
	private Date createdOn;

	private Date updatedOn;
	
	private String agentGroup;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID", corpID)
				.append("preferredAgentCode", preferredAgentCode)
				.append("preferredAgentName", preferredAgentName)
				.append("partnerCode", partnerCode)
				.append("status", status)
				.append("updatedBy", updatedBy)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("agentGroup", agentGroup).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PreferredAgent))
			return false;
		PreferredAgent castOther = (PreferredAgent) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID, castOther.corpID)
				.append(preferredAgentCode, castOther.preferredAgentCode)
				.append(preferredAgentName, castOther.preferredAgentName)
				.append(partnerCode, castOther.partnerCode)
				.append(status, castOther.status)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(agentGroup, castOther.agentGroup).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(preferredAgentCode)
				.append(preferredAgentName)
				.append(partnerCode)
				.append(status)
				.append(updatedBy)
				.append(createdBy)
				.append(createdOn)
				.append(updatedOn)
				.append(agentGroup).toHashCode();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public String getPreferredAgentCode() {
		return preferredAgentCode;
	}

	public void setPreferredAgentCode(String preferredAgentCode) {
		this.preferredAgentCode = preferredAgentCode;
	}

	@Column
	public String getPreferredAgentName() {
		return preferredAgentName;
	}

	public void setPreferredAgentName(String preferredAgentName) {
		this.preferredAgentName = preferredAgentName;
	}
	@Column
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getAgentGroup() {
		return agentGroup;
	}
	public void setAgentGroup(String agentGroup) {
		this.agentGroup = agentGroup;
	}

}
