package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.MyMessage;

public interface MessageManager extends GenericManager<MyMessage, Long> {

	public List<MyMessage> findByToUser(String toUser);
	public List<MyMessage> findByFromUser(String fromUser);
	public List<MyMessage> findByUnreadMessages(String fromUser);
	public List<MyMessage> findByOutbox(String status, String fromUser);
	public int updateFwdStatus(Long noteId);
}
