<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataSecuritySetList.title"/></title> 
<meta name="heading" content="<fmt:message key='dataSecuritySetList.heading'/>"/>
<style>
span.pagelinks {
display:block;font-size:0.95em;margin-bottom:3px;margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;
}
form {
margin-top:-30px;!margin-top:-5px;
}
</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['dataSecuritySetList'].elements[i].value = "";
			}
}

function findUserPermission(setName,position) { 
  var url="findUserSecuritySet.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&name=" + encodeURI(setName);
  ajax_showTooltip(url,position);	
  }
</script>
<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>
</head>
<c:set var="buttons">
     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/dataSecuritySetForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>  	
</c:set>

	<s:form cssClass="form_magn" id="dataSecuritySetList" name="dataSecuritySetList" action="dataSecuritySetSearch" method="post" validate="true">   
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
	
	<table class="table">
	<thead>
	<tr>
	<th><fmt:message key="dataSecuritySetList.name"/></th>
	<th><fmt:message key="dataSecuritySetList.description"/></th>
	<th>&nbsp;</th>	
	</tr>
	</thead>	
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="dataSecuritySet.name" required="true" cssClass="input-text" size="50"/></td>
			<td width="20" align="left"><s:textfield name="dataSecuritySet.description" required="true" cssClass="input-text" size="50"/></td>
			 <td width="200">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
   			</td>
			 </tr>		
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Data Security List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="dataSecuritySetList" class="table" requestURI="" id="dataSecuritySetListId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="id"  href="dataSecuritySetForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="dataSecuritySetList.Id"/>
   	<display:column property="name"  sortable="true" titleKey="dataSecuritySetList.name"/>
   	<display:column property="description"  sortable="true" titleKey="dataSecuritySetList.description"/>
   	<display:column property="corpID"  sortable="true" titleKey="dataSecuritySetList.corpID"/>
   	<display:column title="UserList" style="width: 15px;">
	<a><img align="middle" title="User List" onclick="findUserPermission('${dataSecuritySetListId.name}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
   	</display:table>
	
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 

<script type="text/javascript"> 

//highlightTableRows("dataSecuritySetListId"); 

</script> 
		
		