package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccountContactDao;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.service.AccountContactManager;

public class AccountContactManagerImpl extends GenericManagerImpl<AccountContact, Long> implements AccountContactManager {
	AccountContactDao accountContactDao;   
	public AccountContactManagerImpl(AccountContactDao accountContactDao) {   
        super(accountContactDao);   
        this.accountContactDao = accountContactDao;   
    } 
    
	public List getAccountContacts(String partnerCode,String corpID){
		return accountContactDao.getAccountContacts(partnerCode, corpID);
	}

	public List getNotesForContract(String partnerCode, String corpID) {
		return accountContactDao.getNotesForContract(partnerCode, corpID);
	}

	public List getRelatedNotesForContract(String corpID,String partnerCode) {
		return accountContactDao.getRelatedNotesForContract(corpID,partnerCode);
	}

	public List getRelatedNotesForProfile(String corpID, String partnerCode) {
		return accountContactDao.getRelatedNotesForProfile(corpID, partnerCode);
	}

}
