<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<script type="text/javascript">
<!--

//-->
</script>

<script>
function clear_fields(){
  
			    document.forms['screenConfigurationList'].elements['screenConfiguration.corpID'].value = "";
			    document.forms['screenConfigurationList'].elements['systemConfiguration.tableName'].value = "";
			    document.forms['screenConfigurationList'].elements['systemConfiguration.fieldName'].value = "";
		        
}
</script>
<head>   
    <title><fmt:message key="screenConfigurationList.title"/></title>   
    <meta name="heading" content="<fmt:message key='screenConfigurationList.heading'/>"/>   
</head>
<s:form id="screenConfigurationList" method="post" validate="true">
<s:hidden name="screenConfiguration.id" value="%{screenConfiguration.id}"/>
 
 
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>
<table class="table" style="width:950px"  >
<thead>
<tr>
<th><fmt:message key="screenConfiguration.corpID"/></th>
<th><fmt:message key="systemConfiguration.tableName"/></th>
<th><fmt:message key="systemConfiguration.fieldName"/></th>

</tr></thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
			    <s:textfield name="screenConfiguration.corpID" required="true" cssClass="input-text" size="12"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="screenConfiguration.tableName" required="true" cssClass="input-text" size="12"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="screenConfiguration.fieldName" required="true" cssClass="input-text" size="12"/>
			</td>
			
			
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
 
  <div id="newmnav">
  <ul>
 
</ul>
</div><div class="spn">&nbsp;</div><br>
		
		
   
  <c:set var="buttons">  
      <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editscreen.html"/>'"  
        value="<fmt:message key="button.add"/>"/>
   </c:set>

<table cellspacing="0" cellpadding="0" border="0" width="700px">
	<tbody>
	<tr><td>
      <display:table name="screenConfigurations" class="table" requestURI="" id="screenConfigurationsList" defaultsort="1" export="true" pagesize="10">   
	  	<display:column property="corpID" sortable="true" titleKey="screenConfiguration.corpID"  style="width:75px" href="/editscreen.html" paramId="id" paramProperty="id"/>
	  	<display:column property="tableName" sortable="true" titleKey="systemConfiguration.tableName"  style="width:75px"/>
    	<display:column property="fieldName" sortable="true" titleKey="systemConfiguration.fieldName" style="width:60px"/>
		<display:column property="displayStatus" sortable="true" titleKey="screenConfiguration.displayStatus"  style="width:75px"/>
    <display:setProperty name="export.excel.filename" value="ScreenConfiguration List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ScreenConfiguration List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ScreenConfiguration List.pdf"/>   
</display:table>   
  </td></tr></tbody>
  </table>
  </div>
  
 
<c:out value="${buttons}" escapeXml="false" />  
</s:form>  

<script type="text/javascript">   
    highlightTableRows("screenConfigurationList");   
</script>  