<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="InvoiceRLOMnVf"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Invoice Number" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT 	'InvoiceRLOMnVf-F-PDF-DOCX-RSKY.jrxml',
	serviceorder.`shipNumber` AS serviceorder_shipNumber,
	DATE_FORMAT(accountline.`receivedInvoiceDate`,'%d-%b-%y') AS accountline_receivedInvoiceDate,
	accountline.`recInvoiceNumber` AS accountline_recInvoiceNumber,
	if((trim(serviceorder.prefix)="" || serviceorder.prefix is null),if((trim(serviceorder.firstName)="" || serviceorder.firstName is null),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))),concat(trim(serviceorder.prefix)," ", if((trim(serviceorder.firstName)="" || serviceorder.firstName is null),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))))) AS ShipperName,
	billing.`billToReference` AS billing_billToReference,
	Concat(if(serviceorder.originCity is null || trim(serviceorder.originCity)='',"",trim(serviceorder.originCity)),"",if(serviceorder.originState is null || trim(serviceorder.originState)='',"",if(serviceorder.originCity is null || trim(serviceorder.originCity)='',trim(serviceorder.originState),concat(", ",trim(serviceorder.originState)))),"",if(serviceorder.originCountry is null || trim(serviceorder.originCountry)='',"",if((serviceorder.originCity is null || trim(serviceorder.originCity)='') AND (serviceorder.originState is null || trim(serviceorder.originState)=''),trim(serviceorder.originCountry),concat(", ",trim(serviceorder.originCountry))))) AS serviceorder_origin_CityStateCountry,
	Concat(if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='',"",trim(serviceorder.destinationCity)),"",if(serviceorder.destinationState is null || trim(serviceorder.destinationState)='',"",if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='',trim(serviceorder.destinationState),concat(", ",trim(serviceorder.destinationState)))),"",if(serviceorder.destinationCountry is null || trim(serviceorder.destinationCountry)='',"",if((serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='') AND (serviceorder.destinationState is null || trim(serviceorder.destinationState)=''),trim(serviceorder.destinationCountry),concat(", ",trim(serviceorder.destinationCountry))))) AS serviceorder_destination_CityStateCountry,
	if((accountline.`billToCode`!='' || accountline.`billToCode` is not null),CONCAT(accountline.companydivision,accountline.`recInvoiceNumber`,"/ ",accountline.`billToCode`),CONCAT(accountline.companydivision,accountline.`recInvoiceNumber`)) AS Account_Cross_Reference,
	if(substring(trim(refmaster.`description`),-4,4)='days',concat(refmaster.`description`," ","after invoice date"), refmaster.`description`) AS refmaster_description,
	if(partner.isPrivateParty is true,if(trim(partner.partnerPrefix)='' || partner.partnerPrefix is null,IF(partner.firstName is null || trim(partner.firstName)='',trim(partner.lastName),CONCAT(trim(partner.firstName)," ",trim(partner.lastName))),concat(trim(partner.partnerPrefix)," ",IF(partner.firstName is null || trim(partner.firstName)='',trim(partner.lastName),CONCAT(trim(partner.firstName)," ",trim(partner.lastName))))),IF(partner.firstName is null || trim(partner.firstName)='',trim(partner.lastName),CONCAT(trim(partner.firstName)," ",trim(partner.lastName))))AS PartnerName,
   	if(billing.attnBilling is null || trim(billing.attnBilling)="",'',trim(billing.attnBilling)) AS billing_attnBilling,
	if((partner.`billingAddress1` is null || trim(partner.`billingAddress1`)=''),'',trim(partner.`billingAddress1`)) AS partner_billingAddress1,
	IF((partner.`billingAddress2` is null || trim(partner.`billingAddress2`)=""),'',trim(partner.`billingAddress2`))AS partner_billingAddress2,
	IF((partner.`billingAddress3` is null || trim(partner.`billingAddress3`)=""),'',trim(partner.`billingAddress3`))AS partner_billingAddress3,
	IF((partner.`billingAddress4` is null || trim(partner.`billingAddress4`)="" ),'',trim(partner.`billingAddress4`))AS partner_billingAddress4,
	Concat(if(partner.billingCity is null || trim(partner.billingCity)='',"",trim(partner.billingCity)),"",if(partner.billingState is null || partner.billingState='',"",if(partner.billingCity is null || trim(partner.billingCity)='',partner.billingState,concat(", ",partner.billingState)))) AS partner_billing_CityState,
	concat(if(partner.billingZip is null || trim(partner.billingZip)='',"",trim(partner.billingZip)),"",if(partner.billingCountry is null || partner.billingCountry='','',if(partner.billingZip is null || trim(partner.billingZip)='',partner.billingCountry,concat(", ",partner.billingCountry)))) AS partner_billing_ZipCountry,
	trim(accountline.`description`) AS accountline_description,
	round(accountline.actualRevenue,2) AS accountline_amount,
	billing.specialInstruction as specialInstruction,
	if(systemdefault.company is null || trim(systemdefault.company)='','',trim(systemdefault.company)) as systemdefault_company,
	if(systemdefault.address1 is null || trim(systemdefault.address1)='','',trim(systemdefault.address1)) AS systemdefault_Address1,
	if(systemdefault.address2 is null || trim(systemdefault.address2)='','',trim(systemdefault.address2)) AS systemdefault_Address2, 
	if(systemdefault.address3 is null || trim(systemdefault.address3)='','',trim(systemdefault.address3)) AS systemdefault_Address3,
	Concat(if(systemdefault.city is null || trim(systemdefault.city)='','',trim(systemdefault.city)),"",if(systemdefault.state is null || systemdefault.state='',"",if(systemdefault.city is null || trim(systemdefault.city)='',systemdefault.state,concat(", ",systemdefault.state)))) AS systemdefault_CityState,
	concat(if(systemdefault.zip is null || trim(systemdefault.zip)='','',trim(systemdefault.zip)),"",if(systemdefault.country is null || systemdefault.country='','',if(systemdefault.zip is null || trim(systemdefault.zip)='',if(refmaster_con.description is null || refmaster_con.description='','',refmaster_con.description),concat(", ",if(refmaster_con.description is null || refmaster_con.description='','',refmaster_con.description))))) AS systemdefault_ZipCountry,
	concat("Wire your payments to: SWIFT = ",if(companydivision.swiftcode is null || trim(companydivision.swiftcode)='','',trim(companydivision.swiftcode))) as wording1,
	concat(if(companydivision.description is null || trim(companydivision.description)='','',trim(companydivision.description)),", Account# ",if(companydivision.bankAccountNumber is null || trim(companydivision.bankAccountNumber)='','',trim(companydivision.bankAccountNumber))," at ",if(companydivision.bankName is null || trim(companydivision.bankName)='','',trim(companydivision.bankName)),", ABA# ",if(companydivision.wireTransferAccountNumber is null || trim(companydivision.wireTransferAccountNumber)='','',trim(companydivision.wireTransferAccountNumber))) as wording3,
	Concat("Payment is required in US Dollars. If not paid in US Dollars, we will charge back any loss in exchange or bank fees.")AS wording4
FROM `serviceorder` serviceorder 
	INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID` = $P{Corporate ID}
	INNER JOIN  `accountline` accountline ON serviceorder.`id` = accountline.`serviceOrderId` AND accountline.`corpID` = $P{Corporate ID}
	INNER JOIN systemdefault systemdefault ON systemdefault.corpid=serviceorder.corpid AND systemdefault.`corpID`=$P{Corporate ID}
	LEFT OUTER JOIN `companydivision` companydivision ON serviceorder.`companyDivision` = companydivision.`companyCode` AND companydivision.corpid = $P{Corporate ID}
	LEFT OUTER JOIN `partnerpublic` partner ON accountline.`billToCode` = partner.`partnerCode` AND partner.`corpID` in('TSFT',$P{Corporate ID})
	LEFT OUTER JOIN `refmaster` refmaster on refmaster.`code` = billing.`creditTerms` and refmaster.`parameter` ='CRTERMS' and refmaster.`corpID` in ('TSFT',$P{Corporate ID}) 
	LEFT OUTER JOIN `refmaster` refmaster_con on systemdefault.`country` =refmaster_con.`code` and refmaster_con.`parameter` = 'COUNTRY' and refmaster_con.`corpID` in ($P{Corporate ID},'TSFT') and refmaster_con.`language`='en' 
WHERE   serviceorder.`corpID` = $P{Corporate ID}
	AND accountline.`recInvoiceNumber`=$P{Invoice Number} 
	AND serviceorder.`shipNumber` = $P{Service Order Number}
	-- AND serviceorder.`job` = 'RLO'
	AND (accountline.`recInvoiceNumber` IS NOT NULL OR accountline.`recInvoiceNumber` !='') 
	AND accountline.status is true 
	AND accountline.actualRevenue <>0
	AND (accountline.ignoreForBilling is not true || accountline.ignoreForBilling != true)
GROUP BY accountline.`id`
ORDER BY accountline.`recInvoiceNumber` ASC,
	accountline.`accountLineNumber` ASC,
	accountline.`chargeCode` ASC]]></queryString>

	<field name="InvoiceRLOMnVf-F-PDF-DOCX-RSKY.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="accountline_receivedInvoiceDate" class="java.lang.String"/>
	<field name="accountline_recInvoiceNumber" class="java.lang.String"/>
	<field name="ShipperName" class="java.lang.String"/>
	<field name="billing_billToReference" class="java.lang.String"/>
	<field name="serviceorder_origin_CityStateCountry" class="java.lang.String"/>
	<field name="serviceorder_destination_CityStateCountry" class="java.lang.String"/>
	<field name="Account_Cross_Reference" class="java.lang.String"/>
	<field name="refmaster_description" class="java.lang.String"/>
	<field name="PartnerName" class="java.lang.String"/>
	<field name="billing_attnBilling" class="java.lang.String"/>
	<field name="partner_billingAddress1" class="java.lang.String"/>
	<field name="partner_billingAddress2" class="java.lang.String"/>
	<field name="partner_billingAddress3" class="java.lang.String"/>
	<field name="partner_billingAddress4" class="java.lang.String"/>
	<field name="partner_billing_CityState" class="java.lang.String"/>
	<field name="partner_billing_ZipCountry" class="java.lang.String"/>
	<field name="accountline_description" class="java.lang.String"/>
	<field name="accountline_amount" class="java.math.BigDecimal"/>
	<field name="wording1" class="java.lang.String"/>
	<field name="specialInstruction" class="java.lang.String"/>
	<field name="systemdefault_company" class="java.lang.String"/>
	<field name="systemdefault_Address1" class="java.lang.String"/>
	<field name="systemdefault_Address2" class="java.lang.String"/>
	<field name="systemdefault_Address3" class="java.lang.String"/>
	<field name="systemdefault_CityState" class="java.lang.String"/>
	<field name="systemdefault_ZipCountry" class="java.lang.String"/>
	<field name="wording3" class="java.lang.String"/>
	<field name="wording4" class="java.lang.String"/>

	<variable name="TotalAmount" class="java.math.BigDecimal" resetType="Group" resetGroup="$F{accountline_recInvoiceNumber}" calculation="Sum">
		<variableExpression><![CDATA[$F{accountline_amount}]]></variableExpression>
	</variable>

		<group  name="$F{accountline_recInvoiceNumber}" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{accountline_recInvoiceNumber}]]></groupExpression>
			<groupHeader>
			<band height="227"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="332"
						y="1"
						width="34"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-20"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[SO# :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="27"
						width="161"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box rightPadding="3">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_recInvoiceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="277"
						height="182"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-11"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(	($F{PartnerName}==null || $F{PartnerName}.equals("") ? "" : ($F{PartnerName})+"\n")+
	($F{billing_attnBilling}==null || $F{billing_attnBilling}.equals("") ? "" : ($F{billing_attnBilling})+"\n")+
	($F{partner_billingAddress1}==null || $F{partner_billingAddress1}.equals("") ? "" : ($F{partner_billingAddress1}+"\n"))+
	($F{partner_billingAddress2}==null || $F{partner_billingAddress2}.equals("") ? "" : ($F{partner_billingAddress2}+"\n"))+
	($F{partner_billingAddress3}==null || $F{partner_billingAddress3}.equals("") ? "" : ($F{partner_billingAddress3})+"\n")+
	($F{partner_billingAddress4}==null || $F{partner_billingAddress4}.equals("") ? "" : ($F{partner_billingAddress4})+"\n")+
	($F{partner_billing_CityState}==null || $F{partner_billing_CityState}.equals("") ? "" : ($F{partner_billing_CityState})+"\n")+
	($F{partner_billing_ZipCountry}==null || $F{partner_billing_ZipCountry}.equals("") ? "" :($F{partner_billing_ZipCountry}))
)]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="1"
						width="161"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-19"/>
					<box rightPadding="3">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="279"
						y="89"
						width="86"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-21"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Origin :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="279"
						y="119"
						width="86"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-22"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Destination :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="279"
						y="14"
						width="87"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-26"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoice Date :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="279"
						y="27"
						width="87"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-27"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Invoice# :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="89"
						width="161"
						height="30"
						key="textField"/>
					<box rightPadding="3">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_origin_CityStateCountry}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="119"
						width="161"
						height="30"
						key="textField"/>
					<box rightPadding="3">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_destination_CityStateCountry}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="14"
						width="161"
						height="13"
						key="textField"/>
					<box rightPadding="3">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_receivedInvoiceDate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="279"
						y="40"
						width="87"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-46"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Assignee :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="40"
						width="161"
						height="37"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ShipperName}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="279"
						y="77"
						width="86"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-51"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Your Ref # :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="77"
						width="161"
						height="12"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToReference}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="279"
						y="166"
						width="86"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-58"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Credit Terms :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="166"
						width="161"
						height="17"
						key="textField-46"/>
					<box rightPadding="3"></box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{refmaster_description}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="279"
						y="149"
						width="86"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-61"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Payment Ref. :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="369"
						y="149"
						width="161"
						height="17"
						key="textField-49"/>
					<box rightPadding="3"></box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Account_Cross_Reference}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="436"
						y="205"
						width="93"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-65"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Amount]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="223"
						width="535"
						height="1"
						key="line-7"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="136"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="0"
						y="131"
						width="535"
						height="0"
						key="line-4"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<line direction="TopDown">
					<reportElement
						x="338"
						y="38"
						width="0"
						height="92"
						key="line-5"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="37"
						width="535"
						height="0"
						key="line-6"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="349"
						y="41"
						width="87"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-62"
						positionType="Float"
						isRemoveLineWhenBlank="true"
						isPrintWhenDetailOverflows="true"/>
					<box>					<bottomPen lineWidth="0.0" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Total Charges:]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="436"
						y="41"
						width="96"
						height="18"
						key="textField-52"/>
					<box leftPadding="1" rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TotalAmount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="7"
						width="530"
						height="28"
						key="textField-61"/>
					<box></box>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{specialInstruction}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="41"
						width="319"
						height="88"
						key="textField-72"
						isRemoveLineWhenBlank="true"/>
					<box></box>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Oblique" isBold="false" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{wording1}==null ? "" : $F{wording1})+"\n\n"+($F{wording3}==null ? "" : $F{wording3})+"\n\n"+($F{wording4}==null ? "" : $F{wording4})]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="60"  isSplitAllowed="false" >
				<image  onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="415"
						y="1"
						width="120"
						height="55"
						key="image-1"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/highres_logo.png"]]></imageExpression>
				</image>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="1"
						width="414"
						height="18"
						key="textField-62"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_description}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="436"
						y="1"
						width="96"
						height="18"
						key="textField-65"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1" rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_amount}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="74"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="4"
						width="464"
						height="70"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-68"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(	($F{systemdefault_company}==null || $F{systemdefault_company}.equals("") ? "" : ($F{systemdefault_company})+"\n")+
	($F{systemdefault_Address1}==null || $F{systemdefault_Address1}.equals("") ? "" : ($F{systemdefault_Address1}+"\n"))+
	($F{systemdefault_Address2}==null || $F{systemdefault_Address2}.equals("") ? "" : ($F{systemdefault_Address2}+"\n"))+
	($F{systemdefault_Address3}==null || $F{systemdefault_Address3}.equals("") ? "" : ($F{systemdefault_Address3})+"\n")+
	($F{systemdefault_CityState}==null || $F{systemdefault_CityState}.equals("") ? "" : ($F{systemdefault_CityState})+"\n")+
	($F{systemdefault_ZipCountry}==null || $F{systemdefault_ZipCountry}.equals("") ? "" : ($F{systemdefault_ZipCountry}))
)]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="535"
						height="0"
						key="line-9"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="511"
						y="4"
						width="24"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-70"/>
					<box topPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="464"
						y="4"
						width="47"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-71"/>
					<box topPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
