package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PolicyDocumentDao;
import com.trilasoft.app.model.PolicyDocument;
import com.trilasoft.app.service.PolicyDocumentManager;


public class PolicyDocumentManagerImpl extends GenericManagerImpl<PolicyDocument, Long> implements PolicyDocumentManager {
	PolicyDocumentDao policyDocumentDao;
	
	public PolicyDocumentManagerImpl(PolicyDocumentDao policyDocumentDao){
		super(policyDocumentDao);
		this.policyDocumentDao=policyDocumentDao;
		
	}
	public List checkPolicyId(Long fid) {
		return policyDocumentDao.checkPolicyId(fid);
	}
	public List getMaxId() {
		return policyDocumentDao.getMaxId();
	}
	public int checkPolicyDocumentName(String documentName,String sectionName,String language,String partnerCode,String sessionCorpID){
		return policyDocumentDao.checkPolicyDocumentName(documentName, sectionName, language, partnerCode, sessionCorpID);
	}
}
