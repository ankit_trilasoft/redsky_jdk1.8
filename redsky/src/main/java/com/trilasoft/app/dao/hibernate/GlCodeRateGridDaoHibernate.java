package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.GlCodeRateGridDao;
import com.trilasoft.app.model.CostElement;
import com.trilasoft.app.model.GlCodeRateGrid;

public class GlCodeRateGridDaoHibernate extends GenericDaoHibernate<GlCodeRateGrid, Long> implements GlCodeRateGridDao {
	public GlCodeRateGridDaoHibernate() {
		super(GlCodeRateGrid.class);
		
	}
	public List getAllRateGridList(String sessionCorpID,String costId,String jobType,String routing1,String recGl,String payGl){
		Long cid=Long.parseLong(costId);
		jobType=jobType.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		routing1=routing1.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");		
		recGl=recGl.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		payGl=payGl.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");		
		return getHibernateTemplate().find("from GlCodeRateGrid where costElementId="+cid+" and gridRecGl like '"+recGl+"%' and gridPayGl like '"+payGl+"%' and gridJob like '"+jobType+"%' and gridRouting like '"+routing1+"%'");
	}
}
