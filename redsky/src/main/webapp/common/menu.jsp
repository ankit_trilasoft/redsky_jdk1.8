<%@ include file="/common/taglibs.jsp"%> 
<%@ page import="java.util.Map,
javax.servlet.jsp.jstl.sql.Result,
net.sf.navigator.menu.MenuComponent,
net.sf.navigator.menu.MenuRepository,
com.trilasoft.app.webapp.action.MenuItemAction,
org.springframework.context.ApplicationContext,
org.springframework.web.servlet.support.RequestContextUtils"%>  
<authz:authorize ifNotGranted="ROLE_ANONYMOUS">
<%
ApplicationContext ctx = RequestContextUtils.getWebApplicationContext(request, application);
MenuItemAction menuItemAction = (MenuItemAction)ctx.getBean("menuItemAction");
menuItemAction.initializeRepository();
%>

<%
try{
%>
<menu:useMenuDisplayer name="Velocity" config="WEB-INF/classes/cssHorizontalMenu.vm" permissions="rolesAdapter" repository="repository">       
<ul id="primary-nav" class="menuList">   
    <li class="pad">&nbsp;</li>
    <c:if test="${empty pageContext.request.remoteUser}">   
    <li><a href="<c:url value="/login.jsp"/>" class="current">   
        <fmt:message key="login.title"/></a></li>   
    </c:if>
       
    <c:forEach var="menu" items="${repository.topMenus}">
    	<% try { %>
        <menu:displayMenu name="${menu.name}"/>  
        <%} catch (Exception ex){
        	//ex.printStackTrace();
        }
        	%> 

    </c:forEach>
       
        
</ul> 
</menu:useMenuDisplayer>
<%
}catch(Exception ex){}
%>
</authz:authorize>
