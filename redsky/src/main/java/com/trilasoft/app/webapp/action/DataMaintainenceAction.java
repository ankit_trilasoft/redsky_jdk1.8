package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.collections.IterableMap;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.DataMaintainence;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.DataMaintainenceManager;

public class DataMaintainenceAction extends BaseAction{
	private Long id;
	private DataMaintainenceManager dataMaintainenceManager;
	private DataMaintainence  dataMaintainence;
	private List dataMaintainenceList;
	private List corpIDList;
	private CompanyManager companyManager;
	private List executedResult;
	
	 private String hitFlag="";
	 Date currentdate = new Date();

	 static final Logger logger = Logger.getLogger(DataMaintainenceAction.class);


	
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataMaintainenceList=dataMaintainenceManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    
	public String save()throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew=(dataMaintainence.getId()==null);
		dataMaintainenceManager.save(dataMaintainence);
		String key = (isNew) ? "Added successfully" : "Updated successfully";
		saveMessage(getText(key));
		getComboBox();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		}

	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboBox();
		if(id!=null)
		{
			
			dataMaintainence=dataMaintainenceManager.get(id);
		}else{
		dataMaintainence=new DataMaintainence();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String executeQuery( ){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataMaintainence=dataMaintainenceManager.get(id);
		if(dataMaintainence!=null){
		if((dataMaintainence.getQuery().equals(""))){
		String key = "Please Enter The Correct Query";
		errorMessage(getText(key));
		}else{
			executedResult=dataMaintainenceManager.executeQuery(dataMaintainence.getQuery());	
			if(executedResult.isEmpty())
				{String key = "Please Enter The Correct Query";
			            errorMessage(getText(key));
		        }
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String bulkUpdate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String result="success";
		dataMaintainence=dataMaintainenceManager.get(id);
		executedResult=dataMaintainenceManager.executeQuery(dataMaintainence.getQuery());
		Iterator<String> it= executedResult.iterator();
		while(it.hasNext()){
			String query=it.next();
			if(result.equals("success")){
				result=dataMaintainenceManager.bulkUpdate(query);
			}else{
				errorMessage(getText(result));
				break;
				}				
			}
		if(result.equals("success")){
			String key = "Executed successfully";
			saveMessage(getText(key));
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String delete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataMaintainenceManager.remove(id);
		String key =  "Deleted successfully";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
  public String getComboBox(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		corpIDList=companyManager.getcorpID();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
  }
	public DataMaintainence getDataMaintainence() {
		return dataMaintainence;
	}

	public void setDataMaintainence(DataMaintainence dataMaintainence) {
		this.dataMaintainence = dataMaintainence;
	}

	public List getDataMaintainenceList() {
		return dataMaintainenceList;
	}

	public void setDataMaintainenceList(List dataMaintainenceList) {
		this.dataMaintainenceList = dataMaintainenceList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List getCorpIDList() {
		return corpIDList;
	}

	public void setCorpIDList(List corpIDList) {
		this.corpIDList = corpIDList;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setDataMaintainenceManager(
			DataMaintainenceManager dataMaintainenceManager) {
		this.dataMaintainenceManager = dataMaintainenceManager;
	}
	
	public List getExecutedResult() {
		return executedResult;
	}

	public void setExecutedResult(List executedResult) {
		this.executedResult = executedResult;
	}




	public String getHitFlag() {
		return hitFlag;
	}


	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
}
