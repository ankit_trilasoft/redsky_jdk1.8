 <%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
</head> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style>

<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
	
	<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['contractListForm'].elements['pCode'].value = "";
	document.forms['contractListForm'].elements['description'].value = "";
	document.forms['contractListForm'].elements['billingCountryCode'].value = "";
	document.forms['contractListForm'].elements['billingCountry'].value = "";
	}

	function getContract(){
		var contractCd = document.forms['contractListForm'].elements['pCode'].value;
		var contractDesc = document.forms['contractListForm'].elements['description'].value;
		var contractCountryCode = document.forms['contractListForm'].elements['billingCountryCode'].value;
		var contractCountryCode = document.forms['contractListForm'].elements['billingCountry'].value; 
		url = "searchOrderInitiationBillToCode.html?OADAAccountPortalValidation=${OADAAccountPortalValidation}&decorator=popup&popup=true&partnerCode="+contractCd+"&lastName="+contractDesc;
		document.forms['contractListForm'].action = url;
		document.forms['contractListForm'].submit();
	}
	
	function findAgent(position,str) {
		var partnerCode =str;
			var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&accountLineBillingParam=Billing";
		ajax_showTooltip(url,position); 
		}
</script> 

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" onclick="getContract();" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set> 

<s:form id="contractListForm" method="post" >
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<s:hidden name="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	</c:if>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>

<th><fmt:message key="partner.partnerCode"/></th>

<th><fmt:message key="contract.description"/></th>
<th>Country Code</th>
<th>Country Name</th>
<th>City</th>
<th>Ext Reference</th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="pCode" size="9" required="true" cssClass="text medium" onfocus="onFormLoad();" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			    <td><s:textfield name="description" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			     <td><s:textfield name="billingCountryCode" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			      <td><s:textfield name="billingCountry" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			       <td><s:textfield name="billingCity" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			       <td><s:textfield name="extReference" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<div id="layer1" style="width:100%">
<div id="otabs">
	<ul>
		<li><a class="current"><span>Partners</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div> 

<div id="newmnav">   
</div><div class="spnblk" style="width:800px">&nbsp;</div>
<s:set name="partner" value="partnerPublicChild"/>   
<display:table name="partner" class="table" requestURI="" id="partnerPublicChild" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%"
				decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editOrderInitiation.html" paramId="billToCode" paramProperty="customerFile.billToCode" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>
    	<display:column property="lastName" sortable="true" titleKey="contract.description" paramId="billToName" paramProperty="customerFile.billToName"/>
    	 <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    	 <display:column property="billingCity" sortable="true" title="City" style="width:35px"/>
    	  <display:column property="extReference" sortable="true" title="ExtReference" style="width:35px"/>
    	 <display:column title="Address">
    	 <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'${partnerPublicChild.partnerCode}');" src="<c:url value='/images/address2.png'/>" />
    	</display:column>
    	   	
</display:table>
</div>
</div> 
</s:form>
	
	
	
