package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ExtractQueryFile;

public interface ExtractQueryFileManager extends GenericManager<ExtractQueryFile, Long> { 
	public List oldQueries(String userId); 
	public List fingQueryName(String remoteUser, String sessionCorpID, String queryName, String publicPrivateFlag);
	public List getSchedulerQueryList();
	public void updateSendMailStatus(Long id, String lastEmailSent, String emailStatus, String emailMessage);
	public List findDataExtractMailInfo(Long id);
	public List searchExtractQueryFiles(String queryName,String publicPrivateFlag,String modifiedby,String userId);
}
