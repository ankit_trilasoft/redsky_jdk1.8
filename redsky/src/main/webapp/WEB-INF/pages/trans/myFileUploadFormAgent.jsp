<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />
<c:if test="${param.popup}">
	<link rel="stylesheet" type="text/css" media="all"
		href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" />
</c:if>
<style type="text/css">
.subcontent-tab {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #FFF; text-decoration: none; background:url(images/topband-note.gif) #BCD2EF;
	padding:5px 3px 0px 15px; height:20px; border-right:none; border-left:none; } 

.mainDetailTable { background-image:url(images/filecabinet-bg.gif); background-position:left bottom; background-repeat:repeat-x; border-color:-moz-use-text-color #74B3DC #74B3DC;
border-style:none solid solid; border-width:medium 1px 1px;
} 
.text-area { border:1px solid #219DD1; font: 12px arial;}
</style>

<script>
var http125678 = getHTTPObject();
function setDocCat(targetElement){
	//alert('${soCorpId}');
	var doctype=targetElement.options[targetElement.selectedIndex].value;
	var url="docCategoryByDocType.html?ajax=1&decorator=simple&popup=true&soCorpId=${soCorpId}&docType="+encodeURI(doctype);
	//alert(url);
	http125678.open("GET", url, true);
	http125678.onreadystatechange = handleHttpResponseFordocCategory1;
	http125678.send(null);
}
function handleHttpResponseFordocCategory1(){
	//alert("re...");
    if(http125678.readyState == 4){
		var results = http125678.responseText
      results = results.trim();
		//alert(results);
	document.forms['uploadForm'].elements['myFile.documentCategory'].value = results; 
							     	 
	}
}
function validateFields(){
	if(document.forms['uploadForm'].elements['fileType'].value==""){
		alert("Select a file type");
		return false;
	}
	else{
		if(document.forms['uploadForm'].elements['file'].value==""){
			alert("Select a file");
			return false;
		}else{
			var filename = document.forms['uploadForm'].elements['file'].value;
			var extension=filename.substring(filename.indexOf('.')+1, filename.length);
         	 if(/\.pdf|\.txt|\.jpg|\.jpeg|\.GIF|\.tiff|\.xls|\.xlsx|\.doc|\.docx|\.tif|\.msg$/i.test(filename))
             {
         	    return true;
             }
             else
             {
            	 alert( "Please select a valid file format. \ntxt,pdf,jpg,jpeg,tiff,tif,xls,xlsx,doc,docx are only allowed" ) ;
          		return false;
             }
		}
	}
}

function validateUpload(){

			if(document.forms['uploadForm'].elements['file'].value==""){
			//document.forms['uploadForm'].elements['method:save'].disabled=true;
			document.forms['uploadForm'].elements['file'].focus;
		}
		else{
			//document.forms['uploadForm'].elements['method:save'].disabled=false;
			document.forms['uploadForm'].elements['file'].focus;
			
			
	}
}

function documentAccessControl(){
     var fileType = document.forms['uploadForm'].elements['fileType'].value;
     var url="docAccessControl.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}

function handleHttpResponse2(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                
                var res = results.replace("[",'');
                res = res.replace("]",'');
                if(res !=''){
                if(results.length>2){
                res = res.split("#");
                if(res[10]=='true'){
                	document.forms['uploadForm'].elements['myFile.invoiceAttachment'].value=true;
	             	//document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=true;
	             	var checkedValueIA=document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked;
	  	            document.uploadApplet.setinvoiceAttachment2(checkedValueIA);
             	}else{
             		document.forms['uploadForm'].elements['myFile.invoiceAttachment'].value=false;
	             	//document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=false;
	             	document.uploadApplet.setinvoiceAttachment2('false');
             	}
                
           }
           }
           }
           else{
           document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=false;
           document.uploadApplet.setinvoiceAttachment2('false');
                     }
}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
                  
        
	var http2 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function setDescription(targetElement){
	var flagUloadInv=document.forms['uploadForm'].elements['flagUloadInv'].value
	var invoiceDate=document.forms['uploadForm'].elements['invoiceDate'].value
	var vendorInvoice=document.forms['uploadForm'].elements['vendorInvoice'].value
	if(flagUloadInv)
		{
	var amount=document.forms['uploadForm'].elements['amount'].value
	var currency=document.forms['uploadForm'].elements['currency'].value
	var vatamount=document.forms['uploadForm'].elements['vatamount'].value
	document.forms['uploadForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text+" : "+vendorInvoice+" "+invoiceDate +" / "+currency+" "+amount;
	if(targetElement.options[targetElement.selectedIndex].text==""){
	setForAlert();}
	}
	else
		{
		document.forms['uploadForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text;
		if(targetElement.options[targetElement.selectedIndex].text==""){
		setForAlert();
	    }}
}
function setDescriptionDuplicate(targetElement){

	document.forms['uploadForm'].elements['description'].value = targetElement.options[targetElement.selectedIndex].text;
}

function setDescriptionPartnerAppl(targetElement){
	var checkedValuePP=document.forms['uploadForm'].elements['myFile.isPartnerPortal'].checked;
	document.uploadApplet.setPartnerPortal(checkedValuePP);
}

function setForAlert(){
    if(document.forms['uploadForm'].elements['myFile.description'].value=="")
    {
    alert("Please choose a Document Type before uploading a file");
    self.document.location.reload(true);
   }
}

</script>

</head>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>

<s:form action="uploadAgentsFile.html?id=${fileId}&myFileFor=${fileNameFor}&decorator=popup&popup=true" enctype="multipart/form-data" method="post" validate="true" id="uploadForm" name="uploadForm">
<s:hidden name="customerFile.id" />
<s:hidden name="rowId" id="rowId" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="myFile.fileId"/>
<s:hidden name="amount"  value="<%=request.getParameter("amount") %>" />
<c:set var="amount" value="<%=request.getParameter("amount") %>"/>
<s:hidden name="currency"  value="<%=request.getParameter("currency") %>" />
<s:hidden name="invoiceDate"  value="<%=request.getParameter("invoiceDate") %>" />
<c:set var="invoiceDate" value="<%=request.getParameter("invoiceDate") %>"/>
<s:hidden name="vendorInvoice"  value="<%=request.getParameter("vendorInvoice") %>" />
<c:set var="vendorInvoice" value="<%=request.getParameter("vendorInvoice") %>"/>

<c:set var="currency" value="<%=request.getParameter("currency") %>"/>
<s:hidden name="vatamount"  value="<%=request.getParameter("vatamount") %>" />
<c:set var="vatamount" value="<%=request.getParameter("vatamount") %>"/>
<c:set var="vendorCode" value="<%=request.getParameter("vendorCode") %>"/>
<s:hidden name="vendorCode"  value="<%=request.getParameter("vendorCode") %>" />
<s:hidden name="myFile.customerNumber"/>
<s:hidden name="myFile.corpID"/>
<s:hidden name="myFileFor" />
<s:hidden name="myFile.invoiceAttachment" />
<c:set var="fid" value="${myFile.id}"/>
<s:hidden name="fid" value="${myFile.id}"/>
<c:set var="description" value="${myFile.description}"/>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
<c:set var="flagUloadInv" value="<%=request.getParameter("flagUloadInv") %>"/>
<s:hidden name="flagUloadInv"  value="<%=request.getParameter("flagUloadInv") %>" />

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>"/>
<div id="Layer1">
	<div id="Layer4" style="width:700px;">
	<div id="newmnav">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Upload<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</ul>
	</div>	
	
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:3px;"></div>
	</div>

			<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:550px;">
				<tbody>
				<tr><td colspan="5" width="550px">
					<div class="subcontent-tab" style="border:1px solid #74B3DC; border-bottom:none;"> Upload A Document</div></td>
					</tr>
	                   
					<c:if test="${empty myFile.id}">					
						<tr>
						<td colspan="" valign="top">
						<table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;">
						<tr><td height="4px"></td></tr>
						<tr>
						<td align="right" class="listwhitetext" style="width:90px;" valign="top">Document Type</td>
                        <c:choose>
                        <c:when test="${flagUloadInv}">
						<td id="myDiv" style="width:100px;" valign="top"><s:select name="fileType" cssClass="list-menu" cssStyle="width:250px"  list="%{docsListAccountPortal}"  headerKey="" headerValue="" onchange="setDocCat(this);setDescription(this),documentAccessControl(this);"/></td>

						<td colspan="2" valign="top">
						  
						    <table cellpadding="1" cellspacing="1" border="0" style="margin: 0px;width:280px;">
							<tr>
						       <td align="left" class="listwhitetextFC" width="162px">&nbsp;&nbsp;Partner Code<font color="red" size="2">*</font>&nbsp;<s:textfield cssClass="input-textBlackUpperFC" name="myFile.accountLineVendorCode"  value="${vendorCode}" cssStyle="width:80px" size="10" readonly="true" /></td>
						     <%--   <td align="left"width=""><img align="left"  disabled="true" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" id="openpopup2.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
						    --%> </tr>
						    </table>
						    
						    </td>
						<td width="0px"></td>					
					</tr>
					<tr>
						<td align="right" class="listwhitetextFC" style="width:110px;" height="30px">Document&nbsp;Category<!-- <font color="red" size="2">*</font> --></td>
							<td><s:textfield cssClass="input-textBlackUpperFC" name="myFile.documentCategory"    cssStyle="width:229px" size="25" readonly="true"/></td>
							
						</tr>
					<tr>
					<td align="right" valign="top" class="listwhitetext" style="width:90px">Description</td>
							<td colspan="3" valign="top"><s:textarea cssClass="textarea" readonly="true" name="myFile.description"
								onchange="(document.uploadApplet.setFileDescription(this.value.replace(/ /g,'%20')));"
								 cssStyle="width:246px;height:70px;" />
						</td>
						
						</tr>
						</c:when>
						
				<c:otherwise>
					<td style="width:100px;" valign="top"><s:select name="fileType" cssClass="list-menu" cssStyle="width:250px" list="%{docsListAccountPortal}" headerKey="" headerValue="" onchange="setDocCat(this);setDescription(this);"/></td>
						<td width="0px"></td>					
					</tr>
					<td align="right" class="listwhitetext" style="width:90px;">Document&nbsp;Category</td>
							<td><s:textfield cssClass="input-text" name="myFile.documentCategory" readonly="true"  cssStyle="width:246px" size="25"/></td>
						</tr>
					<tr>
					<td align="right" valign="top" class="listwhitetext" style="width:90px">Description</td>
							<td colspan="3" valign="top"><s:textarea cssClass="textarea"  name="myFile.description"
								onchange="(document.uploadApplet.setFileDescription(this.value.replace(/ /g,'%20')));"
								 cssStyle="width:246px;height:70px;" />
				  </c:otherwise>
				  </c:choose>
								<c:if test="${empty myFile.id}">
						<tr valign="top">
							<td align="left" valign="top" colspan="3" >
							<table border="0" style="margin-bottom: 0px;">
							<tr>
							
							<td align="left" valign="top" colspan="2" width="">
							<table cellpadding="1" cellspacing="0" border="0" class="detailTabLabel" >
							<tr>
							<td class="listwhitetext" style="width:90px" align="right"><fmt:message key='myFile.file' /></td>
								<td><s:file name="file"  label="%{getText('uploadForm.file')}" required="true" size="25" onblur="validateUpload();" onclick="validateUpload();"/></td>
							</tr>
							<tr>
							<td></td>
								<td style="padding-top:12px;"><s:submit key="button.upload" name="myFile.upload" cssClass="cssbuttonA" cssStyle="width:80px;" onclick="return validateFields();" />
								</td>
							</tr>
							<tr>
							<td></td>
							<c:if test="${enterpriseLicense}">
							<td valign="bottom" colspan="" align="left" height="20px"><font style="color:#FF0C0C;"><b>Note:</b><i>Max file size allowed is 10MB</i></font></td>
							</c:if>
							<c:if test="${!enterpriseLicense}">
							<td valign="bottom" colspan="" align="left" height="20px"><font style="color:#FF0C0C;"><b>Note:</b><i>Max file size allowed is 5MB</i></font></td>
							</c:if>
							</tr>
							</table>						
							</td>
							</tr>	
						
									
						</table>
						</td>
						</tr>
					
					</c:if>
					</table>
					</td>
					
					</tr>
					</c:if>
					
				
				</tbody>
			</table>

<table border="0">
	<tbody>	
		<tr>
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdOn' /></b></td>
			
			<td style="width:130px"><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${myFile.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="myFile.createdOn" value="${customerFileCreatedOnFormattedValue}" /> 
				<fmt:formatDate value="${myFile.createdOn}" pattern="${displayDateTimeFormat}" />
			</td>
			<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='billing.createdBy' /></b></td>
			<c:if test="${not empty myFile.id}">
				<s:hidden name="myFile.createdBy" />
				<td style="width:65px"><s:label name="createdBy" value="%{myFile.createdBy}" /></td>
			</c:if>
			<c:if test="${empty myFile.id}">
				<s:hidden name="myFile.createdBy" value="${pageContext.request.remoteUser}" />
				<td style="width:65px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>				
			</tr>
			<tr>
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn' /></b></td>
			<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${myFile.updatedOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="myFile.updatedOn" value="${customerFileupdatedOnFormattedValue}" />
			<td style="width:120px"><fmt:formatDate value="${myFile.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
			<s:hidden name="myFile.updatedBy" value="${pageContext.request.remoteUser}" />
			<td style="width:85px"><s:label name="myFile.updatedBy" value="${pageContext.request.remoteUser}" /></td>
		</tr>
	</tbody>
</table>
<c:if test="${empty myFile.id}">
	<table>
		<tr>
			<td style="padding-left:">
	 			<input type="button" class="cssbuttonA" style="margin-right: 5px;width:70px;" value="Close" class="button" onclick="window.close()" />
	 		</td>	
			
		</tr>				
	</table>
</c:if>
			
</s:form>
<script>
try{
	var errorType = '${fileNotExists}';
	}
	catch(e){}
	try{
	if( errorType != ''){
		alert('${fileNotExists}');
		//location.href="uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=${secure}&forQuotation=${forQuotation}";
	}
	}
	catch(e){}
try{
<c:if test="${hitflag == 1}">
var flagUloadInv=document.forms['uploadForm'].elements['flagUloadInv'].value
if(flagUloadInv)
	{
var row='${rowId}';
window.opener.document.forms['accountFileList'].elements['rowId'].value= row; 
parent.window.opener.submitAfterUpload(); 
alert('Document has been successfully Uploaded.');
window.close();
 if (window.opener && !window.opener.closed) {
window.opener.location.reload();
 }}
else
	{
	alert('Document has been successfully Uploaded and email has been sent.');
	window.close();
	if (window.opener && !window.opener.closed) {
	window.opener.location.reload();
	}}
</c:if>
}
catch(e){}
try{
<c:if test="${hitflag == 2}">
alert('Document has been successfully Uploaded. But Mail Sending Failed');
window.close();
if (window.opener && !window.opener.closed) {
window.opener.location.reload();
}
</c:if>
}
catch(e){}


function winOpen(){
 	var sid=document.forms['uploadForm'].elements['serviceOrder.id'].value;
 	  openWindow('findVendorCode.html?sid=${serviceOrder.id}&partnerType=DF&accType=myFileNew&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=isVenderFilecabitateValue&fld_code=myFile.accountLineVendorCode&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['uploadForm'].elements['myFile.accountLineVendorCode'].select();	
 }
 
var flagUloadInv=document.forms['uploadForm'].elements['flagUloadInv'].value
if(flagUloadInv)
	{
var fileType=document.forms['uploadForm'].elements['fileType'];
setDocCat(fileType);
setDescription(fileType);
documentAccessControl(fileType)
     
document.forms['uploadForm'].elements['myFile.invoiceAttachment'].checked=true;
	}
    
</script>
