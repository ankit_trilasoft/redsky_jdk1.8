package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AgentBaseDao;
import com.trilasoft.app.model.AgentBase;

public class AgentBaseDaoHibernate extends GenericDaoHibernate<AgentBase, Long> implements AgentBaseDao{
	
	public AgentBaseDaoHibernate() {
		super(AgentBase.class);
	}
	
	public List getbaseListByPartnerCode(String partnerCode){
		return getHibernateTemplate().find("from AgentBase where partnerCode like '"+partnerCode+"%'");
	}
	
	public List isExisted(String baseCode, String sessionCorpID, String partnerCode){
		return getHibernateTemplate().find("from AgentBase where baseCode = '"+baseCode+"' AND partnerCode like '"+partnerCode+"%'");
	}

	public void deleteBaseSCAC(String partnerCode, String baseCode, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("delete from AgentBaseSCAC where baseCode = '"+baseCode+"' AND corpID ='"+sessionCorpID+"' AND partnerCode = '"+partnerCode+"'");
	}

}
