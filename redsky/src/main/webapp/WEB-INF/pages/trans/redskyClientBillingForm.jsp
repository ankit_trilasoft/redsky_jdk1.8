<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title>Redsky Billing</title> 
    	<meta name="heading" content="Redsky Billing"/> 
  <script language="javascript" type="text/javascript">
  function yearlist(){
  	var currentTime = new Date()
	var year = currentTime.getFullYear()
	var year1 = currentTime.getFullYear()
	var year2 = currentTime.getFullYear()
	var ddl = document.getElementById('year'); 
	year1=year1-4
	for(i = 1; i <= 3; i++) {
	year1=year1+1;
	var opt = document.createElement("option");
	opt.text=year1;
	opt.value=year1;
	document.getElementById("year").options.add(opt);
	}
	var opt = document.createElement("option");
	opt.text=year;
	opt.value=year;
	document.getElementById("year").options.add(opt);
	for(j = 1; j <= 3; j++) {
	year2=year2+1;
	var opt = document.createElement("option");
	opt.text=year2;
	opt.value=year2;
	document.getElementById("year").options.add(opt);
	}
	var datam=year;
	document.forms['redskyClientBillingForm'].elements['year'].value=datam;

			
}
function validateForm(){

if(document.forms['redskyClientBillingForm'].elements['year'].value=='')
	{
	alert("Select Year");
	return false;
	}
if(document.forms['redskyClientBillingForm'].elements['corpID'].value=='')
	{
	alert("Select Company Name");
	return false;
	}
}
</script>
 
 </head>
 
 <s:form name="redskyClientBillingForm" id="redskyClientBillingForm" action="redskyBillingList" method="post" validate="true" onsubmit="return validateForm()" cssClass="form_magn">
	<s:hidden name="corpid" value="${corpIDParam}"></s:hidden>
	<div id="Layer1" style="width:100%;"> 
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Redsky Billing</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">	
<table border="0" class="" style="width:35%;" cellspacing="0" cellpadding="2">
	<tr>
<th class="listwhitetext"><fmt:message key="year"/></th>
<th class="listwhitetext"><fmt:message key="company.companyName"/></th>
</tr>
		<tr>
			
			<td><s:select id='year'  name='year' cssStyle="width:100px" cssClass="list-menu"  list="%{''}"/></td>
			<td>
		<c:if test="${corpIDParam == 'TSFT'}">
				<s:select name='corpID'cssStyle="width:200px" cssClass="list-menu" headerKey="" headerValue="" list="%{corpIDList}"/>
	 		</c:if>
 		<c:if test="${corpIDParam != 'TSFT'}">
	 			<s:textfield name='corpID'  id="corpID" value="%{companyName}" maxlength="40" required="true" cssClass="input-text" size="30"/>
		</c:if></td>
 		</tr>
 		<tr><td style="!height:15px;"></td></tr>

 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>

 <s:submit type="button" cssClass="cssbutton" cssStyle="width:100px; height:25px" key="button.goToBilling" ></s:submit>
 
  </div>
 
 
 </s:form>
 <script type="text/javascript">
	try{
	yearlist();	
	}
	catch(e){}
</script>
 