<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    <style>
 span.pagelinks {
display:block; font-size:0.95em;
margin-bottom:0px; !margin-bottom:2px;
margin-top:-17px; padding:2px 0px;
text-align:right; width:100%;
!width:100%;
}
</style>
	<c:set var="pType" value="<%=request.getParameter("partnerType")%>"/>
  <s:form id="assignedSecurity" action="" method="post" validate="true">
  <div style="padding-bottom:10px;"></div>
  <div id="newmnav">
  <c:choose>
	  <c:when test="${pType == 'AG' }">
	  	<ul>
	  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Filter Sets<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	  </ul>
	  </c:when>
	  <c:otherwise>
	  <ul>
	  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Security Sets<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	  </ul>
	  </c:otherwise>
  </c:choose>
	</div><div class="spn">&nbsp;</div>
	<div style="padding-bottom:3px;"></div>

	<display:table name="assignedSecurityList" requestURI="" defaultsort="1" id="assignedSecurityList" class="table" pagesize="10" style="width:100%" >
   
    <display:column property="setName"  sortable="true" title="Set Name" style="width: 15%"/>
    <display:column property="setDescription"  sortable="true" title="Set Description" style="width: 15%"/>
    <display:column property="filterName"  sortable="true" title="Filter Name" style="width: 15%"/>
    <display:column property="tableName"  sortable="true" title="Table Name" style="width: 15%"/>
    <display:column property="fieldName"  sortable="true" title="Field Name" style="width: 15%"/>
    <display:column property="filterValues"  sortable="true" title="Filter Values" style="width: 15%"/>
    
   
     <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>
</s:form>