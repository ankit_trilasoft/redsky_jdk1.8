package com.trilasoft.app.webapp.util;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.service.AppInitServletManager;

public class CacheRefresherJob extends QuartzJobBean {
	
	private static Logger log = Logger.getLogger(CacheRefresherJob.class);
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	private AppInitServletManager appInitServletManager ;
	
	
	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		try {
			appCtx = getApplicationContext(context);
			appInitServletManager = (AppInitServletManager) appCtx.getBean("appInitServletManager");
			long strtRef = System.currentTimeMillis();
			appInitServletManager.populatePermissionCache(AppInitServlet.pageActionUrls);
			log.info("Refresh completed in "+(System.currentTimeMillis()-strtRef)+" milli seconds");
		} catch (Exception e) {
			e.printStackTrace();
		}  
	}

	
}
