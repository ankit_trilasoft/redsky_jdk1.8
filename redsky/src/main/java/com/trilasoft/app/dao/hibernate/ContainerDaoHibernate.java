/**
 * @Class Name  Container Dao Hibernate
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.ContainerDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOvatCodeTotal;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.webapp.action.CartonAction;
import com.trilasoft.app.webapp.action.ContainerAction;

public class ContainerDaoHibernate extends GenericDaoHibernate<Container, Long> implements ContainerDao
{   
    public ContainerDaoHibernate() 
    {   
       super(Container.class);
    }
    public List<Container> findByLastName(String lastName) {   
        return getHibernateTemplate().find("from Container where lastName=?", lastName);   
    }
    public List findMaxId() {
		
		return getHibernateTemplate().find("select max(id) from Container");
	}
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from Container where id=?",id);
    }
    public List getGross(String shipNumber) {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(grossWeight, 0)) is null , 0.00 , sum(coalesce(grossWeight, 0)) )  from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getTare(String shipNumber)
    {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if(sum(coalesce(emptyContWeight, 0)) is null , 0.00 , sum(coalesce(emptyContWeight, 0)) )  from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getNet(String shipNumber)
    {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(netWeight, 0)) is null , 0.00 , sum(coalesce(netWeight, 0)) ) from container where status = true and  shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getPieces(String shipNumber)
    {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(pieces, 0)) is null , 0 , sum(coalesce(pieces, 0)) ) from container where  status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getVolume(String shipNumber){
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(volume, 0)) is null , 0.00 , sum(coalesce(volume, 0)) )  from container where status = true and  shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getWeights(String shipNumber)
    {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if ( sum(coalesce(grossWeight, 0)) is null , 0.00 , sum(coalesce(grossWeight, 0)) ) from container where  status = true and  shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
    	
    	String unitQuery="";
    	String unit ="";
		List l =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+sessionCorpID+"'").list();
		if(l!=null && !l.isEmpty() && l.get(0)!=null && (!(l.get(0).toString().trim().equals("")))){
			unit = l.get(0).toString();
		}
		if(unit !=null && unit.trim().equalsIgnoreCase("YES")){
			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
		}
    	 
    	
    	
    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualGrossWeight='" +actualGrossWeight+ "' , actualNetWeight='" +actualNetWeight+ "' , actualTareWeight='"+actualTareWeight+"' " +unitQuery+ " ,updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
    	
    	ServiceOrder serviceOrder = new ServiceOrder();
    	List linkedShipNumberList= new ArrayList();
    	List soList =new ArrayList();
    	
		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
		try{
			soList = getHibernateTemplate().find(query1);
			if(!soList.isEmpty()){
				 serviceOrder = (ServiceOrder)soList.get(0);
			}
		}catch(Exception e){
			e.getStackTrace();
		}
		
	   	try{
	        if(serviceOrder.getIsNetworkRecord()){		        	
	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	        	}else{
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	        	}		        	
	        	
				Iterator  it=linkedShipNumberList.iterator();
		    	while(it.hasNext()){			    			
		    		String shipNum=(String)it.next();
		    		String corpId = shipNum.substring(0, 4);
		    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
		    			unitQuery="";
		    			String unit1 ="";
		        		List list =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+corpId+"'").list();
		        		if(list!=null && !list.isEmpty() && list.get(0)!=null && (!(list.get(0).toString().trim().equals("")))){
		        			unit1 = list.get(0).toString();
		        		}
		        		if(unit1 !=null && unit1.trim().equalsIgnoreCase("YES")){
		        			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
		        		}
		    			
		    			
		    			String cartonContainerUpdateQuery1="update miscellaneous set actualGrossWeight='" +actualGrossWeight+ "' , actualNetWeight='" +actualNetWeight+ "' , actualTareWeight='"+actualTareWeight+"' " +unitQuery+  " ,updatedBy='"+sessionCorpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
		    		}
		    	}
	        }
	   	}catch(Exception ex){
			System.out.println("\n\n\n\n error while syn old data"+ex);
			ex.printStackTrace();
		}
	   	return 1;
    }
    public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy){
    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualCubicFeet='" +actualCubicFeet+ "',netActualCubicFeet='" +actualCubicFeet+ "',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
    	List al=this.getSession().createSQLQuery("select concat(if(grpPref is true,1,0),'#',if(grpStatus is null or grpStatus='',' ',grpStatus),'#',if(grpId is null or grpId='',' ',grpId)) as rec from serviceorder where shipNumber='"+shipNumber+"' and corpId='"+corpID+"'").list();
    	if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
    	{
    		String[] temp=al.get(0).toString().split("#");
	            if((temp[0]!=null)&&(!temp[0].equals(""))&&(temp[0].equalsIgnoreCase("1")))
	            {
	            	if((temp[2].trim()!=null)&&(!temp[2].trim().equals(""))&&(!temp[1].trim().equalsIgnoreCase("Finalized")))
	            	{
	            		try{
	            			String childIdList=getAllChildId(temp[2].trim().toString(),corpID);
	            			if(!childIdList.equalsIgnoreCase(""))
	            			{
	            				List newordersList=weightOfOrdersToGrouped(childIdList,corpID);
	            				if(newordersList!=null && !(newordersList.isEmpty()) && newordersList.get(0)!=null){
	            					 String tempWeight=newordersList.get(0).toString();
	            					 String[] str=tempWeight.split("#");
	            					 if(!str[1].equalsIgnoreCase("0")){	            				    	
	            				    	this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr='"+str[1]+"' where id='"+temp[2].toString()+"'").executeUpdate();	            				    	
	            					 }else{
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr=0 where id='"+temp[2].toString()+"'").executeUpdate();
	            					 }
	            					 if(!str[0].equalsIgnoreCase("0")){
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet='" +str[0]+ "' where id='"+temp[2].toString()+"'").executeUpdate();
	            					 }else{
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet=0 where id='"+temp[2].toString()+"'").executeUpdate();            						 
	            					 }
	            				}
	            			}
	            			
	            		}catch(Exception e){}
	            	}
	            }            
    	}
    	
    	ServiceOrder serviceOrder = new ServiceOrder();
    	List linkedShipNumberList= new ArrayList();
    	List soList =new ArrayList();
    	
		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
		try{
			soList = getHibernateTemplate().find(query1);
			if(!soList.isEmpty()){
				 serviceOrder = (ServiceOrder)soList.get(0);
			}
		}catch(Exception e){
			e.getStackTrace();
		}
		
	   	try{
	        if(serviceOrder.getIsNetworkRecord()){		        	
	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	        	}else{
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	        	}		        	
	        	
				Iterator  it=linkedShipNumberList.iterator();
		    	while(it.hasNext()){			    			
		    		String shipNum=(String)it.next();
		    		String corpId = shipNum.substring(0, 4);
		    		if(!corpId.equalsIgnoreCase(corpID)){
		    			String cartonContainerUpdateQuery1="update miscellaneous set actualCubicFeet='" +actualCubicFeet+ "',netActualCubicFeet='" +actualCubicFeet+ "',updatedBy='"+corpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
		    		}
		    	}
	        }
	   	}catch(Exception ex){
			System.out.println("\n\n\n\n error while syn old data"+ex);
			ex.printStackTrace();
		}
    	
    	return 1;
    	
    }
	public List getVolumeUnit(String shipNumber)
	{
	    List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( coalesce(unit2,' ') is null , '' , coalesce(unit2,' ') ) from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
	}
	public List getWeightUnit(String shipNumber) 
	{
		List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( coalesce(unit1,' ') is null , '', coalesce(unit1,' ') )  from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
	}
	
	
	String sailingDate;
	public List searchForwarding(String containerNo, String bookingNo, String corpid, String blNumber, Date sailDate,String lastName,String vesselFilight,String caed){
		  List <Object> forwardingList = new ArrayList<Object>();
		  
		  List list = new ArrayList();
		  
		  String queryString = "select distinct s.shipnumber, s.lastname, s.billtoname, c.containernumber, sp.booknumber, sp.blnumber, s.id, s.coordinator, sp.etDepart, st.beginLoad,sp.carrierVessels,sp.caed from serviceorder s left outer join  container c on c.serviceorderid=s.id and c.status = true  left outer join servicepartner sp on sp.serviceorderid = s.id and sp.status=true left outer join trackingstatus st on st.id = s.id where ";
		  
		  String queryStringConNo="";
		  String queryStringBookNo="";
		  String queryStringBlNumber="";
		  String queryStringSailDate="";
		  String queryStringCorpID="";
		  String queryStringLastName="";
		  String vesselFilightQuery="";
		  String caedQuery="";
		  if(containerNo.trim().length()>0){
			  if(containerNo.equals("-")){
				  queryStringConNo = "c.containernumber like '%"+containerNo.replaceAll("'", "''").replaceAll(":", "''")+"%' and ";
			  }else{
			  queryStringConNo = "replace(replace(c.containernumber,'-',''),' ','') like '%"+containerNo.replaceAll("'", "''").replaceAll(":", "''").replaceAll("-","").replaceAll(" ", "")+"%' and ";
			  }	
		  }
		  if(bookingNo.trim().length()>0){
			  queryStringBookNo = "sp.booknumber like '%"+bookingNo.replaceAll("'", "''").replaceAll(":", "''")+"%' and ";
		  }
		  if(blNumber.trim().length()>0){
			  queryStringBlNumber = "sp.blnumber like '%"+blNumber.replaceAll("'", "''").replaceAll(":", "''")+"%' and ";
		  }
		  if(lastName.trim().length()>0){
			  queryStringLastName="s.lastname like '%"+lastName.replaceAll("'", "''").replaceAll(":", "''")+"%' and  ";
		  }
		  if(vesselFilight != null && vesselFilight.trim().length()>0){
			  vesselFilightQuery = " sp.carrierVessels like '%"+vesselFilight.replaceAll("'", "''").replaceAll(":", "''")+"%' and s.status not in ('CNCL','CLSD','DLVR') and ";
		  }
		  if(sailDate != null){
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	          StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(sailDate) );
	          sailingDate = nowYYYYMMDD2.toString();
	          
	          queryStringSailDate = " sp.etDepart >='"+sailingDate+"' and "; 
		  }
		  if(caed.trim().length()>0){
			  caedQuery = "sp.caed like '%"+caed.replaceAll("'", "''").replaceAll(":", "''")+"%' and ";
		  }
		  queryStringCorpID = "s.corpid = '"+corpid+"' and controlflag='C' and (moveType='BookedMove' or moveType ='' or moveType is NULL)";
		  
		  String forwardingQuery = new StringBuilder(queryString).append(queryStringConNo).append(queryStringBookNo).append(queryStringBlNumber).append(queryStringLastName).append(queryStringSailDate).append(vesselFilightQuery).append(caedQuery).append(queryStringCorpID).toString();
			  
		  list = this.getSession().createSQLQuery(forwardingQuery)
		  .addScalar("s.shipnumber", Hibernate.STRING)
		  .addScalar("s.lastname", Hibernate.STRING)
		  .addScalar("s.billtoname", Hibernate.STRING)
		  .addScalar("c.containernumber", Hibernate.STRING)
		  .addScalar("sp.booknumber", Hibernate.STRING)
		  .addScalar("sp.blnumber", Hibernate.STRING)
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.coordinator",Hibernate.STRING)
		  .addScalar("sp.etDepart", Hibernate.DATE)
		  .addScalar("st.beginLoad", Hibernate.DATE)
		  .addScalar("sp.carrierVessels", Hibernate.STRING)
		  .addScalar("sp.caed",Hibernate.STRING)
		  .list();
		  
		  Set<String> set = new HashSet<String>();
		  Iterator it=list.iterator();
		  while(it.hasNext()){
			  Object [] row=(Object[])it.next();
			  DTO dto=new DTO();
			  dto.setShipNumber(row[0]);
			  dto.setLastName(row[1]);
			  dto.setBillToName(row[2]);
			  dto.setContainerNumber(row[3]);
			  dto.setBookNumber(row[4]);
			  dto.setBlNumber(row[5]);
			  dto.setId(row[6]);
			  dto.setCoordinator(row[7]);
			  dto.setEtDepart(row[8]);
			  dto.setBeginLoad(row[9]);
			  dto.setCarrierVessels(row[10]);
			  dto.setCaed(row[11]);
			  if(dto.getCarrierVessels() != null && dto.getCarrierVessels().toString().trim().length() > 0){
				  if(!set.contains(dto.getShipNumber().toString()+dto.getCarrierVessels().toString().trim())){
					  forwardingList.add(dto);
					  set.add(dto.getShipNumber().toString()+dto.getCarrierVessels().toString().trim());
				  }
			  }else{
				  forwardingList.add(dto);
			  }
		  }
		  return forwardingList;
	}
	
	public  class DTO{
		private Object shipNumber;
	  	private Object lastName;
		private Object billToName; 
		private Object containerNumber;
		private Object bookNumber; 
		private Object blNumber;
		private Object id;
		private Object coordinator;
		private Object etDepart;
		private Object beginLoad;
		private Object carrierVessels;
		private Object caed;
		
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getContainerNumber() {
			return containerNumber;
		}
		public void setContainerNumber(Object containerNumber) {
			this.containerNumber = containerNumber;
		}
		public Object getBookNumber() {
			return bookNumber;
		}
		public void setBookNumber(Object bookNumber) {
			this.bookNumber = bookNumber;
		}
		public Object getBlNumber() {
			return blNumber;
		}
		public void setBlNumber(Object blNumber) {
			this.blNumber = blNumber;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getCoordinator() {
			return coordinator;
		}
		public void setCoordinator(Object coordinator) {
			this.coordinator = coordinator;
		}
		public Object getBeginLoad() {
			return beginLoad;
		}
		public void setBeginLoad(Object beginLoad) {
			this.beginLoad = beginLoad;
		}
		public Object getEtDepart() {
			return etDepart;
		}
		public void setEtDepart(Object etDepart) {
			this.etDepart = etDepart;
		}
		public Object getCarrierVessels() {
			return carrierVessels;
		}
		public void setCarrierVessels(Object carrierVessels) {
			this.carrierVessels = carrierVessels;
		}
		public Object getCaed() {
			return caed;
		}
		public void setCaed(Object caed) {
			this.caed = caed;
		}
	}
	
	public List refreshWeights(String shipNum, String packingMode, String sessionCorpID){
		String cntnrnum = "select distinct c.cntnrnumber from carton c, serviceorder s where s.id=c.serviceorderid and  c.shipnumber = '"+shipNum+"' and s.packingMode='LVCO' and c.corpID = '"+sessionCorpID+"' and c.status = true  ";
		List listCntnrnum= this.getSession().createSQLQuery(cntnrnum).list();
		return listCntnrnum;	
	}
	
	public List  containerListOtherCorpId(Long sid){
		return getSession().createSQLQuery("select id from container where serviceOrderId='"+sid+"'").list();
	}
	
	public int refreshContainer(String shipNum, String containerNos, String sessionCorpID,String userName){
		List soList =new ArrayList();
		ServiceOrder serviceOrder = new ServiceOrder();
		List linkedShipNumberList= new ArrayList();
		String cntnrnumber = "select distinct ca.cntnrnumber from carton ca, serviceorder s where ca.status =true and  s.id=ca.serviceorderid and  ca.shipnumber = '"+shipNum+"' and s.packingMode='LVCO' and ca.corpID = '"+sessionCorpID+"' ";
		List listCntnrnumber= this.getSession().createSQLQuery(cntnrnumber).list();
		int i=0;
		String cntnrnum =  "select ca.cntnrnumber, sum(ca.grossweight), sum(ca.netweight), sum(ca.volume), count(*), sum(ca.grossweightKilo), sum(ca.netweightKilo), sum(ca.volumeCbm) from carton ca where ca.status =true and  ca.shipnumber = '"+shipNum+"'group by ca.cntnrnumber";
		List listCntnrnum = this.getSession().createSQLQuery(cntnrnum).list();
		if(listCntnrnum.isEmpty()){
			
		}else{
			Iterator it=listCntnrnum.iterator();
			 while(it.hasNext())
		       {
		           Object []row= (Object [])it.next(); 
		           //System.out.println("in while  loop row[0]"+row[0]);
		            i= getHibernateTemplate().bulkUpdate("update Container  set grossWeight = '"+row[1]+"', netWeight = '"+row[2]+"', volume ='"+row[3]+"', pieces= '"+row[4]+"' ,grossweightKilo= '"+row[5]+"', netweightKilo= '"+row[6]+"', volumeCbm= '"+row[7]+"'  where containerNumber = '"+row[0]+"' and corpID='"+sessionCorpID+"' and shipnumber='"+shipNum+"' ");
		            //System.out.println("in while  loop number of line updated "+i);
		       }
		}
		
		String query1 = ("from ServiceOrder where  shipnumber='"+shipNum+"'");
		try{
			soList = getHibernateTemplate().find(query1);
			if(!soList.isEmpty()){
				 serviceOrder = (ServiceOrder)soList.get(0);
			}
		}catch(Exception e){
			e.getStackTrace();
		}
		
	   	try{
	        if(serviceOrder.getIsNetworkRecord()){		        	
	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	        	}else{
	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	        	}		        	
	        	
				Iterator  it=linkedShipNumberList.iterator();
		    	while(it.hasNext()){			    			
		    		String shipNum1=(String)it.next();
		    		String corpId = shipNum1.substring(0, 4);
		    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
		    			if(listCntnrnum.isEmpty()){		    				
		    			}else{
		    				Iterator itr=listCntnrnum.iterator();
		    				 while(itr.hasNext()){
		    					 Object []row= (Object [])itr.next(); 
				           //System.out.println("in while  loop row[0]"+row[0]);
				            String updateQuery="update container  set grossWeight = '"+row[1]+"', netWeight = '"+row[2]+"', volume ='"+row[3]+"', pieces= '"+row[4]+"' ,grossweightKilo= '"+row[5]+"', netweightKilo= '"+row[6]+"', volumeCbm= '"+row[7]+"' ,updatedOn=now(),updatedby='"+sessionCorpID+":"+userName+"'   where containerNumber = '"+row[0]+"' and corpID='"+corpId+"' and shipnumber='"+shipNum1+"' ";
				            i=this.getSession().createSQLQuery(updateQuery).executeUpdate();
		    				}
		    			}
		    		}
		    	}
	        }
	        }catch(Exception e){
				e.getStackTrace();
			}
		
		return i;
	}
	public int updateDeleteStatus(Long ids){
			return getHibernateTemplate().bulkUpdate("delete Container where id='"+ids+"' or containerId='"+ids+"'");
	}
	public List containerList(Long serviceOrderId){
		return getHibernateTemplate().find("from Container where serviceOrderId='"+serviceOrderId+"'");
	}
	public List containerAuditList(Long serviceOrderId){
		//return this.getSession().createSQLQuery("select * from container where serviceOrderId='"+serviceOrderId+"' and status = 'Delete' ").list();
		return getHibernateTemplate().find("from Container where serviceOrderId='"+serviceOrderId+"'");
	}
	
	public List getGrossKilo(String shipNumber){
		List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(grossWeightKilo, 0)) is null , 0.00 , sum(coalesce(grossWeightKilo, 0)) ) from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
	}
    public List getTareKilo(String shipNumber)
    {
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(emptyContWeightKilo,0)) is null , 0.00 , sum(coalesce(emptyContWeightKilo,0)) )  from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getNetKilo(String shipNumber){
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(netWeightKilo,0)) is null , 0.00 , sum(coalesce(netWeightKilo,0)) ) from container where status = true and  shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public List getVolumeCbm(String shipNumber){
    	List L1 = new ArrayList();
    	L1 = this.getSession().createSQLQuery("select if( sum(coalesce(volumeCbm, 0)) is null, 0.00 , sum(coalesce(volumeCbm, 0)) )  from container where status = true and   shipNumber='"+shipNumber+"'").list();
    	return L1;
    }
    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
    	 
    	String unitQuery="";
    	String unit ="";
		List l =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+sessionCorpID+"'").list();
		if(l!=null && !l.isEmpty() && l.get(0)!=null && (!(l.get(0).toString().trim().equals("")))){
			unit = l.get(0).toString();
		}
		if(unit !=null && unit.trim().equalsIgnoreCase("YES")){
			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
		}
    	
    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualGrossWeightKilo='" +actualGrossWeightKilo+ "' , actualNetWeightKilo='" +actualNetWeightKilo+ "' , actualTareWeightKilo='"+actualTareWeightKilo+"' " +unitQuery+ " ,updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
    
    	 ServiceOrder serviceOrder = new ServiceOrder();
     	List linkedShipNumberList= new ArrayList();
     	List soList =new ArrayList();
     	
 		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
 		try{
 			soList = getHibernateTemplate().find(query1);
 			if(!soList.isEmpty()){
 				 serviceOrder = (ServiceOrder)soList.get(0);
 			}
 		}catch(Exception e){
 			e.getStackTrace();
 		}
 		
 	   	try{
 	        if(serviceOrder.getIsNetworkRecord()){		        	
 	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
 	        	}else{
 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
 	        	}		        	
 	        	
 				Iterator  it=linkedShipNumberList.iterator();
 		    	while(it.hasNext()){			    			
 		    		String shipNum=(String)it.next();
 		    		String corpId = shipNum.substring(0, 4);
 		    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
 		    			unitQuery="";
 		    			String unit1 ="";
		        		List list =  this.getSession().createSQLQuery("select unitVariable from systemdefault where corpid='"+corpId+"'").list();
		        		if(list!=null && !list.isEmpty() && list.get(0)!=null && (!(list.get(0).toString().trim().equals("")))){
		        			unit1 = list.get(0).toString();
		        		}
		        		if(unit1 !=null && unit1.trim().equalsIgnoreCase("YES")){
		        			unitQuery=" ,unit1='"+weightUnit+"',unit2='"+volumeUnit+"'  ";
		        		}
 		    			
 		    			String cartonContainerUpdateQuery1="update miscellaneous set actualGrossWeightKilo='" +actualGrossWeightKilo+ "' , actualNetWeightKilo='" +actualNetWeightKilo+ "' , actualTareWeightKilo='"+actualTareWeightKilo+"' "+unitQuery+" ,updatedBy='"+sessionCorpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
 		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
 		    		}
 		    	}
 	        }
 	   	}catch(Exception ex){
 			System.out.println("\n\n\n\n error while syn old data"+ex);
 			ex.printStackTrace();
 		}
 	   	return 1;
    
    }
	String getAllChildId(String grpId, String sessionCorpId){
		String query="select id from serviceorder where grpId= "+grpId+" and corpID='"+sessionCorpId+"'";
		List list=this.getSession().createSQLQuery(query).list();
		String childList="";
		Iterator it =list.iterator();
		while(it.hasNext()){
			String childId=it.next().toString();
			if(childList.equalsIgnoreCase(""))
			{
				childList=childId.trim();
			}else{
				childList=childList+","+childId.trim();
			}
		}
		return childList;
	}
	List weightOfOrdersToGrouped(String idList,String corpid){
		List temp=new ArrayList();
		String query="select"+
		" concat"+
		"("+
		" sum(if(actualCubicFeet is null or actualCubicFeet=0,"+
		" if(netActualCubicFeet is null or netActualCubicFeet=0,"+
		" if(estimateCubicFeet is null or estimateCubicFeet=0,"+
		" if(netEstimateCubicFeet is null or netEstimateCubicFeet=0,"+
		" '0',"+
		" netEstimateCubicFeet),"+
		" estimateCubicFeet),"+
		" netActualCubicFeet),"+
		" actualCubicFeet)),"+
		" '#',"+
		" sum(if(actualCubicMtr is null or actualCubicMtr=0,"+
		" if(netActualCubicMtr is null or netActualCubicMtr=0,"+
		" if(estimateCubicMtr is null or estimateCubicMtr=0,"+
		" if(netEstimateCubicMtr is null or netEstimateCubicMtr=0,"+
		" '0',"+
		" netEstimateCubicMtr),"+
		" estimateCubicMtr),"+
		" netActualCubicMtr),"+
		" actualCubicMtr))"+
		" )"+
		" from miscellaneous where id in("+idList+")";		
		try{
			if(!idList.equalsIgnoreCase(""))
			{
				temp=this.getSession().createSQLQuery(query).list();
			}else{
				temp=null;
			}
		}
		catch(Exception e){
		}
		return temp;
	}
    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy)
    {
    	getHibernateTemplate().bulkUpdate("update Miscellaneous set actualCubicMtr='" +actualCubicMtr+ "',netActualCubicMtr='" +actualCubicMtr+ "',updatedBy='"+updatedBy+"',updatedOn=now() where shipNumber=?",shipNumber);
    	List al=this.getSession().createSQLQuery("select concat(if(grpPref is true,1,0),'#',if(grpStatus is null or grpStatus='',' ',grpStatus),'#',if(grpId is null or grpId='',' ',grpId)) as rec from serviceorder where shipNumber='"+shipNumber+"' and corpId='"+corpID+"'").list();
    	if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
    	{
    		String[] temp=al.get(0).toString().split("#");
	            if((temp[0]!=null)&&(!temp[0].equals(""))&&(temp[0].equalsIgnoreCase("1")))
	            {
	            	if((temp[2].trim()!=null)&&(!temp[2].trim().equals(""))&&(!temp[1].trim().equalsIgnoreCase("Finalized")))
	            	{
	            		try{
	            			String childIdList=getAllChildId(temp[2].trim().toString(),corpID);
	            			if(!childIdList.equalsIgnoreCase(""))
	            			{
	            				List newordersList=weightOfOrdersToGrouped(childIdList,corpID);
	            				if(newordersList!=null && !(newordersList.isEmpty()) && newordersList.get(0)!=null){
	            					 String tempWeight=newordersList.get(0).toString();
	            					 String[] str=tempWeight.split("#");
	            					 if(!str[1].equalsIgnoreCase("0")){
	            				    	this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr='"+str[1]+"' where id='"+temp[2].toString()+"'").executeUpdate();
	            					 }else{
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicMtr=0 where id='"+temp[2].toString()+"'").executeUpdate();           						 
	            					 }
	            					 if(!str[0].equalsIgnoreCase("0")){
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet='" +str[0]+ "' where id='"+temp[2].toString()+"'").executeUpdate();
	            					 }else{
	            						 this.getSession().createSQLQuery("update miscellaneous set estimateCubicFeet=0 where id='"+temp[2].toString()+"'").executeUpdate();            						 
	            					 }
	            				}
	            			}
	            			
	            		}catch(Exception e){}
	            	}
	            }            
    	}
    	
    	ServiceOrder serviceOrder = new ServiceOrder();
     	List linkedShipNumberList= new ArrayList();
     	List soList =new ArrayList();
     	
 		String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
 		try{
 			soList = getHibernateTemplate().find(query1);
 			if(!soList.isEmpty()){
 				 serviceOrder = (ServiceOrder)soList.get(0);
 			}
 		}catch(Exception e){
 			e.getStackTrace();
 		}
 		
 	   	try{
 	        if(serviceOrder.getIsNetworkRecord()){		        	
 	        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
 	        	}else{
 	        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
 	        	}		        	
 	        	
 				Iterator  it=linkedShipNumberList.iterator();
 		    	while(it.hasNext()){			    			
 		    		String shipNum=(String)it.next();
 		    		String corpId = shipNum.substring(0, 4);
 		    		if(!corpId.equalsIgnoreCase(corpID)){
 		    			String cartonContainerUpdateQuery1="update miscellaneous set actualCubicMtr='" +actualCubicMtr+ "',netActualCubicMtr='" +actualCubicMtr+ "',updatedBy='"+corpID+":"+updatedBy+"',updatedOn=now() where shipNumber='"+shipNum+"' and corpId='"+corpId+"'";
 		    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
 		    		}
 		    	}
 	        }
 	   	}catch(Exception ex){
 			System.out.println("\n\n\n\n error while syn old data"+ex);
 			ex.printStackTrace();
 		}
    	return 1;
    	
    }
    public List findMaximumChild(String shipNm) {
    	return getHibernateTemplate().find("select max(id) from Container where shipNumber=?", shipNm);
    }
    public List findMinimumChild(String shipNm){
    	return getHibernateTemplate().find("select min(id) from Container where shipNumber=?", shipNm);
    }
    public List findCountChild(String shipNm){
    	return getHibernateTemplate().find("select count(*) from Container where shipNumber=?", shipNm);
    }
	public List goSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("from  Container where status = true and  serviceOrderId='"+serviceOrderId+"'  and id !='"+id+"' and corpID='"+corpID+"'");
	}
	public List goNextSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  Container where status = true and  serviceOrderId='"+serviceOrderId+"' and id >'"+id+"' and corpID='"+corpID+"' order by 1 ");
	}
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  Container where status = true and  serviceOrderId='"+serviceOrderId+"' and id <'"+id+"' and corpID='"+corpID+"' order by 1 desc");
	}
	public Long findRemoteContainer(String idNumber, Long serviceOrderID) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from container where  idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderID+"").list().get(0).toString());
	}
	public List findRemoteContainerList(String shipNumber) {
		return this.getSession().createSQLQuery("select id from container where   shipNumber='"+shipNumber+"'").list();
	}
	public void deleteNetworkContainer(Long serviceOrderID, String idNumber,String containerStatus) {
		this.getSession().createSQLQuery("update container set status ="+containerStatus+" where idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderID+"").executeUpdate();		
	}
	public List findByServiceOrderId(Long sofid, String sessionCorpID){
		return this.getSession().createSQLQuery("select id from container where status = true and  serviceOrderId='"+sofid+"'and corpID='"+sessionCorpID+"' ").list();
	}
	public List fieldsInfoById(Long grpId, String sessionCorpID){
		//return getHibernateTemplate().find("select concat(c.shipNumber,'#',ifNULL(c.containerNumber,''),'#',ifNULL(c.sealNumber,''),'#', ifNULL(c.customSeal,''),'#',ifNULL(c.sealDate,''),'#',ifNULL(c.size,''))  from Container c  inner join ServiceOrder s  on s.shipNumber=c.shipNumber where s.grpID='"+grpId+"'");
		return this.getSession().createSQLQuery("select concat(c.shipNumber,'#',ifNULL(c.containerNumber,''),'#',ifNULL(c.sealNumber,''),'#', ifNULL(c.customSeal,''),'#',ifNULL(c.sealDate,''),'#',ifNULL(c.size,''))  from container c  inner join serviceorder s  on s.shipNumber=c.shipNumber where s.grpID='"+grpId+"' and c.status = true ").list();
	}
	
	public List containerListByGrpId(Long grpId, Long contId){
		return this.getSession().createSQLQuery("select c.id from container c  inner join serviceorder s  on s.shipNumber=c.shipNumber where c.containerId='"+contId+"' and c.status = true ").list();
	}
	
	public List containerListByIdAndgrpId(Long grpId){
		
		return this.getSession().createSQLQuery("select c.id from container c  inner join serviceorder s  on s.shipNumber=c.shipNumber where ( s.grpID='"+grpId+"' or s.id='"+grpId+"') and c.status = true ").list();
	}
	public List orderIdWithNoContainer(String newIdList, String sessionCorpId){
		return this.getSession().createSQLQuery("select  s.id  from  serviceorder s left outer join container c on s.id=c.serviceorderId and c.status = true  where s.corpId='"+sessionCorpId+"' and (c.containerId is null or c.containerId='' or c.containerId='0' ) and s.id in("+newIdList+")").list();
	}
	
    public List containersForSizeUpdate(String draftOrdersId){
		
		return this.getSession().createSQLQuery("select c.id from container c  inner join serviceorder s  on s.shipNumber=c.shipNumber where (s.grpID in("+draftOrdersId+") or s.id in("+draftOrdersId+")) and c.status = true ").list();
	}
    
    public List containerListOfMaster(Long containerId){
    	
    		return getHibernateTemplate().find("from Container where containerId='"+containerId+"'");

    	}
    
    public List containerWithNoLine(String sessionCorpId,Long grpId,Long masterContainerId){
    	//return this.getSession().createSQLQuery("select  s.id  from  serviceorder s left outer join servicepartner sp on s.id=sp.serviceorderId where s.corpId='"+sessionCorpId+"' and sp.serviceorderId is null  and s.grpId ='"+grpId+"'").list();
    	
    	String temShips="";
    	List listOfChildOrders=new ArrayList();
    	List<ServicePartner> listOfContainers=new ArrayList();
    	StringBuilder formatShipNos=new StringBuilder("");
    	List listOfIdWithNoLine=new ArrayList();
    	try{
    	listOfChildOrders=getHibernateTemplate().find("select shipNumber from ServiceOrder where grpId=?",grpId);
    	if(listOfChildOrders!=null && !listOfChildOrders.isEmpty()){
    		Iterator itt=listOfChildOrders.iterator();
    		while(itt.hasNext()){
    			formatShipNos=formatShipNos.append(",").append("'").append(itt.next()).append("'");
    			
    			
    		}
    		 temShips=formatShipNos.substring(1,formatShipNos.length());
    		 listOfContainers=getHibernateTemplate().find("select shipNumber from Container where  containerId =?",masterContainerId);
    	}
    	if(listOfContainers!=null && !(listOfContainers.isEmpty())){
    		Iterator listIter=listOfContainers.iterator();
    		while(listIter.hasNext()){
    			String temp=listIter.next().toString();
    		//	if(temp.getServicePartnerId()!=)
    			
    			 if(temShips.indexOf(temp)>-1){
    				 temShips=temShips.replace(temp,"");
    				 temShips=temShips.replace("''","");
    				 temShips=temShips.replaceAll(",,",",");
    				    int len=temShips.length()-1;
    				   
    				    if(len==temShips.lastIndexOf(",") && len > -1){
    				    	temShips=temShips.substring(0,len);
    				    
    				    }
    				    if(temShips.indexOf(",")==0){
    				    	temShips=temShips.substring(1,temShips.length());
    				    
    				    }
    			
    		}
    		
    	}
    	
    	
    }
    	if(temShips!=null && !temShips.equalsIgnoreCase("")){
    		listOfIdWithNoLine=getHibernateTemplate().find("select id from ServiceOrder where shipNumber in ("+temShips+")");
    	}
    	}catch (Exception e) {
			System.out.print("error-------------->"+e);
		}
    	
    	return listOfIdWithNoLine;
}
    public void updateContainerId(Long mastercontainerid, String oldIdList){
    	getHibernateTemplate().bulkUpdate("update Container set containerid=null where containerId!=null and serviceOrderId in("+oldIdList+")");
    }
	
	 public List getExistingContainerList(String sessionCorpId,String shipNumber,Long masterContainerId){
		 return getHibernateTemplate().find("select id from Container where status = true and  shipNumber='"+shipNumber+"' and containerId='"+masterContainerId+"' and corpID='"+sessionCorpId+"'");
	 }
	public List getGrossNetwork(String shipNumber, String corpID) {
		return	this.getSession().createSQLQuery("select if( sum(coalesce(grossWeight,0)) is null , 0.00 , sum(coalesce(grossWeight,0)) ) from container where status = true and  shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
	}
	public List getNetNetwork(String shipNumber, String corpID) { 
		return	this.getSession().createSQLQuery("select if( sum(coalesce(netWeight,0)) is null , 0.00 , sum(coalesce(netWeight,0)) )  from container where status = true and   shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
	}
	public List getPiecesNetwork(String shipNumber, String corpID) { 
		return	this.getSession().createSQLQuery("select if( sum(coalesce(pieces,0)) is null , 0, sum(coalesce(pieces,0)) ) from container where status = true and   shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
	}
	public List getVolumeNetwork(String shipNumber, String corpID) { 
		return	this.getSession().createSQLQuery("select if( sum(coalesce(volume,0)) is null , 0.00 , sum(coalesce(volume,0)) )  from container where status = true and   shipNumber='"+shipNumber+"' and corpID='"+corpID+"'").list();
	}
	private ServiceOrder serviceOrder;
	private ContainerAction containerAction;
	public void updateContainerDetails(Long id,String fieldName,String fieldValue,String tableName,String sessionCorpID,String shipNumber, String userName){
	List listCntnrnumber =new ArrayList();
	List soList =new ArrayList();
	ServiceOrder serviceOrder = new ServiceOrder();
	List linkedShipNumberList= new ArrayList();
		String containerNum="";
		if((fieldName!=null && !fieldName.equalsIgnoreCase("")) && fieldName.equalsIgnoreCase("containerNumber")){
			String cntrnNumberQuery = "select containerNumber from container where id='"+id+"' ";
			listCntnrnumber= this.getSession().createSQLQuery(cntrnNumberQuery).list();
			if(listCntnrnumber!=null && !listCntnrnumber.isEmpty() && listCntnrnumber.get(0)!=null){
				containerNum=listCntnrnumber.get(0).toString();
			}
		}
		String query ="";
		if((fieldName!=null && !fieldName.equalsIgnoreCase("")) && fieldName.equalsIgnoreCase("transhipped")){
			if (fieldValue.equalsIgnoreCase("true") || fieldValue.equalsIgnoreCase("false")) {
				boolean fieldValue1 = Boolean.valueOf(fieldValue);
				query="update "+tableName+" set "+fieldName+"="+fieldValue1+",updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
			}else{		
				query="update "+tableName+" set "+fieldName+"='"+fieldValue+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
			}
		}else{		
				query="update "+tableName+" set "+fieldName+"='"+fieldValue+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		}
		getHibernateTemplate().bulkUpdate(query);
		if(fieldName.equalsIgnoreCase("containerNumber") && !containerNum.equalsIgnoreCase("")){
			String cartonContainerUpdateQuery="update Carton set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+userName+"' where shipNumber='"+shipNumber+"' and cntnrNumber='"+containerNum+"'  and corpId='"+sessionCorpID+"'";
			getHibernateTemplate().bulkUpdate(cartonContainerUpdateQuery);
			String vehicleContainerUpdateQuery="update Vehicle set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+userName+"' where shipNumber='"+shipNumber+"' and cntnrNumber='"+containerNum+"'  and corpId='"+sessionCorpID+"'";
			getHibernateTemplate().bulkUpdate(vehicleContainerUpdateQuery);
			String servicePartnerContainerUpdateQuery="update ServicePartner set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+userName+"' where shipNumber='"+shipNumber+"' and cntnrNumber='"+containerNum+"'  and corpId='"+sessionCorpID+"'";
			getHibernateTemplate().bulkUpdate(servicePartnerContainerUpdateQuery);				
			
			
			String query1 = ("from ServiceOrder where  shipnumber='"+shipNumber+"'");
			try{
				soList = getHibernateTemplate().find(query1);
				if(!soList.isEmpty()){
					 serviceOrder = (ServiceOrder)soList.get(0);
				}
			}catch(Exception e){
				e.getStackTrace();
			}
			
		   	try{
		        if(serviceOrder.getIsNetworkRecord()){		        	
		        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
		        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		        	}else{
		        		linkedShipNumberList=getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		        	}		        	
		        	
					Iterator  it=linkedShipNumberList.iterator();
			    	while(it.hasNext()){			    			
			    		String shipNum=(String)it.next();
			    		String corpId = shipNum.substring(0, 4);
			    		if(!corpId.equalsIgnoreCase(sessionCorpID)){
			    			String cartonContainerUpdateQuery1="update carton set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+sessionCorpID+":"+userName+"' where shipNumber='"+shipNum+"' and cntnrNumber='"+containerNum+"'  and corpId='"+corpId+"'";
			    			this.getSession().createSQLQuery(cartonContainerUpdateQuery1).executeUpdate();
			    			String vehicleContainerUpdateQuery1="update vehicle set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+sessionCorpID+":"+userName+"' where shipNumber='"+shipNum+"' and cntnrNumber='"+containerNum+"'  and corpId='"+corpId+"'";
			    			this.getSession().createSQLQuery(vehicleContainerUpdateQuery1).executeUpdate();
			    			String servicePartnerContainerUpdateQuery1="update servicepartner set cntnrNumber='"+fieldValue+"',updatedOn=now(),updatedby='"+sessionCorpID+":"+userName+"' where shipNumber='"+shipNum+"' and cntnrNumber='"+containerNum+"'  and corpId='"+corpId+"'";
			    			this.getSession().createSQLQuery(servicePartnerContainerUpdateQuery1).executeUpdate();
			    			}
			    		}
			    	}					
		        }catch(Exception ex){
	    			System.out.println("\n\n\n\n error while syn old data"+ex);
	    			ex.printStackTrace();
	    		}
			
		}
	}
	
	public void updateContainerDetailsAllFields(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"',"+fieldGrossWeight+"='"+fieldGrossWeightVal+"',"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"',"+fieldGrossWeightKilo+"='"+fieldGrossWeightKiloVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"'";
		getHibernateTemplate().bulkUpdate(query);
	}
	public void updateContainerDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"' ,"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"' ,"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	public void updateContainerDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	public void updateContainerDetailsNetWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"',"+fieldGrossWeight+"='"+fieldGrossWeightVal+"',"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"',"+fieldGrossWeightKilo+"='"+fieldGrossWeightKiloVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolume+"='"+fieldVolumeVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	public void updateContainerTareWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldNetWeight+"='"+fieldNetWeightVal+"' ,"+fieldNetWeightKilo+"='"+fieldNetWeightKiloVal+"' ,"+fieldEmptyConWeight+"='"+fieldEmptyConWeightVal+"',"+fieldemptyContWeightKilo+"='"+fieldemptyContWeightKiloVal+"',"+fieldVolume+"='"+fieldVolumeVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	public void updateContainerDetailsDensityMetric(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID, String userName){
		String query="update "+tableName+" set "+fieldVolume+"='"+fieldVolumeVal+"',"+fieldVolumeCbm+"='"+fieldVolumeCbmVal+"',"+fieldDensity+"='"+fieldDensityVal+"',"+fieldDensityMetric+"='"+fieldDensityMetricVal+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
		
	public List getLinkedShipNumber(String shipNumber, String recordType) {
		try {
			List linkedShipNumberList= new ArrayList();
			if(recordType.equalsIgnoreCase("Primary")){
				List linkedList= this.getSession().createSQLQuery("select concat(IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber='"+shipNumber+"'").list();
				List reloLinkedList=this.getSession().createSQLQuery("select concat(IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,''),'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber='"+shipNumber+"'").list();
				if(linkedList!=null && !linkedList.isEmpty() && linkedList.get(0)!= null){
					String listString=linkedList.get(0).toString();
					String [] listArray=listString.split("#");
					for(String sNumber:listArray){
						if(!sNumber.trim().equalsIgnoreCase("")){
							linkedShipNumberList.add(sNumber);	
						}
					
					}  
				}
				if(reloLinkedList!=null && !reloLinkedList.isEmpty() && reloLinkedList.get(0)!= null){
					String listReloString=reloLinkedList.get(0).toString();
					String [] listReloArray=listReloString.split("#");
					for(String sNumber:listReloArray){
						if(!sNumber.trim().equalsIgnoreCase("")){
							linkedShipNumberList.add(sNumber);	
						}
					}
				}
				if(linkedShipNumberList != null && (!linkedShipNumberList.isEmpty())){
					String linkShipNumLst = "'"+linkedShipNumberList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "','")+"'";
					List linkedListSub=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber in ("+linkShipNumLst+")").list();
					List reloLinkedListSub=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,''),'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber in ("+linkShipNumLst+")").list();
					if(linkedListSub!=null && !linkedListSub.isEmpty() && linkedListSub.get(0)!= null){
						String listStringSub=linkedListSub.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
						String [] listArraySub=listStringSub.split("#");
						for(String sNumber:listArraySub){
							if(!sNumber.trim().equalsIgnoreCase("")){
								linkedShipNumberList.add(sNumber);	
							} 
						}  
					}
					
					if(reloLinkedListSub!=null && !reloLinkedListSub.isEmpty() && reloLinkedListSub.get(0)!= null){
						String listReloStringSub=reloLinkedListSub.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
						String [] listReloArraySub=listReloStringSub.split("#");
						for(String sNumber:listReloArraySub){
							if(!sNumber.trim().equalsIgnoreCase("")){
								linkedShipNumberList.add(sNumber);	
							}
						}
					}
				}
				
			}if(recordType.equalsIgnoreCase("Secondary")){
				List linkedShipNumber= this.getSession().createSQLQuery("select bookingAgentShipNumber from serviceorder where shipNumber='"+shipNumber+"'").list();
				List linkedShipNumber1= this.getSession().createSQLQuery("select bookingAgentShipNumber from serviceorder where shipNumber='"+linkedShipNumber.get(0).toString()+"'").list();
				List linkedList=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(networkAgentExSO,''),'#', IFNULL(originAgentExSO,''),'#',IFNULL(originSubAgentExSO,''),'#',IFNULL(destinationAgentExSO,''),'#',IFNULL(destinationSubAgentExSO,''),'#',IFNULL(bookingAgentExSO,'')) from trackingstatus where shipNumber in ('"+linkedShipNumber.get(0).toString()+"','"+shipNumber+"')").list();
				List reloLinkedList=this.getSession().createSQLQuery("select concat(shipNumber,'#',IFNULL(CAR_vendorCodeEXSO,''),'#', IFNULL(COL_vendorCodeEXSO,''),'#',IFNULL(TRG_vendorCodeEXSO,''),'#',IFNULL(HOM_vendorCodeEXSO,''),'#',IFNULL(RNT_vendorCodeEXSO,''),'#',IFNULL(LAN_vendorCodeEXSO,''),'#',IFNULL(MMG_vendorCodeEXSO,''),'#',IFNULL(ONG_vendorCodeEXSO,''),'#',IFNULL(PRV_vendorCodeEXSO,''),'#',IFNULL(AIO_vendorCodeEXSO,''),'#',IFNULL(EXP_vendorCodeEXSO,''),'#',IFNULL(RPT_vendorCodeEXSO,''),'#',IFNULL(SCH_vendorCodeEXSO,''),'#',IFNULL(TAX_vendorCodeEXSO,''),'#',IFNULL(TAC_vendorCodeEXSO,''),'#',IFNULL(TEN_vendorCodeEXSO,''),'#',IFNULL(VIS_vendorCodeEXSO,''),'#',IFNULL(SET_vendorCodeEXSO,''),'#',IFNULL(WOP_vendorCodeEXSO,'') ,'#',IFNULL(REP_vendorCodeEXSO,'') ,'#',IFNULL(RLS_vendorCodeEXSO,'') ,'#',IFNULL(CAT_vendorCodeEXSO,''),'#',IFNULL(CLS_vendorCodeEXSO,'') ,'#',IFNULL(CHS_vendorCodeEXSO,'') ,'#',IFNULL(DPS_vendorCodeEXSO,'') ,'#',IFNULL(HSM_vendorCodeEXSO,'') ,'#',IFNULL(PDT_vendorCodeEXSO,'') ,'#',IFNULL(RCP_vendorCodeEXSO,'') ,'#',IFNULL(SPA_vendorCodeEXSO,'') ,'#',IFNULL(TCS_vendorCodeEXSO,'') ,'#',IFNULL(MTS_vendorCodeEXSO,'') ,'#',IFNULL(DSS_vendorCodeEXSO,''),'#',IFNULL(FRL_vendorCodeEXSO,'') ,'#',IFNULL(APU_vendorCodeEXSO,'')) from dspdetails where shipNumber in ('"+linkedShipNumber.get(0).toString()+"','"+shipNumber+"')").list();
				
				if(linkedList!=null && !linkedList.isEmpty() && linkedList.get(0)!= null){
					String listString=linkedList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
					//String listString=linkedList.get(0).toString();
					
					String [] listArray=listString.split("#");
					if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && linkedShipNumber.get(0)!= null)
						linkedShipNumberList.add(linkedShipNumber.get(0).toString());
				
					if(linkedShipNumber1!=null && !linkedShipNumber1.isEmpty() && linkedShipNumber1.get(0)!= null)
						linkedShipNumberList.add(linkedShipNumber1.get(0).toString());	
					
						for(String sNumber:listArray){
							if(!sNumber.trim().equalsIgnoreCase("")){
								linkedShipNumberList.add(sNumber);	
							}
						}
					
				}
				if(reloLinkedList!=null && !reloLinkedList.isEmpty() && reloLinkedList.get(0)!= null){
					String listReloString=reloLinkedList.toString().replace("[", "").replace("]", "").replaceAll(" ", "").replaceAll(",", "#");
					// String listReloString=reloLinkedList.get(0).toString();
					String [] listReloArray=listReloString.split("#");
					for(String sNumber:listReloArray){
						if(!sNumber.trim().equalsIgnoreCase("")){
							linkedShipNumberList.add(sNumber);	
						}
						
					} 
				}
			}
			HashSet hs = new HashSet();
			hs.addAll(linkedShipNumberList);
			linkedShipNumberList.clear();
			linkedShipNumberList.addAll(hs);
			linkedShipNumberList.remove("");
			linkedShipNumberList.remove("OrderDecline");
			return linkedShipNumberList;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();	    	
		}
	return null;
	}
	
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void updateStatus(Long id,String containerStatus, String updateRecords) {
		getHibernateTemplate().bulkUpdate("update Container  set status ="+containerStatus+" where id="+id+" ");
		if(updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll")){
			getHibernateTemplate().bulkUpdate("update Container  set status ="+containerStatus+" where containerId="+id+" ");	
		}
		
	}	
	
	public String findContainerNumber(String corpId,String shipNumber){
		List list;
		String containerNumber="";
		list = this.getSession().createSQLQuery("select  containerNumber from container where shipNumber='"+shipNumber+"' and corpId='"+corpId+"' and status is true limit 1").list();
		if(list!=null && (!(list.isEmpty())) && (list.get(0)!=null)){
			containerNumber=list.get(0).toString();
		}
		return containerNumber;
	}
	
	public List findServicePartnerData(String corpId,String shipNumber){
		List list;
		List<Object> returnList=new ArrayList<Object>();
		String query="select a.etDepart,b.etArrival from"
				+ " (SELECT etDepart,shipNumber,corpId  from servicepartner"
				+ " where shipNumber='"+shipNumber+"' and status is true and corpid='"+corpId+"' order by id asc limit 1) a"
				+ " inner join (SELECT etArrival,shipNumber,corpId  from servicepartner"
				+ " where shipNumber='"+shipNumber+"' and status is true and corpid='"+corpId+"' order by id desc limit 1) b"
				+ " on a.shipNumber=b.shipNumber and a.corpid=b.corpId";
		list = this.getSession().createSQLQuery(query).list();
		 Iterator it=list.iterator();
		 DTOforSoQuickView dto=null;
		  while(it.hasNext()) {
			  Object [] row=(Object[])it.next();
			  dto=new DTOforSoQuickView();
			  if(row[0] == null){
					dto.setEdDepart("");
				}else{
					dto.setEdDepart(row[0]);
				}
			  if(row[1] == null){
					dto.setEtArrival("");
				}else{
					dto.setEtArrival(row[1]);
				}
			  returnList.add(dto);
		  }
		  
		if(returnList==null || returnList.size()<0){
			returnList=new ArrayList();
		}
		return returnList;
		
	}
	
	public class DTOforSoQuickView{
		private Object edDepart;
		private Object etArrival;
		public Object getEdDepart() {
			return edDepart;
		}
		public void setEdDepart(Object edDepart) {
			this.edDepart = edDepart;
		}
		public Object getEtArrival() {
			return etArrival;
		}
		public void setEtArrival(Object etArrival) {
			this.etArrival = etArrival;
		}
	}
	
	
}

//End of Class. 