package com.trilasoft.app.model;
import java.util.Date;
import java.util.Set;

import org.appfuse.model.BaseObject;   

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;   
import javax.persistence.FetchType;
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="claimlossticket")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class ClaimLossTicket  extends BaseObject
{
	private String corpID;
	private Long id;
	private Long claimNumber;
	private String shipNumber;
	private String sequenceNumber;
	private String lossNumber;
	private String percent;
	private String chargeBack;
	private Long ticket;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Boolean isAppliedToAllLoss;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("claimNumber", claimNumber).append("shipNumber",
				shipNumber).append("sequenceNumber", sequenceNumber).append(
				"lossNumber", lossNumber).append("percent", percent).append(
				"chargeBack", chargeBack).append("ticket", ticket).append(
				"createdBy", createdBy).append("createdOn", createdOn).append(
				"updatedBy", updatedBy).append("updatedOn", updatedOn).append(
				"isAppliedToAllLoss", isAppliedToAllLoss).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ClaimLossTicket))
			return false;
		ClaimLossTicket castOther = (ClaimLossTicket) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(claimNumber, castOther.claimNumber)
				.append(shipNumber, castOther.shipNumber).append(
						sequenceNumber, castOther.sequenceNumber).append(
						lossNumber, castOther.lossNumber).append(percent,
						castOther.percent).append(chargeBack,
						castOther.chargeBack).append(ticket, castOther.ticket)
				.append(createdBy, castOther.createdBy).append(createdOn,
						castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(isAppliedToAllLoss,
						castOther.isAppliedToAllLoss).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				claimNumber).append(shipNumber).append(sequenceNumber).append(
				lossNumber).append(percent).append(chargeBack).append(ticket)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(isAppliedToAllLoss).toHashCode();
	}
	
	
	@Column(length=6)
	public String getChargeBack() {
		return chargeBack;
	}
	public void setChargeBack(String chargeBack) {
		this.chargeBack = chargeBack;
	}
	
	@Column(length=10)
	public Long getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=10)
	public String getLossNumber() {
		return lossNumber;
	}
	public void setLossNumber(String lossNumber) {
		this.lossNumber = lossNumber;
	}
	@Column(length=6)
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column
	public Boolean getIsAppliedToAllLoss() {
		return isAppliedToAllLoss;
	}
	public void setIsAppliedToAllLoss(Boolean isAppliedToAllLoss) {
		this.isAppliedToAllLoss = isAppliedToAllLoss;
	}
	
	
}
