package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SubcontractorCharges;

public interface SubcontractorChargesDao extends GenericDao<SubcontractorCharges,Long>{
	
	public List findMaxId();
	public List<SubcontractorCharges> findById(Long long1);
	public List<SubcontractorCharges> findAllRecords();
	public List<SubcontractorCharges> registrationNo(String regNo);
	public List findBySysDefault(String corpID);
	public List findBranchList(String corpID);
	public List getsubcontrCompanyCode(String corpID);
	public List getSubContcExtract(String subcontrCompanyCode,String corpid, String postDate, String startInvoiceDates, String endInvoiceDates);
	public List findGlCodeForExtract(String personId,String corpid); 
	public List getSubContcExtractlevel1(String personId ,String corpid,String subcontrCompanyCode);
	public List getSubContcExtractlevel4(String personId ,String corpid,String glCode,String subcontrCompanyCode); 
	public List getSubContcNivExtractlevel1(String personId ,String corpid,String subcontrCompanyCode);
	public List getSubContcNivExtractlevel4(String personId ,String corpid,String glCode,String subcontrCompanyCode);
	public int subContcFileUpdate(String batchName,String subContcPersonId,String corpid);
	public List getNonsettledAmount(String subContcPersonId,String corpid);
	public List findServiceOrder(String regNo);
	public List findRegNo(String serviceOrder);
	public List findMiscellaneousStatus(String serviceOrder);
	public List<SubcontractorCharges> findAmountDesc(String serviceOrder, String corpId,String driver);
	public List getgpSubContcExtract(String sessionCorpID);
	public int gpSubContcFileUpdate(String batchName, String subIdList, String sessionCorpID);
	public List getCompanyCode(String agentCode, String sessionCorpID);
	public List getSubContcExtractHoll(String subcontrCompanyCode,String corpid,String postDate);
	public List getAccountInvoicePostDataList(String corpId);
	public List getSubcontrCompanyCodeDataList(String sessionCorpID);
	public List getSubcontrACCCompanyCodeDataList(String sessionCorpID);
	public List getMergeSubcontrACCCompanyCodeDataList(String sessionCorpID);
	public List getSubContcMergeExtract(String subcontrCompanyCode,String corpid, String postDate, String startInvoiceDates, String endInvoiceDates);
	public List getSSCWGPSubcontrACCCompanyCodeDataList(String sessionCorpID);
	public List getSubContcSSCWGPExtract(String subcontrCompanyCode,String corpid, String postDate, String startInvoiceDates, String endInvoiceDates);
	public int subContcSSCWGPFileUpdate(String batchName, String subIdList, String sessionCorpID);
	}
