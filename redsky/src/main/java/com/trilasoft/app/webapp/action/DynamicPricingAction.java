package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.DynamicPricing;
import com.trilasoft.app.model.DynamicPricingCalender;
import com.trilasoft.app.model.HolidayMaintenance;
import com.trilasoft.app.service.DynamicPricingManager;
import com.trilasoft.app.service.HolidayMaintenanceManager;
import com.trilasoft.app.service.RefMasterManager;

public class DynamicPricingAction extends BaseAction implements Preparable {
	private Long id;	
	private DynamicPricing dynamicPricing;
	private DynamicPricingManager dynamicPricingManager;
	private HolidayMaintenanceManager holidayMaintenanceManager; 
	private RefMasterManager refMasterManager;
	private String sessionCorpID;
	private List dynamicPricingList;
	private String dynamicPricingCategoryType;
	private String holiDayDate;
	private String holiDayRate;
	private Boolean edmSummer;
	private Boolean edmWinter;
	private Boolean calSummer;
	private Boolean calWinter;
	private List dynamicPricingCalenderList;
	private List dynamicPricingCalenderList1;
	 private  Map<String, String> dynamicPricingCategory;
	public void prepare() throws Exception { 
		
	}
	static final Logger logger = Logger.getLogger(DynamicPricingAction.class);
	public DynamicPricingAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	private Long hid;
	@SkipValidation
	public String deleteHolidayMaintenceAjax(){
		holidayMaintenanceManager.remove(hid);
		return SUCCESS;
	}
	@SkipValidation
	public String updateHolidayMaintenceAjax(){
		if((holiDayRate==null)||(holiDayRate.equalsIgnoreCase(""))){
			holiDayRate="0.00";
		}
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date hDayDate=null;
		try{
			hDayDate=dfm.parse(holiDayDate);
		}catch(Exception e){}
		String str="";
		if(dynamicPricingCategoryType.equalsIgnoreCase("EDMSUMMER")){
			str="EDM";
		}else if(dynamicPricingCategoryType.equalsIgnoreCase("EDMWINTER")){
			str="EDM";
		}else if(dynamicPricingCategoryType.equalsIgnoreCase("CALSUMMER")){
			str="CAL";
		}else if(dynamicPricingCategoryType.equalsIgnoreCase("CALWINTER")){
			str="CAL";
		}else{
		}
		List<HolidayMaintenance> al=holidayMaintenanceManager.checkHoliDayList(str,hDayDate,sessionCorpID);
		HolidayMaintenance holidayMaintenance=null;
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
			for(HolidayMaintenance holidayMaintenanceTemp:al){
				holidayMaintenance=holidayMaintenanceTemp;
			}
		}
		if(holidayMaintenance==null){
			holidayMaintenance=new HolidayMaintenance();
			holidayMaintenance.setCreatedBy(getRequest().getRemoteUser());
			holidayMaintenance.setCreatedOn(new Date());
		}
		holidayMaintenance.setCorpID(sessionCorpID);
		holidayMaintenance.setUpdatedBy(getRequest().getRemoteUser());
		holidayMaintenance.setUpdatedOn(new Date());			
		holidayMaintenance.setHoliDayDate(hDayDate);
		holidayMaintenance.setHoliDayBranch(str);
		holidayMaintenance.setHoliDayRate(Double.parseDouble(holiDayRate));
		holidayMaintenanceManager.save(holidayMaintenance);
		return SUCCESS;
	}
	public String dynamicPricingCall(){
		dynamicPricingCategory=refMasterManager.findByParameter(sessionCorpID, "DYNAMICPRICINGCATEGORY");
		if(dynamicPricingCategoryType==null || dynamicPricingCategoryType.equalsIgnoreCase("")){
			dynamicPricingCategoryType="EDMSUMMER";
		}
		dynamicPricing=dynamicPricingManager.getDynamicPricingByType(dynamicPricingCategoryType,sessionCorpID);
		if(dynamicPricing==null){
			dynamicPricing=new DynamicPricing();
			dynamicPricing.setCorpID(sessionCorpID);
			dynamicPricing.setCreatedBy(getRequest().getRemoteUser());
			dynamicPricing.setCreatedOn(new Date());
			dynamicPricing.setUpdatedBy(getRequest().getRemoteUser());
			dynamicPricing.setUpdatedOn(new Date());	
			dynamicPricing.setDynamicPricingType(dynamicPricingCategoryType);
		}
		dynamicPricingList=holidayMaintenanceManager.getHoliDayMaintenanceList(sessionCorpID);
		return SUCCESS;
	}
	private String men2;
	private String men3;
	private String men4;
	private String extraMen;
	private Double type1Temp=new Double(0.00);
	private Double type2Temp=new Double(0.00);
	@SkipValidation
	public String pricingCalendarFullView(){
		
		return SUCCESS;
	}
	private String dynamicPricingCategoryTypeView;
	public String dynamicPricingCalenderView(){
		dynamicPricingCategory=refMasterManager.findByParameter(sessionCorpID, "DYNAMICPRICINGCATEGORY");
		String type1="";
		String type2="";
		if(dynamicPricingCategoryTypeView!=null && dynamicPricingCategoryTypeView.equalsIgnoreCase("Edmonton")){
			type1="EDMSUMMER";
			type2="EDMWINTER";
			dynamicPricingCategoryTypeView="Edmonton";
		}else if(dynamicPricingCategoryTypeView!=null && dynamicPricingCategoryTypeView.equalsIgnoreCase("Calgary")){
			type1="CALSUMMER";
			type2="CALWINTER";
			dynamicPricingCategoryTypeView="Calgary";
		}else{
			type1="EDMSUMMER";
			type2="EDMWINTER";
			dynamicPricingCategoryTypeView="Edmonton";
		}
		dynamicPricingCalenderList=dynamicPricingManager.getDynamicPricingCalenderByType(type1, sessionCorpID);
		dynamicPricingCalenderList1=dynamicPricingManager.getDynamicPricingCalenderByType(type2, sessionCorpID);
		if((dynamicPricingCalenderList!=null)&&(!dynamicPricingCalenderList.isEmpty())&&(dynamicPricingCalenderList.get(0)!=null)){
			DynamicPricingCalender dpc=(DynamicPricingCalender)dynamicPricingCalenderList.get(0);
			Calendar date = Calendar.getInstance();
			date.setTime(dpc.getCurrentDate());
			date.add(Calendar.DATE, -1); 
			SimpleDateFormat myFormat1 = new SimpleDateFormat("yyyy-MM-dd");
			lowestCurrentDate=myFormat1.format(date.getTime());
		}else{
			lowestCurrentDate="2017-01-01";
		}
		if((dynamicPricingCalenderList1!=null)&&(!dynamicPricingCalenderList1.isEmpty())&&(dynamicPricingCalenderList1.get(0)!=null)){
			DynamicPricingCalender dpc=(DynamicPricingCalender)dynamicPricingCalenderList1.get(0);
			Calendar date = Calendar.getInstance();
			date.setTime(dpc.getCurrentDate());
			date.add(Calendar.DATE, -1); 
			SimpleDateFormat myFormat1 = new SimpleDateFormat("yyyy-MM-dd");
			lowestCurrentDate1=myFormat1.format(date.getTime());
		}else{
			lowestCurrentDate1="2017-01-01";
		}
		List <DynamicPricing>dynamicPricingList=dynamicPricingManager.getAll();
		Map<String,Double> extraMenRateMap=new LinkedHashMap<String, Double>();
		for(DynamicPricing dp:dynamicPricingList){
			extraMenRateMap.put(dp.getDynamicPricingType(), dp.getExtraMenRate());
		}
		type1Temp=extraMenRateMap.get(type1);
		type2Temp=extraMenRateMap.get(type2);
		return SUCCESS;
	}
	private String lowestCurrentDate;
	private String lowestCurrentDate1;
	public String save() throws Exception {
		dynamicPricingCategory=refMasterManager.findByParameter(sessionCorpID, "DYNAMICPRICINGCATEGORY");
		dynamicPricingList=holidayMaintenanceManager.getHoliDayMaintenanceList(sessionCorpID);
		boolean isNew = (dynamicPricing.getId() == null);  
				if (isNew) { 
					dynamicPricing.setCreatedOn(new Date());
					dynamicPricing.setCreatedBy(getRequest().getRemoteUser());
		        }
				dynamicPricing.setUpdatedOn(new Date());
				dynamicPricing.setUpdatedBy(getRequest().getRemoteUser()); 
				dynamicPricing.setCorpID(sessionCorpID);
				dynamicPricing=dynamicPricingManager.save(dynamicPricing); 
				if((edmSummer)||(edmWinter)||(calSummer)||(calWinter)){
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
					beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					DynamicPricing dynamicPricingTemp=null;
					if(edmSummer){
						dynamicPricingTemp=dynamicPricingManager.getDynamicPricingByType("EDMSUMMER",sessionCorpID);
						Long id1=null;
						if(dynamicPricingTemp==null){
							dynamicPricingTemp=new DynamicPricing();
						}else{
							id1=dynamicPricingTemp.getId();
						}
						try {
							beanUtilsBean.copyProperties(dynamicPricingTemp, dynamicPricing); 
							if(id1!=null){
								dynamicPricingTemp.setId(id1);
							}else{
								dynamicPricingTemp.setId(null);
							}
							dynamicPricingTemp.setDynamicPricingType("EDMSUMMER");
							dynamicPricingManager.save(dynamicPricingTemp);
						}catch(Exception w){w.printStackTrace();}

					}
					if(edmWinter){
						dynamicPricingTemp=dynamicPricingManager.getDynamicPricingByType("EDMWINTER",sessionCorpID);
						Long id1=null;
						if(dynamicPricingTemp==null){
							dynamicPricingTemp=new DynamicPricing();
						}else{
							id1=dynamicPricingTemp.getId();
						}
						try {
							beanUtilsBean.copyProperties(dynamicPricingTemp, dynamicPricing); 
							if(id1!=null){
								dynamicPricingTemp.setId(id1);
							}else{
								dynamicPricingTemp.setId(null);
							}
							dynamicPricingTemp.setDynamicPricingType("EDMWINTER");
							dynamicPricingManager.save(dynamicPricingTemp);
						}catch(Exception w){w.printStackTrace();}

					}
					if(calSummer){
						dynamicPricingTemp=dynamicPricingManager.getDynamicPricingByType("CALSUMMER",sessionCorpID);
						Long id1=null;
						if(dynamicPricingTemp==null){
							dynamicPricingTemp=new DynamicPricing();
						}else{
							id1=dynamicPricingTemp.getId();
						}
						try {
							beanUtilsBean.copyProperties(dynamicPricingTemp, dynamicPricing); 
							if(id1!=null){
								dynamicPricingTemp.setId(id1);
							}else{
								dynamicPricingTemp.setId(null);
							}
							dynamicPricingTemp.setDynamicPricingType("CALSUMMER");
							dynamicPricingManager.save(dynamicPricingTemp);
						}catch(Exception w){w.printStackTrace();}

					}
					if(calWinter){
						dynamicPricingTemp=dynamicPricingManager.getDynamicPricingByType("CALWINTER",sessionCorpID);
						Long id1=null;
						if(dynamicPricingTemp==null){
							dynamicPricingTemp=new DynamicPricing();
						}else{
							id1=dynamicPricingTemp.getId();
						}
						try {
							beanUtilsBean.copyProperties(dynamicPricingTemp, dynamicPricing); 
							if(id1!=null){
								dynamicPricingTemp.setId(id1);
							}else{
								dynamicPricingTemp.setId(null);
							}
							dynamicPricingTemp.setDynamicPricingType("CALWINTER");
							dynamicPricingManager.save(dynamicPricingTemp);
						}catch(Exception w){w.printStackTrace();}

					}
				}
				String colorType=dynamicPricing.getDynamicPricingType();
				if((colorType==null)||(colorType.equalsIgnoreCase(""))){
					dynamicPricingCategoryType="EDMSUMMER";
				}else if(colorType.equalsIgnoreCase("EDMSUMMER")){
					dynamicPricingCategoryType="EDMSUMMER";
				}else if(colorType.equalsIgnoreCase("EDMWINTER")){
					dynamicPricingCategoryType="EDMWINTER";
				}else if(colorType.equalsIgnoreCase("CALSUMMER")){
					dynamicPricingCategoryType="CALSUMMER";
				}else if(colorType.equalsIgnoreCase("CALWINTER")){
					dynamicPricingCategoryType="CALWINTER";
				}else{
				}
		    	String key = (isNew) ? "dynamicPricing.added" : "dynamicPricing.updated";
			    saveMessage(getText(key));
				return SUCCESS;
	     }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DynamicPricing getDynamicPricing() {
		return dynamicPricing;
	}
	public void setDynamicPricing(DynamicPricing dynamicPricing) {
		this.dynamicPricing = dynamicPricing;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getDynamicPricingList() {
		return dynamicPricingList;
	}
	public void setDynamicPricingList(List dynamicPricingList) {
		this.dynamicPricingList = dynamicPricingList;
	}
	public void setDynamicPricingManager(DynamicPricingManager dynamicPricingManager) {
		this.dynamicPricingManager = dynamicPricingManager;
	}
	public void setHolidayMaintenanceManager(
			HolidayMaintenanceManager holidayMaintenanceManager) {
		this.holidayMaintenanceManager = holidayMaintenanceManager;
	}
	public String getDynamicPricingCategoryType() {
		return dynamicPricingCategoryType;
	}
	public void setDynamicPricingCategoryType(String dynamicPricingCategoryType) {
		this.dynamicPricingCategoryType = dynamicPricingCategoryType;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getDynamicPricingCategory() {
		return dynamicPricingCategory;
	}
	public void setDynamicPricingCategory(Map<String, String> dynamicPricingCategory) {
		this.dynamicPricingCategory = dynamicPricingCategory;
	}
	public String getHoliDayDate() {
		return holiDayDate;
	}
	public void setHoliDayDate(String holiDayDate) {
		this.holiDayDate = holiDayDate;
	}
	public String getHoliDayRate() {
		return holiDayRate;
	}
	public void setHoliDayRate(String holiDayRate) {
		this.holiDayRate = holiDayRate;
	}
	public Boolean getEdmSummer() {
		return edmSummer;
	}
	public void setEdmSummer(Boolean edmSummer) {
		this.edmSummer = edmSummer;
	}
	public Boolean getEdmWinter() {
		return edmWinter;
	}
	public void setEdmWinter(Boolean edmWinter) {
		this.edmWinter = edmWinter;
	}
	public Boolean getCalSummer() {
		return calSummer;
	}
	public void setCalSummer(Boolean calSummer) {
		this.calSummer = calSummer;
	}
	public Boolean getCalWinter() {
		return calWinter;
	}
	public void setCalWinter(Boolean calWinter) {
		this.calWinter = calWinter;
	}
	public List getDynamicPricingCalenderList() {
		return dynamicPricingCalenderList;
	}
	public void setDynamicPricingCalenderList(List dynamicPricingCalenderList) {
		this.dynamicPricingCalenderList = dynamicPricingCalenderList;
	}
	public String getLowestCurrentDate() {
		return lowestCurrentDate;
	}
	public void setLowestCurrentDate(String lowestCurrentDate) {
		this.lowestCurrentDate = lowestCurrentDate;
	}
	public Long getHid() {
		return hid;
	}
	public void setHid(Long hid) {
		this.hid = hid;
	}
	public String getDynamicPricingCategoryTypeView() {
		return dynamicPricingCategoryTypeView;
	}
	public void setDynamicPricingCategoryTypeView(
			String dynamicPricingCategoryTypeView) {
		this.dynamicPricingCategoryTypeView = dynamicPricingCategoryTypeView;
	}
	public List getDynamicPricingCalenderList1() {
		return dynamicPricingCalenderList1;
	}
	public void setDynamicPricingCalenderList1(List dynamicPricingCalenderList1) {
		this.dynamicPricingCalenderList1 = dynamicPricingCalenderList1;
	}
	public String getLowestCurrentDate1() {
		return lowestCurrentDate1;
	}
	public void setLowestCurrentDate1(String lowestCurrentDate1) {
		this.lowestCurrentDate1 = lowestCurrentDate1;
	}
	public String getMen2() {
		return men2;
	}
	public void setMen2(String men2) {
		this.men2 = men2;
	}
	public String getMen3() {
		return men3;
	}
	public void setMen3(String men3) {
		this.men3 = men3;
	}
	public String getMen4() {
		return men4;
	}
	public void setMen4(String men4) {
		this.men4 = men4;
	}
	public String getExtraMen() {
		return extraMen;
	}
	public void setExtraMen(String extraMen) {
		this.extraMen = extraMen;
	}
	public Double getType1Temp() {
		return type1Temp;
	}
	public void setType1Temp(Double type1Temp) {
		this.type1Temp = type1Temp;
	}
	public Double getType2Temp() {
		return type2Temp;
	}
	public void setType2Temp(Double type2Temp) {
		this.type2Temp = type2Temp;
	}
	
}
