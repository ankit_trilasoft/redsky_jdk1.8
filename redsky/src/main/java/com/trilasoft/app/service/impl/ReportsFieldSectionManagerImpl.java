package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ReportsFieldSectionDao;
import com.trilasoft.app.model.ReportsFieldSection;
import com.trilasoft.app.service.ReportsFieldSectionManager;

public class ReportsFieldSectionManagerImpl extends GenericManagerImpl<ReportsFieldSection, Long> implements ReportsFieldSectionManager {
	ReportsFieldSectionDao reportsFieldSectiondao; 
	public ReportsFieldSectionManagerImpl(ReportsFieldSectionDao reportsFieldSectiondao){
		super(reportsFieldSectiondao);   
        this.reportsFieldSectiondao = reportsFieldSectiondao; 
		
	}
	 
	public void saveReportFieldSection(ReportsFieldSection reportsFieldSection) {
		reportsFieldSectiondao.saveReportFieldSection(reportsFieldSection)	;
	}
	
	public List reportfieldSectionList(Long id, String sessionCorpID, String type){
		return reportsFieldSectiondao.reportfieldSectionList(id, sessionCorpID, type);
	}
	  
	public String updateReportsFieldSection(String fieldName,String fieldValue,String sessionCorpID, String type){
    	return reportsFieldSectiondao.updateReportsFieldSection(fieldName, fieldValue, sessionCorpID, type);
    }
	

}
