package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.EquipMaterialsLimitsDao;
import com.trilasoft.app.dao.OperationsResourceLimitsDao;
import com.trilasoft.app.model.EquipMaterialsLimits;
import com.trilasoft.app.model.OperationsResourceLimits;

public class EquipMaterialsLimitsDaoHibernate extends GenericDaoHibernate<EquipMaterialsLimits, Long> implements EquipMaterialsLimitsDao{
	public EquipMaterialsLimitsDaoHibernate() {
		super(EquipMaterialsLimits.class);
		
	}
	public List getEquipMaterialLimitList(String branch,String div,String cat){
		return getHibernateTemplate().find("from EquipMaterialsLimits where branch like '%"+branch+"%' and division like '%"+div+"%' and  category like '%"+cat+"%'");
	}
	public void saveEquipMaterialObject(EquipMaterialsLimits obj){
		//this.getSession().getTransaction().setTimeout(10);
		//this.getSession().getTransaction().begin();
		 getHibernateTemplate().saveOrUpdate(obj);
		 //this.getSession().getTransaction().commit();
	}
	public EquipMaterialsLimits getByDesc(String resource,String corpid){
		EquipMaterialsLimits equipMaterialsLimits=null;
		 List list=getHibernateTemplate().find("from EquipMaterialsLimits where resource = '"+resource+"' and corpId='"+corpid+"'");
		  if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
			  equipMaterialsLimits=(EquipMaterialsLimits) list.get(0);
		  }
	return equipMaterialsLimits;
	}
	 public void updateEquipMaterialObject(EquipMaterialsLimits equipMaterialsLimits,String cngres,String cngwhouse){
		  if(cngwhouse.equals("true")){
			  this.getSession().createSQLQuery("update equipmaterialslimits set category='"+equipMaterialsLimits.getCategory()+"'  , maxResourceLimit="+equipMaterialsLimits.getMaxResourceLimit()+" , minResourceLimit="+equipMaterialsLimits.getMinResourceLimit()+" , qty="+equipMaterialsLimits.getQty()+" , division='"+equipMaterialsLimits.getDivision()+"',branch='"+equipMaterialsLimits.getBranch()+"' , updatedOn=now() where id="+equipMaterialsLimits.getId()).executeUpdate(); 
		  }else {
			  this.getSession().createSQLQuery("update equipmaterialslimits set category='"+equipMaterialsLimits.getCategory()+"'  , maxResourceLimit="+equipMaterialsLimits.getMaxResourceLimit()+" , minResourceLimit="+equipMaterialsLimits.getMinResourceLimit()+" , qty="+equipMaterialsLimits.getQty()+"  ,division='"+equipMaterialsLimits.getDivision()+"', updatedOn=now()  where id="+equipMaterialsLimits.getId()).executeUpdate();
		  }
		  if(cngres.equals("true")){
			  this.getSession().createSQLQuery("update equipmaterialslimits set category='"+equipMaterialsLimits.getCategory()+"' , resource='"+equipMaterialsLimits.getResource()+"' , maxResourceLimit="+equipMaterialsLimits.getMaxResourceLimit()+" , minResourceLimit="+equipMaterialsLimits.getMinResourceLimit()+" ,division='"+equipMaterialsLimits.getDivision()+"', qty="+equipMaterialsLimits.getQty()+"  , updatedOn=now()  where id="+equipMaterialsLimits.getId()).executeUpdate(); 
		  }else {
			  this.getSession().createSQLQuery("update equipmaterialslimits set category='"+equipMaterialsLimits.getCategory()+"'  , maxResourceLimit="+equipMaterialsLimits.getMaxResourceLimit()+" , minResourceLimit="+equipMaterialsLimits.getMinResourceLimit()+" , qty="+equipMaterialsLimits.getQty()+"  ,division='"+equipMaterialsLimits.getDivision()+"' ,updatedOn=now()  where id="+equipMaterialsLimits.getId()).executeUpdate();
		  }
	  }
	 
	 public Map<String,String> getCompanyDivisionMap(String corpid){
		 Map<String, String> parameterMap = new LinkedHashMap<String, String>();		 
		 List list=this.getSession().createSQLQuery("select companyCode,description from companydivision where corpid='"+corpid+"'").list();
		 Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				if(row[0]!=null && row[1]!=null){
				parameterMap.put(((String) row[0]),	((String) row[1]));
				}
			}
			return parameterMap;
		  }	
	
}
