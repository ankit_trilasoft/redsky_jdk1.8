package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ProfileInfo;

public interface ProfileInfoDao extends GenericDao<ProfileInfo, Long> { 
	
	public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode);

}
