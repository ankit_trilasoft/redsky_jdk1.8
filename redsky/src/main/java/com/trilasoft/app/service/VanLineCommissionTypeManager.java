package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.VanLineCommissionType;

public interface VanLineCommissionTypeManager extends GenericManager<VanLineCommissionType, Long> {

	public List<VanLineCommissionType> vanLineCommissionTypeList();

	public List<VanLineCommissionType> searchVanLineCommissionType(String code, String type, String description, String glCode);
	
	public List isExisted(String code, String corpId);
	
	public List<VanLineCommissionType> getGLCodePopupList(String chargesGl);
	
	public List glList(String chargesGl);

	public List searchVanLineCommissionPopup(String code, String type, String description, String glCode);

}