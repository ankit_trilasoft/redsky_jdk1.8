package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.UserDataSecurity;

public interface UserDataSecurityDao extends GenericDao<UserDataSecurity, Long>{
	public List findById(Long id);
}
