<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>


<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript">
	function calculate(Unit1,Unit2) {
    var volumeUnitCbm = document.forms['refCrateForm'].elements['refCrate.unit2'].value;
    var weightUnitKgs = document.forms['refCrateForm'].elements['refCrate.unit3'].value ;
	var factor = 0;
	if(volumeUnitCbm == 'Inches')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0005787;
		}
		else
		{
			factor = 0.0000164;
		}
	}
	if(volumeUnitCbm == 'Ft')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 1;
		}
		else
		{
			factor = 0.02834;
		}
	}
	if(volumeUnitCbm == 'Cm')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 0.0000353;
		}
		else
		{
			factor = 0.000001;
		}
	}
	if(volumeUnitCbm == 'Mtr')
	{
		if(weightUnitKgs == 'Cft')
		{
			factor = 35.288;
		}
		else
		{
			factor = 1;
		}
	}
		var density = (document.forms['refCrateForm'].elements['refCrate.width'].value * document.forms['refCrateForm'].elements['refCrate.height'].value * document.forms['refCrateForm'].elements['refCrate.length'].value );
		document.forms['refCrateForm'].elements['refCrate.volume'].value = Math.round(density*factor*1000)/1000;
		var volume = document.forms['refCrateForm'].elements['refCrate.volume'].value; 
	 }


function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        targetElement.value="0";
	        return false;
        }
    }
    return true;
}

function unitControl(weightUnit,volumeUnit)
{

	var volumeUnitCbm = document.forms['refCrateForm'].elements['refCrate.unit3'].value;
    var weightUnitKgs = document.forms['refCrateForm'].elements['refCrate.unit1'].value;
    if(weightUnitKgs=='Lbs'){
    if(volumeUnitCbm=='Cft'){
    return true;
    }
    else{
    alert("Please select the appropriate unit to process. Like Lbs/Cft or Kgs/Cbm ");
    document.forms['refCrateForm'].elements['refCrate.unit3'].value='Cft';
    return false;
    }
    }
    else{
    if(volumeUnitCbm=='Cbm'){
    return true;
    }
    else{
    alert("Please select the appropriate unit to process. Like Lbs/Cft or Kgs/Cbm ");
    document.forms['refCrateForm'].elements['refCrate.unit3'].value='Cbm';
    return false;
    }
    }
}

</script>
  



<title><fmt:message key="refCrateDetail.title" /></title>
  <meta name="heading" content="<fmt:message key='refCrateDetail.heading'/>" />
	
</head>
<s:form name="refCrateForm" id="refCrateForm" action="saveRefCrate" method="post" validate="true">

	<s:hidden name="refCrate.id" value="%{refCrate.id}"/>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	
	<div id="layer4" style="width:80%;">
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Carton&nbsp;Detail<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
              <li><a href="refCrates.html"><span>Carton&nbsp;List</span></a></li>
      			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${refCrate.id}&tableName=refcrate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
         </ul>
        </div><div class="spn">&nbsp;</div>
     
      <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content"> 
	<table class="" cellspacing="1" cellpadding="0" border="0" style="width:610px">
		<tr> 
			<td class="listwhitetext" height="5px"></td>						
			</tr>
	

	  <tr>
							<td class="listwhitetext"  align="right" style="padding-bottom:3px;" ><fmt:message key='refCrate.cartonType'/></td>
							<td style="padding-bottom:5px;padding-left:3px" colspan="2"><s:textfield name="refCrate.cartonType" size="15" maxlength="15" cssClass="input-text" /></td>
							
						</tr>
						 <tr>
							<td class="listwhitetext"  align="right" style="padding-bottom:3px;width: 130px;" ><fmt:message key='refCrate.emptyContWeight'/></td>
							<td style="padding-bottom:5px;padding-left:3px;width: 60px;"><s:textfield name="refCrate.emptyContWeight" size="8" maxlength="6" cssClass="input-text" onchange="onlyFloat(this);" cssStyle="text-align:right"/></td>
							<td class="listwhitetext" style="padding-bottom:3px;" align="right" width="50px"><fmt:message key='refCrate.unit1'/></td>
							<td class="listwhitetext" align="left" style="padding-bottom:5px; padding-left:3px; width: 290px;"><s:select name="refCrate.unit1" list="%{weightunits}" cssClass="list-menu" cssStyle="width:70px"/></td>
				      	
						</tr>
						<tr>
	  	</tr>
				      	<tr>			
							<td class="listwhitetext"  align="right" style="padding-bottom:3px;"><fmt:message key='refCrate.length'/></td>
	 					    <td align="left" style="padding-bottom:5px;padding-left:3px"><s:textfield key="refCrate.length" size="8" maxlength="5" cssClass="input-text" onchange="onlyFloat(this);"  cssStyle="text-align:right" /></td>
					  	</tr> 
						<tr>
							<td class="listwhitetext"align="right" style="padding-bottom:3px;" ><fmt:message key='refCrate.width'/></td>
							<td align="left" style="padding-bottom:5px;padding-left:3px"><s:textfield key="refCrate.width"  size="8" maxlength="5" cssClass="input-text" onchange="onlyFloat(this);"  cssStyle="text-align:right"/></td>
						                    
						</tr>
						<tr>
							<td class="listwhitetext"  align="right" style="padding-bottom:3px;" ><fmt:message key='refCrate.height'/></td>
							<td style="padding-bottom:5px;padding-left:3px"><s:textfield name="refCrate.height" size="8" maxlength="5" cssClass="input-text" onchange="onlyFloat(this);"  cssStyle="text-align:right"/></td>
						<td class="listwhitetext" style="padding-bottom:3px;" align="right" width=""><fmt:message key='refCrate.unit2'/></td>
							<td class="listwhitetext" align="left" style="padding-bottom:5px; padding-left:3px"><s:select name="refCrate.unit2" list="%{lengthunits}" onchange="unitChangeVolume(this);" cssClass="list-menu" cssStyle="width:70px"/></td>
				
						</tr>
						<tr>
	  		      	</tr>

						<tr>
							<td class="listwhitetext"  align="right" style="padding-bottom:3px;" ><fmt:message key='refCrate.volume'/></td>
							<td style="padding-bottom:5px;padding-left:3px"><s:textfield name="refCrate.volume" size="8" maxlength="15" readonly="true" cssClass="input-text" onchange="onlyFloat(this);"  cssStyle="text-align:right"/></td>
							
						
	  	<td class="listwhitetext" style="padding-bottom:3px;" align="right" width=""><fmt:message key='refCrate.unit3'/></td>
							<td class="listwhitetext" align="left" style="padding-bottom:5px; padding-left:3px"><s:select name="refCrate.unit3" list="%{volumeunits}" cssClass="list-menu" cssStyle="width:70px"/></td>
				      	</tr>
						
	                     <tr>
	                     <td></td>
							<td><input type="button" class="cssbutton" style="width:70px; height:24px"  name="calc" value="Calculate" onclick="calculate('refCrate.unit2','refCrate.unit3');" onmouseover="return unitControl('refCrate.unit1','refCrate.unit3');" onselect="return unitControl('refCrate.unit1','refCrate.unit3');"/> 
						</td></tr>
	               <tr> 
			<td class="listwhitetext" height="5px"></td>						
			</tr>      

	   <tr>
		   <s:hidden name="refCrate.corpID"/>				
		
	
		</tr>
					
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
	
	<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${refCrate.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="refCrate.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${refCrate.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty refCrate.id}">
								<s:hidden name="refCrate.createdBy"/>
								<td ><s:label name="createdBy" value="%{refCrate.createdBy}"/></td>
							</c:if>
							<c:if test="${empty refCrate.id}">
								<s:hidden name="refCrate.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${refCrate.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="refCrate.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${refCrate.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty refCrate.id}">
							<s:hidden name="refCrate.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{refCrate.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty refCrate.id}">
							<s:hidden name="refCrate.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>	
			
	</div>
	<s:submit cssClass="cssbuttonA" method="save" key="button.save" onclick="return calculate('refCrate.unit1','refCrate.unit3');" onmouseover="return unitControl('refCrate.unit1','refCrate.unit3');" onkeypress="return unitControl('refCrate.unit1','refCrate.unit3');"
		cssStyle="width:55px; height:25px" tabindex="23"/>
	
</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("refCrateForm")); 
</script>


