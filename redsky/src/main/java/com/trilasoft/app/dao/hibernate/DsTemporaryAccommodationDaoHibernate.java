package com.trilasoft.app.dao.hibernate;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsTemporaryAccommodationDao;
import com.trilasoft.app.model.DsTemporaryAccommodation;

public class DsTemporaryAccommodationDaoHibernate extends GenericDaoHibernate<DsTemporaryAccommodation, Long> implements DsTemporaryAccommodationDao {
	
	public DsTemporaryAccommodationDaoHibernate(){
		super(DsTemporaryAccommodation.class);
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsTemporaryAccommodation where id = "+id+" ");
	}

	
}
