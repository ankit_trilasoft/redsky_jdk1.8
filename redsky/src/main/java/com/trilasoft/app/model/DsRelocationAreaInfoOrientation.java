package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

@Entity
@Table(name="dsrelocationareainfoorientation")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DsRelocationAreaInfoOrientation  extends BaseObject{
	private Long id;
	private Long serviceOrderId;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private Date flightDeparture;
	private Date flightArrival ;
	private Date initialDateOfServiceRequested;
	private Date providerNotificationSent ;
	private Date providerConfirmationReceived;
	private Date prearrivalDiscussionOfProviderAndFamily;
	private String airlineFlight;
	private String serviceAuthorized;
	private String contactInformation;
	private String vendorCode;
	private String vendorName;
	private String vendorContact;
	private String vendorEmail;
	private Date serviceStartDate;
	private Date serviceEndDate;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("flightDeparture", flightDeparture).append(
						"flightArrival", flightArrival).append(
						"initialDateOfServiceRequested",
						initialDateOfServiceRequested).append(
						"providerNotificationSent", providerNotificationSent)
				.append("providerConfirmationReceived",
						providerConfirmationReceived).append(
						"prearrivalDiscussionOfProviderAndFamily",
						prearrivalDiscussionOfProviderAndFamily).append(
						"airlineFlight", airlineFlight).append(
						"serviceAuthorized", serviceAuthorized).append(
						"contactInformation", contactInformation).append(
						"vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorContact", vendorContact)
				.append("vendorEmail", vendorEmail).append("serviceStartDate",
						serviceStartDate).append("serviceEndDate",
						serviceEndDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsRelocationAreaInfoOrientation))
			return false;
		DsRelocationAreaInfoOrientation castOther = (DsRelocationAreaInfoOrientation) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				serviceOrderId, castOther.serviceOrderId).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(flightDeparture,
						castOther.flightDeparture).append(flightArrival,
						castOther.flightArrival).append(
						initialDateOfServiceRequested,
						castOther.initialDateOfServiceRequested).append(
						providerNotificationSent,
						castOther.providerNotificationSent).append(
						providerConfirmationReceived,
						castOther.providerConfirmationReceived).append(
						prearrivalDiscussionOfProviderAndFamily,
						castOther.prearrivalDiscussionOfProviderAndFamily)
				.append(airlineFlight, castOther.airlineFlight).append(
						serviceAuthorized, castOther.serviceAuthorized).append(
						contactInformation, castOther.contactInformation)
				.append(vendorCode, castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorContact,
						castOther.vendorContact).append(vendorEmail,
						castOther.vendorEmail).append(serviceStartDate,
						castOther.serviceStartDate).append(serviceEndDate,
						castOther.serviceEndDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId).append(
				corpID).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(flightDeparture)
				.append(flightArrival).append(initialDateOfServiceRequested)
				.append(providerNotificationSent).append(
						providerConfirmationReceived).append(
						prearrivalDiscussionOfProviderAndFamily).append(
						airlineFlight).append(serviceAuthorized).append(
						contactInformation).append(vendorCode).append(
						vendorName).append(vendorContact).append(vendorEmail)
				.append(serviceStartDate).append(serviceEndDate).toHashCode();
	}
	@Column(length=25)
	public String getAirlineFlight() {
		return airlineFlight;
	}
	public void setAirlineFlight(String airlineFlight) {
		this.airlineFlight = airlineFlight;
	}
	@Column(length=100)
	public String getContactInformation() {
		return contactInformation;
	}
	public void setContactInformation(String contactInformation) {
		this.contactInformation = contactInformation;
	}
	@Column(length=30)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getFlightArrival() {
		return flightArrival;
	}
	public void setFlightArrival(Date flightArrival) {
		this.flightArrival = flightArrival;
	}
	@Column
	public Date getFlightDeparture() {
		return flightDeparture;
	}
	public void setFlightDeparture(Date flightDeparture) {
		this.flightDeparture = flightDeparture;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getInitialDateOfServiceRequested() {
		return initialDateOfServiceRequested;
	}
	public void setInitialDateOfServiceRequested(Date initialDateOfServiceRequested) {
		this.initialDateOfServiceRequested = initialDateOfServiceRequested;
	}
	@Column
	public Date getPrearrivalDiscussionOfProviderAndFamily() {
		return prearrivalDiscussionOfProviderAndFamily;
	}	
	public void setPrearrivalDiscussionOfProviderAndFamily(
			Date prearrivalDiscussionOfProviderAndFamily) {
		this.prearrivalDiscussionOfProviderAndFamily = prearrivalDiscussionOfProviderAndFamily;
	}
	@Column
	public Date getProviderConfirmationReceived() {
		return providerConfirmationReceived;
	}
	public void setProviderConfirmationReceived(Date providerConfirmationReceived) {
		this.providerConfirmationReceived = providerConfirmationReceived;
	}
	@Column
	public Date getProviderNotificationSent() {
		return providerNotificationSent;
	}
	public void setProviderNotificationSent(Date providerNotificationSent) {
		this.providerNotificationSent = providerNotificationSent;
	}
	@Column(length=10)
	public String getServiceAuthorized() {
		return serviceAuthorized;
	}
	public void setServiceAuthorized(String serviceAuthorized) {
		this.serviceAuthorized = serviceAuthorized;
	}
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column
	public Date getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	@Column(length=225)
	public String getVendorContact() {
		return vendorContact;
	}
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	
	@Column(length=65)
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	
	@Column(length=255)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
}
