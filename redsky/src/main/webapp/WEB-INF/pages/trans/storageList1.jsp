<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="storageList1.title"/></title> 
    <meta name="heading" content="<fmt:message key='storageList1.heading'/>"/> 
</head>
 <s:form id="serviceForm4">
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="content-nav">
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" />Service Order</a></li>
  <li><a href="editBilling.html?id=${serviceOrder.id}" >Billing Info</a></li>
  <li><a href="servicePartners.html?id=${serviceOrder.id}">Partner</a></li>
  <li class="current"><a href="containers.html?id=${serviceOrder.id}" >Container</a></li>
   
  <c:if test="${serviceOrder.job !='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}">Domestic</a></li>
  </c:if>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}">Status</a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}">Status</a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" >Ticket</a></li>
  <li><a href="claims.html?id=${serviceOrder.id}">Claim</a></li>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" >Customer File</a></li>
</ul>
</td></tr>
</tbody></table>
 
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="myDate();"  size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>
 
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="content-nav">
  <li><a  href="containers.html?id=${ServiceOrderID}" >Container</a></li>
  <li><a  href="cartons.html?id=${ServiceOrderID}" >Carton</a></li>
  <li><a  href="vehicles.html?id=${ServiceOrderID}" >Vehicle</a></li>
  <li class="current"><a  href="storages1.html?id=${ServiceOrderID}" >Storage</a></li>
</ul>
</td></tr>
</tbody></table>
  
 <s:set name="storages" value="storages" scope="request"/>
<display:table name="storages" class="table" requestURI="" id="storageList1" export="true" pagesize="10">
 
    
        <display:column property="description" sortable="true" titleKey="storage.description"/> 
         <display:column property="locationId" sortable="true" titleKey="storage.locationId"/>
         <display:column property="pieces" sortable="true" titleKey="storage.pieces"/>
         <display:column property="price" sortable="true" titleKey="storage.price"/> 
          <display:column property="containerId" sortable="true" titleKey="storage.containerId"/> 
          <display:column property="itemNumber" sortable="true" titleKey="storage.itemNumber"/> 
          
 
<display:setProperty name="paging.banner.item_name" value="storage1"/> 
    <display:setProperty name="paging.banner.items_name" value="storage1"/> 
    <display:setProperty name="export.excel.filename" value="Storage1 List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Storage1 List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Storage1 List.pdf"/> 
</display:table>

 
 
 
 </div>

 <c:out value="${buttons}" escapeXml="false" />
  </s:form> 
<script type="text/javascript"> 
    highlightTableRows("storageList"); 
    Form.focusFirstElement($("serviceForm4"));
</script> 