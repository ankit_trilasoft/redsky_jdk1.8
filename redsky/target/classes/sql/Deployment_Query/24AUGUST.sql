ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `originContactLastName` VARCHAR(35) AFTER `ugwIntId`,ADD COLUMN `contactLastName` VARCHAR(35) AFTER `originContactLastName`;

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'Administration', 'Administration', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Extract', 'Extract', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Form/Report', 'Form/Report', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'haulingManagement', 'Hauling Management', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Partner', 'Partner', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'referenceFunction', 'Reference Function', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Resource', 'Resource', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'scheduleResources', 'Schedule Resources', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'serviceOrder', 'Service Order', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'sO/MgmtInfo', 'SO/Mgmt Info', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Vanline', 'Vanline', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'workTicket', 'Work Ticket', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','MODULE',20,true,now(),'kkumar',now(),'kkumar',1,1,'');

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `ugwwNetworkGroup` BIT(1) default false;

DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup` from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);

update partnerpublic set ugwwNetworkGroup = true where partnercode='T38637';

ALTER TABLE `redsky`.`consigneeInstruction` ADD COLUMN `ugwIntId` VARCHAR(50) DEFAULT NULL AFTER `collect`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `ugwIntId` VARCHAR(50) DEFAULT NULL ;
  
Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
 values ('TSFT', 'AUTO', 'Auto', 15, 'VEHICLETYPE','en',now(),'UGWW',now(),'UGWW'),
('TSFT', 'BOAT', 'Boat', 15, 'VEHICLETYPE','en',now(),'UGWW',now(),'UGWW'),
('TSFT', 'MOTORCYCLE', 'Motorcycle', 15,'VEHICLETYPE','en',now(),'UGWW',now(),'UGWW'),
('TSFT', 'OTHER', 'Other', 15,'VEHICLETYPE','en',now(),'UGWW',now(),'UGWW');


INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','VEHICLETYPE',15,true,now(),'UGWW',now(),'UGWW',1,1,'');

ALTER TABLE `redsky`.`vehicle` ADD COLUMN `vehicleType` VARCHAR(20) AFTER `ugwIntId`;


DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findnetworkcorpid` $$
CREATE `findnetworkcorpid`(code varchar(50)) RETURNS varchar(10) CHARSET utf8
BEGIN
DECLARE Name varchar(10) DEFAULT 0;

SELECT distinct agentcorpid INTO Name FROM networkpartners WHERE 
agentpartnercode=code;

RETURN Name;

END $$

DELIMITER ;


DROP TABLE IF EXISTS `redsky`.`integrationloginfo`;
CREATE TABLE  `redsky`.`integrationloginfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `integrationId` varchar(50) DEFAULT NULL,
  `sourceOrderNum` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `destOrderNum` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `operation` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderComplete` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `statusCode` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `detailedStatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(82) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `retrycount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=989 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `redsky`.`networkcontrol`;
CREATE TABLE  `redsky`.`networkcontrol` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sourceCorpId` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `soNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sourceSoNum` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sourceInfo` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `sourceStatus` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `corpId` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actionStatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(82) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;


ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`billing` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`container` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`carton` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `ugwIntId` VARCHAR(50);

ALTER TABLE `redsky`.`vehicle` ADD COLUMN `ugwIntId` VARCHAR(50);





