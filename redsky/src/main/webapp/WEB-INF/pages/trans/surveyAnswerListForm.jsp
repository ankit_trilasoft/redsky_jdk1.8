<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    
    <style type="text/css"> 

h4 {
    color: #444;
    font-size: 14px;
    line-height: 1.3em;
    margin: 0 0 0.25em;
    padding: 0 0 0 15px;
    font-weight:bold;
}
.modal-body {
    padding: 0 20px;
    max-height: calc(100vh - 140px);
    overflow-y: auto;
    margin-top: 10px
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    min-height: 5.43px;
    padding: 10px;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 10px;
    text-align: right;
}
.modal-header .close {
    margin-right: 15px;
    margin-top: -2px;
    text-align: right;
}
@media screen and (min-width: 768px) {
    .custom-class {
        width: 70%;
        /* either % (e.g. 60%) or px (400px) */
    }
}
.hide{
	display:none;
}
.show{
	display:block;
}
</style> 
</head>
<div class="modal-dialog" style="width:40%;">
 <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" style="text-align:right !important;" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Survey&nbsp;Answers</h4>
      </div>
<div class="modal-body">
<s:form name="surveyAnswerForm" id="surveyAnswerForm">
<s:hidden name="childAnslineid" id="childAnslineid" value=""/>
<s:hidden name="childSeqNumListVal" id="childSeqNumListVal" />
<s:hidden name="childAnswerListVal" id="childAnswerListVal" />
<s:hidden name="childStatusListVal" id="childStatusListVal" />
<s:hidden name="surveyAnswerRowlist" id="surveyAnswerRowlist" />
<s:hidden name="parentId" id="parentId" value="${parentId}" />

<div id="surveyAnswerSetUp">
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%;margin:0px;">
	  <tbody>	 	
		  <tr>
		  <td>		  	
		<div class="spn">&nbsp;</div>
		<div id="para1" style="clear:both;">	
		<table style="margin-bottom:0px;">
		    	<tr>
		    	</tr>
		    	</table>		
					 <table class="table" id="dataTableAnswer" style="width:99%;margin-bottom:0px;">
					 <thead>
					 <tr>
						 <th style="text-align:center;">Seq#</th>
						 <th style="text-align:center;">Answer</th>
						 <th style="text-align:center;">Status</th>
					 </tr>
					 </thead>
					 <tbody>
					 
						 <c:forEach var="surveyAnswerItem" items="${surveyAnswerList}">
							<tr>
							<td><s:textfield name="childSequenceNumberList" id="childSeq${surveyAnswerItem.id}" value="${surveyAnswerItem.sequenceNumber}" size="20" maxlength="2" cssClass="input-text" cssStyle="width:20px;"/></td>
							<td><s:textfield name="childAnswerList" id="childAns${surveyAnswerItem.id}" value="${surveyAnswerItem.answer}" cssClass="input-text" maxlength="300" size="10" cssStyle="width:350px;" onkeydown="return checkForColon(event)"/></td>
							<td><s:checkbox name="childStatusList" id="childSta${surveyAnswerItem.id}" value="${surveyAnswerItem.status}" cssStyle="width:30px;"/></td>
						    </tr>
						 </c:forEach>
					 
					 </tbody>
					 </table>
					 </div>
					 </td>
		    		</tr>
		    	
		      	<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		 </tbody>
	</table>
   </div>

<div class="modal-footer" style="padding-bottom:0px !important;">
	<button type="button" class="btn btn-sm btn-primary" onclick="addRowAnswer('dataTableAnswer');">Add</button>
	<button type="button" class="btn btn-sm btn-success" onclick="saveSurveyAnswer()">Save</button>
	<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
</div>

</s:form>
</div></div></div>

<script type="text/javascript">
function checkForColon(evt) {
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==186 || keyCode== 58){
		return false;
	}else{
		return true;
	}
}
function addRowAnswer(tableID) {
	var flag=checkSurveyAnswerField(); 
	if(flag==true){
		    var table = document.getElementById(tableID);
		    var rowCount = table.rows.length;
		    
			var row = table.insertRow(rowCount); 
		    var childNewlineid=document.getElementById('childAnslineid').value;
		    if(childNewlineid==''){
		    	childNewlineid=rowCount;
		      }else{
		    	  childNewlineid=childNewlineid+"~"+rowCount;
		      }
		    document.getElementById('childAnslineid').value=childNewlineid;
		    
		 	var cell1 = row.insertCell(0);
		 	var element1 = document.createElement("input");
			element1.type = "text";
			element1.style.width="20px";
			element1.setAttribute("class", "input-text" );
			element1.setAttribute("maxlength", "2" );
			element1.setAttribute("value", rowCount );
			element1.id='childSeq'+rowCount;
			element1.setAttribute("onkeydown","return onlyNumberValue(event)");
			cell1.appendChild(element1);
		
			var cell2 = row.insertCell(1);
			var element2 = document.createElement("input");
			element2.type = "text";
			element2.style.width="350px";
			element2.setAttribute("class", "input-text" );
			element2.setAttribute("maxlength", "300" );
			element2.id='childAns'+rowCount;
			element2.setAttribute("onkeydown","return checkForColon(event)");
			cell2.appendChild(element2);
			
			var cell3 = row.insertCell(2);
			var element3 = document.createElement("input");
			element3.type = "checkbox";
			element3.style.width="30px";
			element3.id='childSta'+rowCount;
			element3.checked=true;
			cell3.appendChild(element3);
	}	
}

function saveSurveyAnswer(){
	var childSeqNumListVal="";   	
   	var childAnswerListVal="";
   	var childStatusListVal="";
   	var id='';
   	var flag=checkSurveyAnswerField(); 
	if(flag==true){
	    if(document.forms['surveyAnswerForm'].childSequenceNumberList!=undefined){
	        if(document.forms['surveyAnswerForm'].childSequenceNumberList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyAnswerForm'].childSequenceNumberList.length; i++){	
	 	    	 id=document.forms['surveyAnswerForm'].childSequenceNumberList[i].id;
	                id=id.replace(id.substring(0,8),'').trim();
		             if(childSeqNumListVal==''){
		            	 childSeqNumListVal=id+":"+document.forms['surveyAnswerForm'].childSequenceNumberList[i].value;
		             }else{
		            	 childSeqNumListVal= childSeqNumListVal+"~"+id+":"+document.forms['surveyAnswerForm'].childSequenceNumberList[i].value;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyAnswerForm'].childSequenceNumberList.id;
	 	         id=id.replace(id.substring(0,8),'').trim();   
	 	        childSeqNumListVal=id+":"+document.forms['surveyAnswerForm'].childSequenceNumberList.value;
	 	      }	        
	       }
	    if(document.forms['surveyAnswerForm'].childAnswerList!=undefined){
	        if(document.forms['surveyAnswerForm'].childAnswerList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyAnswerForm'].childAnswerList.length; i++){	
	 	    	 id=document.forms['surveyAnswerForm'].childAnswerList[i].id;
	                id=id.replace(id.substring(0,8),'').trim();
		             if(childAnswerListVal==''){
		            	 childAnswerListVal=id+":"+document.forms['surveyAnswerForm'].childAnswerList[i].value;
		             }else{
		            	 childAnswerListVal= childAnswerListVal+"~"+id+":"+document.forms['surveyAnswerForm'].childAnswerList[i].value;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyAnswerForm'].childAnswerList.id;
	 	         id=id.replace(id.substring(0,8),'').trim();   
	 	        childAnswerListVal =id+":"+document.forms['surveyAnswerForm'].childAnswerList.value;
	 	      }	        
	       }
	    if(document.forms['surveyAnswerForm'].childStatusList!=undefined){
	        if(document.forms['surveyAnswerForm'].childStatusList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyAnswerForm'].childStatusList.length; i++){	
	 	    	 id=document.forms['surveyAnswerForm'].childStatusList[i].id;
	                id=id.replace(id.substring(0,8),'').trim();
		             if(childStatusListVal==''){
		            	 childStatusListVal = id+":"+document.forms['surveyAnswerForm'].childStatusList[i].checked;
		             }else{
		            	 childStatusListVal = childStatusListVal+"~"+id+":"+document.forms['surveyAnswerForm'].childStatusList[i].checked;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyAnswerForm'].childStatusList.id;
	 	         id=id.replace(id.substring(0,8),'').trim();   
	 	        childStatusListVal = id+":"+document.forms['surveyAnswerForm'].childStatusList.checked;
	 	      }	        
	      }
	    document.getElementById('childSeqNumListVal').value=childSeqNumListVal;			 
		document.getElementById('childAnswerListVal').value=childAnswerListVal;  
		document.getElementById('childStatusListVal').value=childStatusListVal;
		
		var surveyAnswerRowlist="";
	    var childNewlineid=document.getElementById('childAnslineid').value;
	    if(childNewlineid!=''){
	           var childArrayLine=childNewlineid.split("~");
	           for(var i=0;i<childArrayLine.length;i++){		            	              
	               if(surveyAnswerRowlist==''){
	            	   surveyAnswerRowlist=document.getElementById('childSeq'+childArrayLine[i]).value.trim()+":"+document.getElementById('childAns'+childArrayLine[i]).value.trim()+":"+document.getElementById('childSta'+childArrayLine[i]).checked;
	               }else{
	            	   surveyAnswerRowlist=surveyAnswerRowlist+"~"+document.getElementById('childSeq'+childArrayLine[i]).value+":"+document.getElementById('childAns'+childArrayLine[i]).value+":"+document.getElementById('childSta'+childArrayLine[i]).checked;
	               }
	           }
	     }
	    document.getElementById('surveyAnswerRowlist').value =surveyAnswerRowlist;
	    var childSeqNumListVal = document.getElementById("childSeqNumListVal").value;
	    var childAnswerListVal = document.getElementById("childAnswerListVal").value;
	    var childStatusListVal = document.getElementById("childStatusListVal").value;
	    var surveyAnswerRowlist = document.getElementById("surveyAnswerRowlist").value;
	    var parentId = document.getElementById("parentId").value;
	    var $j = jQuery.noConflict();
	    $j.ajax({
			type: "POST",
			url: "saveSurveyAnswerAjax.html",
			data: {childSeqNumListVal:childSeqNumListVal,childAnswerListVal:childAnswerListVal,childStatusListVal:childStatusListVal,surveyAnswerRowlist:surveyAnswerRowlist,parentId:parentId},
			success: function (data, textStatus, jqXHR) {
				surveyAnswerModalClose();
	           },
				error: function (data, textStatus, jqXHR) {
					
				}
	         });
	}
}

function checkSurveyAnswerField(){
	  var childNewlineid=document.getElementById('childAnslineid').value; 	
	  if(childNewlineid!=''){
	     var childMandatoryArrayLine=childNewlineid.split("~");        
	        for(var i=0;i<childMandatoryArrayLine.length;i++){
	   		 var ansFieldVal=document.getElementById('childAns'+childMandatoryArrayLine[i]).value;
		       if(ansFieldVal==""){
			    	 alert("Please fill answer for line # "+childMandatoryArrayLine[i]);
			    	 return false;
		        }
        }
		 return true;
   }else{
	     return true;
   }
}

</script>
 		  	