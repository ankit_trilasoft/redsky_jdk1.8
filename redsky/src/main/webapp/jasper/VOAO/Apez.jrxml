<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="Apez"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<parameter name="User Name" isForPrompting="false" class="java.lang.String"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
     "Apez-VOAO.jrxml",
     customerfile.`destinationCity` AS customerfile_destinationCity,
     customerfile.`destinationState` AS customerfile_destinationState,
     customerfile.`destinationCountry` AS customerfile_destinationCountry,
     customerfile.`originCity` AS customerfile_originCity,
     customerfile.`originState` AS customerfile_originState,
     customerfile.`originCountry` AS customerfile_originCountry,
     accountline.`estimateRevenueAmount` AS accountline_estimateRevenueAmount,
     customerfile.`sequenceNumber` AS customerfile_sequenceNumber,
     systemdefault.`serviceInclude` AS systemdefault_serviceInclude,
     systemdefault.`serviceExclude` AS systemdefault_serviceExclude,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     customerfile.`billToName` AS customerfile_billToName,
     CONCAT_WS('  ',CONCAT_WS(' ','RE:',null,CONCAT(customerfile.`originCity`,if(CHAR_LENGTH(customerfile.`originState`)>1,CONCAT(', ',customerfile.`originState`,' '),''),if(CHAR_LENGTH(customerfile.`originCountry`)>1,CONCAT(', ',customerfile.`originCountry`,' '),''),'    ')),null,CONCAT_WS(' ','TO:',null,CONCAT(customerfile.`destinationCity`,if(CHAR_LENGTH(customerfile.`destinationState`)>1,CONCAT(', ',customerfile.`destinationState`,' '),''),if(CHAR_LENGTH(customerfile.`destinationCountry`)>1,CONCAT(', ',customerfile.`destinationCountry`,' '),''),''))) As customerfile_FullAddress,
     app_user.`email` AS app_user_email,
     app_user.`phone_number` AS app_user_phone_number,
     CONCAT_WS(' ',app_user.`first_name`,NULL,app_user.`last_name`) As app_user_first_name_last_name,     
     miscellaneous.`estimatedNetWeight` AS miscellaneous_estimatedNetWeight,
     miscellaneous.`unit1` AS miscellaneous_unit1,
     accountline.`quoteDescription` AS accountline_quoteDescription,  
     CONCAT_WS(' ',serviceorder.`firstName`,null,serviceorder.`lastName`) As serviceorder_firstName_lastName,
     miscellaneous.`equipment` AS miscellaneous_equipment     
FROM
     `customerfile` customerfile LEFT OUTER JOIN `serviceorder` serviceorder ON customerfile.`sequenceNumber` = serviceorder.`sequenceNumber` AND customerfile.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `accountline` accountline ON serviceorder.`id` = accountline.`serviceOrderId` AND accountline.`corpID` = $P{Corporate ID}
     INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `app_user` app_user ON serviceorder.`estimator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `notes` notes ON customerfile.`sequenceNumber` = notes.`notesId` AND noteType='Customer File' AND notes.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `systemdefault` systemdefault ON serviceorder.`corpID` = systemdefault.`corpID` AND systemdefault.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `container` container ON serviceorder.`id` = container.`serviceOrderId` AND container.`corpID` = $P{Corporate ID}
     INNER JOIN `miscellaneous` miscellaneous ON serviceorder.`id` = miscellaneous.`id` AND miscellaneous.`corpID` = $P{Corporate ID}      

WHERE serviceorder.`corpID` = $P{Corporate ID} 

AND serviceorder.`shipNumber` LIKE $P{Service Order Number}
AND customerfile.`quotationStatus`<>'Rejected' 
AND accountline.`estimateRevenueAmount`>'0' 
AND customerfile.`controlFlag`='Q']]></queryString>

	<field name="Apez-VOAO.jrxml" class="java.lang.String"/>
	<field name="customerfile_destinationCity" class="java.lang.String"/>
	<field name="customerfile_destinationState" class="java.lang.String"/>
	<field name="customerfile_destinationCountry" class="java.lang.String"/>
	<field name="customerfile_originCity" class="java.lang.String"/>
	<field name="customerfile_originState" class="java.lang.String"/>
	<field name="customerfile_originCountry" class="java.lang.String"/>
	<field name="accountline_estimateRevenueAmount" class="java.math.BigDecimal"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="systemdefault_serviceInclude" class="java.lang.String"/>
	<field name="systemdefault_serviceExclude" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="customerfile_billToName" class="java.lang.String"/>
	<field name="customerfile_FullAddress" class="java.lang.String"/>
	<field name="app_user_email" class="java.lang.String"/>
	<field name="app_user_phone_number" class="java.lang.String"/>
	<field name="app_user_first_name_last_name" class="java.lang.String"/>
	<field name="miscellaneous_estimatedNetWeight" class="java.math.BigDecimal"/>
	<field name="miscellaneous_unit1" class="java.lang.String"/>
	<field name="accountline_quoteDescription" class="java.lang.String"/>
	<field name="serviceorder_firstName_lastName" class="java.lang.String"/>
	<field name="miscellaneous_equipment" class="java.lang.String"/>

	<variable name="OriginCityState" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[($F{customerfile_originCity}==null ? "" : $F{customerfile_originCity})+ "," + " " + ($F{customerfile_originState}==null ? "" : $F{customerfile_originState}) + "," + " " + ($F{customerfile_originCountry}==null ? "" : $F{customerfile_originCountry})]]></variableExpression>
	</variable>
	<variable name="DestinationCityState" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{customerfile_destinationCity}=="null" ? "" : $F{customerfile_destinationCity} + ","  + " " + $F{customerfile_destinationState}=="null" ? "" : $F{customerfile_destinationState} + ","  + " " + ($F{customerfile_destinationCountry}==null ? "" : $F{customerfile_destinationCountry})]]></variableExpression>
	</variable>
	<variable name="total" class="java.math.BigDecimal" resetType="Page" calculation="Sum">
		<variableExpression><![CDATA[$F{accountline_estimateRevenueAmount}]]></variableExpression>
	</variable>

		<group  name="shipNumber" >
			<groupExpression><![CDATA[$F{serviceorder_shipNumber}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="355"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="228"
						width="535"
						height="18"
						key="staticText-8"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Quote for Destination Services]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1"
						y="332"
						width="405"
						height="16"
						key="staticText-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Service Type:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="423"
						y="332"
						width="106"
						height="16"
						key="staticText-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Amount:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="281"
						width="134"
						height="16"
						key="staticText-18"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Our Quote Number:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="134"
						y="281"
						width="401"
						height="16"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="12"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
				</textField>
				<frame>					<reportElement
						x="0"
						y="149"
						width="535"
						height="61"
						key="frame-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
				<staticText>
					<reportElement
						x="0"
						y="7"
						width="66"
						height="16"
						key="staticText-19"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Date:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="23"
						width="66"
						height="16"
						key="staticText-20"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[To:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="39"
						width="66"
						height="16"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Company:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="303"
						y="7"
						width="66"
						height="16"
						key="staticText-22"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[From:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="303"
						y="23"
						width="66"
						height="16"
						key="staticText-23"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[E-mail:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="303"
						y="39"
						width="66"
						height="16"
						key="staticText-24"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Phone:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="375"
						y="24"
						width="160"
						height="16"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{app_user_email}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd MMMMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="71"
						y="7"
						width="233"
						height="16"
						key="textField-8"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times-Roman" size="12"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="375"
						y="41"
						width="160"
						height="16"
						key="textField-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{app_user_phone_number}]]></textFieldExpression>
				</textField>
				</frame>				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="246"
						width="535"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_FullAddress}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="71"
						y="190"
						width="233"
						height="16"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_billToName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="375"
						y="157"
						width="160"
						height="16"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{app_user_first_name_last_name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="303"
						width="52"
						height="16"
						key="staticText-25"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Est.Wgt:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="173"
						y="303"
						width="32"
						height="16"
						key="staticText-26"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Units:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="52"
						y="303"
						width="82"
						height="16"
						key="textField"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{miscellaneous_estimatedNetWeight}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="205"
						y="303"
						width="29"
						height="16"
						key="textField"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{miscellaneous_unit1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="55"
						width="535"
						height="58"
						forecolor="#000000"
						key="staticText-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[1701 Florida Avenue, Northwest
Washington, DC 20009-2697
Tel: (202) 234-5600 Fax: (202) 234-3513
WWW.VOER.COM]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="2"
						y="126"
						width="532"
						height="0"
						forecolor="#999999"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="4.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="346"
						y="1"
						width="154"
						height="15"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="499"
						y="1"
						width="36"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="71"
						y="174"
						width="232"
						height="16"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_firstName_lastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="357"
						y="303"
						width="93"
						height="16"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{miscellaneous_equipment}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="270"
						y="303"
						width="84"
						height="16"
						key="staticText-29"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Container Size:]]></text>
				</staticText>
				<image  scaleImage="FillFrame" vAlign="Middle" hAlign="Center" onErrorType="Icon" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="412"
						y="16"
						width="122"
						height="106"
						key="image-4"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/ANGOLA_Logo.jpg"]]></imageExpression>
				</image>
			</band>
		</columnHeader>
		<detail>
			<band height="22"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="423"
						y="0"
						width="106"
						height="16"
						key="textField"
						positionType="Float"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font size="10"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_estimateRevenueAmount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="406"
						height="16"
						key="textField"
						positionType="Float"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{accountline_quoteDescription}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="330"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="1"
						y="27"
						width="406"
						height="16"
						key="staticText-30"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Our Services Include:]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="1"
						y="81"
						width="406"
						height="16"
						key="staticText-31"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Our Services Exclude:]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="16"
						y="53"
						width="391"
						height="17"
						key="textField-10"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Justified">
						<font size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{systemdefault_serviceInclude}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="16"
						y="111"
						width="391"
						height="18"
						key="textField-11"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Justified">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{systemdefault_serviceExclude}]]></textFieldExpression>
				</textField>
				<image  onErrorType="Icon" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="7"
						y="56"
						width="6"
						height="6"
						key="image-2"
						positionType="Float"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/bullet7.gif"]]></imageExpression>
				</image>
				<image  onErrorType="Icon" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="7"
						y="115"
						width="6"
						height="6"
						key="image-3"
						positionType="Float"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/bullet7.gif"]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="423"
						y="7"
						width="106"
						height="18"
						key="textField-12"/>
					<box leftPadding="1" rightPadding="1" topPadding="1" bottomPadding="1">					<pen lineWidth="0.25" lineStyle="Solid"/>
					<topPen lineWidth="0.25" lineStyle="Solid"/>
					<leftPen lineWidth="0.25" lineStyle="Solid"/>
					<bottomPen lineWidth="0.25" lineStyle="Solid"/>
					<rightPen lineWidth="0.25" lineStyle="Solid"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{total}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="349"
						y="7"
						width="66"
						height="18"
						key="staticText-32"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Total:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="257"
						y="312"
						width="278"
						height="18"
						key="textField-13"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{User Name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="312"
						width="251"
						height="18"
						key="staticText-33"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<text><![CDATA[Printed By]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
