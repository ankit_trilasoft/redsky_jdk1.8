<%@ include file="/common/taglibs.jsp"%>   
<head>   
<title><fmt:message key="partnerAccountRefDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='partnerAccountRefDetail.heading'/>"/>   
<script type="text/javascript">
function checkAccountRef(){
var variable = '${companyDivisionAcctgCodeUnique}';
if(variable!='Y'){
checkCompanyCodeAvailability();
}else{
}
}
function checkCompanyCodeAvailability() {
var variable = '${companyDivisionAcctgCodeUnique}';
    var refType = document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
    var partnerCode = document.forms['partnerAccountRefForm'].elements['partnerAccountRef.partnerCode'].value; 
    var accountCrossReference = document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value;
  if(variable=='Y'){
   var companyDivision= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.companyDivision'].value;
  if(((companyDivision!='${partnerAccountRef.companyDivision}') || (refType!='${partnerAccountRef.refType}'))){
	var url="checkCompanyDivisonExists.html?ajax=1&decorator=simple&popup=true&companyDivision="+encodeURI(companyDivision)+"&partnerCode="+encodeURI(partnerCode)+"&refType="+encodeURI(refType);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse5;
    http4.send(null);
    }
    }
    else {
    if(((refType!='${partnerAccountRef.refType}') || (accountCrossReference!='${partnerAccountRef.accountCrossReference}'))){
    var url="checkAccountCrossReference.html?ajax=1&decorator=simple&popup=true&accountCrossReference="+encodeURI(accountCrossReference)+"&partnerCode="+encodeURI(partnerCode)+"&refType="+encodeURI(refType);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse5;
    http4.send(null);
    }
   }
}
	
function handleHttpResponse5(){
		  if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                var variable = '${companyDivisionAcctgCodeUnique}';
                if(results >= 1 ){
                if(variable=='Y'){
                alert("Company Division is already in use");
               // var accountCrossReference = document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value;
                document.forms['partnerAccountRefForm'].reset();
                <c:if test="${company.autoGenerateAccRef == 'Y'}">
        		var type= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
        		var pType= document.forms['partnerAccountRefForm'].elements['partnerType'].value;
        		document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
        			document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
        	</c:if>
               // document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value = accountCrossReference;
                }else{
               var temp = document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
	                if(temp!='S'){
	                  alert("Accounting Cross Reference is already in use");
	                  document.forms['partnerAccountRefForm'].reset();
	                  <c:if test="${company.autoGenerateAccRef == 'Y'}">
	          			var type= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
	          			var pType= document.forms['partnerAccountRefForm'].elements['partnerType'].value;
	          			document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
	          			document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
	          		</c:if>
	                }
                  }
                 } else {
                
                }
			}	
}


function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http4 = getHTTPObject(); 

function disabledAll(){
	if(!(document.forms['partnerAccountRefForm'].elements['partnerAccountRef.id'].value == '')){
	var i;
		for(i=0;i<=100;i++){
		document.forms['partnerAccountRefForm'].elements[i].disabled = true;
		}
	}
}

function enableAll(){
	if(!(document.forms['partnerAccountRefForm'].elements['partnerAccountRef.id'].value == '')){
	var i;
		for(i=0;i<=100;i++){
		document.forms['partnerAccountRefForm'].elements[i].disabled = false;
		}
	}
}
function checkCompanyDiv(){
if(document.forms['partnerAccountRefForm'].elements['partnerAccountRef.companyDivision'].value == ''){
alert("Company Division is a required field");
return false;
}
}

function setAccountRef(){
	var type= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
	var pType= document.forms['partnerAccountRefForm'].elements['partnerType'].value;
	<c:if test="${empty partnerAccountRef.id}">
		<c:if test="${company.autoGenerateAccRef == 'Y'}">
			if(type=='P'&& (pType=='AG' || pType=='AC' || pType=='CR')){
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value='${paySeq}';
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
			}
			if(type=='R' && (pType=='AG' || pType=='AC' || pType=='CR')){
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value = '${recSeq}';
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
			}
			if(type=='S' && (pType=='AG' || pType=='AC' || pType=='CR')){
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value = '${suspenseNum}';
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= false;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-text';
			}
			checkAccountRef();
		</c:if>
	</c:if>
}

function setAccountRefPUTT(){
	<configByCorp:fieldVisibility componentId="component.AcctRef.autoGenerateAccRefWithPartner"> 
	var type= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
	var pType= document.forms['partnerAccountRefForm'].elements['partnerType'].value;
	var partnerCode='${partner.partnerCode}'
	<c:if test="${empty partnerAccountRef.id}"> 
			if(type=='P'){ 
				var val = "000000";
				var firstPartnercodeChar=partnerCode.substring(0, 1) 
				if('0,1,2,3,4,5,6,7,8,9'.indexOf(firstPartnercodeChar)>=0){
					partnerCode=partnerCode;	
				}else{
					partnerCode=partnerCode.substring(1, partnerCode.length);
				} 
				if(partnerCode.length <=6){
					val = val.substring(partnerCode.length); 
					partnerCode=val+partnerCode;
					}
				partnerCode="C"+partnerCode;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value=partnerCode;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
			}
			if(type=='R' ){
				var val = "000000";
				var firstPartnercodeChar=partnerCode.substring(0, 1) 
				if('0,1,2,3,4,5,6,7,8,9'.indexOf(firstPartnercodeChar)>=0){
					partnerCode=partnerCode;	
				}else{
					partnerCode=partnerCode.substring(1, partnerCode.length);
				} 
				if(partnerCode.length <=6){
					val = val.substring(partnerCode.length); 
					partnerCode=val+partnerCode;
					}
				partnerCode="D"+partnerCode;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value = partnerCode;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
				document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
			} 
	</c:if>
	if(document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].value!=''){
	document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
	document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
	}
	</configByCorp:fieldVisibility>
}

function checkReadOnly(){
	<c:if test="${not empty partnerAccountRef.id}">
		<c:if test="${company.autoGenerateAccRef == 'Y'}">
		 var len = document.forms['partnerAccountRefForm'].length;
     	 for (var i=0 ;i<len ;i++){ 
    	 	 document.forms['partnerAccountRefForm'].elements[i].disabled = true;
     	 }
      </c:if>
  	</c:if>
}
function setProperty(){
	<c:if test="${company.autoGenerateAccRef == 'Y'}">
		var type= document.forms['partnerAccountRefForm'].elements['partnerAccountRef.refType'].value;
		var pType= document.forms['partnerAccountRefForm'].elements['partnerType'].value;
		if(type=='S' && (pType=='AG' || pType=='AC' || pType=='CR')){
			document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].readOnly= true;
			document.forms['partnerAccountRefForm'].elements['partnerAccountRef.accountCrossReference'].className = 'input-textUpper';
		}
	</c:if>
}

</script>
</head>   

<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="partnerAccountRefForm" action="savePartnerAccountRef" method="post" validate="true" onsubmit="return checkCompanyDiv();">   
<s:hidden name="partnerAccountRef.id"/>   
<s:hidden name="partnerAccountRef.partnerCode"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="partner.id"/>
<div id="Layer1" style="width:100%">
	<div id="newmnav">
		  <ul>
		  <%-- Added by kunal for ticket number: 6176 --%>
		  	<c:if test="${partnerType == 'AG'}">
				<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
				<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
				<c:if test="${sessionCorpID!='TSFT' }">
				<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
				</c:if>
			</c:if>
				<c:if test="${partnerType == 'AC'}">
			<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
			<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
			<c:if test="${sessionCorpID!='TSFT' }">
			<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerCode=${partner.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
			</c:if>
			</c:if>
		
			<%-- Modification closed here for ticket number: 6176 --%>
		  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref List</span></a></li>
		  		 <%-- Added by kunal for ticket number: 6176 --%>
		  		<c:if test="${usertype!='ACCOUNT'}">
					<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
						<c:if test="${partnerType == 'AC'}">
							<c:if test="${not empty partner.id}">
								<configByCorp:fieldVisibility componentId="component.standard.accountContactTab"><li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
								<c:if test="${checkTransfereeInfopackage==true}">
									<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
								</c:if>
								<c:if test="${partner.partnerPortalActive == true}">
								 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation"><li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
								</c:if>
							</c:if>
						</c:if>
					</sec-auth:authComponent>
				</c:if>
				<%-- Modification closed here for ticket number: 6176 --%>
							            
				<c:if test="${partnerType == 'VN'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Vendor Detail</span></a></li>
						<c:if test="${sessionCorpID!='TSFT' }">
						<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
						</c:if>
				</c:if>
				<c:if test="${partnerType == 'CR'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Carrier Detail</span></a></li>
				</c:if>
				
				<c:if test="${partnerType == 'AG'}">
						<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
				        <li><a href="baseList.html?id=${partner.id}"><span>Base</span></a></li>
				        <li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				        <%--
				        <c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
			              <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			            </c:if>
			            <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
			             <li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
			            </c:if>
				        
				--%></c:if>
				<c:if test="${partnerType == 'OO'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Owner Ops Detail</span></a></li>
						<li><a href="standardDeductionsList.html?id=${partner.id}&partnerType=OO"><span>Standard Deductions</span></a></li>
				</c:if>
				<c:if test="${partnerType != 'AG'}">
					<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<c:if test='${paramValue == "View"}'>
						<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
					</c:if>
				</c:if>
				<%-- Added by kunal for ticket number: 6176 --%>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC' }">
					<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
					<c:if test="${partner.partnerPortalActive == true  && partnerType == 'AG'}">
					       	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
					       	<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
					       	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					        </configByCorp:fieldVisibility>
					</c:if>
					<c:if test="${partnerType == 'AC'}">
						 <c:if test="${not empty partner.id}">
							<c:url value="frequentlyAskedQuestionsList.html" var="url">
							<c:param name="partnerCode" value="${partner.partnerCode}"/>
							<c:param name="partnerType" value="AC"/>
							<c:param name="partnerId" value="${partner.id}"/>
							<c:param name="lastName" value="${partner.lastName}"/>
							<c:param name="status" value="${partner.status}"/>
							</c:url>
							<li><a href="${url}"><span>FAQ</span></a></li>
							<c:if test="${partner.partnerPortalActive == true}">
								<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
							</c:if>
						</c:if>
					</c:if>			
				</c:if>	
				<%-- Modification closed here for ticket number: 6176 --%>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Acct Ref Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<c:if test="${not empty partnerAccountRef.id}"><li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${partnerAccountRef.id}&tableName=partneraccountref&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li></c:if>
		 </ul>
	</div><div class="spn">&nbsp;</div>
	
</div>
<div id="Layer1" style="width:100%"> 
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:785px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0" cellspacing="2" cellpadding="1">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  	<td align="right">Code</td>
	<td ><s:textfield key="partner.partnerCode" required="true" readonly="true"  cssClass="input-textUpper" size="12"/></td>
	<td align="right">Name</td>
	<td colspan="4" ><s:textfield key="partner.lastName" required="true" size="58" readonly="true" cssClass="input-textUpper"/></td>
	</tr>
	<tr><td height="5px"></td></tr>
    <tr>
    <td align="right" width="30px">Type</td>
	    <td ><s:select list="%{accountRefTypeList}" name="partnerAccountRef.refType" cssClass="list-menu" onchange="checkCompanyCodeAvailability();setAccountRef();setAccountRefPUTT();" /> </td>
		<td align="right" width="170px"><fmt:message key="partnerAccountRef.accountCrossReference"/></td>
		<td><s:textfield key="partnerAccountRef.accountCrossReference" id ="partnerAccountRef.accountCrossReference" maxlength="20" required="true" cssClass="input-text" size="20" onchange="checkAccountRef();" /></td>
	    <td></td>
	    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
    	<td align="right" width="110px"><fmt:message key="partnerAccountRef.companyDivision"/><font color="red" size="2">*</font></td>
    	<td><s:select cssClass="list-menu" name="partnerAccountRef.companyDivision" list="%{findCompanyDivisionByCorpId}"
										cssStyle="width:70px" headerKey="" headerValue="" onchange="checkCompanyCodeAvailability();"/></td>
		</c:if>
	</tr>
	<tr><td height="5px"></td></tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		<table style="width:770px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partnerAccountRef.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerAccountRef.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${partnerAccountRef.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty partnerAccountRef.id}">
								<s:hidden name="partnerAccountRef.createdBy"/>
								<td><s:label name="createdBy" value="%{partnerAccountRef.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partnerAccountRef.id}">
								<s:hidden name="partnerAccountRef.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partnerAccountRef.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerAccountRef.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${partnerAccountRef.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty partnerAccountRef.id}">
							<s:hidden name="partnerAccountRef.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{partnerAccountRef.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partnerAccountRef.id}">
							<s:hidden name="partnerAccountRef.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
		</div>								 
 
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px"  /> 
        <input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"/>'" />
        <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:60px; height:25px " onclick ="setProperty();"/> 
 
</s:form>   
<script type="text/javascript"> 
		try{
		var  permissionTest  = "";
		<sec-auth:authComponent componentId="module.field.partnerAccountRef.allFields">  
		  	permissionTest  = <%=(Integer)request.getAttribute("module.field.partnerAccountRef.allFields" + "Permission")%>;
		</sec-auth:authComponent>
		
		
		if(permissionTest > 2){ 
			enableAll();
		}else{
			disabledAll(); 
		}
		}
		catch(e){}
		try{
		setAccountRef();
		setAccountRefPUTT();
		checkReadOnly();
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>    
<script type="text/javascript">   
    Form.focusFirstElement($("partnerAccountRefForm"));   
</script>