<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
 <head>  
    <title>Price Point</title>   
    <meta name="heading" content="vanLine List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;} 

</style>
</head>
<script type="text/javascript">
// add parser through the tablesorter addParser method 
$(document).ready(function() {
	<c:if test="${pricingDetailsList!='[]'}">
      $.tablesorter.addParser({id: 'alexa',is: function(s){return false;},
        format: function(s) {
          var testCC=$("#CurrencyDD1243").val();
          s = s.replace(""+testCC+"", "").replace(/\,/g,"");
		  s = parseFloat(s);
          return s;
          return s.replace(''+testCC+'', "").replace(/\,/g,'');
        },
        type: 'numeric'
    });
      $(function() {          
    	  $('#pricingDetailsList').trigger("update");
    	    $("#pricingDetailsList").tablesorter({ 
    	    	sortList: [[9,0]],
    	        headers: { 
    	            9: { 
    	                sorter:'alexa' 
    	            } 
    	        } 
    	    }); 
    	});
      </c:if> 
});

</script>
<s:form id="priceingForm" name="priceingForm" method="post" validate="true">
<table style="margin: 0px; width: 100%; text-align: center; font-weight: bold; font-size: 13px;" class="listwhitetext">
<tr><td style="color:#FF0000;"><c:if test="${not empty pricePointMassageDetails}"><c:out value="${pricePointMassageDetails}"></c:out> </c:if></td></tr></table>
<div class="spnblk">&nbsp;</div>
<div id="otabs" style="margin-bottom:0px;">
  <ul>
    <li><a class="current"><span>Pricing&nbsp;Details</span></a></li>
  </ul>
</div>
<div class="listwhitetext" style="position: absolute; margin-left: 150px;width:755px;float:left;">
<div style="float:left;"> 
<b>Market Area:</b>&nbsp;<s:select id="countryMarketAreaDD1243" name="marketAreaDetails22" cssClass="list-menu" cssStyle="width:180px" list="%{marketAreaList}" value="${marketAreaCode}" headerKey="" onchange="priceDetailsChange(this);" headerValue=""  />
</div>
<div style="float:right;">
<b>Quote Currency:</b>&nbsp;<s:select id="CurrencyDD1243" name="marketAreaCurrency22" cssClass="list-menu" cssStyle="width:70px" list="%{country}" value="%{corpIdBaseCurrency}" headerKey="" onchange="priceDetailsCurrencyChange(this);" headerValue=""  />
</div>
</div>
<div class="spnblk">&nbsp;</div>
<s:set name="pricingDetailsList" value="pricingDetailsList" scope="request"/>   
<display:table name="pricingDetailsList" class="table" requestURI="" id="pricingDetailsList" defaultsort="8" pagesize="100" style="width:100%;margin-bottom:0px;" partialList="true" size="100"> 
<display:column  style="width:8%;" title="Partner Code">
 <c:set var="des" value="${pricingDetailsList.lastName}"/>
 <c:set var="focurr" value="${pricingDetailsList.totalBaseCurrency}"/> 
 <c:set var="curr" value="${pricingDetailsList.total}"/>  
  <c:set var="currUnit" value="${pricingDetailsList.currency}"/>  
   <c:set var="foRate" value="${pricingDetailsList.baseRate}"/>
    <c:set var="agentId" value="${pricingDetailsList.agentId}"/>
 <a onclick='setParentValue("${pricingDetailsList.partnerCode}","${fn:escapeXml(des)}","${fn:escapeXml(curr)}","${fn:escapeXml(currUnit)}","${fn:escapeXml(foRate)}","${fn:escapeXml(focurr)}","${fn:escapeXml(agentId)}","${pricingDetailsList.marketArea}","${pricingDetailsList.baseCurrencyCorpId}");'>
 <c:if test="${not empty pricingDetailsList.partnerCode}">
 <c:out value="${pricingDetailsList.partnerCode}"/>
 </c:if>
 <c:if test="${empty pricingDetailsList.partnerCode}">PPI:${pricingDetailsList.agentId}</c:if>
 </a>
  <!--<img id="supp_${pricingDetailsList.agentId}" align="top" onclick="checkSupplement('${pricingDetailsList.agentId}','${pricingDetailsList.marketArea}')" src="${pageContext.request.contextPath}/images/plus-small.png"/>
-->
<input type="hidden" id="invoiceTotal${pricingDetailsList.agentId}"  name="invoiceTotal${pricingDetailsList.agentId}" />
<input type="hidden" id="tariffId${pricingDetailsList.agentId}"  name="tariffId${pricingDetailsList.agentId}" />
</display:column>
<display:column property="lastName" style="width:15%;" sortable="true" title="Name"/>
<display:column title="FIDI" style="width:3%;" >
<c:if test="${pricingDetailsList.fidiNumber=='Y'}">
<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
</c:if>
</display:column>
<display:column title="UGRN" style="width:3%;" >
<c:if test="${pricingDetailsList.utsNumber=='Y'}">
<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
</c:if>
</display:column>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
<display:column property="agentClassification" style="width:10%;" sortable="true" title="Agent&nbsp;Classification"/>
</configByCorp:fieldVisibility>
<display:column property="billingCity" style="width:4%;" title="City"/>
<display:column property="billingCountryCode" style="width:4%;" title="Country"/>
<display:column property="pricingMode" style="width:4%;" title="Type"/>
<display:column style="width:8%;text-align:right;" headerClass="containeralign" title="Weight"><fmt:formatNumber value="${pricingDetailsList.pricingWeight}" maxFractionDigits="2" minFractionDigits="2" />&nbsp;<c:out value="${pricingDetailsList.weightUnit}"/> </display:column>
<display:column style="width:6%;text-align:right;" headerClass="containeralign" title="Volume"><fmt:formatNumber value="${pricingDetailsList.pricingVolume}" maxFractionDigits="2" minFractionDigits="2" />&nbsp;<c:out value="${pricingDetailsList.volumeUnit}"/></display:column>
<display:column  style="width:8%;text-align:right;" headerClass="containeralign" sortable="true" sortProperty="invoiceTotalDec" title="Agent Quote"><c:out value="${pricingDetailsList.invoiceTotalDecCurrency}"/>&nbsp;<fmt:formatNumber value="${pricingDetailsList.invoiceTotalDec}" maxFractionDigits="2" minFractionDigits="2" /></display:column> 
<display:column  style="width:8%;text-align:right;" headerClass="containeralign" sortable="true" sortProperty="total" title="Currency Amt"><c:out value="${pricingDetailsList.baseCurrencyCorpId}"/>&nbsp;<fmt:formatNumber value="${pricingDetailsList.total}" maxFractionDigits="2" minFractionDigits="2" />  
<img id="${pricingDetailsList.agentId}" align="top" onclick="checkQuoteDetails('${pricingDetailsList.agentId}','${pricingDetailsList.marketArea}','${pricingDetailsList.baseCurrencyCorpId}','${pricingDetailsList.total}')" src="${pageContext.request.contextPath}/images/plus-small.png"/>
</display:column>
</display:table>
<!--<div style="background:#BCD2EF;font-family: arial,verdana;font-size: 11px;font-weight: bold;
    height:auto;padding: 2px 3px 3px 2px;border:1px solid #3DAFCB;">
<table style="margin:0px;">
<tr>
<td width="100" valign="top">Market note for :</td><td width="140" valign="top">&nbsp;${marketNameDetails}&nbsp;</td><td valign="top">${marketNotesDetails}&nbsp;</td>
</tr>
</table>
</div>

-->
<c:if test="${marketNotesDetails!=''}">
<div style="font-family: arial,verdana;height:auto;border:2px solid #3DAFCB;border-radius:5px;padding:0px;margin:3px 0px;">
<table width="100%" cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;background:#BCD2EF;border-bottom:1px solid #3DAFCB;">
<tr><td class="listwhitetext" style="padding:3px;padding-bottom:4px;font-size: 12px;"><b>&nbsp;Market Note:&nbsp;${marketNameDetails}&nbsp;</b></td></tr>
</table>
<table class="" cellspacing="0" cellpadding="0" style="margin:0px;padding-left:5px;padding-top:3px;padding-right:3px;">
<tr>
<td class="listwhitetext" style="padding-bottom:8px;line-height:15px;font-size:11px;" >${marketNotesDetails}</td>
</tr>
</table>
</div>
</c:if>
<table>
<tr>
<td><input type="button"  value="Close" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
<tr>
<td><b>Disclaimer:Fx Rates are in accordance with RedSky Rates.</b></td>
</tr>
</table>
</s:form>
