package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.UpdateRuleResult;

public interface UpdateRuleResultDao extends GenericDao<UpdateRuleResult, Long>{
	public List findMaximumId();
}
