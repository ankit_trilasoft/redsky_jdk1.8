<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Tool Tip</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"></td>
	<td align="right"  style="width:100px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table> 


<c:if test="${not empty lName}">
<b>Name :</b> ${lName}
</c:if>
<c:if test="${not empty billingAdd1}">
<br><b>Address : </b> ${billingAdd1}
</c:if>
<c:if test="${not empty billingAdd2}">
${billingAdd2}
</c:if>
<c:if test="${not empty billingAdd3}">
${billingAdd3}
</c:if>
<c:if test="${not empty billingAdd4}">
${billingAdd4}
</c:if>
<c:if test="${not empty billingCity}">
${billingCity}
</c:if>
<c:if test="${not empty billingState}">
${billingState}
</c:if>
<c:if test="${not empty billingZip}">
${billingZip}
</c:if>
<c:if test="${not empty billingCountry}">
<br>${billingCountry}
</c:if>
<c:if test="${not empty billingFax}">
<br><b>Fax:</b>${billingFax}
</c:if>
<c:if test="${not empty billingPhone}">
<br><b>Phone:</b>${billingPhone}
</c:if>
