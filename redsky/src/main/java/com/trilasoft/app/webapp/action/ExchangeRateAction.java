/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "ExchangeRate" object in Redsky .
 * @Class Name	ExchangeRateAction
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        08-Dec-2008
 */
package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ExchangeRate;
import com.trilasoft.app.model.HistoryExchangeRate;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.RefMasterManager;

public class ExchangeRateAction extends BaseAction{
	    
	//static Variables
	private static Map<String, String> currency;
	//List Data type Variables	   
    private List refMasters;   
	private List exchangeRateList;
	private List accExchangeRateList;
	//String Data type Variables		
	private String exchangeCurrency;	
	private String gotoPageString;
	private String validateFormNav;
	private String sessionCorpID;
	private String country;
	//Long Data type Variables	
	private Long id;	
	private Long maxId;
	private Long chagneId;
	//Class Variables of Managers
	private  ExchangeRateManager exchangeRateManager;		
    private RefMasterManager refMasterManager;
	//Class Variables of Models		
    private ExchangeRate exchangeRate;
    private  Map<String, String> yesno;
    private boolean historyExchangeFlag = false;
    private Company company; 
    private CompanyManager companyManager;
    private boolean historyFx;
    private HistoryExchangeRate historyExchangeRate;
    private Date valueDate;
    
    
		
		public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
			this.exchangeRateManager = exchangeRateManager;
		}
		//Method for auto save
		public String saveOnTabChange() throws Exception {
		    	String s = save(); 
		        validateFormNav = "OK";
		        return gotoPageString;
		}
		//A Method to Authenticate User, calling from main menu to set CorpID.
	    public ExchangeRateAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	    //Method for editing  record calling from ExchangeRate list.
	    private HistoryExchangeRate  exchangeRate1;
	    private String flagHit;
	    public String edit() {
			getComboList(sessionCorpID);
			
	        if (id != null) {   
	        	exchangeRate=exchangeRateManager.get(id);
	        } else {   
	        	exchangeRate = new ExchangeRate();
	        	exchangeRate.setCorpID(sessionCorpID);
	        	exchangeRate.setValueDate(new Date());
	        	List baseCurrencyList= exchangeRateManager.searchBaseCurrency(sessionCorpID);
	        	if(!baseCurrencyList.isEmpty())
	        	{
	        		exchangeRate.setBaseCurrency(baseCurrencyList.get(0).toString());
	        	}
	        	exchangeRate.setCreatedOn(new Date());
	        	exchangeRate.setUpdatedOn(new Date());
	        }   
	        
	        return SUCCESS;   
	    }   
	    
	    
	    public String editHistoryExchangeRate() {
	    	getComboList(sessionCorpID);
	    	if(id != null)
        	{
        	//historyExchangeRate=historyExchangeRateManager.get(id1);
        	List <HistoryExchangeRate>extDump = exchangeRateManager.getHistoryFromid(id);
        	for(HistoryExchangeRate dd:extDump){
        		exchangeRate1=dd;
        	}
        	}
	    	 return SUCCESS;   
	    }
	    
	    //Method used to save the ExchangeRate calling from ExchangeRateform save button.	
	    public String save() throws Exception {
			getComboList(sessionCorpID);
			boolean isNew = (exchangeRate.getId() == null);
			
			if(isNew)
		    {
				exchangeRate.setCreatedOn(new Date());
		    }
		     
			exchangeRate.setCorpID(sessionCorpID);
			exchangeRate.setUpdatedBy(getRequest().getRemoteUser());
			exchangeRate.setUpdatedOn(new Date());
			exchangeRateManager.save(exchangeRate);
			
			if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
				String key = (isNew) ? "Exchange rate has been added successfully" : "Exchange rate has been updated successfully";
				saveMessage(getText(key));
				}	
			if (!isNew) {   
	        	return INPUT;   
	        } else {   
	        	maxId = Long.parseLong(exchangeRateManager.findMaximumId().get(0).toString());
	        	exchangeRate = exchangeRateManager.get(maxId);
	          return SUCCESS;
	        } 
		}
	    //Method that return ExchangeRateList form when you click on cancel button.	 
	    @SkipValidation 
	    public String listForm() {
	    	exchangeList();	    	
	        return SUCCESS;   
	    }
	    //Method for getting ExchangeRate list.
	    @SkipValidation 
	    public String exchangeList() {	    	
	    	getComboList(sessionCorpID);
	    	exchangeRateList = exchangeRateManager.getAll();	    	
	        return SUCCESS;   
	    }
	    
	    @SkipValidation 
	    public String historyExchangeList() {	    	
	    	getComboList(sessionCorpID);
	    	historyExchangeFlag = true;
	    	exchangeRateList = exchangeRateManager.getExchangeHistoryList(sessionCorpID);    	
	        return SUCCESS;   
	    }
	    
	    //Method that return ExchangeRateForm. 
	    @SkipValidation 
	    public String exchangeForm() {	    	
	        return SUCCESS;   
	    }
	    
	    // Method that delete ExchangeRate. 
	    @SkipValidation 
	    public String delete() {	    	
	    	List allId=exchangeRateManager.searchID();	    	
	    	Iterator it=allId.iterator();
			while(it.hasNext())
		       {
				Long dbId=(Long)it.next();
				if(chagneId.equals(dbId))
				{
					exchangeRateManager.remove(chagneId);
					saveMessage(getText("exchangeRate.deleted"));
				}
		       }	
	        exchangeList();
	        return SUCCESS; 
	    } 
	    
	    //Method that are used to search record 
	    private String valueDateFormatter;
	    private String valueDateFormatter1;
	    @SkipValidation 
	    public String searchByCurrency() {		    	
	    	getComboList(sessionCorpID);
	    	boolean currencyCode = (exchangeCurrency == null);
	    	if(!currencyCode){
	    		if(historyExchangeFlag == true){
	    			  try {
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yy");
						  if(valueDate==null){
							  valueDateFormatter1="";
							  valueDateFormatter="";
							 }else{
						  valueDateFormatter = formatter.format(valueDate);
						  valueDateFormatter1 = formatter1.format(valueDate); 
					 }
				exchangeRateList=exchangeRateManager.searchAll(exchangeCurrency , valueDateFormatter);
					} catch (Exception e) {
						e.printStackTrace();
					}
	    	   }else{
	    		exchangeRateList=exchangeRateManager.searchAll(exchangeCurrency);
	    	   }	    		   	
	    	}
	        return SUCCESS;   
	    }
	    //Method for getting the values for all comboboxes calling from ExchangeRate form for dropdown list.
	    @SkipValidation
	    public String getComboList(String corpId){	    	
	    	currency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
	    	yesno = refMasterManager.findByParameter(corpId, "YESNO");
	    	company=companyManager.findByCorpID(sessionCorpID).get(0);
	    	if(company!=null){
	    		historyFx = company.getHistoryFx();
	    		
	    	}
	    	return SUCCESS;
	    }
	    
	    //Method for getting the baseCurrencyRate depending upon CorpID and country
	    private ChargesManager chargesManager;
	    private String charge;
	    private String contract;
		private String chargeInsurancePresetList;
	    @SkipValidation
	    public String getAccExchangeRate(){
	    	accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,country); 
	    	/*if("FALSE".equals(country)){
	    		accExchangeRateList = new ArrayList();
	    		chargeInsurancePresetList = chargesManager.findCharges(charge,contract,sessionCorpID);
		    	accExchangeRateList.add(chargeInsurancePresetList);
	    	}else{
	    		accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,country); 
	    	}*/
	    	return SUCCESS;
	    }
	    
	   
	    
	    public void setId(Long id) {   
	        this.id = id;   
	    }  
	    
	   
		
		public void setRefMasterManager(RefMasterManager refMasterManager) {
	        this.refMasterManager = refMasterManager;
	    }
	    public List getRefMasters(){
	    	return refMasters;
	    }
	    		
		public String getValidateFormNav() {
			return validateFormNav;
		}

		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		}
		
		public String getGotoPageString() {
			return gotoPageString;
		}

		public void setGotoPageString(String gotoPageString) {
			this.gotoPageString = gotoPageString;
		}

		public Long getMaxId() {
			return maxId;
		}

		public void setMaxId(Long maxId) {
			this.maxId = maxId;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		}

		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}

		public Long getId() {
			return id;
		}

		public void setRefMasters(List refMasters) {
			this.refMasters = refMasters;
		}
		
		public ExchangeRate getExchangeRate() {
			return exchangeRate;
		}

		public void setExchangeRate(ExchangeRate exchangeRate) {
			this.exchangeRate = exchangeRate;
		}

		public List getExchangeRateList() {
			return exchangeRateList;
		}

		public void setExchangeRateList(List exchangeRateList) {
			this.exchangeRateList = exchangeRateList;
		}

		public static Map<String, String> getCurrency() {
			return currency;
		}

		public String getExchangeCurrency() {
			return exchangeCurrency;
		}

		public void setExchangeCurrency(String exchangeCurrency) {
			this.exchangeCurrency = exchangeCurrency;
		}

		public List getAccExchangeRateList() {
			return accExchangeRateList;
		}

		public void setAccExchangeRateList(List accExchangeRateList) {
			this.accExchangeRateList = accExchangeRateList;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}
		public Long getChagneId() {
			return chagneId;
		}
		public void setChagneId(Long chagneId) {
			this.chagneId = chagneId;
		}
		public Map<String, String> getYesno() {
			return yesno;
		}
		public void setYesno(Map<String, String> yesno) {
			this.yesno = yesno;
		}
		public ChargesManager getChargesManager() {
			return chargesManager;
		}
		public void setChargesManager(ChargesManager chargesManager) {
			this.chargesManager = chargesManager;
		}
		public String getCharge() {
			return charge;
		}
		public void setCharge(String charge) {
			this.charge = charge;
		}
		public String getChargeInsurancePresetList() {
			return chargeInsurancePresetList;
		}
		public void setChargeInsurancePresetList(String chargeInsurancePresetList) {
			this.chargeInsurancePresetList = chargeInsurancePresetList;
		}
		public String getContract() {
			return contract;
		}
		public void setContract(String contract) {
			this.contract = contract;
		}
		public boolean isHistoryExchangeFlag() {
			return historyExchangeFlag;
		}
		public void setHistoryExchangeFlag(boolean historyExchangeFlag) {
			this.historyExchangeFlag = historyExchangeFlag;
		}
		public Company getCompany() {
			return company;
		}
		public void setCompany(Company company) {
			this.company = company;
		}
		public void setCompanyManager(CompanyManager companyManager) {
			this.companyManager = companyManager;
		}
		public boolean isHistoryFx() {
			return historyFx;
		}
		public void setHistoryFx(boolean historyFx) {
			this.historyFx = historyFx;
		}
		public HistoryExchangeRate getHistoryExchangeRate() {
			return historyExchangeRate;
		}
		public void setHistoryExchangeRate(HistoryExchangeRate historyExchangeRate) {
			this.historyExchangeRate = historyExchangeRate;
		}
		public HistoryExchangeRate getExchangeRate1() {
			return exchangeRate1;
		}
		public void setExchangeRate1(HistoryExchangeRate exchangeRate1) {
			this.exchangeRate1 = exchangeRate1;
		}
		public Date getValueDate() {
			return valueDate;
		}
		public void setValueDate(Date valueDate) {
			this.valueDate = valueDate;
		}
		public String getValueDateFormatter1() {
			return valueDateFormatter1;
		}
		public void setValueDateFormatter1(String valueDateFormatter1) {
			this.valueDateFormatter1 = valueDateFormatter1;
		}
		public String getFlagHit() {
			return flagHit;
		}
		public void setFlagHit(String flagHit) {
			this.flagHit = flagHit;
		}

			
	    
	}