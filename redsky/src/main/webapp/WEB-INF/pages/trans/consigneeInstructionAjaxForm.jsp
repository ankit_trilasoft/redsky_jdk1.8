<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Consignee Instruction" in Redsky.
 * @File Name	consigneeInstructionForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        22-Dec-2008
 * --%>

<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="consigneeInstructionDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='consigneeInstructionDetail.heading'/>"/>   

<script language="JavaScript">
 <sec-auth:authComponent componentId="module.script.form.agentScript">
 window.onload = function() { 
	trap();
	<sec-auth:authScript tableList="consigneeInstruction" formNameList="consigneeInstructionForm" transIdList='${serviceOrder.shipNumber}'>
	
	</sec-auth:authScript>
	
	}
	</sec-auth:authComponent>
	
	
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 
		trap();
	var elementsLen=document.forms['consigneeInstruction'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['consigneeInstruction'].elements[i].type=='text')
					{
						document.forms['consigneeInstruction'].elements[i].readOnly =true;
						document.forms['consigneeInstruction'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['consigneeInstruction'].elements[i].disabled=true;
					}
					
						
			}
			
	}
</sec-auth:authComponent>

function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		        	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		      }
		    }
		  }
		  
		  function right(e) {
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		else return true;
		}
function consigneeInstructionFormValue(){
     var shipNum = document.getElementById('shipNum').value;
     var url="consigneeInstructionCode.html?ajax=1&decorator=simple&popup=true&shipNum =" + encodeURI(shipNum);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse577;
     http2.send(null);  
}   

function handleHttpResponse577()
     {
             if (http2.readyState == 4)
             {
                 var results = http2.responseText
                 results = results.trim();  
                 results = results.replace('[','');
                 results=results.replace(']',''); 
                 document.getElementById('DaCode').value = results;
              }
}

function consigneeInstructionFormAddValue(){    
     var shipNum = document.getElementById('shipNum').value;
     var val = document.getElementById('consignmentInstructions').value;
     var DaCode = document.getElementById('DaCode').value;
     var sACode = document.getElementById('sACode').value;
     var bACode = document.getElementById('serviceOrder.bookingAgentCode').value;
     var brokerCode=document.getElementById('brokerCode').value;
     <c:set var="osaFlag" value="F"/>
     <configByCorp:fieldVisibility componentId="component.forwarding.consigneeInstructions.OSA">
     <c:set var="osaFlag" value="T"/>
     </configByCorp:fieldVisibility>
     
     var flagOsa = '${osaFlag}';
     
     if(val== 'NC'){
    	 var url="consigneeInstructionNCAddress.html?ajax=1&decorator=simple&popup=true&shipNum="+encodeURIComponent(shipNum);
	     http9.open("GET", url, true);
	     http9.onreadystatechange = handleHttpResponse555555;
	     http9.send(null);
	     }
	     else{
	    	 
	    	 if(flagOsa =='F' && val != 'O'){
	    	document.getElementById('consigneeAddress').value = '';
	     	document.getElementById('notification').value = '';
	    	 }
	     }  
     
     <configByCorp:fieldVisibility componentId="component.forwarding.consigneeInstructions.OSA">
     if(val== 'O'){
    	 document.getElementById('shipperAddress').value = "OMNI Shipping Services Inc."+ "\n" +"Wolga 12, 2491 BJ, Den Haag"+ "\n"+"Voerman International B.V. as agents for O.S.S.I.";
    	 document.getElementById('shipmentLocation').value='';
    	 document.getElementById('consigneeAddress').value='';
    	 document.getElementById('notification').value = "Overseas Shipper's Association (OSA)"+ "\n" +"P/a: Waalhaven Zuidzijde 19"+ "\n" +"3089 EC Rotterdam"+ "\n" +"The Netherlands"+ "\n" +"Phone: +31-10-5033835";
    	 
    var oriCountry = document.getElementById('oriCountry').value
    var destCountry = document.getElementById('destCountry').value
    	 if(oriCountry == 'United States' || destCountry == 'United States'){
    	 document.getElementById('specialInstructions').value = "BODY VAN DE BL : \nDESCRIPTION OF GOODDS : \nCONTRACTNUMBER OF OSA : \nOcean transportation will be arranged through OMNI Shipping Services, Inc., a tariffed and bonded non-vessel-operating common carrier licensed by the U.S. Federal Maritime Commission license number 013252N. Voerman International B.V. an authorized agent of OMNI Shipping Services.";
    	 }
     }else{
         document.getElementById('shipperAddress').value =   ''; 
         document.getElementById('notification').value ='';
         document.getElementById('specialInstructions').value =""; 
         
     }
     </configByCorp:fieldVisibility>
     if(DaCode!='' && DaCode!="null")
     {
     	if(val=='D' || val== 'S' || val== 'N' )
     	{
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse55;
	     http2.send(null);
	     }
	     else{
	    	 if(flagOsa =='F' && val != 'O'){
	    	document.getElementById('notification').value = '';
	    	 }
	     }
	    if(val== 'S')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http3.open("GET", url, true);
	     http3.onreadystatechange = handleHttpResponse555;
	     http3.send(null);
	     }
	     else{
	    	document.getElementById('consigneeAddress').value = '';
	     }
	     if(val== 'D')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http4.open("GET", url, true);
	     http4.onreadystatechange = handleHttpResponse5555;
	     http4.send(null);
	     }
	     else{
	    	document.getElementById('consigneeAddress').value = '';
	     }
	     if(val== 'BA')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(bACode)+ "&shipNum=" + encodeURI(shipNum);
	     http3.open("GET", url, true);
	     http3.onreadystatechange = handleHttpResponse555;
	     http3.send(null);
	     }
	     else{
	    	document.getElementById('consigneeAddress').value = '';
	     }
	     if(val== 'N')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http5.open("GET", url, true);
	     http5.onreadystatechange = handleHttpResponse55555;
	     http5.send(null);
	     }
	     else{
	    	 if(flagOsa =='F' && val != 'O'){
	     document.getElementById('consigneeAddress').value = '';
	    	 }
	     }
	}
	if(sACode!='' && sACode!="null")
     {
	if(val== 'SA'){
	     var url="consigneeDestinationSubAgentAddress.html?ajax=1&decorator=simple&popup=true&sACode =" + encodeURI(sACode)+ "&shipNum=" + encodeURI(shipNum);
	     http6.open("GET", url, true);
	     http6.onreadystatechange = handleHttpResponse007;
	     http6.send(null);
	     }
	     else{
	    	 if(flagOsa =='F' && val != 'O'){
	     document.getElementById('consigneeAddress').value = '';
	     document.getElementById('notification').value = '';
	    	 }
	     }
	     if(val== 'CD'){
	     var url="consigneeDestinationSubAgentAddress.html?ajax=1&decorator=simple&popup=true&sACode =" + encodeURI(sACode)+ "&shipNum=" + encodeURI(shipNum);
	     http8.open("GET", url, true);
	     http8.onreadystatechange = handleHttpResponse00007;
	     http8.send(null);
	     }
	     else{
	    	 
	    if(flagOsa =='F' && val != 'O'){
	     document.getElementById('consigneeAddress').value = '';
	     document.getElementById('notification').value = '';
	    }
	     }
	     }
	     
	if(brokerCode!='' && brokerCode!="null")
	{
		if(val== 'DN')
    {
    
	    var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(brokerCode)+ "&shipNum=" + encodeURI(shipNum);
	    http67.open("GET", url, true);
	     http67.onreadystatechange = handleHttpResponse567;
	     http67.send(null);
     }
    
	else{
    	document.getElementById('consigneeAddress').value = '';
          }
		}
	
    }
function handleHttpResponse567()
{
        if (http67.readyState == 4)
        {
            var results = http67.responseText
            results = results.trim();  
            var res = results.split("~"); 
           
            if(res[0] == '1')
            {
            res[0] ='';
            }
            else{
            res[0]=res[0]+"\n";
            }
            if(res[1] == '1')
            {
            res[1] = '';
            }
            else{
            res[1]=res[1]+"\n";
            }
            if(res[2] == '1')
            {
            res[2] = '';
            }
            else{
            res[2]=res[2]+"\n";
            }
            if(res[3] == '1')
            {
            res[3] = '';
            }
            else{
            res[3]=res[3]+"\n";
            }
            if(res[4] == '1')
            {
            res[4] = '';
            }
            else{
            res[4]=res[4]+",";
            }
            if(res[5] == '1')
            {
            res[5] = '';
            }
            else{
            res[5]="Phn : "+res[5];
            }
            if(res[6] == '1')
            {
            res[6] = '';
            }
            else{
            res[6]=res[6]+"\n"+"c/o ";
            }
            if(res[7] == '1')
            {
            res[7] = '';
            }
            else{
            res[7]=res[7]+' ';
            }
            if(res[8] == '1')
            {
            res[8] = '';
            }
            else{
            res[8]=res[8]+"\t";
            }
            if(res[16] == '1')
            {
            res[16] = '';
            }
            else{
            res[16]=res[16]+"\n";
            }
            if(res[17] == '1')
            {
            res[17] = '';
            }
            else{
            res[17]=res[17]+",";
            }
            if(res[18] == '1')
            {
            res[18] = '';
            }
            else{
            res[18]=res[18]+",";
            }
            if(res[20] == '1')
            {
            res[20] = '';
            }
            else{
            res[20]=res[20]+",";
            }
            document.getElementById('consigneeAddress').value=res[7]+res[6]+res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
            document.getElementById('notification').value = res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
             
        }

        
}

function handleHttpResponse55()
     {
             if (http2.readyState == 4)
             {
                 var results = http2.responseText
                 results = results.trim(); 
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.getElementById('notification').value = res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse555()
     {
             if (http3.readyState == 4)
             {
                 var results = http3.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+"\n"+"c/o ";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+' ';
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.getElementById('consigneeAddress').value=res[7]+res[6]+res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse5555()
     {
             if (http4.readyState == 4)
             {
                 var results = http4.responseText
                 results = results.trim();
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+",";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.getElementById('consigneeAddress').value=res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse55555()
     {
             if (http5.readyState == 4)
             {
                 var results = http5.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+' ,';
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+' ';
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+" ";
                 }
                 if(res[9] == '1')
                 {
                 res[9] = '';
                 }
                 else{
                 res[9]=res[9]+" ";
                 }
                 if(res[10] == '1')
                 {
                 res[10] = '';
                 }
                 else{
                 res[10]=res[10]+",";
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]="Phn :"+res[11];
                 }
                 if(res[12] == '1')
                 {
                 res[12] = '';
                 }
                 else{
                 if(res[11] != ''){
                 res[12]=","+res[12]+"\n";
                 }
                 else{
                 res[12]="Phn :"+res[12];
                 }
                 }
                 if(res[13] == '1')
                 {
                 res[13] = '';
                 }
                 else{
                 if(res[11] == '' && res[12] == ''){
                 res[13]="Phn :" +res[13];
                 }
                 else{
                 res[13]="," +res[13];
                 }
                 }
                 if(res[14] == '1')
                 {
                 res[14] = '';
                 }
                 else{
                 res[14]=res[14]+"\n";
                 }
                 if(res[15] == '1')
                 {
                 res[15] = '';
                 }
                 else{
                 res[15]=res[15]+",";
                 }
                 if(res[19] == '1')
                 {
                 res[19] = '';
                 }
                 else{
                 res[19]=res[19]+",";
                 }
                 if(res[21] == '1')
                 {
                 res[21] = '';
                 }
                 else{
                 res[21]=res[21]+",";
                 }
                 document.getElementById('consigneeAddress').value=res[7]+res[6]+res[8]+res[9]+res[10]+res[15]+res[19]+res[21]+res[14]+res[11]+res[12]+res[13];
              }
}
function handleHttpResponse555555()
{
        if (http9.readyState == 4)
        {
            var results = http9.responseText
            results = results.trim();  
            var res = results.split("~"); 
            if(res[0] == '1')
            {
            res[0] ='';
            }
            else{
            res[0]=res[0]+"\n";
            }
            if(res[1] == '1')
            {
            res[1] = '';
            }
            else{
            res[1]=res[1]+" ";
            }
            if(res[2] == '1')
            {
            res[2] = '';
            }
            else{
            res[2]=res[2]+"\n";
            }
            if(res[3] == '1')
            {
            res[3] = '';
            }
            else{
            res[3]=res[3]+"\n";
            }
            if(res[4] == '1')
            {
            res[4] = '';
            }
            else{
            res[4]=res[4]+",";
            }
            if(res[5] == '1')
            {
            res[5] = '';
            }
            else{
            	if(res[6]=='1' && res[7]=='1'){
            		res[5]="\nPhn: "+res[5];
            	}else{
            		res[5]="\nPhn: "+res[5]+',';
            	}
            }
            if(res[6] == '1')
            {
            res[6] = '';
            }
            else{
            	if(res[7]=='1' && res[5]==''){
            		res[6]="\nPhn: "+res[6]
            	}else{
            		res[6]=res[6]+',';
            	}
           }
            if(res[7] == '1')
            {
            res[7] = '';
            }
            else{
            	if(res[5]=='' && res[6]==''){
            		res[7]="\nPhn: "+res[7];
            	}else{
            		res[7]= res[7];
            	}
           }
            if(res[8] == '1')
            {
            res[8] = '';
            }
            else{
            res[8]=res[8]+" ";
            }
            if(res[9] == '1')
            {
            res[9] = '';
            }
            else{
            res[9]=res[9]+" ";
            }
            if(res[10] == '1')
            {
            res[10] = '';
            }
            else{
            res[10]=res[10]+",";
            }
            if(res[11] == '1')
            {
            res[11] = '';
            }
            else{
            res[11]=+res[11]+',';
            }
            document.getElementById('consigneeAddress').value=res[1]+res[0]+res[2]+res[3]+res[4]+res[9]+res[10]+res[11]+res[8]+res[5]+res[6]+res[7];
            document.getElementById('notification').value=res[1]+res[0]+res[2]+res[3]+res[4]+res[9]+res[10]+res[11]+res[8]+res[5]+res[6]+res[7];
        }
}
function handleHttpResponse007()
     {
             if (http6.readyState == 4)
             {
                 var results = http6.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+"\n";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5]+",";
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]="\n Phn : "+res[8];
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]=res[11]+",";
                 }
                 document.getElementById('consigneeAddress').value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
                 document.getElementById('notification').value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
              }
}
function handleHttpResponse00007()
     {
             if (http8.readyState == 4)
             {
                 var results = http8.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[10] == '1')
                 {
                 res[10] = '';
                 }
                 else{
                 res[10]=res[10]+" ";
                 }
                 if(res[9] == '1')
                 {
                 res[9] = '';
                 }
                 else{
                 res[9]=res[9]+"\n"+"c/o ";
                 }
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+"\n";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5]+",";
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]="Phn : "+res[8];
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]=res[11]+",";
                 }
                 document.getElementById('consigneeAddress').value = res[10]+res[9]+res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
                 document.getElementById('notification').value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
              }
}


function shipmentLocationAddValue(){   
       var osACode = document.getElementById('osACode').value;
       if(osACode!='' && osACode!="null")
        {
 	     var url="shipmentLocationAddress.html?ajax=1&decorator=simple&popup=true&osACode =" + encodeURI(osACode);
	     http7.open("GET", url, true);
	     http7.onreadystatechange = handleHttpResponse7777;
	     http7.send(null);
	     }
}

function handleHttpResponse7777()
     {
var val = document.getElementById('consignmentInstructions').value        
	if (http7.readyState == 4)
             {
                 var results = http7.responseText
                 results = results.trim();  
                 if(val== 'O'){
                	 <configByCorp:fieldVisibility componentId="component.forwarding.consigneeInstructions.OSA">
                	 document.getElementById('shipmentLocation').value = "";
                	 </configByCorp:fieldVisibility>
                 }else{
                 document.getElementById('shipmentLocation').value = results;
                 }
              }
}





function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http555 = getHTTPObject();
  
 function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    
    
    function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();
    
    function getHTTPObject3()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject3();
  
  
  function getHTTPObject6()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http6 = getHTTPObject6();
  
   function getHTTPObject7()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http7 = getHTTPObject7();  
    
  function getHTTPObject8()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http8 = getHTTPObject8(); 
    
    function getHTTPObject9()
    {
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp)
            {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
        var http9 = getHTTPObject9();
    
// function for validations.
var form_submitted = false;
function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}

  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum = document.getElementById('serviceOrder.id').value;
	var seqNm = document.getElementById('serviceOrder.sequenceNumber').value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum = document.getElementById('serviceOrder.id').value;
	var seqNm = document.getElementById('serviceOrder.sequenceNumber').value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'containersAjaxList.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.getElementById('customerFile.id').value
 var soIdNum=document.getElementById('serviceOrder.id').value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
function goToUrl(id)
	{
		location.href = "containersAjaxList.html?id="+id;
	}
	
function checkLengthLocation(){
	var txt1 =  document.getElementById('shipmentLocation').value;
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Shipment Location.");
		 document.getElementById('shipmentLocation').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}
}   	

function checkLengthAddress(){
	var txt1 =  document.getElementById('shipperAddress').value;
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Shipper.");
		 document.getElementById('shipperAddress').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}
} 

function checkLengthConsignee(){
	var txt1 =  document.getElementById('consigneeAddress').value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Consignee.");
		 document.getElementById('consigneeAddress').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 

function checkLengthNotify(){
	var txt1 = document.getElementById('notification').value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Notify.");
		document.getElementById('notification').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 

function checkLengthSpecialInstBol(){
	var txt1 = document.getElementById('specialInstructions').value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Special Instructions for OBL.");
		document.getElementById('specialInstructions').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 

function getHTTPObject567()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http67 = getHTTPObject567();
    
</script>
<style type="text/css">

/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

</head>   

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="id" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="consigneeInstruction.id"/>
<s:hidden name="sid" value="<%=request.getParameter("id")%>"/>
<s:hidden name="serviceOrder.sid" id="serviceOrder.sid" value="%{serviceOrder.sid}"/>
<s:hidden name="soId" id="soId" value="%{serviceOrder.sid}"/>
<c:set var="shipNum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="shipNum" id="shipNum" value="${serviceOrder.shipNumber}"/>

<s:hidden name="oriCountry" id="oriCountry" value="${serviceOrder.originCountry}"/>
<s:hidden name="destCountry" id="destCountry" value="${serviceOrder.destinationCountry}"/> 

<s:hidden name="consigneeInstruction.serviceOrderId" id="consigneeInstruction.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="consigneeInstruction.corpID" />
<s:hidden name="serviceOrder.sequenceNumber" id="serviceOrder.sequenceNumber"/>
<s:hidden name="oaCode" id="oaCode"/>
<s:hidden name="DaCode" id="DaCode"/>
<s:hidden name="sACode" id="sACode"/>
<s:hidden name="brokerCode" id="brokerCode"/>
<s:hidden name="osACode" id="osACode"/>
<s:hidden name="serviceOrder.bookingAgentCode" id="serviceOrder.bookingAgentCode"/>
<s:hidden name="serviceOrder.ship" id="serviceOrder.ship"/> 
<s:hidden name="customerFile.id" id="customerFile.id" />
<s:hidden name="serviceOrder.id" id="serviceOrder.id"/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="forwardingAjaxVal" value="Yes"/>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="msgClicked" />
<div id="Layer1" style="width:100%">



<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
                     <tr>
                     <td align="right" class="listwhitetext">Consignment&nbsp;Instructions</td>
                    
									<td align="left" style="width:100px"><configByCorp:customDropDown listType="map" list="${specific_isactive}" fieldValue="${consigneeInstruction.consignmentInstructions}"
		       attribute="id=consignmentInstructions class=list-menu name=consigneeInstruction.consignmentInstructions  style=width:307px  headerKey='' headerValue='' onchange='consigneeInstructionFormAddValue();shipmentLocationAddValue()' tabindex='1' "/></td>
									
									<%-- <s:select
										cssClass="list-menu" name="consigneeInstruction.consignmentInstructions" id="consignmentInstructions"
										list="%{specific}" cssStyle="width:307px" onchange="consigneeInstructionFormAddValue();shipmentLocationAddValue()" tabindex="1"/></td> --%>
                       <td colspan="5">
                       <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				        <tr> 
				        <td  class="listwhitetext" align="right"  style="width:20px;"></td>	
				         <td  class="listwhitetext" align="right" >Express&nbsp;Release</td>	  	   		
	  			         <td align="left"  width="50px"><s:checkbox name="consigneeInstruction.expressRelease" value="${consigneeInstruction.expressRelease}" fieldValue="true" tabindex="2"/></td>
				         <td  class="listwhitetext" align="right" >Collect</td>	  	   		
	  			         <td ><s:checkbox name="consigneeInstruction.collect" value="${consigneeInstruction.collect}" fieldValue="true" tabindex="3"/></td>
				        </tr>
				        </table>
				      </td>
                     </tr>
                     
                     <tr>
                     <td align="right"  class="listwhitetext" >Shipper</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.shipperAddress" id="shipperAddress" cssStyle="width:303px;height:75px;" cssClass="textarea" tabindex="4" onkeypress="return checkLengthAddress();"/></td>
                     <td align="right"  class="listwhitetext" style="width:115px;">Shipment Location</td>
                     <td align="left" colspan="4" class="listwhitetext"><s:textarea name="consigneeInstruction.shipmentLocation" id="shipmentLocation" cssStyle="width:275px;height:75px;" tabindex="5" cssClass="textarea" onkeypress="return checkLengthLocation();"/></td>
                     
                     </tr>
                     
                     <tr>
                     <td align="right"  class="listwhitetext">Consignee</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.consigneeAddress" id="consigneeAddress" cssStyle="width:303px;height:95px;" tabindex="6" cssClass="textarea" onkeypress="return checkLengthConsignee();"/></td>
                     <td align="right"  class="listwhitetext">Notify</td>
                     <td align="left" colspan="4" class="listwhitetext"><s:textarea name="consigneeInstruction.notification" id="notification" cssStyle="width:275px;height:95px;" tabindex="7" cssClass="textarea" onkeypress="return checkLengthNotify();"/></td>
                 
                     </tr>
                     <tr>
                     <td align="right"  class="listwhitetext">Special&nbsp;Instructions&nbsp;for&nbsp;OBL</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.specialInstructions" id="specialInstructions"  cssStyle="width:303px;height:55px;" tabindex="8"  cssClass="textarea" onkeypress="return checkLengthSpecialInstBol();" /></td>
                     </tr>		
										</tbody>
									</table>
						<sec-auth:authComponent componentId="module.button.consignee.saveButton">														              
					<s:submit cssClass="cssbuttonA" key="button.save" method="save" cssStyle="width: 55px; margin-left: 144px; margin-top: 10px;" tabindex="9" />   
					<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" tabindex="10"/>
					</sec-auth:authComponent>
									</td>									
								</tr>
</tbody>
</table>	

<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${consigneeInstruction.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="consigneeInstruction.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${consigneeInstruction.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty consigneeInstruction.id}">
								<s:hidden name="consigneeInstruction.createdBy"/>
								<td><s:label name="createdBy" value="%{consigneeInstruction.createdBy}"/></td>
							</c:if>
							<c:if test="${empty consigneeInstruction.id}">
								<s:hidden name="consigneeInstruction.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${consigneeInstruction.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="consigneeInstruction.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${consigneeInstruction.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty consigneeInstruction.id}">
							<s:hidden name="consigneeInstruction.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{consigneeInstruction.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty consigneeInstruction.id}">
							<s:hidden name="consigneeInstruction.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>				
					
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
			
</div>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
  
<script type="text/javascript">
    try{
    consigneeInstructionFormValue();
   }
   catch(e){}
   
</script>  
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}&msgClicked=${msgClicked}" ></c:redirect>
			</c:if>
		</c:if>
		}
		catch(e){}
</script>