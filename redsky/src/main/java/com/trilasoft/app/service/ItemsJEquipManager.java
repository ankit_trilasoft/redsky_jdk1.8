package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ItemsJEquip;

public interface ItemsJEquipManager extends GenericManager<ItemsJEquip, Long> {   
    public List<ItemsJEquip> findByType(String type, String wcontract, Long id);   
    public List<ItemsJEquip> findById(Long id);
    public List<ItemsJEquip> findByDescript(String descript);
    public List<ItemsJEquip> findByDescript(String type,String descript, String wcontract, Long id1);
    public List<ItemsJEquip> findSection(String itemType, String wcontract);
    public List findMaterialExtraInfo(Long id,String workTicket, String contract,String corpID, String type);
    public List salesRequestList(Long id,String ship,String corpID, String type);
    public List overallSalesRequestList(Long id,String ship,String corpID, String type,String begin,String end);
    public List findMaterials(Long id,String workTicket, String contract,String corpID, String type);
    public List searchMaterials(String description,String workTicket, String contract,String corpID, String type);
    public List findContractFromBilling(String shipNumber);
    public List findContractFromCustomerFile(String shipNumber);
    public List checkforBilling(String shipNumber);
    public List findMaterialExtraInfoByContract(String contract,String type,String corpID,String division,String resource);
    public List materialByCategory(String type,String corpID);
    public List findMaterialExtraInfoByContractForSales(String contract,String type,String corpID);
    public List findMaterialByContract(String contract,String type,String corpID);
    //Added on 5 Sept
    //public void save(ItemsJbkEquip itemsJbkEquip);   
    public List miscellaneousRequestList(Long id,String ship,String corpID, String type,String division,String resource);
    public List findMaterialByContractTab(String contract,String type,String corpID);
    
    public List findByDescription(String descript, String contract, String corpID, String type);
    public List findWareHouseDesc(String wrehouse, String sessionCorpID);
    public List getListValue(String descript, String type, Boolean controlled);
    
    public List findContractAndType(String corpID);
    public List findChargesAndType(String contractType, String corpID);
    public List findResourceContractByPartnerId(Long parentId, String corpID);
    public List getResource(String sessionCorpID,String type,String descript);
    public List findCharges(String contract,String chargeCode,String chargeDesc,String sessionCorpID);
    public List findContracts(String contractType,String contractDesc,String sessionCorpID);
    public List findResource(String corpID);
    public List searchResourceList(String resource, String ticket, String contract, String sessionCorpID, String category);
	public List findMaterialByCategory(String category, String sessionCorpID);
	public List findResourceTemplate(String contractType,String corpID);
	public List checkDuplicateResource(String descript, String sessionCorpID);
	public List checkDuplicateResourceDayBasis(String descript, String sessionCorpID,String day,String shipNumber);
	public List findMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division,String resource);
	public List findMaterialExtraMasterInfoByContract(String cont, String sessionCorpID,String cat);
	public List getMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division);
	public Map findInvDescriptionList(String resource,String division,String category,String sessionCorpID);
	public void deleteInventory(Long id);
	public List checkContract( Long id ,String sessionCorpID , String shipNumber);
	public List findResourceTemplateForAccPortal(String contractType,String corpID);
	public String getResourceCategoryValue(String sessionCorpID,Long ticket,String resourceType,String resourceDescription);
    }