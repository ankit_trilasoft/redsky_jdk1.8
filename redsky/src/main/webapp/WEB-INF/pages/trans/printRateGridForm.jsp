<%@page import="java.util.SortedMap"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
  
<head>   
    <title><fmt:message key="partnerRateGridForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRateGridForm.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
    <style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
 

 <script type="text/javascript">
  

</script> 
<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:550px;
}
input.checkbox {border:2px solid red;}
.input-text {
border:1px solid #000000;
color:#000000;
font-family:arial,verdana;
font-size:12px;
height:15px;
text-decoration:none;
}
.list-menu {
border:1px solid #000000;
color:#000000;
font-family:arial,verdana;
font-size:12px;
height:19px;
overflow:hidden;
padding:1px;
text-decoration:none;
}
.input-textUpper {
background-color:#E3F1FE;
border:1px solid #444444;
color:#000000;
font-family:arial,verdana;
font-size:12px;
height:15px;
text-decoration:none;
}
textarea.textarea {
border:1px solid #000000;
}
.p-table  {
border:1px solid #000000;
border-collapse:collapse;
}
.partner-text {
background-color:#E3F1FE;
color:#15428B;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
line-height:20px;
text-decoration:none;
}
</style>
</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerId}" />
<c:set var="fileID" value="%{partnerId}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>

<s:form id="partnerRateGridForm"  name="partnerRateGridForm" action="" method="post" validate="true">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<s:hidden name="partnerId" value="${partnerPublic.id}" />
<s:hidden name="id" value="${partnerRateGrid.id}"/> 
<s:hidden name="showRateGrig" value="<%=request.getParameter("showRateGrig")%>"/>   
<c:set var="showRateGrig" value="<%=request.getParameter("showRateGrig")%>"/>
<s:hidden name="byAdmin" value="<%=request.getParameter("partnerId")%>"/>   
<c:set var="byAdmin" value="<%=request.getParameter("partnerId")%>"/>
<s:hidden name="partnerRateGrid.id" /> 
<s:hidden name="standardOriginNote" value="${standardOriginNote}"/>
<s:hidden name="standardDestinationNote" value="${standardDestinationNote}"/>
<s:hidden name="originSubmitCount" value="${originSubmitCount}"/>
<s:hidden name="destinationSubmitCount" value="${destinationSubmitCount}"/>
<s:hidden name="standardCountryHauling" value="${standardCountryHauling}"/>
<s:hidden name="standardCountryCharges" value="${standardCountryCharges}"/>
<s:hidden name="secondDescription" />
<s:hidden name="userType" value="${userType}"/> 
<c:set var="userType" value="${userType}"/>
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" /> 
<s:hidden name="description" />
<div id="layer1" style="width:80%">
<div id="newmnav">
	<ul> 
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix Detail</span></a></li> 
	</ul> 
	</div>
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;"></div>      
	<div id="content" align="center">
		<div id="liquid-round-top">
   		<div class="top" style=" "><span></span></div>
   			<div class="center-content"> 
				<table cellspacing="1" cellpadding="1" border="0" width="100%" class="detalTabLabel" style="margin: 3px;">
					<tbody>
					      <tr>
					        <td align="right" width="140px" class="listwhitetext"><fmt:message key="partnerRateGrid.partnerCode" /></td>
							<td align="left" width="100px" ><s:textfield cssClass="input-text"  name="partnerRateGrid.partnerCode" cssStyle="width:100px" maxlength="8" readonly="true" value="${partnerPublic.partnerCode}" tabindex="1"/></td>
					        <td align="left" width="100px" colspan="2" ><s:textfield cssClass="input-text"  name="partnerRateGrid.partnerName" cssStyle="width:225px" maxlength="8" readonly="true" value="${partnerPublic.lastName}" tabindex="2"/></td>
					       </tr>
					      <tr>  
					         <td align="right"  class="listwhitetext"><fmt:message key="partnerRateGrid.status"/></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.status" cssStyle="width:100px" readonly="true" tabindex="3"/></td>  
						     <td align="right" width="80px" class="listwhitetext">Effective&nbsp;Date<font color="red" size="2">*</font></td>
							 <c:if test="${not empty partnerRateGrid.effectiveDate}"> 
					           <s:text id="partnerRateGridEffectiveDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.effectiveDate"/></s:text>
				                 <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate" value="%{partnerRateGridEffectiveDate}" readonly="true" cssClass="input-text" size="8" tabindex="4"/>
				                <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].effectiveDate,'calender',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
				                 </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.effectiveDate}"> 
					             <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate"  readonly="true" cssClass="input-text" size="8" tabindex="4"/>
					              <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].effectiveDate,'calender',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
					             </td>
					         </c:if>
					         <c:if test="${partnerRateGrid.status=='Submitted'}"> 
					          <td align="right" class="listwhitetext" ><fmt:message key="partnerRateGrid.publishTariff" /></td>
					          <c:if test="${partnerRateGrid.publishTariff}">
				                 <td align="center"  valign="middle"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				              </c:if>
				              <c:if test="${partnerRateGrid.publishTariff=='false'}">
				                 <td align="center"  valign="middle"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				              </c:if> 
						     </c:if>
						     <c:if test="${partnerRateGrid.status!='Submitted'}">
						       <s:hidden name="partnerRateGrid.publishTariff" /> 
						     </c:if>
						  </tr>
						  <tr>  
							<td align="right" width="100px"  class="listwhitetext"><fmt:message key="partnerRateGrid.tariffApplicability"/><font color="red" size="2">*</font></td>
							<td align="left" ><s:select list="%{tariffApplicabilityList}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue="" name="partnerRateGrid.tariffApplicability" tabindex="6" onchange="fillStandardInclusions(); "/></td>
						    <%--<td align="right" width="100px"  class="listwhitetext">Scope<font color="red" size="2">*</font></td>
							 <c:if test="${partnerRateGrid.status!='Submitted'&& partnerRateGrid.status!='Withdraw'}">
							<td align="left" ><s:select list="%{tariffScopeList}" value="%{multipleTariffScope}" cssClass="list-menu" cssStyle="width:135px;height:50px"  headerKey="" headerValue="" name="partnerRateGrid.tariffScope" tabindex="7" multiple="true"/></td>
						    <td class="listwhitetext" width="240px" align="left">
                              * Use Control + mouse to select multiple scopes
                            </td>
                            </c:if>
                            <c:if test="${partnerRateGrid.status=='Submitted'||partnerRateGrid.status=='Withdraw'}"> 
                            <td align="left" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:290px" name="partnerRateGrid.tariffScope"/></td>
                            </c:if>
						 --%></tr>
						 <tr> 
							 <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.metroCity" /><font color="red" size="2">*</font></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.metroCity" cssStyle="width:100px" maxlength="50" tabindex="8"/></td>
							 <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.currency" /><font color="red" size="2">*</font></td> 
							 <td align="left"><s:select list="%{currencyList}" cssClass="list-menu" cssStyle="width:135px" headerKey="" headerValue="" name="partnerRateGrid.currency" tabindex="9"/></td>
							 
						 </tr>
						 <tr>
						     <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.terminalCountry"/></td>
							 <td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.terminalCountry" cssStyle="width:100px" maxlength="3" readonly="true" value="${partnerPublic.terminalCountryCode}" tabindex="10"/></td>
						      <td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.state" /></td> 
							 <td align="left"><s:select list="%{stateList}" id="state" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.state" value="%{partnerRateGrid.state}" tabindex="11"/></td>  
						     
						 <tr>
						 </tr> 
						
						<%-- 	<td align="right" class="listwhitetext"><fmt:message key="partnerRateGrid.endDate"/></td>
							<c:if test="${not empty partnerRateGrid.endDate}"> 
					           <s:text id="partnerRateGridEndDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.endDate"/></s:text>
				               <td><s:textfield id="endDate" name="partnerRateGrid.endDate" value="%{partnerRateGridEndDate}"  readonly="true" cssClass="input-text" size="8" tabindex="8"/>
				                <img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].endDate,'calender1',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
				                </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.endDate}"> 
					             <td><s:textfield id="endDate" name="partnerRateGrid.endDate"  readonly="true" cssClass="input-text" size="8" tabindex="8"/>
					             <img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['partnerRateGridForm'].endDate,'calender1',document.forms['partnerRateGridForm'].dateFormat.value); return false;" />
					             </td>
					          </c:if> --%>
						
						<tr> 
							
						    
						 </tr>
						 <tr><td width="100%" colspan="6"> 
						<table width="100%" border="0"  class="detalTabLabel" style="margin: 0px;padding: 0px;">
						<tr><td colspan="8">
						 <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                          <tr>
                            <td class="headtab_left"> </td>
                            <td class="headtab_center" >&nbsp;Distance Adjustment Factors </td>
                            <td width="28" valign="top" class="headtab_bg"></td>
                            <td class="headtab_bg_special" >&nbsp; </td>
                            <td class="headtab_right"> </td>
                           </tr>
                         </table>
                        </td>
                       </tr>
                       <tr>
                       <td></td>
                       <td></td>
                       <td align="left"   class="listwhitetext">(Miles)</td>
                       <td align="left"   class="listwhitetext">(Kms)</td>
                       <td align="left"   class="listwhitetext">Extra&nbsp;Charges</td>
                       </tr>	
						<tr> 
							 <td align="right" class="listwhitetext" width="144px" ><fmt:message key="partnerRateGrid.serviceRangeMiles" /><font color="red" size="2">*</font></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext" width="83px" ><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles" cssStyle="text-align:right" size="10" maxlength="6"  onchange="convertToKm();" tabindex="12" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext" width="85px"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms" cssStyle="text-align:right" size="10" maxlength="6" onchange="convertToMiles();" tabindex="13" onkeydown="return onlyNumsAllowed(event)"/></td>
						     <td align="left" class="listwhitetext">Standard Charges Apply</td>
						 </tr>  
						 <tr> 
						     <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles2" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm2();" tabindex="14" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms2" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles2();" tabindex="15" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra2" size="6" maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="16" onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles3" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm3();" tabindex="17" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms3" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles3();" tabindex="18" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra3" size="6"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="19" onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right"   class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles4" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm4();" tabindex="20" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms4" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles4();" tabindex="21" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra4" size="6"  maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="22"  onchange="checkFloat(this)"/>(per cwt)</td>
						 </tr>
						 <tr> 
							 <td></td>
							 <td align="right" class="listwhitetext">Upto</td>
							 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeMiles5"size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToKm5();" tabindex="23" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeKms5" size="10" maxlength="6" cssStyle="text-align:right" onchange="convertToMiles5();" tabindex="24" onkeydown="return onlyNumsAllowed(event)" /></td>
						     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  name="partnerRateGrid.serviceRangeExtra5" size="6" maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="25"  onchange="checkFloat(this)" />(per cwt)</td>
						 </tr>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Base Rate: Flat Charges per Container </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
				     	<%
						 HashMap <String, Set> partnerRateGridWeightMap1 = (HashMap <String, Set>)request.getAttribute("partnerRateGridWeightMap");
					     Iterator mapIteratorFlate = partnerRateGridWeightMap1.entrySet().iterator();
					     while (mapIteratorFlate.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIteratorFlate.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridWeightMap1.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }
							   if((parameter.equals("Flat$Rate"))|| (parameter.equals("Flat$RateHigh"))){
							   %>
							   
					        
					          
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR"); 
					           String[] detailsArray=basisDisplay.toString().split("#");
					           String grid=detailsArray[0];
					           String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	  %> 
					        	<tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					        	<td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" tabindex="<%=indexFlate%>" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td> 
					        	</tr> 
					         <% }
					        %>
					          <% if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	   %>
					        	   <tr>
					               <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
                                   <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" tabindex="<%=indexFlate%>"  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td> 
                                   </tr> 
					          <% } %>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	   %> 
					        	<tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					            <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" tabindex="<%=indexFlate%>" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             </tr>
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
				               String flateLavel="";
				               String indexFlate="26";
				               if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="20'Loose Container";
				            	   indexFlate="26";
				               } 
				               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
				            	   flateLavel="40'Loose Container";
				            	   indexFlate="27";
				               }
				               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
				            	   flateLavel="40'HLoose Container";
				            	   indexFlate="28";
				               }
					        	  %> 
					        	  <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	  <s:hidden name="<%=gridSection+"_"+editable+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					         <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" tabindex="<%=indexFlate%>" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          </tr>
					          <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  String flateLavel="";
				                  String indexFlate="26";
				                  if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="20'Loose Container";
					            	   indexFlate="26";
					               } 
					               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="40'Loose Container";
					            	   indexFlate="27";
					               }
					               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
					            	   flateLavel="40'HLoose Container";
					            	   indexFlate="28";
					               }
					        	   %> 
					        	   <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					             <td ><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" tabindex="<%=indexFlate%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          </tr>
					          <% }%>
					         
							  <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
				                 String flateLavel="";
				                 String indexFlate="26";
				                 if(grid.equals("L20")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="20'Loose Container";
					            	   indexFlate="26";
					               } 
					               if(grid.equals("L40")&& parameter.equals("Flat$Rate")){
					            	   flateLavel="40'Loose Container";
					            	   indexFlate="27";
					               }
					               if(grid.equals("L40")&& parameter.equals("Flat$RateHigh")){
					            	   flateLavel="40'HLoose Container";
					            	   indexFlate="28";
					               }
					        	  %>
					        	  <tr>
					            <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=flateLavel%></td> 
					        	  <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
					             <td ><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					             </tr>
					          <% }%>  
							  
						 <% }}%> 
						<tr><td width="100%" colspan="6">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="detalTabLabel" style="margin: 0px; padding: 2px;">
						<tr><td colspan="8">
						 <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Base Rate: Charges per CWT </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                        </td>
                       </tr>			
	                   <tr>
						<td align="right" colspan="2" class="listwhitetext" ></td> 
						<td align="center" class="listwhitetext" ></td>
					    <td align="center" class="listwhitetext"><b>20' Std</b></td> 
					    <td align="center" class="listwhitetext"><b>40' Std./High</b></td>
						<td align="center" class="listwhitetext"><b>Liftvan</b></td>
						<td align="center" class="listwhitetext"><b>Liftvan</b></td>
						<%-- <td align="center" class="listwhitetext"><b><fmt:message key="partnerRateGrid.tariffFalgGRP" /></b></td>--%> 
					  </tr> 
					  <tr>
						<td align="right" colspan="2" class="listwhitetext" ></td> 
						<td align="center" class="listwhitetext" ><b>Air</b></td>
					    <td align="center" class="listwhitetext"><b>Cont. Loose</b></td> 
					    <td align="center" class="listwhitetext"><b>Cont. Loose</b></td>
						<td align="center" class="listwhitetext"><b>LCL</b></td>
						<td align="center" class="listwhitetext"><b>FCL</b></td>
						<%-- <td align="center" class="listwhitetext"><b><fmt:message key="partnerRateGrid.tariffFalgGRP" /></b></td>--%> 
					  </tr> 
					  <tr>
				         <td align="right" colspan="2" class="listwhitetext" ><b>Applicable</b></td>
				         <c:if test="${partnerRateGrid.tarriffFlagAir}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tarriffFlagAir=='false'}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagL20}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagL20=='false'}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagL40}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagL40=='false'}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagLCL}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagLCL=='false'}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagFCL}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/checked.png'/>" /></td>
				          </c:if>
				          <c:if test="${partnerRateGrid.tariffFlagFCL=='false'}">
				          <td align="center"  valign="top"> <img align="top" class="openpopup" width="13" name="" height="13"  src="<c:url value='/images/unchek.png'/>" /></td>
				          </c:if>
						 <s:hidden name="partnerRateGrid.tarriffFlagAir" value="${partnerRateGrid.tarriffFlagAir}" />
						 <s:hidden name="partnerRateGrid.tariffFlagL20" value="${partnerRateGrid.tariffFlagL20}" />
						 <s:hidden name="partnerRateGrid.tariffFlagL40" value="${partnerRateGrid.tariffFlagL40}" />
						 <s:hidden name="partnerRateGrid.tariffFlagLCL" value="${partnerRateGrid.tariffFlagLCL}" />
						 <s:hidden name="partnerRateGrid.tariffFlagFCL" value="${partnerRateGrid.tariffFlagFCL}" />
						 <td align="center"  valign="top" ></td>
						 <td align="center"  valign="top"></td>
						 <td align="center" valign="top"></td>
						 <td align="center" valign="top"></td>
						<%-- <td align="center" valign="top" width="100px"><s:checkbox name="partnerRateGrid.tariffFalgGRP" value="${partnerRateGrid.tariffFalgGRP}" fieldValue="true" disabled="disabled" onclick="readOnlyOnclick('GRP_Y_');" tabindex="29"/></td> --%>
				       </tr>
				       
				     	<%
						 HashMap <String, Set> partnerRateGridWeightMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridWeightMap");
					     Iterator mapIterator = partnerRateGridWeightMap.entrySet().iterator();
					     while (mapIterator.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridWeightMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }
							   if((!(parameter.equals("Flat$Rate")))&&(!(parameter.equals("Flat$RateHigh")))){
							   %>
							   
					        
					          <tr>
					          <td align="right" colspan="2" class="listwhitetext" style="font-weight: bold;"><s:hidden name="<%=gridSection+"_"+parameter+"_Label"%>" value="<%=label%>"/><%=label%></td> 
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR"); 
					           String[] detailsArray=basisDisplay.toString().split("#");
					           String grid=detailsArray[0];
					           String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	<s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" /></td>  
					         <% }else{ 
					        	 
					         %> 
					            
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					          <% if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %>
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
                                   <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% } else {%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             
					         <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	  <s:hidden name="<%=gridSection+"_"+editable+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					        	   %> 
					        	   <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/> 
					             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
					        	  %>
					        	  <s:hidden name="<%=gridSection+"_"+parameter+"_Basis"%>" value="<%=basis%>"/>  
					             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					          <% }else{%>
					        	 <td><s:hidden id="" name=""  /></td>  
					         <% }%> 
					         
					        
					        
							  </tr>    
						 <% }}%>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Additional Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>	   
					     
						 <%  HashMap <String, Set> partnerRateGridChargesMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridChargesMap");
					     Iterator mapIterator1 = partnerRateGridChargesMap.entrySet().iterator();
					     while (mapIterator1.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator1.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridChargesMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }%> 
					          <tr>
					           <c:set var="labelValue"  value="<%=label%>" scope="request"/>
					           <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=gridSection+"_"+parameter+"_Label"%>"  cssClass="input-text"  value="<%=label%>"  onblur="clickrecall(this,'${labelValue}')" onclick="clickclear(this, '${labelValue}')" onchange="checkChargeHauling('AIR_Y_');checkChargeHauling('L20_Y_');checkChargeHauling('L40_Y_');checkChargeHauling('LCL_Y_');checkChargeHauling('FCL_Y_');"/></td>  
					         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR");
					                  String[] detailsArray=basisDisplay.toString().split("#");
					                  String grid=detailsArray[0];
					                  String editable=detailsArray[1];
					                  String textValue= detailsArray[2];
					                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
					                	  textValue="";  
					                  }
					                  String basis=detailsArray[3];
					        	  %> 
					        	 <c:set var="basis"  value="<%=basis%>" scope="request"/>
					        	<td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>  
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>    
					        	
					             
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (tdHashMap.containsKey("L20"))){
					        	 String basisDisplay=(String)tdHashMap.get("L20");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (!tdHashMap.containsKey("L20"))&&(tdHashMap.containsKey("L40"))){
					        	 String basisDisplay=(String)tdHashMap.get("L40");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }
					         else if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&((tdHashMap.containsKey("LCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("LCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td> 
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& ((tdHashMap.containsKey("FCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("FCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& (!(tdHashMap.containsKey("FCL")))&& ((tdHashMap.containsKey("GRP")))){
					        	 String basisDisplay=(String)tdHashMap.get("GRP");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					          if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %>
					        	    
                                 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% } else {%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
							  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	   %> 
					        	   
					            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					             
					         <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
							   String[] detailsArray=basisDisplay.toString().split("#");
							   String grid=detailsArray[0];
							   String editable=detailsArray[1];
				               String textValue= detailsArray[2];
				               if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				               String basis=detailsArray[3];
					        	  %> 
					        	   
					         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
							  
							   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
							      String[] detailsArray=basisDisplay.toString().split("#");
							      String grid=detailsArray[0];
							      String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
					        	   %> 
					        	   
					             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
					          <% }else{%>
					        	 <td style="margin: 0px; padding: 2px;" ><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }%>
					         
						 <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
							     String[] detailsArray=basisDisplay.toString().split("#");
							     String grid=detailsArray[0];
							     String editable=detailsArray[1];
				                 String textValue= detailsArray[2];
				                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                 String basis=detailsArray[3];
					        	  %>
					        	    
					             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>"  value="<%=textValue%>" /></td> 
					          <% }else{%>
					        	 <td><s:hidden id="" name=""  /></td>  
					         <% }%>
					        
							  </tr>    
						 <% } %>
						 <tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Hauling Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						 
					    <%  HashMap <String, Set> partnerRateGridHaulingMap = (HashMap <String, Set>)request.getAttribute("partnerRateGridHaulingMap");
					     Iterator mapIterator2 = partnerRateGridHaulingMap.entrySet().iterator();
					     while (mapIterator2.hasNext()) {
					    	  Map.Entry entry = (Map.Entry) mapIterator2.next();
					          String key = (String) entry.getKey();
					          String[] str1 = key.toString().split("#");
					          String gridSection=str1[1];
					          String label=str1[2];
					          String parameter=str1[3];
					          Set displayList= partnerRateGridHaulingMap.get(key);
					          
					          Iterator it=displayList.iterator();  
					          HashMap tdHashMap=new LinkedHashMap(); 
					           while(it.hasNext()){ 
								   String details=(String)it.next();
								   String[] detailsArray=details.toString().split("#");
								   String grid=detailsArray[1];
								   String display=detailsArray[1]+"#"+detailsArray[2]+"#"+detailsArray[3]+"#"+detailsArray[4];
								   String basis=detailsArray[4];
								   tdHashMap.put(detailsArray[1].trim(),display); 
							   }%>  
							    <tr>
					           <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=gridSection+"_"+parameter+"_Label"%>"  cssClass="input-text"  value="<%=label%>" readonly="true" size="12" onfocus="checkChargeHauling('AIR_Y_');checkChargeHauling('L20_Y_');checkChargeHauling('L40_Y_');checkChargeHauling('LCL_Y_');checkChargeHauling('FCL_Y_');"/>
					           <c:if test="${partnerRateGrid.status!='Submitted' && partnerRateGrid.status!='Withdraw'}"> 
					           <img align="top"  name="<%="b"+gridSection+"_"+parameter+"_Label"%>"  onclick="findPortName(this);" src="${pageContext.request.contextPath}/images/question.png"/>
					           <img align="top" class="openpopup" width="17" name="<%="a"+gridSection+"_"+parameter+"_Label"%>" height="20" onclick="putPortName(this);" src="<c:url value='/images/open-popup.gif'/>" />
					           </c:if>
					           <c:if test="${partnerRateGrid.status=='Submitted' || partnerRateGrid.status=='Withdraw'}">
					           <img align="top"  name="<%="b"+gridSection+"_"+parameter+"_Label"%>"  onclick="findPortName(this);" src="${pageContext.request.contextPath}/images/question.png"/>
					           <img align="top" class="openpopup" width="17" name="<%="a"+gridSection+"_"+parameter+"_Label"%>" height="20"   src="<c:url value='/images/open-popup-disabled.gif'/>" />
					           </c:if> 
					           </td>  
							         <%  if((tdHashMap.containsKey("AIR"))){ String basisDisplay=(String)tdHashMap.get("AIR");
							                  String[] detailsArray=basisDisplay.toString().split("#");
							                  String grid=detailsArray[0];
							                  String editable=detailsArray[1];
							                  String textValue= detailsArray[2];
							                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
							                	  textValue="";  
							                  }
							                  String basis=detailsArray[3]; 
							        	  %> 
							        	 <c:set var="basis"  value="<%=basis%>" scope="request"/> 
							        	 <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>  
							        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly="" cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>    
							   <% }else if((!(tdHashMap.containsKey("AIR")))&& (tdHashMap.containsKey("L20"))){
					        	 String basisDisplay=(String)tdHashMap.get("L20");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3]; 
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }else if((!(tdHashMap.containsKey("AIR")))&& (!tdHashMap.containsKey("L20"))&&(tdHashMap.containsKey("L40"))){
					        	 String basisDisplay=(String)tdHashMap.get("L40");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
					         <% }
					         else if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&((tdHashMap.containsKey("LCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("LCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td> 
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& ((tdHashMap.containsKey("FCL")))){
					        	 String basisDisplay=(String)tdHashMap.get("FCL");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                 
					         %> 
					             <c:set var="basis"  value="<%=basis%>" scope="request"/> 
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;" ><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
					         if((!(tdHashMap.containsKey("AIR")))&& (!(tdHashMap.containsKey("L20")))&&(!(tdHashMap.containsKey("L40")))&&(!(tdHashMap.containsKey("LCL")))&& (!(tdHashMap.containsKey("FCL")))&& ((tdHashMap.containsKey("GRP")))){
					        	 String basisDisplay=(String)tdHashMap.get("GRP");
				                  String[] detailsArray=basisDisplay.toString().split("#");
				                  String grid=detailsArray[0];
				                  String editable=detailsArray[1];
				                  String textValue= detailsArray[2];
				                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
				                	  textValue="";  
				                  }
				                  String basis=detailsArray[3];
				                  
					         %> 
					            <c:set var="basis"  value="<%=basis%>" scope="request"/>
					            <td style="margin: 0px; padding: 2px;"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:80px" name="<%=gridSection+"_"+parameter+"_Basis"%>"  value="#request['basis']"  headerKey="" headerValue=""/></td>    
					        	<td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>
					        	
					         <% }
							          if(tdHashMap.containsKey("L20")){ String basisDisplay=(String)tdHashMap.get("L20");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	   %>
							        	   
		                                 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" readonly=""  cssStyle="text-align:right" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
							          <% } else {%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
							         
									  <%  if(tdHashMap.containsKey("L40")){ String basisDisplay=(String)tdHashMap.get("L40");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	   %> 
							        	  
							            <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
							             
							         <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
									  
									  <%  if(tdHashMap.containsKey("LCL")){String basisDisplay=(String)tdHashMap.get("LCL");
									   String[] detailsArray=basisDisplay.toString().split("#");
									   String grid=detailsArray[0];
									   String editable=detailsArray[1];
						               String textValue= detailsArray[2];
						               if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						               String basis=detailsArray[3];
						               
							        	  %> 
							        	  
							         <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>  
							          <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
									  
									   <%  if(tdHashMap.containsKey("FCL")){String basisDisplay=(String)tdHashMap.get("FCL");
									      String[] detailsArray=basisDisplay.toString().split("#");
									      String grid=detailsArray[0];
									      String editable=detailsArray[1];
						                  String textValue= detailsArray[2];
						                  if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						                  String basis=detailsArray[3];
						                  
							        	   %> 
							        	   
							             <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" cssStyle="text-align:right" readonly="" cssClass="input-text" size="10" value="<%=textValue%>" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)"/></td>
							          <% }else{%>
							        	 <td style="margin: 0px; padding: 2px;"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" size="10"/></td>  
							         <% }%>
							         
								 <%   if(tdHashMap.containsKey("GRP")){String basisDisplay=(String)tdHashMap.get("GRP");
									     String[] detailsArray=basisDisplay.toString().split("#");
									     String grid=detailsArray[0];
									     String editable=detailsArray[1];
						                 String textValue= detailsArray[2];
						                 if((textValue.equals("0.00"))||(textValue.equals("0"))){
						                	  textValue="";  
						                  }
						                 String basis=detailsArray[3];
						                 
							        	  %>
							        	  
							             <td><s:hidden id="" name="<%=grid+"_"+editable+"_"+gridSection+"_"+parameter+"_Value"%>" value="<%=textValue%>" /></td> 
							          <% }else{%>
							        	 <td><s:hidden id="" name=""   /></td>  
							         <% }%>
							      
									  </tr>    
								 <% } %>  
						</table> 
						</td>
						</tr>  
						</table>
						</td>
						</tr> 
						</tbody> 
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Auto Handling Charges</td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                         </tr>
						</table>
						
						<table border="1" cellspacing="0" cellpadding="2"  class="p-table" style="margin: 6px;">						
						<tr>
						<td align="center" class="partner-text" ><b>Auto Handling</b></td>
						<td align="center" class="partner-text" >To Door</td>
						<td align="center" class="partner-text" >Whse Collect</td> 
						</tr>
						<tr>
						<td align="center" class="partner-text" >W/HHG FCL</td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_hhg_toDoor"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="200"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_lcl_toDoor"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="201"/></td>
						</tr>
						<tr>
						<td align="center" class="partner-text" >RO/RO</td> 
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_hhg_whse"  maxlength="18" cssStyle="text-align:right"  onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="203"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.auto_lcl_whse"  maxlength="18"  cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="204"/></td>
						</tr>
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Storage in Transit Charges </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						
						<table border="1" cellspacing="0" cellpadding="2" class="p-table" style="margin: 6px;"> 
						<tr>
						<td align="right" class="partner-text" ><b>Storage in Transit</b></td> 
						<td align="center" class="partner-text" >Period</td>
						<td align="center" class="partner-text" >Basis</td>
						<td align="center" class="partner-text" >Rate</td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Warehouse Handling Loose</td>
						<td align="left"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" /></td>  
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_whse_loose_basis" tabindex="205"/></td>  
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_whse_loose_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="206"/></td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Warehouse Handling Lift Van</td>
						<td align="left"><s:textfield id="" name="" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_whse_liftvan_basis" tabindex="207"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_whse_liftvan_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="208"/></td>
						</tr>
						<tr>
						<td align="right" class="partner-text" >Storage per (state unit period)</td>
						<td align="left"><s:select list="%{sit_storage_periodList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_storage_period" tabindex="209" /></td>
						<td align="left"><s:select list="%{basisList}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" " name="partnerRateGrid.sit_storage_basis" tabindex="210"/></td>
						<td align="left"><s:textfield cssClass="input-text"  name="partnerRateGrid.sit_storage_rate"  maxlength="18" cssStyle="text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="checkFloat(this)" tabindex="211"/></td>
						</tr>
						</table>
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Standard Inclusions and Exclusions </td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						<table>
						
                       <tr>
                       <td> 
                       <DIV ID="GIS1" ><p align="justify" style="color:#fffff; width:98% ">                                           
						${standardDestinationNote}</p></div></td>
						<td><DIV ID="GIS2" ><p align="justify" style="color:#fffff; width:98% ">  
                       ${standardOriginNote}					    
                        </p></DIV> </td></tr>
                        <s:hidden name="partnerRateGrid.standardInclusions"></s:hidden>
                         </table> 
						
						<table height="2px" style="margin: 1px;" width="100%">
						<tr><td colspan="8">
						  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;" >
                            <tr>
                              <td class="headtab_left"></td>
                              <td class="headtab_center" >&nbsp;Agent Tariff Notes</td>
                              <td width="28" valign="top" class="headtab_bg"></td>
                              <td class="headtab_bg_special" >&nbsp; </td>
                              <td class="headtab_right"></td>
                             </tr>
                            </table>
                           </td>
                       </tr>
						</table>
						<table>
						<tr>
						<td colspan="6" align="left" class="listwhitetext"><s:textarea  name="partnerRateGrid.tariffNotes" rows="6" cols="99"  cssClass="textarea"/></td>
						</tr>
						</table>
						<table><tr><td></td></tr></table>
					</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div> 
<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${partnerRateGrid.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='serviceOrder.createdOn' /></b></td>
				<s:hidden name="partnerRateGrid.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${partnerRateGrid.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='serviceOrder.createdBy' /></b></td>
				<c:if test="${not empty partnerRateGrid.id}">
					<s:hidden name="partnerRateGrid.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{partnerRateGrid.createdBy}" /></td>
				</c:if>
				<c:if test="${empty partnerRateGrid.id}">
					<s:hidden name="partnerRateGrid.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if> 
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='serviceOrder.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${partnerRateGrid.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="partnerRateGrid.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${partnerRateGrid.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='serviceOrder.updatedBy' /></b></td>
				<c:if test="${not empty partnerRateGrid.id}"> 
                <s:hidden name="partnerRateGrid.updatedBy"/> 
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{partnerRateGrid.updatedBy}"/></td>
				</c:if> 
				<c:if test="${empty partnerRateGrid.id}"> 
				<s:hidden name="partnerRateGrid.updatedBy" value="${pageContext.request.remoteUser}"/> 
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if> 
			</tr>
		</tbody>
	</table>  
</s:form>

<s:form id="partnerRateGridForm1"  name="partnerRateGridForm1" action="updateRateGridStatus.html" method="post" validate="true">
<input type="hidden" name="id">
<input type="hidden" name="partnerId">
<input type="hidden" name="status">
</s:form>
<script type="text/javascript">  
try{
window.print();
window.close();
 }
 catch(e){}
</script>  		