<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <head> 
 
</head>   
<s:form id="driverDBChildForm" method="post" >

<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr> 
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<jsp:scriptlet>
     org.displaytag.decorator.TotalTableDecorator totals = new org.displaytag.decorator.TotalTableDecorator();
     totals.setTotalLabel("Grand Total");
     totals.setSubtotalLabel("Sub Total");
     pageContext.setAttribute("totals", totals);
  </jsp:scriptlet>


 <display:table name="driverDBSoDetailsList" class="table" id="driverDBSoDetailsList"  style="width:100%;margin-bottom:0px;"  decorator="totals"> 
   	<display:column property="shipperName"  title="Shipper" ></display:column>
   	<display:column property="shipNumber" group="1"  title="Order #" ></display:column>   	 
   	<display:column property="jobType"  title="Job" ></display:column>
	<display:column property="regNumber"  title="Reg #" ></display:column>
	<display:column property="chargeCode" title="Charge code" ></display:column>
	<display:column title="Revenue" property="distributionAmount" style="text-align:right" format="{0,number,0.00}" total="true" />	
	<display:column property="description" title="Description" ></display:column>	
	<display:column title="Amount" property="actualAmount" style="text-align:right" format="{0,number,0.00}" total="true" />	
</display:table>
 <hr />
</s:form>