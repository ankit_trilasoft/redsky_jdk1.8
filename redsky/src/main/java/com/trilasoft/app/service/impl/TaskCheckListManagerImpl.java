package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.TaskCheckListDao;
import com.trilasoft.app.model.TaskCheckList;
import com.trilasoft.app.service.TaskCheckListManager;

public class TaskCheckListManagerImpl extends GenericManagerImpl<TaskCheckList, Long> implements TaskCheckListManager {
	TaskCheckListDao taskCheckListDao;
	
	public TaskCheckListManagerImpl(TaskCheckListDao taskCheckListDao) {
		super(taskCheckListDao);
		this.taskCheckListDao = taskCheckListDao;
	}
	

	public List getById() {
		return null;
	}
	
	public TaskCheckList getTaskCheckListData(String shipnumber, Long id){
		return taskCheckListDao.getTaskCheckListData(shipnumber, id);
	}

}
