package com.trilasoft.app.webapp.action;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

import javax.activation.MimetypesFileTypeMap;

public class FileUploadAction extends BaseAction implements ServletRequestAware, ServletResponseAware {
	
	private static final long serialVersionUID = -9208910183310010569L;

	private File file;

	private String fileContentType;

	private String fileFileName;

	private String name;

	private static final String CONTENT_TYPE = "text/html";

	private HttpServletRequest request;

	private HttpServletResponse response;

	private String fileType;

	private String id;

	private Long id1;

	private String fileDescription;

	private String customerNumber;

	private String fileId;

	private Boolean isCportal;

	private Boolean isAccportal;

	private Boolean isPartnerPortal;
	
	private Boolean isBookingAgent;

	private Boolean isNetworkAgent;

	private Boolean isOriginAgent;

	private Boolean isSubOriginAgent;

	private Boolean isDestAgent;
	
	private Boolean isSubDestAgent;

	private MyFile fileObj;

	private MyFileManager fileManager;

	private String sessionCorpID;

	private MimetypesFileTypeMap mimetypesFileTypeMap;

	private String mapFolder;
	
	private String documentCategory;
	
	private String myFileFor;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private Long fid;
	private CustomerFileManager customerFileManager;
	private RefMasterManager refMasterManager;
	private CustomerFile customerFile;
	private String noteFor;
	private TrackingStatusManager trackingStatusManager;
	private TrackingStatus trackingStatus;
	
	/**
	 * 
	 * Parameters for upload from drop box
	 * 
	 */
	//dropBoxUpload
	private File dropBoxUpload;
	private String dropBoxUploadFileName;
	private String dropBoxUploadContentType;
	private String valueForDragDrop;
	private Long uploadFileSize;
	private String draggedFileName;
	
	public File getDropBoxUpload() {
		return dropBoxUpload;
	}

	public void setDropBoxUpload(File dropBoxUpload) {
		this.dropBoxUpload = dropBoxUpload;
	}

	public String getDropBoxUploadFileName() {
		return dropBoxUploadFileName;
	}

	public void setDropBoxUploadFileName(String dropBoxUploadFileName) {
		this.dropBoxUploadFileName = dropBoxUploadFileName;
	}

	public String getDropBoxUploadContentType() {
		return dropBoxUploadContentType;
	}

	public void setDropBoxUploadContentType(String dropBoxUploadContentType) {
		this.dropBoxUploadContentType = dropBoxUploadContentType;
	}
	
	
	
	

	public FileUploadAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		mimetypesFileTypeMap = new MimetypesFileTypeMap();
		mimetypesFileTypeMap.addMimeTypes("text/plain txt text TXT");
		mimetypesFileTypeMap.addMimeTypes("application/msword doc");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-powerpoint ppt");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-excel xls xlsx");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-outlook msg");
		mimetypesFileTypeMap.addMimeTypes("application/pdf pdf PDF");
		mimetypesFileTypeMap.addMimeTypes("image/jpeg jpg jpeg");
		mimetypesFileTypeMap.addMimeTypes("image/gif gif");
	}

	public String writeFile(InputStream in, String outputFileName) throws Exception {
		// the directory to upload to
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		outputFileName=outputFileName.replaceAll(" ", "_");
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);

		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		// write the file to the file specified
		OutputStream bos = new FileOutputStream(uploadDir + outputFileName);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];

		while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
			bos.write(buffer, 0, bytesRead);
		}

		bos.close();
		//in.close();

		return dirPath.getAbsolutePath() + Constants.FILE_SEP + outputFileName;
	}

	public void uploadFiles() throws ServletException, IOException {
		System.out.println("applet start");
		processFileUpload(request, response);
	}
	
	
	public void uploadFromDropBox() {
		System.out.println("\n\n================Start Upload from drop box================\n\n");
		System.out.println("File Name: "+getDropBoxUploadFileName());
		System.out.println("File: "+getDropBoxUploadContentType());
		System.out.println("File: "+getDropBoxUpload());
		System.out.println("\n\n================End Upload from drop box================\n\n");
	}
	

	public String saveFileMetadata() {
		String fileType = request.getParameter("fileType");
		String fileDescription = request.getParameter("fileDescription");

		String isAccportal = request.getParameter("isAccportal");
		Boolean isAccportal2 = new Boolean(isAccportal);

		String isPartnerPortal = request.getParameter("isPartnerPortal");
		Boolean isPartnerPortal2 = new Boolean(isPartnerPortal);

		String isCportal = request.getParameter("isCportal");
		Boolean isCportal2 = new Boolean(isCportal);

		String isBookingAgent = request.getParameter("isBookingAgent");
		Boolean isBookingAgent2 = new Boolean(isBookingAgent);
		
		String isNetworkAgent = request.getParameter("isNetworkAgent");
		Boolean isNetworkAgent2 = new Boolean(isNetworkAgent);
		
		String isOriginAgent = request.getParameter("isOriginAgent");
		Boolean isOriginAgent2 = new Boolean(isOriginAgent);
		
		String isSubOriginAgent = request.getParameter("isSubOriginAgent");
		Boolean isSubOriginAgent2 = new Boolean(isSubOriginAgent);
		
		String isDestAgent = request.getParameter("isDestAgent");
		Boolean isDestAgent2 = new Boolean(isDestAgent);
		
		String isSubDestAgent = request.getParameter("isSubDestAgent");
		Boolean isSubDestAgent2 = new Boolean(isSubDestAgent);
				
		String mapFolder = request.getParameter("mapFolder");
		
		String documentCategory = request.getParameter("documentCategory");
		
		String isServiceProvider2 = request.getParameter("isServiceProvider2");
		Boolean isServiceProvider = new Boolean(isServiceProvider2);
		
		String invoiceAttachment2 = request.getParameter("invoiceAttachment2");
		Boolean invoiceAttachment = new Boolean(invoiceAttachment2);
		String isVendorCode = request.getParameter("isVendorCode");
		if(isVendorCode==null){
			isVendorCode="";
		}		
		String isPayingStatus = request.getParameter("isPayingStatus");
		if(isPayingStatus==null){
			isPayingStatus="";
		}
		
		return SUCCESS;
	}
	private String checkFlagBA="";
	private String checkFlagNA="";
	private String checkFlagOA="";
	private String checkFlagSOA="";
	private String checkFlagDA="";
	private String checkFlagSDA="";
	private String checkAgent;
	
	public void processFileUpload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String []setValue = valueForDragDrop.split("~");
		System.out.println(setValue.length);
		
 
		String fileType=setValue[0].toString(); 
		String fileDescription = setValue[1].toString(); 
		String customerNumber = setValue[2].toString();  
		String fileId = setValue[3].toString(); 
		String SoId = setValue[4].toString(); 
		String isAccportal = setValue[5].toString(); 
		Boolean isAccportal2 = Boolean.valueOf(isAccportal); 
		String isCportal = setValue[6].toString();
		Boolean isCportal2 = Boolean.valueOf(isCportal); 
		String isPartnerPortal = setValue[7].toString();	
		Boolean isPartnerPortal2 = Boolean.valueOf(isPartnerPortal); 
		String isBookingAgent = setValue[8].toString();	
		Boolean isBookingAgent2 = Boolean.valueOf(isBookingAgent); 
		String isNetworkAgent = setValue[9].toString();	
		Boolean isNetworkAgent2 = Boolean.valueOf(isNetworkAgent); 
		String isOriginAgent = setValue[10].toString();
		Boolean isOriginAgent2 = Boolean.valueOf(isOriginAgent); 
		String isSubOriginAgent = setValue[11].toString();
		Boolean isSubOriginAgent2 = Boolean.valueOf(isSubOriginAgent); 
		String isDestAgent = setValue[12].toString();
		Boolean isDestAgent2 = Boolean.valueOf(isDestAgent); 
		String isSubDestAgent = setValue[13].toString();
		Boolean isSubDestAgent2 = Boolean.valueOf(isSubDestAgent); 
		String mapFolder = setValue[14].toString();  
		String documentCategory = setValue[15].toString();
		String myFileFor = setValue[16].toString(); 
		String isServiceProvider2 = setValue[17].toString();
		Boolean isServiceProvider = Boolean.valueOf(isServiceProvider2); 
		String invoiceAttachment2 =  setValue[18].toString();
		Boolean invoiceAttachment = new Boolean(invoiceAttachment2); 
		String isVendorCode ="";
		try {
			
			if(setValue[19] !=null){
			 isVendorCode =  setValue[19].toString();
			}
		} catch (Exception e2) { 
			e2.printStackTrace();
		} 
		try {
			String isPayingStatus = "";
			if(setValue[20] !=null){
			isPayingStatus = setValue[20].toString();
			}
		} catch (Exception e2) { 
			e2.printStackTrace();
		}
		/*
		 * The uploading applet will zip all the files together to create afaster upload and to use just one server connection.
		*/
			String filePath = ""; 
		    BufferedInputStream bin=new BufferedInputStream(request.getInputStream());
		    draggedFileName = draggedFileName.replaceAll("#", " ");
		    draggedFileName=draggedFileName.replaceAll(" ", "_");
		try {
			if (fileManager.getMaxId(sessionCorpID).get(0) == null) {
				id1 = 1L;
			filePath = writeFile(bin, id1 + "_" + fileId + "_" + draggedFileName);
			}else{
				Long id1 = Long.parseLong(fileManager.getMaxId(sessionCorpID).get(0).toString());
				id1 = id1 + 1;	
				filePath = writeFile(bin, id1 + "_" + fileId + "_" + draggedFileName);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		File  existsFile= new File(filePath); 
		if (existsFile.exists()) { 
		try {
			 
					fileObj = new MyFile();
					fileObj.setLocation(filePath);
					fileObj.setFileType(fileType);
					if(fileDescription!=null && !fileDescription.equals(""))
					fileObj.setDescription(fileDescription);
					else
						fileObj.setDescription(fileType);	
					fileObj.setCorpID(sessionCorpID);

					fileObj.setCreatedBy(getRequest().getRemoteUser());
					fileObj.setUpdatedBy(getRequest().getRemoteUser());
					fileObj.setCustomerNumber(customerNumber);
					fileObj.setFileId(fileId);
					fileObj.setFileContentType(mimetypesFileTypeMap.getContentType(filePath));

					Long fileSize = uploadFileSize;
					if(fileSize<1048576){
					fileSize = Math.round(fileSize.doubleValue() / 1024);
					fileObj.setFileSize(fileSize.toString() + " " + "KB");
				    }else{
				    	fileSize = Math.round(fileSize.doubleValue() / (1024*1024));
						fileObj.setFileSize(fileSize.toString() + " " + "MB");
				    }
					fileObj.setFileFileName(draggedFileName);
					fileObj.setIsAccportal(isAccportal2);
					fileObj.setIsCportal(isCportal2);
					fileObj.setIsPartnerPortal(isPartnerPortal2);
				    fileObj.setIsBookingAgent(isBookingAgent2);
					fileObj.setIsNetworkAgent(isNetworkAgent2);
					fileObj.setIsOriginAgent(isOriginAgent2);
					fileObj.setIsSubOriginAgent(isSubOriginAgent2);
					fileObj.setIsDestAgent(isDestAgent2);
					fileObj.setIsSubDestAgent(isSubDestAgent2);
					fileObj.setCreatedOn(new Date());
					fileObj.setUpdatedOn(new Date());
					fileObj.setCoverPageStripped(true);
					fileObj.setActive(true);
					fileObj.setIsServiceProvider(isServiceProvider);
					fileObj.setInvoiceAttachment(invoiceAttachment);
					fileObj.setAccountLineVendorCode(isVendorCode);
					if(fileObj.getAccountLineVendorCode()!=null && !fileObj.getAccountLineVendorCode().equals("") && !fileObj.getAccountLineVendorCode().equals("null")){
					fileObj.setAccountLinePayingStatus("New");
					}
					fileObj.setMapFolder(mapFolder);
					
					documentCategory = refMasterManager.findDocCategoryByDocType(sessionCorpID, fileType);
					if(documentCategory!=null && !documentCategory.equals(""))
					{
                    fileObj.setDocumentCategory(documentCategory);
					}else{
						fileObj.setDocumentCategory("General");
					}
					List isSecureList = fileManager.getIsSecure(fileType, sessionCorpID);
					if (isSecureList != null && !isSecureList.isEmpty() && isSecureList.get(0)!=null) {
						String isSecure = isSecureList.get(0).toString();
						if (isSecure.equalsIgnoreCase("Y")) {
							fileObj.setIsSecure(true);
						} else {
							fileObj.setIsSecure(false);
						}

					}
					
					List docList = fileManager.getDocumentCode(fileType, sessionCorpID);
					if (docList != null && !docList.isEmpty() && docList.get(0)!=null) {
						fileObj.setDocCode(docList.get(0).toString());
						} else {
							fileObj.setDocCode("");
						}
					
			       if (myFileFor != null && myFileFor.equals("SO")) {
			    	   serviceOrder = serviceOrderManager.get(Long.parseLong(SoId));
					List shipNumberList = fileManager.getShipNumberList(serviceOrder.getShipNumber());
					if(shipNumberList!=null && !shipNumberList.isEmpty()){
					Iterator it = shipNumberList.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent2){
			        		fileObj.setIsBookingAgent(true);
						}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent2){
							fileObj.setIsNetworkAgent(true);
						}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent2 ){
							fileObj.setIsOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent2 ){
							fileObj.setIsSubOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent2 ){
							fileObj.setIsDestAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent2){
							fileObj.setIsSubDestAgent(true);
						} 
			         }
				}
					
			    	   }
			   	if (myFileFor != null &&  myFileFor.equals("CF")) {
			   		customerFile = customerFileManager.get(Long.parseLong(SoId));
			       List shipNumberList = fileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
								List shipNumberListSo = fileManager.getShipNumberList(shipNumber);
								if(shipNumberListSo!=null && !shipNumberListSo.isEmpty()){
								Iterator it = shipNumberListSo.iterator();
						         while (it.hasNext()) {
							        	String agentFlag=(String)it.next();
							        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent2){
							        		fileObj.setIsBookingAgent(true);
										}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent2){
											fileObj.setIsNetworkAgent(true);
										}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent2 ){
											fileObj.setIsOriginAgent(true);
										}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent2 ){
											fileObj.setIsSubOriginAgent(true);
										}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent2 ){
											fileObj.setIsDestAgent(true);
										}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent2){
											fileObj.setIsSubDestAgent(true);
										} 
							         }
							}
					}
				}
			   	}
					fileObj=fileManager.save(fileObj);
					if (myFileFor != null && myFileFor.equals("SO")) {
						serviceOrder = serviceOrderManager.get(Long.parseLong(SoId));
						trackingStatus = trackingStatusManager.get(Long.parseLong(SoId));
						//fileObj = fileManager.get(fid);
						//String networkLinkId=myFile.getNetworkLinkId();
						if(fileObj.getIsBookingAgent() || fileObj.getIsNetworkAgent() || fileObj.getIsOriginAgent() || fileObj.getIsSubOriginAgent() || fileObj.getIsDestAgent() || fileObj.getIsSubDestAgent() ){																
						List linkedShipNumberList= new ArrayList();	
						String networkLinkId="";
						linkedShipNumberList=fileManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary", fileObj.getIsBookingAgent(), fileObj.getIsNetworkAgent(), fileObj.getIsOriginAgent(), fileObj.getIsSubOriginAgent(), fileObj.getIsDestAgent(), fileObj.getIsSubDestAgent());
						Set set = new HashSet(linkedShipNumberList);
						List linkedSeqNum = new ArrayList(set);
						if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
							Iterator it = linkedSeqNum.iterator();
					         while (it.hasNext()) {
					        	 	 String shipNumber=(String)it.next();
						        	 String removeDatarow = shipNumber.substring(0, 6);
						        	 if(!(fileObj.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
							        	 networkLinkId = sessionCorpID + (fileObj.getId()).toString();
							        	 fileManager.upDateNetworkLinkId(networkLinkId,fileObj.getId());
							             MyFile myFileObj = new MyFile();
							        	 myFileObj.setFileId(shipNumber);
							        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
							        	 myFileObj.setActive(fileObj.getActive());
							        	 myFileObj.setCustomerNumber(shipNumber);
							        	 myFileObj.setDescription(fileObj.getDescription());
							        	 //myFileObj.setFile(myFile.getFile());
							        	 myFileObj.setFileContentType(fileObj.getFileContentType());
							        	 myFileObj.setFileFileName(fileObj.getFileFileName());
							        	 myFileObj.setFileSize(fileObj.getFileSize());
							        	 myFileObj.setFileType(fileObj.getFileType());
							        	 
							        	 myFileObj.setIsCportal(fileObj.getIsCportal());
							        	 myFileObj.setIsAccportal(fileObj.getIsAccportal());
							        	 myFileObj.setIsPartnerPortal(fileObj.getIsPartnerPortal());
							        	 
							        	 myFileObj.setIsBookingAgent(fileObj.getIsBookingAgent());
							        	 myFileObj.setIsNetworkAgent(fileObj.getIsNetworkAgent());
							        	 myFileObj.setIsOriginAgent(fileObj.getIsOriginAgent());
							        	 myFileObj.setIsSubOriginAgent(fileObj.getIsSubOriginAgent());
							        	 myFileObj.setIsDestAgent(fileObj.getIsDestAgent());
							        	 myFileObj.setIsSubDestAgent(fileObj.getIsSubDestAgent());
							        	 myFileObj.setIsSecure(fileObj.getIsSecure());
							        	 myFileObj.setLocation(fileObj.getLocation());
							        	 myFileObj.setInvoiceAttachment(fileObj.getInvoiceAttachment());
							        	 //Added For #7990
							        	 myFileObj.setDocumentCategory(fileObj.getDocumentCategory());
							        	 //end
							        	 myFileObj.setMapFolder(fileObj.getMapFolder());
							        	 myFileObj.setCoverPageStripped(fileObj.getCoverPageStripped());
							        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
							        	 myFileObj.setCreatedOn(new Date());
							        	 myFileObj.setUpdatedBy(getRequest().getRemoteUser());
							        	 myFileObj.setUpdatedOn(new Date());
							        	 myFileObj.setNetworkLinkId(networkLinkId);
							        	 
							        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null 
							        			 && (!(trackingStatus.getContractType().toString().trim().equals("")))  
							        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
						        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(myFileObj.getFileId()));
						        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
						        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
						        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
						        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && fileObj.getFileType().equals("Invoice Client")){
						        	    		myFileObj.setFileType("Invoice Vendor");	
						        	    		}
						        	    	}
							        	 
							        	 if(trackingStatus.getBookingAgentExSO().equals(trackingStatus.getShipNumber()) 
							        			 && (trackingStatus.getOriginAgentExSO().equals(myFileObj.getFileId()) 
							        					 || trackingStatus.getDestinationAgentExSO().equals(myFileObj.getFileId()))
							        			 && fileObj.getFileType().equals("Client Quote")){
							        		 myFileObj.setFileType("Agent Quote"); 
							        	 }
							        	 
							        	 fileManager.save(myFileObj);
						        	 }
						          }
						       }
					         }	
						
					}
					if (myFileFor != null && myFileFor.equals("CF")) {
						if(fileObj.getIsBookingAgent()!=null && fileObj.getIsBookingAgent()){
							checkFlagBA="BA";
						}
                        if(fileObj.getIsNetworkAgent()!=null && fileObj.getIsNetworkAgent()){
                        	checkFlagNA="NA";
                        }
                        if(fileObj.getIsOriginAgent()!=null && fileObj.getIsOriginAgent() ){
                        	checkFlagOA="OA";
                        }
                        if(fileObj.getIsSubOriginAgent()!=null && fileObj.getIsSubOriginAgent()){
                        	checkFlagSOA="SOA";
                        }
                        if(fileObj.getIsDestAgent()!=null && fileObj.getIsDestAgent()){
                        	checkFlagDA="DA";
                        }
                        if(fileObj.getIsSubDestAgent()!=null && fileObj.getIsSubDestAgent()){
                        	checkFlagSDA="SDA";
                        }
						customerFile = customerFileManager.get(Long.parseLong(SoId));
						//fileObj = fileManager.get(fileObj.getId());
						String networkLinkId=fileObj.getNetworkLinkId();
						List previousSequenceNumberList = new ArrayList();
			    		List shipNumberList = fileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
						if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
							Iterator shipIt =  shipNumberList.iterator();
								while(shipIt.hasNext()){
									String shipNumberOld = (String) shipIt.next();
									trackingStatus = trackingStatusManager.get(Long.parseLong(trackingStatusManager.findIdByShipNumber(shipNumberOld).get(0).toString()));
			    		List defCheckedShipNumber = fileManager.getDefCheckedShipNumber(shipNumberOld);
						if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
							checkAgent = defCheckedShipNumber.get(0).toString();				    
						}	
					if(fileObj.getIsBookingAgent() || fileObj.getIsNetworkAgent() || fileObj.getIsOriginAgent() || fileObj.getIsSubOriginAgent() || fileObj.getIsDestAgent() || fileObj.getIsSubDestAgent()){						
												List linkedShipNumberList = fileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
												Set set = new HashSet(linkedShipNumberList);
												List linkedSeqNum = new ArrayList(set);
												if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
													if(networkLinkId ==null || networkLinkId.equals("") || networkLinkId.equalsIgnoreCase("null")){
														networkLinkId = sessionCorpID + (fileObj.getId()).toString();	
													}
													fileManager.upDateNetworkLinkId(networkLinkId,fileObj.getId());
										     		
													 Iterator it = linkedSeqNum.iterator();
											         while (it.hasNext()) {
											        	 	 String shipNumber=(String)it.next();
											        	 	String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
												        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
												        		 previousSequenceNumberList.add(sequenceNumber);
												        	 String removeDatarow = shipNumber.substring(0, 6);
												        	 if(!(fileObj.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
												        		 List linkedId = fileManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
													     		 if(linkedId ==null || linkedId.isEmpty()){
												        		 MyFile myFileObj = new MyFile();
													        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
													        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
													        	 myFileObj.setActive(fileObj.getActive());
													        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
													        	
													        	 myFileObj.setDescription(fileObj.getDescription());
													        
													        	 myFileObj.setFileContentType(fileObj.getFileContentType());
													        	 myFileObj.setFileFileName(fileObj.getFileFileName());
													        	 myFileObj.setFileSize(fileObj.getFileSize());
													        	 myFileObj.setFileType(fileObj.getFileType());
													        	 
													        	 myFileObj.setIsCportal(fileObj.getIsCportal());
													        	 myFileObj.setIsAccportal(fileObj.getIsAccportal());
													        	 myFileObj.setIsPartnerPortal(fileObj.getIsPartnerPortal());
													 			
													        	 myFileObj.setIsBookingAgent(fileObj.getIsBookingAgent());
													        	 myFileObj.setIsNetworkAgent(fileObj.getIsNetworkAgent());
													        	 myFileObj.setIsOriginAgent(fileObj.getIsOriginAgent());
													        	 myFileObj.setIsSubOriginAgent(fileObj.getIsSubOriginAgent());
													        	 myFileObj.setIsDestAgent(fileObj.getIsDestAgent());
													        	 myFileObj.setIsSubDestAgent(fileObj.getIsSubDestAgent());
													        	 if(checkFlagBA.equals("BA")){
														 				myFileObj.setIsBookingAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				myFileObj.setIsBookingAgent(fileObj.getIsBookingAgent());
														 			}
														 			if(checkFlagNA.equals("NA")){
														 				myFileObj.setIsNetworkAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				 myFileObj.setIsNetworkAgent(fileObj.getIsNetworkAgent());
														 			}
														 			if(checkFlagOA.equals("OA")){
														 				myFileObj.setIsOriginAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				myFileObj.setIsOriginAgent(fileObj.getIsOriginAgent());
														 			}
														 			if(checkFlagSOA.equals("SOA")){
														 				myFileObj.setIsSubOriginAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				 myFileObj.setIsSubOriginAgent(fileObj.getIsSubOriginAgent());
														 			}
														 			if(checkFlagDA.equals("DA")){
														 				myFileObj.setIsDestAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				 myFileObj.setIsDestAgent(fileObj.getIsDestAgent());
														 			}
														 			if(checkFlagSDA.equals("SDA")){
														 				myFileObj.setIsSubDestAgent(true);
														 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",fileObj.getFileId(),fileObj.getId(),"check" );
														 			}else{
														 				 myFileObj.setIsSubDestAgent(fileObj.getIsSubDestAgent());
														 			}
													        	 myFileObj.setIsSecure(fileObj.getIsSecure());
													        	 myFileObj.setLocation(fileObj.getLocation());
													        	 myFileObj.setMapFolder(fileObj.getMapFolder());
													        	 myFileObj.setCoverPageStripped(fileObj.getCoverPageStripped());
													        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
													        	 myFileObj.setCreatedOn(new Date());
													        	 myFileObj.setUpdatedBy(getRequest().getRemoteUser());
													        	 myFileObj.setUpdatedOn(new Date());										        	
													        	 myFileObj.setDocumentCategory(fileObj.getDocumentCategory());
													        	 myFileObj.setInvoiceAttachment(fileObj.getInvoiceAttachment());
													        	 myFileObj.setNetworkLinkId(networkLinkId);
													        	 
													        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null 
													        			 && (!(trackingStatus.getContractType().toString().trim().equals("")))  
													        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
												        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
												        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
												        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
												        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
												        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && fileObj.getFileType().equals("Invoice Client")){
												        	    		myFileObj.setFileType("Invoice Vendor");	
												        	    		}
												        	    	}
													        	 
													        	 if(trackingStatus.getBookingAgentExSO().equals(trackingStatus.getShipNumber()) 
													        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) 
													        					 || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
													        			 && fileObj.getFileType().equals("Client Quote")){
													        		 myFileObj.setFileType("Agent Quote"); 
													        	 }
													        	 
													        	 fileManager.save(myFileObj);
													     		 }else{
													     			fileObj = fileManager.get(fid);
																	 if(checkFlagBA.equals("BA")){
																		 fileObj.setIsBookingAgent(true);
																		 fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsBookingAgent(fileObj.getIsBookingAgent());
															 			}
															 			if(checkFlagNA.equals("NA")){
															 				fileObj.setIsNetworkAgent(true);
															 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsNetworkAgent(fileObj.getIsNetworkAgent());
															 			}
															 			if(checkFlagOA.equals("OA")){
															 				fileObj.setIsOriginAgent(true);
															 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsOriginAgent(fileObj.getIsOriginAgent());
															 			}
															 			if(checkFlagSOA.equals("SOA")){
															 				fileObj.setIsSubOriginAgent(true);
															 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsSubOriginAgent(fileObj.getIsSubOriginAgent());
															 			}
															 			if(checkFlagDA.equals("DA")){
															 				fileObj.setIsDestAgent(true);
															 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsDestAgent(fileObj.getIsDestAgent());
															 			}
															 			if(checkFlagSDA.equals("SDA")){
															 				fileObj.setIsSubDestAgent(true);
															 				fileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",fileObj.getFileId(),fileObj.getId(),"check" );
															 			}else{
															 				fileObj.setIsSubDestAgent(fileObj.getIsSubDestAgent());
															 			}
																 } 	 
												          }
												      }
											         }
												
											}	
										}
									}
						}			
					
					if(fileObj.getNetworkLinkId()!=null && !(fileObj.getNetworkLinkId().equalsIgnoreCase(""))){
						customerFile = customerFileManager.get(Long.parseLong(SoId));
						//fileObj = fileManager.get(fid);
						List linkedIdList = fileManager.findlinkedIdList(networkLinkId);
						List linkedShipNumberList = null;
						if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
							List shipNumberList1 = fileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
							if (shipNumberList1!=null && !shipNumberList1.isEmpty()) {
								Iterator shipIt =  shipNumberList1.iterator();
									while(shipIt.hasNext()){
										String shipNumberOld = (String) shipIt.next();
										if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){
								linkedShipNumberList = fileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
							}
						}
							Iterator it = linkedIdList.iterator();
					         while (it.hasNext()) {
					        	 Long id = Long.parseLong(it.next().toString());
					        	 MyFile newMyFile = fileManager.getForOtherCorpid(id);
					        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
						        		 newMyFile.setIsBookingAgent(false);
							        	 newMyFile.setIsNetworkAgent(false);
							        	 newMyFile.setIsOriginAgent(false);   
							        	 newMyFile.setIsSubOriginAgent(false);
							        	 newMyFile.setIsDestAgent(false);
							        	 newMyFile.setIsSubDestAgent(false);
							        	 newMyFile.setNetworkLinkId("");
					        	 	}else{
					        	 		if(checkFlagBA.equals("BA")){
					        	 			newMyFile.setIsBookingAgent(true);
							 			}else{
							 				newMyFile.setIsBookingAgent(fileObj.getIsBookingAgent());
							 			}
							 			if(checkFlagNA.equals("NA")){
							 				newMyFile.setIsNetworkAgent(true);
							 			}else{
							 				newMyFile.setIsNetworkAgent(fileObj.getIsNetworkAgent());
							 			}
							 			if(checkFlagOA.equals("OA")){
							 				newMyFile.setIsOriginAgent(true);
							 			}else{
							 				newMyFile.setIsOriginAgent(fileObj.getIsOriginAgent());
							 			}
							 			if(checkFlagSOA.equals("SOA")){
							 				newMyFile.setIsSubOriginAgent(true);
							 			}else{
							 				newMyFile.setIsSubOriginAgent(fileObj.getIsSubOriginAgent());
							 			}
							 			if(checkFlagDA.equals("DA")){
							 				newMyFile.setIsDestAgent(true);
							 			}else{
							 				newMyFile.setIsDestAgent(fileObj.getIsDestAgent());
							 			}
							 			if(checkFlagSDA.equals("SDA")){
							 				newMyFile.setIsSubDestAgent(true);
							 			}else{
							 				newMyFile.setIsSubDestAgent(fileObj.getIsSubDestAgent());
							 			}	
					        	    	      	    	
							        	 newMyFile.setIsCportal(fileObj.getIsCportal());
							        	 newMyFile.setIsAccportal(fileObj.getIsAccportal());
							        	 newMyFile.setIsPartnerPortal(fileObj.getIsPartnerPortal());
					        	 	  	}
					        	 	    newMyFile.setInvoiceAttachment(fileObj.getInvoiceAttachment());
						        	 	newMyFile.setDocumentCategory(fileObj.getDocumentCategory());
						        	 	newMyFile.setUpdatedBy(getRequest().getRemoteUser());
						        	 	newMyFile.setUpdatedOn(new Date());
						        	 	fileManager.save(newMyFile);	
				            }
				          }
						}
					}
				
					}

				

					/*	} catch (ZipException ze) {
				
				 * We want to catch each sip exception separately because there
				 * is a possibility that we can read more files from the archive 
				 * even if one of them is corrupted.
				 
				ze.printStackTrace();
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//in.close();
		}

		/*
		 * Now that we have finished uploading the files we will send a reponse
		 * to the server indicating our success.
		 */

		response.setContentType(CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		out.println("<html><head><title>ImageSrv</title></head></html>");
		out.flush();
		out.close();
		response.setStatus(HttpServletResponse.SC_OK);
		}else{
			response.setContentType(CONTENT_TYPE);
			PrintWriter out = response.getWriter();
			out.println("<html><head><title>ImageSrv</title></head></html>");
			out.flush();
			out.close(); 
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	public String execute() throws Exception {
		if (this.cancel != null) {
			return "cancel";
		}

		if (file == null || file.length() > 2097152) {
			addActionError(getText("maxLengthExceeded"));
			return INPUT;
		}

		String filePath = writeFile(new FileInputStream(file), fileFileName + fileType + fileId);
		getRequest().setAttribute("location", filePath);

		String link = getRequest().getContextPath() + "/resources" + "/" + getRequest().getRemoteUser() + "/";

		getRequest().setAttribute("link", link + fileFileName + fileType + fileId);

		return SUCCESS;
	}

	public String start() {
		return INPUT;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public File getFile() {
		return file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;

	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;

	}

	public String getFileDescription() {
		return fileDescription;
	}

	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public MyFileManager getFileManager() {
		return fileManager;
	}

	public void setFileManager(MyFileManager fileManager) {
		this.fileManager = fileManager;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public Boolean getIsAccportal() {
		return isAccportal;
	}

	public void setIsAccportal(Boolean isAccportal) {
		this.isAccportal = isAccportal;
	}

	public Boolean getIsCportal() {
		return isCportal;
	}

	public void setIsCportal(Boolean isCportal) {
		this.isCportal = isCportal;
	}

	public Boolean getIsPartnerPortal() {
		return isPartnerPortal;
	}

	public void setIsPartnerPortal(Boolean isPartnerPortal) {
		this.isPartnerPortal = isPartnerPortal;
	}

	public String getMapFolder() {
		return mapFolder;
	}

	public void setMapFolder(String mapFolder) {
		this.mapFolder = mapFolder;
	}

	public Boolean getIsBookingAgent() {
		return isBookingAgent;
	}

	public void setIsBookingAgent(Boolean isBookingAgent) {
		this.isBookingAgent = isBookingAgent;
	}

	public Boolean getIsNetworkAgent() {
		return isNetworkAgent;
	}

	public void setIsNetworkAgent(Boolean isNetworkAgent) {
		this.isNetworkAgent = isNetworkAgent;
	}

	public Boolean getIsOriginAgent() {
		return isOriginAgent;
	}

	public void setIsOriginAgent(Boolean isOriginAgent) {
		this.isOriginAgent = isOriginAgent;
	}

	public Boolean getIsSubOriginAgent() {
		return isSubOriginAgent;
	}

	public void setIsSubOriginAgent(Boolean isSubOriginAgent) {
		this.isSubOriginAgent = isSubOriginAgent;
	}

	public Boolean getIsDestAgent() {
		return isDestAgent;
	}

	public void setIsDestAgent(Boolean isDestAgent) {
		this.isDestAgent = isDestAgent;
	}

	public Boolean getIsSubDestAgent() {
		return isSubDestAgent;
	}

	public void setIsSubDestAgent(Boolean isSubDestAgent) {
		this.isSubDestAgent = isSubDestAgent;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getMyFileFor() {
		return myFileFor;
	}

	public void setMyFileFor(String myFileFor) {
		this.myFileFor = myFileFor;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Long getFid() {
		return fid;
	}

	public void setFid(Long fid) {
		this.fid = fid;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public String getCheckAgent() {
		return checkAgent;
	}

	public void setCheckAgent(String checkAgent) {
		this.checkAgent = checkAgent;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getValueForDragDrop() {
		return valueForDragDrop;
	}

	public void setValueForDragDrop(String valueForDragDrop) {
		this.valueForDragDrop = valueForDragDrop;
	}

	public Long getUploadFileSize() {
		return uploadFileSize;
	}

	public void setUploadFileSize(Long uploadFileSize) {
		this.uploadFileSize = uploadFileSize;
	}

	public String getDraggedFileName() {
		return draggedFileName;
	}

	public void setDraggedFileName(String draggedFileName) {
		this.draggedFileName = draggedFileName;
	}

	public String getNoteFor() {
		return noteFor;
	}

	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

}
