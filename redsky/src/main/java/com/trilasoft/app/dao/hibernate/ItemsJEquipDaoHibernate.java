package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.ItemsJEquipDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.MaterialDto;
  
public class ItemsJEquipDaoHibernate extends GenericDaoHibernate<ItemsJEquip, Long> implements ItemsJEquipDao {   
  
    private List itemsJEquips;
	private List ls;
	  private String newstatus;
	  private static Map pageFieldVisibilityMap = AppInitServlet.pageFieldVisibilityComponentMap;
	public ItemsJEquipDaoHibernate(){   
        super(ItemsJEquip.class);   
    }   
  
    public List<ItemsJEquip> findByType(String type, String wcontract, Long id) {
    	//System.out.println(wcontract);
    	return getHibernateTemplate().find("from ItemsJEquip where contract like '" +wcontract+ "%' AND type='"+type+"' AND descript not in (SELECT descript FROM ItemsJbkEquip where ticket="+id+") ");
	}   
    
    public List<ItemsJEquip> findById(Long id) {
    	ls = getHibernateTemplate().find("from ItemsJEquip where id=?", id);
    	//System.out.println(ls);
		return ls;
	}   
    
    /*public List<ItemsJEquip> findByDescript(String descript) {
		return getHibernateTemplate().find("from ItemsJEquip where descript=?", descript);
	}   */
    
    
    
    /*public List<ItemsJEquip> findByDescript(String type, String descript, String wcontract, Long id1) {
    	System.out.println(type);
    	System.out.println(descript);
    	newstatus = "U";
    	//System.out.println(id);
    	
    	if(type != "" && descript != "") {
    		itemsJEquips = getHibernateTemplate().find("from ItemsJEquip where contract like '" +wcontract+ "%' AND type like '" + type + "%' AND descript like '" + descript + "%' AND flag like '" + newstatus + "%' AND descript not in (SELECT descript FROM ItemsJbkEquip where ticket="+id1+")" );
        	
            }
    	
        if(type != "" && descript.equals("")) {
        	
        	itemsJEquips = getHibernateTemplate().find("from ItemsJEquip where contract like '" +wcontract+ "%' AND type like '" + type + "%' AND flag like '" + newstatus + "%'  AND descript not in (SELECT descript FROM ItemsJbkEquip where ticket="+id1+")");
        	
        }
        if(type.equals("") && descript != "") {
        	itemsJEquips = getHibernateTemplate().find("from ItemsJEquip where contract like '" +wcontract+ "%' AND descript like '" + descript + "%' AND flag like '" + newstatus + "%' AND descript not in (SELECT descript FROM ItemsJbkEquip where ticket="+id1+")");
        	
        }
        return itemsJEquips;
    }*/

	public List<ItemsJEquip> findByDescript(String descript) {
		return getHibernateTemplate().find("from ItemsJEquip where descript=?", descript);
	}
	public List<ItemsJEquip> findSection(String itemType, String wcontract)
	{
		newstatus = "U";
		
		itemsJEquips= getHibernateTemplate().find("from ItemsJEquip where contract like '" +wcontract+ "%' AND flag='" + newstatus + "' AND type like '" + itemType + "%'");
		 return itemsJEquips;
	}

	public List findMaterials(Long id,String workTicket, String contract, String corpID, String type) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and contract like '"+contract+"%' and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		
		String MasterList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag,type,'Y' as checkType FROM itemsjbkequip where (workTicketID = "+id+" or ticket="+workTicket+" ) and type='"+type+"' and (qty > 0 or actualQty >0) and corpID='"+corpID+"' group by id order by descript;";
		
		listMasterList=this.getSession().createSQLQuery(MasterList).list();
		if(listMasterList.isEmpty()){
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.qty as actual ,JEquip.qty as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			String jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.descript,0 ,0,0,JEquip.cost,0,0,type,'' FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		}
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){materialDto.setQty("");}else{materialDto.setQty(row[1].toString());}
	           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	           if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
	           if(row[4] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[4].toString());}
	           if(row[5] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[5].toString());}
	           if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
	           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
	           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
	           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
	           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
	           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
	           if(row[12] == null){materialDto.setType("");}else{materialDto.setType(row[12].toString());}
	           if(row[13] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[13].toString());}	          
	           list1.add(materialDto);
		}
		
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	//
	public List miscellaneousRequestList(Long id,String ship,String corpID, String type,String division,String resource) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and contract like '"+contract+"%' and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		
		/*String MasterList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag,type,'Y' as checkType FROM itemsjbkequip where (workTicketID = "+id+" or ticket="+workTicket+" ) and type='"+type+"' and (qty > 0 or actualQty >0) and corpID='"+corpID+"' group by id order by descript;";
		
		listMasterList=this.getSession().createSQLQuery(MasterList).list();
		if(listMasterList.isEmpty()){
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.qty as actual ,JEquip.qty as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			String jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,0 as cost,0,0,JEquip.category,'',JEquip.branch,division,(if(maxResourceLimit is null or maxResourceLimit='',0,maxResourceLimit)-if(qty is null or qty='',0,qty)) as freeQty,0,minResourceLimit as minLevel,0,0 FROM equipmaterialslimits as JEquip where JEquip.corpID='"+corpID+"' and JEquip.category='"+type+"' group by JEquip.resource;";
			listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		}*/
		String jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"' and ship='"+ship+"' and equipMaterialsId=em.id and division like '%"+division+"%' and resource like '%"+resource+"%'";;
		if(type!=null && !type.equals("")){
		jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"' and ship='"+ship+"' and em.branch='"+type+"'  and equipMaterialsId=em.id and division like '%"+division+"%' and resource like '%"+resource+"%'";
		}else{
			jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"' and ship='"+ship+"'  and equipMaterialsId=em.id and division like '%"+division+"%' and resource like '%"+resource+"%'";
		}
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	          // if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	          // if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
	           if(row[3] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[3].toString());}
	           if(row[1] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[1].toString());}
	           if(row[4] == null || (row[4].toString().equals(""))){
	        	   //materialDto.setReturned("");
	        	  /* materialDto.setCustName("");
	        	   materialDto.setCustCell("");
	        	   materialDto.setEmailId("");
	        	   materialDto.setRefferedBy("");*/
	        	   materialDto.setComments("");
	           }
	           else{
	        	   materialDto.setComments(row[4].toString());
	        	   
	        	  /* String info=row[4].toString();
	        	   String infos[]=info.split("-");	        	  
	        	   
	        	  // materialDto.setReturned(infos[0]);
	        	   materialDto.setCustName(infos[0]);
	        	   materialDto.setCustCell(infos[2]);
	        	   materialDto.setEmailId(infos[1]);
	        	   materialDto.setRefferedBy(infos[3]);*/
	        	  
	        	   }
	           if(row[5] == null ){
	        	   materialDto.setRefferedBy("") ; 
	           }else{
	        	   materialDto.setRefferedBy(row[5].toString());  
	           }
	           if(row[6] == null){materialDto.setEquipMaterialsId(0l);}else{materialDto.setEquipMaterialsId(Long.parseLong(row[6].toString()));}
	           if(row[7] == null){materialDto.setTotalCost("0");}else{materialDto.setTotalCost((row[7].toString()));}
	           materialDto.setHtml(Long.parseLong(row[0].toString()));
	          /* if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
	           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
	           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
	           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
	           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
	           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
	           if(row[12] == null){materialDto.setType("");}else{materialDto.setType(row[12].toString());}
	           if(row[13] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[13].toString());}
	           if(row[14] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[14].toString());}
	           if(row[15] == null){materialDto.setLocation("");}else{materialDto.setLocation(row[15].toString());}
	           if(row[16] == null){materialDto.setFreeQty("");}else{materialDto.setFreeQty(row[16].toString());}
	           if(row[17] == null){materialDto.setTotalCost("");}else{materialDto.setTotalCost(row[17].toString());}
	           if(row[18] == null){materialDto.setMinLevel("");}else{materialDto.setMinLevel(row[18].toString());}
	           if(row[19] == null){materialDto.setSalesPrice("");}else{materialDto.setSalesPrice(row[19].toString());}
	           if(row[20] == null){materialDto.setOoprice("");}else{materialDto.setOoprice(row[20].toString());}*/
	           list1.add(materialDto);
		}
		
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	//
	public List overallSalesRequestList(Long id,String ship,String corpID, String type,String begindt,String enddt) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		Date dt=new Date();		
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		String dt1=df.format(dt);
		//System.out.println("equal................................"+dt1.equals(enddt));
		
		String jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'   and division='HSRG'  and (j.createdOn between '"+begindt+"' and '"+enddt+"' or j.createdOn like '%"+enddt+"%') and ship in ('OS','CS')";;
		if(begindt.equals(enddt)){
		if(type!=null && !type.equals("")){
		jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice  FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and em.branch='"+type+"' and division='HSRG'  and (j.createdOn between '"+begindt+"' and '"+enddt+"' or (j.createdOn like '%"+enddt+"%' or j.updatedOn like '%"+enddt+"%')) and ship in ('OS','CS')";
		}else{
			jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and division='HSRG'  and (j.createdOn between '"+begindt+"' and '"+enddt+"' or (j.createdOn like '%"+enddt+"%' or j.updatedOn like '%"+enddt+"%')) and ship in ('OS','CS')";
		}
		}else{
			if(type!=null && !type.equals("")){
				jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice  FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and em.branch='"+type+"' and division='HSRG'  and j.createdOn between '"+begindt+"' and '"+enddt+"' and ship in ('OS','CS')";
				}else{
					jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and division='HSRG'  and j.createdOn between '"+begindt+"' and '"+enddt+"' and ship in ('OS','CS')";
				}
		}
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[2] == null){materialDto.setSoldQty("");}else{materialDto.setSoldQty(row[2].toString());}	         
	           if(row[3] == null){materialDto.setReturned("0");}else{materialDto.setReturned(row[3].toString());}
	           if(row[1] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[1].toString());}
	           if(row[4] == null || (row[4].toString().equals(""))){	        	  
	        	   materialDto.setComments("");
	           }
	           else{
	        	   materialDto.setComments(row[4].toString());       	 
	        	  
	        	   }
	           if(row[5] == null ){
	        	   materialDto.setRefferedBy("") ; 
	           }else{
	        	   materialDto.setRefferedBy(row[5].toString());  
	           }
	           if(row[6] == null){materialDto.setEquipMaterialsId(0l);}else{materialDto.setEquipMaterialsId(Long.parseLong(row[6].toString()));}
	           if(row[7] == null){materialDto.setTotalCost("0");}else{materialDto.setTotalCost((row[7].toString()));}
	          if(row[8] == null){
	        	  materialDto.setCreatedOn("");
	          }else{
	        	  String  statusDateNew="";
	        		try {
	        		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");		 	
	        		 	statusDateNew =  dateformatYYYYMMDD.format(row[8]);
	        		 	materialDto.setCreatedOn(statusDateNew);		 	
	        	    }catch (Exception e) {
	        	    	materialDto.setCreatedOn("");
	        			e.printStackTrace();
	        		}  
	        	  
	        	  }
	          if(row[9] == null){materialDto.setCustomerName("");}else{materialDto.setCustomerName(row[9].toString());}
	           if(row[10] == null){materialDto.setPayMethod("");}else{materialDto.setPayMethod(row[10].toString());}
	           if(row[11] != null && !(row[11].toString().equals(""))){
	        	   materialDto.setInvoiceSeqNumber(row[11].toString());
	        	   }
	           if(row[12] == null){materialDto.setUnitCost("0");}else{materialDto.setUnitCost((row[12].toString()));}
	           list1.add(materialDto);
		}
		
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	
	
	
	
	public List salesRequestList(Long id,String ship,String corpID, String type) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		
		String jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'   and division='HSRG' and ship in ('OS','CS')";;
		if(type!=null && !type.equals("")){
		jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and em.branch='"+type+"' and division='HSRG'  and ship in ('OS','CS')";
		}else{
			jEqipList = "select j.id,descript,actualQty,returned,comments,refferedBy,j.equipMaterialsId,j.cost,j.createdOn,j.customerName,j.payMethod,j.invoiceSeqNumber,j.salePrice  FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"'  and division='HSRG'  and ship in ('OS','CS')";
		}
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[2] == null){materialDto.setSoldQty("");}else{materialDto.setSoldQty(row[2].toString());}	         
	           if(row[3] == null){materialDto.setReturned("0");}else{materialDto.setReturned(row[3].toString());}
	           if(row[1] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[1].toString());}
	           if(row[4] == null || (row[4].toString().equals(""))){
	        	   
	        	   materialDto.setComments("");
	           }
	           else{
	        	   materialDto.setComments(row[4].toString()); 	   
	        	  
	        	  
	        	   }
	           if(row[5] == null ){
	        	   materialDto.setRefferedBy("") ; 
	           }else{
	        	   materialDto.setRefferedBy(row[5].toString());  
	           }
	           if(row[6] == null){materialDto.setEquipMaterialsId(0l);}else{materialDto.setEquipMaterialsId(Long.parseLong(row[6].toString()));}
	           if(row[7] == null){materialDto.setTotalCost("0");}else{materialDto.setTotalCost((row[7].toString()));}
	          if(row[8] == null){
	        	  materialDto.setCreatedOn("");
	          }else{
	        	  String  statusDateNew="";
	        		try {
	        		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");		 	
	        		 	statusDateNew =  dateformatYYYYMMDD.format(row[8]);
	        		 	materialDto.setCreatedOn(statusDateNew);		 	
	        	    }catch (Exception e) {
	        	    	materialDto.setCreatedOn("");
	        			e.printStackTrace();
	        		}  
	        	  
	        	  }
	          if(row[9] == null){materialDto.setCustomerName("");}else{materialDto.setCustomerName(row[9].toString());}
	           if(row[10] == null){materialDto.setPayMethod("");}else{materialDto.setPayMethod(row[10].toString());}
	           if(row[11] != null && !(row[11].toString().equals(""))){
	        	   materialDto.setInvoiceSeqNumber(row[11].toString());
	        	   }
	           if(row[12] == null){materialDto.setUnitCost("0");}else{materialDto.setUnitCost((row[12].toString()));}
	           list1.add(materialDto);
		}
		
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	//
	public List findMaterialExtraInfo(Long id,String workTicket, String contract, String corpID, String type) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and contract like '"+contract+"%' and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		
		/*String MasterList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag,type,'Y' as checkType FROM itemsjbkequip where (workTicketID = "+id+" or ticket="+workTicket+" ) and type='"+type+"' and (qty > 0 or actualQty >0) and corpID='"+corpID+"' group by id order by descript;";
		
		listMasterList=this.getSession().createSQLQuery(MasterList).list();
		if(listMasterList.isEmpty()){
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.qty as actual ,JEquip.qty as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
			String jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,0 as cost,0,0,JEquip.category,'',JEquip.branch,division,(if(maxResourceLimit is null or maxResourceLimit='',0,maxResourceLimit)-if(qty is null or qty='',0,qty)) as freeQty,0,minResourceLimit as minLevel,0,0 FROM equipmaterialslimits as JEquip where JEquip.corpID='"+corpID+"' and JEquip.category='"+type+"' group by JEquip.resource;";
			listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		}*/
		
		String jEqipList = "select j.id,descript,actualQty,returned,j.equipMaterialsId FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and j.corpID='"+corpID+"' and ticket="+workTicket+"  and workTicketId="+id+" group by j.descript";
		if(type!=null && !(type.equals(""))){
			jEqipList = "select j.id,descript,actualQty,returned,j.equipMaterialsId FROM itemsjbkequip j,equipmaterialslimits em where  em.id=j.equipMaterialsId and em.resource=j.descript and em.branch='"+type+"' and j.corpID='"+corpID+"' and ticket="+workTicket+"  and workTicketId="+id+" group by j.descript";	
		}
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           materialDto.setHtml(Long.parseLong(row[0].toString()));
	           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	          // if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	          // if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
	           if(row[3] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[3].toString());}
	           if(row[1] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[1].toString());}
	           if(row[4] == null){materialDto.setEquipMaterialsId(0l);}else{materialDto.setEquipMaterialsId(Long.parseLong(row[4].toString()));}
	          /* if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
	           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
	           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
	           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
	           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
	           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
	           if(row[12] == null){materialDto.setType("");}else{materialDto.setType(row[12].toString());}
	           if(row[13] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[13].toString());}
	           if(row[14] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[14].toString());}
	           if(row[15] == null){materialDto.setLocation("");}else{materialDto.setLocation(row[15].toString());}
	           if(row[16] == null){materialDto.setFreeQty("");}else{materialDto.setFreeQty(row[16].toString());}
	           if(row[17] == null){materialDto.setTotalCost("");}else{materialDto.setTotalCost(row[17].toString());}
	           if(row[18] == null){materialDto.setMinLevel("");}else{materialDto.setMinLevel(row[18].toString());}
	           if(row[19] == null){materialDto.setSalesPrice("");}else{materialDto.setSalesPrice(row[19].toString());}
	           if(row[20] == null){materialDto.setOoprice("");}else{materialDto.setOoprice(row[20].toString());}*/
	           list1.add(materialDto);
		}
		
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	//

	public List<ItemsJEquip> findByDescript(String descript, String wcontract, Long id1) {
		return null;
	}

	public List searchMaterials(String description, String workTicket, String contract, String corpID, String category) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' and descript like '"+description.replaceAll("'", "''")+"%' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip left outer join itemsjbkequip as JbkEquip on JEquip.descript = JbkEquip.descript where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and contract like '"+contract+"%' and JEquip.type='"+type+"' and JEquip.corpID='"+corpID+"' and JEquip.descript like '"+ description.replaceAll("'", "''")+"%' group by JEquip.descript;";
		//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' and descript like '"+description.replaceAll("'", "''")+"%' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip left outer join itemsjbkequip as JbkEquip on JEquip.descript = JbkEquip.descript where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and JEquip.type='"+type+"' and JEquip.corpID='"+corpID+"' and JEquip.descript like '"+ description.replaceAll("'", "''")+"%' group by JEquip.descript;";
		if(description==null) description="";
		if(category==null) category="";
		String MasterList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag,type as checkType,count(*) as count FROM itemsjbkequip where ticket="+workTicket+" and type like '%"+category+"%' and (qty > 0 or actualQty >0) and corpID='"+corpID+"' group by descript order by id;";
		List masterList=this.getSession().createSQLQuery(MasterList).list();
		if(masterList!=null && (!(masterList.isEmpty()))){
			String jbkEquipList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge, cost, packLabour, flag, type as checkType,count(*) as count FROM itemsjbkequip where ticket="+workTicket+" and type like '%"+category+"%' and descript like '"+description.replaceAll("'", "''")+"%' and corpID='"+corpID+"' and qty >0 group by descript order by id;";
			listMasterList=this.getSession().createSQLQuery(jbkEquipList).list();
		}else{
			String jEquipList="SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.descript,0 ,0,0,JEquip.cost,0,0, type as checkType ,count(*) as count FROM itemsjequip as JEquip where JEquip.type like '%"+category+"%' and JEquip.corpID='"+corpID+"' and JEquip.descript like '"+ description.replaceAll("'", "''")+"%' group by JEquip.descript order by id;";		
			listMasterList=this.getSession().createSQLQuery(jEquipList).list();
		}		
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){materialDto.setQty("");}else{materialDto.setQty(row[1].toString());}
	           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	           if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
	           if(row[4] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[4].toString());}
	           if(row[5] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[5].toString());}
	           if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
	           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
	           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
	           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
	           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
	           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
	           if(row[12] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[12].toString());}
	           if(row[13] == null){materialDto.setCount(new Long(0));}else{materialDto.setCount(Long.parseLong(row[13].toString()));}
	           list1.add(materialDto);
	      }
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	public List searchResourceList(String description, String workTicket, String contract, String corpID, String category) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		if(description==null) description="";
		if(category==null) category="";
		Boolean temp=false;
		try {
			if(pageFieldVisibilityMap.containsKey(corpID +"-component.field.itemsjequip.qtyField")) {
				Integer i= (Integer) pageFieldVisibilityMap.get(corpID +"-component.field.itemsjequip.qtyField") ;
				if(i>=2){
				temp=true;
				}
			} 
			} catch (Exception e) {
				log.error("Error in CorpComponentPermission the  Field Visibility cache",e);
			}
		String jbkEquipList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge, cost, packLabour, flag, type as checkType, estHour,comments,contractRate FROM itemsjbkequip where ticket="+workTicket+" and type like '%"+category+"%' and descript like '"+description.replaceAll("'", "''")+"%' and corpID='"+corpID+"' "+((!temp)?"and qty >0":"")+" order by id;";
		listMasterList=this.getSession().createSQLQuery(jbkEquipList).list();	
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           MaterialDto materialDto=new MaterialDto();
	           materialDto.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){materialDto.setQty("");}else{materialDto.setQty(row[1].toString());}
	           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
	           if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
	           if(row[4] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[4].toString());}
	           if(row[5] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[5].toString());}
	           if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
	           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
	           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
	           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
	           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
	           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
	           if(row[12] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[12].toString());}
	           //if(row[13] == null){materialDto.setCount(new Long(0));}else{materialDto.setCount(Long.parseLong(row[13].toString()));}
	           if(row[13] == null){materialDto.setEstHour("1.0");}else{materialDto.setEstHour(row[13].toString());}
	           if(row[14] == null){materialDto.setComments("");}else{materialDto.setComments(row[14].toString());}
	           if(row[15] == null){materialDto.setContractRate("");}else{materialDto.setContractRate(row[15].toString());}
	           list1.add(materialDto);
	      }
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	public List findContractFromBilling(String shipNumber) {
		return getHibernateTemplate().find("select contract from Billing where shipNumber='"+shipNumber+"'");
	}

	public List findContractFromCustomerFile(String shipNumber) {
		return getHibernateTemplate().find("select contract from CustomerFile where sequenceNumber='"+shipNumber+"'");
	}

	public List checkforBilling(String shipNumber) {
		return getHibernateTemplate().find("select id from Billing where shipNumber='"+shipNumber+"'");
	}
	public List findMaterialByContract(String contract,String type,String corpID){
		if(type==null) type="";
		if(contract==null) contract="";
		return getHibernateTemplate().find("select distinct descript from ItemsJEquip where corpID = '"+corpID+"' and contract  like '"+contract+"%' and type like '"+type+"%'order by 1"); 
	}
	/*public List findMaterialExtraInfoByContract(String contract,String type,String corpID){
		if(type==null) type="";
		if(contract==null) contract="";
		return this.getSession().createSQLQuery("select distinct descript from itemsjequip where corpID = '"+corpID+"' and contract  like '"+contract+"%' and type like '"+type+"%'order by 1").list();
	}*/
	public List findMaterialExtraInfoByContract(String contract,String type,String corpID,String division,String resource){
		//if(type==null) type="";
		List list=new ArrayList();
		List list1=new ArrayList();
		if(contract==null) contract="";
		if(division==null) division="";
		if(resource==null) resource="";
		if(type!=null && !type.equals("")){
		 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"'  and branch='"+type+" ' and resource like '%"+resource+"%'  and division like '%"+division+"%' and qty >0 order by 1").list();
		}else{
			 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"' and resource like '%"+resource+"%'  and division like '%"+division+"%'  and qty >0 order by 1").list();
		}
		Map<Long,String> desMap=new HashMap<Long,String>();
		Iterator it=list.iterator();
			while(it.hasNext()){
		           Object []row= (Object [])it.next();
		           if(row[0]!=null && row[1]!=null){
		           desMap.put(Long.parseLong(row[0].toString()),row[1].toString());
		           }
			}
			list1.add(desMap);
			return list1;
	}
	public List materialByCategory(String type,String corpID){
		//if(type==null) type="";
		List list=new ArrayList();
		List list1=new ArrayList();		
		if(type!=null && !type.equals("")){
		 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"'  and branch='"+type+" '  order by 1").list();
		}else{
			 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"'   order by 1").list();
		}
		Map<Long,String> desMap=new HashMap<Long,String>();
		Iterator it=list.iterator();
			while(it.hasNext()){
		           Object []row= (Object [])it.next();
		           if(row[0]!=null && row[1]!=null){
		           desMap.put(Long.parseLong(row[0].toString()),row[1].toString());
		           }
			}
			list1.add(desMap);
			return list1;
	}
	public List findMaterialExtraInfoByContractForSales(String contract,String branch,String corpID){
		//if(type==null) type="";
		List list=new ArrayList();
		List list1=new ArrayList();
		if(contract==null) contract="";
		if(branch!=null && !branch.equals("")){
		 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"'  and branch='"+branch+" ' and JEquip.division='HSRG' and JEquip.qty>0 order by JEquip.resource").list();
		}/*else{
			 list= this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+corpID+"' and JEquip.division='OMD'  order by 1").list();
		}*/
		Map<Long,String> desMap=new HashMap<Long,String>();
		Iterator it=list.iterator();
			while(it.hasNext()){
		           Object []row= (Object [])it.next();
		           if(row[0]!=null && row[1]!=null){
		           desMap.put(Long.parseLong(row[0].toString()),row[1].toString());
		           }
			}
			list1.add(desMap);
			return list1;
	}
	public List findMaterialByCategory(String category, String sessionCorpID){
		if(category==null) category="";
		return getHibernateTemplate().find("select distinct descript from ItemsJEquip where corpID = '"+sessionCorpID+"' and type like '"+category+"%'order by 1"); 
	}
	public List findMaterialByContractTab(String contract,String type,String corpID){
		return getHibernateTemplate().find("from ItemsJEquip where corpID = '"+corpID+"' and contract  like '"+contract+"%' and type like '"+type+"%'"); 
	}
	
	public List findByDescription(String descript, String contract, String corpID, String type) {
		return getHibernateTemplate().find("from ItemsJEquip where corpID = '"+corpID+"' and contract  like '"+contract+"%' and type like '"+type+"%' and descript like '"+descript+"%'"); 
	}
	public List findWareHouseDesc(String wrehouse, String sessionCorpID){
		return getHibernateTemplate().find("select description from RefMaster where  code  like '"+wrehouse+"%' and  parameter ='house'"); 
	}
	public List getListValue(String descript, String type, Boolean controlled){
		descript=descript.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		type=type.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");	
		//controlled=controlled.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		String a = descript + "%";
		String b = type + "%";
		Boolean c = controlled;		
		Criteria crit = this.getSession().createCriteria(ItemsJEquip.class);
		return crit.add(Restrictions.ilike("descript", a)).add(Restrictions.ilike("type", b)).add(Restrictions.eq("controlled", c)).list();
	}

	public List findContractAndType(String corpID){
		List<ItemsJEquip> list = getHibernateTemplate().find("from Contract where corpID='" + corpID +"'");
		return list;
	}
	public List findChargesAndType(String contractType,String corpID){
		List<ItemsJEquip> list = getHibernateTemplate().find("from Charges where corpID='" + corpID +"' AND contract='" + contractType +"'");
		return list;
	}

	public List getListValue(String descript, String type, String controlled) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List findResourceContractByPartnerId(Long parentId, String corpID){
		String query = "from ResourceContractCharges where corpID='" + corpID +"' AND parentId='" + parentId +"'";
		List list = getHibernateTemplate().find(query);
		return list;
	}
	public List getResource(String sessionCorpID,String type,String descript){
		String query = "select descript from ItemsJEquip where corpID='" + sessionCorpID +"' AND type='" + type +"' and descript like '"+descript+"%'";
		List<ItemsJEquip> list = new ArrayList<ItemsJEquip>();
		list = getHibernateTemplate().find(query);
		return list;
	}
	 public List findCharges(String contract,String chargeCode,String chargeDesc,String sessionCorpID){
		 if(chargeCode.length() > 0){
			 chargeCode = chargeCode.toUpperCase();
		 }
		 String query = "from Charges where corpID='" + sessionCorpID +"' AND contract='" + contract +"' and charge like '%"+chargeCode+"%' and description like '%"+chargeDesc+"%'";
		 List<ItemsJEquip> list = getHibernateTemplate().find(query);
		 return list;
	 }

	public List findContracts(String contractType, String contractDesc,	String sessionCorpID) {
		 if(contractType.length() > 0){
//			 contractType = contractType.toUpperCase();
		 }
		List<ItemsJEquip> list = getHibernateTemplate().find("from Contract where corpID='" + sessionCorpID +"' AND contract like '%" + contractType +"%' and description like '%"+contractDesc+"%'");
		return list;
	}

	public List findResource(String corpID) {
		List<ItemsJEquip> list = getHibernateTemplate().find("select descript from ItemsJEquip where corpID='" + corpID +"'");
		return list;
	}
	public List findResourceTemplate(String contractType,String corpID){
		String query="select i.type,i.descript,i.cost,i.id,i.frequentlyUsedResources from itemsjequip i where i.corpId='"+corpID+"'"+
						" and i.template is true order by i.frequentlyUsedResources desc"; 
		List list = this.getSession().createSQLQuery(query).list();
		return list;
	}
	
	public List findResourceTemplateForAccPortal(String contractType,String corpID){
		String query="select i.type,i.descript,i.cost,i.id,i.frequentlyUsedResources from itemsjequip i where i.corpId='"+corpID+"'"+
						" and i.resourceForAccPortal is true"; 
		List list = this.getSession().createSQLQuery(query).list();
		return list;
	}
	
	public List checkDuplicateResource(String descript, String sessionCorpID){			
		List check = this.getSession().createSQLQuery("select descript from itemsjequip where descript='"+descript+"' AND corpID='" +sessionCorpID+ "'").list(); 
		return check;
	}

	public List checkDuplicateResourceDayBasis(String descript,String sessionCorpID, String day,String shipNumber) {
		String query = "select id,descript from itemsjbkequip where shipNum='"+shipNumber+"' and day='"+day+"' and descript='"+StringEscapeUtils.escapeSql(descript)+"' and revisionInvoice is not true and ticket is null";
		List check = this.getSession().createSQLQuery(query).list(); 
		return getConvertObjectToModel(check);
	}	
	private List<ItemsJbkEquip> getConvertObjectToModel(List list1){
		List<ItemsJbkEquip> itemList = new ArrayList<ItemsJbkEquip>();
		Iterator it=list1.iterator();
		while(it.hasNext()){
			ItemsJbkEquip u = new ItemsJbkEquip();
	           Object []row= (Object [])it.next();
	           u.setId(Long.valueOf(row[0].toString()));
	           u.setDescript(row[1].toString());
	           
	           itemList.add(u);
		}
		return itemList;
	}
	

public List findMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division,String resource) {
	List list1 = new ArrayList();
	List listMasterList = new ArrayList();
	String jEqipList="SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(JEquip.qty) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id as cid FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'  and category='"+cat+" ' and branch like '%"+branch+"%' and division like '%"+division+"%' and resource like '%"+resource+"%';";	
	if(cat==null || cat.equals("")){
		if(branch==null || branch.equals("")){
			jEqipList="SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(JEquip.qty) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id as cid FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'   and division like '%"+division+"%' and resource like '%"+resource+"%';";
		}else{
			jEqipList="SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(JEquip.qty) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id as cid FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'  and branch like '%"+branch+"%' and division like '%"+division+"%' and resource like '%"+resource+"%' ;";
			
		}
		
		
	}else {
		if(branch==null || branch.equals("")){
			jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(JEquip.qty) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id as cid FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'  and category='"+cat+" '  and division like '%"+division+"%' and resource like '%"+resource+"%';";
			
		}else{
			jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(JEquip.qty) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id as cid FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'  and category='"+cat+" ' and branch like '%"+branch+"%' and division like '%"+division+"%' and resource like '%"+resource+"%';";
			
		}
		
	}
	
	
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
	
	Iterator it=listMasterList.iterator();
	while(it.hasNext()){
           Object []row= (Object [])it.next();
           MaterialDto materialDto=new MaterialDto();
           materialDto.setId(Long.parseLong(row[0].toString()));
           if(row[1] == null){materialDto.setQty("");}else{materialDto.setQty(row[1].toString());}
           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
           if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
           if(row[4] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[4].toString());}
           if(row[5] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[5].toString());}
           if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
           if(row[12] == null){materialDto.setType("");}else{materialDto.setType(row[12].toString());}
           if(row[13] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[13].toString());}
           if(row[14] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[14].toString());}
           if(row[15] == null){materialDto.setLocation("");}else{materialDto.setLocation(row[15].toString());}
           if(row[16] == null){materialDto.setFreeQty("");}else{materialDto.setFreeQty(row[16].toString());}
           if(row[17] == null){materialDto.setTotalCost("");}else{materialDto.setTotalCost(row[17].toString());}
           if(row[18] == null){materialDto.setMinLevel("");}else{materialDto.setMinLevel(row[18].toString());}
           if(row[19] == null){materialDto.setSalesPrice("");}else{materialDto.setSalesPrice(row[19].toString());}
           if(row[20] == null){materialDto.setOoprice("");}else{materialDto.setOoprice(row[20].toString());}
           if(row[21] == null){materialDto.setMaxResourceLimit("");}else{materialDto.setMaxResourceLimit(row[21].toString());}
           if(row[22] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[22].toString());}
           if(row[23] == null){materialDto.setDivision("");}else{materialDto.setDivision(row[23].toString());}
            materialDto.setEquipMaterialsId(Long.parseLong(row[24].toString()));
           list1.add(materialDto);
	}
	
	if(list1.isEmpty()){
		return null;
	}else{
		return list1;
	}
	
}
public java.util.Map findInvDescriptionList(String resource,String division,String category,String sessionCorpID){
	Map<Long,String> desMap=new HashMap<Long,String>();	
	List list=this.getSession().createSQLQuery("SELECT  JEquip.id,concat(JEquip.resource,'-','Warehouse','-',JEquip.branch,'-','Available Quantity','-',CAST(JEquip.qty as CHAR),'-','Unit Cost','-',CAST(c.unitCost as CHAR)) FROM equipmaterialslimits as JEquip,equipmaterialscost as c where JEquip.id=c.equipMaterialsId and  JEquip.corpID='"+sessionCorpID+"' and branch='"+resource+"' and division='"+division+"'  and category='"+category+" ' order by 1").list();
	Iterator it=list.iterator();
	while(it.hasNext()){
           Object []row= (Object [])it.next();
           if(row[0]!=null && row[1]!=null){
           desMap.put(Long.parseLong(row[0].toString()),row[1].toString());
           }
	}
	return desMap;
}
public List getMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division) {
	List list1 = new ArrayList();
	List listMasterList = new ArrayList();
	//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and contract like '"+contract+"%' and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
	//String MasterList="SELECT distinct id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag FROM itemsjbkequip where ticket="+workTicket+" and type='"+type+"' UNION SELECT distinct JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.descript not in (SELECT distinct descript FROM itemsjbkequip where ticket="+workTicket+") and JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
	
	//String MasterList="SELECT id,qty,actualQty,actual,returned,descript,charge,otCharge,dtCharge,cost,packLabour,flag,type,'Y' as checkType FROM itemsjbkequip where (workTicketID = "+id+" or ticket="+workTicket+" ) and type='"+type+"' and (qty > 0 or actualQty >0) and corpID='"+corpID+"' group by id order by descript;";
	
	//listMasterList=this.getSession().createSQLQuery(MasterList).list();
	
		//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.cost as actual ,JEquip.cost as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		//String jEqipList = "SELECT JEquip.id,JEquip.qty,JEquip.qty as actualQty,JEquip.qty as actual ,JEquip.qty as returned,JEquip.descript,JEquip.charge,JEquip.otCharge,JEquip.dtCharge,JEquip.cost,JEquip.packLabour,JEquip.flag FROM itemsjequip as JEquip where JEquip.corpID='"+corpID+"' and JEquip.type='"+type+"' group by JEquip.descript;";
		String jEqipList = "SELECT JEquip.id,0,0 as actualQty,0 as actual ,0 as returned,JEquip.resource,0 ,0,0,c.unitCost as cost,0,0,JEquip.category,'',JEquip.branch,division,(if(maxResourceLimit is null or maxResourceLimit='',0,maxResourceLimit)-if(qty is null or qty='',0,qty)) as freeQty,0,minResourceLimit as minLevel,c.salestCost,c.ownerOpCost,maxResourceLimit,branch,division,c.id FROM equipmaterialslimits as JEquip,equipmaterialscost c where JEquip.id=c.equipMaterialsId and JEquip.corpID='"+sessionCorpID+"'  and category='"+cat+" ' and branch like '%"+branch+"%' and division like '%"+division+"%' group by JEquip.resource;";
		listMasterList=this.getSession().createSQLQuery(jEqipList).list();
	
	Iterator it=listMasterList.iterator();
	while(it.hasNext()){
           Object []row= (Object [])it.next();
           MaterialDto materialDto=new MaterialDto();
           materialDto.setId(Long.parseLong(row[0].toString()));
           if(row[1] == null){materialDto.setQty("");}else{materialDto.setQty(row[1].toString());}
           if(row[2] == null){materialDto.setActualQty("");}else{materialDto.setActualQty(row[2].toString());}
           if(row[3] == null){materialDto.setActual("");}else{materialDto.setActual(row[3].toString());}
           if(row[4] == null){materialDto.setReturned("");}else{materialDto.setReturned(row[4].toString());}
           if(row[5] == null){materialDto.setDescript("");}else{materialDto.setDescript(row[5].toString());}
           if(row[6] == null){materialDto.setCharge("");}else{materialDto.setCharge(row[6].toString());}
           if(row[7] == null){materialDto.setOtCharge("");}else{materialDto.setOtCharge(row[7].toString());}
           if(row[8] == null){materialDto.setDtCharge("");}else{materialDto.setDtCharge(row[8].toString());}
           if(row[9] == null){materialDto.setCost("");}else{materialDto.setCost(row[9].toString());}
           if(row[10] == null){materialDto.setPackLabour("");}else{materialDto.setPackLabour(row[10].toString());}
           if(row[11] == null){materialDto.setFlag("");}else{materialDto.setFlag(row[11].toString());}
           if(row[12] == null){materialDto.setType("");}else{materialDto.setType(row[12].toString());}
           if(row[13] == null){materialDto.setCheckType("");}else{materialDto.setCheckType(row[13].toString());}
           if(row[14] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[14].toString());}
           if(row[15] == null){materialDto.setLocation("");}else{materialDto.setLocation(row[15].toString());}
           if(row[16] == null){materialDto.setFreeQty("");}else{materialDto.setFreeQty(row[16].toString());}
           if(row[17] == null){materialDto.setTotalCost("");}else{materialDto.setTotalCost(row[17].toString());}
           if(row[18] == null){materialDto.setMinLevel("");}else{materialDto.setMinLevel(row[18].toString());}
           if(row[19] == null){materialDto.setSalesPrice("");}else{materialDto.setSalesPrice(row[19].toString());}
           if(row[20] == null){materialDto.setOoprice("");}else{materialDto.setOoprice(row[20].toString());}
           if(row[21] == null){materialDto.setMaxResourceLimit("");}else{materialDto.setMaxResourceLimit(row[21].toString());}
           if(row[22] == null){materialDto.setBranch("");}else{materialDto.setBranch(row[22].toString());}
           if(row[23] == null){materialDto.setDivision("");}else{materialDto.setDivision(row[23].toString());}
            materialDto.setEquipMaterialsId(Long.parseLong(row[24].toString()));
           list1.add(materialDto);
	}
	
	if(list1.isEmpty()){
		return null;
	}else{
		return list1;
	}
	
}
public List findMaterialExtraMasterInfoByContract(String cont, String sessionCorpID,String cat) {
	if(cont==null) cont="";
	if(cat==null || cat.equals("")){
		return this.getSession().createSQLQuery("select distinct descript from itemsjequip where corpID = '"+sessionCorpID+"' and contract  like '"+cont+"%'   order by 1 ").list();
		
	}else{
		return this.getSession().createSQLQuery("select distinct descript from itemsjequip where corpID = '"+sessionCorpID+"' and contract  like '"+cont+"%' and type like '"+cat+"%'  order by 1 ").list();
		
	}
	
}
public void  deleteInventory(Long id){
	this.getSession().createSQLQuery("delete  from equipmaterialscost where equipMaterialsId="+id).executeUpdate();
	this.getSession().createSQLQuery("delete  from equipmaterialslimits where id="+id).executeUpdate();
}
public List checkContract( Long id ,String sessionCorpID,String shipNumber)
{
	String query ="select c.quantity2preset , c.payablePreset  from charges c inner join resourcecontractcharges r on c.contract = r.contract and  c.charge = r.charge inner join billing b on b.contract=r.contract where c.corpid='"+sessionCorpID+"'and r.parentId = '"+id+"' and b.shipnumber='"+shipNumber+"' " ;
	List list = getSession().createSQLQuery(query).list();
	return list;
}

public String getResourceCategoryValue(String sessionCorpID,Long ticket,String resourceType,String resourceDescription) {
	String query ="select * from itemsjbkequip where corpid='"+sessionCorpID+"' and ticket='"+ticket+"' and type='"+resourceType+"' and descript='"+resourceDescription+"'";
	String dataFound="";
	try{
		List dataList= this.getSession().createSQLQuery(query).list();
			if(dataList !=null && (!(dataList.isEmpty())) && dataList.get(0)!=null){
				dataFound	= "Y";
			}else{
				dataFound	= "N";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	return dataFound;
}
}
