package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TaskDetail;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TaskDetailManager;
import com.trilasoft.app.service.WorkTicketManager;

public class OperationsIntelligenceAction extends BaseAction implements Preparable{
	private OperationsIntelligenceManager operationsIntelligenceManager;
	private OperationsIntelligence operationsIntelligence;
	private String sessionCorpID;
	private String usertype;
	private String fieldName;
	private String fieldValue;
	private String tableName;
	private Long id;
	private String shipNumber;
	private List resourceListForOI;
	private Map<String, String> resourceCategory;
	private Map<String, String> house;
	private Map<String, String> tcktservc;
	private RefMasterManager refMasterManager;
	private List resourceTransferOI;
	private List resourceTransferThirdParty;
	private String tempScopeOfWo;
	private String scopeOfWorkOrder;
	private String workTicket;
	private ItemsJbkEquipManager itemsJbkEquipManager;
	private ItemsJbkEquip itemsJbkEquip;
	private Long itemsJbkEquipId;
	private WorkTicketManager workTicketManager;
	private String statusOfWorkTicket = "";
	private String calculateOI;
	private String resourceContractURL;
	private User user;
	private Set<Role> roles;
	private List ticketByDate;
	private List checkQuantityForOI;
	private SystemDefaultManager systemDefaultManager;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private Map<String, String> QUOTESTATUS;
	private String companies="";
	private Map<String, String> job;
	private String contractforResource;
	private Billing billing;
    private CustomerFile customerFile;
	private List claims;
	private CustomerFileManager customerFileManager;
	private BillingManager billingManager;
	private MiscellaneousManager miscellaneousManager;
	private Miscellaneous miscellaneous;
	private Company company;
	private CompanyManager companyManager;
	private TaskDetail taskDetail;
	private TaskDetailManager taskDetailManager;
	private Boolean flag=false;
	private Boolean uIFlagForOI=false;
	private List selectiveInvoiceList;
	private boolean OIStatus ;
	private Map<String, String> house_isactive;
	private Map<String, String> tcktservc_isactive;
	private PartnerPrivateManager partnerPrivateManager;
	private String oiJobList;
	static final Logger logger = Logger.getLogger(OperationsIntelligenceAction.class);
	Date currentdate = new Date();
	
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		if(resourceListForOIStatus==null||resourceListForOIStatus.equals(""))
		{
			resourceListForOIStatus="true";
		}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
			oiJobList=company.getOiJob();  
		  }
		
	}
	
	public OperationsIntelligenceAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
         usertype = user.getUserType();
	}
	
	 @SkipValidation	
	 public String updateOperationsIntelligence()
	 {
		 operationsIntelligenceManager.updateOperationsIntelligence(id,fieldName,fieldValue,tableName,shipNumber,getRequest().getRemoteUser());
		 return SUCCESS;
	 }
	private String oiActiveId;
	private String oiInactiveId;
	 @SkipValidation
	    public String updateStatusResourceListForOI(){
		 try {
			 if(oiActiveId!=null && !oiActiveId.equalsIgnoreCase("")){
				 String[] oiActiveIdList = oiActiveId.split("~");
				 for (String activeIdList:oiActiveIdList){
						String[] activeListVal = activeIdList.split("#");
						
						id = Long.parseLong(activeListVal[0]);
						OIStatus = true;
						if(!activeListVal[1].equalsIgnoreCase("NA")){
							itemsJbkEquipId = Long.parseLong(activeListVal[1]);
						}else{
							itemsJbkEquipId = null;
						}
						if(!activeListVal[2].equalsIgnoreCase("NA")){
							workTicket = activeListVal[2];
						}else{
							workTicket = "";
						}
						workorder = activeListVal[3];
					 	String billToCodeVal="";
						BigDecimal estConsumableFromPP = new BigDecimal(0.00);
						BigDecimal revConsumableFromPP = new BigDecimal(0.00);
						BigDecimal revisionScopeOfAB5Surcharge  = new BigDecimal(0.00);
						BigDecimal aB5Surcharge = new BigDecimal(0.00);
						Boolean revisionaeditAB5Surcharge = true;
						Boolean estConsumCheck = false;
						estConsumCheck = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"estimateConsumablePercentage");
						if(estConsumCheck == false){
							List<Billing> list = billingManager.getByShipNumber(shipNumber);
							for (Billing billing : list) {
								billToCodeVal = billing.getBillToCode();
							}
							List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
							if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
						    PartnerPrivate partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
						    	estConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
						    	if(estConsumableFromPP==null || estConsumableFromPP.equals("")){
									estConsumableFromPP = new BigDecimal(0.00);
								}
							}
						}
						Boolean editAB5Surcharge = false;
						editAB5Surcharge = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"editAB5Surcharge");
						if(editAB5Surcharge == false){
                           if(aB5Surcharge==null || aB5Surcharge.equals("")){
                        	   aB5Surcharge = new BigDecimal(0.00);
											}
                                       }
                     Boolean revConsumCheck = false;
						revConsumCheck = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"revisionConsumablePercentage");
						if(revConsumCheck == true){
							List<Billing> list = billingManager.getByShipNumber(shipNumber);
							for (Billing billing : list) {
								billToCodeVal = billing.getBillToCode();
							}
							List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
							if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
						    PartnerPrivate partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
						    	revConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
						    	if(revConsumableFromPP==null || revConsumableFromPP.equals("")){
						    		revConsumableFromPP = new BigDecimal(0.00);
								}
							}
						}
						revisionaeditAB5Surcharge = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"revisionaeditAB5Surcharge");
						if(revisionaeditAB5Surcharge == false){
                           if(revisionScopeOfAB5Surcharge==null || revisionScopeOfAB5Surcharge.equals("")){
                        	   revisionScopeOfAB5Surcharge = new BigDecimal(0.00);
									}
                                       }
					selectiveInvoiceList = operationsIntelligenceManager.updateSelectiveInvoice(id,OIStatus,sessionCorpID,itemsJbkEquipId,shipNumber,workTicket,workorder,estConsumCheck,estConsumableFromPP,revConsumCheck,revConsumableFromPP,aB5Surcharge,editAB5Surcharge,revisionScopeOfAB5Surcharge,revisionaeditAB5Surcharge);
					
					if(OIStatus==true && (workTicket!=null && !workTicket.equalsIgnoreCase(""))){
						operationsIntelligence=operationsIntelligenceManager.get(id);
						
						itemsJbkEquip = new ItemsJbkEquip();
						itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
						itemsJbkEquip.setCreatedOn(new Date());
						itemsJbkEquip.setUpdatedBy(usertype);
						itemsJbkEquip.setUpdatedOn(new Date());
						itemsJbkEquip.setType(operationsIntelligence.getType());
						itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
						//itemsJbkEquip.setCost(Double.valueOf(0));
						itemsJbkEquip.setComments(operationsIntelligence.getComments());
						itemsJbkEquip.setBeginDate(new Date());
						itemsJbkEquip.setEndDate(new Date());
						itemsJbkEquip.setShipNum(shipNumber);
						itemsJbkEquip.setCorpID(sessionCorpID);
						itemsJbkEquip.setRevisionInvoice(false);
						itemsJbkEquip.setReturned(0.00);
						if((operationsIntelligence.getType()!=null && !operationsIntelligence.getType().equalsIgnoreCase("")) && (operationsIntelligence.getType().equalsIgnoreCase("C") || operationsIntelligence.getType().equalsIgnoreCase("V"))){
							itemsJbkEquip.setCost(operationsIntelligence.getEstimatedbuyrate().doubleValue());
						}else{
							itemsJbkEquip.setCost(0.00);
						}
						itemsJbkEquip.setActual(operationsIntelligence.getEstimatedexpense().doubleValue());
						itemsJbkEquip.setActualQty(0);
						
						try {
							itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						
						try {
							itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
							itemsJbkEquip.setWorkTicketID(Long.parseLong(operationsIntelligence.getWorkTicketId()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						itemsJbkEquip.setTicket(Long.parseLong(workTicket));
						itemsJbkEquip.setTicketTransferStatus(true);
						itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
						
						operationsIntelligence.setItemsJbkEquipId(itemsJbkEquip.getId().toString());
						operationsIntelligenceManager.save(operationsIntelligence);
						 
						//operationsIntelligenceManager.updateOperationsIntelligenceItemsJbkEquipId(operationsIntelligence.getId(), itemsJbkEquip.getId(),operationsIntelligence.getShipNumber(),sessionCorpID);
						if(operationsIntelligence.getTicket() != null){
							operationsIntelligenceManager.getScopeWithValues(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),operationsIntelligence.getType(),operationsIntelligence.getTicket());
						}
					}
				 }
			}
			
			 if(oiInactiveId!=null && !oiInactiveId.equalsIgnoreCase("")){
				 String[] oiInactiveIdList = oiInactiveId.split("~");
				 for (String inActiveIdList:oiInactiveIdList){
						String[] inActiveListVal = inActiveIdList.split("#");
						
						id = Long.parseLong(inActiveListVal[0]);
						OIStatus = false;
						if(!inActiveListVal[1].equalsIgnoreCase("NA")){
							itemsJbkEquipId = Long.parseLong(inActiveListVal[1]);
						}else{
							itemsJbkEquipId = null;
						}
						if(!inActiveListVal[2].equalsIgnoreCase("NA")){
							workTicket = inActiveListVal[2];
						}else{
							workTicket = "";
						}
						workorder = inActiveListVal[3];
					 	String billToCodeVal="";
						BigDecimal estConsumableFromPP = new BigDecimal(0.00);
						BigDecimal revConsumableFromPP = new BigDecimal(0.00);
						BigDecimal revisionScopeOfAB5Surcharge  = new BigDecimal(0.00);
						BigDecimal aB5Surcharge = new BigDecimal(0.00);
						Boolean estConsumCheck = false;
						estConsumCheck = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"estimateConsumablePercentage");
						if(estConsumCheck == false){
							List<Billing> list = billingManager.getByShipNumber(shipNumber);
							for (Billing billing : list) {
								billToCodeVal = billing.getBillToCode();
							}
							List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
							if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
						    PartnerPrivate partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
						    	estConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
						    	if(estConsumableFromPP==null || estConsumableFromPP.equals("")){
									estConsumableFromPP = new BigDecimal(0.00);
								}
							}
						}
						Boolean editAB5Surcharge = false;
						editAB5Surcharge = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"editAB5Surcharge");
						if(editAB5Surcharge == false){
                           if(aB5Surcharge==null || aB5Surcharge.equals("")){
                        	   aB5Surcharge = new BigDecimal(0.00);
											}
                                       }
						Boolean revConsumCheck = false;
						revConsumCheck = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"revisionConsumablePercentage");
						if(revConsumCheck == true){
							List<Billing> list = billingManager.getByShipNumber(shipNumber);
							for (Billing billing : list) {
								billToCodeVal = billing.getBillToCode();
							}
							List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
							if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
						    PartnerPrivate partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
						    	revConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
						    	if(revConsumableFromPP==null || revConsumableFromPP.equals("")){
						    		revConsumableFromPP = new BigDecimal(0.00);
								}
							}
						}
						
						Boolean revisionaeditAB5Surcharge = false;
						revisionaeditAB5Surcharge = operationsIntelligenceManager.findConsumableChecked(shipNumber,workorder,"revisionaeditAB5Surcharge");
						if(revisionaeditAB5Surcharge == false){
                           if(revisionScopeOfAB5Surcharge==null || revisionScopeOfAB5Surcharge.equals("")){
                        	   revisionScopeOfAB5Surcharge = new BigDecimal(0.00);
									}
                                       }
					selectiveInvoiceList = operationsIntelligenceManager.updateSelectiveInvoice(id,OIStatus,sessionCorpID,itemsJbkEquipId,shipNumber,workTicket,workorder,estConsumCheck,estConsumableFromPP,revConsumCheck,revConsumableFromPP,aB5Surcharge,editAB5Surcharge,revisionScopeOfAB5Surcharge,revisionaeditAB5Surcharge);
					if(workTicket != null && !workTicket.equals("")){
						operationsIntelligence=operationsIntelligenceManager.get(id);
						operationsIntelligenceManager.getScopeWithValues(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),operationsIntelligence.getType(),operationsIntelligence.getTicket());
					}
				 }
			}
			
			List estimateRevisionSumVal = operationsIntelligenceManager.findEstimateRevisionExpenseRevenueSum(shipNumber);
		    int tempMultiplyVal = 100;
		    BigDecimal tempDivideVal = new BigDecimal(100);
		    Iterator itr=estimateRevisionSumVal.iterator();
			 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 if(obj[0]!=null){
					 estimatedExpenseSum = new BigDecimal(obj[0].toString());
					 estimatedExpenseSum = estimatedExpenseSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
				 }
				 if(obj[1]!=null){
					 estimatedRevenueSum = new BigDecimal(obj[1].toString());
					 estimatedRevenueSum = estimatedRevenueSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
				 }
				 if(obj[2]!=null){
					 revisionExpenseSum = new BigDecimal(obj[2].toString());
					 revisionExpenseSum = revisionExpenseSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
				 }
				 if(obj[3]!=null){
					 revisionRevenueSum = new BigDecimal(obj[3].toString());
					 revisionRevenueSum = revisionRevenueSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
				 }
			 }
			 estGrossMarginForRevenueVal = estimatedRevenueSum.subtract(estimatedExpenseSum);
			 estGrossMarginForRevenueVal = estGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 revGrossMarginForRevenueVal = revisionRevenueSum.subtract(revisionExpenseSum);
			 revGrossMarginForRevenueVal = revGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 if(estimatedRevenueSum.doubleValue()!=0.00){
				 estGrossMarginForRevenuePercentageVal = estGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(estimatedRevenueSum,0, BigDecimal.ROUND_HALF_UP);
			 }
			 if(revisionRevenueSum.doubleValue()!=0.00){
				 revGrossMarginForRevenuePercentageVal = revGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(revisionRevenueSum,0, BigDecimal.ROUND_HALF_UP);
			 }
			calculateOI = estimatedExpenseSum+","+estimatedRevenueSum+","+estGrossMarginForRevenueVal+","+estGrossMarginForRevenuePercentageVal+","+revisionExpenseSum+","+revisionRevenueSum+","+revGrossMarginForRevenueVal+","+revGrossMarginForRevenuePercentageVal; 
			operationsIntelligenceManager.updateCalculationOfOI(calculateOI,shipNumber);
			
			List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(shipNumber);
		     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
				for (ServiceOrder serviceOrderTemp : serviceOrderList) {
					serviceOrder=serviceOrderTemp;
				}
		     }
			
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		return SUCCESS; 	
	}

	public OperationsIntelligence getOperationsIntelligence() {
		return operationsIntelligence;
	}

	public void setOperationsIntelligence(
			OperationsIntelligence operationsIntelligence) {
		this.operationsIntelligence = operationsIntelligence;
	}

	public void setOperationsIntelligenceManager(
			OperationsIntelligenceManager operationsIntelligenceManager) {
		this.operationsIntelligenceManager = operationsIntelligenceManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	private List scopeForWorkOrder;
	@SkipValidation
	public String addResourcesAjax(){
		try {
			Long StartTime = System.currentTimeMillis();
			String maxWorkOrder = operationsIntelligenceManager.FindMaxWorkOrder(shipNumber);
			List OIValue = operationsIntelligenceManager.FindValueFromOI(shipNumber);
			
			operationsIntelligence = new OperationsIntelligence();
			operationsIntelligence.setShipNumber(shipNumber);
			operationsIntelligence.setCorpID(sessionCorpID);
			operationsIntelligence.setQuantitiy(new BigDecimal(1.00));
			operationsIntelligence.setEsthours(1.0);
			if(OIValue != null){
			operationsIntelligence.setEstmatedSalesTax(new BigDecimal(OIValue.get(0).toString().split("~")[0]));
			operationsIntelligence.setEstmatedConsumables(new BigDecimal(OIValue.get(0).toString().split("~")[1]));
			operationsIntelligence.setEstmatedValuation(new BigDecimal(OIValue.get(0).toString().split("~")[2]));
			operationsIntelligence.setRevisionSalesTax(new BigDecimal(OIValue.get(0).toString().split("~")[3]));
			operationsIntelligence.setRevisionConsumables(new BigDecimal(OIValue.get(0).toString().split("~")[4]));
			operationsIntelligence.setRevisionValuation(new BigDecimal(OIValue.get(0).toString().split("~")[5]));
			operationsIntelligence.setEstimatedTotalExpenseForOI(new BigDecimal(OIValue.get(0).toString().split("~")[6]));
			operationsIntelligence.setEstimatedTotalRevenueForOI(new BigDecimal(OIValue.get(0).toString().split("~")[7]));
			operationsIntelligence.setEstimatedGrossMarginForOI(new BigDecimal(OIValue.get(0).toString().split("~")[8]));
			operationsIntelligence.setEstimatedGrossMarginPercentageForOI(new BigDecimal(OIValue.get(0).toString().split("~")[9]));
			operationsIntelligence.setRevisedTotalExpenseForOI(new BigDecimal(OIValue.get(0).toString().split("~")[10]));
			operationsIntelligence.setRevisedTotalRevenueForOI(new BigDecimal(OIValue.get(0).toString().split("~")[11]));
			operationsIntelligence.setRevisedGrossMarginForOI(new BigDecimal(OIValue.get(0).toString().split("~")[12]));
			operationsIntelligence.setRevisedGrossMarginPercentageForOI(new BigDecimal(OIValue.get(0).toString().split("~")[13]));
			operationsIntelligence.setEstmatedAB5Surcharge(new BigDecimal(OIValue.get(0).toString().split("~")[14]));
			operationsIntelligence.setRevisionaB5Surcharge(new BigDecimal(OIValue.get(0).toString().split("~")[15]));
			}
			if(maxWorkOrder != null && !maxWorkOrder.isEmpty()){
			operationsIntelligence.setWorkorder(maxWorkOrder);
			}else{
			operationsIntelligence.setWorkorder("WO_01");
			}
			operationsIntelligenceManager.save(operationsIntelligence);
			
			//operationsIntelligenceManager.updateExpenseRevenueConsumables(shipNumber);
			 resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax(shipNumber, sessionCorpID,"","true");
			 resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			 List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(operationsIntelligence.getShipNumber());
		     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
				for (ServiceOrder serviceOrderTemp : serviceOrderList) {
					serviceOrder=serviceOrderTemp;
				}
		     }
		     distinctWorkOrderList = operationsIntelligenceManager.findDistinctWorkOrder(operationsIntelligence.getShipNumber(),sessionCorpID);
			/* scopeForWorkOrder=operationsIntelligenceManager.scopeOfWorkOrder(shipNumber, sessionCorpID,"");*/
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\n\n\n\nTime taken to Execute addResourcesAjax() "+timeTaken+"\n\n\n\n\n");
		} catch (Exception e) {
	    	e.printStackTrace();
		}
		 return SUCCESS;
	}
	private String distinctWorkOrder;
	@SkipValidation
	public String copyWorkOrderToResources(){
		Long StartTime = System.currentTimeMillis();
		try {
			String  maxWorkOrder = operationsIntelligenceManager.FindMaxWorkOrder(shipNumber);
			List allWorkOrderList = operationsIntelligenceManager.copyWorkOrderToResource(distinctWorkOrder,shipNumber);
			Iterator it = allWorkOrderList.iterator();
			while(it.hasNext())
			{
				OperationsIntelligence	operationsIntelligence1 = (OperationsIntelligence)it.next();
				operationsIntelligence = new OperationsIntelligence();
				operationsIntelligence.setWorkorder(maxWorkOrder);
				operationsIntelligence.setShipNumber(operationsIntelligence1.getShipNumber());
				operationsIntelligence.setCorpID(operationsIntelligence1.getCorpID());
				//operationsIntelligence.setDate(operationsIntelligence1.getDate());
				//operationsIntelligence.setTicket(operationsIntelligence1.getTicket());
				operationsIntelligence.setTicketStatus(operationsIntelligence1.getTicketStatus());
				operationsIntelligence.setWorkTicketId(operationsIntelligence1.getWorkTicketId());
				operationsIntelligence.setQuantitiy(operationsIntelligence1.getQuantitiy());
				operationsIntelligence.setEsthours(operationsIntelligence1.getEsthours());
				operationsIntelligence.setComments(operationsIntelligence1.getComments());
				operationsIntelligence.setEstimatedquantity(operationsIntelligence1.getEstimatedquantity());
				operationsIntelligence.setEstimatedbuyrate(operationsIntelligence1.getEstimatedbuyrate());
				operationsIntelligence.setEstimatedexpense(operationsIntelligence1.getEstimatedexpense());
				operationsIntelligence.setEstimatedsellrate(operationsIntelligence1.getEstimatedsellrate());
				operationsIntelligence.setEstimatedrevenue(operationsIntelligence1.getEstimatedrevenue());
				operationsIntelligence.setRevisionquantity(operationsIntelligence1.getRevisionquantity());
				operationsIntelligence.setRevisionbuyrate(operationsIntelligence1.getRevisionbuyrate());
				operationsIntelligence.setRevisionexpense(operationsIntelligence1.getRevisionexpense());
				operationsIntelligence.setRevisionsellrate(operationsIntelligence1.getRevisionsellrate());
				operationsIntelligence.setRevisionrevenue(operationsIntelligence1.getRevisionrevenue());
				operationsIntelligence.setType(operationsIntelligence1.getType());
				operationsIntelligence.setDescription(operationsIntelligence1.getDescription());
				operationsIntelligence.setScopeOfWorkOrder(operationsIntelligence1.getScopeOfWorkOrder());
				//operationsIntelligence.setItemsJbkEquipId(operationsIntelligence1.getItemsJbkEquipId());
				operationsIntelligence.setRevisionConsumables(operationsIntelligence1.getRevisionConsumables().add(operationsIntelligence1.getRevisionScopeOfConsumables()));
				operationsIntelligence.setEstmatedConsumables(operationsIntelligence1.getEstmatedConsumables().add(operationsIntelligence1.getConsumables()));
				operationsIntelligence.setEstmatedSalesTax(operationsIntelligence1.getEstmatedSalesTax());
				operationsIntelligence.setRevisionSalesTax(operationsIntelligence1.getRevisionSalesTax());
				operationsIntelligence.setEstmatedValuation(operationsIntelligence1.getEstmatedValuation());
				operationsIntelligence.setRevisionValuation(operationsIntelligence1.getRevisionValuation());
				operationsIntelligence.setRevisedQuantity(operationsIntelligence1.getRevisedQuantity());
				operationsIntelligence.setRevisionComment(operationsIntelligence1.getRevisionComment());
				operationsIntelligence.setConsumables(operationsIntelligence1.getConsumables());
				operationsIntelligence.setRevisionScopeOfConsumables(operationsIntelligence1.getRevisionScopeOfConsumables());
				operationsIntelligence.setUpdatedBy(getRequest().getRemoteUser());
				operationsIntelligence.setUpdatedOn(new Date());
				
				operationsIntelligence.setaB5Surcharge(operationsIntelligence1.getaB5Surcharge());
				operationsIntelligence.setRevisionScopeOfAB5Surcharge(operationsIntelligence1.getRevisionScopeOfAB5Surcharge());
				operationsIntelligence.setEstmatedAB5Surcharge(operationsIntelligence1.getEstmatedAB5Surcharge().add(operationsIntelligence1.getaB5Surcharge()));
				operationsIntelligence.setRevisionaB5Surcharge(operationsIntelligence1.getRevisionaB5Surcharge().add(operationsIntelligence1.getRevisionScopeOfAB5Surcharge()));
				
				operationsIntelligenceManager.save(operationsIntelligence);
			}
			 //resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax(shipNumber, sessionCorpID,"");
			// resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			/* scopeForWorkOrder=operationsIntelligenceManager.scopeOfWorkOrder(shipNumber, sessionCorpID,"");*/
		} catch (Exception e) {
	    	e.printStackTrace();
		}
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\n\n\n\nTime taken to Execute copyWorkOrderToResources() "+timeTaken+"\n\n\n\n\n");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String addWorkOrderToResources(String shipNumberOld,String shipNumber,Long sid,OperationsIntelligenceManager operationsIntelligenceManager  ){
		try {
			
			/*StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	        StackTraceElement element = stackTrace[2];
	        System.out.println("I was called by a method named: " + element.getMethodName());
	        System.out.println("That method is in class: " + element.getClassName());*/
			
			List allWorkOrderList = operationsIntelligenceManager.addWorkOrderToResource(shipNumberOld);
			
			Iterator it = allWorkOrderList.iterator();
			while(it.hasNext())
			{
				OperationsIntelligence	operationsIntelligence1 = (OperationsIntelligence)it.next();
				operationsIntelligence = new OperationsIntelligence();
				operationsIntelligence.setWorkorder(operationsIntelligence1.getWorkorder());
				operationsIntelligence.setShipNumber(shipNumber);
				operationsIntelligence.setServiceOrderId(sid);
				operationsIntelligence.setCorpID(operationsIntelligence1.getCorpID());
				//operationsIntelligence.setDate(operationsIntelligence1.getDate());
				//operationsIntelligence.setTicket(operationsIntelligence1.getTicket());
				//operationsIntelligence.setTicketStatus(operationsIntelligence1.getTicketStatus());
				//operationsIntelligence.setWorkTicketId(operationsIntelligence1.getWorkTicketId());
				operationsIntelligence.setQuantitiy(operationsIntelligence1.getQuantitiy());
				operationsIntelligence.setEsthours(operationsIntelligence1.getEsthours());
				operationsIntelligence.setComments(operationsIntelligence1.getComments());
				operationsIntelligence.setEstimatedquantity(operationsIntelligence1.getEstimatedquantity());
				operationsIntelligence.setEstimatedbuyrate(operationsIntelligence1.getEstimatedbuyrate());
				operationsIntelligence.setEstimatedexpense(operationsIntelligence1.getEstimatedexpense());
				operationsIntelligence.setEstimatedsellrate(operationsIntelligence1.getEstimatedsellrate());
				operationsIntelligence.setEstimatedrevenue(operationsIntelligence1.getEstimatedrevenue());
				operationsIntelligence.setRevisionquantity(new BigDecimal(0));
				//operationsIntelligence.setRevisionbuyrate(operationsIntelligence1.getRevisionbuyrate());
				//operationsIntelligence.setRevisionexpense(operationsIntelligence1.getRevisionexpense());
				//operationsIntelligence.setRevisionsellrate(operationsIntelligence1.getRevisionsellrate());
				//operationsIntelligence.setRevisionrevenue(operationsIntelligence1.getRevisionrevenue());
				operationsIntelligence.setType(operationsIntelligence1.getType());
				operationsIntelligence.setDescription(operationsIntelligence1.getDescription());
				operationsIntelligence.setScopeOfWorkOrder(operationsIntelligence1.getScopeOfWorkOrder());
				//operationsIntelligence.setItemsJbkEquipId(operationsIntelligence1.getItemsJbkEquipId());
				//operationsIntelligence.setRevisionConsumables(operationsIntelligence1.getRevisionConsumables());
				operationsIntelligence.setEstmatedConsumables(operationsIntelligence1.getEstmatedConsumables());
				operationsIntelligence.setEstmatedSalesTax(operationsIntelligence1.getEstmatedSalesTax());
				//operationsIntelligence.setRevisionSalesTax(operationsIntelligence1.getRevisionSalesTax());
				operationsIntelligence.setEstmatedValuation(operationsIntelligence1.getEstmatedValuation());
				//operationsIntelligence.setRevisionValuation(operationsIntelligence1.getRevisionValuation());
				operationsIntelligence.setRevisedQuantity(new BigDecimal(0));
				//operationsIntelligence.setRevisionComment(operationsIntelligence1.getRevisionComment());
				operationsIntelligence.setUpdatedBy(getRequest().getRemoteUser());
				operationsIntelligence.setCreatedBy(getRequest().getRemoteUser());
				operationsIntelligence.setNumberOfComputer(operationsIntelligence1.getNumberOfComputer());
				operationsIntelligence.setNumberOfEmployee(operationsIntelligence1.getNumberOfEmployee());
				operationsIntelligence.setSalesTax(operationsIntelligence1.getSalesTax());
				operationsIntelligence.setConsumables(operationsIntelligence1.getConsumables());
				//operationsIntelligence.setRevisionHours(0.00);
				operationsIntelligence.setCreatedOn(new Date());
				operationsIntelligence.setUpdatedOn(new Date());
				operationsIntelligence.setDisplayPriority(operationsIntelligence1.getDisplayPriority());
				operationsIntelligence.setEstimatedGrossMarginForOI(operationsIntelligence1.getEstimatedGrossMarginForOI());
				operationsIntelligence.setEstimatedTotalExpenseForOI(operationsIntelligence1.getEstimatedTotalExpenseForOI());
				operationsIntelligence.setEstimatedTotalRevenueForOI(operationsIntelligence1.getEstimatedTotalRevenueForOI());
				//operationsIntelligence.setRevisedGrossMarginForOI(operationsIntelligence1.getRevisedGrossMarginForOI());
				//operationsIntelligence.setRevisedTotalExpenseForOI(operationsIntelligence1.getRevisedTotalExpenseForOI());
				//operationsIntelligence.setRevisedTotalRevenueForOI(operationsIntelligence1.getRevisedTotalRevenueForOI());
				operationsIntelligence.setEstimatedGrossMarginPercentageForOI(operationsIntelligence1.getEstimatedGrossMarginPercentageForOI());
				//operationsIntelligence.setRevisedGrossMarginPercentageForOI(operationsIntelligence1.getRevisedGrossMarginPercentageForOI());
				operationsIntelligence.setaB5Surcharge(operationsIntelligence1.getaB5Surcharge());
				operationsIntelligence.setEstmatedAB5Surcharge(operationsIntelligence1.getEstmatedAB5Surcharge());
				//operationsIntelligence.setRevisionaB5Surcharge(operationsIntelligence1.getRevisionaB5Surcharge());
				
				operationsIntelligenceManager.save(operationsIntelligence);
			}
			 //resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax(shipNumber, sessionCorpID,"");
			// resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			/* scopeForWorkOrder=operationsIntelligenceManager.scopeOfWorkOrder(shipNumber, sessionCorpID,"");*/
		} catch (Exception e) {
	    	e.printStackTrace();
		}
		 return SUCCESS;
	}
private String action;
private String template;
	public String insertWorkOrderToResources(){
		Long StartTime = System.currentTimeMillis();
		String woNumberFromOI = distinctWorkOrder.split("_")[1];
		Integer workOrderIntValue;
		if(action.equalsIgnoreCase("Before"))
		{ workOrderIntValue = Integer.parseInt(woNumberFromOI)-1;
			 
		     if(workOrderIntValue==0)
		         {
				 workOrderIntValue = 1;
	              }
			 else
			 {
				 workOrderIntValue = Integer.parseInt(woNumberFromOI)-1;
			 }
		}
		else
		{		 workOrderIntValue = Integer.parseInt(woNumberFromOI)+1;
		
        }
		Integer woValue = Integer.parseInt(woNumberFromOI);
		List distinctList = operationsIntelligenceManager.distinctWOForInsert(action,shipNumber, distinctWorkOrder);
		Iterator it = distinctList.iterator();
		while(it.hasNext()){
		OperationsIntelligence operationsIntelligence =(OperationsIntelligence) it.next();
		String wo = operationsIntelligence.getWorkorder();
		Integer woNumber = Integer.parseInt(wo.split("_")[1]);
		if(usertype.equalsIgnoreCase("ACCOUNT")){
			usertype = "AC_"+getRequest().getRemoteUser();
		}else if(usertype.equalsIgnoreCase("DRIVER")){
			usertype = "DR_"+getRequest().getRemoteUser();
		}else{
			usertype = getRequest().getRemoteUser();
		}
	    operationsIntelligenceManager.updateWorkOrder(wo, shipNumber,usertype);
		}
		operationsIntelligence = new OperationsIntelligence();
		if(workOrderIntValue <10){
		operationsIntelligence.setWorkorder("WO_0"+workOrderIntValue.intValue());
		}else{
			operationsIntelligence.setWorkorder("WO_"+workOrderIntValue.intValue());	
		}
		operationsIntelligence.setShipNumber(shipNumber);
		operationsIntelligence.setCorpID(sessionCorpID);
		operationsIntelligence.setQuantitiy(new BigDecimal(1));
		operationsIntelligence.setEsthours(1.0);
		operationsIntelligenceManager.save(operationsIntelligence);
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\n\n\n\nTime taken to Execute insertWorkOrderToResources() "+timeTaken+"\n\n\n\n\n");
		return SUCCESS;
	}
	
	private String oiValue;
	private Long sid;
	private String delimeter1;
	private String delimeter;
	private String returnAjaxStringValue; 
	private String checkForContainsTicket;
	public Date getDateFormat(String dateTemp){
		try {
			if(dateTemp == null || "".equals(dateTemp.trim())){
				return null;
			}else{
				try {
					return new SimpleDateFormat("yyyy-MM-dd").parse(dateTemp);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		Date date = null;
		return date;
	}
	private List workTicketList;
	private String resourceListForOIStatus;
	@SkipValidation
	public String autoSaveOIAjax(){
		Long StartTime = System.currentTimeMillis();
		String billToCodeVal="";
		List partnerPrivateList=new ArrayList();
		System.out.println("\n\n\n\n sid "+sid);
		serviceOrder = serviceOrderManager.get(sid);
		billing = billingManager.get(sid);
		PartnerPrivate partnerPrivateObj = null;
		if(billing!=null){
		billToCodeVal = billing.getBillToCode(); 
		partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
		if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
		     partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
		} 
		}
		if(resourceListForOIStatus==null||resourceListForOIStatus.equals(""))
		{
			resourceListForOIStatus="true";
		} 
		resourceContractURL="?id="+id;
		String workOrderGroup= "";
		
		BigDecimal estConsumableFromPP = new BigDecimal(0.00);
		BigDecimal surchargePercentage = new BigDecimal(0.00);
		BigDecimal revConsumableFromPP = new BigDecimal(0.00);
		MathContext mc = new MathContext(4);
		BigDecimal multiplyVal = new BigDecimal(0.05);
		BigDecimal divideVal = new BigDecimal(100);
		boolean editAB5SurchargeCheck = false;
		boolean revisionaeditAB5Surcharge = false;
		if(oiValue != null && oiValue.trim().length() > 0){
			String strAcc[] = oiValue.split(delimeter1);
			for (String str : strAcc) {
				String str1[] = str.split(delimeter);
				Long id=Long.parseLong(str1[0]);
				if((new BigDecimal(str1[5].toString()).compareTo( BigDecimal.ZERO) == 0)  && !(str1[3].equalsIgnoreCase("T"))){
					workOrderGroup ="'"+str1[1].toString().trim()+"'";
				}
				operationsIntelligence=operationsIntelligenceManager.get(id);
				operationsIntelligence.setUpdatedBy(getRequest().getRemoteUser());
				operationsIntelligence.setUpdatedOn(new Date());
				
				operationsIntelligence.setWorkorder(str1[1]+"");
				try{
					if(str1[2].trim() == null || str1[2].trim().equalsIgnoreCase("null") || str1[2].trim().equalsIgnoreCase("")){
						operationsIntelligence.setDate(null);
					}else{
						operationsIntelligence.setDate(getDateFormat(str1[2]));
					}
				
				}catch(Exception e){
					operationsIntelligence.setDate(null);
					e.printStackTrace();
				}
				operationsIntelligence.setType(str1[3]+"");
				operationsIntelligence.setDescription(str1[4]+"");
				try{
					operationsIntelligence.setQuantitiy(new BigDecimal(str1[5].toString()));
					}catch(Exception e){
						operationsIntelligence.setQuantitiy(new BigDecimal(0));
						e.printStackTrace();
					}
					if(new BigDecimal(str1[5].toString()).compareTo( BigDecimal.ZERO) == 0){
					 operationsIntelligence.setTicketStatus("Cancel");
					}
				operationsIntelligence.setEsthours(new Double(str1[6]+""));
				try{
					operationsIntelligence.setEstimatedquantity(new BigDecimal(str1[8].toString()));
				}catch(Exception e){
					operationsIntelligence.setEstimatedquantity(new BigDecimal(0));
					e.printStackTrace();
				}
				operationsIntelligence.setEstimatedbuyrate(new BigDecimal(str1[9]+""));
				operationsIntelligence.setEstimatedexpense(new BigDecimal(str1[10]+""));
				operationsIntelligence.setEstimatedsellrate(new BigDecimal(str1[11]+""));
				operationsIntelligence.setEstimatedrevenue(new BigDecimal(str1[12]+""));
				
				if(str1[3].equalsIgnoreCase("C") || str1[3].equalsIgnoreCase("V")){
					boolean estConsumCheck = false;
					//estConsumCheck = operationsIntelligenceManager.findConsumableChecked(operationsIntelligence.getShipNumber(),str1[1]+"","estimateConsumablePercentage");
					if(operationsIntelligence.getEstimateConsumablePercentage() !=null && operationsIntelligence.getEstimateConsumablePercentage() && operationsIntelligence.getStatus() && (operationsIntelligence.getQuantitiy() == null  || operationsIntelligence.getQuantitiy().doubleValue()!=0) && (operationsIntelligence.getTicketStatus() == null || operationsIntelligence.getTicketStatus().trim().equals(""))){
						estConsumCheck = true;
					}
					if((!(estConsumCheck))){ 
						if(partnerPrivateObj!=null ){ 
					    	estConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
					    	if(estConsumableFromPP==null || estConsumableFromPP.equals("")){
								estConsumableFromPP = new BigDecimal(0.00);
							}
						}
						if(estConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
							operationsIntelligence.setConsumables(operationsIntelligence.getEstimatedrevenue().multiply(estConsumableFromPP).divide(divideVal, mc));
						}else{
							operationsIntelligence.setConsumables(operationsIntelligence.getEstimatedrevenue().multiply(multiplyVal, mc));
						}
					}
					
					operationsIntelligence.setEstimateConsumablePercentage(estConsumCheck);
				}else{
					operationsIntelligence.setConsumables(new BigDecimal(0.00));
				}
			//--------------------------------------------	
				if(str1[3].equalsIgnoreCase("C") || str1[3].equalsIgnoreCase("V")){
					//estConsumCheck = operationsIntelligenceManager.findConsumableChecked(operationsIntelligence.getShipNumber(),str1[1]+"","estimateConsumablePercentage");
					if(operationsIntelligence.getEditAB5Surcharge() !=null && operationsIntelligence.getEditAB5Surcharge() && operationsIntelligence.getStatus() && (operationsIntelligence.getQuantitiy() == null  || operationsIntelligence.getQuantitiy().doubleValue()!=0) && (operationsIntelligence.getTicketStatus() == null || operationsIntelligence.getTicketStatus().trim().equals(""))){
						editAB5SurchargeCheck = true;
					}
					if((!(editAB5SurchargeCheck))){ 
                         if(surchargePercentage==null || surchargePercentage.equals("")){
                        	 surchargePercentage = new BigDecimal(0.00);
							}
					
						if(surchargePercentage.compareTo(BigDecimal.ZERO) != 0){
							operationsIntelligence.setaB5Surcharge(operationsIntelligence.getEstimatedrevenue().multiply(surchargePercentage).divide(divideVal, mc));
						}
					}
					
					operationsIntelligence.setEditAB5Surcharge(editAB5SurchargeCheck);
				}else{
					operationsIntelligence.setaB5Surcharge(new BigDecimal(0.00));
				}
				//------------------------------------------------------------------
				try{
					operationsIntelligence.setRevisionquantity(new BigDecimal(str1[13].toString()));
				}catch(Exception e){
					operationsIntelligence.setRevisionquantity(new BigDecimal(0));
					e.printStackTrace();
				}
				operationsIntelligence.setRevisionbuyrate(new BigDecimal(str1[14]+""));
				operationsIntelligence.setRevisionexpense(new BigDecimal(str1[15]+""));
				operationsIntelligence.setRevisionsellrate(new BigDecimal(str1[16]+""));
				operationsIntelligence.setRevisionrevenue(new BigDecimal(str1[17]+""));
				if(str1[3].equalsIgnoreCase("C") || str1[3].equalsIgnoreCase("V")){
					boolean revConsumCheck = false;
					//revConsumCheck = operationsIntelligenceManager.findConsumableChecked(operationsIntelligence.getShipNumber(),str1[1]+"","revisionConsumablePercentage");
					if(operationsIntelligence.getRevisionConsumablePercentage() !=null && operationsIntelligence.getRevisionConsumablePercentage() && operationsIntelligence.getStatus() && (operationsIntelligence.getQuantitiy() == null  || operationsIntelligence.getQuantitiy().doubleValue()!=0) && (operationsIntelligence.getTicketStatus() == null || operationsIntelligence.getTicketStatus().trim().equals(""))){
						revConsumCheck = true;
					}
					if(!(revConsumCheck)){
						/*List<Billing> list = billingManager.getByShipNumber(operationsIntelligence.getShipNumber());
						for (Billing billing : list) {
							billToCodeVal = billing.getBillToCode();
						}
						List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);*/
						if(partnerPrivateObj!=null ){ 
					    	revConsumableFromPP = partnerPrivateObj.getConsumablePercentage();
					    	if(revConsumableFromPP==null || revConsumableFromPP.equals("")){
								revConsumableFromPP = new BigDecimal(0.00);
							}
						}
						if(revConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
							operationsIntelligence.setRevisionScopeOfConsumables(operationsIntelligence.getEstimatedrevenue().multiply(revConsumableFromPP).divide(divideVal, mc));
						}else{
							operationsIntelligence.setRevisionScopeOfConsumables(operationsIntelligence.getEstimatedrevenue().multiply(multiplyVal, mc));
						}
					}
					
					operationsIntelligence.setRevisionConsumablePercentage(revConsumCheck);
				}else{
					operationsIntelligence.setRevisionScopeOfConsumables(new BigDecimal(0.00));
				}
				
				if(str1[3].equalsIgnoreCase("C") || str1[3].equalsIgnoreCase("V")){
					if(operationsIntelligence.getRevisionaeditAB5Surcharge() !=null && operationsIntelligence.getRevisionaeditAB5Surcharge() && operationsIntelligence.getStatus() && (operationsIntelligence.getQuantitiy() == null  || operationsIntelligence.getQuantitiy().doubleValue()!=0) && (operationsIntelligence.getTicketStatus() == null || operationsIntelligence.getTicketStatus().trim().equals(""))){
						revisionaeditAB5Surcharge = true;
					}
					if(!(revisionaeditAB5Surcharge)){
						 if(surchargePercentage==null || surchargePercentage.equals("")){
                        	 surchargePercentage = new BigDecimal(0.00);
							}
						if(surchargePercentage.compareTo(BigDecimal.ZERO) != 0){
							operationsIntelligence.setRevisionScopeOfAB5Surcharge  (operationsIntelligence.getEstimatedrevenue().multiply(surchargePercentage).divide(divideVal, mc));
						}else{
							operationsIntelligence.setRevisionScopeOfAB5Surcharge(operationsIntelligence.getEstimatedrevenue().multiply(multiplyVal, mc));
						}
					}
					operationsIntelligence.setRevisionaeditAB5Surcharge(revisionaeditAB5Surcharge);
				}else{
					operationsIntelligence.setRevisionScopeOfAB5Surcharge(new BigDecimal(0.00));
				}
				
				try{
					operationsIntelligence.setRevisedQuantity(new BigDecimal(str1[19].toString()));
				}catch(Exception e){
					operationsIntelligence.setRevisedQuantity(new BigDecimal(0));
					e.printStackTrace();
				}
				if(!workOrderGroup.equalsIgnoreCase("") && operationsIntelligence.getTicket()!=null){
					operationsIntelligenceManager.changeStatusOfWorkOrder(workOrderGroup,operationsIntelligence.getShipNumber(),getRequest().getRemoteUser(),operationsIntelligence.getItemsJbkEquipId());
				}
				operationsIntelligence = operationsIntelligenceManager.save(operationsIntelligence);
				if(estConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
					operationsIntelligenceManager.updateEstimateAndRevisionConsumablesWithParnerPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Estimate",estConsumableFromPP);
				}else{
					operationsIntelligenceManager.updateEstimateAndRevisionConsumablesWithOutParnerPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Estimate");
				}
				
				if(!editAB5SurchargeCheck){
					operationsIntelligenceManager.updateEstimateAndRevisionAB5SurchargeWithPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Estimate",surchargePercentage);
				}
				if(revConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
					operationsIntelligenceManager.updateEstimateAndRevisionConsumablesWithParnerPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Revision",revConsumableFromPP);
				}else{
					operationsIntelligenceManager.updateEstimateAndRevisionConsumablesWithOutParnerPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Revision");
				}
				if((operationsIntelligence.getEstimateConsumablePercentage()!=null && operationsIntelligence.getEstimateConsumablePercentage()) && (operationsIntelligence.getRevisionConsumablePercentage()!=null && operationsIntelligence.getRevisionConsumablePercentage())){
					operationsIntelligenceManager.updateEstimateAndRevisionConsumables(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder());
				}
				if(!revisionaeditAB5Surcharge){
					operationsIntelligenceManager.updateEstimateAndRevisionAB5SurchargeWithPercentage(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),"Revision",surchargePercentage);
				}
				if(str1[3].equalsIgnoreCase("T") && new BigDecimal(str1[5].toString()).compareTo( BigDecimal.ZERO) == 0 && operationsIntelligence !=null && operationsIntelligence.getTicket()!=null && (operationsIntelligence.getQuantitiy()==null || operationsIntelligence.getQuantitiy().doubleValue()==0)){
					operationsIntelligenceManager.changeStatusOfWorkOrderForThirdParty(str1[1].toString(),operationsIntelligence.getShipNumber(),getRequest().getRemoteUser(),operationsIntelligence.getTicket());
				}
				/*if(operationsIntelligence.getScopeOfWorkOrder() == null || operationsIntelligence.getScopeOfWorkOrder().isEmpty()){
				String scopeValueOfWorkOrder = operationsIntelligenceManager.scopeByWorkOrderInUserPortal(operationsIntelligence.getWorkorder(), operationsIntelligence.getShipNumber());
				if(scopeValueOfWorkOrder != null && !scopeValueOfWorkOrder.isEmpty()){
				try{
					if(!scopeValueOfWorkOrder.split("~")[0].toString().contains("aa")){
					scopeOfWorkOrder = scopeValueOfWorkOrder.split("~")[0].toString();
					}
					if(!scopeValueOfWorkOrder.split("~")[1].toString().contains("aa")){
					numberOfComputer = scopeValueOfWorkOrder.split("~")[1].toString();
					}
					if(!scopeValueOfWorkOrder.split("~")[2].toString().contains("aa")){
					numberOfEmployee = scopeValueOfWorkOrder.split("~")[2].toString();
					}
					if(!scopeValueOfWorkOrder.split("~")[3].toString().contains("aa")){
					salesTax = Double.parseDouble(scopeValueOfWorkOrder.split("~")[3].toString());
					}
					if(!scopeValueOfWorkOrder.split("~")[4].toString().contains("aa")){
					consumables = scopeValueOfWorkOrder.split("~")[4].toString();
					}
				
				if(scopeValueOfWorkOrder != null && !(scopeValueOfWorkOrder.equals("")))
				{
				  // operationsIntelligenceManager.updateOperationsIntelligenceForScope(operationsIntelligence.getWorkorder(), scopeOfWorkOrder, operationsIntelligence.getShipNumber(),operationsIntelligence.getTicket());
					operationsIntelligenceManager.saveScopeInOperationsIntelligence(operationsIntelligence.getWorkorder(),scopeOfWorkOrder,operationsIntelligence.getShipNumber(),operationsIntelligence.getTicket(),numberOfComputer, numberOfEmployee, salesTax, consumables,"revisionFalse");				
				}
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				}
				}*/
				if(operationsIntelligence.getTicket() != null){
					operationsIntelligenceManager.getScopeWithValues(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),operationsIntelligence.getType(),operationsIntelligence.getTicket());
				}
				String DateForOIFromWT = null;
				String workTicketIdForLink = null;
				if(operationsIntelligence.getType().equalsIgnoreCase("T"))
				{
				if(operationsIntelligence.getItemsJbkEquipId() != null)
				{
					String checkForResourceCopyTowokTicket = operationsIntelligenceManager.checkForResourceTransFerWorkTicket(sessionCorpID, operationsIntelligence.getShipNumber(), str1[1].toString());
					  if(checkForResourceCopyTowokTicket.length()>0){
					  String workTicketForLink = checkForResourceCopyTowokTicket.split("~")[0];
					  workTicketIdForLink = checkForResourceCopyTowokTicket.split("~")[1];
					  DateForOIFromWT = checkForResourceCopyTowokTicket.split("~")[2];
					  }
					
				ItemsJbkEquip itemsJbkEquip ;
				itemsJbkEquip=itemsJbkEquipManager.get(Long.parseLong((operationsIntelligence.getItemsJbkEquipId()).toString()));
				itemsJbkEquip.setType(operationsIntelligence.getType());
				itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
				itemsJbkEquip.setComments(operationsIntelligence.getComments());
				itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
				itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
				if((operationsIntelligence.getType()!=null && !operationsIntelligence.getType().equalsIgnoreCase("")) && (operationsIntelligence.getType().equalsIgnoreCase("C") || operationsIntelligence.getType().equalsIgnoreCase("v"))){
					itemsJbkEquip.setCost(operationsIntelligence.getEstimatedbuyrate().doubleValue());
				}else{
					itemsJbkEquip.setCost(0.00);
				}
				itemsJbkEquip.setActual(operationsIntelligence.getEstimatedexpense().doubleValue());
				itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
				}
				}
				if(!(operationsIntelligence.getType().equalsIgnoreCase("T")))
			    { 
				String checkForResourceCopyTowokTicket = operationsIntelligenceManager.checkForResourceTransFerWorkTicket(sessionCorpID, operationsIntelligence.getShipNumber(), str1[1].toString());
				if(checkForResourceCopyTowokTicket.length()>0){
				String workTicketForLink = checkForResourceCopyTowokTicket.split("~")[0];
				workTicketIdForLink = checkForResourceCopyTowokTicket.split("~")[1];
				DateForOIFromWT = checkForResourceCopyTowokTicket.split("~")[2];
				String targetActualVal = checkForResourceCopyTowokTicket.split("~")[3];		
			  ItemsJbkEquip itemsJbkEquip ;
			  if(operationsIntelligence.getItemsJbkEquipId() == null)
			    {
				  
				 itemsJbkEquip = new ItemsJbkEquip();
				itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
				itemsJbkEquip.setCreatedOn(new Date());
				itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
				itemsJbkEquip.setUpdatedOn(new Date());
				itemsJbkEquip.setType(operationsIntelligence.getType());
				itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
				itemsJbkEquip.setComments(operationsIntelligence.getComments());
				itemsJbkEquip.setBeginDate(new Date());
				itemsJbkEquip.setEndDate(new Date());
				itemsJbkEquip.setShipNum(operationsIntelligence.getShipNumber());
				itemsJbkEquip.setCorpID(sessionCorpID);
				itemsJbkEquip.setRevisionInvoice(false);
				itemsJbkEquip.setReturned(0.00);
				if((operationsIntelligence.getType()!=null && !operationsIntelligence.getType().equalsIgnoreCase("")) && (operationsIntelligence.getType().equalsIgnoreCase("C") || operationsIntelligence.getType().equalsIgnoreCase("v"))){
					itemsJbkEquip.setCost(operationsIntelligence.getEstimatedbuyrate().doubleValue());
				}else{
					itemsJbkEquip.setCost(0.00);
				}
				itemsJbkEquip.setActual(operationsIntelligence.getEstimatedexpense().doubleValue());
				itemsJbkEquip.setActualQty(0);
				
				try {
					itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				}
				
				try {
					itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
					itemsJbkEquip.setWorkTicketID(Long.parseLong(workTicketIdForLink.toString()));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				itemsJbkEquip.setTicket(Long.parseLong(workTicketForLink.toString()));
				itemsJbkEquip.setTicketTransferStatus(true);
				itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
				 Date date = new Date();
				 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
				    String date1 = df.format(date);
				    if(usertype.equalsIgnoreCase("ACCOUNT")){
						usertype = "AC_"+getRequest().getRemoteUser();
					}else if(usertype.equalsIgnoreCase("DRIVER")){
						usertype = "DR_"+getRequest().getRemoteUser();
					}else{
						usertype = getRequest().getRemoteUser();
					}
				 operationsIntelligenceManager.updateOperationsIntelligenceItemsJbkEquipId(operationsIntelligence.getId(), itemsJbkEquip.getId(),operationsIntelligence.getShipNumber(),sessionCorpID);
				operationsIntelligenceManager.updateOperationsIntelligenceTicketNo(Long.parseLong(workTicketForLink.toString()) , str1[1].toString(),operationsIntelligence.getShipNumber(),sessionCorpID,"OI",DateForOIFromWT,Long.parseLong(workTicketIdForLink.toString()),usertype,targetActualVal);
				if(workTicketForLink != null && !workTicketForLink.equals("")){
					operationsIntelligenceManager.getScopeWithValues(operationsIntelligence.getShipNumber(),operationsIntelligence.getWorkorder(),operationsIntelligence.getType(),Long.parseLong(workTicketForLink.toString()));
				}
			}else{
				try {
					itemsJbkEquip=itemsJbkEquipManager.get(Long.parseLong((operationsIntelligence.getItemsJbkEquipId()).toString()));
					itemsJbkEquip.setType(operationsIntelligence.getType());
					itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
					itemsJbkEquip.setComments(operationsIntelligence.getComments());
					itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
					itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
					if((operationsIntelligence.getType()!=null && !operationsIntelligence.getType().equalsIgnoreCase("")) && (operationsIntelligence.getType().equalsIgnoreCase("C") || operationsIntelligence.getType().equalsIgnoreCase("v"))){
						itemsJbkEquip.setCost(operationsIntelligence.getEstimatedbuyrate().doubleValue());
					}else{
						itemsJbkEquip.setCost(0.00);
					}
					itemsJbkEquip.setActual(operationsIntelligence.getEstimatedexpense().doubleValue());
					itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			    	
				}
			  }
				
				
				/*if(workTicketIdForLink !=null){*/
					if(workTicketIdForLink != null && !(workTicketIdForLink.isEmpty())){
					workTicketList = workTicketManager.getServiceForOI(workTicketIdForLink, operationsIntelligence.getCorpID().toString(), operationsIntelligence.getShipNumber().toString());
					Boolean wareHouseCheck = false;
				    String warehouseInWT="";
				    String serviceInWT="";
				    String targetActualInWT = "";
				    String ticketForWt="";
				    Date dateForWT;
				    
					String resorceTypeOI;
					String resorceDescriptionOI;
					BigDecimal resourceQuantitiyOI = new BigDecimal(0) ;
					
					String WTResorceTypeOI;
					String WTResorceDescriptionOI;
					BigDecimal WTResourceQuantitiyOI = new BigDecimal(0);
					
					String resorceType = "";
					String resorceDescription = "";
					BigDecimal resourceQuantitiy = new BigDecimal(0);
					if(workTicketList != null && !(workTicketList.isEmpty()))
				   {
					 Iterator it = workTicketList.iterator();
					 while(it.hasNext())
					 {
						 WorkTicket workTicket123 = (WorkTicket)it.next();
						 warehouseInWT = workTicket123.getWarehouse();
						 serviceInWT = workTicket123.getService();
						 targetActualInWT = workTicket123.getTargetActual();
						 ticketForWt = String.valueOf(workTicket123.getTicket());
					 }
				   }
					if(targetActualInWT.toString().equalsIgnoreCase("T")){
					wareHouseCheck = workTicketManager.getCheckedWareHouse(warehouseInWT, sessionCorpID);
					 if(wareHouseCheck == true){
						 wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
						 Iterator iter123  =wtServiceTypeDyna.iterator();
							while (iter123.hasNext()) {
								if (typeService.equals("")) {
									typeService = iter123.next().toString();
								}else{
									typeService = typeService + "," + iter123.next().toString();
								}
							} 
							user = userManager.getUserByUsername(getRequest().getRemoteUser());
							roles = user.getRoles();
							if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
							}else{
								if((wtServiceTypeDyna.contains(serviceInWT.toString()))){
									resorceTypeOI =  str1[3].toString();
									resorceDescriptionOI = str1[4].toString();
									if(operationsIntelligence.getTicket()==null){
										resourceQuantitiyOI = new BigDecimal(str1[5].toString());
									}
									ticketByDate = workTicketManager.getAllTicketByDate(typeService, getDateFormat(DateForOIFromWT),warehouseInWT,sessionCorpID,"0");	
									if(ticketByDate != null && !(ticketByDate.isEmpty())){
									Iterator it1 = ticketByDate.iterator();
										while(it1.hasNext()){
											Object[] obj = (Object[]) it1.next();
											WTResorceTypeOI = obj[1].toString();
											WTResorceDescriptionOI = obj[2].toString();
											WTResourceQuantitiyOI= new BigDecimal(obj[0].toString());
											if(resorceTypeOI.equalsIgnoreCase(WTResorceTypeOI) && resorceDescriptionOI.equalsIgnoreCase(WTResorceDescriptionOI)){
												resourceQuantitiyOI =resourceQuantitiyOI.add(WTResourceQuantitiyOI);
											}
										}
									}	
								
									String hubIDForQTY = null;
									if(warehouseInWT.equalsIgnoreCase("01"))
									{hubIDForQTY = "Union City";}
									if(warehouseInWT.equalsIgnoreCase("02")){ hubIDForQTY = "Ontario";}
									if(warehouseInWT.equalsIgnoreCase("03")){hubIDForQTY = "Sacramento";}
									if(warehouseInWT.equalsIgnoreCase("04")){hubIDForQTY = "Third Party Warehouse";
									}
									checkQuantityForOI = workTicketManager.checkQuantityInOperation(resorceTypeOI,resorceDescriptionOI,hubIDForQTY,sessionCorpID);
									if(checkQuantityForOI != null){
										Iterator it2 = checkQuantityForOI.iterator();	
										while(it2.hasNext()){
											Object[] obj = (Object[]) it2.next();
											resorceType = 	obj[2].toString();
											resorceDescription = obj[1].toString();
											BigDecimal bd=new BigDecimal(obj[0].toString());
											resourceQuantitiy = new BigDecimal(obj[0].toString());
										}
										
										if(resorceTypeOI.equalsIgnoreCase(resorceType) && resorceDescriptionOI.equalsIgnoreCase(resorceDescription)){
											resourceQuantitiyOI = resourceQuantitiy.subtract(resourceQuantitiyOI);
										}
									}
								}	
							}
					 }
					 if (resourceQuantitiyOI.compareTo(BigDecimal.ZERO) < 0){
						String checkResource = "'"+ticketForWt+"'";
						String checkFotTicketContains= ticketForWt;
						if(returnAjaxStringValue==null || returnAjaxStringValue.isEmpty())
						{
							returnAjaxStringValue = checkResource;
							checkForContainsTicket = checkFotTicketContains;
						}else{
							if(!(checkForContainsTicket.contains(checkFotTicketContains))){		
								returnAjaxStringValue = returnAjaxStringValue+","+checkResource;
								checkForContainsTicket = checkForContainsTicket+" "+checkFotTicketContains;
							}
						}
					}
				}
			}
		}
		}
		if(returnAjaxStringValue !=null && !(returnAjaxStringValue.isEmpty())){
			workTicketManager.updateStatusOfWorkTicket(returnAjaxStringValue,operationsIntelligence.getShipNumber(),operationsIntelligence.getCorpID());
		}
	    //operationsIntelligenceManager.updateExpenseRevenueConsumables(operationsIntelligence.getShipNumber());
	    /*List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(operationsIntelligence.getShipNumber());
	     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
			for (ServiceOrder serviceOrderTemp : serviceOrderList) {
				serviceOrder=serviceOrderTemp;
			}
	     }*/
	    
	    List estimateRevisionSumVal = operationsIntelligenceManager.findEstimateRevisionExpenseRevenueSum(operationsIntelligence.getShipNumber());
	    int tempMultiplyVal = 100;
	    BigDecimal tempDivideVal = new BigDecimal(100);
	    Iterator itr=estimateRevisionSumVal.iterator();
		 while(itr.hasNext()){
			 Object obj[]=(Object[])itr.next();
			 if(obj[0]!=null){
				 estimatedExpenseSum = new BigDecimal(obj[0].toString());
				 estimatedExpenseSum = estimatedExpenseSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 }
			 if(obj[1]!=null){
				 estimatedRevenueSum = new BigDecimal(obj[1].toString());
				 estimatedRevenueSum = estimatedRevenueSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 }
			 if(obj[2]!=null){
				 revisionExpenseSum = new BigDecimal(obj[2].toString());
				 revisionExpenseSum = revisionExpenseSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 }
			 if(obj[3]!=null){
				 revisionRevenueSum = new BigDecimal(obj[3].toString());
				 revisionRevenueSum = revisionRevenueSum.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
			 }
		 }
		 estGrossMarginForRevenueVal = estimatedRevenueSum.subtract(estimatedExpenseSum);
		 estGrossMarginForRevenueVal = estGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
		 revGrossMarginForRevenueVal = revisionRevenueSum.subtract(revisionExpenseSum);
		 revGrossMarginForRevenueVal = revGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(tempDivideVal,2, BigDecimal.ROUND_HALF_UP);
		 if(estimatedRevenueSum.doubleValue()!=0.00){
			 estGrossMarginForRevenuePercentageVal = estGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(estimatedRevenueSum,0, BigDecimal.ROUND_HALF_UP);
		 }
		 if(revisionRevenueSum.doubleValue()!=0.00){
			 revGrossMarginForRevenuePercentageVal = revGrossMarginForRevenueVal.multiply(new BigDecimal(tempMultiplyVal)).divide(revisionRevenueSum,0, BigDecimal.ROUND_HALF_UP);
		 }
		calculateOI = estimatedExpenseSum+","+estimatedRevenueSum+","+estGrossMarginForRevenueVal+","+estGrossMarginForRevenuePercentageVal+","+revisionExpenseSum+","+revisionRevenueSum+","+revGrossMarginForRevenueVal+","+revGrossMarginForRevenuePercentageVal; 
		operationsIntelligenceManager.updateCalculationOfOI(calculateOI,operationsIntelligence.getShipNumber());
		resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax(operationsIntelligence.getShipNumber(),sessionCorpID,"","true");
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		distinctWorkOrderList = operationsIntelligenceManager.findDistinctWorkOrder(operationsIntelligence.getShipNumber(),sessionCorpID);
		//showDistinctWorkOrder = operationsIntelligenceManager.showDistinctWorkOrderForCopy(operationsIntelligence.getShipNumber());
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\n\n\n\nTime taken to Execute autoSaveOIAjax() "+timeTaken+"\n\n\n\n\n");
		 return SUCCESS;
	}
	private String distinctWorkOrderList;
	private BigDecimal estimatedConsumablesValue = new BigDecimal(0.00);
	private BigDecimal revisionConsumablesValue = new BigDecimal(0.00);
	private BigDecimal estimatedExpenseSum = new BigDecimal(0.00);
	private BigDecimal estimatedRevenueSum = new BigDecimal(0.00);
	private BigDecimal revisionExpenseSum = new BigDecimal(0.00);
	private BigDecimal revisionRevenueSum = new BigDecimal(0.00);
	private BigDecimal estGrossMarginForRevenueVal = new BigDecimal(0.00);
	private BigDecimal revGrossMarginForRevenueVal = new BigDecimal(0.00);
	private BigDecimal estGrossMarginForRevenuePercentageVal = new BigDecimal(0.00);
	private BigDecimal revGrossMarginForRevenuePercentageVal = new BigDecimal(0.00);
	private String type;
	private String description;
	private String sellRateBuyRate;

	@SkipValidation
	public String sellReteAndBuyRateByDescriptionAjax()
	{
		sellRateBuyRate = operationsIntelligenceManager.getSellRateBuyRate(type,shipNumber,description,sessionCorpID);
		
		return SUCCESS;	
	}
	List listFinal;
	private List wtServiceTypeDyna;
	private String typeService="";
	private String serviceFromSystemDefault;
	private String warehouseFromSystemDefault;
	private String fromSystemDefault;
	@SkipValidation
	public String transferResourceToWorkTicketForOI()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"transferResourceToWorkTicketForOI method " +" Start");
		fromSystemDefault = systemDefaultManager.OIServicesFromSystemDefault(sessionCorpID);
		if(fromSystemDefault != null && !(fromSystemDefault.isEmpty()))
		{
		serviceFromSystemDefault = fromSystemDefault.split("~")[0]; 
		warehouseFromSystemDefault = fromSystemDefault.split("~")[1]; 
		}
		listFinal = operationsIntelligenceManager.distinctWorkOrderForWT(shipNumber,sessionCorpID);
		resourceTransferThirdParty = operationsIntelligenceManager.distinctWorkOrderForThirdParty(shipNumber, sessionCorpID);
		resourceTransferOI = new ArrayList();
		if(listFinal != null && listFinal.size() > 0 )
		{
		resourceTransferOI.addAll(listFinal);
		}
		if(resourceTransferThirdParty != null && resourceTransferThirdParty.size() > 0 )
		{
		resourceTransferOI.addAll(resourceTransferThirdParty);
		}
		/*CopyOnWriteArrayList  copy = new CopyOnWriteArrayList();*/
		Collections.sort(resourceTransferOI, new WorkOrderComp());
		/*Iterator it = resourceTransferOI.iterator();
		while(it.hasNext())
		{
			OperationsIntelligence operationsIntelligence = (OperationsIntelligence)it.next();
			String workOrder = operationsIntelligence.getWorkorder();
			copy.add(operationsIntelligence);
		}
	int aa= copy.size();
		resourceTransferOI = new ArrayList(copy);*/
		/*resourceTransferOI = new ArrayList(resourceTransferOI);*/
		 String parameters="'TCKTSERVC','HOUSE'";
		 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
		 tcktservc_isactive = new LinkedHashMap<String, String>();
		 house_isactive = new LinkedHashMap<String, String>();
		 tcktservc= new LinkedHashMap<String, String>();
		 house= new LinkedHashMap<String, String>();
		 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,parameters);
		for (RefMasterDTO refObj : allParamValue) {
		 	if(refObj.getParameter().trim().equalsIgnoreCase("TCKTSERVC")){
		 		tcktservc_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		tcktservc.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("HOUSE")){
		 		house_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		house.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}}
		//tcktservc= refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		//house =	refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		 Iterator iter123  =wtServiceTypeDyna.iterator();
			while (iter123.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter123.next().toString();
				} else {
					typeService = typeService + "," + iter123.next().toString();
				}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"transferResourceToWorkTicketForOI method " +" End");
		return SUCCESS;	
	}
	
	class WorkOrderComp implements Comparator
	{

		public int compare(Object o1, Object o2) {
			OperationsIntelligence OperationsIntelligence1 = (OperationsIntelligence)o1;
			OperationsIntelligence OperationsIntelligence2 = (OperationsIntelligence)o2;
			return OperationsIntelligence1.getWorkorder().compareTo(OperationsIntelligence2.getWorkorder());
		}
		
	}
	
	public String deletefromOIResorcesList()
		{
		if(usertype.equalsIgnoreCase("ACCOUNT")){
			usertype = "AC_"+getRequest().getRemoteUser();
		}else if(usertype.equalsIgnoreCase("DRIVER")){
			usertype = "DR_"+getRequest().getRemoteUser();
		}else{
			usertype = getRequest().getRemoteUser();
		}
		operationsIntelligenceManager.deletefromOIResorcesList(id,shipNumber,usertype);
		operationsIntelligenceManager.changeStatusOfWorkTicketFromDel(id,shipNumber,usertype,itemsJbkEquipId);
		if(workTicket!=null && !(workTicket.equalsIgnoreCase("")))
		{
		operationsIntelligenceManager.deletefromWorkTicketResorcesList(itemsJbkEquipId,shipNumber,workTicket,sessionCorpID);
		}
		return SUCCESS;	
		}
	private String workorder;
	private Integer sizeOfScope=500;
	private String numberOfComputer= "";
	private String numberOfEmployee = "";
	private Double salesTax=0.0;
	private String consumables = "";
	private String aB5Surcharge = "";

	private List crewList;
	private String targetActualValue = "";
	@SkipValidation
	public String scopeOfWO()
	{
		/*try{
			if(ticket != null)
			crewList = operationsIntelligenceManager.crewListBYTicket(ticket,sessionCorpID);
		}catch(Exception e){
			
		}
		
		String valueForScope = operationsIntelligenceManager.findScopeOfWorkOrder(id,shipNumber);
		if(!valueForScope.split("~")[0].toString().contains("aa")){
		scopeOfWorkOrder = valueForScope.split("~")[0].toString();
		}
		if(!valueForScope.split("~")[1].toString().contains("aa")){
		numberOfComputer = valueForScope.split("~")[1].toString();
		}
		if(!valueForScope.split("~")[2].toString().contains("aa")){
		numberOfEmployee = valueForScope.split("~")[2].toString();
		}
		if(!valueForScope.split("~")[3].toString().contains("aa")){
		salesTax = Double.parseDouble(valueForScope.split("~")[3].toString());
		}
		if(!valueForScope.split("~")[4].toString().contains("aa")){
		consumables = valueForScope.split("~")[4].toString();
		}
		if(!valueForScope.split("~")[5].toString().contains("aa")){
			targetActualValue = valueForScope.split("~")[5].toString();
		}
		if(scopeOfWorkOrder != null && !(scopeOfWorkOrder.isEmpty())){
			sizeOfScope	= sizeOfScope - scopeOfWorkOrder.length();
		}else{
			sizeOfScope = sizeOfScope;
		}*/
		try{
			operationsIntelligence=operationsIntelligenceManager.get(id);
			if(operationsIntelligence.getScopeOfWorkOrder()!=null && !operationsIntelligence.getScopeOfWorkOrder().equalsIgnoreCase("")){
				sizeOfScope	= sizeOfScope - operationsIntelligence.getScopeOfWorkOrder().length();
			}
			/*if(ticket != null){
				crewList = operationsIntelligenceManager.crewListBYTicket(ticket,sessionCorpID);
			}*/
		}catch(Exception e){
		e.printStackTrace();	
		}
		return SUCCESS;	
	}
	
	@SkipValidation
	public String revisionScopeOfWO(){
		try{
			/*String valueForScope = operationsIntelligenceManager.findRevisionScopeOfWorkOrder(id,shipNumber);
			if(!valueForScope.split("~")[0].toString().contains("aa")){
			scopeOfWorkOrder = valueForScope.split("~")[0].toString();
			}
			if(!valueForScope.split("~")[1].toString().contains("aa")){
			numberOfComputer = valueForScope.split("~")[1].toString();
			}
			if(!valueForScope.split("~")[2].toString().contains("aa")){
			numberOfEmployee = valueForScope.split("~")[2].toString();
			}
			if(!valueForScope.split("~")[3].toString().contains("aa")){
			salesTax = Double.parseDouble(valueForScope.split("~")[3].toString());
			}
			if(!valueForScope.split("~")[4].toString().contains("aa")){
			consumables = valueForScope.split("~")[4].toString();
			}
			if(scopeOfWorkOrder != null && !(scopeOfWorkOrder.isEmpty())){
				sizeOfScope	= sizeOfScope - scopeOfWorkOrder.length();
			}else{
				sizeOfScope = sizeOfScope;
			}*/
			operationsIntelligence=operationsIntelligenceManager.get(id);
			if(operationsIntelligence.getRevisionScopeOfWorkOrder()!=null && !operationsIntelligence.getRevisionScopeOfWorkOrder().equalsIgnoreCase("")){
				sizeOfScope	= sizeOfScope - operationsIntelligence.getRevisionScopeOfWorkOrder().length();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	private String valueOfCommentForRevised="";
	private String cmtValue="";
	private String commentField;
	private String flagForCmt;
	private String decodedUrl;
	@SkipValidation
	public String openCommentForRevised()
	{
		 valueOfCommentForRevised = operationsIntelligenceManager.valueOfCommentForRevised(id,shipNumber,commentField);
		 return SUCCESS;	
	}
	
	
	@SkipValidation
	public String saveCommentForRevised()
	{
		try {
			 decodedUrl = new URLCodec().decode(cmtValue);
			System.out.println(decodedUrl);
		} catch (DecoderException e) {
			e.printStackTrace();
		}
		 operationsIntelligenceManager.saveCmtForRevised(id,shipNumber,decodedUrl.trim(),commentField);
		 return SUCCESS;	
	}
	private String revisionScopeValue;
	private boolean consumablePercentageCheck ;
	private boolean editAB5Surcharge ;


	@SkipValidation
	public String setScopeOfWorkOrder(){
		String billToCodeVal = "";
		 BigDecimal consumableFromPP = new BigDecimal(0.00);
		//if(consumablePercentageCheck){
			 List<Billing> list = billingManager.getByShipNumber(shipNumber);
				for (Billing billing : list) {
					billToCodeVal = billing.getBillToCode();
				}
				List partnerPrivateList =partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, billToCodeVal);
				if(partnerPrivateList!=null && !partnerPrivateList.isEmpty() && partnerPrivateList.get(0)!=null){
			    PartnerPrivate partnerPrivateObj = (PartnerPrivate)partnerPrivateList.get(0);
			    	consumableFromPP = partnerPrivateObj.getConsumablePercentage();
				}
		// }
		operationsIntelligenceManager.saveScopeInOperationsIntelligence(workorder,tempScopeOfWo,shipNumber,ticket,numberOfComputer, numberOfEmployee, salesTax, consumables,revisionScopeValue,consumablePercentageCheck,aB5Surcharge,editAB5Surcharge,consumableFromPP);
		return SUCCESS;	
	}
	
	private Long ticket;
	@SkipValidation
	public String statusOfWorkTicketForOI()
	{
		statusOfWorkTicket = workTicketManager.statusOfWorkTicket(ticket, sessionCorpID);
	return SUCCESS;	
	}
	
	private String distinctTicketByWo;
	@SkipValidation
	public String statusOfWorkTicketByWO()
	{
		distinctTicketByWo=operationsIntelligenceManager.checkForTicketBasedOnWorkOrder(sessionCorpID, shipNumber, workorder);
		if(distinctTicketByWo != null && !(distinctTicketByWo.isEmpty())) 
		{
		statusOfWorkTicket = workTicketManager.statusOfWorkTicket(Long.parseLong(distinctTicketByWo), sessionCorpID);
		}
	return SUCCESS;	
	}
	
	private String ticketBasedOnWorkOrder;
	private String ticketStatusBasedOnWorkOrder;
	@SkipValidation
	public String validateWorkOrderByTicketForOI()
	{
		
		ticketBasedOnWorkOrder = operationsIntelligenceManager.checkForTicketBasedOnWorkOrder(sessionCorpID, shipNumber, workorder);
		if(!(ticketBasedOnWorkOrder.equalsIgnoreCase("")))
		{
		ticketStatusBasedOnWorkOrder = workTicketManager.statusOfWorkTicket(Long.parseLong(ticketBasedOnWorkOrder), sessionCorpID);
		if(ticketStatusBasedOnWorkOrder.equalsIgnoreCase("T") || ticketStatusBasedOnWorkOrder.equalsIgnoreCase("P"))
		{
			statusOfWorkTicket="";
		}else{
			statusOfWorkTicket=ticketStatusBasedOnWorkOrder;
		}}
	return SUCCESS;	
	}
	
	@SkipValidation
	public String validateResourecsTransferToWorkTicketButton()
	{
		String  resourceList ="";
		resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax( shipNumber, sessionCorpID,"","true");
		 if(resourceListForOI!=null && !resourceListForOI.isEmpty() && resourceListForOI.get(0)!=null){
			 resourceList="true" ;
		 }
		return resourceList;
	}
	private String forDistinctWo = "false";
	private List findDistinctWorkOrder;
	@SkipValidation
	public String distinctWorkOrder()
	{
		forDistinctWo="true";
		findDistinctWorkOrder = operationsIntelligenceManager.distinctWorkOrderForOI(shipNumber, sessionCorpID,"");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findResourcesByWorkOrder()
	{
		resourceListForOI = operationsIntelligenceManager.findResourcesByWorkOrder(workorder, shipNumber);
		return SUCCESS ;
	}
	
	private String salesTaxFieldName;
    private Double salesTaxForOI;
	@SkipValidation
	public void updateSalesTaxForOI()
	{
		operationsIntelligenceManager.updateSalesTax(salesTaxForOI, shipNumber,salesTaxFieldName);
	}
	
	
	private List showDistinctWorkOrder;
	@SkipValidation
	public String showDistinctWorkOrderForCopy ()
	{
		showDistinctWorkOrder = operationsIntelligenceManager.showDistinctWorkOrderForCopy(shipNumber,sessionCorpID);
		return SUCCESS;
	}
	private Map<String, String> estimatorList;
	private List companyDivis=new ArrayList();
	private Map<String, String> JOB_STATUS;
	private boolean checkAccessQuotation=false;
	private List operationResourceListForOI;
	private Map<String, String> commodits;
	private Map<String, String> commodit;
	private List distinctOperationResourceList;
	private List taskDetailsList;
	private Long workTicketId;
	private String selectedWO;
	private  Map<String, String> taskStatus = new HashMap<String, String>();
	private String resouceTemplate="";
	List<String> list = new ArrayList<String>();
	public String findAllResourcesAjaxForPortal(){
		operationResourceListForOI = operationsIntelligenceManager.findAllResourcesAjax( shipNumber, sessionCorpID,resouceTemplate,"true");	
		Iterator it = operationResourceListForOI.iterator();
		while(it.hasNext()){
			OperationsIntelligence	operationsIntelligence = (OperationsIntelligence)it.next();
			list.add(operationsIntelligence.getWorkorder());
			}
		if( list.size() == new HashSet<String>(list).size()){
			uIFlagForOI = true;
		}
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		return SUCCESS;
	}
	
	public String findAllDistinctResources()
	{
		distinctOperationResourceList = operationsIntelligenceManager.distinctWorkOrderForOI(shipNumber, sessionCorpID,"forTaskDetails");
		tcktservc= refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		return SUCCESS;
	}
	
	public String findAllTaskByTicket()
	{
		taskDetailsList = operationsIntelligenceManager.taskDetailsListForOI(sessionCorpID,ticket);
		//distinctOperationResourceList = operationsIntelligenceManager.distinctWorkOrderForOI(shipNumber, sessionCorpID,"forTaskDetails");
		distictWOList = operationsIntelligenceManager.distinctWOForOI(shipNumber,sessionCorpID,"",workorder);
		taskStatus.put("Open", "Open");
		taskStatus.put("Closed", "Closed");
		taskStatus.put("Transfer", "Transfer");
		category = refMasterManager.findByParameter(sessionCorpID, "TASK_PARAMETER");
		return SUCCESS;
	}
	
	private String currentId;
	private List distictWOList;
	private Map<String, String> category;
	public String showTaskDetails(){
		taskDetailsList = operationsIntelligenceManager.taskDetailsListForOI(sessionCorpID,ticket);
		//distinctOperationResourceList = operationsIntelligenceManager.distinctWorkOrderForOI(shipNumber, sessionCorpID,"forTaskDetails");
		distictWOList = operationsIntelligenceManager.distinctWOForOI(shipNumber,sessionCorpID,"",workorder);
		taskStatus.put("Open", "Open");
		taskStatus.put("Closed", "Closed");
		taskStatus.put("Transfer", "Transfer");
		category = refMasterManager.findByParameter(sessionCorpID, "TASK_PARAMETER");
		return SUCCESS;
	}
	
	String taskDetailValue="";
	@SkipValidation
	public String autoSaveTaskDetails(){
		/*taskDetail.setShipNumber(shipNumber);*/
		if(usertype.equalsIgnoreCase("ACCOUNT")){
			usertype = "AC_"+getRequest().getRemoteUser();
		}else if(usertype.equalsIgnoreCase("DRIVER")){
			usertype = "DR_"+getRequest().getRemoteUser();
		}
		taskDetailManager.saveTaskDetails(id,fieldName,fieldValue,sessionCorpID,selectedWO,usertype);
		if(fieldName.equalsIgnoreCase("workorder")){
			taskDetailsList = taskDetailManager.getTaskFromWorkOrder(shipNumber,selectedWO);
			taskDetailValue= taskDetailManager.getValueFromTaskDetails(id,shipNumber);
		if(taskDetailsList != null && taskDetailsList.size() > 0){
			Iterator it = taskDetailsList.iterator();
			while(it.hasNext()){
				OperationsIntelligence	operationsIntelligence = (OperationsIntelligence)it.next();
				taskDetail = new TaskDetail();
				taskDetail.setCorpId(operationsIntelligence.getCorpID());
				//taskDetail.setWorkorder(selectedWO);
				taskDetail.setTicket(operationsIntelligence.getTicket());
				taskDetail.setWorkTicketId(Long.parseLong(operationsIntelligence.getWorkTicketId()));
				taskDetail.setStatus("Open");
				/*if(taskDetailValue.split("~")[0] != null){
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date date = null;
					try {
						date = dateformatYYYYMMDD1.parse(taskDetailValue.split("~")[0]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if(date != null){
				taskDetail.setDate(date);
				} }*/
				if(taskDetailValue.split("~")[0] != null){
				taskDetail.setDetails(taskDetailValue.split("~")[0]);
				}
				
				try {
					if(taskDetailValue.split("~")[1] != null && !taskDetailValue.split("~")[1].isEmpty() && taskDetailValue.split("~")[1] != "null"){
					taskDetail.setComment(taskDetailValue.split("~")[1]);
					}else{
						taskDetail.setComment("");	
					}
				} catch (Exception e) {
					taskDetail.setComment("");
					e.printStackTrace();
				}
				
				try {
					if(taskDetailValue.split("~")[2] != null && !taskDetailValue.split("~")[2].isEmpty() && taskDetailValue.split("~")[2] != "null"){
					taskDetail.setCategory(taskDetailValue.split("~")[2]);
					}else{
						taskDetail.setCategory("");	
					}
				} catch (Exception e) {
					taskDetail.setCategory("");	
					e.printStackTrace();
				}
				//taskDetail.setCopyId(id);
				taskDetail.setShipNumber(shipNumber);
				taskDetail.setUpdatedBy(usertype);
				taskDetail.setCreatedOn(new Date());
				taskDetailManager.save(taskDetail);
				taskDetailManager.updateTransferField(id,shipNumber);
			}
		}
		}
		return SUCCESS;
	}
	
	public String addTaskDetailsByAjax(){
		if(usertype.equalsIgnoreCase("ACCOUNT")){
			usertype = "AC_"+getRequest().getRemoteUser();
		}else if(usertype.equalsIgnoreCase("DRIVER")){
			usertype = "DR_"+getRequest().getRemoteUser();
		}
		taskDetail = new TaskDetail();
		taskDetail.setCorpId(sessionCorpID);
		taskDetail.setTicket(ticket);
		taskDetail.setWorkTicketId(workTicketId);
		taskDetail.setCreatedBy(getRequest().getRemoteUser());
		taskDetail.setUpdatedBy(usertype);
		taskDetail.setCreatedOn(new Date());
		taskDetail.setUpdatedOn(new Date());
		taskDetail.setWorkorder(workorder);
		taskDetail.setStatus("Open");
		taskDetail.setShipNumber(shipNumber);
		taskDetailManager.save(taskDetail);
		return SUCCESS;
	}
	
	@SkipValidation
	public String autoSaveOIForAccPortal(){
	resourceContractURL="?id="+id;
	String workOrderGroup= "";
	if(oiValue != null && oiValue.trim().length() > 0){
		String strAcc[] = oiValue.split(delimeter1);
		for (String str : strAcc) {
			String str1[] = str.split(delimeter);
			Long id=Long.parseLong(str1[0].toString());
			/*if(Long.parseLong(str1[5].toString()) != 0 && !(str1[3].equalsIgnoreCase("T"))){
				if(workOrderGroup.isEmpty()){
					workOrderGroup ="'"+str1[1].toString().trim()+"'";
				}
			}*/
			
			if((new BigDecimal(str1[5].toString()).compareTo( BigDecimal.ZERO) == 0)  && !(str1[3].equalsIgnoreCase("T"))){
				workOrderGroup ="'"+str1[1].toString().trim()+"'";
			}
			
			operationsIntelligence=operationsIntelligenceManager.get(id);
			if(usertype.equalsIgnoreCase("ACCOUNT")){
				usertype = "AC_"+getRequest().getRemoteUser();
			}else if(usertype.equalsIgnoreCase("DRIVER")){
				usertype = "DR_"+getRequest().getRemoteUser();
			}
			operationsIntelligence.setUpdatedBy(usertype);
			operationsIntelligence.setUpdatedOn(new Date());
			
			operationsIntelligence.setWorkorder(str1[1]+"");
			try{
			operationsIntelligence.setDate(((str1[2] == null || "".equals(str1[2].trim())) ? null : getDateFormat(str1[2])));
			
			}catch(Exception e){
				operationsIntelligence.setDate(null);
				e.printStackTrace();}
			operationsIntelligence.setType(str1[3]+"");
			operationsIntelligence.setDescription(str1[4]+"");
			try{
				//operationsIntelligence.setQuantitiy(Integer.parseInt(str1[5].toString()));
				operationsIntelligence.setQuantitiy(new BigDecimal(str1[5].toString()));
				}catch(Exception e){
					operationsIntelligence.setQuantitiy(new BigDecimal(0));
					e.printStackTrace();
				}
				if((Long.parseLong(str1[5].toString())) == 0)
				{
				 operationsIntelligence.setTicketStatus("Cancel");
				}/*else{
					 operationsIntelligence.setTicketStatus("");
				}*/
			operationsIntelligence.setEsthours(new Double(str1[6]+""));
			operationsIntelligence.setComments(str1[7].toString().replaceAll("\\r\\n|\\r|\\n", " "));
			try{
				//operationsIntelligence.setEstimatedquantity(Integer.parseInt(str1[8].toString()));
				operationsIntelligence.setEstimatedquantity(new BigDecimal(str1[8].toString()));
				}catch(Exception e){
					operationsIntelligence.setEstimatedquantity(new BigDecimal(0));
					e.printStackTrace();
				}
			operationsIntelligence.setEstimatedbuyrate(new BigDecimal(str1[9]+""));
			operationsIntelligence.setEstimatedexpense(new BigDecimal(str1[10]+""));
			operationsIntelligence.setEstimatedsellrate(new BigDecimal(str1[11]+""));
			operationsIntelligence.setEstimatedrevenue(new BigDecimal(str1[12]+""));
			try{
			//operationsIntelligence.setRevisionquantity(Integer.parseInt(str1[13].toString()));
				operationsIntelligence.setRevisionquantity(new BigDecimal(str1[13].toString()));
			}catch(Exception e){
				operationsIntelligence.setRevisionquantity(new BigDecimal(0));
				e.printStackTrace();
			}
			operationsIntelligence.setRevisionbuyrate(new BigDecimal(str1[14]+""));
			operationsIntelligence.setRevisionexpense(new BigDecimal(str1[15]+""));
			operationsIntelligence.setRevisionsellrate(new BigDecimal(str1[16]+""));
			operationsIntelligence.setRevisionrevenue(new BigDecimal(str1[17]+""));
			if(!workOrderGroup.equalsIgnoreCase("") && operationsIntelligence.getTicket()!=null){
				operationsIntelligenceManager.changeStatusOfWorkOrder(workOrderGroup,operationsIntelligence.getShipNumber(),getRequest().getRemoteUser(),operationsIntelligence.getItemsJbkEquipId());
			}
			operationsIntelligence = operationsIntelligenceManager.save(operationsIntelligence);
			if(str1[3].equalsIgnoreCase("T") && Long.parseLong(str1[5].toString()) == 0 && operationsIntelligence !=null && operationsIntelligence.getTicket()!=null && (operationsIntelligence.getQuantitiy()==null || operationsIntelligence.getQuantitiy().doubleValue()==0))
			{
				operationsIntelligenceManager.changeStatusOfWorkOrderForThirdParty(str1[1].toString(),operationsIntelligence.getShipNumber(),getRequest().getRemoteUser(),operationsIntelligence.getTicket());
			}
			
			if(operationsIntelligence.getScopeOfWorkOrder() == null)
			{
			String scopeOfWorkOrder = operationsIntelligenceManager.scopeByWorkOrder(operationsIntelligence.getWorkorder(), operationsIntelligence.getShipNumber());
			if(scopeOfWorkOrder != null && !(scopeOfWorkOrder.equals("")))
			{
			   operationsIntelligenceManager.updateOperationsIntelligenceForScope(operationsIntelligence.getWorkorder(), scopeOfWorkOrder, operationsIntelligence.getShipNumber(),operationsIntelligence.getTicket());
			}
			}
			
			
			 String DateForOIFromWT = null;
			String workTicketIdForLink = null;
			if(operationsIntelligence.getType().equalsIgnoreCase("T"))
			{
			if(operationsIntelligence.getItemsJbkEquipId() != null)
			{
				String checkForResourceCopyTowokTicket = operationsIntelligenceManager.checkForResourceTransFerWorkTicket(sessionCorpID, operationsIntelligence.getShipNumber(), str1[1].toString());
				  if(checkForResourceCopyTowokTicket.length()>0){
				  String workTicketForLink = checkForResourceCopyTowokTicket.split("~")[0];
				  workTicketIdForLink = checkForResourceCopyTowokTicket.split("~")[1];
				  DateForOIFromWT = checkForResourceCopyTowokTicket.split("~")[2];
				  }
				
			ItemsJbkEquip itemsJbkEquip ;
			itemsJbkEquip=itemsJbkEquipManager.get(Long.parseLong((operationsIntelligence.getItemsJbkEquipId()).toString()));
			itemsJbkEquip.setType(operationsIntelligence.getType());
			itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
			itemsJbkEquip.setComments(operationsIntelligence.getComments());
			itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
			itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
			itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
			}
			}
			if(!(operationsIntelligence.getType().equalsIgnoreCase("T")))
			{
		  String checkForResourceCopyTowokTicket = operationsIntelligenceManager.checkForResourceTransFerWorkTicket(sessionCorpID, operationsIntelligence.getShipNumber(), str1[1].toString());
		  if(checkForResourceCopyTowokTicket.length()>0){
		  String workTicketForLink = checkForResourceCopyTowokTicket.split("~")[0];
		  workTicketIdForLink = checkForResourceCopyTowokTicket.split("~")[1];
		  DateForOIFromWT = checkForResourceCopyTowokTicket.split("~")[2];
		  String targetActualVal = checkForResourceCopyTowokTicket.split("~")[3];
		  ItemsJbkEquip itemsJbkEquip ;
		  if(operationsIntelligence.getItemsJbkEquipId() == null)
		    {
			 itemsJbkEquip = new ItemsJbkEquip();
			itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
			itemsJbkEquip.setCreatedOn(new Date());
			itemsJbkEquip.setUpdatedBy(usertype);
			itemsJbkEquip.setUpdatedOn(new Date());
			itemsJbkEquip.setType(operationsIntelligence.getType());
			itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
			itemsJbkEquip.setComments(operationsIntelligence.getComments());
			itemsJbkEquip.setBeginDate(new Date());
			itemsJbkEquip.setEndDate(new Date());
			itemsJbkEquip.setShipNum(shipNumber);
			itemsJbkEquip.setCorpID(sessionCorpID);
			itemsJbkEquip.setRevisionInvoice(false);
			itemsJbkEquip.setReturned(0.00);
			itemsJbkEquip.setCost(0.00);
			itemsJbkEquip.setActual(0.00);
			itemsJbkEquip.setActualQty(0);
			
			try {
				itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
			
			try {
				itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
				itemsJbkEquip.setWorkTicketID(Long.parseLong(workTicketIdForLink.toString()));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			itemsJbkEquip.setTicket(Long.parseLong(workTicketForLink.toString()));
			itemsJbkEquip.setTicketTransferStatus(true);
			itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
			 Date date = new Date();
			 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
			    String date1 = df.format(date);
			 operationsIntelligenceManager.updateOperationsIntelligenceItemsJbkEquipId(operationsIntelligence.getId(), itemsJbkEquip.getId(),operationsIntelligence.getShipNumber(),sessionCorpID);
			 operationsIntelligenceManager.updateOperationsIntelligenceTicketNo(Long.parseLong(workTicketForLink.toString()) , str1[1].toString(),operationsIntelligence.getShipNumber(),sessionCorpID,"OI",DateForOIFromWT,Long.parseLong(workTicketIdForLink.toString()),usertype,targetActualVal);
		}else{
			try {
				itemsJbkEquip=itemsJbkEquipManager.get(Long.parseLong((operationsIntelligence.getItemsJbkEquipId()).toString()));
				itemsJbkEquip.setType(operationsIntelligence.getType());
				itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
				itemsJbkEquip.setComments(operationsIntelligence.getComments());
				itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
				itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
				itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		    	
			}}
				if(workTicketIdForLink != null && !(workTicketIdForLink.isEmpty()))
				{
				workTicketList = workTicketManager.getServiceForOI(workTicketIdForLink, operationsIntelligence.getCorpID().toString(), operationsIntelligence.getShipNumber().toString());
				Boolean wareHouseCheck = false;
			    String warehouseInWT="";
			    String serviceInWT="";
			    String targetActualInWT = "";
			    String ticketForWt="";
			    Date dateForWT;
			    
				String resorceTypeOI;
				String resorceDescriptionOI;
				int resourceQuantitiyOI =0 ;
				
				String WTResorceTypeOI;
				String WTResorceDescriptionOI;
				Integer WTResourceQuantitiyOI;
				
				String resorceType = "";
				String resorceDescription = "";
				Integer resourceQuantitiy = 0;
				if(workTicketList != null && !(workTicketList.isEmpty()))
			   {
				 Iterator it = workTicketList.iterator();
				 while(it.hasNext())
				 {
					 WorkTicket workTicket123 = (WorkTicket)it.next();
					 warehouseInWT = workTicket123.getWarehouse();
					 serviceInWT = workTicket123.getService();
					 targetActualInWT = workTicket123.getTargetActual();
					 ticketForWt = String.valueOf(workTicket123.getTicket());
				 }
			   }
				if(targetActualInWT.toString().equalsIgnoreCase("T")){
				wareHouseCheck = workTicketManager.getCheckedWareHouse(warehouseInWT, sessionCorpID);
				 if(wareHouseCheck == true){
					 wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
					 Iterator iter123  =wtServiceTypeDyna.iterator();
						while (iter123.hasNext()) {
							if (typeService.equals("")) {
								typeService = iter123.next().toString();
					} else {
								typeService = typeService + "," + iter123.next().toString();
					}
					} 
						user = userManager.getUserByUsername(getRequest().getRemoteUser());
						roles = user.getRoles();
						if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
						}else{
							if((wtServiceTypeDyna.contains(serviceInWT.toString())))
							{
							resorceTypeOI =  str1[3].toString();
							resorceDescriptionOI = str1[4].toString();
							if(operationsIntelligence.getTicket()==null)
							{
						    resourceQuantitiyOI =(int)(long)Long.parseLong(str1[5].toString());
							}
						ticketByDate = workTicketManager.getAllTicketByDate(typeService, getDateFormat(DateForOIFromWT),warehouseInWT,sessionCorpID,"0");	
						if(ticketByDate != null && !(ticketByDate.isEmpty()))
						{
						Iterator it1 = ticketByDate.iterator();
						while(it1.hasNext())
						{
							Object[] obj = (Object[]) it1.next();
							WTResorceTypeOI = obj[1].toString();
							WTResorceDescriptionOI = obj[2].toString();
							WTResourceQuantitiyOI= Integer.parseInt(obj[0].toString());
							if(resorceTypeOI.equalsIgnoreCase(WTResorceTypeOI) && resorceDescriptionOI.equalsIgnoreCase(WTResorceDescriptionOI))
						{
							resourceQuantitiyOI =resourceQuantitiyOI+WTResourceQuantitiyOI;
						}
						}}	
						
						String hubIDForQTY = null;
						if(warehouseInWT.equalsIgnoreCase("01"))
						{hubIDForQTY = "Union City";}
						if(warehouseInWT.equalsIgnoreCase("02")){ hubIDForQTY = "Ontario";}
						if(warehouseInWT.equalsIgnoreCase("03")){hubIDForQTY = "Sacramento";}
						if(warehouseInWT.equalsIgnoreCase("04")){hubIDForQTY = "Third Party Warehouse";
						}
						checkQuantityForOI = workTicketManager.checkQuantityInOperation(resorceTypeOI,resorceDescriptionOI,hubIDForQTY,sessionCorpID);
						if(checkQuantityForOI != null)
						{
							Iterator it2 = checkQuantityForOI.iterator();	
							while(it2.hasNext())
							{
								Object[] obj = (Object[]) it2.next();
								resorceType = 	obj[2].toString();
								resorceDescription = obj[1].toString();
								BigDecimal bd=new BigDecimal(obj[0].toString());
								resourceQuantitiy = Integer.valueOf(bd.intValue());
							}
							
							if(resorceTypeOI.equalsIgnoreCase(resorceType) && resorceDescriptionOI.equalsIgnoreCase(resorceDescription))
							{
								resourceQuantitiyOI = resourceQuantitiy-resourceQuantitiyOI;
							}
						}
							}	
						}
				 }
				if(resourceQuantitiyOI < 0)
				{
					
					String checkResource = "'"+ticketForWt+"'";
					String checkFotTicketContains= ticketForWt;
					if(returnAjaxStringValue==null || returnAjaxStringValue.isEmpty())
					{
						returnAjaxStringValue = checkResource;
						checkForContainsTicket = checkFotTicketContains;
					}else{
						if(!(checkForContainsTicket.contains(checkFotTicketContains)))
								{		
						returnAjaxStringValue = returnAjaxStringValue+","+checkResource;
						checkForContainsTicket = checkForContainsTicket+" "+checkFotTicketContains;
								}}
				}
				}
				}
	}}
	if(returnAjaxStringValue !=null && !(returnAjaxStringValue.isEmpty()))
	{
	workTicketManager.updateStatusOfWorkTicket(returnAjaxStringValue,operationsIntelligence.getShipNumber(),operationsIntelligence.getCorpID());
	}
	//operationsIntelligenceManager.updateCalculationOfOI(calculateOI,operationsIntelligence.getShipNumber());
	//operationsIntelligenceManager.updateCalculationOfOI(calculateOI,operationsIntelligence.getShipNumber());
	//operationsIntelligenceManager.changeStatusOfWorkOrder(workOrderGroup,operationsIntelligence.getShipNumber(),getRequest().getRemoteUser());
	 return SUCCESS;
}
	private List copyEstimateList = new ArrayList();
	@SkipValidation
	public String copyEstimateOandIPopUp(){
		copyEstimateList = operationsIntelligenceManager.findDataForCopyEstimate(shipNumber,sessionCorpID);
		return SUCCESS;
	}
	
	private String oid;
	public void copyToRevisionOandI(){
		try{
			if(oid.contains(",")){
				oid = oid.replace(",", "~");
			}
			String arr[]=oid.split("~");
			 for(int x=0;x<arr.length;x++){
				 Long id=Long.parseLong(arr[x].trim().toString());
				 operationsIntelligence=operationsIntelligenceManager.get(id);
				 
				 operationsIntelligence.setRevisedQuantity(operationsIntelligence.getQuantitiy());
				 operationsIntelligence.setRevisionbuyrate(operationsIntelligence.getEstimatedbuyrate());
				 operationsIntelligence.setRevisionexpense(operationsIntelligence.getEstimatedexpense());
				 operationsIntelligence.setRevisionsellrate(operationsIntelligence.getEstimatedsellrate());
				 operationsIntelligence.setRevisionrevenue(operationsIntelligence.getEstimatedrevenue());
				 operationsIntelligence.setRevisionquantity(new BigDecimal(operationsIntelligence.getEsthours()));
				 operationsIntelligence.setRevisionScopeOfWorkOrder(operationsIntelligence.getScopeOfWorkOrder());
				 operationsIntelligence.setRevisionScopeOfNumberOfComputer(operationsIntelligence.getNumberOfComputer());
				 operationsIntelligence.setRevisionScopeOfNumberOfEmployee(operationsIntelligence.getNumberOfEmployee());
				 operationsIntelligence.setRevisionScopeOfSalesTax(operationsIntelligence.getSalesTax());
				 operationsIntelligence.setRevisionScopeOfConsumables(operationsIntelligence.getConsumables());
				 operationsIntelligence.setRevisionConsumablePercentage(operationsIntelligence.getEstimateConsumablePercentage());
				 operationsIntelligence.setRevisionScopeOfAB5Surcharge(operationsIntelligence.getaB5Surcharge());
				 operationsIntelligenceManager.save(operationsIntelligence);
				 operationsIntelligenceManager.updateExpenseRevenueConsumables(operationsIntelligence.getShipNumber());
				 operationsIntelligenceManager.updateExpenseRevenueAB5Surcharge(operationsIntelligence.getShipNumber());
				 operationsIntelligenceManager.updateOperationIntelligenceRevisionSalesTax(operationsIntelligence.getShipNumber());
			 }
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}

	public void setJOB_STATUS(Map<String, String> jOB_STATUS) {
		JOB_STATUS = jOB_STATUS;
	}

	public List getResourceListForOI() {
		return resourceListForOI;
	}

	public void setResourceListForOI(List resourceListForOI) {
		this.resourceListForOI = resourceListForOI;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getOiValue() {
		return oiValue;
	}

	public void setOiValue(String oiValue) {
		this.oiValue = oiValue;
	}

	

	public String getDelimeter1() {
		return delimeter1;
	}

	public void setDelimeter1(String delimeter1) {
		this.delimeter1 = delimeter1;
	}

	public String getDelimeter() {
		return delimeter;
	}

	public void setDelimeter(String delimeter) {
		this.delimeter = delimeter;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSellRateBuyRate() {
		return sellRateBuyRate;
	}

	public void setSellRateBuyRate(String sellRateBuyRate) {
		this.sellRateBuyRate = sellRateBuyRate;
	}

	public List getResourceTransferOI() {
		return resourceTransferOI;
	}

	public void setResourceTransferOI(List resourceTransferOI) {
		this.resourceTransferOI = resourceTransferOI;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public List getResourceTransferThirdParty() {
		return resourceTransferThirdParty;
	}

	public void setResourceTransferThirdParty(List resourceTransferThirdParty) {
		this.resourceTransferThirdParty = resourceTransferThirdParty;
	}

	public List getListFinal() {
		return listFinal;
	}

	public void setListFinal(List listFinal) {
		this.listFinal = listFinal;
	}

	public String getTempScopeOfWo() {
		return tempScopeOfWo;
	}

	public void setTempScopeOfWo(String tempScopeOfWo) {
		this.tempScopeOfWo = tempScopeOfWo;
	}

	public String getScopeOfWorkOrder() {
		return scopeOfWorkOrder;
	}

	public void setScopeOfWorkOrder(String scopeOfWorkOrder) {
		this.scopeOfWorkOrder = scopeOfWorkOrder;
	}

	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	public ItemsJbkEquip getItemsJbkEquip() {
		return itemsJbkEquip;
	}

	public void setItemsJbkEquip(ItemsJbkEquip itemsJbkEquip) {
		this.itemsJbkEquip = itemsJbkEquip;
	}

	public Long getItemsJbkEquipId() {
		return itemsJbkEquipId;
	}

	public void setItemsJbkEquipId(Long itemsJbkEquipId) {
		this.itemsJbkEquipId = itemsJbkEquipId;
	}

	public String getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(String workTicket) {
		this.workTicket = workTicket;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public String getStatusOfWorkTicket() {
		return statusOfWorkTicket;
	}

	public void setStatusOfWorkTicket(String statusOfWorkTicket) {
		this.statusOfWorkTicket = statusOfWorkTicket;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public List getScopeForWorkOrder() {
		return scopeForWorkOrder;
	}

	public void setScopeForWorkOrder(List scopeForWorkOrder) {
		this.scopeForWorkOrder = scopeForWorkOrder;
	}

	public String getWorkorder() {
		return workorder;
	}

	public void setWorkorder(String workorder) {
		this.workorder = workorder;
	}

	public String getTicketBasedOnWorkOrder() {
		return ticketBasedOnWorkOrder;
	}

	public void setTicketBasedOnWorkOrder(String ticketBasedOnWorkOrder) {
		this.ticketBasedOnWorkOrder = ticketBasedOnWorkOrder;
	}

	public String getTicketStatusBasedOnWorkOrder() {
		return ticketStatusBasedOnWorkOrder;
	}

	public void setTicketStatusBasedOnWorkOrder(String ticketStatusBasedOnWorkOrder) {
		this.ticketStatusBasedOnWorkOrder = ticketStatusBasedOnWorkOrder;
	}

	public List getFindDistinctWorkOrder() {
		return findDistinctWorkOrder;
	}

	public void setFindDistinctWorkOrder(List findDistinctWorkOrder) {
		this.findDistinctWorkOrder = findDistinctWorkOrder;
	}

	public String getCalculateOI() {
		return calculateOI;
	}

	public void setCalculateOI(String calculateOI) {
		this.calculateOI = calculateOI;
	}

	public String getForDistinctWo() {
		return forDistinctWo;
	}

	public void setForDistinctWo(String forDistinctWo) {
		this.forDistinctWo = forDistinctWo;
	}

	public String getResourceContractURL() {
		return resourceContractURL;
	}

	public void setResourceContractURL(String resourceContractURL) {
		this.resourceContractURL = resourceContractURL;
	}

	public String getDistinctTicketByWo() {
		return distinctTicketByWo;
	}

	public void setDistinctTicketByWo(String distinctTicketByWo) {
		this.distinctTicketByWo = distinctTicketByWo;
	}

	public Integer getSizeOfScope() {
		return sizeOfScope;
	}

	public void setSizeOfScope(Integer sizeOfScope) {
		this.sizeOfScope = sizeOfScope;
	}

	public List getWtServiceTypeDyna() {
		return wtServiceTypeDyna;
	}

	public void setWtServiceTypeDyna(List wtServiceTypeDyna) {
		this.wtServiceTypeDyna = wtServiceTypeDyna;
	}

	public String getTypeService() {
		return typeService;
	}

	public void setTypeService(String typeService) {
		this.typeService = typeService;
	}

	public List getWorkTicketList() {
		return workTicketList;
	}

	public void setWorkTicketList(List workTicketList) {
		this.workTicketList = workTicketList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public List getTicketByDate() {
		return ticketByDate;
	}

	public void setTicketByDate(List ticketByDate) {
		this.ticketByDate = ticketByDate;
	}

	public List getCheckQuantityForOI() {
		return checkQuantityForOI;
	}

	public void setCheckQuantityForOI(List checkQuantityForOI) {
		this.checkQuantityForOI = checkQuantityForOI;
	}

	public String getCheckForContainsTicket() {
		return checkForContainsTicket;
	}

	public void setCheckForContainsTicket(String checkForContainsTicket) {
		this.checkForContainsTicket = checkForContainsTicket;
	}

	public Double getSalesTaxForOI() {
		return salesTaxForOI;
	}

	public void setSalesTaxForOI(Double salesTaxForOI) {
		this.salesTaxForOI = salesTaxForOI;
	}

	public String getSalesTaxFieldName() {
		return salesTaxFieldName;
	}

	public void setSalesTaxFieldName(String salesTaxFieldName) {
		this.salesTaxFieldName = salesTaxFieldName;
	}

	public List getShowDistinctWorkOrder() {
		return showDistinctWorkOrder;
	}

	public void setShowDistinctWorkOrder(List showDistinctWorkOrder) {
		this.showDistinctWorkOrder = showDistinctWorkOrder;
	}

	public String getDistinctWorkOrder() {
		return distinctWorkOrder;
	}

	public void setDistinctWorkOrder(String distinctWorkOrder) {
		this.distinctWorkOrder = distinctWorkOrder;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public String getServiceFromSystemDefault() {
		return serviceFromSystemDefault;
	}

	public void setServiceFromSystemDefault(String serviceFromSystemDefault) {
		this.serviceFromSystemDefault = serviceFromSystemDefault;
	}

	public String getWarehouseFromSystemDefault() {
		return warehouseFromSystemDefault;
	}

	public void setWarehouseFromSystemDefault(String warehouseFromSystemDefault) {
		this.warehouseFromSystemDefault = warehouseFromSystemDefault;
	}

	public String getFromSystemDefault() {
		return fromSystemDefault;
	}

	public void setFromSystemDefault(String fromSystemDefault) {
		this.fromSystemDefault = fromSystemDefault;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public String getContractforResource() {
		return contractforResource;
	}

	public void setContractforResource(String contractforResource) {
		this.contractforResource = contractforResource;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public List getClaims() {
		return claims;
	}

	public void setClaims(List claims) {
		this.claims = claims;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}

	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Map<String, String> getQUOTESTATUS() {
		return QUOTESTATUS;
	}

	public void setQUOTESTATUS(Map<String, String> qUOTESTATUS) {
		QUOTESTATUS = qUOTESTATUS;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getCompanies() {
		return companies;
	}

	public void setCompanies(String companies) {
		this.companies = companies;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public Map<String, String> getEstimatorList() {
		return estimatorList;
	}

	public void setEstimatorList(Map<String, String> estimatorList) {
		this.estimatorList = estimatorList;
	}

	public List getOperationResourceListForOI() {
		return operationResourceListForOI;
	}

	public void setOperationResourceListForOI(List operationResourceListForOI) {
		this.operationResourceListForOI = operationResourceListForOI;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public List getDistinctOperationResourceList() {
		return distinctOperationResourceList;
	}

	public void setDistinctOperationResourceList(List distinctOperationResourceList) {
		this.distinctOperationResourceList = distinctOperationResourceList;
	}

	public TaskDetail getTaskDetail() {
		return taskDetail;
	}

	public void setTaskDetail(TaskDetail taskDetail) {
		this.taskDetail = taskDetail;
	}

	public Long getWorkTicketId() {
		return workTicketId;
	}

	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}

	public void setTaskDetailManager(TaskDetailManager taskDetailManager) {
		this.taskDetailManager = taskDetailManager;
	}

	public List getTaskDetailsList() {
		return taskDetailsList;
	}

	public void setTaskDetailsList(List taskDetailsList) {
		this.taskDetailsList = taskDetailsList;
	}

	public Map<String, String> getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(Map<String, String> taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getSelectedWO() {
		return selectedWO;
	}

	public void setSelectedWO(String selectedWO) {
		this.selectedWO = selectedWO;
	}

	public String getCurrentId() {
		return currentId;
	}

	public void setCurrentId(String currentId) {
		this.currentId = currentId;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public List getDistictWOList() {
		return distictWOList;
	}

	public void setDistictWOList(List distictWOList) {
		this.distictWOList = distictWOList;
	}
	
	public String getResouceTemplate() {
		return resouceTemplate;
	}
																
	public void setResouceTemplate(String resouceTemplate) {
		this.resouceTemplate = resouceTemplate;
	}

	public Map<String, String> getCategory() {
		return category;
	}

	public void setCategory(Map<String, String> category) {
		this.category = category;
	}

	public String getNumberOfComputer() {
		return numberOfComputer;
	}

	public void setNumberOfComputer(String numberOfComputer) {
		this.numberOfComputer = numberOfComputer;
	}

	public String getNumberOfEmployee() {
		return numberOfEmployee;
	}

	public void setNumberOfEmployee(String numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}

	public String getConsumables() {
		return consumables;
	}

	public void setConsumables(String consumables) {
		this.consumables = consumables;
	}

	public Double getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(Double salesTax) {
		this.salesTax = salesTax;
	}

	public Boolean getuIFlagForOI() {
		return uIFlagForOI;
	}

	public void setuIFlagForOI(Boolean uIFlagForOI) {
		this.uIFlagForOI = uIFlagForOI;
	}

	public String getValueOfCommentForRevised() {
		return valueOfCommentForRevised;
	}

	public void setValueOfCommentForRevised(String valueOfCommentForRevised) {
		this.valueOfCommentForRevised = valueOfCommentForRevised;
	}

	public String getCmtValue() {
		return cmtValue;
	}

	public void setCmtValue(String cmtValue) {
		this.cmtValue = cmtValue;
	}

	public String getCommentField() {
		return commentField;
	}

	public void setCommentField(String commentField) {
		this.commentField = commentField;
	}

	public String getFlagForCmt() {
		return flagForCmt;
	}

	public void setFlagForCmt(String flagForCmt) {
		this.flagForCmt = flagForCmt;
	}

	public List getCrewList() {
		return crewList;
	}

	public void setCrewList(List crewList) {
		this.crewList = crewList;
	}

	public List getCopyEstimateList() {
		return copyEstimateList;
	}

	public void setCopyEstimateList(List copyEstimateList) {
		this.copyEstimateList = copyEstimateList;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public List getSelectiveInvoiceList() {
		return selectiveInvoiceList;
	}

	public void setSelectiveInvoiceList(List selectiveInvoiceList) {
		this.selectiveInvoiceList = selectiveInvoiceList;
	}

	public boolean isOIStatus() {
		return OIStatus;
	}

	public void setOIStatus(boolean oIStatus) {
		OIStatus = oIStatus;
	}

	public String getRevisionScopeValue() {
		return revisionScopeValue;
	}

	public void setRevisionScopeValue(String revisionScopeValue) {
		this.revisionScopeValue = revisionScopeValue;
	}

	public String getResourceListForOIStatus() {
		return resourceListForOIStatus;
	}

	public void setResourceListForOIStatus(String resourceListForOIStatus) {
		this.resourceListForOIStatus = resourceListForOIStatus;
	}

	public String getTargetActualValue() {
		return targetActualValue;
	}

	public void setTargetActualValue(String targetActualValue) {
		this.targetActualValue = targetActualValue;
	}

	public Map<String, String> getHouse_isactive() {
		return house_isactive;
	}

	public void setHouse_isactive(Map<String, String> house_isactive) {
		this.house_isactive = house_isactive;
	}

	public Map<String, String> getTcktservc_isactive() {
		return tcktservc_isactive;
	}

	public void setTcktservc_isactive(Map<String, String> tcktservc_isactive) {
		this.tcktservc_isactive = tcktservc_isactive;
	}

	public BigDecimal getEstimatedExpenseSum() {
		return estimatedExpenseSum;
	}

	public void setEstimatedExpenseSum(BigDecimal estimatedExpenseSum) {
		this.estimatedExpenseSum = estimatedExpenseSum;
	}

	public BigDecimal getEstimatedRevenueSum() {
		return estimatedRevenueSum;
	}

	public void setEstimatedRevenueSum(BigDecimal estimatedRevenueSum) {
		this.estimatedRevenueSum = estimatedRevenueSum;
	}

	public BigDecimal getRevisionExpenseSum() {
		return revisionExpenseSum;
	}

	public void setRevisionExpenseSum(BigDecimal revisionExpenseSum) {
		this.revisionExpenseSum = revisionExpenseSum;
	}

	public BigDecimal getRevisionRevenueSum() {
		return revisionRevenueSum;
	}

	public void setRevisionRevenueSum(BigDecimal revisionRevenueSum) {
		this.revisionRevenueSum = revisionRevenueSum;
	}

	public BigDecimal getEstGrossMarginForRevenueVal() {
		return estGrossMarginForRevenueVal;
	}

	public void setEstGrossMarginForRevenueVal(
			BigDecimal estGrossMarginForRevenueVal) {
		this.estGrossMarginForRevenueVal = estGrossMarginForRevenueVal;
	}

	public BigDecimal getRevGrossMarginForRevenueVal() {
		return revGrossMarginForRevenueVal;
	}

	public void setRevGrossMarginForRevenueVal(
			BigDecimal revGrossMarginForRevenueVal) {
		this.revGrossMarginForRevenueVal = revGrossMarginForRevenueVal;
	}

	public BigDecimal getEstGrossMarginForRevenuePercentageVal() {
		return estGrossMarginForRevenuePercentageVal;
	}

	public void setEstGrossMarginForRevenuePercentageVal(
			BigDecimal estGrossMarginForRevenuePercentageVal) {
		this.estGrossMarginForRevenuePercentageVal = estGrossMarginForRevenuePercentageVal;
	}

	public BigDecimal getRevGrossMarginForRevenuePercentageVal() {
		return revGrossMarginForRevenuePercentageVal;
	}

	public void setRevGrossMarginForRevenuePercentageVal(
			BigDecimal revGrossMarginForRevenuePercentageVal) {
		this.revGrossMarginForRevenuePercentageVal = revGrossMarginForRevenuePercentageVal;
	}

	public BigDecimal getEstimatedConsumablesValue() {
		return estimatedConsumablesValue;
	}

	public void setEstimatedConsumablesValue(BigDecimal estimatedConsumablesValue) {
		this.estimatedConsumablesValue = estimatedConsumablesValue;
	}

	public BigDecimal getRevisionConsumablesValue() {
		return revisionConsumablesValue;
	}

	public void setRevisionConsumablesValue(BigDecimal revisionConsumablesValue) {
		this.revisionConsumablesValue = revisionConsumablesValue;
	}

	public String getDistinctWorkOrderList() {
		return distinctWorkOrderList;
	}

	public void setDistinctWorkOrderList(String distinctWorkOrderList) {
		this.distinctWorkOrderList = distinctWorkOrderList;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public boolean isConsumablePercentageCheck() {
		return consumablePercentageCheck;
	}

	public void setConsumablePercentageCheck(boolean consumablePercentageCheck) {
		this.consumablePercentageCheck = consumablePercentageCheck;
	}

	public String getOiActiveId() {
		return oiActiveId;
	}

	public void setOiActiveId(String oiActiveId) {
		this.oiActiveId = oiActiveId;
	}

	public String getOiInactiveId() {
		return oiInactiveId;
	}

	public void setOiInactiveId(String oiInactiveId) {
		this.oiInactiveId = oiInactiveId;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}
	
	public boolean isEditAB5Surcharge() {
		return editAB5Surcharge;
	}


	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}


	public void setEditAB5Surcharge(boolean editAB5Surcharge) {
		this.editAB5Surcharge = editAB5Surcharge;
	}
	
	public String getaB5Surcharge() {
		return aB5Surcharge;
	}

	public void setaB5Surcharge(String aB5Surcharge) {
		this.aB5Surcharge = aB5Surcharge;
	}

}
