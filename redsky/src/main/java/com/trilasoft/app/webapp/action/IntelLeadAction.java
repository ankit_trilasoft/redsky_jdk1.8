package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.log4j.Logger;
import org.appfuse.model.User;

import com.opensymphony.xwork2.ActionSupport;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.IntelLead;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.IntelLeadManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class IntelLeadAction extends ActionSupport{
	
	private String sessionCorpID;
	private String usertype;
	private IntelLeadManager intelLeadManager;
	private List intelLeadList;
	private Long id;
	private IntelLead intelLead;
	private CustomerFileManager customerFileManager;
	private ServiceOrderManager serviceOrderManager;
	private BillingManager billingManager;
	private CustomerFile customerFile;
	private ServiceOrder serviceOrder;
	private Billing billing;
	private List maxSequenceNumber;
	private String seqNum;
	private Long autoSequenceNumber;
	private String userName="";
	Date currentdate = new Date();
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	private List maxShip;
	private String ship;
	private Long autoShip;
	private String shipnumber;
	private RefMasterManager refMasterManager;
	private List countryCode; 
	private List stateCode;
	static final Logger logger = Logger.getLogger(ToDoRuleAction.class);
	
	public void prepare() throws Exception {
	}

	public IntelLeadAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
         usertype = user.getUserType();
         userName=user.getUsername();
	}

	
	public String intelLeadList()
	{
		intelLeadList = intelLeadManager.getIntelLeadLists(sessionCorpID);
		return SUCCESS;
	}
	
	public String orderCreatedFromIntel(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+userName+" Start");
		Long StartTime = System.currentTimeMillis();
		intelLead = intelLeadManager.get(id);
		System.out.println(intelLead.getAuthType());
		CustomerFile customerFileNew= new CustomerFile();
		customerFileNew.setMoveType("BookedMove");
		customerFileNew.setCorpID(sessionCorpID);
		maxSequenceNumber = customerFileManager.findMaximumSequenceNumber(sessionCorpID);
		if (maxSequenceNumber.get(0) == null) {
			customerFileNew.setSequenceNumber(sessionCorpID + "1000001");
		} else {
			autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
			seqNum = sessionCorpID + autoSequenceNumber.toString();
			customerFileNew.setSequenceNumber(seqNum);
		}
		//customerFileNew.setBookingAgentSequenceNumber(customerFileUGWW.getSequenceNumber());
		//customerFileNew.setIsNetworkRecord(true);
		customerFileNew.setCreatedBy("IntelLead: "+userName);
		customerFileNew.setUpdatedBy("IntelLead: "+userName);
		customerFileNew.setCreatedOn(new Date());
		customerFileNew.setUpdatedOn(new Date());
		customerFileNew.setLastName(intelLead.getEmployeeName());
		//customerFileNew.setIsNetworkGroup(false);
		customerFileNew.setContractType("");
		customerFileNew.setBookingAgentCode("T131500");
		customerFileNew.setBookingAgentName("Suddath Relocation Systems of New York, Inc.");
		customerFileNew.setSource("CO");
		customerFileNew.setCompanyDivision("SUDD");
		customerFileNew.setMoveDate(intelLead.getServiceStartDate());
		customerFileNew.setStatusNumber(1);
		customerFileNew.setControlFlag("C");
		customerFileNew.setBillToCode("T143465");
		customerFileNew.setBillToName("INTEL CORPORATION");
		customerFileNew.setAccountCode("");
		customerFileNew.setAccountName("");
		//customerFileNew.setCoordinator("coridator");
		customerFileNew.setBillPayMethod("");
		customerFileNew.setSystemDate(new Date());
		customerFileNew.setStatus("NEW");
		customerFileNew.setStatusDate(new Date());
		customerFileNew.setContract("");  
		customerFileNew.setJob("INT");
		customerFileNew.setCoordinator("ADMINSUDD");
		customerFileNew.setSurveyTime("00:00");
		customerFileNew.setSurveyTime2("00:00");
		
		
		countryCode = refMasterManager.findCountryCode(intelLead.getFromCountry().toString(), sessionCorpID);
		if(countryCode!=null && !(countryCode.isEmpty()) && countryCode.get(0)!=null){
		customerFileNew.setOriginCountryCode(countryCode.get(0).toString());
		}
		//customerFileNew.setOriginCountry(intelLead.getFromCountry());
		customerFileNew.setOriginCity(intelLead.getFromCity());
		customerFileNew.setOriginZip(intelLead.getFromPostalCode());
		
		stateCode = refMasterManager.findStateCode(intelLead.getFromState(), sessionCorpID);
		if(stateCode!=null && !(stateCode.isEmpty()) && stateCode.get(0)!=null){
		customerFileNew.setOriginState(stateCode.get(0).toString());
		}
		
		//customerFileNew.setOriginState(intelLead.getFromState());
		customerFileNew.setDestinationCity(intelLead.getToCityToState());
		
		
		countryCode = refMasterManager.findCountryCode(intelLead.getToCountry().toString(), sessionCorpID);
		if(countryCode!=null && !(countryCode.isEmpty()) && countryCode.get(0)!=null){
		System.out.println(countryCode.get(0).toString());
			customerFileNew.setDestinationCountryCode(countryCode.get(0).toString());
		}
		
		//customerFileNew.setDestinationCountry(intelLead.getToCountry());
		customerFileNew.setDestinationZip("");
		customerFileNew=customerFileManager.save(customerFileNew);
		System.out.println("CustomerFile Created "+ customerFileNew);
		
		//intelLeadManager.updateActionValue(id,sessionCorpID);
		
		
		ServiceOrder serviceOrderNew= new ServiceOrder();
		Miscellaneous miscellaneousNew = new Miscellaneous();
		Billing billingNew = new Billing();
		TrackingStatus trackingStatusNew=new TrackingStatus();
		
		
		serviceOrderNew.setId(null);
				serviceOrderNew.setMoveType("BookedMove");
		serviceOrderNew.setCorpID(customerFileNew.getCorpID());
		maxShip = customerFileManager.findMaximumShipExternal(customerFileNew.getSequenceNumber(),customerFileNew.getCorpID());
		if (maxShip.get(0) == null) {
			ship = "01";

		} else {
			autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
			//System.out.println(autoShip);
			if ((autoShip.toString()).length() == 1) {
				ship = "0" + (autoShip.toString());
			} else {
				ship = autoShip.toString();
			}
	}
		//serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
		shipnumber = customerFileNew.getSequenceNumber() + ship;
		serviceOrderNew.setLastName(customerFileNew.getLastName());
		serviceOrderNew.setShip(ship);
		serviceOrderNew.setShipNumber(shipnumber);
		serviceOrderNew.setControlFlag("C");
		serviceOrderNew.setSequenceNumber(customerFileNew.getSequenceNumber());
		serviceOrderNew.setCustomerFile(customerFileNew);
		/*serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
		serviceOrderNew.setBillToName(customerFileNew.getBillToName());*/
		serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
		serviceOrderNew.setBillToName(customerFileNew.getBillToName());
		serviceOrderNew.setCreatedBy("IntelLead: "+userName);
		serviceOrderNew.setUpdatedBy("IntelLead: "+userName);
		serviceOrderNew.setCreatedOn(new Date());
		serviceOrderNew.setUpdatedOn(new Date());
		//serviceOrderNew.setNetworkSO(true);
		//serviceOrderNew.setStatus("NEW");
		serviceOrderNew.setCustomerFileId(customerFileNew.getId());
		serviceOrderNew.setBookingAgentCode(customerFileNew.getBookingAgentCode());
		serviceOrderNew.setBookingAgentName(customerFileNew.getBookingAgentName());
		//serviceOrderNew.setIsNetworkRecord(true);
		serviceOrderNew.setCommodity("HHG");
		serviceOrderNew.setCompanyDivision(customerFileNew.getCompanyDivision());
		//serviceOrderNew.setJob("INT");
		serviceOrderNew.setRouting("");
		//serviceOrderNew.setStatus("NEW");
		serviceOrderNew.setStatusNumber(1);
		serviceOrderNew.setStatusDate(new Date());
		serviceOrderNew.setMode("");
		serviceOrderNew.setJob(customerFileNew.getJob());
		serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
		serviceOrderNew.setEstimator(customerFileNew.getEstimator());
		serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
		serviceOrderNew.setDestinationCountry(customerFileNew.getDestinationCountry());
		serviceOrderNew.setDestinationCity(customerFileNew.getDestinationCity());
		serviceOrderNew.setDestinationZip(customerFileNew.getDestinationZip());
		serviceOrderNew.setOriginCountry(customerFileNew.getOriginCountry());
		serviceOrderNew.setOriginState(customerFileNew.getOriginState());
		serviceOrderNew.setOriginZip(customerFileNew.getOriginZip());
		serviceOrderNew.setOriginState(customerFileNew.getOriginState());
		serviceOrderNew=serviceOrderManager.save(serviceOrderNew);
		
		intelLeadManager.updateActionValue(id,sessionCorpID,shipnumber,seqNum);
		
		
	/*	 List miscellaneousFieldToSync=customerFileManager.findFieldToSyncCreate("Miscellaneous","","");
			Iterator miscellaneousFields=miscellaneousFieldToSync.iterator();
			while(miscellaneousFields.hasNext()){
				String field=miscellaneousFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(miscellaneousNew,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
//				beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}*/
		 miscellaneousNew.setId(serviceOrderNew.getId());
		 miscellaneousNew.setCorpID(serviceOrderNew.getCorpID());
		 miscellaneousNew.setShipNumber(serviceOrderNew.getShipNumber());
		 miscellaneousNew.setShip(serviceOrderNew.getShip());
		 miscellaneousNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		 miscellaneousNew.setCreatedBy("IntelLead:"+userName);
		 miscellaneousNew.setUpdatedBy("IntelLead:"+userName);
		 miscellaneousNew.setCreatedOn(new Date());
		 miscellaneousNew.setUpdatedOn(new Date());
		 miscellaneousNew.setUnit1("")	; 
		 miscellaneousNew.setUnit2("")	; 
		 miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
		
		 
		 //TrackingStatus trackingStatusNew=new TrackingStatus();
		
		/*List trackingStatusFieldToSync=customerFileManager.findFieldToSyncCreate("TrackingStatus","","");
		Iterator trackingStatusFields=trackingStatusFieldToSync.iterator();
		while(trackingStatusFields.hasNext()){
			String field=trackingStatusFields.next().toString();
			String[] splitField=field.split("~");
			String fieldFrom=splitField[0].trim();
			String fieldTo=splitField[1].trim();
			PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
			try{
          		beanUtilsBean.setProperty(trackingStatusNew,fieldTo,beanUtilsBean.getProperty(trackingStatus, fieldFrom));
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}*/
		
		//TrackingStatus trackingStatusNew = new TrackingStatus();
		trackingStatusNew.setId(serviceOrderNew.getId());
		trackingStatusNew.setContractType("");
		trackingStatusNew.setCorpID(serviceOrderNew.getCorpID());
		trackingStatusNew.setServiceOrder(serviceOrderNew);
		trackingStatusNew.setMiscellaneous(miscellaneousNew);
		trackingStatusNew.setShipNumber(serviceOrderNew.getShipNumber());
		trackingStatusNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		trackingStatusNew.setSurveyTimeFrom(customerFileNew.getSurveyTime());
		trackingStatusNew.setSurveyTimeTo(customerFileNew.getSurveyTime2());
		trackingStatusNew.setSurveyDate(customerFileNew.getSurvey());
		//trackingStatusNew.setBookingAgentContact(serviceOrderUGWW.getCoordinator());		
		
		/*if(forSection.equalsIgnoreCase("destination")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("destinationSub")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("origin")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("originSub")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("bookingAgentCode")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setBookingAgentExSO(serviceOrderUGWW.getShipNumber()); 
			trackingStatusUGWW.setBookingAgentExSO(serviceOrderUGWW.getShipNumber());
			//trackingStatusNew.setBookingAgentContact(trackingStatusUGWW.getBookingAgentContact());
			try{
				trackingStatusNew.setBookingAgentEmail(customerFileManager.findCoordEmailAddress(trackingStatusUGWW.getBookingAgentContact()).get(0).toString());
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				} 
		}*/
		trackingStatusNew.setCreatedBy("IntelLead:"+userName);
		trackingStatusNew.setUpdatedBy("IntelLead:"+userName);
		trackingStatusNew.setCreatedOn(new Date());
		trackingStatusNew.setUpdatedOn(new Date());
	/*	try{
			 if(trackingStatusNew.getForwarderCode()!=null && (!(trackingStatusNew.getForwarderCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getForwarderCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setForwarderCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 try{
			 if(trackingStatusNew.getBrokerCode()!=null && (!(trackingStatusNew.getBrokerCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getBrokerCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setBrokerCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		*/
		trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
		
	        try {
				billingNew.setBillToCode(customerFileNew.getBillToCode());
				billingNew.setBillToName(customerFileNew.getBillToName());
				billingNew.setContract(customerFileNew.getContract());
				billingNew.setBillTo1Point(customerFileNew.getBillPayMethod());
				billingNew.setBillingInstructionCodeWithDesc("CON : As per Contract");
				billingNew.setBillingInstruction("As per Contract");
				billingNew.setSpecialInstruction("As per Quote");
				// }
				billingNew.setId(serviceOrderNew.getId());
				billingNew.setCorpID(serviceOrderNew.getCorpID());
				billingNew.setShipNumber(serviceOrderNew.getShipNumber());
				billingNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				billingNew.setServiceOrder(serviceOrderNew);
				billingNew.setCreatedBy("IntelLead:" + userName);
				billingNew.setUpdatedBy("IntelLead:" + userName);
				billingNew.setSystemDate(new Date());
				billingNew.setCreatedOn(new Date());
				billingNew.setUpdatedOn(new Date());
				billingNew.setBillToAuthority(intelLead.getAuthID());
				billingNew.setAuthUpdated(intelLead.getAuthDate());
				/*billingNew.setAuditor(customerFileNew.getAuditor());
				billingNew.setPersonBilling(customerFileNew.getPersonBilling());
				billingNew.setPersonPayable(customerFileNew.getPersonPayable());
				billingNew.setPersonPricing(customerFileNew.getPersonPricing());*/
				
				try {
					if (billingNew.getVendorCode() != null
							&& (!(billingNew.getVendorCode()
									.equalsIgnoreCase("")))) {
						boolean checkVendorCode = billingManager
								.findVendorCode(billingNew.getCorpID(),
										billingNew.getVendorCode());
						if (checkVendorCode) {

						} else {
							billingNew.setVendorCode("");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				billingNew = billingManager.save(billingNew);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to Accept the service order OF ugww : "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+userName+" End");	
		return SUCCESS;
	}
	
	public String intelLeadForm(){
		intelLead = intelLeadManager.get(id);
		return SUCCESS;
	}
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public void setIntelLeadManager(IntelLeadManager intelLeadManager) {
		this.intelLeadManager = intelLeadManager;
	}

	public List getIntelLeadList() {
		return intelLeadList;
	}

	public void setIntelLeadList(List intelLeadList) {
		this.intelLeadList = intelLeadList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IntelLead getIntelLead() {
		return intelLead;
	}

	public void setIntelLead(IntelLead intelLead) {
		this.intelLead = intelLead;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public List getMaxSequenceNumber() {
		return maxSequenceNumber;
	}

	public void setMaxSequenceNumber(List maxSequenceNumber) {
		this.maxSequenceNumber = maxSequenceNumber;
	}

	public String getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	public Long getAutoSequenceNumber() {
		return autoSequenceNumber;
	}

	public void setAutoSequenceNumber(Long autoSequenceNumber) {
		this.autoSequenceNumber = autoSequenceNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public List getMaxShip() {
		return maxShip;
	}

	public void setMaxShip(List maxShip) {
		this.maxShip = maxShip;
	}

	public String getShip() {
		return ship;
	}

	public void setShip(String ship) {
		this.ship = ship;
	}

	public Long getAutoShip() {
		return autoShip;
	}

	public void setAutoShip(Long autoShip) {
		this.autoShip = autoShip;
	}

	public String getShipnumber() {
		return shipnumber;
	}

	public void setShipnumber(String shipnumber) {
		this.shipnumber = shipnumber;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	

}
