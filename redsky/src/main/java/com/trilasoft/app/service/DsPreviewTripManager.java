package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsPreviewTrip;

public interface DsPreviewTripManager extends GenericManager<DsPreviewTrip, Long>{

	List getListById(Long id);

}
