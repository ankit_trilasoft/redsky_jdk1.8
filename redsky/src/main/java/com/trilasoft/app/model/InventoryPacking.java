package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="inventorypacking")
public class InventoryPacking {
	private String corpID;
	private String quantity;
	private String comment;
	private String mode;
	private String volume;
	private String location;
	private String boxName;
	private Integer width;
    private Integer height;
	private Integer length;
	private String item;
    private Integer weight;
	private String itemType;
	private String itemQuantity;
	private Integer value;
	private String conditon;
	private Boolean dismantling;
	private Boolean assembling;
	private Boolean specialContainer;
	private String make;
	private String model;
	private String year;
	private String serialnumber;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private Long id;
	private Long serviceOrderID;
	private Integer pieceID;
	private Boolean part;
	private Boolean valuable;
	private String title;
	private String materials;
	private String author;
	private String imagesAvailablityFlag;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("quantity", quantity).append("comment", comment)
				.append("mode", mode).append("volume", volume)
				.append("location", location).append("boxName", boxName)
				.append("width", width).append("height", height)
				.append("length", length).append("item", item)
				.append("weight", weight).append("itemType", itemType)
				.append("itemQuantity", itemQuantity).append("value", value)
				.append("conditon", conditon)
				.append("dismantling", dismantling)
				.append("assembling", assembling)
				.append("specialContainer", specialContainer)
				.append("make", make).append("model", model)
				.append("year", year).append("serialnumber", serialnumber)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("id", id).append("serviceOrderID", serviceOrderID)
				.append("pieceID", pieceID).append("part", part)
				.append("valuable", valuable).append("title", title)
				.append("materials", materials).append("author", author)
				.append("imagesAvailablityFlag", imagesAvailablityFlag)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof InventoryPacking))
			return false;
		InventoryPacking castOther = (InventoryPacking) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(quantity, castOther.quantity)
				.append(comment, castOther.comment)
				.append(mode, castOther.mode).append(volume, castOther.volume)
				.append(location, castOther.location)
				.append(boxName, castOther.boxName)
				.append(width, castOther.width)
				.append(height, castOther.height)
				.append(length, castOther.length).append(item, castOther.item)
				.append(weight, castOther.weight)
				.append(itemType, castOther.itemType)
				.append(itemQuantity, castOther.itemQuantity)
				.append(value, castOther.value)
				.append(conditon, castOther.conditon)
				.append(dismantling, castOther.dismantling)
				.append(assembling, castOther.assembling)
				.append(specialContainer, castOther.specialContainer)
				.append(make, castOther.make).append(model, castOther.model)
				.append(year, castOther.year)
				.append(serialnumber, castOther.serialnumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(id, castOther.id)
				.append(serviceOrderID, castOther.serviceOrderID)
				.append(pieceID, castOther.pieceID)
				.append(part, castOther.part)
				.append(valuable, castOther.valuable)
				.append(title, castOther.title)
				.append(materials, castOther.materials)
				.append(author, castOther.author)
				.append(imagesAvailablityFlag, castOther.imagesAvailablityFlag)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(quantity)
				.append(comment).append(mode).append(volume).append(location)
				.append(boxName).append(width).append(height).append(length)
				.append(item).append(weight).append(itemType)
				.append(itemQuantity).append(value).append(conditon)
				.append(dismantling).append(assembling)
				.append(specialContainer).append(make).append(model)
				.append(year).append(serialnumber).append(createdBy)
				.append(updatedBy).append(updatedOn).append(createdOn)
				.append(id).append(serviceOrderID).append(pieceID).append(part)
				.append(valuable).append(title).append(materials)
				.append(author).append(imagesAvailablityFlag).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getServiceOrderID() {
		return serviceOrderID;
	}
	public void setServiceOrderID(Long serviceOrderID) {
		this.serviceOrderID = serviceOrderID;
	}
	@Column
	public Boolean getAssembling() {
		return assembling;
	}
	public void setAssembling(Boolean assembling) {
		this.assembling = assembling;
	}
	@Column
	public String getBoxName() {
		return boxName;
	}
	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}
	@Column
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Column
	public String getConditon() {
		return conditon;
	}
	public void setConditon(String conditon) {
		this.conditon = conditon;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Boolean getDismantling() {
		return dismantling;
	}
	public void setDismantling(Boolean dismantling) {
		this.dismantling = dismantling;
	}
	@Column
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	@Column
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	@Column
	public String getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(String itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	@Column
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	@Column
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	@Column
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@Column
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Column
	public Boolean getSpecialContainer() {
		return specialContainer;
	}
	public void setSpecialContainer(Boolean specialContainer) {
		this.specialContainer = specialContainer;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@Column
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	@Column
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	@Column
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	@Column
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	@Column
	public Integer getPieceID() {
		return pieceID;
	}
	public void setPieceID(Integer pieceID) {
		this.pieceID = pieceID;
	}
	@Column
	public Boolean getPart() {
		return part;
	}
	public void setPart(Boolean part) {
		this.part = part;
	}
	@Column
	public Boolean getValuable() {
		return valuable;
	}
	public void setValuable(Boolean valuable) {
		this.valuable = valuable;
	}
	@Column
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column
	public String getMaterials() {
		return materials;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}
	@Column
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Column
	public String getSerialnumber() {
		return serialnumber;
	}
	public void setSerialnumber(String serialnumber) {
		this.serialnumber = serialnumber;
	}
	@Column
	public String getImagesAvailablityFlag() {
		return imagesAvailablityFlag;
	}
	public void setImagesAvailablityFlag(String imagesAvailablityFlag) {
		this.imagesAvailablityFlag = imagesAvailablityFlag;
	}
}
