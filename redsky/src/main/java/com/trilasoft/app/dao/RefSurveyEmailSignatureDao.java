package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.RefSurveyEmailSignature;
public interface RefSurveyEmailSignatureDao extends GenericDao<RefSurveyEmailSignature, Long>{

}
