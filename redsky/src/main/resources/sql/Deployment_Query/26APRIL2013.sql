// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1;

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`auditSetup` ADD COLUMN `alertValue` VARCHAR(25) NULL  AFTER `alertRole` ;

update auditSetup set alertValue='All Updates' where isAlert is true;

CREATE  TABLE `redsky`.`mss` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `soNumber` VARCHAR(25) NULL ,
  `workTicketNumber` VARCHAR(25) NULL ,
  `workTicketService` VARCHAR(25) NULL ,
  `submitAs` VARCHAR(25) NULL ,
  `billingDivision` VARCHAR(25) NULL ,
  `affiliatedTo` VARCHAR(25) NULL ,
  `urgent` BIT(1) NULL ,  
  `transportType` VARCHAR(15) Null,
  `transportTypeRadio` VARCHAR(15) Null,
  `requestedOriginDate` DATETIME NULL ,
  `requestedDestinationDate` DATETIME NULL ,
  `serviceOverMultipleDays` BIT(1) NULL ,
  `weight`  decimal(19,2) NULL ,
  `loadingStartDate` DATETIME NULL ,
  `loadingEndDate` DATETIME NULL ,
  `packingStartDate` DATETIME NULL ,
  `packingEndDate` DATETIME NULL ,
  `deliveryStartDate` DATETIME NULL ,
  `deliveryEndDate` DATETIME NULL ,
  `mssOrderNumber` VARCHAR(25) NULL ,  
  `vip` BIT(1) NULL ,
  `originServiceCategory` VARCHAR(45) NULL ,
  `originServices` VARCHAR(45) NULL ,
  `purchaseOrderNumber` VARCHAR(20) NULL ,  
  `destinationServiceCtegory` VARCHAR(45) NULL ,
  `destinationServices` VARCHAR(45) NULL ,
`shipperFirstName` VARCHAR(80) NULL,
`shipperLastName` VARCHAR(80) NULL,
`shipperEmailAddress` VARCHAR(65) NULL,
`shipperOriginAddress1` VARCHAR(50) NULL,
`shipperOriginAddress2` VARCHAR(50) NULL,
`shipperOriginCity` VARCHAR(50) NULL,
`shipperOriginState` VARCHAR(50) NULL,
`shipperOriginZip` VARCHAR(15) NULL,
`shipperDestinationAddress1` VARCHAR(50) NULL,
`shipperDestinationAddress2` VARCHAR(50) NULL,
`shipperDestinationCity` VARCHAR(50) NULL,
`shipperDestinationState` VARCHAR(50) NULL,
`shipperDestinationZip` VARCHAR(15) NULL,
`mssOrderOriginPhoneNumbers` VARCHAR(30) NULL,
`mssOrderDestinationPhoneNumbers` VARCHAR(30) NULL,
  `destinationAppr` BIT(1) NULL ,
  `destinationCod` BIT(1) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,  
  PRIMARY KEY (`id`) );

CREATE  TABLE `redsky`.`mssgrid` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `mssId` BIGINT(20) NULL ,
  `corpId` VARCHAR(10) NULL ,
  `transportType` VARCHAR(15) NULL ,
  `description` VARCHAR(100) NULL ,
  `length` VARCHAR(25) NULL ,
  `width` VARCHAR(25) NULL ,
  `height` VARCHAR(25) NULL ,
  `ply` BIT(1) NULL ,
  `intlIspmis` BIT(1) NULL ,
  `appr` BIT(1) NULL ,
  `cod` BIT(1) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  PRIMARY KEY (`id`) );

CREATE  TABLE `redsky`.`mssoriginservice` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `mssId` BIGINT(20) NULL ,
  `corpId` VARCHAR(10) NULL ,
  `oriItems` VARCHAR(45) NULL ,
  `oriAppr` BIT(1) NULL ,
  `oriCod` BIT(1) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  PRIMARY KEY (`id`) );

ALTER TABLE `redsky`.`mss` ADD COLUMN `workTicketId` BIGINT(20) NULL  AFTER `updatedBy`;
 
CREATE  TABLE `redsky`.`mssdestinationservice` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `mssId` BIGINT(20) NULL ,
  `corpId` VARCHAR(10) NULL ,
  `destinationItems` VARCHAR(45) NULL ,
  `destinationAppr` BIT(1) NULL ,
  `destinationCod` BIT(1) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  PRIMARY KEY (`id`) );
  
ALTER TABLE `redsky`.`mss` CHANGE COLUMN `billingDivision` `billingDivision` INT(10) NULL  , CHANGE COLUMN `affiliatedTo` `affiliatedTo` INT(10) NULL  , CHANGE COLUMN `weight` `weight` INT(10) NULL  ;

ALTER TABLE `redsky`.`mss` DROP COLUMN `destinationCod` , DROP COLUMN `destinationAppr` ;

ALTER TABLE `redsky`.`mssoriginservice` MODIFY COLUMN `oriItems` BIGINT(20) DEFAULT NULL,  MODIFY COLUMN `oriAppr` BIT(1);

ALTER TABLE `redsky`.`mssdestinationservice` MODIFY COLUMN `destinationItems` BIGINT(20) DEFAULT NULL,  MODIFY COLUMN `destinationAppr` BIT(1);

ALTER TABLE `redsky`.`mss` ADD COLUMN `location` VARCHAR(245) DEFAULT NULL AFTER `workTicketId`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','MSS_SERVICE_CAT',45,true,now(),'subrat',now(),'subrat',1,1,'');

ALTER TABLE `redsky`.`mssdestinationservice` MODIFY COLUMN `destinationItems` VARCHAR(45) DEFAULT NULL;

ALTER TABLE `redsky`.`mssoriginservice` MODIFY COLUMN `oriItems` VARCHAR(45) BINARY DEFAULT NULL;

ALTER TABLE `redsky`.`todorule` ADD COLUMN `publishRule` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`mss` MODIFY COLUMN `billingDivision` VARCHAR(25) DEFAULT NULL,  MODIFY COLUMN `affiliatedTo` VARCHAR(25) DEFAULT NULL;

ALTER TABLE `redsky`.`loss` ADD COLUMN `selfBlame` VARCHAR(5) NULL  AFTER `chargeBackCurrency` ;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','SUBMITAS',45,true,now(),'subrat',now(),'subrat',1,1,'');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','BILLTODIVISION',45,true,now(),'subrat',now(),'subrat',1,1,'');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','AFFILIATEDAS',45,true,now(),'subrat',now(),'subrat',1,1,'');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','MSS_ORIGIN_CAT',45,true,now(),'subrat',now(),'subrat',1,1,'');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','MSS_Destination_CAT',45,true,now(),'subrat',now(),'subrat',1,1,'');

update company set localeCountry='IN' where corpid='STAR';

Insert into refmaster (corpid, code, description, fieldlength,bucket,parameter) values ('TSFT', 'serviceorder', 'serviceorder', 25,'companyDivision', 'DATA_SECURITY_TABLE');

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isBookingAgent` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isNetworkAgent` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isOriginAgent` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isSubOriginAgent` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isDestAgent` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isSubDestAgent` BIT(1) DEFAULT b'0';
















