package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DataSecurityFilterDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.DataSecurityFilter;

public class DataSecurityFilterDaoHibernate extends GenericDaoHibernate<DataSecurityFilter, Long> implements DataSecurityFilterDao{

	public DataSecurityFilterDaoHibernate() {
		super(DataSecurityFilter.class);
		
	}

	private HibernateUtil hibernateUtil;
	public List findById(Long id) {
		return null;
	}

	public List getFilterData(Long userId, String sessionCorpID) {
		return this.getSession().createSQLQuery("select tableName,filterValues,fieldName, dss.name from userdatasecurity uds , " +
				"datasecuritypermission dsp , datasecurityfilter dsf , datasecurityset dss where uds.userdatasecurity_id = dsp.datasecurityset_id " +
				"and dsp.datasecurityset_id = dss.id and uds.user_id="+userId+" and dsf.corpID='"+sessionCorpID+"' and dsf.id=dsp.datasecurityfilter_id").list();
	}

	public List getUsingId(Long id) {
		return  getHibernateTemplate().find("from DataSecurityFilter where id="+id+"");
	}

	public String getParentCorpId(String sessionCorpID) {
		return this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list().get(0).toString();

	}

	public List PartnerAccessCorpIdList() {
		return this.getSession().createSQLQuery("select corpID from company where networkFlag is true ").list();
	}

	public List getChildAgentCode(String filterValues, String sessionCorpID,String parentCorpID) {
		String query = "select partnercode from partnerpublic where agentparent = '"+filterValues+"' and corpid in ('"+sessionCorpID+"','TSFT','"+parentCorpID+"') and status ='Approved' and partnercode <> '"+filterValues+"' ";
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	}
	
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public Map getChildAgentCodeMap() { 
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,corpid,group_concat(partnercode) from partnerpublic where agentparent != '' and  agentparent is not null  and agentparent !=partnercode and status ='Approved' group by agentparent order by agentparent";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list();
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[2] !=null){
		String partnercode = row[2].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[1].toString().trim()+"~"+row[0].toString().trim(), codeList);
			} 
		return childAgentCodeMap;
	}

}
