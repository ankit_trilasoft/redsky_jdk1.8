<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Agent Directory</title>   
    <meta name="heading" content="Agent Directory"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	document.getElementsByName('customerName')[0].value='';
	document.getElementsByName('customerEmailId')[0].value='';
}


function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}
function quotesRemoval(){
	if(document.getElementsByName('customerName')[0].value.indexOf("'")>-1){
		var val =document.getElementsByName('customerName')[0].value;
		document.getElementsByName('customerName')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
	}
	if(document.getElementsByName('customerEmailId')[0].value.indexOf("'")>-1){
		val =document.getElementsByName('customerEmailId')[0].value;
		document.getElementsByName('customerEmailId')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
	}
  }
</script>

    <script type="text/javascript">
		// window.opener.document.form.field.value
		function getValue(name, mail, phone,type,isEnable){
			//alert("hiiiii");
			
				window.opener.document.forms['customerFileForm'].elements['customerFile.originAgentContact'].value = name;
				window.opener.document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value = mail;
				window.opener.document.forms['customerFileForm'].elements['customerFile.originAgentPhoneNumber'].value = phone;
				
			
			
			window.close();
		}

		
	</script>
<s:form name="searchStatusBooking" id="searchStatusBooking" action="searchStatusBooking" method="post">
<s:hidden name="decorator" value="popup"/>
<s:hidden name="popup" value="true"/>
<s:hidden name="partnerCode" value="${partnerCode}"/>
<s:hidden name="agentType" value="${agentType}"/>
<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Name</th>
		<th>Email ID</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:textfield name="customerName" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="customerEmailId" size="30" required="true" cssClass="input-text" /></td>		
				<td>
	       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search" onclick="quotesRemoval();"/>  
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div> 
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
</head>

</div><div class="spn" style="width:100%">&nbsp;</div><br><br><br>

<s:set name="userCustomerContactList" value="userCustomerContactList" scope="request"/>  
<display:table name="userCustomerContactList" class="table" requestURI="" id="userCustomerContactList" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" >   
	    <display:column sortable="true" title="Full Name" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "getValue('${fn:replace(userCustomerContactList.fullName,"'","\\'")}','${userCustomerContactList.email}','${userCustomerContactList.phoneNumber}','${userCustomerContactList.userType}','${userCustomerContactList.enabled}');">
	    	 <c:out value="${userCustomerContactList.fullName}" /></a>
	    </display:column>
        <display:column property="jobFunction" sortable="true" title="Job Function" style="width:65px"/>  
        <display:column title="RedSky User" sortable="true" style="width:45px">      
 		<c:if test="${userCustomerContactList.enabled==false}">				
			<img src="${pageContext.request.contextPath}/images/cancel001.gif" />				 	
		</c:if>
		<c:if test="${userCustomerContactList.enabled==true}">				
			 <img src="${pageContext.request.contextPath}/images/tick01.gif" />		 	
		</c:if>
		</display:column>	
        <display:column property="userTitle" sortable="true" title="Job Title" style="width:65px"/>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "sendEmail('${userCustomerContactList.email}');">
	    	 <c:out value="${userCustomerContactList.email}" /></a>
	    </display:column>
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userCustomerContactList.phoneNumber}" /></a>
	    </display:column>
</display:table> 
 
</div>
</s:form>

