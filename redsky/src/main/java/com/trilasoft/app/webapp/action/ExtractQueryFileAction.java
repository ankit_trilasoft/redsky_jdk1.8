package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.service.ExtractQueryFileManager;

public class ExtractQueryFileAction extends BaseAction{
	private ExtractQueryFileManager extractManager;
	private ExtractQueryFile  extractQuery =new ExtractQueryFile();

	public ExtractQueryFile getExtractQuery() {
		return extractQuery;
	}

	public void setExtractQuery(ExtractQueryFile extractQuery) {
		this.extractQuery = extractQuery;
	}

	public void setExtractManager(ExtractQueryFileManager extractManager) {
		this.extractManager = extractManager;
	}
	
	
	public String save() throws Exception{
		String query=extractQuery.getQueryName();
		extractManager.save(extractQuery);
		return SUCCESS;
		
	}
	
}
