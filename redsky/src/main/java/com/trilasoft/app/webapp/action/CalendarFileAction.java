package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CalendarFile;
import com.trilasoft.app.service.CalendarFileManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;

public class CalendarFileAction extends BaseAction {
	
	private Long id;
	private CalendarFileManager calendarFileManager;
	private CalendarFile calendarFile;
	private List calendarFiles;
	private String surveyCorpID;
	private String sessionCorpID;
	private String survDay;
	private List survDays;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private  Map<String, String> calendarcategory = new LinkedHashMap<String, String>();;
	private  Map<String, String> job;
	private  Map<String, String> event_type;
	private  Map<String, String> saleConsultant = new LinkedHashMap<String, String>();
	
	private RefMasterManager refMasterManager;
	private List refMasters;
	private  String userRole =new String();
	private String popup;
	private String popupval;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CalendarFileAction.class);

	
	public CalendarFileAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
      	Set<Role> roles  = user.getRoles();
        Role role=new Role();
        String roleListTemp="";
	    Iterator it = roles.iterator();
         while(it.hasNext()) {
        	role=(Role)it.next();
        		roleListTemp += role.getName();
         }
         if(roleListTemp.contains("ROLE_COORD")){
        	 userRole="ROLE_COORD";
         }else if(roleListTemp.contains("ROLE_ADMIN")){
        	 userRole="ROLE_ADMIN";
         }else if(roleListTemp.contains("ROLE_SALE")){
        	 userRole="ROLE_SALE";
         }else if(roleListTemp.contains("ROLE_CONSULTANT")){
        	 userRole="ROLE_CONSULTANT";
         }else {
        	 userRole="";
         }    	
    }

	private String gotoPageString;

	   public String getGotoPageString() {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return gotoPageString;

	   }

	   public void setGotoPageString(String gotoPageString) {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   this.gotoPageString = gotoPageString;

	   }

	   public String saveOnTabChange() throws Exception {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 String s = save(); 
	          ////String s1 = saveCompanyEvent();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	           return gotoPageString;
	   }

	
	
	
	
	public CalendarFile getCalendarFile() {
		return calendarFile;
	}
	public void setCalendarFile(CalendarFile calendarFile) {
		this.calendarFile = calendarFile;
	}
	public void setCalendarFileManager(CalendarFileManager calendarFileManager) {
		this.calendarFileManager = calendarFileManager;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		calendarFiles = calendarFileManager.findByUserName(sessionCorpID,getRequest().getRemoteUser());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String edit(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(id != null){
			calendarFile = calendarFileManager.get(id);
			/*user = userManager.getUserByUsername(getRequest().getRemoteUser().toUpperCase());
			roles = user.getRoles();
			if ((roles.toString().indexOf("ROLE_COORD")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_SALE")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_ADMIN")) > -1) {
				calendarFile.setUserName(sessionCorpID);
			}*/
		}else{
			calendarFile = new CalendarFile();
			calendarFile.setCorpID(sessionCorpID);
			calendarFile.setFromTime("00:00");
			calendarFile.setToTime("00:00");
			user = userManager.getUserByUsername(getRequest().getRemoteUser().toUpperCase());
			roles = user.getRoles();
			if ((roles.toString().indexOf("ROLE_SALE")) > -1 || (roles.toString().indexOf("ROLE_CONSULTANT")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_COORD")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_ADMIN")) > -1) {
				calendarFile.setUserName(sessionCorpID);
			}
			calendarFile.setCreatedOn(new Date());
			calendarFile.setUpdatedOn(new Date());
		}
		surveyCorpID = sessionCorpID;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private List driverListData;
	@SkipValidation
	public String openDriverAgentPopup(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		driverListData=calendarFileManager.getDriverListDataSummary(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String editDriverEvent(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(id != null){
			calendarFile = calendarFileManager.get(id);
			}else{
			calendarFile = new CalendarFile();
			calendarFile.setCorpID(sessionCorpID);
			calendarFile.setUpdatedBy(getRequest().getRemoteUser().toUpperCase());
			calendarFile.setUpdatedOn(new Date());
			calendarFile.setCreatedOn(new Date());
			calendarFile.setUpdatedOn(new Date());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public synchronized String saveDriverEvent() throws Exception {
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		hitFlag="1";
		boolean isNew = (calendarFile.getId() == null);  
		if(isNew){			
			calendarFile.setCreatedOn(new Date());
		}		
		calendarFile.setUpdatedOn(new Date());
		calendarFile.setUpdatedBy(getRequest().getRemoteUser());
		calendarFile.setCorpID(sessionCorpID);
		int days = calendarFile.getSurveyDays();
		
		if(isNew){
			for (int x = -1; x < days; x++) {
			calendarFile.setCategory("Driver");
			calendarFileManager.save(calendarFile);  
		if(!(calendarFile.getSurveyDate() == null)){
			Date Survey = calendarFile.getSurveyDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(Survey);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			Date surys = cal.getTime();
			calendarFile.setSurveyDate(surys);			
			}
		}
	    }else{
	    	calendarFile.setCategory("Driver");	
	    calendarFileManager.save(calendarFile);
	    	}
	         if(gotoPageString==null || gotoPageString.equalsIgnoreCase("")){
	          	String key = (isNew) ? "Event added successfully" : "Event updated successfully";
	          	saveMessage(getText(key));
	          }
	       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String editNewEvent(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(id != null){
			calendarFile = calendarFileManager.get(id);
			/*user = userManager.getUserByUsername(getRequest().getRemoteUser().toUpperCase());
			roles = user.getRoles();
			if ((roles.toString().indexOf("ROLE_COORD")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_SALE")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_ADMIN")) > -1) {
				calendarFile.setUserName(sessionCorpID);
			}*/
		}else{
			calendarFile = new CalendarFile();
			calendarFile.setCorpID(sessionCorpID);
			calendarFile.setFromTime("00:00");
			calendarFile.setToTime("00:00");
			user = userManager.getUserByUsername(getRequest().getRemoteUser().toUpperCase());
			roles = user.getRoles();
			if ((roles.toString().indexOf("ROLE_SALE")) > -1 || (roles.toString().indexOf("ROLE_CONSULTANT")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_COORD")) > -1) {
				calendarFile.setUserName(getRequest().getRemoteUser().toUpperCase());
			}
			else if ((roles.toString().indexOf("ROLE_ADMIN")) > -1) {
				calendarFile.setUserName(sessionCorpID);
			}
			calendarFile.setCreatedOn(new Date());
			calendarFile.setUpdatedOn(new Date());
		}
		surveyCorpID = sessionCorpID;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public synchronized String save() throws Exception {
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		hitFlag="";
		boolean isNew = (calendarFile.getId() == null);  
		if(isNew){
			calendarFile.setCreatedOn(new Date());
		}
		calendarFile.setUpdatedOn(new Date());
		calendarFile.setUpdatedBy(getRequest().getRemoteUser());
		calendarFile.setCorpID(sessionCorpID);
		int days = calendarFile.getSurveyDays();
		if(isNew){
			hitFlag="1";
			for (int x = -1; x < days; x++) {
		calendarFileManager.save(calendarFile);
		if(!(calendarFile.getSurveyDate() == null)){
			Date Survey = calendarFile.getSurveyDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(Survey);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			Date surys = cal.getTime();
			calendarFile.setSurveyDate(surys);
			}
		}
	    }else{
	    	hitFlag="1";
	    	calendarFileManager.save(calendarFile);
		} 
		String key = (isNew) ? "Event added successfully" : "Event updated successfully";   
        ////saveMessage(getText(key));  
        searchScheduledSurveyList();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public synchronized String saveCompanyEvent() throws Exception {
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (calendarFile.getId() == null);  
		if(isNew){
			calendarFile.setCreatedOn(new Date());
		}
		calendarFile.setUpdatedOn(new Date());
		calendarFile.setUpdatedBy(getRequest().getRemoteUser());
		calendarFile.setCorpID(sessionCorpID);
		int days = calendarFile.getSurveyDays();
		if(isNew){
			for (int x = -1; x < days; x++) {
			calendarFileManager.save(calendarFile);  
		if(!(calendarFile.getSurveyDate() == null)){
			Date Survey = calendarFile.getSurveyDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(Survey);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			Date surys = cal.getTime();
			calendarFile.setSurveyDate(surys);
			}
		}
	    }else{
	    calendarFileManager.save(calendarFile);
	    	}
		//String key = (isNew) ? "Event added successfully" : "Event updated successfully";   
        ////saveMessage(getText(key));  
		search();
		
	    // if(popup.equalsIgnoreCase("true")){
	         if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
	          	String key = (isNew) ? "Event added successfully" : "Event updated successfully";
	          	saveMessage(getText(key));
	          }
	       	///	}
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/*public String saveCompanyEvent() throws Exception{
		boolean isNew = (calendarFile.getId() == null); 
		if(isNew){
			calendarFile.setCreatedOn(new Date());
		}
		calendarFile.setUpdatedOn(new Date());
		calendarFile.setUpdatedBy(getRequest().getRemoteUser());
		calendarFile.setCorpID(sessionCorpID);
		int days = calendarFile.getSurveyDays();
		if(isNew){
		for (int x = -1; x < days; x++) {
		calendarFileManager.save(calendarFile);
		if(!(calendarFile.getSurveyDate() == null)){
			Date Survey = calendarFile.getSurveyDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(Survey);
			cal.add(Calendar.DAY_OF_YEAR, 1);
			Date surys = cal.getTime();
			calendarFile.setSurveyDate(surys);
			}
		}
	    }else{
		calendarFileManager.save(calendarFile);
	    }
		String key = (isNew) ? "Event added successfully" : "Event updated successfully";   
        saveMessage(getText(key));  
        search();
        getComboList(sessionCorpID);
		return SUCCESS;
	}*/
	private Map<String, String> jobs;
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		event_type =refMasterManager.findByParameter(corpId, "EVENT_TYPE");
		calendarcategory=refMasterManager.findByParameter(corpId, "CALENDARCATEGORY");
		saleConsultant=refMasterManager.findSaleConsultant(corpId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public List getCalendarFiles() {
		return calendarFiles;
	}
	private String id1;
	
	public String delete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		calendarFileManager.remove(Long.parseLong(id1));
		String key ="Event deleted successfully";
      	saveMessage(getText(key));
		if(consult == null ){
			consult = sessionCorpID;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return search();
	}
	
	public String deleteDriver(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		calendarFileManager.remove(id);
			String key ="Driver deleted successfully";
	      	saveMessage(getText(key));
	  	  hitFlag="1";
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	}	
	
	private String hitFlag;
	public String deleteSurvey(){
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			calendarFileManager.remove(id);
			String key ="Survey deleted successfully";
	      	saveMessage(getText(key));
			
		///   saveMessage(getText("survey deleted"));
		/*
		  user = userManager.getUserByUsername(getRequest().getRemoteUser());
			roles = user.getRoles();
			if ((roles.toString().indexOf("ROLE_SALE")) > -1) {
				consult = getRequest().getRemoteUser().toUpperCase();
			} else {
				consult = "";
			}
			surveyFrom = new Date().toString();
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_YEAR, 7);
			Date surveyTo1 = cal.getTime();
			surveyTo = surveyTo1.toString();
			if (surveyCity == null) {
				surveyCity = "";
			}
			if (surveyJob == null) {
				surveyJob = "";
			}
			//scheduledSurveys = customerFileManager.findAllScheduledSurveys(sessionCorpID, consult);
			scheduledSurveys = customerFileManager.scheduledSurveys(sessionCorpID, consult, surveyFrom, surveyTo, surveyCity, surveyJob);
			surveyCorpID = sessionCorpID;*/
		  hitFlag="1";
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	}	
	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}
	private List timeHours;
	private String usrName;
	public String userDefaultData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		timeHours = calendarFileManager.getUserFormSysDefault(usrName);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public List getTimeHours() {
		return timeHours;
	}

	public void setTimeHours(List timeHours) {
		this.timeHours = timeHours;
	}
	private String consult;
	private String surveyFrom;
	private String surveyTo;
	
	public String search(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		/*		if(consult == null ){
			consult = sessionCorpID;
		}
		if(surveyFrom == null ){
			surveyFrom = "";
		}
		if(surveyTo == null ){
			surveyTo = "";
		}*/
		
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		roles = user.getRoles();

		if ((roles.toString().indexOf("ROLE_SALE")) > -1 || (roles.toString().indexOf("ROLE_CONSULTANT")) > -1) {
			consult = getRequest().getRemoteUser();
		} else if(consult == null ){
			consult = sessionCorpID;
		
		}
		
		calendarFiles = calendarFileManager.searchMyEvents(sessionCorpID,consult,surveyFrom,surveyTo);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getConsult() {
		return consult;
	}

	public void setConsult(String consult) {
		this.consult = consult;
	}

	public String getSurveyFrom() {
		return surveyFrom;
	}

	public void setSurveyFrom(String surveyFrom) {
		this.surveyFrom = surveyFrom;
	}

	public String getSurveyTo() {
		return surveyTo;
	}

	public void setSurveyTo(String surveyTo) {
		this.surveyTo = surveyTo;
	}
	
	private String surveyCity;

	private String surveyJob;
	private CustomerFileManager customerFileManager;
	private User user;
	private Set<Role> roles;
	private List scheduledSurveys;

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	 private String eventFrom;
	 private String eventTo;
	 private List driverEventList;
	@SkipValidation
	public String scheduledEventList() throws ParseException {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		eventFrom = new Date().toString();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_YEAR, 30);
		Date surveyTo1 = cal.getTime();
		eventTo = surveyTo1.toString();
		driverEventList = customerFileManager.driverEventList(sessionCorpID, eventFrom, eventTo);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String driverCode;
	private String myDriverCode;
	private String driverFirstName;
	private String driverLastName;
	private String driverCodeName="";
	@SkipValidation
	public String searchEventsList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (sessionCorpID == null) {
			sessionCorpID = "";
		}
		if (driverCode == null) {
			driverCode = "";
		}
		if (driverFirstName == null) {
			driverFirstName = "";
		}
		if (driverLastName == null) {
			driverLastName = "";
		}
		if (eventFrom == null) {
			eventFrom = "";
		}
		if (eventTo == null) {
			eventTo = "";
		}
		driverEventList = customerFileManager.searchAllScheduledEvent(sessionCorpID, eventFrom, eventTo,driverCode,driverFirstName,driverLastName);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String driverDetailCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		driverCodeName=calendarFileManager.driverDetailCode(myDriverCode);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String searchScheduledSurveyList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(sessionCorpID == null){sessionCorpID = "";}
		if(consult == null){consult = "";}
		if(surveyFrom == null){surveyFrom = "";}
		if(surveyTo == null){surveyTo = "";}
		if(surveyCity == null){surveyCity = "";}
		if(surveyJob == null){surveyJob = "";}
		
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		roles = user.getRoles();

		if ((roles.toString().indexOf("ROLE_SALE")) > -1 || (roles.toString().indexOf("ROLE_CONSULTANT")) > -1) {
			consult = getRequest().getRemoteUser();
		} 
		scheduledSurveys = customerFileManager.searchAllScheduledSurveys(sessionCorpID, consult, surveyFrom, surveyTo, surveyCity,surveyJob);
		surveyCorpID = sessionCorpID;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public List getScheduledSurveys() {
		return scheduledSurveys;
	}

	public void setScheduledSurveys(List scheduledSurveys) {
		this.scheduledSurveys = scheduledSurveys;
	}

	public String getSurveyCity() {
		return surveyCity;
	}

	public void setSurveyCity(String surveyCity) {
		this.surveyCity = surveyCity;
	}

	public String getSurveyJob() {
		return surveyJob;
	}

	public void setSurveyJob(String surveyJob) {
		this.surveyJob = surveyJob;
	}

	public String getSurveyCorpID() {
		return surveyCorpID;
	}

	public void setSurveyCorpID(String surveyCorpID) {
		this.surveyCorpID = surveyCorpID;
	}

	public String getSurvDay() {
		return survDay;
	}

	public void setSurvDay(String survDay) {
		this.survDay = survDay;
	}

	public List getSurvDays() {
		return survDays;
	}

	public void setSurvDays(List survDays) {
		this.survDays = survDays;
	}

	public  Map<String, String> getSale() {
		return sale;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public  String getUserRole() {
		return userRole;
	}

	public  void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getPopup() {
		return popup;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}


	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public Map<String, String> getEvent_type() {
		return event_type;
	}


	/**
	 * @return the saleConsultant
	 */
	public Map<String, String> getSaleConsultant() {
		return saleConsultant;
	}

	/**
	 * @return the jobs
	 */
	public Map<String, String> getJobs() {
		return jobs;
	}

	public Map<String, String> getCalendarcategory() {
		return calendarcategory;
	}

	public void setCalendarcategory(Map<String, String> calendarcategory) {
		this.calendarcategory = calendarcategory;
	}

	public String getDriverCode() {
		return driverCode;
	}

	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}

	public List getDriverEventList() {
		return driverEventList;
	}

	public void setDriverEventList(List driverEventList) {
		this.driverEventList = driverEventList;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}

	public String getEventFrom() {
		return eventFrom;
	}

	public void setEventFrom(String eventFrom) {
		this.eventFrom = eventFrom;
	}

	public String getEventTo() {
		return eventTo;
	}

	public void setEventTo(String eventTo) {
		this.eventTo = eventTo;
	}

	public List getDriverListData() {
		return driverListData;
	}

	public void setDriverListData(List driverListData) {
		this.driverListData = driverListData;
	}

	public String getMyDriverCode() {
		return myDriverCode;
	}

	public void setMyDriverCode(String myDriverCode) {
		this.myDriverCode = myDriverCode;
	}

	public String getDriverCodeName() {
		return driverCodeName;
	}

	public void setDriverCodeName(String driverCodeName) {
		this.driverCodeName = driverCodeName;
	}
	

}
