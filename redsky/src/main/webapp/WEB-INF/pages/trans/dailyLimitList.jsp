<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<head>   
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
    <title>Operation Daily Limit</title>   
    <meta name="heading" content="Operation Daily Limit"/>  
    
<style>
.tab{
border:1px solid #74B3DC;
}
table th { white-space: nowrap;} 
span.pagelinks {  
    font-size:0.9em;
}
</style> 
<script language="javascript" type="text/javascript">
function assignResources(){
	var category = document.forms['dailyLimitListForm'].elements['category'].value;
	if(category==''){
		alert('Please select the category first.');
		document.forms['dailyLimitListForm'].elements['resource'].value='';
		return false;
	}
	window.open("assignResources.html?category="+category+"&flagType=list&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=500, height=500,top=0, left=0,menubar=no")
}
function clear_fields(){
	document.forms['dailyLimitListForm'].elements['category'].value='';
	document.forms['dailyLimitListForm'].elements['resource'].value='';
	document.forms['dailyLimitListForm'].elements['workDate'].value='';	
}	

var namesVec = new Array("130.png", "129.png");
var root='images/';

function swapImg(ima){
nr = ima.getAttribute('src').split('/');
nr = nr[nr.length-1]
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function HideShow(id){
	//alert(id);
	animatedcollapse.toggle('resourceLimit'+id);
	animatedcollapse.toggle('resourceLimitRecord'+id);
}

function hideDiv(){
	var loopCount = 0;
	var pageNumber = document.forms['dailyLimitListForm'].elements['pageCount'].value;
	if(pageNumber==''){
		pageNumber=1;
	}
	var list= '${listCount}';
	pageNumber = (pageNumber-1)*10;
	<c:forEach var="accLine" items="${dailyLimits}" varStatus="loopStatus">
	try{
	if(loopCount != pageNumber){
		loopCount++;
	}else{
		var accLineId = '${accLine.id}';
		var divId = 'resourceLimit'+accLineId;
		var imgElement = document.getElementById('resourceLimit'+accLineId);
		if(imgElement!='null'){
			animatedcollapse.addDiv(divId, 'fade=1,persist=0,hide=0');
			animatedcollapse.init();
			imgElement.style.display = 'block';	
		}
		var divId1 = 'resourceLimitRecord'+accLineId;
		var imgElementOne = document.getElementById('resourceLimitRecord'+accLineId);
		if(imgElementOne!='null'){
			animatedcollapse.addDiv(divId1, 'fade=1,persist=0,hide=1');
			animatedcollapse.init()
			imgElementOne.style.display = 'none';	
		}	
	}
	}catch(e){
	}
	 </c:forEach>
}
</script>
</head>
<s:form cssClass="form_magn" id="dailyLimitListForm" action="dailyControlList.html?hub=${hub}" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
<s:hidden name="pageCount" />

<div id="newmnav" class="nav_tabs"><!-- sandeep -->
				  <ul>
				  	<li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
				  	<li><a href="editHubLimitOps.html?hubID=${hub}"><span>Operational&nbsp;/&nbsp;WH Management</span></a></li>
				  	<li><a href="resourceControlList.html?hub=${hub}"><span>Resource Limit List</span></a></li>
				    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Daily Limit List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  </ul>
		</div>
		<div style="width: 700px" >
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom: 3px"></div>
		</div>
		
		<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;"></div>
<div id="content" align="center" style="width: 100%" >
<div id="liquid-round">
   	<div class="top"><span></span></div>
   	<div class="center-content">
		<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  		<tbody>
		  		<tr>
				  	<td align="left" class="listwhitetext">
					  	<table class="detailTabLabel" border="0" cellpadding="2">
							  <tbody>  	
							  	<tr>
							  		<td align="left" width="5px"></td>
							  		<td align="left" colspan="4">Hub&nbsp;/&nbsp;WH Selected : <strong>
							  		<c:forEach var="entry" items="${distinctHubDescription}">
									<c:if test="${hub==entry.key}">
									<c:out value="${entry.value}" />
									</c:if>
									</c:forEach>
							  		</strong></td>
								</tr>
								<tr>
									<td align="right" width="15px">Category</td>
									<td width="85px"><s:select cssClass="list-menu" name="category" list="%{resourceCategory}" cssStyle="width:100px" headerKey="" headerValue="" /></td>
			                        <td align="right" width="15px">Resource</td>
							        <td width="100px" ><s:textfield name="resource"  cssClass="input-text" cssStyle="width:100px;" readonly="true" onkeydown="return onlyDel(event,this);"/></td> 
							    	<td><img align="left" class="openpopup" width="17" height="20" onclick="assignResources();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							    	<td align="right" width="70px">Limit&nbsp;Date</td>							    
									<c:if test="${not empty workDate}">
										<s:text id="dateFormattedValue" name="${FormDateValue}"><s:param name="value" value="workDate" /></s:text>
										<td align="left" style="width:100px; !width:100px;"><s:textfield cssClass="input-text" id="workDate" name="workDate" value="%{dateFormattedValue}" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/><img id="workDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									<c:if test="${empty workDate}">
										<td align="left" style="width:140px; !width:100px;"><s:textfield cssClass="input-text" id="workDate" name="workDate" required="true" size="8" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this);" cssStyle="width:100px;"/><img id="workDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									<td width="150px" align="right"><s:submit cssClass="cssbutton" method="list" cssStyle="width:60px; height:25px;" key="button.search"/>
									<input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/></td> 
								</tr>
							  </tbody>
						</table>
					 </td>
		    	</tr>		  	
		 	</tbody>
		</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
		
<div id="Layer1" style="width:100%;">
<c:if test="${hub!='0'}">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editDailyLimit.html?hub=${hub}"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  
</c:if>

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="dailyLimits" value="dailyLimits" scope="request"/>			  
				<c:set var="rowCount"  value="1" />
				<display:table name="dailyLimits" class="table" requestURI="" id="dailyLimits"  pagesize="10" style="width:100%;" >   
				<display:column style="margin:0px;padding:0px;vertical-align:top;padding-top:5px;width:15px" >
					<c:if test="${dailyLimits.item != ''}" >
					<c:set var="countLine" value="${0}" />
					<c:set var="str1" value="${dailyLimits.item}"/>
		            <c:forEach var="num" items="${fn:split(str1, '^')}">
		            <c:set var="countLine" value="${countLine+1}" />
		            </c:forEach>
		            <c:if test="${countLine == '1'}" >
						<span style="margin : 0px 0px 0px 3px;" ></span>
					</c:if>
					<c:if test="${countLine != '1'}" >
						<span style="margin : 0px 0px 0px 3px;" >
							<a onclick="HideShow(${dailyLimits.id})" ><img src="${pageContext.request.contextPath}/images/arrow-limit.png" width="10" height="10"/></a>
						</span>
					</c:if>
					</c:if>
                    </display:column>
					<display:column property="workDate" style="width:100px;vertical-align:top;" sortable="true" href="editDailyLimit.html?hub=${hub}" paramId="id" paramProperty="id" title="Limit Date" format="{0,date,dd-MMM-yyyy}"></display:column>
					
		       	   <display:column title="<table style='margin:0px;padding:0px;width:100%;'>
		       	   <tr><td style='width:220px;'>Category</td>
		       	   <td style='width:220px;'>Resource</td>
		       	   <td style='width:130px;'>Resource Limit</td></tr></table>" >		       	 	
		            
		           
		           
		           <c:if test="${dailyLimits.item != ''}" >
		            
		           <div id="resourceLimitRecord${dailyLimits.id}" >	   	           
		           <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;margin:0px;padding:0px;margin-top:-5px;width:100%;">	
	           	   <c:set var="rowCount"  value="2" />
	           	   <c:set var="oneLine" value="${0}" />
		           <c:set var="str1" value="${dailyLimits.item}"/>
		           <c:forEach var="num" items="${fn:split(str1, '^')}">
			           <tr>
			           <c:set var="str2" value="${num}"/>
			            <c:set var="colCount" value="1" />
			            <c:set var="idCount" value="1" />
			            <c:set var="catVal" value="" />
			            <c:set var="res" value="" />
			           <c:forEach var="num2" items="${fn:split(str2, '~')}">
			            <c:set var="oneLine" value="${oneLine+1}" />
			            <c:if test="${oneLine <= '3'}" >
			            <c:if test="${idCount == '1' }" >
			           		<c:set var="catVal" value="${num2}" />
			           	</c:if>			           	 
			            <c:if test="${idCount == '2' }" >
			           		<c:set var="res" value="${num2}" />
			           		<c:set var="idCount" value="3" />
			           	</c:if>
			           	<c:if test="${idCount == '1' }" >
			        		<c:set var="idCount" value="2" />
			        	</c:if>	
			        	</c:if>		           
			           </c:forEach>
			           <c:forEach var="num1" items="${fn:split(str2, '~')}" varStatus="rightAlign">
			           <c:if test="${oneLine <= '3'}" >
			           <c:if test="${resource == '' }" >
			           		<c:if test="${catVal==category}">
			        			<td  style="color:green;" width="75" >
			        		</c:if>
			        		<c:if test="${catVal!=category}">
			        			<td width="75" >
			        		</c:if>
			           </c:if>
			           <c:if test="${resource != '' }" >
			          		 <c:if test="${catVal==category && res==resource}">
			        			<td  style="color:green;" width="75" >
			        		</c:if>
			           		<c:if test="${catVal!= category && res!=resource}">
			        			<td  width="75" >
			        		</c:if>
			        		<c:if test="${catVal == category && res!=resource}">
			        			<td  width="75" >
			        		</c:if>			        		
			           </c:if>			               	         
						<c:choose>
							<c:when	test="${num1=='M'}">						 
								<c:out value="Material"/>						
							</c:when>
							<c:when	test="${num1=='C'}">
								<c:out value="Crew"/>
							</c:when>
							<c:when	test="${num1=='V'}">
								<c:out value="Vehicle"/>
							</c:when>
							<c:when	test="${num1=='E'}">
								<c:out value="Equipment"/>
							</c:when>
							<c:otherwise>
							<c:choose> 
							<c:when test="${rightAlign.last }">
							<div align="right" style="margin:0px; text-align:right;width:100%;"><font class="">
							<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${num1}" /></font>
                           </div>
							</c:when>
							<c:otherwise>
								<c:out value="${num1}"/>
							</c:otherwise>
							</c:choose>	
							</c:otherwise>
						</c:choose>									   
			            </td>
			               </c:if>
				       	 </c:forEach>				       	 
				       	 </tr>
			    	</c:forEach></table>			    	
			    	</div>	
		            
		            	           		           
		            <div id="resourceLimit${dailyLimits.id}" >	   	           
		           <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;margin:0px;padding:0px;margin-top:-5px;width:100%;">	
	           	   <c:set var="rowCount"  value="2" />
		           <c:set var="str1" value="${dailyLimits.item}"/>
		           <c:forEach var="num" items="${fn:split(str1, '^')}">
			           <tr>
			           <c:set var="str2" value="${num}"/>
			            <c:set var="colCount" value="1" />
			            <c:set var="idCount" value="1" />
			            <c:set var="catVal" value="" />
			            <c:set var="res" value="" />
			           <c:forEach var="num2" items="${fn:split(str2, '~')}">
			            <c:if test="${idCount == '1' }" >
			           		<c:set var="catVal" value="${num2}" />
			           	</c:if>			           	 
			            <c:if test="${idCount == '2' }" >
			           		<c:set var="res" value="${num2}" />
			           		<c:set var="idCount" value="3" />
			           	</c:if>
			           	<c:if test="${idCount == '1' }" >
			        		<c:set var="idCount" value="2" />
			        	</c:if>			           
			           </c:forEach>
			           <c:forEach var="num1" items="${fn:split(str2, '~')}" varStatus="rightAlign1">
			           <c:if test="${resource == '' }" >
			           		<c:if test="${catVal==category}">
			        			<td  style="color:green;" width="75" >
			        		</c:if>
			        		<c:if test="${catVal!=category}">
			        			<td width="75" >
			        		</c:if>
			           </c:if>
			           <c:if test="${resource != '' }" >
			          		 <c:if test="${catVal==category && res==resource}">
			        			<td  style="color:green;" width="75" >
			        		</c:if>
			           		<c:if test="${catVal!= category && res!=resource}">
			        			<td  width="75" >
			        		</c:if>
			        		<c:if test="${catVal == category && res!=resource}">
			        			<td  width="75" >
			        		</c:if>			        		
			           </c:if>			               	         
						<c:choose>
							<c:when	test="${num1=='M'}">						 
								<c:out value="Material"/>						
							</c:when>
							<c:when	test="${num1=='C'}">
								<c:out value="Crew"/>
							</c:when>
							<c:when	test="${num1=='V'}">
								<c:out value="Vehicle"/>
							</c:when>
							<c:when	test="${num1=='E'}">
								<c:out value="Equipment"/>
							</c:when>
							<c:otherwise>
								<c:choose> 
							<c:when test="${rightAlign1.last }">
							<div align="right" style="margin:0px; text-align:right;width:100%;"><font class="">
							<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${num1}" /></font>
                           </div>
							</c:when>
							<c:otherwise>
								<c:out value="${num1}"/>
							</c:otherwise>
							</c:choose>	
							</c:otherwise>
						</c:choose>									   
			            </td>
				       	 </c:forEach>				       	 
				       	 </tr>
			    	</c:forEach></table>			    	
			    	</div>
			    	
			    	
			    		
			    	</c:if>		    					
					</display:column>
						<display:column property="dailyLimits" sortProperty="dailyLimits" headerClass="containeralign" style="text-align:right;vertical-align:top;" title="Daily&nbsp;Limit"/>			
						<display:column headerClass="containeralign" title="Est&nbsp;Wt" style="width:55px; text-align:right;vertical-align:top;"><div align="right">
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${dailyLimits.dailyMaxEstWt}" /></div>
						</display:column>
						
						<display:column headerClass="containeralign" title="Est&nbsp;Vol" style="width:55px; text-align:right;vertical-align:top;"><div align="right">
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${dailyLimits.dailyMaxEstVol}" /></div>
						</display:column>
					<c:if test="${hubWarehouseLimit=='Crw'}">
						<display:column property="dailyCrewLimit" sortProperty="dailyCrewLimit" headerClass="containeralign" style="text-align:right;vertical-align:top;" title="Crew&nbsp;Limit"/>
					</c:if>
					<display:column property="comment" style="width:100px;vertical-align:top;" maxLength="15" title="Comments"/>
					
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
	<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr></table>
</div>
</s:form>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
try{
	document.forms['dailyLimitListForm'].elements['pageCount'].value=<%= request.getParameter("d-7043515-p") %>;
	hideDiv();
}
catch(e){
}
</script>

