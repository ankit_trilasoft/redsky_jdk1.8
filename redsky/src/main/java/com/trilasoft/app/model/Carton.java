/**
 * Implementation of <strong>ModelSupport</strong> that contains convenience methods.
 * This class represents the basic model on "Carton" object in Redsky that allows for Job management.
 * @Class Name	Carton
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */



package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name = "carton")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Carton extends BaseObject{
	
	private Long id;
	private Long serviceOrderId;
	//new added fields
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	//
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String cartonType;//crateNum
	private String type;
	private BigDecimal grossWeight;
	private BigDecimal netWeight;
	private BigDecimal length;
	private BigDecimal width;
	private BigDecimal volume;
	private BigDecimal height;
	private Integer pieces;
	private BigDecimal emptyContWeight;//tare
	private String idNumber;
	private String totalLine;
	private String container;
	private String numberOfCarton;//numbers
	private BigDecimal crateit;
	private BigDecimal uncrate;
	private String cntnrNumber;
	private ServiceOrder serviceOrder;
	private String unit1;
	private String unit2;
	private String unit3;
	private BigDecimal totalGrossWeight;
	private BigDecimal totalNetWeight;
	private BigDecimal totalVolume;
	private BigDecimal density;
	private Integer totalPieces;
	private BigDecimal totalTareWeight;
	
	private BigDecimal grossWeightKilo= new BigDecimal(0);
	private BigDecimal emptyContWeightKilo= new BigDecimal(0);
	private BigDecimal netWeightKilo;
	private BigDecimal volumeCbm = new BigDecimal(0); 
	private BigDecimal densityMetric;
	private String description;
	private String ugwIntId;
	private boolean status = true;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("cartonType", cartonType).append("type", type)
				.append("grossWeight", grossWeight)
				.append("netWeight", netWeight).append("length", length)
				.append("width", width).append("volume", volume)
				.append("height", height).append("pieces", pieces)
				.append("emptyContWeight", emptyContWeight)
				.append("idNumber", idNumber).append("totalLine", totalLine)
				.append("container", container)
				.append("numberOfCarton", numberOfCarton)
				.append("crateit", crateit).append("uncrate", uncrate)
				.append("cntnrNumber", cntnrNumber)
				.append("unit1", unit1)
				.append("unit2", unit2).append("unit3", unit3)
				.append("totalGrossWeight", totalGrossWeight)
				.append("totalNetWeight", totalNetWeight)
				.append("totalVolume", totalVolume).append("density", density)
				.append("totalPieces", totalPieces)
				.append("totalTareWeight", totalTareWeight)
				.append("grossWeightKilo", grossWeightKilo)
				.append("emptyContWeightKilo", emptyContWeightKilo)
				.append("netWeightKilo", netWeightKilo)
				.append("volumeCbm", volumeCbm)
				.append("densityMetric", densityMetric)
				.append("description", description)
				.append("ugwIntId", ugwIntId)
				.append("status", status).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Carton))
			return false;
		Carton castOther = (Carton) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(cartonType, castOther.cartonType)
				.append(type, castOther.type)
				.append(grossWeight, castOther.grossWeight)
				.append(netWeight, castOther.netWeight)
				.append(length, castOther.length)
				.append(width, castOther.width)
				.append(volume, castOther.volume)
				.append(height, castOther.height)
				.append(pieces, castOther.pieces)
				.append(emptyContWeight, castOther.emptyContWeight)
				.append(idNumber, castOther.idNumber)
				.append(totalLine, castOther.totalLine)
				.append(container, castOther.container)
				.append(numberOfCarton, castOther.numberOfCarton)
				.append(crateit, castOther.crateit)
				.append(uncrate, castOther.uncrate)
				.append(cntnrNumber, castOther.cntnrNumber) 
				.append(unit1, castOther.unit1).append(unit2, castOther.unit2)
				.append(unit3, castOther.unit3)
				.append(totalGrossWeight, castOther.totalGrossWeight)
				.append(totalNetWeight, castOther.totalNetWeight)
				.append(totalVolume, castOther.totalVolume)
				.append(density, castOther.density)
				.append(totalPieces, castOther.totalPieces)
				.append(totalTareWeight, castOther.totalTareWeight)
				.append(grossWeightKilo, castOther.grossWeightKilo)
				.append(emptyContWeightKilo, castOther.emptyContWeightKilo)
				.append(netWeightKilo, castOther.netWeightKilo)
				.append(volumeCbm, castOther.volumeCbm)
				.append(densityMetric, castOther.densityMetric)
				.append(description, castOther.description)
				.append(ugwIntId, castOther.ugwIntId)
				.append(status, castOther.status).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId)
				.append(corpID).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(sequenceNumber)
				.append(ship).append(shipNumber).append(cartonType)
				.append(type).append(grossWeight).append(netWeight)
				.append(length).append(width).append(volume).append(height)
				.append(pieces).append(emptyContWeight).append(idNumber)
				.append(totalLine).append(container).append(numberOfCarton)
				.append(crateit).append(uncrate).append(cntnrNumber)
				.append(unit1).append(unit2).append(unit3)
				.append(totalGrossWeight).append(totalNetWeight)
				.append(totalVolume).append(density).append(totalPieces)
				.append(totalTareWeight).append(grossWeightKilo)
				.append(emptyContWeightKilo).append(netWeightKilo)
				.append(volumeCbm).append(densityMetric).append(description)
				.append(ugwIntId)
				.append(status).toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}  
    
	
	
	@Column(length=50)
	public String getCntnrNumber() {
		return cntnrNumber;
	}
	public void setCntnrNumber(String cntnrNumber) {
		this.cntnrNumber = cntnrNumber;
	}
	@Column(length=20)
	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=15)
	public String getCartonType() {
		return cartonType;
	}
	public void setCartonType(String cartonType) {
		this.cartonType = cartonType;
	}
	@Column(length=20)
	public BigDecimal getCrateit() {
		return crateit;
	}
	public void setCrateit(BigDecimal crateit) {
		this.crateit = crateit;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=20)
	public BigDecimal getEmptyContWeight() {
		return emptyContWeight;
	}
	public void setEmptyContWeight(BigDecimal emptyContWeight) {
		this.emptyContWeight = emptyContWeight;
	}
	@Column(length=20)
	public BigDecimal getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}
	@Column(length=20)
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=10)
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	@Column(length=20)
	public BigDecimal getLength() {
		return length;
	}
	public void setLength(BigDecimal length) {
		this.length = length;
	}
	@Column(length=20)
	public BigDecimal getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}
	@Column(length=10)
	public String getNumberOfCarton() {
		return numberOfCarton;
	}
	public void setNumberOfCarton(String numberOfCarton) {
		this.numberOfCarton = numberOfCarton;
	}
	@Column(length=5)
	public Integer getPieces() {
		return pieces;
	}
	public void setPieces(Integer pieces) {
		this.pieces = pieces;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=1)
	public String getTotalLine() {
		return totalLine;
	}
	public void setTotalLine(String totalLine) {
		this.totalLine = totalLine;
	}
	@Column(length=20)
	public BigDecimal getUncrate() {
		return uncrate;
	}
	public void setUncrate(BigDecimal uncrate) {
		this.uncrate = uncrate;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column(length=20)
	public BigDecimal getWidth() {
		return width;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	@Column(length=10)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=10)
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length=10)
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Column(length=10)
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	@Column
	public BigDecimal getTotalGrossWeight() {
		return totalGrossWeight;
	}
	public void setTotalGrossWeight(BigDecimal totalGrossWeight) {
		this.totalGrossWeight = totalGrossWeight;
	}
	@Column
	public BigDecimal getTotalNetWeight() {
		return totalNetWeight;
	}
	public void setTotalNetWeight(BigDecimal totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}
	@Column
	public BigDecimal getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(BigDecimal totalVolume) {
		this.totalVolume = totalVolume;
	}
	@Column(length=20)
	public BigDecimal getDensity() {
		return density;
	}
	public void setDensity(BigDecimal density) {
		this.density = density;
	}
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	/**
	 * @return the totalPieces
	 */
	@Column
	public Integer getTotalPieces() {
		return totalPieces;
	}
	/**
	 * @param totalPieces the totalPieces to set
	 */
	public void setTotalPieces(Integer totalPieces) {
		this.totalPieces = totalPieces;
	}
	/**
	 * @return the densityMetric
	 */
	@Column(length=20)
	public BigDecimal getDensityMetric() {
		return densityMetric;
	}
	/**
	 * @param densityMetric the densityMetric to set
	 */
	public void setDensityMetric(BigDecimal densityMetric) {
		this.densityMetric = densityMetric;
	}
	/**
	 * @return the emptyContWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEmptyContWeightKilo() {
		return emptyContWeightKilo;
	}
	/**
	 * @param emptyContWeightKilo the emptyContWeightKilo to set
	 */
	public void setEmptyContWeightKilo(BigDecimal emptyContWeightKilo) {
		this.emptyContWeightKilo = emptyContWeightKilo;
	}
	/**
	 * @return the grossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getGrossWeightKilo() {
		return grossWeightKilo;
	}
	/**
	 * @param grossWeightKilo the grossWeightKilo to set
	 */
	public void setGrossWeightKilo(BigDecimal grossWeightKilo) {
		this.grossWeightKilo = grossWeightKilo;
	}
	/**
	 * @return the netWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getNetWeightKilo() {
		return netWeightKilo;
	}
	/**
	 * @param netWeightKilo the netWeightKilo to set
	 */
	public void setNetWeightKilo(BigDecimal netWeightKilo) {
		this.netWeightKilo = netWeightKilo;
	}
	/**
	 * @return the volumeCbm
	 */
	@Column(length=20)
	public BigDecimal getVolumeCbm() {
		return volumeCbm;
	}
	/**
	 * @param volumeCbm the volumeCbm to set
	 */
	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}
	/**
	 * @return the totalTareWeight
	 */
	@Column(length=20)
	public BigDecimal getTotalTareWeight() {
		return totalTareWeight;
	}
	/**
	 * @param totalTareWeight the totalTareWeight to set
	 */
	public void setTotalTareWeight(BigDecimal totalTareWeight) {
		this.totalTareWeight = totalTareWeight;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	
	@Column
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}
