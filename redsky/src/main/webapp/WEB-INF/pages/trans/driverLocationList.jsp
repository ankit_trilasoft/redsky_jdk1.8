<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>   
    <title>Driver Location List</title>   
    <meta name="heading" content="Driver Location List"/>   
    <script language="javascript" type="text/javascript">
</script> 

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style>
<SCRIPT type="text/javascript">
function getDriverInvolvement(partnerCode,driverName,vLocation,driverVanId) {

	   	  		   if(partnerCode!='')
	   	  			{	
	                	window.open("driverInvolvementMap.html?driverId="+partnerCode+"&driverName="+driverName+"&vLocation="+vLocation+"&driverVanId="+driverVanId+"&decorator=popup&popup=true","forms",',type=fullWindow,fullscreen,scrollbars=yes,resizable=yes')					
					}
	  }
</SCRIPT>

</head>
<s:form name="driverLocationList" action="">
<div id="layer1" style="width:100%;">
<div id="newmnav">
	  <ul>
	       <li id="newmnav1" style="background:#FFF "><a class="current"><span>Driver List</span></a></li>	  	
		 <li><a href="driverLocationMap.html" ><span>Driver Map</span></a></li>					  	
		 <li><a href="eventList.html?from=driver" ><span>Events</span></a></li>					  			 
	  </ul>
</div>
<div class="spn">&nbsp;</div>
<table  width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr>
	<td>				
<s:set name="dLocationList" value="dLocationList" scope="request"/>  
<display:table defaultsort="8" defaultorder="descending" name="dLocationList" class="table" id="dLocationList" export="true" requestURI="" pagesize="10" style="width:100%;"> 
<display:column  property="name" title="Name" sortable="true" style="width:160px"/>
<display:column  property="driverAgency" title="Agency" sortable="true" style="width:65px"/>
<display:column   title="Driver Code" sortable="true" style="width:65px">

<a  href="#" onclick="getDriverInvolvement('${dLocationList.partnerCode}','${dLocationList.name}','${dLocationList.vanLastLocation}','${dLocationList.validNationalCode}');" /><c:out value="${dLocationList.validNationalCode}" /></display:column>

<display:column  property="currentVanAgency" title="Van&nbsp;Agency" sortable="true" style="width:65px"/>
<display:column  property="currentVanID" title="Van&nbsp;ID" sortable="true" style="width:65px"/>
<display:column  property="currentTractorAgency" title="Tractor&nbsp;Agency" sortable="true" style="width:65px"/>
<display:column  property="currentTractorID" title="Tractor&nbsp;ID" sortable="true" style="width:65px"/>

<display:column  property="vanLastLocation" title="Last Location" sortable="true" style="width:120px"/>
<display:column  property="reportTime" title="Report On" sortable="true" style="width:130px" format="{0,date,dd-MMM-yyyy}"/>
<display:column  property="vanAvailCube" title="Avail Cube" style="width:65px;text-align: right;" format="{0,number,0000}"/>	
<display:column  property="nextVanLocation" title="Next Van Location" sortable="true" style="width:100px"/>
<display:column  property="nextReportOn" title="Next Report On" style="width:110px" format="{0,date,yyyy-MMM-dd}"/>
<display:setProperty name="paging.banner.item_name" value="Driver Location"/>   
<display:setProperty name="paging.banner.items_name" value="Driver Location"/> 
<display:setProperty name="export.excel.filename" value="Driver Location List.xls"/>   
<display:setProperty name="export.csv.filename" value="Driver Location List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Driver Location List.pdf"/> 
</display:table>
</td>
</tr>
</tbody>
</table>
</div>
</s:form>