<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="customList.title"/></title> 
    <meta name="heading" content="<fmt:message key='customList.heading'/>"/> 
    
 <script language="JavaScript" type="text/javascript" >   
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http1129 = getHTTPObject();
var http11295 = getHTTPObject();
function getTransactionIdList(targetElement){
		   	var url = "getTransactionIdListAjax.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(targetElement);
		 	  http11295.open("GET", url, true); 
		 	  http11295.onreadystatechange = function(){handleHttpResponse11295(targetElement)};
		 	  http11295.send(null);
			}		
 function handleHttpResponse11295(id){
 	if (http11295.readyState == 4){
             var results = http11295.responseText
             results = results.trim();
             if(results=='false' || results==false){
             	deleteCustomDetails1(id);
             }else{
            	 alert('This is already linked, Please choose another');
 			}
 		}
 }
function deleteCustomDetails1(id){
			var agree=confirm("Are you sure you wish to remove this row?");
			if (agree){
				showOrHide(1);
		   	var url = "deleteCustomDetailsAjax.html?ajax=1&id="+encodeURI(id);
		 	  http1129.open("GET", url, true); 
		 	  http1129.onreadystatechange = handleHttpResponse1129;
		 	  http1129.send(null);
			}else{
				return false;
			}
		}
 function handleHttpResponse1129(){
 	if (http1129.readyState == 4){
             var results = http1129.responseText
             getCustomDetails();
             showOrHide(0);
 	}
 }
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
 
<c:set var="buttons">  
        <input type="button" class="cssbutton1" onclick="window.open('editCustomAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();"   
        value="Add" style="width:60px; height:25px;margin-bottom:5px;" />
 </c:set> 
<s:form id="serviceForm1" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="shipNumber" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="sid" />
<div id="Layer3" style="width:100%; !width:100%;">
<div class="spn">&nbsp;</div>
 </div>
<s:set name="customs" value="customs" scope="request"/> 
<display:table name="customs" class="table" requestURI="" id="customList" export="false" style="width:99%; margin-left: 5px;margin:2px 0 10px;"> 
     <sec-auth:authComponent componentId="module.tab.custom.customTab">
     <display:column  title="ID#" paramId="id" paramProperty="id">
     <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.id}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();"  style="cursor:pointer">
     <c:out value="${customList.id}"/></a>
     </display:column>
     </sec-auth:authComponent>
     <display:column property="ticket" sortable="true" title="Ticket#" style="width:50px" />
     <c:if test="${customList.movement=='Out'}">
     <c:if test="${not empty customList.transactionId}"> 
     <display:column sortable="true" titleKey="custom.movement"  style="width:80px">
     <c:out value="${customList.movement}"/>
     <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.transactionId}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes')"> (#<c:out value="${customList.transactionId}"></c:out>)</a>
     </display:column> 
     </c:if>
     <c:if test="${empty customList.transactionId}">
     <display:column sortable="true" titleKey="custom.movement" style="width:80px"><c:out value="${customList.movement}" /></display:column>
     </c:if>
     </c:if>
     <c:if test="${customList.movement =='In'}">
     <display:column sortable="true" titleKey="custom.movement" style="width:50px"><c:out value="${customList.movement}" /></display:column> 
     </c:if>
     <display:column property="entryDate" sortable="true" titleKey="custom.entryDate" format="{0,date,dd-MMM-yyyy}" style="width:50px"  /> 
     <display:column property="documentType" sortable="true" title="Doc Type" style="width:50px" /> 
     <display:column property="documentRef" sortable="true" titleKey="custom.documentRef" style="width:80px" /> 
     <display:column property="pieces" sortable="true" titleKey="custom.pieces" style="width:20px" /> 
     <display:column property="goods" sortable="true" titleKey="custom.goods" style="width:150px" /> 
     <display:column property="volume" sortable="true" titleKey="custom.volume" style="width:50px" /> 
     <display:column property="weight" sortable="true" titleKey="custom.weight" style="width:50px" /> 
     <display:column property="status" sortable="true" title="Status" style="width:50px" /> 
     <sec-auth:authComponent componentId="module.tab.custom.customTab">
     <display:column title="Remove" style="text-align:center; width:20px;">
	   <a><img align="middle" onclick="getTransactionIdList(${customList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	 </display:column> 
	 </sec-auth:authComponent>   
</display:table> 

<s:hidden name="serviceOrderId" value="%{serviceOrder.id}" />
</s:form>
<sec-auth:authComponent componentId="module.tab.custom.customTab">
<c:out value="${buttons}" escapeXml="false" /> 
</sec-auth:authComponent> 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script> 