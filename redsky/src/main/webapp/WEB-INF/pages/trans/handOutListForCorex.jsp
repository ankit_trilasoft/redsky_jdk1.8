<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
.textarea-comment {border:1px solid #219dd1;color:#000;height:17px; width:100px}
.textarea-comment:focus{height:45px; width: 200px;}
.setwidth{width:50px;}
</style>
<c:if test="${flagForHandOutList}">
	<table class="detailTabLabel" cellpadding="0" cellspacing="0"
		border="0" style="width: 100%;">
		<tr valign="top">
			<td align="left"><b>Hand Out List </b></td>
			<td align="right" style="width: 30px;"><img valign="top"
				align="right" class="openpopup" onclick="ajax_hideTooltip()"
				src="<c:url value='/images/closetooltip.gif'/>" /></td>
		</tr>
	</table>
</c:if>
<SCRIPT LANGUAGE="JavaScript">

function findAllResource1()
{
    var st2=document.getElementById('handOutForCorex');
    st2.style.display = 'none';
    setTimeout(function(){findAllHandOut();}, 1000); 
    st2.style.display = 'block';
}

function findAllHandOut1(){
    var st2=document.getElementById('handOutForCorexAjax');
    st2.style.display = 'none';
    setTimeout(function(){findAllHandOut();}, 1000); 
    st2.style.display = 'block';
}

function saveNotesInItem(target,id,workTicketId){
	 $.get("autoSaveNotesInItem.html?ajax=1&decorator=simple&popup=true", 
	            {id:id,valueOfNotes:target.value,workTicketId,workTicketId},
	            function(data){
	            	findAllHandOut();
	        });
}
function checkMsg(target){
    document.getElementById(target.id).style.height = "17px";

}
function checkMsg1(target){
    document.getElementById(target.id).style.height = "45px";
    document.getElementById(target.id).style.width = "200px;";
}

/* function totalWeightAndTotalVolume(){
    var totalWeight = '0';
    var totalVolume = '0';
    var totalActualQty='0';
    var flag =true;
    var holist = "${handOutList}";
    var hoTicketId=location.search.split('ticket=')[2];
    <c:forEach var="entry" items="${handOutList}"> 
    var hoTicket = '${entry.hoTicket}';
    var weight = '${entry.weightReceived}';
    var volume = '${entry.volumeReceived}';
    var workTicketId = '${entry.workTicketId}';
    var shipNumber = "${entry.shipNumber}";
    var actualQty = '${entry.quantityReceived}';
    if(hoTicket != null && hoTicket != ''){
    totalActualQty = totalActualQty*1+actualQty*1;
    totalWeight=totalWeight*1+weight*1;
    totalVolume=totalVolume*1+volume*1;
    }
    if(!hoTicket){
    	flag = false;	
    }
    </c:forEach>
    if(totalWeight !='0'){
    document.getElementById('totalWeight').value=totalWeight.toFixed(2);
    document.getElementById('actWeight').value=totalWeight.toFixed(2);
    }else{
    	document.getElementById('totalWeight').value='0.00';
    	totalWeight='0.00'
    }
    if(totalVolume !='0'){
    document.getElementById('totalVolume').value=totalVolume.toFixed(2);
    document.getElementById('actVolume').value=totalVolume.toFixed(2);
    }else{
    	 document.getElementById('totalVolume').value='0.00';	
    	 totalVolume='0.00';
     }
    if(totalActualQty !='0'){
        document.getElementById('totalActualQty').value=totalActualQty;
        document.getElementById('estimatedPieces').value=totalActualQty.toFixed(2);
            $.get("totalActQty.html?ajax=1&decorator=simple&popup=true", 
                    {workTicketId:workTicketId,totalActualQty:totalActualQty,totalWeight:totalWeight,totalVolume:totalVolume,shipNumber:shipNumber},
                    function(data){ 
                });
        }else{
            document.getElementById('totalActualQty').value='0.00';
            document.getElementById('actVolume').value='0.00';   
            document.getElementById('actWeight').value='0.00';   
            document.getElementById('estimatedPieces').value='0.00';   
            totalActualQty='0.00';
            $.get("totalActQty.html?ajax=1&decorator=simple&popup=true", 
                    {workTicketId:workTicketId,totalActualQty:totalActualQty,totalWeight:totalWeight,totalVolume:totalVolume,shipNumber:shipNumber},
                    function(data){ 
                });
        }
    
    if(!flag){
    	document.getElementById('chkforHo').checked=false;  
    }else{
    	document.getElementById('chkforHo').checked=true;
    }
    if(holist=='[]'){
       document.getElementById('chkforHo').checked=false;
        document.getElementById('chkforHo').disabled = true;
    }else{
    	document.getElementById('chkforHo').disabled = false;
    }
}
totalWeightAndTotalVolume(); */

function totalWeightAndTotalVolume(){
    var totalWeight = '0';
    var totalVolume = '0';
    var totalActualQty='0';
    var flag =true;
    var holist = "${handOutList}";
    var actWeight=  document.getElementById('actWeight').value;
    var actVolume=   document.getElementById('actVolume').value;
    var estPices = document.getElementById('estimatedPieces').value
    <c:forEach var="entry" items="${handOutList}"> 
    var hoTicket = '${entry.hoTicket}';
    var weight = '${entry.weightReceived}';
    var volume = '${entry.volumeReceived}';
    var workTicketId = '${entry.workTicketId}';
    var actualQty = '${entry.quantityReceived}';
    totalActualQty = totalActualQty*1+actualQty*1;
    totalWeight=totalWeight*1+weight*1;
    totalVolume=totalVolume*1+volume*1;
    if(!hoTicket){
        flag = false;   
    }
    </c:forEach>
    document.getElementById('totalWeight').value=actWeight;
    document.getElementById('totalVolume').value=actVolume;
    document.getElementById('totalActualQty').value=estPices;
    if(totalActualQty !='0'){
        //document.getElementById('totalActualQty').value=totalActualQty;
            $.get("totalActQty.html?ajax=1&decorator=simple&popup=true", 
                    {workTicketId:workTicketId,totalActualQty:totalActualQty},
                    function(data){ 
                        
                });
        }else{
          //  document.getElementById('totalActualQty').value='0.00';
        }
    
    if(!flag){
        document.getElementById('chkforHo').checked=false;  
    }else{
        document.getElementById('chkforHo').checked=true;
    }
    if(holist=='[]'){
       document.getElementById('chkforHo').checked=false;
        document.getElementById('chkforHo').disabled = true;
    }else{
        document.getElementById('chkforHo').disabled = false;
    }
}
totalWeightAndTotalVolume();

</script>
<s:set name="handOutList" value="handOutList" scope="request" />
<c:if test ="${ticketAssignedStatus=='Assigned'}">
<c:set var = "assignedValue" value ="true"/>
</c:if>
<display:table name="handOutList" class="table"
	style="margin:3px 0px 5px 0px" requestURI="" id="handOutList">
		 <display:column style="width:5px;" >
     <img  id="partialHO${handOutList.id}" title="Partial Handout" src="<c:url value='/images/externalScope.png'/>" onclick="openDataListForm('${handOutList.id}','${handOutList.serviceOrderId}','${handOutList.workTicketId}','${handOutList.shipNumber}','${handOutList.ticket}','HO');"/> 
     </display:column>
	<display:column title="HO">
	
		<div style="text-align: center">
		
			<c:if
				test="${handOutList.hoTicket !=null && handOutList.hoTicket !=''}">
				<c:if
					test="${handOutList.released == null || handOutList.released == ''}">
					<s:checkbox id="dd${handOutList.id}" name="dd" value="true" disabled="${assignedValue}"
						onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
				</c:if>

				<c:if
					test="${handOutList.released != null && handOutList.released != ''}">
					<s:checkbox id="dd${handOutList.id}" name="dd" value="true"
						disabled="${assignedValue}"
						onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
				</c:if>

			</c:if>

			<c:if
				test="${handOutList.hoTicket ==null || handOutList.hoTicket ==''}">
				<s:checkbox id="dd${handOutList.id}" name="dd" disabled="${assignedValue}"
					onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
			</c:if>
		</div>
	</display:column>
	<display:column property="line" title="Line"
		headerClass="containeralign" style="width:40px;text-align:right;">
	</display:column>
	<display:column property="ticket" title="Ticket#" headerClass="containeralign" />
	<display:column property="itemDesc" style="width:300px;"
		title="Item Description" maxLength="15" />
	<%-- <display:column property="line" sortable="true" title="Line" style="width:40px;text-align:right;"/> --%>
	<display:column style="width:250px;" title="Warehouse">
		<c:forEach var="chrms" items="${house}" varStatus="loopStatus">
			<c:choose>
				<c:when test="${chrms.key == handOutList.warehouse}">
					<c:set var="selectedInd" value=" selected"></c:set>
					<c:out value="${chrms.value}"></c:out>
				</c:when>
			</c:choose>
			</option>
		</c:forEach>
	</display:column>
	<display:column property="released" title="Released"
		style="width:100px" format="{0,date,dd-MMM-yyyy}"></display:column>
	<display:column property="quantityReceived" headerClass="containeralign"
		style="width:100px;text-align:right;" title="Actual&nbsp;Qty"></display:column>
	<display:column property="quantityShipped" headerClass="containeralign"
		style="width:100px;text-align:right;" title="Qty&nbsp;Shipped"></display:column>
	<display:column property="packId" headerClass="containeralign"
		style="width:150px;text-align:right" maxLength="10" title="PackId" />
		
		<display:column property="shippingPackId"  style="width:50px;" headerClass="setwidth"
        maxLength="10" title="New&nbsp;Shipping&nbsp;PackID" />
	
	<c:if test="${flagForHandOutList == false}">
	<display:column property="weightReceived" headerClass="containeralign"
		style="width:80px;text-align:right;" title="Actual&nbsp;Weight" />
        </c:if>
        <c:if test="${flagForHandOutList}">
        <display:column property="weightReceived" headerClass="containeralign"
        style="width:80px;text-align:right;" title="Actual Weight" />
        </c:if>
	<display:column style="width:50px;" title="Unit&nbsp;Weight">
		<c:if
			test="${handOutList.weightReceived !=null && handOutList.weightReceived !=''}">
			<c:out value="${handOutList.unitOfWeight}"></c:out>
		</c:if>
	</display:column>
	
	 <c:if test="${flagForHandOutList}">
	<display:column property="volumeReceived" headerClass="containeralign"
		style="width:80px;text-align:right;" title="Actual Volume" />
	</c:if>
	  <c:if test="${flagForHandOutList == false}">
	  <display:column property="volumeReceived" headerClass="containeralign"
        style="width:80px;text-align:right;" title="Actual&nbsp;Volume" />
	  </c:if>	
    
	<display:column style="width:50px;" title="Unit&nbsp;Volume">
		<c:if
			test="${handOutList.volumeReceived !=null && handOutList.volumeReceived !=''}">
			<c:out value="${handOutList.unitOfVolume}"></c:out>
		</c:if>
	</display:column>

	<c:if test="${flagForHandOutList==false}">
		<display:column title="Notes" style="width:300px;">
			<c:if
				test="${handOutList.released == null || handOutList.released == ''}">
				<textarea class="textarea-comment pr-f11"
					title="<c:out value="${handOutList.notes}"></c:out>"
					maxlength="490" id="notes${handOutList.id}"
					name="notes${handOutList.id}" onmouseover=""
					onmouseout="checkMsg(this);" onclick="checkMsg1(this)"
					onchange="saveNotesInItem(this,'${handOutList.id}','${handOutList.workTicketId}');"
					style="width: 250px"><c:out
						value='${handOutList.notes}' /></textarea>
			</c:if>
			<c:if
				test="${handOutList.released != null && handOutList.released != ''}">
				<textarea class="inupt-textUpper"
					style="width: 250px; height: 15px; background-color: #FAFAFA;"
					title="<c:out value="${handOutList.notes}"></c:out>"
					 maxlength="490" id="notes${handOutList.id}"
					name="notes${handOutList.id}" readonly><c:out
						value='${handOutList.notes}' /></textarea>
			</c:if>
		</display:column>
	</c:if>
	<c:if test="${flagForHandOutList}">
		<display:column property="notes" title="Notes" maxLength="10"
			style="width:300px;">
		</display:column>
	</c:if>
	<c:if test="${flagForHandOutList}">
		<display:column style="width:50px;">
			<input type="button" value="Save" class="cssbutton1"
				style="width: 42px; height: 22px; margin-bottom: 5px;"
				onclick="updateHoTicket('${handOutList.id}','${handOutList.ticket}')" />
		</display:column>
	</c:if>
	<c:if test="${flagForHandOutList==false}">
		<display:footer>
			<tr>
				<td align="right" colspan="5"></td>
				<td></td>
				<td style="text-align: right ! important; padding-right: 2px;"><div
						style="float: right; line-height: 18px; padding-right: 2px;">Total:</div></td>
						
						<td
                    style="width: 85px; text-align: right ! important; padding-right: 2px;">
                    <div style="float: right">
                        <div style="float: left; padding-left: 2px;">
                            <input type="text" id='totalActualQty'
                                style="text-align: right; width: 60px;" readonly="true"
                                size="10" class="input-textUpper myclass" />
                        </div>
                    </div>
                </td>
				<td align="right" colspan="3"></td>	
				<td
					style="width: 85px; text-align: right ! important; padding-right: 2px;">
					<div style="float: right">
						<div style="float: left; padding-left: 2px;">
							<input type="text" id='totalWeight'
								style="text-align: right; width: 60px;" readonly="true"
								size="10" class="input-textUpper myclass" />
						</div>
					</div>
				</td>
				<td></td>
				<td
					style="text-align: right ! important; padding-right: 2px; width: 85px;">
					<div style="float: right">
						<div style="float: left; line-height: 18px;"></div>
						<div style="float: left; padding-right: 2px;">
							<input type="text" style="text-align: right; width: 60px;"
								readonly="true" id='totalVolume' size="10"
								class="input-textUpper myclass" />
						</div>
					</div>
				</td>
				<td></td>
				<td></td>
			</tr>
		</display:footer>
	</c:if>
</display:table>