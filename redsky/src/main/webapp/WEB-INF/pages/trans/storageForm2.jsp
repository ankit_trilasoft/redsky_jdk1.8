<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%> 
 
<head>
    <title><fmt:message key="storageDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='storageDetail.heading'/>"/>
    <style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>


<script>
function  checkLengthOnSave(){
	var txt = document.forms['storageForm2'].elements['storage.description'].value;
	if(txt.length >1000){
		alert("Cannot enter more than 1000 characters in description field.");
		document.forms['storageForm2'].elements['storage.description'].value = txt.substring(0,1000);
		return false;
	}else{
		return true;
	}
}
</script>
<script>
function ContainerAutoSave(clickType){
if ('${autoSavePrompt}' == 'No'){
	var noSaveAction;
		if(document.forms['storageForm2'].elements['gotoPageString'].value == 'gototab.storage'){
                noSaveAction = 'storages2.html';
        }
		processAutoSave(document.forms['storageForm2'],'saveTraceStorage!saveOnTabChange.html', noSaveAction );
	}
	else{
    if(!(clickType == 'save')){
    var id1 = document.forms['storageForm2'].elements['storage.id'].value;
       if (document.forms['storageForm2'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='storageDetail.heading'/>");
        if(agree){
            document.forms['storageForm2'].action = 'saveTraceStorage!saveOnTabChange.html';
            document.forms['storageForm2'].submit();
        }else{
if(document.forms['storageForm2'].elements['gotoPageString'].value == 'gototab.storage'){
                location.href = 'storages2.html';
               }
        }
    }else{
    
if(document.forms['storageForm2'].elements['gotoPageString'].value == 'gototab.storage'){
                location.href = 'storages2.html';
    }
    }
   }
   }
  }

function changeStatus()
{
    document.forms['storageForm2'].elements['formStatus'].value = '1';
}
</script>
</head>


<s:form id="storageForm2" action="saveTraceStorage" method="post" validate="true"> 
<s:hidden name="storage.id" value="%{storage.id}"/> 
<s:hidden name="storage.corpID" /> 
<s:hidden name="storage.idNum" /> 
<s:hidden name="storage.oldLocation" /> 
<s:hidden name="storage.type" />
<s:hidden name="storage.model" />
<s:hidden name="storage.serial" />
<s:hidden name="measQuantity" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.storage' }">
    <c:redirect url="/storages2.html?id=${storage.id}"/>
</c:when>
</c:choose>
</c:if>
<div id="Layer1" style="width:85%">
<div id="newmnav">
						  <ul>
						   	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Trace Storage Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						  	 <li><a onclick="setReturnString('gototab.storage');return  ContainerAutoSave('none');"><span>Trace Storage List</span></a></li>
						  	 </ul>
				</div><div class="spn">&nbsp;</div>
				<div style="padding-bottom:0px;"></div>
			<div id="content" align="center" >
				<div id="liquid-round">
   			<div class="top"><span></span></div>
   				<div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:600px">
		<tbody>
			<tr>
				<td>
				<!--<div class="subcontent-tab">Tracing&nbsp;Storage&nbsp;Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				--><table class="detailTabLabel" cellspacing="1" cellpadding="1" border="0">
					<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Ticket #</td>						
													
							<td class="listwhitetext" ><s:textfield key="storage.ticket" required="false" readonly="true" cssClass="input-textUpper"/></td>
							<td align="right" class="listwhitetext" style="width:136px"></td>
							<td align="right" class="listwhitetext">S/O #</td>
							
							<td class="listwhitetext" ><s:textfield key="storage.shipNumber" readonly="true" required="false" cssClass="input-textUpper"/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
						<tr>
							
							<td class="listwhitetext" align="right">Shipper&nbsp;Name</td>
							
							<td class="listwhitetext" ><s:textfield name="shipperName" readonly="true" value="${workTicket.lastName}, ${workTicket.firstName}" required="false" cssClass="input-textUpper"/></td>
						
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						
							
							<td class="listwhitetext" align="right">Location</td>
							
							<td class="listwhitetext"><s:textfield key="storage.locationId"  required="true" readonly="true" cssClass="input-textUpper"/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
						<tr>
							
							<!--<td class="listwhitetext" align="right">Item #</td>
							
							<td class="listwhitetext"><s:textfield key="storage.itemNumber"  required="false" readonly="true" cssClass="input-textUpper"/></td>
							
							<td align="right" class="listwhitetext" style="width:10px"></td>-->
							<td class="listwhitetext" align="right">Item Tag</td>							
							<td class="listwhitetext"><s:textfield key="storage.itemTag"  required="false" readonly="true" cssClass="input-textUpper"/></td>
                            <td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdVOER">
							<td class="listwhitetext" align="right">Storage Id</td>							
							<td class="listwhitetext"><s:textfield key="storage.storageId"  required="true" readonly="true" cssClass="input-textUpper"/></td>
							</configByCorp:fieldVisibility>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.storageStorageIdSSCW">
		                     <td></td>
		                     <td><s:hidden name="storage.storageId" /></td>
			                 </configByCorp:fieldVisibility>
							<td align="right" class="listwhitetext" style="width:10px"></td>
						
						</tr>
						
						<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Container&nbsp;#</td>
							
							<td class="listwhitetext"><s:textfield key="storage.containerId"  required="false" cssClass="input-text" tabindex="2"/></td>
					
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						
							<td class="listwhitetext" align="right">Price</td>
							
							<td class="listwhitetext"><s:textfield key="storage.price"  required="false" cssClass="input-text" tabindex="3"/></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
						<tr>
							<td class="listwhitetext" align="right">Pieces</td>
							
							<td class="listwhitetext"><s:textfield key="storage.pieces"  required="false" cssClass="input-text" tabindex="4"/></td>
						
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						
							<td class="listwhitetext" align="right">Job</td>
							
							<td class="listwhitetext"><s:textfield key="workTicket.jobType"  required="false" cssClass="input-text" tabindex="5"/></td>
							<td class="listwhitetext"></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" width="90" valign="top">Description</td>
							
							<td class="listwhitetext" colspan="10"><s:textarea name="storage.description"  cssClass="textarea" cols="80" rows="3" tabindex="1" /></td>
							<td class="listwhitetext"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:2px;"></td>	
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			<table width="700px">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='storage.createdOn'/></b></td>
							<c:if test="${not empty storage.createdOn}">
								<s:text id="customerFilecreatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="storage.createdOn" /></s:text>
								<td valign="top"><s:hidden name="storage.createdOn" value="%{customerFilecreatedOnFormattedValue}" /></td>
								<td style="width:120px"><fmt:formatDate value="${storage.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							</c:if>
							<c:if test="${empty storage.createdOn}">
								<td style="width:75px"></td>		
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='storage.createdBy' /></b></td>
							<c:if test="${not empty storage.id}">
								<s:hidden name="storage.createdBy"/>
								<td ><s:label name="createdBy" value="%{storage.createdBy}"/></td>
							</c:if>
							<c:if test="${empty storage.id}">
								<s:hidden name="storage.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='storage.updatedOn'/></b></td>
							<c:if test="${not empty storage.updatedOn}">
								<s:text id="customerFileupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="storage.updatedOn" /></s:text>
								<s:hidden name="storage.updatedOn" value="%{customerFileupdatedOnFormattedValue}" />
								<td style="width:120px"><fmt:formatDate value="${storage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							</c:if>
							<c:if test="${empty storage.updatedOn}">
								<td style="width:75px"></td>		
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='storage.updatedBy' /></b></td>
							<c:if test="${not empty storage.id}">
								<s:hidden name="storage.updatedBy"/>
								<td ><s:label name="updatedBy" value="%{storage.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty storage.id}">
								<s:hidden name="storage.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					</tbody>
				</table>
<s:hidden name="serviceOrder.shipNumber"/>	
<s:hidden name="workTicket.lastName"/>			
<s:hidden name="workTicket.firstName"/>			

<s:hidden name="storage.toRelease" value="%{storage.toRelease}"/>

</div>
<!--<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="saveTraceStorage" key="button.save" onclick="return checkLengthOnSave();" tabindex="6"/>-->  

</s:form>
   
<script type="text/javascript"> 
    Form.focusFirstElement($("storageForm2")); 
    try{
    for(i=0;i<=80;i++)
			{
			
					document.forms['storageForm2'].elements[i].disabled = true;
			}
    }
    catch(e){}
    
</script> 