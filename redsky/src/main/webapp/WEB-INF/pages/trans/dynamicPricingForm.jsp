<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Dynamic Pricing</title>   
    <meta name="heading" content="Dynamic Pricing"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>    
	div#mydiv{margin-top: -45px;}	
	
	.wrap {
    width: 228px;
}

  .HMtable {    
   width:235px;
  border-collapse: collapse;
}

.HMtable tr td {
    border: 1px solid #E0E0E0;
    border-collapse: collapse;
    padding: 4px 3px 5px 5px;
    width: 42px;
    word-wrap: break-word;
    font-family: arial,verdana;
    font-size: 12px;
    color:#000000;
    
}
.HMtable tr.even {
    background-color: #E4E6F2;
    border-top: 1px solid #C0C0C0;
    color: #000000;
}
.HMtable tr.odd {
    background: none repeat scroll 0 0 #FFFFFF;
    border-top: 1px solid #C0C0C0;
    color: #000E4D;
}
.HMtable tr td.head {    
 background: url("/redsky/images/bg_listheader.png") repeat-x scroll 0 0 #BCD2EF;
    border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    height: 20px;
    padding: 2px 3px 3px 5px;
    text-decoration: none;
}

.inner_table {
    height: 111px;
    overflow-y: auto;
 	width:252px;
 
}
.EmptyHM {
	background: none repeat scroll 0 0 #FFFFFF;
    border:2px solid #74B3DC;
    border-top:none;;
    float: left;
    height: 10px;
    padding: 5px;
    text-align: left;
    width: 214px;
}
</style>
	
<!-- Modified script 12-Dec-2013 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript">
function checkCategory(target){
	var type=target.value;
	if((type!='')&&(type!=null)&&(type!=undefined)){
    document.forms['dynamicPricingForm'].action ="dynamicPricingCall.html?dynamicPricingCategoryType="+type;
	document.forms['dynamicPricingForm'].submit();
	}
}
function checkCategory1(type){	
	if((type!='')&&(type!=null)&&(type!=undefined)){
    document.forms['dynamicPricingForm'].action ="dynamicPricingCall.html?dynamicPricingCategoryType="+type;
	document.forms['dynamicPricingForm'].submit();
	}
}
function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  	   if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	if (agree){
		 var url="deleteHolidayMaintenceAjax.html?ajax=1&decorator=simple&popup=true&hid="+id;
		 httpTemplateDelete.open("GET", url, true);
		 httpTemplateDelete.onreadystatechange = handleHttpResponseForTemplateDelete;
		 httpTemplateDelete.send(null);
	}else{
		return false;
	}	
}
function handleHttpResponseForTemplateDelete(){
	if (httpTemplateDelete.readyState == 4){
		document.forms['dynamicPricingForm'].elements['holiDayDate'].value='';
		document.forms['dynamicPricingForm'].elements['holiDayRate'].value='';
		window.location.reload();
	}
}
  var httpTemplate = getHTTPObject();
  var httpTemplateDelete = getHTTPObject();
function updateHolidayMaintence(){
	 var holiDayDate=document.forms['dynamicPricingForm'].elements['holiDayDate'].value;
	 var holiDayRate=document.forms['dynamicPricingForm'].elements['holiDayRate'].value;
	 var dynamicPricingType=document.forms['dynamicPricingForm'].elements['dynamicPricing.dynamicPricingType'].value;
	 holiDayDate=convertItProperDateFormate(holiDayDate);
	 if((holiDayRate!=null)&&(holiDayRate!=undefined)&&(holiDayRate!='')&&(holiDayRate!='0')&&(holiDayRate!='0.0')&&(holiDayRate!='0.00')&&(holiDayDate!=null)&&(holiDayDate!=undefined)&&(holiDayDate!='')&&(dynamicPricingType!=null)&&(dynamicPricingType!=undefined)&&(dynamicPricingType!='')){
		 var url="updateHolidayMaintenceAjax.html?ajax=1&decorator=simple&popup=true&holiDayDate="+encodeURI(holiDayDate)+"&holiDayRate="+encodeURI(holiDayRate)+"&dynamicPricingCategoryType="+dynamicPricingType;
		 httpTemplate.open("GET", url, true);
		 httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		 httpTemplate.send(null);
	 }else{
		 alert("Please Input Right Data.");
	 }
}
function handleHttpResponseForTemplate(){
	if (httpTemplate.readyState == 4){
		document.forms['dynamicPricingForm'].elements['holiDayDate'].value='';
		document.forms['dynamicPricingForm'].elements['holiDayRate'].value='';
		window.location.reload();
	}
}
function checkValidation(){
	var dynamicPricingType=document.forms['dynamicPricingForm'].elements['dynamicPricing.dynamicPricingType'].value;
	if(dynamicPricingType==''){
		alert("Please Select Dynamic Pricing");
		return false;
	}else{
		
	}
}
function calculateDynamicPricingMethod(){
	var regularHourlyRatePer2Men=document.forms['dynamicPricingForm'].elements['dynamicPricing.regularHourlyRatePer2Men'].value;
	 var roundValue=0.00;
	if(regularHourlyRatePer2Men!='' && parseFloat(regularHourlyRatePer2Men)!=0){
		var extraMenRate=document.forms['dynamicPricingForm'].elements['dynamicPricing.extraMenRate'].value;
		roundValue=parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate);
		roundValue=Math.round(roundValue*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularHourlyRatePer3Men'].value=roundValue;
		roundValue=(2*parseFloat(extraMenRate))+parseFloat(regularHourlyRatePer2Men);
		roundValue=Math.round(roundValue*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularHourlyRatePer4Men'].value=roundValue;
		var temp=0.00;
		var deviationRange1forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forMoreThan21Days'].value=temp;

		temp=0.00;
		var deviationRange2forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forMoreThan21Days'].value=temp;

		temp=0.00;
		var deviationRange3forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange4forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange5forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange6forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange1forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2))
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2))
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange2forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange3forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forBetween14to21Days'].value;
		temp=Math.round(temp*100)/100;
		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange4forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange5forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange6forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forBetween14to21Days'].value=temp;
					
		var temp=0.00;
		var deviationRange1forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange2forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange3forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange4forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange5forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange6forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forBetween7to13Days'].value=temp;

		var temp=0.00;
		var deviationRange1forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange2forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange3forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange4forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange5forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange6forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(regularHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forLessThan7Days'].value=temp;
					
	}else{
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange1forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange1forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange1forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange2forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange2forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange2forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange3forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange3forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange3forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange4forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange4forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange4forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange5forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange5forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange5forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer2MenRange6forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer3MenRange6forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.regularRatePer4MenRange6forLessThan7Days'].value="0.00";
		

		}
	var monthEndHourlyRatePer2Men=document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndHourlyRatePer2Men'].value;
	if(monthEndHourlyRatePer2Men!='' && parseFloat(monthEndHourlyRatePer2Men)!=0){
		var extraMenRate=document.forms['dynamicPricingForm'].elements['dynamicPricing.extraMenRate'].value;
		roundValue=parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate);
		roundValue=Math.round(roundValue*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndHourlyRatePer3Men'].value=roundValue;
		roundValue=(2*parseFloat(extraMenRate))+parseFloat(monthEndHourlyRatePer2Men);
		roundValue=Math.round(roundValue*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndHourlyRatePer4Men'].value=roundValue;
		
		var temp=0.00;
		var deviationRange1forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forMoreThan21Days'].value=temp;

		temp=0.00;
		var deviationRange2forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange3forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange4forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange5forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forMoreThan21Days'].value=temp;			

		temp=0.00;
		var deviationRange6forMoreThan21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forMoreThan21Days'].value;
		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forMoreThan21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forMoreThan21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forMoreThan21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forMoreThan21Days'].value=temp;

		temp=0.00;
		var deviationRange1forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange2forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange3forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forBetween14to21Days'].value;		
		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange4forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange5forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forBetween14to21Days'].value=temp;			

		temp=0.00;
		var deviationRange6forBetween14to21Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forBetween14to21Days'].value;
		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forBetween14to21Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forBetween14to21Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forBetween14to21Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forBetween14to21Days'].value=temp;
					
		var temp=0.00;
		var deviationRange1forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange2forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange3forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange4forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange5forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forBetween7to13Days'].value=temp;
		
		var temp=0.00;
		var deviationRange6forBetween7to13Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forBetween7to13Days'].value;
		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forBetween7to13Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forBetween7to13Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forBetween7to13Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forBetween7to13Days'].value=temp;

		var temp=0.00;
		var deviationRange1forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange1forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange1forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange2forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange2forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange2forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange3forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange3forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange3forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange4forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange4forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange4forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange5forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange5forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange5forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forLessThan7Days'].value=temp;

		var temp=0.00;
		var deviationRange6forLessThan7Days=document.forms['dynamicPricingForm'].elements['dynamicPricing.deviationRange6forLessThan7Days'].value;
		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(2-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forLessThan7Days'].value=temp;

		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(3-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forLessThan7Days'].value=temp;			

		temp=(1+(parseFloat(deviationRange6forLessThan7Days)/100))*(parseFloat(monthEndHourlyRatePer2Men)+parseFloat(extraMenRate)*(4-2));
		temp=Math.round(temp*100)/100;
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forLessThan7Days'].value=temp;
		
	}else{
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forMoreThan21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forMoreThan21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forBetween14to21Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forBetween14to21Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forBetween7to13Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forBetween7to13Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange1forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange1forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange1forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange2forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange2forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange2forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange3forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange3forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange3forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange4forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange4forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange4forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange5forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange5forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange5forLessThan7Days'].value="0.00";

		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer2MenRange6forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer3MenRange6forLessThan7Days'].value="0.00";
		document.forms['dynamicPricingForm'].elements['dynamicPricing.monthEndRatePer4MenRange6forLessThan7Days'].value="0.00";
	}

	
}
function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	} 
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode; 
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==173) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
	}

</script>	
	</head>
	<s:form id="dynamicPricingForm" action="saveDynamicPricing" onsubmit="return checkValidation();" method="post" validate="true">
	<s:hidden name="id" value="${dynamicPricing.id}" />
	<s:hidden name="dynamicPricing.id" />	
	<s:hidden name="dynamicPricing.corpId" />	
	<!-- Main Table	-->	
	<table id="mytable" cellspacing="0" cellpadding="0" border="0" width="100%" class="LabelDP">
	<tr>	
	<td>
	
	<table cellspacing="1" cellpadding="2" border="0" width="100%">
	<tr><td class="listwhitetext" height="10"></td></tr>
	<tr>	
	<td class="listwhitelargetext" align="right"><b>Dynamic Pricing</b></td>
	<td></td>
	<td>
		<s:select  cssClass="list-menu"  key="dynamicPricing.dynamicPricingType" cssStyle="width:150px" list="%{dynamicPricingCategory}" onchange="checkCategory(this);" headerKey="" headerValue=""  />
	</td>		
	</tr>
	</table>
	
	<table cellspacing="1" cellpadding="2" border="0">
	<tr>	
	<td valign="top">
	<table cellspacing="1" cellpadding="2" border="0">
	<tr>
	<td></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td> 
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Regular Hourly Rate</td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)"  cssClass="input-text" key="dynamicPricing.regularHourlyRatePer2Men" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularHourlyRatePer3Men" maxlength="" size="8" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularHourlyRatePer4Men" maxlength="" size="8" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Month End Hourly Rate</td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndHourlyRatePer2Men" onchange="calculateDynamicPricingMethod();" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndHourlyRatePer3Men" maxlength="" size="8" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndHourlyRatePer4Men" maxlength="" size="8" /></td>
	</tr>	
	
	<tr>
	<td class="listwhitetext" align="right">Extra Men</td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.extraMenRate" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>		
	</tr>
	
	<tr><td class="listwhitetext" height="5"></td></tr>
	
	<tr>
	<td class="listwhitetext" align="right"># of Employees</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.noOfEmployees" maxlength="" size="8" /></td>		
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Saturday Rate %</td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.saturdayRatePersent" maxlength="" size="8" /></td>		
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Sunday Rate %</td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.sundayRatePersent" maxlength="" size="8" /></td>		
	</tr>	
	
	<tr><td class="listwhitetext" height="5"></td></tr>
	
	<tr>	
	<td class="listwhitetext" align="right">Range</td> 
	<td class="listwhitetext" align="center">Min Men</td> 
	<td class="listwhitetext" align="center">Max Men</td> 
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #1</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange1Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange1Men" maxlength="" size="8" /></td>	
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #2</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange2Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange2Men" maxlength="" size="8" /></td>	
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #3</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange3Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange3Men" maxlength="" size="8" /></td>	
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #4</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange4Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange4Men" maxlength="" size="8" /></td>	
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #5</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange5Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange5Men" maxlength="" size="8" /></td>	
	</tr>	
	
	<tr>
	<td class="listwhitetext" align="right">Range #6</td>
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.minRange6Men" maxlength="" size="8" /></td>	
	<td align="left"><s:textfield onkeydown="return onlyNumberAllowed(event)" cssClass="input-text" key="dynamicPricing.maxRange6Men" maxlength="" size="8" /></td>	
	</tr>	
	</table>
	
	<table cellspacing="1" cellpadding="2" border="0">
	<tr><td class="listwhitetext" height="10"></td></tr>
	<tr>	
	<td class="listwhitetext" align="center" colspan="2"><b>Holiday Maintenance</b></td> 	
	</tr>
	
	<tr>
	<td width="1"></td>	 
	<td class="listwhitetext" align="right">Date</td> 
	<td><s:textfield name="holiDayDate" cssClass="input-text" id="date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /></td>
	<td><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>	
	<td class="listwhitetext" align="right">Rate %</td> 
	<td><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" name="holiDayRate" size="5" /></td>	
	</tr>
	<tr><td class="listwhitetext" height="5"></td></tr>
	<tr>
	<td colspan="5"></td>
	<td class="listwhitetext" align="middle">
	<input type="button" class="cssbuttonA" style="width:47px;height:20px;" onclick="updateHolidayMaintence();" value="Add"  />
	</td>
	</tr>
	
	<tr>
	<td width="67"></td>	
	<td colspan="5" align="left">	

    <table class="HMtable" cellspacing="1" style="margin:0px;">
        <tr>
            <td class="head" style="width:66px;">Date</td>
            <td class="head">Percent</td>
            <td class="head">BRANCH</td>
             <c:if test="${dynamicPricingList!='[]'}"> 
             <td class="head">Remove</td>
             </c:if>
        </tr>
    </table>
    <div class="wrap">
    <div class="inner_table">
        <table class="HMtable" style="margin:0px;border:2px solid #74B3DC;border-top:none;">
         <c:if test="${dynamicPricingList!='[]'}"> 
			<c:forEach var="individualItem" items="${dynamicPricingList}" varStatus="rowCounter">
			<c:if test="${rowCounter.count % 2 == 0}">
			 	<tr class="even">
			</c:if>
			<c:if test="${rowCounter.count % 2 != 0}">
				<tr class="odd">
			</c:if>
			 		<td>
						<c:set var="ss" value="${individualItem.holiDayDate}"/>
						<c:set var="string2" value="${fn:substring(ss, 0, 10)}" />
						<c:set var="string1" value="${fn:replace(string2, '-', '/')}" />
						<c:out value="${string1}"/>
					</td>
			 		<td><c:out value="${individualItem.holiDayRate}"/></td>
			 		<td><c:out value="${individualItem.holiDayBranch}"/></td>
			 		<td><a><img align="middle" onclick="confirmSubmit(${individualItem.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a></td>
				</tr>
			</c:forEach>
		 </c:if>
	     <c:if test="${dynamicPricingList=='[]'}"> 
	     <div class="EmptyHM">Nothing found to display.</div>
	     </c:if>
    </table>
    </div>
</div>
	</td>
	</tr>
	
	</table>
	
	</td>
	<!-- 2nd Column	-->
	<td valign="top">
	<table cellspacing="1" cellpadding="2" border="0">
	
	<tr>
	<td class="listwhitetext" align="center"></td>
	<td width="10"></td>
	<td class="listwhitetext" align="center"></td>	
	<td width="10"></td>
	<td class="listwhitetext" colspan="3" align="center"><b>Regular Rate</b></td> 	 
	<td width="10"></td>
	<td class="listwhitetext" colspan="3" align="center"><b>Month End Rate</b></td> 	
	</tr>
	
	<tr>
	<td class="listwhitetext" align="center">> 21 Days Lead Time</td>
	<td width="10"></td>
	<td class="listwhitetext" align="center">Deviation (%)</td>	
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td> 
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #1</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange1forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange1forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #2</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange2forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange2forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #3</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange3forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange3forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #4</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange4forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange4forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #5</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange5forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange5forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #6</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange6forMoreThan21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange6forMoreThan21Days" maxlength="" size="5" /></td>
	</tr>
		
	<tr><td class="listwhitetext" height="10"></td></tr>
	
	<!-- 1st Row End -->
	
	<tr>
	<td class="listwhitetext" align="center">21-14 Days Lead Time</td>
	<td width="10"></td>
	<td class="listwhitetext" align="center">Deviation (%)</td>	
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td> 
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #1</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange1forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange1forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #2</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange2forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange2forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #3</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange3forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange3forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #4</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange4forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange4forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #5</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange5forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange5forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #6</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange6forBetween14to21Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange6forBetween14to21Days" maxlength="" size="5" /></td>
	</tr>
	<tr><td class="listwhitetext" height="10"></td></tr>
	<!-- 2nd Row End -->
	
	<tr>
	<td class="listwhitetext" align="center">13-7 Days Lead Time</td>
	<td width="10"></td>
	<td class="listwhitetext" align="center">Deviation (%)</td>	
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td> 
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #1</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange1forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange1forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #2</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange2forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange2forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #3</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange3forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange3forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #4</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange4forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange4forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #5</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange5forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange5forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #6</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange6forBetween7to13Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange6forBetween7to13Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr><td class="listwhitetext" height="10"></td></tr>
	
	<!-- 3rd Row End -->
	
	<tr>
	<td class="listwhitetext" align="center"> <7 Days Lead Time</td>
	<td width="10"></td>
	<td class="listwhitetext" align="center">Deviation (%)</td>	
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td> 
	<td width="10"></td>
	<td class="listwhitetext" align="center">2 Men</td> 
	<td class="listwhitetext" align="center">3 Men</td> 
	<td class="listwhitetext" align="center">4 Men</td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #1</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange1forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange1forLessThan7Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #2</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange2forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange2forLessThan7Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #3</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange3forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange3forLessThan7Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #4</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange4forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange4forLessThan7Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #5</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange5forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange5forLessThan7Days" maxlength="" size="5" /></td>
	</tr>
	
	<tr>
	<td class="listwhitetext" align="right">Range #6</td>
	<td width="10"></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.deviationRange6forLessThan7Days" maxlength="" onchange="calculateDynamicPricingMethod();" size="8" /></td>
	<td width="10"></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer2MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer3MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.regularRatePer4MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	<td></td>	
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer2MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer3MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	<td align="left"><s:textfield onkeydown="return onlyFloatNumsAllowed(event)" cssClass="input-text" key="dynamicPricing.monthEndRatePer4MenRange6forLessThan7Days" maxlength="" size="5" /></td>
	</tr>	
	</table>	
	
	<table cellspacing="3" cellpadding="2" border="0" style="padding-left:8px;">	
	<tr>
	<td class="listwhitetext" align="middle" colspan="2"></td></tr>	
	<tr>
	<td class="listwhitetext" width="200"></td>
	<td class="listwhitetext" height="0"></td>
	<td class="listwhitetext" height="0"></td>
	<td class="listwhitetext" align="middle">
	<s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" value="OK"  theme="simple"/>
	
	</td>
	<td class="listwhitetext" align="middle">
		<s:reset cssClass="cssbutton1"  cssStyle="width:55px; height:25px" value="Cancel"  theme="simple"/>
	</td>
	</tr>
	</table>
	
	</td>
	
	<!-- 3rd Column	-->
	
	<td valign="top">	
	
	<table cellspacing="1" cellpadding="2" border="0">	
	<tr><td class="listwhitetext" height="15"></td></tr>	
	</table>
	
	<fieldset style="height:113px;padding:0px;">
	<table cellspacing="3" cellpadding="2" border="0">	
	<tr>
	<td class="listwhitetext" align="right" colspan="2"><b>Also Update Deviaton for...</b></td>
	</tr>
	<tr><td class="listwhitetext" height="0"></td></tr>
	
	<tr>
	<c:set var="isEdmSummer" value="false"/>
	<c:if test="${edmSummer}">
	<c:set var="isEdmSummer" value="true"/>
	</c:if>
	<td><s:checkbox name="edmSummer" id="edmSummer" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" /></td>
	<td class="listblacktext" align="left">Edmonton - Summer</td>
	</tr>
	
	<tr>
	<c:set var="isEdmWinter" value="false"/>
	<c:if test="${edmWinter}">
	<c:set var="isEdmWinter" value="true"/>
	</c:if>
	<td><s:checkbox name="edmWinter" id="edmWinter" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" /></td>
	<td class="listblacktext" align="left">Edmonton - Winter</td>
	</tr>
	
	<tr>
	<c:set var="isCalSummer" value="false"/>
	<c:if test="${calSummer}">
	<c:set var="isCalSummer" value="true"/>
	</c:if>
	<td><s:checkbox name="calSummer" id="calSummer" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" /></td>
	<td class="listblacktext" align="left">Calgary - Summer</td>
	</tr>
	
	<tr>
	<c:set var="isCalWinter" value="false"/>
	<c:if test="${calWinter}">
	<c:set var="isCalWinter" value="true"/>
	</c:if>
	<td><s:checkbox name="calWinter" id="calWinter" cssStyle="margin:0px;" value="${isVipFlag}" fieldValue="true" /></td>
	<td class="listblacktext" align="left">Calgary - Winter</td>
	</tr>
	
	</table>
	</fieldset>
	
	<table cellspacing="1" cellpadding="2" border="0">	
	<tr><td class="listwhitetext" height="34"></td></tr>	
	</table>
	
	<fieldset style="height:137px;padding:0px;background-color:#EFEFEF;">
	<table cellspacing="3" cellpadding="2" border="0" style="padding-left:8px;">	
	<tr>
	<td class="listwhitetext" align="middle" colspan="2"><b>Color Legends</b></td>
	</tr>	
	<tr>
	<td class="listwhitetext" align="middle">
	<input type="button" class="ButtonYellow" style="width:125px;height:20px;" onclick="checkCategory1('EDMSUMMER')" value="Edmonton - Summer"  />
	</td>
	</tr>	
	<tr>
	<td class="listwhitetext" align="middle">
	<input type="button" class="ButtonDarkYellow" style="width:125px;height:20px;" onclick="checkCategory1('EDMWINTER')" value="Edmonton - Winter"  />
	</td>
	</tr>
	<tr>
	<td class="listwhitetext" align="middle">
	<input type="button" class="ButtonPink" style="width:125px;height:20px;" onclick="checkCategory1('CALSUMMER')" value="Calgary - Summer"  />
	</td>
	</tr>
	<tr>
	<td class="listwhitetext" align="middle">
	<input type="button" class="ButtonDarkPink" style="width:125px;height:20px;" onclick="checkCategory1('CALWINTER')" value="Calgary - Winter"  />
	</td>
	</tr>	
	</table>
	</fieldset>
	</td>	
	</tr>
	</table>
	</td>
	</tr>
	</table>
	
	<table width="90%">
	<tbody>
	<tr><td align="left" rowspan="1"></td></tr>
	<tr><fmt:formatDate var="dynamicPricingCreatedOnFormattedValue" value="${dynamicPricing.createdOn}" 
			pattern="${displayDateTimeEditFormat}"/>
		<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='dynamicPricing.createdOn'/></td>
		<s:hidden name="dynamicPricing.createdOn" value="${dynamicPricingCreatedOnFormattedValue}" />
		<td ><fmt:formatDate value="${dynamicPricing.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='dynamicPricing.createdBy' /></td>		
		
		<c:if test="${not empty dynamicPricing.id}">
				<s:hidden name="dynamicPricing.createdBy"/>
				<td ><s:label name="createdBy" value="%{dynamicPricing.createdBy}"/></td>
			</c:if>
			<c:if test="${empty dynamicPricing.id}">
				<s:hidden name="dynamicPricing.createdBy" value="${pageContext.request.remoteUser}"/>
				<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='dynamicPricing.updatedOn'/></td>
		<fmt:formatDate var="dynamicPricingUpdatedOnFormattedValue" value="${dynamicPricing.updatedOn}" 
			pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="dynamicPricing.updatedOn" value="${dynamicPricingUpdatedOnFormattedValue}"/>
		<td ><fmt:formatDate value="${dynamicPricing.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='dynamicPricing.updatedBy' /></td>
		<c:if test="${not empty dynamicPricing.id}">
			<s:hidden name="dynamicPricing.updatedBy"/>
			<td style="width:85px"><s:label name="updatedBy" value="%{dynamicPricing.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty dynamicPricing.id}">
			<s:hidden name="dynamicPricing.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
	</table>
	</s:form>
	<script type="text/javascript">
var colorType='${dynamicPricingCategoryType}';
if((colorType==null)||(colorType==undefined)||(colorType=='')){
	document.getElementById('mytable').style.backgroundColor = '#FDFDE3';
}else if(colorType=='EDMSUMMER'){
	document.getElementById('mytable').style.backgroundColor = '#FDFDE3';
}else if(colorType=='EDMWINTER'){
	 document.getElementById('mytable').style.backgroundColor = '#FFF0AB';
}else if(colorType=='CALSUMMER'){
	 document.getElementById('mytable').style.backgroundColor = '#FFF4E6';
}else if(colorType=='CALWINTER'){
	 document.getElementById('mytable').style.backgroundColor = '#FFC3DD';
}else{
	
}
</script>
	<script>
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	</script>