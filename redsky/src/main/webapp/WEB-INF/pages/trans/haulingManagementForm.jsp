<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>

<head>
<title><fmt:message key="haulingMgtForm.title" /></title>
<meta name="heading" content="<fmt:message key='haulingMgtForm.heading'/>" />
<style type="text/css">
    #doublescroll { overflow: auto; overflow-y: hidden; }
    #doublescroll p { margin: 0; padding: 1em; white-space: nowrap; }
    div#main p {margin-top:0px;}

</style>
<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader{
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->

</head>
<script language="javascript" type="text/javascript">
function userStatusCheck(target){
	var targetElement = target;
	var ids=targetElement.value;
	if(targetElement.checked){
  		var userCheckStatus = document.forms['haulingMgtForm'].elements['userCheck'].value;
  		if(userCheckStatus == ''){
  			document.forms['haulingMgtForm'].elements['userCheck'].value = ids;
  		}else{
  			var userCheckStatus=document.forms['haulingMgtForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
  			document.forms['haulingMgtForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
  		}
	}if(targetElement.checked==false){
 		var userCheckStatus = document.forms['haulingMgtForm'].elements['userCheck'].value;
 		var userCheckStatus=document.forms['haulingMgtForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
 		document.forms['haulingMgtForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
 		}
	}
function getUserStatus(target){
	var tripIds=target.value;
	var agree=confirm('Unchecking will remove the order from the selected Trip. Please select OK to process');	
	if(agree){		
	    var url="clearTripStatus.html?ajax=1&decorator=simple&popup=true&tripIds=" + encodeURI(tripIds);
		http22.open("GET", url, true);
	    http22.onreadystatechange = handleHttpResponse22;
	    http22.send(null);
		}else{
			var tripNumber=document.forms['haulingMgtForm'].elements['tempTripNumber'].value;
			var position=document.forms['haulingMgtForm'].elements['tempPosition'].value;
		findTripOrders(tripNumber,position,'');
		}		
		}

function handleHttpResponse22(){
	 if (http22.readyState == 4){
	       var result= http22.responseText
	       document.forms['haulingMgtForm'].action = 'haulingMgtList.html';
	       document.forms['haulingMgtForm'].submit();        
	 }
	}
function openHailingListPopoup(){
//validateFromZipCode();
//validateToZipCode();
//alert('open');
setTimeout("showHide('block')",20);
var selectedDriverAgency = "";
var selDriverAgency="";
var selObj = document.getElementById('driverAgency');
var i;
var count = 0;
for (i=0; i<selObj.options.length; i++) {
	 if (selObj.options[i].selected) {
		 selDriverAgency = selDriverAgency+","+selObj.options[i].value;
		 selectedDriverAgency = selectedDriverAgency +",'"+ selObj.options[i].value+"'";
	    count++;
	  }
	}
if(selDriverAgency!=''){
selDriverAgency = selDriverAgency.trim();
document.forms['haulingMgtForm'].elements['selDriverAgency'].value =  selDriverAgency.substring(1,selDriverAgency.length);
}
if(selectedDriverAgency!=''){
selectedDriverAgency = selectedDriverAgency.trim();
document.forms['haulingMgtForm'].elements['selectedDriverAgency'].value = selectedDriverAgency.substring(1,selectedDriverAgency.length);
}
//window.location.href="haulingMgtList.html?fromDate="+fromDate+"&toDate="+toDate+"&filterdDrivers="+filterdDrivers+"&opsHubList="+opsHubList+"&driverAgency="+selectedArray+"&selfHaul="+selfHaul+"&toArea="+toArea+"&fromArea="+fromArea;
//document.forms[0].action = 'haulingMgtList.html';
//document.forms[0].submit();

var selectedJob = "";
var selJobTypes="";
var selObj = document.getElementById('jobTypes');

var i;
var count = 0;
for (i=0; i<selObj.options.length; i++) {
	 if (selObj.options[i].selected) {
		 selJobTypes = selJobTypes+","+selObj.options[i].value;
		 selectedJob = selectedJob +",'"+ selObj.options[i].value+"'";
	    count++;
	  }
	}
selJobTypes = selJobTypes.trim();
selectedJob = selectedJob.trim();
document.forms['haulingMgtForm'].elements['selJobTypes'].value =  selJobTypes.substring(1,selJobTypes.length);
document.forms['haulingMgtForm'].elements['selectedJob'].value = selectedJob.substring(1,selectedJob.length);
//window.location.href="haulingMgtList.html?fromDate="+fromDate+"&toDate="+toDate+"&filterdDrivers="+filterdDrivers+"&opsHubList="+opsHubList+"&driverAgency="+selectedArray+"&selfHaul="+selfHaul+"&toArea="+toArea+"&fromArea="+fromArea;
document.forms['haulingMgtForm'].action = 'haulingMgtList.html';
document.forms['haulingMgtForm'].submit();
showHide("none");
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}
function openURL(sURL) {
	var newWindow = window.open(sURL);
} 

function clearField()
{  		
	document.forms['haulingMgtForm'].elements['driverAgency'].value = "";
	document.forms['haulingMgtForm'].elements['fromZip'].value = "";
    document.forms['haulingMgtForm'].elements['toZip'].value ="";
    document.forms['haulingMgtForm'].elements['fromRadius'].value = "";
    document.forms['haulingMgtForm'].elements['toRadius'].value ="";
    document.forms['haulingMgtForm'].elements['filterdDrivers'].value ="";
    document.forms['haulingMgtForm'].elements['opsHubList'].value = "";
    document.forms['haulingMgtForm'].elements['selfHaul'].value ="All";
    document.forms['haulingMgtForm'].elements['fromArea'].value ="";
    document.forms['haulingMgtForm'].elements['toArea'].value = "";		
    document.forms['haulingMgtForm'].elements['jobTypes'].value = "";	
    document.forms['haulingMgtForm'].elements['jobStatus'].value = "All";	
    document.forms['haulingMgtForm'].elements['filterdTripNumber'].value = "";
    document.forms['haulingMgtForm'].elements['filterdRegumber'].value = "";
    		
}
function onlyNumeric(targetElement)
{   
var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid pin number.");
        targetElement.value="";
        return false;
        }
    }
    return true;
}

function createTrip()
    {
    var tripIds=document.forms['haulingMgtForm'].elements['userCheck'].value;
    if(tripIds==''){
        alert('Please select the records.');
    }else{
    var url="updateTripStatus.html?ajax=1&decorator=simple&popup=true&tripIds=" + encodeURI(tripIds);
	http22.open("GET", url, true);
    http22.onreadystatechange = handleHttpResponse2;
    http22.send(null);
	}
    }
function handleHttpResponse2(){
 if (http22.readyState == 4){
       var result= http22.responseText
       document.forms['haulingMgtForm'].action = 'haulingMgtList.html';
       document.forms['haulingMgtForm'].submit();          
 }
}
var http22 = getHTTPObject22();
function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function findTripOrders(tripNumber,position,weight){
	document.forms['haulingMgtForm'].elements['tempTripNumber'].value=tripNumber;
	document.forms['haulingMgtForm'].elements['tempPosition'].value=position;
	var condition='';
	if(weight != '' && weight != undefined ){
		condition = "&estWgt="+weight;
	}
	var url="findTripOrdersList.html?ajax=1&decorator=simple&popup=true&tripNumber=" + encodeURI(tripNumber)+condition;
	ajax_showTooltip(url,position);	
	
}
function exportHaulingMgt(){
	var fromdate =document.forms['haulingMgtForm'].elements['fromdate'].value
	var toDate =document.forms['haulingMgtForm'].elements['toDate'].value
	var filterdDrivers =document.forms['haulingMgtForm'].elements['filterdDrivers'].value
	var opsHubList =document.forms['haulingMgtForm'].elements['opsHubList'].value
	var filterdTripNumber =document.forms['haulingMgtForm'].elements['filterdTripNumber'].value
	var selfHaul=document.forms['haulingMgtForm'].elements['selfHaul'].value
	var fromArea=document.forms['haulingMgtForm'].elements['fromArea'].value
	var toArea=document.forms['haulingMgtForm'].elements['toArea'].value
	var selectedDriverAgency=document.forms['haulingMgtForm'].elements['selectedDriverAgency'].value
	var selectedJob=document.forms['haulingMgtForm'].elements['selectedJob'].value
	var fromZip=document.forms['haulingMgtForm'].elements['fromZip'].value
	var toZip=document.forms['haulingMgtForm'].elements['toZip'].value
	var fromRadius=document.forms['haulingMgtForm'].elements['fromRadius'].value
	var toRadius=document.forms['haulingMgtForm'].elements['toRadius'].value
	var jobStatus=document.forms['haulingMgtForm'].elements['jobStatus'].value
	location.href='exportHaulingMgt.html?fromDate='+fromdate+'&toDate='+toDate+'&filterdDrivers='+filterdDrivers+'&opsHubList='+opsHubList+'&filterdTripNumber='+filterdTripNumber+'&selfHaul='+selfHaul+'&fromArea='+fromArea+'&selectedDriverAgency='+selectedDriverAgency+'&toArea='+toArea+'&fromZip='+fromZip+'&toZip='+toZip+'&fromRadius='+fromRadius+'&selectedJob='+selectedJob+'&toRadius='+toRadius+'&jobStatus='+jobStatus;
}
function defaultRadius(){
	var fromZip=document.forms['haulingMgtForm'].elements['fromZip'].value
	var toZip=document.forms['haulingMgtForm'].elements['toZip'].value
	var fromRadius=document.forms['haulingMgtForm'].elements['fromRadius'].value
	var toRadius=document.forms['haulingMgtForm'].elements['toRadius'].value
	if(fromZip!='' && fromRadius==''){
		document.forms['haulingMgtForm'].elements['fromRadius'].value='50';
	}
	if(toZip!=''&& toRadius==''){
	   document.forms['haulingMgtForm'].elements['toRadius'].value='50'
	}
}
function updateStatus(orderNo,element){
	var elementValue=element.value;
	if(elementValue=='NEW'||elementValue=='CANCEL'){
		//setTimeout("showHide('block')",20);
		//alert(orderNo+"^^"+element.value);
		var url="updateJobStatus.html?ajax=1&decorator=simple&popup=true&soStatus="+elementValue+"&orderNo=" + encodeURI(orderNo);
		http33.open("GET", url, true);
	    http33.onreadystatechange = handleHttpResponse3;
	    http33.send(null);
	}
}
function handleHttpResponse3(){
	 if (http33.readyState == 4){
	       var result= http33.responseText
	       document.forms['haulingMgtForm'].action = 'haulingMgtList.html';
	       document.forms['haulingMgtForm'].submit();          
	 }
	// showHide("none");
	}
	var http33 = getHTTPObject33();
	function getHTTPObject33(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}

	function findToolTipForVehicle(orderNo,position,job){	
		<configByCorp:fieldVisibility componentId="component.field.Alternative.showVehicleOnJob">
		if(job =='CBR'){
			  var url="findToolTipForVehicle.html?ajax=1&decorator=simple&popup=true&orderNo=" + encodeURI(orderNo)+"&job="+job;
			  ajax_showTooltip(url,position);
		}
		</configByCorp:fieldVisibility>
	  }
</script>

<s:form name="haulingMgtForm" id="haulingMgtForm"  action="" method="post">
<s:hidden name="from" />
<s:hidden name="selectedDriverAgency"/>
<s:hidden name="selDriverAgency"/>
<s:hidden name="selectedJob"/>
<s:hidden name="selJobTypes"/>
<s:hidden name="userCheck"/>
<s:hidden name="tempTripNumber"/>
<s:hidden name="tempPosition" />
<div id="layer1" style="width:100%">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="otabs">
	<ul>
		<li><a class="current"><span>Hauling Management Form</span></a></li>
	</ul>
	</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
	<table  border="0" class="detailTabLabel" >
	<tbody>	
	   <tr>
		    <td class="listwhitetext" align="right" width=""><b>Target&nbsp;Loading</b>&nbsp;&nbsp;&nbsp;From&nbsp;Date</td>
			<s:text id="fromDateDateFormattedValue" name="${FormDateValue}"> <s:param name="value"  value="${fromdateF}"/></s:text>
			<td width=""><s:textfield cssClass="input-text" id="fromdate" name="fromDate" value="${fromdateF}"  size="14" maxlength="11" readonly="true" cssStyle="width:73px;"/> <img id="fromdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							
			<td class="listwhitetext" align="left" style="padding-left:114px;" colspan="6">To&nbsp;Date
			<s:text id="toDateDateFormattedValue" name="${FormDateValue}"> <s:param name="value"  value="${todateF}"/></s:text>
			<s:textfield cssClass="input-text" id="todate" name="toDate" value="${todateF}"  size="14" maxlength="11"  readonly="true" cssStyle="width:112px;"/> <img id="todate_trigger" style="vertical-align:bottom;padding-right:11px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
		   </td>
		</tr>
		
		<tr>
			<td class="listwhitetext" align="right" width="">Job&nbsp;Status&nbsp;</td>	
		  <td><s:select cssClass="list-menu" name ="jobStatus" list="{'All','ACTIVE','HOLD','DWLD'}"  cssStyle="width:100px"/>
		  <td colspan="4"  rowspan="3" class="listwhitetext" align="left">
		<table style="margin:0px;padding:0px;">
		<tr>
			<td class="listwhitetext" align="right" width="148px">Driver&nbsp;Agency<font size="2" color="red">*</font></td>
			<td><s:select cssClass="list-menu" id="driverAgency" name ="driverAgency" list="%{driverAgencyList}" value="%{selectedDriverTypeList}" multiple="true" headerKey="" headerValue="" cssStyle="width:136px;height:100px"/></td>
		</tr>
		</table>
		</td>
		<td align="left" class="listwhitetext" rowspan="3" colspan="4" style="padding-left:20px">
		<table class="detailTabLabel" border="0" cellspacing="3" style="margin:0px;padding:0px;">
		 <tr>
		 <td class="listwhitetext" align="left">From&nbsp;Area&nbsp;</td>
     	 <td><s:select cssClass="list-menu" cssStyle="width:92px;" name ="fromArea" list="%{fromAreaList}" headerKey="" headerValue=""/></td>
		 <td width=""></td>
		 <td class="listwhitetext" align="right">To&nbsp;Area&nbsp;</td>
     	<td><s:select cssClass="list-menu" cssStyle="width:92px;" name ="toArea" list="%{toAreaList}" headerKey="" headerValue=""/></td>
		 </tr>
		 <tr><td align="left" class="listwhitetext"></td></tr>
		 <tr>
		 <td class="listwhitetext" align="right">From&nbsp;Zip&nbsp;</td>
     	 <td><s:textfield name="fromZip"  cssClass="input-text" cssStyle="width:88px;" size="10" maxlength="10" onblur="defaultRadius();" onchange="return onlyNumeric(this);"/></td>
     	 <td width="10px"></td>
		 <td class="listwhitetext" align="right">To&nbsp;Zip&nbsp;</td>
     	 <td><s:textfield name="toZip"  cssClass="input-text" cssStyle="width:88px;" size="10" maxlength="10" onblur="defaultRadius();" onchange="return onlyNumeric(this);" /></td>
		 </tr>
		 	 <tr><td align="left" class="listwhitetext"></td></tr>
		 <tr>
		 <td class="listwhitetext" align="right">Radius&nbsp;</td>
     	 <td class="listwhitetext" align="left"><s:select cssClass="list-menu" cssStyle="width:92px;" name ="fromRadius" list="%{radiusList}" headerKey="" headerValue="" />Miles</td>     	
		  <td width=""></td>
		 <td class="listwhitetext" align="right">Radius&nbsp;</td>
     	 <td class="listwhitetext" align="left"><s:select cssClass="list-menu" cssStyle="width:92px;" name ="toRadius" list="%{radiusList}" headerKey="" headerValue="" />Miles</td>
		 
		 </tr>
		 </table>
		</td>
		</tr>
		<tr>
		<td class="listwhitetext" align="right">Job Type<font size="2" color="red">*</font></td>	  	  
	  	<td><s:select cssClass="list-menu" cssStyle="width:100px;height:50px;overflow:auto;"  id="jobTypes" name="jobTypes" list="%{jobTypes}" value="%{selectedJobTypesList}" multiple="true" headerKey="" headerValue="" /></td>		
	
		 </tr>
		 <tr>
		<td class="listwhitetext" align="right">Ops&nbsp;Hub</td>
		<td align="left"><s:select cssClass="list-menu" name ="opsHubList" list="%{opshub}" headerKey="" headerValue="" cssStyle="width:100px"/></td>
	
		 </tr>
		 <tr>
		 <td class="listwhitetext" align="right">&nbsp;Reg&nbsp;#</td>
		 <td align="left"><s:textfield name="filterdRegumber"  cssClass="input-text" onchange="valid(this,'special');" cssStyle="width:97px;" size="10" maxlength="20" /></td>		 
		 <td class="listwhitetext" align="right" >Drivers</td>
		 <td colspan="3"><s:select cssClass="list-menu" name ="filterdDrivers" list="{'','Unassigned drivers','Our drivers','All'}" cssStyle="width:137px;"/></td>
		 
		 <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;padding-left:25px;">
		 <b>* Use Control + mouse to multiple selections</b>
		 </td>
		 </tr>
		 <tr>
		 <td class="listwhitetext" align="right">&nbsp;&nbsp;Trip&nbsp;No.</td>
		 <td><s:textfield name="filterdTripNumber"  cssClass="input-text" cssStyle="width:97px;" size="10" maxlength="10" onchange="valid(this,'special');"/></td>
		<td class="listwhitetext" align="right" style="padding-left:107px;!padding-left:17px;">Self&nbsp;Haul</td>
		 <td><s:select cssClass="list-menu" name ="selfHaul" list="{'All','S','N','R'}" headerKey=" " headerValue=" " cssStyle="width:137px;"/></td>
		 </tr>
		 <tr>		
		
		 <td colspan="3" rowspan="3" valign="top">
		 <table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px">
		 <tr>
		<td  colspan="3" align="left" style="padding:10px 0px 0px 80px;">
     <input type="button" name="GHS" style="height:25px;width:150px;" value="Generate Hauling List" class="cssbuttonB" onclick="openHailingListPopoup();" />
     <input type="button" name="Clear" style="height:25px;width:60px;" value="Clear" class="cssbuttonB" onclick="clearField();" />
     </td>
		 </tr>
		 </table>		 
		 </td>
		 <td colspan="3" rowspan="3"><input type="button" name="Create Trip" style="height:25px;width:80px;margin-top:6px;" value="Create Trip" class="cssbuttonB" onclick="createTrip();" /></td>
		 </tr>
	</tbody></table>
 </div>
	<div class="bottom-header" style="margin-top:50px;"><span></span></div> 
</div>
</div>

</div>
</s:form>

<s:form id="haulingMgtList"> 
<c:if test="${haulingMgt!=''}"> 
<div id="layer1" style="width: 100%;margin: 0px;">

<s:set name="haulingMgt" value="haulingMgt" scope="request"/> 

<!-- <div id="doublescroll"> -->
<div style="overflow-x:scroll;">
<p>
<display:table name="haulingMgt" class="table" requestURI="" id="haulingMgtList" export="false" pagesize="100" style="width:99%;margin-top: 5px;!margin-top:2px;"> 
  	<display:column title="Select" style="width:5px;">
<c:if test="${haulingMgtList.driver=='' && haulingMgtList.tripNumber==''}">
<input type="checkbox" id="checkboxId" name="DD" value="${haulingMgtList.id}" onclick="userStatusCheck(this)"/>
</c:if>
<c:if test="${(haulingMgtList.tripNumber!='') || (haulingMgtList.driver!='')}">
<input type="checkbox" id="checkboxId" name="DD" disabled="disabled"/>
</c:if>
</display:column>
<c:if test="${haulingMgtList.jobStatus!='DWNLD' && haulingMgtList.jobStatus!='HOLD' }">
<display:column property="jobStatus"  title="Status" style="width:50px" />
</c:if>
<c:if test="${haulingMgtList.jobStatus=='DWNLD' || haulingMgtList.jobStatus=='HOLD'}">
<display:column   title="Status"  style="width:80px" >
<s:select cssClass="list-menu"  list="{'${haulingMgtList.jobStatus}','NEW','CANCEL'}" onchange="return updateStatus('${haulingMgtList.orderNo}',this)" cssStyle="width:80px"/>
</display:column>
</c:if>
<display:column sortable="true" title="Trip&nbsp;No" style="width:100px;" sortProperty="tripNumber">
<div style="width:90px;">
<c:out value="${haulingMgtList.tripNumber}"/>
<c:if test="${haulingMgtList.tripNumber!=''}">
<img id="target" onclick ="findTripOrders('${haulingMgtList.tripNumber}',this,'${haulingMgtList.estWgt}');" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
</c:if>
</div>
</display:column>
  	
  	<display:column sortable="true" title="Hauling VanLine Code" style="width:100px" sortProperty="haulingAgentVanlineCode">
  	<c:out value="${haulingMgtList.haulingAgentVanlineCode}"/>
  	</display:column>
  	<display:column sortable="true" title="Self Haul" style="width:50px" sortProperty="selfHaul">
  	<c:out value="${haulingMgtList.selfHaul}"/>
  	</display:column>
  	<display:column sortable="true" title="Driver" style="width:70px" sortProperty="driver">
  	<c:out value="${haulingMgtList.driver}"/>
  	</display:column>
  	<display:column sortable="true" title="Shipper" style="width:100px" sortProperty="shiper">
  	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.shiper}" /></a>
  	</display:column>
  	<display:column sortable="true" title="Job" style="width:50px" sortProperty="job">
  	<div style="width:40px" onmouseover="findToolTipForVehicle('${haulingMgtList.id}',this,'${haulingMgtList.job}');" onmouseout="ajax_hideTooltip();" >
  	<c:out value="${haulingMgtList.job}"/></div>
  	</display:column>
  	<display:column  sortable="true" title="Order No" style="width:100px" sortProperty="orderNo">
  		<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.orderNo}"/></a>
  	</display:column>
  	<display:column sortable="true" title="Ship Type" style="width:40px" sortProperty="shipmentType">
	<c:out value="${haulingMgtList.shipmentType}"/>
	</display:column>
	<display:column sortable="true" title="Origin" style="width:100px" sortProperty="origin">
	<c:out value="${haulingMgtList.origin}"/>
	</display:column>
	<display:column property="fromArea" sortable="true" title="From Area" style="width:50px" />
	<display:column sortable="true" title="Origin&nbsp;Agent" style="width:100px" >
	<c:out value="${haulingMgtList.originAgentCode}"/>
	<c:if test="${not empty haulingMgtList.originVanlineCode }">
	(<c:out value="${haulingMgtList.originVanlineCode}"/>)
	</c:if>
	</display:column>
	<display:column  sortable="true" title="Origin&nbsp;Agent&nbsp;Address" style="width:100px" >
	<c:out value="${haulingMgtList.originAgentAddress}"/>
	</display:column>
	<display:column sortable="true" title="Destination" style="" sortProperty="destination">
	<c:out value="${haulingMgtList.destination}"/>
	</display:column>
	<display:column property="toArea" sortable="true" title="To&nbsp;Area" style="width:80px" />
	<display:column sortable="true" title="Destination&nbsp;Agent" style="width:100px" >
	<c:out value="${haulingMgtList.destinationAgentCode}"/>
	<c:if test="${not empty haulingMgtList.destinationVanlineCode}">
	(<c:out value="${haulingMgtList.destinationVanlineCode}"/>)
	</c:if>
	</display:column>
	<display:column sortable="true" title="Destination&nbsp;Agent&nbsp;Address" style="width:100px" >
	<c:out value="${haulingMgtList.destinationAgentAddress}"/>
	</display:column>
   <display:column sortable="true" title="Pack&nbsp;Beg" style="width:100px" format="{0,date,dd-MMM-yyyy}" sortProperty="beginPacking" >
	<c:out value="${haulingMgtList.beginPacking}"/>
	</display:column>
	<display:column sortable="true" title="Pack&nbsp;End" style="width:100px" format="{0,date,dd-MMM-yyyy}"  sortProperty="endPacking">
	<c:out value="${haulingMgtList.endPacking}"/>
	</display:column>
	<display:column sortable="true" title="Pack Count" style="width:50px" >
	<c:out value="${haulingMgtList.loadCount}"/>
	</display:column>
	<display:column sortable="true" title="Load Beg" style="width:100px"  format="{0,date,dd-MMM-yyyy}" sortProperty="loadBeg">
	<c:out value="${haulingMgtList.loadBeg}"/>
	</display:column>
	<display:column sortable="true" title="Load End" style="width:80px"  format="{0,date,dd-MMM-yyyy}" sortProperty="loadEnd">
	<c:out value="${haulingMgtList.loadEnd}"/>
	</display:column>
	<display:column  sortable="true" title="Delivery Beg" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="deliveryBeg">
	<c:out value="${haulingMgtList.deliveryBeg}"/>
	</display:column>
	<display:column sortable="true" title="Delivery End" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="deliveryEnd">
	<c:out value="${haulingMgtList.deliveryEnd}"/>
	</display:column>
	<display:column sortable="true" title="Delivery&nbsp;Act" style="width:100px" >
	<c:out value="${haulingMgtList.deliveryA}"/>
	</display:column>
	<display:column sortable="true" title="ETA" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="ETA">
	<c:out value="${haulingMgtList.ETA}"/>
	</display:column>
	<display:column sortable="true" title="Driver Paper" style="" sortProperty="driverPaper">
	<c:out value="${haulingMgtList.driverPaper}"/>
	</display:column>
	<display:column sortable="true" title="Est.Rev." style=""><div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${haulingMgtList.estimatedRevenue}" /></div>
	</display:column>
	<display:column sortable="true" title="Est.Wgt." style=""><div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${haulingMgtList.estWgt}" /></div>
	</display:column>
	<display:column property="packAuthorize" sortable="true" title="Pack&nbsp;Authorize" style="width:100px" />
	<display:column property="tarif" sortable="true" title="Tariff" style="width:100px" />
	<display:column property="shipmentRegistrationDate" sortable="true" title="Register" format="{0,date,dd-MMM-yyyy}" style="width:100px" />
	<display:column property="survey" sortable="true" title="Survey" format="{0,date,dd-MMM-yyyy}" style="width:100px" />
	<display:column sortable="true" title="Disc%" style=""><div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${haulingMgtList.discount}" /></div>
	</display:column>
	<display:column sortable="true" title="MIL%" style=""><div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${haulingMgtList.transportationDiscount}" /></div>
	</display:column>	
	<display:column sortable="true" title="Unpack" style="width:70px" >
	<c:out value="${haulingMgtList.unPack}"/>
	</display:column>
	<display:column sortable="true" title="Van&nbsp;#" style="width:70px" >
	<c:out value="${haulingMgtList.vanID}"/>
	</display:column>
	<display:column sortable="true" title="SIT-D" style="width:50px" >
	<c:out value="${haulingMgtList.sitDestinationYN}"/>
	</display:column>
	<display:column sortable="true" title="G11" style="width:30px" >
	<c:out value="${haulingMgtList.g11}"/>
	</display:column>
</display:table>
</p>
</div>
</div>
</c:if>
<c:if test="${not empty haulingMgt}">
<td> <input type="button" class="cssbutton" style="width:100px; height:25px;margin-top:5px;" value="Export to Excel" onclick="exportHaulingMgt()"/> </td>	
</c:if>
<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Work In Progress...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form> 
<script type="text/javascript">
    function DoubleScroll(element) {
        var scrollbar= document.createElement('div');
        scrollbar.appendChild(document.createElement('div'));
        scrollbar.style.overflow= 'auto';
        scrollbar.style.overflowY= 'hidden';
        scrollbar.firstChild.style.width= element.scrollWidth+'px';
        scrollbar.firstChild.style.paddingTop= '10px';
        scrollbar.firstChild.appendChild(document.createTextNode('\xA0'));
        scrollbar.onscroll= function() {
            element.scrollLeft= scrollbar.scrollLeft;
        };
        element.onscroll= function() {
            scrollbar.scrollLeft= element.scrollLeft;
        };
        element.parentNode.insertBefore(scrollbar, element);
    }

    DoubleScroll(document.getElementById('doublescroll'));
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
try{
	
	var Haul='${selfHaul}';
if( Haul=='' || Haul==null){
	document.forms['haulingMgtForm'].elements['selfHaul'].value ="All";	
}else{
	document.forms['haulingMgtForm'].elements['selfHaul'].value =Haul;	
}
}catch(e){

}

</script>

