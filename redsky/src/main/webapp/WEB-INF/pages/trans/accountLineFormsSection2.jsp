<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${accountLine.createdOn}"  pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='serviceOrder.createdOn' /></b></td>
				<s:hidden name="accountLine.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${accountLine.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='serviceOrder.createdBy' /></b></td>
				<c:if test="${not empty accountLine.id}">
					<s:hidden name="accountLine.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{accountLine.createdBy}" /></td>
				</c:if>
				<c:if test="${empty accountLine.id}">
					<s:hidden name="accountLine.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if> 
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='serviceOrder.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${accountLine.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="accountLine.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${accountLine.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='serviceOrder.updatedBy' /></b></td>
				<c:if test="${not empty accountLine.id}"> 
                <s:hidden name="accountLine.updatedBy"/> 
				<td style="width:155px ; font-size:.90em"><s:label name="updatedBy" value="%{accountLine.updatedBy}"/></td>
				 </c:if> 
				<c:if test="${empty accountLine.id}"> 
				<s:hidden name="accountLine.updatedBy" value="${pageContext.request.remoteUser}"/>
				 <td style="width:155px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				 </c:if> 
			</tr>
		</tbody>
	</table>