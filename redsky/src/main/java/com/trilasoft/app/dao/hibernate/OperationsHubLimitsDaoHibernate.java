package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.OperationsHubLimitsDao;
import com.trilasoft.app.dao.hibernate.WorkTicketDaoHibernate.WorkTicketStorageDTO;
import com.trilasoft.app.model.OperationsHubLimits;

public class OperationsHubLimitsDaoHibernate extends GenericDaoHibernate<OperationsHubLimits, Long> implements OperationsHubLimitsDao{
	
	public OperationsHubLimitsDaoHibernate() {
		super(OperationsHubLimits.class);
	}
	
	public List isExisted(String hub, String sessionCorpID){
		return this.getSession().createSQLQuery("select * from operationshublimits where hubID = '"+hub+"' AND corpID ='"+sessionCorpID+"'").list();
	}

	public List<OperationsHubLimits> getHub(String hubID) {
		return getHibernateTemplate().find("from OperationsHubLimits where hubID = '"+hubID+"'" );
	}
	
	public List findListByHub(String sessionCorpID){
		String	query="select distinct o.hubID,o.id,o.dailyOpHubLimit,o.dailyOpHubLimitPC,o.minServiceDayHub,o.dailyMaxEstWt,o.dailyMaxEstVol,o.dailyCrewCapacityLimit,o.dailyCrewThreshHoldLimit from operationshublimits o,refmaster r where o.corpId='"+sessionCorpID+"' and r.flex1=o.hubID and r.parameter='HOUSE'";
		List list=getSession().createSQLQuery(query).list();
		List result= new ArrayList();
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			HubLimitListDTO HbListDTO = new HubLimitListDTO();
			HbListDTO.setHubID(obj[0]);
			HbListDTO.setId(obj[1]);
			HbListDTO.setDailyOpHubLimit(obj[2]);
			HbListDTO.setDailyOpHubLimitPC(obj[3]);
			HbListDTO.setMinServiceDayHub(obj[4]);
			HbListDTO.setDailyMaxEstWt(obj[5]);
			HbListDTO.setDailyMaxEstVol(obj[6]);
			HbListDTO.setDailyCrewLimit(obj[7]);
			HbListDTO.setDailyCrewTholdLimit(obj[8]);
			
			result.add(HbListDTO);
		}
		
		return result;
	}
	public class HubLimitListDTO{
		 private Object hubID;
		 private Object dailyOpHubLimit;
		 private Object dailyOpHubLimitPC;
		 private Object minServiceDayHub;
		 private Object dailyMaxEstWt;
		 private Object dailyMaxEstVol;
		 private Object id;
		 private Object dailyCrewLimit;
		 private Object dailyCrewTholdLimit;
		public Object getHubID() {
			return hubID;
		}
		public void setHubID(Object hubID) {
			this.hubID = hubID;
		}
		public Object getDailyOpHubLimit() {
			return dailyOpHubLimit;
		}
		public void setDailyOpHubLimit(Object dailyOpHubLimit) {
			this.dailyOpHubLimit = dailyOpHubLimit;
		}
		public Object getDailyOpHubLimitPC() {
			return dailyOpHubLimitPC;
		}
		public void setDailyOpHubLimitPC(Object dailyOpHubLimitPC) {
			this.dailyOpHubLimitPC = dailyOpHubLimitPC;
		}
		public Object getMinServiceDayHub() {
			return minServiceDayHub;
		}
		public void setMinServiceDayHub(Object minServiceDayHub) {
			this.minServiceDayHub = minServiceDayHub;
		}
		public Object getDailyMaxEstWt() {
			return dailyMaxEstWt;
		}
		public void setDailyMaxEstWt(Object dailyMaxEstWt) {
			this.dailyMaxEstWt = dailyMaxEstWt;
		}
		public Object getDailyMaxEstVol() {
			return dailyMaxEstVol;
		}
		public void setDailyMaxEstVol(Object dailyMaxEstVol) {
			this.dailyMaxEstVol = dailyMaxEstVol;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getDailyCrewLimit() {
			return dailyCrewLimit;
		}
		public void setDailyCrewLimit(Object dailyCrewLimit) {
			this.dailyCrewLimit = dailyCrewLimit;
		}
		public Object getDailyCrewTholdLimit() {
			return dailyCrewTholdLimit;
		}
		public void setDailyCrewTholdLimit(Object dailyCrewTholdLimit) {
			this.dailyCrewTholdLimit = dailyCrewTholdLimit;
		}
	}
	
	public List getAllRecord(String sessionCorpID) {	
		String query= "select distinct hubID as 'hub',flex1,'',a.id as 'id',dailyOpHubLimit,dailyOpHubLimitPC,minServiceDayHub,dailyMaxEstWt,dailyMaxEstVol,dailyCrewCapacityLimit,dailyCrewThreshHoldLimit " +
		"from operationshublimits a, refmaster b " +
		"where a.corpid='"+sessionCorpID+"' and b.corpid='"+sessionCorpID+"' and b.flex1=a.hubID and parameter='HOUSE' " +
		"union " +
		"select distinct flex1 as 'hub','',b.code,a.id as 'id', dailyOpHubLimit,dailyOpHubLimitPC,minServiceDayHub,dailyMaxEstWt,dailyMaxEstVol,dailyCrewCapacityLimit,dailyCrewThreshHoldLimit " +
		"from operationshublimits a, refmaster b " +
		"where a.corpid='"+sessionCorpID+"' and b.corpid='"+sessionCorpID+"' and b.code=a.hubID and parameter='HOUSE' order by 1,3"; 
		List list= getSession().createSQLQuery(query).list();
		List result= new ArrayList();
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			HubLimitDTO HbDTO = new HubLimitDTO();
			HbDTO.setHub(obj[0]);
			HbDTO.setFlex1(obj[1]);
			HbDTO.setCode(obj[2]);
			HbDTO.setId(obj[3]);
			HbDTO.setDailyOpHubLimit(obj[4]);
			HbDTO.setDailyOpHubLimitPC(obj[5]);
			HbDTO.setMinServiceDayHub(obj[6]);
			HbDTO.setDailyMaxEstWt(obj[7]);
			HbDTO.setDailyMaxEstVol(obj[8]);
			HbDTO.setDailyCrewLimit(obj[9]);
			HbDTO.setDailyCrewTholdLimit(obj[10]);
			
			result.add(HbDTO);
		}
		
		return result;
	}
	public class HubLimitDTO{
		 private Object hub;
		 private Object flex1;
		 private Object dailyOpHubLimit;
		 private Object dailyOpHubLimitPC;
		 private Object minServiceDayHub;
		 private Object dailyMaxEstWt;
		 private Object dailyMaxEstVol;
		 private Object code;
		 private Object id;
		 private Object dailyCrewLimit;
		 private Object dailyCrewTholdLimit;
		 
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		
		public Object getCode() {
			return code;
		}
		public void setCode(Object code) {
			this.code = code;
		}
		public Object getDailyMaxEstWt() {
			return dailyMaxEstWt;
		}
		public void setDailyMaxEstWt(Object dailyMaxEstWt) {
			this.dailyMaxEstWt = dailyMaxEstWt;
		}
		public Object getDailyMaxEstVol() {
			return dailyMaxEstVol;
		}
		public void setDailyMaxEstVol(Object dailyMaxEstVol) {
			this.dailyMaxEstVol = dailyMaxEstVol;
		}
		public Object getHub() {
			return hub;
		}
		public void setHub(Object hub) {
			this.hub = hub;
		}
		public Object getFlex1() {
			return flex1;
		}
		public void setFlex1(Object flex1) {
			this.flex1 = flex1;
		}
		public Object getDailyOpHubLimit() {
			return dailyOpHubLimit;
		}
		public void setDailyOpHubLimit(Object dailyOpHubLimit) {
			this.dailyOpHubLimit = dailyOpHubLimit;
		}
		public Object getDailyOpHubLimitPC() {
			return dailyOpHubLimitPC;
		}
		public void setDailyOpHubLimitPC(Object dailyOpHubLimitPC) {
			this.dailyOpHubLimitPC = dailyOpHubLimitPC;
		}
		public Object getMinServiceDayHub() {
			return minServiceDayHub;
		}
		public void setMinServiceDayHub(Object minServiceDayHub) {
			this.minServiceDayHub = minServiceDayHub;
		}
		public Object getDailyCrewLimit() {
			return dailyCrewLimit;
		}
		public void setDailyCrewLimit(Object dailyCrewLimit) {
			this.dailyCrewLimit = dailyCrewLimit;
		}
		public Object getDailyCrewTholdLimit() {
			return dailyCrewTholdLimit;
		}
		public void setDailyCrewTholdLimit(Object dailyCrewTholdLimit) {
			this.dailyCrewTholdLimit = dailyCrewTholdLimit;
		}
	}		
	
	
	public void updateDailyOpHubLimitParent(Integer tempHubLimit,Integer tempHubLimitOld,Integer tempHubLimitNew,String sessionCorpID,String hubID){
		String query="";
		String query1="";
		String tempParentName="";
		String tempOpHubLimitOldValue="";
		int  totalOpHubLimit=0;
		List dailyOpHubLimitOldValue=null;
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			dailyOpHubLimitOldValue  = this.getSession().createSQLQuery("select dailyOpHubLimit from operationshublimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' ").list();
		}
		if(dailyOpHubLimitOldValue!=null && !dailyOpHubLimitOldValue.isEmpty() && dailyOpHubLimitOldValue.get(0)!=null) {
			 tempOpHubLimitOldValue = dailyOpHubLimitOldValue.get(0).toString().trim();
			 if(tempHubLimitOld < tempHubLimitNew){
				 totalOpHubLimit=Integer.parseInt(tempOpHubLimitOldValue)+(tempHubLimit);
			 }else{
				 totalOpHubLimit=Integer.parseInt(tempOpHubLimitOldValue)-(tempHubLimit);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationshublimits set dailyOpHubLimit ='"+totalOpHubLimit+"' where hubID ='"+tempParentName+"' and corpid='"+sessionCorpID+"' ";			
					this.getSession().createSQLQuery(query).executeUpdate();
        
        		query1="update operationshublimits set dailyOpHubLimit ='"+totalOpHubLimit+"' where hubID ='0' and corpid='"+sessionCorpID+"' ";			
        			this.getSession().createSQLQuery(query1).executeUpdate();
        }else{
        		query1="update operationshublimits set dailyOpHubLimit ='"+tempHubLimitNew+"' where hubID ='0' and corpid='"+sessionCorpID+"' ";			
        			this.getSession().createSQLQuery(query1).executeUpdate();
        }
	}
	
	public void updateDailyMaxEstWtParent(BigDecimal tempdailyMaxEstWt,BigDecimal dailyMaxEstWtOld,BigDecimal dailyMaxEstWtNew, String sessionCorpID,String hubID){
		String query="";
		String query1="";
		String tempParentName="";
		List dailyMaxEstWtOldValue=null;
		String tempDailyMaxEstWtOldValue="";
		BigDecimal  totalDailyMaxEstWt=new BigDecimal(0.00);
		int res;
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyMaxEstWtOldValue  = this.getSession().createSQLQuery("select dailyMaxEstWt from operationshublimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' ").list();
		}
		if(dailyMaxEstWtOldValue!=null && !dailyMaxEstWtOldValue.isEmpty() && dailyMaxEstWtOldValue.get(0)!=null){
			 tempDailyMaxEstWtOldValue = dailyMaxEstWtOldValue.get(0).toString().trim();
			 res=dailyMaxEstWtOld.compareTo( dailyMaxEstWtNew);
			 BigDecimal bd= new BigDecimal (tempDailyMaxEstWtOldValue); 
			 if(res== -1){
				 totalDailyMaxEstWt=(bd).add(tempdailyMaxEstWt);
			 }else if( res == 1 ){
				 totalDailyMaxEstWt=(bd).subtract(tempdailyMaxEstWt);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationshublimits set dailyMaxEstWt ='"+totalDailyMaxEstWt+"' where hubID ='"+tempParentName+"' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query).executeUpdate();
					
				query1="update operationshublimits set dailyMaxEstWt ='"+totalDailyMaxEstWt+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}else{
				query1="update operationshublimits set dailyMaxEstWt ='"+dailyMaxEstWtNew+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}
	}
	
	public void updateDailyMaxEstVolParent(BigDecimal tempdailyMaxEstVol,BigDecimal dailyMaxEstVolOld,BigDecimal dailyMaxEstVolNew, String sessionCorpID,String hubID){
		String query="";
		String query1="";
		String tempParentName="";
		List dailyMaxEstVolOldValue=null;
		String tempDailyMaxEstVolOldValue="";
		BigDecimal  totalDailyMaxEstVol=new BigDecimal(0.00);
		int res;
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyMaxEstVolOldValue  = this.getSession().createSQLQuery("select dailyMaxEstVol from operationshublimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' ").list();
		}
		if(dailyMaxEstVolOldValue!=null && !dailyMaxEstVolOldValue.isEmpty() && dailyMaxEstVolOldValue.get(0)!=null){
			tempDailyMaxEstVolOldValue = dailyMaxEstVolOldValue.get(0).toString().trim();
			 res=dailyMaxEstVolOld.compareTo( dailyMaxEstVolNew);
			 BigDecimal bd= new BigDecimal (tempDailyMaxEstVolOldValue); 
			 if(res== -1){
				 totalDailyMaxEstVol=(bd).add(tempdailyMaxEstVol);
			 }else if( res == 1 ){
				 totalDailyMaxEstVol=(bd).subtract(tempdailyMaxEstVol);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationshublimits set dailyMaxEstVol ='"+totalDailyMaxEstVol+"' where hubID ='"+tempParentName+"' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query).executeUpdate();
					
				query1="update operationshublimits set dailyMaxEstVol ='"+totalDailyMaxEstVol+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}else{
				query1="update operationshublimits set dailyMaxEstVol ='"+dailyMaxEstVolNew+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}
	}
	
	public List findUnassignedParentValue(String sessionCorpID,String wareHouse){
		String tempParentName="";
		List parentHubValue=new ArrayList();
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+wareHouse+"' ").list();
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 String query="select concat(dailyOpHubLimit,'~',dailyOpHubLimitPC,'~',minServiceDayHub,'~',dailyMaxEstWt,'~',dailyMaxEstVol) from operationshublimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"'";
			 parentHubValue  = this.getSession().createSQLQuery(query).list();
		}
		return parentHubValue;
	}
	public void updateDailyDefLimit(String sessionCorpID,String dailyOpHubLimitPC){
		String query="";
			query="update operationshublimits set dailyOpHubLimitPC ='"+dailyOpHubLimitPC+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	public void updateMinServDay(String sessionCorpID,String minServiceDayHub){
		String query="";
			query="update operationshublimits set minServiceDayHub ='"+minServiceDayHub+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	public void updateCrewLimitParent(Integer tempCrewLimit,Integer tempCrewValueOld,Integer tempCrewValueNew, String sessionCorpID,String hubID){
		String query="";
		String query1="";
		String tempParentName="";
		String tempCrewLimitOldValue="";
		int  totalCrewLimit=0;
		List dailyCrewLimitValue=new ArrayList();;
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			dailyCrewLimitValue  = this.getSession().createSQLQuery("select dailyCrewCapacityLimit from operationshublimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' ").list();
		}
		if(dailyCrewLimitValue!=null && !dailyCrewLimitValue.isEmpty() && dailyCrewLimitValue.get(0)!=null) {
			tempCrewLimitOldValue = dailyCrewLimitValue.get(0).toString().trim();
			 if(tempCrewValueOld < tempCrewValueNew){
				 totalCrewLimit=Integer.parseInt(tempCrewLimitOldValue)+(tempCrewLimit);
			 }else{
				 totalCrewLimit=Integer.parseInt(tempCrewLimitOldValue)-(tempCrewLimit);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
			query="update operationshublimits set dailyCrewCapacityLimit ='"+totalCrewLimit+"' where hubID ='"+tempParentName+"' and corpid='"+sessionCorpID+"' ";			
				this.getSession().createSQLQuery(query).executeUpdate();
				query1="update operationshublimits set dailyCrewCapacityLimit ='"+totalCrewLimit+"' where hubID ='0' and corpid='"+sessionCorpID+"' ";			
    			this.getSession().createSQLQuery(query1).executeUpdate();				
		}
	}
	public void updateCrewDailyLimit(String sessionCorpID,String dailyCrewLimit){
		String query="";
			query="update operationshublimits set dailyCrewCapacityLimit ='"+dailyCrewLimit+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	public void updateCrewThreshHoldLimit(String sessionCorpID,String dailyCrewThreshHoldLimit){
		String query="";
			query="update operationshublimits set dailyCrewThreshHoldLimit ='"+dailyCrewThreshHoldLimit+"' where hubID ='0' and corpid='"+sessionCorpID+"'";
		this.getSession().createSQLQuery(query).executeUpdate();
	}
}
