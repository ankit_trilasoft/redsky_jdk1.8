package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.DsTaxServices;

public interface DsTaxServicesManager extends GenericManager<DsTaxServices, Long>{

	List getListById(Long id);

}
