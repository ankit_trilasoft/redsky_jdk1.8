<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<style>

div#content {
height:auto;
left:0;
margin:0 auto 20px 20px;
width:300px;
min-height:260px;
padding:0;
position:relative;
text-align:left;
}
</style>

</head>



<script language="JavaScript">

function sedValueToMain(){ 
var qVal= window.document.forms['dataExtractSupport'].elements['queryName'].value;
if(qVal == null || qVal ==''){
alert('Please Enter Query Name');
 return false ;
}
if(document.forms['dataExtractSupport'].elements['autoGenerator'].value=='true') {
if(document.forms['dataExtractSupport'].elements['queryScheduler'].value==""){
alert("Plese select scheduling criteria");
return false ;
}else{
if(document.forms['dataExtractSupport'].elements['queryScheduler'].value=="M"){
if(document.forms['dataExtractSupport'].elements['perMonthScheduler'].value==""){
alert("Plese select monthly scheduler");
return false ;
} 
}
if(document.forms['dataExtractSupport'].elements['queryScheduler'].value=="W"){
if(document.forms['dataExtractSupport'].elements['perWeekScheduler'].value==""){
alert("Plese select weekly scheduler");
return false ;
} 
}
}
if(document.forms['dataExtractSupport'].elements['email'].value==""){
alert('Please Enter E-mail');
return false ;
}
} 
window.opener.document.forms['reportDataExtractForm'].elements['publicPrivateFlag'].value=window.document.forms['dataExtractSupport'].elements['publicPrivateFlag'].value;
window.opener.document.forms['reportDataExtractForm'].elements['queryName'].value=window.document.forms['dataExtractSupport'].elements['queryName'].value;
window.opener.document.forms['reportDataExtractForm'].elements['queryScheduler'].value=window.document.forms['dataExtractSupport'].elements['queryScheduler'].value;
window.opener.document.forms['reportDataExtractForm'].elements['perWeekScheduler'].value=window.document.forms['dataExtractSupport'].elements['perWeekScheduler'].value;
window.opener.document.forms['reportDataExtractForm'].elements['perMonthScheduler'].value=window.document.forms['dataExtractSupport'].elements['perMonthScheduler'].value;
window.opener.document.forms['reportDataExtractForm'].elements['perQuarterScheduler'].value=window.document.forms['dataExtractSupport'].elements['perQuarterScheduler'].value;
window.opener.document.forms['reportDataExtractForm'].elements['autoGenerator'].value=window.document.forms['dataExtractSupport'].elements['autoGenerator'].value;
window.opener.document.forms['reportDataExtractForm'].elements['email'].value=window.document.forms['dataExtractSupport'].elements['email'].value;
if(window.document.forms['dataExtractSupport'].elements['autoGenerator'].value=='true'){
window.opener.document.forms['reportDataExtractForm'].elements['autoGenerator'].checked='checked';
}
window.opener.findPreviewBillingReportNew();
self.close(); 
return true;
} 

function checkEmail() {
if(document.forms['dataExtractSupport'].elements['email'].value!=''){
var emailFilter2=/^.+@.+\..{2,3}$/ 
var emailValue2=document.forms['dataExtractSupport'].elements['email'].value;
if (!(emailFilter2.test(emailValue2))) {
alert("Invalid E-mail Address! Please re-enter.")
document.forms['dataExtractSupport'].elements['email'].value='';
document.forms['dataExtractSupport'].elements['email'].select();
return false;
}else{
return true;
}
} 
} 
</script>
<script language="JavaScript">
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
    function checkQueryName()
    {
     
     var queryName = document.forms['dataExtractSupport'].elements['queryName'].value; 
     var publicPrivateFlag = document.forms['dataExtractSupport'].elements['publicPrivateFlag'].value; 
     publicPrivateFlag=publicPrivateFlag.trim(); 
     queryName=queryName.trim(); 
     if(queryName!=''){
     var url="checkQueryName.html?ajax=1&decorator=simple&popup=true&queryName="+ encodeURI(queryName)+ "&publicPrivateFlag=" +encodeURI(publicPrivateFlag); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null); 
    }
    if(queryName==''){
    return  sedValueToMain();
    }
    }
function handleHttpResponse2()
        { 
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();  
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results==0)
                {
 					return  sedValueToMain();
                 }
                 else
                 {
                     alert("Please select another query name.");
                     var queryName = document.forms['dataExtractSupport'].elements['queryName'].value=''; 
                     return false ; 
                 }
             }
        }
        
function showQueryScheduler(targetElement){
if(targetElement.checked)
     {
     
     document.forms['dataExtractSupport'].elements['autoGenerator'].value='true';
     document.getElementById("hid1").style.display="block";
     document.getElementById("emailDiv").style.display="block";
      
     }
     if(targetElement.checked==false){
     document.forms['dataExtractSupport'].elements['autoGenerator'].value='false';
     document.getElementById("hid1").style.display="none"; 
     document.getElementById("emailDiv").style.display="none";
     }
}
function showScheduler(targetElement){
var queryScheduler=document.forms['dataExtractSupport'].elements['queryScheduler'].value; 
if(queryScheduler=="D"){
}
if(queryScheduler=="M"){
document.getElementById("hid3").style.display="block"; 
document.getElementById("hid2").style.display="none";
document.getElementById("hid4").style.display="none";  
} 
else if(queryScheduler=="W"){
document.getElementById("hid2").style.display="block";
document.getElementById("hid3").style.display="none"; 
document.getElementById("hid4").style.display="none"; 
} 
else if(queryScheduler=="Q"){
document.getElementById("hid4").style.display="none";
document.getElementById("hid2").style.display="none"; 
document.getElementById("hid3").style.display="none"; 
}
else{
document.getElementById("hid2").style.display="none"; 
document.getElementById("hid3").style.display="none"; 
document.getElementById("hid4").style.display="none"; 
}
}

</script>

<s:form id="dataExtractSupport" name="dataExtractSupport" method="post"  action="extractSupport.html?&decorator=popup&popup=true" validate="true">
<div id="layer1" style="">
<table class="mainDetailTable" border="0" cellpadding="2" cellspacing="0" style="width:435px; " align="center"><tr>

<td align="left" style="" class="subcontent-tab" colspan="2">Enter Query Type and Name</td></tr>
<tr height="3px"></tr>
<tr>
<td class="listwhitetext" align="right" width="160"> Select Flag value <font color="red" size="2">*</font></td>
<td>
<s:select list="{'private','public'}" name="publicPrivateFlag" cssClass="list-menu" cssStyle="width:128px" ></s:select>
</td>
</tr>
<tr height="5px"></tr>
<tr>
  
<td class="listwhitetext" align="right">Enter Query Name<font color="red" size="2">*</font></td>
<td>
<s:textfield name="queryName" title="queryName" key="queryName" required="true" cssClass="input-text" size="38" maxlength="30" />
</td>
</tr>
<tr>
  
<td class="listwhitetext" align="right">Auto Generator Extract & Email</td>
<td>
 <input type="checkbox" name="autoGenerator" id="" value="" onclick="showQueryScheduler(this);" /> 
</td>
</tr>
<tr><td align="left" colspan="2" style="margin:0px;padding: 0px;">
<table border="0" class="detailTabLabel" style="margin:0px;padding: 0px;">
<tr>
<td width="" style="margin:0px;padding: 0px;">
<div id="emailDiv" style="margin:0px;padding: 0px;">
	  	<table class="detailTabLabel" border="0">
            <tr><td class="listwhitetext" align="right" colspan="2"><b>&nbsp;&nbsp;For multiple emails, please use comma to separate addresses</b></td></tr>
            <tr><td></td></tr>
            <tr> 
            <td class="listwhitetext" align="right" width="157px">E-Mail<font color="red" size="2">*</font></td> 
            <td><s:textfield name="email"  key="email" required="true" value="${useremail}" cssClass="input-text" size="38" onchange="checkEmail();"/>
            </td>
            
            </tr>
        </table>
   </div>
</td></tr>
<tr>
<td width="" style="margin:0px;padding: 0px;">
<div id="hid1" style="margin:0px;padding: 0px;">
	  	<table class="detailTabLabel" border="0">
            <tr>
            <td class="listwhitetext" align="right" width="157px">Scheduling Criteria<font color="red" size="2">*</font></td>
            <td>
            <s:select cssClass="list-menu" cssStyle="width:110px"  name="queryScheduler"  value="%{queryScheduler}" list="%{querySchedulerList}"  headerKey="" headerValue="" onchange="showScheduler(this);"/>
            </td>
            </tr>
        </table>
   </div>
</td></tr>

<tr><td style="margin:0px;padding: 0px;">
   <div id="hid2" style="margin:0px;padding: 0px;">
	  	<table class="detailTabLabel" border="0"> 
            <tr>
            <td class="listwhitetext" align="right" width="157px">Weekly Scheduler<font color="red" size="2">*</font></td>
            
            <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="perWeekScheduler"  value="%{perWeekScheduler}" list="%{perWeekSchedulerList}"  headerKey="" headerValue="" /></td>
            
            </tr>
        </table>
   </div>
</td></tr>

<tr><td style="margin:0px;padding: 0px;">
   <div id="hid3" style="margin:0px;padding: 0px;">
	  	<table class="detailTabLabel" border="0"> 
            <tr>
            <td class="listwhitetext" align="right" width="157px">Monthly Scheduler<font color="red" size="2">*</font></td>
            
            <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="perMonthScheduler"  value="%{perMonthScheduler}" list="%{perMonthSchedulerList}"  headerKey="" headerValue="" /></td>
            
            </tr>
        </table>
   </div>


</td></tr>
<tr><td style="margin:0px;padding: 0px;">
   <div id="hid4" style="margin:0px;padding: 0px;">
	  	<table class="detailTabLabel" border="0"> 
            <tr>
            <td class="listwhitetext" align="right" width="157px">Quarterly Scheduler</td>
            
            <td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="perQuarterScheduler"  value="%{perQuarterScheduler}" list="%{perQuarterSchedulerList}"  headerKey="" headerValue="" /></td>
            
            </tr>
        </table>
   </div>
  
</td></tr>
</table>
<tr height="5px"></tr>
<tr>
<td></td>
<td>
<input type="button" class="cssbutton" style="width:110px; height:25px" value="Save & Close " onClick="return  checkQueryName();">
</td>
<tr height="5px"></tr>
</table>
</div>
</s:form>
<script type="text/javascript">
document.getElementById("hid1").style.display="none"; 
document.getElementById("hid2").style.display="none"; 
document.getElementById("hid3").style.display="none"; 
document.getElementById("hid4").style.display="none";
document.getElementById("emailDiv").style.display="none";   
</script>  

</html>
