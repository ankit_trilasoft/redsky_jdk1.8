package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.LossDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.LossPicture;

public class LossDaoHibernate extends GenericDaoHibernate<Loss, Long> implements LossDao {
	private HibernateUtil hibernateUtil;

	public LossDaoHibernate() {
		super(Loss.class);
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public List findMaximum() {
		return getHibernateTemplate().find("select max(idNumber1) from Loss");
	}

	public List<Loss> findByShipNumber(String shipNumber) {
		return getHibernateTemplate().find("from Loss where shipNumber=?", shipNumber);
	}

	public List<Loss> findByfindClaimLossTickets(String lossNumber) {
		return getHibernateTemplate().find("from Loss where lossNumber=?", lossNumber);
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from Loss");
	}

	public List checkById(Long id) {
		return getHibernateTemplate().find("select id from Loss where id=?", id);
	}

	public List findLossByIdNumber(int idNum) {
		return getHibernateTemplate().find("from Loss where idNumber1=?", idNum);
	}

	/*public Boolean ticketExists(Long ticket,String lossNumber){
	 Boolean checkTicket;

	 List ticketList = getHibernateTemplate().find("from ClaimLossTicket where lossNumber=?",lossNumber);
	 if(ticketList.isEmpty()){
	 checkTicket = true;
	 }else{
	 getHibernateTemplate().bulkUpdate("Update ClaimLossTicket set ticket = "+ticket+" where lossNumber=?",lossNumber);
	 checkTicket = false;
	 }

	 return checkTicket;
	 }
	 public Boolean checkTicket(Long ticket,String lossNumber){
	 Boolean existsTic;
	 List ticketList = getHibernateTemplate().find("from ClaimLossTicket where lossNumber=?",lossNumber);
	 if(ticketList.isEmpty()){
	 existsTic = true;
	 }else{
	 existsTic = false;
	 }
	 return existsTic;
	 }*/
	public List<Loss> findLossList() {
		List losses = null;
		String columns = "id,idNumber1,itemDescription,lossComment,lossType,damageByAction,lossAction,paidCompensation3Party,paidCompensationCustomer";
		losses = getHibernateTemplate().find("select " + columns + " from Loss ");
		try {
			losses = hibernateUtil.convertScalarToComponent(columns, losses, Loss.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return losses;
	}

	public List findWH(Long claimNumber, String corpID) {
		return getHibernateTemplate().find("select warehouse, damageByAction from Loss where claimNumber='" + claimNumber + "' AND corpID ='"+ corpID +"'");
	}

	public List findCountLoss(Long claimNumber, String sessionCorpID) {
		return getHibernateTemplate().find("select count(*) from Loss where claimNumber='"+claimNumber+"' AND corpID ='"+sessionCorpID+"'");
	}

	public List findMaximumLoss(Long claimNumber, String sessionCorpID) {
		return getHibernateTemplate().find("select max(id) from Loss where claimNumber='"+claimNumber+"' AND corpID ='"+sessionCorpID+"'");
	}

	public List findMinimumLoss(Long claimNumber, String sessionCorpID) {
		return getHibernateTemplate().find("select min(id) from Loss where claimNumber='"+claimNumber+"' AND corpID ='"+sessionCorpID+"'");
	}

	public List goNextLossChild(Long claimNumber, Long id, String sessionCorpID) {
		return getHibernateTemplate().find("select id from  Loss where claimNumber='"+claimNumber+"' AND id >'"+id+"' and corpID='"+sessionCorpID+"' order by 1 ");
	}

	public List goPrevLossChild(Long claimNumber, Long id, String sessionCorpID) {
		return getHibernateTemplate().find("select id from  Loss where claimNumber='"+claimNumber+"' AND id <'"+id+"' and corpID='"+sessionCorpID+"' order by 1 desc");
	}

	public List goLossChild(Long claimNum, Long id, String sessionCorpID) {
		return getHibernateTemplate().find("from  Loss where claimNumber='"+claimNum+"' and id !='"+id+"' and corpID='"+sessionCorpID+"'");
	}
	public int updateLossChequeNumberCustmer(String chequeNumberCustmer,Long claimNumber, String shipNumber){
		return getHibernateTemplate().bulkUpdate("update Loss set chequeNumberCustmer='"+chequeNumberCustmer+"' where paidCompensationCustomer >0.0 and (chequeNumberCustmer is null or chequeNumberCustmer='') and claimNumber='"+claimNumber+"' and shipNumber='"+shipNumber+"'");
	}
	public int updateLossChequeNumber3Party(String chequeNumber3Party,Long claimNumber, String shipNumber){
		return getHibernateTemplate().bulkUpdate("update Loss set chequeNumber3Party='"+chequeNumber3Party+"' where paidCompensation3Party >0.0 and (chequeNumber3Party is null or chequeNumber3Party='') and claimNumber='"+claimNumber+"' and shipNumber='"+shipNumber+"'");
	}

	public List lossList(Long claimId) {
		return getHibernateTemplate().find("from Loss where claimId='"+claimId+"'");
	}

	public List findMaximumIdNumber1(String corpID) { 
		return this.getSession().createSQLQuery("select max(idNumber1) from loss where corpid= '"+corpID+"' ").list();
	}

	public List findMaximumIdNumber(Long id) {
		List list=  this.getSession().createSQLQuery("select max(idNumber) from loss where claimId='"+id+"'").list();
		return list;
	}

	public Long findRemoteLoss(String idNumber, Long id) {
		Long ID=null;
		try{
		List idList= this.getSession().createSQLQuery("select id from loss where idNumber='"+idNumber+"' and claimId="+id+"").list();
		if(idList !=null && (!(idList.isEmpty())) && idList.get(0)!=null && (!(idList.get(0).toString().equals("")))){
		ID = Long.parseLong(idList.get(0).toString());	
		}
		}catch(Exception e){
			
		}
		return ID;
	}
	public List lossForOtherCorpId(Long id,String corpId){
		List idList= this.getSession().createSQLQuery("select id,idNumber1,itemDescription,lossComment,lossType,warehouse,damageByAction,lossAction,paidCompensation3Party,chequeNumber3Party,paidCompensationCustomer,chequeNumberCustmer,itmClmsItemSeqNbr,claimNumber from loss where claimId="+id+" and corpId in ('TSFT','"+corpId+"')").list();
		Iterator it = idList.iterator();
		List newList = new ArrayList();
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			Loss p = new Loss();
			if(row[0] != null){
				p.setId(Long.parseLong(row[0]+""));
			}
			if(row[1] != null){
				p.setIdNumber1(Integer.parseInt(row[1]+""));
			}
			if(row[2] != null){
				p.setItemDescription((String)row[2]);
			}
			if(row[3] != null){
				p.setLossComment((String)row[3]);
			}
			if(row[4] != null){
				p.setLossType((String)row[4]);
			}
			if(row[5] != null){
				p.setWarehouse((String)row[5]);
			}
			if(row[6] != null){
				p.setDamageByAction((String)row[6]);
			}
			if(row[7] != null){
				p.setLossAction((String)row[7]);
			}
			if(row[8] != null){
				p.setPaidCompensation3Party(new BigDecimal(row[8]+""));
			}
			if(row[9] != null){
				p.setChequeNumber3Party((String)row[9]);
			}
			if(row[10] != null){
				p.setPaidCompensationCustomer(new BigDecimal(row[10]+""));
			}
			if(row[11] != null){
				p.setChequeNumberCustmer((String)row[11]);
			}
			if(row[12] != null){
				p.setItmClmsItemSeqNbr((String)row[12]);
			}else{
				p.setItmClmsItemSeqNbr("");
			}
			if(row[13] != null){
				p.setClaimNumber(Long.parseLong(row[13].toString()));
			}
			newList.add(p);
		}
		return newList;
	}
	public List getpertaininglossvalue(Long claimNumber, String itmClmsItem , String sessionCorpID){
		return getHibernateTemplate().find("from PertainingLoss where claimNumber='"+claimNumber+"' and itmClmsItemSeqNbr='"+itmClmsItem+"' and corpid= '"+sessionCorpID+"'");
	}
	public List<LossPicture> getLossPictureList(String lossId,String claimId){
	   return getHibernateTemplate().find("from LossPicture where claimId='"+claimId+"' and lossId='"+lossId+"' and createdBy='Claim_INT' ");	
	}
	
	public List getLossPictureData(Long lossId,String sessionCorpID){
		 return getHibernateTemplate().find("from LossPicture where lossId = '"+lossId+"' and corpID='"+sessionCorpID+"' and createdBy<>'Claim_INT' ");
		}
	
	public List getLossDocumentImageServletAction(Long id, String corpID){
		return getHibernateTemplate().find("select concat(photoUri,'~' , fileName) from LossPicture where id = '"+id+"'  and corpID='"+corpID+"' and createdBy<>'Claim_INT' ");
	} 
	
}