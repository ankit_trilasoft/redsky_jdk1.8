package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.UpdateRule;
import com.trilasoft.app.webapp.action.*;

import java.util.Map;

public interface UpdateRuleManager  extends GenericManager<UpdateRule, Long> 
{
public List getByCorpID( String sessionCorpID);
public List <DataCatalog> selectfield(String entity,String corpID);
public List <DataCatalog> selectDatefield(String corpID);
public List searchById(Long id,String  fieldToUpdate,String  validationContion,String  ruleStatus);
public String executeTestRule(UpdateRule updateRule, String sessionCorpID);	
public void changestatus(String status, Long id);
public List executeThisRule(UpdateRule updateRule, String sessionCorpID, String userName);
public <UpdateRuleResult> List getToDoResultByRuleNumber(String ruleNumber);
public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId);
public List findNumberOfResultEntered(String sessionCorpID, Long id);
public List findMaximum(String sessionCorpID);
public void updateResults(Long ruleNumber);
}
