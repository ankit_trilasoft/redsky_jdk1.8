package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.LumpSumInsuranceDao;
import com.trilasoft.app.model.LumpSumInsurance;
import com.trilasoft.app.service.LumpSumInsuranceManager;

public class LumpSumInsuranceManagerImpl extends GenericManagerImpl<LumpSumInsurance, Long> implements LumpSumInsuranceManager {
	LumpSumInsuranceDao lumpsuminsurancedao;
	public LumpSumInsuranceManagerImpl(LumpSumInsuranceDao lumpsuminsurancedao){
		super(lumpsuminsurancedao);
		this.lumpsuminsurancedao=lumpsuminsurancedao;
	}
	public List getItemList(Long sid,String sessionCorpID){
	  return lumpsuminsurancedao.getItemList(sid,sessionCorpID);
	   }
		public List getVehicle(Long sid,String sessionCorpID){
			return lumpsuminsurancedao.getVehicle(sid,sessionCorpID);
		}
		public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID,String updatedBy){
			lumpsuminsurancedao.updateInsurance( newField,fieldValue, id, sid, cid, sessionCorpID, updatedBy);
		}
		public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID){
			lumpsuminsurancedao.updateVehicle(newField,fieldValue, id, sid, vehicleIdNumber, sessionCorpID);
		}
		public String getLinkedLumpSumInsuranceData(Long id, Long sid) {
			return lumpsuminsurancedao.getLinkedLumpSumInsuranceData(id, sid);
		}
		public void deleteNetworkLumpsuminsurance(Long id, Long networkSynchedId) {
			lumpsuminsurancedao.deleteNetworkLumpsuminsurance(id, networkSynchedId); 
			
		}
		public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid) {
			lumpsuminsurancedao.updateLinkedInsurance(newField, fieldValue, networkSynchedId, sid);
			
		}
		public void updateLinkedVehicleData(String newField, String fieldValue, Long id, String idNumber) {
			lumpsuminsurancedao.updateLinkedVehicleData(newField, fieldValue, id, idNumber);
			
		} 
}
