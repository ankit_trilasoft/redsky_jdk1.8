<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <script language="javascript" type="text/javascript">
  <sec-auth:authComponent componentId="module.script.form.agentScript">
  window.onload = function() { 
	trap();
	<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm3" transIdList='${serviceOrder.shipNumber}'>
	
	</sec-auth:authScript>
	
	}
 </sec-auth:authComponent>
 
 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 

		
		//trap();
					
	}
</sec-auth:authComponent>
 
 function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		        	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		        	 

		      }
		    }
		  }
		  
		  function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}

function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script>
<head>   
    <title><fmt:message key="vehicleList.title"/></title>   
    <meta name="heading" content="<fmt:message key='vehicleList.heading'/>"/> 
    <style type="text/css">


/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>



<script>
function goToCustomerDetail(targetValue){
        document.forms['serviceForm3'].elements['id'].value = targetValue;
        document.forms['serviceForm3'].action = 'editVehicle.html?from=list';
        document.forms['serviceForm3'].submit();
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	     location.href = "updateVehicleStatus.html?id="+encodeURI(targetElement)+"&sid=${ServiceOrderID}";
     }else{
		return false;
	}
}
  

	
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    function updateVehicleDetails(id,fieldName,target,tableName){
    	showOrHide(1);
    	var fieldValue ='';
    	if(fieldName=='width' || fieldName=='height' || fieldName=='length'){
	    	if(target.value ==undefined || target.value ==''){
	    		fieldValue=0;
			}else{
				fieldValue = target.value;
			}
	    }else{
	    	fieldValue = target.value;
	    }
    	 
    	var url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
    	  http22222.open("GET", url, true); 
    	  http22222.onreadystatechange = function(){handleHttpResponse22222(id,fieldName)};
    	  http22222.send(null);	 
    }
    function handleHttpResponse22222(id,fieldName){
    	if (http22222.readyState == 4){
                var results = http22222.responseText
                results = results.trim();
                if(fieldName=='height' || fieldName=='width' || fieldName=='length'){
                	calculateVolume(id);
                }
                getVehicleDetails();
                showOrHide(0);
    	}
    }
    var http22222 = getHTTPObject();
    
   function deleteVehicleDetails(targetElement,targetElementStatus){
	   var massage="";
		if(targetElementStatus.checked==false){
			massage="Are you sure you wish to deactivate this row?"
		}else{
			massage="Are you sure you wish to activate this row?"
		}  
	   var agree=confirm(massage);
		if (agree){
			showOrHide(1);
			var vehicleStatus=false;
			if(targetElementStatus.checked==false){
				vehicleStatus=false;	
			}else{
				vehicleStatus=true;
			}
	   	var url = "deleteVehicleDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&vehicleStatus="+vehicleStatus;
	 	  http122.open("GET", url, true); 
	 	  http122.onreadystatechange = handleHttpResponse122;
	 	  http122.send(null);
		}else{
			if(targetElementStatus.checked==false){
				document.getElementById('status'+targetElement).checked=true;
				}else{
					document.getElementById('status'+targetElement).checked=false;
				}
		}
   }
   function handleHttpResponse122(){
   	if (http122.readyState == 4){
               var results = http122.responseText
               getVehicleDetails();
               showOrHide(0);
   	}
   }
   var http122 = getHTTPObject();
   
   function calculateVolume(id) {
	    var weightUnitKgs = document.getElementById('unit2'+id).value;
	    var volumeUnitCbm = document.getElementById('unit3'+id).value;
	    var width= eval (document.getElementById('width'+id).value);
		var height= eval (document.getElementById('height'+id).value);
		var length= eval (document.getElementById('length'+id).value);
		
		if(width ==undefined || width ==''){
			width=0;
		}
		if(height ==undefined || height ==''){
			height=0;
		}
		if(length ==undefined || length ==''){
			length=0;
		}
	    
		var density=0;
		var factor = 0;
		if(volumeUnitCbm == 'Inches'){
			if(weightUnitKgs == 'Cft'){
				factor = 0.0005787;
			}else{
				factor = 0.0000164;
			}
		}
		if(volumeUnitCbm == 'Ft'){
			if(weightUnitKgs == 'Cft'){
				factor = 1;
			}else{
				factor = 0.02834;
			}
		}
		if(volumeUnitCbm == 'Cm'){
			if(weightUnitKgs == 'Cft'){
				factor = 0.0000353;
			}else{
				factor = 0.000001;
			}
		}
		if(volumeUnitCbm == 'Mtr'){
			if(weightUnitKgs == 'Cft'){
				factor = 35.3147;
			}else{
				factor = 1;
			}
		}
		
		var density = width*length*height;
		var aa = Math.round(density*factor*1000)/1000;
			document.getElementById('volume'+id).value = aa;
			var volume = document.getElementById('volume'+id).value;
			updateContainerVolumn11(id,'volume',volume,'Vehicle'); 
	 }
   function updateContainerVolumn11(id,fieldName,target,tableName){	
		var fieldValue ="";		
		fieldValue = target.value;		
		if(fieldValue==undefined){
			fieldValue = target;
		}
		var url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
		  httpVolumn11.open("GET", url, true); 
		  httpVolumn11.onreadystatechange = function(){handleHttpResponseVolumn11(id,fieldName)};
		  httpVolumn11.send(null);	 
	}
	function handleHttpResponseVolumn11(id,fieldName){
		if (httpVolumn11.readyState == 4){		
	            var results = httpVolumn11.responseText
	            results = results.trim();
		}
	}
	var httpVolumn11 = getHTTPObject();
    
</script>     
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<c:set var="fieldV"  value="<%=request.getParameter("field") %>"/>
<s:hidden name="fieldV" id="fieldV" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1V" id="field1V" value="<%=request.getParameter("field1") %>" />
<c:set var="field1V" value="<%=request.getParameter("field1V") %>"/>
<c:set var="buttons">  
<c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
 
     <input type="button" class="cssbutton1" onclick="window.open('editVehicleAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=550,width=900,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Add" style="width:60px; height:25px;margin-bottom:5px;" />   
         
     
</c:set>
  <s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
  <div id="Layer3" style="width:100%;">


<s:set name="vehicles" value="vehicles" scope="request"/>   
<display:table name="vehicles" class="table" requestURI="" id="vehicleList" export="false"  defaultsort="1" style="width:100%; margin-left:5px;margin:2px 0 10px;">   
    <display:column  titleKey="vehicle.idNumber" style="width:20px;" >
   <sec-auth:authComponent componentId="module.link.vehicle.id">
     <a href="javascript:openWindow('editVehicleAjax.html?id=${vehicleList.id}&decorator=popup&popup=true',900,550)" style="cursor:pointer">  
   	<c:out value="${vehicleList.idNumber}"/></a>
    </sec-auth:authComponent>
    </display:column>
    <display:column headerClass="containeralign" style="width:70px;text-align:right;padding-right: 0.3em;"   titleKey="vehicle.year">
    	<input type="text" class="input-text" style="text-align:right;width:65px" maxlength="4" name="vehicle.year" id="year${vehicleList.id}" onchange="updateVehicleDetails('${vehicleList.id}','year',this,'Vehicle');" value="<c:out value="${vehicleList.year}"/>"
    </display:column>
    <display:column  titleKey="container.containerNumber" style="text-align:left;width:50px;">
    	<select name ="vehicle.cntnrNumber" id="cntnrNumber${vehicleList.id}" style="width:120px" onchange="updateVehicleDetails('${vehicleList.id}','cntnrNumber',this,'Vehicle');" class="list-menu">
						<option value="<c:out value='' />">
							<c:out value=""></c:out>
							</option> 		      
				      <c:forEach var="chrms" items="${containerNumberListVehicle}" varStatus="loopStatus">
				           <c:choose>
			                        <c:when test="${chrms == vehicleList.cntnrNumber}">
			                        <c:set var="selectedInd" value=" selected"></c:set>
			                        </c:when>
			                    <c:otherwise>
			                        <c:set var="selectedInd" value=""></c:set>
			                     </c:otherwise>
		                    </c:choose>
				      		<option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
				                    <c:out value="${chrms}"></c:out>
				             </option>
					</c:forEach>		      
			</select> 
    	
    </display:column>
    <display:column  titleKey="vehicle.make" style="width:50px;" >
    	<input type="text" class="input-text" style="text-align:left;width:60px" maxlength="10" name="vehicle.make" id="make${vehicleList.id}" onchange="updateVehicleDetails('${vehicleList.id}','make',this,'Vehicle');" value="<c:out value="${vehicleList.make}"/>"
    </display:column>
    <display:column  titleKey="vehicle.model" style="width:50px;">
    	<input type="text" class="input-text" style="text-align:left;width:60px" maxlength="20" name="vehicle.model" id="model${vehicleList.id}" onchange="updateVehicleDetails('${vehicleList.id}','model',this,'Vehicle');" value="<c:out value="${vehicleList.model}"/>"
    </display:column>

    <display:column   headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;" titleKey="vehicle.length">   
    	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="vehicle.length" id="length${vehicleList.id}" onchange="onlyFloat(this);updateVehicleDetails('${vehicleList.id}','length',this,'Vehicle');" value="<c:out value="${vehicleList.length}"/>"
    </display:column>
    <display:column   headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;"  titleKey="vehicle.width">
    	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="vehicle.width" id="width${vehicleList.id}" onchange="onlyFloat(this);updateVehicleDetails('${vehicleList.id}','width',this,'Vehicle');" value="<c:out value="${vehicleList.width}"/>"
    </display:column>
    	<display:column headerClass="containeralign"  title="Height" style="width:70px;text-align: right;padding-right: 0.3em;" >
		<input type="text" class="input-text" style="text-align:right;width:90px" name="vehicle.height" id="height${vehicleList.id}" onchange="onlyNumeric(this);updateVehicleDetails('${vehicleList.id}','height',this,'Vehicle');" value="<c:out value="${vehicleList.height}"/>"
	</display:column>
    <display:column   headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;" titleKey="vehicle.weight">
    	<input type="text" class="input-text" style="text-align:right;width:90px" maxlength="10" name="vehicle.weight" id="weight${vehicleList.id}" onchange="onlyFloat(this);updateVehicleDetails('${vehicleList.id}','weight',this,'Vehicle');" value="<c:out value="${vehicleList.weight}"/>"
    </display:column>
        <display:column  headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;" titleKey="vehicle.volume">
     	<input type="text" class="input-text" style="text-align:right;width:100px" maxlength="10" name="vehicle.volume" id="volume${vehicleList.id}" onchange="onlyFloat(this);updateVehicleDetails('${vehicleList.id}','volume',this,'Vehicle');" value="<c:out value="${vehicleList.volume}"/>"
     </display:column>
    <display:column headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;"  titleKey="vehicle.doors">
    	<input type="text" class="input-text" style="text-align:right;width:50px" maxlength="3" name="vehicle.doors" id="doors${vehicleList.id}" onchange="onlyNumeric(this);updateVehicleDetails('${vehicleList.id}','doors',this,'Vehicle');" value="<c:out value="${vehicleList.doors}"/>"
    </display:column>
    <display:column headerClass="containeralign" style="text-align: right;width:50px;padding-right: 0.3em;"   titleKey="vehicle.cylinders">
    	<input type="text" class="input-text" style="text-align:right;width:50px" maxlength="3" name="vehicle.cylinders" id="cylinders${vehicleList.id}" onchange="onlyNumeric(this);updateVehicleDetails('${vehicleList.id}','cylinders',this,'Vehicle');" value="<c:out value="${vehicleList.cylinders}"/>"
    </display:column>
         <display:column headerClass="hidden"  title="Unit2" style="width:70px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:100px" name="vehicle.unit2" id="unit2${vehicleList.id}" value="<c:out value="${vehicleList.unit2}"/>"
	</display:column>
	 <display:column headerClass="hidden"  title="Unit3" style="width:70px" class="hidden">
		<input type="hidden" class="input-text" style="text-align:right;width:100px" name="vehicle.unit3" id="unit3${vehicleList.id}" value="<c:out value="${vehicleList.unit3}"/>"
	</display:column>

	<sec-auth:authComponent componentId="module.tab.container.auditTab">
	    <display:column title="Audit" style="width:25px; text-align: center;">
	    	<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${vehicleList.id}&tableName=vehicle&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
	    </display:column>
    </sec-auth:authComponent> 
    <display:column title="Status" style="width:45px;text-align: center;">
			<c:if test="${vehicleList.status}">
          <input type="checkbox"  name="status${vehicleList.id}" id="status${vehicleList.id}"  checked="checked" onclick="deleteVehicleDetails(${vehicleList.id},this);" />
        </c:if>
       <c:if test="${vehicleList.status==false}">
		<input type="checkbox"  name="status${vehicleList.id}" id="status${vehicleList.id}"  onclick="deleteVehicleDetails(${vehicleList.id},this);" />
       </c:if> 	   
				   
 </display:column> 
    <display:setProperty name="paging.banner.item_name" value="vehicle"/>  
    <display:setProperty name="paging.banner.items_name" value="vehicle"/>
  
    <display:setProperty name="export.excel.filename" value="Vehicle List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Vehicle List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Vehicle List.pdf"/>   
</display:table>   
 <sec-auth:authComponent componentId="module.button.vehicle.addButton">
<c:out value="${buttons}" escapeXml="false" />
</sec-auth:authComponent>
</div>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<s:hidden name="id"/>
<script type="text/javascript">   
    Form.focusFirstElement($("serviceForm3"));   
</script> 
<script type="text/javascript">
    var fieldNameV = document.getElementById('fieldV').value;
    var fieldName1V = document.getElementById('field1V').value;
	if(fieldNameV!='' && fieldNameV.indexOf("vehicle")!= -1){
		fieldNameV = fieldNameV.replace("vehicle.","");
		document.getElementById(fieldNameV).className = 'rules-textUpper';
	}
	if(fieldName1V!='' && fieldName1V.indexOf("vehicle")!= -1){
		fieldName1V = fieldName1V.replace("vehicle.","");
		document.getElementById(fieldName1V).className = 'rules-textUpper';		
	}
 </script>   
