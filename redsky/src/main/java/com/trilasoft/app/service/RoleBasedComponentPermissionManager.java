package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RoleBasedComponentPermission;

public interface RoleBasedComponentPermissionManager extends GenericManager<RoleBasedComponentPermission, Long>{
	
	public boolean isAccessAllowed(String componentId, String roles);
	
	public List findMaximumId();

	public List searchcompanyPermission(String componentId, String description,String role);
	
	public List getRole(String corpID);

	public int getPermission(String componentId, String roles);
	public String getUserRoleInTransaction(String transId,String partnerIdList);
	public List getModulePermissions(String[] tableList, String roles);
	public String getPartnerIdList(String userName);
	public void updateCurrentRolePermission(String corpId);
}
