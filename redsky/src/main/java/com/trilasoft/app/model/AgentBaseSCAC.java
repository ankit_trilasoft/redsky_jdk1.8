package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="agentbasescac")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class AgentBaseSCAC extends BaseObject {
	private String corpID;
	private Long id;
	private String baseCode;
	private String SCACCode;
	private String partnerCode;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Boolean internationalLOIflag;
	private Boolean domesticLOIflag;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("baseCode", baseCode).append("SCACCode",
				SCACCode).append("partnerCode", partnerCode).append(
				"createdBy", createdBy).append("updatedBy", updatedBy).append(
				"createdOn", createdOn).append("updatedOn", updatedOn)
				.append("internationalLOIflag", internationalLOIflag).append("domesticLOIflag", domesticLOIflag)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgentBaseSCAC))
			return false;
		AgentBaseSCAC castOther = (AgentBaseSCAC) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(baseCode, castOther.baseCode).append(
						SCACCode, castOther.SCACCode).append(partnerCode,
				castOther.partnerCode).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,castOther.updatedOn)
						.append(internationalLOIflag,castOther.internationalLOIflag)
						.append(domesticLOIflag,castOther.domesticLOIflag).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(baseCode)
				.append(SCACCode).append(partnerCode).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(internationalLOIflag).append(domesticLOIflag)
				.toHashCode();
	}
	@Column(length=10)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column(length=10)
	public String getBaseCode() {
		return baseCode;
	}
	public void setBaseCode(String baseCode) {
		this.baseCode = baseCode;
	}
	@Column(length=20)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public String getSCACCode() {
		return SCACCode;
	}
	public void setSCACCode(String SCACCode) {
		this.SCACCode = SCACCode;
	}
	@Column
	public Boolean getDomesticLOIflag() {
		return domesticLOIflag;
	}
	public void setDomesticLOIflag(Boolean domesticLOIflag) {
		this.domesticLOIflag = domesticLOIflag;
	}
	@Column
	public Boolean getInternationalLOIflag() {
		return internationalLOIflag;
	}
	public void setInternationalLOIflag(Boolean internationalLOIflag) {
		this.internationalLOIflag = internationalLOIflag;
	}
	

}
