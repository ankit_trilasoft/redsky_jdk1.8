package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.GLCommissionType;


public interface GLCommissionTypeDao extends GenericDao<GLCommissionType, Long> {
	
	public List<GLCommissionType> gLCommissionTypeList(String contract, String charge);
	
	//public List<VanLineGLType> searchGLTypeCommission(String glType, String description, String glCode);
	public List<GLCommissionType> glList(String gl);
}