package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.IntegrationLogDao;
import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.IntegrationLogDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.IntegrationLog;
import com.trilasoft.app.model.WorkTicket;

public class IntegrationLogDaoHibernate extends GenericDaoHibernate<IntegrationLog, Long> implements IntegrationLogDao {
	private List integrationLogs;

	private HibernateUtil hibernateUtil;

	// /String toProcessedOn;
	public IntegrationLogDaoHibernate() {
		super(IntegrationLog.class);
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	String processedOn;

	private String MasterList;

	public List<IntegrationLog> findIntegrationLogs(String fileName, Date date, String recordID, String message) {
		List integrationLogs = null;
		String columns = "id,corpID,message,fileName,batchID,processedOn,recordID,transactionID";
		if (!(date == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date));
			processedOn = nowYYYYMMDD1.toString();
		}

		if (date != null) {
			integrationLogs = getHibernateTemplate().find(
					"select " + columns + " from IntegrationLog where processedOn like '" + processedOn + "%' AND fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND recordID like '" + recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '" + message.replaceAll("'", "''").replaceAll(":", "''") + "%'");
		}
		if (date == null) {
			integrationLogs = getHibernateTemplate().find(
					"select " + columns + " from IntegrationLog where fileName like '" + fileName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''") + "%' AND message like '" + message.replaceAll("'", "''").replaceAll(":", "''") + "%'");
		}
		try {
			integrationLogs = hibernateUtil.convertScalarToComponent(columns, integrationLogs, IntegrationLog.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * if( !(processedOn == null) ){ SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd"); StringBuilder nowYYYYMMDD1 = new StringBuilder(
		 * dateformatYYYYMMDD1.format( processedOn ) ); toProcessedOn = nowYYYYMMDD1.toString(); }
		 */
		/*
		 * if(processedOn != null) { SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd"); StringBuilder formatedDate1 = new StringBuilder(
		 * dateformatYYYYMMDD1.format( processedOn ) ); integrationLogs = getHibernateTemplate().find("from IntegrationLog where fileName like '" + fileName+"%' AND recordID like '" +
		 * recordID+"%' AND message like '" + message+"%' AND processedOn='"+formatedDate1+"'"); } if(processedOn == null){ integrationLogs = getHibernateTemplate().find("from
		 * IntegrationLog where fileName like '" + fileName+"%' AND recordID like '" + recordID+"%' AND message like '" + message+"%'"); }
		 */
		return integrationLogs;
	}

	public List findServiceOrder(String shipNum, String corpID) {
		return getHibernateTemplate().find("from ServiceOrder where shipNumber=?", shipNum);

	}
	
	public class MemoUploadDTO {
		
		private String so;

		private String  note;

		private String  uploadStatus;

		private String  uploadDate;
		
		private String  customerNumber;
		
		private Long id;

		public String getSo() {
			return so;
		}

		public void setSo(String so) {
			this.so = so;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}

		public String getUploadStatus() {
			return uploadStatus;
		}

		public void setUploadStatus(String uploadStatus) {
			this.uploadStatus = uploadStatus;
		}

		public String getUploadDate() {
			return uploadDate;
		}

		public void setUploadDate(String uploadDate) {
			this.uploadDate = uploadDate;
		}

		public String getCustomerNumber() {
			return customerNumber;
		}

		public void setCustomerNumber(String customerNumber) {
			this.customerNumber = customerNumber;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		
		
	}

	public class IntegrationLogDTO {
		private Object id;

		private Object transactionID;

		private Object message;

		private Object fileName;

		private Object batchID;

		private Object processedOn;

		private Object recordID;

		private Object serviceOrderId;
		
		private Object count;
		
		private Object xmlCount;
		
		private Object controlFlag;

		public Object getBatchID() {
			return batchID;
		}

		public void setBatchID(Object batchID) {
			this.batchID = batchID;
		}

		public Object getFileName() {
			return fileName;
		}

		public void setFileName(Object fileName) {
			this.fileName = fileName;
		}

		public Object getId() {
			return id;
		}

		public void setId(Object id) {
			this.id = id;
		}

		public Object getMessage() {
			return message;
		}

		public void setMessage(Object message) {
			this.message = message;
		}

		public Object getProcessedOn() {
			return processedOn;
		}

		public void setProcessedOn(Object processedOn) {
			this.processedOn = processedOn;
		}

		public Object getRecordID() {
			return recordID;
		}

		public void setRecordID(Object recordID) {
			this.recordID = recordID;
		}

		public Object getTransactionID() {
			return transactionID;
		}

		public void setTransactionID(Object transactionID) {
			this.transactionID = transactionID;
		}

		public Object getServiceOrderId() {
			return serviceOrderId;
		}

		public void setServiceOrderId(Object serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}

		public Object getCount() {
			return count;
		}

		public void setCount(Object count) {
			this.count = count;
		}

		public Object getXmlCount() {
			return xmlCount;
		}

		public void setXmlCount(Object xmlCount) {
			this.xmlCount = xmlCount;
		}

		public Object getControlFlag() {
			return controlFlag;
		}

		public void setControlFlag(Object controlFlag) {
			this.controlFlag = controlFlag;
		}

	}

	public List getAllWithServiceOrderId(String corpId) {
		List integrationLogs = new ArrayList();
		String MasterList = "select i.id as iid, i.transactionID as itid, i.message imsg ,i.fileName ifn, i.batchID ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, s.id as sid FROM integrationlog i, serviceorder s " +
				"where i.transactionId=s.shipNumber AND i.corpID = '"+corpId+"'" +
				"union select i.id as iid, i.transactionID as itid, i.message imsg ,i.fileName ifn, i.batchID ibid, i.processedOn as ipon, i.recordID as irid, v.id as sid FROM integrationlog i, vanline v " +
				"where i.transactionId = v.lineNumber and filename like 'st%' AND i.corpID = '"+corpId+"'" +
				"group by i.id order by ipon desc";
		List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
		"ipon").addScalar("irid").addScalar("sid").list();

		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			integrationLogDTO.setId(Long.parseLong(row[0].toString()));
			if (row[1] == null) {
				integrationLogDTO.setTransactionID("");
			} else {
				integrationLogDTO.setTransactionID(row[1].toString());
			}
			if (row[2] == null) {
				integrationLogDTO.setMessage("");
			} else {
				integrationLogDTO.setMessage(row[2].toString());
			}
			if (row[3] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[3].toString());
			}
			if (row[4] == null) {
				integrationLogDTO.setBatchID("");
			} else {
				integrationLogDTO.setBatchID(row[4].toString());
			}
			if (row[5] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[5].toString());
			}
			if (row[6] == null) {
				integrationLogDTO.setRecordID("");
			} else {
				integrationLogDTO.setRecordID(row[6].toString());
			}
			if (row[7] == null) {
				integrationLogDTO.setServiceOrderId("");
			} else {
				integrationLogDTO.setServiceOrderId(row[7].toString());
			}

			integrationLogs.add(integrationLogDTO);
		}
		return integrationLogs;

	}
	public List findMemoUpload(String shipNumber, String note, String memoUploadStatus, String date, String corpId) {
		List integrationLogs = new ArrayList();
		String MasterList = "";

		MasterList = "select n.note as note,n.customerNumber as custNum,so.shipNumber as shipNum,n.memoUploadStatus as memoUpSataus,n.memoLastUploadedDate as memoUpDate,n.id as id FROM  notes n, serviceorder so " +
				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and  n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL')";
		/*if (date == null) {
			
				MasterList = "select n.note as note,n.customerNumber as custNum,s.shipNumber as shipNum,n.memoUploadStatus as memoUpSataus,n.memoLastUploadedDate as memoUpDate FROM from notes n, serviceorder so " +
						"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and  n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL') and n.memoUploadStatus like '"+ memoUploadStatus.replaceAll("'", "''").replaceAll(":", "''")	+ "%' and n.note like '"+ note.replaceAll("'", "''").replaceAll(":", "''")	+ "%' and s.shipNumber like '" + note.replaceAll("'", "''").replaceAll(":", "''")	+ "%'";
			
		}
		if (date != null) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date));			
				
				MasterList = "select n.note as note,n.customerNumber as custNum,s.shipNumber as shipNum,n.memoUploadStatus as memoUpSataus,n.memoLastUploadedDate as memoUpDate FROM from notes n, serviceorder so " +
				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and  n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL') and n.memoUploadStatus like '"+ memoUploadStatus.replaceAll("'", "''").replaceAll(":", "''")	+ "%' and n.note like '"+ note.replaceAll("'", "''").replaceAll(":", "''")	+ "%' and s.shipNumber like '" + note.replaceAll("'", "''").replaceAll(":", "''")	+ "%' and  memoLastUploadedDate like like '"+ formatedDate1+ "%'";
			
		}*/
		List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("note").addScalar("custNum").addScalar("shipNum").addScalar("memoUpSataus").addScalar("memoUpDate").addScalar("id").setMaxResults(500).list();

		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			MemoUploadDTO integrationLogDTO = new MemoUploadDTO();			
			if (row[0] == null) {
				integrationLogDTO.setNote("");
			} else {
				integrationLogDTO.setNote(row[0].toString());
			}
			if (row[1] == null) {
				integrationLogDTO.setCustomerNumber("");
			} else {
				integrationLogDTO.setCustomerNumber(row[1].toString());
			}
			if (row[2] == null) {
				integrationLogDTO.setSo("");
			} else {
				integrationLogDTO.setSo(row[2].toString());
			}
			if (row[3] == null) {
				integrationLogDTO.setUploadStatus("");
			} else {
				integrationLogDTO.setUploadStatus(row[3].toString());
			}
			if (row[4] == null) {
				integrationLogDTO.setUploadDate("");
			} else {
				integrationLogDTO.setUploadDate(row[4].toString());
			}
			if (row[5] == null) {
				integrationLogDTO.setId(0L);
			} else {
				integrationLogDTO.setId(new Long(row[5].toString()));
			}
			integrationLogs.add(integrationLogDTO);
		}
		return integrationLogs;
	}

	public List findIntegrationCenterLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId){
		List integrationCenterLogs = new ArrayList();
		String MasterList = "";
		String maxProcessDate ="";
		if (date == null) {
			if(intType!=null && !(intType.equals("")) && intType.equals("qt")){
				//Date start		
				  try{
					  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"'").list().get(0).toString();
					  
				  }catch(Exception e){
					  maxProcessDate="";
				  }
				 
				  SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				 Date dd=null;
				try{
				dd=dateformatYYYYMMDD1.parse(maxProcessDate);
				}catch(Exception e){}
				StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
				  //end
				String qtg="select id as iid ,sequenceNumber  as itid,'Quotes Uploaded Successfully' as imsg,'Quotes-2-Go' as ifn,date_format(createdOn,'%d-%b-%Y %H:%i:%s') as ibid,date_format(createdOn,'%d-%b-%Y %H:%i:%s') as ipon,registrationNumber as irid,id as sid,controlFlag as controlFlag from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"'  and registrationNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and sequenceNumber like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' order by createdOn desc";		
				 
				 List listMasterList = this.getSession().createSQLQuery(qtg).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							integrationLogDTO.setMessage(row[2].toString());
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}					
					
				} else if(intType!=null && !(intType.equals("")) && intType.equals("tu")){	
					 maxProcessDate ="";
					  try{
						  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='DOWNLOADED') and corpID ='"+corpId+"' and customerNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileId like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'").list().get(0).toString();
						  
					  }catch(Exception e){
						  maxProcessDate="";
					  }
					 
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date dd=null;
					try{
					dd=dateformatYYYYMMDD1.parse(maxProcessDate);
					}catch(Exception e){}
					//StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
					  //end
					  String transdoc="select id as iid ,fileId  as itid,'Trans Docs Uploaded Successfully' as imsg,'Trans Docs' as ifn,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ibid,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ipon,customerNumber as irid,id as sid,transdocstatus as controlFlag from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='DOWNLOADED') and corpID ='"+corpId+"'  and customerNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileId like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' order by iid desc ";
					
					List listMasterList = this.getSession().createSQLQuery(transdoc).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							if(row[8].toString().equals("UPLOAD_FAILED")){
								integrationLogDTO.setMessage("Trans Docs Send Failed.");
							}else{
							integrationLogDTO.setMessage(row[2].toString());
							}
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}				
					
				}
                else if(intType!=null && !(intType.equals("")) && intType.equals("td")){
                	 maxProcessDate ="";
					  try{
						  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='') and transdocstatus='DOWNLOADED' and corpID ='"+corpId+"'").list().get(0).toString();
						  
					  }catch(Exception e){
						  maxProcessDate="";
					  }
					 
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date dd=null;
					try{
					dd=dateformatYYYYMMDD1.parse(maxProcessDate);
					}catch(Exception e){}
					//StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
					  //end
					  String transdoc="select id as iid ,fileId  as itid,'Trans Docs Downloaded Successfully' as imsg,'Trans Docs' as ifn,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ibid,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ipon,customerNumber as irid,id as sid,transdocstatus as controlFlag from myfile where (transdocstatus is not null and transdocstatus!='') and (transdocstatus='DOWNLOADED' or transdocstatus='DOWNLOAD_FAILED') and corpID ='"+corpId+"'  and customerNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileId like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' order by iid desc ";
					
					List listMasterList = this.getSession().createSQLQuery(transdoc).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							if(row[8].toString().equals("DOWNLOAD_FAILED")){
								integrationLogDTO.setMessage("Trans Docs Receive Failed.");
							}else{
							integrationLogDTO.setMessage(row[2].toString());
							}
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}				
					
					
				}
             else if(intType!=null && !(intType.equals("")) && intType.equals("mu")){
            	//Date start		
				  try{
					  maxProcessDate = this.getSession().createSQLQuery("select max(n.memoLastUploadedDate) FROM  notes n, serviceorder so " +
								"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL')").list().get(0).toString();
					  
				  }catch(Exception e){
					  maxProcessDate="";
				  }
				 
				  SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				 Date dd=null;
				try{
				dd=dateformatYYYYMMDD1.parse(maxProcessDate);
				}catch(Exception e){}
				StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
				  //end
       		String memoQuery="select so.id as iid ,so.shipNumber  as itid,'Memo Uploaded Successfully' as imsg,'Memo' as ifn,date_format(n.memoLastUploadedDate,'%d-%b-%Y %H:%i:%s') as ibid,date_format(n.memoLastUploadedDate,'%d-%b-%Y %H:%i:%s') as ipon,so.registrationNumber as irid,so.id as sid,so.controlFlag as controlFlag  FROM  notes n, serviceorder so " +
       				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL')  and so.registrationNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and so.shipNumber like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'";
       	 List listMasterList = this.getSession().createSQLQuery(memoQuery).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
			"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

			Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
				integrationLogDTO.setId(Long.parseLong(row[0].toString()));
				if (row[1] == null) {
					integrationLogDTO.setTransactionID("");
				} else {
					integrationLogDTO.setTransactionID(row[1].toString());
				}
				if (row[2] == null) {
					integrationLogDTO.setMessage("");
				} else {
					integrationLogDTO.setMessage(row[2].toString());
				}
				if (row[3] == null) {
					integrationLogDTO.setFileName("");
				} else {
					integrationLogDTO.setFileName(row[3].toString());
				}
				if (row[4] == null) {
					integrationLogDTO.setBatchID("");
				} else {
					integrationLogDTO.setBatchID(row[4].toString());
				}
				if (row[5] == null) {
					integrationLogDTO.setProcessedOn("");
				} else {
					integrationLogDTO.setProcessedOn(row[5].toString());
				}
				if (row[6] == null) {
					integrationLogDTO.setRecordID("");
				} else {
					integrationLogDTO.setRecordID(row[6].toString());
				}
				if (row[7] == null) {
					integrationLogDTO.setServiceOrderId("");
				} else {
					integrationLogDTO.setServiceOrderId(row[7].toString());
				}
				if (row[8] == null) {
					integrationLogDTO.setControlFlag("");
				}else{
					integrationLogDTO.setControlFlag(row[8].toString());
				}
				integrationCenterLogs.add(integrationLogDTO);
			}				
		
	
	
}else if(intType!=null && !(intType.equals(""))){
	MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%d-%b-%Y %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s " 
		+ "where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
		+ fileName.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND recordID like '"
		+ recordID.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND message like '"
		+ message.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND transactionID like '" 
		+ transId.replaceAll("'", "''").replaceAll(":", "''")
		+ "%'" 
		+" and fileName like '"+ intType+"%' "
		/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " 
		+ "where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
		+ fileName.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND recordID like '"
		+ recordID.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND message like '" 
		+ message.replaceAll("'", "''").replaceAll(":", "''") 
		+ "%' AND transactionID like '" + transId.replaceAll("'", "''").replaceAll(":", "''")+"%'" +*/
	    + " group by i.id order by iid desc ";
	List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
	"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();
	Iterator it = listMasterList.iterator();
	while (it.hasNext()) {
		Object[] row = (Object[]) it.next();
		IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
		integrationLogDTO.setId(Long.parseLong(row[0].toString()));
		if (row[1] == null) {
			integrationLogDTO.setTransactionID("");
		} else {
			integrationLogDTO.setTransactionID(row[1].toString());
		}
		if (row[2] == null) {
			integrationLogDTO.setMessage("");
		} else {
			integrationLogDTO.setMessage(row[2].toString());
		}
		if (row[3] == null) {
			integrationLogDTO.setFileName("");
		} else {
			integrationLogDTO.setFileName(row[3].toString());
		}
		if (row[4] == null) {
			integrationLogDTO.setBatchID("");
		} else {
			integrationLogDTO.setBatchID(row[4].toString());
		}
		if (row[5] == null) {
			integrationLogDTO.setProcessedOn("");
		} else {
			integrationLogDTO.setProcessedOn(row[5].toString());
		}
		if (row[6] == null) {
			integrationLogDTO.setRecordID("");
		} else {
			integrationLogDTO.setRecordID(row[6].toString());
		}
		if (row[7] == null) {
			integrationLogDTO.setServiceOrderId("");
		} else {
			integrationLogDTO.setServiceOrderId(row[7].toString());
		}
		if (row[8] == null) {
			integrationLogDTO.setControlFlag("");
		}else{
			integrationLogDTO.setControlFlag(row[8].toString());
		}
		integrationCenterLogs.add(integrationLogDTO);
	}				


	
}else if(intType==null || (intType.equals(""))){
	MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%d-%b-%Y %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s " 
		+ "where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
		+ fileName.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND recordID like '"
		+ recordID.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND message like '"
		+ message.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND transactionID like '" 
		+ transId.replaceAll("'", "''").replaceAll(":", "''")
		+ "%'" 
		
		/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " 
		+ "where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
		+ fileName.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND recordID like '"
		+ recordID.replaceAll("'", "''").replaceAll(":", "''")
		+ "%' AND message like '" 
		+ message.replaceAll("'", "''").replaceAll(":", "''") 
		+ "%' AND transactionID like '" + transId.replaceAll("'", "''").replaceAll(":", "''")+"%'" +*/
	    + " group by i.id order by iid desc ";
	List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
	"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();
	Iterator it = listMasterList.iterator();
	while (it.hasNext()) {
		Object[] row = (Object[]) it.next();
		IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
		integrationLogDTO.setId(Long.parseLong(row[0].toString()));
		if (row[1] == null) {
			integrationLogDTO.setTransactionID("");
		} else {
			integrationLogDTO.setTransactionID(row[1].toString());
		}
		if (row[2] == null) {
			integrationLogDTO.setMessage("");
		} else {
			integrationLogDTO.setMessage(row[2].toString());
		}
		if (row[3] == null) {
			integrationLogDTO.setFileName("");
		} else {
			integrationLogDTO.setFileName(row[3].toString());
		}
		if (row[4] == null) {
			integrationLogDTO.setBatchID("");
		} else {
			integrationLogDTO.setBatchID(row[4].toString());
		}
		if (row[5] == null) {
			integrationLogDTO.setProcessedOn("");
		} else {
			integrationLogDTO.setProcessedOn(row[5].toString());
		}
		if (row[6] == null) {
			integrationLogDTO.setRecordID("");
		} else {
			integrationLogDTO.setRecordID(row[6].toString());
		}
		if (row[7] == null) {
			integrationLogDTO.setServiceOrderId("");
		} else {
			integrationLogDTO.setServiceOrderId(row[7].toString());
		}
		if (row[8] == null) {
			integrationLogDTO.setControlFlag("");
		}else{
			integrationLogDTO.setControlFlag(row[8].toString());
		}
		integrationCenterLogs.add(integrationLogDTO);
	}				

	
}
		}
		if (date != null) {
			SimpleDateFormat dateformatYYYYMMDD11 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD11.format(date));
			if(intType!=null && !(intType.equals("")) && intType.equals("qt")){
				//Date start		
				  try{
					  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"'").list().get(0).toString();
					  
				  }catch(Exception e){
					  maxProcessDate="";
				  }
				 
				  SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				 Date dd=null;
				try{
				dd=dateformatYYYYMMDD1.parse(maxProcessDate);
				}catch(Exception e){}
				StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
				  //end
				String qtg="select id as iid ,sequenceNumber  as itid,'Quotes Uploaded Successfully' as imsg,'Quotes-2-Go' as ifn,date_format(createdOn,'%d-%b-%Y %H:%i:%s') as ibid,date_format(createdOn,'%d-%b-%Y %H:%i:%s') as ipon,registrationNumber as irid,id as sid,'' as controlFlag from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"' and createdOn like '"+formatedDate1+ "%' and registrationNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and sequenceNumber like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'";		
				 
				 List listMasterList = this.getSession().createSQLQuery(qtg).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							integrationLogDTO.setMessage(row[2].toString());
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}					
					
				} else if(intType!=null && !(intType.equals("")) && intType.equals("tu")){	
					 maxProcessDate ="";
					  try{
						  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='UPLOAD_FAILED') and transdocstatus!='DOWNLOADED' and corpID ='"+corpId+"'").list().get(0).toString();
						  
					  }catch(Exception e){
						  maxProcessDate="";
					  }
					 
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date dd=null;
					try{
					dd=dateformatYYYYMMDD1.parse(maxProcessDate);
					}catch(Exception e){}
					StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
					  //end
					  String transdoc="select id as iid ,fileId  as itid,'Trans Docs Uploaded Successfully' as imsg,'Trans Docs' as ifn,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ibid,updatedOn as ipon,customerNumber as irid,id as sid,'' as controlFlag from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='UPLOAD_FAILED') and transdocstatus!='DOWNLOADED' and corpID ='"+corpId+"' and  updatedOn like '"+formatedDate1+ "%' and customerNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileId like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'";
					
					List listMasterList = this.getSession().createSQLQuery(transdoc).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							integrationLogDTO.setMessage(row[2].toString());
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}				
					
				}
                else if(intType!=null && !(intType.equals("")) && intType.equals("td")){
                	 maxProcessDate ="";
					  try{
						  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='UPLOAD_FAILED') and transdocstatus='DOWNLOADED' and corpID ='"+corpId+"'").list().get(0).toString();
						  
					  }catch(Exception e){
						  maxProcessDate="";
					  }
					 
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date dd=null;
					try{
					dd=dateformatYYYYMMDD1.parse(maxProcessDate);
					}catch(Exception e){}
					StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
					  //end
					  String transdoc="select id as iid ,fileId  as itid,'Trans Docs Downloaded Successfully' as imsg,'Trans Docs' as ifn,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ibid,date_format(updatedOn,'%d-%b-%Y %H:%i:%s') as ipon,customerNumber as irid,id as sid,'' as controlFlag from myfile where (transdocstatus is not null and transdocstatus!='' and transdocstatus!='UPLOAD_FAILED') and transdocstatus='DOWNLOADED' and corpID ='"+corpId+"' and  updatedOn like '"+formatedDate1+ "%' and customerNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileId like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'";
					
					List listMasterList = this.getSession().createSQLQuery(transdoc).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
					"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

					Iterator it = listMasterList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
						integrationLogDTO.setId(Long.parseLong(row[0].toString()));
						if (row[1] == null) {
							integrationLogDTO.setTransactionID("");
						} else {
							integrationLogDTO.setTransactionID(row[1].toString());
						}
						if (row[2] == null) {
							integrationLogDTO.setMessage("");
						} else {
							integrationLogDTO.setMessage(row[2].toString());
						}
						if (row[3] == null) {
							integrationLogDTO.setFileName("");
						} else {
							integrationLogDTO.setFileName(row[3].toString());
						}
						if (row[4] == null) {
							integrationLogDTO.setBatchID("");
						} else {
							integrationLogDTO.setBatchID(row[4].toString());
						}
						if (row[5] == null) {
							integrationLogDTO.setProcessedOn("");
						} else {
							integrationLogDTO.setProcessedOn(row[5].toString());
						}
						if (row[6] == null) {
							integrationLogDTO.setRecordID("");
						} else {
							integrationLogDTO.setRecordID(row[6].toString());
						}
						if (row[7] == null) {
							integrationLogDTO.setServiceOrderId("");
						} else {
							integrationLogDTO.setServiceOrderId(row[7].toString());
						}
						if (row[8] == null) {
							integrationLogDTO.setControlFlag("");
						}else{
							integrationLogDTO.setControlFlag(row[8].toString());
						}
						integrationCenterLogs.add(integrationLogDTO);
					}				
					
					
				}
             else if(intType!=null && !(intType.equals("")) && intType.equals("mu")){
            	//Date start		
				  try{
					  maxProcessDate = this.getSession().createSQLQuery("select max(n.memoLastUploadedDate) FROM  notes n, serviceorder so " +
								"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL')").list().get(0).toString();
					  
				  }catch(Exception e){
					  maxProcessDate="";
				  }
				 
				  SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				 Date dd=null;
				try{
				dd=dateformatYYYYMMDD1.parse(maxProcessDate);
				}catch(Exception e){}
				StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
				  //end
       		String memoQuery="select so.id as iid ,so.shipNumber  as itid,'Memo Uploaded Successfully' as imsg,'Memo' as ifn,date_format(n.memoLastUploadedDate,'%d-%b-%Y %H:%i:%s') as ibid,date_format(n.memoLastUploadedDate,'%d-%b-%Y %H:%i:%s') as ipon,so.registrationNumber as irid,so.id as sid,so.controlFlag as controlFlag  FROM  notes n, serviceorder so " +
       				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL') and  n.memoLastUploadedDate like '"+formatedDate1+ "%' and so.registrationNumber like '"+ recordID.replaceAll("'", "''").replaceAll(":", "''")+"%' and so.shipNumber like '"+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%'";
       	 List listMasterList = this.getSession().createSQLQuery(memoQuery).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
			"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

			Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
				integrationLogDTO.setId(Long.parseLong(row[0].toString()));
				if (row[1] == null) {
					integrationLogDTO.setTransactionID("");
				} else {
					integrationLogDTO.setTransactionID(row[1].toString());
				}
				if (row[2] == null) {
					integrationLogDTO.setMessage("");
				} else {
					integrationLogDTO.setMessage(row[2].toString());
				}
				if (row[3] == null) {
					integrationLogDTO.setFileName("");
				} else {
					integrationLogDTO.setFileName(row[3].toString());
				}
				if (row[4] == null) {
					integrationLogDTO.setBatchID("");
				} else {
					integrationLogDTO.setBatchID(row[4].toString());
				}
				if (row[5] == null) {
					integrationLogDTO.setProcessedOn("");
				} else {
					integrationLogDTO.setProcessedOn(row[5].toString());
				}
				if (row[6] == null) {
					integrationLogDTO.setRecordID("");
				} else {
					integrationLogDTO.setRecordID(row[6].toString());
				}
				if (row[7] == null) {
					integrationLogDTO.setServiceOrderId("");
				} else {
					integrationLogDTO.setServiceOrderId(row[7].toString());
				}
				if (row[8] == null) {
					integrationLogDTO.setControlFlag("");
				}else{
					integrationLogDTO.setControlFlag(row[8].toString());
				}
				integrationCenterLogs.add(integrationLogDTO);
			}				
		
	
	
}else if(intType!=null && !(intType.equals(""))){
	MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%d-%b-%Y %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
	+ fileName.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND recordID like '"
	+ recordID.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND message like '"
	+ message.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND processedOn like '"
	+ formatedDate1
	+ "%' AND transactionID like '" 
	+ transId.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' "
	/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
	+ fileName.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND recordID like '"
	+ recordID.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND message like '" 
	+ message.replaceAll("'", "''").replaceAll(":", "''") 
	+ "%' AND processedOn like '" 
	+ formatedDate1 
	+ "%' AND transactionID like '" 
	+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' "*/
	+ "  and fileName like '"+ intType+"%'  group by i.id order by iid desc ";
	List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
	"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();
	Iterator it = listMasterList.iterator();
	while (it.hasNext()) {
		Object[] row = (Object[]) it.next();
		IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
		integrationLogDTO.setId(Long.parseLong(row[0].toString()));
		if (row[1] == null) {
			integrationLogDTO.setTransactionID("");
		} else {
			integrationLogDTO.setTransactionID(row[1].toString());
		}
		if (row[2] == null) {
			integrationLogDTO.setMessage("");
		} else {
			integrationLogDTO.setMessage(row[2].toString());
		}
		if (row[3] == null) {
			integrationLogDTO.setFileName("");
		} else {
			integrationLogDTO.setFileName(row[3].toString());
		}
		if (row[4] == null) {
			integrationLogDTO.setBatchID("");
		} else {
			integrationLogDTO.setBatchID(row[4].toString());
		}
		if (row[5] == null) {
			integrationLogDTO.setProcessedOn("");
		} else {
			integrationLogDTO.setProcessedOn(row[5].toString());
		}
		if (row[6] == null) {
			integrationLogDTO.setRecordID("");
		} else {
			integrationLogDTO.setRecordID(row[6].toString());
		}
		if (row[7] == null) {
			integrationLogDTO.setServiceOrderId("");
		} else {
			integrationLogDTO.setServiceOrderId(row[7].toString());
		}
		if (row[8] == null) {
			integrationLogDTO.setControlFlag("");
		}else{
			integrationLogDTO.setControlFlag(row[8].toString());
		}
		integrationCenterLogs.add(integrationLogDTO);
	}				

	
}else if(intType==null || (intType.equals(""))){
	MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%d-%b-%Y %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
	+ fileName.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND recordID like '"
	+ recordID.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND message like '"
	+ message.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND processedOn like '"
	+ formatedDate1
	+ "%' AND transactionID like '" 
	+ transId.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' "
	/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
	+ fileName.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND recordID like '"
	+ recordID.replaceAll("'", "''").replaceAll(":", "''")
	+ "%' AND message like '" 
	+ message.replaceAll("'", "''").replaceAll(":", "''") 
	+ "%' AND processedOn like '" 
	+ formatedDate1 
	+ "%' AND transactionID like '" 
	+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' "*/
	+ " group by i.id order by iid desc ";
	List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
	"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();
	Iterator it = listMasterList.iterator();
	while (it.hasNext()) {
		Object[] row = (Object[]) it.next();
		IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
		integrationLogDTO.setId(Long.parseLong(row[0].toString()));
		if (row[1] == null) {
			integrationLogDTO.setTransactionID("");
		} else {
			integrationLogDTO.setTransactionID(row[1].toString());
		}
		if (row[2] == null) {
			integrationLogDTO.setMessage("");
		} else {
			integrationLogDTO.setMessage(row[2].toString());
		}
		if (row[3] == null) {
			integrationLogDTO.setFileName("");
		} else {
			integrationLogDTO.setFileName(row[3].toString());
		}
		if (row[4] == null) {
			integrationLogDTO.setBatchID("");
		} else {
			integrationLogDTO.setBatchID(row[4].toString());
		}
		if (row[5] == null) {
			integrationLogDTO.setProcessedOn("");
		} else {
			integrationLogDTO.setProcessedOn(row[5].toString());
		}
		if (row[6] == null) {
			integrationLogDTO.setRecordID("");
		} else {
			integrationLogDTO.setRecordID(row[6].toString());
		}
		if (row[7] == null) {
			integrationLogDTO.setServiceOrderId("");
		} else {
			integrationLogDTO.setServiceOrderId(row[7].toString());
		}
		if (row[8] == null) {
			integrationLogDTO.setControlFlag("");
		}else{
			integrationLogDTO.setControlFlag(row[8].toString());
		}
		integrationCenterLogs.add(integrationLogDTO);
	}				
}
		}
		
		
		return integrationCenterLogs;
	}
	
	public List findIntegrationLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId) {
		List integrationLogs = new ArrayList();
		String MasterList = "";
		if (date == null) {
			if (fileName.startsWith("st")) {
				if(intType!=null && !(intType.equals(""))){
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " +
					"where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber  and fileName like '"
				+ fileName.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND recordID like '"
				+ recordID.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND message like '" 
				+ message.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND transactionID like '" 
				+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileName like '"
				+ intType+"%' group by i.id order by iid desc ";
					
				}else{
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " +
					"where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber  and fileName like '"
				+ fileName.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND recordID like '"
				+ recordID.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND message like '" 
				+ message.replaceAll("'", "''").replaceAll(":", "''")
				+ "%' AND transactionID like '" 
				+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' group by i.id order by iid desc ";
					
				}
				
			} else {
				if(intType!=null && !(intType.equals(""))){
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s " 
						+ "where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
						+ fileName.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND recordID like '"
						+ recordID.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND message like '"
						+ message.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND transactionID like '" 
						+ transId.replaceAll("'", "''").replaceAll(":", "''")
						+ "%'" 
						+" and fileName like '"+ intType+"%' "
						/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " 
						+ "where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
						+ fileName.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND recordID like '"
						+ recordID.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND message like '" 
						+ message.replaceAll("'", "''").replaceAll(":", "''") 
						+ "%' AND transactionID like '" + transId.replaceAll("'", "''").replaceAll(":", "''")+"%'" +*/
					    + " group by i.id order by iid desc ";
					
				}else{
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s " 
						+ "where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
						+ fileName.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND recordID like '"
						+ recordID.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND message like '"
						+ message.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND transactionID like '" 
						+ transId.replaceAll("'", "''").replaceAll(":", "''")
						+ "%'" 
						/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v " 
						+ "where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
						+ fileName.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND recordID like '"
						+ recordID.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND message like '" 
						+ message.replaceAll("'", "''").replaceAll(":", "''") 
						+ "%' AND transactionID like '" + transId.replaceAll("'", "''").replaceAll(":", "''")+"%'" +*/
					    + " group by i.id order by iid desc ";
					
				}
				
			}
		}
		if (date != null) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date));
			if (fileName.startsWith("st")) {
				if(intType!=null && !(intType.equals(""))){
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber  and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '" 
					+ message.replaceAll("'", "''").replaceAll(":", "''") 
					+ "%' AND processedOn like '" 
					+ formatedDate1 
					+ "%' AND transactionID like '" 					
					+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' and fileName like '"+ intType+"%' " +
							"group by i.id order by iid desc ";
					
				}else{
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber  and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '" 
					+ message.replaceAll("'", "''").replaceAll(":", "''") 
					+ "%' AND processedOn like '" 
					+ formatedDate1 
					+ "%' AND transactionID like '" 
					+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' group by i.id order by iid desc ";
					
				}
				
			} else {
				if(intType!=null && !(intType.equals(""))){
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '"
					+ message.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND processedOn like '"
					+ formatedDate1
					+ "%' AND transactionID like '" 
					+ transId.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' "
					/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '" 
					+ message.replaceAll("'", "''").replaceAll(":", "''") 
					+ "%' AND processedOn like '" 
					+ formatedDate1 
					+ "%' AND transactionID like '" 
					+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' "*/
					+ "  and fileName like '"+ intType+"%'  group by i.id order by iid desc ";
				}else{
					MasterList = "select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, s.id as sid,s.controlFlag as controlFlag FROM integrationlog i, serviceorder s where i.corpID = '"+corpId+"' AND i.transactionId=s.shipNumber and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '"
					+ message.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND processedOn like '"
					+ formatedDate1
					+ "%' AND transactionID like '" 
					+ transId.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' "
					/*+"union select i.id as iid, i.transactionID as itid, i.message as imsg, i.fileName as ifn, i.batchID as ibid, date_format(i.processedOn,'%Y-%m-%d %H:%i:%s') as ipon, i.recordID as irid, v.id as sid,'' as controlFlag FROM integrationlog i, vanline v where i.corpID = '"+corpId+"' AND i.transactionId = v.lineNumber and fileName like '"
					+ fileName.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND recordID like '"
					+ recordID.replaceAll("'", "''").replaceAll(":", "''")
					+ "%' AND message like '" 
					+ message.replaceAll("'", "''").replaceAll(":", "''") 
					+ "%' AND processedOn like '" 
					+ formatedDate1 
					+ "%' AND transactionID like '" 
					+ transId.replaceAll("'", "''").replaceAll(":", "''")+"%' "*/
					+ " group by i.id order by iid desc ";
				}
				
			}
		}
		List listMasterList = this.getSession().createSQLQuery(MasterList).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
		"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			integrationLogDTO.setId(Long.parseLong(row[0].toString()));
			if (row[1] == null) {
				integrationLogDTO.setTransactionID("");
			} else {
				integrationLogDTO.setTransactionID(row[1].toString());
			}
			if (row[2] == null) {
				integrationLogDTO.setMessage("");
			} else {
				integrationLogDTO.setMessage(row[2].toString());
			}
			if (row[3] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[3].toString());
			}
			if (row[4] == null) {
				integrationLogDTO.setBatchID("");
			} else {
				integrationLogDTO.setBatchID(row[4].toString());
			}
			if (row[5] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[5].toString());
			}
			if (row[6] == null) {
				integrationLogDTO.setRecordID("");
			} else {
				integrationLogDTO.setRecordID(row[6].toString());
			}
			if (row[7] == null) {
				integrationLogDTO.setServiceOrderId("");
			} else {
				integrationLogDTO.setServiceOrderId(row[7].toString());
			}
			if (row[8] == null) {
				integrationLogDTO.setControlFlag("");
			}else{
				integrationLogDTO.setControlFlag(row[8].toString());
			}
			integrationLogs.add(integrationLogDTO);
		}
		return integrationLogs;
	}
	
	public List getWeeklyXML(String corpId){
		  List integrationLogs = new ArrayList();
		  
		 
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(processedon) from integrationlog where corpid = '"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  maxProcessDate="";
		  }
		 
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		
		String MasterList = "select if(left(filename,2)='20' or left(filename,2)='CW' or left(filename,2)='BO','DOCS', (if(left(filename,2)='dp','Dispatch', (if(left(filename,2)='ds','Distribution', (if(left(filename,2)='me','Memo', (if(left(filename,2)='rg','Registration', (if(left(filename,2)='st','Wkly.Stmnt',(if(left(filename,2)='DO','DOC',if(left(filename,2)='cl','Claims',(if(left(filename,2)='du','Dispatch Upload',(if(left(filename,2)='rt','Rating',left(filename,2))))))))))))))))))) 'XML', date_format(processedon,'%Y-%m-%d') 'DATE', count(*) 'Elements_Updated', count(distinct filename) 'No_Of_XML' from integrationlog where corpID ='"+ corpId +"' AND processedon >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by left( filename,2), date_format(processedon,'%Y-%m-%d')order by left( filename,2), date_format(processedon,'%Y-%m-%d') desc";
		List listMasterList = this.getSession().createSQLQuery(MasterList)
		.addScalar("XML")
		.addScalar("DATE")
		.addScalar("Elements_Updated")
		.addScalar("No_Of_XML")
		.list();
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			if (row[0] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[0].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[1].toString());
			}
			
			if (row[2] == null) {
				integrationLogDTO.setCount("");
			} else {
				integrationLogDTO.setCount(row[2].toString());
			}
			
			if (row[3] == null) {
				integrationLogDTO.setXmlCount("");
			} else {
				integrationLogDTO.setXmlCount(row[3].toString());
			}

			integrationLogs.add(integrationLogDTO);
		}
		return integrationLogs;
		
		//return getHibernateTemplate().find("select left( fileName,2) 'XML', date_format(processedOn,'%Y-%m-%d') 'DATE', count(*) 'Records' from IntegrationLog where processedOn >= date_sub(sysdate(), interval 7 DAY) group by left( fileName,2), date_format(processedOn,'%Y-%m-%d')order by left( fileName,2), date_format(processedOn,'%Y-%m-%d') desc");
	}
}
