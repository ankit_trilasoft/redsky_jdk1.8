delimiter $$

CREATE trigger redsky.trigger_add_history_trackingstatus_after AFTER INSERT on redsky.trackingstatus

for each row BEGIN

DECLARE fieldNameList LONGTEXT;
DECLARE oldValueList LONGTEXT;
DECLARE newValueList LONGTEXT;
DECLARE BAcode LONGTEXT;
DECLARE HAcode LONGTEXT;
DECLARE OAcode LONGTEXT;
DECLARE DAcode LONGTEXT;
DECLARE OSAcode LONGTEXT;
DECLARE DSAcode LONGTEXT;
DECLARE BRcode LONGTEXT;
DECLARE FRcode LONGTEXT;
DECLARE NPcode LONGTEXT;

SET fieldNameList = " ";
SET oldValueList = " ";
SET newValueList = " ";
SET BAcode = " ";
SET HAcode = " ";
SET OAcode = " ";
SET DAcode = " ";
SET OSAcode = " ";
SET DSAcode = " ";
SET BRcode = " ";
SET FRcode = " ";
SET NPcode = " ";


select bookingagentcode into BAcode from serviceorder where id=NEW.id;

select haulingAgentCode into HAcode from miscellaneous where id=NEW.id;

select originagent into OAcode from trackingstatus where id=NEW.id;

select destinationagent into DAcode from trackingstatus where id=NEW.id;

select originsubagent into OSAcode from trackingstatus where id=NEW.id;

select destinationsubagent into DSAcode from trackingstatus where id=NEW.id;

select brokercode into BRcode from trackingstatus where id=NEW.id;

select forwardercode into FRcode from trackingstatus where id=NEW.id;

select networkPartnerCode into NPcode from trackingstatus where id=NEW.id;


update sodashboard set role=(trim( both '/' from concat(if(OAcode is not null and OAcode<>'','OA/',''),
if(DAcode is not null and DAcode <>'','DA/',''),
if(OSAcode is not null and OSAcode<>'','OSA/',''),
if(DSAcode is not null and DSAcode<>'','DSA/',''),
if(BRcode is not null and BRcode<>'','BR/',''),
if(FRcode is not null and FRcode<>'','FR/',''),
if(BAcode is not null and BAcode<>'','BA/',''),
if(NPcode is not null and NPcode<>'','NA/',''),
if(HAcode is not null and HAcode<>'','HA/','')))) where serviceorderid=NEW.id;

END $$

delimiter;
