<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Driver Commission Plan</title> 
    <style>
    span.pagelinks{margin-top:-10px;}
    </style> 
 </head>
 <c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="searchDriverCommission" key="button.search"/>   

 <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="driverCommissionPlanList"  name="driverCommissionPlanList"  method="post" >
<s:hidden name="delCommissionId"></s:hidden>
<div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
				<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>Commission Plan</th>
<th>Charge code</th>
<th>Description</th>
<th>Contract</th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>		
	    <td><s:select cssClass="list-menu" name="driverCommissionPlan.plan" id="ExnewPlans"  list="%{planCommissionList}"  headerKey="" headerValue=""  cssStyle="width:150px;" /></td>
	    <td><s:select cssClass="list-menu" name="driverCommissionPlan.charge" id="charge5"  list="%{chargeList}"  onchange="setChargeValue(this);" headerKey="" headerValue=""  cssStyle="width:150px;" /></td>
	    <td><s:textfield name="driverCommissionPlan.description" size="35" required="true" cssClass="input-text"  onkeyup="valid(this,'special')" onblur="valid(this,'special')" cssStyle="width:150px;"/></td>
	    <td><s:select cssClass="list-menu" name="driverCommissionPlan.contract" id="ExnewPlans"  list="%{contractList}"  headerKey="" headerValue=""  cssStyle="width:150px;" /></td>
	   	<td width="130px" align="center">
		<c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Driver Commission List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
     <s:set name="commissionPlan" value="commissionPlan" scope="request"/>
     <display:table name="commissionPlan" class="table" requestURI="" id="commissionPlanList"  defaultsort="2" pagesize="20"  >   
		<display:column title="" ><input type="checkbox" id="${commissionPlanList.id}" name="commissionId" value="${commissionPlanList.id}" /></display:column>
		<display:column property="plan" sortable="true" title="Commission Plan"></display:column>
		<display:column property="charge" title="Charge Code" sortable="true" href="editDriverCommission.html" paramId="id" paramProperty="id" ></display:column>
		 <display:column property="description" sortable="true"  title="Charge Code Description"/>
		 <display:column property="contract" title="Contract" sortable="true" ></display:column>
		 <display:column  title="Percentage%" ><s:textfield  size="8" id="Percentage${commissionPlanList.id}" cssClass="input-text" cssStyle="text-align:right;" value="${commissionPlanList.percent}" onchange="updateCommissionPercent('${commissionPlanList.id}','${commissionPlanList.plan}','${commissionPlanList.charge}','${commissionPlanList.contract}',this);" onkeypress="onlyFloat(this);" name=""  /> </display:column>
		
		  <display:column title="Amount Basis">
		  <select id="commissionPlanAmount${commissionPlanList.id}" name ="commissionPlanAmount" class="list-menu" onchange="updateCommissionAmount('${commissionPlanList.id}','${commissionPlanList.plan}','${commissionPlanList.charge}','${commissionPlanList.contract}',this)"> 
			<c:forEach var="chrms" items="${amtList}" varStatus="loopStatus">
              <c:choose>
               <c:when test="${chrms == commissionPlanList.amountBasis}">
               <c:set var="selectedInd" value=" selected"></c:set>
               </c:when>
              <c:otherwise>
               <c:set var="selectedInd" value=""></c:set>
              </c:otherwise>
              </c:choose>
              <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
            	<c:out value="${chrms}"></c:out>
              </option>
            </c:forEach> 
		 </select>		  		 		 
		 </display:column>		 
    <display:setProperty name="paging.banner.item_name" value="commissionPlan"/>   
    <display:setProperty name="paging.banner.items_name" value="commissionPlan"/>   
  
    <display:setProperty name="export.excel.filename" value="Charges List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Charges List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Charges List.pdf"/>   
</display:table> 
<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="commissionPlanFile();" value="Add"/>
<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="deleteCommissionPlanId();" value="Delete"/>
  
  
  <c:if test="${hitFlag=='2'}">
		 <c:redirect url="/driverCommissionList.html?"/>
	</c:if>
</s:form>
<script type="text/javascript">
function commissionPlanFile(){
	/*var chckedVal="";
	var counter=0;
	var chkObj = document.forms['driverCommissionPlanList'].elements['commissionId'];
	if(chkObj.length>1){
		for(var i=0; i<chkObj.length; i++){
			if(chkObj[i].checked){
				counter=counter+1;
				chckedVal=chckedVal+","+chkObj[i].value;
			}
		}
		chckedVal = chckedVal.replace(chckedVal.charAt(0),'').trim();

		if(counter==1){
			location.href="editDriverCommission.html?id="+chckedVal;
		}else if(counter>1){
			alert('Select only one record')
		}else{
			location.href="editDriverCommission.html";
		}

	}else{
		var tt=document.forms['driverCommissionPlanList'].elements['commissionId'].value;
		var kk1=document.getElementById(tt).checked;
		if(kk1==true){
			location.href="editDriverCommission.html?id="+tt;
		}else{
			location.href="editDriverCommission.html";
		}
	}*/
	location.href="editDriverCommission.html";
}
function updateCommissionPercent(id,plan,charge,contract,target){
	var targetValue=target.value;
	var planAmount=document.getElementById('commissionPlanAmount'+id).value;
	var url="updateCommissionPercent.html?decorator=simple&popup=true&chargeValue="+encodeURI(charge)+"&planAmount="+encodeURI(planAmount)+"&contractValue="+encodeURI(contract)+"&percentValue="+encodeURI(targetValue)+"&planValue="+encodeURI(plan);
	http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponsePlanValue;
    http2.send(null);
}
function handleHttpResponsePlanValue(){
	
}
function updateCommissionAmount(id,plan,charge,contract,target){
	var targetValue=target.value;
	//alert(targetValue)
	var planPercentage=document.getElementById('Percentage'+id).value;
	var url="updateCommissionPercent.html?decorator=simple&popup=true&chargeValue="+encodeURI(charge)+"&planAmount="+encodeURI(targetValue)+"&contractValue="+encodeURI(contract)+"&percentValue="+encodeURI(planPercentage)+"&planValue="+encodeURI(plan);
	http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponsePlanValue;
    http2.send(null);
}
function handleHttpResponsePlanValue(){
	
}
var http2 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function deleteCommissionPlanId(){
	var chckedVal="";
	var chkObj = document.forms['driverCommissionPlanList'].elements['commissionId'];
	if(chkObj.length>1){
	for(var i=0; i<chkObj.length; i++){
		if(chkObj[i].checked){
			chckedVal=chckedVal+","+chkObj[i].value;
		}
	}
	chckedVal = chckedVal.replace(chckedVal.charAt(0),'').trim();
	}
	else{
	if(document.forms['driverCommissionPlanList'].elements['commissionId'].checked){
		chckedVal=document.forms['driverCommissionPlanList'].elements['commissionId'].value
	 }
	} 
	if(chckedVal==''){
			alert('Please check value to remove.');
			return false;
	}else{
		var type='Confirmation of deletion of the record.';
	 	var agree = confirm(type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
	 	if(agree){
			document.forms['driverCommissionPlanList'].elements['delCommissionId'].value= chckedVal;
		    var url = "deldriverCommission.html?decorator=popup&popup=true";
			document.forms['driverCommissionPlanList'].action = url;
			document.forms['driverCommissionPlanList'].submit();
			return true;
	 	}else{
	 		document.forms['driverCommissionPlanList'].elements['commissionId'].checked=false;
	 		return false;
	 	}
	}
}
function clear_fields(){
	document.forms['driverCommissionPlanList'].elements['driverCommissionPlan.description'].value="";
	document.forms['driverCommissionPlanList'].elements['driverCommissionPlan.plan'].value="";
	document.forms['driverCommissionPlanList'].elements['driverCommissionPlan.charge'].value="";
	document.forms['driverCommissionPlanList'].elements['driverCommissionPlan.contract'].value="";
}
</script>