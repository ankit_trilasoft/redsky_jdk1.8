<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Gl Rate Grid Form</title>   
    <meta name="heading" content="Gl Rate Grid Form"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
    </style>
    <script type="text/javascript">
    function checkMandatoryfield()
    {
		document.forms['glRateGridForm'].elements['associatedAcc'].value="NO";
    	var jo=document.forms['glRateGridForm'].elements['glCodeRateGrid.gridJob'].value;
    	var ro=document.forms['glRateGridForm'].elements['glCodeRateGrid.gridRouting'].value;
    	var companyDivision=document.forms['glRateGridForm'].elements['glCodeRateGrid.companyDivision'].value;
    	var rgl=document.forms['glRateGridForm'].elements['glCodeRateGrid.gridRecGl'].value;
    	var pgl=document.forms['glRateGridForm'].elements['glCodeRateGrid.gridPayGl'].value;
    	if((jo=="")&&(ro=='')&& (companyDivision =='')){
			alert("Please Select Either Job Type or Routing or companyDivision.");
			return false;
        }else if((rgl=="")&&(pgl=='')){
			alert("Please Select Either Receivable GL or Payable GL.");
			return false;
        }else if(document.forms['glRateGridForm'].elements['formStatus'].value=='2'){
        	var glCodeId= '${glCodeRateGrid.id}'
        	if(glCodeId!=''){
       		var chk = confirm('Do you want to update associated accountlines ?');
         	 		if(chk){
         	 			document.forms['glRateGridForm'].elements['associatedAcc'].value="YES";
          	 			return true;			
         	 		}else{
         	 			document.forms['glRateGridForm'].elements['associatedAcc'].value="NO";
         	 			return true;
         	 		}
         	 return true;
        	}else{
        		return true;
        	}
    	}else{
        	 return true;
        }            
    }
    function changeStatus() {
    	   document.forms['glRateGridForm'].elements['formStatus'].value = '2'; 
    	}
    </script>
</head>
<s:form id="glRateGridForm" action="saveGlRateGrid.html?decorator=popup&popup=true" onsubmit="return checkMandatoryfield();" method="post" validate="true">
	<div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Gl Rate Grid Details</span></a></li>
	  	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${glCodeRateGrid.id}&tableName=glcoderategrid&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	  </ul>
</div>
<div class="spn">&nbsp;</div>
	<s:hidden name="id" value="${glCodeRateGrid.id}" />
	<s:hidden name="glCodeRateGrid.id" />	
	<s:hidden name="glCodeRateGrid.costElementId" value="${costId}"/>
	<s:hidden name="associatedAcc" value="NO"/>
	<s:hidden name="formStatus"  />
	<s:hidden name="oldJobType" />
	<s:hidden name="oldRouting" />
    <s:hidden name="oldRecGl" />
	<s:hidden name="oldPayGl" />
	<div id="content" align="center" style="margin-bottom:7px;">
    <div id="liquid-round">
    <div class="top" style="!margin-top:2px;"><span></span></div>
    <div class="center-content">
    <table cellspacing="1" cellpadding="2" border="0">
	<tr>
	<td align="right" class="listwhitetext" >Company&nbsp;Division</td>
	<td><s:select name="glCodeRateGrid.companyDivision" list="%{companyDivis}" cssStyle="width:90px"  onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	<td class="listwhitetext" align="right">Job&nbsp;Type</td>
	<td style="width:150px;"><s:select name="glCodeRateGrid.gridJob" list="%{job}" onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	<td align="right" class="listwhitetext" >Routing</td>
	<td><s:select name="glCodeRateGrid.gridRouting" list="%{routing}" onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	</tr>
	<%--<tr>
	<td align="right" class="listwhitetext" >Company Division</td>
	<td><s:select name="glCodeRateGrid.companyDivision" list="%{companyDivis}" cssStyle="width:90px"  onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	</tr>
	--%><tr><td class="listwhitetext" align="right">Receivable&nbsp;GL</td><td><s:select name="glCodeRateGrid.gridRecGl" list="%{glcodes}"  cssStyle="width:90px"  onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	<td class="listwhitetext" >Payable&nbsp;GL</td>
	<td colspan=""><s:select name="glCodeRateGrid.gridPayGl" list="%{glcodes}" cssStyle="width:90px"  onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
	</tr>
	   
    </table>
</div><div class="bottom-header"><span></span></div></div></div>
<table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:680px; margin-bottom:10px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:60px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:108px">
								<fmt:formatDate var="portCreatedOnFormattedValue" value="${glCodeRateGrid.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="glCodeRateGrid.createdOn" value="${portCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${glCodeRateGrid.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						
						<td align="right" class="listwhitetext" width="63"><b><fmt:message key='customerFile.createdBy' /></b></td>
						
						<c:if test="${not empty glCodeRateGrid.id}">
							<s:hidden name="glCodeRateGrid.createdBy" />
							<td style="width:65px"><s:label name="createdBy" value="%{glCodeRateGrid.createdBy}" /></td>
						</c:if>
						<c:if test="${empty glCodeRateGrid.id}">
							<s:hidden name="glCodeRateGrid.createdBy" value="${pageContext.request.remoteUser}" />
							<td style="width:65px"><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<fmt:formatDate var="portUpdatedOnFormattedValue" value="${glCodeRateGrid.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<td style="width:66px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<s:hidden name="glCodeRateGrid.updatedOn"  value="${portUpdatedOnFormattedValue}"/>
						<td style="width:110px"><fmt:formatDate value="${glCodeRateGrid.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:66px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty glCodeRateGrid.id}">
							<s:hidden name="glCodeRateGrid.updatedBy" />
							<td ><s:label name="updatedBy" value="%{glCodeRateGrid.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty glCodeRateGrid.id}">
							<s:hidden name="glCodeRateGrid.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>
<s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick=""/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
</s:form>
<script type="text/javascript">
try{
	<c:if test="${hitflag2=='1'}">
		alert('Please enter proper values.');
	</c:if>
	<c:if test="${hitflag2=='2'}">
	alert("This combination of Job Type  & Routing  already exist.");
</c:if>	
<c:if test="${hitflag2=='0'}">
parent.window.opener.document.location.reload();
window.close();
</c:if>	
}catch(e){}
</script>