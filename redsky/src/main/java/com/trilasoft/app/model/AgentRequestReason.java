package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "agentRequestReason")

public class AgentRequestReason extends BaseObject {

	private Long id;

	private String corpID;
	
	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;

	private Long agentRequestId;
	
	private String comment;
	private String reason;
	private String counter;
	
	private AgentRequest agentRequest;


	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID)
			
				.append("updatedBy", updatedBy).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("agentRequestId", agentRequestId)
				.append("comment", comment).append("reason", reason)
				.append("counter", counter)
				
				.toString();
	}

	

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AgentRequestReason))
			return false;
		AgentRequestReason castOther = (AgentRequestReason) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(agentRequestId, castOther.agentRequestId)
				.append(comment, castOther.comment)
				.append(reason, castOther.reason)
				.append(counter, castOther.counter)
				
                .isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(updatedBy).append(createdBy)
				.append(createdOn).append(updatedOn)
				.append(agentRequestId)
				.append(comment)
				.append(reason).append(counter).toHashCode();
		
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getAgentRequestId() {
		return agentRequestId;
	}



	public void setAgentRequestId(Long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}



	public String getComment() {
		return comment;
	}



	public void setComment(String comment) {
		this.comment = comment;
	}



	public String getReason() {
		return reason;
	}



	public void setReason(String reason) {
		this.reason = reason;
	}



	public Date getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public Date getUpdatedOn() {
		return updatedOn;
	}



	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}



	

	

	@ManyToOne
	@JoinColumn(name="agentRequestId", updatable=false, insertable=false, referencedColumnName="id")
	public AgentRequest getAgentRequest() {
		return agentRequest;
	}



	public void setAgentRequest(AgentRequest agentRequest) {
		this.agentRequest = agentRequest;
	}



	public String getCounter() {
		return counter;
	}



	public void setCounter(String counter) {
		this.counter = counter;
	}


}
