<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Claim Item List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="losss" class="table" requestURI="" id="lossList" export="false" defaultsort="1" >
		<display:column titleKey="loss.idNumber1"><a onclick="goToUrlChild(${lossList.id});" style="cursor:pointer"><c:out value="${lossList.idNumber1}" /></a></display:column> 
		<display:column property="lossType" titleKey="loss.lossType" />
		<display:column property="warehouse" title="Warehouse" maxLength="6"  style="width:60px"/>
	</display:table>
</div>