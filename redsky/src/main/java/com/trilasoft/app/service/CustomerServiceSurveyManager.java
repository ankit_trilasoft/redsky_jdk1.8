package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CustomerServiceSurvey;
import com.trilasoft.app.model.Miscellaneous;

public interface CustomerServiceSurveyManager extends GenericManager<CustomerServiceSurvey, Long> {
	public Map<String,String> getCustomerServiceList(Long id);

}
