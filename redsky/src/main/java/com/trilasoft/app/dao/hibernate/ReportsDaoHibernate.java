package com.trilasoft.app.dao.hibernate;

import groovy.ui.SystemOutputInterceptor;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.hibernate.Hibernate;

import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ReportsFieldSection;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.dao.ReportsDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
  
public class ReportsDaoHibernate extends GenericDaoHibernate<Reports, Long> implements ReportsDao {   
	private HibernateUtil hibernateUtil;
	public ReportsDaoHibernate() {   
        super(Reports.class);   
	} 
	public List findByModule(String module, String corpID) { 
	   	List reports = null;
	   	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu,docsxfer,pdf";
	   	reports = getHibernateTemplate().find("select " + columns + " from Reports where module = '" + module + "' AND corpID = '" + corpID +  "' AND formReportFlag = 'F' AND enabled = 'Yes' AND subModule NOT IN ('Claims','WorkTicket')");
	   	try {
	   		reports= hibernateUtil.convertScalarToComponent(columns, reports, Reports.class);
	   	} catch (Exception e) {
            e.printStackTrace();
	   	}
		return reports;
    
   ///     return getHibernateTemplate().find("from Reports where module like '" + module + "%' AND corpID like '" + corpID +  "%' AND formReportFlag = 'F' AND enabled = 'Yes' AND subModule NOT IN ('Claims','WorkTicket')");   
	}
	
	public List findByCriteria(String module, String subModule, String menu, String reportName, String description, String corpID, String type){
		return getHibernateTemplate().find("from Reports where module like '" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' AND subModule like '" + subModule.replaceAll("'", "''").replaceAll(":", "''") + "%' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND reportName like '%" +reportName.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID like '" + corpID.replaceAll("'", "''") +  "%' AND formReportFlag like '"+type+"%'");
	}
    
    public List findByModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String routingfilter){
		//return getHibernateTemplate().find("from Reports where module = '" + module+ "' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID = '" + corpID + "' AND formReportFlag = 'F' AND enabled = '"+enabled +"'");
    	String billToCodeCondition="";
	   	String billToCodes ="";
	   	if(billToCode==null){
	   	 billToCodes="";
	   	 billToCodeCondition = "(billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null)";
	   	} else {
	   	 billToCodes=billToCode;
	   	 billToCodeCondition = "(((billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null) and excludefromform is false)"
	   							+ " or ((billToCode not like '%"+billToCodes+ "%') and excludefromform is true))";
	   	}
		List reports = new ArrayList();;
	   	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu, docsxfer, pdf, rtf, runTimeParameterReq,docx";
	  	List reportList = this.getSession().createSQLQuery("select " + columns + ",'D' as type,emailOut,emailBody from reports " +
	  			"where module = '" + module+ "' AND subModule = '"+ subModule + "' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID = '" + corpID + "' AND formReportFlag='F' AND enabled like 'Yes' AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND (routing like '%"+routingfilter+ "%' or routing ='') AND (job like '%"+ jobType + "%' or job ='' or job is null) AND "+billToCodeCondition+" AND (mode like '%"+modes+ "%' or mode ='' or mode is null)  " +
	  			"UNION " +
	  			"select " + columns + ",'R' as type,emailOut,emailBody from reports " +
	  			"where module = '" + module+ "' AND subModule != '"+ subModule + "' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID = '" + corpID + "' AND formReportFlag='F' AND enabled like 'Yes' AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND (routing like '%"+routingfilter+ "%' or routing ='') AND (job like '%"+ jobType + "%' or job ='' or job is null) AND "+billToCodeCondition+" AND (mode like '%"+modes+ "%' or mode ='' or mode is null) order by 14,3,7")
	   	  .addScalar("id", Hibernate.LONG)
		  .addScalar("module", Hibernate.STRING)
		  .addScalar("subModule", Hibernate.STRING)
		  .addScalar("corpID", Hibernate.STRING)
		  .addScalar("formReportFlag", Hibernate.STRING)
		  .addScalar("enabled", Hibernate.STRING)
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("reportComment", Hibernate.STRING)
		  .addScalar("menu", Hibernate.STRING)
		  .addScalar("docsxfer", Hibernate.STRING)
		  .addScalar("pdf", Hibernate.STRING)
		  .addScalar("rtf", Hibernate.STRING)
		  .addScalar("runTimeParameterReq", Hibernate.STRING)
		  .addScalar("type", Hibernate.STRING)
		  .addScalar("emailOut",Hibernate.STRING)
		  .addScalar("emailBody", Hibernate.STRING)
		  .addScalar("docx",Hibernate.STRING)
		  .list();
	   	Iterator it=reportList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setModule(row[1].toString());
			dTO.setSubModule(row[2].toString());
			dTO.setCorpID(row[3].toString());
			dTO.setFormReportFlag(row[4].toString());
			dTO.setEnabled(row[5].toString());
			dTO.setDescription(row[6].toString());
			dTO.setReportComment(row[7].toString());
			dTO.setMenu(row[8].toString());
			dTO.setDocsxfer(row[9].toString());
			dTO.setPdf(row[10].toString());
			dTO.setRtf(row[11].toString());
			dTO.setParamReq(row[12].toString());
			dTO.setType(row[13].toString());
			dTO.setEmailOut(row[14].toString());
			if(row[15]!=null){dTO.setEmailBody(row[15].toString());}else{dTO.setEmailBody("");}
			dTO.setDocx(row[16].toString());
			reports.add(dTO);
	       } 

		return reports;
    }
    
    public List findByRelatedModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled){
    		return getHibernateTemplate().find("from Reports where subModule like '%' AND module = '" + module+ "' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID = '" + corpID + "' AND formReportFlag = 'F' AND enabled = '"+enabled +"' AND subModule NOT IN ('Claims','WorkTicket')");
    }
    
    public JasperReport getReportTemplate(Long reportId) {
    		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    		User user = (User)auth.getPrincipal();
    		String sessionCorpID = user.getCorpID().toUpperCase();
		JasperReport jasperReport = null;
		Reports report = this.get(reportId);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/jasper");
			jasperReport = JasperCompileManager.compileReport(path + "//"+sessionCorpID+"//" + report.getReportName());
		} catch (JRException e) {
			e.printStackTrace();
		}
	
		return jasperReport;
	}
    
    public JasperPrint generateReport(Long reportId, Map parameters) { 
    		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    		User user = (User)auth.getPrincipal();
    		String sessionCorpID = user.getCorpID().toUpperCase();
		JasperReport jasperReport;
		JasperPrint jasperPrint = null;
		Reports report = this.get(reportId);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/jasper");
			jasperReport = JasperCompileManager.compileReport(path + "//"+sessionCorpID+"//" + report.getReportName());
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, this.getSession().connection());
			System.out.println("Jasper Print.........................."+jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return jasperPrint;
    }

	public JasperPrint generateReport(Long reportId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		String sessionCorpID = user.getCorpID().toUpperCase();
		JasperReport jasperReport;
		JasperPrint jasperPrint = null;
		Reports report = this.get(reportId);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/jasper");
			jasperReport = JasperCompileManager.compileReport(path + "//"+sessionCorpID+"//" + report.getReportName());
			jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), this.getSession().connection());
			
		} catch (JRException e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}

	public List findByCorpID(String corpID) {
		return getHibernateTemplate().find("from Reports where corpID=?",corpID);
	}
	
	public List findByEnabled(String module , String menu, String subModule, String description, String sessionCorpID,String userName, Set<Role> roles,Boolean SalesPortalAccess){
		String roleList = "";
		for (Role role : roles){
			if (!roleList.equals("")) roleList += ",";
			if(role.getName().toString().equalsIgnoreCase("ROLE_SALES_PORTAL")){
				if(SalesPortalAccess.equals(true)){
					description="Client Summary";
				}else{
					description="Commission Report With Lock";	
				}
				
			}
			roleList += "'" + role.getId() + "'" ;
		}
		List list1 = new ArrayList();		
			List reportList = this.getSession().createSQLQuery("select r.id, if(b.reportId is null || b.reportId ='null' ,'', b.reportId), if(b.id is null || b.id ='null' ,'', b.id) ,r.description, r.module, r.reportName, r.reportComment, r.subModule, r.docsxfer, r.menu, r.createdBy, if(b.createdBy is null || b.createdBy='null' ,'',b.createdBy) " +
					"FROM reports r " +
					"LEFT OUTER JOIN reportbookmark b on r.id=b.reportId " +
					"LEFT OUTER JOIN reports_role repRole on r.id=repRole.reports_id " +
					"WHERE module like '%" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
					"AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
					"AND subModule like '%" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
					"AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' " + 
					"AND r.corpID = '"+ sessionCorpID+"' and  r.formReportFlag='R' and enabled='yes' and (repRole.role_id in("+roleList+") or repRole.role_id is null) group by r.id")
			  .addScalar("r.id", Hibernate.LONG)
			  .addScalar("if(b.id is null || b.id ='null' ,'', b.id)", Hibernate.LONG)
			  .addScalar("r.description", Hibernate.STRING)
			  .addScalar("r.module", Hibernate.STRING)
			  .addScalar("r.reportName", Hibernate.STRING)
			  .addScalar("r.reportComment", Hibernate.STRING)
			  .addScalar("r.subModule", Hibernate.STRING)
			  .addScalar("r.docsxfer", Hibernate.STRING)
			  .addScalar("r.menu", Hibernate.STRING)
			  .addScalar("r.createdBy", Hibernate.STRING)
			  .list();
		   	Iterator it=reportList.iterator();
			DTO dTO=null;
			while(it.hasNext()){
				Object []row= (Object [])it.next();
				dTO=new DTO();
				dTO.setId(Long.parseLong(row[0].toString()));
				dTO.setBid(Long.parseLong(row[1].toString()));
				dTO.setDescription(row[2].toString());
				dTO.setModule(row[3].toString());
				dTO.setReportName(row[4].toString());
				dTO.setReportComment(row[5].toString());
				dTO.setSubModule(row[6].toString());
				dTO.setDocsxfer(row[7].toString());
				dTO.setMenu(row[8].toString());
				dTO.setCreatedBy(row[9].toString());
				
				List commName=this.getSession().createSQLQuery("select b.createdBy from reportbookmark b,reports r where b.createdBy='"+userName+"' and b.reportId='"+row[0]+"'").addScalar("b.createdBy", Hibernate.STRING).list();
				if(!commName.isEmpty()){
					dTO.setBkMarkcreatedBy(commName.get(0).toString());
				}
				List reportId=this.getSession().createSQLQuery("select b.reportId from reportbookmark b where b.createdBy='"+userName+"' and b.reportId='"+row[0]+"'").addScalar("b.reportId", Hibernate.LONG).list();
				if(!reportId.isEmpty()){
					dTO.setBrid(reportId.get(0).toString());
				}
				list1.add(dTO);
		       }
			return list1;
		
		}
		//		return getHibernateTemplate().find("from Reports where formReportFlag='R' AND enabled=?","Yes");
	
	
	/*public Map<String, String> findByDocsxfer(String enabled, String docsxfer){
		return getHibernateTemplate())).findByDocsxfer("from Reports where enabled=?","Yes" + "AND docsxfer=?", "Yes");
	}*/
	
	public List findByDocsxfer(){
		return getHibernateTemplate().find("from Reports where docsxfer=?","Yes");
	}
	
	public List findByFormFlag(String userName ,String module , String menu, String subModule, String description, String sessionCorpID){
		List list1 = new ArrayList();
		List reportList =this.getSession().createSQLQuery("select r.id, if(b.reportId is null || b.reportId ='null' ,'', b.reportId) as brid, if(b.id is null || b.id ='null' ,'', b.id) as bid ,r.description, r.module, r.reportName, r.reportComment, r.subModule, r.docsxfer, r.menu, r.createdBy, if(b.createdBy is null || b.createdBy='null' ,'',b.createdBy) as bkMarkcreatedBy " +
				"FROM reports r " +
				"LEFT OUTER JOIN reportbookmark b on r.id=b.reportId and b.createdBy='"+userName+"' " +
				"WHERE module like '%" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
				"AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' " +
				"AND subModule like '%" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
				"AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' " +
				"AND r.corpID ='"+ sessionCorpID+"' and  r.formReportFlag='F' and enabled='Yes' group by r.id")
		  .addScalar("r.id", Hibernate.LONG)
		  .addScalar("brid", Hibernate.LONG)
		  .addScalar("bid", Hibernate.LONG)
		  .addScalar("r.description", Hibernate.STRING)
		  .addScalar("r.module", Hibernate.STRING)
		  .addScalar("r.reportName", Hibernate.STRING)
		  .addScalar("r.reportComment", Hibernate.STRING)
		  .addScalar("r.subModule", Hibernate.STRING)
		  .addScalar("r.docsxfer", Hibernate.STRING)
		  .addScalar("r.menu", Hibernate.STRING)
		  .addScalar("r.createdBy", Hibernate.STRING)
		  .addScalar("bkMarkcreatedBy", Hibernate.STRING)
		  .list();
		Iterator it=reportList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setBrid(Long.parseLong(row[1].toString()));
			dTO.setBid(Long.parseLong(row[2].toString()));
			dTO.setDescription(row[3].toString());
			dTO.setModule(row[4].toString());
			dTO.setReportName(row[5].toString());
			dTO.setReportComment(row[6].toString());
			dTO.setSubModule(row[7].toString());
			dTO.setDocsxfer(row[8].toString());
			dTO.setMenu(row[9].toString());
			dTO.setCreatedBy(row[10].toString());
			dTO.setBkMarkcreatedBy(row[11].toString());
			list1.add(dTO);
	       }
		return list1;
		
		//return getHibernateTemplate().find("from Reports where formReportFlag='F' AND enabled=?","Yes");
	}
	
	public List findBySubModule(String module, String subModule, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes) {   
	   	List reports = null;
	   	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu,docsxfer,pdf";
	   	//reports = getHibernateTemplate().find("select " + columns + " from Reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes'");
		reports = getHibernateTemplate().find("select " + columns + " from Reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND corpID = '" + corpID + "'AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND (job like '%"+ jobType + "%' or job ='' or job is null) AND (billToCode like '%"+billToCode+ "%' or billToCode ='' or billToCode is null) AND (mode like '%"+modes+ "%' or mode ='' or mode is null) AND formReportFlag='F' AND enabled like 'Yes'");
		 try {
	   		reports= hibernateUtil.convertScalarToComponent(columns, reports, Reports.class);
	   	} catch (Exception e) {
            e.printStackTrace();
	   	}
		return reports;
         //// return getHibernateTemplate().find("from Reports where module like '" + module + "%' AND subModule like '"+ subModule + "%'AND corpID like '" + corpID +  "%' AND formReportFlag='F' AND enabled like '" + enabled + "%'");   
	}
	
	public List findBySubModule1(String module, String subModule, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String recInvNumb,String jobNumber){
		
		
		
			String columns = "id, module, subModule, corpID, companyDivision, job, formReportFlag, enabled, description, reportComment, menu,docsxfer,pdf,rtf,runTimeParameterReq,emailOut,docx";
			String billToCodeCondition="";
		   	String billToCodes ="";
		   	String getId="";
		   	String tableNameFromSubModules="";
		   	String reportFieldName="";
		   	String reportFieldValue="";
		   	String tableName="";
		   	String columnName="";
		   	String fieldVal="";
		   	String dataType="";
		   	String ControlParamterId="";
		   	String ControlParamterNotId="";
		   	String controlParamter="";
		   	String controlNotParamter="";
		   	boolean excuteParameter=false;
		   	boolean excuteParameterNotId=false;
		   	try{
		   		if(billToCode==null)
		   		{
		   			billToCodes="";
		   			billToCodeCondition = "(billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null)";
		   		}else 
		   		{
		   			billToCodes=billToCode;
		   			billToCodeCondition = "(((billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null) and excludefromform is false)"
		   							+ " or ((billToCode not like '%"+billToCodes+ "%') and excludefromform is true))";
		   		}
			
			
		   		String reportIdQuery="select id from reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND corpID = '" + corpID + "'AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND (job like '%"+ jobType + "%' or job ='' or job is null) AND "+billToCodeCondition+" AND (mode like '%"+modes+ "%' or mode ='' or mode is null) AND formReportFlag='F' AND enabled like 'Yes' ";
		   		List reportId =this.getSession().createSQLQuery(reportIdQuery).list();
				if(reportId.size()>0 && !reportId.isEmpty() && reportId!=null)
				{
						Iterator itr = reportId.iterator();
						while(itr.hasNext())
						{
							if(getId.equals(""))
							{
								getId = "'"+itr.next().toString()+"'";
							}
							else
							{
								getId = getId+","+"'"+itr.next().toString()+"'";
							}
						}
				}
		
				if(getId.equals(""))
				{
					getId="'"+getId+"'";
				}
				if(subModule.equals("Accounting"))
				{
					tableNameFromSubModules="accountline";
				}
				
				else if(subModule.equals("workTicket"))
				{
					tableNameFromSubModules="workticket";
				}
				else {
			
				}
		
				String fieldsectionId="from ReportsFieldSection where corpId='"+corpID+"' and reportid in("+getId+") and type='controll' ";
				List<ReportsFieldSection> list=getHibernateTemplate().find(fieldsectionId);
				// For Loop Start 
				for(ReportsFieldSection reportsFieldSection : list)
				{
					reportFieldName=reportsFieldSection.getFieldname();
					reportFieldValue=reportsFieldSection.getFieldvalue();
					if(reportFieldName!=null && !reportFieldName.equals(""))
						{
							String[] reportSection=reportFieldName.split("\\."); 
								tableName=reportSection[0];
								columnName=reportSection[1];
						}
			
					try{
						String a1="";
						if(columnName !=null && columnName.equalsIgnoreCase("creditInvoiceNumber")){
							columnName="sum(actualRevenue)";
						}
						a1="select "+columnName+" from "+tableNameFromSubModules+"  where corpid='"+corpID+"' and shipNumber='"+jobNumber+"' and recInvoiceNumber='"+recInvNumb+"' ";	
						List al=this.getSession().createSQLQuery(a1).list();
						if(al!=null && (!(al.isEmpty())) && al.get(0)!=null && (!(al.get(0).toString().trim().equals(""))))
							{
								fieldVal	=al.get(0).toString().trim();
							}
						}
					catch(NullPointerException np){
						np.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
					if(columnName!=null && !columnName.equals("") && columnName.equalsIgnoreCase("sum(actualRevenue)")){
						
					}else{
					try{
					String dataTypeQuery="select data_type from information_schema.columns where table_schema = 'redsky' and table_name = '"+tableNameFromSubModules+"' and column_name = '"+columnName+"'";
					List a2=this.getSession().createSQLQuery(dataTypeQuery).list();
					if(a2!=null && (!(a2.isEmpty())) && a2.get(0)!=null && (!(a2.get(0).toString().trim().equals(""))))
					{
						dataType	=a2.get(0).toString().trim();
					}
					if(fieldVal!=null || !fieldVal.equalsIgnoreCase("")){
						if(dataType.equals("datetime")  && !(reportFieldValue.equalsIgnoreCase("not null")|| reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NOT NULL")|| reportFieldValue.equalsIgnoreCase("NULL") || reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equalsIgnoreCase(" ") || reportFieldValue.equalsIgnoreCase("")))
						{
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							Date date = format.parse(fieldVal);
					   
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
							StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(date) );
							fieldVal= nowYYYYMMDD.toString();
						}
						}}catch(NullPointerException np){
						np.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
					
					if(fieldVal.equalsIgnoreCase("TRUE") || fieldVal.equalsIgnoreCase("True") || fieldVal.equalsIgnoreCase("true")){
						fieldVal="true";
					}
					if(reportFieldValue.equalsIgnoreCase("TRUE") || reportFieldValue.equalsIgnoreCase("True") || reportFieldValue.equalsIgnoreCase("true")){
						reportFieldValue="true";
					}
					if(fieldVal.equalsIgnoreCase("FALSE") || fieldVal.equalsIgnoreCase("False") || fieldVal.equalsIgnoreCase("false")){
						fieldVal="false";
					}
					if(reportFieldValue.equalsIgnoreCase("FALSE") || reportFieldValue.equalsIgnoreCase("False") || reportFieldValue.equalsIgnoreCase("false")){
						reportFieldValue="false";
					}
					
			
					if(fieldVal==null || fieldVal.equalsIgnoreCase("") || fieldVal.equalsIgnoreCase("false"))
					{
						if(reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NULL")||  reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equals("' '") || reportFieldValue.equals("''") || reportFieldValue.equalsIgnoreCase("false"))
						{
							reportFieldValue=fieldVal;
						}
					}
					}
					if(columnName!=null && !columnName.equals("") && columnName.equalsIgnoreCase("sum(actualRevenue)")){

						if((fieldVal!=null) && !(fieldVal.equalsIgnoreCase(""))){
							if(reportFieldValue!=null){
						
							
									if(!(reportFieldValue.equals("' '"))){
								if(!(reportFieldValue.equals("''"))){
							if(reportFieldValue.equalsIgnoreCase("not null") || reportFieldValue.equalsIgnoreCase("NOT NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
								if(fieldVal!=null && (!(fieldVal.equals(""))) && Double.parseDouble(fieldVal)<0){
								reportFieldValue=fieldVal;
								}
							}
							if(reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
								if(fieldVal!=null && (!(fieldVal.equals(""))) && Double.parseDouble(fieldVal)>0){
								reportFieldValue=fieldVal;
								}
							}
							}
							} 
							}
							}
						
					}else{
					if((fieldVal!=null) && !(fieldVal.equalsIgnoreCase(""))){
						if(reportFieldValue!=null){
						if(!reportFieldValue.equalsIgnoreCase("null")){
							if(!reportFieldValue.equalsIgnoreCase("NULL")){
								if(!(reportFieldValue.equals("' '"))){
							if(!(reportFieldValue.equals("''"))){
						if(reportFieldValue.equalsIgnoreCase("not null") || reportFieldValue.equalsIgnoreCase("NOT NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
							reportFieldValue=fieldVal;
						}
						}
						}
						}
						}
						}
						}
					}
					
					if(columnName!=null && !columnName.equals(""))
					{
						if(fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
						{
							if(ControlParamterId.equals("")){
								ControlParamterId = ""+reportsFieldSection.getReportId().toString()+"";
							}
							else{
								ControlParamterId = ControlParamterId+","+""+reportsFieldSection.getReportId().toString()+"";
							}
				
						}else
						{
							if(!fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
							{
								if(ControlParamterNotId.equals(""))
								{
									ControlParamterNotId = ""+reportsFieldSection.getReportId().toString()+"";
								}
								else
								{
									ControlParamterNotId = ControlParamterNotId+","+""+reportsFieldSection.getReportId().toString()+"";
								}
							}
						}
					}
					fieldVal="";
				}// For Loop Stop
		
				Set<String> parameterSet=new TreeSet<String>();
				if(ControlParamterId!=null && !ControlParamterId.equals(""))
				{
					String[] input =ControlParamterId.split(",");
					for( int i = 0; i <= input.length-1; i++)
						{
							String element = input[i];
							parameterSet.add(element);
						}
				}else{
			
				}
			Iterator itr1 = parameterSet.iterator();
			while(itr1.hasNext())
			{
				if(controlParamter.equals(""))
				{
					controlParamter = "'"+itr1.next().toString()+"'";
					excuteParameter=true;
				}
				else
				{
					controlParamter = controlParamter+","+"'"+itr1.next().toString()+"'";
					excuteParameter=true;
				}
			}
			if(controlParamter.equals(""))
			{
				controlParamter="'"+controlParamter+"'";
			}
		
		
			Set<String> parameterNotSet=new TreeSet<String>();
			if(ControlParamterNotId!=null && !ControlParamterNotId.equals(""))
			{
				String[] input =ControlParamterNotId.split(",");
			for( int j = 0; j <= input.length-1; j++)
			{
			    String element = input[j];
			    parameterNotSet.add(element);
			}
			}else{
			
			}
			Iterator itr2 = parameterNotSet.iterator();
			while(itr2.hasNext())
			{
				if(controlNotParamter.equals(""))
				{
					controlNotParamter = "'"+itr2.next().toString()+"'";
					excuteParameterNotId=true;
				}
				else
				{
					controlNotParamter = controlNotParamter+","+"'"+itr2.next().toString()+"'";
					excuteParameterNotId=true;
				}
			}
			if(controlNotParamter.equals(""))
			{
				controlNotParamter="'"+controlNotParamter+"'";
			}
		
		}catch(NullPointerException np){
			if(controlNotParamter.equals(""))
			{
				controlNotParamter="'"+controlNotParamter+"'";
			}
			if(controlParamter.equals(""))
			{
				controlParamter="'"+controlParamter+"'";
			}
			np.printStackTrace();
		}
		   	catch(Exception e){
		   		if(controlNotParamter.equals(""))
				{
					controlNotParamter="'"+controlNotParamter+"'";
				}
		   		if(controlParamter.equals(""))
				{
					controlParamter="'"+controlParamter+"'";
				}
			e.printStackTrace();
		}
		
		List reports = null;
		String query="";
	   	try{
	   	 query="select " + columns + " from Reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND corpID = '" + corpID + "'AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND ";
	   			query=query+ "(job like '%"+ jobType + "%' or job ='' or job is null) AND "+billToCodeCondition+" AND (mode like '%"+modes+ "%' or mode ='' or mode is null) " ;
	   			query=query+ "AND formReportFlag='F' AND enabled like 'Yes'  ";
	   			if(excuteParameter && !excuteParameterNotId){
	   				query=query+ " And id in("+controlParamter+") ";
	   			}else if (!excuteParameter && excuteParameterNotId)
	   			{
				  query=query+ " And id not in("+controlNotParamter+") ";
	   			}
	   			else if (excuteParameter && excuteParameterNotId)
	   			{
				  query=query+ " And id not in("+controlNotParamter+") And id in("+controlParamter+") ";
	   			}
			  else{
				  query=query+ "And id in( case when "+controlParamter+"="+controlParamter+" then id else "+controlParamter+" end) ";
						  
				  		
			  }
	   			query=query + "order by case when agentroles is not null and agentroles<>''  then agentroles else 'zzzzz' end,case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end  ";
	   	
	   	}catch(Exception e){
	   		query="";
	   		query="select " + columns + " from Reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND corpID = '" + corpID + "'AND (companyDivision like '%"+ companyDivision + "%' or companyDivision ='' or companyDivision is null) AND ";
   			query=query+ "(job like '%"+ jobType + "%' or job ='' or job is null) AND "+billToCodeCondition+" AND (mode like '%"+modes+ "%' or mode ='' or mode is null) " ;
   			query=query+ "AND formReportFlag='F' AND enabled like 'Yes' order by case when agentroles is not null and agentroles<>''  then agentroles else 'zzzzz' end ,case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end  ";
	   		e.printStackTrace();
	   		
	   	}
	   	reports = getHibernateTemplate().find(query);
	   	try {
	   		reports= hibernateUtil.convertScalarToComponent(columns, reports, Reports.class);
	   	} catch (Exception e) {
            e.printStackTrace();
	   	}
		return reports;
         //// return getHibernateTemplate().find("from Reports where module like '" + module + "%' AND subModule like '"+ subModule + "%'AND corpID like '" + corpID +  "%' AND formReportFlag='F' AND enabled like '" + enabled + "%'");   
	}
	
	public List findReportByUserCriteria(String module, String menu, String subModule, String description, String corpID){
		return getHibernateTemplate().find("from Reports where module like '" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' AND menu like '" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND subModule like '" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND description like '" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID like '" + corpID.replaceAll("'", "''") +  "%' AND enabled = 'Yes' AND formReportFlag='R'");
	}
	
	public List findFormByUserCriteria(String module, String menu, String subModule, String description, String corpID){
		return getHibernateTemplate().find("from Reports where module like '%" + module.replaceAll("'", "''").replaceAll(":", "''") + "%' AND menu like '%" + menu.replaceAll("'", "''").replaceAll(":", "''") + "%' AND subModule like '%" +subModule.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND description like '%" +description.replaceAll("'", "''").replaceAll(":", "''")  + "%' AND corpID like '" + corpID.replaceAll("'", "''") +  "%' AND enabled = 'Yes' AND formReportFlag='F'");
	}
	
	public List findByModuleCoordinator(String sessionCorpID) {
		return this.getSession().createSQLQuery("select upper(a.username) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id AND a.corpID='"+sessionCorpID+"' and r.name='ROLE_COORD' and a.account_enabled=true order by upper(a.username) ASC").list();
		//return getHibernateTemplate().find("select upper(a.username) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id AND a.corpID='"+sessionCorpID+"' and r.name='ROLE_COORD' and a.account_enabled=true order by upper(a.username) ASC");
	}  
	
	public List findByModuleEstimator(String sessionCorpID) {
		return getHibernateTemplate().find("select estimator from ServiceOrder where corpID='"+sessionCorpID+"' AND estimator NOT IN('','null') GROUP BY estimator ORDER BY estimator ASC");
	}
	
	public List findByCompanyCode(String sessionCorpID) {
		return getHibernateTemplate().find("select distinct companyCode FROM CompanyDivision where corpID='"+sessionCorpID+"'");
	} 
	
	public List findContract(String sessionCorpID) {
		return getHibernateTemplate().find("SELECT contract FROM Contract  where corpID='"+sessionCorpID+"' order by contract");
	} 
		
	public Map<String,String> findByModuleReports(String sessionCorpID) {
		List<RefMaster> list;
		list =   getHibernateTemplate().find("from RefMaster where  parameter='HOUSE'" );
		Map<String,String> parameterMap = new LinkedHashMap<String,String>();
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode() , refMaster.getCode()+" : "+ refMaster.getDescription());
		}
		return parameterMap;
	}
	
	public List getReportsValidationFlag(String sessionCorpID) {
		return getHibernateTemplate().find("select reportValidationFlag from Company where corpID='"+sessionCorpID +"' ");
	}

	public Map<String, String> findByPayableStatus() {
		String blankKey="-";
		String blankValue="blank";
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='PAY_STATUS'");
		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		parameterMap.put(blankKey, blankValue);
		return parameterMap;
		
	}
	
	public Map<String, String> findByPayingStatus() {
		String blankKey="-";
		String blankValue="Not Approved/Posted";
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='ACC_STATUS'");
		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		parameterMap.put(blankKey, blankValue);
		return parameterMap;
		
	}

	public Map<String,String> findByCommodity(String sessionCorpID) {
	    List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='COMMODITS' ");

	    Map<String, String> parameterMap = new TreeMap<String, String>();

	    for(RefMaster refMaster : list){
		    parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	    }
	    return parameterMap;
	  }
	
	public Map<String,String> findByCommodities(String sessionCorpID) {
	    List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter in ('COMMODITS','COMMODIT') group by code ");

	    Map<String, String> parameterMap = new TreeMap<String, String>();

	    for(RefMaster refMaster : list){
		    parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	    }
	    return parameterMap;
	  }

	
	public Map<String, String> findByOmni() {
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='OMNI'");
		Map<String, String> parameterMap = new TreeMap<String, String>();
		
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	}


	public List findByMode() {
		return getHibernateTemplate().find("select mode from ServiceOrder where mode in('Air', 'SEA') GROUP BY mode");
	}

	
	public List findCrewName(String sessionCorpID) {
		return getHibernateTemplate().find("select distinct crewName from TimeSheet where corpID='"+sessionCorpID+"' and workdate >= '2009-01-01' order by 1");
	}
	public List findCrewType(String sessionCorpID) {
		return getHibernateTemplate().find("select distinct crewType from TimeSheet where corpID='"+sessionCorpID+"' and workdate >= '2009-01-01' order by 1");
	}
	
	public List findUserRoles(String corpID){
	    	return getHibernateTemplate().find("select name from Role where corpID=?",corpID);
	}

	public Map<String, String> findByRouting(String sessionCorpID) {
	    String blankKey="%";
	    String blankValue="All";
	    List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='ROUTING' AND description!='' ");

	    Map<String, String> parameterMap = new TreeMap<String, String>();

	    for(RefMaster refMaster : list){
		    parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	    }
	    parameterMap.put(blankKey, blankValue);
	    return parameterMap;
	}
	
	
	public Map<String, String> findByBank(String sessionCorpID) {
	    List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='BANKCODE' AND description!='' ");
	    Map<String, String> parameterMap = new TreeMap<String, String>();
	    for(RefMaster refMaster : list){
		    parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	    }
	   return parameterMap;
	}

	public void saveReport(Reports reports) {
		getHibernateTemplate().saveOrUpdate(reports);
		
	}

	public Map<String, String> findByCurrency() {
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='CURRENCY' AND description!=''");

		Map<String, String> parameterMap = new TreeMap<String, String>();

		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	     }
		return parameterMap;
	}

	public Map<String, String> findByJob(String sessionCorpID) {
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='JOB' AND description!='' ");

		Map<String, String> parameterMap = new TreeMap<String, String>();

		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	     }
		return parameterMap;
	}
	
	public Map<String, String> findByBaseScore(String sessionCorpID) {
		String blankKey="%";
	    String blankValue="All";
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='MILITARY' AND description!='' ");

		Map<String, String> parameterMap = new TreeMap<String, String>();

		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
	     }
		parameterMap.put(blankKey, blankValue);
		return parameterMap;
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public String getDefaultStationary(String jobNumber, String invoiceNumber,String sessionCorpID){
		String defaultStationary="";
		if(jobNumber!=null && jobNumber.equals("")){
			List reportListdefault =	this.getSession().createSQLQuery("select case when p.agentParent is not null and p.agentparent<>'' then (select if((emailPrintOption is null or emailPrintOption=''),(select emailPrintOption from partnerprivate where partnercode=p.partnercode and corpid='"+sessionCorpID+"' ),emailPrintOption) from partnerprivate where partnercode=p.agentParent and corpid='"+sessionCorpID+"') when(p.agentParent is  null or p.agentparent='') then (select emailPrintOption from partnerprivate where partnercode=p.partnercode and corpid='"+sessionCorpID+"') end as 'emailPrintOption' from partnerprivate pp ,partnerpublic p where pp.partnercode='"+invoiceNumber+"'  and pp.corpid='UTSI' and pp.partnercode=p.partnercode ").list();	
		 	if(reportListdefault!=null && !reportListdefault.isEmpty()){
			defaultStationary=reportListdefault.get(0).toString();
		  }
		}else{
		List reportList =	this.getSession().createSQLQuery("select distinct p.emailPrintOption from accountline a left outer join partnerprivate p on a.billToCode=p.partnercode where a.recInvoiceNumber='"+invoiceNumber+"' and p.emailPrintOption is not null and p.emailPrintOption!='' and a.shipnumber='"+jobNumber+"' and a.corpid='"+sessionCorpID+"' ").list();
		if(reportList!=null && !reportList.isEmpty()){
			defaultStationary=reportList.get(0).toString();
		}
		}
		return defaultStationary;
	}
	public String generateReportExtract(Long reportId, Map parameters) { 
		String query ="";
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	     User user = (User)auth.getPrincipal();
	     String sessionCorpID = user.getCorpID().toUpperCase();
		JasperReport jasperReport;
		Reports report = this.get(reportId);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/jasper");
			jasperReport = JasperCompileManager.compileReport(path + "//"+sessionCorpID+"//" + report.getReportName());
			query = jasperReport.getQuery().getText();
			while (query.indexOf("$P") > 0){
	                int paramPos = query.indexOf("$P");
	                String paramName = query.substring(paramPos + 3, query.indexOf("}", paramPos + 3));
	                Object paramValue = parameters.get(paramName);
	                String paramValueString = "";
	                if (paramValue instanceof Date){
	                    //format parameter for Date type paramValueString
	               	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	      			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(paramValue));
	      			paramValueString = nowYYYYMMDD1.toString();
	               	query = query.substring(0, paramPos) + "'"+paramValueString+"'" + query.substring(query.indexOf("}", paramPos + 3)+1);
	                } else if (paramValue instanceof Long){
	               	 //format parameter for Long type paramValueString
	               	paramValueString = paramValue.toString();
	                    query = query.substring(0, paramPos) + "'"+paramValueString+"'" + query.substring(query.indexOf("}", paramPos + 3)+1);
	                } else {
	               	paramValueString = paramValue.toString();
	                    query = query.substring(0, paramPos) + "'"+paramValueString+"'" + query.substring(query.indexOf("}", paramPos + 3)+1);
	                }
	          }
		} catch (JRException e) {
			e.printStackTrace();
		}
		return query;
	}
	
	
	public List findBySubModules(String module, String subModule, String corpID, String enabled) {   
	   	List reports = new ArrayList();;
	   	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu, docsxfer, pdf, rtf, runTimeParameterReq";
	  	List reportList =	this.getSession().createSQLQuery("select " + columns + ",'D' as type,emailOut,emailBody from reports where module = '" + module + "' AND subModule = '"+ subModule +"' AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes'   UNION select " + columns + ",'R' as type from reports where module = '" + module + "' AND subModule != '"+ subModule + "'  AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes' order by 14,3,7")
	   	  .addScalar("id", Hibernate.LONG)
		  .addScalar("module", Hibernate.STRING)
		  .addScalar("subModule", Hibernate.STRING)
		  
		  
		  .addScalar("corpID", Hibernate.STRING)
		  .addScalar("formReportFlag", Hibernate.STRING)
		  .addScalar("enabled", Hibernate.STRING)
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("reportComment", Hibernate.STRING)
		  .addScalar("menu", Hibernate.STRING)
		  .addScalar("docsxfer", Hibernate.STRING)
		  .addScalar("pdf", Hibernate.STRING)
		  .addScalar("rtf", Hibernate.STRING)
		  .addScalar("runTimeParameterReq", Hibernate.STRING)
		  .addScalar("type", Hibernate.STRING)
		  .addScalar("emailOut",Hibernate.STRING)
		  .addScalar("emailBody", Hibernate.STRING)
		  .list();
	   	Iterator it=reportList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setModule(row[1].toString());
			dTO.setSubModule(row[2].toString());
			dTO.setCorpID(row[3].toString());
			dTO.setFormReportFlag(row[4].toString());
			dTO.setEnabled(row[5].toString());
			dTO.setDescription(row[6].toString());
			dTO.setReportComment(row[7].toString());
			dTO.setMenu(row[8].toString());
			dTO.setDocsxfer(row[9].toString());
			dTO.setPdf(row[10].toString());
			dTO.setRtf(row[11].toString());
			dTO.setParamReq(row[12].toString());
			dTO.setType(row[13].toString());
			dTO.setEmailOut(row[14].toString());
			if(row[15]!=null){dTO.setEmailBody(row[15].toString());}else{dTO.setEmailBody("");}
			reports.add(dTO);
	       }
		return reports;
      }
	
	
	
	public List findBySubModules1(String module, String subModule,String companyDivision,String jobType,String billToCode,String modes, String corpID, String enabled,Long id,String jobNumber) {
		List reports = new ArrayList();
		String routing="";
	   	String getId="";
	   	String reportFieldName="";
	   	String reportFieldValue="";
	   	String tableName="";
	   	String columnName="";
	   	String ControlParamterId="";
	   	String ControlParamterNotId="";
	   	String controlParamter="";
	   	String controlNotParamter="";
	   	String tableNameFromSubModules="";
	   	boolean excuteParameter=false;
	   	boolean excuteParameterNotId=false;
	   	String a1="";
		String fieldVal="";
		String dataType="";
		String dataTypeQuery="";
		String billToCodeCondition="";
	   	String billToCodes ="";
		String mode1 ="";
		String query="";
		String jobTypes ="";
		String companyDivisions ="";
		try{
	   	////////////////////////////////
	   	
	   			try{
	   						if(subModule.equals("Container"))
	   							{
	   								subModule="Forwarding";
	   							}
	   						if(subModule.equals("serviceOrder") || subModule.equals("Quote"))
	   							{
	   								List<ServiceOrder> list=getHibernateTemplate().find("from ServiceOrder where id='"+id+"' and shipNumber='"+jobNumber+"'");
	   									for(ServiceOrder serviceOrder : list)
	   									{
	   										routing=serviceOrder.getRouting();
	   									}
	   							}
	   			}
	   			catch(Exception e)
	   				{
	   					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	   					e.printStackTrace();
	   				}
	   				//////////////////////////	   	
	   				
	   					if(companyDivision==null)
	   					{
	   							companyDivisions="";
	   					} 
	   					else{
	   							companyDivisions=companyDivision;
	   						}
	   	
	   				if(jobType==null)
	   					{
	   							jobTypes="";
	   					}
	   				else{
	   							jobTypes=jobType;
	   					}
	   				if(billToCode==null)
	   				{
	   					billToCodes="";
	   					billToCodeCondition = "(billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null)";
	   				} else 
	   				{
	   					billToCodes=billToCode;
	   					billToCodeCondition = "(((billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null) and excludefromform is false)"
	   							+ " or ((billToCode not like '%"+billToCodes+ "%') and excludefromform is true))";
	   				}
	   	
	   
	   				if(modes==null)
	   				{
	   					mode1="";
	   				} 
	   				else 
	   					{
	   						mode1=modes;
	   					}
	   	
	   		try{
	   				String reportIdQuery="select id from reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='') AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) "
	   									+ "AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes'   "
	   									+ "UNION "
	   									+ "select id from reports where module = '" + module + "' AND subModule != '"+ subModule + "' AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='')AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') "
	   									+ "AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes' ";
	   				List reportId =this.getSession().createSQLQuery(reportIdQuery).list();
	   					if(reportId.size()>0 && !reportId.isEmpty() && reportId!=null)
	   					{
	   							Iterator itr = reportId.iterator();
	   							while(itr.hasNext())
	   							{
	   								if(getId.equals(""))
	   								{
	   									getId = "'"+itr.next().toString()+"'";
	   								}
	   								else
	   								{
	   									getId = getId+","+"'"+itr.next().toString()+"'";
	   								}
	   							}
	   					}
					
	   					if(getId.equals(""))
	   					{
	   						getId="'"+getId+"'";
	   					}
	   					if(subModule.equals("Accounting"))
	   					{
						tableNameFromSubModules="accountline";
	   					}
	   					else if(subModule.equals("ASML"))
	   					{
	   						tableNameFromSubModules="customerfile";
	   					}
	   					else if(subModule.equals("Claims"))
	   					{
	   						tableNameFromSubModules="claim";
	   					}
	   					else if(subModule.equals("Billing"))
	   					{
	   						tableNameFromSubModules="billing";
	   					}
	   					else if(subModule.equals("customerFile"))
	   					{
	   						tableNameFromSubModules="customerfile";
	   					}
	   					else if(subModule.equals("Container"))
	   					{
	   						tableNameFromSubModules="container";
	   					}
	   					else if(subModule.equals("Miscellaneous"))
	   					{
	   						tableNameFromSubModules="miscellaneous";
	   					}
	   					else if(subModule.equals("Status"))
	   					{
	   						tableNameFromSubModules="trackingstatus";
	   					}
	   					else if(subModule.equals("workTicket"))
	   					{
	   						tableNameFromSubModules="workticket";
	   					}
	   					else if(subModule.equals("serviceOrder"))
	   					{
	   						tableNameFromSubModules="serviceorder";
	   					}
	   					else if(subModule.equals("Service Partner"))
	   					{
	   						tableNameFromSubModules="servicepartner";
	   					}
	   					else if(subModule.equals("Quotation"))
	   					{
	   						tableNameFromSubModules="customerfile";
	   					}
	   					else if(subModule.equals("Quote"))
	   					{
	   						tableNameFromSubModules="serviceorder";
	   					}else {
						
	   					}
					
					String fieldsectionId="from ReportsFieldSection where corpId='"+corpID+"' and reportid in("+getId+") and type='controll' ";
					List<ReportsFieldSection> list=getHibernateTemplate().find(fieldsectionId);
					// For Loop Start 
					for(ReportsFieldSection reportsFieldSection : list)
					{
						reportFieldName=reportsFieldSection.getFieldname();
						reportFieldValue=reportsFieldSection.getFieldvalue();
						if(reportFieldName!=null && !reportFieldName.equals(""))
						{
							String[] reportSection=reportFieldName.split("\\."); 
							tableName=reportSection[0];
							columnName=reportSection[1];
						}
						if(columnName !=null && (!(columnName.toString().trim().equals("")))){
						try{
						if(subModule.equals("customerFile"))
						{
							a1="select "+columnName+" from "+tableNameFromSubModules+"  where corpid='"+corpID+"' and sequenceNumber='"+jobNumber+"' ";
						}else{
							a1="select "+columnName+" from "+tableNameFromSubModules+"  where corpid='"+corpID+"' and shipNumber='"+jobNumber+"' ";	
						}
						
						List al=this.getSession().createSQLQuery(a1).list();
						if(al!=null && (!(al.isEmpty())) && al.get(0)!=null && (!(al.get(0).toString().trim().equals(""))))
						{
							fieldVal	=al.get(0).toString().trim();
						}
						}
						catch(NullPointerException np){
							np.printStackTrace();
						}
						catch(Exception e){
							e.printStackTrace();
						}
						}
						try{
						dataTypeQuery="select data_type from information_schema.columns where table_schema = 'redsky' and table_name = '"+tableNameFromSubModules+"' and column_name = '"+columnName+"'";
						List a2=this.getSession().createSQLQuery(dataTypeQuery).list();
						if(a2!=null && (!(a2.isEmpty())) && a2.get(0)!=null && (!(a2.get(0).toString().trim().equals(""))))
						{
							dataType	=a2.get(0).toString().trim();
						}
						if(fieldVal!=null || !fieldVal.equalsIgnoreCase("")){
						if(dataType.equals("datetime")  && !(reportFieldValue.equalsIgnoreCase("not null")|| reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NOT NULL")|| reportFieldValue.equalsIgnoreCase("NULL") || reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equalsIgnoreCase(" ") || reportFieldValue.equalsIgnoreCase("")))
						{
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							Date date = format.parse(fieldVal);
					   
							SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
							StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(date) );
							fieldVal= nowYYYYMMDD.toString();
						}
						}}catch(NullPointerException np){
							np.printStackTrace();
						}
						catch(Exception e){
							e.printStackTrace();
						}
				
						if(fieldVal.equalsIgnoreCase("TRUE") || fieldVal.equalsIgnoreCase("True") || fieldVal.equalsIgnoreCase("true")){
							fieldVal="true";
						}
						if(reportFieldValue.equalsIgnoreCase("TRUE") || reportFieldValue.equalsIgnoreCase("True") || reportFieldValue.equalsIgnoreCase("true")){
							reportFieldValue="true";
						}
						if(fieldVal.equalsIgnoreCase("FALSE") || fieldVal.equalsIgnoreCase("False") || fieldVal.equalsIgnoreCase("false")){
							fieldVal="false";
						}
						if(reportFieldValue.equalsIgnoreCase("FALSE") || reportFieldValue.equalsIgnoreCase("False") || reportFieldValue.equalsIgnoreCase("false")){
							reportFieldValue="false";
						}
						
				
						if(fieldVal==null || fieldVal.equalsIgnoreCase("") || fieldVal.equalsIgnoreCase("false"))
						{
							if(reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NULL")||  reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equals("' '") || reportFieldValue.equals("''") || reportFieldValue.equalsIgnoreCase("false"))
							{
								reportFieldValue=fieldVal;
							}
						}
						
						if((fieldVal!=null) && !(fieldVal.equalsIgnoreCase(""))){
							if(reportFieldValue!=null){
							if(!reportFieldValue.equalsIgnoreCase("null")){
								if(!reportFieldValue.equalsIgnoreCase("NULL")){
									if(!(reportFieldValue.equals("' '"))){
								if(!(reportFieldValue.equals("''"))){
								if(reportFieldValue.equalsIgnoreCase("not null") || reportFieldValue.equalsIgnoreCase("NOT NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
								reportFieldValue=fieldVal;
							}
							}
							}
							}
							}
							}
							}
						
						if(columnName!=null && !columnName.equals(""))
						{
							if(fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
							{
								if(ControlParamterId.equals("")){
									ControlParamterId = ""+reportsFieldSection.getReportId().toString()+"";
								}
								else{
									ControlParamterId = ControlParamterId+","+""+reportsFieldSection.getReportId().toString()+"";
								}
							
							}else
							{
								if(!fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
								{
									if(ControlParamterNotId.equals(""))
									{
										ControlParamterNotId = ""+reportsFieldSection.getReportId().toString()+"";
									}
									else
									{
										ControlParamterNotId = ControlParamterNotId+","+""+reportsFieldSection.getReportId().toString()+"";
									}
								}
							}
						}
						fieldVal="";
					}// For Loop Stop
					
						Set<String> parameterSet=new TreeSet<String>();
						if(ControlParamterId!=null && !ControlParamterId.equals(""))
							{
								String[] input =ControlParamterId.split(",");
								for( int i = 0; i <= input.length-1; i++)
									{
										String element = input[i];
										parameterSet.add(element);
									}
							}else{
						
							}
						Iterator itr1 = parameterSet.iterator();
						while(itr1.hasNext())
						{
							if(controlParamter.equals(""))
							{
								controlParamter = "'"+itr1.next().toString()+"'";
								excuteParameter=true;
							}
							else
							{
								controlParamter = controlParamter+","+"'"+itr1.next().toString()+"'";
								excuteParameter=true;
							}
						}
						if(controlParamter.equals(""))
						{
							controlParamter="'"+controlParamter+"'";
						}
					
					
						Set<String> parameterNotSet=new TreeSet<String>();
						if(ControlParamterNotId!=null && !ControlParamterNotId.equals(""))
						{
							String[] input =ControlParamterNotId.split(",");
						for( int j = 0; j <= input.length-1; j++)
						{
						    String element = input[j];
						    parameterNotSet.add(element);
						}
						}else{
						
						}
						Iterator itr2 = parameterNotSet.iterator();
						while(itr2.hasNext())
						{
							if(controlNotParamter.equals(""))
							{
								controlNotParamter = "'"+itr2.next().toString()+"'";
								excuteParameterNotId=true;
							}
							else
							{
								controlNotParamter = controlNotParamter+","+"'"+itr2.next().toString()+"'";
								excuteParameterNotId=true;
							}
						}
						if(controlNotParamter.equals(""))
						{
							controlNotParamter="'"+controlNotParamter+"'";
						}
	   			}
	   			catch(NullPointerException np)
	   			{
	   				if(controlParamter.equals(""))
					{
						controlParamter="'"+controlParamter+"'";
					}
	   				
	   				if(controlNotParamter.equals(""))
					{
						controlNotParamter="'"+controlNotParamter+"'";
					}
	   				String logMessage =  "Exception:"+ np.toString()+" at #"+np.getStackTrace()[0].toString();
	   				np.printStackTrace();
	   			}
	   			catch(ArrayIndexOutOfBoundsException ex)
	   			{
	   				if(controlParamter.equals(""))
					{
						controlParamter="'"+controlParamter+"'";
					}
	   				
	   				if(controlNotParamter.equals(""))
					{
						controlNotParamter="'"+controlNotParamter+"'";
					}
	   				
	   				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
	   				ex.printStackTrace();
	   			}
	   			catch(Exception e)
	   			{
	   				if(controlParamter.equals(""))
					{
						controlParamter="'"+controlParamter+"'";
					}
	   				
	   				if(controlNotParamter.equals(""))
					{
						controlNotParamter="'"+controlNotParamter+"'";
					}
	   				
	   				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	   				e.printStackTrace();
	   			}
	    
	    try{
	    	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu, companyDivision, job, billToCode, mode, docsxfer, pdf, rtf, runTimeParameterReq, docx";
	    	query="select " + columns + ",'D' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
			  query=query	+ "from reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='') AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) ";
			  query=query+ "AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
			  query=query+ "AND enabled like 'Yes'  ";
			  /*if(excuteParameter && !excuteParameterNotId){
			  query=query+ " And id in("+controlParamter+") ";
			  }else if (!excuteParameter && excuteParameterNotId){
				  query=query+ " And id not in("+controlNotParamter+") ";
			  }
			  else if (excuteParameter && excuteParameterNotId){
				  query=query+ " And id not in("+controlNotParamter+") And id in("+controlParamter+") ";
			  }
			  else{
				  query=query+ "And id in( case when "+controlParamter+"="+controlParamter+" then id else "+controlParamter+" end)  ";
			  }*/
			  query=query+ "UNION ";
			  query=query+ "select " + columns + ",'R' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
			  query=query	+ "from reports where module = '" + module + "' AND subModule != '"+ subModule + "' AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='')AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') ";
			  query=query+ "AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
			  query=query+ "AND enabled like 'Yes' ";
			  query=query+ "order by sortingSubmodule,if(agentroles <>'',agentroles,'zzzzz'),case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end,description ";
	    }catch(Exception e){
	    	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu, companyDivision, job, billToCode, mode, docsxfer, pdf, rtf, runTimeParameterReq, docx";
		    query="select " + columns + ",'D' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
				  query=query	+ "from reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='') AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) ";
				  query=query+ "AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
				  query=query+ "AND enabled like 'Yes' ";
				  query=query+ "UNION ";
				  query=query+ "select " + columns + ",'R' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
				  query=query	+ "from reports where module = '" + module + "' AND subModule != '"+ subModule + "' AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='')AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') ";
				  query=query+ "AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
				  query=query+ "AND enabled like 'Yes' ";
				  query=query+ "order by sortingSubmodule,if(agentroles <>'',agentroles,'zzzzz'),case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end,description ";
				  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	e.printStackTrace();
	    }
	   List reportList =	this.getSession().createSQLQuery(query)
	   	  .addScalar("id", Hibernate.LONG)
		  .addScalar("module", Hibernate.STRING)
		  .addScalar("subModule", Hibernate.STRING)
		  .addScalar("corpID", Hibernate.STRING)
		  .addScalar("formReportFlag", Hibernate.STRING)
		  .addScalar("enabled", Hibernate.STRING)
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("reportComment", Hibernate.STRING)
		  .addScalar("menu", Hibernate.STRING)
		  .addScalar("companyDivision", Hibernate.STRING)
		  .addScalar("job", Hibernate.STRING)
		  .addScalar("billToCode", Hibernate.STRING)
		  .addScalar("mode", Hibernate.STRING)
		  .addScalar("docsxfer", Hibernate.STRING)
		  .addScalar("pdf", Hibernate.STRING)
		  .addScalar("rtf", Hibernate.STRING)
		  .addScalar("runTimeParameterReq", Hibernate.STRING)
		  .addScalar("type", Hibernate.STRING)
		  .addScalar("emailOut",Hibernate.STRING)
		  .addScalar("emailBody", Hibernate.STRING)
		  .addScalar("docx", Hibernate.STRING)
		  .addScalar("sortingSubmodule", Hibernate.STRING)
		  .list();
	   	Iterator it=reportList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setModule(row[1].toString());
			dTO.setSubModule(row[2].toString());
			dTO.setCorpID(row[3].toString());
			dTO.setFormReportFlag(row[4].toString());
			dTO.setEnabled(row[5].toString());
			dTO.setDescription(row[6].toString());
			dTO.setReportComment(row[7].toString());
			dTO.setMenu(row[8].toString());
			dTO.setCompanyDivision(row[9].toString());
			dTO.setJob(row[10].toString());
			dTO.setBillToCode(row[11].toString());
			dTO.setMode(row[12].toString());
			dTO.setDocsxfer(row[13].toString());
			dTO.setPdf(row[14].toString());
			dTO.setRtf(row[15].toString());
			dTO.setParamReq(row[16].toString());
			dTO.setType(row[17].toString());
			dTO.setEmailOut(row[18].toString());
			if(row[19]!=null){dTO.setEmailBody(row[19].toString());}else{dTO.setEmailBody("");}
			dTO.setDocx(row[20].toString());
			dTO.setSortingSubmodule(row[21].toString());
			reports.add(dTO);
	       }
		
		}catch(Exception e){
	    	String columns = "id, module, subModule, corpID, formReportFlag, enabled, description, reportComment, menu, companyDivision, job, billToCode, mode, docsxfer, pdf, rtf, runTimeParameterReq, docx";
		    query="select " + columns + ",'D' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
				  query=query	+ "from reports where module = '" + module + "' AND subModule = '"+ subModule + "'AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='') AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) ";
				  query=query+ "AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
				  query=query+ "AND enabled like 'Yes' ";
				  query=query+ "UNION ";
				  query=query+ "select " + columns + ",'R' as type,emailOut,emailBody,if(subModule in ('"+subModule+"'),'A','B') as sortingSubmodule,agentRoles,sequenceNo ";
				  query=query	+ "from reports where module = '" + module + "' AND subModule != '"+ subModule + "' AND (companyDivision like '%"+companyDivisions+ "%' or companyDivision ='')AND (job like '%"+jobTypes+ "%' or job ='') AND (routing like '%"+routing+ "%' or routing ='') ";
				  query=query+ "AND "+billToCodeCondition+" AND (mode like '%"+mode1+ "%' or mode ='' or mode is null) AND corpID = '" + corpID +  "' AND formReportFlag='F' ";
				  query=query+ "AND enabled like 'Yes' ";
				  query=query+ "order by sortingSubmodule,if(agentroles <>'',agentroles,'zzzzz'),case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end ";
				  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	e.printStackTrace();
			    	 List reportList =	this.getSession().createSQLQuery(query)
			    		   	  .addScalar("id", Hibernate.LONG)
			    			  .addScalar("module", Hibernate.STRING)
			    			  .addScalar("subModule", Hibernate.STRING)
			    			  .addScalar("corpID", Hibernate.STRING)
			    			  .addScalar("formReportFlag", Hibernate.STRING)
			    			  .addScalar("enabled", Hibernate.STRING)
			    			  .addScalar("description", Hibernate.STRING)
			    			  .addScalar("reportComment", Hibernate.STRING)
			    			  .addScalar("menu", Hibernate.STRING)
			    			  .addScalar("companyDivision", Hibernate.STRING)
			    			  .addScalar("job", Hibernate.STRING)
			    			  .addScalar("billToCode", Hibernate.STRING)
			    			  .addScalar("mode", Hibernate.STRING)
			    			  .addScalar("docsxfer", Hibernate.STRING)
			    			  .addScalar("pdf", Hibernate.STRING)
			    			  .addScalar("rtf", Hibernate.STRING)
			    			  .addScalar("runTimeParameterReq", Hibernate.STRING)
			    			  .addScalar("type", Hibernate.STRING)
			    			  .addScalar("emailOut",Hibernate.STRING)
			    			  .addScalar("emailBody", Hibernate.STRING)
			    			  .addScalar("docx", Hibernate.STRING)
			    			  .addScalar("sortingSubmodule", Hibernate.STRING)
			    			  .list();
			    		   	Iterator it=reportList.iterator();
			    			DTO dTO=null;
			    			while(it.hasNext()){
			    				Object []row= (Object [])it.next();
			    				dTO=new DTO();
			    				dTO.setId(Long.parseLong(row[0].toString()));
			    				dTO.setModule(row[1].toString());
			    				dTO.setSubModule(row[2].toString());
			    				dTO.setCorpID(row[3].toString());
			    				dTO.setFormReportFlag(row[4].toString());
			    				dTO.setEnabled(row[5].toString());
			    				dTO.setDescription(row[6].toString());
			    				dTO.setReportComment(row[7].toString());
			    				dTO.setMenu(row[8].toString());
			    				dTO.setCompanyDivision(row[9].toString());
			    				dTO.setJob(row[10].toString());
			    				dTO.setBillToCode(row[11].toString());
			    				dTO.setMode(row[12].toString());
			    				dTO.setDocsxfer(row[13].toString());
			    				dTO.setPdf(row[14].toString());
			    				dTO.setRtf(row[15].toString());
			    				dTO.setParamReq(row[16].toString());
			    				dTO.setType(row[17].toString());
			    				dTO.setEmailOut(row[18].toString());
			    				if(row[19]!=null){dTO.setEmailBody(row[19].toString());}else{dTO.setEmailBody("");}
			    				dTO.setDocx(row[20].toString());
			    				dTO.setSortingSubmodule(row[21].toString());
			    				reports.add(dTO);
			    			}
	    }
	  
		return reports;
		
      }
	
	public List findQuotationFormList(String corpID, String companyDivision,String jobType,String billToCode,String modes,String jobNumber){
	   	List reports = new ArrayList();
	   	String routing="";
	   	String getId="";
	   	String reportFieldName="";
	   	String reportFieldValue="";
	   	String tableName="";
	   	String columnName="";
	   	String ControlParamterId="";
	   	String ControlParamterNotId="";
	   	String controlParamter="";
	   	String controlNotParamter="";
	   	String tableNameFromSubModules="";
	   	boolean excuteParameter=false;
	   	boolean excuteParameterNotId=false;
	   	String a1="";
		String fieldVal="";
		String dataType="";
		String dataTypeQuery="";
		String billToCodeCondition="";
	   	String billToCodes ="";
		String mode1 ="";
		String query="";
		String jobTypes ="";
	   	String companyDivisions ="";
	   	try{
	   	if(companyDivision==null){
	   		companyDivisions="";
	   	} else {
	   		companyDivisions=companyDivision;
	   	}
	   	if(jobType==null){
	   		jobTypes="";
	   	} else {
	   		jobTypes=jobType;
	   	}
	   	if(billToCode==null){
	   	 billToCodes="";
	   	 billToCodeCondition = "(billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null)";
	   	} else {
	   	 billToCodes=billToCode;
	   	 billToCodeCondition = "(((billToCode like '%"+billToCodes+ "%' or billToCode ='' or billToCode is null) and excludefromform is false)"
	   							+ " or ((billToCode not like '%"+billToCodes+ "%') and excludefromform is true))";
	   	}
	   	
	   	if(modes==null){
	   		mode1="";
	   	}else {
	   		mode1=modes;
	   	}
	   	
	   	try{
				
				List<ServiceOrder> list=getHibernateTemplate().find("from ServiceOrder where  shipNumber='"+jobNumber+"'");
					for(ServiceOrder serviceOrder : list)
					{
						routing=serviceOrder.getRouting();
					}
			
	   		}
	   			catch(Exception e)
	   				{
	   					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	   					e.printStackTrace();
	   				}
	   	
	   	String reportIdQuery="select id from reports "
   				+ " where module = 'quote' AND subModule = 'quote' and menu='quotation'"
   				+ " AND (companyDivision like '%"+companyDivisions+"%' or companyDivision ='')"
   				+ " AND "+billToCodeCondition+" " 
   				+ " AND (mode like '%"+mode1+"%' or mode ='' or mode is null)" 
   				+ " AND (job like '%"+jobTypes+"%' or job ='')"		
   				+ " AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes' ";
		List reportId =this.getSession().createSQLQuery(reportIdQuery).list();
		if(reportId.size()>0 && !reportId.isEmpty() && reportId!=null)
		{
			Iterator itr = reportId.iterator();
			while(itr.hasNext())
			{
				if(getId.equals(""))
				{
					getId = "'"+itr.next().toString()+"'";
				}
				else
				{
					getId = getId+","+"'"+itr.next().toString()+"'";
				}
			}
	}

	if(getId.equals(""))
	{
		getId="'"+getId+"'";
	}
	   	
	   	String fieldsectionId="from ReportsFieldSection where corpId='"+corpID+"' and reportid in("+getId+") and type='controll' ";
		List<ReportsFieldSection> list=getHibernateTemplate().find(fieldsectionId);
		// For Loop Start 
		for(ReportsFieldSection reportsFieldSection : list)
		{
			reportFieldName=reportsFieldSection.getFieldname();
			reportFieldValue=reportsFieldSection.getFieldvalue();
			if(reportFieldName!=null && !reportFieldName.equals(""))
				{
					String[] reportSection=reportFieldName.split("\\."); 
						tableName=reportSection[0];
						columnName=reportSection[1];
				}
	
			try{
				tableNameFromSubModules="serviceorder";
				a1="select "+columnName+" from "+tableNameFromSubModules+"  where corpid='"+corpID+"' and shipNumber='"+jobNumber+"'  ";	
				List al=this.getSession().createSQLQuery(a1).list();
				if(al!=null && (!(al.isEmpty())) && al.get(0)!=null && (!(al.get(0).toString().trim().equals(""))))
					{
						fieldVal	=al.get(0).toString().trim();
					}
				}
			catch(NullPointerException np){
				np.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			try{
			 dataTypeQuery="select data_type from information_schema.columns where table_schema = 'redsky' and table_name = '"+tableNameFromSubModules+"' and column_name = '"+columnName+"'";
			List a2=this.getSession().createSQLQuery(dataTypeQuery).list();
			if(a2!=null && (!(a2.isEmpty())) && a2.get(0)!=null && (!(a2.get(0).toString().trim().equals(""))))
			{
				dataType	=a2.get(0).toString().trim();
			}
			if(fieldVal!=null || !fieldVal.equalsIgnoreCase("")){
				if(dataType.equals("datetime")  && !(reportFieldValue.equalsIgnoreCase("not null")|| reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NOT NULL")|| reportFieldValue.equalsIgnoreCase("NULL") || reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equalsIgnoreCase(" ") || reportFieldValue.equalsIgnoreCase("")))
				{
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
					Date date = format.parse(fieldVal);
			   
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(date) );
					fieldVal= nowYYYYMMDD.toString();
				}
				}}catch(NullPointerException np){
				np.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			if(fieldVal.equalsIgnoreCase("TRUE") || fieldVal.equalsIgnoreCase("True") || fieldVal.equalsIgnoreCase("true")){
				fieldVal="true";
			}
			if(reportFieldValue.equalsIgnoreCase("TRUE") || reportFieldValue.equalsIgnoreCase("True") || reportFieldValue.equalsIgnoreCase("true")){
				reportFieldValue="true";
			}
			if(fieldVal.equalsIgnoreCase("FALSE") || fieldVal.equalsIgnoreCase("False") || fieldVal.equalsIgnoreCase("false")){
				fieldVal="false";
			}
			if(reportFieldValue.equalsIgnoreCase("FALSE") || reportFieldValue.equalsIgnoreCase("False") || reportFieldValue.equalsIgnoreCase("false")){
				reportFieldValue="false";
			}
			
	
			if(fieldVal==null || fieldVal.equalsIgnoreCase("") || fieldVal.equalsIgnoreCase("false"))
			{
				if(reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NULL")||  reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equals("' '") || reportFieldValue.equals("''") || reportFieldValue.equalsIgnoreCase("false"))
				{
					reportFieldValue=fieldVal;
				}
			}
			
			if((fieldVal!=null) && !(fieldVal.equalsIgnoreCase(""))){
				if(reportFieldValue!=null){
				if(!reportFieldValue.equalsIgnoreCase("null")){
					if(!reportFieldValue.equalsIgnoreCase("NULL")){
						if(!(reportFieldValue.equals("' '"))){
					if(!(reportFieldValue.equals("''"))){
				if(reportFieldValue.equalsIgnoreCase("not null") || reportFieldValue.equalsIgnoreCase("NOT NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
					reportFieldValue=fieldVal;
				}
				}
				}
				}
				}
				}
				}
			
			if(columnName!=null && !columnName.equals(""))
			{
				if(fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
				{
					if(ControlParamterId.equals("")){
						ControlParamterId = ""+reportsFieldSection.getReportId().toString()+"";
					}
					else{
						ControlParamterId = ControlParamterId+","+""+reportsFieldSection.getReportId().toString()+"";
					}
		
				}else
				{
					if(!fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
					{
						if(ControlParamterNotId.equals(""))
						{
							ControlParamterNotId = ""+reportsFieldSection.getReportId().toString()+"";
						}
						else
						{
							ControlParamterNotId = ControlParamterNotId+","+""+reportsFieldSection.getReportId().toString()+"";
						}
					}
				}
			}
			fieldVal="";
		}// For Loop Stop
		
		Set<String> parameterSet=new TreeSet<String>();
		if(ControlParamterId!=null && !ControlParamterId.equals(""))
		{
			String[] input =ControlParamterId.split(",");
			for( int i = 0; i <= input.length-1; i++)
				{
					String element = input[i];
					parameterSet.add(element);
				}
		}else{
	
		}
	Iterator itr1 = parameterSet.iterator();
	while(itr1.hasNext())
	{
		if(controlParamter.equals(""))
		{
			controlParamter = "'"+itr1.next().toString()+"'";
			excuteParameter=true;
		}
		else
		{
			controlParamter = controlParamter+","+"'"+itr1.next().toString()+"'";
			excuteParameter=true;
		}
	}
	if(controlParamter.equals(""))
	{
		controlParamter="'"+controlParamter+"'";
	}


	Set<String> parameterNotSet=new TreeSet<String>();
	if(ControlParamterNotId!=null && !ControlParamterNotId.equals(""))
	{
		String[] input =ControlParamterNotId.split(",");
	for( int j = 0; j <= input.length-1; j++)
	{
	    String element = input[j];
	    parameterNotSet.add(element);
	}
	}else{
	
	}
	Iterator itr2 = parameterNotSet.iterator();
	while(itr2.hasNext())
	{
		if(controlNotParamter.equals(""))
		{
			controlNotParamter = "'"+itr2.next().toString()+"'";
			excuteParameterNotId=true;
		}
		else
		{
			controlNotParamter = controlNotParamter+","+"'"+itr2.next().toString()+"'";
			excuteParameterNotId=true;
		}
	}
	if(controlNotParamter.equals(""))
	{
		controlNotParamter="'"+controlNotParamter+"'";
	}

}catch(NullPointerException np){
	if(controlNotParamter.equals(""))
	{
		controlNotParamter="'"+controlNotParamter+"'";
	}
	if(controlParamter.equals(""))
	{
		controlParamter="'"+controlParamter+"'";
	}
	np.printStackTrace();
}
   	catch(Exception e){
   		if(controlNotParamter.equals(""))
		{
			controlNotParamter="'"+controlNotParamter+"'";
		}
   		if(controlParamter.equals(""))
		{
			controlParamter="'"+controlParamter+"'";
		}
	e.printStackTrace();
}
	   	
	   	
	   	
	   	try{
	   		
	   		query="select id, menu, description,docsxfer, pdf, rtf,emailOut,'D' as type,runTimeParameterReq,module, subModule,reportComment,docx from reports ";
	   		query=query+ "  where module = 'quote' AND subModule = 'quote' and menu='quotation'";
	   		query=query+ "  AND (companyDivision like '%"+companyDivisions+"%' or companyDivision ='')";
	   		query=query+ "  AND "+billToCodeCondition+" " ;
	   		query=query+ "  AND (mode like '%"+mode1+"%' or mode ='' or mode is null)" ;
	   		query=query+ "  AND (job like '%"+jobTypes+"%' or job ='')"		;
	   		query=query+  " AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes'";
	   		if(excuteParameter && !excuteParameterNotId){
   				query=query+ " And id in("+controlParamter+") ";
   			}else if (!excuteParameter && excuteParameterNotId)
   			{
			  query=query+ " And id not in("+controlNotParamter+") ";
   			}
   			else if (excuteParameter && excuteParameterNotId)
   			{
			  query=query+ " And id not in("+controlNotParamter+") And id in("+controlParamter+") ";
   			}
		  else{
			  query=query+ "And id in( case when "+controlParamter+"="+controlParamter+" then id else "+controlParamter+" end) ";
					  
			  		
		  }
   			query=query + "order by case when agentroles is not null and agentroles<>''  then agentroles else 'zzzzz' end,case when sequenceNo is not null and sequenceNo<>0 then sequenceNo else 99999 end  ";
   	
	   	}catch(Exception ex){
	   		query="";
	   	 query="select id, menu, description,docsxfer, pdf, rtf,emailOut,'D' as type,runTimeParameterReq,module, subModule,reportComment,docx from reports "
	   				+ " where module = 'quote' AND subModule = 'quote' and menu='quotation'"
	   				+ " AND (companyDivision like '%"+companyDivisions+"%' or companyDivision ='')"
	   				+ " AND "+billToCodeCondition+" " 
	   				+ " AND (mode like '%"+mode1+"%' or mode ='' or mode is null)" 
	   				+ " AND (job like '%"+jobTypes+"%' or job ='')"		
	   				+ " AND corpID = '" + corpID +  "' AND formReportFlag='F' AND enabled like 'Yes'";
	   		
	   	}
	   	List reportList =	this.getSession().createSQLQuery(query).list();
	   	
	   	Iterator it=reportList.iterator();
		DTO dTO=null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dTO=new DTO();
			dTO.setId(Long.parseLong(row[0].toString()));
			dTO.setMenu(row[1].toString());
			dTO.setDescription(row[2].toString());
			dTO.setDocsxfer(row[3].toString());
			dTO.setPdf(row[4].toString());
			dTO.setRtf(row[5].toString());
			dTO.setEmailOut(row[6].toString());
			dTO.setType(row[7].toString());
			dTO.setParamReq(row[8].toString());
			dTO.setModule(row[9].toString());
			dTO.setSubModule(row[10].toString());
			dTO.setReportComment(row[11].toString());
			dTO.setDocx(row[12].toString());
			
			reports.add(dTO);
	       }
	   	
		return reports;
	}
	
	
	public class DTO{
		private Object  id;
		private Object  description;
		private Object  subModule;
		private Object  reportComment;
		private Object module;
		private Object  reportName;
		private Object  docsxfer;
		private Object  menu;
		private Object  bid;
		private Object  brid;
		private Object  createdBy;
		private Object  bkMarkcreatedBy;
		private Object  corpID;
		private Object  formReportFlag;
		private Object  pdf;
		private Object  rtf;
		private Object  enabled;
		private Object  type;
		private Object  paramReq;
		private Object companyDivision;
		private Object job;
		private Object billToCode;
		private Object mode;
		private Object emailOut;
		private Object emailBody;
		private Object docx;
		private Object sortingSubmodule;
		
		/**
		 * @return the mode
		 */
		public Object getMode() {
		    return mode;
		}
		/**
		 * @param mode the mode to set
		 */
		public void setMode(Object mode) {
		    this.mode = mode;
		}
		public  Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getEnabled() {
			return enabled;
		}
		public void setEnabled(Object enabled) {
			this.enabled = enabled;
		}
		public Object getFormReportFlag() {
			return formReportFlag;
		}
		public void setFormReportFlag(Object formReportFlag) {
			this.formReportFlag = formReportFlag;
		}
		public Object getPdf() {
			return pdf;
		}
		public void setPdf(Object pdf) {
			this.pdf = pdf;
		}
		public Object getRtf() {
			return rtf;
		}
		public void setRtf(Object rtf) {
			this.rtf = rtf;
		}
		public Object getType() {
			return type;
		}
		public void setType(Object type) {
			this.type = type;
		}
		/**
		 * @return the bid
		 */
		public Object getBid() {
			return bid;
		}
		/**
		 * @param bid the bid to set
		 */
		public void setBid(Object bid) {
			this.bid = bid;
		}
		/**
		 * @return the bkMarkcreatedBy
		 */
		public Object getBkMarkcreatedBy() {
			return bkMarkcreatedBy;
		}
		/**
		 * @param bkMarkcreatedBy the bkMarkcreatedBy to set
		 */
		public void setBkMarkcreatedBy(Object bkMarkcreatedBy) {
			this.bkMarkcreatedBy = bkMarkcreatedBy;
		}
		/**
		 * @return the brid
		 */
		public Object getBrid() {
			return brid;
		}
		/**
		 * @param brid the brid to set
		 */
		public void setBrid(Object brid) {
			this.brid = brid;
		}
		/**
		 * @return the createdBy
		 */
		public Object getCreatedBy() {
			return createdBy;
		}
		/**
		 * @param createdBy the createdBy to set
		 */
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		/**
		 * @return the description
		 */
		public Object getDescription() {
			return description;
		}
		/**
		 * @param description the description to set
		 */
		public void setDescription(Object description) {
			this.description = description;
		}
		/**
		 * @return the docsxfer
		 */
		public Object getDocsxfer() {
			return docsxfer;
		}
		/**
		 * @param docsxfer the docsxfer to set
		 */
		public void setDocsxfer(Object docsxfer) {
			this.docsxfer = docsxfer;
		}
		/**
		 * @return the id
		 */
		public Object getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(Object id) {
			this.id = id;
		}
		/**
		 * @return the menu
		 */
		public Object getMenu() {
			return menu;
		}
		/**
		 * @param menu the menu to set
		 */
		public void setMenu(Object menu) {
			this.menu = menu;
		}
		/**
		 * @return the module
		 */
		public Object getModule() {
			return module;
		}
		/**
		 * @param module the module to set
		 */
		public void setModule(Object module) {
			this.module = module;
		}
		/**
		 * @return the reportComment
		 */
		public Object getReportComment() {
			return reportComment;
		}
		/**
		 * @param reportComment the reportComment to set
		 */
		public void setReportComment(Object reportComment) {
			this.reportComment = reportComment;
		}
		/**
		 * @return the reportName
		 */
		public Object getReportName() {
			return reportName;
		}
		/**
		 * @param reportName the reportName to set
		 */
		public void setReportName(Object reportName) {
			this.reportName = reportName;
		}
		/**
		 * @return the subModule
		 */
		public Object getSubModule() {
			return subModule;
		}
		/**
		 * @param subModule the subModule to set
		 */
		public void setSubModule(Object subModule) {
			this.subModule = subModule;
		}
		public Object getParamReq() {
			return paramReq;
		}
		public void setParamReq(Object paramReq) {
			this.paramReq = paramReq;
		}
		public Object getBillToCode() {
		    return billToCode;
		}
		public void setBillToCode(Object billToCode) {
		    this.billToCode = billToCode;
		}
		public Object getEmailOut() {
			return emailOut;
		}
		public void setEmailOut(Object emailOut) {
			this.emailOut = emailOut;
		}
		public Object getEmailBody() {
			return emailBody;
		}
		public void setEmailBody(Object emailBody) {
			this.emailBody = emailBody;
		}
		public Object getDocx() {
			return docx;
		}
		public void setDocx(Object docx) {
			this.docx = docx;
		}
		public Object getSortingSubmodule() {
			return sortingSubmodule;
		}
		public void setSortingSubmodule(Object sortingSubmodule) {
			this.sortingSubmodule = sortingSubmodule;
		}
}
	public Map<String, String> findOwner(String corpId, String parameter) {
		String blankKey=" ";
		String blankValue="BLANK";
		List<User> list;
		if (parameter.equals("ALL_USER")) {
			list = this.getSession().createSQLQuery("SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and app_user.account_enabled=TRUE and app_user.corpid in ('" + corpId + "') order by alias").list();
		} else {
			list = this.getSession().createSQLQuery("SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and app_user.account_enabled=TRUE and role.name='" + parameter + "' and app_user.corpid in ('" + corpId + "') order by alias").list();
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		//parameterMap.put(blankKey, blankValue);
		parameterMap.put(blankKey, blankValue);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			parameterMap.put(row[0].toString().toUpperCase(), row[1].toString().toUpperCase());
		}
		return parameterMap;

	}
	
	
	public Map<String, String> findJobOwner(String corpId, String parameter) {
		String blankKey=" ";
		String blankValue="BLANK";
		List<User> list;
		if (parameter.equals("ALL_USER")) {
			list = this.getSession().createSQLQuery("SELECT distinct (alias), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and app_user.account_enabled=TRUE and app_user.corpid in ('" + corpId + "') order by alias").list();
		} else {
			list = this.getSession().createSQLQuery("SELECT distinct (alias), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and app_user.account_enabled=TRUE and role.name='" + parameter + "' and app_user.corpid in ('" + corpId + "') order by alias").list();
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		//parameterMap.put(blankKey, blankValue);
		parameterMap.put(blankKey, blankValue);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			parameterMap.put(row[0].toString().toUpperCase(), row[1].toString().toUpperCase());
		}
		return parameterMap;

	}
	
	public List checkBillCodes(String billCode, String corpID) {
	        String query="SELECT partnercode FROM partnerpublic where partnercode in ("+billCode+") and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')";
	 	List checkBillCode= this.getSession().createSQLQuery(query).list();
	 	List checkBillCodes=null;
	 	if(!checkBillCode.isEmpty()){
	 	String query1="SELECT partnercode FROM partnerpublic where partnercode in ("+billCode+") and status <> 'Approved' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')";
	 	checkBillCodes= this.getSession().createSQLQuery(query1).list();
	 	} else {
	 	checkBillCodes = new ArrayList(Arrays.asList(billCode)); 
	        }
	 	return checkBillCodes;
		}
	
	
	public List checkModes(String modes, String corpID) {
	        String query="SELECT distinct description FROM refmaster where parameter='MODE' and description in ("+modes+") and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')";
	        List checkModes= this.getSession().createSQLQuery(query).list();
	 	return checkModes;
		}
	
	public void updateTableStatus(String modelName,String fieldName,String value,Long sid){
		String query="update "+modelName+" set "+fieldName+"="+value+" where id="+sid+"";
		if(value!=null && value.equalsIgnoreCase("now()")){
			query="update "+modelName+" set "+fieldName+"=now() where id="+sid+"";
		}
		try{ 
         this.getSession().createSQLQuery(query).executeUpdate();
		}catch(Exception e){
			getHibernateTemplate().bulkUpdate(query);
		}
	}
	
	public List getServiceOrderGraph(String sessionCorpID, String jrxmlName){
		List list = this.getSession().createSQLQuery("select id from reports where corpid='" + sessionCorpID + "' and description in('" + jrxmlName + "')").list();
		return list;
	}
	
	public void updateTableStatusWithCondition(String modelName,String fieldName,String value,String condition,Long sid){
		String query="";
		if(value.equalsIgnoreCase("value")|| value.equals("false")){
			query="update "+modelName+" set "+fieldName+"="+value+" where "+condition+" and id="+sid+" ";
			if(value!=null && value.equalsIgnoreCase("now()")){
				query="update "+modelName+" set "+fieldName+"=now() where "+condition+" and id="+sid+" ";
			}
		}else{
			query="update "+modelName+" set "+fieldName+"='"+value+"' where "+condition+" and id="+sid+" ";
			if(value!=null && value.equalsIgnoreCase("now()")){
				query="update "+modelName+" set "+fieldName+"=now() where "+condition+" and id="+sid+" ";
			}			
		}
		try{ 
         this.getSession().createSQLQuery(query).executeUpdate();
		}catch(Exception e){
			getHibernateTemplate().bulkUpdate(query);
		}
	}
	public void updateTableStatusWithCondition1(String modelName,String fieldName,String value,String condition,String sid){
		String query="";
		if(value.equalsIgnoreCase("value")|| value.equals("false")){
			query="update "+modelName+" set "+fieldName+"="+value+" where "+condition+" and id in("+sid+")";
			if(value!=null && value.equalsIgnoreCase("now()")){
				query="update "+modelName+" set "+fieldName+"=now() where "+condition+" and id in("+sid+")";
			}					
		}else{
			query="update "+modelName+" set "+fieldName+"='"+value+"' where "+condition+" and id in("+sid+")";
			if(value!=null && value.equalsIgnoreCase("now()")){
				query="update "+modelName+" set "+fieldName+"=now() where "+condition+" and id in("+sid+")";
			}			
		}
		try{ 
         this.getSession().createSQLQuery(query).executeUpdate();
		}catch(Exception e){
			getHibernateTemplate().bulkUpdate(query);
		}
	}
	
	public Reports getReportTemplateForWT(String jrxmlName, String module,String corpId){
		Reports report = null;
		JasperReport jasperReport = null;
		
		 List<Reports> list = getHibernateTemplate().find("from Reports where reportName='"+jrxmlName+"' and corpId='"+corpId+"'");
		 if(list != null && !list.isEmpty() && list.size()>0){
			 report =  list.get(0);
		 }

		 return report;
		 
	}
	public List<PrintDailyPackage> getByCorpId(String corpId) {
		return getHibernateTemplate().find("from PrintDailyPackage where corpId='"+corpId+"'");
	}
	
	public JasperPrint generateReport(JasperReport jasperReport, Map parameters) {
		JasperPrint jasperPrint = null;
		Connection con = this.getSession().connection();
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);
			System.out.println("Jasper Print Daily Package.........................."+jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}finally{
			try {
				if(con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return jasperPrint;
	}
	public List getdescription(String corpId,String module){
		return getHibernateTemplate().find("select distinct description from Reports where corpId='"+corpId+"' and module ='"+module.trim()+"' order by description");
		/*List list = getHibernateTemplate().find("select distinct description from Reports where corpId='"+corpId+"' order by description");
		List lst = new ArrayList();
		for (Object object : list) {
			String desc = object.toString();
			desc = desc.replaceAll("1099", "'s");
			lst.add(desc);
		}
		Collections.sort(lst);
		return lst;*/
	}
	
	public String getReportCommentByJrxmlName(String jrxmlName,String corpId){
		List list = getHibernateTemplate().find("select reportComment from Reports where reportName='"+jrxmlName+"' and corpId='"+corpId+"'");
		for (Object object : list) {
			
		}
		return "";
	}
	
	public List getReportCommentByModule(String module,String corpId){
		return getHibernateTemplate().find("select distinct description from Reports where module='"+module+"' and corpId='"+corpId+"' order by description");
	}
	public List getReportId(String corpId){
		return  getHibernateTemplate().find("select id from Reports where reportName='DriverSettlementID.jrxml' and corpId='"+corpId+"'");
	}
	
	public String getBillToCodeByFormName(String jrxmlName,String corpId){
		String value="";
		String query="select billToCode from reports where description = '"+jrxmlName+"' and corpId='"+corpId+"'";
		List list = this.getSession().createSQLQuery(query).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			value=list.get(0).toString();
		}
		return value;
	}
	
	public List findByRedskyCustomer(){
		return this.getSession().createSQLQuery("select name from ehour.CUSTOMER where active='Y'order by 1").list();
	}
	public List findByRedskyCustomerCorp(String RedSkyCustomer){
		return this.getSession().createSQLQuery("select code from ehour.CUSTOMER where active='Y' and name='"+RedSkyCustomer+"' order by 1").list();
	}
	public String billToDefaultStationary(String sessionCorpID,	String invoiceNumber){
		String billToDefaultStationary="";
		List list = this.getSession().createSQLQuery("select p.emailPrintOption from partnerprivate p INNER JOIN accountline a on p.partnercode=a.billtocode and a.corpId='"+sessionCorpID+"' where p.corpId='"+sessionCorpID+"' and a.recinvoicenumber='"+invoiceNumber+"' order by 1").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			billToDefaultStationary=list.get(0).toString();
		}
		return billToDefaultStationary;
	}
	
	public Map<String, String> findRelocationServices(String sessionCorpID){
		List<RefMaster> list=getHibernateTemplate().find("from RefMaster where parameter='SERVICE' and corpid in ('TSFT','"+sessionCorpID+"') and flex3='SURVEYEMAIL' group by code order by description");
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
		}
	public List findRelatedSO(String sessionCorpID, String jobNumber, String custID){
		List list = new ArrayList();
		list = this.getSession().createSQLQuery("select shipnumber from serviceorder where corpid ='"+sessionCorpID+"' and sequencenumber = '"+custID+"' and shipnumber != '"+jobNumber+"' and job='RLO' order by 1 ").list();
		return list;
		}
	public List getVanLineCodeCompDiv(String sessionCorpID){
		return getHibernateTemplate().find("select distinct vanLineCode from CompanyDivision where vanLineCode != '' and corpID ='"+sessionCorpID+"' ");
		}
	public List findContainer(String sessionCorpID, String shipNumber) {
		return getHibernateTemplate().find("select containerNumber FROM Container where status = true and  shipNumber='" + shipNumber + "' and corpID ='"+sessionCorpID+"' and containerNumber != ''");
		}
	public JasperReport getReportTemplate(Long id, String preferredLanguage){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		String sessionCorpID = user.getCorpID().toUpperCase();
		JasperReport jasperReport = null;
		Reports report = this.get(id);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/jasper");
			String reportPathName  = path + "//"+sessionCorpID+"//" + report.getReportName();
			if((report.getIsMultiLingual() == true) && preferredLanguage != null) {
			int dotPos = report.getReportName().indexOf(".");
			 String reportNewName = report.getReportName().substring(0,dotPos)  + "_" + preferredLanguage + report.getReportName().substring(dotPos);
			 if (new File(path + "//"+sessionCorpID+"//" + reportNewName).exists()){
			 reportPathName = path + "//"+sessionCorpID+"//" + reportNewName;
			 }
			 }
			 jasperReport = JasperCompileManager.compileReport(reportPathName);
			 } catch (JRException e) {
			 e.printStackTrace();
			 }
			return jasperReport;
	}
	
	public Map<String,String> findByCompanyDivisions(String sessionCorpID) {
	    List<CompanyDivision> list=getHibernateTemplate().find("from CompanyDivision where corpID='"+sessionCorpID+"' group by companyCode ");

	    Map<String, String> parameterMap = new TreeMap<String, String>();

	    for(CompanyDivision companyDivision : list){
		    parameterMap.put(companyDivision.getCompanyCode(),companyDivision.getCompanyCode());
	    }
	    return parameterMap;
	  }
	public List getIdfromMyfileforServiceOrder(String JobNumber,String sessionCorpID,String invoiceNumber,String reportName){
		try{
		List list = new ArrayList();
		String fileFileName="";
		if(!invoiceNumber.equals("")){
			 fileFileName=invoiceNumber+"-"+"Invoice"+"-"+reportName;
		}
		list = this.getSession().createSQLQuery("select id from myfile where corpID ='"+sessionCorpID+"' and customerNumber = '"+JobNumber+"' and fileFileName='"+fileFileName+"'  ").list();
		if(list==null){
			list=new ArrayList();
		}
		return list;
		}
		catch(NullPointerException np){
			 np.printStackTrace();
			 return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String getAgentRoleList(Long reportId){
		String agentRoles = "";
		try{	
			List roles = this.getSession().createSQLQuery("SELECT agentRoles FROM reports where id="+reportId).list();
			if(roles==null){
				return agentRoles;
			}else{
				agentRoles=roles.toString();
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return agentRoles;
		}
		return agentRoles;
	}
	public List findAgentGroup(String sessionCorpID){
	    return getHibernateTemplate().find("select distinct agentGroup from PartnerPublic where corpid in ('"+sessionCorpID+"','TSFT') and agentGroup is not null and trim(agentGroup)!=''  group by agentGroup order by agentGroup");
	}
	
	public List findBySubModuleFromAccountline(String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String shipNumber)
	{
		List reportId=null;
	   	String getId="";
	   	String reportFieldName="";
	   	String reportFieldValue="";
	   	String tableName="";
	   	String columnName="";
	   	String ControlParamterId="";
	   	String ControlParamterNotId="";
	   	String controlParamter="";
	   	String controlNotParamter="";
	   	String tableNameFromSubModules="";
	   	boolean excuteParameter=false;
	   	boolean excuteParameterNotId=false;
		String fieldVal="";
		String dataType="";

		String billToCodeCondition="";
	   	String billToCodes ="";
	   	if(billToCode==null){
	   	 billToCodes="";
	   	 billToCodeCondition = "(e.billToCode like '%"+billToCodes+"%' or e.billToCode='' or e.billToCode is null)";
	   	} else {
	   	 billToCodes=billToCode;
	   	 billToCodeCondition = "(((e.billToCode like '%"+billToCodes+ "%' or e.billToCode ='' or e.billToCode is null) and e.excludeFromForm is false)"
	   							+ " or ((e.billToCode not like '%"+billToCodes+ "%') and e.excludeFromForm is true))";
	   	}

	   	try
	   	{
	   		
	   		if(shipNumber.equals("Collective")){
	   			reportId=this.getSession().createSQLQuery("select id from reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and e.description like '%collective%'  and e.corpID='"+corpID+"'").list();	
	   		}else{
	   			reportId=this.getSession().createSQLQuery("select id from reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and (e.description like '%invoice%' or e.description like '%credit%')  and NOT (e.description like '%collective%')  and e.corpID='"+corpID+"'").list();
	   		}
	   			if(reportId.size()>0 && !reportId.isEmpty() && reportId!=null)
	   				{
	   					Iterator itr = reportId.iterator();
	   					while(itr.hasNext())
	   						{
	   							if(getId.equals(""))
	   							{
	   								getId = "'"+itr.next().toString()+"'";
	   							}
	   							else
	   							{
	   								getId = getId+","+"'"+itr.next().toString()+"'";
	   							}
	   						}
	   				}

	   			if(getId.equals(""))
	   				{
	   					getId="'"+getId+"'";
	   				}
	   			List<ReportsFieldSection> list=getHibernateTemplate().find("from ReportsFieldSection where corpId='"+corpID+"' and reportid in("+getId+") and type='controll'");
	   			// For Loop Start 
	   			for(ReportsFieldSection reportsFieldSection : list)
	   				{
	   					reportFieldName=reportsFieldSection.getFieldname();
	   					reportFieldValue=reportsFieldSection.getFieldvalue();
	   						if(reportFieldName!=null && !reportFieldName.equals(""))
	   							{
	   								String[] reportSection=reportFieldName.split("\\."); 
	   								tableName=reportSection[0];
	   								columnName=reportSection[1];
	   							}
	   						try
	   							{
	   								List al=this.getSession().createSQLQuery("select "+columnName+" from accountline  where corpid='"+corpID+"' and shipNumber='"+shipNumber+"' ").list();
	   								if(al!=null && (!(al.isEmpty())) && al.get(0)!=null && (!(al.get(0).toString().trim().equals(""))))
	   								{
	   									fieldVal	=al.get(0).toString().trim();
	   								}
	   							}
	   						catch(NullPointerException np)
	   							{
	   								np.printStackTrace();
	   							}
	   						catch(Exception e)
	   							{
	   								e.printStackTrace();
	   							}
	   						try
	   							{
	   								List a2=this.getSession().createSQLQuery("select data_type from information_schema.columns where table_schema = 'redsky' and table_name = 'accountline' and column_name = '"+columnName+"'").list();
	   								if(a2!=null && (!(a2.isEmpty())) && a2.get(0)!=null && (!(a2.get(0).toString().trim().equals(""))))
	   									{
	   										dataType	=a2.get(0).toString().trim();
	   									}
	   								if(fieldVal!=null || !fieldVal.equalsIgnoreCase(""))
	   									{
	   										if(dataType.equals("datetime")  && !(reportFieldValue.equalsIgnoreCase("not null")|| reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NOT NULL")|| reportFieldValue.equalsIgnoreCase("NULL") || reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equalsIgnoreCase(" ") || reportFieldValue.equalsIgnoreCase("")))
	   										{
	   											SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	   											Date date = format.parse(fieldVal);
			   
	   											SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	   											StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(date) );
	   											fieldVal= nowYYYYMMDD.toString();
	   										}
	   									}
	   							}catch(NullPointerException np)
	   							{
	   								np.printStackTrace();
	   							}
	   							catch(Exception e)
	   							{
	   								e.printStackTrace();
	   							}
			
	   						if(fieldVal.equalsIgnoreCase("TRUE") || fieldVal.equalsIgnoreCase("True") || fieldVal.equalsIgnoreCase("true"))
	   						{
	   							fieldVal="true";
	   						}
	   						if(reportFieldValue.equalsIgnoreCase("TRUE") || reportFieldValue.equalsIgnoreCase("True") || reportFieldValue.equalsIgnoreCase("true")){
	   							reportFieldValue="true";
	   						}
	   						if(fieldVal.equalsIgnoreCase("FALSE") || fieldVal.equalsIgnoreCase("False") || fieldVal.equalsIgnoreCase("false")){
	   							fieldVal="false";
	   						}
	   						if(reportFieldValue.equalsIgnoreCase("FALSE") || reportFieldValue.equalsIgnoreCase("False") || reportFieldValue.equalsIgnoreCase("false")){
	   							reportFieldValue="false";
	   						}
			
	
	   						if(fieldVal==null || fieldVal.equalsIgnoreCase("") || fieldVal.equalsIgnoreCase("false"))
	   						{
	   							if(reportFieldValue.equalsIgnoreCase("null") || reportFieldValue.equalsIgnoreCase("NULL")||  reportFieldValue==" " || reportFieldValue=="" || reportFieldValue.equals("' '") || reportFieldValue.equals("''") || reportFieldValue.equalsIgnoreCase("false"))
	   							{
	   								reportFieldValue=fieldVal;
	   							}
	   						}
			
	   						if((fieldVal!=null) && !(fieldVal.equalsIgnoreCase(""))){
	   							if(reportFieldValue!=null){
	   								if(!reportFieldValue.equalsIgnoreCase("null")){
	   									if(!reportFieldValue.equalsIgnoreCase("NULL")){
	   										if(!(reportFieldValue.equals("' '"))){
	   											if(!(reportFieldValue.equals("''"))){
	   												if(reportFieldValue.equalsIgnoreCase("not null") || reportFieldValue.equalsIgnoreCase("NOT NULL") || (reportFieldValue.equalsIgnoreCase("true") && !fieldVal.equalsIgnoreCase("false")) || (reportFieldValue.equalsIgnoreCase("true") && fieldVal.equalsIgnoreCase("true"))){
	   													reportFieldValue=fieldVal;
	   												}
	   											}
	   										}
	   									}
	   								}
	   							}
	   						}
			
	   						if(columnName!=null && !columnName.equals(""))
	   							{
	   								if(fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
	   								{
	   									if(ControlParamterId.equals("")){
	   										ControlParamterId = ""+reportsFieldSection.getReportId().toString()+"";
	   									}
	   									else{
	   										ControlParamterId = ControlParamterId+","+""+reportsFieldSection.getReportId().toString()+"";
	   									}
		
	   								}else
	   								{
	   									if(!fieldVal.toLowerCase().equals(reportFieldValue.toLowerCase()))
	   									{
	   										if(ControlParamterNotId.equals(""))
	   										{
	   											ControlParamterNotId = ""+reportsFieldSection.getReportId().toString()+"";
	   										}
	   										else
	   										{
	   											ControlParamterNotId = ControlParamterNotId+","+""+reportsFieldSection.getReportId().toString()+"";
	   										}
	   									}
	   								}
	   							}
	   						fieldVal="";
	   				}// For Loop Stop
			Set<String> parameterSet=new TreeSet<String>();
			if(ControlParamterId!=null && !ControlParamterId.equals(""))
				{
					String[] input =ControlParamterId.split(",");
					for( int i = 0; i <= input.length-1; i++)
					{
						String element = input[i];
						parameterSet.add(element);
					}
				}
				else{
	
				}
			Iterator itr1 = parameterSet.iterator();
			while(itr1.hasNext())
				{
					if(controlParamter.equals(""))
					{
						controlParamter = "'"+itr1.next().toString()+"'";
						excuteParameter=true;
					}
					else
					{
						controlParamter = controlParamter+","+"'"+itr1.next().toString()+"'";
						excuteParameter=true;
					}
				}
			if(controlParamter.equals(""))
			{
				controlParamter="'"+controlParamter+"'";
			}


			Set<String> parameterNotSet=new TreeSet();
			if(ControlParamterNotId!=null && !ControlParamterNotId.equals(""))
			{
				String[] input =ControlParamterNotId.split(",");
				for( int j = 0; j <= input.length-1; j++)
				{
					String element = input[j];
					parameterNotSet.add(element);
				}
			}
			else{
	
			}
			Iterator itr2 = parameterNotSet.iterator();
			while(itr2.hasNext())
			{
				if(controlNotParamter.equals(""))
				{
					controlNotParamter = "'"+itr2.next().toString()+"'";
					excuteParameterNotId=true;
				}
				else
				{
					controlNotParamter = controlNotParamter+","+"'"+itr2.next().toString()+"'";
					excuteParameterNotId=true;
				}
			}
			if(controlNotParamter.equals(""))
				{
					controlNotParamter="'"+controlNotParamter+"'";
				}

	}
	   	catch(NullPointerException np)
	   	{
	   		if(controlNotParamter.equals(""))
	   			{
	   				controlNotParamter="'"+controlNotParamter+"'";
	   			}
	   		if(controlParamter.equals(""))
	   			{
	   				controlParamter="'"+controlParamter+"'";
	   			}
	   		np.printStackTrace();
	   	}
	   	catch(Exception e)
	   	{
	   		if(controlNotParamter.equals(""))
	   			{
	   				controlNotParamter="'"+controlNotParamter+"'";
	   			}
	   		if(controlParamter.equals(""))
	   			{
	   				controlParamter="'"+controlParamter+"'";
	   			}
	   		e.printStackTrace();
	   	}
	   	List reports=new ArrayList();
	   try{	
		   if(shipNumber.equals("Collective")){
	   		if(excuteParameter && !excuteParameterNotId){
	   			reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and e.description like '%collective%'   and e.corpID='"+corpID+"' and e.id in("+controlParamter+")");
			}else if (!excuteParameter && excuteParameterNotId)
			{
				reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and e.description like '%collective%'   and e.corpID='"+corpID+"' ");
			}
			else if (excuteParameter && excuteParameterNotId)
			{
				reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and e.description like '%collective%'   and e.corpID='"+corpID+"' and e.id not in("+controlNotParamter+") and e.id in("+controlParamter+")");
			}
			else{
				reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and e.description like '%collective%'   and e.corpID='"+corpID+"' and e.id in( case when "+controlParamter+"="+controlParamter+" then e.id else "+controlParamter+" end) ");
			}
		   }else{
			   if(excuteParameter && !excuteParameterNotId){
		   			reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and (e.description like '%invoice%' or e.description like '%credit%'  )  and NOT (e.description like '%collective%') and e.corpID='"+corpID+"' and e.id in("+controlParamter+")");
				}else if (!excuteParameter && excuteParameterNotId)
				{
					reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and (e.description like '%invoice%' or e.description like '%credit%'  )  and NOT (e.description like '%collective%') and e.corpID='"+corpID+"' ");
				}
				else if (excuteParameter && excuteParameterNotId)
				{
					reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and (e.description like '%invoice%' or e.description like '%credit%'  )  and NOT (e.description like '%collective%') and e.corpID='"+corpID+"' and e.id not in("+controlNotParamter+") and e.id in("+controlParamter+")");
				}
				else{
					reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and (e.description like '%invoice%' or e.description like '%credit%'  )  and NOT (e.description like '%collective%') and e.corpID='"+corpID+"' and e.id in( case when "+controlParamter+"="+controlParamter+" then e.id else "+controlParamter+" end) ");
				}  
		   }
	   	
	   }
		/////////////////////////////////////////////
	  
	   catch(Exception e){
		
		   reports=getHibernateTemplate().find("from Reports e where e.menu='billing' and (e.job like '%"+jobType+"%' or e.job='' or e.job is null) and (e.companyDivision like '%"+companyDivision+"%' or e.companyDivision='' or e.companyDivision is null ) and (e.mode like '%"+modes+"%' or e.mode='' or e.mode is null ) and e.enabled='Yes' and e.autoupload is true and "+billToCodeCondition+" and  (e.description like '%invoice%' ) and NOT (e.description like '%collective%')  and e.corpID='"+corpID+"'");	
	   }
	return reports;
}
	
	public String getDefaultStationaryFromServiceorder(String partnerCode,String sessionCorpID){
		String defaultStationary="";
		if(partnerCode!=null && (!partnerCode.equals(""))){
			String query="select emailPrintOption from partnerprivate where  emailPrintOption is not null and emailPrintOption!='' and partnercode='"+partnerCode+"' and corpid='"+sessionCorpID+"'";
		List reportList =	this.getSession().createSQLQuery(query).list();
		if(reportList!=null && !reportList.isEmpty() &&(reportList.get(0)!=null)){
			defaultStationary=reportList.get(0).toString();
		}
		}
		return defaultStationary;
	}
	
	public List findFormsForAutocomplete(String moduleName,String jrxmlName,String corpId){
		String subModuleName="";
		if(moduleName!=null && !moduleName.equalsIgnoreCase("") && moduleName.equalsIgnoreCase("CustomerFile")){
			subModuleName = "submodule in ('customerFile','serviceOrder')";
		}else if(moduleName!=null && !moduleName.equalsIgnoreCase("") && moduleName.equalsIgnoreCase("ServiceOrder")){
			subModuleName = "submodule in ('serviceOrder','Forwarding','Billing','Miscellaneous','Status','Domestic')";
		}
		List formsList = new ArrayList();
		List reportList = this.getSession().createSQLQuery("select reportName,description,id from reports where reportName like'"+jrxmlName+"%' and module='"+moduleName+"' and "+subModuleName+" and corpID='"+corpId+"' and formReportFlag='F' AND enabled='Yes'").list();
		Iterator it=reportList.iterator();
		DTO formsDto = null;
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			formsDto = new DTO();
			formsDto.setReportName(row[0].toString());
			formsDto.setDescription(row[1].toString());
			formsDto.setId(Long.parseLong(row[2].toString()));
			formsList.add(formsDto);
		}
		return formsList;
	}
	
	public List findReportId(String jrxmlName,String corpId){
		return  getHibernateTemplate().find("select id from Reports where reportName='"+jrxmlName+"' and corpId='"+corpId+"'");
	}
	
}
