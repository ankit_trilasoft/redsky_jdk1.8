package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity 
@Table(name="trucktrailers")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class TruckTrailers extends BaseObject {
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String truckNumber;
	private String ownerPayTo;
	private String trailerCode;
	private String trailerPayTo;
	private Boolean primaryTrailer;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("createdBy", createdBy).append("createdOn",
				createdOn).append("updatedBy", updatedBy).append("updatedOn",
				updatedOn).append("truckNumber", truckNumber).append(
				"ownerPayTo", ownerPayTo).append("trailerCode", trailerCode)
				.append("trailerPayTo", trailerPayTo).append("primaryTrailer",
						primaryTrailer).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TruckTrailers))
			return false;
		TruckTrailers castOther = (TruckTrailers) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(truckNumber,
						castOther.truckNumber).append(ownerPayTo,
						castOther.ownerPayTo).append(trailerCode,
						castOther.trailerCode).append(trailerPayTo,
						castOther.trailerPayTo).append(primaryTrailer,
						castOther.primaryTrailer).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(truckNumber).append(ownerPayTo)
				.append(trailerCode).append(trailerPayTo)
				.append(primaryTrailer).toHashCode();
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=8)
	public String getOwnerPayTo() {
		return ownerPayTo;
	}
	public void setOwnerPayTo(String ownerPayTo) {
		this.ownerPayTo = ownerPayTo;
	}	
	@Column(length=25)
	public String getTrailerCode() {
		return trailerCode;
	}
	public void setTrailerCode(String trailerCode) {
		this.trailerCode = trailerCode;
	}
	@Column(length=8)
	public String getTrailerPayTo() {
		return trailerPayTo;
	}
	public void setTrailerPayTo(String trailerPayTo) {
		this.trailerPayTo = trailerPayTo;
	}
	@Column(length=25)
	public String getTruckNumber() {
		return truckNumber;
	}
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column
	public Boolean getPrimaryTrailer() {
		return primaryTrailer;
	}
	public void setPrimaryTrailer(Boolean primaryTrailer) {
		this.primaryTrailer = primaryTrailer;
	}
	
	
	
	
}
