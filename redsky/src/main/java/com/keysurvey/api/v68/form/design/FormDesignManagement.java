/**
 * FormDesignManagement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public interface FormDesignManagement extends javax.xml.rpc.Service {
    public java.lang.String getFormDesignManagementServicePortAddress();

    public com.keysurvey.api.v68.form.design.FormDesignManagementService getFormDesignManagementServicePort() throws javax.xml.rpc.ServiceException;

    public com.keysurvey.api.v68.form.design.FormDesignManagementService getFormDesignManagementServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
