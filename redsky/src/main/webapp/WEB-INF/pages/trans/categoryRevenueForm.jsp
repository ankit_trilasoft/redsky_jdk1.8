<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
<meta name="heading" content="<fmt:message key='categoryRevenueForm.title'/>"/> 
<title><fmt:message key="categoryRevenueForm.heading"/></title> 
</head>

<s:form id="categoryRevenueForm" name="categoryRevenueForm" action="savecategoryRevenueForm" method="post" validate="true">
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.revenueList' }">
    <c:redirect url="/categoryRevenueList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<div id="Layer1" style="width: 700px" onkeydown="changeStatus();">
<div id="newmnav">
  <ul>
    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Revenue Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    <%-- <li><a onclick="setReturnString('gototab.revenueList');return autoSave();" ><span>Category Revenue List</span></a></li> --%>
    <li><a href="categoryRevenueList.html" ><span>Category Revenue List</span></a></li>
     </ul>
</div><div class="spn">&nbsp;</div>
		
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">		
 <table class="" cellspacing="0" cellpadding="0" border="0" style="width: 100%">
  <tbody>
  	<tr><td align="left" height="7px"></td></tr>
   	<tr>
		 
		   	<td align="left" colspan="3" style="padding-left:30px" class="listwhitetext"><s:select cssClass="list-menu"   name="categoryRevenue.companyDivision" headerKey=" " headerValue=" " list="%{compDevision}" cssStyle="width:205px" onchange="changeStatus();" tabindex="9"/> 
		   	&nbsp;&nbsp;<fmt:message key="payrollform.companyDivision"/></td>
		  	<td></td>
		  	<td></td>
		  	</tr>
  <tr><td align="left" height="7px"></td></tr>
		  	
  <tr>
  	<td align="left" class="listwhitetext" colspan="6">
  	
  	<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0" width="100%" >
		  <tbody>  	
		  	<tr>
			 <td width="35"></td>
		  	<td align="left" class="subcontenttabChild" width="147"><b>
		  	  <fmt:message key="categoryRevenueForm.number"/></b></td>
		  	<td align="left" class="subcontenttabChild" width="250" ><b>
		  	  <fmt:message key="categoryRevenueForm.percent"/></b></td>
		  	<td align="left" class="subcontenttabChild"><b><fmt:message key="categoryRevenueForm.category"/></b></td>
		  
		  	</tr>	  	
		 
		  	<tr height="5px"></tr>
		  	<tr>
		  	<td width="35"></td>
		  	<td align="left" width="147"><s:textfield name="categoryRevenue.numberOfCrew1" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="1"/></td>
		  	<td align="left" width="250"><s:textfield name="categoryRevenue.percentPerPerson1" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="2"/></td>
		  	<td align="left" width="527"><fmt:message key="categoryRevenueForm.category1"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left" width="147"><s:textfield name="categoryRevenue.numberOfCrew2"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="3"/></td>
		  	<td align="left" width="250"><s:textfield name="categoryRevenue.percentPerPerson2" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="4"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category2"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew3"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="5"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson3" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="6"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category3"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew4"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="7"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson4" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="8"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category4"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew5"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="9"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson5" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="10"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category5"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew6"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="11"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson6" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="12"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category6"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew7"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="13"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson7" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="14"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category6"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew8" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="15"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson8" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="16"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category6"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew9" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();"  onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="17"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson9" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="18"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category6"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.numberOfCrew10"  cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="3" size="6"   cssClass="input-text" readonly="false" tabindex="19"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.percentPerPerson10" cssStyle="text-align:right" onchange="CalulateTotalNumber();CalulateTotalPercentage();" onkeydown="return onlyNumsAllowed(event)" maxlength="10" size="12"   cssClass="input-text" readonly="false" tabindex="20"/></td>
		  	<td align="left"><fmt:message key="categoryRevenueForm.category6"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="5px"></td>
		  	</tr>
		  	<tr>
		  	<td></td>
		  	<td align="left"><b>Total Crew</b></td>
		  	<td align="left"><b><fmt:message key="categoryRevenueForm.total"/></b></td>
		  	</tr>
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:textfield name="categoryRevenue.totalCrew"  cssStyle="text-align:right" maxlength="5" size="6"   cssClass="input-textUpper" readonly="true"/></td>
		  	<td align="left"><s:textfield name="categoryRevenue.totalpercentage"  cssStyle="text-align:right" size="12"   cssClass="input-textUpper" readonly="true"/></td>
		  	
		  	</tr>
		  	<tr>
		  
		  	<td></td>
		  	<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{prate}" headerKey="" headerValue="" name="categoryRevenue.secondSet" cssStyle="width:205px" onchange="changeStatus();" tabindex="21"/></td> 
		  	
		  	<td align="left"><fmt:message key="categoryRevenueForm.secondSet"/></td>
		  	</tr>
		  	<tr height="10px"></tr>
		  	</table>
  	
		  
		 </td>
		</tr>
	</tbody>
</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

		  
		<tr><td>
 		  <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.createdOn'/></b></td>
							<td style="width:200px">
							<fmt:formatDate var="categoryRevenueCreatedOnFormattedValue" value="${categoryRevenue.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="categoryRevenue.createdOn" value="${categoryRevenueCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${categoryRevenue.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.createdBy' /></b></td>
							<c:if test="${not empty categoryRevenue.id}">
								<s:hidden name="categoryRevenue.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{categoryRevenue.createdBy}"/></td>
							</c:if>
							<c:if test="${empty categoryRevenue.id}">
								<s:hidden name="categoryRevenue.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='systemDefault.updatedOn'/></b></td>
							<fmt:formatDate var="categoryRevenueupdatedOnFormattedValue" value="${categoryRevenue.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="categoryRevenue.updatedOn" value="${categoryRevenueupdatedOnFormattedValue}"/>
							<td style="width:200px"><fmt:formatDate value="${categoryRevenue.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='systemDefault.updatedBy' /></b></td>
							<c:if test="${not empty categoryRevenue.id}">
								<s:hidden name="categoryRevenue.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{categoryRevenue.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty categoryRevenue.id}">
								<s:hidden name="categoryRevenue.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"  onclick= "return CheckTotalPercentage();"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		<td align="right">
        		<c:if test="${not empty categoryRevenue.id}">
		       <input type="button" class="cssbutton1" value="Add New Record" style="width:120px;" onclick="location.href='<c:url value="/editcategoryRevenueForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table>
</div>
<s:hidden name="categoryRevenue.id"/>
</s:form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript">
function roundOf()
{	
	//alert("alert");
	alert(document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value);
	document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value = Math.round(document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value);
}
	
function changeStatus(){
	document.forms['categoryRevenueForm'].elements['formStatus'].value = '1';
}
	
function autoSave(clickType){
	if(CheckTotalPercentage())
	{
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${categoryRevenue.id}"/>';
			var id1 = document.forms['categoryRevenueForm'].elements['categoryRevenue.id'].value;
			if(document.forms['categoryRevenueForm'].elements['gotoPageString'].value == 'gototab.revenueList'){
				noSaveAction = 'categoryRevenueList.html';
			}
		processAutoSave(document.forms['categoryRevenueForm'], 'savecategoryRevenueForm!saveOnTabChange.html', noSaveAction);
	}else{
	var id1 = document.forms['categoryRevenueForm'].elements['categoryRevenue.id'].value;	
	if (document.forms['categoryRevenueForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='categoryRevenueForm.heading'/>");
		if(agree){
			document.forms['categoryRevenueForm'].action = 'savecategoryRevenueForm!saveOnTabChange.html';
			document.forms['categoryRevenueForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['categoryRevenueForm'].elements['gotoPageString'].value == 'gototab.revenueList'){
				location.href = 'categoryRevenueList.html';
				}
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['categoryRevenueForm'].elements['gotoPageString'].value == 'gototab.revenueList'){
				location.href = 'categoryRevenueList.html';
				}
	}
	}
}
}
}
}
</script>

<script type="text/javascript">
function CheckTotalPercentage()
{	
	var v1=document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value;
	if(v1<100 || v1>100)
	{
		alert("Total Percentage should be 100.\nPlease re-enter the values. ");
		return false;
	}
	return true;
	
	//alert(document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value);
	//document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value = Math.round(document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value);
}
</script>

<script language="JavaScript">
	function CalulateTotalNumber()
	{
		//alert("H");
		var TotalNumber=0*1;
		var N1=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew1'].value;
		var N2=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew2'].value;
		var N3=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew3'].value;
		var N4=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew4'].value;
		var N5=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew5'].value;
		var N6=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew6'].value;
		var N7=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew7'].value;
		var N8=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew8'].value;
		var N9=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew9'].value;
		var N10=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew10'].value;
		TotalNumber=N1*1+N2*1+N3*1+N4*1+N5*1+N6*1+N7*1+N8*1+N9*1+N10*1;
		document.forms['categoryRevenueForm'].elements['categoryRevenue.totalCrew'].value=TotalNumber*1;
	}
</script>

<script language="JavaScript">
	function CalulateTotalPercentage()
	{
		//alert("H");
		var TotalNumberPercentage=0*1;
		var N1=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew1'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson1'].value;
		var N2=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew2'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson2'].value;;
		var N3=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew3'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson3'].value;;
		var N4=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew4'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson4'].value;;
		var N5=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew5'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson5'].value;;
		var N6=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew6'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson6'].value;;
		var N7=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew7'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson7'].value;;
		var N8=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew8'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson8'].value;;
		var N9=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew9'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson9'].value;;
		var N10=document.forms['categoryRevenueForm'].elements['categoryRevenue.numberOfCrew10'].value * document.forms['categoryRevenueForm'].elements['categoryRevenue.percentPerPerson10'].value;;
		TotalNumber=N1*1+N2*1+N3*1+N4*1+N5*1+N6*1+N7*1+N8*1+N9*1+N10*1;
		document.forms['categoryRevenueForm'].elements['categoryRevenue.totalpercentage'].value=TotalNumber*1;
	}
</script>

<script language="JavaScript">
	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||( keyCode==110); 
	}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
try{
CalulateTotalPercentage(100.0);
}
catch(e){}
</script>
		  	