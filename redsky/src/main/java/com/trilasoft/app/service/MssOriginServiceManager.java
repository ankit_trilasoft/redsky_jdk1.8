package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.MssOriginService;

public interface MssOriginServiceManager extends GenericManager<MssOriginService, Long>{

}
