DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_policydocument BEFORE DELETE ON policydocument
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

  
   
   
   IF (OLD.documentName <> '') THEN
       select CONCAT(fieldNameList,'policydocument.documentName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentName,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.fileSize <> '') THEN
       select CONCAT(fieldNameList,'policydocument.fileSize~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileSize,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'policydocument.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
  
   
 
   
   IF (OLD.partnerCode <> '') THEN
       select CONCAT(fieldNameList,'policydocument.partnerCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.description <> '') THEN
       select CONCAT(fieldNameList,'policydocument.description~') into fieldNameList;
       select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.file <> '') THEN
       select CONCAT(fieldNameList,'policydocument.file~') into fieldNameList;
       select CONCAT(oldValueList,OLD.file,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.sectionName <> '') THEN
       select CONCAT(fieldNameList,'policydocument.sectionName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sectionName,'~') into oldValueList;
   END IF;
  
   
   
   IF (OLD.language <> '') THEN
       select CONCAT(fieldNameList,'policydocument.language~') into fieldNameList;
       select CONCAT(oldValueList,OLD.language,'~') into oldValueList;
   END IF;
  
      IF (OLD.fileName <> '') THEN
       select CONCAT(fieldNameList,'policydocument.fileName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileName,'~') into oldValueList;
   END IF;
  
      IF (OLD.fileLocation <> '') THEN
       select CONCAT(fieldNameList,'policydocument.fileLocation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileLocation,'~') into oldValueList;
   END IF;

      IF (OLD.contentFileType <> '') THEN
       select CONCAT(fieldNameList,'policydocument.contentFileType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contentFileType,'~') into oldValueList;
   END IF;
  
      IF (OLD.parentId <> '') THEN
       select CONCAT(fieldNameList,'policydocument.parentId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.parentId,'~') into oldValueList;
   END IF;
  
      IF (OLD.docSequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'policydocument.docSequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.docSequenceNumber,'~') into oldValueList;
   END IF;
  

   CALL add_tblHistory (OLD.id,"policydocument", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END $$
delimiter;