<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="<fmt:message key='stoExtractCreditForm.heading'/>"/> 
	<title><fmt:message key="stoExtractCreditForm.heading"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->
<script language="javascript" type="text/javascript"> 
function checkDateField(){

	if(document.forms['stoExtractCreditForm'].elements['stoRecInvoiceDate'].value=='')
	{
		alert('Please enter Received Invoice Date ');
		return false;
	}

}
		
		
</script>
	

</head>
<body style="background-color:#444444;">
<s:form cssClass="form_magn" id="stoExtractCreditForm" name="stoExtractCreditForm" action="storageExtractCredit" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="Layer1" style="width:100%;"><!-- sandeep -->
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Extract for Credit Card</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">

 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:550px" >
 <tbody>
		  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>


		  	<tr>		  		
		  		<td align="right" width="120px" class="listwhitetext" style="padding-bottom:5px">Received Invoice Date<font color="red" size="2">*</font></td>	
		  		
		  		<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="stoRecInvoiceDate" name="stoRecInvoiceDate"  size="8" maxlength="11" readonly="true" /> 
		  		<img id="stoRecInvoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></td>
			    <td class="listwhitetext" align="right">Company Division</td>
			    <td ><s:select cssClass="list-menu" cssStyle="width:100px"  name="companyDivision" list="%{companyCodeList}"  headerKey="%" headerValue="All" /></td>
			</tr>
				  	
		  	<tr>		  	  		
	  			<td width="50px"></td>			
				<td colspan="4"  style="padding-left:4px;"><s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" method="storageExtractCredit" value="Extract" onclick="return checkDateField();" />	  		 				           	
				<s:reset cssClass="cssbutton" key="Clear" cssStyle="width:70px; height:25px "/>   
				</td>
        	</tr>
        	<tr>
		  		<td align="left" height="20px"></td>
		  	</tr>
        	</tbody>
        	</table>
        	 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
         

</div>

</s:form>

<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
</script>

		