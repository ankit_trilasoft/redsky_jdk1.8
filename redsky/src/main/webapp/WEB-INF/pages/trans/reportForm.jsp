<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
    <title><fmt:message key="reportsList.title"/></title>
    <meta name="heading" content="<fmt:message key='reportsList.heading'/>"/>

 <style type="text/css">
table.pickList11 td select {
    width: 400px;
}
form .info {
text-align:left;
padding-left:15px;
}

</style>
<script type="text/javascript">
function formTypes(){
	var formTypeVal = document.forms['reportForm'].elements['reports.formReportFlag'].value;
	if(formTypeVal == "F"){
		document.getElementById("formTypeValF1").style.display = "block";
		document.getElementById("formTypeValF2").style.display = "block";
		document.getElementById("formTypeValF3").style.display = "block";
		document.getElementById("formTypeValF4").style.display = "block";
		document.getElementById("formTypeValR1").style.display = "none";
		document.getElementById("formTypeValR2").style.display = "none";
		document.getElementById("formTypeValR3").style.display = "none";
		document.getElementById("formTypeValR4").style.display = "none";
		document.getElementById("formTypeValR5").style.display = "none";
		document.getElementById("formTypeValR6").style.display = "none";
		document.getElementById("controlDiv").style.display = "block";
		document.getElementById("extractThemeId").style.display = "none";
	}
	if(formTypeVal=='R'){
		document.getElementById("formTypeValF1").style.display = "none";
		document.getElementById("formTypeValF2").style.display = "none";
		document.getElementById("formTypeValF3").style.display = "none";
		document.getElementById("formTypeValF4").style.display = "none";
		document.getElementById("formTypeValR1").style.display = "block";
		document.getElementById("formTypeValR2").style.display = "block";
		document.getElementById("formTypeValR3").style.display = "block";
		document.getElementById("formTypeValR4").style.display = "block";
		document.getElementById("formTypeValR5").style.display = "block";
		document.getElementById("formTypeValR6").style.display = "block";
		document.getElementById("controlDiv").style.display = "none";
		document.getElementById("extractThemeId").style.display = "block";
	}
	}
	
/* function validateCategory(){
	var category = document.getElementById('reports.menu').value;
	if(category=='' || category==null){
		alert("Report Category is a required field.");
		return false;
	}
} */
	
</script>
<script type="text/javascript">

function onFormSubmit(theForm) {
	selectAll('reportsRoles');
	savefieldList();
	savefieldControlList();
}
</script>
<script language="JavaScript">


 function addRow(tableID) {
	var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    document.forms['reportForm'].elements['rowCountvalue'].value=rowCount;
    var newlineid=document.getElementById('newlineid').value;              
    if(newlineid==''){
       newlineid=rowCount;
    	}else{
        	newlineid=newlineid+"~"+rowCount;
     	}
      document.getElementById('newlineid').value=newlineid; 
      
      var row = table.insertRow(rowCount); 
      
      var cell0 = row.insertCell(0);
      cell0.innerHTML = rowCount;
      cell0.setAttribute("class", "listwhitetext" );
      var element1 = document.createElement("input");
      element1.type = "hidden";
      element1.id='num'+rowCount;
      element1.value=rowCount;
      element1.name='reportNumberList';
      cell0.appendChild(element1);

      
      var cell1 = row.insertCell(1);
      var element2 = document.createElement("input");
      element2.type = "text";
      element2.setAttribute("class", "input-text" );
      element2.setAttribute("style", "width:70%" );
      element2.id='fieldname'+rowCount;
      cell1.appendChild(element2);
     
      var cell2 = row.insertCell(2);
      var element3 = document.createElement("input");
      element3.type = "text";
      element3.id='fieldvalue'+rowCount;
      element3.setAttribute("class", "input-text" );
      element3.setAttribute("style", "width:40%" );
      cell2.appendChild(element3);            

            
  }
  
  
function savefieldList(){
	var id='';
    var fieldnameUpdated="";
    var fieldvalueUpdated="";
	
	  var are=document.forms['reportForm'].elements['rowCountvalue'].value;
		 if(are==null||are==''){
			 are='${reportfieldSectionListSize}';
		 }
		 if(document.forms['reportForm'].fieldnameList!=undefined){
			 if(are!=1){
				  for (i=0; i<document.forms['reportForm'].fieldnameList.length; i++){
		        	 id=document.forms['reportForm'].fieldnameList[i].id;
	                 id=id.replace(id.substring(0,9),'').trim();
	 	             if(fieldnameUpdated==''){
	 	            	fieldnameUpdated=id+":"+document.forms['reportForm'].fieldnameList[i].value;
	 	             }else{
	 	            	fieldnameUpdated= fieldnameUpdated+"~"+id+":"+document.forms['reportForm'].fieldnameList[i].value;
	 	             }
		 	      }	
		 	 }else{
		 			 id=document.forms['reportForm'].fieldnameList.id;
		 	         id=id.replace(id.substring(0,9),'').trim(); 
		 	        fieldnameUpdated=id+": "+document.forms['reportForm'].fieldnameList.value;
		 	 }	        
		 }
		 if(document.forms['reportForm'].fieldvalueList!=undefined){
			if(are!=1){
	             for (i=0; i<document.forms['reportForm'].fieldvalueList.length; i++){	
		 	    	 id=document.forms['reportForm'].fieldvalueList[i].id;
	                 id=id.replace(id.substring(0,10),'').trim();
	 	             if(fieldvalueUpdated==''){
	 	            	fieldvalueUpdated=id+":"+document.forms['reportForm'].fieldvalueList[i].value;
	 	             }else{
	 	            	fieldvalueUpdated= fieldvalueUpdated+"~"+id+":"+document.forms['reportForm'].fieldvalueList[i].value;
	 	             }
		 	     }	
  	        }else{
		 	    	id=document.forms['reportForm'].fieldvalueList.id;
		 	        id=id.replace(id.substring(0,10),'').trim();   
		 	        fieldvalueUpdated=id+": "+document.forms['reportForm'].fieldvalueList.value;
		 	}	        
		 }
		 document.getElementById('fieldnameUpdated').value=fieldnameUpdated;
 		 document.getElementById('fieldvalueUpdated').value=fieldvalueUpdated;
	     var newlist="";
	     var newlineid=document.getElementById('newlineid').value; 
	     if(newlineid!=''){
	            var arrayLine=newlineid.split("~");
	            for(var i=0;i<arrayLine.length;i++){
	            	if(document.getElementById('fieldname'+arrayLine[i]).value!='' && document.getElementById('fieldvalue'+arrayLine[i]).value!=''){
		                if(newlist==''){
		                	newlist=document.getElementById('fieldname'+arrayLine[i]).value+":"+document.getElementById('fieldvalue'+arrayLine[i]).value;
		                }else{
		                	newlist=newlist+"~"+document.getElementById('fieldname'+arrayLine[i]).value+":"+document.getElementById('fieldvalue'+arrayLine[i]).value;
		                }
	            	}
	              }
	     }
		  document.getElementById('reportfieldsectionlist').value =newlist;
		} 

function addRowControl(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  document.forms['reportForm'].elements['controlRowCountValue'].value=rowCount;
  var newlineid=document.getElementById('newControlLineId').value;              
  if(newlineid==''){
     newlineid=rowCount;
  	}else{
      	newlineid=newlineid+"~"+rowCount;
   	}
    document.getElementById('newControlLineId').value=newlineid; 
    
    var row = table.insertRow(rowCount); 
    
    var cell0 = row.insertCell(0);
    cell0.innerHTML = rowCount;
    cell0.setAttribute("class", "listwhitetext" );
    var element1 = document.createElement("input");
    element1.type = "hidden";
    element1.id='num'+rowCount;
    element1.value=rowCount;
    element1.name='controlReportNumberList';
    cell0.appendChild(element1);

    
    var cell1 = row.insertCell(1);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.setAttribute("class", "input-text" );
    element2.setAttribute("style", "width:70%" );
    element2.id='controlFieldname'+rowCount;
    cell1.appendChild(element2);
   
    var cell2 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.id='controlFieldvalue'+rowCount;
    element3.setAttribute("class", "input-text" );
    element3.setAttribute("style", "width:40%" );
    cell2.appendChild(element3);
          
}
	   
function savefieldControlList(){
	var id='';
    var controlFieldNameUpdated="";
    var controlFieldValueUpdated="";
	
	  var are=document.forms['reportForm'].elements['controlRowCountValue'].value;
	   if(are==null||are==''){
			 are='${reportfieldSectionListControlSize}';
		 }
		 if(document.forms['reportForm'].controlFieldnameList!=undefined){
			 if(are!=1){
		    	  for (i=0; i<document.forms['reportForm'].controlFieldnameList.length; i++){	
		 	    	 id=document.forms['reportForm'].controlFieldnameList[i].id;
		 	    	 id=id.replace(id.substring(0,16),'').trim();
	                 if(controlFieldNameUpdated==''){
	 	            	controlFieldNameUpdated=id+":"+document.forms['reportForm'].controlFieldnameList[i].value;
	 	             }else{
	 	            	controlFieldNameUpdated= controlFieldNameUpdated+"~"+id+":"+document.forms['reportForm'].controlFieldnameList[i].value;
	 	             }
		 	      }	
		 	 }else{
		 	    	 id=document.forms['reportForm'].controlFieldnameList.id;
		 	         id=id.replace(id.substring(0,16),'').trim(); 
		 	        controlFieldNameUpdated=id+": "+document.forms['reportForm'].controlFieldnameList.value;
		 	 }	        
		 }
		 if(document.forms['reportForm'].controlFieldvalueList!=undefined){
	        if(are!=1){
		 	     for (i=0; i<document.forms['reportForm'].controlFieldvalueList.length; i++){	
		 	    	 id=document.forms['reportForm'].controlFieldvalueList[i].id;
	                 id=id.replace(id.substring(0,17),'').trim();
	 	             if(controlFieldValueUpdated==''){
	 	            	controlFieldValueUpdated=id+":"+document.forms['reportForm'].controlFieldvalueList[i].value;
	 	             }else{
	 	            	controlFieldValueUpdated= controlFieldValueUpdated+"~"+id+":"+document.forms['reportForm'].controlFieldvalueList[i].value;
	 	             }
		 	     }	
  	        }else{
  	        		id=document.forms['reportForm'].controlFieldvalueList.id;
		 	        id=id.replace(id.substring(0,17),'').trim();   
		 	       controlFieldValueUpdated=id+": "+document.forms['reportForm'].controlFieldvalueList.value;
		 	}	        
		 }
		 document.getElementById('controlFieldNameUpdated').value=controlFieldNameUpdated;
 		 document.getElementById('controlFieldValueUpdated').value=controlFieldValueUpdated;
	     var newlist="";
	     var newlineid=document.getElementById('newControlLineId').value;
	     if(newlineid!=''){
	            var arrayLine=newlineid.split("~");
	            for(var i=0;i<arrayLine.length;i++){
	            	if(document.getElementById('controlFieldname'+arrayLine[i]).value!='' && document.getElementById('controlFieldvalue'+arrayLine[i]).value!=''){
	                    if(newlist==''){
		                	newlist=document.getElementById('controlFieldname'+arrayLine[i]).value+":"+document.getElementById('controlFieldvalue'+arrayLine[i]).value;
		                }else{
		                	newlist=newlist+"~"+document.getElementById('controlFieldname'+arrayLine[i]).value+":"+document.getElementById('controlFieldvalue'+arrayLine[i]).value;
		                }
	            	}
	              }
	     }
	     
		  document.getElementById('reportfieldSectionControllist').value =newlist;
		}	   
	   
 function checkMandatoryAddLineField(){
	  var newlineid=document.getElementById('newlineid').value;
	  var tempId=''
	  <c:forEach var="individualItem" items="${reportfieldSectionList}" >
	    if(tempId==''){
			  tempId='${individualItem.id}';
		  }else{
			  tempId=tempId+'~${individualItem.id}';
		  }
	  </c:forEach>
	  if(tempId!=''){
			  if(newlineid==''){
				  newlineid=tempId;
			  }else{
				  newlineid=tempId+"~"+newlineid;
			  }
	  }
	  var flag=true;
	  var msg='';
	  
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("~");
   		 var fieldname='';
		 var fieldvalue='';		
         for(var i=0;i<arrayLine.length && flag;i++){
	   		  fieldname=document.getElementById('fieldname'+arrayLine[i]).value;
			  fieldvalue=document.getElementById('fieldvalue'+arrayLine[i]).value;																
	          if(((fieldname=="") && (fieldvalue!=""))||((fieldname!="") && (fieldvalue==""))){
		    	 msg="Filling One Field Will Not Populate The Form Via Email.";
		    	flag=false;
	          }else if((fieldname=="") && (fieldvalue=="") && flag){
	        	  msg="FieldName And Value For Line # "+(i+1)+" Is Blank, Do you want Continue.";
	          }else{
	        	  
	          }
         }
      }
	  if(!flag){
		  alert(msg);
		  return false;
	  }else if(msg!=''){
		  var agree = confirm(msg);
	         if(agree){
	        	 return true;
	         }else{
	        	 return false;
	         }
	  }else{
		  return true;
	  }
} 

function checkMandatoryAddLineFieldControl(){
	  var newlineid=document.getElementById('newControlLineId').value;
	  var tempId=''
	  <c:forEach var="individualItem" items="${reportfieldSectionControlList}" >
	    if(tempId==''){
			  tempId='${individualItem.id}';
		  }else{
			  tempId=tempId+'~${individualItem.id}';
		  }
	  </c:forEach>
	  if(tempId!=''){
			  if(newlineid==''){
				  newlineid=tempId;
			  }else{
				  newlineid=tempId+"~"+newlineid;
			  }
	  }
	  var flag=true;
	  var msg='';
	  
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("~");
   		 var fieldname='';
		 var fieldvalue='';		
         for(var i=0;i<arrayLine.length && flag;i++){
	   		  fieldname=document.getElementById('controlFieldname'+arrayLine[i]).value;
	   		  fieldvalue=document.getElementById('controlFieldname'+arrayLine[i]).value;
	   		  if(((fieldname=="") && (fieldvalue!=""))||((fieldname!="") && (fieldvalue==""))){
		    	 msg="Filling One Field Will Not Populate The Form Via Email.";
		    	flag=false;
	          }else if((fieldname=="") && (fieldvalue=="") && flag){
	        	  msg="FieldName And Value For Line # "+(i+1)+" Is Blank, Do you want Continue.";
	          }else{
	        	  
	          }
         }
      }
	  if(!flag){
		  alert(msg);
		  return false;
	  }else if(msg!=''){
		  var agree = confirm(msg);
	         if(agree){
	        	 return true;
	         }else{
	        	 return false;
	         }
	  }else{
		  return true;
	  }
}
	

 
 function changeStatus(){
	/* document.forms['auditSetupForm'].elements['formStatus'].value = '1'; */
	document.forms['reportForm'].elements['formStatus'].value = '1';
}
	
	
	function autoSave(clickType){
	
	if(!(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){

			var noSaveAction = '<c:out value="${reports.id}"/>';

			var id1 = document.forms['reportForm'].elements['reports.id'].value;

			if(document.forms['reportForm'].elements['gotoPageString'].value == 'gototab.reportList'){

			noSaveAction = 'reportList.html';

			}
			
		processAutoSave(document.forms['reportForm'], 'saveReport!saveOnTabChange.html', noSaveAction);

		}else{
			var id1 = document.forms['reportForm'].elements['reports.id'].value;
			
			if (document.forms['reportForm'].elements['formStatus'].value == '1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Audit Setup Form");
				if(agree){
					document.forms['reportForm'].action = 'saveReport!saveOnTabChange.html';
					document.forms['reportForm'].submit();
				}else{
					if(id1 != ''){
					if(document.forms['reportForm'].elements['gotoPageString'].value == 'gototab.reportList'){
						location.href = 'reportList.html';
						}
					
							
				}
		}
	}else{
		if(id1 != ''){
					if(document.forms['reportForm'].elements['gotoPageString'].value == 'gototab.reportList'){	
							location.href = 'reportList.html';
							}
			
			
					}
			}
		}
	}
}
 	
 	
 	function validateSecForm()
 	{
 	 	if(document.forms['reportForm'].elements['reports.docsxfer'].value=='No'){
 	 		document.forms['reportForm'].elements['reports.targetCabinet'].disabled=true;
	 	 	document.forms['reportForm'].elements['reports.secureForm'].value='No';
	 	 	document.forms['reportForm'].elements['reports.secureForm'].disabled=true;
 		}
 		else{
 		document.forms['reportForm'].elements['reports.targetCabinet'].disabled=false;
 		document.forms['reportForm'].elements['reports.secureForm'].disabled=false;
 		}
 	}
function checkJobValidation(target){ 	
var job=document.forms['reportForm'].elements['reports.job'].value;
job=job.trim();
if(job!=''){
var jobLength = job.length;
var reminder = ((jobLength-1)%6);
var startPoint = job.substring(0,2)
var endPoint = job.substring(jobLength-2,jobLength)
if(startPoint!="('")
{
    
	alert("Incorrect Job Type");
	document.forms['reportForm'].elements['reports.job'].select();
	return false;
}
else if(endPoint!="')")
{
	
	alert("Incorrect Job Type");
	document.forms['reportForm'].elements['reports.job'].select();
	return false;
}
else if(reminder > 0)
{
	alert("Incorrect Job Type");
	document.forms['reportForm'].elements['reports.job'].select();
	return false;
}
else if(target!='save')
{
	alert("Correct job type");
}
}
return true;
}
</script>

<script type="text/javascript">
function checkCompanyDivisionValidation(target){ 	
var cmpDiv=document.forms['reportForm'].elements['reports.companyDivision'].value;
cmpDiv=cmpDiv.trim();
if(cmpDiv!=''){
var cmpDivLength = cmpDiv.length;
var reminder = ((cmpDivLength-1)%(cmpDivLength-1));
var startPoint = cmpDiv.substring(0,2)
var endPoint = cmpDiv.substring(cmpDivLength-2,cmpDivLength)
if(startPoint!="('")
{
    
	alert("Incorrect company division");
	document.forms['reportForm'].elements['reports.companyDivision'].select();
	return false;
}
else if(endPoint!="')")
{
	
	alert("Incorrect company division");
	document.forms['reportForm'].elements['reports.companyDivision'].select();
	return false;
}
else if(reminder > 0)
{
	alert("Incorrect  company division");
	document.forms['reportForm'].elements['reports.companyDivision'].select();
	return false;
}
else if(target!='save')
{
	alert("Correct company division");
}
}
return true;
}
 	
</script>

<script type="text/javascript">
function checkModeValidation(target){ 	
var mode=document.forms['reportForm'].elements['reports.mode'].value;
mode=mode.trim();
if(mode!=''){
var modeLength = mode.length;
var startPoint = mode.substring(0,2)
var endPoint = mode.substring(modeLength-2,modeLength)
if(startPoint!="('")
{
	alert("Incorrect Pattern for Mode");
	document.forms['reportForm'].elements['reports.mode'].select();
	return false;
}
else if(endPoint!="')")
{	
	alert("Incorrect Pattern for Mode");
	document.forms['reportForm'].elements['reports.mode'].select();
	return false;
}
else if(target!='save')
{
	for (i = 0; i < mode.length; i++){   
     var c = mode.charAt(i);
     var c1 = mode.charAt(i+1);
     if(((c == "'") && (c1 == "'")) || ((c == ",") && (c1 != "'")) || ((c != "'") && (c1 == ",")) || (c == " ")) {
     alert("Incorrect Pattern for Mode");
     document.forms['reportForm'].elements['reports.mode'].select();
	 return false;
     }}
    checkMode();
}
}
return true;
}

</script>
<script type="text/javascript">

function checkMode() {
	var modes= document.forms['reportForm'].elements['reports.mode'].value;
	var mode1=modes.replace("(",'');
	mode1 = mode1.replace(")",'');
	var url="checkModes.html?ajax=1&decorator=simple&popup=true&modes="+encodeURI(mode1);
	http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponse5;
    http5.send(null);
}


function handleHttpResponse5()
        {
		  if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                var res = results.replace("[",'');
                res = res.replace("]",'');
                if(res.length-1 > 1){
                alert("Invalid Mode. "+res);
                document.forms['reportForm'].elements['reports.billToCode'].value='';
                } else {
                alert("Validated Successfully.");
                }
			}	
        }

</script>
<script type="text/javascript">
function checkBillToCodeValidation(target){ 	
var billCode=document.forms['reportForm'].elements['reports.billToCode'].value;
billCode=billCode.trim();
if(billCode!=''){
var billCodeLength = billCode.length;
var startPoint = billCode.substring(0,2)
var endPoint = billCode.substring(billCodeLength-2,billCodeLength)
if(startPoint!="('")
{
	alert("Incorrect Pattern for Bill To Code");
	document.forms['reportForm'].elements['reports.billToCode'].select();
	return false;
}
else if(endPoint!="')")
{	
	alert("Incorrect Pattern for Bill To Code");
	document.forms['reportForm'].elements['reports.billToCode'].select();
	return false;
}
else if(target!='save')
{
	for (i = 0; i < billCode.length; i++){   
    var c = billCode.charAt(i);
    var c1 = billCode.charAt(i+1);
     if(((c == "'") && (c1 == "'")) || ((c == ",") && (c1 != "'")) || ((c != "'") && (c1 == ",")) || (c == " ")) {
     alert("Incorrect Pattern for Bill To Code");
     document.forms['reportForm'].elements['reports.billToCode'].select();
	 return false;
     }}
    checkBillCode();
}
}
return true;
}




function checkBillCode() {
	var billCodes= document.forms['reportForm'].elements['reports.billToCode'].value;
	var billCode=billCodes.replace("(",'');
	billCode = billCode.replace(")",'');
	var url="checkBillCodes.html?ajax=1&decorator=simple&popup=true&billCode="+encodeURI(billCode);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse4;
    http4.send(null);
}


function handleHttpResponse4()
        {
		  if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                var res = results.replace("[",'');
                res = res.replace("]",'');
                if(res.length-1 > 1){
                alert("Invalid Bill-To-Code.");
                // document.forms['reportForm'].elements['reports.billToCode'].value='';
                } else {
                alert("Validated Successfully.");
                }
			}	
        }
        
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
    }	
    var http4 = getHTTPObject();  
    
    
    
    function getHTTPObject1() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
    }
    var http5 = getHTTPObject1();
        
    function checkBillToCodeBlank(){
    	if(document.forms['reportForm'].elements['reports.excludeFromForm'].checked){
        	var billToCode = document.forms['reportForm'].elements['reports.billToCode'].value;
        	if(billToCode==''){
        		alert('Please fill Bill To Code.');
        		document.forms['reportForm'].elements['reports.excludeFromForm'].checked = false;
        	}
    	}
    }
    
    
</script>	
<script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
</head>
<s:form id="reportForm" name="reportForm" action="saveReport" method="post" onsubmit="return checkMandatoryAddLineField();checkMandatoryAddLineFieldControl();" validate="true">
<s:hidden name="rowCountvalue"/>
<s:hidden name="controlRowCountValue"/>
<s:hidden name="newlineid" id="newlineid" />
<s:hidden name="newControlLineId" id="newControlLineId" />
<s:hidden name="reportfieldsectionlist" id="reportfieldsectionlist" />
<s:hidden name="reportfieldSectionControllist" id="reportfieldSectionControllist" /> 
<s:hidden name="fieldnameUpdated" id="fieldnameUpdated" />
<s:hidden name="fieldvalueUpdated" id="fieldvalueUpdated" />
<s:hidden name="controlFieldNameUpdated" id="controlFieldNameUpdated" />
<s:hidden name="controlFieldValueUpdated" id="controlFieldValueUpdated" />  

<s:hidden name="reports.id" value="%{reports.id}"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>



<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
		<c:when test="${gotoPageString == 'gototab.reportList' }">
	    	<c:redirect url="/reportList.html"/>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</c:if> 


			<c:set var="ischecked1" value="false"/>
			<c:if test="${reports.csv}">
			<c:set var="ischecked1" value="true"/>
			</c:if>
			<c:set var="ischecked2" value="false"/>
			<c:if test="${reports.xls}">
			<c:set var="ischecked2" value="true"/>
			</c:if>
			<c:set var="ischecked3" value="false"/>
			<c:if test="${reports.html}">
			<c:set var="ischecked3" value="true"/>
			</c:if>
			<c:set var="ischecked4" value="false"/>
			<c:if test="${reports.pdf}">
			<c:set var="ischecked4" value="true"/>
			</c:if>
			<c:set var="ischecked5" value="false"/>
			<c:if test="${reports.rtf}">
			<c:set var="ischecked5" value="true"/>
			</c:if>			
			<c:set var="ischecked6" value="false"/>
			<c:if test="${reports.extract}">
			<c:set var="ischecked6" value="true"/>
			</c:if>
			<c:set var="ischecked7" value="false"/>
			<c:if test="${reports.docx}">
			<c:set var="ischecked7" value="true"/>
			</c:if>			

<div id="Layer1" onkeydown="changeStatus()" style="width:100%;">

	<div id="newmnav">
	  	<ul>
	  		<!--<li><a href="reportList.html"><span>Reports List</span></a></li>-->
	  		<li><a onclick="onFormSubmit(this.form);setReturnString('gototab.reportList');return autoSave('none');"><span>Reports List</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current""><span>Report Information<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${reports.id}&tableName=reports,reports_role&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="2" border="0">
	<tbody>
		
		<tr>
			<td class="listwhitetext" align="right">Type<font color="red" size="2">*</font></td>
			<td><s:select cssClass="list-menu" id="formReportFlag" name="reports.formReportFlag" list="%{reportFlag}" headerValue="" headerKey="" cssStyle="width:250px" onchange="formTypes();"/></td>
			<td class="listwhitetext" align="right">Enabled</td>
			<td width="50"><s:select cssClass="list-menu" name="reports.enabled" list="{'Yes','No'}" cssStyle="width:50px;"/>
			<td class="listwhitetext" align="right">Accessed</td>
			<td width="50"><s:textfield key="reports.reportHits" readonly="true" cssClass="input-textUpper" cssStyle="width:46px;" /></td>
			<td class="listwhitetext" align="left">&nbsp;&nbsp;Auto&nbsp;Upload
			<s:checkbox cssStyle="vertical-align:middle;" name="reports.autoupload"></s:checkbox></td>
			<td class="listwhitetext" align="left" colspan="2">&nbsp;&nbsp;Is&nbsp;Multi&nbsp;Lingual
			<s:checkbox cssStyle="vertical-align:middle;" name="reports.isMultiLingual"></s:checkbox></td>
		</tr>
		<tr><td colspan="8" height="2px"></td></tr>
		<tr>
			<td width="90px" class="listwhitetext" align="right">Report&nbsp;Category<font color="red" size="2">*</font></td>
			<%-- <td style="width:150px;"><s:textfield key="reports.menu" cssClass="input-text" maxLength="100" cssStyle="width:246px;" /></td> --%>
			<td><s:select cssClass="list-menu" name="reports.menu" id="reports.menu" list="%{reportCategory}" cssStyle="width:250px" headerKey="" headerValue=""/></td>
			
			<td class="listwhitetext" align="right" colspan="0">Param Required</td> 
			<td><s:select cssClass="list-menu" name="reports.runTimeParameterReq" list="{'Yes','No'}" cssStyle="width:50px" /></td>
			<td class="listwhitetext" align="right" width="62">File&nbsp;Upload</td>
			<td colspan="6">
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1" style="margin:0px;padding:0px;">
			<tr>
			<td><s:select cssClass="list-menu" name="reports.docsxfer" list="{'Yes','No'}" cssStyle="width:50px" onchange="validateSecForm(this);"/></td>
			<td class="listwhitetext" align="right">Target&nbsp;Cabinet</td>
			<td><s:select cssClass="list-menu" name="reports.targetCabinet" list="{'Truck','Partner','Invoice Client'}" cssStyle="width:70px" /></td>
			<td class="listwhitetext" align="right">Secure&nbsp;Form</td>
			<td><s:select cssClass="list-menu" name="reports.secureForm" list="{'No','Yes'}" cssStyle="width:50px" /></td>
			</tr>
			</table> 
			</td>		
		</tr>
		<tr><td colspan="8" height="2px"></td></tr>
		<tr>
		<td class="listwhitetext" align="right">Module<font color="red" size="2">*</font></td>
			<td><s:select cssClass="list-menu" name="reports.module" list="%{moduleReport}" cssStyle="width:250px" headerKey="" headerValue=""/></td>
			<td class="listwhitetext" align="right">Email&nbsp;Out</td>
			<td width="50"><s:select cssClass="list-menu" name="reports.emailOut" list="{'No','Yes'}" cssStyle="width:50px" /></td>			
		</tr>
		<tr><td colspan="9" height="2px"></td></tr>
		<tr>							
			<td class="listwhitetext" align="right">Sub Module<font color="red" size="2">*</font></td>
			<td colspan="1"><s:select cssClass="list-menu" name="reports.subModule" list="%{subModuleReport}" cssStyle="width:250px" headerKey="" headerValue="" /></td>
			
			<td valign="middle" rowspan="5" class="listwhitetext" width="120" align="right">Email&nbsp;Body</td>
			<td colspan="5" rowspan="5" valign="top" ><s:textarea  name="reports.emailBody" cssStyle="height:73px;width:323px;" readonly="false" cssClass="textarea"/></td>
			
		</tr>
		<tr><td colspan="9" height="2px"></td></tr>
		<tr>
			<td class="listwhitetext" align="right">Report Name<font color="red" size="2">*</font></td>
			<td colspan="7"><s:textfield key="reports.description" cssClass="input-text" cssStyle="width:246px;" maxLength="100"/></td>	
		</tr>
		<tr><td colspan="9" height="2px"></td></tr>	
		<tr>		
			<td class="listwhitetext" align="right">Report&nbsp;Actual&nbsp;Name<font color="red" size="2">*</font></td>
			<td colspan="0"><s:textfield key="reports.reportName" cssClass="input-text" cssStyle="width:246px;" maxLength="100"/></td>					
		</tr>
		<tr><td colspan="9" height="2px"></td></tr>	
		<tr>
			<td valign="top" class="listwhitetext" align="right" style="padding-top:6px;">Report&nbsp;Description</td>
			<td colspan="1" valign="top" ><s:textarea  name="reports.reportComment" rows="4" cols="40" cssStyle="width:245px;height:102px;" readonly="false" cssClass="textarea"/></td>
			<td colspan="6" rowspan="2" valign="top">
				<table style="margin: 0px;">
					<tr>
					<td width="90px" class="listwhitetext" align="right">URL&nbsp;attached&nbsp;with&nbsp;Email</td>
					<td colspan="6" style="width:150px;" ><s:textfield key="reports.attachedURL" cssClass="input-text" maxLength="149" cssStyle="width:325px;" /></td>	
					</tr>
					<tr>
						<td class="listwhitetext" align="right">Agent Role</td>
						<td style="padding-top: 4px;" align="left"><s:select
										name="reports.agentRoles" list="%{roleMap}"
										value="%{multiplRoleType}" multiple="true" cssClass="list-menu"
										cssStyle="width:150px;height:78px" headerKey="" headerValue="" />
								</td>
					<tr>
						<td class="listwhitetext" align="right">Sequence&nbsp;Number</td>
						<td style="padding-top: 4px;"><s:select headerKey="" headerValue="" cssClass="list-menu" name="reports.sequenceNo" list="{'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'}" cssStyle="width:150px" /></td>
					</tr>
					<tr>
					<td colspan="5" >
				<div id="extractThemeId">
				<table style="margin:0px;">
						<tr>
						<td class="listwhitetext" align="right" style="width:113px;">Extract&nbsp;Theme</td>
						<td><s:select cssClass="list-menu" name="reports.extractTheme" list="{'','RED'}" cssStyle="width:150px" headerKey="" headerValue=""/></td>
						</tr>
					</table>
					</div>
			</td>
			</tr>
				</table>
			</td>
		</tr>
		
		<tr>	
			<td class="listwhitetext" valign="top" colspan="2">
				<fieldset style="width:333px;!width:335px;">
					<legend>Report Output</legend>
					
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
						<tr>	
				  			<td align="right" width="30px;">
				  			<div id="formTypeValF1">
				  				<s:checkbox key="reports.rtf"  value="${ischecked5}" fieldValue="true"/>
				  			</div>
				  			</td>
					  		<td align="left" class="listwhitetext">
					  		<div id="formTypeValF2">
					  			<fmt:message key="reports.rtf"/>
					  		</div>
					  		</td>		  	
					  		<td align="right" width="30px;">
					  		<div id="formTypeValF3">
					  			<s:checkbox key="reports.docx"  value="${ischecked7}" fieldValue="true"/>
					  		</div>
					  		</td>
					  		<td align="left" class="listwhitetext"><div id="formTypeValF4">DOCX</div></td>
					  		<td align="right" width="30px;">
					  			<s:checkbox key="reports.pdf"  value="${ischecked4}" fieldValue="true"/>
					  		</td>
					  		<td class="listwhitetext">
					  			<fmt:message key="reports.pdf"/>&nbsp;&nbsp;</td>
					  		<td align="right">
					  		<div id="formTypeValR1">
					  			<s:checkbox key="reports.csv"  value="${ischecked1}" fieldValue="true"/>
					  			</div>
					  		</td>
					  		<td class="listwhitetext">
					  		<div id="formTypeValR2">
					  			<fmt:message key="reports.csv"/>&nbsp;&nbsp;</div></td>
							<td align="right" width="30px;">
							<div id="formTypeValR3">
								<s:checkbox key="reports.xls" value="${ischecked2}" fieldValue="true"/>
								</div>
							</td>
					  		<td class="listwhitetext">
					  		<div id="formTypeValR4">
					  			<fmt:message key="reports.xls"/>&nbsp;&nbsp;</div></td>				
					  		<td align="right" width="30px;">
					  		<div id="formTypeValR5">
					  			<s:checkbox key="reports.extract"  value="${ischecked6}" fieldValue="true"/>
					  			</div>
					  		</td>
					  		<td align="left" class="listwhitetext"><div id="formTypeValR6">EXTRACT</div></td>		  	
				  		</tr>		  		
					</table>
					<%-- <div id="formTypeValR">
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
						<tr>	
							<td align="right"><s:checkbox key="reports.csv"  value="${ischecked1}" fieldValue="true"/></td>
					  		<td class="listwhitetext"><fmt:message key="reports.csv"/>&nbsp;&nbsp;</td>
							<td align="right" width="30px;"><s:checkbox key="reports.xls" value="${ischecked2}" fieldValue="true"/></td>
					  		<td class="listwhitetext"><fmt:message key="reports.xls"/>&nbsp;&nbsp;</td>				
					  		<td align="right" width="30px;"><s:checkbox key="reports.extract"  value="${ischecked6}" fieldValue="true"/></td>
					  		<td align="left" class="listwhitetext">EXTRACT</td>	
				  		</tr>		  		
					</table>
					</div> --%>
				</fieldset>
			</td>
			
		</tr>
		
		<tr >
       	  <td align="left" class="listwhitetext" colspan="9">
       	  <div id="controlDiv">
       	 	        <fieldset style="width:96.6%; !width:96.5%;">
			            <legend>Controls</legend>
			            
			            <table class="pickList11" style="margin:0px;">
			 <tr>
			 <td colspan="15" align="left">
			 <table style="margin:0px;">
			 <tr>		
			<td class="listwhitetext" align="right">Company&nbsp;Divisions</td>
			<td style="width:150px;"><s:textfield key="reports.companyDivision" cssClass="input-text" maxLength="100" cssStyle="width:210px;" size="38"/></td>
			<td><input type="button" style="width:158px;" value="Validate Company Division" class="cssbuttonA" name="validate_companyDivision" onclick="return checkCompanyDivisionValidation(this);"/></td>
			<td class="listwhitetext" align="right">Job<font color="red" size="2"></font></td>
			<td><s:textfield cssClass="input-text" name="reports.job" maxLength="100" cssStyle="width:210px;" size="38"/></td>
			<td colspan="3"><input type="button" class="cssbuttonA" style="width:120px;" name="validate_job"  value="Validate Job List" onclick="return checkJobValidation(this);"/></td></td>
			</tr>
			<tr>
			<td class="listwhitetext" align="right">Bill&nbsp;To&nbsp;Code</font></td>
			<td style="width:150px;"><s:textfield key="reports.billToCode" cssClass="input-text" cssStyle="width:210px;" size="38" /></td>
			<td><input type="button" style="width:158px;" value="Validate Bill To Codes" class="cssbuttonA" name="validate_billToCode" onclick="return checkBillToCodeValidation(this);"/></td>			 
			<td class="listwhitetext" align="right">Mode</td>
			<td><s:textfield cssClass="input-text" name="reports.mode" maxLength="100" cssStyle="width:210px;" size="38"/></td>
			<td colspan="3"><input type="button" class="cssbuttonA" style="width:120px;" name="validate_mode"  value="Validate Mode List" onclick="return checkModeValidation(this);"/></td></td>
			</tr>
			<tr>
			<td class="listwhitetext" align="left">&nbsp;&nbsp;Exclude&nbsp;Bill&nbsp;To&nbsp;Code</td>
			<td colspan="2"><s:checkbox cssStyle="vertical-align:middle;" name="reports.excludeFromForm" onchange="checkBillToCodeBlank()"/></td>
			<td width="40" class="listwhitetext" align="right">Routing</td>	
			<td><s:select cssClass="list-menu" name="reports.routing" list="%{routing}" cssStyle="width:113px" onchange="changeStatus();" tabindex="" /></td>
			</tr>
			<tr><td colspan="5" height="5px"></td></tr>				
			<tr>
       	  <td align="left" class="listwhitetext" colspan="9">
       	 	<fieldset style="width:95%;">
			 <!-- <legend>Conditions Update</legend> -->
			 <legend>Control Filter Value</legend>
			       <table class="table" id="dataforControllTable" style="width:99.4%;margin-bottom:2px;" >
				<tr>
				
				
				 <th class="listwhitetext" align="right" style="width:10px;background:#f9f9f9;" >#</th>
				 <th class="listwhitetext" align="right" style="background:#f9f9f9;"><b>Field&nbsp;Name</b></th>
				 <th class="listwhitetext" align="right" style="background:#f9f9f9;"><b>Value</b></th>
				 </tr>
				 <tbody>
		 		<c:set var="controlCountLine" value="${0}" />
		 		<c:forEach var="controlIndividualItem" items="${reportfieldSectionControlList}" >
		 		 <tr>
		 		 <c:set var="controlCountLine" value="${controlCountLine+1}" />
		 		 <td  class="listwhitetext" align="right" >${controlCountLine}
		 		 <s:hidden name="controlReportNumberList" value="${controlIndividualItem.id}" id="num${controlIndividualItem.id}" /></td>
		 		 <td  class="listwhitetext" align="right" >
		 		 <input type="text" value="${controlIndividualItem.fieldname}" class="input-text" style="width:70%"  name="controlFieldnameList" id="controlFieldname${controlIndividualItem.id}"  maxlength="50"  /></td>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${controlIndividualItem.fieldvalue}" class="input-text" style="width:40%"  name="controlFieldvalueList" id="controlFieldvalue${controlIndividualItem.id}"    maxlength="50"  /></td>
				 </tr>
				</c:forEach>
				</tbody>
			</table>  
			</fieldset>
			</td>
			</tr>
				<tr>
				<td>
				 <input type="button" class="cssbutton1" id="addLine" name="add" style="width:55px; height:25px;" value="Add" onclick="addRowControl('dataforControllTable');" />
				 </td>
			</tr>		 
			 </table>
			 </td>
			 </tr>
			               
			            </table>
			            
			        </fieldset>
			        </div>
			     </td>
			</tr>			
		
			<tr>
			<td align="left" class="listwhitetext" colspan="9">
       	 	<fieldset style="width:96.6%">
			 <legend>Condition Update</legend>
			<table class="table" id="dataforTable" style="width:95%;margin-bottom:2px;margin-left:15px;" >
				<tr>
				 <th class="listwhitetext" align="right" style="width:10px;background:#f9f9f9;" >#</th>
				 <th class="listwhitetext" align="right" style="background:#f9f9f9;"><b>Field&nbsp;Name</b></th>
				 <th class="listwhitetext" align="right" style="background:#f9f9f9;"><b>Value</b></th>
				 </tr>
				 <tbody>
		 		<c:set var="countLine" value="${0}" />
		 		<c:forEach var="individualItem" items="${reportfieldSectionList}" >
		 		 <tr>
		 		 <c:set var="countLine" value="${countLine+1}" />
		 		 <td  class="listwhitetext" align="right" >${countLine}
		 		 <s:hidden name="reportNumberList" value="${individualItem.id}" id="num${individualItem.id}" /></td>
		 		 <td  class="listwhitetext" align="right" >
		 		 <input type="text" value="${individualItem.fieldname}" class="input-text" style="width:70%" name="fieldnameList" id="fieldname${individualItem.id}"  maxlength="50"  /></td>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.fieldvalue}" class="input-text" style="width:40%" name="fieldvalueList" id="fieldvalue${individualItem.id}"    maxlength="50"  /></td>
				 </tr>
				</c:forEach>				
				</tbody>
			</table>
			<table style="margin:0px;">
			<tr>				
				 <td>
				 <input type="button" class="cssbutton1" id="addLine" name="add" style="width:55px;margin-left:13px;" value="Add" onclick="addRow('dataforTable');" />
				 </td>
				 </tr>	
				 <tr>
			<%-- <td valign="middle" class="listwhitetext" align="right" >Condition</td>
			<td colspan="5" valign="top" ><s:textarea  name="reports.formCondition" cssStyle="height:50px;width:400px;" readonly="false" cssClass="textarea"/></td> --%>
			<td valign="top" style="padding-top: 10px;padding-left:12px;" class="listwhitetext" colspan="5">Condition<br><s:textarea  name="reports.formCondition" cssStyle="height:50px;width:400px;" readonly="false" cssClass="textarea"/></td>
	
	        </tr>
			</table>
			
			</fieldset>
			</td>
			</tr>
			<tr>
			<td colspan="10">
			<fieldset style="width:97.2%;padding-right:5px;">
			
			<table>
			 <tr>
			                    <th class="pickLabel">
			                        <label class="required"><fmt:message key="user.availableRoles"/></label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required"><fmt:message key="user.roles"/></label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${availRoles}" scope="request"/>
			                <s:set name="rightList" value="reports.roleList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availRoles && !reportsRoles"/>
			                    <c:param name="rightId" value="reportsRoles"/>
			                </c:import>
			</table>
			</fieldset>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
<div class="bottom-header" style="margin-top:45px;"><span></span></div>
</div>
</div>
			  	<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:72px"><b><fmt:message key='reports.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:130px">
								<fmt:formatDate var="vanLineGLTypeCreatedOnFormattedValue" value="${reports.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="reports.createdOn" value="${vanLineGLTypeCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${reports.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='reports.createdBy' /></b></td>
							<c:if test="${not empty reports.id}">
								<s:hidden name="reports.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{reports.createdBy}"/></td>
							</c:if>
							<c:if test="${empty reports.id}">
								<s:hidden name="reports.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='reports.updatedOn'/></b></td>
							<fmt:formatDate var="vanLineGLTypeupdatedOnFormattedValue" value="${reports.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="reports.updatedOn" value="${vanLineGLTypeupdatedOnFormattedValue}"/>
							<td style="width:130px"><fmt:formatDate value="${reports.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='reports.updatedBy' /></b></td>
							
							<c:if test="${not empty reports.id}">
								<s:hidden name="reports.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{reports.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty reports.id}">
								<s:hidden name="reports.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
</div>
	 	<s:submit cssClass="cssbutton1" cssStyle="width:60px;" method="save" key="button.save" onclick="onFormSubmit(this.form);"/>  	
</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("reportForm")); 
  try{
    validateSecForm();
	}
	catch(e){}
	
</script>

<script type="text/javascript">
formTypes();
</script> 