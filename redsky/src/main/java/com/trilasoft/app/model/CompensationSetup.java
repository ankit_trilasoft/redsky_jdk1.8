package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "compensationsetup")
public class CompensationSetup extends BaseObject {
	private Long id;
	private String contract;
	private String corpid;
	private Long contractid;
	private Integer compensationyear;
	private BigDecimal  commissionpercentage = new BigDecimal("0.00");
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("contract", contract).append("corpid", corpid)
				.append("contractid", contractid)
				.append("compensationyear", compensationyear)
				.append("commissionpercentage", commissionpercentage)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CompensationSetup))
			return false;
		CompensationSetup castOther = (CompensationSetup) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(contract, castOther.contract)
				.append(corpid, castOther.corpid)
				.append(contractid, castOther.contractid)
				.append(compensationyear, castOther.compensationyear)
				.append(commissionpercentage, castOther.commissionpercentage)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(contract)
				.append(corpid).append(contractid).append(compensationyear)
				.append(commissionpercentage).append(createdBy)
				.append(updatedBy).append(createdOn)
				.append(updatedOn).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column
	public String getCorpid() {
		return corpid;
	}
	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}
	@Column
	public Long getContractid() {
		return contractid;
	}
	public void setContractid(Long contractid) {
		this.contractid = contractid;
	}
	@Column
	public Integer getCompensationyear() {
		return compensationyear;
	}
	public void setCompensationyear(Integer compensationyear) {
		this.compensationyear = compensationyear;
	}
	@Column
	public BigDecimal getCommissionpercentage() {
		return commissionpercentage;
	}
	public void setCommissionpercentage(BigDecimal commissionpercentage) {
		this.commissionpercentage = commissionpercentage;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
