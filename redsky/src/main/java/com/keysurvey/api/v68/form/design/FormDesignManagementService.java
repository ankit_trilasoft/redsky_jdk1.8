/**
 * FormDesignManagementService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public interface FormDesignManagementService extends java.rmi.Remote {
    public com.keysurvey.api.v68.form.design.WSQuestion[] getQuestions(long formId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void deleteQuestion(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSQuestion getQuestion(long questionId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public long addQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSAnswer[] getAnswers(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSAnswer getAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public long addAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSForm getForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void deleteAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSFormTree getFormTree(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public long createCompleteForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSFormDesign formDesign, com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy, com.keysurvey.api.v68.form.design.WSFormNotes formNotes, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public long createForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void updateForm(com.keysurvey.api.v68.form.design.WSForm form) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void deleteForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public long createCompleteQuestion(com.keysurvey.api.v68.form.design.WSQuestion question, com.keysurvey.api.v68.form.design.WSQuestionSettings questionSettings) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void updateQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void updateAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSSubmissionPolicy getSubmissionPolicy(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setSubmissionPolicy(com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSFormNotes getFormNotes(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setFormNotes(com.keysurvey.api.v68.form.design.WSFormNotes formNotes) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSFormDesign getFormDesign(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setFormDesign(com.keysurvey.api.v68.form.design.WSFormDesign formDesign) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSQuestionSettings getQuestionSettings(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setQuestionSettings(com.keysurvey.api.v68.form.design.WSQuestionSettings questionSetting) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void moveQuestion(long questionId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public int getQuestionPosition(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void moveAnswer(long answerId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public int getAnswerPosition(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSAnswer getAnswerByPosition(long questionId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSQuestion getQuestionByPosition(long formId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setFormLogic(com.keysurvey.api.v68.form.design.WSFormLogic formLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSFormLogic getFormLogic(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSQuestionLogic getQuestionLogic(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public void setQuestionLogic(long questionId, com.keysurvey.api.v68.form.design.WSQuestionLogic questionLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.design.WSForm[] getForms(long accountId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
}
