/**
 * 
 */
package com.trilasoft.app.model;

/**
 * @author GVerma
 *
 */
public class MoversDTO {
	public String address;
	public String location;
	public String city;
	public String state;
	public String country;
	public String zip;
	public String rooms;
	public String floors;
	public String comments;
	public String hasDamage;
	public String photos;
	public String inventory;
	public String inventoryDesc;
	public String ticket;
	public String oItemCondition;
	public String dItemCondition;
	public String oRooms;
	public String dRooms;
	public String oComments;
	public String dComments;
	public String oPhotos;
	public String dPhotos;
	public String oPacker;
	public String dPacker;
	public String delivered ; 
	public String quantity;
	public String cartonContent;
	public String packingCode;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getFloors() {
		return floors;
	}
	public void setFloors(String floors) {
		this.floors = floors;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getHasDamage() {
		return hasDamage;
	}
	public void setHasDamage(String hasDamage) {
		this.hasDamage = hasDamage;
	}
	public String getPhotos() {
		return photos;
	}
	public void setPhotos(String photos) {
		this.photos = photos;
	}
	public String getInventory() {
		return inventory;
	}
	public void setInventory(String inventory) {
		this.inventory = inventory;
	}
	public String getInventoryDesc() {
		return inventoryDesc;
	}
	public void setInventoryDesc(String inventoryDesc) {
		this.inventoryDesc = inventoryDesc;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getoItemCondition() {
		return oItemCondition;
	}
	public void setoItemCondition(String oItemCondition) {
		this.oItemCondition = oItemCondition;
	}
	public String getdItemCondition() {
		return dItemCondition;
	}
	public void setdItemCondition(String dItemCondition) {
		this.dItemCondition = dItemCondition;
	}
	public String getoRooms() {
		return oRooms;
	}
	public void setoRooms(String oRooms) {
		this.oRooms = oRooms;
	}
	public String getdRooms() {
		return dRooms;
	}
	public void setdRooms(String dRooms) {
		this.dRooms = dRooms;
	}
	public String getoComments() {
		return oComments;
	}
	public void setoComments(String oComments) {
		this.oComments = oComments;
	}
	public String getdComments() {
		return dComments;
	}
	public void setdComments(String dComments) {
		this.dComments = dComments;
	}
	public String getoPhotos() {
		return oPhotos;
	}
	public void setoPhotos(String oPhotos) {
		this.oPhotos = oPhotos;
	}
	public String getdPhotos() {
		return dPhotos;
	}
	public void setdPhotos(String dPhotos) {
		this.dPhotos = dPhotos;
	}
	public String getoPacker() {
		return oPacker;
	}
	public void setoPacker(String oPacker) {
		this.oPacker = oPacker;
	}
	public String getdPacker() {
		return dPacker;
	}
	public void setdPacker(String dPacker) {
		this.dPacker = dPacker;
	}
	public String getDelivered() {
		return delivered;
	}
	public void setDelivered(String delivered) {
		this.delivered = delivered;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getCartonContent() {
		return cartonContent;
	}
	public void setCartonContent(String cartonContent) {
		this.cartonContent = cartonContent;
	}
	public String getPackingCode() {
		return packingCode;
	}
	public void setPackingCode(String packingCode) {
		this.packingCode = packingCode;
	}
	

}
