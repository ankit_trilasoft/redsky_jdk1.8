<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title><fmt:message key="defaultAccountLineDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='defaultAccountLineDetail.heading'/>"/>
<style type="text/css">
#overlay11 { filter:alpha(opacity=70); -moz-opacity:0.7; 
-khtml-opacity: 0.7; opacity: 0.7; 
position:fixed; width:100%; height:100%;left:0px;top:0px; 
z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
</head>
<div id="Layer5" style="width:100%">
<div id="main" align="center">
                <h5>${showException}</h5>
</div>
<div id="newmnav">
  <ul>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>Copy Default Account Line <img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul></div><div class="spn">&nbsp;</div>
 </div>

<%-- <s:form id="defaultAccountLineCopyContract" name="defaultAccountLineCopyContract" action="copyDefaultAccountLine?decorator=popup&popup=true"  method="post"  validate="true"> --%>
<s:form id="defaultAccountLineCopyContract" name="defaultAccountLineCopyContract" action='${empty param.popup?"copyDefaultAccountLine.html":"copyDefaultAccountLine.html?decorator=popup&popup=true"}'  method="post"  validate="true">
<s:hidden name="checkFlag" value="${checkFlag}"/>
<s:hidden name="checkStatus" value="${checkStatus}"/>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%"> 

<table class="" cellspacing="0" cellpadding="0" border="0" style="width:650px;margin:0px;">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-20px; "><span></span></div>
   <div class="center-content">
    <fieldset >
   <legend>Copy Contract</legend>
				<table cellspacing="0" cellpadding="1" border="0">
					
						<tr>
						    <td width="10px"></td>
							<td align="left" class="listwhitetext">From&nbsp;Contract<font color="red" size="2">*</font></td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext">To&nbsp;Contract<font color="red" size="2">*</font></td>
							</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select id="fromContract" cssClass="list-menu" name="fromContract" list="%{contractListFromTemplate}" headerKey="" headerValue="" cssStyle="width:260px"  tabindex=""/></td>
							<td width="10px"></td>
<td align="left"><s:select cssClass="list-menu" id="toContract"   name="toContract" list="%{contractListFromContract}" headerKey="" headerValue=""   cssStyle="width:260px" tabindex=""/></td>
						</tr>
						</table>
						</fieldset>
						</div>
<div class="bottom-header"><span></span></div>
</div>
<s:set name="chargessTemplate" value="chargessTemplate" scope="request"/>

<div class="spnblk" align="center">&nbsp;
<c:if test="${hitFlag=='3'}">
<font color="red" style="text-align:center;width:100%;font:12px arial,verdana;"><b>
${showMessage}<br/>
</b>
</font>
</c:if>
</div>
<table  border="0" align="left"  cellpadding="1" cellspacing="1" style="margin:0px;width:100%;"  >	
	<tr><td><div id="bookingCreated" style="align:center" >
	<c:if test="${(empty chargessTemplate) || (hitFlag=='2' || hitFlag=='3' || hitFlag=='4')}">
	<s:submit cssClass="cssbuttonA"  key="Copy"  cssStyle="width:55px;" onclick="return checkforValidation();" />
	</c:if>
	<c:if test="${not empty chargessTemplate}">
	<c:if test="${hitFlag!='3'}">
	<s:submit cssClass="cssbuttonA"  key="Save" name="saveTemplate"  cssStyle="width:55px;" onclick="return checkforValidation();" />
	</c:if> 
	</c:if>
	<c:if test="${not empty chargessTemplate || not empty hitFlag}">
	<s:submit cssClass="cssbuttonA"  key="Reset" cssStyle="width:55px;" onclick="return ResetForm();" /> 
	</c:if>
     <input type="button" class="cssbutton1" value="Cancel" onclick="moveforParentPage();"/>
     </div></td>
      <c:if test="${not empty chargessTemplate}">
 	<c:if test="${hitFlag!='3'}"> 
	<td style="width:69px;text-align:left;vertical-align:bottom;"><input style="margin:0px;vertical-align:top;" type="checkbox" name="copyTemplateIdAll" id="copyTemplateIdALL"  checked="checked"  onclick="unCheckFunctionAll(this)"/>&nbsp;<b>ALL&nbsp;</b></td>
 	</c:if>
 	</c:if>   
     
     </tr>
	</table>
<c:if test="${not empty chargessTemplate}">
<display:table name="chargessTemplate" class="table" requestURI="" id="chargessTemplate" defaultsort="" >
<display:column    title="#" paramId="id" paramProperty="id" headerClass="containeralign" style="text-align:right;">
  	<c:out value='${chargessTemplate_rowNum}'/>
  </display:column>   
    <display:column property="jobType"  title="Job"  />
    <display:column property="serviceType"  title="Service"  />
    <display:column property="mode"  title="Mode" />  
    <display:column property="chargeCode"  title="Charge&nbsp;Code"  /> 
    <display:column title="Check" >
		<input type="checkbox" name="copyTemplateId" id="copyTemplateId${chargessTemplate.id}" value="${chargessTemplate.id}" checked="checked"  onclick="unCheckFunction(this)"/> 
	</display:column>  
</display:table> 
 </c:if>
 
 <div style="text-align:center;">
 <c:if test="${not empty misMatchTemplateList}">
 <c:if test="${hitFlag=='5'}">
<font color="red" style="text-align:center;width:100%;font:12px arial,verdana;"><b>
${showMessage}<br/><br/>
</b>
</font>
</c:if>
 </c:if>
  </div>
 <c:if test="${not empty misMatchTemplateList}">
 <display:table name="misMatchTemplateList" class="table" requestURI="" id="misMatchTemplateList" defaultsort="" >
 	<display:column    title="#" paramId="id" paramProperty="id" headerClass="containeralign" style="text-align:right;">
  	<c:out value='${misMatchTemplateList_rowNum}'/>
  </display:column>  
    <display:column property="jobType"  title="Job" />
    <display:column property="serviceType"  title="Service" />
    <display:column property="mode"  title="Mode" /> 
    <display:column property="chargeCode"  title="Charge&nbsp;Code" /> 
</display:table> 
</c:if>

</div>
	</tbody>
</table>
</div>
<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>
<script type="text/javascript">
<c:if test="${hitFlag=='1'}">
window.opener.document.forms['defaultAccountLineFormr'].elements['generateMessage'].value= "AddLineMessage"; 
document.cookie = 'generateMessage' + "=" + escape ('AddLineMessage');
parent.window.opener.progressBarAutoSave('0');
parent.window.opener.document.location.reload();
window.close();
</c:if>

<c:if test="${hitFlag=='2' || hitFlag=='4'}">
var showMessage='${showMessage}';
alert(showMessage);
</c:if>


function moveforParentPage(){ 
	parent.window.opener.progressBarAutoSave('0');
	window.close(); 
}

var form_submitted = false;

function submit_form()
{
	
	var checkButtonClick=0;
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    checkButtonClick++;
	 if(checkButtonClick>1){
			 document.forms['defaultAccountLineCopyContract'].elements['saveTemplate'].disabled = true;
	 }
	 progressBarAutoSave(0);
    return true;
  }
}

function checkforValidation(){
	var checkCheckBox=false;
	<c:if test="${(empty chargessTemplate) && (hitFlag=='2' || hitFlag=='3' || hitFlag=='4')}">
	document.forms['defaultAccountLineCopyContract'].elements['checkStatus'].value="";
	</c:if>
	
	if(document.forms['defaultAccountLineCopyContract'].elements['fromContract'].value==''){
		alert("Please select From Contract.");
		return false;
	}
	if(document.forms['defaultAccountLineCopyContract'].elements['toContract'].value=='')
	{
		alert("Please select To Contract.");
		return false;
	}
	if(document.forms['defaultAccountLineCopyContract'].elements['fromContract'].value==document.forms['defaultAccountLineCopyContract'].elements['toContract'].value)
	{
		alert("Can not copy in same Contract. Please select different Contracts.");
		return false;
	}
	else{
		progressBarAutoSave(1);
		if(document.forms['defaultAccountLineCopyContract'].elements['checkFlag'].value!="")
		{
			
			var elements =document.getElementsByName('copyTemplateId');
			for(var i=0; i < elements.length; i++)
			{
				if(elements[i].checked)
				{
					checkCheckBox=true;
				}
			}
			if(!checkCheckBox)
			{
				alert("Please select at least One Template Line.");
				return false;
			}
			document.forms['defaultAccountLineCopyContract'].elements['checkFlag'].value="";
			if(document.forms['defaultAccountLineCopyContract'].elements['fromContract'].value!=''){
				document.forms['defaultAccountLineCopyContract'].elements['fromContract'].disabled=false;
			}
			if(document.forms['defaultAccountLineCopyContract'].elements['toContract'].value!='')
			{
				document.forms['defaultAccountLineCopyContract'].elements['toContract'].disabled=false;
			}
		}
		submit_form();
	}
}

<c:if test="${not empty chargessTemplate}">
if(document.forms['defaultAccountLineCopyContract'].elements['fromContract'].value!=''){
	document.forms['defaultAccountLineCopyContract'].elements['fromContract'].disabled=true;
}
if(document.forms['defaultAccountLineCopyContract'].elements['toContract'].value!='')
{
	document.forms['defaultAccountLineCopyContract'].elements['toContract'].disabled=true;
}
</c:if>

function ResetForm(){
	progressBarAutoSave(1);
	document.forms['defaultAccountLineCopyContract'].elements['fromContract'].value="";
	document.forms['defaultAccountLineCopyContract'].elements['toContract'].value="";
	document.forms['defaultAccountLineCopyContract'].elements['checkFlag'].value='';
	document.forms['defaultAccountLineCopyContract'].elements['checkStatus'].value='';
	document.forms['defaultAccountLineCopyContract'].action= "copyContractFromDefaultAccountLineList.html?decorator=popup&popup=true";
	document.forms['defaultAccountLineCopyContract'].submit();
	progressBarAutoSave(0);
}


	function progressBarAutoSave(value) {
		 if (value==0) {
		       if (document.layers)
		           document.layers["overlay11"].visibility='hide';
		        else
		           document.getElementById("overlay11").style.visibility='hidden';
		   }
		   else if (value==1) {
		      if (document.layers)
		          document.layers["overlay11"].visibility='show';
		       else
		          document.getElementById("overlay11").style.visibility='visible';
		   }
	}
	
	function unCheckFunctionAll(source) {
		<c:if test="${not empty chargessTemplate}">
	   var checkboxes = document.getElementsByName('copyTemplateId');
	   for(var i=0, n=checkboxes.length;i<n;i++) {
	    	  checkboxes[i].checked = source.checked;
	    	}
	   </c:if>
	}
	
	function unCheckFunction(sourc) {
		<c:if test="${not empty chargessTemplate}">
		var flag=false;
		var checkboxes = document.getElementsByName('copyTemplateId');
		var checkboxesAll = document.getElementsByName('copyTemplateIdAll');
		if(sourc.checked==false){
			for(var i=0, n=checkboxesAll.length;i<n;i++) {
				checkboxesAll[i].checked = sourc.checked;
		    	}
		}else{
			flag=true;
			for(var j=0, m=checkboxes.length;j<m;j++) {
				if(checkboxes[j].checked==false){
					
					flag=false;
					//alert("ganesh111**"+flag)
				}/* else{
					flag=false;
					alert("ganesh111**"+flag)
				} */
		}
			if(flag==false){
				for(var k=0, o=checkboxesAll.length;k<o;k++) {
					checkboxesAll[k].checked = false;
			    	}
		}else{
			    		for(var l=0, p=checkboxesAll.length;l<p;l++) {
							checkboxesAll[l].checked = true;
			    		}
			    	}
		}
		
		
	   </c:if>
	}

</script>
<script type="text/javascript">
try{ 
	progressBarAutoSave(0);
}catch(e){}
</script>
