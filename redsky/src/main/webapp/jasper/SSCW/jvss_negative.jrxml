<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="jvss_negative"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="842"
		 columnWidth="552"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Booked_Date_From" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Booked_Date_To" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[SELECT
    "jvss_negative.jrxml",
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`status` AS serviceorder_status,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`firstName` AS serviceorder_firstName,
     customerfile.`bookingDate` AS customerfile_bookingDate,
     concat(serviceorder.`firstName`,' ',serviceorder.`lastName`) as name1,
     serviceorder.`estimatedGrossMargin` AS accountline_estimateRevenueAmount,
     serviceorder.`revisedGrossMargin` AS accountline_revisionRevenueAmount,
     serviceorder.`actualGrossMargin` AS accountline_actualRevenue
FROM  
     `customerfile` customerfile LEFT OUTER JOIN  `serviceorder` serviceorder ON customerfile.`id` = serviceorder.`customerFileId`
      INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id`
WHERE customerfile.`corpId`= $P{Corporate ID}
AND billing.`billtoname`='Cartus Moving Services'
AND customerfile.`bookingDate`>=$P{Booked_Date_From}
AND customerfile.`bookingDate`<=$P{Booked_Date_To}
AND (serviceorder.`estimatedGrossMargin`<0 or serviceorder.`revisedGrossMargin`<0 or serviceorder.`actualGrossMargin`<0)
GROUP BY serviceorder.`shipNumber`]]></queryString>

	<field name="jvss_negative.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="customerfile_bookingDate" class="java.sql.Timestamp"/>
	<field name="name1" class="java.lang.String"/>
	<field name="accountline_estimateRevenueAmount" class="java.math.BigDecimal"/>
	<field name="accountline_revisionRevenueAmount" class="java.math.BigDecimal"/>
	<field name="accountline_actualRevenue" class="java.math.BigDecimal"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="27"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="552"
						height="27"
						forecolor="#666666"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<text><![CDATA[Negative Gross Margin Report]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="21"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="2"
						width="93"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-1"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[SO#]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="93"
						y="2"
						width="102"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-3"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipper]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="195"
						y="2"
						width="33"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-6"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Mode]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="20"
						width="552"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="552"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						mode="Opaque"
						x="228"
						y="2"
						width="111"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-11"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Estimate Amount(US$)]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="339"
						y="2"
						width="111"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-12"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Revised  Amount(US$)]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="450"
						y="2"
						width="102"
						height="18"
						forecolor="#000000"
						backcolor="#999999"
						key="staticText-13"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<text><![CDATA[Actual Amount(US$)]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="93"
						height="18"
						backcolor="#CCCCCC"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="195"
						y="0"
						width="33"
						height="18"
						backcolor="#CCCCCC"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="93"
						y="0"
						width="102"
						height="18"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{name1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="228"
						y="0"
						width="111"
						height="18"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_estimateRevenueAmount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="339"
						y="0"
						width="111"
						height="18"
						backcolor="#CCCCCC"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_revisionRevenueAmount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="450"
						y="0"
						width="102"
						height="18"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_actualRevenue}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="2"
						width="194"
						height="16"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="411"
						y="2"
						width="77"
						height="16"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="488"
						y="2"
						width="64"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="552"
						height="0"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
