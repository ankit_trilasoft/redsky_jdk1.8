<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="customList.title"/></title> 
    <meta name="heading" content="<fmt:message key='customList.heading'/>"/> 
    
 <script language="JavaScript" type="text/javascript" >   
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'containers.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceForm1'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
  function goToUrl(id)
	{
		location.href = "containers.html?id="+id;
	}
	
var http5 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function confirmSubmit(targetElement){  
	var agree=confirm("Are you sure you want to remove this Custom?");
	var sid=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var did = targetElement;
	if (agree){
		 location.href="deleteCustom.html?id="+did+"&sid="+sid;
	}else{
		return false;
	}
}

</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
 
<c:set var="buttons">  
        <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editCustom.html?sid=${serviceOrder.id}"/>'"  
        value="<fmt:message key="button.add"/>"/> 
 </c:set> 
<s:form id="serviceForm1" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="shipNumber" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />

 <div id="Layer1" style="width:100%; ">
<div id="newmnav" style="float: left;margin-bottom:0px;">
  <ul>
  <li>
  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
  <a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a>
  </sec-auth:authComponent>
  </li>
  <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	   <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose> 
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab">
  <li id="newmnav1" style="background:#FFF "><a href="containers.html?id=${serviceOrder.id}" class="current" ><span>Forwarding</span></a></li>
   </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
     <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.statusTab">
    <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  </sec-auth:authComponent>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
   <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
   </sec-auth:authComponent>
   </configByCorp:fieldVisibility>
   <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
       <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
 </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
    <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
</sec-auth:authComponent>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;width: 60px; float: left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a>
		</td> 
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>		
		</c:if></tr></table>
<div class="spn">&nbsp;</div>

</div>
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 <div id="Layer3" style="width:100%; !width:100%;">
<div id="newmnav">   
 <ul>
 <sec-auth:authComponent componentId="module.tab.container.sSContainertTab">
  <li><a  href="containers.html?id=${serviceOrder.id}" ><span>SS Container</span></a></li>
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.tab.container.pieceCountTab">
  <li><a  href="cartons.html?id=${serviceOrder.id}" ><span>Piece Count</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.vehicleTab">
  <li><a  href="vehicles.html?id=${serviceOrder.id}" ><span>Vehicle</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.routingTab">
  <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Routing</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
  <li><a href="editConsignee.html?sid=${serviceOrder.id}"><span>Consignee Instructions</span></a></li>
 </sec-auth:authComponent> 
  <li id="newmnav1" style="background:#FFF "><a href="customs.html?id=${serviceOrder.id}" class="current" ><span>Customs</span></a></li>
 <sec-auth:authComponent componentId="module.tab.container.auditTab">
  <li>
    <a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=custom&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
    <span>Audit</span></a></li>
    </sec-auth:authComponent>
  </ul>
  
</div>

<div class="spn">&nbsp;</div>
 </div>
<s:set name="customs" value="customs" scope="request"/> 
<display:table name="customs" class="table" requestURI="" id="customList" export="true" pagesize="25"> 
     <sec-auth:authComponent componentId="module.tab.custom.customTab">
     <display:column property="id" sortable="true" title="ID#" href="editCustom.html?sid=${serviceOrder.id}" paramId="id" paramProperty="id" style="width:8px" />
     </sec-auth:authComponent>
     <display:column property="ticket" sortable="true" title="Ticket#" style="width:50px" />
     <c:if test="${customList.movement=='Out'}">
     <c:if test="${not empty customList.transactionId}"> 
     <display:column sortable="true" titleKey="custom.movement"  style="width:80px">
     <c:out value="${customList.movement}"/>
     <a href="editCustom.html?sid=${serviceOrder.id}&id=${customList.transactionId}"> (#<c:out value="${customList.transactionId}"></c:out>)</a>
     </display:column> 
     </c:if>
     <c:if test="${empty customList.transactionId}">
     <display:column sortable="true" titleKey="custom.movement" style="width:80px"><c:out value="${customList.movement}" /></display:column>
     </c:if>
     </c:if>
     <c:if test="${customList.movement =='In'}">
     <display:column sortable="true" titleKey="custom.movement" style="width:50px"><c:out value="${customList.movement}" /></display:column> 
     </c:if>
     <display:column property="entryDate" sortable="true" titleKey="custom.entryDate" format="{0,date,dd-MMM-yyyy}" style="width:50px"  /> 
     <display:column property="documentType" sortable="true" title="Doc Type" style="width:50px" /> 
     <display:column property="documentRef" sortable="true" titleKey="custom.documentRef" style="width:80px" /> 
     <display:column property="pieces" sortable="true" titleKey="custom.pieces" style="width:20px" /> 
     <display:column property="goods" sortable="true" titleKey="custom.goods" style="width:150px" /> 
     <display:column property="volume" sortable="true" titleKey="custom.volume" style="width:50px" /> 
     <display:column property="weight" sortable="true" titleKey="custom.weight" style="width:50px" /> 
     <display:column property="status" sortable="true" title="Status" style="width:50px" /> 
     <sec-auth:authComponent componentId="module.tab.custom.customTab">
     <display:column title="Remove" style="text-align:center; width:20px;">
	   <a><img align="middle" onclick="confirmSubmit(${customList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	 </display:column> 
	 </sec-auth:authComponent>   
    <display:setProperty name="paging.banner.item_name" value="custom"/> 
    <display:setProperty name="paging.banner.items_name" value="people"/> 
 
    <display:setProperty name="export.excel.filename" value="Custom List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Custom List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Custom List.pdf"/> 
</display:table> 

<s:hidden name="serviceOrderId" value="%{serviceOrder.id}" />
</s:form>
<sec-auth:authComponent componentId="module.tab.custom.customTab">
<c:out value="${buttons}" escapeXml="false" /> 
</sec-auth:authComponent> 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/customs.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>   