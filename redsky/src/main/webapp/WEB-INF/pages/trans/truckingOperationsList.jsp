<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="truckingOperationsList.title"/></title>   
    <meta name="heading" content="<fmt:message key='truckingOperationsList.heading'/>"/>   
    
	<style>
	
	span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-10px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
font-size:.85em;
}
	</style>
<script type="text/javascript">
function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	     location.href = "updateTruckingOperations.html?id="+encodeURI(targetElement)+"&ticket=${ticket}&sid=${sid}&tid=${tid}";
     }else{
		return false;
	}
}
function sendDataMoveCloud(){
	 var url="sendMoveToCloudTruck.html?decorator=simple&popup=true&cid=${serviceOrder.customerFileId}&tid=${workTicket.id}";
	    httpMoveCloud.open("GET", url, true);
	    showOrHide(1);
	    httpMoveCloud.onreadystatechange = handleHttpResponseForMoveCloud;
	    httpMoveCloud.send(null);
}
function handleHttpResponseForMoveCloud()
{
      if (httpMoveCloud.readyState == 4)
       { 
            var results = httpMoveCloud.responseText
            results = results.trim();
          if(results=='Y'){
          alert("Request is scheduled for MoveCloud.");
      	   //document.forms['searchForm'].action="truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}";
      	   //document.forms['searchForm'].submit();    
      	  location.href ="truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}";
          }
          showOrHide(0);
       }
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var httpMoveCloud = getHTTPObject();
    
    function showOrHide(value) {
        if (value==0) {
           	if (document.layers)
               document.layers["overlay"].visibility='hide';
            else
               document.getElementById("overlay").style.visibility='hidden';
       	}else if (value==1) {
       		if (document.layers)
              document.layers["overlay"].visibility='show';
           	else
              document.getElementById("overlay").style.visibility='visible';
       	} }  
</script>
</head>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<div id="newmnav">
		  
		  
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
			  <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  <c:if test="${forwardingTabVal!='Y'}"> 
	   			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	          <c:if test="${serviceOrder.job =='INT'}">
	            <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	          </c:if>
	          </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			 <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  </sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 5px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
<!--<div id="newmnav">
		  <ul>
		    <li><a href="editWorkTicketUpdate.html?id=${tid}"><span>Work Ticket</span></a></li>
		     <li id="newmnav1" style="background:#FFF "><a class="current"><span>Truck<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
  <div class="spn">&nbsp;</div>
		</div>
		
--><s:form id="searchForm" action="searchTrucks" method="post" validate="true"> 
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;margin-left:1.5%;" onclick="location.href='<c:url value="/editTruckOperation.html?ticket=${ticket}&sid=${sid}&tid=${tid}"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set> 

<c:set var="ticket" value="<%= request.getParameter("ticket")%>" />
<s:hidden name="ticket" value="<%= request.getParameter("ticket")%>" />
 <c:set var="sid" value="<%=request.getParameter("sid") %>" />
 <s:hidden name="sid" value="${sid}" />
 <c:set var="tid" value="<%=request.getParameter("tid") %>" />
 <s:hidden name="tid" value="${tid}" /> 
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="emailSetupValue" />
<s:hidden name="moveCount" />
  
<s:set name="truckingOperationsList" value="truckingOperationsList" scope="request"/>
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;">
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table>

<!--<div id="otabs" style="margin-top:7px">
	<ul>
		<li><a class="current"><span>Trucking Operations List</span></a></li>
	</ul>
</div>
-->
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;">		
<div id="newmnav">
		  <ul>
		    <li><a href="editWorkTicketUpdate.html?id=${tid}"><span>Work Ticket</span></a></li>
		     <sec-auth:authComponent componentId="module.tab.workTicket.materialEquipmentTab">
			  			<li><a href="itemsJbkEquipExtraInfo.html?wid=${tid}&sid=${serviceOrder.id}&itemType=E"><span>Equipment&nbsp;And&nbsp;Material</span></a></li>
					</sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.workTicket.materialTab">
		    <li><a href="itemsJbkEquips.html?id=${tid}&itemType=M&sid=${sid}"><span>Material</span></a></li>
		    </sec-auth:authComponent>
		    <!--
		    <li><a href="itemsJbkResourceList.html?id=${tid}&sid=${sid}"><span>Resource</span></a></li>
			-->
			<sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab">
			<li><a href="itemsJbkResourceList.html?id=${tid}&itemType=E&sid=${sid}"><span>Resource</span></a></li>
			</sec-auth:authComponent>
			<li><a href="workTicketCrews.html?id=${tid}&sid=${sid}"><span>Crew</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current""><span>Trucks<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
		    <li><a href="bookStorages.html?id=${tid} "><span>Whse/Mgmt</span></a></li>
		    </configByCorp:fieldVisibility>
		    <sec-auth:authComponent componentId="module.tab.workTicket.formsTab">
			<li><a onclick="window.open('subModuleReports.html?id=${tid}&jobNumber=${serviceOrder.shipNumber}&noteID=${ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			</sec-auth:authComponent>
  		  </ul><div class="spn">&nbsp;</div>
</div>
</td></tr></table>

<display:table name="truckingOperationsList" class="table" requestURI="" id="truckingOperationsList" export="false" pagesize="25">   
    <display:column sortable="true" titleKey="truckingOperations.localTruckNumber" sortProperty="localTruckNumber"><a href="editTruckOperation.html?ticket=${ticket}&sid=${sid}&tid=${tid}&id=${truckingOperationsList.id}"><c:out value="${truckingOperationsList.localTruckNumber }" /></a>
    </display:column>   
    <display:column property="description" sortable="true" title="Description"/>
    <display:column title="Remove" style="width:45px;">
				    <a><img align="middle" onclick="confirmSubmit(${truckingOperationsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
</display:table>   
  </tr>
  </tbody>
  </table>
  </div>
  <c:out value="${buttons}" escapeXml="false" />
                           <c:if test="${surveyTool=='MoveCloud' && (workTicket.service=='DL' || workTicket.service=='PK')}">
  							<c:if test="${moveCount > 0}">										
							<td align="left" class="listwhitetext" width="50"><img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;margin-left:17px; margin-top:1px;" title="Move Cloud" src="<c:url value='/images/move4u.png'/>"  />
							</td>
							<td width="20">
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img  src="<c:url value='/images/ques-small.gif'/>" title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img  src="<c:url value='/images/cancel001.gif'/>" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  src="<c:url value='/images/tick01.gif'/>" title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
							<td><input type="button" value="Send To MoveCloud" class="cssbuttonB" style="height:25px" onclick="sendDataMoveCloud()" /></td>
							</c:if>
</s:form>  
<script type="text/javascript">   
  try{
    <c:if test="${hitFlag == 1}" >
			<c:redirect url="/truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}"  />
	</c:if>
	}
	catch(e){}
</script>  
