<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<head>   
    <title>Page Action Form</title>   
    <meta name="heading" content="Page Action Form"/> 

<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}
#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<script language="javascript" type="text/javascript"> 
</script>
</head>
<s:form id="form2" action="savePageAction.html?menuId=${menuId}&decorator=popup&popup=true;document.getElementById('loader').style.display = 'block'" method="post">

<s:hidden name="pageId"/>
	<div id="newmnav">
	  <ul>
		<li><a href="pageUrlList.html?menuUrl=${menuUrl}&menuId=${menuId}&menuName=${menuName}&parentN=${parentN}&corp=${corp}&decorator=popup&popup=true" ><span>Page Action List</span></a></li>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Page Action</span></a></li>					  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
	<div id="content" align="center">
	<div id="liquid-round">
    <div class="top" style="!margin-top:2px;"><span></span></div>
    <div class="center-content">
    
<table class="" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
<tr>
<td align="right" class="listwhitetext" >URL</td>
<c:if test="${pageId == null || pageId == ''}" >
<td><s:textfield cssClass="input-text" name="pageUrl" maxlength="50" size="30" /></td>
</c:if>
<c:if test="${pageId != null && pageId != ''}" >
<td><s:textfield cssClass="input-textUpper" name="pageUrl"  size="30" readonly="true"/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.parentName"/></td>
<td align="left"><s:textfield cssClass="input-textUpper" name="parentN" size="20" readonly="true"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.name"/></td>
<td align="left"><s:textfield cssClass="input-textUpper" name="menuName" size="40" readonly="true"/></td>
<td align="right" class="listwhitetext">Menu URL</td>
<td align="left"><s:textfield cssClass="input-textUpper" name="menuUrl" size="40" readonly="true"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.corpID"/></td>
<td align="left"><s:textfield cssClass="input-textUpper" name="corp" size="20" readonly="true"/></td>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.description"/></td>
<td align="left"><s:textfield cssClass="input-text" name="pageDesc" size="40" maxlength="50" required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext">Page Action Module</td>
<c:if test="${pageId == null || pageId == ''}" >
<td align="left"><s:textfield cssClass="input-text" name="pageModule" size="20" maxlength="50" required="true"/></td>
</c:if>
<c:if test="${pageId != null && pageId != ''}" >
<td align="left"><s:textfield cssClass="input-textUpper" name="pageModule" size="20" readonly="true"/></td>
</c:if>
<td align="right" class="listwhitetext">Page Action Method</td>
<c:if test="${pageId == null || pageId == ''}" >
<td align="left"><s:textfield cssClass="input-text" name="pageMethod" size="40" maxlength="50" required="true" /></td>
</c:if>
<c:if test="${pageId != null && pageId != ''}" >
<td align="left"><s:textfield cssClass="input-textUpper" name="pageMethod" size="40" readonly="true"/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext">Page Action Class</td>
<c:if test="${pageId == null || pageId == ''}" >
<td align="left" colspan="3"><s:textfield cssClass="input-text" name="pageClass" size="40" maxlength="100" required="true"/></td>
</c:if>
<c:if test="${pageId != null && pageId != ''}" >
<td align="left"><s:textfield cssClass="input-textUpper" name="pageClass" size="40" readonly="true"/></td>
</c:if>
</tr>
</tbody>
</table>    
    
	</div><div class="bottom-header"><span></span></div></div></div>
			
			
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" key="button.save" theme="simple" onclick=""/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
<div id="loader" style="text-align:center; display:none">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>        	
</s:form>
