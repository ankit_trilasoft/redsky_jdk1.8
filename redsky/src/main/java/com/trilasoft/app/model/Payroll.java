//---Created By Bibhash

package com.trilasoft.app.model;

import java.util.Date;

import org.appfuse.model.BaseObject;   

import javax.persistence.Entity;   
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;   
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="crew")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class Payroll extends BaseObject {
	
	private Long id;  
	private String corpID;
    public String firstName;   
    public String lastName;
    public String initial;
    public String userName;
   
    public String title;   
    
    public String officePhone;
    public String officeFax;
    public String rank;
    public String warehouse;
    public String branch;
    public String info;
    public String department;
    public String typeofWork;
    public String employeeId;
    public String accntingCrossRef;
    public String bucket;
    public String vlCode;
    public String socialSecurityNumber;
    public String payhour;
    public String overtimetype;
    public String cdl;
 
    public String drivingClass;
  
    public String greenCard;
 
    public String late;
    public Date hired;
    public Date unionName;
    public Date terminatedDate;
    public Date begginingPension;
    public Date expiration;

    public Date classificationEffective;
    public Date cdlExpires;
    public Date suspended;
    public Date doTPhysical;
    public Date licenceReview;
    public boolean usCitizen;
    public String active;
    private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	
	private Double sickHours;
	private Double sickUsed;
	private Double vacationHrs;
	private Double vacationUsed;	
	
	private String companyDivision;
	
	private Date beginHealthWelf;
	private Double personalDayUsed;
	
	private Double carryOverAllowed;
	private Double tenure;
	private Date lastAnniversaryDate;
	private Double sickCarriedOver;
	private Double unscheduledLeave;
	private Boolean ignoreForTimeSheet;
	private Integer paidSickLeave;
	private String partnerCode;
	private String partnerName;
	private String licenceNumber;
	private String integrationTool;
	private String deviceNumber;
	private String supervisorName;
	public boolean contractor;
	private String crewEmail;
	private String integrationPassword;
	private String crewGroup;
	private int totalManPower=0;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("firstName", firstName)
				.append("lastName", lastName).append("initial", initial)
				.append("userName", userName).append("title", title)
				.append("officePhone", officePhone)
				.append("officeFax", officeFax).append("rank", rank)
				.append("warehouse", warehouse).append("branch", branch)
				.append("info", info).append("department", department)
				.append("typeofWork", typeofWork)
				.append("employeeId", employeeId)
				.append("accntingCrossRef", accntingCrossRef)
				.append("bucket", bucket).append("vlCode", vlCode)
				.append("socialSecurityNumber", socialSecurityNumber)
				.append("payhour", payhour)
				.append("overtimetype", overtimetype).append("cdl", cdl)
				.append("drivingClass", drivingClass)
				.append("greenCard", greenCard).append("late", late)
				.append("hired", hired).append("unionName", unionName)
				.append("terminatedDate", terminatedDate)
				.append("begginingPension", begginingPension)
				.append("expiration", expiration)
				.append("classificationEffective", classificationEffective)
				.append("cdlExpires", cdlExpires)
				.append("suspended", suspended)
				.append("doTPhysical", doTPhysical)
				.append("licenceReview", licenceReview)
				.append("usCitizen", usCitizen).append("active", active)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("valueDate", valueDate).append("sickHours", sickHours)
				.append("sickUsed", sickUsed)
				.append("vacationHrs", vacationHrs)
				.append("vacationUsed", vacationUsed)
				.append("companyDivision", companyDivision)
				.append("beginHealthWelf", beginHealthWelf)
				.append("personalDayUsed", personalDayUsed)
				.append("carryOverAllowed", carryOverAllowed)
				.append("tenure", tenure)
				.append("lastAnniversaryDate", lastAnniversaryDate)
				.append("sickCarriedOver", sickCarriedOver)
				.append("unscheduledLeave", unscheduledLeave)
				.append("ignoreForTimeSheet", ignoreForTimeSheet)
				.append("paidSickLeave", paidSickLeave)
				.append("partnerCode", partnerCode)
				.append("partnerName", partnerName)
				.append("licenceNumber", licenceNumber)
				.append("integrationTool", integrationTool)
				.append("deviceNumber", deviceNumber)
				.append("supervisorName", supervisorName).append(contractor)
				.append("crewEmail",crewEmail)
				.append("integrationPassword",integrationPassword)
				.append("crewGroup",crewGroup)
				.append("totalManPower",totalManPower).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Payroll))
			return false;
		Payroll castOther = (Payroll) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(initial, castOther.initial)
				.append(userName, castOther.userName)
				.append(title, castOther.title)
				.append(officePhone, castOther.officePhone)
				.append(officeFax, castOther.officeFax)
				.append(rank, castOther.rank)
				.append(warehouse, castOther.warehouse)
				.append(branch, castOther.branch)
				.append(info, castOther.info)
				.append(department, castOther.department)
				.append(typeofWork, castOther.typeofWork)
				.append(employeeId, castOther.employeeId)
				.append(accntingCrossRef, castOther.accntingCrossRef)
				.append(bucket, castOther.bucket)
				.append(vlCode, castOther.vlCode)
				.append(socialSecurityNumber, castOther.socialSecurityNumber)
				.append(payhour, castOther.payhour)
				.append(overtimetype, castOther.overtimetype)
				.append(cdl, castOther.cdl)
				.append(drivingClass, castOther.drivingClass)
				.append(greenCard, castOther.greenCard)
				.append(late, castOther.late)
				.append(hired, castOther.hired)
				.append(unionName, castOther.unionName)
				.append(terminatedDate, castOther.terminatedDate)
				.append(begginingPension, castOther.begginingPension)
				.append(expiration, castOther.expiration)
				.append(classificationEffective,
						castOther.classificationEffective)
				.append(cdlExpires, castOther.cdlExpires)
				.append(suspended, castOther.suspended)
				.append(doTPhysical, castOther.doTPhysical)
				.append(licenceReview, castOther.licenceReview)
				.append(usCitizen, castOther.usCitizen)
				.append(active, castOther.active)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(valueDate, castOther.valueDate)
				.append(sickHours, castOther.sickHours)
				.append(sickUsed, castOther.sickUsed)
				.append(vacationHrs, castOther.vacationHrs)
				.append(vacationUsed, castOther.vacationUsed)
				.append(companyDivision, castOther.companyDivision)
				.append(beginHealthWelf, castOther.beginHealthWelf)
				.append(personalDayUsed, castOther.personalDayUsed)
				.append(carryOverAllowed, castOther.carryOverAllowed)
				.append(tenure, castOther.tenure)
				.append(lastAnniversaryDate, castOther.lastAnniversaryDate)
				.append(sickCarriedOver, castOther.sickCarriedOver)
				.append(unscheduledLeave, castOther.unscheduledLeave)
				.append(ignoreForTimeSheet, castOther.ignoreForTimeSheet)
				.append(paidSickLeave, castOther.paidSickLeave)
				.append(partnerCode, castOther.partnerCode)
				.append(partnerName, castOther.partnerName)
				.append(licenceNumber, castOther.licenceNumber)
				.append(integrationTool, castOther.integrationTool)
				.append(deviceNumber, castOther.deviceNumber)
				.append(supervisorName, castOther.supervisorName)
				.append(contractor, castOther.contractor)
				.append(crewEmail, castOther.crewEmail)
				.append(integrationPassword, castOther.integrationPassword)
				.append(crewGroup,castOther.crewGroup)
				.append(totalManPower,castOther.totalManPower)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(firstName).append(lastName).append(initial)
				.append(userName).append(title).append(officePhone)
				.append(officeFax).append(rank).append(warehouse)
				.append(branch).append(info).append(department)
				.append(typeofWork).append(employeeId).append(accntingCrossRef)
				.append(bucket).append(vlCode).append(socialSecurityNumber)
				.append(payhour).append(overtimetype).append(cdl)
				.append(drivingClass).append(greenCard).append(late)
				.append(hired).append(unionName).append(terminatedDate)
				.append(begginingPension).append(expiration)
				.append(classificationEffective).append(cdlExpires)
				.append(suspended).append(doTPhysical).append(licenceReview)
				.append(usCitizen).append(active).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(valueDate).append(sickHours).append(sickUsed)
				.append(vacationHrs).append(vacationUsed)
				.append(companyDivision).append(beginHealthWelf)
				.append(personalDayUsed).append(carryOverAllowed)
				.append(tenure).append(lastAnniversaryDate)
				.append(sickCarriedOver).append(unscheduledLeave)
				.append(ignoreForTimeSheet).append(paidSickLeave)
				.append(partnerCode).append(partnerName).append(licenceNumber)
				.append(integrationTool).append(deviceNumber).append(supervisorName).append(contractor)
				.append(crewEmail)
				.append(integrationPassword)
			    .append(crewGroup)
				.append(totalManPower).toHashCode();
	}
	@Column(length=25)
	public String getAccntingCrossRef() {
		return accntingCrossRef;
	}
	public void setAccntingCrossRef(String accntingCrossRef) {
		this.accntingCrossRef = accntingCrossRef;
	}
	@Column
	public Date getBegginingPension() {
		return begginingPension;
	}
	public void setBegginingPension(Date begginingPension) {
		this.begginingPension = begginingPension;
	}
	@Column(length=25)
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	@Column(length=25)
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	@Column(length=25)
	public String getCdl() {
		return cdl;
	}
	public void setCdl(String cdl) {
		this.cdl = cdl;
	}
	@Column
	public Date getCdlExpires() {
		return cdlExpires;
	}
	public void setCdlExpires(Date cdlExpires) {
		this.cdlExpires = cdlExpires;
	}
	@Column
	public Date getClassificationEffective() {
		return classificationEffective;
	}
	public void setClassificationEffective(Date classificationEffective) {
		this.classificationEffective = classificationEffective;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=50)
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	@Column
	public Date getDoTPhysical() {
		return doTPhysical;
	}
	public void setDoTPhysical(Date doTPhysical) {
		this.doTPhysical = doTPhysical;
	}
	@Column(length=40)
	public String getDrivingClass() {
		return drivingClass;
	}
	public void setDrivingClass(String drivingClass) {
		this.drivingClass = drivingClass;
	}
	@Column(length=25)
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	@Column
	public Date getExpiration() {
		return expiration;
	}
	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
	@Column(length=25)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(length=25)
	public String getGreenCard() {
		return greenCard;
	}
	public void setGreenCard(String greenCard) {
		this.greenCard = greenCard;
	}
	@Column
	public Date getHired() {
		return hired;
	}
	public void setHired(Date hired) {
		this.hired = hired;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=100)
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	@Column(length=10)
	public String getInitial() {
		return initial;
	}
	public void setInitial(String initial) {
		this.initial = initial;
	}
	@Column(length=25)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(length=25)
	public String getLate() {
		return late;
	}
	public void setLate(String late) {
		this.late = late;
	}
	@Column
	public Date getLicenceReview() {
		return licenceReview;
	}
	public void setLicenceReview(Date licenceReview) {
		this.licenceReview = licenceReview;
	}
	@Column(length=25)
	public String getOfficeFax() {
		return officeFax;
	}
	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}
	@Column(length=50)
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	@Column(length=50)
	public String getOvertimetype() {
		return overtimetype;
	}
	public void setOvertimetype(String overtimetype) {
		this.overtimetype = overtimetype;
	}
	@Column(length=10)
	public String getPayhour() {
		return payhour;
	}
	public void setPayhour(String payhour) {
		this.payhour = payhour;
	}
	@Column(length=5)
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	@Column(length=25)
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}
	@Column
	public Date getSuspended() {
		return suspended;
	}
	public void setSuspended(Date suspended) {
		this.suspended = suspended;
	}
	@Column(length=50)
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(length=50)
	public String getTypeofWork() {
		return typeofWork;
	}
	public void setTypeofWork(String typeofWork) {
		this.typeofWork = typeofWork;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public boolean isUsCitizen() {
		return usCitizen;
	}
	public void setUsCitizen(boolean usCitizen) {
		this.usCitizen = usCitizen;
	}
	@Column
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=100)
	public String getVlCode() {
		return vlCode;
	}
	public void setVlCode(String vlCode) {
		this.vlCode = vlCode;
	}
	@Column(length=50)
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column
	public Date getUnionName() {
		return unionName;
	}
	public void setUnionName(Date unionName) {
		this.unionName = unionName;
	}
	@Column
	public Date getTerminatedDate() {
		return terminatedDate;
	}
	public void setTerminatedDate(Date terminatedDate) {
		this.terminatedDate = terminatedDate;
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=25)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(length=5)
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@Column(length=5)
	public Double getSickHours() {
		return sickHours;
	}
	public void setSickHours(Double sickHours) {
		this.sickHours = sickHours;
	}
	@Column(length=5)
	public Double getSickUsed() {
		return sickUsed;
	}
	public void setSickUsed(Double sickUsed) {
		this.sickUsed = sickUsed;
	}
	@Column(length=5)
	public Double getVacationHrs() {
		return vacationHrs;
	}
	public void setVacationHrs(Double vacationHrs) {
		this.vacationHrs = vacationHrs;
	}
	@Column(length=5)
	public Double getVacationUsed() {
		return vacationUsed;
	}
	public void setVacationUsed(Double vacationUsed) {
		this.vacationUsed = vacationUsed;
	}
	@Column(length=25)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	@Column
	public Date getBeginHealthWelf() {
		return beginHealthWelf;
	}
	public void setBeginHealthWelf(Date beginHealthWelf) {
		this.beginHealthWelf = beginHealthWelf;
	}
	@Column(length=5)
	public Double getPersonalDayUsed() {
		return personalDayUsed;
	}
	public void setPersonalDayUsed(Double personalDayUsed) {
		this.personalDayUsed = personalDayUsed;
	}
	@Column(length=5)
	public Double getCarryOverAllowed() {
		return carryOverAllowed;
	}
	public void setCarryOverAllowed(Double carryOverAllowed) {
		this.carryOverAllowed = carryOverAllowed;
	}
	@Column(length=8)
	public Double getTenure() {
		return tenure;
	}
	public void setTenure(Double tenure) {
		this.tenure = tenure;
	}
	@Column
	public Date getLastAnniversaryDate() {
		return lastAnniversaryDate;
	}
	public void setLastAnniversaryDate(Date lastAnniversaryDate) {
		this.lastAnniversaryDate = lastAnniversaryDate;
	}
	@Column
	public Double getSickCarriedOver() {
		return sickCarriedOver;
	}
	public void setSickCarriedOver(Double sickCarriedOver) {
		this.sickCarriedOver = sickCarriedOver;
	}
	/**
	 * @return the unscheduledLeave
	 */
	@Column(length=5)
	public Double getUnscheduledLeave() {
		return unscheduledLeave;
	}
	/**
	 * @param unscheduledLeave the unscheduledLeave to set
	 */
	public void setUnscheduledLeave(Double unscheduledLeave) {
		this.unscheduledLeave = unscheduledLeave;
	}
	public Boolean getIgnoreForTimeSheet() {
		return ignoreForTimeSheet;
	}
	public void setIgnoreForTimeSheet(Boolean ignoreForTimeSheet) {
		this.ignoreForTimeSheet = ignoreForTimeSheet;
	}
	@Column(length=5)
	public Integer getPaidSickLeave() {
		return paidSickLeave;
	}
	public void setPaidSickLeave(Integer paidSickLeave) {
		this.paidSickLeave = paidSickLeave;
	}
	@Column(length=8)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column(length=255)
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getLicenceNumber() {
		return licenceNumber;
	}
	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	@Column
	public String getIntegrationTool() {
		return integrationTool;
	}
	public void setIntegrationTool(String integrationTool) {
		this.integrationTool = integrationTool;
	}
	@Column
	public String getDeviceNumber() {
		return deviceNumber;
	}
	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}
	public String getSupervisorName() {
		return supervisorName;
	}
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}
	
	@Column
	public boolean isContractor() {
		return contractor;
	}
	public void setContractor(boolean contractor) {
		this.contractor = contractor;
	}
	@Column
	public String getCrewEmail() {
		return crewEmail;
	}
	public void setCrewEmail(String crewEmail) {
		this.crewEmail = crewEmail;
	}
	@Column
	public String getIntegrationPassword() {
		return integrationPassword;
	}
	public void setIntegrationPassword(String integrationPassword) {
		this.integrationPassword = integrationPassword;
	}
	@Column
	public String getCrewGroup() {
		return crewGroup;
	}
	public void setCrewGroup(String crewGroup) {
		this.crewGroup = crewGroup;
	}
	@Column
	public int getTotalManPower() {
		return totalManPower;
	}
	public void setTotalManPower(int totalManPower) {
		this.totalManPower = totalManPower;
	}
	
}