package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CrewCapacityDao;
import com.trilasoft.app.model.CrewCapacity;


public class CrewCapacityDaoHibernate extends GenericDaoHibernate<CrewCapacity, Long>implements CrewCapacityDao{

	public CrewCapacityDaoHibernate() {
		super(CrewCapacity.class);
	}
	public List<CrewCapacity> findRecordByHub(String crewGroupval, String sessionCorpID){		
		return getHibernateTemplate().find("from CrewCapacity where crewgroup='"+crewGroupval+"'");
	}
	public List findAllCrewGroup(String corpId){
		List al=getSession().createSQLQuery("select distinct crewgroup from crewcapacity where corpId ='"+corpId+"' order by crewgroup").list();
		return al;
	}
	 public List findOldGroupName(String sessionCorpID, String gName){
		 String query="select crewgroup from crewcapacity where crewgroup='"+gName+"' AND corpID='" +sessionCorpID+ "'";
			List check = this.getSession().createSQLQuery(query).list(); 
			return check;
		}
	 public Map<String,String> findExistingJobName(String sessionCorpID){
		 Map<String,String> crewMap= new LinkedHashMap<String, String>();
		 String query="select crewgroup,jobtype from crewcapacity where corpID='" +sessionCorpID+ "'";
			List check = this.getSession().createSQLQuery(query).list(); 
			Iterator itr=check.iterator();
			while(itr.hasNext()){
				Object obj[]=(Object[])itr.next();
				crewMap.put(obj[0]+"", obj[1]+"");
			}
			return crewMap;
		}
	 public List findParentHubByWarehouse(String warehouse,String sessionCorpID){
		 List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+warehouse+"' ").list();
		 return parentName;
	 }
	 public CrewCapacity findGroupNameFromCrewCapacity(String jobType, String sessionCorpID){
		 List crewList = new ArrayList();
		 CrewCapacity crewCapacity = new CrewCapacity();
		 String query ="from CrewCapacity where jobType like '%"+jobType+"%'";
		 crewList = getHibernateTemplate().find(query);
		 if(!crewList.isEmpty()){
			 crewCapacity = (CrewCapacity)crewList.get(0);
		}
		 return crewCapacity;
	 }
}
