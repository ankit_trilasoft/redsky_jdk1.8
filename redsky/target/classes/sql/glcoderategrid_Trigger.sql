delimiter $$

CREATE trigger redsky.trigger_add_history_glcoderategrid
BEFORE UPDATE on redsky.glcoderategrid
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;
    


    

   IF (OLD.costElementId <> NEW.costElementId ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.costElementId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.costElementId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.costElementId,'~') into newValueList;
  END IF;
    


    




   IF (OLD.costElement <> NEW.costElement ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.costElementId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.costElementId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.costElementId,'~') into newValueList;
  END IF;
    


    




   IF (OLD.gridRecGl <> NEW.gridRecGl ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.gridRecGl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gridRecGl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gridRecGl,'~') into newValueList;
  END IF;
    


    




   IF (OLD.gridPayGl <> NEW.gridPayGl ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.gridPayGl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gridPayGl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gridPayGl,'~') into newValueList;
  END IF;
    


    




   IF (OLD.gridJob <> NEW.gridJob ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.gridJob~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gridJob,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gridJob,'~') into newValueList;
  END IF;
    


    




   IF (OLD.gridRouting <> NEW.gridRouting ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.gridRouting~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gridRouting,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gridRouting,'~') into newValueList;
  END IF;
    


    




   IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'glcoderategrid.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;
    


 CALL add_tblHistory (OLD.id,"glcoderategrid", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;