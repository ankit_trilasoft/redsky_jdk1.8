package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="tenancyutilityservice")
public class TenancyUtilityService extends BaseObject{
	
	private Long id;
	private String corpId;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	private Long parentId;
	private String parentFieldName;
	private String TEN_utilityVendorName ;
	private String TEN_utilityVendorCode;
	private BigDecimal TEN_utilityAllowanceAmount= new BigDecimal(0);
	private String TEN_utilityAllowanceCurrency;
	private String TEN_billingCycleUtility;
	private Boolean userUpdatedRow;
	private String TEN_utilityVatDesc;
	private Date TEN_utilityBillThrough;
	private Date TEN_utilityEndDate;
	private String TEN_utilityVatPercent;
	
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TenancyUtilityService))
			return false;
		TenancyUtilityService castOther = (TenancyUtilityService) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(parentFieldName, castOther.parentFieldName)
				.append(TEN_utilityVendorName, castOther.TEN_utilityVendorName)
				.append(TEN_utilityVendorCode, castOther.TEN_utilityVendorCode)
				.append(TEN_utilityAllowanceAmount, castOther.TEN_utilityAllowanceAmount)
				.append(TEN_utilityAllowanceCurrency, castOther.TEN_utilityAllowanceCurrency)
				.append(TEN_billingCycleUtility, castOther.TEN_billingCycleUtility)
				.append(userUpdatedRow, castOther.userUpdatedRow)
				.append(TEN_utilityVatDesc, castOther.TEN_utilityVatDesc)
				.append(TEN_utilityBillThrough, castOther.TEN_utilityBillThrough)
				.append(TEN_utilityEndDate, castOther.TEN_utilityEndDate)
				.append(TEN_utilityVatPercent, castOther.TEN_utilityVatPercent)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(createdOn).append(createdBy).append(updatedOn).append(updatedBy)
				.append(parentFieldName).append(TEN_utilityVendorName).append(TEN_utilityVendorCode).append(TEN_utilityAllowanceAmount)
				.append(TEN_utilityAllowanceCurrency).append(TEN_billingCycleUtility).append(userUpdatedRow)
				.append(TEN_utilityVatDesc).append(TEN_utilityBillThrough).append(TEN_utilityEndDate).append(TEN_utilityVatPercent)
				.toHashCode();
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("parentFieldName", parentFieldName).append("TEN_utilityVendorName", TEN_utilityVendorName)
				.append("TEN_utilityVendorRef", TEN_utilityVendorCode).append("TEN_utilityAllowanceAmount", TEN_utilityAllowanceAmount)
				.append("TEN_utilityAllowanceCurrency", TEN_utilityAllowanceCurrency).append("TEN_billingCycleUtility", TEN_billingCycleUtility)
				.append("userUpdatedRow", userUpdatedRow).append("TEN_utilityVatDesc", TEN_utilityVatDesc)
				.append("TEN_utilityBillThrough", TEN_utilityBillThrough).append("TEN_utilityEndDate", TEN_utilityEndDate)
				.append("TEN_utilityVatPercent", TEN_utilityVatPercent)
				.toString();
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getParentFieldName() {
		return parentFieldName;
	}

	public void setParentFieldName(String parentFieldName) {
		this.parentFieldName = parentFieldName;
	}

	public String getTEN_utilityVendorName() {
		return TEN_utilityVendorName;
	}

	public void setTEN_utilityVendorName(String tEN_utilityVendorName) {
		TEN_utilityVendorName = tEN_utilityVendorName;
	}

	public String getTEN_utilityVendorCode() {
		return TEN_utilityVendorCode;
	}

	public void setTEN_utilityVendorCode(String tEN_utilityVendorCode) {
		TEN_utilityVendorCode = tEN_utilityVendorCode;
	}

	public BigDecimal getTEN_utilityAllowanceAmount() {
		return TEN_utilityAllowanceAmount;
	}

	public void setTEN_utilityAllowanceAmount(BigDecimal tEN_utilityAllowanceAmount) {
		TEN_utilityAllowanceAmount = tEN_utilityAllowanceAmount;
	}

	public String getTEN_utilityAllowanceCurrency() {
		return TEN_utilityAllowanceCurrency;
	}

	public void setTEN_utilityAllowanceCurrency(String tEN_utilityAllowanceCurrency) {
		TEN_utilityAllowanceCurrency = tEN_utilityAllowanceCurrency;
	}

	public String getTEN_billingCycleUtility() {
		return TEN_billingCycleUtility;
	}

	public void setTEN_billingCycleUtility(String tEN_billingCycleUtility) {
		TEN_billingCycleUtility = tEN_billingCycleUtility;
	}

	public Boolean getUserUpdatedRow() {
		return userUpdatedRow;
	}

	public void setUserUpdatedRow(Boolean userUpdatedRow) {
		this.userUpdatedRow = userUpdatedRow;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getTEN_utilityVatDesc() {
		return TEN_utilityVatDesc;
	}

	public void setTEN_utilityVatDesc(String tEN_utilityVatDesc) {
		TEN_utilityVatDesc = tEN_utilityVatDesc;
	}

	public Date getTEN_utilityBillThrough() {
		return TEN_utilityBillThrough;
	}

	public void setTEN_utilityBillThrough(Date tEN_utilityBillThrough) {
		TEN_utilityBillThrough = tEN_utilityBillThrough;
	}

	public Date getTEN_utilityEndDate() {
		return TEN_utilityEndDate;
	}

	public void setTEN_utilityEndDate(Date tEN_utilityEndDate) {
		TEN_utilityEndDate = tEN_utilityEndDate;
	}

	public String getTEN_utilityVatPercent() {
		return TEN_utilityVatPercent;
	}

	public void setTEN_utilityVatPercent(String tEN_utilityVatPercent) {
		TEN_utilityVatPercent = tEN_utilityVatPercent;
	}
}
