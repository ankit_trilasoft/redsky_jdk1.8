<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head> 
<title>Resource List</title> 
<meta name="heading" content="Resource List"/> 

<script type="text/javascript">
	function search(){
		var id = document.forms['gridForm'].elements['id'].value;
		var sid = document.forms['gridForm'].elements['sid'].value;
		var url = "itemsJbkResourceList.html"
		document.forms['gridForm'].action = url;
		document.forms['gridForm'].submit();
	}
	function clear_fields(){
	    document.forms['gridForm'].elements['resource'].value = "";
	    document.forms['gridForm'].elements['category'].value = "";
	}

    function sortMaterialsListList(temp) { 
    	var fieldName=  document.forms['gridForm'].elements['fieldName'].value;
    	var sortType=document.forms['gridForm'].elements['sortType'].value;
    	if(fieldName.trim()=='') {
    		document.forms['gridForm'].elements['sortType'].value='asc';
    	}else{
    		if(fieldName.trim()==temp )
    		{
        	if(sortType.trim()=='asc'){
    			document.forms['gridForm'].elements['sortType'].value='desc';
        	}else{
    			document.forms['gridForm'].elements['sortType'].value='asc';        		
    		}
    		}
    		else{
    			document.forms['gridForm'].elements['sortType'].value='asc';        		
    		}
    	}
    	document.forms['gridForm'].elements['fieldName'].value=temp;  	
    	document.forms['gridForm'].action ='previewInvoicelines.html';
    	//var check =saveValidation();
 		//if(check){
    	document.forms['gridForm'].submit();
   		 //} 
    }
    
</script>

<script type="text/javascript">

function addRow(tableID) {
		  var table = document.getElementById(tableID);
          var rowCount = table.rows.length;
          document.forms['gridForm'].elements['rowCountvalue'].value=rowCount;
          var qty;
          var previousrow=Number(rowCount)-1;
          if(document.getElementById('qty'+previousrow)==null){
            	qty=0;
          }else{
          	qty=document.getElementById('qty'+previousrow).value;
          }          
          //if(qty==''){
           // 	alert("Est. quantity is required field and can not be blank");
          //}else{
          	var newlineid=document.getElementById('newlineid').value;              
          	if(newlineid==''){
              	newlineid=rowCount;
          	}else{
              	newlineid=newlineid+"~"+rowCount;
           	}
            document.getElementById('newlineid').value=newlineid; 
            
            var row = table.insertRow(rowCount); 
            
            var cell2 = row.insertCell(0);
            cell2.innerHTML = rowCount;
            cell2.setAttribute("class", "listwhitetext" );
            var element999 = document.createElement("input");
            element999.type = "hidden";
            element999.id='num'+rowCount;
            element999.value=rowCount;
            element999.name='materialsNumberList';
            cell2.appendChild(element999);
 
            
            var cell15 = row.insertCell(1);
            var element3 = document.createElement("select");
            element3.setAttribute("class", "list-menu" );
            element3.id="cat"+rowCount;
            element3.style.width="90px"
            element3.setAttribute("onchange","validateCategoryValue(this);");
       	 	var categoryList='${resourceCategory}';
       	 	categoryList=categoryList.replace('{','').replace('}','');
       	 	var categoryarray=categoryList.split(",");
            var optioneleBlankCategory= document.createElement("option");
            //optioneleBlankCategory.value="";
            //optioneleBlankCategory.text=""; 
            element3.options.add(optioneleBlankCategory);  
            for(var i=0;i<categoryarray.length;i++){
	            var value=categoryarray[i].split("=");
	            var optionele= document.createElement("option");
	            optionele.value=value[0];
	            optionele.text=value[1]; 
	            element3.options.add(optionele);             
            }
            cell15.appendChild(element3);
            
            var cell13 = row.insertCell(2);
            var element2 = document.createElement("input");
            element2.type = "text";
            element2.setAttribute("class", "input-text" );
            element2.style.width="70%";
            element2.id='res'+rowCount;
            element2.setAttribute("onchange","validateResourceValue(this)");
            element2.setAttribute("onkeyup","autofillRes('"+rowCount+"')");
            cell13.appendChild(element2);
            var elementimg=document.createElement("img");
            elementimg.setAttribute("onclick","chk('"+rowCount+"',this)");
            elementimg.setAttribute("width","14");
            elementimg.setAttribute("height","14");
            elementimg.setAttribute("style","align:top;padding-left:5px");
            //HEIGHT=14 WIDTH=14 ALIGN=TOP
            elementimg.setAttribute("src","<c:url value='/images/plus-small.png'/>");
            cell13.appendChild(elementimg);
            var newDiv = document.createElement("div");
            newDiv.style.width="70%";
            newDiv.id='choices'+rowCount;
            cell13.appendChild(newDiv);
                          
            var cell6 = row.insertCell(3);
            var element4 = document.createElement("input");
            element4.type = "text";
            element4.setAttribute("class", "input-text" );
            element4.setAttribute("style","text-align:right");
            element4.style.width="70px";
            element4.id='qty'+rowCount;
            element4.setAttribute("onchange","isInteger(this);saveJbkResourceList();");
            element4.setAttribute("maxlength","9");
            cell6.appendChild(element4);

            var cell9 = row.insertCell(4);
            var element9 = document.createElement("input");
            element9.type = "text";
            element9.setAttribute("value", "" );
            element9.setAttribute("class", "input-text" );
            element9.style.width="160px";
            element9.id='com'+rowCount;
            element9.name='com'+rowCount;
            element9.setAttribute("onkeyup","checkSPChar('com"+rowCount+"');");
            cell9.appendChild(element9);
            
            var cell50 = row.insertCell(5);
            var element51 = document.createElement("input");
            element51.type = "text";
            element51.setAttribute("value", "1.0" );
            element51.setAttribute("class", "input-text" );
            element51.setAttribute("style","text-align:right");
            element51.style.width="70px";
            element51.id='est'+rowCount;
            element51.setAttribute("onchange","checkFloatNew(this);checkFloat(this);");
            cell50.appendChild(element51);
           
            var cell20 = row.insertCell(6);
            var element20 = document.createElement("input");
            element20.type = "text";
            element20.id='ret'+rowCount;
            element20.setAttribute("class", "input-text" );
            element20.setAttribute("style","text-align:right");
            element20.style.width="70px";
            element20.setAttribute("onchange","checkFloatNew(this);checkFloat(this);");
            cell20.appendChild(element20);            

            var cell6 = row.insertCell(7);
            var element6 = document.createElement("input");
            element6.type = "text";
            element6.setAttribute("class", "input-text" );
            element6.setAttribute("style","text-align:right");
            element6.style.width="70px"
            element6.id='acq'+rowCount;
            element6.setAttribute("onchange","isInteger(this);actualCost('"+rowCount+"');");
            cell6.appendChild(element6);
            
            var cell7 = row.insertCell(8);
            var element7 = document.createElement("input");
            element7.type = "text";
            element7.setAttribute("class", "input-text" );
            element7.setAttribute("style","text-align:right");
            element7.style.width="70px"
            element7.id='cos'+rowCount;
            element7.setAttribute("onchange","checkFloatNew(this);checkFloat(this);actualCost('"+rowCount+"');");
            cell7.appendChild(element7);
            
            var cell222 = row.insertCell(9);
            var element222 = document.createElement("input");
            element222.type = "text";
            element222.setAttribute("class", "input-text" );
            element222.setAttribute("style","text-align:right");
            element222.style.width="70px";
            element222.id='act'+rowCount;
            element222.setAttribute("onchange","checkFloatNew(this);checkFloat(this);");
            cell222.appendChild(element222);            
            //}         
        }


function chk(result,position){
	      var category=document.getElementById('cat'+result).value;
	      var resource=document.getElementById('res'+result).value;
	      findAvailableQtyMethod(category,resource,position);
	}  

		function checkFloatNew(temp) {
		    var check='';  
		    var i; 
			var s = temp.value;
			var fieldName = temp.id;  
			var result2=fieldName.replace(fieldName.substring(0,3),''); 
			var count = 0;
			var countArth = 0;
		
		    for (i = 0; i < s.length; i++) {   
		        var c = s.charAt(i); 
		        if(c == '.')  {
		        	count = count+1
		        }
		        if(c == '-')    {
		        	countArth = countArth+1
		        }
		        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{       	  
		          document.getElementById(fieldName).select();
		          document.getElementById(fieldName).value=''; 
		       	   
		       	} 
		        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
		        	check='Invalid'; 
		        }  }  
		    if(check=='Invalid'){     
			    document.getElementById(fieldName).select();
			    document.getElementById(fieldName).value=''; 		     
		    }  else{
			    s=Math.round(s*100)/100;
			    var value=""+s;
			    if(value.indexOf(".") == -1){
			    	value=value+".00";
			    } 
			    if((value.indexOf(".")+3 != value.length)){
			    	value=value+"0";
			    }
			    document.getElementById(fieldName).value=value; 
		    }  
		}
		function checkFloat(temp)  { 
		    var check='';  
		    var i; 
			var s = temp.value;
			var fieldName = temp.id;  
			var count = 0;
			var countArth = 0;
		    for (i = 0; i < s.length; i++) {   
		        var c = s.charAt(i); 
		        if(c == '.')  {
		        	count = count+1
		        }
		        if(c == '-')    {
		        	countArth = countArth+1
		        }
		        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
		       	  alert("Invalid data." ); 
		          document.getElementById(fieldName).select();
		          document.getElementById(fieldName).value=''; 
		       	   
		       	} 
		        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
		        	check='Invalid'; 
		        }  }  
		    if(check=='Invalid'){ 
		    alert("Invalid data ." ); 
		    document.getElementById(fieldName).select();
		    document.getElementById(fieldName).value=''; 
		     
		    }else{
		    s=Math.round(s*100)/100;
		    var value=""+s;
		    if(value.indexOf(".") == -1){
		    value=value+".00";
		    } 
		    if((value.indexOf(".")+3 != value.length)){
		    	value=value+"0";
		    }
		    	document.getElementById(fieldName).value=value; 
		    }  
		}
		function isInteger(targetElement)
		{   var i;
			var s = targetElement.value;
		    for (i = 0; i < s.length; i++)
		    {   
		        var c = s.charAt(i);
		        if (((c < "0") || (c > "9"))) {
		        alert("Only numbers are allowed here");
		        //alert(targetElement.id);
		        document.getElementById(targetElement.id).value='';
		        document.getElementById(targetElement.id).select();
		        return false;
		        }
		    }
		    return true;
		}
		
		function isNumberKey(evt)
	       {
	          var charCode = (evt.which) ? evt.which : evt.keyCode;
	          if (charCode != 46 && charCode > 31 
	            && (charCode < 48 || charCode > 57))
	             return false;

	          return true;
	       }
		function checkSPChar(id) {
//			 valid(document.forms['entitlementForm'].elements['entitlement.eOption'],'special');
			 var chArr = new Array("%","&","|","\\","#",":","~","\"");
			 var str = document.getElementById(id).value;
		     for ( var i = 0; i < chArr.length; i++ ) {
		    	 str = str.split(chArr[i]).join(''); 
			 }
			 document.getElementById(id).value = str;
			 return true;
		 }
		function saveJbkResourceList(){
		 var resourceListServer="";
	     var categoryListServer="";
	     var qtyListServer="";
	     var comListServer="";
	     var estListServer ="";
	     var returnedListServer ="";
	     var actQtyListServer ="";
	     var costListServer ="";
	     var actualListServer =""; 
	     var id='';
    	 var flag=checkMandatoryAddLineField(); 
		 if(flag==true){
		  var are=document.forms['gridForm'].elements['rowCountvalue'].value;
			 if(are==null||are==''){
				 are='${materialsListSize}';
			 }
			    if(document.forms['gridForm'].resourceList!=undefined){
			        if(are!=1){
			 	      for (i=0; i<document.forms['gridForm'].resourceList.length; i++){	
			 	    	 id=document.forms['gridForm'].resourceList[i].id;
	 	                 id=id.replace(id.substring(0,3),'').trim();
		 	             if(resourceListServer==''){
		 	            	resourceListServer=id+": "+document.forms['gridForm'].resourceList[i].value;
		 	             }else{
		 	            	resourceListServer= resourceListServer+"~"+id+": "+document.forms['gridForm'].resourceList[i].value;
		 	             }
			 	        }	
			 	      }else{
			 	    	 id=document.forms['gridForm'].resourceList.id;
			 	         id=id.replace(id.substring(0,3),'').trim();   
			 	    	 resourceListServer=id+": "+document.forms['gridForm'].resourceList.value;
			 	      }	        
			       }  
			    if(document.forms['gridForm'].categoryList!=undefined){
			        if(document.forms['gridForm'].categoryList.size!=0){
			 	      for (i=0; i<document.forms['gridForm'].categoryList.length; i++){	
			 	    	 id=document.forms['gridForm'].categoryList[i].id;
	 	                 id=id.replace(id.substring(0,3),'').trim();
		 	             if(categoryListServer==''){
		 	            	categoryListServer=id+": "+document.forms['gridForm'].categoryList[i].value;
		 	             }else{
		 	            	categoryListServer= categoryListServer+"~"+id+": "+document.forms['gridForm'].categoryList[i].value;
		 	             }
			 	        }	
			 	      }else{	
			 	    	 id=document.forms['gridForm'].categoryList.id;
			 	         id=id.replace(id.substring(0,3),'').trim();   
			 	    	 categoryListServer=id+": "+document.forms['gridForm'].categoryList.value;
			 	      }	        
			       }
			    
			    if(document.forms['gridForm'].qtyList!=undefined){
			        if(document.forms['gridForm'].qtyList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].qtyList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].qtyList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(qtyListServer==''){
	 	             		  qtyListServer=id+": "+document.forms['gridForm'].qtyList[i].value;
	 	                   }else{
	 	                	  qtyListServer= qtyListServer+"~"+id+": "+document.forms['gridForm'].qtyList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].qtyList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          qtyListServer=id+": "+document.forms['gridForm'].qtyList.value;
			 	     }	        
			 	 }   
			    
			    if(document.forms['gridForm'].comList!=undefined){
			        if(document.forms['gridForm'].comList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].comList.length; i++){
	 	             	   id=document.forms['gridForm'].comList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(comListServer==''){
	 	             		  comListServer=id+": "+document.forms['gridForm'].comList[i].value;
	 	                   }else{
	 	                	  comListServer= comListServer+"~"+id+": "+document.forms['gridForm'].comList[i].value;
	 	                   }
			 	      }
			 	     }else{
			 	          id=document.forms['gridForm'].comList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          comListServer=id+": "+document.forms['gridForm'].comList.value;
			 	     }
			 	 }


			    if(document.forms['gridForm'].estList!=undefined){
			        if(document.forms['gridForm'].estList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].estList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].estList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(estListServer==''){
	 	             		 estListServer=id+": "+document.forms['gridForm'].estList[i].value;
	 	                   }else{
	 	                	  estListServer= estListServer+"~"+id+": "+document.forms['gridForm'].estList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].estList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	         estListServer=id+": "+document.forms['gridForm'].estList.value;
			 	     }	        
			 	 }   
			    
			    if(document.forms['gridForm'].returnedList!=undefined){
			        if(document.forms['gridForm'].returnedList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].returnedList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].returnedList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(returnedListServer==''){
	 	             		 returnedListServer=id+": "+document.forms['gridForm'].returnedList[i].value;
	 	                   }else{
	 	                	  returnedListServer= returnedListServer+"~"+id+": "+document.forms['gridForm'].returnedList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].returnedList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          returnedListServer=id+": "+document.forms['gridForm'].returnedList.value;
			 	     }	        
			 	 }
			    if(document.forms['gridForm'].actQtyList!=undefined){
			        if(document.forms['gridForm'].actQtyList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].actQtyList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].actQtyList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(actQtyListServer==''){
	 	             		  actQtyListServer=id+": "+document.forms['gridForm'].actQtyList[i].value;
	 	                   }else{
	 	                	  actQtyListServer= actQtyListServer+"~"+id+": "+document.forms['gridForm'].actQtyList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].actQtyList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          actQtyListServer=id+": "+document.forms['gridForm'].actQtyList.value;
			 	     }	        
			 	 }   
  
			    if(document.forms['gridForm'].costList!=undefined){
			        if(document.forms['gridForm'].costList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].costList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].costList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(costListServer==''){
	 	             		 costListServer=id+": "+document.forms['gridForm'].costList[i].value;
	 	                   }else{
	 	                	  costListServer= costListServer+"~"+id+": "+document.forms['gridForm'].costList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].costList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          costListServer=id+": "+document.forms['gridForm'].costList.value;
			 	     }	        
			 	 } 
			    
			    if(document.forms['gridForm'].actualList!=undefined){
			        if(document.forms['gridForm'].actualList.length!=undefined){
			 	      for (i=0; i<document.forms['gridForm'].actualList.length; i++){	        	           
	 	             	   id=document.forms['gridForm'].actualList[i].id;
	 	                   id=id.replace(id.substring(0,3),'');
	 	             	   if(actualListServer==''){
	 	             		 	actualListServer=id+": "+document.forms['gridForm'].actualList[i].value;
	 	                   }else{
	 	                	  	actualListServer= actualListServer+"~"+id+": "+document.forms['gridForm'].actualList[i].value;
	 	                   }
			 	       }	
			 	     }else{	   
			 	          id=document.forms['gridForm'].actualList.id;
			 	          id=id.replace(id.substring(0,3),'');     
			 	          actualListServer=id+": "+document.forms['gridForm'].actualList.value;
			 	     }	        
			 	 } 
				 document.getElementById('resourceListServer').value=resourceListServer;
				 document.getElementById('categoryListServer').value=categoryListServer;  
				 document.getElementById('qtyListServer').value=qtyListServer;
				 document.getElementById('comListServer').value=comListServer;
				 document.getElementById('estListServer').value=estListServer;  
			     document.getElementById('returnedListServer').value=returnedListServer;
			     document.getElementById('actQtyListServer').value=actQtyListServer;    
			     document.getElementById('costListServer').value=costListServer;
			     document.getElementById('actualListServer').value=actualListServer;		       

			     var resourceslist="";
			     var newlineid=document.getElementById('newlineid').value; 
			     if(newlineid!=''){
			            var arrayLine=newlineid.split("~");
			            for(var i=0;i<arrayLine.length;i++){              
			                if(resourceslist==''){
			                	resourceslist=document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('res'+arrayLine[i]).value+" :"+document.getElementById('qty'+arrayLine[i]).value+" :"+document.getElementById('com'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('ret'+arrayLine[i]).value+" :"+document.getElementById('acq'+arrayLine[i]).value+" :"+document.getElementById('cos'+arrayLine[i]).value+" :"+document.getElementById('act'+arrayLine[i]).value;
			                }else{
			                	resourceslist=resourceslist+"~"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('res'+arrayLine[i]).value+" :"+document.getElementById('qty'+arrayLine[i]).value+" :"+document.getElementById('com'+arrayLine[i]).value+" :"+document.getElementById('est'+arrayLine[i]).value+" :"+document.getElementById('ret'+arrayLine[i]).value+" :"+document.getElementById('acq'+arrayLine[i]).value+" :"+document.getElementById('cos'+arrayLine[i]).value+" :"+document.getElementById('act'+arrayLine[i]).value;
			                }
			              }
			         }
			     
			    document.getElementById('resourceslist').value =resourceslist;
			    //alert(document.getElementById('resourceslist').value);
			    var id = document.forms['gridForm'].elements['id'].value;
			    var checkHubResourceLimit=document.forms['gridForm'].elements['checkHubResourceLimit'].value;
				var service = '${workTicket.service}';
				var targetActual='${workTicket.targetActual}';
				var warehouse = '${workTicket.warehouse}';
				if(targetActual!='C' && targetActual!='P' && checkHubResourceLimit !='Y' && (service=='LD' || service=='PK' || service=='DU' || service=='DL' || service=='UP' || service=='PL') && warehouse!=''){
					showOrHide(1);
					var url="exceedResourceLimit.html?ajax=1&decorator=simple&popup=true&id="+id+"&qtyListServer="+qtyListServer+"&resourceslist="+resourceslist+"&resourceListServer="+resourceListServer+"&comListServer="+comListServer+"&estListServer="+estListServer+"&categoryListServer="+categoryListServer+"&returnedListServer="+returnedListServer+"&actQtyListServer="+actQtyListServer+"&costListServer="+costListServer+"&actualListServer="+actualListServer+"&checkHubResourceLimit="+checkHubResourceLimit; 
					httpHub.open("GET", url, true);
					httpHub.onreadystatechange = handleHttpHubResponse;
					httpHub.send(null);	
				}else{
					showOrHide(1);
					document.forms['gridForm'].action="saveItemsJbkResourceList.html";
					document.forms['gridForm'].submit();
			  		return true;	
			  	} 
		   }
		}
		function handleHttpHubResponse(){
		    if(httpHub.readyState == 4){ 
		      var results = httpHub.responseText;
		      results = results.trim();
		     	if(results == 'Y'){
		     		showOrHide(0);
		          	var agree = confirm("The hub limit for this resource is exceeding the limit. This Ticket will be sent in Dispatch Queue for Approval.");
		          	if(agree){
		          		showOrHide(1);
				        document.forms['gridForm'].action="saveItemsJbkResourceList.html";
				        document.forms['gridForm'].submit();
		          		return true;
		          	}else{
		          		return false;	
		          	}
		     	 }else{
		     		showOrHide(1);
		     		document.forms['gridForm'].action="saveItemsJbkResourceList.html";
		     		document.forms['gridForm'].submit();
		       		return true; 
		     	 }
		      }
		}

		var httpHub = getHTTPObjectHub();
		function getHTTPObjectHub(){
		    var xmlhttp;
		    if(window.XMLHttpRequest){
		        xmlhttp = new XMLHttpRequest();
		    }else if (window.ActiveXObject){
		        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		        if (!xmlhttp)
		        {
		            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		        }
		    }
		    return xmlhttp;
		}	
		function autofillRes(rowcount){
			//alert(rowcount);
			var divId='choices'+rowcount;
			var resourceId='res'+rowcount;
			var categoryId='cat'+rowcount;
			//alert(categoryId+""+);
			checkAutocompleter(resourceId,divId,categoryId);
		}
function findAvailableQtyMethod(category,resource,position){
	category = category.trim();	
	resource=resource.trim();
	var url="findAvailableQty.html?ajax=1&decorator=simple&popup=true&id=${workTicket.id}&category="+category+"&resource=" + resource;
			ajax_showTooltip(url,position);
		}
function checkAutocompleter(target1,target2,target3){
	 var tempcategory= document.getElementById(target3).value;
	 var tempcategory1=tempcategory.trim();
	 var tempresource= document.getElementById(target1).value;
	// alert(tempcategory+"------"+tempresource+"@@@@@@@@@@@"+target2);
	document.getElementById('gridForm').setAttribute("AutoComplete","off");
	
	 document.forms['gridForm'].elements['tempId'].value=target1;
	 document.forms['gridForm'].elements['tempId2'].value=target2;
	//alert(target1+"------"+target2+"@@@@@@@@@@@"+target3);
	 if(tempcategory1==null){
		 tempcategory1='';
	 }
	 if(tempresource==null){
		 tempresource='';
	 }
	//alert(tempcategory);
	 // alert(tempresource);
	 //new Ajax.Request('/redsky/resourceAutocomplete.html?tempCategory=E&tempresource=M&decorator=simple&popup=true',
	new Ajax.Request('/redsky/resourceAutocomplete.html?tempCategory='+tempcategory1+'&tempresource='+tempresource+'&decorator=simple&popup=true',
			  {
		
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      //alert(response);
			      var container = $(target2);
			      container.update(response);
			      container.show();
			    },
			    onFailure: function(){ alert('Something went wrong...') }
			  });
	}
function checkValue(result){
var targetID=document.forms['gridForm'].elements['tempId'].value;
var targetID2=document.forms['gridForm'].elements['tempId2'].value;
//alert(result.id);
document.getElementById(targetID).value=result.innerHTML;
document.getElementById(targetID2).style.display = "none";
var rec='0';
<c:forEach var="entry" items="${itemsJequipMap}">
	if('${entry.key}'==result.innerHTML){
	rec='${entry.value}';
	}
</c:forEach>
var idVal=targetID.replace('res','');
document.getElementById('cos'+idVal).value=rec;
validateResource(targetID);
}	
function checkExceedLimit(){
	var date1 = '${workTicket.date1}';
	var date2 = '${workTicket.date2}';
	var estimatedWeight= '${workTicket.estimatedWeight}';
	var estimatedCubicFeet= '${workTicket.estimatedCubicFeet}';
	var warehouse = '${workTicket.warehouse}';
	var id = document.forms['gridForm'].elements['id'].value;
	var service = '${workTicket.service}';
	var targetActual='${workTicket.targetActual}';
	if(targetActual!='C' && (service=='LD' || service=='PK' || service=='DU' || service=='DL' || service=='UP' || service=='PL') && date1!='' && date2!='' && warehouse!=''){
	    var url="chekDailyHubLimitForRes.html?ajax=1&decorator=simple&popup=true&id="+id; 
//alert(url);
httpHubLimit.open("GET", url, true);
httpHubLimit.onreadystatechange = handleHttpHubLimitResponse;
httpHubLimit.send(null);	
	}
}

function handleHttpHubLimitResponse()  
{
    if (httpHubLimit.readyState == 4)  { 
      var results = httpHubLimit.responseText;
      results = results.trim(); 
      //alert("results"+results);
     	    document.forms['gridForm'].elements['checkHubResourceLimit'].value=results;          		
             	    }
}

var httpHubLimit = getHTTPObjectHubLimit();
function getHTTPObjectHubLimit(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function actualCost(target){
var actualQuantity= document.getElementById('acq'+target).value;
var unitCost= document.getElementById('cos'+target).value;
if(actualQuantity!='' && unitCost!=''){
var mulValue=actualQuantity*unitCost;
document.getElementById('act'+target).value=mulValue;

}
}

function myFunction(target) {
	var unitCost= document.getElementById('cos'+target).value;
    var n = unitCost.toFixed(2);
   document.getElementById('cos'+target).value = n;
}
 function calc(target)

{var actualQuantity= document.getElementById('acq'+target).value;
var unitCost= document.getElementById('cos'+target).value;
var actualCost=document.getElementById('act'+target).value;
	 if(( actualQuantity==0 || actualQuantity=='')  && (unitCost==0 || unitCost=='')  && (actualCost !=''||actualCost==0)) 
		
	{
	   document.getElementById('cos'+target).value=document.getElementById('act'+target).value;	 
	   var gh=	document.getElementById('act'+target).value/document.getElementById('cos'+target).value;
		document.getElementById('acq'+target).value=gh;	
	}
	 
	 if(isFinite( document.getElementById('acq'+target).value)==false || isFinite(document.getElementById('cos'+target).value)==false)
		{
		var quan=0;
		 document.getElementById('acq'+target).value=quan;
		
		}
	 
	 } 

	 
	

function actualQuantity(target)
{ 
	var actualQuantity= document.getElementById('acq'+target).value;
	var unitCost= document.getElementById('cos'+target).value;
	var actualCost=document.getElementById('act'+target).value;
	if(actualQuantity=='')
		{
		
		 var hj=actualCost/unitCost;     
	

		 
		 document.getElementById('acq'+target).value=Math.round(hj);    
		
			 }   
	if(isFinite( document.getElementById('acq'+target).value)==false)
		{
		var quan=0;
		 document.getElementById('acq'+target).value=quan;
		
		}
		
		} 
	
	
	
function unitCost(target)
{
	
var actual= document.getElementById('acq'+target).value;
var mulVale= document.getElementById('act'+target).value;

if((actual!='' || actual!=0) && ( mulVale!='' || mulVale!=0 )){
         var unit=(mulVale/actual).toFixed(2);
        document.getElementById('cos'+target).value=unit;}
if(isFinite(unit)==false)
	{
	var gh=0;
	document.getElementById('cos'+target).value=gh;

	
	}

	
	}

function validateResource(target){
	var resourceValue= document.getElementById(target).value;
	if(document.forms['gridForm'].resourceList!=undefined){
		for (i=0; i<document.forms['gridForm'].resourceList.length; i++){	
		       id=document.forms['gridForm'].resourceList[i].id;
	           id=id.replace(id.substring(0,3),'').trim();
	           var resValue=document.forms['gridForm'].resourceList[i].value;
	           target1=target.replace(target.substring(0,3),'').trim();
	           if(target1!=id && resourceValue==resValue ){
	              alert('The resource already exists for this work ticket');
	              document.getElementById(target).value="";
	       }
	    }	
	}
}

function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}
</script>
   <script type="text/javascript">
   function disabledAll(){
		var elementsLen=document.forms['gridForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['gridForm'].elements[i].type=='text'){
				document.forms['gridForm'].elements[i].readOnly =true;
				document.forms['gridForm'].elements[i].className = 'input-textUpper';
			}else if(document.forms['gridForm'].elements[i].type=='textarea'){
				document.forms['gridForm'].elements[i].readOnly =true;
				document.forms['gridForm'].elements[i].className = 'textareaUpper';
			}else{
				document.forms['gridForm'].elements[i].disabled=true;
			} 
		}
	}
   </script>
<style>
span.pagelinks {
    display: block;
    font-size: 0.85em;
    margin-bottom: 5px;
    margin-top: -9px;
    padding: 2px 0;
    text-align: right;
    width: 100%;
}
#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>	
</head>

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="gridForm" name="gridForm" action="itemsJbkResourceList.html?id=${workTicket.id}&sid=${serviceOrder.id}"  method="post">
	<c:set var="qtyField" value="N" />
  	<configByCorp:fieldVisibility componentId="component.field.itemsjequip.qtyField">	
	<c:set var="qtyField" value="Y" />
	</configByCorp:fieldVisibility>

 <div id="Layer8" style="width:100%">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>		
	<c:set var="sid" value="<%=request.getParameter("sid") %>" />
	<s:hidden name="sid" value="<%=request.getParameter("sid") %>" />
	<c:set var="id" value="<%=request.getParameter("id") %>"/>
	<s:hidden name="id"  value="<%=request.getParameter("id") %>"/>
    <s:hidden name="tempId"/>
	<s:hidden name="tempId2"/>
	<s:hidden name="rowCountvalue"/>
	<s:hidden name="checkHubResourceLimit"/>
    <s:hidden name="fieldName" value="${fieldName}"/>
    <s:hidden name="sortType" value="${sortType}"/>
    
    <s:hidden name="newlineid" id="newlineid" />
    <s:hidden name="resourceListServer" id="resourceListServer" />
  	<s:hidden name="categoryListServer" id="categoryListServer" />
  	<s:hidden name="qtyListServer" id="qtyListServer" />
  	<s:hidden name="comListServer" id="comListServer" />
  	<s:hidden name="returnedListServer" id="returnedListServer" />
  	<s:hidden name="actQtyListServer" id="actQtyListServer" />
  	<s:hidden name="estListServer" id="estListServer" />
  	<s:hidden name="costListServer" id="costListServer" />
  	<s:hidden name="actualListServer" id="actualListServer" />
  	<s:hidden name="resourceslist" id="resourceslist" />
  	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
  	<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
	</configByCorp:fieldVisibility>
<div id="newmnav">
	    <ul>
	    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
		  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
		  </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			 <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
		  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		  </sec-auth:authComponent>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
		  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		  </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		  <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
		  <c:if test="${forwardingTabVal!='Y'}"> 
	   			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
  			</c:if>
  			<c:if test="${forwardingTabVal=='Y'}">
  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
  			</c:if>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
		  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		  </c:if>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
             <c:if test="${serviceOrder.job =='INT'}">
               <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
             </c:if>
           </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
		   <c:if test="${serviceOrder.job =='RLO'}"> 
 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		</c:if>
		<c:if test="${serviceOrder.job !='RLO'}"> 
				<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		</c:if>		
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">		  
		  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </sec-auth:authComponent>
		  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
		   <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
		  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		  </sec-auth:authComponent>
		  </configByCorp:fieldVisibility>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
		  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
		</sec-auth:authComponent>
		 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
		</ul>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
	
	
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;"><%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table></td>				
<table width="100%" style="margin:0px;">
	<tr>
		<td style="margin:0px;">		
		<div id="newmnav">
				  <ul>
				       <c:if test="${usertype=='DRIVER'}">
		              <li><a href="editWorkTicketUpdate.html?id=${id}"><span>Work Ticket</span></a></li>
		              </c:if>
				 	<sec-auth:authComponent componentId="module.tab.workTicket.workTicketTab">
				    <li><a href="editWorkTicketUpdate.html?id=${id}"><span>Work Ticket</span></a></li>
				    </sec-auth:authComponent>
				    <sec-auth:authComponent componentId="module.tab.workTicket.materialEquipmentTab">
			  			<li><a href="itemsJbkEquipExtraInfo.html?wid=${id}&sid=${serviceOrder.id}&itemType=E"><span>Equipment&nbsp;And&nbsp;Material</span></a></li>
					</sec-auth:authComponent>
				    <sec-auth:authComponent componentId="module.tab.workTicket.materialTab">
					<li><a href="itemsJbkEquips.html?id=${id}&itemType=M&sid=${sid}"><span>Material</span></a></li>
					</sec-auth:authComponent>
					<sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab">
					<li id="newmnav1" style="background:#FFF"><a class="current"><span>Resource</span></a></li>
				    </sec-auth:authComponent>
				    <sec-auth:authComponent componentId="module.tab.workTicket.crewTab">
				    <li><a href="workTicketCrews.html?id=${id}&sid=${sid}"><span>Crew</span></a></li>
				  </sec-auth:authComponent>
				  <configByCorp:fieldVisibility componentId="component.tab.workTicket.TruckingOperations">
					<c:if test="${usertype!='DRIVER'}">
				    <li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
				    </c:if>
				    </configByCorp:fieldVisibility>
				    <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
				    <sec-auth:authComponent componentId="module.tab.workTicket.whseMgmtTab">
				    <li><a href="bookStorages.html?id=${id} "><span>Whse/Mgmt</span></a></li>
				    </sec-auth:authComponent>
				    </configByCorp:fieldVisibility>
				    <sec-auth:authComponent componentId="module.tab.workTicket.formsTab">
					<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					</sec-auth:authComponent>
		  		</ul><div class="spn">&nbsp;</div>
		 </div>
		 </td>
	</tr>
</table>
			
<div id="content" align="center" >
<div id="liquid-round-top">
	<div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
	<div class="center-content">
	<table>
	     <tr>
			<td colspan="6">
			<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
				<tbody>
					<tr>
						<td align="right" height="30" class="listwhitetext"><fmt:message key='workTicket.ticket' /></td>
						<td align="left" ><s:textfield name="workTicket.ticket"  cssClass="input-textUpper" size="10" readonly="true" /></td>
						<td align="right" class="listwhitetext"><fmt:message key="workTicket.service" /></td>
						<td colspan="3"><s:textfield name="workTicket.service" cssClass="input-textUpper" size="10" readonly="true"/></td>
						<td align="right" class="listwhitetext">Warehouse</td> 
						<td colspan="3"><s:textfield name="workTicket.warehouse1"  value="${wrehouseDesc}" cssClass="input-textUpper" size="50" readonly="true"/></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div> 								
<div id="otabs">
	  <ul>
	    <li><a class="current"><span>Search</span></a></li>
	  </ul>
</div>
<div class="spnblk">&nbsp;</div>

<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content">
 <table class="table" width="97%" >
		<thead>
			<tr>
				<th>Category</th>
				<th>Resource</th>
				<th></th>
			</tr>
		</thead>	
			<tbody>
				<tr>
					<td><s:select cssClass="list-menu" name="category" list="%{resourceCategory}" headerKey="" headerValue="" cssStyle="width:200px"/></td>
					<td width="300px" ><s:textfield name="resource"  cssClass="input-text" cssStyle="width:200px;" /></td>	
				<td>
				    <input type="button"  class="cssbutton1" value="Search" style="!margin-bottom: 15px;"  onclick="search()"/>   
				    <input type="button"  class="cssbutton1" value="Clear" style="!margin-bottom: 15px;"  onclick="clear_fields();"/>  
				</td>  
				</tr>
		</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin:0px;"><span>Resource List</span></a></li>		 
	</ul>
	
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
		<div class="listwhitetext" style="position:absolute;padding:5px 110px;font-weight:bold;"><font style="font-size:12px;">Note:</font> Please add/ edit resources from O&I tab, Any resources added in this screen will not be synched to O&I Tab</div>
	  </sec-auth:authComponent>
	  </c:if>
</div>
	
<div class="spnblk">&nbsp;</div>
<div id="para1" style="clear:both;">
		 <table class="table" id="dataTable" style="width:100%;">
		 <thead>
			 <tr>
				 <th width="20px">#</th>
				 
				 <c:if test="${fieldName!='a.type' || fieldName==''}">
				 <th style="width: 13%">Category<font color="red" size="2">*</font></th>
				 </c:if>
				 <c:if test="${fieldName=='a.type'}">
				 <th style="width: 13%"><font color="black">Category</font><font color="red" size="2">*</font></th>
				 </c:if>
				 		 		 
				 <c:if test="${fieldName!='a.descript' || fieldName==''}">
				 <th style="width: 15%">Resource<font color="red" size="2">*</font></th>
				 </c:if>
				 <c:if test="${fieldName=='a.descript'}">
				 <th style="width: 15%"><font color="black">Resource</font></th>
				 </c:if>
				 <c:if test="${fieldName!='a.qty'}">
				 <th style="width: 10%">Est. Quantity
				 <c:choose>			 
				 <c:when test="${qtyField=='N'}">
				  <font color="red" size="2">*</font>
			 	 </c:when>
			 	 <c:otherwise>
			 	 </c:otherwise>
			 	 </c:choose>
			 	 </th>	 
				 </c:if>
				 <c:if test="${fieldName=='a.qty'}">
				 <th style="width: 10%"><font color="black">Est. Quantity</font></th>
				 </c:if>
				 
				 <c:if test="${fieldName!='a.comments' || fieldName==''}">
			     <th style="width: 15%">Comments<font color="red" size="2"></font></th>
				 </c:if>
				 <c:if test="${fieldName=='a.comments'}">
				 <th style="width: 15%"><font color="black">Comments</font></th>
				 </c:if>
				 
				 <c:if test="${fieldName!='a.estHour' || fieldName==''}">
			     <th style="width: 10%">Est. Hour<font color="red" size="2"></font></th>
				 </c:if>
				 <c:if test="${fieldName=='a.estHour'}">
				 <th style="width: 10%"><font color="black">Est. Hour</font></th>
				 </c:if>
		
				 <c:if test="${fieldName!='a.returned' || fieldName==''}">
				 <th style="width: 10%">Returned Quantity</th>
				 </c:if>
				 <c:if test="${fieldName=='a.returned'}">		 
				 <th style="width: 10%"><font color="black">Returned Quantity</a></th>		 		 
				 </c:if>
		
				 <c:if test="${fieldName!='a.actualQty' || fieldName==''}">
				 <th style="width: 10%">Act. Quantity</th>
				 </c:if>
				 <c:if test="${fieldName=='a.actualQty'}">
				 <th style="width: 10%"><font color="black">Act. Quantity</font></th>		 
				 </c:if>
				 
				 <c:if test="${fieldName!='a.cost' || fieldName==''}">
				 <th style="width: 10%">Unit Cost</th>
				 </c:if>
				 <c:if test="${fieldName=='a.cost'}">		 
				 <th style="width: 10%"><font color="black">Unit Cost</font></th>		 
				 </c:if>
				 
		
				 <c:if test="${fieldName!='a.actual' || fieldName==''}">
				 <th style="width: 10%">Actual Cost</th>
				 </c:if>
				 <c:if test="${fieldName=='a.actual'}">
				 <th style="width: 10%"><font color="black">Actual Cost</font></th>		 
				 </c:if>
				 
				 <c:if test="${fieldName!='a.contractRate' || fieldName==''}">
				 <th style="width: 10%">Contract Rate</th>
				 </c:if>
				 <c:if test="${fieldName=='a.contractRate'}">
				 <th style="width: 10%"><font color="black">Contract Rate</font></th>		 
				 </c:if>
			 </tr>
		 </thead>
		 <tbody>
		 <c:set var="countLine" value="${0}" />
		 <c:forEach var="individualItem" items="${materialsList}" >
			 <tr>
			 	<c:set var="countLine" value="${countLine+1}" />
				 <td  class="listwhitetext" align="right" >${countLine}<s:hidden name="materialsNumberList" value="${individualItem.id}" id="num${individualItem.id}" /></td>
				  <c:if test="${individualItem.checkType!='' && individualItem.checkType!=null}">
				 	<td><s:select cssClass="list-menu" list="%{resourceCategory}"  value="'${individualItem.checkType}'" headerKey="" headerValue="" name="categoryList" id="cat${individualItem.id}" cssStyle="width:90px;" onchange="validateResourceCategory('cat${individualItem.id}','res${individualItem.id}');"/></td>		 
				 </c:if>		 
				 <c:if test="${individualItem.checkType=='' || individualItem.checkType==null}">
				 	<td ><s:select cssClass="list-menu" list="%{resourceCategory}"  value="'${individualItem.checkType}'" id="cat${individualItem.id}" headerKey="" headerValue="" cssStyle="width:90px;"/></td>
				 </c:if>
				 
				 <c:if test="${individualItem.descript!='' && individualItem.descript!=null}">
				 <td><s:textfield cssClass="input-text" name="resourceList" value="${individualItem.descript}" id="res${individualItem.id}" onkeyup="checkAutocompleter('res${individualItem.id}','choices${individualItem.id}','cat${individualItem.id}');"  maxlength="25" cssStyle="width:70%;" onchange="validateResourceCategory('cat${individualItem.id}','res${individualItem.id}');"/><img id="target" onclick ="chk('${individualItem.id}',this);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				 <span id="indicator1" style="display: none"><img src="${pageContext.request.contextPath}/images/spinner.gif" alt="Working..." /></span>
                <div id="choices${individualItem.id}" ></div>
				 </td>		 
				 </c:if>		 
				 <c:if test="${individualItem.descript=='' || individualItem.descript==null}">
				 <td ><s:textfield cssClass="input-text" name="resourceList" value="${individualItem.descript}" id="res${individualItem.id}" onkeyup="checkAutocompleter('res${individualItem.id}','choices${individualItem.id}','cat${individualItem.id}');"  maxlength="25" cssStyle="width:70%;"/>
				  <span id="indicator1" style="display: none"><img src="${pageContext.request.contextPath}/images/spinner.gif" alt="Working..." /></span>
                <div id="choices${individualItem.id}" ></div>
				 </td>
				 </c:if>
								 
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.qty}" class="input-text" name="qtyList" id="qty${individualItem.id}" onchange="isInteger(this);saveJbkResourceList();"  maxlength="9" style="width:70px; text-align:right" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.comments}" class="input-text" name="comList" id="com${individualItem.id}" onkeyup="checkSPChar('com${individualItem.id}');" onchange="checkSPChar('com${individualItem.id}');"  maxlength="30" style="width:160px;" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.estHour}" class="input-text" name="estList" id="est${individualItem.id}" onchange="checkFloatNew(this);checkFloat(this);" maxlength="17" style="width:70px; text-align:right" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.returned}" class="input-text" name="returnedList" id="ret${individualItem.id}" onchange="returnedqty('${individualItem.id}')" onkeypress="" maxlength="17" style="width:70px; text-align:right" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.actualQty}" class="input-text" name="actQtyList" id="acq${individualItem.id}"  maxlength="6" onchange="isInteger(this);actualCost('${individualItem.id}');calc('${individualItem.id}');unitCost('${individualItem.id}');" onkeypress="" maxlength="17" style="width:70px; text-align:right" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.cost}" class="input-text" name="costList" id="cos${individualItem.id}" maxlength="6"  onkeypress="return isNumberKey(event);" onclick="myFunction(${individualItem.id});" onchange="checkFloatNew(this);checkFloat(this);actualCost('${individualItem.id}');calc('${individualItem.id}');actualQuantity('${individualItem.id}');"onkeypress="" maxlength="17" style="width:70px; text-align:right" /></td>
				 <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.actual}" class="input-text" name="actualList" id="act${individualItem.id}" maxlength="12"  onkeypress="return isNumberKey(event);" onchange="checkFloatNew(this);checkFloat(this);unitCost('${individualItem.id}');calc('${individualItem.id}');actualQuantity('${individualItem.id}');" onkeypress="" maxlength="17" style="width:70px; text-align:right" /></td>
				  <td  class="listwhitetext" align="right" ><input type="text" value="${individualItem.contractRate}" class="input-text" name="contractRatelList"  maxlength="12" readonly="true" onkeypress="return isNumberKey(event);" onchange="checkFloatNew(this);checkFloat(this);unitCost('${individualItem.id}');calc('${individualItem.id}');actualQuantity('${individualItem.id}');" onkeypress="" maxlength="17" style="width:70px; text-align:right" /></td>		
			 </tr>
		 </c:forEach>
		 </tbody>		 		  
		 </table>
		 </div> 
	  <c:if test="${usertype!='DRIVER'}">
	  <input type="button" class="cssbutton1" id="addLine" name="add" style="width:55px; height:25px;margin-left:10px;" value="Add" onclick="addRow('dataTable');" />
      <input type="button" class="cssbutton1" id="saveLine" name="save" style="width:55px; height:25px;margin-left:10px;" value="Save" onclick="saveJbkResourceList();" />
      </c:if>
</div>
<c:if test="${hitFlag == '1'}" >
		<c:redirect url="/itemsJbkResourceList.html?id=${workTicket.id}&sid=${serviceOrder.id}"  />
	</c:if>
	<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="${pageContext.request.contextPath}/images/ajax-loader.gif" />    
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
</s:form>
 <script>
	function checkMandatoryAddLineField(){
		  var newlineid=document.getElementById('newlineid').value; 	
		  var qtyField='${qtyField}';
		  if(newlineid!=''){
		     var arrayLine=newlineid.split("~");        
		        for(var i=0;i<arrayLine.length;i++){
		   		 var resource=document.getElementById('res'+arrayLine[i]).value;
				 var category=document.getElementById('cat'+arrayLine[i]).value;																
				 var quantity=document.getElementById('qty'+arrayLine[i]).value;
				 var estHour=document.getElementById('est'+arrayLine[i]).value;
				 var returned=document.getElementById('ret'+arrayLine[i]).value;
				 var actualQ=document.getElementById('acq'+arrayLine[i]).value;
				 var cost=document.getElementById('cos'+arrayLine[i]).value;
				 var actual=document.getElementById('act'+arrayLine[i]).value;
			       if(category==""){
				    	 alert("Please select catagory for line # "+arrayLine[i]);
				    	 return false;
			        }
			       if(resource==""){
				    	 alert("Please enter Resource code for line # "+arrayLine[i]);
				    	 return false;
			        }
				   if(quantity=="" && qtyField=='N'){
				    	 alert("Please Enter Est. Quantity for line # "+arrayLine[i]);
				    	 return false;
			       }
				   if(estHour==""){
					   document.getElementById('est'+arrayLine[i]).value='1.0';
			       }
				   if(returned==""){
					   document.getElementById('ret'+arrayLine[i]).value='0.00';
			       }
				   if(actualQ==""){
					   document.getElementById('acq'+arrayLine[i]).value='0';
			       }
				   if(cost==""){
					   document.getElementById('cos'+arrayLine[i]).value='0.00';
			       }
				   if(actual==""){
					   document.getElementById('act'+arrayLine[i]).value='0.00';
			       }
	            }
			    return true;
	       }else{
	 	        return true;
	       }
	  } 
setTimeout("showOrHide(0)",2000);

function validateCategoryValue(target){
	var categoryVal = target.value;
	var categoryValId = target.id;
	categoryValId = categoryValId.substring(3);
	var resourceVal = document.getElementById("res"+categoryValId).value;
	var ticketVal = document.forms['gridForm'].elements['workTicket.ticket'].value;
		new Ajax.Request('getResourceCategoryValueAjax.html?ajax=1&decorator=simple&popup=true&ticket='+ticketVal+'&type='+categoryVal.trim()+'&resource='+resourceVal,
				{
				method:'get',
				onSuccess: function(transport){
					var response = transport.responseText || "";
					response = response.trim();
					if(response=='Y'){
						document.getElementById("res"+categoryValId).value="";
						alert('The resource already exists for this work ticket');
						return false;
					}
				},
				onFailure: function(){ 
				}
			});
}
function validateResourceValue(target){
	showOrHide(1);
	var resourceVal = target.value;
	var resourceValId = target.id;
	resourceValId = resourceValId.substring(3);
	var categoryVal = document.getElementById("cat"+resourceValId).value;
	var ticketVal = document.forms['gridForm'].elements['workTicket.ticket'].value;
		new Ajax.Request('getResourceCategoryValueAjax.html?ajax=1&decorator=simple&popup=true&ticket='+ticketVal+'&type='+categoryVal.trim()+'&resource='+resourceVal,
				{
				method:'get',
				onSuccess: function(transport){
					var response = transport.responseText || "";
					response = response.trim();
					showOrHide(0);
					if(response=='Y'){
						document.getElementById("res"+resourceValId).value="";
						alert('The resource already exists for this work ticket');
						return false;
					}
				},
				onFailure: function(){ 
				}
			});
}

function validateResourceCategory(typeId,descriptionId){
	var categoryVal = document.getElementById(typeId).value;
	var resourceVal = document.getElementById(descriptionId).value;
	var resourceValId = descriptionId.substring(3);
	var ticketVal = document.forms['gridForm'].elements['workTicket.ticket'].value;
		new Ajax.Request('getResourceCategoryValueAjax.html?ajax=1&decorator=simple&popup=true&ticket='+ticketVal+'&type='+categoryVal.trim()+'&resource='+resourceVal,
				{
				method:'get',
				onSuccess: function(transport){
					var response = transport.responseText || "";
					response = response.trim();
					if(response=='Y'){
						document.getElementById("res"+resourceValId).value="";
						alert('The resource already exists for this work ticket');
						return false;
					}
				},
				onFailure: function(){ 
				}
			});
}




function returnedqty(target)
{
	var estqty= document.getElementById('qty'+target).value;  
	var retqty= document.getElementById('ret'+target).value;
    var actqty=estqty-retqty;
    var mulVale= document.getElementById('acq'+target).value=actqty;
	
	
	}
</script>
<script type="text/javascript">
checkExceedLimit();
try{
	document.getElementById('gridForm').setAttribute("AutoComplete","off");
}catch(e){
	
}
</script>
<script type="text/javascript">
try{
	//Created by subrat BUG #7530
	<sec-auth:authComponent componentId="module.tab.workTicket.DRIVER">
	disabledAll();
	</sec-auth:authComponent>
}catch(e){
	
}
</script>

