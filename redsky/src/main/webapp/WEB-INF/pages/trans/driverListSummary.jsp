<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Driver List</title> 
<meta name="heading" content="Driver List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
!margin-top:-18px;
padding:0px;
text-align:right;
width:100%;
}
#otabs {
margin-bottom:0;
!margin-bottom:-20px;
margin-left:40px;
position:relative;
}
</style>

<script language="javascript" type="text/javascript">
function getOwnerResult(partnerCode,firstName,lastName)
{
var pCode = document.forms['driverListData'].elements[partnerCode].value;	
var fName = document.forms['driverListData'].elements[firstName].value;
var lName = document.forms['driverListData'].elements[lastName].value;
window.opener.document.forms['myDriverEventForm'].elements['calendarFile.personId'].value=pCode;
window.opener.document.forms['myDriverEventForm'].elements['calendarFile.firstName'].value=fName;
window.opener.document.forms['myDriverEventForm'].elements['calendarFile.lastName'].value=lName;
window.close();
}

</script>
</head>

	<s:form id="driverListData" name="driverListData" action='' method="post" validate="true">   
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Driver</span></a></li>
		  </ul>
		  
	</div>
<div class="spnblk">&nbsp;</div>
	<display:table name="driverListData" class="table" requestURI="" id="driverListData" export="true" defaultsort="" pagesize="10" style="width:100%" >   
	<display:column title="Driver"><a href="#" onclick="getOwnerResult('partnerCode${driverListData.id}','firstName${driverListData.id}','lastName${driverListData.id}');"/><c:out value="${driverListData.partnerCode}"/></a>
	<s:hidden name="partnerCode${driverListData.id}" value="${driverListData.partnerCode}"  />
	<s:hidden name="firstName${driverListData.id}" value="${driverListData.firstName}"  />
	<s:hidden name="lastName${driverListData.id}" value="${driverListData.lastName}"  />
	</display:column>
	<display:column property="firstName"  sortable="true" title="First Name"/>
	<display:column property="lastName"  sortable="true" title="Last Name"/>   	
   	</display:table>
	
	</div>
</s:form> 

<script type="text/javascript"> 

</script> 