<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Email View</title>   
    <meta name="heading" content="Email View"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
    .headtab_bg_center {
     width: 70%;
	}
	.textareaUpper {background-color: #E3F1FE; border: 1px solid #ABADB3;color: #000000;
    font-family: arial,verdana;  font-size: 12px;     text-decoration: none;
	}
	.signaturetxt{font-weight:bold;vertical-align:top;}
	.sigText{vertical-align:top;}
	div#mydiv{margin-top: -45px;}
	.blue{color:#15428B;font-family:Arial,Helvetica,sans-serif;font-size:1em;font-weight:bold;}
.green{color:#04b915;font-family:Arial,Helvetica,sans-serif;font-size:1em;font-weight:bold;}
.red{color:#b90404;font-family:Arial,Helvetica,sans-serif;font-size:1em;font-weight:bold;}
</style>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
</script>	
	</head>
	<s:form id="tableCatalogForm" action="" onsubmit="" method="post" validate="true">
		<div id="content" align="center">
	<div id="liquid-round">
    <div class="top" style="!margin-top:2px;"><span></span></div>
    <div class="center-content">
	<table cellspacing="1" cellpadding="2" border="0">
	<c:if test="${emailFlag != 'SaveForEmail' || retryCount>2}" >
	 <c:choose> 
	 <c:when test="${fn:indexOf(emailFlag,'SaveForEmail')>-1}">
		<tr>
		<td width="70px"><img src="${pageContext.request.contextPath}/images/mail_ntsent.gif" HEIGHT=70 WIDTH=70 style="cursor: default" onclick="notExists();"/></td>
		<td colspan="3" class="red" style="font-weight:bold;font-size:14px;color:gray;">Error occured while sending mail to 
		${recipientTo}  
		<c:if test="${not empty recipientCC}" >
		,
		</c:if>
		${recipientCC}
		<c:if test="${not empty rBcc}" >
		,
		</c:if>
		${rBcc}</td>
		</tr>
	 </c:when>
	 <c:otherwise>
		<tr>
		<td width="70px"><img src="${pageContext.request.contextPath}/images/mail_sent.gif" HEIGHT=70 WIDTH=70 style="cursor: default" onclick="notExists();"/></td>
		<td class="green" colspan="3" style="font-weight:bold;font-size:14px;color:gray;">Your mail has been successfully sent to 
		${recipientTo}  
		<c:if test="${not empty recipientCC}" >
		,
		</c:if>
		${recipientCC}
		<c:if test="${not empty rBcc}" >
		,
		</c:if>
		${rBcc}</td>
		</tr>
	 </c:otherwise>
	 </c:choose>
	</c:if>
	<c:if test="${emailFlag == 'SaveForEmail' && retryCount<=2}" >
	<tr>
	<td width="70px"><img src="${pageContext.request.contextPath}/images/mail_pending.gif" HEIGHT=70 WIDTH=70 style="cursor: default" onclick="notExists();"/></td>
	<td colspan="3" class="yellow" style="font-weight:bold;font-size:14px;color:gray;">Please wait...while your mail is being sent to 
		${recipientTo}  
		<c:if test="${not empty recipientCC}" >
		,
		</c:if>
		${recipientCC}
		<c:if test="${not empty rBcc}" >
		,
		</c:if>
		${rBcc}</td>
	</tr>
	</c:if>
	<tr> <td class="listwhitetext signaturetxt" align="right">Sent:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left">
	<fmt:timeZone value="${sessionTimeZone}" >
	<fmt:formatDate value="${emailSetupDateSent}" pattern="${displayDateTimeFormat}"/>
	</fmt:timeZone></td></tr> 
	<tr> <td class="listwhitetext signaturetxt" align="right">From:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left">${rSignature}</td></tr> 
	<tr> <td class="listwhitetext signaturetxt" align="right">To:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left">${recipientTo}</td></tr> 
	<tr> <td class="listwhitetext signaturetxt" align="right">Cc:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left">${recipientCC}</td></tr> 
	<tr> <td class="listwhitetext signaturetxt" align="right">Subject:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left">${reportSubject}</td></tr> 
	<tr><td style="padding-bottom:10px;"></td></tr>
	<tr> <td class="listwhitetext signaturetxt" align="right">Body:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left" colspan="3">${reportBody}</td></tr>
	<tr><td style="padding-bottom:10px;"></td></tr>
	<tr> <td class="listwhitetext signaturetxt" align="right">Signature:</td>&nbsp;&nbsp;<td class="listwhitetext sigText" align="left" colspan="3">${reportSignature}</td></tr>
	<tr><td style="padding-bottom:5px;"></td></tr>
	<tr> <td class="listwhitetext signaturetxt" align="right">Email Status:</td>&nbsp;&nbsp;
	<c:if test="${emailFlag != 'SaveForEmail' || retryCount>2}" >
	 	<c:choose> 
		 <c:when test="${fn:indexOf(emailFlag,'SaveForEmail')>-1}">
		 <td class="listwhitetext" align="left" style="font-weight:bold;color:red;">Failed</td>
		</c:when>
		<c:otherwise>
		 <td class="listwhitetext" align="left" style="font-weight:bold;color:green;">Sent</td>
		</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${emailFlag == 'SaveForEmail' && retryCount<=2}" >
			<td class="listwhitetext" align="left" style="font-weight:bold;color:orange;">Pending</td>
	</c:if>
   </table>
	</div><div class="bottom-header"><span></span></div></div></div>
	</s:form>