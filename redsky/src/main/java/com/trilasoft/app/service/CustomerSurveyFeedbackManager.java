package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CustomerSurveyFeedback;

public interface CustomerSurveyFeedbackManager extends GenericManager<CustomerSurveyFeedback, Long> {

}
