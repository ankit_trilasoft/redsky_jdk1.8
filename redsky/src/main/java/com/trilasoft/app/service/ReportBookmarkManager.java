package com.trilasoft.app.service;

import org.appfuse.model.Role;
import org.appfuse.service.GenericManager;
import java.util.List;
import java.util.Set;

import com.trilasoft.app.model.ReportBookmark;



public interface ReportBookmarkManager extends GenericManager<ReportBookmark, Long>  {

    
    public List isExist(Long reportId, String userName);

	public List getBookMarkedReportList(String userName, String module, String menu, String subModule,String description ,String sessionCorpID, Set<Role> roles);

	public List getBookMarkedFormList(String userName ,String module, String menu, String subModule, String description, String sessionCorpID);

	
}
