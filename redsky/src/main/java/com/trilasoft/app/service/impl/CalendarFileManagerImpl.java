package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CalendarFileDao;
import com.trilasoft.app.model.CalendarFile;
import com.trilasoft.app.service.CalendarFileManager;

public class CalendarFileManagerImpl extends GenericManagerImpl<CalendarFile, Long> implements CalendarFileManager {   
	CalendarFileDao calendarFileDao;   
    public CalendarFileManagerImpl(CalendarFileDao calendarFileDao) {   
        super(calendarFileDao);   
        this.calendarFileDao = calendarFileDao;   
    } 
    public List findByUserName(String corpID,String user){
    	return calendarFileDao.findByUserName(corpID, user);
    }
    
    public List getUserFormSysDefault(String usrName){
    	return calendarFileDao.getUserFormSysDefault(usrName);
    }
    
    public List searchMyEvents(String sessionCorpID,String consult,String surveyFrom,String surveyTo){
    	return calendarFileDao.searchMyEvents(sessionCorpID,consult,surveyFrom,surveyTo);
    }
    public List getDriverListDataSummary(String sessionCorpID){
    	return calendarFileDao.getDriverListDataSummary(sessionCorpID);
    }
    public String driverDetailCode(String myDriverCode){
    	return calendarFileDao.driverDetailCode(myDriverCode);
    }
}
