package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.FreightMarkUpControl;

public interface FreightMarkUpManager extends GenericManager<FreightMarkUpControl, Long>{
	 public List checkById(Long id);
}
