package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SubcontractorChargesDao;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.service.SubcontractorChargesManager;

public class SubcontractorChargesManagerImpl extends GenericManagerImpl<SubcontractorCharges, Long> implements SubcontractorChargesManager{

	SubcontractorChargesDao subcontractorChargesDao ;
    
     
    public SubcontractorChargesManagerImpl(SubcontractorChargesDao subcontractorChargesDao) { 
        super(subcontractorChargesDao); 
        this.subcontractorChargesDao = subcontractorChargesDao; 
    }


	public List findMaxId() {
		return subcontractorChargesDao.findMaxId();
	}


	public List<SubcontractorCharges> findAllRecords() {
		
		return subcontractorChargesDao.findAllRecords();
	}


	public List<SubcontractorCharges> findById(Long long1) {
		
		return subcontractorChargesDao.findById(long1);
	} 
	 
	 public List<SubcontractorCharges> registrationNo(String regNo)
	   {
		   return subcontractorChargesDao.registrationNo(regNo) ;
	   }

	 public List findServiceOrder(String regNo){
		 return subcontractorChargesDao.findServiceOrder(regNo);
		 
	 }
	 public List findRegNo(String serviceOrder){
		 return subcontractorChargesDao.findRegNo(serviceOrder);
		 
	 }
	public List findBySysDefault(String corpID) {
		
		return subcontractorChargesDao.findBySysDefault(corpID);
	}


	public List findBranchList(String corpID) { 
		
		return subcontractorChargesDao.findBranchList(corpID);
	}


	public List getsubcontrCompanyCode(String corpID) {
		return subcontractorChargesDao.getsubcontrCompanyCode(corpID);
	} 


	public List getSubContcExtract(String subcontrCompanyCode,  String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) { 
		
		return subcontractorChargesDao.getSubContcExtract(subcontrCompanyCode,  corpid,  postDate, startInvoiceDates,  endInvoiceDates);
	}


	public List getSubContcExtractlevel1(String personId,String corpid ,String subcontrCompanyCode) {
		
		return subcontractorChargesDao.getSubContcExtractlevel1(personId,corpid,subcontrCompanyCode);
	}


	public List findGlCodeForExtract(String personId, String corpid) {
		
		return subcontractorChargesDao.findGlCodeForExtract(personId, corpid);
	}


	public List getSubContcExtractlevel4(String personId,String corpid, String glCode,String subcontrCompanyCode) {
		return subcontractorChargesDao.getSubContcExtractlevel4(personId,corpid, glCode,subcontrCompanyCode);
	}


	public List getSubContcNivExtractlevel1(String personId ,String corpid,String subcontrCompanyCode) {
		
		return subcontractorChargesDao.getSubContcNivExtractlevel1(personId,corpid,subcontrCompanyCode);
	}


	public List getSubContcNivExtractlevel4(String personId ,String corpid,String glCode,String subcontrCompanyCode) {
		return subcontractorChargesDao.getSubContcNivExtractlevel4(personId,corpid, glCode,subcontrCompanyCode);
		
	}


	public int subContcFileUpdate(String batchName, String subContcPersonId,String corpid) {
		
		return subcontractorChargesDao.subContcFileUpdate(batchName, subContcPersonId,corpid);
	}
	public List getNonsettledAmount(String subContcPersonId,String corpid){
		return subcontractorChargesDao.getNonsettledAmount(subContcPersonId, corpid);
	}
	public List findMiscellaneousStatus(String serviceOrder){
		return subcontractorChargesDao.findMiscellaneousStatus(serviceOrder);
	}


	public List<SubcontractorCharges> findAmountDesc(String serviceOrder, String corpId,String driver) {
		return subcontractorChargesDao.findAmountDesc(serviceOrder, corpId,driver);
	}


	public List getgpSubContcExtract(String sessionCorpID) {
		return subcontractorChargesDao.getgpSubContcExtract(sessionCorpID);
	}


	public int gpSubContcFileUpdate(String batchName, String subIdList, String sessionCorpID) {
		return subcontractorChargesDao.gpSubContcFileUpdate(batchName, subIdList, sessionCorpID);
	}
	public List getCompanyCode(String agentCode, String sessionCorpID){
		return subcontractorChargesDao.getCompanyCode(agentCode,sessionCorpID);
	}


	public List getSubContcExtractHoll(String subcontrCompanyCode, String corpid,String postDate) {
		return subcontractorChargesDao.getSubContcExtractHoll(subcontrCompanyCode, corpid,postDate);
	}
	public List getAccountInvoicePostDataList(String corpId){
		return subcontractorChargesDao.getAccountInvoicePostDataList(corpId);
	}


	public List getSubcontrCompanyCodeDataList(String sessionCorpID) {
		return subcontractorChargesDao.getSubcontrCompanyCodeDataList(sessionCorpID);
	}


	public List getSubcontrACCCompanyCodeDataList(String sessionCorpID) {
		return subcontractorChargesDao.getSubcontrACCCompanyCodeDataList(sessionCorpID);
	}


	public List getMergeSubcontrACCCompanyCodeDataList(String sessionCorpID) {
		return subcontractorChargesDao.getMergeSubcontrACCCompanyCodeDataList(sessionCorpID);
	}


	public List getSubContcMergeExtract(String subcontrCompanyCode, String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) {
		return subcontractorChargesDao.getSubContcMergeExtract(subcontrCompanyCode, corpid, postDate, startInvoiceDates, endInvoiceDates);
	}


	public List getSSCWGPSubcontrACCCompanyCodeDataList(String sessionCorpID) {
		return subcontractorChargesDao.getSSCWGPSubcontrACCCompanyCodeDataList(sessionCorpID);
	}


	public List getSubContcSSCWGPExtract(String subcontrCompanyCode, String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) {
		return subcontractorChargesDao.getSubContcSSCWGPExtract(subcontrCompanyCode, corpid, postDate, startInvoiceDates, endInvoiceDates);
	}


	public int subContcSSCWGPFileUpdate(String batchName, String subIdList, String sessionCorpID) {
		return subcontractorChargesDao.subContcSSCWGPFileUpdate(batchName, subIdList, sessionCorpID);
	}  
	 }
