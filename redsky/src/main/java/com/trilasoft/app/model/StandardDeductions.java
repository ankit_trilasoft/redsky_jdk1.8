package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "standarddeductions")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class StandardDeductions extends BaseObject {
	private Long id;

	private String corpID;

	private String partnerCode;

	private String chargeCode;

	private String status;

	private String chargeCodeDesc;
	
	private String serviceorder;

	private BigDecimal amount;

	private BigDecimal maxDeductAmt;

	private BigDecimal rate;

	private Date lastDeductionDate;

	private BigDecimal amtDeducted;

	private BigDecimal deduction;
	private BigDecimal balanceAmount;
	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	private Boolean personalEscrow;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("partnerCode", partnerCode).append("chargeCode",
				chargeCode).append("status", status).append("chargeCodeDesc",
				chargeCodeDesc).append("serviceorder", serviceorder).append(
				"amount", amount).append("maxDeductAmt", maxDeductAmt).append(
				"rate", rate).append("lastDeductionDate", lastDeductionDate)
				.append("amtDeducted", amtDeducted).append("deduction",
						deduction).append("balanceAmount", balanceAmount)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("personalEscrow", personalEscrow).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof StandardDeductions))
			return false;
		StandardDeductions castOther = (StandardDeductions) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(partnerCode, castOther.partnerCode)
				.append(chargeCode, castOther.chargeCode).append(status,
						castOther.status).append(chargeCodeDesc,
						castOther.chargeCodeDesc).append(serviceorder,
						castOther.serviceorder)
				.append(amount, castOther.amount).append(maxDeductAmt,
						castOther.maxDeductAmt).append(rate, castOther.rate)
				.append(lastDeductionDate, castOther.lastDeductionDate).append(
						amtDeducted, castOther.amtDeducted).append(deduction,
						castOther.deduction).append(balanceAmount,
						castOther.balanceAmount).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(personalEscrow,
						castOther.personalEscrow).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				partnerCode).append(chargeCode).append(status).append(
				chargeCodeDesc).append(serviceorder).append(amount).append(
				maxDeductAmt).append(rate).append(lastDeductionDate).append(
				amtDeducted).append(deduction).append(balanceAmount).append(
				createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(personalEscrow).toHashCode();
	}

	@Column(length = 10)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Column(length = 20)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmtDeducted() {
		return amtDeducted;
	}

	public void setAmtDeducted(BigDecimal amtDeducted) {
		this.amtDeducted = amtDeducted;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeCodeDesc() {
		return chargeCodeDesc;
	}

	public void setChargeCodeDesc(String chargeCodeDesc) {
		this.chargeCodeDesc = chargeCodeDesc;
	}

	public BigDecimal getMaxDeductAmt() {
		return maxDeductAmt;
	}

	public void setMaxDeductAmt(BigDecimal maxDeductAmt) {
		this.maxDeductAmt = maxDeductAmt;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getDeduction() {
		return deduction;
	}

	public void setDeduction(BigDecimal deduction) {
		this.deduction = deduction;
	}

	public Date getLastDeductionDate() {
		return lastDeductionDate;
	}

	public void setLastDeductionDate(Date lastDeductionDate) {
		this.lastDeductionDate = lastDeductionDate;
	}
	@Column(length = 15)
	public String getServiceorder() {
		return serviceorder;
	}

	public void setServiceorder(String serviceorder) {
		this.serviceorder = serviceorder;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Boolean getPersonalEscrow() {
		return personalEscrow;
	}

	public void setPersonalEscrow(Boolean personalEscrow) {
		this.personalEscrow = personalEscrow;
	}
}
