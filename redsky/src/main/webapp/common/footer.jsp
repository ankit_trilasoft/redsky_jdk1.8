<%@ include file="/common/taglibs.jsp" %>

 <table width="100%" style="height:25px; padding:3px 5px 5px 5px; margin: 0px"><tr><td valign="middle">
 
    <span class="left"><fmt:message key="webapp.version"/>
     <c:if test="${sessionExp!='true'}">
    <c:if test="${not empty pageContext.request.remoteUser}"> |
		<fmt:message key="user.status"/> <authz:authentication operation="fullName"/>
		</c:if>
		</c:if>
    </span>
    <span class="right">
        &copy; <fmt:message key="copyright.year"/> <a href="<fmt:message key="company.url"/>"><fmt:message key="company.name"/></a>
    </span>
</td></tr></table>
