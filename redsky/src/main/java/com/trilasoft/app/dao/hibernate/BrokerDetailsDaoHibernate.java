package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.BrokerDetailsDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.BrokerDetails;

public class BrokerDetailsDaoHibernate extends GenericDaoHibernate<BrokerDetails, Long> implements BrokerDetailsDao {   
    public BrokerDetailsDaoHibernate()
    {   
      super(BrokerDetails.class);   
    }   
    private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public List getByShipNumber(String shipNumber) {
		return getHibernateTemplate().find("from BrokerDetails where shipNumber=?",shipNumber);
	}
}
