<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="storageList.title" /></title>
<meta name="heading" content="<fmt:message key='storageList.heading'/>" />
</head>


<c:set var="buttons">
	<input type="button" style="margin-right: 5px"
		onclick="location.href='<c:url value="/editStorage.html"/>'"
		value="<fmt:message key="button.add"/>" />

	<input type="button"
		onclick="location.href='<c:url value="/mainMenu.html"/>'"
		value="<fmt:message key="button.done"/>" />
</c:set>

<c:out value="${buttons}" escapeXml="false" />

<s:set name="storages" value="storages" scope="request" />
<display:table name="storages" class="table" requestURI=""
	id="storageList" export="true" pagesize="25">
	<tr>
		<display:column property="id" sortable="true" href="editStorage.html"
			paramId="id" paramProperty="id" titleKey="storage.id" />
		<display:column property="description" sortable="true"
			titleKey="storage.description" />
		<display:column property="measQuantity" sortable="true"
			titleKey="storage.measQuantity" />
	</tr>
	<display:setProperty name="paging.banner.item_name" value="storage" />
	<display:setProperty name="paging.banner.items_name" value="storage" />

	<display:setProperty name="export.excel.filename"
		value="Storage List.xls" />
	<display:setProperty name="export.csv.filename"
		value="Storage List.csv" />
	<display:setProperty name="export.pdf.filename"
		value="Storage List.pdf" />
</display:table>

<c:out value="${buttons}" escapeXml="false" />

<script type="text/javascript"> 
    highlightTableRows("searchstorageList"); 
</script>
