<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="RevenueBillByRouting"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="595"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="0"
		 topMargin="2"
		 bottomMargin="2"
		 whenNoDataType="AllSectionsNoDetail"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Bill To" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select  "RevenueBillByRouting-SSCW.jrxml",
   if(serviceorder.`routing` is null || trim(serviceorder.`routing`)='',"Other",serviceorder.`routing`) AS Routing,
   round(sum(accountline.actualRevenue),0) AS "Actual Revenue",
   serviceorder.`shipNumber` AS "Ship Number",
   serviceorder.`firstName` AS "First Name",
   serviceorder.`lastName` AS "Last Name",
   serviceorder.`originCity` AS "Origin City",
     serviceorder.`originState` AS "Origin State",
   serviceorder.`originCountry` AS "Origin Country",
   serviceorder.`destinationCity` AS "Destination City",
   serviceorder.`destinationCountry` AS "Destination Country",
     serviceorder.`destinationState` AS "Destination State",
     serviceorder.`serviceType` AS "Service Type",
     serviceorder.`packingMode` AS "Packing Mode",
   DATE_FORMAT(billing.`revenueRecognition`,'%d %M %Y') AS "Revenue Recognition",
    DATE_FORMAT(billing.`billComplete`,'%d %M %Y') AS "BillComplete",
   billing.`payMethod` AS "Pay Method",
     if(serviceorder.mode='Air', miscellaneous.`actualGrossWeight`,miscellaneous.`actualNetWeight`) AS Weight,
     trackingstatus.`billLading` AS trackingstatus_billLading,
     DATE_FORMAT(trackingstatus.`deliveryA`,'%d %M %Y') AS "Delivery Actual",
     DATE_FORMAT(trackingstatus.`loadA`,'%d %M %Y') AS "Load Actual",
     DATE_FORMAT(trackingstatus.`packA`,'%d %M %Y') AS "Pack Actual",
     trackingstatus.`originAgent` AS "Origin Agent",
     trackingstatus.`destinationAgent` AS "Destination Agent",
     serviceorder.`bookingAgentName` AS "Booking Agent",
    accountline.`billtoname` AS "Bill To",
   round(sum(accountline.actualRevenue/1000),0) "ActualRevenue",
DATE_FORMAT(concat(year(now())-1,'-01-01'),'%d %M %Y')   "Start Date",
   DATE_FORMAT(concat(year(now())-1,'-12-31'),'%d %M %Y')   "End Date"
FROM accountline accountline, serviceorder serviceorder,billing billing,miscellaneous miscellaneous,trackingstatus trackingstatus
WHERE accountline.`serviceOrderId` = serviceorder.`id`
AND serviceorder.`id`=billing.`id`
AND serviceorder.`id`=miscellaneous.`id`
AND serviceorder.`id`=trackingstatus.`id`
and serviceorder.`status` not in ('CNCL','DWNLD')
and accountline.`billtocode`=$P{Bill To}
and DATE_FORMAT(billing.`revenuerecognition`,'%d %m %Y') between (concat(year(now())-1,'-01-01') and concat(year(now())-1,'-12-31'))
and accountline.`status` is true
and accountline.`actualRevenue` <> 0
and accountline.`receivedInvoiceDate` is not null
and serviceorder.corpId = $P{Corporate ID}
group by serviceorder.`id`
order by routing,serviceorder.`id`]]></queryString>

	<field name="RevenueBillByRouting-SSCW.jrxml" class="java.lang.String"/>
	<field name="Routing" class="java.lang.String"/>
	<field name="Actual Revenue" class="java.math.BigDecimal"/>
	<field name="Ship Number" class="java.lang.String"/>
	<field name="First Name" class="java.lang.String"/>
	<field name="Last Name" class="java.lang.String"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin State" class="java.lang.String"/>
	<field name="Origin Country" class="java.lang.String"/>
	<field name="Destination City" class="java.lang.String"/>
	<field name="Destination Country" class="java.lang.String"/>
	<field name="Destination State" class="java.lang.String"/>
	<field name="Service Type" class="java.lang.String"/>
	<field name="Packing Mode" class="java.lang.String"/>
	<field name="Revenue Recognition" class="java.lang.String"/>
	<field name="BillComplete" class="java.lang.String"/>
	<field name="Pay Method" class="java.lang.String"/>
	<field name="Weight" class="java.math.BigDecimal"/>
	<field name="trackingstatus_billLading" class="java.lang.String"/>
	<field name="Delivery Actual" class="java.lang.String"/>
	<field name="Load Actual" class="java.lang.String"/>
	<field name="Pack Actual" class="java.lang.String"/>
	<field name="Origin Agent" class="java.lang.String"/>
	<field name="Destination Agent" class="java.lang.String"/>
	<field name="Booking Agent" class="java.lang.String"/>
	<field name="Bill To" class="java.lang.String"/>
	<field name="ActualRevenue" class="java.math.BigDecimal"/>
	<field name="Start Date" class="java.lang.String"/>
	<field name="End Date" class="java.lang.String"/>

	<sortField name="ActualRevenue" order="Descending" />

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="68"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="147"
						y="9"
						width="301"
						height="25"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Revenue Analysis by Routing Type]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="25"
						y="67"
						width="535"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="152"
						y="48"
						width="101"
						height="19"
						key="staticText-8"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Revenue Period]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="253"
						y="48"
						width="118"
						height="19"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Start Date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="371"
						y="48"
						width="19"
						height="19"
						key="staticText-9"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[to]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="390"
						y="48"
						width="143"
						height="19"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{End Date}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="327"  isSplitAllowed="true" >
				<pieChart>
					<chart evaluationTime="Report"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="3"
						width="300"
						height="300"
						key="element-1"
						isPrintRepeatedValues="false"/>
					<box></box>
						<chartLegend textColor="#000000" backgroundColor="#FFFFFF" >
					</chartLegend>
					</chart>
					<pieDataset>
						<dataset resetType="None" >
						</dataset>
						<keyExpression><![CDATA[$F{Routing}]]></keyExpression>
						<valueExpression><![CDATA[$F{ActualRevenue}]]></valueExpression>
						<labelExpression><![CDATA[$F{Routing} + ": " + "$" +$F{ActualRevenue}]]></labelExpression>
				<sectionHyperlink >
				</sectionHyperlink>
					</pieDataset>
					<piePlot 
isCircular="true" >
						<plot backcolor="#FFFFFF" />
					</piePlot>
				</pieChart>
				<staticText>
					<reportElement
						x="25"
						y="308"
						width="535"
						height="19"
						forecolor="#FF0033"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Note: Values are in $000s]]></text>
				</staticText>
				<stackedBar3DChart>
					<chart  hyperlinkTarget="Self" >
					<reportElement
						x="300"
						y="3"
						width="295"
						height="297"
						key="element-2"/>
					<box></box>
						<chartLegend textColor="#000000" backgroundColor="#FFFFFF" >
					</chartLegend>
					</chart>
					<categoryDataset>
						<dataset >
						</dataset>
						<categorySeries>
							<seriesExpression><![CDATA[$F{Routing}]]></seriesExpression>
							<categoryExpression><![CDATA[$F{Routing}]]></categoryExpression>
							<valueExpression><![CDATA[$F{ActualRevenue}]]></valueExpression>
							<labelExpression><![CDATA[$F{Routing} + ": " + "$" +$F{ActualRevenue}]]></labelExpression>
				<itemHyperlink >
				</itemHyperlink>
						</categorySeries>
					</categoryDataset>
					<bar3DPlot >
						<plot />
						<categoryAxisFormat>
							<axisFormat >
							</axisFormat>
						</categoryAxisFormat>
						<valueAxisFormat>
							<axisFormat >
							</axisFormat>
						</valueAxisFormat>
					</bar3DPlot>
				</stackedBar3DChart>
			</band>
		</summary>
</jasperReport>
