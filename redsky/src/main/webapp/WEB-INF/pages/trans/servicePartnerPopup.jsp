<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="servicePartnerPopup.title"/></title>   
    <meta name="heading" content="<fmt:message key='servicePartnerPopup.heading'/>"/>   
</head>

<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editPartnerAddForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
           
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>  

<s:form id="partnerListForm" action='${empty param.popup?"searchPartners.html":"searchPartners.html?decorator=popup&popup=true"}' method="post" >  

<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
</c:if>

<!--
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">Search</div></td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>

<table class="table" width="90%" >
<thead>
<tr>
<th><fmt:message key="servicePartner.lastName"/></th>
<th><fmt:message key="servicePartner.partnerCode"/></th>
<th><fmt:message key="servicePartner.billingCountryCode"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="servicePartner.lastName" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="servicePartner.partnerCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="servicePartner.billingCountryCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:submit cssClass="button" method="search" key="button.search"/>   
			</td>
		</tr>
		</tbody>
	</table>
	 -->
</s:form>
<s:set name="servicePartners" value="servicePartners" scope="request"/>  
<table class='detailTabLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody>
	    <tr>
	      <td width="4" align="right"><img width="2" height="25"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">List</td>
			<td width="4" align="left"><img width="2" height="25"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
	    </tr>
	</tbody>
</table>
<display:table name="servicePartners" class="table" requestURI="" id="servicePartnerPopup" export="${empty param.popup}" defaultsort="2" pagesize="6" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   

	 <c:if test="${empty param.popup}">  
		<display:column property="carrierCode" sortable="true" titleKey="servicePartner.carrierCode"
		href="editCharges.html" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="servicePartner.carrierCode"/>   
    </c:if>

    <display:column property="partnerType" sortable="true" titleKey="servicePartner.partnerType"/>    
    <display:column property="carrierCode" sortable="true" titleKey="servicePartner.carrierCode"/>
    <display:column property="carrierName" sortable="true" titleKey="servicePartner.carrierName"/>
    <display:column property="city" sortable="true" titleKey="servicePartner.city"/>
    <display:column property="carrierDeparture" sortable="true" titleKey="servicePartner.carrierDeparture"/>
    <display:column property="etDepart" sortable="true" titleKey="servicePartner.etDepart" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="carrierArrival" sortable="true" titleKey="servicePartner.carrierArrival"/>
    <display:column property="etArrival" sortable="true" titleKey="servicePartner.etArrival" format="{0,date,dd-MMM-yyyy}"/>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="servicePartners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
 
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>

<script type="text/javascript">   
   //highlightTableRows("partnerList");   
</script>