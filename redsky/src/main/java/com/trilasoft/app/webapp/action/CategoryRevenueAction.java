package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CategoryRevenue;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.service.CategoryRevenueManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;

public class CategoryRevenueAction extends BaseAction{
	
	
	private Long id;
	private Long maxId;
	private String sessionCorpID;
	private CategoryRevenueManager categoryRevenueManager;
	private PayrollManager payrollManager;
	private CategoryRevenue categoryRevenue;
	private List<CategoryRevenue> categoryRevenueList;
	private Set categoryRevenueSet;
	private RefMasterManager refMasterManager;
	private Map<String, String> prate;
	private Map<String, String> compDevision;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CategoryRevenueAction.class);

	 public CategoryRevenueAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	
	public CategoryRevenue getCategoryRevenue() {
		return categoryRevenue;
	}

	public void setCategoryRevenue(CategoryRevenue categoryRevenue) {
		this.categoryRevenue = categoryRevenue;
	}

	public List<CategoryRevenue> getCategoryRevenueList() {
		return categoryRevenueList;
	}

	public void setCategoryRevenueList(List<CategoryRevenue> categoryRevenueList) {
		this.categoryRevenueList = categoryRevenueList;
	}

	public Set getCategoryRevenueSet() {
		return categoryRevenueSet;
	}

	public void setCategoryRevenueSet(Set categoryRevenueSet) {
		this.categoryRevenueSet = categoryRevenueSet;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public void setCategoryRevenueManager(
			CategoryRevenueManager categoryRevenueManager) {
		this.categoryRevenueManager = categoryRevenueManager;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		prate=refMasterManager.findByParameter(corpId, "P_RATE");
		compDevision=payrollManager.getOpenCompnayDevisionCode(corpId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String categoryRevenueList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		categoryRevenueList=categoryRevenueManager.getAll();
		categoryRevenueSet=new HashSet(categoryRevenueList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		if (id != null)
		{
			categoryRevenue=categoryRevenueManager.get(id);
			try{
				if(categoryRevenue !=null && categoryRevenue.getCompanyDivision()!=null && (!(categoryRevenue.getCompanyDivision().toString().trim().equals("")))){
					
					if(compDevision.containsKey(categoryRevenue.getCompanyDivision())){
						
					}else{
						compDevision=payrollManager.getOpenCompnayDevisionCode(sessionCorpID);
					}
					}
				}
			catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		else
		{
			categoryRevenue=new CategoryRevenue();
			categoryRevenue.setCreatedOn(new Date());
			categoryRevenue.setValueDate(new Date());
			categoryRevenue.setUpdatedOn(new Date());
		
		}
		categoryRevenue.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		boolean isNew = (categoryRevenue.getId() == null);
		
		if(isNew)
	    {
			categoryRevenue.setCreatedOn(new Date());
	    }
	      else
	        {
	    	  categoryRevenue.setCreatedOn(categoryRevenue.getCreatedOn());
	        }
		
		categoryRevenue.setCorpID(sessionCorpID);
		categoryRevenue.setUpdatedBy(getRequest().getRemoteUser());
		categoryRevenue.setUpdatedOn(new Date());
		categoryRevenue= categoryRevenueManager.save(categoryRevenue);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "CategoryRevenue has been added successfully" : "CategoryRevenue has been updated successfully";
			saveMessage(getText(key));
		}
		
		if (!isNew) { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(categoryRevenueManager.findMaximumId().get(0).toString());
        	categoryRevenue = categoryRevenueManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;
        } 
	}
	
	public String search(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean secondSet=categoryRevenue.getSecondSet()==null;
		boolean totalCrew=categoryRevenue.getTotalCrew()==null;
		boolean numberOfCrew1=categoryRevenue.getNumberOfCrew1()==null;
		boolean numberOfCrew2=categoryRevenue.getNumberOfCrew2()==null;
		boolean numberOfCrew3=categoryRevenue.getNumberOfCrew3()==null;
		boolean numberOfCrew4=categoryRevenue.getNumberOfCrew4()==null;
		if(!secondSet || !totalCrew || !numberOfCrew1 || !numberOfCrew2 || !numberOfCrew3 || !numberOfCrew4) {
			categoryRevenueList=categoryRevenueManager.searchCategoryRevenue(categoryRevenue.getSecondSet(), categoryRevenue.getTotalCrew(), categoryRevenue.getNumberOfCrew1(),categoryRevenue.getNumberOfCrew2(),categoryRevenue.getNumberOfCrew3(),categoryRevenue.getNumberOfCrew4());
			categoryRevenueSet = new HashSet(categoryRevenueList);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	private String gotoPageString;
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String s = save();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return gotoPageString;
    }

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Map<String, String> getPrate() {
		return prate;
	}

	public void setPrate(Map<String, String> prate) {
		this.prate = prate;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}

	public Map<String, String> getCompDevision() {
		return compDevision;
	}

	public void setCompDevision(Map<String, String> compDevision) {
		this.compDevision = compDevision;
	}

}
