<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Email Configuration Details</title>   
    <meta name="heading" content="Email Configuration Details"/>   

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<style>
span.pagelinks {display:block;font-size:0.9em;margin-bottom:3px;margin-top:-9px;!margin-top:-20px;padding:2px 0px;text-align:right;width:100%;}
.CentText{text-align:center !important;}
</style>
<script language="javascript">
var http10 = getHTTPObject();
var http11 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validFieldCheck()
{
	
	var recipient0=document.forms['emailConfigrationDetailsForm'].elements['recipient0'].value;
	var subject0=document.forms['emailConfigrationDetailsForm'].elements['subject0'].value;
	var body0=document.forms['emailConfigrationDetailsForm'].elements['body0'].value;
	var signature0=document.forms['emailConfigrationDetailsForm'].elements['signature0'].value;

	var recipient1=document.forms['emailConfigrationDetailsForm'].elements['recipient1'].value;
	var subject1=document.forms['emailConfigrationDetailsForm'].elements['subject1'].value;
	var body1=document.forms['emailConfigrationDetailsForm'].elements['body1'].value;
	var signature1=document.forms['emailConfigrationDetailsForm'].elements['signature1'].value;

	var recipient2=document.forms['emailConfigrationDetailsForm'].elements['recipient2'].value;
	var subject2=document.forms['emailConfigrationDetailsForm'].elements['subject2'].value;
	var body2=document.forms['emailConfigrationDetailsForm'].elements['body2'].value;
	var signature2=document.forms['emailConfigrationDetailsForm'].elements['signature2'].value;

	var recipient3=document.forms['emailConfigrationDetailsForm'].elements['recipient3'].value;
	var subject3=document.forms['emailConfigrationDetailsForm'].elements['subject3'].value;
	var body3=document.forms['emailConfigrationDetailsForm'].elements['body3'].value;
	var signature3=document.forms['emailConfigrationDetailsForm'].elements['signature3'].value;

	var recipient4=document.forms['emailConfigrationDetailsForm'].elements['recipient4'].value;
	var subject4=document.forms['emailConfigrationDetailsForm'].elements['subject4'].value;
	var body4=document.forms['emailConfigrationDetailsForm'].elements['body4'].value;
	var signature4=document.forms['emailConfigrationDetailsForm'].elements['signature4'].value;

	var recipient5=document.forms['emailConfigrationDetailsForm'].elements['recipient5'].value;
	var subject5=document.forms['emailConfigrationDetailsForm'].elements['subject5'].value;
	var body5=document.forms['emailConfigrationDetailsForm'].elements['body5'].value;
	var signature5=document.forms['emailConfigrationDetailsForm'].elements['signature5'].value;

	var recipient6=document.forms['emailConfigrationDetailsForm'].elements['recipient6'].value;
	var subject6=document.forms['emailConfigrationDetailsForm'].elements['subject6'].value;
	var body6=document.forms['emailConfigrationDetailsForm'].elements['body6'].value;
	var signature6=document.forms['emailConfigrationDetailsForm'].elements['signature6'].value;
	var surveyFrom=document.forms['emailConfigrationDetailsForm'].elements['surveyFrom'].value;
	var delayInterval=document.forms['emailConfigrationDetailsForm'].elements['delayInterval'].value;
	var sendLimit=document.forms['emailConfigrationDetailsForm'].elements['sendLimit'].value;
		if(delayInterval=="")		{
	  	 		alert("Please Enter Delay Days");
	  	 		document.forms['emailConfigrationDetailsForm'].elements['delayInterval'].value = '';
	  	 		return false; 
		}
		if(sendLimit=="")		{
	  	 		alert("Please Enter Send Limit");
	  	 		document.forms['emailConfigrationDetailsForm'].elements['sendLimit'].value = '';
	  	 		return false; 
		}
		if(surveyFrom=="")		{
  	 		alert("Please Select Survey From");
  	 		document.forms['emailConfigrationDetailsForm'].elements['surveyFrom'].value = '';
  	 		return false; 
		}
		
		if((recipient0!="")||(subject0!="")||(body0!="")||(signature0!=""))
		{
			
  	 		if((recipient0=="")||(subject0=="")||(body0=="")||(signature0==""))
  	 		{
  	  	 		alert("Please Enter All Items Of First Survey Request");
  	  		return false; 
  	 		}
		}
		if((recipient1!="")||(subject1!="")||(body1!="")||(signature1!=""))
		{
			
  	 		if((recipient1=="")||(subject1=="")||(body1=="")||(signature1==""))
  	 		{
  	  	 		alert("Please Enter All Items Of First Reminder");
  	  	 		
  	  		return false; 
  	 		}
		}
		if((recipient2!="")||(subject2!="")||(body2!="")||(signature2!=""))
		{
			
  	 		if((recipient2=="")||(subject2=="")||(body2=="")||(signature2==""))
  	 		{
  	  	 		alert("Please Enter All Items Of Second Reminder");
  	  	 		
  	  		return false; 
  	 		}
		}
		if((recipient3!="")||(subject3!="")||(body3!="")||(signature3!=""))
		{
			
  	 		if((recipient3=="")||(subject3=="")||(body3=="")||(signature3==""))
  	 		{
  	  	 		alert("Please Enter All Items Of Third Reminder");
  	  	 		
  	  		return false; 
  	 		}
		}
		if((recipient4!="")||(subject4!="")||(body4!="")||(signature4!=""))
		{
			
  	 		if((recipient4=="")||(subject4=="")||(body4=="")||(signature4==""))
  	 		{
  	  	 		alert("Please Enter All Items Of Fourth Reminder");
  	  	 		
  	  		return false; 
  	 		}
		}
		if((recipient5!="")||(subject5!="")||(body5!="")||(signature5!=""))
		{
			
  	 		if((recipient5=="")||(subject5=="")||(body5=="")||(signature5==""))
  	 		{
  	  	 		alert("Please Enter All Items Of Fifth Reminder");
  	  	 		
  	  		return false; 
  	 		}
		}
		if((recipient6!="")||(subject6!="")||(body6!="")||(signature6!=""))
		{
			
  	 		if((recipient6=="")||(subject6=="")||(body6=="")||(signature6==""))
  	 		{
  	  	 		alert("Please Enter All Items Of Sixth Reminder");
  	  		return false;  	  	 		 
  	 		}
		}
		if((sendLimit=="1")&&((recipient0=="")||(recipient1=="")))
		{
			alert("You must fill value of 0-1th Record");
			return false;  	  	 	
		}
		if((sendLimit=="2")&&((recipient0=="")||(recipient1=="")||(recipient2=="")))
		{
			alert("You must fill value of 0-2th Record");
			return false;  	  	 	
		}
		if((sendLimit=="3")&&((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")))
		{
			alert("You must fill value of 0-3th Record");
			return false;  	  	 	
		}
		if((sendLimit=="4")&&((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4=="")))
		{
			alert("You must fill value of 0-4th Record");
			return false;  	  	 	
		}
		if((sendLimit=="5")&&((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4=="")||(recipient5=="")))
		{
			alert("You must fill value of 0-5th Record");
			return false;  	  	 	
		}
		if((sendLimit=="6")&&((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4=="")||(recipient5=="")||(recipient6=="")))
		{
			alert("You must fill value of 0-6th Record");
			return false;  	  	 	
		}
		if(recipient0!="")
		{
			
  	 		if(recipient0=="")
  	 		{
  	  	 		alert("Please Fill All values  Before 1th Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient1!="")
		{
			
  	 		if((recipient0=="")||(recipient1==""))
  	 		{
  	  	 		alert("Please Fill All values  Before 3rd Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient2!="")
		{
			
  	 		if((recipient0=="")||(recipient1=="")||(recipient2==""))
  	 		{
  	  	 		alert("Please Fill All values  Before 4th Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient3!="")
		{
			
  	 		if((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3==""))
  	 		{
  	  	 		alert("Please Fill All values  Before 5th Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient4!="")
		{
			
  	 		if((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4==""))
  	 		{
  	  	 		alert("Please Fill All values  Before 6th Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient5!="")
		{
			
  	 		if((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4=="")||(recipient5==""))
  	 		{
  	  	 		alert("Please Fill All values  Before 7th Record");
  	 			return false;  	  	 		 
  	 		}
		}
		if(recipient6!="")
		{
			
  	 		if((recipient0=="")||(recipient1=="")||(recipient2=="")||(recipient3=="")||(recipient4=="")||(recipient5=="")||(recipient6==""))
  	 		{
  	  	 		alert("Please Fill All values  upto 7th Record");
  	 			return false;  	  	 		 
  	 		}
		}
}
function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");
	 var accountId = document.forms['emailConfigrationDetailsForm'].elements['accountId'].value;
	 var mode = document.forms['emailConfigrationDetailsForm'].elements['mode'].value;
	 accountId=accountId+"~"+mode;
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=QSE&excludeFPU=TRUE&partnerCode="+accountId;
		  http10.open("GET", url, true); 
		  http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=QSE&excludeFPU=FALSE&partnerCode="+accountId;
		  http10.open("GET", url, true); 
	      http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }
}
function handleHttpResponse(){
    if (http10.readyState == 4)
    {
    			  var results = http10.responseText
	               results = results.trim();					               
				if(results=="")	{
				
				}
				else{
				
				} 
    } 
}
function updateChildFromParent()
{
  		  var accountId = document.forms['emailConfigrationDetailsForm'].elements['accountId'].value;
		  var mode = document.forms['emailConfigrationDetailsForm'].elements['mode'].value;
		  accountId=accountId+"~"+mode;
		  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&partnerCode="+accountId+"&pageListType=QSE";
		  http11.open("GET", url, true); 
		  http11.onreadystatechange = handleHttpResponse1; 
	      http11.send(null); 
}
function handleHttpResponse1(){
    if (http11.readyState == 4)
    {
    			  var results = http11.responseText
	               results = results.trim();					               
				if(results!="")	{
				alert(results);				
				} 
    } 
}
function goToEdit(fieldName,param,length){
	var id=document.forms['emailConfigrationDetailsForm'].elements[fieldName].value;
	if(id!=''){
		 window.open('editRefMastermenu.html?parameter='+param+'&id='+id+'&l='+length, '_blank');		
	}	
}
function goToEditURL(fieldName,length,type){
	var surveyId=document.forms['emailConfigrationDetailsForm'].elements[fieldName].value;
	var refmasterUrlId='';
	var refmasterJobId='';
	var refmasterTransferreId='';
	var refmasterRecommendId='';
	var refmasterDestinationId='';
	var refmasterCoordinatorId='';
	var refmasterOriginId='';
	var refmasterOverallId='';
	var refmasterPremoveId='';
	var refmasterNpsId='';
	
	if(surveyId!=''){
		<c:forEach var="entry" items="${surveyIdMap}">
		if(surveyId=="${entry.value}"){
			refmasterUrlId="${entry.key}";
		}
		</c:forEach>
		<c:forEach var="entry" items="${answerIdMap}">
		var temp1='${entry.value}';
		var temp=temp1.split("~");		
		if(temp[0].trim()==surveyId){
			if(temp[1].trim()=='JOBID'){
				refmasterJobId="${entry.key}";
			}else if(temp[1].trim()=='TRANSFERRE'){
				refmasterTransferreId="${entry.key}";
			}else if(temp[1].trim()=='RECOMMEND'){
				refmasterRecommendId="${entry.key}";
			}else if(temp[1].trim()=='DESTINATION'){
				refmasterDestinationId="${entry.key}";
			}else if(temp[1].trim()=='COORDINATOR'){
				refmasterCoordinatorId="${entry.key}";
			}else if(temp[1].trim()=='ORIGIN'){
				refmasterOriginId="${entry.key}";
			}else if(temp[1].trim()=='OVERALL'){
				refmasterOverallId="${entry.key}";
			}else if(temp[1].trim()=='PREMOVE'){
				refmasterPremoveId="${entry.key}";
			}else if(temp[1].trim()=='NPS'){
				refmasterNpsId="${entry.key}";
			}else {
			}
		}
		</c:forEach>
			   window.open("emailConfigrationPopop.html?configType="+type+"&type=edit&refmasterUrlId="+refmasterUrlId+"&refmasterJobId="+refmasterJobId+"&refmasterTransferreId="+refmasterTransferreId+"&refmasterRecommendId="+refmasterRecommendId+"&refmasterDestinationId="+refmasterDestinationId+"&refmasterCoordinatorId="+refmasterCoordinatorId+"&refmasterOriginId="+refmasterOriginId+"&refmasterOverallId="+refmasterOverallId+"&refmasterPremoveId="+refmasterPremoveId+"&refmasterNpsId="+refmasterNpsId+"&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=500,height=350");			 		
	}
}
function openWindowMethod(type)
{
	if(type=='surveyfrom'){
		window.open("emailConfigrationPopop.html?configType="+type+"&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=500,height=350");
	}else{
		window.open("emailConfigrationPopop.html?configType="+type+"&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=350,height=250");		
	}
}
</script>
</head>
<s:form id="emailConfigrationDetailsForm" action="saveEmailConfigDetails" onsubmit="return validFieldCheck();" method="post" validate="true">

<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Email Configuration</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<s:hidden name="mode"/>
<s:hidden name="shipNumber"/>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table width="100%" border="0" style="border:2px solid #74B3DC;text-align:right;padding:3px;">		
				<tr><td class="listwhitetext" >Partner&nbsp;Code</td>
  				    <td align="left">
					    <s:textfield name="accountId" required="true"  cssClass="input-textUpper" size="8" readonly="true"/>
					</td>
					<td class="listwhitetext">Partner&nbsp;Name</td>
					<td align="left">
					    <s:textfield name="accountName" required="true" cssClass="input-textUpper" size="37" readonly="true"/>
					</td>
					<td class="listwhitetext">Survey&nbsp;Form
					<font color="red" size="2">*</font></td>
  				    <td align="left">
					    <s:select name="surveyFrom" list="%{surveyFromList}" cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
					    <a href="#" onclick="openWindowMethod('surveyfrom');" style="text-decoration:underline;cursor:pointer;"><img src="images/file_add.png" style="vertical-align: bottom;"></a>
					    <a onclick="goToEditURL('surveyFrom','200','surveyfrom')" style="text-decoration:underline;cursor:pointer;"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>					    
					</td>
					<td class="listwhitetext">Delay&nbsp;Days<font color="red" size="2">*</font></td>
  				    <td align="left">
					    <s:select name="delayInterval" list="{'1','2','3','4','5','6','7','8','9','10','11','12','13','14'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
					</td>
					<td class="listwhitetext">Send&nbsp;Limit<font color="red" size="2">*</font></td>
					<td align="left">
					    <s:select name="sendLimit" list="{'1','2','3','4','5','6'}" cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
					</td>					
				</tr>		
							
		</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
      <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Email Configuration List</span></a></li>		    
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>	
		 <table class="table" id="dataTable" style="!margin-top:5px;">
		 <thead>
		 <tr>		 
		 <th width="35px"># Sent</th>		 		 
		 <th width="115px"><a href="#" onclick="sortPayableProcessingList('a.shipNumber');">Description</a></th>
		 <th width="">Recipient<a onclick="openWindowMethod('recipient');"> <img src="images/file_add.png" style="vertical-align: middle;"></a></th>
		 <th width="">Subject<a onclick="openWindowMethod('subject');"> <img src="images/file_add.png" style="vertical-align: middle;"></a></th>
		 <th width="">Body<a onclick="openWindowMethod('body');"> <img src="images/file_add.png" style="vertical-align: middle;"></a></th>
		 <th width="">Signature<a onclick="openWindowMethod('signature');"> <img src="images/file_add.png" style="vertical-align: middle;"> </a></th>		 		 		 
		 </tr>
		 </thead>
		 <tbody>
		 
		 <tr>
		 <td  class="listwhitetext CentText" >0</td>
		 <td  class="listwhitetext" align="right" width="12%">First Survey Request</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient0" list="%{parameterRecipient}" id="recipient0" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient0','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject0" list="%{parameterSubject}" id="subject0" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject0','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body0" list="%{parameterBody}" id="body0" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body0','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td> 		
		 <td  class="listwhitetext" align="right" ><s:select name="signature0" list="%{parameterSignature}" id="signature0" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature0','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText" >1</td>
		 <td  class="listwhitetext" align="right" >First Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient1" list="%{parameterRecipient}" id="recipient1" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient1','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject1" list="%{parameterSubject}" id="subject1" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject1','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body1" list="%{parameterBody}" id="body1" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body1','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature1" list="%{parameterSignature}" id="signature1" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature1','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText"  >2</td>
		 <td  class="listwhitetext" align="right" >Second Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient2" list="%{parameterRecipient}" id="recipient2" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient2','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject2" list="%{parameterSubject}" id="subject2" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject2','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body2" list="%{parameterBody}" id="body2" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body2','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature2" list="%{parameterSignature}" id="signature2" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature2','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText"  >3</td>
		 <td  class="listwhitetext" align="right" >Third Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient3" list="%{parameterRecipient}" id="recipient3" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient3','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject3" list="%{parameterSubject}" id="subject3" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject3','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body3" list="%{parameterBody}" id="body3" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body3','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature3" list="%{parameterSignature}" id="signature3" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature3','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText" >4</td>
		 <td  class="listwhitetext" align="right" >Fourth Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient4" list="%{parameterRecipient}" id="recipient4" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient4','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject4" list="%{parameterSubject}" id="subject4" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject4','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body4" list="%{parameterBody}" id="body4" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body4','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature4" list="%{parameterSignature}" id="signature4" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature4','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText"  >5</td>
		 <td  class="listwhitetext" align="right" >Fifth Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient5" list="%{parameterRecipient}" id="recipient5" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient5','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject5" list="%{parameterSubject}" id="subject5" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject5','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body5" list="%{parameterBody}" id="body5" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body5','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature5" list="%{parameterSignature}" id="signature5" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature5','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
		 </tr>
		 <tr>
		 <td  class="listwhitetext CentText"  >6</td>
		 <td  class="listwhitetext" align="right" >Sixth Reminder</td>
		 <td  class="listwhitetext" align="right" ><s:select name="recipient6" list="%{parameterRecipient}" id="recipient6" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('recipient6','QSURVEYRECIPIENT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="subject6" list="%{parameterSubject}" id="subject6" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('subject6','QSURVEYSUBJECT','200')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
 		 </td>
 		 <td  class="listwhitetext" align="right" ><s:select name="body6" list="%{parameterBody}" id="body6" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('body6','QSURVEYBODY','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a> 		 
 		 </td>
		 <td  class="listwhitetext" align="right" ><s:select name="signature6" list="%{parameterSignature}" id="signature6" cssClass="list-menu"  cssStyle="width:160px" headerKey="" headerValue=""/>&nbsp;<a onclick="goToEdit('signature6','QSURVEYSIGNATURE','50')"><img src="images/file_edit.png" style="vertical-align: bottom;"></a>
		 </td>		 
		 </tr>		 
		 </tbody>		 		  
		 </table>		
        
        <table width="100%" cellpadding="2" style="margin:0px;padding:0px;">
        <tr>
		<td align="left">
 		 <div class="listwhitetext" ><i>*Click On <img src="images/file_add.png" style="vertical-align: bottom;"> To Add New SurveyForm,Recipient, Subject, Body, And /Or Signature.</i></div>
		 <div class="listwhitetext" ><i>*Click On <img src="images/file_edit.png" style="vertical-align: middle;"> To Edit SurveyForm,Recipient, Subject, Body, And /Or Signature.</i></div>
		 <div class="listwhitetext" style="height:5px;"></div>
		</td>
		<td align="right">
		<table width="100%" style="margin:0px;padding:0px;">
		<tr align="right">
		<td align="right" colspan="2"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px;" onclick="updateChildFromParent();"/></td><td></td>
		</tr>
		<tr align="right">
		<td align="right" class="listwhitetext">Exclude from Parent updates</td>
		<td class="listwhitetext" width="8px" align="right"><s:checkbox name="excludeFromParent" id="excludeFPU" value="${excludeFPU}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>				 		  	   	 	               	
	</tr>												
	</table>
</td>
</tr>
<tr><td align="left" class="vertlinedata" colspan="3"></td></tr>	
</table>       
        
<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save"/>
<td></td>   
 <s:reset type="button" cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.reset"/> 
</div>		
</s:form>
<script type="text/javascript">
try{
		  	<c:if test="${excludeFPU==true}">
							document.getElementById("excludeFPU").checked=true;
			</c:if>
		  	<c:if test="${excludeFPU==false}">
			document.getElementById("excludeFPU").checked=false;
			</c:if>
}
catch(e){}
</script>