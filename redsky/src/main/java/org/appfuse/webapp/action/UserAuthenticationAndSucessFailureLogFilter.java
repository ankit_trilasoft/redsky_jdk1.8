package org.appfuse.webapp.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.Authentication;
import org.acegisecurity.AuthenticationException;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.acegisecurity.ui.AbstractProcessingFilter;
import org.appfuse.model.User;
import org.appfuse.service.SignUpService;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;

public class UserAuthenticationAndSucessFailureLogFilter extends AbstractProcessingFilter{
	
	private SignUpService signUpService;
	private UserManager userManager;
	
	public static final String ACEGI_SECURITY_FORM_USERNAME_KEY = "j_username";
    public static final String ACEGI_SECURITY_FORM_PASSWORD_KEY = "j_password";
    public static final String ACEGI_SECURITY_LAST_USERNAME_KEY = "ACEGI_SECURITY_LAST_USERNAME";

    //~ Methods ========================================================================================================

    public Authentication attemptAuthentication(HttpServletRequest request)  throws AuthenticationException {
        
    	String currentUser = obtainUsername(request);
        String currentPassword = obtainPassword(request);

        if (currentUser == null) {
        	currentUser = "";
        }

        if (currentPassword == null) {
        	currentPassword = "";
        }

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(currentUser, currentPassword);
     // Place the last username attempted into HttpSession for views
        request.getSession().setAttribute(ACEGI_SECURITY_LAST_USERNAME_KEY, currentUser);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        
        try{
            List al=signUpService.findUserDetails(currentUser);
            if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
    			User usr=(User)al.get(0);
    			usr.setLogin_status("TryLogin"); 
    			usr.setLogin_time(new Date());
			    userManager.saveUser(usr); 
    					
    					   String algorithm="SHA";
    	            	   String pass1=usr.getPassword();
    	            	   String pass2=StringUtil.encodePassword(currentPassword, algorithm);
    	            	   if(!pass1.equals(pass2)){
    	            		   Date curr=new Date();
    	            			SimpleDateFormat fm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    	            			String dtt=fm.format(curr);
    	            			SimpleDateFormat fm1=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss") ;
    	            			String dtt11=fm1.format(curr);
    	            			String dtt1=fm.format(curr);
    	               			dtt1=dtt1+"  "+"FAILURE";
    	               			dtt11=dtt11+"  "+"Failed";
    	               			dtt11=dtt11+"      "+"Incorrect Password";
    	            		   userManager.updateUserLoginFailureReason(currentUser,dtt11,dtt1);
    	            		   
    	            	   }else if(pass1.equals(pass2)){            		   
    	            		   	Date curr=new Date();
    	           				SimpleDateFormat fm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    	           				String dtt=fm.format(curr);
    	               			Date currr=new Date();
    	               			SimpleDateFormat fm11=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss") ;
    	               			String dttright1=fm11.format(currr);
    	               			String dtt2=fm.format(currr);
    	               			if((usr.isAccountLocked())){
    	               				String dtt3=dttright1;
    	               				String dtt4=dtt2;
    	               				dtt4=dtt4+"  "+"FAILURE";
    	               				dtt3=dtt3+"  "+"Failed";
    	                			dtt3=dtt3+"      "+"Account Locked";
    	                			userManager.updateUserLoginFailureReason(currentUser,dtt3,dtt4);
    	               				
    	               			}else if((usr.isAccountExpired())){
    	               				String dtt3=dttright1;
    	               				String dtt4=dtt2;
    	               				dtt4=dtt4+"  "+"FAILURE";
    	               				dtt3=dtt3+"  "+"Failed";
    	                			dtt3=dtt3+"      "+"Account Expired";
    	                			userManager.updateUserLoginFailureReason(currentUser,dtt3,dtt4);
    	               				
    	               			}else if((usr.isCredentialsExpired())){
    	               				String dtt3=dttright1;
    	               				String dtt4=dtt2;
    	               				dtt4=dtt4+"  "+"FAILURE";
    	               				dtt3=dtt3+"  "+"Failed";
    	                			dtt3=dtt3+"      "+"Password Expired";
    	                			userManager.updateUserLoginFailureReason(currentUser,dtt3,dtt4);
    	               				
    	               			}else{
    	               				dtt=dtt+"  "+"SUCCESS";
    	               				userManager.updateUserLoginFailureReason(currentUser,"",dtt);
    	               				
    	               			}
    	               				
    	            	   }
    	            	   
    				}
            }catch(Exception e){
            	e.printStackTrace();
            }

        return this.getAuthenticationManager().authenticate(authRequest);
    }

    /**
     * This filter by default responds to <code>/j_acegi_security_check</code>.
     *
     * @return the default
     */
    public String getDefaultFilterProcessesUrl() {
        return "/j_acegi_security_check";
    }

    public void init(FilterConfig filterConfig) throws ServletException {}

    protected String obtainPassword(HttpServletRequest request) {
        return request.getParameter(ACEGI_SECURITY_FORM_PASSWORD_KEY);
    }
   
    protected String obtainUsername(HttpServletRequest request) {
        return request.getParameter(ACEGI_SECURITY_FORM_USERNAME_KEY);
    }
    
    protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

	public void setSignUpService(SignUpService signUpService) {
		this.signUpService = signUpService;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}


}
