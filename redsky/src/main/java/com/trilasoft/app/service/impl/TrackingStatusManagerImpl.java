/**
 * Implementation of TrackingStatusManager interface.
 * Set the Dao for communication with the data layer.
 * @param 		dao
 * This class represents the basic "TrackingStatus" object in Redsky that allows for TrackingStatus  of Shipment.
 * @Class Name	TrackingStatusManagerImpl
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */
package com.trilasoft.app.service.impl;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set; 
import org.appfuse.service.impl.GenericManagerImpl; 
import com.trilasoft.app.dao.TrackingStatusDao;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.TrackingStatusManager;
public class TrackingStatusManagerImpl extends GenericManagerImpl<TrackingStatus, Long> implements TrackingStatusManager { 
	TrackingStatusDao trackingStatusDao;
	
    public TrackingStatusManagerImpl(TrackingStatusDao trackingStatusDao) { 
        super(trackingStatusDao); 
        this.trackingStatusDao = trackingStatusDao; 
    } 
    public List<AccountLine> findByBooker(Long id){   
        return trackingStatusDao.findByBooker(id);   
    }
    public List<AccountLine> findByOriginAgent(String shipNumber){   
        return trackingStatusDao.findByOriginAgent(shipNumber);   
    }
	 public List<AccountLine> findByDestinationAgent(String shipNumber){   
	        return trackingStatusDao.findByDestinationAgent(shipNumber);   
	    }
    public List findMaximumId(){
    	return trackingStatusDao.findMaximumId(); 
    }
	
    public List checkById(Long id) { 
        return trackingStatusDao.checkById(id); 
    }
	public List countForStorage(String shipNumber){
		   return trackingStatusDao.countForStorage(shipNumber);
	   }
	public int updateTrack(Long id, String blNumber,String hblNumber){
		return trackingStatusDao.updateTrack(id, blNumber,hblNumber);
	}
	public List findETD(String shipNumber, String etDeparts){
		return trackingStatusDao.findETD(shipNumber,etDeparts);
	}
	
	public List<AccountLine> findByOriginAgentCode(String shipNumber){
		return trackingStatusDao.findByOriginAgentCode(shipNumber);
	}
	public String getAgentTypeRole(String sessionCorpID, Long id){
		return trackingStatusDao.getAgentTypeRole(sessionCorpID,id);
	}
	
	public List<AccountLine> findByDestinationAgentCode(String shipNumber){
		return trackingStatusDao.findByDestinationAgentCode(shipNumber);
	}
	public List<TrackingStatus> findByOriginAgentInTrackingStauts(String shipNumber){
		return trackingStatusDao.findByOriginAgentInTrackingStauts(shipNumber);
	}
	
	 public List<TrackingStatus> findByOriginAgentCodeInTrackingStauts(Long id){
		 return trackingStatusDao.findByOriginAgentCodeInTrackingStauts(id);
	 }
	
    public List<TrackingStatus> findByDestinationAgentInTrackingStauts(String shipNumber){
    	return trackingStatusDao.findByDestinationAgentInTrackingStauts(shipNumber);
    }
   
    public List<TrackingStatus> findByDestinationAgentCodeInTrackingStauts(Long id){
    	return trackingStatusDao.findByDestinationAgentCodeInTrackingStauts(id);
    }
    public List omniReportExtract(String bookCode,String endDates,String companyCode, String corpId,String jobTypeMulitiple) {
    	return trackingStatusDao.omniReportExtract(bookCode, endDates, companyCode, corpId,jobTypeMulitiple);
    }
    public Boolean getdefaultVendorAccLine(String corpID){
    	return trackingStatusDao.getdefaultVendorAccLine(corpID);
    }
    public List findSalesStatus(String seuenceNumber){
    	return trackingStatusDao.findSalesStatus(seuenceNumber);
    }
    public int updateSalesStatus(String seuenceNumber,String userName){
    	return trackingStatusDao.updateSalesStatus(seuenceNumber,userName);
    }
    public List findByoriginSubAgentCode(String originSubAgentCode) {
		
		 return trackingStatusDao.findByoriginSubAgentCode(originSubAgentCode);
	}
	public List findIdByShipNumber(String shipNumber){
		 return trackingStatusDao.findIdByShipNumber(shipNumber);
	}

	public List findVanLineCodeList(String partnerCode, String sessionCorpID) {
		return trackingStatusDao.findVanLineCodeList(partnerCode, sessionCorpID);
	}

	public List findVanLineAgent(String vanLineCode, String sessionCorpID) {
		return trackingStatusDao.findVanLineAgent(vanLineCode, sessionCorpID);
	}

	public boolean checkEmailAlertSent(String forwarderCode, String sessionCorpID) {
		return trackingStatusDao.checkEmailAlertSent(forwarderCode, sessionCorpID);
	}
	public List findBookingAgentCode(String companyCode, String corpID) {
		return trackingStatusDao.findBookingAgentCode(companyCode, corpID);
	}
	public void getOmniReport (String shipNumber, String corpID){
		trackingStatusDao.getOmniReport(shipNumber, corpID);
	}
	public Boolean getIsNetworkBookingAgent(String sessionCorpID,String bookingAgentCode) {
		return trackingStatusDao.getIsNetworkBookingAgent(sessionCorpID, bookingAgentCode);
	}
	public Boolean getIsNetworkAgent(String sessionCorpID, String agentCode) {
		return trackingStatusDao.getIsNetworkAgent(sessionCorpID, agentCode);
	}
	public String getExternalCorpID(String sessionCorpID, String agentCode) {
		return trackingStatusDao.getExternalCorpID(sessionCorpID, agentCode);
	}
	public void updateExternalSo(Long id, String shipNumber) {
		trackingStatusDao.updateExternalSo(id, shipNumber);
	}
	public String getLocalBookingAgentCode(String corpID,String bookingAgentCode) {
		return trackingStatusDao.getLocalBookingAgentCode(corpID, bookingAgentCode);
	}
	public void removeExternalLink(Long customerFileId, String agentExSo, String userName, Long Id, String shipNum, String sessionCorpID, Boolean soNetworkGroup) {
		trackingStatusDao.removeExternalLink(customerFileId, agentExSo,  userName,  Id,  shipNum,  sessionCorpID, soNetworkGroup)	;	
	}
	public String[] getLocalAgent(String agentCorpID, String sessionCorpID,String agentCode) {
		return trackingStatusDao.getLocalAgent(agentCorpID, sessionCorpID, agentCode);
	}
	public String getCompanyDivision(String bookingAgentCode,String sessionCorpID, String externalCorpID) {
		return trackingStatusDao.getCompanyDivision(bookingAgentCode, sessionCorpID, externalCorpID);
	}
	public List<TrackingStatus> getNetworkedTrackingStatus(String sequenceNumber) {
		return trackingStatusDao.getNetworkedTrackingStatus(sequenceNumber);
	}
	public Boolean checkNetworkSection(String bookingAgentShipNumber, String section) {
		return trackingStatusDao.checkNetworkSection(bookingAgentShipNumber, section);
	}
	
	public List downloadXMLFile(String sessionCorpID,String fileNumber){
		return trackingStatusDao.downloadXMLFile(sessionCorpID, fileNumber);
		
	}
	public List getCountryCode(String fileNumber){
		return trackingStatusDao.getCountryCode(fileNumber);
	}
	public List omniReportExtractByOmni(String bookCode,String endDates,String companyCode,String corpId,String jobTypeMulitiple){
	    return trackingStatusDao.omniReportExtractByOmni(bookCode, endDates, companyCode, corpId,jobTypeMulitiple);  
	}
	public boolean getContractType(String sessionCorpID, String contract) {
		return trackingStatusDao.getContractType(sessionCorpID, contract);
	}
	public boolean getCMMContractType(String sessionCorpID, String contract) {
		return trackingStatusDao.getCMMContractType(sessionCorpID, contract);
	}
	public boolean getDMMContractType(String sessionCorpID, String contract) {
		return trackingStatusDao.getDMMContractType(sessionCorpID, contract);
	}
	public String[] findParentBillTo(String billToCode, String sessionCorpID) {
		return trackingStatusDao.findParentBillTo(billToCode, sessionCorpID);
	}
	public List getIsNetworkAgentCorpId(String sessionCorpID, String agentCode) {
		return trackingStatusDao.getIsNetworkAgentCorpId(sessionCorpID, agentCode);
	}
	public String getAgentTypeRoleForm(String sessionCorpID, Long id, String bookingAgentCode, String networkPartnerCode, String originAgentCode, String destinationAgentCode, String originSubAgentCode, String destinationSubAgentCode) {
		return trackingStatusDao.getAgentTypeRoleForm(sessionCorpID, id, bookingAgentCode, networkPartnerCode, originAgentCode, destinationAgentCode, originSubAgentCode, destinationSubAgentCode);
	}
	public void updateDMMAgentBilltocode(String sessionCorpID, Long id, String networkPartnerCode, String networkPartnerName) {
		 trackingStatusDao.updateDMMAgentBilltocode(sessionCorpID, id, networkPartnerCode, networkPartnerName);
	}
	public List getAllBookingAgentList(String sessionCorpID) {
		return trackingStatusDao.getAllBookingAgentList(sessionCorpID);
	}
	
	public String checkIsRedSkyUser(String partnerCode) {
		return trackingStatusDao.checkIsRedSkyUser(partnerCode);
	}
	public boolean getAgentsExistInRedSky(String partnerCode, String sessionCorpID,String name, String emailId) {
		return trackingStatusDao.getAgentsExistInRedSky( partnerCode,sessionCorpID,name,emailId);
	}
	public List<TrackingStatus> getCurrentDetail(Long soid) {
		return trackingStatusDao.getCurrentDetail(soid);
	}
	public void updateNetworkflagDSP(Long id) {
		trackingStatusDao.updateNetworkflagDSP(id);		
	}
	public String findMoveTypePartnerValue(String sessionCorpID, String partnerCode){
		return trackingStatusDao.findMoveTypePartnerValue(sessionCorpID,partnerCode);
	}
	public List findDocumentFromMyFileCF(String seqNumber,String fieldValue,String sessionCorpID){
		return trackingStatusDao.findDocumentFromMyFileCF(seqNumber,fieldValue,sessionCorpID);
	}
	public List findDocumentFromMyFileSO(String seqNumber,String shpNumber,String fieldValue,String sessionCorpID){
		return trackingStatusDao.findDocumentFromMyFileSO(seqNumber,shpNumber,fieldValue,sessionCorpID);
	}
	/*public void updateSOExtFalg(Long id){
		trackingStatusDao.updateSOExtFalg(id);
	}*/
	public Map<Long, TrackingStatus> getTrackingStatusObjectMap( Set serviceorderIdSet) {
		return trackingStatusDao.getTrackingStatusObjectMap(serviceorderIdSet);
	}
	public List getvenderCodeDetais(String networkVendorCode){
		return trackingStatusDao.getvenderCodeDetais(networkVendorCode);
	}
	public List getSOListbyCF(Long cid,Long id){
		 return trackingStatusDao.getSOListbyCF(cid,id);
	 }
	public void updateTrackingStatusAgentDetails(StringBuffer query){
		trackingStatusDao.updateTrackingStatusAgentDetails(query);
	}
	public Map<String,String> findByAgent(String shipNumber,String type){
		return trackingStatusDao.findByAgent(shipNumber, type);
	}
	public boolean getCMMContractTypeForOtherCorpid(String sessionCorpID, String contract) {
		return trackingStatusDao.getCMMContractTypeForOtherCorpid(sessionCorpID, contract);
	}
	public boolean getDMMContractTypeForOtherCorpid(String sessionCorpID, String contract) {
		return trackingStatusDao.getDMMContractTypeForOtherCorpid(sessionCorpID, contract);
	}
	
	public String checkNetworkCoordinatorInUser(String cfCoordinatorname, String soCoordinatorname, String sessionCorpID){
		return trackingStatusDao.checkNetworkCoordinatorInUser(cfCoordinatorname,soCoordinatorname,sessionCorpID);
	}
	public String getRddDaysFromPartner(String fieldName,String soBillToCode,String sessionCorpID){
		return trackingStatusDao.getRddDaysFromPartner(fieldName,soBillToCode,sessionCorpID);
	}
	public List validatePreferredAgentCode(String partnerCode,String corpid, String billToCode)
	   {
		   return trackingStatusDao.validatePreferredAgentCode(partnerCode,corpid, billToCode) ;
	   }
	
	public void updateLinkedShipNumberforCompletedDate(Set linkedshipNumber,Date serviceCompleteDate,String corpID,String userName){
		trackingStatusDao.updateLinkedShipNumberforCompletedDate(linkedshipNumber,serviceCompleteDate,corpID,userName);
	}
	public List getEmailFromPortalUser(String partnerCode,String corpid)
	   {
		   return trackingStatusDao.getEmailFromPortalUser(partnerCode,corpid) ;
	   }
	 public  String loadInfoByCode(String originCode,String corpId) 
	 { 
		 return trackingStatusDao.loadInfoByCode(originCode, corpId) ;
	 }
	public String checkAllInvoicedLinkedAccountLines(String shipnumber,String linkedshipnumberTS, String contractType) {
		 return trackingStatusDao.checkAllInvoicedLinkedAccountLines(shipnumber,linkedshipnumberTS, contractType);
	}
	
	

} 


