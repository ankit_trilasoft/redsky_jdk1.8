package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.EntitlementManager;
import com.trilasoft.app.service.PartnerPublicManager;

public class EntitlementAction extends BaseAction implements Preparable{
	
	private Long id;
	private String sessionCorpID;
	private Entitlement entitlement;
	private EntitlementManager entitlementManager;
	private Long partnerPrivateId;
	private Long customerFileId;
	private String partnerCode;
	private String eOption;
	
	private String partnerPrivateURL;
	private List OptionList;
	private String btnType;
	private String checkDup;
    private PartnerPublic partnerPublic;
	
	private PartnerPublicManager partnerPublicManager;
	
	public void prepare() throws Exception { 
		
	}
	
	public EntitlementAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	
	public String edit() {
		if (id != null) {
			entitlement = entitlementManager.get(id);
		} else {
			entitlement = new Entitlement(); 
			entitlement.setCreatedOn(new Date());
			entitlement.setUpdatedOn(new Date());
			entitlement.setPartnerPrivateId(partnerPrivateId);
			entitlement.setPartnerCode(partnerCode);
		} 
		return SUCCESS;
	}
	public String save() throws Exception {
		/*OptionList = entitlementManager.checkOptionDuplicate(entitlement.getPartnerCode(),sessionCorpID,entitlement.geteOption());
		if(OptionList.size() > 0){
			partnerPrivateURL = "?partnerCode="+entitlement.getPartnerCode()+"&partnerPrivateId="+entitlement.getPartnerPrivateId()+"&checkDup=YES";
			return SUCCESS;
		}else{*/
		try{
			partnerPrivateURL = "?checkDup=NO";
			boolean isNew = (entitlement.getId() == null);  
				if (isNew) { 
					entitlement.setCreatedOn(new Date());
		        } 
				partnerPublic = partnerPublicManager.get(new Long(entitlement.getPartnerPrivateId()));
				if(partnerPublic.getPartnerType()!=null && (!(partnerPublic.getPartnerType().toString().trim().equals("")))  && partnerPublic.getStatus().equalsIgnoreCase("Approved") && (partnerPublic.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic.getPartnerType().equalsIgnoreCase("DMM"))){
				entitlement.setCorpID("TSFT");	
				}else{
				entitlement.setCorpID(sessionCorpID);
				}
				entitlement.setUpdatedOn(new Date());
				entitlement.setUpdatedBy(getRequest().getRemoteUser()); 
				entitlementManager.save(entitlement);  
			    String key = (isNew) ?"Entitlement details have been saved." :"Entitlement details have been saved." ;
			    saveMessage(getText(key)); 
//		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	public String deleteEntitlement() throws Exception {
		entitlementManager.remove(id);
		partnerPrivateURL = "?partnerId="+partnerPrivateId+"&partnerCode="+partnerCode+"&partnerType=AC";
		return SUCCESS;
	}
	
	@SkipValidation
	  public String checkOptionDuplicate(){   
		if(partnerCode!=null && !"".equals(partnerCode.trim())){
	        OptionList = entitlementManager.checkOptionDuplicate(partnerCode,sessionCorpID,eOption);
	        if(OptionList != null && OptionList.size()>0){
	        	checkDup = "YES";
	        }else{
	        	checkDup = "NO";
	        }
		}
		return SUCCESS; 
	  }
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Entitlement getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(Entitlement entitlement) {
		this.entitlement = entitlement;
	}

	public EntitlementManager getEntitlementManager() {
		return entitlementManager;
	}

	public void setEntitlementManager(EntitlementManager entitlementManager) {
		this.entitlementManager = entitlementManager;
	}

	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}
	
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerPrivateURL() {
		return partnerPrivateURL;
	}

	public void setPartnerPrivateURL(String partnerPrivateURL) {
		this.partnerPrivateURL = partnerPrivateURL;
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	
	public String geteOption() {
		return eOption;
	}

	public void seteOption(String eOption) {
		this.eOption = eOption;
	}

	public List getOptionList() {
		return OptionList;
	}

	public void setOptionList(List optionList) {
		OptionList = optionList;
	}

	public String getBtnType() {
		return btnType;
	}

	public void setBtnType(String btnType) {
		this.btnType = btnType;
	}

	public String getCheckDup() {
		return checkDup;
	}

	public void setCheckDup(String checkDup) {
		this.checkDup = checkDup;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}
	
}
