<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	fieldset {
			  padding: 0.0em;	
			  position: relative;
			  margin-right:75em
			  width: 90%;  
			  }

	</style>
	<script language="javascript" type="text/javascript">
		function onLoad() {
			//document.forms['partnerForm'].elements['billingCountryCode'].value=document.forms['partnerForm'].elements['partner.billingCountryCode'].value+' : '+document.forms['partnerForm'].elements['partner.billingCountry'].value;
			//document.forms['partnerForm'].elements['terminalCountryCode'].value=document.forms['partnerForm'].elements['partner.terminalCountryCode'].value+' : '+document.forms['partnerForm'].elements['partner.terminalCountry'].value;
			//document.forms['partnerForm'].elements['mailingCountryCode'].value=document.forms['partnerForm'].elements['partner.mailingCountryCode'].value+' : '+document.forms['partnerForm'].elements['partner.mailingCountry'].value;
			var i;
			for(i=0;i<=80;i++)
			{
					document.forms['partnerForm'].elements[i].disabled = true;
			}
		}
	</script>
	<script type="text/javascript"> 
		function toMail(){ 
			document.forms['partnerForm'].elements['partner.mailingAddress1'].value=document.forms['partnerForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerForm'].elements['partner.mailingAddress2'].value=document.forms['partnerForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerForm'].elements['partner.mailingAddress3'].value=document.forms['partnerForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerForm'].elements['partner.mailingAddress4'].value=document.forms['partnerForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerForm'].elements['partner.mailingEmail'].value=document.forms['partnerForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerForm'].elements['partner.mailingCity'].value=document.forms['partnerForm'].elements['partner.billingCity'].value;		
			document.forms['partnerForm'].elements['partner.mailingFax'].value=document.forms['partnerForm'].elements['partner.billingFax'].value;		
			document.forms['partnerForm'].elements['partner.mailingZip'].value=document.forms['partnerForm'].elements['partner.billingZip'].value;		
			document.forms['partnerForm'].elements['partner.mailingCountry'].value=document.forms['partnerForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerForm'].elements['partner.mailingPhone'].value=document.forms['partnerForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerForm'].elements['partner.mailingTelex'].value=document.forms['partnerForm'].elements['partner.billingTelex'].value;		
			document.forms['partnerForm'].elements['partner.mailingState'].value=document.forms['partnerForm'].elements['partner.billingState'].value;		
			document.forms['partnerForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerForm'].elements['partner.billingCountryCode'].value;		
		}
		function toTerm(){ 
			document.forms['partnerForm'].elements['partner.terminalAddress1'].value=document.forms['partnerForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerForm'].elements['partner.terminalAddress2'].value=document.forms['partnerForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerForm'].elements['partner.terminalAddress3'].value=document.forms['partnerForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerForm'].elements['partner.terminalAddress4'].value=document.forms['partnerForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerForm'].elements['partner.terminalEmail'].value=document.forms['partnerForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerForm'].elements['partner.terminalCity'].value=document.forms['partnerForm'].elements['partner.billingCity'].value;		
			document.forms['partnerForm'].elements['partner.terminalFax'].value=document.forms['partnerForm'].elements['partner.billingFax'].value;		
			document.forms['partnerForm'].elements['partner.terminalZip'].value=document.forms['partnerForm'].elements['partner.billingZip'].value;		
			document.forms['partnerForm'].elements['partner.terminalCountry'].value=document.forms['partnerForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerForm'].elements['partner.terminalPhone'].value=document.forms['partnerForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerForm'].elements['partner.terminalTelex'].value=document.forms['partnerForm'].elements['partner.billingTelex'].value;		
			document.forms['partnerForm'].elements['partner.terminalState'].value=document.forms['partnerForm'].elements['partner.billingState'].value;		
			document.forms['partnerForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerForm'].elements['partner.billingCountryCode'].value;		
		}
	</script>
	<script type="text/javascript">
		function autoPopulate_partner_billingCountry(targetElement) {
		
			var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		function autoPopulate_partner_terminalCountry(targetElement) {
		
			var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
			}
		function autoPopulate_partner_mailingCountry(targetElement) {
			
			var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
		}	
	</script>
	
</head> 

<s:form id="partnerForm" action='${empty param.popup?"savePartner.html":"savePartner.html?decorator=popup&popup=true"}' method="post" validate="true">   
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<s:hidden name="partner.id" value="%{partner.id}"/>  
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partner.partnerCode" value="%{partner.partnerCode}"/>

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<div id="newmnav">
				  <ul>
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"} ><span>List</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Address</span></a></li>
			  		<c:if test="${param.popup}">  
			  		<li><a href="editPartnerDetail.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" ><span>Details</span></a></li>
			  		<li><a href="editPartnerRemarks.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Remarks</span></a></li>
			  		<c:if test="${partnerType == 'AC'}">
			  			<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  			<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li>
			  			<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  		</c:if> 
			  		</c:if>
			  		<c:if test="${empty param.popup}">  
			  		<li><a href="editPartnerDetail.html?id=${partner.id}" ><span>Details</span></a></li>
			  		<li><a href="editPartnerRemarks.html?id=${partner.id}" ><span>Remarks</span></a></li> 
			  		</c:if>
				  </ul>
		</div><div class="spn">&nbsp;</div><br> 
<div id="Layer1">

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr>
									<td align="left" class="listwhitetext">Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
								</tr>
								<tr>
									<s:hidden key="partner.firstName" value=""/>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text"
									size="79" maxlength="80"  onkeydown="return onlyCharsAllowed(event)"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" size="1" required="true"
									cssClass="input-text" maxlength="10" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
									required="true" cssClass="input-textUpper" maxlength="8" size="15" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
								</tr>
							</tbody>
						</table>
				<fieldset>
				<LEGEND ACCESSKEY=B>Billing Address</LEGEND>
					<table>
						<tbody>
							<tr>
								<td align="left" class="listwhitetext" width="40px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress1"  size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingCity" size="6" maxlength="20" onkeydown="return onlyCharsAllowed(event)"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingFax" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this);"/></td>
							</tr>
							<tr>
								<td align="left" class="listwhitetext" width="40px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress2" size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingState'/></td>
								<td><s:select name="partner.billingState" list="%{bstates}" cssStyle="width:65px" headerKey="" headerValue=": Leave Empty"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingPhone" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
							</tr>
							<tr>
								<td align="left" class="listwhitetext" width="40px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress3" size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingZip" size="6" maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingTelex'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingTelex" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
							</tr>
							<tr>
								<td align="left" class="listwhitetext" width="40px"></td>
								<td><s:textfield cssClass="input-text" name="partner.billingAddress4" size="40" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/></td>
								<td colspan="2"><s:select cssClass="list-menu" name="partner.billingCountry" list="%{country}" cssStyle="width:102px"  onchange="autoPopulate_partner_billingCountry(this);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingEmail"  size="20" maxlength="65"/></td>
							</tr>
						</tbody>
					</table>
				</fieldset>
				<fieldset>
				<LEGEND ACCESSKEY=T>Terminal Address</LEGEND>
					<table>
						<tbody>
							
							<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress1"  size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalCity" size="6" maxlength="15" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalFax" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
							</tr>
							<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress2" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState'/></td>
									<td><s:select name="partner.terminalState" list="%{tstates}" cssStyle="width:65px" headerKey="" headerValue=": Leave Empty" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalPhone" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
							</tr>
							<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress3" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalZip" size="6" maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalTelex" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
							</tr>
							<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalAddress4" size="40" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode'/></td>
									<td colspan="2"><s:select cssClass="list-menu" name="partner.terminalCountry" list="%{country}" cssStyle="width:102px"  onchange="autoPopulate_partner_terminalCountry(this);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalEmail"  size="20"  maxlength="65"/></td>
									
							</tr>
						</tbody>
					</table>
				</fieldset>
				<fieldset>
				<LEGEND ACCESSKEY=M>Mailing Address</LEGEND>
					<table>
						<tbody>
								
								<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress1"  size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingCity" size="6" maxlength="15" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingFax" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
								</tr>
								<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress2" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState'/></td>
									<td><s:select name="partner.mailingState" list="%{mstates}" cssStyle="width:65px" headerKey="" headerValue=": Leave Empty" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingPhone" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
								</tr>
								<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress3" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingZip" size="6" maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingTelex" maxlength="20" size="20" onkeydown="return onlyNumsAllowed(event)" onchange="return validatePhone(this)"/></td>
								</tr>
								<tr>
									<td align="left" class="listwhitetext" width="40px"></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingAddress4" size="40" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode'/></td>
									<td colspan="2"><s:select cssClass="list-menu" name="partner.mailingCountry" list="%{country}" cssStyle="width:102px" onchange="autoPopulate_partner_mailingCountry(this);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="20" maxlength="65"/></td>
									
								</tr>
						</tbody>
					</table>
				</fieldset>
			</td>
		</tr>
	
	</tbody>
</table>
</div>
<table style="width:700px">
<tr>
<td align="center">
<table>
<tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdOn'/></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedOn'/></td>
						<s:hidden name="partner.updatedOn"/>
						<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.updatedBy"/>
								<td><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
					</tr>
</tbody>
</table>  



</td>
</tr>
</table>
<s:hidden key="partner.isAccount"/>
<s:hidden key="partner.isAgent"/>
<s:hidden key="partner.isBroker"/>
<s:hidden key="partner.isVendor"/>
<s:hidden key="partner.isCarrier"/>
<s:hidden key="partner.isDriver"/>
<s:hidden key="partner.qc"/>
<s:hidden key="partner.doNotUse"  />
<s:hidden key="partner.lastUpdate"  />
<s:hidden key="partner.updateBy"  />
<s:hidden key="partner.rank" />
<s:hidden key="partner.opened"  />
<s:hidden key="partner.openBy"  />
<s:hidden key="partner.abbreviation"  />
<s:hidden key="partner.warehouse" />
<s:hidden key="partner.zone"  />
<s:hidden key="partner.billToGroup"/>
<s:hidden key="partner.invoicePercentage"  />
<s:hidden key="partner.coordinator" />
<s:hidden key="partner.billPayType"/>
<s:hidden key="partner.billPayOption"/>
<s:hidden key="partner.salesMan"/>
<s:hidden key="partner.scheduleCode" />
<s:hidden key="partner.billingInstructionCode"/>
<s:hidden key="partner.commissionType"/>
<s:hidden key="partner.effectiveDate" />
<s:hidden key="partner.billingInstruction" />
<s:hidden key="partner.driverAgency"  />
<s:hidden key="partner.shortPercentage"  />
<s:hidden key="partner.longPercentage"  />
<s:hidden key="partner.localPercentage"  />
<s:hidden key="partner.client4"/>
<s:hidden key="partner.validNationalCode"/>
<s:hidden key="partner.client5" />
<s:hidden key="partner.idCode"/>
<s:hidden key="partner.client6"/>

<s:hidden key="partner.client7"/>
<s:hidden key="partner.client1"/>
<s:hidden key="partner.client8"/>
<s:hidden key="partner.client2" />
<s:hidden key="partner.client9"/>
<s:hidden key="partner.client3" />
<s:hidden key="partner.client10"/>
<s:hidden key="partner.perDiemFreeDays"  />
<s:hidden key="partner.perDiemCost1"  />
<s:hidden key="partner.perDiemCost2"  />
<s:hidden key="partner.remarks1"  />
<s:hidden key="partner.remarks2"  />
<s:hidden key="partner.remarks3"  />
<s:hidden key="partner.remarks4"  />
<s:hidden key="partner.remarks5"  />

<c:if test="${ empty param.popup}" >
          
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="save" key="button.save"/>   
        <c:if test="${not empty partner.id}">    
            <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="delete" key="button.delete" onclick="return confirmDelete('partner')"/>   
        </c:if>   
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px "  method="cancel" key="button.cancel"/>   

</c:if> 

</s:form>  

<script type="text/javascript">   
try{
<c:if test="${not empty param.popup}" >
	for(i=0;i<=80;i++)
			{
					document.forms['partnerForm'].elements[i].disabled = true;
			}
</c:if>
}
catch(e){}
	Form.focusFirstElement($("partnerForm"));   
</script>  
