package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ScrachCard;

public interface ScrachCardManager extends GenericManager<ScrachCard, Long> {

	public List getScrachCardByUser(String userName);
	public List getSchedulerScrachCardByUser(String userName ,Date workDate);
	public List getSchedulerScrachCardByDate(Date workDate);
}
