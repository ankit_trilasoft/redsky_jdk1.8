<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Attached Document To Work Ticket</title>
<meta name="heading" content="Attached Document To Work Ticket'" />
<script type="text/JavaScript">
function closeForm(){
	self.close();
}
function getDocumentIdList(){
	var idList="";
	<c:forEach var="myFile" items="${myFileList}">
		var controlObj = document.getElementById('${myFile.id}');
		if(controlObj.checked){
			if(idList==''){
				idList = '${myFile.id}';
			}else{
				idList = idList+ ',${myFile.id}';
			}
		}
	</c:forEach>
	var url = "attchedSelectedDoc.html?id=${workTicket.id}&sid=${serviceOrder.id}&myFileListId="+idList;
	window.location.assign(url);
}
window.onload = function() {	
								if('${temp}'=='load'){
									window.opener.location.reload();
									self.close();
								}
							}
</script>
</head>
	<display:table name="myFileList" class="table-fc"  id="myFileList" defaultsort="3" pagesize="50" style="width:100%;" > 
		<display:column title="" style="width:2px;">
			<c:if test="${fn:contains(workTicket.attachedFile, myFileList.id)}">
  					 <input type="checkbox" id="${myFileList.id}" name="${myFileList.id}" value="${myFileList.id}"  checked = "checked" />
			</c:if>
			<c:if test="${not fn:contains(workTicket.attachedFile, myFileList.id)}">
  					 <input type="checkbox" id="${myFileList.id}" name="${myFileList.id}" value="${myFileList.id}"   />
			</c:if>
		</display:column>
		<display:column title="#" style="width:2px;">
			<c:set var="orNum" value="${fn:substring(myFileList.fileId,11,14)}" />
			<c:if test="${orNum == ''}"> <c:out value="00"/> </c:if>
			<c:if test="${orNum != ''}"> <c:out value="${orNum}"/> </c:if>
		</display:column>
	    <display:column property="fileType"  maxLength="100" title="Document&nbsp;Type" style="width:100px;"/>
		<display:column property="description"  title="Description" />
	</display:table>
	<div><input type="button" name="Confirm Attachment" style="width:150px;" value="Confirm Attachment" class="cssbutton" onclick="getDocumentIdList()" />
	&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="Close" style="width:50px;" value="Close" class="cssbutton" onclick="closeForm();" /></div>
