<%@ include file="/common/taglibs.jsp"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@ include file="/common/tooltip.jsp"%>
<title><fmt:message key="activeWorkTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='activeWorkTicketList.heading'/>" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#overlayPopUp { filter:alpha(opacity=70); -moz-opacity:0.7; 
-khtml-opacity: 0.7; opacity: 0.7; 
position:fixed; width:100%; height:100%;left:0px;top:0px; 
z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>

<% 
	Date date11 = new Date();
	SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
	String dt11=sdfDestination.format(date11);
%>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>

<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</head> 
<s:form id="activeWorkTicketList" name="activeWorkTicketList" action="generateInvoiveForServiceOrder.html?ajax=1&btntype=yes&decorator=popup&popup=true" onsubmit="return checkCreditInvoce()" method="post">
<div id="overlayPopUp">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr> <td height="200px"></td> </tr>
<tr>
    <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
     <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Invoice Generation is in process. Please don't refresh the page.</font>
    </td>
  </tr>
    <tr>
   <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
    <img src="<c:url value='/images/ajax-loader.gif'/>" />       
     </td>
   </tr>
 </table>
 </td>
</tr>
</table>  
</div>   
</div>
<s:hidden name="serviceOrder.bookingAgentCheckedBy" />
<s:hidden name="workTiketReviewFlag" id="workTiketReviewFlag" value="false"/>
<div id="bookingAgent" style="align:center; margin-bottom:20px;" >
<table style="width:100%" border="0"><tr>
<table border="0" class="mainDetailTable" cellpadding="0" cellspacing="0"style="width:100%;margin:0px; border:2px solid #3DAFCB;"> 
	<tr>
	<td>
	<table  border="0" align="left"  cellpadding="0" cellspacing="0" class="detailTabLabel"  style="margin:0px;padding:10px;width:100%;">
	<tr>
	<td align="left" colspan="2" class="listwhitetext"><b>This Service Order will be Processed for the following information: </b></td>  
	</tr>
		<tr><td height="10px"></td></tr>
    <tr>
    <td align="left" class="listwhitetext" style="width:100%;"><b>The Company Division for this Service Order is : </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left;padding-left: 3px;" name="serviceOrder.companyDivision" size="37" tabindex="" readonly="true"/>
     </td>
    </tr>
 
	<tr><td height="10px"></td></tr>
    <tr>
    <td align="left" class="listwhitetext"><b>The Invoice Lines Company Division is/are:   </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left; margin-left:28px; padding-left: 3px;" value="${accCompanyDivision} "size="37" tabindex="" readonly="true"/>
     </td>
    </tr><tr><td height="10px"></td></tr>
    <tr>
    <td align="left" class="listwhitetext"><b>The Booking Code for this Service Order is :  </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left;margin-left:21px; padding-left: 3px;" name="serviceOrder.bookingAgentCode" size="37" tabindex="" readonly="true"/>
    <img align="top" class="openpopup" width="17" height="20" title="View details" onclick="viewPartnerDetails(this);" src="<c:url value='/images/address2.png'/>" />&nbsp;
	</td>
	<tr><td height="10px"></td></tr>
    <td align="left" class="listwhitetext"><b>The Booking Agent name is :    </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left;margin-left:105px; padding-left: 3px;" name="serviceOrder.bookingAgentName"  size="37" tabindex="" readonly="true"/>
   </td>
	</tr>
<tr><td height="10px"></td></tr>
    <tr>
    <td align="left" class="listwhitetext" colspan="4"><b>The invoice lines Bill To Name is : </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left;margin-left:82px; padding-left: 3px;" name="billToNameForDetails" size="37" tabindex="" readonly="true"/>
    <img align="top" class="openpopup" width="17" height="20" title="View details" onclick="viewPartnerDetailsForBillToCode(this);" src="<c:url value='/images/address2.png'/>" />&nbsp;
    </td>
    </tr>
    
    <tr><td height="10px"></td></tr>
    <tr>
    <td align="left" class="listwhitetext" colspan="4"><b>The invoice lines Bill To Code is: </b>  
    <s:textfield cssClass="input-textUpper" cssStyle="text-align:left;margin-right:23px;margin-left:87px; width:205px; padding-left: 3px;" name="billToCodeForTicketView" value="${billToCodeForTicket}" size="42" tabindex="" readonly="true"/></td>
    </tr>
     <c:if test="${checkFieldVisibilityOnGenerateInvoice ==true}">
	<tr><td height="10px"></td></tr>
    <tr><td height="10px"></td></tr> 
    <c:forEach var="reportParameter" items="${reportParameters}">
	<tr>

       <td width="110px" align="left"  class="listwhitetext"> <b><c:out value="${reportParameter.name}"/> : </b> 
		<s:hidden cssClass="input-textUpper"  name="reportParameterName" value="${reportParameter.name}"  tabindex="" />
	 		<s:select cssClass="list-menu" name="reportParameterValue" list="%{inputParameters}" value="%{printOptionsValue}" headerKey="" headerValue="" cssStyle="width:212px;margin-left:185px;" >
	 		</s:select>
		
		</td>
		
	 	
	 	<!-- <td width="130px" align="left"  class="listwhitetext">
	 		<s:hidden cssClass="input-textUpper"  name="reportParameterName" value="${reportParameter.name}"  tabindex="" />
	 		<s:select cssClass="list-menu" name="reportParameterValue" list="%{inputParameters}" value="%{printOptionsValue}" headerKey="" headerValue="" cssStyle="width:150px" >
	 		</s:select>
	 	</td> -->
	</tr>
	</c:forEach>
	</c:if> 
	 <tr><td height="10px"></td></tr> 
	</table> 
	</td></tr>
	
	<tr>
	<td colspan="15">
	<table  border="0" align="left"  cellpadding="0" cellspacing="0" style="margin:0px;">
	<configByCorp:fieldVisibility componentId="component.generateInvoice.showReciprocitySection">
	<tr>
	<td colspan="2" id="bookingCreatedReciprocity">
	<table  border="0" align="left"  cellpadding="0" cellspacing="3" class="detailTabLabel"  style="margin:0px;">
	<tr>
	<td align="right" class="listwhitetext" width="77">Routing</td>
	<td><s:select cssClass="list-menu" name="serviceOrder.routing" list="%{routing}" cssStyle="width:60px"  /></td>
	<td align="right" class="listwhitetext" >Mode</td>
	<td><s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}" cssStyle="width:60px"  /></td>
	<c:if test="${serviceOrder.job !='RLO'}">
	<td align="right" class="listwhitetext" >Service</td>
	<td><s:select cssClass="list-menu" name="serviceOrder.serviceType" list="%{service}" cssStyle="width:100px" /></td>
	</c:if>
	<c:if test="${serviceOrder.job =='RLO'}">
	<td></td>
	<td></td>
	</c:if>
	</tr> 
	<tr><td height="10px"></td></tr>
	<tr> 
	<td align="right" class="listwhitetext">Equipment</td>
	<td><s:select cssClass="list-menu" name="miscellaneous.equipment" list="%{EQUIP}" cssStyle="width:60px" headerKey="" headerValue=" "/></td>
	<td align="right" class="listwhitetext">Weight</td>
    <td align="left" >
    <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
          <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/>
     </c:if>
    <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
        <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/>
    </c:if>
    <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
         <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualGrossWeight" value="%{miscellaneous.actualNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/>
    </c:if>
    <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
        <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/>
    </c:if>
                                           
    <s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
    
    <td align="right" class="listwhitetext">Volume</td>
    <td align="left" >
    <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
       <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" size="6" required="true" maxlength="10" readonly="true"/>
    </c:if>
    <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
       <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualCubicFeet" value="%{miscellaneous.actualCubicMtr}" size="6" required="true" maxlength="10" readonly="true"/>
    </c:if>
    <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
         <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualCubicFeet" value="%{miscellaneous.netActualCubicMtr}" size="6" maxlength="10" required="true" readonly="true"/>
    </c:if>
    <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
         <s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" id="actualCubicFeet" value="%{miscellaneous.netActualCubicFeet}" size="6" maxlength="10" required="true" readonly="true"/>
    </c:if>
      <s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true"/></td>
    </tr>
    </table>
	</td>
	</tr> 
    <tr><td height="10px"></td></tr>
    <tr>
	<td colspan="2" id="bookingCreatedReciprocity1">
	<table  border="0" align="left"  cellpadding="0" cellspacing="3" class="detailTabLabel"  style="margin:0px;  ">
    <tr>
    <td align="right" class="listwhitetext" width="152">Origin&nbsp;Given</td>
    <td><s:textfield cssClass="input-text" id="trackingOriginGivenCode" key="trackingStatus.originGivenCode" cssStyle="width:45px;" maxlength="8"  onchange="valid(this,'special');checkOriginGiven();"  tabindex="" /></td>
	<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenOriginGiven(); document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
	<td><s:textfield cssStyle="width:145px"  cssClass="input-textUpper" id="trackingOriginGivenName" key="trackingStatus.originGivenName" readonly="true" onkeyup="findPartnerDetails('trackingOriginGivenName','trackingOriginGivenCode','trackingOriginOriginGivenDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);"required="true" tabindex="" />
	<div id="trackingOriginOriginGivenDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	</td>
	<td align="right" class="listwhitetext">Destination&nbsp;Given</td>
	<td><s:textfield cssClass="input-text" id="trackingDestinationGivenCode" key="trackingStatus.destinationGivenCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');checkDestinationGiven();" tabindex="" /></td>
	<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenDestinationGiven(); document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
	<td><s:textfield cssStyle="width:145px"  cssClass="input-textUpper" id="trackingdestinationGivenName" key="trackingStatus.destinationGivenName" readonly="true" onkeyup="findPartnerDetails('trackingdestinationGivenName','trackingDestinationGivenCode','trackingOriginDestinationGivenDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" required="true" tabindex="" />
	<div id="trackingOriginDestinationGivenDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	</td>
    </tr>
	<tr><td height="10px"></td></tr>
	<tr>
	<td align="right" class="listwhitetext">Origin&nbsp;Received</td>
	<td><s:textfield cssClass="input-text" id="trackingOriginReceivedCode" key="trackingStatus.originReceivedCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');checkOriginReceived();"  tabindex="" /></td>
	<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenOriginReceived(); document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
	<td><s:textfield cssStyle="width:145px"  cssClass="input-textUpper" id="trackingOriginReceivedName" key="trackingStatus.originReceivedName" readonly="true" onkeyup="findPartnerDetails('trackingOriginReceivedName','trackingOriginReceivedCode','trackingOriginOriginReceivedDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" onchange="checkOriginReceived();" onblur="checkOriginReceived();" required="true" tabindex="" />
	<div id="trackingOriginOriginReceivedDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	</td>
	<td align="right" class="listwhitetext">Destination&nbsp;Received</td>
	<td><s:textfield cssClass="input-text" id="trackingDestinationReceivedCode" key="trackingStatus.destinationReceivedCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special'); checkDestinationReceived();"   tabindex="" /></td>
	<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenDestinationReceived(); document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
	<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationReceivedName" key="trackingStatus.destinationReceivedName" readonly="true" onkeyup="findPartnerDetails('trackingDestinationReceivedName','trackingDestinationReceivedCode','trackingOriginDestinationReceivedDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);"required="true" tabindex="" />
	<div id="trackingOriginDestinationReceivedDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	</td>
	</tr>
	</table>
	</td>
	</tr> 
	</configByCorp:fieldVisibility>
	</table>
	</td>
	</tr>
    <tr>
	<td>
	<table  border="0" align="left"  cellpadding="1" cellspacing="1" class="detailTabLabel"  >
	<tr><td height="5px"></td></tr>
	<tr><td><div id="bookingCreated" style="align:center" ><table><tr>
	<td align="left" class="listwhitetext" width="360px"><b>Is the above information correct?  </b>
	 <input type="button" class="cssbutton1"  value="Yes" name="yes" onclick="cheackBookingAgent();"/>  
     <input type="button" class="cssbutton1" value="No" onclick="companyMessage();"/>   
     </td>
	</tr></td></table></div></td></tr>
	</table>
	</td>
	</tr> 
	</table></td></tr></table></div>
<div id="totalhid" style="align:center">
<s:hidden name="shipNumberForTickets" value="<%=request.getParameter("shipNumberForTickets")%>"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="buttonType" value="<%=request.getParameter("buttonType")%>"/> 
<s:hidden name="assignInvoiceButton" value="<%=request.getParameter("assignInvoiceButton")%>"/> 

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="userUncheck"/>
<s:hidden name="userCheck"/> 
<s:hidden name="billToCodeForTicket"value="${billToCodeForTicket}"/>

<s:hidden name="companyDivisionForTicket"value="${companyDivisionForTicket}"/>
 
<s:hidden name="assignInvoiceNumber"/>
<s:hidden name="sumActualRevenue"value="${sumActualRevenue}"/> 
<s:hidden name="jobtypeForTicket"value="${jobtypeForTicket}"/>
<s:hidden name="accountInterface" value="${accountInterface}"/>
<s:hidden  name="recAccDateTo" value="${recAccDateTo}"/>
<s:hidden name="recAccDateForm"/> 
 <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
<s:hidden name="recInvoiceDateToFormat1"/>
</configByCorp:fieldVisibility>
<s:hidden name="workTicketsListSize" value="${workTicketsListSize}"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" /> 
<s:hidden name="authorizationStatus" /> 

<c:if test="${workTickets!='[]'}"> 
<div style=" width:100%; overflow-x:auto;">
<div><img style="cursor: default; padding-left:160px;margin-bottom:-22px" src="${pageContext.request.contextPath}/images/key_wt_new.jpg"  />
 </div>
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Work Ticket List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk" style="margin-bottom:-18px;">&nbsp;</div>

<s:set name="workTickets" value="workTickets" scope="request" /> 
<display:table name="workTickets" class="table" requestURI="" id="workTicketList" export="true" defaultsort="4" pagesize="1000"  >
		<display:column style="width:5px" titleKey="workTicket.targetActual"><c:if test="${workTicketList.targetActual == 'T'}"><img id="target" src="${pageContext.request.contextPath}/images/target.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'A'}"><img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'C'}"><img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if></display:column>
		<display:column sortable="true" titleKey="workTicket.ticket"><a href="editWorkTicketUpdate.html?id=${workTicketList.id}" target="_blank" ><c:out value="${workTicketList.ticket}"></c:out></a></display:column>   
		<display:column property="shipNumber" sortable="true" titleKey="workTicket.shipNumber"  />
		<display:column property="registrationNumber" sortable="true" style="width:110px" titleKey="workTicket.registrationNumber" />
		<c:if test="${serviceOrder.job=='OFF'}">
		<display:column property="authorizationNumber" sortable="true" title="PO#" />
		</c:if>
		<display:column property="lastName" sortable="true" titleKey="workTicket.lastName" maxLength="12"/>
		<display:column property="firstName" sortable="true" titleKey="workTicket.firstName" />
		<display:column property="jobType" sortable="true" titleKey="workTicket.jobType" />
		<display:column property="jobMode" sortable="true" titleKey="workTicket.jobMode" />
		<%-- <display:column property="city" sortable="true" titleKey="workTicket.city" />
		<display:column property="destinationCity" sortable="true" titleKey="workTicket.destinationCity" />--%>
		<display:column property="instructions" sortable="true" titleKey="workTicket.instructions"  maxLength="11" style="width:110px"/>
		<display:column property="service" sortable="true" titleKey="workTicket.service" />
		<display:column property="date1" sortable="true" titleKey="workTicket.date1" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="date2" sortable="true" titleKey="workTicket.date2" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="warehouse" sortable="true" style="width:5px" titleKey="workTicket.warehouse" />
		<display:column  title="Bill&nbsp;Cleared">
		<c:if test="${not empty workTicketList.invoiceNumber && workTicketList.invoiceNumber!=''}">
		<c:if test="${workTicketList.reviewStatus=='Billed'}">
       	<input type="checkbox" style="margin-left:35px;" checked="checked" name="checkV" id="${workTicketList.id}" disabled="disabled"/>
	   	</c:if>
       	<c:if test="${workTicketList.reviewStatus=='UnBilled' || workTicketList.reviewStatus== null || workTicketList.reviewStatus== ''}">
       	<input type="checkbox"  style="margin-left:35px;" name="checkV" id="${workTicketList.id}" disabled="disabled" />
       	</c:if>
		</c:if>
		<c:if test="${empty workTicketList.invoiceNumber || workTicketList.invoiceNumber==''}">
		<c:if test="${workTicketList.reviewStatus=='Billed'}">
       	<input type="checkbox" style="margin-left:35px;" checked="checked" name="checkV" id="${workTicketList.id}" onclick="userStatusUncheck(${workTicketList.id},this)" />
	   	</c:if>
       	<c:if test="${workTicketList.reviewStatus=='UnBilled' || workTicketList.reviewStatus== null || workTicketList.reviewStatus== ''}">
       	<input type="checkbox"  style="margin-left:35px;" name="checkV" id="${workTicketList.id}" onclick="userStatusCheck(${workTicketList.id},this)" />
       	</c:if>
       	</c:if>
		
		</display:column> 
		<display:column property="reviewStatusDate" sortable="true" style="width:5px" title="Cleared&nbsp;Date" format="{0,date,dd-MMM-yyyy}"/>
		<display:setProperty name="paging.banner.item_name" value="workticket" />
		<display:setProperty name="paging.banner.items_name" value="workTickets" />
		
		<display:setProperty name="export.excel.filename" value="WorkTicket List.xls" />
		<display:setProperty name="export.csv.filename" value="WorkTicket List.csv" />
		<display:setProperty name="export.pdf.filename" value="WorkTicket List.pdf" />
	</display:table>
	
</div>
</c:if> 
<c:if test="${workTickets=='[]'&& accountInterface=='Y'}">
<table style="margin-left:-2px;margin:0px;" border="0">
<tr> 
<td align="left" class="subcontent-tab" width="362px" ><b>No Work tickets found </b></td> 
</tr>
</table>
<table  style="margin:0px;">
<td>
<table  border="0" class="mainDetailTable" style="width:370px;margin:0px;"> 
	<tbody>
	<tr><td height="10"></td></tr>
	<tr> </td>
		<td align="right"  class="listwhitebox" width="64px">Posting&nbsp;Date</td>
<c:choose>
			<c:when test="${currentOpenPeriod}">
				<td align="left"><s:select cssClass="list-menu" name="openPeriod" list="%{openPeriodList}" headerKey=""	headerValue="" onchange="findRecAccDateToFormat();"/></td>
				<s:hidden id="recAccDateToFormat"  name="recAccDateToFormat" />
			</c:when>
			<c:otherwise>
		<c:if test="${recAccDateToFormat!=''&& recAccDateToFormat!=null}">
		<s:text id="recAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="recAccDateToFormat"/></s:text>
		<td width="260px" >
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		<s:textfield cssClass="input-textUpper" id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  size="8" maxlength="11" onblur="calcDateFlexibility();" readonly="true"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		<s:textfield cssClass="input-textUpper" id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  size="8" maxlength="11" onblur="calcDate();" readonly="true"/>
		</c:if>
		<img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recAccDateToFormat==''||recAccDateToFormat==null}">
		<td width="260px">
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		<s:textfield cssClass="input-textUpper" id="recAccDateToFormat" name="recAccDateToFormat" size="8" maxlength="11"  onblur="calcDateFlexibility();" readonly="true"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		<s:textfield cssClass="input-textUpper" id="recAccDateToFormat" name="recAccDateToFormat" size="8" maxlength="11"  onblur="calcDate();" readonly="true"/>
		</c:if>
		<img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</c:otherwise>
		</c:choose>
	 </td>
	  <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
		<td align="right"  class="listwhitebox" width="64px">Invoice&nbsp;Date</td>
		<c:if test="${recInvoiceDateToFormat!=''&& recInvoiceDateToFormat!=null}">
		<s:text id="recInvoiceDateToFormat" name="${FormDateValue}"><s:param name="value" value="recInvoiceDateToFormat"/></s:text>
		<td width="260px" ><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat"  name="recInvoiceDateToFormat" value="%{recInvoiceDateToFormat}"  size="8" maxlength="11"   onblur="calcInvoiceDate();" readonly="true"/><img id="recInvoiceDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recInvoiceDateToFormat==''||recInvoiceDateToFormat==null}">
		<td width="260px"><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat" name="recInvoiceDateToFormat" size="8" maxlength="11"  readonly="true"/><img id="recInvoiceDateToFormat_trigger" onblur="calcInvoiceDate();"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
				
		</tr>
		
		<tr> 
		<td colspan="4">
		<div id="hidAuthorization">
         <table  border="0"width="83%">
         <tbody>
          <tr> <td width="31%"></td>
            <td align="right"  class="listwhitebox" ><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
            <td width="120px"><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="23" maxlength="20" tabindex="9"  onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
            <td align="left" ><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
            </td>
         </tr>
        </tbody>
       </table>
     </div>
		</td> 
		</tr>
		<tr> 
		<c:if test="${sumActualRevenue < 0}">
		<td colspan="4">
		<c:if test="${creditInvoiceAmmountList=='[]'}">
		<b>There is no invoice for creditInvoice </b> 
		</c:if>
		<c:if test="${creditInvoiceAmmountList!='[]'}">
		<s:set name="creditInvoiceAmmountList" value="creditInvoiceAmmountList" scope="request"/>  
        <display:table name="creditInvoiceAmmountList" class="table" requestURI="" id="creditInvoiceAmmountList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="sumActualRevenue" sortable="true" title="Invoice Amount" style="width:70px" />
		<display:column property="sumCreditInvoiceAmmount" sortable="true" title="Credited Amount" style="width:70px" />
		<display:column property="availableAmount" sortable="true" title="Available Amount" style="width:70px" />
		<c:if test="${creditInvoiceAmmountList.availableAmount>0}">
		<c:if test="${creditInvoiceAmmountList.availableAmount>=absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="radioCreditInvoice" id=${creditInvoiceAmmountList.recInvoiceNumber} value=${creditInvoiceAmmountList.recInvoiceNumber}/>
 		</display:column> 
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<0||creditInvoiceAmmountList.availableAmount=='0'||creditInvoiceAmmountList.availableAmount=='0.00'}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
        </display:table> 
        </c:if> 
		</td> 
        <s:hidden name="creditInvoiceNumber" />
        </c:if>
        <c:if test="${sumActualRevenue >= 0}">
        <s:hidden name="creditInvoiceNumber"value=""/>
        </c:if> 
		</tr>
	 	<td style="float:right;">
    <!--<c:if test="${trackingStatus.soNetworkGroup !='true'}">
	<c:if test="${empty billing.billComplete}">
		<table style="margin-bottom:10px;">
	   <tr><td height="10px"></td></tr> 
	   		<%-- <td width="110px" align="left"  class="listwhitetext">&nbsp;&nbsp;&nbsp;<b><c:out value="${reportParameter.name}"/></b> :</td>--%>
	    <tr>	<td width="135px" align="left" class="listwhitetext">&nbsp;&nbsp;&nbsp; </td>
				<td><input type="checkbox" id="billingCompleted" name="billingCompleted" onchange="checkWorkTicketReviewStatus(this);"/></td>
				<td align="left"  class="listwhitebox">Is Billing completed for this Order</td>
			</tr>
		</table>
	</c:if>
    </c:if>-->
    <input type="checkbox" id="billingCompleted" name="billingCompleted" onchange="checkWorkTicketReviewStatus(this);"/>
	</td>
	<td align="left"  class="listwhitebox">Is Billing completed for this Order</td>
		</tbody>
		</table></td></tr></table>
		<table border="0" >
		<tbody>
		<tr>
		
		<td align="left">
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;" onclick="checkFormDate();" onmouseover="calcDateFlexibility();" onblur="return check_all();"/>
		</c:if>  
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;" onclick="checkFormDate();" onmouseover="calcDate();" onblur="return check_all();"/>
		</c:if>
        </td> 
         <td align="left"><input type="button" class="cssbutton1" style="width:200px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/>    
        </td>
		</tr> 
	</tbody></table> 
</c:if>
<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
<table>
<tr> 
<td></td>
<td align="left" class="subcontent-tab" width="340px"><b>No Work tickets found </b></td> 
</tr> 
</table>
<table border="0">
  <tr>
   <td>
    <div id="hidAuthorization">
      <table  border="0" class="mainDetailTable" style="width:350px">
      <tbody>
     <tr><td height="10"></td></tr>
      <tr>
        <td align="right"  class="listwhitebox" width="120px"><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
        <td width="120px"><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="23" maxlength="20" tabindex="9" onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
        <td align="left"><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
        </td>
      </tr> 
      <tr><td height="10"></td></tr>
    </tbody>
    </table>
   </div>
  </td>
  </tr>
  <tr> 
		<c:if test="${sumActualRevenue < 0}"> 
		<td colspan="4">
		<c:if test="${creditInvoiceAmmountList=='[]'}">
		<b>There is no invoice for creditInvoice </b> 
		</c:if>
		<c:if test="${creditInvoiceAmmountList!='[]'}">
		<s:set name="creditInvoiceAmmountList" value="creditInvoiceAmmountList" scope="request"/>  
        <display:table name="creditInvoiceAmmountList" class="table" requestURI="" id="creditInvoiceAmmountList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="sumActualRevenue" sortable="true" title="Invoice Amount" style="width:70px" />
		<display:column property="sumCreditInvoiceAmmount" sortable="true" title="Credited Amount" style="width:70px" />
		<display:column property="availableAmount" sortable="true" title="Available Amount" style="width:70px" />
		<c:if test="${creditInvoiceAmmountList.availableAmount>0}">
		<c:if test="${creditInvoiceAmmountList.availableAmount>=absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="radioCreditInvoice" id=${creditInvoiceAmmountList.recInvoiceNumber} value=${creditInvoiceAmmountList.recInvoiceNumber}/>
 		</display:column> 
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<0||creditInvoiceAmmountList.availableAmount=='0'||creditInvoiceAmmountList.availableAmount=='0.00'}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
        </display:table> 
        </c:if> 
        </td> 
		<s:hidden name="creditInvoiceNumber" />
        </c:if>
        <c:if test="${sumActualRevenue >= 0}">
        <s:hidden name="creditInvoiceNumber"value=""/>
        </c:if>
		</td>
      </tr>
  <tr>
    <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice" cssStyle="width:120px;margin-left:15px;" onclick="return check_all();" onmouseover=""/></td>  
    <td><input type="button" class="cssbutton1" style="width:200px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/>    
        </td> 
  </tr>
</table> 
</c:if>

<c:if test="${workTickets=='[]'&& accountInterface==''}">
<table>
<tr> 
<td></td>
<td align="left" class="subcontent-tab" width="340px"><b>No Work tickets found </b></td> 
</tr> 
</table>
<table border="0">
<tr>
<td>
<div id="hidAuthorization">
     <table  border="0" class="mainDetailTable" style="width:350px">
     <tbody>
     <tr><td height="10"></td></tr>
      <tr>
        <td align="right"  class="listwhitebox" width="120px"><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
        <td width="120px"><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="28" maxlength="20" tabindex="9" onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
        <td align="left"><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
        </td>
      </tr> 
      <tr><td height="10"></td></tr>
    </tbody>
    </table>
   </div>
</td>
</tr>
<tr>  
<c:if test="${sumActualRevenue < 0}"> 
		<td colspan="4">
		<c:if test="${creditInvoiceAmmountList=='[]'}">
		<b>There is no invoice for creditInvoice </b> 
		</c:if>
		<c:if test="${creditInvoiceAmmountList!='[]'}">
		<s:set name="creditInvoiceAmmountList" value="creditInvoiceAmmountList" scope="request"/>  
        <display:table name="creditInvoiceAmmountList" class="table" requestURI="" id="creditInvoiceAmmountList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="sumActualRevenue" sortable="true" title="Invoice Amount" style="width:70px" />
		<display:column property="sumCreditInvoiceAmmount" sortable="true" title="Credited Amount" style="width:70px" />
		<display:column property="availableAmount" sortable="true" title="Available Amount" style="width:70px" />
		<c:if test="${creditInvoiceAmmountList.availableAmount>0}">
		<c:if test="${creditInvoiceAmmountList.availableAmount>=absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="radioCreditInvoice" id=${creditInvoiceAmmountList.recInvoiceNumber} value=${creditInvoiceAmmountList.recInvoiceNumber}/>
 		</display:column> 
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<0||creditInvoiceAmmountList.availableAmount=='0' ||creditInvoiceAmmountList.availableAmount=='0.00'}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
        </display:table> 
        </c:if> 
		</td> 
		<s:hidden name="creditInvoiceNumber" />
        </c:if>
        <c:if test="${sumActualRevenue >= 0}">
        <s:hidden name="creditInvoiceNumber"value=""/>
        </c:if>
		</td>
 </tr>
<tr>
<td align="left"><s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice" cssStyle="width:120px;margin-left:15px;" onclick="return check_all();"onmouseover=""/></td>  
<td><input type="button" class="cssbutton1" style="width:200px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/>    
</td> 
</tr>
</table> 
</c:if>

<c:if test="${accountInterface=='Y'}">
<c:if test="${workTickets!='[]'}"> 
	<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		   		
		   		<td align="right" class="listwhitetext"><b>Have all these work tickets been reviewed for invoicing ?</b></td>
		  		<c:if test="${accountInterface=='Y'}">
		  		<td align="left">
        		<input type="button" class="cssbutton1"  value="Yes" name="yes" onclick="return check_all();"/>  
        		</td>
               </c:if>
        		<td align="right">
        		<input type="button" class="cssbutton1" style="width: 43px;" value="No" onclick="message();"/> 
        		<input type="button" class="cssbutton1" style="width:140px; height:25px" value="Mark Till Today Tickets" name="Mark" onclick="checkAllTillToday();"/>    
        		</td>
        		<td align="right">
        		<input type="button" class="cssbutton1" style="width:100px; height:25px" value="Mark All Tickets" name="Mark" onclick="checkAll();"/>    
        		</td>
        		
       	  	</tr>		  	
		  </tbody>
	</table>
	</c:if>
	</c:if>
	<c:if test="${accountInterface=='N'||accountInterface==''}">
	<c:if test="${workTickets!='[]'}">
	
	<!--<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  	<td>
		  		<div id="hidAuthorization">
                <table  border="0" class="mainDetailTable" style="width:350px">
                <tbody>
                <tr><td height="10"></td></tr>
                   <tr>
                      <td align="right"  class="listwhitebox" width="120px"><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
                      <td width="120px" ><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="28" maxlength="20" tabindex="9" onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
                      <td align="left"><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
                   </td>
                   </tr>
                   <tr><td height="10"></td></tr>
                  </tbody>
                  </table>
                  </div> 
		  		</td>
		  	</tr>
		  	</tbody>
	       </table>
	       --><table  border="0"> 
	       	<tbody>	  	
		     <tr>
		         
		   		<td align="right" class="listwhitetext" ><b>Have all these work tickets been reviewed for invoicing ?</b></td>
		  		<c:if test="${accountInterface=='N'||accountInterface==''}"> 
		  		<td align="left">
		  		<input type="button" class="cssbutton1"  value="Yes" name="yes" onclick="return check_all();"/>
             <%-- <s:submit cssClass="cssbutton1" type="button" value="Yes" name="yes" onclick="return check_all();" onmouseover=""/>  
        		--%>
        		</td>
          </c:if> 
        		<td align="right">
        		<input type="button" class="cssbutton1" value="No" onclick="message();"/> 
        		<input type="button" class="cssbutton1" style="width:230px; height:25px" value="Mark Till Today Tickets" name="Mark" onclick="checkAllTillToday();"/>    
        		<input type="button" class="cssbutton1" style="width:230px; height:25px" value="Mark All Tickets" name="Mark" onclick="checkAll();"/> 
        		</td>
       	  	</tr>		  	
		  </tbody>
	</table> 
	</c:if>
	</c:if>
	
	<c:if test="${workTickets!='[]'&& accountInterface=='Y'}"> 
	<div id="hid2" style="align:center">
	<table border="0"> 
	<tr>
	<td>
	<table  border="0" align="center"  class="mainDetailTable" style="width:370px">
	<tbody>
	<tr><td height="10"></td></tr>
	<tr> 
		<td align="left"  class="listwhitebox" width="20px">Posting&nbsp;Date</td>
		<c:choose>
			<c:when test="${currentOpenPeriod}">
				<td align="left"><s:select cssClass="list-menu" name="openPeriod" list="%{openPeriodList}" headerKey=""	headerValue="" onchange="findRecAccDateToFormat();"/></td>
				<s:hidden id="recAccDateToFormat"  name="recAccDateToFormat" />
			</c:when>
			<c:otherwise>
		<c:if test="${recAccDateToFormat!=''&& recAccDateToFormat!=null}">
		<s:text id="recAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="recAccDateToFormat"/></s:text>
		<td width="260px" ><s:textfield cssClass="input-textUpper" id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  size="8" maxlength="11"  readonly="true"/><img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recAccDateToFormat==''|| recAccDateToFormat==null}">
		<td width="260px"><s:textfield cssClass="input-textUpper" id="recAccDateToFormat" name="recAccDateToFormat" size="8" maxlength="11"  readonly="true"/><img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</c:otherwise>
		</c:choose>
		 <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
		<td align="right"  class="listwhitebox" width="64px">Invoice&nbsp;Date</td>
		<c:if test="${recInvoiceDateToFormat!=''&& recInvoiceDateToFormat!=null}">
		<s:text id="recInvoiceDateToFormat" name="${FormDateValue}"><s:param name="value" value="recInvoiceDateToFormat"/></s:text>
		<td width="260px" ><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat"  name="recInvoiceDateToFormat" value="%{recInvoiceDateToFormat}"  size="8" maxlength="11"  readonly="true"/><img id="recInvoiceDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recInvoiceDateToFormat==''||recInvoiceDateToFormat==null}">
		<td width="260px"><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat" name="recInvoiceDateToFormat" size="8" maxlength="11"  readonly="true"/><img id="recInvoiceDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
		</configByCorp:fieldVisibility>
		</tr>
		<tr>
		<td colspan="4">
	   <div id="hidAuthorization">
       <table   border="0"width="83%">
       <tbody>
       <tr>
        <td  align="right"  class="listwhitebox" ><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
        <td width="120px"><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="23" maxlength="20" tabindex="9" onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
        <td align="left"><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
        </td>
      </tr>
    </tbody>
    </table>
   </div>
		</td> 
		</tr>
		<tr> 
		<c:if test="${sumActualRevenue < 0}">
		<td colspan="4">
		<c:if test="${creditInvoiceAmmountList=='[]'}">
		<b>There is no invoice for creditInvoice </b> 
		</c:if>
		<c:if test="${creditInvoiceAmmountList!='[]'}"> 
		<s:set name="creditInvoiceAmmountList" value="creditInvoiceAmmountList" scope="request"/>  
        <display:table name="creditInvoiceAmmountList" class="table" requestURI="" id="creditInvoiceAmmountList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="sumActualRevenue" sortable="true" title="Invoice Amount" style="width:70px" />
		<display:column property="sumCreditInvoiceAmmount" sortable="true" title="Credited Amount" style="width:70px" />
		<display:column property="availableAmount" sortable="true" title="Available Amount" style="width:70px" />
		<c:if test="${creditInvoiceAmmountList.availableAmount>0}">
		<c:if test="${creditInvoiceAmmountList.availableAmount>=absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="radioCreditInvoice" id=${creditInvoiceAmmountList.recInvoiceNumber} value=${creditInvoiceAmmountList.recInvoiceNumber}/>
 		</display:column> 
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<absSumActualRevenue}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
 		</c:if>
 		<c:if test="${creditInvoiceAmmountList.availableAmount<0||creditInvoiceAmmountList.availableAmount=='0' ||creditInvoiceAmmountList.availableAmount=='0.00'}">
		<display:column   title="" style="width:10px" > 
 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
 		</display:column> 
 		</c:if>
        </display:table> 
        </c:if> 
		</td> 
		<s:hidden name="creditInvoiceNumber" />
        </c:if>
        <c:if test="${sumActualRevenue >= 0}">
         <s:hidden name="creditInvoiceNumber"value=""/>
        </c:if> 
		</tr>
			<td style="float:right;">
    <c:if test="${trackingStatus.soNetworkGroup !='true'}">
	<c:if test="${empty billing.billComplete}">
	<input type="checkbox" id="billingCompleted" name="billingCompleted" onchange="checkWorkTicketReviewStatus(this);"/>
	</c:if>
    </c:if>
	</td>
	<td align="left"  class="listwhitebox">Is Billing completed for this Order</td>
		</tbody></table>
		<table><tbody>
		<tr>
		<td align="left">
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;" onclick="checkFormDate()" onmouseover="calcDateFlexibility();"onmousemove="return check_all();"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;" onclick="checkFormDate()" onmouseover="calcDate();"onmousemove="return check_all();"/>
		</c:if>  
        </td>
        <td align="left"><input type="button" class="cssbutton1" style="width:200px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/>    
        </td>
          <%-- <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Generate Invoice Without Posting Date" name="Generate_Invoice"  cssStyle="width:250px;" onclick="blankPostingDate();" onmouseover="return check_all();"/>  
        </td>--%>
		</tr> 
	</tbody></table>
	</td></tr></table>
	</div> 
	</c:if> 
	<c:if test="${accountInterface=='N'||accountInterface==''}">
	<c:if test="${workTickets!='[]'}">
	<div id="hidInvButton" style="align:center">
	<table border="0"> 
	<tr><td width="40%"></td>
	<td>
	<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  	<td>
		  		<div id="hidAuthorization">
                <table  border="0" class="mainDetailTable" style="width:350px">
                <tbody>
                <tr><td height="10"></td></tr>
                   <tr>
                      <td align="right"  class="listwhitebox" width="120px"><fmt:message key="accountLine.authorization" /><font color="red" size="2">*</font></td>
                      <td width="120px" ><s:textfield cssClass="input-text" cssStyle="text-align:left" name="authorizationNoTicket" size="28" maxlength="20" tabindex="9" onblur="findAuthorizationNo();" onkeydown="buttonDisabled()"/></td>
                      <td align="left"><img align="top" class="openpopup" width="17" height="20"onclick="window.open('searchAuthorizationNo.html?type=AC&shipNumber=${shipNumberForTickets}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=authorizationNoTicket&fld_code=firstDescription','mywindow','width=800,height=700');document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].select()" src="<c:url value='/images/open-popup.gif'/>" />
                   </td>
                   </tr>
                   <tr><td height="10"></td></tr>
                  </tbody>
                  </table>
                  </div> 
		  		</td>
		  	</tr>
		  	<tr> 
		     <c:if test="${sumActualRevenue < 0}">
				<td colspan="4">
				<c:if test="${creditInvoiceAmmountList=='[]'}">
				<b>There is no invoice for creditInvoice </b> 
		        </c:if>
		        <c:if test="${creditInvoiceAmmountList!='[]'}">
				<s:set name="creditInvoiceAmmountList" value="creditInvoiceAmmountList" scope="request"/>  
		        <display:table name="creditInvoiceAmmountList" class="table" requestURI="" id="creditInvoiceAmmountList" style="width:100%"  pagesize="100" >   
		 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
				<display:column property="sumActualRevenue" sortable="true" title="Invoice Amount" style="width:70px" />
				<display:column property="sumCreditInvoiceAmmount" sortable="true" title="Credited Amount" style="width:70px" />
				<display:column property="availableAmount" sortable="true" title="Available Amount" style="width:70px" />
				<c:if test="${creditInvoiceAmmountList.availableAmount>0}">
				<c:if test="${creditInvoiceAmmountList.availableAmount>=absSumActualRevenue}">
				<display:column   title="" style="width:10px" > 
		 		<input style="vertical-align:bottom;" type="radio" name="radioCreditInvoice" id=${creditInvoiceAmmountList.recInvoiceNumber} value=${creditInvoiceAmmountList.recInvoiceNumber}/>
		 		</display:column> 
		 		</c:if>
		 		<c:if test="${creditInvoiceAmmountList.availableAmount<absSumActualRevenue}">
				<display:column   title="" style="width:10px" > 
		 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
		 		</display:column> 
		 		</c:if>
		 		</c:if>
		 		<c:if test="${creditInvoiceAmmountList.availableAmount<0||creditInvoiceAmmountList.availableAmount=='0' ||creditInvoiceAmmountList.availableAmount=='0.00'}">
				<display:column   title="" style="width:10px" > 
		 		<input style="vertical-align:bottom;" type="radio" name="" disabled="disabled"/>
		 		</display:column> 
		 		</c:if>
		        </display:table>
		        </c:if>  
		      </td> 
		      <s:hidden name="creditInvoiceNumber" />
              </c:if>
              <c:if test="${sumActualRevenue >= 0}">
              <s:hidden name="creditInvoiceNumber"value=""/>
             </c:if> 
		  	</tr>
		  	</tbody>
	       </table>
	       <table><tbody>
		<tr>
		<td align="left">
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;margin-left:15px;" onclick="checkFormDate()" onmouseover="calcDateFlexibility();"onmousemove="return check_all();"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		<s:submit cssClass="cssbutton1" type="button" value="Generate Invoice" name="GenerateInvoice"  cssStyle="width:120px;margin-left:15px;" onclick="checkFormDate()" onmouseover="calcDate();"onmousemove="return check_all();"/>
		</c:if>  
        </td>
        <td align="left"><input type="button" class="cssbutton1" style="width:200px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/>    
        </td> 
		</tr> 
	</tbody></table></td></tr></table>
	       </div>
	</c:if>
	</c:if>
<s:hidden  id="recAccDateDammy"  name="recAccDateDammy" value="%{recAccDateToFormat}" /> 
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<div id="mydiv" style="position:absolute"></div>
<div id="assignInvoiceDiv" style="align:center" >
<table  border="0"><tr><td>
<c:if test="${assignAllInvoiceList!='[]'}">  
<s:set name="assignAllInvoiceList" value="assignAllInvoiceList" scope="request"/>  
<display:table name="assignAllInvoiceList" class="table" requestURI="" id="assignAllInvoiceList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="receivedInvoiceDate" sortable="true" title="Dated" style="width:70px" />
		<display:column property="billtocode" sortable="true" title="BilltoCode" style="width:70px" />
		<display:column property="billedAmount" sortable="true" title="Billed Amount" style="width:70px" />
		<display:column property="unbilledAmount" sortable="true" title="Unbilled Amount" style="width:70px" /> 
		<display:column   title="" style="width:10px" >
		<c:if test="${assignAllInvoiceList.recInvoiceNumber!=''}">
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${assignAllInvoiceList.recInvoiceNumber} value=${assignAllInvoiceList.recInvoiceNumber}/>
 		</c:if>
 		</display:column> 
</display:table>  
</c:if></td></tr>
<tr><td>
<input type="button" name="Submit" onclick="return fillAssignInvoice();" value="Add Lines To Invoice" class="cssbuttonA" style="width:170px; height:25px" >
<input type="button" class="cssbuttonA"  value="Return Without Invoicing" name="Return_Without_Invoice" style="width:170px; height:25px" onclick="hideAssignInvoice();"/>  </td>
</table></div>


<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Invoice Generation is in process. Please don't refresh the page.</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>


</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<%-- Modified By Kunal Sharma at 13-Jan-2012 --%> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<%-- Modification closed here --%>	

<script type="text/javascript">
function findRecAccDateToFormat(){
	document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value = document.forms['activeWorkTicketList'].elements['openPeriod'].value
	document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value = document.forms['activeWorkTicketList'].elements['openPeriod'].value
}
function winOpenOriginGiven(){
	window.open('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originGivenName&fld_code=trackingStatus.originGivenCode','mywindow12','width=800,height=700');	
} 

function winOpenOriginReceived(){
	window.open('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originReceivedName&fld_code=trackingStatus.originReceivedCode','mywindow13','width=800,height=700');	
}
function winOpenDestinationGiven(){
	window.open('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fourthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationGivenName&fld_code=trackingStatus.destinationGivenCode','mywindow14','width=800,height=700');	
}
function winOpenDestinationReceived(){
	window.open('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fourthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationReceivedName&fld_code=trackingStatus.destinationReceivedCode','mywindow15','width=800,height=700');	
}


function checkOriginGiven(){
    var vendorId = document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].value;
    if(vendorId == ''){
    	document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9110;
	    http2.send(null);
	}	 
}

function handleHttpResponse9110(){
	 if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#");
       if(res.length>2){
       	if(res[2] == 'Approved'){
      			document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenName'].value = res[1];
          		document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].select();
       	}else{
      			alert("Origin Given code is not approved" ); 
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenName'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].select();
      		}  
      	}else{
	            alert("Origin Given code not valid" );
	        	document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenName'].value="";
				document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].value="";
				document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].select();
       } } }
function checkDestinationGiven(){
    var vendorId=document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].value;
    
    if(vendorId == ''){
    	document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9130;
	    http2.send(null);
    }	 
    } 
    
function handleHttpResponse9130(){
	 if (http2.readyState == 4){
     var results = http2.responseText;
     results = results.trim();
     var res = results.split("#"); 
     if(res.length>2){
     	if(res[2] == 'Approved'){
    			document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenName'].value = res[1];
        		document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].select();
     	}else{
    			alert("Destination Given Code  is not approved" ); 
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenName'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].select();
    		}  
    	}else{
          alert("Destination Given Code not valid" );
      	 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenName'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].select();
     } } }			   
function checkOriginReceived(){
    var vendorId = document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].value;
    
    if(vendorId == ''){
    	document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9120;
	    http2.send(null);
    }	 
    } 
    
function handleHttpResponse9120(){
	 if (http2.readyState == 4){
      var results = http2.responseText
      results = results.trim();
      var res = results.split("#"); 
      if(res.length>2){
      	if(res[2] == 'Approved'){
     			document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedName'].value = res[1];
         		document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].select();
      	}else{
     			alert("Origin Received Code  is not approved" ); 
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedName'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].select();
     		}  
     	}else{
           alert("Origin Received Code not valid" );
       	 document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedName'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].select();
      } } }
      
function checkDestinationReceived(){
    var vendorId = document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].value;
    if(vendorId == ''){
    	document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedName'].value="";
    }		
    
    if(vendorId !=''){
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse9140;
    http2.send(null);
    }	 
     }  
     
function handleHttpResponse9140(){
	 if (http2.readyState == 4){
    var results = http2.responseText
    results = results.trim();
    var res = results.split("#"); 
    if(res.length>2){
    	if(res[2] == 'Approved'){
   			document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedName'].value = res[1];
       		document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].select();
    	}else{
   			alert("Destination Received Code  is not approved" ); 
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedName'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].value="";
			    document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].select();
   		}  
   	}else{
         alert("Destination Received Code not valid" );
     	 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedName'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].value="";
			 document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].select();
    } } }
			   

function autoPopulate_recievedDate() {
	try{
		<configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);	
	document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value=datam;
	</configByCorp:fieldVisibility>
}catch(e){}
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}
var http2 = getHTTPObject();
var http5555 = getHTTPObject();

function checkWorkTicketReviewStatus(chk)
{
	document.getElementById('workTiketReviewFlag').value=chk.checked;
	if(chk.checked){
		showHide("block");
	 var ship = document.forms['activeWorkTicketList'].elements['shipNumberForTickets'].value; 
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponseWorkTicketReview;
     http2.send(null);
	}
}

function handleHttpResponseWorkTicketReview() { 
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length >=1)  {	
                	showHide("none");
     	    	   alert('Billing cannot be marked complete as there are work tickets which have not been '+
     	    			   'cleared. Please open tickets and clear them '+
     	    			   'before marking the billing complete.');
     	    	   document.getElementById('billingCompleted').checked=false;
     	    	   document.getElementById('workTiketReviewFlag').value=false;
                }else{ 
                	findBillingCompleteAccrueAudit();
     	       }
               }
        }

function findBillingCompleteAccrueAudit(){ 
	var accuralBillingComplete=false;
	<configByCorp:fieldVisibility componentId="component.billing.accuralBillingComplete">
	accuralBillingComplete=true;
	var sid = document.forms['activeWorkTicketList'].elements['sid'].value
    var url="reviewAccrueStatusList.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid);
    http5555.open("GET", url, true);
    http5555.onreadystatechange = handleHttpResponseAccrue;
    http5555.send(null);
    </configByCorp:fieldVisibility>
	if(accuralBillingComplete == false){ 
		document.getElementById('workTiketReviewFlag').value=true;
  	    showHide("none");
	}
}

function handleHttpResponseAccrue()
{ 
     if (http5555.readyState == 4)  {
        var results = http5555.responseText
        results = results.trim();
        results = results.replace('[','');
        results=results.replace(']',''); 
        if(results.length >=1)
        {	
        	showHide("none");
        	alert('There are open accrual(s) on line # '+results);
        	document.getElementById('billingCompleted').checked=false;
	    	document.getElementById('workTiketReviewFlag').value=false;
        }else{
        	document.getElementById('workTiketReviewFlag').value=true;
	    	showHide("none");
        } 
       }
}
	 
	
</script>

<SCRIPT LANGUAGE="JavaScript">

function pick() {
	   window.opener.document.forms['serviceForm1'].elements['buttonType'].value= "invoice"; 
	   window.opener.document.forms['serviceForm1'].elements['generateMassage'].value= "invoiceMassage"; 
	   document.cookie = 'generateMassage' + "=" + escape ('invoiceMassage');
	   document.cookie = 'buttonType' + "=" + escape ('invoice'); 
	   //parent.window.opener.document.location.reload();
	   parent.window.opener.pageReload();
  	   window.close();
}
window.onbeforeunload = function () {
	parent.window.opener.progressBarAutoSave('0'); 
	}
	
function message(){
	alert('Please review work ticket and create an Invoice line');
	parent.window.opener.progressBarAutoSave('0');
	window.close();
}

function traceChange(szDivID,targetElement){
	var dfd = document.forms['activeWorkTicketList'].elements['onSiteStatusCheck'].value;
	if(dfd == ''){
		document.forms['activeWorkTicketList'].elements['onSiteStatusCheck'].value = szDivID+'#'+targetElement.value;
	}else{
		if(dfd.indexOf(szDivID+'#') >-1){
			var splitDFD = dfd.split(',');
			var dfd = '';
			for(i = 0; i<splitDFD.length ; i++){
				if(splitDFD[i].indexOf(szDivID+'#') > -1){
					splitDFD[i] = szDivID+'#'+targetElement.value;
				}
				if(dfd == ''){
					dfd = splitDFD[i]
				}else{
					dfd = dfd+','+splitDFD[i];
				}
			}
		}else{
			 dfd=document.forms['activeWorkTicketList'].elements['onSiteStatusCheck'].value = dfd + ',' + szDivID+'#'+targetElement.value;
		}
	 	document.forms['activeWorkTicketList'].elements['onSiteStatusCheck'].value = dfd.replace( ',,' , ',' );
	}
}  
</script>
<script language="javascript" type="text/javascript">

  function userStatusUncheck(rowId, targetElement) // 1 visible, 0 hidden
   {
    
    if(!targetElement.checked)
     {
      var userUnckeckStatus = document.forms['activeWorkTicketList'].elements['userUncheck'].value;
      if(userUnckeckStatus == '')
       {
	     document.forms['activeWorkTicketList'].elements['userUncheck'].value = rowId;
       }
      else
       {
         var userUnckeckStatus=document.forms['activeWorkTicketList'].elements['userUncheck'].value = userUnckeckStatus + ',' + rowId;
         document.forms['activeWorkTicketList'].elements['userUncheck'].value = userUnckeckStatus.replace( ',,' , ',' );
       }
    }
   if(targetElement.checked==true)
    {
      var userUnckeckStatus = document.forms['activeWorkTicketList'].elements['userUncheck'].value;
      var userUnckeckStatus=document.forms['activeWorkTicketList'].elements['userUncheck'].value = userUnckeckStatus.replace( rowId , '' );
      document.forms['activeWorkTicketList'].elements['userUncheck'].value = userUnckeckStatus.replace( ',,' , ',' );
    }
  }
  
  
  
  function userStatusCheck(rowId, targetElement) // 1 visible, 0 hidden
   { 
    rowId=targetElement.id;
    if(targetElement.checked)
     {
      var userCheckStatus = document.forms['activeWorkTicketList'].elements['userCheck'].value;
      if(userCheckStatus == '')
      {
	  document.forms['activeWorkTicketList'].elements['userCheck'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['activeWorkTicketList'].elements['userCheck'].value = userCheckStatus + ',' + rowId;
      document.forms['activeWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['activeWorkTicketList'].elements['userCheck'].value;
     var userCheckStatus=document.forms['activeWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['activeWorkTicketList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     }
    }
  var form_submitted = false;
  function checkCreditInvoce() {
	  progressBarAutoSave('1');
  if (form_submitted)
	    {
	      alert ("Your form has already been submitted. Please wait...");
	      return false;
 }else {
  form_submitted = true;	 
  <c:if test="${currentOpenPeriod}">
  if(document.forms['activeWorkTicketList'].elements['openPeriod'].value == ''){
	  alert("Please select the Posting Date.")
	  progressBarAutoSave('0');
	  form_submitted = false;
	  return false;
  }
  </c:if>
  var sumActualRevenue= eval(document.forms['activeWorkTicketList'].elements['sumActualRevenue'].value);  
   var creditInvoiceNumber= document.forms['activeWorkTicketList'].elements['creditInvoiceNumber'].value;  
   creditInvoiceNumber=creditInvoiceNumber.trim();
   if(sumActualRevenue<0 && creditInvoiceNumber==''){
	   var radioCreditInvoiceValue=document.forms['activeWorkTicketList'].elements['radioCreditInvoice'];
	   if(radioCreditInvoiceValue != undefined){
   var invoiceNumber = valButton1(document.forms['activeWorkTicketList'].elements['radioCreditInvoice']);   
	    if (invoiceNumber == null)  {
	          alert("Please select any radio button");
	          form_submitted = false;
	          progressBarAutoSave('0'); 
	       	  return false; 
       } else {
    	       
	        invoiceNumber = invoiceNumber.replace('/',''); 
	        document.forms['activeWorkTicketList'].elements['creditInvoiceNumber'].value=invoiceNumber; 
	        
           }  
  } else {
	  alert("Credit invoice amount does not match with any previous invoiced amount.");
	  form_submitted = false;
	  progressBarAutoSave('0');
   	  return false; 
  }
	}
    }}
  function progressBarAutoSave(tar){
		showOrHideAutoSave(tar);
		}

		function showOrHideAutoSave(value) {
		    if (value==0) {
		        if (document.layers)
		           document.layers["overlayPopUp"].visibility='hide';
		        else
		           document.getElementById("overlayPopUp").style.visibility='hidden';
		   }
		   else if (value==1) {
		       if (document.layers)
		          document.layers["overlayPopUp"].visibility='show';
		       else
		          document.getElementById("overlayPopUp").style.visibility='visible';
		   }
		}
  
 function check_all() { 
  this.form = document.getElementById('activeWorkTicketList');
  if (this.form)
  {
     this.checkboxNodes = this.form.getElementsByTagName('input'); 
     cheackAccountInterface();
  } 
}

 var DisplayRow=new Array();
// alert("hruhretestki vcalue"+test);
//alert(document.forms['activeWorkTicketList'].elements['test'].value);
 $(document).ready(function() {
		//alert("fvgdfhgvkhdvjhdfhv")
	    $("#workTicketList tr").each(function() {
	    	var MyRows = $('table#workTicketList').find('tbody').find('tr');
	    	for (var i = 0; i < MyRows.length; i++) {
	    		<c:if test="${serviceOrder.job=='OFF'}">
	    		var MyIndexValue = $(MyRows[i]).find('td:eq(11)').html();
	    		 </c:if>
	    		 <c:if test="${serviceOrder.job!='OFF'}">
	    		var MyIndexValue = $(MyRows[i]).find('td:eq(10)').html();
	    		 </c:if>
	    	DisplayRow[i] = MyIndexValue;
			  }
	   });
}); 
 function dayFormatChange(day){
	 if(day<10){
		 if(day == '01') {   
			 day = "1";
		   } else if(day == '02') {
			   day = "2";
		   } else if(day == '03')  {
			   day = "3";
		   } else if(day == '04') {
			   day = "4";
		   }  else if(day == '05')  {
			   day = "5";
		   } else if(day == '06') {
			   day = "6";
		   }  else if(day == '07')  {
			   day = "7";
		   } else if(day == '08')  {
			   day = "8";
		   }  else if(day == '09')  {
			   day = "9";
		   }
	 
	 }return day
 }
 
 
 function dateFormatChange(month){
	 
	 if(month == 'Jan') {   
	     month = "01";
	   } else if(month == 'Feb') {
	       month = "02";
	   } else if(month == 'Mar')  {
	       month = "03"
	   } else if(month == 'Apr') {
	       month = "04"
	   }  else if(month == 'May')  {
	       month = "05"
	   } else if(month == 'Jun') {
	       month = "06"
	   }  else if(month == 'Jul')  {
	       month = "07"
	   } else if(month == 'Aug')  {
	       month = "08"
	   }  else if(month == 'Sep')  {
	       month = "09"
	   }  else if(month == 'Oct')  {
	       month = "10"
	   }  else if(month == 'Nov')  {
	       month = "11"
	   }  else if(month == 'Dec') {
	       month = "12";
	   }
	 return month
 }
 
 
 
 function checkAllTillToday(){
	 
	 document.forms['activeWorkTicketList'].elements['userCheck'].value = ""; 
		var len = document.forms['activeWorkTicketList'].elements['checkV'].length;
		if(len>1){
		for (i = 0; i < len; i++){ 
		if(document.forms['activeWorkTicketList'].elements['checkV'][i].getAttribute("disabled")=='disabled'){
			}
			else{
			document.forms['activeWorkTicketList'].elements['checkV'][i].checked = false ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV'][i]);
			} 
		}
		}else{	
			 
			if(document.forms['activeWorkTicketList'].elements['checkV'].getAttribute("disabled")=='disabled'){	
			}
			else{
			document.forms['activeWorkTicketList'].elements['checkV'].checked = false ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV']);
			} 
			}
		var len = document.forms['activeWorkTicketList'].elements['checkV'].length; 
		var q = new Date();
		var m = q.getMonth()+1;
		var d = q.getDate();
		var y = q.getFullYear();
	    var q= y+"-"+m+"-"+d;
	    var currdte= new Date(q);
		document.forms['activeWorkTicketList'].elements['userCheck'].value = ""; 
		if(len>1){
		for (i = 0; i < len; i++){
			var DisplayDate = DisplayRow[i] 
			var DsplySplitResult = DisplayDate.split("-"); 
			var day = DsplySplitResult[0];
			var month = DsplySplitResult[1];
			var year = DsplySplitResult[2];
		    var remvday =	dayFormatChange(day);
			var curectMonth=dateFormatChange(month);
			var finalDate = year+"-"+curectMonth+"-"+remvday;
		    var	chkDate=new Date(finalDate);
		    if(chkDate<=currdte)
			{
			document.forms['activeWorkTicketList'].elements['checkV'][i].checked = true ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV'][i]);
		}}
		}
		else{ 
			var DisplayDate = DisplayRow[0] 
		var DsplySplitResult = DisplayDate.split("-"); 
		var day = DsplySplitResult[0];
		var month = DsplySplitResult[1];
		var year = DsplySplitResult[2];
		 var remvday =	dayFormatChange(day);
		var curectMonth=dateFormatChange(month)
	    var finalDate = year+"-"+curectMonth+"-"+remvday;
		chkDate=new Date(finalDate);
		if((chkDate<=currdte)&&(len==undefined))
			{ 
				document.forms['activeWorkTicketList'].elements['checkV'].checked = true ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV']);
		}}
	    cheackAccountInterface(); 
	}
	 
	 
  function checkAll(){
	  
	
	  	document.forms['activeWorkTicketList'].elements['userCheck'].value = ""; 
		var len = document.forms['activeWorkTicketList'].elements['checkV'].length;
		if(len>1){
		for (i = 0; i < len; i++){ 
			document.forms['activeWorkTicketList'].elements['checkV'][i].checked = true ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV'][i]);
		}
		}else{	
			document.forms['activeWorkTicketList'].elements['checkV'].checked = true ; 
			userStatusCheck('',document.forms['activeWorkTicketList'].elements['checkV']);
		}
	    cheackAccountInterface(); 
  }
function cheackAccountInterface()
	{
      var accountInterface = document.forms['activeWorkTicketList'].elements['accountInterface'].value; 
      if(accountInterface=='Y')
       { 
         document.getElementById("hid2").style.display="block";
        
       } 
   <c:if test="${accountInterface=='N'||accountInterface==''}">
	<c:if test="${workTickets!='[]'}">
	document.getElementById("hidInvButton").style.display="block";
	</c:if>
	</c:if>
   }
   
   
   
   function checkFormDate(){
	  //document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
      var fdate = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value;
      document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=fdate;
      if(fdate==''){
       document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=''; 
      } 
      <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
      var fdate1 = document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value;
      document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=fdate1;
      if(fdate1==''){
       document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=''; 
      } 
      </configByCorp:fieldVisibility>
    }
    
    function blankPostingDate(){
       document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=''; 
       document.forms['activeWorkTicketList'].elements['recAccDateForm'].value='';
       <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
       document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value='';
       </configByCorp:fieldVisibility>
    }
 </script>
<script type="text/javascript">  

function calcDateFlexibility(){
	  if(document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value==''||document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value=='') {
	  }else{
		     var date4 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
			 var date1 = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value; 
			 var date1SplitResult = date1.split("-");
			 var day1 = date1SplitResult[0];
			 var month1 = date1SplitResult[1];
			 var year1 = date1SplitResult[2];
			   	 year1 = '20'+year1;
				 if(month1 == 'Jan'){ month1 = "01";  }
		    else if(month1 == 'Feb'){ month1 = "02";  }
			else if(month1 == 'Mar'){ month1 = "03";  }
			else if(month1 == 'Apr'){ month1 = "04";  }
			else if(month1 == 'May'){ month1 = "05";  }
			else if(month1 == 'Jun'){ month1 = "06";  }
			else if(month1 == 'Jul'){ month1 = "07";  }
			else if(month1 == 'Aug'){ month1 = "08";  }
			else if(month1 == 'Sep'){ month1 = "09";  }
			else if(month1 == 'Oct'){ month1 = "10";  }																					
			else if(month1 == 'Nov'){ month1 = "11";  }
			else if(month1 == 'Dec'){ month1 = "12";  }
					
			 var date2 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
			 var date2SplitResult = date2.split("-");
			 var day2 = date2SplitResult[0];
			 var month2 = date2SplitResult[1];
			 var year2 = date2SplitResult[2];
			   	 year2 = '20'+year2;
				 if(month2 == 'Jan'){ month2 = "01";  }
		    else if(month2 == 'Feb'){ month2 = "02";  }
			else if(month2 == 'Mar'){ month2 = "03";  }
			else if(month2 == 'Apr'){ month2 = "04";  }
			else if(month2 == 'May'){ month2 = "05";  }
			else if(month2 == 'Jun'){ month2 = "06";  }
			else if(month2 == 'Jul'){ month2 = "07";  }
			else if(month2 == 'Aug'){ month2 = "08";  }
			else if(month2 == 'Sep'){ month2 = "09";  }
			else if(month2 == 'Oct'){ month2 = "10";  }																					
			else if(month2 == 'Nov'){ month2 = "11";  }
			else if(month2 == 'Dec'){ month2 = "12";  }

		    var currentD = '<%= dt11 %>';
		    var date3SplitResult = currentD.split("-");
	 		var day3 = date3SplitResult[2];
	 		var month3 = date3SplitResult[1];
	 		var year3 = date3SplitResult[0];
	 		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
	 		var sysDefaultDate = new Date(month2+"/"+day2+"/"+year2);		
	 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
	 		  var daysApart = Math.round((selectedDate-currentDate)/86400000);
	 		 var daysApartNew = Math.round((sysDefaultDate-currentDate)/86400000);
	 		var daysApartCurrent = Math.round((sysDefaultDate-selectedDate)/86400000);
	 		  if(daysApartNew<=0){
		 		  if((daysApart<0)&&(daysApartCurrent!=0)){
			 			 document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
			 			document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].focus();
			 		    alert("You cannot enter posting date less than the current date."); 
			 		  }
	 		  }else{
		 		  if(daysApart<0){
		 				 document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
	 					document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].focus();
	 			    	alert("You cannot enter posting date less than the current date."); 
	 		  			}
	 		  }
	 		 /* else{
	 	 		  if((parseInt(month1)>=parseInt(month2))&&(parseInt(year1)>=parseInt(year2))){
	 	 		  }else{
	 	  		    alert("You can not enter Posting Date less than System Default current month"); 
	 	 		    document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
	 	 		  }  
	 		  }*/ 
	  }
}

function calcDate()
  {  
  if(document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value==''||document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value=='')
  {
  //alert('in if date blank')
  }
else
{ 
 var date2 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
 var date1 = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value; 
 var date3 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value;
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var month1 = 0;
   var month2 = month;
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
       month1 = 1;
   }
   else if(month == 'Feb')
   {
       month = "02";
       month1 = 2;
   }
   else if(month == 'Mar')
   {
       month = "03"
       month1 = 3;
   }
   else if(month == 'Apr')
   {
       month = "04"
       month1 = 4;
   }
   else if(month == 'May')
   {
       month = "05"
       month1 = 5;
   }
   else if(month == 'Jun')
   {
       month = "06"
       month1 = 6;
   }
   else if(month == 'Jul')
   {
       month = "07"
       month1 = 7;
   }
   else if(month == 'Aug')
   {
       month = "08"
       month1 = 8;
   }
   else if(month == 'Sep')
   {
       month = "09"
       month1 = 9;
   }
   else if(month == 'Oct')
   {
       month = "10"
       month1 = 10;
   }
   else if(month == 'Nov')
   {
       month = "11"
       month1 = 11;
   }
   else if(month == 'Dec')
   {
       month = "12";
       month1 = 12;
   }
   var year1 = '20'+year;
   var day1 = getLastDayOfMonth(month1,year1); 
   var date4 = day1+"-"+month2+"-"+year; 
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  //document.forms['accountLineForms'].elements['accountLine.storageDays'].value = daysApart; 
  var flag = 0;
  if(daysApart<0)
  {
    alert("You can not enter Posting Date less than system default Posting Date "); 
    flag = 1;
    document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date3;
  }
  if(flag == 0)
  {
  var corpid='${accCorpID}' 
  if(corpid=='SSCW'){
   if(day1 != day)
   {
    alert("Posting date should be last day of the month.\n\nSystem will change the posting date as "+date4); 
   	document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value = date4;
   }
   }
  } 
}
}

function calcInvoiceDate()
{    var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var olddate = new Date()
var day = olddate.getDate(); 
if (day < 10) { 
   day = "0" + day; 
} 
var m = strArray[olddate.getMonth()];
var year = olddate.getFullYear().toString().substr(-2); 

	var invoiceDate=document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value;
	 var date1SplitResult = invoiceDate.split("-");
	 var day1 = date1SplitResult[0];
	 var month1 = date1SplitResult[1];
	 var year1 = date1SplitResult[2];
	   	 year1 = '20'+year1;
	   	 if(month1 == 'Jan'){ month1 = "01";  }
		    else if(month1 == 'Feb'){ month1 = "02";  }
			else if(month1 == 'Mar'){ month1 = "03";  }
			else if(month1 == 'Apr'){ month1 = "04";  }
			else if(month1 == 'May'){ month1 = "05";  }
			else if(month1 == 'Jun'){ month1 = "06";  }
			else if(month1 == 'Jul'){ month1 = "07";  }
			else if(month1 == 'Aug'){ month1 = "08";  }
			else if(month1 == 'Sep'){ month1 = "09";  }
			else if(month1 == 'Oct'){ month1 = "10";  }																					
			else if(month1 == 'Nov'){ month1 = "11";  }
			else if(month1 == 'Dec'){ month1 = "12";  }
	var  current_datetime = new Date()
    var formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
   

   var currentD ='<%= (new java.text.SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>';
 
   var date3SplitResult = currentD.split("-");
		var day3 = date3SplitResult[2];
		var month3 = date3SplitResult[1];
		var year3 = date3SplitResult[0];
		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
		var currentDate = new Date(month3+"/"+day3+"/"+year3);
		
		  var daysApart = Math.round((selectedDate-currentDate)/86400000);
		if(daysApart>0){
			
			 alert("Invoice date can't be in future")
			 document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value=day + "-" + m + "-" + year;
		    return false;
			}
	

}
  function getLastDayOfMonth(month,year)
{
    var day;
    switch(month)
    {
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            day = 31;
            break;
        case 4 :
        case 6 :
        case 9 :
        case 11 :
               day = 30;
            break;
        case 2 :
            if( ( (year % 4 == 0) && ( year % 100 != 0) )
                           || (year % 400 == 0) )
                day = 29;
            else
                day = 28;
            break;

    }
    return day;

}

 </script>
<script type="text/javascript">
function checkMultiAuthorization()
    {  
    var billToCode = document.forms['activeWorkTicketList'].elements['billToCodeForTicket'].value; 
   // alert(billToCode);
    var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billToCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null);
  
}
function handleHttpResponse41()
        {

             if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                //alert(results); 
                if(results=='true') 
                {
                document.forms['activeWorkTicketList'].elements['authorizationStatus'].value='true'; 
                document.getElementById("hidAuthorization").style.display="block";
                var authNoTicket = document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].value;
                //alert(authNoTicket);
                if(authNoTicket == '')
                {
                	<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
    		          document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	            </c:if>
    	            <c:if test="${workTickets=='[]'&& accountInterface==''}">
    		          document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	            </c:if>
    	           <c:if test="${accountInterface=='Y'}">
    	               //document.forms['activeWorkTicketList'].elements['Generate_Invoice'].disabled=true;
    	                document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	           </c:if>
    	           <c:if test="${accountInterface=='N'||accountInterface==''}">
	                   <c:if test="${workTickets!='[]'}">
	                     document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
	                   </c:if>
	               </c:if>
                }
                else
                {
                	<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
    		             document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	            </c:if>
    	            <c:if test="${workTickets=='[]'&& accountInterface==''}">
    		             document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	            </c:if>
    	            <c:if test="${accountInterface=='Y'}"> 
    	                document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	            </c:if>
    	            <c:if test="${accountInterface=='N'||accountInterface==''}">
	                   <c:if test="${workTickets!='[]'}">
	                     document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
	                   </c:if>
	               </c:if>
                }
                
                }
                else{ 
                document.getElementById("hidAuthorization").style.display="none"; 
                }
             }
        }
    var http55 = getHTTPObject55();
    
  function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
 
 
</script>

<script type="text/javascript"> 
 
 function buttonDisabled()
 {
 	var status= document.forms['activeWorkTicketList'].elements['authorizationStatus'].value;
 	if(status=='true')
 	{
	  	<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${workTickets=='[]'&& accountInterface==''}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${accountInterface=='Y'}">
    	//document.forms['activeWorkTicketList'].elements['Generate_Invoice'].disabled=true;
    	document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${accountInterface=='N'||accountInterface==''}">
	     <c:if test="${workTickets!='[]'}">
	     document.forms['activeWorkTicketList'].elements['yes'].disabled=true;
	     </c:if>
	     </c:if>
    }
 }
 
function checkAuthorizationNo()
{
  var status= document.forms['activeWorkTicketList'].elements['authorizationStatus'].value;
 if(status=='true')
 {
	var authNoTicket = document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].value;
    //alert(authNoTicket);
    if(authNoTicket == '')
    {
    	<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${workTickets=='[]'&& accountInterface==''}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${accountInterface=='Y'}">
    	//document.forms['activeWorkTicketList'].elements['Generate_Invoice'].disabled=true;
    	document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
    	</c:if>
    	<c:if test="${accountInterface=='N'||accountInterface==''}">
	     <c:if test="${workTickets!='[]'}">
	     document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true;
	     </c:if>
	     </c:if>
    }
    else
    {
    	<c:if test="${workTickets=='[]'&& accountInterface=='N'}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	</c:if>
    	<c:if test="${workTickets=='[]'&& accountInterface==''}">
    		document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	</c:if>
    	<c:if test="${accountInterface=='Y'}">
    	//document.forms['activeWorkTicketList'].elements['Generate_Invoice'].disabled=false;
    	document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
    	</c:if>
    	<c:if test="${accountInterface=='N'||accountInterface==''}">
	     <c:if test="${workTickets!='[]'}">
	     document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=false;
	     </c:if>
	     </c:if>
    }
    }
}

</script>
<script type="text/javascript">
   function getHTTPObject()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();   
</script>  

 <script type="text/javascript">

  function findAuthorizationNo()
   { 
        var authorizationNo = document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].value; 
        var url="findAuthorizationNo.html?ajax=1&decorator=simple&popup=true&authNoInvoice="+encodeURI(authorizationNo);
        //alert(url);
        http55.open("GET", url, true);
        http55.onreadystatechange = handleHttpResponse222;
        http55.send(null);
  } 

function handleHttpResponse222()
        {
          var authorizationNo=document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].value
          authorizationNo=authorizationNo.trim();
             if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                //alert(results); 
                if(results.length>1)   
                 { 
                    checkAuthorizationNo();
                 }
                else 
                 { 
                   if(authorizationNo!='')
                   {
                    alert("Invalid Authorization No selected" ); 
                   }
                   document.forms['activeWorkTicketList'].elements['authorizationNoTicket'].value=""; 
                   checkAuthorizationNo();
			   }
             }
        }


function cheackBookingAgent()
	{
	    var serviceOrderId = document.forms['activeWorkTicketList'].elements['sid'].value;
        //var url="updateBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
        //http2.open("GET", url, true);
        //http2.onreadystatechange = handleHttpResponse;
        //http2.send(null); 
         var agree = true
        var url = "updateBookingAgent.html"; 
		var parameters = "ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId);
		<configByCorp:fieldVisibility componentId="component.generateInvoice.showReciprocitySection">
		var routing = document.forms['activeWorkTicketList'].elements['serviceOrder.routing'].value;
		var mode = document.forms['activeWorkTicketList'].elements['serviceOrder.mode'].value;
		var serviceType = document.forms['activeWorkTicketList'].elements['serviceOrder.serviceType'].value;
		var equipment = document.forms['activeWorkTicketList'].elements['miscellaneous.equipment'].value;
		var originGivenCode = document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenCode'].value;
		var originGivenName = document.forms['activeWorkTicketList'].elements['trackingStatus.originGivenName'].value.replace('& ','`');
		var destinationGivenCode = document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenCode'].value;
		var destinationGivenName = document.forms['activeWorkTicketList'].elements['trackingStatus.destinationGivenName'].value.replace('& ','`');
		var originReceivedCode = document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedCode'].value;
		var originReceivedName = document.forms['activeWorkTicketList'].elements['trackingStatus.originReceivedName'].value.replace('& ','`');
		var destinationReceivedCode = document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedCode'].value;
		var destinationReceivedName = document.forms['activeWorkTicketList'].elements['trackingStatus.destinationReceivedName'].value.replace('& ','`');
		var invoiceDetailData=serviceOrderId+'~'+routing+'~'+mode+'~'+serviceType+'~'+equipment+'~'+originGivenCode+'~'+originGivenName+'~'+destinationGivenCode+'~'+destinationGivenName+'~'+originReceivedCode+'~'+originReceivedName+'~'+destinationReceivedCode+'~'+destinationReceivedName;
		parameters = "ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId)+"&invoiceDetailData=" + encodeURI(invoiceDetailData);
		agree = confirm("Do you agree with the Reciprocity data?");
		</configByCorp:fieldVisibility>
		if(agree){
		http2.open("POST", url, true); 
		http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http2.setRequestHeader("Content-length", parameters .length);
		http2.setRequestHeader("Connection", "close");
		http2.onreadystatechange = handleHttpResponse;
		http2.send(parameters);
        var accountInterface = document.forms['activeWorkTicketList'].elements['accountInterface'].value; 
        if(accountInterface=='Y')
         {  
            document.getElementById("totalhid").style.display="block"; 
         }
        else 
         { 	
           document.getElementById("totalhid").style.display="none";
         } 
		}
   }
   function handleHttpResponse() { 
         if (http2.readyState == 4)
             {
                 var result= http2.responseText         
              }
     } 
     
 function companyMessage(){ 
	alert('Please review Service Order detail.');
	parent.window.opener.progressBarAutoSave('0');
	window.close(); 
}	
function findAssignInvoice() { 
	var sumActualRevenue= eval(document.forms['activeWorkTicketList'].elements['sumActualRevenue'].value);  
	   var creditInvoiceNumber= document.forms['activeWorkTicketList'].elements['creditInvoiceNumber'].value;  
	   creditInvoiceNumber=creditInvoiceNumber.trim();
	   if(sumActualRevenue<0 && creditInvoiceNumber==''){
		   var radioCreditInvoiceValue=document.forms['activeWorkTicketList'].elements['radioCreditInvoice'];
		   if(radioCreditInvoiceValue != undefined){
	   var invoiceNumber = valButton1(document.forms['activeWorkTicketList'].elements['radioCreditInvoice']);   
		    if (invoiceNumber == null)  {
		          alert("Please select any radio button");
		          form_submitted = false;
		          progressBarAutoSave('0'); 
		       	  return false; 
	       } else {
	    	       
		        invoiceNumber = invoiceNumber.replace('/',''); 
		        document.forms['activeWorkTicketList'].elements['creditInvoiceNumber'].value=invoiceNumber; 
		        
	           }  
	  } else {
		  alert("Credit invoice amount does not match with any previous invoiced amount.");
		  form_submitted = false;
		  progressBarAutoSave('0');
	   	  return false; 
	  }
		} 
	
    var serviceOrderId = document.forms['activeWorkTicketList'].elements['sid'].value;
    var billToCode = document.forms['activeWorkTicketList'].elements['billToCodeForTicket'].value; 
    var companyDivisionForTicket = document.forms['activeWorkTicketList'].elements['companyDivisionForTicket'].value;
    var url="findAssignInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId)+"&companyDivisionForTicket="+encodeURI(companyDivisionForTicket)+"&billToCodeForTicket="+encodeURI(billToCode); 
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpInvoiceResponse;
    http2.send(null); 
  }

function handleHttpInvoiceResponse() {
             if (http2.readyState == 4) {
                var results = http2.responseText 
                results = results.trim();  
                if(results.length>2)
                {
                document.forms['activeWorkTicketList'].action ='assignInvoiceList.html?assignInvoiceButton=yes&decorator=popup&popup=true';
                document.forms['activeWorkTicketList'].submit(); 
                }
                else if(results.length<=4)
                {
                 alert("No Available Invoice to Add lines to");
                }
            }
}

function fillAssignInvoice(){ 
       var invoiceNumber = valButton(document.forms['activeWorkTicketList'].elements['radiobilling']);   
	    if (invoiceNumber == null)  {
	          alert("Please select any radio button"); 
	       	  return false;
	       } else   { 
	        invoiceNumber = invoiceNumber.replace('/',''); 
	        document.forms['activeWorkTicketList'].elements['assignInvoiceNumber'].value=invoiceNumber; 
	        var agree = confirm("Press OK to proceed, or press Cancel.");
            if(agree)  { 
               document.forms['activeWorkTicketList'].action ='generateInvoiveForServiceOrder.html?&btntype=yes&decorator=popup&popup=true';
               document.forms['activeWorkTicketList'].submit(); 
             }  else {
             document.forms['activeWorkTicketList'].elements['assignInvoiceNumber'].value='';
             return false; 
             } 
           }  
      }
      function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['activeWorkTicketList'].elements['radiobilling'].value; 
    } 
  }
  
function valButton1(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['activeWorkTicketList'].elements['radioCreditInvoice'].value; 
    } 
  }
  
 function hideAssignInvoice(){
 document.getElementById("assignInvoiceDiv").style.display="none"; 
 cheackAccountInterface();
 }
 
   function findCheckComboBox(){ 
   var userCheckStatus = document.forms['activeWorkTicketList'].elements['userCheck'].value; 
    if(userCheckStatus == "" || userCheckStatus ==",") {
		  } else {
			userCheckStatus=userCheckStatus.trim();
		    if(userCheckStatus.indexOf(",")==0) {
			  userCheckStatus=userCheckStatus.substring(1);
			 }
			if(userCheckStatus.lastIndexOf(",")==userCheckStatus.length-1) {
			  userCheckStatus=userCheckStatus.substring(0, userCheckStatus.length -1);
			 } 
            var splituserCheck = userCheckStatus.split(',');
             for(i = 0; i<splituserCheck.length ; i++) {
	             document.getElementById(splituserCheck[i]).checked = true;  
             } 
         }
    }
	function trap(){
		 if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++) {
			      	try{
			      		if(document.images[i].src.indexOf('calender.png')>0){ 
							document.images[i].src = 'images/navarrow.gif';
							var el = document.getElementById(document.images[i].id);
							if((el.getAttribute("id")).indexOf('trigger')>0){
								el.removeAttribute("id");
							} } }
		      	catch(e){}
					} }  } 
    
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
try{
	progressBarAutoSave('0');
}catch(e){}
try{
	 <c:if test="${postingDateStop}"> 
	 trap();
	 </c:if>
	 }
catch(e){}
try{
checkMultiAuthorization();
}
catch(e){}
try{
if(document.forms['activeWorkTicketList'].elements['btntype'].value=='yes'){
pick();
}
}
catch(e){}

try{
<c:if test="${accountInterface=='Y'}"> 
if(document.forms['activeWorkTicketList'].elements['serviceOrder.bookingAgentCheckedBy'].value=='')
	 {
	    document.getElementById("totalhid").style.display="none";
	    document.getElementById("bookingAgent").style.display="block";
	    document.getElementById("bookingCreated").style.display="block"; 
	    document.getElementById("bookingCreatedReciprocity").style.display="block"; 
	    document.getElementById("bookingCreatedReciprocity1").style.display="block"; 
	 }
	if(document.forms['activeWorkTicketList'].elements['serviceOrder.bookingAgentCheckedBy'].value!='')
	 {
	   document.getElementById("totalhid").style.display="block";
	   document.getElementById("bookingAgent").style.display="block";
	   document.getElementById("bookingCreated").style.display="none";
	   document.getElementById("bookingCreatedReciprocity").style.display="none";
	   document.getElementById("bookingCreatedReciprocity1").style.display="none";
	  }
</c:if> 
}
catch(e){}
try{
<c:if test="${accountInterface!='Y'}"> 
      document.getElementById("totalhid").style.display="block";
      document.getElementById("bookingAgent").style.display="none";
	  document.getElementById("bookingCreated").style.display="none";
	  document.getElementById("bookingCreatedReciprocity").style.display="none";
	  document.getElementById("bookingCreatedReciprocity1").style.display="none";
	  
</c:if>
}
catch(e){} 	  
//document.getElementById("totalhid").style.display="none";
try{
if(document.forms['activeWorkTicketList'].elements['assignInvoiceButton'].value=='yes'){ 
document.getElementById("assignInvoiceDiv").style.display="block"; 
}
}
catch(e){}
try{
if(document.forms['activeWorkTicketList'].elements['assignInvoiceButton'].value!='yes'){
document.getElementById("assignInvoiceDiv").style.display="none"; 
}
}
catch(e){}
try{
if(document.forms['activeWorkTicketList'].elements['assignInvoiceButton'].value=='yes'){
document.forms['activeWorkTicketList'].elements['radiobilling'].checked=true;
}
}
catch(e){}
<c:if test="${sumActualRevenue < 0}">
try{
document.forms['activeWorkTicketList'].elements['radioCreditInvoice'].checked=true;
}catch(e){}
</c:if>
<c:if test="${sumActualRevenue < 0}"> 
<c:if test="${creditInvoiceAmmountList=='[]'}"> 
try{
document.forms['activeWorkTicketList'].elements['GenerateInvoice'].disabled=true; 
}catch(e){} 
</c:if>
</c:if>
try{
findCheckComboBox();
}
catch(e){}
try{
<c:if test="${accountInterface=='Y'}">
<c:if test="${workTickets!='[]'}">
document.getElementById("hid2").style.display="none";
</c:if>
</c:if>
}
catch(e){}
try{
<c:if test="${accountInterface=='N'||accountInterface==''}">
<c:if test="${workTickets!='[]'}">
document.getElementById("hidInvButton").style.display="none";
</c:if>
</c:if>
}
catch(e){}

<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true && accCorpID=='VOER'}">
setOnSelectBasedMethods(["checkFormDate(),calcDateFlexibility(),calcInvoiceDate()"]);
</c:if>
<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false && accCorpID=='VOER'}">
setOnSelectBasedMethods(["checkFormDate(),calcDate(),calcInvoiceDate()"]);
</c:if>
<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true && accCorpID!='VOER'}">
setOnSelectBasedMethods(["checkFormDate(),calcDateFlexibility()"]);
</c:if>
<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false && accCorpID!='VOER'}">
setOnSelectBasedMethods(["checkFormDate(),calcDate()"]);
</c:if>
autoPopulate_recievedDate();
setCalendarFunctionality(); 
</script>
<script type="text/javascript">
	<c:if test="${workTiketReviewFlag =='true'}">
		document.getElementById('billingCompleted').checked=true;
	  	document.getElementById('workTiketReviewFlag').value=true;
	</c:if>
</script>

<script type="text/javascript">
function viewPartnerDetails(position) {
	var partnerCode = "";
	var originalCorpID='${accCorpID}';
	partnerCode = document.forms['activeWorkTicketList'].elements['serviceOrder.bookingAgentCode'].value; 
	var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
	ajax_showTooltip(url,position);
	return false
}
</script>

<script type="text/javascript">
function viewPartnerDetailsForBillToCode(position) {
    var partnerCode = "";
    var originalCorpID='${accCorpID}';
    partnerCode = '${billToCodeForTicket}';
    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
    ajax_showTooltip(url,position);
    return false
}
</script>

