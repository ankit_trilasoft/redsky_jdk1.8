package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ImfEntitlementsDao;
import com.trilasoft.app.model.ImfEntitlements;
import com.trilasoft.app.service.ImfEntitlementsManager;

public class ImfEntitlementsManagerImpl extends GenericManagerImpl<ImfEntitlements,Long> implements ImfEntitlementsManager{
	ImfEntitlementsDao imfEntitlementsDao;
	
	public ImfEntitlementsManagerImpl(ImfEntitlementsDao imfEntitlementsDao){
		super(imfEntitlementsDao);
		this.imfEntitlementsDao=imfEntitlementsDao;
		
	}

	public List findById(Long id) {
		return imfEntitlementsDao.findById(id);
	}

	public List findMaximumId() {
		return imfEntitlementsDao.findMaximumId();
	}

	public List getImfEntitlements(String sequenceNumber) {
		return imfEntitlementsDao.getImfEntitlements(sequenceNumber);
	}
	
	public List getImfEstimate(){
		return imfEntitlementsDao.getImfEstimate();
	}
	
	public List getImfEstimateList(){
		return imfEntitlementsDao.getImfEstimateList();
	}
	
	public List getImfEstimateActual(){
		return imfEntitlementsDao.getImfEstimateActual();
	}
	public List getImfEstimateAgent(){
		return imfEntitlementsDao.getImfEstimateAgent();
	}
	public List getImfEstimateDown(){		
		return imfEntitlementsDao.getImfEstimateDown();
	}
	public void getImfEstimateListUpdate(String sequenceNumber){
		 imfEntitlementsDao.getImfEstimateListUpdate(sequenceNumber);
	}
	public void getImfEstimateUpdate(String shipNumber){
		 imfEntitlementsDao.getImfEstimateUpdate(shipNumber);
	}
	public void getImfEstimateActualUpdate(String shipNumber){
		imfEntitlementsDao.getImfEstimateActualUpdate(shipNumber);
	}
	public List getImfEstimateSscar(){
		return imfEntitlementsDao.getImfEstimateSscar();
	}
	public List getImfEstimateTsto(){
		return imfEntitlementsDao.getImfEstimateTsto();	
	}
	public List getImfEstimateTbkstg(){
		return imfEntitlementsDao.getImfEstimateTbkstg();	
	}
	public List getImfEstimateSshist(){
		return imfEntitlementsDao.getImfEstimateSshist();	
	}
	public List checkById(String seqNum){
		return imfEntitlementsDao.checkById(seqNum);
	}
	public List getImfEstimateTstghist(){
		return imfEntitlementsDao.getImfEstimateTstghist();	
	}
	public List officeDocMove(String corpID){
		return imfEntitlementsDao.officeDocMove(corpID);
	}
	public List docExtract(String corpID){
		return imfEntitlementsDao.docExtract(corpID);
	}
	public List findDosSCAC(String sessionCorpID){
		return imfEntitlementsDao.findDosSCAC(sessionCorpID);
		
	}
	public List officeDocMoveNew(String corpID){
		return imfEntitlementsDao.officeDocMoveNew(corpID);
	}
}
