<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="companyPermissionForm.title"/></title> 
<meta name="heading" content="<fmt:message key='companyPermissionForm.heading'/>"/>

<style>
.text-area {border:1px solid #219DD1;}
.description-head {color:#003366; font-family:Arial,Helvetica,sans-serif; font-size:11px; font-weight:bold;}
</style>
</head>
 <s:form id="companyPermissionForm" name="companyPermissionForm" action="saveCompanyPermissionForm" method="post" validate="true">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
 <s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.tabPermissionList' }">
    <c:redirect url="/companyPermissionList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if> 
 <div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 
	<div id="newmnav">
	  <ul>
	  	<li><a onclick="setReturnString('gototab.tabPermissionList');return autoSave();"><span>Tab Permission List</span></a></li>
	  	<!--<li><a href="companyPermissionList.html"><span>Tab Permission List</span></a></li>
	    --><li id="newmnav1" style="background:#FFF "><a class="current"><span>Tab Permission Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${corpComponentPermission.id}&tableName=corp_comp_permission&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	  </ul>
	</div><div class="spn">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">	

  	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="detailTabLabel">
		  <tbody>  	
		  	<tr>
		  		<td width="15%" height="3" align="left"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext"><fmt:message key="companyPermission.componentId"/><font color="red" size="2">*</font></td>
	  		  <td width="47%" align="left"><s:textfield name="corpComponentPermission.componentId"  maxlength="100" size="50" cssClass="input-text" onchange="trimFieldValue(this);" cssStyle="width:288px;"/></td>
		  		<td width="38%" align="right"></td>
	  		</tr>
		  	<tr>
		  	  <td align="right" class="listwhitetext"><fmt:message key="companyPermission.mask"/><font color="red" size="2">*</font></td>
		  	  <td align="left"><s:select cssClass="list-menu"   name="corpComponentPermission.mask" list="%{display}" headerKey="" headerValue="" cssStyle="width:292px" onchange="changeStatus()"/></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
		  	<tr>
		  	  <td align="right" class="listwhitetext">CorpID<font color="red" size="2">*</font></td>
		  	  <c:if test="${sessionCorpID=='TSFT'}">
		  	  <td align="left"><s:select cssClass="list-menu"   name="corpComponentPermission.corpID" list="%{globalCorpID}" headerKey="" headerValue="" onchange="changeStatus()"/></td>
		  	  </c:if>
		  	  <c:if test="${sessionCorpID!='TSFT'}">
		  	  <td align="left"><s:textfield name="corpComponentPermission.corpID" value="%{sessionCorpID}" cssClass="input-text" size="3" readonly="true" cssStyle="width:288px;"/></td>
		  	  </c:if>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
			<tr>
		  	  <td align="right" class="listwhitetext"><fmt:message key="companyPermission.description"/></td>
		  	  <td align="left"><s:textarea name="corpComponentPermission.description"  rows="4" cols="50" cssClass="textarea" readonly="false"/></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
	  	    <tr>
					   <td  height="20px"></td>
						
						</tr>
  </tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>		
		 
 		  <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
					   <td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:60px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<s:text id="corpComponentPermissionOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="corpComponentPermission.createdOn" /></s:text>
							<td valign="top"><s:hidden name="corpComponentPermission.createdOn" value="%{corpComponentPermissionOnFormattedValue}" /></td>
							<td style="width:130px"><fmt:formatDate value="${corpComponentPermission.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty corpComponentPermission.id}">
								<s:hidden name="corpComponentPermission.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{corpComponentPermission.createdBy}"/></td>
							</c:if>
							<c:if test="${empty corpComponentPermission.id}">
								<s:hidden name="corpComponentPermission.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<s:text id="corpComponentPermissionupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="corpComponentPermission.updatedOn" /></s:text>
							<td valign="top"><s:hidden name="corpComponentPermission.updatedOn" value="%{corpComponentPermissionupdatedOnFormattedValue}" /></td>
							<td style="width:130px"><fmt:formatDate value="${corpComponentPermission.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty corpComponentPermission.id}">
								<s:hidden name="corpComponentPermission.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{corpComponentPermission.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty corpComponentPermission.id}">
								<s:hidden name="corpComponentPermission.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
					  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  	<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		   <td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/>  
        		</td>       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>        		
        		<td align="right">
        		<input type="button" value="Cancel" class="cssbutton1" onclick="location.href='<c:url value="companyPermissionList.html"/>'"> 
        		</td>        		
        		<td align="right">
        		<c:if test="${not empty corpComponentPermission.id}">
		       <input type="button" class="cssbutton1" value="Add New Record" style="width:120px;" onclick="location.href='<c:url value="/companyPermissionForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table>
		</div>
	<s:hidden name="corpComponentPermission.id"/>
	</s:form>

<script type="text/javascript">

function trimFieldValue(target){
	var fieldValue = target.value.trim();
	document.forms['companyPermissionForm'].elements['corpComponentPermission.componentId'].value=fieldValue;
}

function changeStatus(){
	document.forms['companyPermissionForm'].elements['formStatus'].value = '1';
}
	
	function autoSave(clickType){
	if(!(clickType == 'save')){
     if ('${autoSavePrompt}' == 'No'){
	if(document.forms['companyPermissionForm'].elements['gotoPageString'].value == 'gototab.tabPermissionList'){
					noSaveAction = 'companyPermissionList.html';
	}
	processAutoSave(document.forms['companyPermissionForm'], 'saveCompanyPermissionForm!saveOnTabChange.html', noSaveAction);
	
	
	}else{
		var id1 = document.forms['companyPermissionForm'].elements['corpComponentPermission.id'].value;
		
		if (document.forms['companyPermissionForm'].elements['formStatus'].value == '1'){
			var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='companyPermissionForm.heading'/>");
			if(agree){
				document.forms['companyPermissionForm'].action = 'saveCompanyPermissionForm!saveOnTabChange.html';
				document.forms['companyPermissionForm'].submit();
			}else{
				if(id1 != ''){
				if(document.forms['companyPermissionForm'].elements['gotoPageString'].value == 'gototab.tabPermissionList'){
					location.href = 'companyPermissionList.html';
					}
			}
			}
		}else{
		if(id1 != ''){
			if(document.forms['companyPermissionForm'].elements['gotoPageString'].value == 'gototab.tabPermissionList'){
					location.href = 'companyPermissionList.html';
					}
			
	  	 		}
	  		 }
		  }
	 	}
	}

</script>