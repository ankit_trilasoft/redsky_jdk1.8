package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.DriverReconciliationDao;
import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.partnerDetailsList;
import com.trilasoft.app.dao.hibernate.VanLineDaoHibernate.DTOForVanLine;
import com.trilasoft.app.dao.hibernate.VanLineDaoHibernate.DtoAgencyWeekending;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.ErrorLogManager;


public class DriverReconciliationDaoHibernate  extends GenericDaoHibernate<VanLine, Long> implements DriverReconciliationDao {
	public DriverReconciliationDaoHibernate() {   
        super(VanLine.class);  
    } 
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	 protected HttpServletRequest getRequest() {
	        return ServletActionContext.getRequest();  
	  }
	
	public List getVanLineCodeCompDiv(String sessionCorpID){
		return getHibernateTemplate().find("select distinct vanLineCode from CompanyDivision where vanLineCode != '' and corpID=?",sessionCorpID);
	}
	public List getWeekendingList(String agent,String ownerCode, String corpId){
		List list1=new ArrayList();
		String query="select weekending, reconcilestatus, count(*)  from vanline force index (agent) where corpID='"+corpId+"' and agent = '"+agent+"'  and weekending >= date_sub(now(),interval 365 DAY) group by weekending, reconcilestatus order by weekending desc, reconcilestatus";					 
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			parameterMap.put(row[0].toString()+""+row[1].toString(), row[2].toString());
		}
		String query1="select distinct weekending from vanline force index (agent) where corpID='"+corpId+"' and agent = '"+agent+"'  and weekending >= date_sub(now(),interval 365 DAY) group by weekending, reconcilestatus order by weekending desc, reconcilestatus";					 
		List listb= this.getSession().createSQLQuery(query1).list(); 
		Iterator it1=listb.iterator();
		while(it1.hasNext()){
			Object row= (Object)it1.next();
			DtoAgencyWeekending dtoAgencyWeekending = new DtoAgencyWeekending(); 
			dtoAgencyWeekending.setWeekending((Date)row);
			if(parameterMap.get(row.toString()+"Suspense")!=null){
				dtoAgencyWeekending.setSuspenseCount(parameterMap.get(row.toString()+"Suspense").toString());
			}else{
				dtoAgencyWeekending.setSuspenseCount("0");
			}
			if(parameterMap.get(row.toString()+"Approved")!=null){
				dtoAgencyWeekending.setApprovedCount(parameterMap.get(row.toString()+"Approved").toString());
			}else{
				dtoAgencyWeekending.setApprovedCount("0");
			}
			if(parameterMap.get(row.toString()+"Reconcile")!=null){
				dtoAgencyWeekending.setReconciledCount(parameterMap.get(row.toString()+"Reconcile").toString());
			}else{
				dtoAgencyWeekending.setReconciledCount("0");
			}
			list1.add(dtoAgencyWeekending);
		}
		return list1;	
	}
	public  class DtoAgencyWeekending{
		private Date weekending;
		private String recordCount;		
		private String suspenseCount;
		private String approvedCount;
		private String reconciledCount;
		public Date getWeekending() {
			return weekending;
		}
		public void setWeekending(Date weekending) {
			this.weekending = weekending;
		}
		public String getRecordCount() {
			return recordCount;
		}
		public void setRecordCount(String recordCount) {
			this.recordCount = recordCount;
		}
		public String getSuspenseCount() {
			return suspenseCount;
		}
		public void setSuspenseCount(String suspenseCount) {
			this.suspenseCount = suspenseCount;
		}
		public String getApprovedCount() {
			return approvedCount;
		}
		public void setApprovedCount(String approvedCount) {
			this.approvedCount = approvedCount;
		}
		public String getReconciledCount() {
			return reconciledCount;
		}
		public void setReconciledCount(String reconciledCount) {
			this.reconciledCount = reconciledCount;
		}
	}
	 public class DriverPreviewDTO{
		 private Object orderNumber;
		 private Object registration;
		 private Object statementDate;
		 private Object ShipperName;
		 private Object totalPaid;
		 private Object total;
		 private Object variance;
		 private Object status;
		 private Object aId;
		 private Object vId;
		 private Object soId;
		 private Object chargeCode;
		 private Object totalRevenue;
		 private Object amount;
		 private Object distributionAmount;
		 private Object actualExpense;
		 private Object amtDueAgent;
		 private Object description;
		 private Object estimateVendorName;
		 private Object vendorCode;
		 private Object glType;
		 
		public Object getOrderNumber() {
			return orderNumber;
		}
		public void setOrderNumber(Object orderNumber) {
			this.orderNumber = orderNumber;
		}
		public Object getRegistration() {
			return registration;
		}
		public void setRegistration(Object registration) {
			this.registration = registration;
		}
		public Object getStatementDate() {
			return statementDate;
		}
		public void setStatementDate(Object statementDate) {
			this.statementDate = statementDate;
		}
		public Object getShipperName() {
			return ShipperName;
		}
		public void setShipperName(Object shipperName) {
			ShipperName = shipperName;
		}
		public Object getTotalPaid() {
			return totalPaid;
		}
		public void setTotalPaid(Object totalPaid) {
			this.totalPaid = totalPaid;
		}
		public Object getTotal() {
			return total;
		}
		public void setTotal(Object total) {
			this.total = total;
		}
		public Object getVariance() {
			return variance;
		}
		public void setVariance(Object variance) {
			this.variance = variance;
		}
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getaId() {
			return aId;
		}
		public void setaId(Object aId) {
			this.aId = aId;
		}
		public Object getvId() {
			return vId;
		}
		public void setvId(Object vId) {
			this.vId = vId;
		}
		public Object getChargeCode() {
			return chargeCode;
		}
		public void setChargeCode(Object chargeCode) {
			this.chargeCode = chargeCode;
		}
		public Object getTotalRevenue() {
			return totalRevenue;
		}
		public void setTotalRevenue(Object totalRevenue) {
			this.totalRevenue = totalRevenue;
		}
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getDistributionAmount() {
			return distributionAmount;
		}
		public void setDistributionAmount(Object distributionAmount) {
			this.distributionAmount = distributionAmount;
		}
		public Object getActualExpense() {
			return actualExpense;
		}
		public void setActualExpense(Object actualExpense) {
			this.actualExpense = actualExpense;
		}
		public Object getAmtDueAgent() {
			return amtDueAgent;
		}
		public void setAmtDueAgent(Object amtDueAgent) {
			this.amtDueAgent = amtDueAgent;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getSoId() {
			return soId;
		}
		public void setSoId(Object soId) {
			this.soId = soId;
		}
		public Object getEstimateVendorName() {
			return estimateVendorName;
		}
		public void setEstimateVendorName(Object estimateVendorName) {
			this.estimateVendorName = estimateVendorName;
		}
		public Object getVendorCode() {
			return vendorCode;
		}
		public void setVendorCode(Object vendorCode) {
			this.vendorCode = vendorCode;
		}
		public Object getGlType() {
			return glType;
		}
		public void setGlType(Object glType) {
			this.glType = glType;
		}

		 
		 
	 }
	 String statusDateNew;
	public List getDriverPreview(String agent,String ownerCode,Date weekending,String sessionCorpID){
		
		List previewDetails=new ArrayList();
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekending) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
	 List <Object> lista = new ArrayList<Object>();
	 	if(agent.indexOf(":")==0){
	 		String query="select s.shipnumber,s.registrationNumber,concat(if(s.firstName is null or s.firstName='','',s.firstName),' ',if(s.lastname is null or s.lastname='','',s.lastname)) as shiperName, v.weekEnding,a.actualexpense,v.amtDueAgent,v.amtDueAgent - sum(a.actualexpense) as variance,v.reconcileStatus,a.id as aId,v.id as vId ,a.serviceOrderId" +
	 				"from accountline as a, serviceorder as s,vanline as v force index (agent) , companydivision  as c " +
	 				"where v.corpID='CWMS' and s.corpID='CWMS' and a.corpID='CWMS' and c.corpID='CWMS' and c.vanLineCode= v.accountingAgent and c.companyCode = a.companyDivision and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType " +
	 				"and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null and v.regNum <> '' " +
	 				"and v.regNum is not null  and a.status = true and a.download = true  and a.recGl is not null and a.recGl <>'' " +
	 				"and  v.agent like '" + agent.replaceAll("'", "''").replaceAll(":","''") + "%' and v.weekEnding like '"+statusDateNew+"%' and a.actualExpense <>'' and a.actualExpense is not null and a.actualExpense!='0.00' and a.vendorCode<>'' and a.vendorCode is not null " ;
	 			   if(ownerCode!=null && !ownerCode.equals("")){
				 	query=query+" and a.vendorCode='"+ownerCode+"' ";
				 	}			 		
		 		    query=query+" group by a.shipNumber order by a.shipNumber ";
	 		lista= this.getSession().createSQLQuery(query).list();
	 		
	 	}else{
	 		String query="select s.shipnumber,s.registrationNumber,concat(if(s.firstName is null or s.firstName='','',s.firstName),' ',if(s.lastname is null or s.lastname='','',s.lastname)) as shiperName, v.weekEnding,sum(a.actualexpense),sum(v.amtDueAgent),'' as variance,v.reconcileStatus,'' as aId,'' as vId,a.serviceOrderId " +
	 				"from accountline as a, serviceorder as s,vanline as v force index (agent) " +
	 				"where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' " +
	 				"and  a.serviceOrderId = s.id and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType " +
	 				"and s.registrationNumber=v.regNum and v.glType <> '' and v.glType is not null and v.shipper <> '' and v.shipper is not null " +
	 				"and v.regNum <> '' and v.regNum is not null  and a.status = true and a.download = true  " +
	 				"and a.recGl is not null and a.recGl <>''  and  v.agent like '" + agent.replaceAll("'", "''") + "%' " +
	 				"and v.weekEnding like '"+statusDateNew+"%' and a.actualExpense <>'' and a.actualExpense is not null and a.actualExpense!='0.00' and a.vendorCode<>'' and a.vendorCode is not null ";
			 		if(ownerCode!=null && !ownerCode.equals("")){
			 		query=query+"and a.vendorCode='"+ownerCode+"'";
			 		}			 		
	 		        query=query+" group by a.shipNumber order by a.shipNumber ";
	 		      
	 		lista= this.getSession().createSQLQuery(query).list();
	 	}
	 	 Iterator it1=lista.iterator(); 
	 	DriverPreviewDTO previewDTO =null;
	 	 while(it1.hasNext()){
	 	  Object [] row=(Object[])it1.next();
	 	 previewDTO=new DriverPreviewDTO();
	 	 previewDTO.setOrderNumber(row[0]);
	 	 previewDTO.setRegistration(row[1]);
	 	BigDecimal AmtDueAgenttotal=new BigDecimal(0.0000);
	 	 if(row[1]!=null && !row[1].toString().equalsIgnoreCase("")){
	 		String query="select a.chargeCode,b.contract,sum(a.distributionAmount),sum(a.actualExpense),v.amtDueAgent,v.reconcileStatus,a.description,'' as aid ,s.registrationNumber,a.serviceOrderId,s.id,a.vendorCode,a.estimateVendorName ,v.glType " +
			"from accountline as a, serviceorder as s,billing as b,vanline as v force index (agent) where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and b.corpID='"+sessionCorpID+"' "  +
					" and  a.serviceOrderId = s.id and  b.id = s.id and s.registrationNumber=v.regNum and s.registrationNumber='"+row[1]+"' " +
					"and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and v.shipper <> '' and v.shipper is not null and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' " +
					"and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true and a.recGl is not null and a.recGl <>'' and a.actualExpense <>'' and a.actualExpense is not null and a.actualExpense!='0.00'  and a.vendorCode<>'' and a.vendorCode is not null ";
	               if(ownerCode!=null && !ownerCode.equals("")){
 		            query=query+" and a.vendorCode='"+ownerCode+"' ";
 		           }
	               query=query+" group by a.chargeCode ";
	    List listaAcc12= this.getSession().createSQLQuery(query).list();
	    Iterator it2=listaAcc12.iterator(); 
	   // BigDecimal actualExpenseTotal=new BigDecimal(0.0000); 	
	    while(it2.hasNext()){
	    Object [] row12=(Object[])it2.next();
	    if(row12[4]!=null && !row12[4].toString().equals("")){
	    AmtDueAgenttotal=AmtDueAgenttotal.add(new BigDecimal(row12[4].toString()));
	    }
	    }
	 	 }
	 	 previewDTO.setShipperName(row[2]);
	 	 previewDTO.setStatementDate(row[3]);
	 	 previewDTO.setTotalPaid(row[4]);
	 	 previewDTO.setTotal(AmtDueAgenttotal);
	 	 previewDTO.setVariance(row[6]);
	 	 previewDTO.setStatus(row[7]);
	 	 previewDTO.setaId(row[8]);	 	
	 	 previewDTO.setvId(row[9]);
	 	previewDTO.setSoId(row[10]);
	 	previewDetails.add(previewDTO);
	 	 }
	 	
		return previewDetails;		
	}
	public List getDriverAccPreview(String vanRegNum,String agent,String ownerCode,Date weekending,String sessionCorpID){
		String ownerPlan="";		
		List previewAccDetails=new ArrayList();
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(weekending) );
	        statusDateNew = nowYYYYMMDD.toString();
	        
	 	}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
		}
		List <Object> listaAcc = new ArrayList<Object>();
		String query="select a.chargeCode,b.contract,sum(a.distributionAmount),sum(a.actualExpense),v.amtDueAgent,v.reconcileStatus,a.description,'' as aid ,s.registrationNumber,a.serviceOrderId,s.id,a.vendorCode,a.estimateVendorName ,v.glType " +
				"from accountline as a, serviceorder as s,billing as b,vanline as v force index (agent) where v.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and a.corpID='"+sessionCorpID+"' and b.corpID='"+sessionCorpID+"' "  +
						" and  a.serviceOrderId = s.id and  b.id = s.id and s.registrationNumber=v.regNum and s.registrationNumber='"+vanRegNum+"' " +
						"and if((a.glType ='' or a.glType is null),a.chargeCode,a.glType) = v.glType and v.shipper <> '' and v.shipper is not null and v.agent like '" + agent.replaceAll("'", "''") + "%' and v.weekEnding like '"+statusDateNew+"%' " +
						"and v.regNum <> '' and v.regNum is not null and a.status = true and a.download = true and a.recGl is not null and a.recGl <>'' and a.actualExpense <>'' and a.actualExpense is not null and a.actualExpense!='0.00'  and a.vendorCode<>'' and a.vendorCode is not null ";
		               if(ownerCode!=null && !ownerCode.equals("")){
	 		            query=query+" and a.vendorCode='"+ownerCode+"' ";
	 		           }
		               query=query+" group by a.chargeCode ";
		listaAcc= this.getSession().createSQLQuery(query).list();
		 Iterator it2=listaAcc.iterator(); 
		 	DriverPreviewDTO previewAccDTO =null;
		 	String commissionPer="";
	 		BigDecimal actualExpensePercent=new BigDecimal(0.00);
	 		BigDecimal AmtDueAgentPercent=new BigDecimal(0.00);
	 		BigDecimal commissionAccPercent=new BigDecimal(0.00);
	 		BigDecimal variancePercent=new BigDecimal(0.00);
		 	 while(it2.hasNext()){
		 		commissionPer="";
		 		actualExpensePercent=new BigDecimal(0.00);
		 		AmtDueAgentPercent=new BigDecimal(0.00);
		 		commissionAccPercent=new BigDecimal(0.00);
		 		variancePercent=new BigDecimal(0.00);
		 		Object [] row=(Object[])it2.next();
		 		List list = this.getSession().createSQLQuery("select pp.commissionPlan from partnerprivate pp left outer join partnerpublic p on pp.partnerCode=p.partnerCode and p.corpID ='"+sessionCorpID+"' where pp.partnerCode='"+row[11]+"' and pp.corpID ='"+sessionCorpID+"' and p.isOwnerOp is true").list();
				if(list!=null && !(list.isEmpty()) && (list.get(0)!=null)){
					ownerPlan=list.get(0).toString();
				}
		 		previewAccDTO=new DriverPreviewDTO();
		 		previewAccDTO.setChargeCode(row[0]);
		 		if(row[0]!=null && !row[0].toString().equals("")){
		 		List commissionPercent = this.getSession().createSQLQuery("select percent from drivercommissionplan  where charge='"+row[0]+"' and contract like '%"+row[1]+"%' and plan='"+ownerPlan+"' and corpId='"+sessionCorpID+"'").list();
		 		if(commissionPercent!=null && !commissionPercent.isEmpty() && commissionPercent.get(0)!=null && !commissionPercent.get(0).toString().equals("")){
		 			commissionAccPercent=new BigDecimal(commissionPercent.get(0).toString());
		 		}
		 		}
		 		previewAccDTO.setDistributionAmount(row[2]);
		 		if(row[3]!=null && !row[3].toString().equals("")){
		 			actualExpensePercent=new BigDecimal(row[3].toString());
		 			//actualExpensePercent=actualExpensePercent.multiply(commissionAccPercent);
		 			//actualExpensePercent=actualExpensePercent.divide(new BigDecimal(100));
		 			previewAccDTO.setActualExpense(actualExpensePercent);
		 		}else{
		 			previewAccDTO.setActualExpense("0.00");
		 		}
		 		previewAccDTO.setAmtDueAgent(row[4]);
		 		if(row[4]!=null && !row[4].toString().equals("")){
		 			AmtDueAgentPercent=new BigDecimal(row[4].toString());
		 			AmtDueAgentPercent=AmtDueAgentPercent.multiply(commissionAccPercent);
		 			AmtDueAgentPercent=AmtDueAgentPercent.divide(new BigDecimal(100));
		 			DecimalFormat twoDForm = new DecimalFormat("#.##");
		 			previewAccDTO.setTotal(twoDForm.format(AmtDueAgentPercent));
		 		}else{
		 			previewAccDTO.setTotal("0.00");
		 		}
		 		if(actualExpensePercent!=null && !actualExpensePercent.equals("0.00") && AmtDueAgentPercent!=null && !AmtDueAgentPercent.equals("0.00") ){
		 			variancePercent=AmtDueAgentPercent.subtract(actualExpensePercent);
		 			DecimalFormat twoDForm = new DecimalFormat("#.##");
		 			if(twoDForm.format(variancePercent)!=null && (twoDForm.format(variancePercent).toString().equals("-0") || twoDForm.format(variancePercent).toString().equals("0")) ){
		 				previewAccDTO.setVariance("0.00");
		 				previewAccDTO.setStatus("Reconcile");
		 			}else{
		 				previewAccDTO.setVariance(twoDForm.format(variancePercent));
		 				previewAccDTO.setStatus("Dispute");
		 			}
		 			
		 		}else {
		 			previewAccDTO.setVariance(variancePercent);
		 			previewAccDTO.setStatus("Dispute");
		 		}
		 		
		 		previewAccDTO.setDescription(row[6]);
		 		//previewAccDTO.setaId(row[7]);
		 		previewAccDTO.setSoId(row[9]);
		 		previewAccDTO.setVendorCode(row[11]);
		 		previewAccDTO.setEstimateVendorName(row[12]);
		 		previewAccDTO.setGlType(row[13]);
		 		previewAccDetails.add(previewAccDTO);
		 	 }
		return previewAccDetails;	
	}
	public List findPartnerDetailsForAutoComplete(String partnerCodeAutoCopmlete,String corpid){
		List partnerDetailList1=new ArrayList();
		String query="";				
		 query="select distinct(partnercode),lastname,billingcountry,billingstate,billingcity,concat(if(isAccount is true,'Account',if(isAgent is true,'Agent',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType from partner where  partnercode like'"
				+ partnerCodeAutoCopmlete.replaceAll("'", "''")
				+ "%' and corpID in ('"
				+ hibernateUtil.getParentCorpID(corpid)
				+ "','"
				+ corpid
				+ "') and isOwnerOp=true and status='Approved' order by partnercode limit 100";				
		List partnerDetailList=this.getSession().createSQLQuery(query).list();
		partnerAutoDetailsList dto = null;
		Iterator it = partnerDetailList.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new partnerAutoDetailsList();
			dto.setPartnercode(row[0]);
			dto.setLastname(row[1]);
			dto.setBillingcountry(row[2]);
			dto.setBillingstate(row[3]);
			dto.setBillingcity(row[4]);
			dto.setPartnerType(row[5]);
			partnerDetailList1.add(dto);
		}
		return partnerDetailList1;	
	}
	public class partnerAutoDetailsList{
		private Object partnercode;
		private Object lastname;
		private Object billingcountry;
		private Object billingstate;
		private Object billingcity;
		private Object partnerType;
		
		public Object getPartnercode() {
			return partnercode;
		}
		public void setPartnercode(Object partnercode) {
			this.partnercode = partnercode;
		}
		public Object getLastname() {
			return lastname;
		}
		public void setLastname(Object lastname) {
			this.lastname = lastname;
		}
		public Object getBillingcountry() {
			return billingcountry;
		}
		public void setBillingcountry(Object billingcountry) {
			this.billingcountry = billingcountry;
		}
		public Object getBillingstate() {
			return billingstate;
		}
		public void setBillingstate(Object billingstate) {
			this.billingstate = billingstate;
		}
		public Object getBillingcity() {
			return billingcity;
		}
		public void setBillingcity(Object billingcity) {
			this.billingcity = billingcity;
		}
		public Object getPartnerType() {
			return partnerType;
		}
		public void setPartnerType(Object partnerType) {
			this.partnerType = partnerType;
		}			
		
	}
}
