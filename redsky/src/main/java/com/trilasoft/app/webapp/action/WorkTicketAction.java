/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "WorkTicket" object in Redsky that allows for WorkTicket of Shipment.
 * @Class Name	WorkTicketAction
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.displaytag.properties.SortOrderEnum;

import com.google.gson.Gson;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.WorkTicketDaoHibernate.WorkTicketStorageDTO;
import com.trilasoft.app.dao.hibernate.dto.WorkPlanDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.AuditTrail;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.BookStorage;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CoraxLog;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.model.Custom;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.MaterialDto;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.model.ScheduleResourceOperation;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.StandardAddresses;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Truck;
import com.trilasoft.app.model.TruckingOperations;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.model.WorkTicketTimeManagement;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.BookStorageManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CrewCapacityManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.OperationsDailyLimitsManager;
import com.trilasoft.app.service.OperationsHubLimitsManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.PrintDailyPackageManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.ScheduleResourceOperationManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.StandardAddressesManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.service.StorageManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TimeSheetManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.TruckManager;
import com.trilasoft.app.service.TruckingOperationsManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.app.service.WorkTicketTimeManagementManager;
import com.trilasoft.app.service.CoraxLogManager;
import com.trilasoft.app.webapp.action.ItemDataAction.sortedItemListComp;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;
import com.trilasoft.qtg.wsclient.QtgClientServiceCall;


public class WorkTicketAction extends BaseAction implements Preparable {
	private  String paraMeter1;
	private  String fDate;
	private  String tDate;
	private  Map<String, String> qc = new LinkedHashMap<String, String>();
	private Map<String, String> lead;
    private Map<String, String> parkingList;
    private Map<String, String> siteList;
	private Map<String,String>  elevatorList;
	private Map<String, String> state;
	private Map<String, String> country;
	private Map<String, String> survey;
	private Map<String, String> schedule;
	private Map<String, String> paytype;
	private Map<String, String> job;
	private Map<String, String> ostates;
	private Map<String, String> dstates;
	private Map<String, String> onhandstorage;
	private Map<String, String> organiza;
	private Map<String, String> peak;
	private Map<String, String> mode;
	private Map<String, String> service;
	private Map<String, String> tcktservc;
	private Map<String, String> dept;
	private Map<String, String> house;
	private Map<String, String> confirm;
	private Map<String, String> tcktactn;
	private Map<String, String> woinstr;
	private Map<String, String> measure;
	private Map<String, String> billStatus;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private Map<String,String> parking;
	private  Map<String, String> coord = new LinkedHashMap<String, String>();
	private Map<String, String> instructionCodeWithEnblCountry;
	private Map<String, String> originAddress;
	private Map<String, String> destinationAddress;
	private String customerFileJob;
	private String custId;	
	private String addType;
	private String descType;
	private String temp;
	private String descp;
	private String jsonCountryCodeText;
	private List<String> opshub;
	private  Map<String, String> exec = new LinkedHashMap<String, String>();
	private  Map<String, String> storageOut;
	private  Map<String, String> yesno;
	private List containerList = new ArrayList();
	private TrackingStatus trackingStatus;
	private CustomerFile customerFile;
	private Miscellaneous miscellaneous;
	private String userRole = new String();
	private Set serviceOrders;
	private Set workTickets;
	private Set locations;
	private Set secondStorages;
	private Set<Payroll> payrolls;
	private List servicePartners;
	private List ls;
	private List checkTicketList;
	private List storages;
	private List bookStorages;
	private List refMasters;
	private List itemsJbkEquips;
	private List itemsJEquips;
	private List<MaterialDto> itemsJEquipss;
	private List containers;
	private List material;
	private List materialsList;
	private List billCompleteList;
	private List countReleaseStorageList;
	private Map<String, String> locTYPE;
	private static Map<String, String> loctype_isactive;
	private int rowcount;
	private Long i;
	private Long sid;
	private Long soid; 
	private Long id;
	private Long id1;
	private Long ticket;
	private Long autoTicket;
	private Long autoSequenceNumber;
	private String itemType;
	private String sessionCorpID;
	private String countTicketBillingNotes;
	private String countTicketStaffNotes;
	private String countTicketSchedulingNotes;
	private String countTicketOriginNotes;
	private String countTicketDestinationNotes;
	private String countForTicketNotes;
	private String countTicketStorageNotes;
	private String warehouse;
	private String payMethod = new String("");
	private String payMethods = new String("");
	private String locationId;
	private String type;
	private String occupied;
	private String contract;
	private String wcontract;
	private String sequenceNumber;
	private String gotoPageString;
	private String validateFormNav;
	private String materialDescription;
	private String revisedChargeValue;
	private String checkCompanyOriginAgent="False";
	private String checkCompanyDestinationAgent="False";
	private String shipnum;
	private String ticketSortOrder;
	private String orderForTicket;
	private String hubId;
	private String service1;
	private ServiceOrder serviceOrder;
	private ServicePartner servicePartner;
	private Container container;
	private WorkTicket workTicket;
	private ItemsJEquip itemsJEquip;
	private Billing billing;
	private WorkTicketTimeManagement workTicketTimeManagement;
	private Location location;
	private ExtendedPaginatedList workTicketsExt;
	private Payroll payroll;
	private AccessInfo accessInfo;
	private PaginateListFactory paginateListFactory;
	private ItemsJEquipManager itemsJEquipManager;
	private ItemsJbkEquipManager itemsJbkEquipManager;
	private CustomerFileManager customerFileManager;
	private WorkTicketManager workTicketManager;
	private StorageManager storageManager;
	private TrackingStatusManager trackingStatusManager;
	private LocationManager locationManager;
	private BookStorageManager bookStorageManager;
	private ServiceOrderManager serviceOrderManager;
	private ServicePartnerManager servicePartnerManager;
	private NotesManager notesManager;
	private MiscellaneousManager miscellaneousManager;
	private BillingManager billingManager;
	private ContainerManager containerManager;
	private RefMasterManager refMasterManager;
	private PagingLookupManager pagingLookupManager;
	private PayrollManager payrollManager;
	private WorkTicketTimeManagementManager workTicketTimeManagementManager;
	private MyFileManager myFileManager ;
	private Storage storage;
	private List companyDivis = new ArrayList();
	private User user;
	private Set<Role> roles;
	private List<SystemDefault> sysDefaultDetail;
	private SystemDefaultManager systemDefaultManager;
	private SystemDefault systemDefault;
	private String countCheck;
	private String weightVolumeCheck;
	private String exceedDate = "";
	private String flag = "";
	private String flag3 = "";
	private Date fromDate;
	private Date toDate;
	private List workList;
	private List workListDetails;
	private List actSummaryList;
	private String shipSize;
	private List customerSO;
	private String minShip;
	private String countShip;
	private String maxTicket;
	private String countTicket;
	private String minTicket;
	private long tempTic;
	private List ticketSO;
	private Long soIdNum;
	private Long sidNum;
	private String showMap;
	private List dispathMapList;
	private Date date1;
	private Date date2;
	private Boolean originMap;
	private Boolean destinMap;
	private Boolean stateMD;
	private Boolean stateVA;
	private Boolean stateDC;
	private String from;
	private String mapStates;
	private OperationsHubLimits operationsHubLimits;
	private String hubID;
	private OperationsHubLimitsManager operationsHubLimitsManager;
	private OperationsDailyLimitsManager operationsDailyLimitsManager;
	private StorageLibraryManager storageLibraryManager;
	private Map<String, String> storageType;
	private Map<String, String> storageMode;
	private Set storageLibraries;
	private String storageId;
	private String storageTypeValue;
	private String storageModeType;
	private CustomManager customManager;
	private String countForTicketMovementIn;
	private StorageLibrary storageLibrary;
	private String hitFlag;
	private String countForTicketMovementOut;
	private Company company;
	private CompanyManager companyManager;
	private String currencySign;
	private String dfltWhse;
	private List workTicketStorageExt;	
	private List handoutPiecesAndVolumeList;
	private Integer totalHandoutPieces;
	private Double totalHandoutVolume;
	private String warehouseValue;
	private String wrehouse;
	private String wrehouseDesc;
	private Map<String, String> resourceCategory;
	private AuditTrail auditTrail;
	private AuditTrailManager auditTrailManager;
	private Boolean checkOpsRole;
	private Boolean checkCoordinator;
	private String oldTargetActual;
	private List resourceTransfer = new ArrayList();
	private Map<String, String> resourceTransferMapDTO= new HashMap<String, String>();
	private String shipNumber;
	private String btntype;
	private String resourceTicketURL;
	private String tickets;
	private String ticketPendingStatus;
	private String ticketStatus;
	private String enbState;
	private Map<String, String> countryCod;
	private Map<String, String> countryDsc;
	private List fullAddForOCity;
	private String shipNumberForCity;
	private String ocityForCity;
	private String dcityForCity;
	Date currentdate = new Date();
	private String billToAuthority;
	private Date authUpdated;
	private String usertype;
	private String userParentAgent;
	private String voxmeIntergartionFlag;
	private String customsInTicketNumber;
	private Map<String,String> localParameter;
	private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
	private TimeSheetManager timeSheetManager;
	private TimeSheet timeSheet;
	private PayrollAllocation payrollAllocation;
	private Truck truck;
	private TruckManager truckManager;
	private TruckingOperationsManager truckingOperationsManager;
	private TruckingOperations truckingOperations;
	private boolean networkAgent=false;
	private List myFileList;
	public String myFileListId;
	private CrewCapacity crewCapacity;
	private CrewCapacityManager crewCapacityManager;
	private Boolean checkCrewListRole;
	private List wtServiceTypeDyna;
	private String typeService="";
	private String amid;
	private String pmid;
	private String instructions;
	private String workorder;
	private OperationsIntelligenceManager operationsIntelligenceManager;
	private Boolean checkForLimitInOI;
	private String dataForCheckLimit;
	private List dataItemList;
	private Map<String,String> itemsList;
	private CoraxLog coraxLog;
	private CoraxLogManager coraxLogManager;
	private List addressBookList;
	private StandardAddressesManager standardAddressesManager;
	private StandardAddresses standardAddresses;
	private EmailSetupManager emailSetupManager;
	private List parentParameterValuesList;
	Map<String,String> tempWoinstrMap;
	private Map<String, String> woinstr_isactive;
	private Map<String, String> onhandstorage_isactive;
	private Map<String, String> tcktactn_isactive;
	private Map<String,String > tcktservc_isactive;
	private Map<String, String> billStatus_isactive;
	private Map<String, String> house_isactive;
	private  Map<String, String> storageOut_isactive;
	private String oiJobList; 
    private Map sortedItemList = new LinkedHashMap();
	static final Logger logger = Logger.getLogger(WorkTicketAction.class);
	private Date tempDate;
	private String oiValue;
	private String delimeter1;
	private String delimeter;
	private String tempForStatus;
	private Boolean emailFlag=false;
	private String coordinatorEmail;
	private String resorceTypeOI;
	private String resorceDescriptionOI;
	private Integer resourceQuantitiyOI = 0;
	private String WTResorceTypeOI;
	private String WTResorceDescriptionOI;
	private Integer WTResourceQuantitiyOI = 0;
	private String resorceType;
	private String resorceDescription;
	private Integer resourceQuantitiy = 0;
	private List OIListByWorkOrder;
	private List ticketByDate;
	private String returnAjaxStringValue;
	private List checkQuantityForOI;
	private long limitForThreshold;
	private String msgForThreshold;
	private boolean updateValue=false;
	private String dummyDistanceUnit;
	private String billingCheckFlag="N";
	private String mmValidation = "NO";
	private List serviceOrderSearchType;  
	private String serviceOrderSearchVal;
	public String targetActual;
	private String msgClicked;
	private List planningCalendarViewList;
	private Map<String, String> houseHub;
	private Integer count=0;
	private String defaultHub;
	private String wHouse;
	private String checkForAppendFields;
	private String calendarViewStyle;
	private String printStart;
	private String printEnd;
	private List stateDesc1;
	private String stateDesc;
	private String check;
	private String check1;
	private  Map<String, String> stateDesc1Map;
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private List timeRequirementList=new ArrayList();
	private String timeManagementList; 
	private String resourceExList;
	private String storageJob;
	private Boolean visibilityForSSCW=false;
	private String workTicketDestinationShuttle="";
	private String workTicketOriginShuttle="";
	private String stoJob;
	private Integer docCount = new Integer(0);
	private List AjaxParameterValuesList;
	private Boolean visibilityForWeightVolume=false;
	private String hubWarehouseLimit;
	private List<CrewCapacity> crewCapacityList;
	private String threshHoldFlag="No";
	private Boolean visibilityForVoer=false;
	private Boolean opsMgmtWarehouseCheck=false;
	private String checkLimit;
	private String workticketService;
	private String status;
	private Double estimatedWeight;
	private Double estimatedCubicFeet;
	private String stdAddId;
	private Map<String, String> hub;
	private Long customerFileId;
	private String wareHouseUtilization;
	private String operationChk;	
	private  Map<String, String> distinctHubDescription;
	private String handout;
	private String ids;
	private Long hoTicket;
	private List workTicketServiceList;
	private String workTicketServiceId;
	private String wtktServiceId;
	private String serviceCheck;
	private List handOutVolumePieces;
	private List serviceDesc;
	private String code;
	String workTicketReviewStatus;
	private List crewNameContractor;
	private Long wid;
	private WorkTicket addCopyWorkTicket;
	private String copyWorkTicketFlag="NO";
	private String showList;
	private List crewNameList;
	private String crewName;
	private String nameWithCode;
	private String coordinatorName;
	private List driverTicketSchedule;
	private List availableTrucks;
	private List availableCrewsDetails;
	private String omdCrewsNumList;
	private String wRHouse;
	private List dailySheetList;
	private Date dailySheetDate;
	private String dailySheetDateDisplay;
	private String crewNumList;
	private String dailySheetNotesValue;
	private List assignedCrewsEdit;
	private String assignedCrews;
	private String absentCrews;
	private String wTicket;
	private Long oId;
	private List wTAddList;
	private String wOAddress1;
	private String wOAddress2;
	private String wOAddress3;
	private String wOriginCountry;
	private String wOState;
	private String wOCity;
	private String wOZip;
	private Long wOId;
	private Double wActWt;
	private String wDAddress1;
	private String wDAddress2;
	private String wDAddress3;
	private String wDestinCountry;
	private String wDState;
	private String wDCity;
	private String wDZip;
	private String wDailySheetNotes;
	private List middleList;
	private List leftList;
	private List absentCrewsEdit;
	private List rightListTruck;
	private List leftListTruck;
	private Long dSTicket;
	private String dSSNumber;
	private String dSCName;
	private String dSWHouse;
	private String availableCrews;
	private String dSJob;
	private String dSService;
	private String assignedTrucks;
	private List scId;
	private ScheduleResourceOperationManager scheduleResourceOperationManager;
	private ScheduleResourceOperation scheduleResourceOperation;
	private Date dailySheetCalenderDate;	
	private String dailySheetwRHouse;
	private Map<String,List>dailySheetCalenderList1;
	private String timeRequirementId;
	private String workTicketId;
	private String ticketNo;
	private String crewCalTempDate = "";
	public String crewCalendarwRHouse;
	private Date crewCalendarDate;
	private Map<String,String>crewCalanderList;
	private String pathSeparator;
	private String path;
	private PrintDailyPackageManager printDailyPackageManager;
	private ReportsManager reportsManager;
	private String printFlag;
	private boolean jobFlag = false;
	private boolean serviceflag = false;
	private boolean modeFlag = false;
	private boolean militaryFlag = false;
	private List serviceArrList = new ArrayList();
	private List jobArrList = new ArrayList();
	private String residenceName;
	private String serviceCode;
	private String instructionCode;
	
	private String dashBoardHideJobsList;
	//	 A Method to Authenticate User, calling from main menu to set CorpID.
	public WorkTicketAction() {
		try
		{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.usertype=user.getUserType();
		this.userParentAgent=user.getParentAgent();
		checkOpsRole = false;
		checkCoordinator = false;
		checkCrewListRole = false;
		Set<Role> roles = user.getRoles();
		Role role = new Role();
		Iterator it = roles.iterator();
		String userRole1;
		while (it.hasNext()) {
			role = (Role) it.next();
			userRole1 = role.getName();
			if(userRole1.equalsIgnoreCase("ROLE_OPS_MGMT")){
				checkOpsRole = true;
			}
			if(userRole1.equalsIgnoreCase("ROLE_COORD") || userRole1.equalsIgnoreCase("ROLE_SALE")){
				checkCoordinator = true;
			}
			if (userRole1.equalsIgnoreCase("ROLE_FINANCE") || userRole1.equalsIgnoreCase("ROLE_BILLING")) {
				userRole = role.getName();
			}
			if (userRole1.equalsIgnoreCase("ROLE_OPS_MGMT") || userRole1.equalsIgnoreCase("ROLE_ADMIN")) {
				checkCrewListRole = true;
			}

		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String transferResourceToWorkTicket() {
		try
		{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		resourceTransfer = workTicketManager.getWorkTicketToTransfer(shipNumber, sessionCorpID);
		for (int i = 0; i < resourceTransfer.size(); i++) {
			WorkTicketStorageDTO dto = (WorkTicketStorageDTO)resourceTransfer.get(i);
			resourceTransferMapDTO.put(dto.getId().toString(), dto.getId()+"-"+dto.getTicket()+"-"+dto.getDay());
		}
		

		}
		catch(Exception  e)
		{e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String saveTransferResourceToWorkTicket() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		Set set = new HashSet();
		Set<String> ticketSet = new HashSet();
		if(tickets != null){
			String ticketAll[] = tickets.split(",");
			for (int i = 0; i < ticketAll.length; i++) {
				String ticketArr[]= ticketAll[i].split("-");
				String ticketId = ticketArr[0];
					String ticketNo = ticketArr[1];
					String day = ticketArr[2];
					day = day.replace("Day", "").replaceAll(" ","");
					
					ticketSet.add(ticketNo);
					set.add(day);
					
					List<ItemsJbkEquip> jbkEquip = itemsJbkEquipManager.getResource(sessionCorpID,shipNumber,day);
					for (ItemsJbkEquip itemsJbkEquip1 : jbkEquip) {
						ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
						itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
						itemsJbkEquip.setCreatedOn(new Date());
						itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
						itemsJbkEquip.setUpdatedOn(new Date());
						itemsJbkEquip.setDay(day);
						itemsJbkEquip.setType(itemsJbkEquip1.getType());
						itemsJbkEquip.setDescript(itemsJbkEquip1.getDescript());
						itemsJbkEquip.setCost(Double.valueOf(0));
						itemsJbkEquip.setComments(itemsJbkEquip1.getComments());
						itemsJbkEquip.setBeginDate(itemsJbkEquip1.getBeginDate());
						itemsJbkEquip.setEndDate(itemsJbkEquip1.getEndDate());
						itemsJbkEquip.setShipNum(shipNumber);
						itemsJbkEquip.setCorpID(sessionCorpID);
						itemsJbkEquip.setRevisionInvoice(false);
						
						itemsJbkEquip.setReturned(0.00);
						itemsJbkEquip.setCost(0.00);
						itemsJbkEquip.setActual(0.00);
						itemsJbkEquip.setActualQty(0);
						
						try {
							double d = Double.valueOf(itemsJbkEquip1.getCost());
							itemsJbkEquip.setQty((int)d);
							itemsJbkEquip.setQty(itemsJbkEquip1.getQty());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						}
						
						try {
							itemsJbkEquip.setEstHour(itemsJbkEquip1.getEstHour());
							itemsJbkEquip.setWorkTicketID(Long.valueOf(ticketId));
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						itemsJbkEquip.setTicket(Long.valueOf(ticketNo));
						itemsJbkEquip.setTicketTransferStatus(true);
						
						itemsJbkEquipManager.save(itemsJbkEquip);      
					}
					for (ItemsJbkEquip itemsJbkEquip1 : jbkEquip) {
						itemsJbkEquip1.setWorkTicketID(Long.valueOf(ticketId));
						itemsJbkEquipManager.save(itemsJbkEquip1);     
					}
			}
			if (set != null && set.size() > 0) {
				itemsJbkEquipManager.updateTicketTransferStatus(shipNumber,set);
			}
		}
		if(ticketStatus != null && ticketStatus.startsWith("P")){
			ticketStatus=ticketStatus.replaceFirst("P","");
			workTicketManager.updateTicketStatusPending(ticketStatus, sessionCorpID,"P");
		}
	resourceTicketURL = "?shipNumber="+shipNumber+"&btnType=YES";
		}
		catch(Exception e )
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public Date getDateFormat(String dateTemp){
		try {
			if(dateTemp == null || "".equals(dateTemp.trim())){
				return null;
			}else{
				try {
					
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(dateTemp);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              dateTemp = df.format(du);  
					return new SimpleDateFormat("yyyy-MM-dd").parse(dateTemp);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		Date date = null;
		return date;
	}
	public synchronized String createWorkTicketFromOI() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" createWorkTicketFromOI method  Start");
		try
		{
		if(oiValue != null && oiValue.trim().length() > 0){
			String strAcc[] = oiValue.split(delimeter1);
			String totalTicket="";
			String coordinatorName="";
			for (String str : strAcc) {
				String str1[] = str.split(delimeter);
				Long id1=Long.parseLong(str1[0]);
		miscellaneous = miscellaneousManager.get(id);
		workTicket = new WorkTicket();
		serviceOrder = serviceOrderManager.get(id);
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());		
		customerFile = serviceOrder.getCustomerFile();
		billing = billingManager.get(serviceOrder.getId());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start point 2");
		String amid = str1[2].toString();
		String pmid = str1[3].toString();
		coordinatorName = serviceOrder.getCoordinator();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		if ((itemsJEquipManager.checkforBilling(serviceOrder.getShipNumber())).isEmpty()) {
			workTicket.setContract(serviceOrder.getCustomerFile().getContract());

		} else {
			workTicket.setContract(billing.getContract()); 
		}
		List pM = workTicketManager.findBillingPayMethod(serviceOrder.getShipNumber());
		if (!pM.isEmpty() && pM.get(0)!=null) {
			payMethod = pM.get(0).toString();
		}
		workTicket.setServiceOrder(serviceOrder);
		workTicket.setShipNumber(shipNumber);
		autoTicket = workTicketManager.findMaximum(sessionCorpID);
		workTicket.setTicket(autoTicket);
		workTicket.setPayMethod(payMethod);
		workTicket.setService(str1[4]+"");
		workTicket.setWarehouse(str1[5]+"");
		double hours = operationsIntelligenceManager.getHoursWithValues(shipNumber,str1[7].toString(),str1[8].toString());
		workTicket.setHours(hours);
		String valueOfInstruction = operationsIntelligenceManager.getScopeWithValues(shipNumber,str1[7].toString(),str1[8].toString());
		if(str1[6] == null || str1[6] == " " || str1[6].toString().equalsIgnoreCase(" ") || str1[6].toString().isEmpty())
		{
			workTicket.setCrewVehicleSummary("Commercial Work");
		}else{
		workTicket.setCrewVehicleSummary(str1[6]+"");
		}
		workTicket.setCreatedBy("System :"+getRequest().getRemoteUser());
		workTicket.setUpdatedBy("System :"+getRequest().getRemoteUser());
		workTicket.setReviewStatus("UnBilled");
		 if (str1[8].toString().equalsIgnoreCase("T"))
			{
			 workTicket.setScheduledArrive(str1[11].toString());
			 workTicket.setTargetActual("T");
			 try{
				 workTicket.setCrewVehicleSummary(str1[14].toString()+" "+str1[13].toString()+" "+str1[12].toString());
			 }catch(Exception e){
					workTicket.setInstructions("");	
				}
			}else
		if(str1[11].toString().equalsIgnoreCase("Y") || str1[11].toString().equalsIgnoreCase("T") || str1[11].toString().equalsIgnoreCase("F"))
		{
		workTicket.setTargetActual("P");
		}else{
		workTicket.setTargetActual("T");
		workTicket.setScheduledArrive(str1[12].toString());
		if(valueOfInstruction == null || valueOfInstruction.equalsIgnoreCase(" ")){
			workTicket.setCrewVehicleSummary(str1[13].toString());	
		}else{
			try{
			    workTicket.setCrewVehicleSummary(valueOfInstruction.split("~")[0].toString());
				workTicket.setInstructions(valueOfInstruction.toString().split("~")[1]);
				}catch(Exception e){
					workTicket.setInstructions("");	
				}
		}
		}
		workTicket.setStatusDate(new Date());
		workTicket.setLeaveWareHouse("00:00");
		workTicket.setBeginningTime("00:00");
		workTicket.setEndTime("00:00");
		workTicket.setTimeFrom("00:00");
		workTicket.setTimeTo("00:00");
		workTicket.setCreatedOn(new Date());
		workTicket.setDate1(((str1[1] == null || "".equals(str1[1].trim())) ? null : getDateFormat(str1[1])));
		workTicket.setDate2(((str1[1] == null || "".equals(str1[1].trim())) ? null : getDateFormat(str1[1])));
		workTicket.setUpdatedOn(new Date());
		workTicket.setOpenDate(new Date());
		workTicket.setCorpID(sessionCorpID);
		workTicket.setPrefix(serviceOrder.getPrefix());
		workTicket.setMi(serviceOrder.getMi());
		workTicket.setFirstName(serviceOrder.getCustomerFile().getFirstName());
		workTicket.setLastName(serviceOrder.getCustomerFile().getLastName());
		workTicket.setSuffix(serviceOrder.getSuffix());
		workTicket.setJobType(serviceOrder.getJob());
		workTicket.setJobMode(serviceOrder.getMode());
		workTicket.setEstimator(serviceOrder.getEstimator());
		workTicket.setSalesMan(serviceOrder.getSalesMan());
		workTicket.setCoordinator(serviceOrder.getCoordinator());
		workTicket.setOriginCompany(serviceOrder.getOriginCompany());
		workTicket.setAddress1(serviceOrder.getOriginAddressLine1());
		workTicket.setAddress2(serviceOrder.getOriginAddressLine2());
		workTicket.setAddress3(serviceOrder.getOriginAddressLine3());
		workTicket.setCity(serviceOrder.getOriginCity());
		workTicket.setState(serviceOrder.getOriginState());
		workTicket.setZip(serviceOrder.getOriginZip());
		check=workTicket.getState();
		check1=workTicket.getDestinationState();
		workTicket.setOriginCountry(serviceOrder.getOriginCountry());
		workTicket.setOriginCountryCode(serviceOrder.getOriginCountryCode());
		workTicket.setPhone(serviceOrder.getOriginDayPhone());
		workTicket.setHomePhone(serviceOrder.getOriginHomePhone());
		workTicket.setOriginContactWeekend(serviceOrder.getOriginContactWork());
		workTicket.setOriginFax(serviceOrder.getOriginFax());
		workTicket.setOriginContactName(serviceOrder.getOriginContactName());
		workTicket.setOriginContactPhone(serviceOrder.getOriginContactPhone());
		workTicket.setDestinationCompany(serviceOrder.getDestinationCompany());
		workTicket.setDestinationAddress1(serviceOrder.getDestinationAddressLine1());
		workTicket.setDestinationAddress2(serviceOrder.getDestinationAddressLine2());
		workTicket.setDestinationAddress3(serviceOrder.getDestinationAddressLine3());
		workTicket.setDestinationCity(serviceOrder.getDestinationCity());
		workTicket.setDestinationZip(serviceOrder.getDestinationZip());
		
		workTicket.setDestinationCountry(serviceOrder.getDestinationCountry());
		workTicket.setDestinationState(serviceOrder.getDestinationState());
		workTicket.setDestinationCountryCode(serviceOrder.getDestinationCountryCode());
		workTicket.setDestinationPhone(serviceOrder.getDestinationDayPhone());
		workTicket.setDestinationHomePhone(serviceOrder.getDestinationHomePhone());
		workTicket.setDestinationFax(serviceOrder.getDestinationFax());
		workTicket.setContactName(serviceOrder.getContactName());
		workTicket.setContactPhone(serviceOrder.getContactPhone());
		workTicket.setContactFax(serviceOrder.getContactFax());
		workTicket.setAccount(serviceOrder.getBillToCode());
		workTicket.setAccountName(serviceOrder.getBillToName());
		if(pmid.equalsIgnoreCase("true"))
		{
		workTicket.setResourceReqdPm(true);
		}else{
			workTicket.setResourceReqdPm(false);
		}
		if(amid.equalsIgnoreCase("true"))
		{
			workTicket.setResourceReqdAm(true);
		}else{
			workTicket.setResourceReqdAm(false);
		}
		workTicket.setOriginMobile(serviceOrder.getOriginMobile());
		workTicket.setDestinationMobile(serviceOrder.getDestinationMobile());
		if(workTicket.getOriginPreferredContactTime() == null)
			workTicket.setOriginPreferredContactTime(serviceOrder.getOriginPreferredContactTime());
		if(workTicket.getDestPreferredContactTime() == null)
			workTicket.setDestPreferredContactTime(serviceOrder.getDestPreferredContactTime());
		
		workTicket.setPhoneExt(serviceOrder.getOriginDayExtn());
		workTicket.setDestinationPhoneExt(serviceOrder.getDestinationDayExtn());
		workTicket.setSequenceNumber(serviceOrder.getSequenceNumber());
		workTicket.setCompanyDivision(serviceOrder.getCompanyDivision());
		workTicket.setUnit1(miscellaneous.getUnit1());
		workTicket.setUnit2(miscellaneous.getUnit2());
		
		if (serviceOrder.getMode().equalsIgnoreCase("AIR")) {
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			if (miscellaneous.getEstimateGrossWeight() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeight().doubleValue()));
			}
			if (miscellaneous.getActualGrossWeight() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeight().doubleValue()));
			}
			if (miscellaneous.getEstimateCubicFeet() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicFeet().doubleValue()));
			}
			if (miscellaneous.getActualCubicFeet() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getActualCubicFeet().doubleValue()));
			}
		} else
		{
			if (miscellaneous.getEstimateGrossWeightKilo() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeightKilo().doubleValue()));
			}
			if (miscellaneous.getActualGrossWeightKilo() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeightKilo().doubleValue()));
			}
			if (miscellaneous.getEstimateCubicMtr() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicMtr().doubleValue()));
			}
			if (miscellaneous.getActualCubicMtr() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getActualCubicMtr().doubleValue()));
			}
		}
		}else{
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			if (miscellaneous.getEstimatedNetWeight() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeight().doubleValue()));
			}
			if (miscellaneous.getActualNetWeight() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeight().doubleValue()));
			}
			if (miscellaneous.getNetEstimateCubicFeet() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicFeet().doubleValue()));
			}
			if (miscellaneous.getNetActualCubicFeet() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicFeet().doubleValue()));
			}
		}
				else{
					if (miscellaneous.getEstimatedNetWeightKilo() == null) {
						workTicket.setEstimatedWeight(new Double(0.0));
					} else {
						workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeightKilo().doubleValue()));
					}
					if (miscellaneous.getActualNetWeightKilo() == null) {
						workTicket.setActualWeight(new Double(0.0));
					} else {
						workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeightKilo().doubleValue()));
					}
					if (miscellaneous.getNetEstimateCubicMtr() == null) {
						workTicket.setEstimatedCubicFeet(new Double(0.0));
					} else {
						workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicMtr().doubleValue()));
					}
					if (miscellaneous.getNetActualCubicMtr() == null) {
						workTicket.setActualVolume(new Double(0.0));
					} else {
						workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicMtr().doubleValue()));
					}
				}
		}
	String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
	visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
	if(visibilityForSSCW){
	AccessInfo accInfo = workTicketManager.getAccessinfo(sessionCorpID,id);
	if(accInfo!=null){
	if(accInfo.getOriginElevator().equals(true)){
		workTicket.setOriginElevator("Present");
	}else{
		workTicket.setOriginElevator("");
	}
	if(accInfo.getOriginReserveParking().equals(true)){
		workTicket.setParking("Parking Permit");
	}else{
		workTicket.setParking("");
	}
	if(accInfo.getOriginLongCarry().equals(true)){
		workTicket.setOriginLongCarry("Yes");
	}else{
		workTicket.setOriginLongCarry("");
	}
	if(accInfo.getOriginShuttleRequired().equals(true)){
		workTicketOriginShuttle="Yes";
	}else{
		workTicketOriginShuttle="";
	}
	if(accInfo.getDestinationElevator().equals(true)){
		workTicket.setDestinationElevator("Present");
	}else{
		workTicket.setDestinationElevator("");
	}
	if(accInfo.getDestinationReserveParking().equals(true)){
		workTicket.setDest_park("Parking Permit");
	}else{
		workTicket.setDest_park("");
	}
	if(accInfo.getDestinationLongCarry().equals(true)){
		workTicket.setDestinationLongCarry("Yes");
	}else{
		workTicket.setDestinationLongCarry("");
	}
	if(accInfo.getDestinationShuttleRequired().equals(true)){
		workTicketDestinationShuttle="Yes";
	}else{
		workTicketDestinationShuttle="";
	}
	}}
	String typeOI = str1[8].toString();
	 if (typeOI.equalsIgnoreCase("T")&& str1[9]!=null )
		{
	if(str1[9]!=null && str1[9].toString() != null && !(str1[9].toString().equalsIgnoreCase("")))
	{
	workTicket.setVendorCode(str1[10]+"");
	workTicket.setVendorName(str1[9]+"");
	}}
	 
	 workTicket=workTicketManager.save(workTicket);
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start point 3");
	 if(totalTicket.isEmpty()){
	 totalTicket=String.valueOf(autoTicket);
	 }else{
		 totalTicket = totalTicket+","+ String.valueOf(autoTicket);
	 }
	 
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" END");
    Long tempTicketId = workTicket.getId();
   List operationsIntelligenceList;
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"  operationsIntelligenceList Start ");
	if(usertype.equalsIgnoreCase("ACCOUNT")){
		usertype = "AC_"+getRequest().getRemoteUser();
	}else if(usertype.equalsIgnoreCase("DRIVER")){
		usertype = "DR_"+getRequest().getRemoteUser();
	}else{
		usertype = getRequest().getRemoteUser();
	}
    if (typeOI.equalsIgnoreCase("T"))
	{
    	operationsIntelligenceManager.updateOperationsIntelligenceTicketNo(autoTicket ,str1[7].toString(),shipNumber,sessionCorpID,str1[0].toString(),str1[1].toString(),tempTicketId,usertype,workTicket.getTargetActual());
    	operationsIntelligenceList = operationsIntelligenceManager.getResourceFromOIForTP(sessionCorpID,shipNumber,str1[7].toString(),str1[0].toString());
	}else{
		operationsIntelligenceManager.updateOperationsIntelligenceTicketNo(autoTicket ,str1[7].toString(),shipNumber,sessionCorpID,"",str1[1].toString(),tempTicketId,usertype,workTicket.getTargetActual());
		 operationsIntelligenceList = operationsIntelligenceManager.getResourceFromOI(sessionCorpID,shipNumber,str1[7].toString());
	}
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"  operationsIntelligenceList Start point 1");
		Iterator it = operationsIntelligenceList.iterator();
		while(it.hasNext())
		{
			String charge1 = "";
			OperationsIntelligence operationsIntelligence = (OperationsIntelligence)it.next();
			ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
			itemsJbkEquip.setCreatedBy("System :"+getRequest().getRemoteUser());
			itemsJbkEquip.setCreatedOn(new Date());
			itemsJbkEquip.setUpdatedBy("System :"+getRequest().getRemoteUser());
			itemsJbkEquip.setUpdatedOn(new Date());
			itemsJbkEquip.setType(operationsIntelligence.getType());
			itemsJbkEquip.setDescript(operationsIntelligence.getDescription());
			itemsJbkEquip.setComments(operationsIntelligence.getComments());
			itemsJbkEquip.setBeginDate(new Date());
			itemsJbkEquip.setEndDate(new Date());
			itemsJbkEquip.setShipNum(shipNumber);
			itemsJbkEquip.setCorpID(sessionCorpID);
			itemsJbkEquip.setRevisionInvoice(false);
			
			itemsJbkEquip.setReturned(0.00);
			if(operationsIntelligence.getType().equals("C") || operationsIntelligence.getType().equals("V")){
			itemsJbkEquip.setCost(operationsIntelligence.getEstimatedbuyrate().doubleValue());
			}else{
				itemsJbkEquip.setCost(0.00);
			}
			
			/*if(operationsIntelligence.getType().equalsIgnoreCase("C"))
			 {
			 charge1 = "Crew";
			 }
			 if(operationsIntelligence.getType().equalsIgnoreCase("E"))
			 {
			 charge1 = "Equipment";
			 }
			 if(operationsIntelligence.getType().equalsIgnoreCase("M"))
			 {
			 charge1 = "Material";
			 }
			 if(operationsIntelligence.getType().equalsIgnoreCase("V"))
			 {
			 charge1 = "Vehicle";
			 }
			 if(operationsIntelligence.getType().equalsIgnoreCase("T"))
			 {
			 charge1 = "Third Party";
			 }*/
			
			itemsJbkEquip.setContractRate(new BigDecimal(operationsIntelligenceManager.getPresetValue(billing.getContract(), operationsIntelligence.getType(), sessionCorpID,operationsIntelligence.getDescription())));
			itemsJbkEquip.setActual(operationsIntelligence.getEstimatedexpense().doubleValue());
			itemsJbkEquip.setActualQty(0);
			
			try {
				itemsJbkEquip.setQty(operationsIntelligence.getQuantitiy().intValue());
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
			
			try {
				itemsJbkEquip.setEstHour(operationsIntelligence.getEsthours());
				itemsJbkEquip.setWorkTicketID(tempTicketId);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			itemsJbkEquip.setTicket(autoTicket);
			itemsJbkEquip.setTicketTransferStatus(true);
			
			itemsJbkEquip = itemsJbkEquipManager.save(itemsJbkEquip);
			Long itemsJbkEquipId = itemsJbkEquip.getId();
			operationsIntelligenceManager.updateOperationsIntelligenceItemsJbkEquipId(operationsIntelligence.getId(),itemsJbkEquipId,shipNumber,sessionCorpID);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"  operationsIntelligenceList Start point 2");
		}  
			if(emailFlag){
				String userMail="";
				String userSignature="";
				String coorinatorMail = "";
				String coorinatorSignature = "";
				String coorinatorLastName ="";
				 String msgText1 ="";
				 String userName="";
				 String subject = "";
				 String userFirstName="";
				 String userLasttName="";
			List<User> user=userManager.findUserDetails(getRequest().getRemoteUser());
			if (!(user == null || user.isEmpty())) {
				for (User user1 : user) {
					 userMail = user1.getEmail();
					 userSignature=user1.getSignature();
					 userFirstName = user1.getFirstName();
					 userLasttName = user1.getLastName();
					 userName = userFirstName +" "+userLasttName;
				}
			}	
			
			List<User> userCoorinator=userManager.findUserDetails(coordinatorName);
			if (!(userCoorinator == null || userCoorinator.isEmpty())) {
				for (User userCoorinator1 : userCoorinator) {
					coorinatorMail = userCoorinator1.getEmail();
					coorinatorSignature=userCoorinator1.getSignature();
					coorinatorLastName = userCoorinator1.getLastName();
				}
			}
			
			subject = "Ticket Notification for Order Number "+shipNumber;
			msgText1 = "Hello " +coorinatorLastName+",\n\n         New Ticket ("+totalTicket+") are assigned for order number("+shipNumber+").\n\n\n\nThanks, "+"\n" +userName;
			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"","", "", msgText1,subject, sessionCorpID, "Account Portal", shipNumber, "");
			}
			}
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" createWorkTicketFromOI method  End");
		return SUCCESS;
	}
		public String  checkLimitForOI()  throws Exception{
			try
			{
			String ticketStatus = "";
			Long countCheckForOI = null;	
			String str1[] = dataForCheckLimit.split(",");
			 Boolean wareHouseCheck = false;
			 if(str1[1]!=null && !str1[1].equals("")){
				 wareHouseCheck = workTicketManager.getCheckedWareHouse(str1[1].toString(), sessionCorpID);
				 if(wareHouseCheck == true){
					 wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
					 Iterator iter123  =wtServiceTypeDyna.iterator();
						while (iter123.hasNext()) {
							if (typeService.equals("")) {
								typeService = iter123.next().toString();
							}else{
								typeService = typeService + "," + iter123.next().toString();
							}
						}
						operationsHubLimits  = workTicketManager.getOperationsHub(str1[1].toString(), sessionCorpID);
						user = userManager.getUserByUsername(getRequest().getRemoteUser());
						roles = user.getRoles();
						if (operationsHubLimits.getId() == null){		
							flag = "hub";
							return INPUT;
							}
						if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
						}else{
						if((wtServiceTypeDyna.contains(str1[0].toString()))){
							Long dailyCount = Long.parseLong(operationsHubLimits.getDailyOpHubLimit());
							Long dailyCountPC = Long.parseLong(operationsHubLimits.getDailyOpHubLimitPC());
							limitForThreshold = workTicketManager.msgForThreshold(dailyCount,dailyCountPC,getDateFormat(str1[2]),str1[1].toString(),sessionCorpID);
							Long count = (dailyCount * dailyCountPC)/100;
							String str="";
			    			   if(operationsHubLimits.getMinServiceDayHub()==null ||operationsHubLimits.getMinServiceDayHub().toString().equalsIgnoreCase("")){
			    				   str="0";
			    			   }else{
			    				   str=operationsHubLimits.getMinServiceDayHub().toString();
			    			   }
			    				   
			    			int minServiceDay = Integer.parseInt(str);
				    		int weekEndDays = 0;
				    		Date newDate = new Date();
			    			
			    			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newDate));
				    		String todayDate = nowYYYYMMDD1.toString();
				    		Date tempDate  = dateformatYYYYMMDD1.parse(todayDate);
				    		Calendar calc = Calendar.getInstance();
				    		calc.setTime(tempDate);
				    		if(calc.get(Calendar.DAY_OF_WEEK) == 1 || calc.get(Calendar.DAY_OF_WEEK) == 7){
				    			weekEndDays = 1;
				    		}

				    		for (int i=1; i<= minServiceDay; i++){
				    			Calendar cal = Calendar.getInstance();
				    			cal.setTime(tempDate);
				    			cal.add(Calendar.DAY_OF_MONTH, 1);
				    			tempDate = cal.getTime();
				    			
				    			if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7){
				    				weekEndDays++;
				    		     }
				    		}

				    		int intervalDays = minServiceDay + weekEndDays;
				    		Calendar cal1 = Calendar.getInstance();
				    		cal1.setTime(dateformatYYYYMMDD1.parse(todayDate));
			    			cal1.add(Calendar.DAY_OF_MONTH, intervalDays);
			    			Date endDate = cal1.getTime();

				    		if (cal1.get(Calendar.DAY_OF_WEEK) == 1 || cal1.get(Calendar.DAY_OF_WEEK) == 7){
				    			Calendar cal2 = Calendar.getInstance();
					    		cal2.setTime(endDate);
				    			cal2.add(Calendar.DAY_OF_MONTH, 2);
				    			endDate = cal2.getTime();
			    		     }
				    		
				    		if( getDateFormat(str1[2]).before(endDate)){
			    			   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){	
			    				   returnAjaxStringValue = "checkMinimumServiceDays";
			    			   }else{
			    				   returnAjaxStringValue=msgForThreshold;
			    			   }
				    		}
				    		
				    		if(returnAjaxStringValue == null)
				    		{
						OIListByWorkOrder = operationsIntelligenceManager.getResourceFromOI(sessionCorpID, shipNumber, str1[3].toString());
						Iterator it = OIListByWorkOrder.iterator();
						while(it.hasNext())
						{
						OperationsIntelligence operationsIntelligence = (OperationsIntelligence)it.next();
						resorceTypeOI = operationsIntelligence.getType().toString();
						resorceDescriptionOI =operationsIntelligence.getDescription().toString();
						resourceQuantitiyOI = operationsIntelligence.getQuantitiy().intValue();
						ticketByDate = workTicketManager.getAllTicketByDate(typeService, getDateFormat(str1[2]),str1[1].toString(),sessionCorpID,"0");
						if(ticketByDate != null && !(ticketByDate.isEmpty()))
						{
						Iterator it1 = ticketByDate.iterator();
						while(it1.hasNext())
						{
							Object[] obj = (Object[]) it1.next();
							WTResorceTypeOI = obj[1].toString();
							WTResorceDescriptionOI = obj[2].toString();
							WTResourceQuantitiyOI= Integer.parseInt(obj[0].toString());
							if(resorceTypeOI.equalsIgnoreCase(WTResorceTypeOI) && resorceDescriptionOI.equalsIgnoreCase(WTResorceDescriptionOI))
						{
							resourceQuantitiyOI =resourceQuantitiyOI+WTResourceQuantitiyOI;
						}
						}}
						String hubIDForQTY = null;
						if(str1[1].toString().equalsIgnoreCase("01")){hubIDForQTY = "Union City";}
						if(str1[1].toString().equalsIgnoreCase("02")){ hubIDForQTY = "Ontario";}
						if(str1[1].toString().equalsIgnoreCase("03")){hubIDForQTY = "Sacramento";}
						if(str1[1].toString().equalsIgnoreCase("04")){hubIDForQTY = "Third Party Warehouse";
						}
						checkQuantityForOI = workTicketManager.checkQuantityInOperation(resorceTypeOI,resorceDescriptionOI,hubIDForQTY,sessionCorpID);
						if(checkQuantityForOI != null)
						{
							Iterator it2 = checkQuantityForOI.iterator();	
							while(it2.hasNext())
							{
								Object[] obj = (Object[]) it2.next();
								resorceType = 	obj[2].toString();
								resorceDescription = obj[1].toString();
								BigDecimal bd=new BigDecimal(obj[0].toString());
								resourceQuantitiy = Integer.valueOf(bd.intValue());
							}
							
							if(resorceTypeOI.equalsIgnoreCase(resorceType) && resorceDescriptionOI.equalsIgnoreCase(resorceDescription)){
								resourceQuantitiyOI = resourceQuantitiy-resourceQuantitiyOI;
								if(resourceQuantitiyOI <= 0){
									returnAjaxStringValue = String.valueOf("exceedLimit ,"+resourceQuantitiyOI);
								break;
								}
							}
						}}
						if(resourceQuantitiyOI >0 ){
							if (operationsHubLimits != null){
								countCheckForOI = workTicketManager.getDailyOpsLimitForOi(dailyCount,typeService, getDateFormat(str1[2]),str1[1].toString(),sessionCorpID);
							}
							
					 if(countCheckForOI == null){
						 countCheckForOI=(long) 1;
					 }else if(countCheckForOI  <= 0){
					 returnAjaxStringValue = String.valueOf(countCheckForOI);
					 }else if(limitForThreshold >=  count && limitForThreshold < dailyCount)
						{
							msgForThreshold ="thresholdMsg";
							returnAjaxStringValue=msgForThreshold;
						}
					 }}}}}
			 }else{
				String serviceWarehouse = workTicketManager.getServiceAndWareHouseByTicket(str1[4].toString(),sessionCorpID);
				String serviceWhouse[] = serviceWarehouse.split("~");
				str1[0] = serviceWhouse[0];
				str1[1] = serviceWhouse[1];
				ticketStatus = serviceWhouse[2];
				wareHouseCheck = workTicketManager.getCheckedWareHouse(str1[1].toString(), sessionCorpID);
				
				if((ticketStatus!=null && !ticketStatus.equals("")) && ticketStatus.equalsIgnoreCase("T")){
				if(wareHouseCheck == true){
					 wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
					 Iterator iter123  =wtServiceTypeDyna.iterator();
						while (iter123.hasNext()) {
							if (typeService.equals("")) {
								typeService = iter123.next().toString();
							}else{
								typeService = typeService + "," + iter123.next().toString();
							}
						}
						operationsHubLimits  = workTicketManager.getOperationsHub(str1[1].toString(), sessionCorpID);
						user = userManager.getUserByUsername(getRequest().getRemoteUser());
						roles = user.getRoles();
						if (operationsHubLimits.getId() == null){		
							flag = "hub";
							return INPUT;
							}
						if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
						}else{
						if((wtServiceTypeDyna.contains(str1[0].toString()))){
							Long dailyCount = Long.parseLong(operationsHubLimits.getDailyOpHubLimit());
							Long dailyCountPC = Long.parseLong(operationsHubLimits.getDailyOpHubLimitPC());
							limitForThreshold = workTicketManager.msgForThreshold(dailyCount,dailyCountPC,getDateFormat(str1[2]),str1[1].toString(),sessionCorpID);
							Long count = (dailyCount * dailyCountPC)/100;
							String str="";
			    			   if(operationsHubLimits.getMinServiceDayHub()==null ||operationsHubLimits.getMinServiceDayHub().toString().equalsIgnoreCase("")){
			    				   str="0";
			    			   }
			    			   else{
			    				   str=operationsHubLimits.getMinServiceDayHub().toString();
			    			   }
			    				   
			    			int minServiceDay = Integer.parseInt(str);
				    		int weekEndDays = 0;
				    		Date newDate = new Date();
			    			
			    			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newDate));
				    		String todayDate = nowYYYYMMDD1.toString();
				    		Date tempDate  = dateformatYYYYMMDD1.parse(todayDate);
				    		Calendar calc = Calendar.getInstance();
				    		calc.setTime(tempDate);
				    		if(calc.get(Calendar.DAY_OF_WEEK) == 1 || calc.get(Calendar.DAY_OF_WEEK) == 7){
				    			weekEndDays = 1;
				    		}

				    		for (int i=1; i<= minServiceDay; i++){
				    			Calendar cal = Calendar.getInstance();
				    			cal.setTime(tempDate);
				    			cal.add(Calendar.DAY_OF_MONTH, 1);
				    			tempDate = cal.getTime();
				    			
				    			if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7){
				    				weekEndDays++;
				    		     }
				    		}

				    		int intervalDays = minServiceDay + weekEndDays;
				    		Calendar cal1 = Calendar.getInstance();
				    		cal1.setTime(dateformatYYYYMMDD1.parse(todayDate));
			    			cal1.add(Calendar.DAY_OF_MONTH, intervalDays);
				    		Date endDate = cal1.getTime();

				    		if (cal1.get(Calendar.DAY_OF_WEEK) == 1 || cal1.get(Calendar.DAY_OF_WEEK) == 7){
				    			Calendar cal2 = Calendar.getInstance();
					    		cal2.setTime(endDate);
				    			cal2.add(Calendar.DAY_OF_MONTH, 2);
				    			endDate = cal2.getTime();
			    		     }
				    		
				    		if( getDateFormat(str1[2]).before(endDate)){
			    			   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){	
			    				   returnAjaxStringValue = "checkMinimumServiceDays";
			    			   }else{
			    				   returnAjaxStringValue=msgForThreshold;
			    			   }
				    		}
				    		
				    		if(returnAjaxStringValue == null)
				    		{
						OIListByWorkOrder = operationsIntelligenceManager.getResourceFromOI(sessionCorpID, shipNumber, str1[3].toString());
						Iterator it = OIListByWorkOrder.iterator();
						while(it.hasNext()){
						OperationsIntelligence operationsIntelligence = (OperationsIntelligence)it.next();
						resorceTypeOI = operationsIntelligence.getType().toString();
						resorceDescriptionOI =operationsIntelligence.getDescription().toString();
						resourceQuantitiyOI = operationsIntelligence.getQuantitiy().intValue();
						ticketByDate = workTicketManager.getAllTicketByDate(typeService, getDateFormat(str1[2]),str1[1].toString(),sessionCorpID,operationsIntelligence.getItemsJbkEquipId());
						if(ticketByDate != null && !(ticketByDate.isEmpty())){
						Iterator it1 = ticketByDate.iterator();
						while(it1.hasNext())
						{
							Object[] obj = (Object[]) it1.next();
							WTResorceTypeOI = obj[1].toString();
							WTResorceDescriptionOI = obj[2].toString();
							WTResourceQuantitiyOI= Integer.parseInt(obj[0].toString());
							if(resorceTypeOI.equalsIgnoreCase(WTResorceTypeOI) && resorceDescriptionOI.equalsIgnoreCase(WTResorceDescriptionOI))
						{
							resourceQuantitiyOI =resourceQuantitiyOI+WTResourceQuantitiyOI;
						}
						}}
						String hubIDForQTY = null;
						if(str1[1].toString().equalsIgnoreCase("01"))
						{hubIDForQTY = "Union City";}
						if(str1[1].toString().equalsIgnoreCase("02")){ hubIDForQTY = "Ontario";}
						if(str1[1].toString().equalsIgnoreCase("03")){hubIDForQTY = "Sacramento";}
						if(str1[1].toString().equalsIgnoreCase("04")){hubIDForQTY = "Third Party Warehouse";
						}
						checkQuantityForOI = workTicketManager.checkQuantityInOperation(resorceTypeOI,resorceDescriptionOI,hubIDForQTY,sessionCorpID);
						if(checkQuantityForOI != null)
						{
							Iterator it2 = checkQuantityForOI.iterator();	
							while(it2.hasNext())
							{
								Object[] obj = (Object[]) it2.next();
								resorceType = 	obj[2].toString();
								resorceDescription = obj[1].toString();
								BigDecimal bd=new BigDecimal(obj[0].toString());
								resourceQuantitiy = Integer.valueOf(bd.intValue());
							}
							
							if(resorceTypeOI.equalsIgnoreCase(resorceType) && resorceDescriptionOI.equalsIgnoreCase(resorceDescription)){
								resourceQuantitiyOI = resourceQuantitiy-resourceQuantitiyOI;
								if(resourceQuantitiyOI <= 0){
									returnAjaxStringValue = String.valueOf("exceedLimit ,"+resourceQuantitiyOI);
								break;
								}
							}
						}}
						if(resourceQuantitiyOI >0 ){
							if (operationsHubLimits != null){
								countCheckForOI = workTicketManager.getDailyOpsLimitForOi(dailyCount,typeService, getDateFormat(str1[2]),str1[1].toString(),sessionCorpID);
							}
					 if(countCheckForOI == null){
						 countCheckForOI=(long) 1;
					 }else if(countCheckForOI  <= 0){
						 returnAjaxStringValue = String.valueOf(countCheckForOI);
					 }else if(limitForThreshold >=  count && limitForThreshold < dailyCount){
							msgForThreshold ="thresholdMsg";
							returnAjaxStringValue=msgForThreshold;
						}
					 }}}}}
				}else{
					returnAjaxStringValue = "NoDateChange";
				}
			 }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		return SUCCESS;
	}
	public void updateDateByTicketAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		if(usertype.equalsIgnoreCase("ACCOUNT")){
			usertype = "AC_"+getRequest().getRemoteUser();
		}else if(usertype.equalsIgnoreCase("DRIVER")){
			usertype = "DR_"+getRequest().getRemoteUser();
		}else{
			usertype = getRequest().getRemoteUser();
		}
			workTicketManager.updateDateByTicket(ticket,date1,sessionCorpID,usertype,updateValue);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	private Set checkHubLimitStatus(String shipNumber){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		Set statusSet = new HashSet();
		try
		{
		if(tickets != null){
			String ticketAll[] = tickets.split(",");
			for (int i = 0; i < ticketAll.length; i++) {
				String fromDate ="";
		    	String toDate = "";
		    	double dailyResourceLimit=0;
				double usedEstQty=0;
				double availableQty=0;
				String ticketArr[]= ticketAll[i].split("-");
				String ticketId = ticketArr[0];
				String ticketNo = ticketArr[1];
				String day = ticketArr[2];
					
		WorkTicket workTicket = workTicketManager.get(Long.valueOf(ticketId));
		List<ItemsJbkEquip> itemList = itemsJbkEquipManager.findByShipNumAndTicket(shipNumber, workTicket.getTicket().toString());
    	company= companyManager.findByCorpID(sessionCorpID).get(0);
    	OperationsResourceLimits operationsResourceLimits  = itemsJbkEquipManager.getOperationsResourceHub(workTicket.getWarehouse(), sessionCorpID);
    	
    	if (!(workTicket.getDate1() == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
			fromDate = nowYYYYMMDD1.toString();
		}

		if (!(workTicket.getDate2() == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
			toDate = nowYYYYMMDD2.toString();
		}
		if(itemList!=null && itemList.size() > 0){
			for (ItemsJbkEquip resourceList : itemList) {
				Boolean wareHouseCheck = false;
				wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
				if(wareHouseCheck == true){
				  if(!(workTicket.getTargetActual().equalsIgnoreCase("C")) && (workTicket.getService().equalsIgnoreCase("PK") || workTicket.getService().equalsIgnoreCase("LD") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL") || workTicket.getService().equalsIgnoreCase("UP") || workTicket.getService().equalsIgnoreCase("PL"))){
					if(operationsResourceLimits!=null){
						List dailyResourceLimitList  = itemsJbkEquipManager.getDailyResourceLimit(resourceList.getType().trim(),resourceList.getDescript().trim(),fromDate, toDate,operationsResourceLimits.getHubId(), sessionCorpID);
						if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && dailyResourceLimitList.get(0)!=null){
							dailyResourceLimit = Double.parseDouble(dailyResourceLimitList.get(0).toString());
						}
						List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(resourceList.getType().trim(),resourceList.getDescript().trim(),fromDate, toDate,operationsResourceLimits.getHubId(),workTicket.getService(), sessionCorpID,workTicket.getTicket());
						if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && usedEstQuanitityList.get(0)!=null){
							usedEstQty = Double.parseDouble(usedEstQuanitityList.get(0).toString());
						}
						if(dailyResourceLimit>0){
							availableQty=dailyResourceLimit-usedEstQty;
							}
						if((usedEstQty > dailyResourceLimit) && dailyResourceLimit>0 && (company.getWorkticketQueue()!=null && company.getWorkticketQueue())){
							statusSet.add(ticketNo.toString());
						}			
				     }
				  }
				}
			}	
		}}}	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return statusSet;
	}
	
	//	  End of Method.
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		try
		{
		if(getRequest().getParameter("ajax") == null){ 
		getComboList(sessionCorpID);
		ostates = new HashMap<String, String>();
		dstates = new HashMap<String, String>(); 
		companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,"");
		ostates = new HashMap<String, String>();
		dstates = new HashMap<String, String>();
		enbState = customerFileManager.enableStateList(sessionCorpID);
	    sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {				
				dummyDistanceUnit=systemDefault.getDefaultDistanceCalc();
					if(systemDefault.getBillingCheck()){
						billingCheckFlag="Y";
					}
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					hubWarehouseLimit = "Crw";
				}else{
					hubWarehouseLimit = "Tkt";
				}
				if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
	            	operationChk ="H";
	            }else{
	            	operationChk ="W";
	            }
			}
		}
	}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
         if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
		}
         if (company != null && company.getDashBoardHideJobs() != null) {
        					dashBoardHideJobsList = company.getDashBoardHideJobs();	 
        				}  
		currencySign=company.getCurrencySign();	
		if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		if(company.getUTSI()!=null){
			 networkAgent=company.getUTSI();
		}else{
			networkAgent=false;
		}
		if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
			mmValidation= "Yes";
			if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
				mmValidation= "No";
			}
		}
		String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
		visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

	}
	
	// Setting time zero
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
    // A Method to get the list of WorkTickets, calling from main menu in then move management.	
	public String showSearchCriteria() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		serviceOrderSearchType = new ArrayList();
		serviceOrderSearchType.add("Default Match");
		serviceOrderSearchType.add("Start With");
		serviceOrderSearchType.add("End With");
		serviceOrderSearchType.add("Exact Match");
		if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
			serviceOrderSearchVal="Default Match";
		}		
		workTicketsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
     	user = userManager.getUserByUsername(getRequest().getRemoteUser());
		dfltWhse=user.getDefaultWarehouse();
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//  A Method to get the list of WorkTickets search view , calling from search WorkTickets List.
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicketsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		List<SearchCriterion> searchCriteria = new ArrayList();
		searchCriteria.add(new SearchCriterion("date1", new Date(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
		pagingLookupManager.getAllRecordsPage(WorkTicket.class, workTicketsExt, searchCriteria);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//  A Method to get billing status of ServiceOrder.
	@SkipValidation
	public String billComplete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		billCompleteList = workTicketManager.findBillComlete(shipnum);
		String[] str1= new String[6];
		if(billCompleteList!=null && !billCompleteList.isEmpty() && billCompleteList.get(0)!=null){
		 str1 = billCompleteList.get(0).toString().split("#");
		revisedChargeValue = str1[0] + "#" + str1[1] + "#" + str1[2] + "#" + str1[3];
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findAccountingHold(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try
		 
		 {
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		revisedChargeValue=workTicketManager.findAccountingHold(serviceOrder.getBillToCode());
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String checkWorkTicketTargetActualAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		try
		{
		targetActual = workTicketManager.findTargetActualByTicketNum(ticket,sessionCorpID);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.
@SkipValidation
public String countReleaseStorageDate(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	 try
	 {
	countReleaseStorageList=workTicketManager.countReleaseStorageDate(shipnum);
	 }
	 catch(Exception e)
	 {
		 e.printStackTrace();
	 }
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
}
	//  A Method to get the list of WorkTickets search view , calling from search WorkTickets List.
	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		boolean lastName = (workTicket.getLastName() == null);
		boolean firstName = (workTicket.getFirstName() == null);
		boolean shipNumber = (workTicket.getShipNumber() == null);
		boolean targetActual = (workTicket.getTargetActual() == null);
		boolean ticket = (workTicket.getTicket() == null);
		boolean warehouse = (workTicket.getWarehouse() == null);
		boolean service = (workTicket.getService() == null);
		boolean date1 = (workTicket.getDate1() == null);
		boolean date2 = (workTicket.getDate2() == null);
		serviceOrderSearchType = new ArrayList();
		serviceOrderSearchType.add("Default Match");
		serviceOrderSearchType.add("Start With");
		serviceOrderSearchType.add("End With");
		serviceOrderSearchType.add("Exact Match");
		if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
			serviceOrderSearchVal="Default Match";
		}		
		
		if (!ticket || !lastName || !firstName || !shipNumber || !targetActual || !warehouse || !service || !date1 || !date2) {
			workTicketsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			workTicketsExt.setPageSize(50);
			String sort = (String) getRequest().getParameter("sort");
			if (sort == null || sort.equals("")) {
				if(ticketSortOrder ==null || ticketSortOrder.equalsIgnoreCase("")){
					workTicketsExt.setSortCriterion("date1");
					}else{
						workTicketsExt.setSortCriterion(ticketSortOrder);
					}
				if(orderForTicket ==null || orderForTicket.equalsIgnoreCase("")){
					workTicketsExt.setSortDirection(SortOrderEnum.fromName("descending"));
					}else{
						workTicketsExt.setSortDirection(SortOrderEnum.fromName(orderForTicket));
					}
		           }
		
			List<SearchCriterion> searchCriteria = new ArrayList();
			
			if(serviceOrderSearchVal.equalsIgnoreCase("Start With")){
				searchCriteria.add(new SearchCriterion("lastName", workTicket.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
				searchCriteria.add(new SearchCriterion("firstName", workTicket.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
				searchCriteria.add(new SearchCriterion("shipNumber", workTicket.getShipNumber().replaceAll("'", "").replaceAll("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));	
				searchCriteria.add(new SearchCriterion("bondedGoods", workTicket.getBondedGoods(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (workTicket.getTargetActual().equals("")) {
					searchCriteria.add(new SearchCriterion("targetActual", "C", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					} else {
						searchCriteria.add(new SearchCriterion("targetActual", workTicket.getTargetActual(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						}
				if (workTicket.getTicket() != null) {
					searchCriteria.add(new SearchCriterion("ticket", workTicket.getTicket().toString(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
				}
				searchCriteria.add(new SearchCriterion("warehouse", workTicket.getWarehouse(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if(hubId!=null && !(hubId.equalsIgnoreCase(""))){
					String hubList=workTicketManager.getHubIdList(sessionCorpID, hubId).toString();
					hubList=hubList.replace("[", "");
					String hubList1=hubList.replace("]", "");
					String hubIdList="warehouse in ("+hubList1+")";
				searchCriteria.add(new SearchCriterion("", hubIdList, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}
				searchCriteria.add(new SearchCriterion("service", workTicket.getService(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if (workTicket.getDate1() != null && workTicket.getDate2() == null) {
					searchCriteria.add(new SearchCriterion("date1", workTicket.getDate1(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() == null && workTicket.getDate2() != null) {
					searchCriteria.add(new SearchCriterion("date2", workTicket.getDate2(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() != null && workTicket.getDate2() != null) {
					Date date1Str = workTicket.getDate1();
					Date date2Str = workTicket.getDate2();
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date1Str));
					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate2 = new StringBuilder(dateformatYYYYMMDD2.format(date2Str));
					String dateCriteria = "((date1 between '" + formatedDate1 + "' and '" + formatedDate2 + "') or (date2 between '" + formatedDate1 + "' and  '" + formatedDate2
					+ "') or (date1 < '" + formatedDate1 + "' and date2 > '" + formatedDate2 + "'))";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}			
				
			}else if(serviceOrderSearchVal.equalsIgnoreCase("End With")){
				searchCriteria.add(new SearchCriterion("lastName", workTicket.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
				searchCriteria.add(new SearchCriterion("firstName", workTicket.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
				searchCriteria.add(new SearchCriterion("shipNumber", workTicket.getShipNumber().replaceAll("'", "").replaceAll("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));	
				searchCriteria.add(new SearchCriterion("bondedGoods", workTicket.getBondedGoods(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (workTicket.getTargetActual().equals("")) {
					searchCriteria.add(new SearchCriterion("targetActual", "C", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					} else {
						searchCriteria.add(new SearchCriterion("targetActual", workTicket.getTargetActual(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						}
				if (workTicket.getTicket() != null) {
					searchCriteria.add(new SearchCriterion("ticket", workTicket.getTicket().toString(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
				}
				searchCriteria.add(new SearchCriterion("warehouse", workTicket.getWarehouse(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if(hubId!=null && !(hubId.equalsIgnoreCase(""))){
					String hubList=workTicketManager.getHubIdList(sessionCorpID, hubId).toString();
					hubList=hubList.replace("[", "");
					String hubList1=hubList.replace("]", "");
					String hubIdList="warehouse in ("+hubList1+")";
				searchCriteria.add(new SearchCriterion("", hubIdList, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}
				searchCriteria.add(new SearchCriterion("service", workTicket.getService(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if (workTicket.getDate1() != null && workTicket.getDate2() == null) {
					searchCriteria.add(new SearchCriterion("date1", workTicket.getDate1(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() == null && workTicket.getDate2() != null) {
					searchCriteria.add(new SearchCriterion("date2", workTicket.getDate2(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() != null && workTicket.getDate2() != null) {
					Date date1Str = workTicket.getDate1();
					Date date2Str = workTicket.getDate2();
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date1Str));
					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate2 = new StringBuilder(dateformatYYYYMMDD2.format(date2Str));
					String dateCriteria = "((date1 between '" + formatedDate1 + "' and '" + formatedDate2 + "') or (date2 between '" + formatedDate1 + "' and  '" + formatedDate2
					+ "') or (date1 < '" + formatedDate1 + "' and date2 > '" + formatedDate2 + "'))";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}			
			}else if(serviceOrderSearchVal.equalsIgnoreCase("Exact Match")){
				   if((workTicket.getLastName()!=null)&&(!workTicket.getLastName().trim().equalsIgnoreCase(""))){
					   searchCriteria.add(new SearchCriterion("lastName", workTicket.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					   }else{
					   searchCriteria.add(new SearchCriterion("lastName", workTicket.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					   }
				   if((workTicket.getFirstName()!=null)&&(!workTicket.getFirstName().trim().equalsIgnoreCase(""))){
					   searchCriteria.add(new SearchCriterion("firstName", workTicket.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					   }else{
					   searchCriteria.add(new SearchCriterion("firstName", workTicket.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					   }
				   if((workTicket.getShipNumber()!=null)&&(!workTicket.getShipNumber().trim().equalsIgnoreCase(""))){
					   searchCriteria.add(new SearchCriterion("shipNumber", workTicket.getShipNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					   }else{
					   searchCriteria.add(new SearchCriterion("shipNumber", workTicket.getShipNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					   }
				searchCriteria.add(new SearchCriterion("bondedGoods", workTicket.getBondedGoods(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (workTicket.getTargetActual().equals("")) {
					searchCriteria.add(new SearchCriterion("targetActual", "C", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					} else {
						searchCriteria.add(new SearchCriterion("targetActual", workTicket.getTargetActual(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						}
				if ((workTicket.getTicket() != null)&&(!workTicket.getTicket().toString().equalsIgnoreCase(""))){
					searchCriteria.add(new SearchCriterion("ticket", workTicket.getTicket().toString(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				}
				searchCriteria.add(new SearchCriterion("warehouse", workTicket.getWarehouse(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if(hubId!=null && !(hubId.equalsIgnoreCase(""))){
					String hubList=workTicketManager.getHubIdList(sessionCorpID, hubId).toString();
					hubList=hubList.replace("[", "");
					String hubList1=hubList.replace("]", "");
					String hubIdList="warehouse in ("+hubList1+")";
				searchCriteria.add(new SearchCriterion("", hubIdList, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}
				searchCriteria.add(new SearchCriterion("service", workTicket.getService(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if (workTicket.getDate1() != null && workTicket.getDate2() == null) {
					searchCriteria.add(new SearchCriterion("date1", workTicket.getDate1(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() == null && workTicket.getDate2() != null) {
					searchCriteria.add(new SearchCriterion("date2", workTicket.getDate2(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() != null && workTicket.getDate2() != null) {
					Date date1Str = workTicket.getDate1();
					Date date2Str = workTicket.getDate2();
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date1Str));
					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate2 = new StringBuilder(dateformatYYYYMMDD2.format(date2Str));
					String dateCriteria = "((date1 between '" + formatedDate1 + "' and '" + formatedDate2 + "') or (date2 between '" + formatedDate1 + "' and  '" + formatedDate2
					+ "') or (date1 < '" + formatedDate1 + "' and date2 > '" + formatedDate2 + "'))";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}			
			}else{
				searchCriteria.add(new SearchCriterion("lastName", workTicket.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("firstName", workTicket.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("shipNumber", workTicket.getShipNumber().replaceAll("'", "").replaceAll("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));	
				searchCriteria.add(new SearchCriterion("bondedGoods", workTicket.getBondedGoods(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (workTicket.getTargetActual().equals("")) {
					searchCriteria.add(new SearchCriterion("targetActual", "C", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					} else {
						searchCriteria.add(new SearchCriterion("targetActual", workTicket.getTargetActual(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						}
				if (workTicket.getTicket() != null) {
					searchCriteria.add(new SearchCriterion("ticket", workTicket.getTicket().toString(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				}
				searchCriteria.add(new SearchCriterion("warehouse", workTicket.getWarehouse(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if(hubId!=null && !(hubId.equalsIgnoreCase(""))){
					String hubList=workTicketManager.getHubIdList(sessionCorpID, hubId).toString();
					hubList=hubList.replace("[", "");
					String hubList1=hubList.replace("]", "");
					String hubIdList="warehouse in ("+hubList1+")";
				searchCriteria.add(new SearchCriterion("", hubIdList, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}
				searchCriteria.add(new SearchCriterion("service", workTicket.getService(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
				if (workTicket.getDate1() != null && workTicket.getDate2() == null) {
					searchCriteria.add(new SearchCriterion("date1", workTicket.getDate1(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() == null && workTicket.getDate2() != null) {
					searchCriteria.add(new SearchCriterion("date2", workTicket.getDate2(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
				}
				if (workTicket.getDate1() != null && workTicket.getDate2() != null) {
					Date date1Str = workTicket.getDate1();
					Date date2Str = workTicket.getDate2();
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date1Str));
					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate2 = new StringBuilder(dateformatYYYYMMDD2.format(date2Str));
					String dateCriteria = "((date1 between '" + formatedDate1 + "' and '" + formatedDate2 + "') or (date2 between '" + formatedDate1 + "' and  '" + formatedDate2
					+ "') or (date1 < '" + formatedDate1 + "' and date2 > '" + formatedDate2 + "'))";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				}			
				
			}
			pagingLookupManager.getAllRecordsPage(WorkTicket.class, workTicketsExt, searchCriteria);
		}
		if(workTicket.getDate1() != null && workTicket.getDate2() != null){
			if(workTicket.getDate1().equals(workTicket.getDate2())){
				showMap = "1";
			}	
		}}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

//  A Method to get the list of WorkTickets storage List.
	@SkipValidation
	public String workTicketStorageList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try
		 {
		if(warehouse==null){
			warehouse="";
		}
		workTicketStorageExt= workTicketManager.findWorkTicketStorageList(date1,date2,"",warehouse,sessionCorpID);
		 }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.
		@SkipValidation
	public String planningCalendarList() {
			try
			{
		count=0;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		houseHub = refMasterManager.findByParameterHubWithWarehouse(sessionCorpID, "HOUSE",hub);
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		for(Map.Entry<String, String> rec:hub.entrySet()){
			if(rec.getKey()!=null && !rec.getKey().equalsIgnoreCase("")){
				count++;
				defaultHub=rec.getKey();
			}
		}
		if(count>1){
			defaultHub="";
		}
		if((checkForAppendFields!=null && !checkForAppendFields.equalsIgnoreCase("")) && (checkForAppendFields.equalsIgnoreCase("Yes"))){
			if(wHouse==null || wHouse.trim().equalsIgnoreCase("")){
				if(hubID==null || hubID.trim().equalsIgnoreCase("")){
					planningCalendarViewList= workTicketManager.planningCalendarListForUnip(sessionCorpID,houseHub.get(defaultHub),printStart,printEnd);
				}else{
					planningCalendarViewList= workTicketManager.planningCalendarListForUnip(sessionCorpID,houseHub.get(hubID),printStart,printEnd);
				}
			}else{
				planningCalendarViewList= workTicketManager.planningCalendarListForUnip(sessionCorpID,"'"+wHouse+"'",printStart,printEnd);
			}
		}else{
			if(wHouse==null || wHouse.trim().equalsIgnoreCase("")){
				if(hubID==null || hubID.trim().equalsIgnoreCase("")){
					planningCalendarViewList= workTicketManager.planningCalendarList(sessionCorpID,houseHub.get(defaultHub));
				}else{
					planningCalendarViewList= workTicketManager.planningCalendarList(sessionCorpID,houseHub.get(hubID));
				}
			}else{
				planningCalendarViewList= workTicketManager.planningCalendarList(sessionCorpID,"'"+wHouse+"'");
			}
		}
		
		
		calendarViewStyle=type;
		
		
		
		if(calendarViewStyle==null || calendarViewStyle.equalsIgnoreCase("")){
			calendarViewStyle = "basicWeek";
		}}
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	
	//	 Method for editing and adding the WorkTickets record calling from WorkTickets list and add buttons.
		public synchronized String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try
		 {
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		String contract = "";
		if (id != null) {
			workTicket = workTicketManager.get(id);
			try{
				String myFileIds = workTicket.getAttachedFile();
				if(myFileIds!=null && !myFileIds.equals("")){
					if(myFileIds.contains(",")){
						String [] myFileIdArr = myFileIds.split(",");
						docCount = myFileIdArr.length;
					}else{
						docCount = 1;
					}
					
				}
				}catch(Exception e){
					e.printStackTrace();
					logger.warn("Error in attaching doc. "+e.getStackTrace()[0]);
				}
			
			if((workTicket.getOriginCountry()!=null && !workTicket.getOriginCountry().equalsIgnoreCase("")) && (workTicket.getOriginCountryCode()==null || workTicket.getOriginCountryCode().equalsIgnoreCase(""))){
				   workTicket.setOriginCountryCode(countryDsc.get(workTicket.getOriginCountry()));
				   }else if((workTicket.getOriginCountryCode()!=null && !workTicket.getOriginCountryCode().equalsIgnoreCase(""))||(workTicket.getOriginCountry()==null || workTicket.getOriginCountry().equalsIgnoreCase(""))){
				    workTicket.setOriginCountry(countryCod.get(workTicket.getOriginCountryCode()));
				   }

		    if((workTicket.getDestinationCountry()!=null && !workTicket.getDestinationCountry().equalsIgnoreCase("")) &&  (workTicket.getDestinationCountryCode()==null || workTicket.getDestinationCountryCode().equalsIgnoreCase(""))){
				       workTicket.setDestinationCountryCode(countryDsc.get(workTicket.getDestinationCountry()));
				      }else if((workTicket.getDestinationCountryCode()!=null && !workTicket.getDestinationCountryCode().equalsIgnoreCase(""))||(workTicket.getDestinationCountry()==null || workTicket.getDestinationCountry().equalsIgnoreCase(""))){
				       workTicket.setDestinationCountry(countryCod.get(workTicket.getDestinationCountryCode()));
		    }
		    ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
			serviceOrder = workTicket.getServiceOrder(); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			billing = billingManager.get(serviceOrder.getId());
			billToAuthority=billing.getBillToAuthority();
			authUpdated=billing.getAuthUpdated();
			customerFile = serviceOrder.getCustomerFile();
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			workTicket.setOpenDate(new Date());
			containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
			companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(sessionCorpID, "",serviceOrder.getCompanyDivision());
			if(!companyDivis.contains(workTicket.getCompanyDivision())){
		    	companyDivis.add(workTicket.getCompanyDivision());
		    }
			storages=workTicketManager.findStorageUnit(workTicket.getTicket(),workTicket.getShipNumber());
			if(workTicket.getService().equalsIgnoreCase("HO") && sessionCorpID.equalsIgnoreCase("VOER")){
			customsInTicketNumber=workTicketManager.getCustomsTicketList(serviceOrder.getId(),sessionCorpID);	
			}
			handoutPiecesAndVolumeList = workTicketManager.handoutWeightandVolume(workTicket.getTicket(),workTicket.getShipNumber(),sessionCorpID);
			Iterator itWtPieces = handoutPiecesAndVolumeList.iterator();
	         while (itWtPieces.hasNext()) {
	        	 Object [] row=(Object[])itWtPieces.next();
	        	 if(row[0]!=null){
	        		 totalHandoutPieces = Integer.parseInt(row[0].toString());
	        	 }else{
	        		 totalHandoutPieces = 0; 
	        	 }
	        	 if(row[1]!=null){
	        		 totalHandoutVolume = Double.parseDouble(row[1].toString());
	        	 }else{
	        		 totalHandoutVolume = 0.00;
	        	 }
	         }
			workTicket.setEstimatedCartoons(Integer.parseInt(workTicketManager.findTotalEstimatedCartons(workTicket.getTicket())));
			check=workTicket.getState();
			check1=workTicket.getDestinationState();
			List pM = workTicketManager.findBillingPayMethod(serviceOrder.getShipNumber());
			if (!pM.isEmpty() && pM.get(0)!=null) {
				payMethod = pM.get(0).toString();
			}
			workTicket.setPayMethod(payMethod);
			if ((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()) {
				workTicket.setContract(serviceOrder.getCustomerFile().getContract());
			} else {
				workTicket.setContract(billing.getContract());				
			}
			Map<String,String> tempMap=getInstructionCodeWithEnblCountry();
			Iterator iterator = tempMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				String keys=(String) mapEntry.getKey();
				String inscodeval[]=keys.split(":");
				String inscode=inscodeval[0];
				String insval=inscodeval[1];
				String vals=(String)mapEntry.getValue();
				String countparam[]=vals.split(",");
				String flag="0";
				for(int i=0;i<countparam.length;i++){
					if(workTicket.getDestinationCountryCode().equals(countparam[i])){
						flag="1";
					}
				}
				if(flag.equals("1")){			
					
					flag="0";
					
				}
				else{
					if(countparam!=null && countparam.length>0){
						woinstr.remove(inscode);	
					}
				}
					
			}
			String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
			visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			if(visibilityForSSCW){
				if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(true)){
					workTicketOriginShuttle="Yes";
				}else if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(false)){
					workTicketOriginShuttle="No";
				}else{
					workTicketOriginShuttle="";
				}
				if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(true)){
					workTicketDestinationShuttle="Yes";
				}else if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(false)){
					workTicketDestinationShuttle="No";
				}else{
					workTicketDestinationShuttle="";
				}
			}		    
			timeRequirementList=workTicketTimeManagementManager.getTimeManagementManagerDetails(workTicket.getId(),workTicket.getTicket(),sessionCorpID);
			try{
				parentParameterValuesList=refMasterManager.getParentParameterList(sessionCorpID,"TCKTSERVC",workTicket.getService());
			if(parentParameterValuesList!=null && parentParameterValuesList.size()>0 && !parentParameterValuesList.isEmpty()){
				String aa = parentParameterValuesList.toString().replace("[", "").replace("]", "");
				String countparam[]=aa.split(",");
				
				woinstr=new LinkedHashMap<String, String>();
				Iterator iter = tempWoinstrMap.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iter.next();
					String keys=(String) mapEntry.getKey();
					String vals=(String)mapEntry.getValue();
					for(int i=0;i<countparam.length;i++){
						if(keys.equals(ltrim(countparam[i].toString())) && !woinstr.containsKey(keys)){
							woinstr.put(keys, vals);
						}
							
					}
			}
			}}
			catch(Exception e){
				e.printStackTrace();
			}
		} else {
			workTicket = new WorkTicket();
			serviceOrder = serviceOrderManager.get(id);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(id);
			trackingStatus = trackingStatusManager.get(id);
			billing = billingManager.get(id);
			billToAuthority=billing.getBillToAuthority();
			authUpdated=billing.getAuthUpdated();
			customerFile = serviceOrder.getCustomerFile();
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			if ((itemsJEquipManager.checkforBilling(serviceOrder.getShipNumber())).isEmpty()) {
				workTicket.setContract(serviceOrder.getCustomerFile().getContract());
			} else {
				workTicket.setContract(billing.getContract());				
			}
			containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
			List pM = workTicketManager.findBillingPayMethod(serviceOrder.getShipNumber());
			if (pM!=null && !pM.isEmpty()&& pM.get(0)!=null) {
				payMethod = pM.get(0).toString();
			}
			workTicket.setPayMethod(payMethod);
			workTicket.setReviewStatus("UnBilled");
			workTicket.setOriginPreferredContactTime(serviceOrder.getOriginPreferredContactTime());
			workTicket.setDestPreferredContactTime(serviceOrder.getDestPreferredContactTime());
			workTicket.setCompanyDivision(serviceOrder.getCompanyDivision());
			companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, serviceOrder.getBookingAgentCode());
			workTicket.setSequenceNumber(serviceOrder.getSequenceNumber());
			workTicket.setCorpID(sessionCorpID);
			workTicket.setCreatedOn(new Date());
			workTicket.setUpdatedOn(new Date());
			workTicket.setTargetActual("T");
			workTicket.setStatusDate(new Date());
			workTicket.setOpenDate(new Date());			
			Map<String,String> tempMap=getInstructionCodeWithEnblCountry();
			Iterator iterator = tempMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				String keys=(String) mapEntry.getKey();
				String inscodeval[]=keys.split(":");
				String inscode=inscodeval[0];
				String insval=inscodeval[1];
				String vals=(String)mapEntry.getValue();
				String countparam[]=vals.split(",");
				String flag="0";
				for(int i=0;i<countparam.length;i++){
					if(serviceOrder.getDestinationCountryCode().equals(countparam[i])){
						flag="1";
					}
				}
				if(flag.equals("1")){			
					workTicket.setInstructionCode(inscode);	
					workTicket.setInstructions(insval);
					flag="0";
					
				}
				else{
					if(countparam!=null && countparam.length>0){
						woinstr.remove(inscode);	
					}
				}
					
			}
			String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
			visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			if(visibilityForSSCW){
			AccessInfo accInfo = workTicketManager.getAccessinfo(sessionCorpID,id);
			if(accInfo!=null){
			if(accInfo.getOriginElevator().equals(true)){
				workTicket.setOriginElevator("Present");
			}else{
				workTicket.setOriginElevator("");
			}
			if(accInfo.getOriginReserveParking().equals(true)){
				workTicket.setParking("Parking Permit");
			}else{
				workTicket.setParking("");
			}
			if(accInfo.getOriginLongCarry().equals(true)){
				workTicket.setOriginLongCarry("Yes");
			}else{
				workTicket.setOriginLongCarry("");
			}
			if(accInfo.getOriginShuttleRequired().equals(true)){
				workTicketOriginShuttle="Yes";
			}else{
				workTicketOriginShuttle="";
			}
			if(accInfo.getDestinationElevator().equals(true)){
				workTicket.setDestinationElevator("Present");
			}else{
				workTicket.setDestinationElevator("");
			}
			if(accInfo.getDestinationReserveParking().equals(true)){
				workTicket.setDest_park("Parking Permit");
			}else{
				workTicket.setDest_park("");
			}
			if(accInfo.getDestinationLongCarry().equals(true)){
				workTicket.setDestinationLongCarry("Yes");
			}else{
				workTicket.setDestinationLongCarry("");
			}
			if(accInfo.getDestinationShuttleRequired().equals(true)){
				workTicketDestinationShuttle="Yes";
			}else{
				workTicketDestinationShuttle="";
			}
			}
			}		
			
		}
		dataItemList=workTicketManager.getAllDataItemList(id,workTicket.getShipNumber());
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        maxTicket = workTicketManager.findMaximumTicket(serviceOrder.getShipNumber()).get(0).toString();
        minTicket =  workTicketManager.findMinimumTicket(serviceOrder.getShipNumber()).get(0).toString();
        countTicket =  workTicketManager.findCountTicket(serviceOrder.getShipNumber()).get(0).toString();
		getNotesForIconChange();
		crewNameList=workTicketManager.findCrewFirstLastName(workTicket.getTicket(),sessionCorpID);
		if(crewNameList!=null && !crewNameList.isEmpty()){
		crewName=crewNameList.toString().replace("[", "").replace("]", "").trim(); 
		if(crewName!=null && !crewName.equalsIgnoreCase("")){
			showList="Show";			
		}else{
			showList="Hide";
		}
		}else{
			showList="Hide";
		}}
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public static String ltrim(String s) {
	    int i = 0;
	    while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
	        i++;
	    }
	    return s.substring(i);
	} 
	
	@SkipValidation
	public String getInstructionCodeList(){
		try {
			AjaxParameterValuesList=refMasterManager.getParentParameterListAjax(sessionCorpID,"TCKTSERVC",serviceCode,instructionCode);
			
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
	    	 return CANCEL;
		}

		return SUCCESS;	
	}

	//	  End of Method.

	//	 Method for editing and adding the WorkTickets record calling from WorkTickets list and add buttons.
	
	@SkipValidation
	public synchronized String editWorkTicket() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		String contract = "";
		if (id != null) {
			serviceOrder = serviceOrderManager.get(id);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(id);
			trackingStatus = trackingStatusManager.get(id);
			billing = billingManager.get(id);
			billToAuthority=billing.getBillToAuthority();
			authUpdated=billing.getAuthUpdated();
			customerFile = serviceOrder.getCustomerFile();
			String shipnum = serviceOrder.getShipNumber();
			String authorName = "";
			servicePartners = workTicketManager.findMaximumship(shipnum);
			if (!servicePartners.isEmpty()) {
				Iterator it = servicePartners.iterator();
				if (it.hasNext()) {
					authorName = (String) (it.next());
				}
			}
			containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
			workTicket = new WorkTicket();
			serviceOrder = serviceOrderManager.get(id);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			if ((itemsJEquipManager.checkforBilling(serviceOrder.getShipNumber())).isEmpty()) {
				workTicket.setContract(serviceOrder.getCustomerFile().getContract());

			} else {
				workTicket.setContract(billing.getContract()); 
			}
			List pM = workTicketManager.findBillingPayMethod(serviceOrder.getShipNumber());
			if (!pM.isEmpty() && pM.get(0)!=null) {
				payMethod = pM.get(0).toString();
			}
			workTicket.setPayMethod(payMethod);
			workTicket.setStorageMeasurement(measure.get(billingManager.get(id).getStorageMeasurement()));
			workTicket.setReviewStatus("UnBilled");
			workTicket.setTargetActual("T");
			workTicket.setStatusDate(new Date());
			workTicket.setScheduledArrive("00:00");
			workTicket.setLeaveWareHouse("00:00");
			workTicket.setBeginningTime("00:00");
			workTicket.setEndTime("00:00");
			workTicket.setTimeFrom("00:00");
			workTicket.setTimeTo("00:00");
			workTicket.setCarrier(authorName);
			workTicket.setCreatedOn(new Date());
			
			workTicket.setUpdatedOn(new Date());
			workTicket.setOpenDate(new Date());
			workTicket.setCorpID(sessionCorpID);
			workTicket.setPrefix(serviceOrder.getPrefix());
			workTicket.setMi(serviceOrder.getMi());
			workTicket.setFirstName(serviceOrder.getCustomerFile().getFirstName());
			workTicket.setLastName(serviceOrder.getCustomerFile().getLastName());
			workTicket.setSuffix(serviceOrder.getSuffix());
			workTicket.setJobType(serviceOrder.getJob());
			workTicket.setJobMode(serviceOrder.getMode());
			workTicket.setEstimator(serviceOrder.getEstimator());
			workTicket.setSalesMan(serviceOrder.getSalesMan());
			workTicket.setCoordinator(serviceOrder.getCoordinator());
			workTicket.setOriginCompany(serviceOrder.getOriginCompany());
			workTicket.setAddress1(serviceOrder.getOriginAddressLine1());
			workTicket.setAddress2(serviceOrder.getOriginAddressLine2());
			workTicket.setAddress3(serviceOrder.getOriginAddressLine3());
			workTicket.setCity(serviceOrder.getOriginCity());
			workTicket.setState(serviceOrder.getOriginState());
			workTicket.setZip(serviceOrder.getOriginZip());
			check=workTicket.getState();
			check1=workTicket.getDestinationState();
			workTicket.setOriginCountry(serviceOrder.getOriginCountry());
			workTicket.setOriginCountryCode(serviceOrder.getOriginCountryCode());
			workTicket.setPhone(serviceOrder.getOriginDayPhone());
			workTicket.setHomePhone(serviceOrder.getOriginHomePhone());
			workTicket.setOriginContactWeekend(serviceOrder.getOriginContactWork());
			workTicket.setOriginFax(serviceOrder.getOriginFax());
			workTicket.setOriginContactName(serviceOrder.getOriginContactName());
			workTicket.setOriginContactPhone(serviceOrder.getOriginContactPhone());
			workTicket.setDestinationCompany(serviceOrder.getDestinationCompany());
			workTicket.setDestinationAddress1(serviceOrder.getDestinationAddressLine1());
			workTicket.setDestinationAddress2(serviceOrder.getDestinationAddressLine2());
			workTicket.setDestinationAddress3(serviceOrder.getDestinationAddressLine3());
			workTicket.setDestinationCity(serviceOrder.getDestinationCity());
			workTicket.setDestinationZip(serviceOrder.getDestinationZip());
			
			workTicket.setDestinationCountry(serviceOrder.getDestinationCountry());
			Map<String,String> tempMap=getInstructionCodeWithEnblCountry();
			Iterator iterator = tempMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				String keys=(String) mapEntry.getKey();
				String inscodeval[]=keys.split(":");
				String inscode=inscodeval[0];
				String insval=inscodeval[1];
				String vals=(String)mapEntry.getValue();
				String countparam[]=vals.split(",");
				String flag="0";
				for(int i=0;i<countparam.length;i++){
					if(serviceOrder.getDestinationCountryCode().equals(countparam[i])){
						flag="1";
					}
				}
				if(flag.equals("1")){
					
					workTicket.setInstructionCode(inscode);	
					workTicket.setInstructions(insval);
					flag="0";
					
				}
				else{
					if(countparam!=null && countparam.length>0){
						woinstr.remove(inscode);	
					}
				}
					
			}
			
			workTicket.setDestinationState(serviceOrder.getDestinationState());
			workTicket.setDestinationCountryCode(serviceOrder.getDestinationCountryCode());
			workTicket.setDestinationPhone(serviceOrder.getDestinationDayPhone());
			workTicket.setDestinationHomePhone(serviceOrder.getDestinationHomePhone());
			workTicket.setDestinationFax(serviceOrder.getDestinationFax());
			workTicket.setContactName(serviceOrder.getContactName());
			workTicket.setContactPhone(serviceOrder.getContactPhone());
			workTicket.setContactFax(serviceOrder.getContactFax());
			workTicket.setAccount(serviceOrder.getBillToCode());
			workTicket.setAccountName(serviceOrder.getBillToName());
			
			workTicket.setOrderBy(serviceOrder.getOrderBy());
			
			workTicket.setOriginMobile(serviceOrder.getOriginMobile());
			workTicket.setDestinationMobile(serviceOrder.getDestinationMobile());
			if(workTicket.getOriginPreferredContactTime() == null)
				workTicket.setOriginPreferredContactTime(serviceOrder.getOriginPreferredContactTime());
			if(workTicket.getDestPreferredContactTime() == null)
				workTicket.setDestPreferredContactTime(serviceOrder.getDestPreferredContactTime());
			
			workTicket.setPhoneExt(serviceOrder.getOriginDayExtn());
			workTicket.setDestinationPhoneExt(serviceOrder.getDestinationDayExtn());
			workTicket.setSequenceNumber(serviceOrder.getSequenceNumber());
			workTicket.setCompanyDivision(serviceOrder.getCompanyDivision());
			workTicket.setUnit1(miscellaneous.getUnit1());
			workTicket.setUnit2(miscellaneous.getUnit2());
			
				containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
			companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(sessionCorpID, "",serviceOrder.getCompanyDivision());
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
			workTicket.setWarehouse(user.getDefaultWarehouse());
			storages=workTicketManager.findStorageUnit(workTicket.getTicket(),workTicket.getShipNumber());
			handoutPiecesAndVolumeList = workTicketManager.handoutWeightandVolume(workTicket.getTicket(),workTicket.getShipNumber(),sessionCorpID);
			Iterator wtPiecesIterator = handoutPiecesAndVolumeList.iterator();
	         while (wtPiecesIterator.hasNext()) {
	        	 Object [] row=(Object[])wtPiecesIterator.next();
	        	 if(row[0]!=null){
	        		 totalHandoutPieces = Integer.parseInt(row[0].toString());
	        	 }else{
	        		 totalHandoutPieces = 0; 
	        	 }
	        	 if(row[1]!=null){
	        		 totalHandoutVolume = Double.parseDouble(row[1].toString());
	        	 }else{
	        		 totalHandoutVolume = 0.00;
	        	 }
	         }
	         String permKeyForValues = sessionCorpID +"-"+"component.workTicket.ticketDetails.weightAndVolume";
				visibilityForWeightVolume=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKeyForValues) ;
				if(visibilityForWeightVolume){
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
				if (miscellaneous.getEstimatedNetWeight() == null && miscellaneous.getEstimateGrossWeight() == null) {
					workTicket.setEstimatedWeight(new Double(0.0));
				} else if (miscellaneous.getEstimatedNetWeight() == null){
					workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeight().doubleValue()));
				} else{
					workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeight().doubleValue()));
				}
				
				
				if (miscellaneous.getActualNetWeight() == null && miscellaneous.getActualGrossWeight() == null) {
					workTicket.setActualWeight(new Double(0.0));
				} else if (miscellaneous.getActualNetWeight() == null){
					workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeight().doubleValue()));
				}else {
					workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeight().doubleValue()));
				}
				
				
				if (miscellaneous.getNetEstimateCubicFeet() == null && miscellaneous.getEstimateCubicFeet() == null) {
					workTicket.setEstimatedCubicFeet(new Double(0.0));
				} else if (miscellaneous.getNetEstimateCubicFeet() == null) {
					workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicFeet().doubleValue()));
				} else {
					workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicFeet().doubleValue()));
				}
				
				if (miscellaneous.getNetActualCubicFeet() == null && miscellaneous.getActualCubicFeet() ==null) {
					workTicket.setActualVolume(new Double(0.0));
				} else if(miscellaneous.getNetActualCubicFeet() == null){
					workTicket.setActualVolume(new Double(miscellaneous.getActualCubicFeet().doubleValue()));
				}  else {
					workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicFeet().doubleValue()));
				}
			} else
			{
				
				
				if (miscellaneous.getEstimatedNetWeightKilo() == null && miscellaneous.getEstimateGrossWeightKilo() == null) {
					workTicket.setEstimatedWeight(new Double(0.0));
				}  else if(miscellaneous.getEstimatedNetWeightKilo() == null ) {
					workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeightKilo().doubleValue()));
				}else {
					workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeightKilo().doubleValue()));
				}
				
				
				if (miscellaneous.getActualNetWeightKilo() == null && miscellaneous.getActualGrossWeightKilo() == null) {
					workTicket.setActualWeight(new Double(0.0));
				}else if(miscellaneous.getActualNetWeightKilo() == null) {
					workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeightKilo().doubleValue()));
				}else {
					workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeightKilo().doubleValue()));
				}
				
				
				if (miscellaneous.getNetEstimateCubicMtr() == null && miscellaneous.getEstimateCubicMtr() == null) {
					workTicket.setEstimatedCubicFeet(new Double(0.0));
				} else if (miscellaneous.getNetEstimateCubicMtr() == null ) {
					workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicMtr().doubleValue()));
				}else {
					workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicMtr().doubleValue()));
				}
				
				
				if (miscellaneous.getNetActualCubicMtr() == null && miscellaneous.getActualCubicMtr() == null) {
					workTicket.setActualVolume(new Double(0.0));
				} else if (miscellaneous.getNetActualCubicMtr() == null) {
					workTicket.setActualVolume(new Double(miscellaneous.getActualCubicMtr().doubleValue()));
				}else{
					workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicMtr().doubleValue()));
				}
			}
			
				
		}else{

			if (serviceOrder.getMode().equalsIgnoreCase("AIR")) {
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			if (miscellaneous.getEstimateGrossWeight() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeight().doubleValue()));
			}
			if (miscellaneous.getActualGrossWeight() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeight().doubleValue()));
			}
			if (miscellaneous.getEstimateCubicFeet() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicFeet().doubleValue()));
			}
			if (miscellaneous.getActualCubicFeet() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getActualCubicFeet().doubleValue()));
			}
		} else
		{
			if (miscellaneous.getEstimateGrossWeightKilo() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimateGrossWeightKilo().doubleValue()));
			}
			if (miscellaneous.getActualGrossWeightKilo() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualGrossWeightKilo().doubleValue()));
			}
			if (miscellaneous.getEstimateCubicMtr() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getEstimateCubicMtr().doubleValue()));
			}
			if (miscellaneous.getActualCubicMtr() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getActualCubicMtr().doubleValue()));
			}
		}
		}
			else {
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			if (miscellaneous.getEstimatedNetWeight() == null) {
				workTicket.setEstimatedWeight(new Double(0.0));
			} else {
				workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeight().doubleValue()));
			}
			if (miscellaneous.getActualNetWeight() == null) {
				workTicket.setActualWeight(new Double(0.0));
			} else {
				workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeight().doubleValue()));
			}
			if (miscellaneous.getNetEstimateCubicFeet() == null) {
				workTicket.setEstimatedCubicFeet(new Double(0.0));
			} else {
				workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicFeet().doubleValue()));
			}
			if (miscellaneous.getNetActualCubicFeet() == null) {
				workTicket.setActualVolume(new Double(0.0));
			} else {
				workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicFeet().doubleValue()));
			}
		}
				else{
					if (miscellaneous.getEstimatedNetWeightKilo() == null) {
						workTicket.setEstimatedWeight(new Double(0.0));
					} else {
						workTicket.setEstimatedWeight(new Double(miscellaneous.getEstimatedNetWeightKilo().doubleValue()));
					}
					if (miscellaneous.getActualNetWeightKilo() == null) {
						workTicket.setActualWeight(new Double(0.0));
					} else {
						workTicket.setActualWeight(new Double(miscellaneous.getActualNetWeightKilo().doubleValue()));
					}
					if (miscellaneous.getNetEstimateCubicMtr() == null) {
						workTicket.setEstimatedCubicFeet(new Double(0.0));
					} else {
						workTicket.setEstimatedCubicFeet(new Double(miscellaneous.getNetEstimateCubicMtr().doubleValue()));
					}
					if (miscellaneous.getNetActualCubicMtr() == null) {
						workTicket.setActualVolume(new Double(0.0));
					} else {
						workTicket.setActualVolume(new Double(miscellaneous.getNetActualCubicMtr().doubleValue()));
					}
				}
		}	
			
	
			
		}
			String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
			visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			if(visibilityForSSCW){
			AccessInfo accInfo = workTicketManager.getAccessinfo(sessionCorpID,id);
			if(accInfo!=null){
			if(accInfo.getOriginElevator().equals(true)){
				workTicket.setOriginElevator("Present");
			}else{
				workTicket.setOriginElevator("");
			}
			if(accInfo.getOriginReserveParking().equals(true)){
				workTicket.setParking("Parking Permit");
			}else{
				workTicket.setParking("");
			}
			if(accInfo.getOriginLongCarry().equals(true)){
				workTicket.setOriginLongCarry("Yes");
			}else{
				workTicket.setOriginLongCarry("");
			}
			if(accInfo.getOriginShuttleRequired().equals(true)){
				workTicketOriginShuttle="Yes";
			}else{
				workTicketOriginShuttle="";
			}
			if(accInfo.getDestinationElevator().equals(true)){
				workTicket.setDestinationElevator("Present");
			}else{
				workTicket.setDestinationElevator("");
			}
			if(accInfo.getDestinationReserveParking().equals(true)){
				workTicket.setDest_park("Parking Permit");
			}else{
				workTicket.setDest_park("");
			}
			if(accInfo.getDestinationLongCarry().equals(true)){
				workTicket.setDestinationLongCarry("Yes");
			}else{
				workTicket.setDestinationLongCarry("");
			}
			if(accInfo.getDestinationShuttleRequired().equals(true)){
				workTicketDestinationShuttle="Yes";
			}else{
				workTicketDestinationShuttle="";
			}
			}
			}		
		} else {
			serviceOrder = new ServiceOrder();
		}
		ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
		dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
		originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
		destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
		addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
	}

	//	  End of Method.
	
	 	@SkipValidation 
	    public String editNextTicket(){
	 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	 		try
	 		{
	 		tempTic=Long.parseLong((workTicketManager.goNextSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
	 		}
	 		 catch(Exception e)
	 		 {
	 			 e.printStackTrace();
	 		 }
	 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	 		return SUCCESS;   
	    }
	    @SkipValidation 
	    public String editPrevTicket(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	try
	    	{
	    	tempTic=Long.parseLong((workTicketManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;   
	    } 
	      
	    @SkipValidation 
	    public String ticketOtherSO(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	try
	    	{
	    	 ticketSO=workTicketManager.goSOChild(sidNum,sessionCorpID,soIdNum);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;   
	    } 
	    @SkipValidation
	    private List findLinkerShipNumber(ServiceOrder serviceOrder) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List linkedShipNumberList= new ArrayList();
			try
			{
	    	
	    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	    	}else{
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	    	}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return linkedShipNumberList;
	    }
	    @SkipValidation
	    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List<Object> recordList= new ArrayList();
			try
			{
	    	Iterator it =linkedShipNumber.iterator();
	    	while(it.hasNext()){
	    		String shipNumber= it.next().toString();
	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
	    		recordList.add(serviceOrderRemote);
	    	}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return recordList;
	    	}
	    @SkipValidation
	    private void synchornizeServiceOrder(List<Object> records, ServiceOrder serviceOrder,TrackingStatus trackingStatus) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String fieldType="";
			String uniqueFieldType=""; 
			try
			{
	    	Iterator  it=records.iterator();
	    	while(it.hasNext()){
	    		ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();
	    		String soJobType = serviceOrderToRecods.getJob();
	    		if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
	    		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
	    		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
	    			uniqueFieldType=trackingStatusToRecods.getContractType();
	    		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
	    			fieldType="CMM/DMM";
	    		}
	    		List fieldToSync=customerFileManager.findFieldToSync("ServiceOrder",fieldType,uniqueFieldType);
	    		fieldType="";
	   		    uniqueFieldType=""; 
	    		Iterator it1=fieldToSync.iterator();
	    		while(it1.hasNext()){
	    			String field=it1.next().toString();
	    			String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
	    			try{
	    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
	    				try{
	    				beanUtilsBean.setProperty(serviceOrderToRecods,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
	    				}catch(Exception ex){
	    					ex.printStackTrace();
						}
	    				
	    			}catch(Exception ex){
	    				ex.printStackTrace();
	    			}
	    			
	    		}
	    		try{
	    	    if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
	    	    if(serviceOrder.getJob()!=null && (!(serviceOrder.getJob().toString().equals(""))) && (serviceOrder.getJob().toString().equalsIgnoreCase("RLO")) ) {
	    	    	serviceOrderToRecods.setServiceType(serviceOrder.getServiceType());	
	    	    }
	    		serviceOrderToRecods.setUpdatedBy(serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());
	    		serviceOrderToRecods.setUpdatedOn(new Date());
	    	    }}catch( Exception e){
	    	    	e.printStackTrace();
	    	    }
	    	    try{
	    	    	if(!networkAgent){
	    	    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
	    	    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
	    	    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
	        		}
	    	    	}
	    	    	}
	    	    	if(networkAgent){ 
	        	    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
	        	    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
	        	    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
	            		}
	        	    	}
	        	    	
	    	    		
	    	    	}
	    	    }catch( Exception e){
	    	    	e.printStackTrace();
	    	    }
	    	    if(serviceOrderToRecods.getCorpID().toString().equalsIgnoreCase("UGWW"))
	    	    	serviceOrderToRecods.setJob(soJobType);
	    	    
	    		 serviceOrderManager.save(serviceOrderToRecods);
	    	}
	    		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    	}
	    @SkipValidation
	    public List<Object> findTrackingStatusRecords(List linkedShipNumber,	ServiceOrder serviceOrder) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	List<Object> recordList= new ArrayList();
        	try
        	{
        	Iterator it =linkedShipNumber.iterator();
        	while(it.hasNext()){
        		String shipNumber= it.next().toString();
        		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
        		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
        		recordList.add(trackingStatusRemote);
        	}}
        	 catch(Exception e)
        	 {
        		 e.printStackTrace();
        	 }
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return recordList;
        	}
	    @SkipValidation
	    public void synchornizeTrackingStatus(List<Object> records, TrackingStatus trackingStatus,ServiceOrder serviceOrder) {
        	String fieldType="";
    		String uniqueFieldType="";
          try
          {
    		Iterator  it=records.iterator();
        	while(it.hasNext()){
        		TrackingStatus trackingStatusToRecods=(TrackingStatus)it.next();
        		String forwarderCode="";
           		String brokerCode="";
           	    if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
           	    	forwarderCode=trackingStatusToRecods.getForwarderCode();
           	    }
           	    if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
           	    	brokerCode =trackingStatusToRecods.getBrokerCode();
           	    }
        		if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
        		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
        			uniqueFieldType=trackingStatusToRecods.getContractType();
        		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
        			fieldType="CMM/DMM";
        		}
        		List fieldToSync=customerFileManager.findFieldToSync("TrackingStatus",fieldType,uniqueFieldType);
        		fieldType="";
       		    uniqueFieldType=""; 
        		Iterator it1=fieldToSync.iterator();
        		while(it1.hasNext()){
        			String field=it1.next().toString();
        			String[] splitField=field.split("~");
    				String fieldFrom=splitField[0].trim();
    				String fieldTo=splitField[1].trim();
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
        			try{
        				beanUtilsBean.setProperty(trackingStatusToRecods,fieldTo,beanUtilsBean.getProperty(trackingStatus, fieldFrom));
        			}catch(Exception ex){
        				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
        			}
        		}
        		try{
	               	 if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
	        		  trackingStatusToRecods.setUpdatedBy(trackingStatus.getCorpID()+":"+getRequest().getRemoteUser());
	        		  trackingStatusToRecods.setUpdatedOn(new Date());
	               	 }
	            }catch(Exception e){
	            	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
	            }
        		try{
   				 if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
   					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getForwarderCode());
   					if(checkVendorCode){
   						
   					}else{
   						trackingStatusToRecods.setForwarderCode("");	
   					}
   				 }else{
   				if(forwarderCode!=null && (!(forwarderCode.equals("")))){
   				 trackingStatusToRecods.setForwarderCode(forwarderCode); 
   			    }
   				 }
   				 }catch(Exception e){
   					e.printStackTrace(); 
   				 }
   			 try{
   				 if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
   					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getBrokerCode());
   					if(checkVendorCode){
   						
   					}else{
   						trackingStatusToRecods.setBrokerCode("");	
   					}
   				 }else{
   				 if(brokerCode!=null && (!(brokerCode.equals("")))){
   				 trackingStatusToRecods.setBrokerCode(brokerCode); 
   			    }
   				 }
   				 }catch(Exception e){
   					e.printStackTrace(); 
   				 }
        		trackingStatusManager.save(trackingStatusToRecods);
        	}
        	}}
catch(Exception e)
{
	e.printStackTrace();}
        }
	public synchronized String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String permKeyVoer = sessionCorpID +"-"+"component.field.State.NotManadotry";
		visibilityForVoer=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKeyVoer) ;
		
		String opsPermKey = sessionCorpID +"-"+"component.field.opsMgmt.warehouse";
		opsMgmtWarehouseCheck=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(opsPermKey) ;
		
		if(!opsMgmtWarehouseCheck){
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		 Iterator iter123  =wtServiceTypeDyna.iterator();
			while (iter123.hasNext()) {
				if (typeService.equals("")) {
					typeService = iter123.next().toString();
				} else {
					typeService = typeService + "," + iter123.next().toString();
				}
			}
		 }
		if(opsMgmtWarehouseCheck){
			if(operationChk.equalsIgnoreCase("W")){
				typeService = "";
				wtServiceTypeDyna=refMasterManager.getServiceTypeListByWarehouse(sessionCorpID, "TCKTSERVC",workTicket.getWarehouse());
			    Iterator iter1  =wtServiceTypeDyna.iterator();
					while (iter1.hasNext()) {
						if (typeService.equals("")) {
							typeService = iter1.next().toString();
						} else {
							typeService = typeService + "," + iter1.next().toString();
						}
					}
			}else{
				wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
				 Iterator iter123  =wtServiceTypeDyna.iterator();
					while (iter123.hasNext()) {
						if (typeService.equals("")) {
							typeService = iter123.next().toString();
						} else {
							typeService = typeService + "," + iter123.next().toString();
						}
					}
			}
		}
		serviceOrder = serviceOrderManager.get(soid);
		dataItemList=workTicketManager.getAllDataItemList(id,serviceOrder.getShipNumber());
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		workTicket.setServiceOrder(serviceOrder);
		String permKey = sessionCorpID +"-"+"component.field.LongCarry.ShowForSSCW";
		visibilityForSSCW=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;		
		if(visibilityForSSCW){
			if(workTicketOriginShuttle.equalsIgnoreCase("Yes")){				
				workTicket.setOriginShuttle(true);
			}else if(workTicketOriginShuttle.equalsIgnoreCase("No")){
				workTicket.setOriginShuttle(false);
			}else{
				
			}
			if(workTicketDestinationShuttle.equals("Yes")){			
				workTicket.setDestinationShuttle(true);
			}else if(workTicketDestinationShuttle.equals("No")){
				workTicket.setDestinationShuttle(false);
			}else{
				
			}
		}
		if(workTicket.getEstimatedWeight()==null || workTicket.getEstimatedWeight().equals("") ){
			workTicket.setEstimatedWeight(0.0);
		}
		
		if(workTicket.getActualWeight()==null  || workTicket.getActualWeight().equals("")){
			workTicket.setActualWeight(0.0);
		}
		if(workTicket.getEstimatedCubicFeet()==null  || workTicket.getEstimatedCubicFeet().equals("")){
			workTicket.setEstimatedCubicFeet(0.0);
		}
		if(workTicket.getCrates()==null  || workTicket.getCrates().equals("")){
			workTicket.setCrates(0);
		}
		if(workTicket.getEstimatedPieces()==null  || workTicket.getEstimatedPieces().equals("")){
			workTicket.setEstimatedPieces(0);
		}
		if(workTicket.getActualVolume()==null  || workTicket.getActualVolume().equals("")){
			workTicket.setActualVolume(0.0);
		} 
		boolean updateSoStatus=false;
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());		
		customerFile = serviceOrder.getCustomerFile();
		billing = billingManager.get(serviceOrder.getId());
		if(billToAuthority!=null && !(billToAuthority.equals("")) && authUpdated!=null ){
			workTicketManager.updateBillToAuthority(billToAuthority,authUpdated,billing.getId());	
		}
		if (cancel != null) {
			return "cancel";
		}
		Date cal12 = new Date();
	    DateFormat formatter12 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter12.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss12 =formatter12.format(cal12);
    	SimpleDateFormat dateformatYYYYMMDD12 = new SimpleDateFormat("yyyy-MM-dd"); 
		Date currentTimeZoneWise12 =  dateformatYYYYMMDD12.parse(ss12);
		String key1 ="System cannot accept actualizing future dates, Please edit following field:";
		if((workTicket.getTargetActual().equalsIgnoreCase("A"))){
		if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
	    if(workTicket.getDate1().after(currentTimeZoneWise12)){
	    if(workTicket.getDate1()!=null){
			String key = "Actual Begin Date.";
			validateFormNav = "NOTOK";
			errorMessage(getText(key1+" "+key));
			workTicket.setDate1(null);
			workTicket.setDate2(null);
			return INPUT;
				}
		}
	    if(workTicket.getDate2().after(currentTimeZoneWise12)){
		    if(workTicket.getDate2()!=null){
				String key = "Actual End Date.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key1+" "+key));
				workTicket.setDate2(null); 
				return INPUT;		
		    }
			}
		}}
		if(workTicket.getInstructionCode().equalsIgnoreCase("") && workTicket.getInstructions().equalsIgnoreCase("")){
			ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			check=workTicket.getState();
			check1=workTicket.getDestinationState();
			String key = "Please Enter the Instruction Code / Instructions To Save the Work Ticket";
			validateFormNav = "NOTOK";
			errorMessage(getText(key));
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
		}
		if(visibilityForVoer){
		if(((workTicket.getOriginCountry().equalsIgnoreCase("United States")) && (workTicket.getState() == null || workTicket.getState().equalsIgnoreCase("")))) {
			ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			check=workTicket.getState();
			check1=workTicket.getDestinationState();
			String key = "State is a required field in Origin.";
			validateFormNav = "NOTOK";
			errorMessage(getText(key));
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
		}
		if(((workTicket.getDestinationCountry().equalsIgnoreCase("United States")) && ( workTicket.getDestinationState() ==null || workTicket.getDestinationState().equalsIgnoreCase("")))) {
			ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			check=workTicket.getState();
			check1=workTicket.getDestinationState();
			String key = "State is a required field in Destination.";
			validateFormNav = "NOTOK";
			errorMessage(getText(key));
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
		}
		}else{

			if(((workTicket.getOriginCountry().equalsIgnoreCase("United States") || workTicket.getOriginCountry().equalsIgnoreCase("India")) && (workTicket.getState() == null || workTicket.getState().equalsIgnoreCase("")))) {
				ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
				dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
				originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
				destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
				addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
				check=workTicket.getState();
				check1=workTicket.getDestinationState();
				String key = "State is a required field in Origin.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	            return SUCCESS;
			}
			if(((workTicket.getDestinationCountry().equalsIgnoreCase("United States") || workTicket.getDestinationCountry().equalsIgnoreCase("India")) && ( workTicket.getDestinationState() ==null || workTicket.getDestinationState().equalsIgnoreCase("")))) {
				ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
				dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
				originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
				destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
				addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
				check=workTicket.getState();
				check1=workTicket.getDestinationState();
				String key = "State is a required field in Destination.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	            return SUCCESS;
			}
			
		}
		storages=workTicketManager.findStorageUnit(workTicket.getTicket(),workTicket.getShipNumber());
		handoutPiecesAndVolumeList = workTicketManager.handoutWeightandVolume(workTicket.getTicket(),workTicket.getShipNumber(),sessionCorpID);
		Iterator wtPiecesIterator = handoutPiecesAndVolumeList.iterator();
         while (wtPiecesIterator.hasNext()) {
        	 Object [] row=(Object[])wtPiecesIterator.next();
        	 if(row[0]!=null){
        		 totalHandoutPieces = Integer.parseInt(row[0].toString());
        	 }else{
        		 totalHandoutPieces = 0; 
        	 }
        	 if(row[1]!=null){
        		 totalHandoutVolume = Double.parseDouble(row[1].toString());
        	 }else{
        		 totalHandoutVolume = 0.00;
        	 }
         }
		boolean isNew = (workTicket.getId() == null);
		
		workTicket.setCorpID(sessionCorpID);
		if (isNew) {
			workTicket.setCreatedOn(new Date());
		}
		workTicket.setUpdatedOn(new Date());
		workTicket.setUpdatedBy(getRequest().getRemoteUser());
		
		String key = "";
		countCheck="";
		Boolean wareHouseCheck = false;
		wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
		List checkCompanyDivisionList=workTicketManager.checkCompanyDivisionAgentCode(sessionCorpID);
		Iterator checkCompanyDestination=checkCompanyDivisionList.iterator();
		while(checkCompanyDestination.hasNext()){
			String a=(String)checkCompanyDestination.next();
			if(a.equals(trackingStatus.getDestinationAgentCode())){
				checkCompanyDestinationAgent="True";
			}
		}
		Iterator checkCompanyOrigin=checkCompanyDivisionList.iterator();
		while(checkCompanyOrigin.hasNext()){
			String a=(String)checkCompanyOrigin.next();
			if(a.equals(trackingStatus.getOriginAgentCode())){
				checkCompanyOriginAgent="True";
			}
		}
	BigDecimal noOfCrewFromPeople = new BigDecimal(0.00);
	BigDecimal  tempRequiredNoPeople = new BigDecimal(0.00);
	operationsHubLimits  = workTicketManager.getOperationsHub(workTicket.getWarehouse(), sessionCorpID);
	if((hubWarehouseLimit!=null && !hubWarehouseLimit.equalsIgnoreCase("")) && hubWarehouseLimit.equalsIgnoreCase("Tkt")) {
		if(wareHouseCheck == true){
			if((!(workTicket.getTargetActual().equalsIgnoreCase("C"))) && (wtServiceTypeDyna.contains(workTicket.getService())) ){
				user = userManager.getUserByUsername(getRequest().getRemoteUser());
				roles = user.getRoles();
				
				if (operationsHubLimits.getId() == null){		
					flag = "hub";
					return INPUT;
				}
				if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
		    		
				}else{
    	   		Boolean checkDate1 = false;
    	   		if(!isNew){
    			   checkDate1 = workTicketManager.checkDate1(workTicket.getWarehouse(), workTicket.getService(), workTicket.getDate1(), workTicket.getId(), sessionCorpID, workTicket.getEstimatedCubicFeet(), workTicket.getEstimatedWeight(),workTicket.getDate2());
    	   		}    		      		
    		    if(isNew || checkDate1== true){
    			String str="";
    			   if(operationsHubLimits.getMinServiceDayHub()==null ||operationsHubLimits.getMinServiceDayHub().toString().equalsIgnoreCase("")){
    				   str="0";
    			   }
    			   else{
    				   str=operationsHubLimits.getMinServiceDayHub().toString();
    			   }
    				   
    			int minServiceDay = Integer.parseInt(str);
	    		int weekEndDays = 0;
	    		Date newDate = new Date();
    			
    			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newDate));
	    		String todayDate = nowYYYYMMDD1.toString();
	    		
	    		Date tempDate  = dateformatYYYYMMDD1.parse(todayDate);
	    		
	    		Calendar calc = Calendar.getInstance();
	    		calc.setTime(tempDate);
	    		if(calc.get(Calendar.DAY_OF_WEEK) == 1 || calc.get(Calendar.DAY_OF_WEEK) == 7){
	    			weekEndDays = 1;
	    		}

	    		for (int i=1; i<= minServiceDay; i++){
	    			Calendar cal = Calendar.getInstance();
	    			cal.setTime(tempDate);
	    			cal.add(Calendar.DAY_OF_MONTH, 1);
	    			tempDate = cal.getTime();
	    			
	    			if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7){
	    				weekEndDays++;
	    		     }
	    		}

	    		int intervalDays = minServiceDay + weekEndDays;
	    		Calendar cal1 = Calendar.getInstance();
	    		cal1.setTime(dateformatYYYYMMDD1.parse(todayDate));
    			cal1.add(Calendar.DAY_OF_MONTH, intervalDays);
    			
	    		Date endDate = cal1.getTime();

	    		if (cal1.get(Calendar.DAY_OF_WEEK) == 1 || cal1.get(Calendar.DAY_OF_WEEK) == 7){
	    			Calendar cal2 = Calendar.getInstance();
		    		cal2.setTime(endDate);
	    			cal2.add(Calendar.DAY_OF_MONTH, 2);
	    			endDate = cal2.getTime();
    		     }
	    		
	    		if(workTicket.getDate1().before(endDate)){
    			   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
    				   workTicket.setTargetActual("P");
    			   }else{
    				   flag = "true";
    				   countCheck = "Limit";
    					 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    				   return INPUT;
    			   }
	    		}
    	   }
    		  if(isNew || (!isNew && checkDate1== true)){
    			if (operationsHubLimits != null){
    				   Long dailyCount = Long.parseLong(operationsHubLimits.getDailyOpHubLimit());
    				   Long dailyCountPC = Long.parseLong(operationsHubLimits.getDailyOpHubLimitPC());
    				   Double dailyMaxEstWt = 0.00;
    				   Double dailyMaxEstVol = 0.00;
    				   if(operationsHubLimits.getDailyMaxEstWt() != null){
    				        dailyMaxEstWt = Double.parseDouble(operationsHubLimits.getDailyMaxEstWt().toString());
    				   }
    				   if(operationsHubLimits.getDailyMaxEstVol() != null){
    				        dailyMaxEstVol = Double.parseDouble(operationsHubLimits.getDailyMaxEstVol().toString()); 
    				   }
	    			   countCheck = workTicketManager.getDailyOpsLimit(workTicket.getDate1(), workTicket.getDate2(), dailyCount, dailyCountPC, sessionCorpID, operationsHubLimits.getHubID(), workTicket.getEstimatedWeight(), workTicket.getEstimatedCubicFeet(),dailyMaxEstWt, dailyMaxEstVol,workTicket.getId());
	    			   if(countCheck.equals("") && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){countCheck="resourceCheckListValue:"+date1;}
	    			   if(countCheck.length()>15){	    				   
	    				   exceedDate = countCheck.substring(countCheck.indexOf(":")+1, countCheck.length());
		    			   countCheck = countCheck.substring(0, countCheck.indexOf(":"));
		    			   String key3="";
		    			   if(countCheck.equalsIgnoreCase("GreaterWeightVolume")){
		    				   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
			    				   workTicket.setTargetActual("P");
			    			   }else{
			    				   flag = "true";
			    				   return INPUT;
			    			   }
		    				}else {
		    					if((workTicket.getTargetActual().equals("P") || (workTicket.getTargetActual().equals("R"))) && (!countCheck.equalsIgnoreCase("Greater")) && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
				    				   workTicket.setTargetActual("T");
				    			   }
		    				}
			    			if(countCheck.equalsIgnoreCase("Greater")){
			    				if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
				    			  workTicket.setTargetActual("P");
				    			}else{
			    				  flag = "true";
			    				  return INPUT;
				    			}
			    			}else {
		    					if((workTicket.getTargetActual().equals("P") || (workTicket.getTargetActual().equals("R"))) && (!countCheck.equalsIgnoreCase("GreaterWeightVolume")) && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
				    				   workTicket.setTargetActual("T");
				    			   }
		    				}
		    			   if(countCheck.equalsIgnoreCase("Percent")){
		    				  countCheck="Percent";
		    				  flag = "true";
		    				  flag3 = "true";
		    				  if (isNew) {
			    				autoTicket = workTicketManager.findMaximum(sessionCorpID);
			    				workTicket.setTicket(autoTicket);
		    				  }
		    				  if((workTicket.getOriginCountry()!=null && !workTicket.getOriginCountry().equalsIgnoreCase("")) && (workTicket.getOriginCountryCode()==null || workTicket.getOriginCountryCode().equalsIgnoreCase(""))){
		    						workTicket.setOriginCountryCode(countryDsc.get(workTicket.getOriginCountry()));
		    					 }else if((workTicket.getOriginCountryCode()!=null && !workTicket.getOriginCountryCode().equalsIgnoreCase(""))||(workTicket.getOriginCountry()==null || workTicket.getOriginCountry().equalsIgnoreCase(""))){
		    						 workTicket.setOriginCountry(countryCod.get(workTicket.getOriginCountryCode()));
		    					 }

		    					    if((workTicket.getDestinationCountry()!=null && !workTicket.getDestinationCountry().equalsIgnoreCase("")) && (workTicket.getDestinationCountryCode()==null || workTicket.getDestinationCountryCode().equalsIgnoreCase(""))){
		    					    	workTicket.setDestinationCountryCode(countryDsc.get(workTicket.getDestinationCountry()));
		    					    }else if((workTicket.getDestinationCountryCode()!=null && !workTicket.getDestinationCountryCode().equalsIgnoreCase(""))||(workTicket.getDestinationCountry()==null || workTicket.getDestinationCountry().equalsIgnoreCase(""))){
		    					    	workTicket.setDestinationCountry(countryCod.get(workTicket.getDestinationCountryCode()));
		    					    }
		    				  workTicket=workTicketManager.save(workTicket);
		    				  
		    				  containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
		    				  ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
		    				  dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
		    				  originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
		    				  destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
		    				  addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
		    				  if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			    					key = (isNew) ? "The Work Ticket has been added, the ticket # is: " + workTicket.getTicket() : "The Work Ticket has been updated successfully";
			    					saveMessage(getText(key));
			    					String key2="Ticket is being saved but your date "+exceedDate+" are close to the operational service limit.";
			    					saveMessage(getText(key2));			    					
			    			  }
		    				  getNotesForIconChange();
		    				  if (!isNew) {
		    						 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    					  return "FORWARD";
		    				  } else {
		    					  i = Long.parseLong(workTicketManager.findMaximumId().get(0).toString());
		    					  workTicket = workTicketManager.get(i);
		    						 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    					  return "FORWARD";
		    				  }
		    			   }
	    				   
	    			   }if(!isNew && checkDate1== true){
		       		    	String chkDate1="";
		       		    	String chkDate2="";
		       		    	if (!(workTicket.getDate1() == null)) {
		       					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		       					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
		       					chkDate1 = nowYYYYMMDD1.toString();
		       				}

		       				if (!(workTicket.getDate2() == null)) {
		       					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		       					StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
		       					chkDate2 = nowYYYYMMDD2.toString();
		       				}
		       		    	List itemsJbkList = workTicketManager.findWorkTicketResourceList(id);
		       		    	if(itemsJbkList!=null && (!(itemsJbkList.isEmpty()))){
		       		    		Iterator iter = itemsJbkList.iterator();
		       		    		while(iter.hasNext()){
		       		    			Long itemsJbkId = Long.parseLong(iter.next().toString());
		       		    			ItemsJbkEquip itemsJbkEquip = itemsJbkEquipManager.get(itemsJbkId);
		       						int usedEstQuanitity = 0;
		       						List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(),chkDate1, chkDate2, operationsHubLimits.getHubID(), workTicket.getService(), sessionCorpID,workTicket.getTicket());
		       						if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && usedEstQuanitityList.get(0)!=null){
		       							   usedEstQuanitity = Integer.parseInt(usedEstQuanitityList.get(0).toString());
		       						}
		       						
		       				       List dailyResourceLimitList = itemsJbkEquipManager.getDailyResourceLimit(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(), chkDate1, chkDate2, operationsHubLimits.getHubID(), sessionCorpID);
		       					   int dailyResourceLimit = 0;
		   						   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && dailyResourceLimitList.get(0)!=null){
		   							   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
		   							   dailyResourceLimit = dailyQty.intValue();							
		   						   }
		   						   if(usedEstQuanitity > dailyResourceLimit && dailyResourceLimit>0){
		   							if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
						    			  workTicket.setTargetActual("P");break;
						    			}
		   						   }else {
				    					if((!countCheck.equalsIgnoreCase("Greater")) && (!countCheck.equalsIgnoreCase("GreaterWeightVolume")) && (workTicket.getTargetActual().equals("P") || (workTicket.getTargetActual().equals("R"))) && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
						    				   workTicket.setTargetActual("T");
						    			   }
				    				}
		       		    		}
		       		    	}
		       		    }
	    			   
	    	   }
    		  }
    		  
    	   }
				}				 
		    }
		}else{
			if(((!(workTicket.getTargetActual().equalsIgnoreCase("C"))) && (!(workTicket.getTargetActual().equalsIgnoreCase("P"))) && (!(workTicket.getTargetActual().equalsIgnoreCase("R")))) && (wtServiceTypeDyna.contains(workTicket.getService()))){
				user = userManager.getUserByUsername(getRequest().getRemoteUser());
				roles = user.getRoles();
				WorkTicket workTicketTemp =null;
				Boolean valDiff = false;
				String oldDate1 ="";
				String currentDate1 ="";
				String oldDate2 ="";
				String currentDate2 ="";
				String longCarryValue="";
    			Boolean shuttleValue = false;
    			String elevatorValue ="";
				BigDecimal  oldRequiredCrewVal = new BigDecimal(0.00);
				Double tempActualWt = workTicket.getActualWeight();
				Double tempEstimatedWt = workTicket.getEstimatedWeight();
				if(workTicket.getActualWeight()==null || workTicket.getActualWeight().equals("")){
					tempActualWt =0.00;
				}	
				if(workTicket.getEstimatedWeight()==null || workTicket.getEstimatedWeight().equals("")){
					tempEstimatedWt = 0.00;
				}    			
				String serviceType = workTicket.getService();
				String tempWareHouse = workTicket.getWarehouse();
				String oldTicketNumber="";
				if((!tempWareHouse.equalsIgnoreCase("L")) && (!tempWareHouse.equalsIgnoreCase("M")) && (!tempWareHouse.equalsIgnoreCase("D")) && (!tempWareHouse.equalsIgnoreCase("T")) && (!tempWareHouse.equalsIgnoreCase("8")) && (!tempWareHouse.equalsIgnoreCase("A")) && (!tempWareHouse.equalsIgnoreCase("4")) && (!tempWareHouse.equalsIgnoreCase("P"))){
				if(workTicket.getId()!=null){
					workTicketTemp = workTicketManager.get(workTicket.getId());
						if(workTicketTemp.getDate1()!=null){
							SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
							StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicketTemp.getDate1()));
							oldDate1 = nowYYYYMMDD2.toString();
						}
						if(workTicket.getDate1()!=null){
							SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
							StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate1()));
							currentDate1 = nowYYYYMMDD2.toString();
						}
						if(workTicketTemp.getDate2()!=null){
							SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
							StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicketTemp.getDate2()));
							oldDate2 = nowYYYYMMDD2.toString();
						}
						if(workTicket.getDate2()!=null){
							SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
							StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
							currentDate2 = nowYYYYMMDD2.toString();
						}
						if(workTicketTemp.getActualWeight()==null || workTicketTemp.getActualWeight().equals("")){
							workTicketTemp.setActualWeight(0.00);
						}
						if(workTicketTemp.getEstimatedWeight()==null || workTicketTemp.getEstimatedWeight().equals("")){
							workTicketTemp.setEstimatedWeight(0.00);
						}
						if(workTicketTemp.getRequiredCrew()!=null){
							oldRequiredCrewVal = workTicketTemp.getRequiredCrew();
						}
						if(workTicketTemp.getTicket()!=null){
							oldTicketNumber = workTicketTemp.getTicket().toString();
						}
						if(((workTicketTemp.getTargetActual().equalsIgnoreCase(workTicket.getTargetActual())) && (!valDiff))){
							valDiff = false;
						}else{
							valDiff = true;	
						}						
						if(((oldDate1.equalsIgnoreCase(currentDate1)) && (!valDiff))){
							valDiff = false;
						}else{
							valDiff = true;	
						}
						if(((oldDate2.equalsIgnoreCase(currentDate2)) && (!valDiff))){
							valDiff = false;
						}else{
							valDiff = true;	
						}
						if(((workTicketTemp.getService().equalsIgnoreCase(workTicket.getService())) && (!valDiff))){							
							valDiff = false;
						}else{
							valDiff = true;							
						}
						if(((workTicketTemp.getWarehouse().equalsIgnoreCase(workTicket.getWarehouse())) && (!valDiff))){							
							valDiff = false;
						}else{
							valDiff = true;							
						}
						if((workTicketTemp.getEstimatedWeight().doubleValue()==tempEstimatedWt) && (!valDiff)){
							valDiff = false;
						}else{
							valDiff = true;							
						}
						if(((workTicketTemp.getActualWeight().doubleValue()==tempActualWt) && (!valDiff))){
							valDiff = false;
						}else{
							valDiff = true;							
						}	
						if(((serviceType.equalsIgnoreCase("PK") ||serviceType.equalsIgnoreCase("PL") || serviceType.equalsIgnoreCase("LD")) && (!valDiff))){
							if(workTicketTemp.getOriginLongCarry().equalsIgnoreCase(workTicket.getOriginLongCarry()) && (!valDiff)){
								valDiff = false;
							}else{
								valDiff = true;							
							}
							if(((workTicketTemp.getOriginElevator().equalsIgnoreCase(workTicket.getOriginElevator())) && (!valDiff))){
								valDiff = false;
							}else{
								valDiff = true;							
							}
							if((workTicketTemp.getOriginShuttle()!=null && workTicketTemp.getOriginShuttle().toString().equalsIgnoreCase(workTicket.getOriginShuttle().toString())) && (!valDiff)){
								valDiff = false;
							}else{
								valDiff = true;							
							}
						}
						if(((serviceType.equalsIgnoreCase("DL") ||serviceType.equalsIgnoreCase("DU") || serviceType.equalsIgnoreCase("UP")) && (!valDiff))){
							if(workTicketTemp.getDestinationLongCarry().equalsIgnoreCase(workTicket.getDestinationLongCarry()) && (!valDiff)){
								valDiff = false;
							}else{
								valDiff = true;							
							}
							if(((workTicketTemp.getDestinationElevator().equalsIgnoreCase(workTicket.getDestinationElevator())) && (!valDiff))){
								valDiff = false;
							}else{
								valDiff = true;							
							}
							if(((workTicketTemp.getDestinationShuttle()!=null && workTicketTemp.getDestinationShuttle().toString().equalsIgnoreCase(workTicket.getDestinationShuttle().toString())) && (!valDiff))){
								valDiff = false;
							}else{
								valDiff = true;							
							}
						}
				}
				if (isNew || valDiff) {					
				List hubName = crewCapacityManager.findParentHubByWarehouse(tempWareHouse,sessionCorpID);
				if((hubName!=null && !hubName.isEmpty() && hubName.get(0)!=null)){
					String jobType= workTicket.getJobType();
					crewCapacity =  crewCapacityManager.findGroupNameFromCrewCapacity(jobType,sessionCorpID);
					if(crewCapacity!=null && !crewCapacity.equals("")){
					Class<?> cls = crewCapacity.getClass();
	    			Field tempwt1 = null;
	    			Field tempwt2 = null;
	    			Field tempwt3 = null;
	    			Object minWt=null;
	    			Object maxWt=null;
	    			Object avgWt=null;	    			
					if(serviceType.equalsIgnoreCase("PK") ||serviceType.equalsIgnoreCase("PL") || serviceType.equalsIgnoreCase("LD")){
						longCarryValue = workTicket.getOriginLongCarry();
						shuttleValue = workTicket.getOriginShuttle();
						elevatorValue = workTicket.getOriginElevator();
					}else if((serviceType.equalsIgnoreCase("DL") ||serviceType.equalsIgnoreCase("DU") || serviceType.equalsIgnoreCase("UP"))){
						longCarryValue = workTicket.getDestinationLongCarry();
						shuttleValue = workTicket.getDestinationShuttle();
						elevatorValue = workTicket.getDestinationElevator();
					}
					String workTicketMod = workTicket.getJobMode();
					Field temService = null;
					Object servType=null;
					Field temMode = null;
					Object modeType=null;
					Field temAsAvgWt = null;
					Object asAvgWt=null;
					Field temCrew = null;
					Object crewNo=null;
					Field temPhyCon = null;
					Object phyCondition=null;
					Field temAdjustPercent = null;
					Object adjustmaentPercentage=null;
					BigDecimal  tempCapacityReductionShuttle = new BigDecimal(0.00);
					BigDecimal  tempCapacityReductionLongCarry = new BigDecimal(0.00);
					BigDecimal  tempOriginElevatorValue = new BigDecimal(0.00);
					BigDecimal  tempTotalAdjustPerValue = new BigDecimal(0.00);
					BigDecimal  divideValHundred=new BigDecimal(100);
					BigDecimal tempAverageWeight = new BigDecimal(0.00);
					BigDecimal tempAssumptionAvrgWt = new BigDecimal(0.00);					
					int tempAdjustmaentPercentage;	
					Double kgWtMultiplyValue = 2.20;
					if((workTicket.getUnit1()!=null && !workTicket.getUnit1().equalsIgnoreCase("")) && workTicket.getUnit1().equalsIgnoreCase("Kgs")){
						tempActualWt = (tempActualWt)*(kgWtMultiplyValue);
						tempEstimatedWt = (tempEstimatedWt)*(kgWtMultiplyValue);
					}
					if(workTicket.getJobType().equalsIgnoreCase("STO")){
					    if(tempActualWt > 0 && tempActualWt <= 1000){
					    	tempActualWt = new Double("1000.10");
					    }else if(tempActualWt >= 1000){
					    	tempActualWt = tempActualWt;
					    }else if(tempActualWt==0 && tempEstimatedWt > 0 && tempEstimatedWt <= 1000){
					    	tempEstimatedWt = new Double("1000.10");
					    }else if(tempActualWt==0 && tempEstimatedWt > 0 && tempEstimatedWt >= 1000){
					    	tempEstimatedWt = tempEstimatedWt ;
					    }
					}
					if(workTicket.getJobType().equalsIgnoreCase("GST") || workTicket.getJobType().equalsIgnoreCase("INT")){
						if(tempActualWt > 0 && tempActualWt <= 200){
					    	tempActualWt = new Double("200.10");
					    }else if(tempActualWt >= 200){
					    	tempActualWt = tempActualWt;
					    }else if(tempActualWt==0 && tempEstimatedWt > 0 && tempEstimatedWt <= 200){
					    	tempEstimatedWt = new Double("200.10");
					    }else if(tempActualWt==0 && tempEstimatedWt > 0 && tempEstimatedWt >= 200){
					    	tempEstimatedWt = tempEstimatedWt ;
					    }
					}
				    if(tempActualWt > 0 || tempEstimatedWt > 0){
						try{
							for(int i=1;i<=25;i++){
								tempwt1 = cls.getDeclaredField("weightRangeMinQty"+i);
								tempwt1.setAccessible(true);
								minWt=tempwt1.get(crewCapacity);
		    					tempwt2 = cls.getDeclaredField("weightRangeMaxQty"+i);
		    					tempwt2.setAccessible(true);
		    					maxWt=tempwt2.get(crewCapacity);
		    					if(tempActualWt > 0){
								if(tempActualWt>=Double.parseDouble(minWt.toString())&&tempActualWt<=Double.parseDouble(maxWt.toString())){
										tempwt3 = cls.getDeclaredField("weightRangeAverageWeight"+i);
										tempwt3.setAccessible(true);
										avgWt=tempwt3.get(crewCapacity);							
				    					break;
									}else if(tempActualWt>Double.parseDouble("20999")){
										avgWt = new BigDecimal(20500.00);
										break;
									}
		    					}else if(tempEstimatedWt>=Double.parseDouble(minWt.toString())&&tempEstimatedWt<=Double.parseDouble(maxWt.toString())){
			    						tempwt3 = cls.getDeclaredField("weightRangeAverageWeight"+i);
										tempwt3.setAccessible(true);
										avgWt=tempwt3.get(crewCapacity);							
				    					break;
								}else if(tempEstimatedWt>Double.parseDouble("20999")){
										avgWt = new BigDecimal(20500.00);
										break;
								}
							}							
							for(int i=1;i<=7;i++){
								temService = cls.getDeclaredField("assumptionServiceType"+i);
								temService.setAccessible(true);
								servType=temService.get(crewCapacity);
								temMode = cls.getDeclaredField("assumptionMode"+i);
								temMode.setAccessible(true);
								modeType=temMode.get(crewCapacity);
								if(jobType.equalsIgnoreCase("GST") || jobType.equalsIgnoreCase("INT")){
								if(serviceType.equalsIgnoreCase("PL") && servType.toString().equalsIgnoreCase("PL") && workTicketMod.equalsIgnoreCase("Air") && modeType.toString().equalsIgnoreCase("Air")){
									temAsAvgWt = cls.getDeclaredField("assumptionAverageWeight"+i);
									temAsAvgWt.setAccessible(true);
									asAvgWt=temAsAvgWt.get(crewCapacity);
									temCrew = cls.getDeclaredField("assumptionCrew"+i);
									temCrew.setAccessible(true);
									crewNo=temCrew.get(crewCapacity);
									break;
								}else if(((serviceType.equalsIgnoreCase("PL") && servType.toString().equalsIgnoreCase("PL") && workTicketMod.equalsIgnoreCase("Sea")) && (modeType.toString().equalsIgnoreCase("Sea") || modeType.toString().equalsIgnoreCase("All")))){
									temAsAvgWt = cls.getDeclaredField("assumptionAverageWeight"+i);
									temAsAvgWt.setAccessible(true);
									asAvgWt=temAsAvgWt.get(crewCapacity);
									temCrew = cls.getDeclaredField("assumptionCrew"+i);
									temCrew.setAccessible(true);
									crewNo=temCrew.get(crewCapacity);
									break;								
								}else if(serviceType.equalsIgnoreCase(servType.toString()) && !servType.toString().equalsIgnoreCase("PL")){
									temAsAvgWt = cls.getDeclaredField("assumptionAverageWeight"+i);
									temAsAvgWt.setAccessible(true);
									asAvgWt=temAsAvgWt.get(crewCapacity);
									temCrew = cls.getDeclaredField("assumptionCrew"+i);
									temCrew.setAccessible(true);
									crewNo=temCrew.get(crewCapacity);
									break;
									}
								}else{
									if(serviceType.equalsIgnoreCase(servType.toString())){
										temAsAvgWt = cls.getDeclaredField("assumptionAverageWeight"+i);
										temAsAvgWt.setAccessible(true);
										asAvgWt=temAsAvgWt.get(crewCapacity);
										temCrew = cls.getDeclaredField("assumptionCrew"+i);
										temCrew.setAccessible(true);
										crewNo=temCrew.get(crewCapacity);
										break;
										}
									}
								}
								tempAverageWeight = (BigDecimal)avgWt;
								tempAssumptionAvrgWt = (BigDecimal) asAvgWt;
								int tempCrewNo = Integer.parseInt(crewNo.toString());
								tempRequiredNoPeople = (tempAverageWeight.multiply(new BigDecimal(tempCrewNo)).divide(tempAssumptionAvrgWt,2, RoundingMode.HALF_UP));
				
								for(int i=1;i<=6;i++){
									temPhyCon = cls.getDeclaredField("adjustmentPhysicalCondition"+i);
									temPhyCon.setAccessible(true);
									phyCondition=temPhyCon.get(crewCapacity);
									if(shuttleValue && phyCondition.toString().equalsIgnoreCase("Shuttle")){
										temAdjustPercent = cls.getDeclaredField("adjustmentPercentage"+i);
										temAdjustPercent.setAccessible(true);
										adjustmaentPercentage=temAdjustPercent.get(crewCapacity);
										tempAdjustmaentPercentage = Integer.parseInt(adjustmaentPercentage.toString());
										tempCapacityReductionShuttle = (tempAverageWeight.multiply(new BigDecimal(tempAdjustmaentPercentage)).divide(divideValHundred,2, RoundingMode.HALF_UP));
									}else if((longCarryValue!=null && !longCarryValue.equalsIgnoreCase("") && longCarryValue.equalsIgnoreCase("Yes") ) && phyCondition.toString().equalsIgnoreCase("Long Carry")){
										temAdjustPercent = cls.getDeclaredField("adjustmentPercentage"+i);
										temAdjustPercent.setAccessible(true);
										adjustmaentPercentage=temAdjustPercent.get(crewCapacity);
										tempAdjustmaentPercentage = Integer.parseInt(adjustmaentPercentage.toString());
										tempCapacityReductionLongCarry = (tempAverageWeight.multiply(new BigDecimal(tempAdjustmaentPercentage)).divide(new BigDecimal(100)));
									}else if(((elevatorValue!=null && !elevatorValue.equalsIgnoreCase("")) && (!elevatorValue.equalsIgnoreCase("NA") && !elevatorValue.equalsIgnoreCase("Not possible"))) && phyCondition.toString().equalsIgnoreCase("Elevator")){
										temAdjustPercent = cls.getDeclaredField("adjustmentPercentage"+i);
										temAdjustPercent.setAccessible(true);
										adjustmaentPercentage=temAdjustPercent.get(crewCapacity);
										tempAdjustmaentPercentage = Integer.parseInt(adjustmaentPercentage.toString());
										tempOriginElevatorValue = (tempAverageWeight.multiply(new BigDecimal(tempAdjustmaentPercentage)).divide(divideValHundred,2, RoundingMode.HALF_UP));
									}else{
										
									}									
								}
								tempTotalAdjustPerValue = tempCapacityReductionShuttle.add(tempCapacityReductionLongCarry).add(tempOriginElevatorValue);
								BigDecimal newValAfterDeduction = tempAverageWeight.subtract(tempTotalAdjustPerValue);
								BigDecimal noOfPeopleAfterCalculation = (tempAverageWeight.multiply(tempRequiredNoPeople).divide(newValAfterDeduction,2, RoundingMode.HALF_UP));
								BigDecimal divideVal = new BigDecimal(tempCrewNo);
								noOfCrewFromPeople = (noOfPeopleAfterCalculation.divide(divideVal,2, RoundingMode.HALF_UP));								
								
								if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
						    		
								}else{
					    			String str="";
					    			if(operationsHubLimits.getMinServiceDayHub()==null ||operationsHubLimits.getMinServiceDayHub().toString().equalsIgnoreCase("")){
					    				   str="0";
					    			   } else{
					    				   str=operationsHubLimits.getMinServiceDayHub().toString();
					    			 }
					    			int minServiceDay = Integer.parseInt(str);
						    		int weekEndDays = 0;
						    		Date newDate = new Date();
					    			
					    			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
						    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newDate));
						    		String todayDate = nowYYYYMMDD1.toString();
						    		
						    		Date tempDate  = dateformatYYYYMMDD1.parse(todayDate);
						    		
						    		Calendar calc = Calendar.getInstance();
						    		calc.setTime(tempDate);
						    		if(calc.get(Calendar.DAY_OF_WEEK) == 1 || calc.get(Calendar.DAY_OF_WEEK) == 7){
						    			weekEndDays = 1;
						    		}

						    		for (int i=1; i<= minServiceDay; i++){
						    			Calendar cal = Calendar.getInstance();
						    			cal.setTime(tempDate);
						    			cal.add(Calendar.DAY_OF_MONTH, 1);
						    			tempDate = cal.getTime();
						    			
						    			if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7){
						    				weekEndDays++;
						    		     }
						    		}

						    		int intervalDays = minServiceDay + weekEndDays;
						    		Calendar cal1 = Calendar.getInstance();
						    		cal1.setTime(dateformatYYYYMMDD1.parse(todayDate));
					    			cal1.add(Calendar.DAY_OF_MONTH, intervalDays);
					    			
						    		Date endDate = cal1.getTime();

						    		if (cal1.get(Calendar.DAY_OF_WEEK) == 1 || cal1.get(Calendar.DAY_OF_WEEK) == 7){
						    			Calendar cal2 = Calendar.getInstance();
							    		cal2.setTime(endDate);
						    			cal2.add(Calendar.DAY_OF_MONTH, 2);
						    			endDate = cal2.getTime();
					    		     }
						    		
						    		if(workTicket.getDate1().before(endDate)){
					    			   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
					    				   workTicket.setTargetActual("P");
					    			   }else{
					    				   flag = "true";
					    				   countCheck = "Limit";					    					
					    				   return INPUT;
					    			   }
						    		}
					    	   		
						    		String threshHoldOperationalCrewValue = workTicketManager.findcrewNoAndCrewThreshHold(workTicket.getDate1(),workTicket.getDate2(),workTicket.getWarehouse(),sessionCorpID,noOfCrewFromPeople,oldTicketNumber);
									if(threshHoldOperationalCrewValue.contains("INPUT")){
										flag = "true";
										 countCheck = "CrewMax";
										return INPUT;
									}else if(threshHoldOperationalCrewValue.contains("Call")){											
										String key5="Ticket is being saved but your date are close to the operational service limit.";
				    					saveMessage(getText(key5));
									}
								}
							workTicket.setRequiredCrew(noOfCrewFromPeople);
							}catch(Exception e){e.printStackTrace();}
				    	}
					}
				  }
				}
			}else{workTicket.setRequiredCrew(noOfCrewFromPeople);}
		}else{workTicket.setRequiredCrew(noOfCrewFromPeople);}
	}
		if (isNew) {
			autoTicket = workTicketManager.findMaximum(sessionCorpID);
			workTicket.setTicket(autoTicket);
		}
		if(workTicket.getService().equalsIgnoreCase("PUC") || workTicket.getService().equalsIgnoreCase("DLC") || workTicket.getService().equalsIgnoreCase("MIS"))
		{
			workTicketManager.updateServiceOrderInland(workTicket.getVendorCode(),serviceOrder.getId(), sessionCorpID);
		}
		if((workTicket.getOriginCountry()!=null && !workTicket.getOriginCountry().equalsIgnoreCase("")) && (workTicket.getOriginCountryCode()==null || workTicket.getOriginCountryCode().equalsIgnoreCase(""))){
			workTicket.setOriginCountryCode(countryDsc.get(workTicket.getOriginCountry()));
		 }else if((workTicket.getOriginCountryCode()!=null && !workTicket.getOriginCountryCode().equalsIgnoreCase(""))||(workTicket.getOriginCountry()==null || workTicket.getOriginCountry().equalsIgnoreCase(""))){
			 workTicket.setOriginCountry(countryCod.get(workTicket.getOriginCountryCode()));
		 }

		    if((workTicket.getDestinationCountry()!=null && !workTicket.getDestinationCountry().equalsIgnoreCase("")) && (workTicket.getDestinationCountryCode()==null || workTicket.getDestinationCountryCode().equalsIgnoreCase(""))){
		    	workTicket.setDestinationCountryCode(countryDsc.get(workTicket.getDestinationCountry()));
		    }else if((workTicket.getDestinationCountryCode()!=null && !workTicket.getDestinationCountryCode().equalsIgnoreCase(""))||(workTicket.getDestinationCountry()==null || workTicket.getDestinationCountry().equalsIgnoreCase(""))){
		    	workTicket.setDestinationCountry(countryCod.get(workTicket.getDestinationCountryCode()));
		    }
		workTicket = workTicketManager.save(workTicket);
		
		if(timeManagementList!=null && (!(timeManagementList.trim().equals("")))){
			String[] str = timeManagementList.split("~");
			for (String string : str) {
				String[] strRes=string.split("@");
				String requirements = strRes[0].toString().trim();		
				String startTime = strRes[1].toString().trim();		
				String endtime = strRes[2].toString().trim();	
				workTicketTimeManagement= new WorkTicketTimeManagement();
				workTicketTimeManagement.setCorpId(sessionCorpID);
				if(requirements.equals("A")){
					workTicketTimeManagement.setTimeRequirements("");
				}else{
					workTicketTimeManagement.setTimeRequirements(requirements);
				}				
				workTicketTimeManagement.setTicket(workTicket.getTicket());
				workTicketTimeManagement.setWorkTicketId(workTicket.getId());
				if(startTime.equals("A")){
					workTicketTimeManagement.setStartTime("00:00");
				}else{
					workTicketTimeManagement.setStartTime(startTime);
				}
				if(endtime.equals("A")){
					workTicketTimeManagement.setEndtime("00:00");	
				}else{
					workTicketTimeManagement.setEndtime(endtime);
				}				
				workTicketTimeManagement.setCreatedBy(getRequest().getRemoteUser());
				workTicketTimeManagement.setCreatedOn(new Date());
				workTicketTimeManagement.setUpdatedBy(getRequest().getRemoteUser());
				workTicketTimeManagement.setUpdatedOn(new Date());
				workTicketTimeManagementManager.save(workTicketTimeManagement);
			}			
		}
		try {
			if(resourceExList!=null &&(!(resourceExList.trim().equals("")))){
				String [] temp = resourceExList.split("~");
				for (String string : temp) {
					String [] resArr  =string.split("@");
					Long tempResId = Long.parseLong(resArr[0]);
				WorkTicketTimeManagement resObj = workTicketTimeManagementManager.get(tempResId);
				resObj.setTimeRequirements(resArr[1].toString().trim());
				if(resArr[2].toString().equals("")){
					resObj.setStartTime("00:00");	
				}else{
					resObj.setStartTime(resArr[2].toString().trim());
				}
				if(resArr[3].toString().equals("")){
					resObj.setEndtime("00:00");	
				}else{
					resObj.setEndtime(resArr[3].toString().trim());
				}
				
				resObj.setUpdatedBy(getRequest().getRemoteUser());
				resObj.setUpdatedOn(new Date());
				workTicketTimeManagementManager.save(resObj);
				}
				
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Map<String,String> tempMap=getInstructionCodeWithEnblCountry();
		Iterator iterator = tempMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			String keys=(String) mapEntry.getKey();
			String inscodeval[]=keys.split(":");
			String inscode=inscodeval[0];
			String insval=inscodeval[1];
			String vals=(String)mapEntry.getValue();
			String countparam[]=vals.split(",");
			String flag="0";
			for(int i=0;i<countparam.length;i++){
				if(workTicket.getDestinationCountryCode().equals(countparam[i])){
					flag="1";
				}
			}
			if(flag.equals("1")){			
				
				flag="0";
				
			}
			else{
				if(countparam!=null && countparam.length>0){
					woinstr.remove(inscode);	
				}
			}
				
		}
		 
		String warehouseManager = refMasterManager.findByParameterHouse(sessionCorpID, workTicket.getWarehouse(), "HOUSE");
		if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
		  if(checkLimit.equalsIgnoreCase("Y") && (oldTargetActual!=null && (!oldTargetActual.equalsIgnoreCase("AR")) && (!oldTargetActual.equalsIgnoreCase("CR"))) && workTicket.getTargetActual().equalsIgnoreCase("P")){			
			auditTrail = new AuditTrail();
			auditTrail.setCorpID(workTicket.getCorpID());
			auditTrail.setXtranId(workTicket.getId().toString());
			auditTrail.setTableName("workticket");
			auditTrail.setFieldName(workTicket.getTargetActual());
			auditTrail.setDescription("Warehouse Manager");
			if(isNew){
				auditTrail.setOldValue("");
			}else{
				auditTrail.setOldValue("Target");
			}
			auditTrail.setNewValue("Pending");
			auditTrail.setUser(getRequest().getRemoteUser());
			auditTrail.setDated(new Date());
			auditTrail.setAlertRole("Warehouse Manager");
			auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
			auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
			if(warehouseManager!=null){
				auditTrail.setAlertUser(warehouseManager);
			}else{
				auditTrail.setAlertUser("");
			}
			auditTrail.setIsViewed(false);			
			auditTrailManager.save(auditTrail);
		}
		if(oldTargetActual!=null && (oldTargetActual.equalsIgnoreCase("CR")) && workTicket.getTargetActual().equalsIgnoreCase("R")){
			auditTrail = new AuditTrail();
			auditTrail.setCorpID(workTicket.getCorpID());
			auditTrail.setXtranId(workTicket.getId().toString());
			auditTrail.setTableName("workticket");
			auditTrail.setFieldName(workTicket.getTargetActual());
			auditTrail.setDescription("Coordinator");
			auditTrail.setOldValue("Pending");
			auditTrail.setNewValue("Rejected");
			auditTrail.setUser(getRequest().getRemoteUser());
			auditTrail.setDated(new Date());
			auditTrail.setAlertRole("Coordinator");
			auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
			auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
		    auditTrail.setAlertUser(serviceOrder.getCoordinator());
			auditTrail.setIsViewed(false);			
			auditTrailManager.save(auditTrail);
		  }
		if(oldTargetActual!=null && (oldTargetActual.equalsIgnoreCase("AR") || oldTargetActual.equalsIgnoreCase("CR")) && workTicket.getTargetActual().equalsIgnoreCase("P")){
			auditTrail = new AuditTrail();
			auditTrail.setCorpID(workTicket.getCorpID());
			auditTrail.setXtranId(workTicket.getId().toString());
			auditTrail.setTableName("workticket");
			auditTrail.setFieldName(workTicket.getTargetActual());
			auditTrail.setDescription("Warehouse Manager");
			auditTrail.setOldValue("Rejected");
			auditTrail.setNewValue("Pending");
			auditTrail.setUser(getRequest().getRemoteUser());
			auditTrail.setDated(new Date());
			auditTrail.setAlertRole("Warehouse Manager");
			auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
			auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
			if(warehouseManager!=null){
				auditTrail.setAlertUser(warehouseManager);
			}else{
				auditTrail.setAlertUser("");
			}
			auditTrail.setIsViewed(false);			
			auditTrailManager.save(auditTrail);
		  }
		if(oldTargetActual!=null && (oldTargetActual.equalsIgnoreCase("AR") || oldTargetActual.equalsIgnoreCase("CR")) && workTicket.getTargetActual().equalsIgnoreCase("T")){
			auditTrail = new AuditTrail();
			auditTrail.setCorpID(workTicket.getCorpID());
			auditTrail.setXtranId(workTicket.getId().toString());
			auditTrail.setTableName("workticket");
			auditTrail.setFieldName(workTicket.getTargetActual());
			auditTrail.setDescription("Coordinator");
			auditTrail.setOldValue("Rejected");
			auditTrail.setNewValue("Target");
			auditTrail.setUser(getRequest().getRemoteUser());
			auditTrail.setDated(new Date());
			auditTrail.setAlertRole("Coordinator");
			auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
			auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
		    auditTrail.setAlertUser(serviceOrder.getCoordinator());
			auditTrail.setIsViewed(false);			
			auditTrailManager.save(auditTrail);
		  }
		if(oldTargetActual!=null && (oldTargetActual.equalsIgnoreCase("AP")) && workTicket.getTargetActual().equalsIgnoreCase("T")){
			auditTrail = new AuditTrail();
			auditTrail.setCorpID(workTicket.getCorpID());
			auditTrail.setXtranId(workTicket.getId().toString());
			auditTrail.setTableName("workticket");
			auditTrail.setFieldName(workTicket.getTargetActual());
			auditTrail.setDescription("Coordinator");
			auditTrail.setOldValue("Pending");
			auditTrail.setNewValue("Target");
			auditTrail.setUser(getRequest().getRemoteUser());
			auditTrail.setDated(new Date());
			auditTrail.setAlertRole("Coordinator");
			auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
			auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
		    auditTrail.setAlertUser(serviceOrder.getCoordinator());
			auditTrail.setIsViewed(false);			
			auditTrailManager.save(auditTrail);
		  }
		}
		countForTicketMovementIn = customManager.countForTicket(workTicket.getTicket(),serviceOrder.getId(),"In",sessionCorpID).get(0).toString();
		if(countForTicketMovementIn.equals("0")){
		if((workTicket.getService().equalsIgnoreCase("RC")) && (workTicket.getBondedGoods().equalsIgnoreCase("Y"))){
			Custom custom= new Custom();
			custom.setMovement("In");
			custom.setTicket(workTicket.getTicket().toString());	
			custom.setServiceOrderId(serviceOrder.getId());
			custom.setDocumentType("T-1");
			custom.setPieces(0);			
			custom.setCorpID(sessionCorpID);
			custom.setCreatedBy(getRequest().getRemoteUser());
			custom.setCreatedOn(new Date());
			custom.setUpdatedBy(getRequest().getRemoteUser());
			custom.setUpdatedOn(new Date());
			custom.setEntryDate(new Date());
			custom.setVolume(new BigDecimal(0));
	        custom.setStatus("Open");
	        custom.setEntryDate(workTicket.getDate1());
			custom.setServiceOrder(serviceOrder);
			try{
				customManager.save(custom);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					
				}
		  }
		}
		
		countForTicketMovementOut = customManager.countForTicket(workTicket.getTicket(),serviceOrder.getId(),"Out",sessionCorpID).get(0).toString();;
		if(countForTicketMovementOut.equals("0")){
		if((workTicket.getService().equalsIgnoreCase("RL")) && (workTicket.getBondedGoods().equalsIgnoreCase("Y"))){
			Custom custom= new Custom();
			custom.setMovement("Out");
			custom.setTicket(workTicket.getTicket().toString());
			custom.setServiceOrderId(serviceOrder.getId());
			custom.setPieces(0);			
			custom.setCorpID(sessionCorpID);
			custom.setCreatedBy(getRequest().getRemoteUser());
			custom.setCreatedOn(new Date());
			custom.setUpdatedBy(getRequest().getRemoteUser());
			custom.setUpdatedOn(new Date());
			custom.setEntryDate(new Date());
			custom.setVolume(new BigDecimal(0));
			custom.setServiceOrder(serviceOrder);
			try{
			customManager.save(custom);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				
			}
		 }
		}
		if(workTicket.getService().equalsIgnoreCase("PAD") && workTicket.getTargetActual().equalsIgnoreCase("A") && (trackingStatus.getPackA()==null || trackingStatus.getDeliveryA()==null)){
			workTicketManager.updateTicketStatusPackADetails(workTicket.getDate2(),serviceOrder.getId(),sessionCorpID);
		}
		if(workTicket.getService().equalsIgnoreCase("LD") &&  (serviceOrder.getJob().equalsIgnoreCase("STF") || serviceOrder.getJob().equalsIgnoreCase("STO")) && workTicket.getTargetActual().equalsIgnoreCase("T") && trackingStatus.getBeginLoad()==null)
		{
			workTicketManager.updateTrackingStatusBeginLoad(workTicket.getDate1(), serviceOrder.getShipNumber() ,sessionCorpID);
		}
		if(workTicket.getService().equalsIgnoreCase("RS") &&  (serviceOrder.getJob().equalsIgnoreCase("UVL") || serviceOrder.getJob().equalsIgnoreCase("MVL")|| serviceOrder.getJob().equalsIgnoreCase("VIS")) && workTicket.getTargetActual().equalsIgnoreCase("A") && trackingStatus.getSitDestinationIn()==null)
		{
			workTicketManager.updateTrackingStatusSitDestinationIn(workTicket.getDate1(), serviceOrder.getId() ,sessionCorpID);
		}	
		
		if((workTicket.getService().equalsIgnoreCase("AC")) && (workTicket.getTargetActual().equalsIgnoreCase("A")) &&  (serviceOrder.getJob().equalsIgnoreCase("GST") || serviceOrder.getJob().equalsIgnoreCase("UVL") || serviceOrder.getJob().equalsIgnoreCase("MVL") || serviceOrder.getJob().equalsIgnoreCase("STO") || serviceOrder.getJob().equalsIgnoreCase("DOM") || serviceOrder.getJob().equalsIgnoreCase("VAI") || serviceOrder.getJob().equalsIgnoreCase("LOC")) && !((workTicket.getDate1().equals(billing.getGstAccess1Service())) || (workTicket.getDate1().equals(billing.getGstAccess2Service())) || (workTicket.getDate1().equals(billing.getGstAccess3Service()))))
		{
			   if(billing.getGstAccess1Service() == null){	
			workTicketManager.updateBillingGSTAccess1Service(workTicket.getDate1(), serviceOrder.getShipNumber() ,sessionCorpID);
		} else if(billing.getGstAccess2Service() == null){
			workTicketManager.updateBillingGSTAccess2Service(workTicket.getDate1(), serviceOrder.getShipNumber() ,sessionCorpID);
		} else if (billing.getGstAccess3Service() == null){
			workTicketManager.updateBillingGSTAccess3Service(workTicket.getDate1(), serviceOrder.getShipNumber() ,sessionCorpID);
		} else {}
		}
		
		if(workTicket.getService().equalsIgnoreCase("PL") && (workTicket.getTargetActual().equalsIgnoreCase("A")) && trackingStatus.getLoadA()==null){
			if((serviceOrder.getStatusNumber().intValue()<10)){
				updateSoStatus=true;
			}
			workTicketManager.updateTrackingStatusLoadA(workTicket.getDate2(), serviceOrder.getShipNumber() ,sessionCorpID,updateSoStatus,getRequest().getRemoteUser());
		 }
		
		if(workTicket.getService().equalsIgnoreCase("PL") && (workTicket.getTargetActual().equalsIgnoreCase("A")) && trackingStatus.getPackA()==null){
			if((serviceOrder.getStatusNumber().intValue()<10)){
				updateSoStatus=true;
			}	
			workTicketManager.updateTrackingStatusPackA(workTicket.getDate2(), serviceOrder.getShipNumber() ,sessionCorpID,updateSoStatus);
			}
		if(checkCompanyOriginAgent.equalsIgnoreCase("True") && trackingStatus.getLoadA()==null && workTicket.getService().equalsIgnoreCase("LD") && workTicket.getTargetActual().equalsIgnoreCase("A")){
			if((serviceOrder.getStatusNumber().intValue()<10)){
				updateSoStatus=true;
			}
			workTicketManager.updateTrackingStatusLoadA(workTicket.getDate2(), serviceOrder.getShipNumber() ,sessionCorpID,updateSoStatus,getRequest().getRemoteUser());
		}else{
		if(workTicket.getService().equalsIgnoreCase("LD") &&  (serviceOrder.getJob().equalsIgnoreCase("STF") || serviceOrder.getJob().equalsIgnoreCase("STO")) && workTicket.getTargetActual().equalsIgnoreCase("A") && trackingStatus.getLoadA()==null){
			if((serviceOrder.getStatusNumber().intValue()<10)){
				updateSoStatus=true;
			}
			workTicketManager.updateTrackingStatusLoadA(workTicket.getDate1(), serviceOrder.getShipNumber() ,sessionCorpID,updateSoStatus,getRequest().getRemoteUser());
		}
		}
		    Date cal = new Date();
		    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	        String ss =formatter.format(cal);
	    	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
			Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
		if(checkCompanyDestinationAgent.equalsIgnoreCase("True") && !(serviceOrder.getJob().equalsIgnoreCase("STF") || serviceOrder.getJob().equalsIgnoreCase("STO") || serviceOrder.getJob().equalsIgnoreCase("STL")) && workTicket.getTargetActual().equalsIgnoreCase("A") && trackingStatus.getDeliveryA()==null && (workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL"))){
			if((serviceOrder.getStatusNumber().intValue()<60)){
				updateSoStatus=true;
			}
			workTicketManager.updateTrackingStatusDeliveryA(workTicket.getDate2(), serviceOrder.getShipNumber(), sessionCorpID,updateSoStatus);
			}
		if(checkCompanyOriginAgent.equalsIgnoreCase("True") && trackingStatus.getPackA()==null && workTicket.getService().equalsIgnoreCase("PK") && workTicket.getTargetActual().equalsIgnoreCase("A")){
			if((serviceOrder.getStatusNumber().intValue()<10)){
				updateSoStatus=true;	
			}
			workTicketManager.updateTrackingStatusPackA(workTicket.getDate2(), serviceOrder.getShipNumber() ,sessionCorpID,updateSoStatus);
		}
		
		if(checkCompanyOriginAgent.equalsIgnoreCase("True") && trackingStatus.getLeftWHOn()==null && workTicket.getService().equalsIgnoreCase("RL") && workTicket.getTargetActual().equalsIgnoreCase("A")){
			workTicketManager.updateTrackingStatusLeftWhon(workTicket.getDate2(), serviceOrder.getShipNumber() ,sessionCorpID);
		}
		if(workTicket.getStorageOut().equalsIgnoreCase("F") && (serviceOrder.getJob().equalsIgnoreCase("STF") || serviceOrder.getJob().equalsIgnoreCase("STO")) && (workTicket.getService().equalsIgnoreCase("RL") || workTicket.getService().equalsIgnoreCase("DU") || workTicket.getService().equalsIgnoreCase("DL"))){
			workTicketManager.updateBillingStorageOut(workTicket.getDate1(), serviceOrder.getShipNumber(), sessionCorpID);
			if((serviceOrder.getStatusNumber().intValue()<60)){
				updateSoStatus=true;
			}
			workTicketManager.updateTrackingStatusDeliveryA(workTicket.getDate1(), serviceOrder.getShipNumber(), sessionCorpID, updateSoStatus);
			
			workTicketManager.updateTrackingStatusDeliveryShipper(workTicket.getDate1(),serviceOrder.getShipNumber(), sessionCorpID);
			
			List locationReleaseStorageDate = workTicketManager.locationReleaseStorageDate(serviceOrder.getShipNumber(), sessionCorpID);
			if(locationReleaseStorageDate!=null && (!locationReleaseStorageDate.isEmpty())){
			Iterator it=locationReleaseStorageDate.iterator();
			while(it.hasNext())
			{
				id = Long.parseLong(it.next().toString());
				if (id != null) {
					storage = storageManager.get(id);
					BookStorage bookStorageNew = new BookStorage();
					bookStorageNew.setDescription(storage.getDescription());
					bookStorageNew.setCorpID(sessionCorpID);
					bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
					bookStorageNew.setCreatedOn(new Date());
					bookStorageNew.setUpdatedOn(new Date());
					bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
					bookStorageNew.setWhat("R");
					bookStorageNew.setShipNumber(storage.getShipNumber());
					bookStorageNew.setIdNum(storage.getIdNum());
					bookStorageNew.setDated(new Date());
					bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());
					if (storage.getPieces() == null) {
						bookStorageNew.setPieces(0);
					} else {
						bookStorageNew.setPieces(storage.getPieces());
					}
					bookStorageNew.setPrice(storage.getPrice());
					bookStorageNew.setContainerId(storage.getContainerId());
					bookStorageNew.setItemTag(storage.getItemTag());
					bookStorageNew.setOldLocation("");
					bookStorageNew.setLocationId(storage.getLocationId());
					bookStorageNew.setTicket(workTicket.getTicket());
					bookStorageNew.setStorage(storage);
					bookStorageManager.save(bookStorageNew);
					location = locationManager.getByLocation(storage.getLocationId());
					if(location.getCapacity()!=null){
					if(location.getCapacity().equalsIgnoreCase("1")){
					location.setOccupied("");
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);
					}
					}
                    if(workTicket.getDate2()!=null){
                        storage.setReleaseDate(workTicket.getDate2());	
					}else{
					    storage.setReleaseDate(new Date());	
					}
					storage.setUpdatedOn(new Date());
					storage.setUpdatedBy(getRequest().getRemoteUser());
					storage.setToRelease(workTicket.getTicket());
					storageManager.save(storage);
				}
			}
		}
		}
		containerList = workTicketManager.findContainer("", serviceOrder.getShipNumber());
		check=workTicket.getState();
		check1=workTicket.getDestinationState();
		ostates = customerFileManager.findDefaultStateList(workTicket.getOriginCountryCode(), sessionCorpID);
		dstates = customerFileManager.findDefaultStateList(workTicket.getDestinationCountryCode(), sessionCorpID);
		originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
		destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
		addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			key = (isNew) ? "The Work Ticket has been added, the ticket # is: " + workTicket.getTicket() : "The Work Ticket has been updated successfully";
			saveMessage(getText(key));
		}
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        
        List maxTicketTemp = workTicketManager.findMaximumTicket(serviceOrder.getShipNumber());
        if(maxTicketTemp!=null && !maxTicketTemp.isEmpty() && maxTicketTemp.get(0)!=null)
        maxTicket = maxTicketTemp.get(0).toString();
        
        List minTicketTemp = workTicketManager.findMinimumTicket(serviceOrder.getShipNumber());
        if(minTicketTemp!=null && !minTicketTemp.isEmpty() && minTicketTemp.get(0)!=null)
        minTicket =  minTicketTemp.get(0).toString();
        
        List countTicketTemp =workTicketManager.findCountTicket(serviceOrder.getShipNumber());
        if(countTicketTemp!=null && !countTicketTemp.isEmpty() && countTicketTemp.get(0)!=null)
           countTicket =  countTicketTemp.get(0).toString();
        
        sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
		for (SystemDefault systemDefault : sysDefaultDetail) {
			stoJob = systemDefault.getStorage();
			if(stoJob.contains(serviceOrder.getJob())){
			} else { 
			if(!((workTicket.getWarehouse() == null) || (workTicket.getWarehouse().equals("")))){
		        List actualizedTicketWareHouse=workTicketManager.actualizedTicketWareHouse(workTicket.getWarehouse(), serviceOrder.getShipNumber(), sessionCorpID);
		        if(actualizedTicketWareHouse!=null &&!(actualizedTicketWareHouse.isEmpty()) && actualizedTicketWareHouse.get(0)!=null){
		        workTicketManager.billingWareHouse(actualizedTicketWareHouse.get(0).toString(), serviceOrder.getShipNumber(), sessionCorpID);
		     }
			}
			}
			}
        
       }
		if((copyWorkTicketFlag!=null)&&(copyWorkTicketFlag.equalsIgnoreCase("YES"))){
			List itemsJbkAllRecords = workTicketManager.getAllRecord(wid,sessionCorpID);
			java.util.Iterator<ItemsJbkEquip> idIterator =  itemsJbkAllRecords.iterator(); 
     		while (idIterator.hasNext()){
     		ItemsJbkEquip itemsJbkEquipOld = (ItemsJbkEquip) idIterator.next();	     		
			ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();				
			itemsJbkEquip.setCorpID(workTicket.getCorpID());
			itemsJbkEquip.setWorkTicketID(workTicket.getId());
			itemsJbkEquip.setTicket(workTicket.getTicket());
			itemsJbkEquip.setCreatedBy(getRequest().getRemoteUser());
			itemsJbkEquip.setCreatedOn(new Date());
			itemsJbkEquip.setUpdatedBy(getRequest().getRemoteUser());
			itemsJbkEquip.setUpdatedOn(new Date());
			itemsJbkEquip.setType(itemsJbkEquipOld.getType());
			itemsJbkEquip.setDescript(itemsJbkEquipOld.getDescript());
			itemsJbkEquip.setQty(itemsJbkEquipOld.getQty());
			itemsJbkEquip.setEstHour(itemsJbkEquipOld.getEstHour());
			itemsJbkEquip.setId(null);
			itemsJbkEquipManager.save(itemsJbkEquip);	
	  }
	  }
		 
		try{ 
		if(serviceOrder.getIsNetworkRecord()!=null && serviceOrder.getIsNetworkRecord()==true){
			
			 
			trackingStatus = trackingStatusManager.get(soid);
			
			TrackingStatus trackingStatusWorkTicket = trackingStatusManager.getCurrentDetail(soid).get(0); 
			Date beginLoad=trackingStatusWorkTicket.getBeginLoad();
			Date loadA=trackingStatusWorkTicket.getLoadA();
			Date deliveryA=trackingStatusWorkTicket.getDeliveryA();
			Date packA=trackingStatusWorkTicket.getPackA();
			Date leftWHOn=trackingStatusWorkTicket.getLeftWHOn();
			Date deliveryShipper=trackingStatusWorkTicket.getDeliveryShipper();
			trackingStatus.setBeginLoad(beginLoad);
			trackingStatus.setLoadA(loadA);
			trackingStatus.setDeliveryA(deliveryA);
			trackingStatus.setPackA(packA);
			trackingStatus.setLeftWHOn(leftWHOn);
			trackingStatus.setDeliveryShipper(deliveryShipper);
			serviceOrder=serviceOrderManager.get(soid);
			ServiceOrder serviceOrderWorkTicket = serviceOrderManager.getCurrentDetail(soid).get(0); 
			String inlandCode=serviceOrderWorkTicket.getInlandCode();
			String status=serviceOrderWorkTicket.getStatus();
			Date statusDate=serviceOrderWorkTicket.getStatusDate();
			Integer statusNumber=serviceOrderWorkTicket.getStatusNumber();
			serviceOrder.setInlandCode(inlandCode);
			List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
			synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus);
			List<Object> trackingStatusRecords=findTrackingStatusRecords(linkedShipNumber,serviceOrder);
			synchornizeTrackingStatus(trackingStatusRecords,trackingStatus,serviceOrder);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
        getNotesForIconChange();
        if(serviceOrder.getCorpID().equals("CWMS") && oiJobList!= null &&  oiJobList.indexOf(serviceOrder.getJob().toString().trim())>=0  && workTicket.getCreatedBy().contains("System :") && !workTicket.getTargetActual().equals("C")){
	    	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
				String chkDate1 = nowYYYYMMDD1.toString();
				if(usertype.equalsIgnoreCase("ACCOUNT")){
					usertype = "AC_"+getRequest().getRemoteUser();
				}else if(usertype.equalsIgnoreCase("DRIVER")){
					usertype = "DR_"+getRequest().getRemoteUser();
				}else{
					usertype = getRequest().getRemoteUser();
				}	
	    	operationsIntelligenceManager.updateOIDate(workTicket.getTicket(), workTicket.getShipNumber(), chkDate1,usertype,workTicket.getTargetActual());
	    }
        if(serviceOrder.getCorpID().equals("CWMS") &&  oiJobList!=null && oiJobList.indexOf(serviceOrder.getJob().toString().trim())>=0  && workTicket.getCreatedBy().contains("System :") 
        		&& workTicket.getTargetActual().equals("C")){
        	if(usertype.equalsIgnoreCase("ACCOUNT")){
        		usertype = "AC_"+getRequest().getRemoteUser();
        	}else if(usertype.equalsIgnoreCase("DRIVER")){
        		usertype = "DR_"+getRequest().getRemoteUser();
        	}else{
        		usertype = getRequest().getRemoteUser();
        	}
	    	   	operationsIntelligenceManager.updateOIResourceTicketAndDate(workTicket.getTicket(), workTicket.getShipNumber(), usertype);
	    }
        if(workTicket.getId()!=null && workTicket.getService().equals("RC")){
		    insertIntoCoraxLog(workTicket,"Supplier");
		    }
		   if(workTicket.getId()!=null && workTicket.getService().equals("HO")){
			   insertIntoCoraxLog(workTicket,"Customer");
		    }

		if (!isNew) {
			flag3 = "true";
			return INPUT;
		} else {
			i = Long.parseLong(workTicketManager.findMaximumId().get(0).toString());
			workTicket = workTicketManager.get(i);
			flag3 = "true";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}

public String checkHubLimit() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (id == null);
		String key = "";
		countCheck="";
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		Boolean wareHouseCheck = false;
		try
		{
		wareHouseCheck = workTicketManager.getCheckedWareHouse(warehouse, sessionCorpID);
		if(wareHouseCheck == true){
			wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
			if((!(status.equalsIgnoreCase("C"))) && (wtServiceTypeDyna.contains(workticketService))){
				user = userManager.getUserByUsername(getRequest().getRemoteUser());
				roles = user.getRoles();				
				operationsHubLimits  = workTicketManager.getOperationsHub(warehouse, sessionCorpID);
				if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
		    		
				}else{
    	   		Boolean checkDate1 = false;
    	   		if(!isNew){
    			   checkDate1 = workTicketManager.checkDate1(warehouse, workticketService, date1, id, sessionCorpID, estimatedCubicFeet, estimatedWeight, date2);
    	   		}    		      		   		 
    		    if(isNew || (!isNew && checkDate1== true)){
    			   if (operationsHubLimits != null){
    				   Long dailyCount = Long.parseLong(operationsHubLimits.getDailyOpHubLimit());
    				   Long dailyCountPC = Long.parseLong(operationsHubLimits.getDailyOpHubLimitPC());
    				   Double dailyMaxEstWt = 0.00;
    				   Double dailyMaxEstVol = 0.00;
    				   if(operationsHubLimits.getDailyMaxEstWt() != null){
    				        dailyMaxEstWt = Double.parseDouble(operationsHubLimits.getDailyMaxEstWt().toString());
    				   }
    				   if(operationsHubLimits.getDailyMaxEstVol() != null){
    				        dailyMaxEstVol = Double.parseDouble(operationsHubLimits.getDailyMaxEstVol().toString()); 
    				   }
    				   countCheck = workTicketManager.getDailyOpsLimit(date1, date2, dailyCount, dailyCountPC, sessionCorpID, operationsHubLimits.getHubID(), estimatedWeight, estimatedCubicFeet, dailyMaxEstWt, dailyMaxEstVol,id);
	    			   if(countCheck.length()>15){	    				   
	    				   exceedDate = countCheck.substring(countCheck.indexOf(":")+1, countCheck.length());
		    			   countCheck = countCheck.substring(0, countCheck.indexOf(":"));
		    			   if(countCheck.equalsIgnoreCase("GreaterWeightVolume")){
		    				   if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
		    					   checkLimit="Y"; 
			    			   }
		    				}
			    			if(countCheck.equalsIgnoreCase("Greater")){
			    				if(company.getWorkticketQueue()!=null && company.getWorkticketQueue()){		    			   
			    					checkLimit="Y"; 
				    			}
			    			}	    				   
	    			   }
	    			   
	    			  if(!isNew && checkDate1== true){
	       		    	String chkDate1="";
	       		    	String chkDate2="";
	       		    	if (!(date1 == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
	       					chkDate1 = nowYYYYMMDD1.toString();
	       				}

	       				if (!(date2 == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
	       					chkDate2 = nowYYYYMMDD2.toString();
	       				}
	       		    	List itemsJbkList = workTicketManager.findWorkTicketResourceList(id);
	       		    	if(itemsJbkList!=null && (!(itemsJbkList.isEmpty()))){
	       		    		Iterator iter = itemsJbkList.iterator();
	       		    		while(iter.hasNext()){
	       		    			Long itemsJbkId = Long.parseLong(iter.next().toString());
	       		    			ItemsJbkEquip itemsJbkEquip = itemsJbkEquipManager.get(itemsJbkId);
	       						int usedEstQuanitity = 0;
	       						List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(),chkDate1, chkDate2, operationsHubLimits.getHubID(), workticketService, sessionCorpID,itemsJbkEquip.getTicket());
	       						if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty()))){
	       							   usedEstQuanitity = Integer.parseInt(usedEstQuanitityList.get(0).toString());
	       						}
	       						
	       				       List dailyResourceLimitList = itemsJbkEquipManager.getDailyResourceLimit(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(), chkDate1, chkDate2, operationsHubLimits.getHubID(), sessionCorpID);
	       					   int dailyResourceLimit = 0;
	   						   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && dailyResourceLimitList.get(0)!=null){
	   							   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
	   							   dailyResourceLimit = dailyQty.intValue();							
	   						   }
	   						   if(usedEstQuanitity > dailyResourceLimit && dailyResourceLimit>0 && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
	   							   checkLimit="Y";
	   						   }
	       		    		}
	       		    	}
	       		    }
	    			   
	    		   }
    		    }
  	           }
			  }				 
		    }}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	
	@SkipValidation
	public String checkHubLimitForResourceSOAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		countCheck="";		
		try
		{
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		roles = user.getRoles();
		Boolean wareHouseCheck = false;
		if(tickets != null){
			String ticketArr[]= tickets.split("-");
			String ticketId = ticketArr[0];
			String type = ticketArr[1];
			String descript = ticketArr[2];
			String qty = ticketArr[3];
			
			WorkTicket workTicket = workTicketManager.get(Long.valueOf(ticketId));
			if(!("P".equals(workTicket.getTargetActual()))){
			wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);

			if(wareHouseCheck == true){
				wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
				if((!(workTicket.getTargetActual().equals("C"))) && wtServiceTypeDyna.contains(workTicket.getService().trim()) ){
					operationsHubLimits  = workTicketManager.getOperationsHub(workTicket.getWarehouse(), sessionCorpID);
					if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
					}else{
	       		    	String chkDate1="";
	       		    	String chkDate2="";
	       		    	if (!(workTicket.getDate1() == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
	       					chkDate1 = nowYYYYMMDD1.toString();
	       				}

	       				if (!(workTicket.getDate2() == null)) {
	       					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	       					StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
	       					chkDate2 = nowYYYYMMDD2.toString();
	       				}
   						int usedEstQuanitity = Integer.parseInt(qty);
   						
   				       List dailyResourceLimitList = itemsJbkEquipManager.getDailyResourceLimit(type, descript, chkDate1, chkDate2, operationsHubLimits.getHubID(), sessionCorpID);
   					   int dailyResourceLimit = 0;
					   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty()))){
						   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
						   dailyResourceLimit = dailyQty.intValue();							
					   }
					   if(usedEstQuanitity > dailyResourceLimit && dailyResourceLimit>0 && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
						   ticketPendingStatus="Y";
					   }
					}
				}				 
			}	
			}
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String checkHubLimitForResource(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (id == null);
		String key = "";
		countCheck="";		
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		roles = user.getRoles();
		Boolean wareHouseCheck = false;
		Set set = new HashSet();
		if(tickets != null){
			String ticketAll[] = tickets.split(",");
			for (int i = 0; i < ticketAll.length; i++) {
				String ticketArr[]= ticketAll[i].split("-");
					String ticketId = ticketArr[0];
					String ticketNo = ticketArr[1];
					String day = ticketArr[2];
					
					WorkTicket workTicket = workTicketManager.get(Long.valueOf(ticketId));
					if(!("P".equals(workTicket.getTargetActual()))){
					wareHouseCheck = workTicketManager.getCheckedWareHouse(workTicket.getWarehouse(), sessionCorpID);
		
					if(wareHouseCheck == true){
						wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
						if((!(workTicket.getTargetActual().equals("C"))) && (wtServiceTypeDyna.contains(workTicket.getService()))){
											
							operationsHubLimits  = workTicketManager.getOperationsHub(workTicket.getWarehouse(), sessionCorpID);
							if((roles.toString().indexOf("ROLE_OPS_MGMT")) > -1){
					    		
							}else{			    	   		
				       		    	String chkDate1="";
				       		    	String chkDate2="";
				       		    	if (!(workTicket.getDate1() == null)) {
				       					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				       					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workTicket.getDate1()));
				       					chkDate1 = nowYYYYMMDD1.toString();
				       				}
			
				       				if (!(workTicket.getDate2() == null)) {
				       					SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				       					StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(workTicket.getDate2()));
				       					chkDate2 = nowYYYYMMDD2.toString();
				       				}
				       				day = day.replaceFirst("Day", "").trim();
				       				List<ItemsJbkEquip> itemsJbkList =  itemsJbkEquipManager.getResource(sessionCorpID, shipNumber, day);
				       				for (ItemsJbkEquip itemsJbkEquip : itemsJbkList) {
				       						int usedEstQuanitity = 0;
				       						List usedEstQuanitityList = itemsJbkEquipManager.usedEstQuanitityList(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(),chkDate1, chkDate2, operationsHubLimits.getHubID(), workTicket.getService(), sessionCorpID,workTicket.getTicket());
				       						if(usedEstQuanitityList!=null && (!(usedEstQuanitityList.isEmpty())) && usedEstQuanitityList.get(0)!=null){
				       							   usedEstQuanitity = Integer.parseInt(usedEstQuanitityList.get(0).toString());
				       							usedEstQuanitity=usedEstQuanitity+itemsJbkEquip.getQty();
				       						}
				       						
				       				       List dailyResourceLimitList = itemsJbkEquipManager.getDailyResourceLimit(itemsJbkEquip.getType(), itemsJbkEquip.getDescript(), chkDate1, chkDate2, operationsHubLimits.getHubID(), sessionCorpID);
				       					   int dailyResourceLimit = 0;
				   						   if(dailyResourceLimitList!=null && (!(dailyResourceLimitList.isEmpty())) && dailyResourceLimitList.get(0)!=null){
				   							   Double dailyQty=  (Double.parseDouble(dailyResourceLimitList.get(0).toString()));
				   							   dailyResourceLimit = dailyQty.intValue();							
				   						   }
				   						   if(usedEstQuanitity > dailyResourceLimit && dailyResourceLimit>0 && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
				   							   checkLimit="Y";
				   							   
				   							   if(ticketPendingStatus != null && ticketPendingStatus.length() > 0){
				   								   ticketPendingStatus=ticketPendingStatus+","+workTicket.getTicket();
				   							   }else{
				   								   ticketPendingStatus=workTicket.getTicket().toString();
				   							   }
				   							break;
				   						   }
				       		    	}
			  	        }
					}				 
				}	
			}
			}
		}
		String s = set.toString().replace("[", "").replace("]", ""); 
		ticketPendingStatus = checkLimit+ticketPendingStatus;
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	@SkipValidation
	public Map<String,String> findByParameterLocal(String parameter){ 
		localParameter = new HashMap<String, String>();
		if(parameterMap.containsKey(parameter)){
			localParameter = parameterMap.get(parameter);
		}
		return localParameter;
	}
	
	@SkipValidation
	public String ticketDocsMethod(){
		myFileList = new ArrayList();
		workTicket = workTicketManager.get(id);
		try{
			String myFileIds = workTicket.getAttachedFile();
			if(myFileIds!=null && !myFileIds.equals("")){
				if(myFileIds.contains(",")){
					String [] myFileIdArr = myFileIds.split(",");
					for (String tempId : myFileIdArr) {
						Long myDocId = Long.parseLong(tempId);
						MyFile myFile = myFileManager.get(myDocId) ;
						myFileList.add(myFile);
					}
				}else{
					Long myDocId = Long.parseLong(myFileIds);
					MyFile myFile = myFileManager.get(myDocId) ;
					myFileList.add(myFile);
				}
				
			}
			}catch(Exception e){
				e.printStackTrace();
				logger.warn("Error in attaching doc. "+e.getStackTrace()[0]);
			}
		return SUCCESS;
	}
	
	@SkipValidation
	public String attachedDoc(){
		try
		{
		serviceOrder   = serviceOrderManager.get(sid);
		workTicket = workTicketManager.get(id);
		myFileList = myFileManager.getListByCustomerNumberForTicket(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber(), "true") ;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SkipValidation
	public String attchedSelectedDocMethod(){
		try
		{
		workTicket = workTicketManager.get(id);
		workTicket.setAttachedFile(myFileListId);
		workTicket = workTicketManager.save(workTicket);
		temp = "load";
		attachedDoc();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
		
	}
	//	 Method used in if any change is done in calling from WorkTicket form for auto save button
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		validateFormNav = "OK";
		String s = save();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	//	  End of Method.

	// Method for getting the values for all comboboxes calling from WorkTicket form for dropdown list.	
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 Gson gson = new Gson();
		 try
		 {
		 String parameters="'LEAD','SURVEY','STATE','COUNTRY', 'JOB','SCHEDULE','PAYTYPE','ONHANDSTORAGE','ORGANIZA','PEAK', 'MODE','SERVICE','STORAGEOUT','TCKTSERVC','DEPT','HOUSE','PEAK','TCKTSERVC','DEPT','HOUSE', 'CONFIRM','TCKTACTN','WOINSTR','MEASURE','BILLSTATUS','YESNO','LOCTYPE','PARKING','OPSHUB','STORAGE_TYPE','STORAGEMODE','PARKING_DOCK',  'SITE_LIST','ELEVATOR_DOCK','COUNTRY','ITEMS','OPSHUB','HOUSE'";
		LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
		lead = new LinkedHashMap<String, String>();
		survey = new LinkedHashMap<String, String>();
		state  = new LinkedHashMap<String, String>();
		country = new LinkedHashMap<String, String>();
		schedule = new LinkedHashMap<String, String>();
		paytype = new LinkedHashMap<String, String>();
		job = new LinkedHashMap<String, String>();
		onhandstorage = new LinkedHashMap<String, String>();
		organiza = new LinkedHashMap<String, String>();
		peak = new LinkedHashMap<String, String>();
		mode = new LinkedHashMap<String, String>();
		service = new LinkedHashMap<String, String>();
		storageOut = new LinkedHashMap<String, String>();
		tcktservc = new LinkedHashMap<String, String>();
		house = new LinkedHashMap<String, String>();
		confirm = new LinkedHashMap<String, String>();
		tcktactn = new LinkedHashMap<String, String>();
		woinstr = new LinkedHashMap<String, String>();
		instructionCodeWithEnblCountry = new LinkedHashMap<String, String>();
		measure = new LinkedHashMap<String, String>();
		billStatus = new LinkedHashMap<String, String>();
		yesno = new LinkedHashMap<String, String>();
		locTYPE = new LinkedHashMap<String, String>();
		parking = new LinkedHashMap<String, String>();
		opshub = new ArrayList<String>();
		storageType = new LinkedHashMap<String, String>();
		storageMode = new LinkedHashMap<String, String>();
		parkingList = new LinkedHashMap<String, String>();
		siteList = new LinkedHashMap<String, String>();
		elevatorList = new LinkedHashMap<String, String>();
		countryCod = new LinkedHashMap<String, String>();
		countryDsc = new LinkedHashMap<String, String>();
		itemsList = new LinkedHashMap<String, String>();
		loctype_isactive = new LinkedHashMap<String, String>();
		woinstr_isactive = new LinkedHashMap<String, String>();
		tcktactn_isactive= new LinkedHashMap<String, String>();
		tcktservc_isactive= new LinkedHashMap<String, String>();
		billStatus_isactive=new LinkedHashMap<String, String>();
		house_isactive=new LinkedHashMap<String, String>();
		onhandstorage_isactive=new LinkedHashMap<String, String>();
		storageOut_isactive=new LinkedHashMap<String, String>();
		hub =new LinkedHashMap<String, String>();
		houseHub =new LinkedHashMap<String, String>();
		house =new LinkedHashMap<String, String>();
		List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
		for (RefMasterDTO refObj : allParamValue) {
		 	if(refObj.getParameter().trim().equalsIgnoreCase("OPSHUB")){
		 		opshub.add(refObj.getCode());
		 	}else if(refObj.getParameter().trim().equalsIgnoreCase("COUNTRY")){
		 		country.put(refObj.getDescription().trim(), refObj.getDescription().trim());
		 		countryCod.put(refObj.getCode().trim(), refObj.getDescription().trim());
		 		countryDsc.put(refObj.getDescription().trim(), refObj.getCode().trim());
		 	}else if(refObj.getParameter().trim().equalsIgnoreCase("STATE")){
		 		state.put(refObj.getDescription().trim(), refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("LOCTYPE")){
		 		loctype_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("TCKTACTN")){
		 		tcktactn_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		tcktactn.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("TCKTSERVC")){
		 		tcktservc_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		tcktservc.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("BILLSTATUS")){
		 		billStatus_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		billStatus.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("HOUSE")){
		 		house_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		house.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("ONHANDSTORAGE")){
		 		onhandstorage_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		onhandstorage.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("STORAGEOUT")){
		 		storageOut_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 		storageOut.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("OPSHUB")){
		 		hub.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("HOUSE")){
		 		house.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 		houseHub.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 	}
		 	
		 	else if(refObj.getParameter().trim().equalsIgnoreCase("WOINSTR")){
		 		woinstr.put(refObj.getCode().trim(), refObj.getDescription().trim());
		 		woinstr_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(), refObj.getDescription().trim());
		 		if(refObj.getBucket2()!=null && !refObj.getBucket2().trim().equals("")){
		 			instructionCodeWithEnblCountry.put(refObj.getCode().trim()+":"+ refObj.getDescription().trim(),refObj.getBucket2().trim());
		 		}
		 	}else {
		 		if(parameterMap.containsKey(refObj.getParameter().trim())){
		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		 		}else{
		 			tempParemeterMap = new LinkedHashMap<String, String>();
		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
		 		}
		 	}
		
	}
		
		lead = findByParameterLocal("LEAD");
		survey= findByParameterLocal("SURVEY");
		schedule=  findByParameterLocal("SCHEDULE");
		paytype=  findByParameterLocal("PAYTYPE");
		job=  findByParameterLocal("JOB");
		organiza=  findByParameterLocal("ORGANIZA");
		peak=  findByParameterLocal("PEAK");
		mode=  findByParameterLocal("MODE");
		service=  findByParameterLocal("SERVICE");
		confirm=  findByParameterLocal("CONFIRM");
		measure=  findByParameterLocal("MEASURE");
		yesno=  findByParameterLocal("YESNO");
		locTYPE=  findByParameterLocal("LOCTYPE");
		parking=  findByParameterLocal("PARKING");
		storageType=  findByParameterLocal("STORAGE_TYPE");
		storageMode=  findByParameterLocal("STORAGEMODE");
		parkingList=  findByParameterLocal("PARKING_DOCK");
		siteList=  findByParameterLocal("SITE_LIST");
		elevatorList=  findByParameterLocal("ELEVATOR_DOCK");
		itemsList=  findByParameterLocal("ITEMS");
		material = itemsJEquipManager.findMaterialByContract(contract, itemType, sessionCorpID);
		jsonCountryCodeText = gson.toJson(countryCod);
		String roles ="'ROLE_COORD','ROLE_SALE','ROLE_QC','ROLE_EXECUTIVE'";
		exec = new TreeMap<String, String>();
		List <UserDTO> allUser = refMasterManager.getAllUser(corpId,roles);
		for (UserDTO userDTO : allUser) {
			if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
				coord.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
			}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){
				sale.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
			}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_QC")){
				qc.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
			}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
				exec.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
			}
		}
	 	
		List<String> list = new ArrayList<String>(itemsList.keySet());
		Collections.sort(list,new sortedItemListComp());
		Collections.sort(list,new sortedItemListComp());
		Iterator it = list.iterator();
				while(it.hasNext())
				{
				String aa = (String) it.next();
				sortedItemList.put(aa, aa);
				}
				
				tempWoinstrMap = new LinkedHashMap<String, String>();
				tempWoinstrMap=woinstr;
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 
	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	class sortedItemListComp implements Comparator
	{
		public int compare(Object o1, Object o2) {
			
			String num1 = ((String) o1).split(":")[0];
			Integer aa =Integer.parseInt(num1);
			  String num2 = ((String) o2).split(":")[0];
			  Integer bb =Integer.parseInt(num2);
			  return  aa.compareTo(bb);
		}
	}

	//	  End of Method.

	// Method getting the count of notes according to which the icon color changes in form	
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	try
	{
		List noteTicketBilling = notesManager.countForTicketBillingNotes(workTicket.getTicket());
		List noteTicketStaff = notesManager.countForTicketStaffNotes(workTicket.getTicket());
		List noteTicketScheduling = notesManager.countForTicketSchedulingNotes(workTicket.getTicket());
		List noteTicketOrigin = notesManager.countForTicketOriginNotes(workTicket.getTicket());
		List noteTicketDestination = notesManager.countForTicketDestinationNotes(workTicket.getTicket());
		List noteTicketTicket = notesManager.countForTicketNotes(workTicket.getTicket());
		List noteTicketStorage = notesManager.countTicketStorageNotes(workTicket.getTicket());

		if (noteTicketBilling.isEmpty()) {
			countTicketBillingNotes = "0";
		} else {
			countTicketBillingNotes="0";
			if((noteTicketBilling).get(0)!=null){
			countTicketBillingNotes = ((noteTicketBilling).get(0)).toString();
			}
		}
		if (noteTicketStaff.isEmpty()) {
			countTicketStaffNotes = "0";
		} else {
			countTicketStaffNotes="0";
			if((noteTicketStaff).get(0)!=null){
			countTicketStaffNotes = ((noteTicketStaff).get(0)).toString();
			}
		}
		if (noteTicketScheduling.isEmpty()) {
			countTicketSchedulingNotes = "0";
		} else {
			countTicketSchedulingNotes = "0";
			if(noteTicketScheduling.get(0)!=null){
			countTicketSchedulingNotes = ((noteTicketScheduling).get(0)).toString();
			}
		}
		if (noteTicketOrigin.isEmpty()) {
			countTicketOriginNotes = "0";
		} else {
			countTicketOriginNotes = "0";
			if((noteTicketOrigin).get(0)!=null){
			countTicketOriginNotes = ((noteTicketOrigin).get(0)).toString();
			}
		}
		if (noteTicketDestination.isEmpty()) {
			countTicketDestinationNotes = "0";
		} else {
			countTicketDestinationNotes = "0";
			if((noteTicketDestination).get(0)!=null){
			countTicketDestinationNotes = ((noteTicketDestination).get(0)).toString();
			}
		}
		if (noteTicketTicket.isEmpty()) {
			countForTicketNotes = "0";
		} else {
			countForTicketNotes = "0";
			if((noteTicketTicket).get(0)!=null){
			countForTicketNotes = ((noteTicketTicket).get(0)).toString();
			}
		}
		if (noteTicketStorage.isEmpty()) {
			countTicketStorageNotes = "0";
		} else {
			countTicketStorageNotes = "0";
			if((noteTicketStorage).get(0)!=null){
			countTicketStorageNotes = ((noteTicketStorage).get(0)).toString();
			}
		}}
	catch(Exception e)
	{e.printStackTrace();}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of storages calling from  workTicket form.	
	@SkipValidation
	public String storagesList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		storages = workTicketManager.getStorageList(workTicket.getShipNumber());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.
	
	//	 Method to get list of storages calling from  workTicket form.	
	@SkipValidation
	public String storagesLists() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of locations calling from  workTicket form.	
	@SkipValidation
	public String locationsList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		storage = storageManager.get(id1);
		locationId = storage.getLocationId();
		locationId = locationId.substring(0, 2);
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		location = locationManager.getByLocation(storage.getLocationId());
		if(location==null){
			String message="Location ID missing so Move not possible!";
			errorMessage(getText(message));
			hitFlag="2";
		}else{
			type = location.getType();
		}
		
		ls = locationManager.findByLocationWH(locationId, occupied, type, warehouse);
		locations = new HashSet(ls);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	@SkipValidation
	public String moveStorageUnitLocationList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		storage = storageManager.get(id1);
		locationId = storage.getLocationId();
		locationId = locationId.substring(0, 2);
		location = locationManager.getByLocation(storage.getLocationId());
		storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
		
		if(storageLibrary==null){
			String message="Storage ID missing so Move not possible!";
			errorMessage(getText(message));
			hitFlag="0";
		}
		if(location==null){
			String message="Location ID missing so Move not possible!";
			errorMessage(getText(message));
			hitFlag="0";
		}else{
			type = location.getType();
		}
		ls = locationManager.findByLocationWH(locationId, occupied, type, warehouse);
		locations = new HashSet(ls);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String moveStorageUnitStorageList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		storage = storageManager.get(id1);
		locationId = storage.getLocationId();
		storageId = storage.getStorageId();
		location = locationManager.getByLocation(storage.getLocationId());
		storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
		
		if(storageLibrary==null){
			String message="Storage ID missing so Move not possible!";
			errorMessage(getText(message));
			hitFlag="0";
		}
		if(location==null){
			String message="Location ID missing so Move not possible!";
			errorMessage(getText(message));
			hitFlag="0";
		}else{
			type = location.getType();
		}
		ls = storageManager.findStoragesByLocation(storage.getLocationId());
		locations = new HashSet(ls);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	//	 Method to get list of locations calling from  workTicket form.	
	@SkipValidation
	public String locationsLists() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		locations = new HashSet(locationManager.getAll());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of locations calling from  workTicket form.
	@SkipValidation
	public String getAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		temp = workTicketManager.getAddress(Long.parseLong(custId), sessionCorpID, addType);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
		
	}

	@SkipValidation
	public String getAddressDescription()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		descp = workTicketManager.getAddressDescription(Long.parseLong(custId), sessionCorpID, descType);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
		
	}

	@SkipValidation
	public String getStandardAddressDescription()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		descp = workTicketManager.getStandardAddressDescription(Long.parseLong(stdAddId), sessionCorpID);		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
		
	}
	@SkipValidation
	public String itemsJEquipList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		itemsJEquips = itemsJEquipManager.findByType(itemType, wcontract, id);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Equipment calling from  workTicket form.	
	@SkipValidation
	public String itemsJEquipListsearch() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		boolean myType = (itemsJEquip.getType() == null);
		boolean myDescription = (itemsJEquip.getDescript() == null);
		if (!myType || !myDescription) {
			itemsJEquips = itemsJEquipManager.findByDescript(itemsJEquip.getType(), itemsJEquip.getDescript(), wcontract, id);
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.		
	@SkipValidation
	public String searchMaterials() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String contract = "";
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder=serviceOrderManager.get(workTicket.getServiceOrderId());
		billing=billingManager.get(serviceOrder.getId());		
		if ((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()) {
			Object tempContract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0);
			if(tempContract!=null)
			{
			contract = tempContract.toString();
			}
		} else {
			contract=billing.getContract();
		}
		boolean myDescription = (materialDescription == null);
		String itemTypes = itemType;
		if (!myDescription) {
			materialsList = itemsJEquipManager.searchMaterials(materialDescription, workTicket.getTicket().toString(), contract, sessionCorpID, itemTypes);
		}}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.	
	@SkipValidation
	public String itemsJbkEquipList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		trackingStatus = trackingStatusManager.get(sid);
		miscellaneous = miscellaneousManager.get(sid);
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		
		materialsList = new ArrayList();
		String contract = "";
		workTicket = workTicketManager.get(id);
		if ((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()) {
			Object tempContract=itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber()).get(0);
			if(tempContract!=null)
			{
			contract = tempContract.toString();
			}
		} else {
			contract = billing.getContract();
		}
		wrehouse=workTicket.getWarehouse();
  	    if(wrehouse!=null || wrehouse!=""){
  	    	List wrehouseDescTemp =itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID);
  	    	if(wrehouseDescTemp!=null && !wrehouseDescTemp.isEmpty() && wrehouseDescTemp.get(0)!=null)
  	    wrehouseDesc= wrehouseDescTemp.get(0).toString();
  	    }
		Long ticket = workTicket.getTicket();
		if (itemType.equalsIgnoreCase("M")) {
			materialsList = itemsJEquipManager.findMaterials(id,ticket.toString(), contract, sessionCorpID, itemType);
		} else {
			materialsList = itemsJEquipManager.findMaterials(id,ticket.toString(), contract, sessionCorpID, itemType);
		}}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.	    
	@SkipValidation
	public String searchEqipment() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		billing = billingManager.get(serviceOrder.getId());
		resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
		workTicket = workTicketManager.get(id);
		
		String contract = "";
		if ((itemsJEquipManager.checkforBilling(workTicket.getShipNumber())).isEmpty()) {
			List tempContract = itemsJEquipManager.findContractFromCustomerFile(workTicket.getSequenceNumber());
			if(tempContract!=null && !(tempContract.isEmpty()) && tempContract.get(0)!=null){
				contract = tempContract.get(0).toString();
			}
		} else {
			contract = billing.getContract();
		}
		wrehouse=workTicket.getWarehouse();
  	    if(wrehouse!=null || wrehouse!=""){
  	    	List list = itemsJEquipManager.findWareHouseDesc(wrehouse,sessionCorpID);
  	    	if(list!=null &&  !list.isEmpty() && list.get(0)!=null){
  	    		wrehouseDesc= list.get(0).toString();
  	    	}
  	    }
  	    materialsList = itemsJEquipManager.findMaterials(id,workTicket.getTicket().toString(), contract, sessionCorpID, itemType);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.	
	@SkipValidation
	public String workTicketsList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		serviceOrder = serviceOrderManager.get(id);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		trackingStatus = trackingStatusManager.get(id);
		miscellaneous = miscellaneousManager.get(id);
		billing = billingManager.get(id);
		customerFile = serviceOrder.getCustomerFile();
		workTickets=new HashSet(serviceOrderManager.getWorkTicketList(serviceOrder.getId()));
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.	
	@SkipValidation
	public String customerWTList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{		
		workTickets = workTicketManager.findByCustomerTicket(ticket, sequenceNumber);
		rowcount = workTickets.size();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	//	  End of Method.

	//	 Method to get list of Materials calling from  workTicket form.	
	@SkipValidation
	public String searchLocation() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		 billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		ls = locationManager.findByLocationWH(locationId, occupied, type, warehouse);
		locations = new HashSet(ls);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String searchStorageLibrary() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		ls = storageLibraryManager.findStorageLibraryList(sessionCorpID);
		storageLibraries = new HashSet(ls);
		} catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String storageLibraryUnitSearch(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		ls=storageLibraryManager.storageLibraryUnitSearch(storageId,storageTypeValue,storageModeType);
		storageLibraries=new HashSet(ls);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	   }

	//	  End of Method.

	@SkipValidation
	public String searchLocationMove() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		ls = locationManager.findByLocationWHMove(locationId, occupied, type, warehouse);
		locations = new HashSet(ls);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String searchLocatesUnitMove() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		ls = locationManager.findByLocationWHMove(locationId, occupied, type, warehouse);
		locations = new HashSet(ls);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	//	 Method to get list of Materials calling from  workTicket form.		 
	public String timeSheets() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTickets = new HashSet(workTicketManager.getAll());
		payrolls = new HashSet(payrollManager.getAll());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}

	//	  End of Method.
	public String findCityStateValid(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String originZipValidCode=workTicket.getZip();
        List list=	customerFileManager.findCityState(originZipValidCode,sessionCorpID);
        if(! list.isEmpty()){
        	return "valid" ;
        }else{
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return null;
        }
       
          }
    public String findDestCityStateValid(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	String destZipValidCode=workTicket.getDestinationZip();
	    List list=	customerFileManager.findCityState(destZipValidCode,sessionCorpID);
	    if(! list.isEmpty()){
	    	return "valid" ;
	    }else{
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return null;
	    }
	
    }
    
  public String workPlan(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");		
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		houseHub = refMasterManager.findByParameterHubWithWarehouse(sessionCorpID, "HOUSE",hub);
		
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())&&!checkCoordinator) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					operationChk ="H";
				}else{
		            if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
		            	operationChk ="H";
		            }else{
		            	operationChk ="W";
		            }
				}
			}
		}else{
	    	operationChk ="H";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String workPlanList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");	
		houseHub = refMasterManager.findByParameterHubWithWarehouse(sessionCorpID, "HOUSE",hub);
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		hubID=hubID.replaceAll(",","");
		hubID=hubID.trim();
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())&&!checkCoordinator) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					operationChk ="H";
				}else{
		            if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
		            	operationChk ="H";
		            }else{
		            	operationChk ="W";
		            }
				}
			}
		}else{
	    	operationChk ="H";
		}

		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yyyy");
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(fromDate));
fDate = nowYYYYMMDD2.toString();
		
		StringBuilder nowYYYMMDD3 = new StringBuilder(dateformatYYYYMMDD2.format(toDate));
tDate= nowYYYMMDD3.toString();
paraMeter1="false";
workListDetails= workTicketManager.getWorkPlanList(fromDate, toDate, hubID,wHouse,wareHouseUtilization, sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
return SUCCESS;
	}
	
	public String summaryList(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");		
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		houseHub = refMasterManager.findByParameterHubWithWarehouse(sessionCorpID, "HOUSE",hub);
		
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())&&!checkCoordinator) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getHubWarehouseOperationalLimit()!=null && systemDefault.getHubWarehouseOperationalLimit().equalsIgnoreCase("Crew")){
					operationChk ="H";
				}else{
		            if(systemDefault.getOperationMgmtBy()!=null && systemDefault.getOperationMgmtBy().equalsIgnoreCase("By Hub")){
		            	operationChk ="H";
		            }else{
		            	operationChk ="W";
		            }
				}
			}
		}else{
	    	operationChk ="H";
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yyyy");
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(fromDate));
fDate = nowYYYYMMDD2.toString();
		
		StringBuilder nowYYYMMDD3 = new StringBuilder(dateformatYYYYMMDD2.format(toDate));
tDate= nowYYYMMDD3.toString();


	    paraMeter1="true";
		workList = workTicketManager.getWorkPlanList(fromDate, toDate, hubID,wHouse,wareHouseUtilization, sessionCorpID);
		
		Map <Map, Map> crewWHAndHubVal = new HashMap<Map, Map>();
		if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
			String hubWorkList = workTicketManager.getHubValueWorkPlanList(hubID, sessionCorpID);
			if((hubWorkList!=null) && (!hubWorkList.equals(""))){
				String	hubWorkListTemp[] = hubWorkList.split("~");
				String dailyOpHub = (hubWorkListTemp[0]);
				String dailyWtHub = (hubWorkListTemp[1]);
				
				SortedMap <String, String> hubMap = new TreeMap<String, String>();
				hubMap.put(dailyOpHub, dailyWtHub);
				getRequest().setAttribute("hubMap", hubMap);		
			  }	
			crewWHAndHubVal = workTicketManager.findRequiredCrewValue(fromDate,toDate,hubID,sessionCorpID);
		}
		SortedMap <String, Map> dateMap = new TreeMap<String, Map>();
		SortedMap <String, String> utilizationMap = new TreeMap<String, String>();
		
		String dailyHubLimit = "";
		BigDecimal dailyWtLimit = new BigDecimal(0.00);
		BigDecimal reqCrew = new BigDecimal(0);
		Iterator it = workList.iterator();
		while (it.hasNext()){
			Object listObject = it.next();
			
			String warehouse = (String) ((WorkPlanDTO) listObject).getWarehouse();
			BigDecimal avgWgt = new BigDecimal(((WorkPlanDTO) listObject).getAvgWt().toString());
			Date displayDate = (Date) ((WorkPlanDTO) listObject).getDateDisplay();
			String service = (String) ((WorkPlanDTO) listObject).getService();
			if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
				dailyHubLimit = (String) ((WorkPlanDTO) listObject).getDailyOpHubLimit();
				dailyWtLimit = new BigDecimal(((WorkPlanDTO) listObject).getDailyMaxEstWt().toString()); 
				reqCrew = new BigDecimal(((WorkPlanDTO) listObject).getRequiredcrew().toString());
			}
			
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD2.format(displayDate));
			String date = nowYYYYMMDD1.toString();
			
			SortedMap <String, Map> warehouseMap = (SortedMap<String, Map>)dateMap.get(date);
			if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
				utilizationMap.put(warehouse, dailyHubLimit+"~"+dailyWtLimit);			
			}
			if(warehouseMap == null){
				warehouseMap = new TreeMap<String, Map>();
				dateMap.put(date, warehouseMap);
			}
			SortedMap <String, Map> serviceMap = (SortedMap<String, Map>)warehouseMap.get(warehouse);
			
			Map valueMap = null;
			if(serviceMap == null){
				serviceMap = new TreeMap<String, Map>();
				
				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0); 
				valueMap.put("crew", 0); 
				serviceMap.put("DL", valueMap);

				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0); 
				valueMap.put("crew", 0);
				serviceMap.put("DU", valueMap);
				
				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0); 
				valueMap.put("crew", 0);
				serviceMap.put("LD", valueMap);
				
				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0);
				valueMap.put("crew", 0);
				serviceMap.put("PK", valueMap);

				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0);
				valueMap.put("crew", 0);
				serviceMap.put("UP", valueMap);
				
				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0); 
				valueMap.put("crew", 0);
				serviceMap.put("PL", valueMap);
				
				warehouseMap.put(warehouse, serviceMap);
			}
			
			valueMap = serviceMap.get(service);
			if (valueMap == null) {
				valueMap = new HashMap<String, String>();
				valueMap.put("count", 0); 
				valueMap.put("sum", 0); 
				valueMap.put("crew", 0);
				serviceMap.put(service, valueMap);				
			}
			
			Integer count = 0;
			if (valueMap.get("count") == null ){
				count = 0;
			} else {
			 	count = new Integer(valueMap.get("count").toString());
			}
			valueMap.put("count", count+1);
			 
			BigDecimal sum = new BigDecimal(0.00);
			if (valueMap.get("sum") == null ){
				sum = new BigDecimal(0.00);
			} else {
			 	sum = new BigDecimal(valueMap.get("sum").toString());
			}
			valueMap.put("sum", sum.add(avgWgt));
			
			BigDecimal crew = new BigDecimal(0.00);
			if (valueMap.get("crew") == null ){
				crew = new BigDecimal(0.00);
			} else {
				crew = new BigDecimal(valueMap.get("crew").toString());
			}
			valueMap.put("crew", crew.add(reqCrew));
		}
		if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
			getRequest().setAttribute("utilizationMap", utilizationMap);
			getRequest().setAttribute("crewWHAndHubVal", crewWHAndHubVal);
		}
		getRequest().setAttribute("dateMap", dateMap);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	} 
	
	public String dispathMapWorkTicketData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String filterMapData(){	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	public InputStream getDispathMapWorkTicketDataType(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			ByteArrayInputStream output = null;
			try
			{
		if(from.equalsIgnoreCase("main")){
			originMap = true;
			destinMap = true;
			stateMD = true;
			stateVA = true;
			stateDC = true;
			mapStates = "";
			dispathMapList = workTicketManager.getDispathMapList(date1, date2, sessionCorpID, originMap,destinMap,hubId);
		}
		if(from.equalsIgnoreCase("map")){
			if(mapStates.trim().length()>0){
				if (mapStates.indexOf(",") == 0) {
					mapStates = mapStates.substring(1);
				}
			}
			if(mapStates.trim().length()>2){
				if (mapStates.lastIndexOf(",") == mapStates.length() - 1) {
					mapStates = mapStates.substring(0, mapStates.length() - 1);
				}
			}	
			
			dispathMapList = workTicketManager.getDispathMapList(date1, date2, sessionCorpID,originMap,destinMap,hubId);
		}
		int count = 0;
		StringBuilder dispathMapPoints = new StringBuilder(); 
		Iterator it=dispathMapList.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next(); 
			count++; 
			if (count > 1) dispathMapPoints.append(","); else dispathMapPoints.append("");
			dispathMapPoints.append("{\"address\":\"" + obj[0].toString().replaceAll("#", "") + "\", \"bDate\": \"" + obj[1] + "\", " +
				"\"eDate\": \"" + obj[2] + "\", \"eWeight\": \"" + obj[3] + "\", " +
				"\"wService\": \"" + obj[4] + "\" , \"wCustName\": \"" + obj[5] + "\", " +
				"\"wTicket\": \"" + obj[6] + "\", " +
				"\"state\": \"" + obj[7] + "\"," +
				"\"city\": \"" + obj[8] + "\"," +
				"\"zip\": \"" + obj[9] + "\"} "); 
		}
		String prependStr = "{ \"count\":" + count + ", \"dispathMapPoints\":[";
		String appendStr = "]}";
		String s=prependStr + dispathMapPoints.toString() + appendStr;
		output= new ByteArrayInputStream(s.getBytes()); 
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return output;
	}
	public String dispathMapWorkTicket(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		originMap = true;
		destinMap = true;
		opshub=refMasterManager.occupied("OPSHUB", sessionCorpID);
		}
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	


	public String printOpsCalendar(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		if(fromDate == null){
		fromDate = new Date();
		}
		hubID=hubID.replaceAll(",","");
		hubID=hubID.trim();
		workList = workTicketManager.getOpsList(fromDate, hubID, sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		}
	
	
	

	public String opsCalendarList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		hub = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");		
		houseHub = refMasterManager.findByParameterHubWithWarehouse(sessionCorpID, "HOUSE",hub);
		if(fromDate == null){
			fromDate = new Date();
		}
		if(type.equalsIgnoreCase("previous")){
			Calendar calc = Calendar.getInstance();
			calc.setTime(fromDate);
			calc.add(Calendar.DATE, -14);
			fromDate = calc.getTime();
		}else if(type.equalsIgnoreCase("next")){
			Calendar calc = Calendar.getInstance();
			calc.setTime(fromDate);
			calc.add(Calendar.DATE, 14);
			fromDate = calc.getTime();
		}
		else if(type.equalsIgnoreCase("refresh")){ 
		}
		else{
			fromDate = new Date();
		}

		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub);
		distinctHubDescription.putAll(house);
		hubID=hubID.replaceAll(",","");
		hubID=hubID.trim();
		workList = workTicketManager.getOpsList(fromDate, hubID, sessionCorpID);
		from = "list";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String changeHandoutAccess(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicketManager.changeHandoutAccess(ticket, sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
		
	public void updateForHandout(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try
		{
		workTicketManager.updateStoForHandout(handout,ids,hoTicket);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	public String handoutProcessing(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		workTicketServiceList = workTicketManager.findWorkTicketService(ticket,sessionCorpID);
		if(workTicketServiceList!=null &&  !(workTicketServiceList.isEmpty()) && workTicketServiceList.get(0)!=null){
			workTicketServiceId = workTicketServiceList.get(0).toString();
	        String []Str = workTicketServiceId.split("\\~");
	        wtktServiceId = Str[0];
	        serviceCheck = Str[1];
	        warehouse = Str[2];	        
		}else{
			String key = "Ticket "+ticket+" is not valid.";
			saveMessage(getText(key));
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String handOutVolumePieces(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		handOutVolumePieces = workTicketManager.totalHandOutVolumePieces(ticket,ids,sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String reciprocityAnalysisReport(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
     
//	<-- created this method as per Bug 6535 -->
    public String findToolTipService(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceDesc = workTicketManager.findServiceDesc(code, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    
     
    @SkipValidation
    public String workTicketReviewForBillCompleteAjax(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	try
    	{
    	Long startTime = System.currentTimeMillis();
    	workTicketReviewStatus = workTicketManager.findWorkTicketReview(shipnum, sessionCorpID);
    	Long timeTaken = System.currentTimeMillis()-startTime;
    	logger.warn("\n\nTime taken to execute WorkTicket Review list: "+timeTaken+"\n\n");
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}return SUCCESS;
	}
	
    public String getCrewNameContractors(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		crewNameContractor=workTicketManager.getCrewNameByContractor(ticket, sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String addWithCopyWorkTicketList()throws Exception{		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		workTicket = new WorkTicket();
		serviceOrder = serviceOrderManager.get(sid);
		customerFile = serviceOrder.getCustomerFile();
		String id = workTicketManager.findWorkTicketIdList(serviceOrder.getShipNumber(), sessionCorpID);
		addCopyWorkTicket =workTicketManager.get(Long.parseLong(id));
		originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
		destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
		addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
		
		try
		{
		    BeanUtilsBean beanUtilsWorkTicket = BeanUtilsBean.getInstance();
		    beanUtilsWorkTicket.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		    beanUtilsWorkTicket.copyProperties(workTicket, addCopyWorkTicket);
		    
		}catch(Exception e){
			e.printStackTrace();
		}
		if(workTicket.getDate1().before(new Date()) && workTicket.getDate2().before(new Date())){
			workTicket.setDate1(null);
			workTicket.setDate2(null);
		}
		workTicket.setReviewStatus("UnBilled");
		workTicket.setTargetActual("T");
		workTicket.setStatusDate(new Date());			
		workTicket.setCreatedOn(new Date());
		workTicket.setCreatedBy(getRequest().getRemoteUser());
		workTicket.setUpdatedOn(new Date());
		workTicket.setUpdatedBy(getRequest().getRemoteUser());
		workTicket.setOpenDate(new Date());
		workTicket.setCorpID(sessionCorpID);
		workTicket.setId(null);
		workTicket.setTicket(null);
		workTicket.setRevenue(null);
		workTicket.setExtraPayroll(null);
		workTicket.setCollectCOD(null);
		workTicket.setNoCharge(null);
		workTicket.setApprovedBy("");
		workTicket.setApprovedDate(null);
		workTicket.setCrewRevenue(null);
		workTicket.setTicketAssignedStatus("");
		workTicket.setOrderId("");
		workTicket.setOriginAddressBook(addCopyWorkTicket.getOriginAddressBook());
		workTicket.setDestinationAddressBook(addCopyWorkTicket.getDestinationAddressBook());
		if(visibilityForSSCW){
			if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(true)){
				workTicketOriginShuttle="Yes";
			}else if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(false)){
				workTicketOriginShuttle="No";
			}else{
				workTicketOriginShuttle="";
			}
			if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(true)){
				workTicketDestinationShuttle="Yes";
			}else if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(false)){
				workTicketDestinationShuttle="No";
			}else{
				workTicketDestinationShuttle="";
			}
		}
		if(addCopyWorkTicket.getScanned() !=null){
			workTicket.setScanned(null);
		}
		copyWorkTicketFlag="YES";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String addWithCopyWorkTicket()throws Exception{		
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		getComboList(sessionCorpID);		
		wtServiceTypeDyna=refMasterManager.serviceTypeList(sessionCorpID, "TCKTSERVC");
		if (id == null) {
			serviceOrder = serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			workTicket = new WorkTicket();
			serviceOrder = serviceOrderManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			addCopyWorkTicket = (WorkTicket)workTicketManager.get(wid);
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());
			addressBookList = standardAddressesManager.getAddressLine1(serviceOrder.getJob(),sessionCorpID);
			
	         try
				{
	   		    BeanUtilsBean beanUtilsWorkTicket = BeanUtilsBean.getInstance();
	   		    beanUtilsWorkTicket.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
	   		    beanUtilsWorkTicket.copyProperties(workTicket, addCopyWorkTicket);
	   		    
				}catch(Exception e){
					e.printStackTrace();
				}
				workTicket.setOrderId("");
				workTicket.setReviewStatus("UnBilled");
				workTicket.setTargetActual("T");
				workTicket.setStatusDate(new Date());			
				workTicket.setCreatedOn(new Date());
				workTicket.setCreatedBy(getRequest().getRemoteUser());
				workTicket.setUpdatedOn(new Date());
				workTicket.setUpdatedBy(getRequest().getRemoteUser());
				workTicket.setOpenDate(new Date());
				workTicket.setCorpID(sessionCorpID);
				workTicket.setId(null);
				workTicket.setInvoiceNumber("");
				workTicket.setTicket(null);
				workTicket.setRevenue(null);
				workTicket.setExtraPayroll(null);
				workTicket.setCollectCOD(null);
				workTicket.setNoCharge(null);
				workTicket.setApprovedBy("");
				workTicket.setApprovedDate(null);
				workTicket.setCrewRevenue(null);
				workTicket.setTicketAssignedStatus("");
				workTicket.setOriginAddressBook(addCopyWorkTicket.getOriginAddressBook());
				workTicket.setDestinationAddressBook(addCopyWorkTicket.getDestinationAddressBook());
				if(visibilityForSSCW){
					if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(true)){
						workTicketOriginShuttle="Yes";
					}else if(workTicket.getOriginShuttle()!=null && workTicket.getOriginShuttle().equals(false)){
						workTicketOriginShuttle="No";
					}else{
						workTicketOriginShuttle="";
					}
					if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(true)){
						workTicketDestinationShuttle="Yes";
					}else if(workTicket.getDestinationShuttle()!=null && workTicket.getDestinationShuttle().equals(false)){
						workTicketDestinationShuttle="No";
					}else{
						workTicketDestinationShuttle="";
					}
				}
				if(addCopyWorkTicket.getScanned() !=null){
					workTicket.setScanned(null);
				}
				try{
					parentParameterValuesList=refMasterManager.getParentParameterList(sessionCorpID,"TCKTSERVC",addCopyWorkTicket.getService());
				if(parentParameterValuesList!=null && parentParameterValuesList.size()>0 && !parentParameterValuesList.isEmpty()){
					String aa = parentParameterValuesList.toString().replace("[", "").replace("]", "");
					String countparam[]=aa.split(",");
					
					woinstr=new LinkedHashMap<String, String>();
					Iterator iter = tempWoinstrMap.entrySet().iterator();
					while (iter.hasNext()) {
						Map.Entry mapEntry = (Map.Entry) iter.next();
						String keys=(String) mapEntry.getKey();
						String vals=(String)mapEntry.getValue();
						for(int i=0;i<countparam.length;i++){
							if(keys.equals(ltrim(countparam[i].toString())) && !woinstr.containsKey(keys)){
								woinstr.put(keys, vals);
							}
								
						}
				}
				}}
				catch(Exception e){
					e.printStackTrace();
				}
				copyWorkTicketFlag="YES";
				
				
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String findVendorCodeWithName()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		nameWithCode = workTicketManager.findVendorCodeWithName(code,sessionCorpID);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String findToolTipCoordinatorByShipNum(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		coordinatorName = workTicketManager.findCoordinatorByShipNum(shipNumber, sessionCorpID);
		}
       catch(Exception e)
       {e.printStackTrace();}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String driverSchedule(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		return SUCCESS;
	}
	@SkipValidation
	public String driverScheduleList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		driverTicketSchedule = workTicketManager.getdriverTicket(fromDate, toDate,userParentAgent,sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	public String dailySheet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try
		{
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		dailySheetList = workTicketManager.findDailySheetList(wRHouse,dailySheetDate,sessionCorpID);
		availableCrewsDetails = workTicketManager.findAvailableCrews(wRHouse,dailySheetDate,sessionCorpID);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
		public String searchDailySheet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		dailySheetList = workTicketManager.findDailySheetList(wRHouse,dailySheetDate,sessionCorpID);
		crewNumList = workTicketManager.findCrewNoList(wRHouse,dailySheetDate,sessionCorpID);
		availableCrewsDetails = workTicketManager.findAvailableCrews(wRHouse,dailySheetDate,sessionCorpID);
		availableTrucks = workTicketManager.searchAvailableTrucks(wRHouse,dailySheetDate,sessionCorpID);
		omdCrewsNumList = workTicketManager.findOMDCrewNoList(wRHouse,dailySheetDate,sessionCorpID);
		dailySheetNotesValue = workTicketManager.findDailySheetNotes(wRHouse,dailySheetDate,sessionCorpID);
		
		if(dailySheetDate!=null){
    		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(dailySheetDate));
			dailySheetDateDisplay = nowYYYYMMDD2.toString();
    	}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
 @SkipValidation
	    public String findOriginAddWorkTicket(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	try
	    	{
	    	wTAddList = workTicketManager.findOriginAddWorkTicket(oId, sessionCorpID);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	 
	 	 @SkipValidation
	    public String updateWTOriAddress(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
            try
            {
	    	WorkTicket workTicketTemp = null;
	    	workTicketTemp = workTicketManager.get(wOId);
	    	workTicketTemp.setAddress1(wOAddress1);
	    	workTicketTemp.setAddress2(wOAddress2);
	    	workTicketTemp.setAddress3(wOAddress3);
	    	workTicketTemp.setOriginCountry(wOriginCountry);
	    	workTicketTemp.setState(wOState);
	    	workTicketTemp.setCity(wOCity);
	    	workTicketTemp.setZip(wOZip);
	    	workTicketTemp.setDestinationAddress1(wDAddress1);
	    	workTicketTemp.setDestinationAddress2(wDAddress2);
	    	workTicketTemp.setDestinationAddress3(wDAddress3);
	    	workTicketTemp.setDestinationCountry(wDestinCountry);
	    	workTicketTemp.setDestinationState(wDState);
	    	workTicketTemp.setDestinationCity(wDCity);
	    	workTicketTemp.setDestinationZip(wDZip);
	    	workTicketTemp.setActualWeight(wActWt);
	    	workTicketTemp.setUpdatedOn(new Date());
	    	workTicketTemp.setUpdatedBy(getRequest().getRemoteUser());
	    	workTicketTemp = workTicketManager.save(workTicketTemp);
	    	searchDailySheet();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}	
	 @SkipValidation
	    public String saveDailySheetNote(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	try
	    	{
	    	workTicketManager.updateDailySheetNote(wRHouse,dailySheetDate,sessionCorpID,wDailySheetNotes);
	    	searchDailySheet();
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	 	@SkipValidation
		public String findAssignedCrewsForEditingAjax(){
	 		try
	 		{
			middleList = workTicketManager.findAssignedCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);	
	 		}
	 		catch(Exception e)
	 		{
              e.printStackTrace();	 			
	 		}
			return SUCCESS;
		}
	
		@SkipValidation
		public String findAvailableCrewAjax(){
			try
			{
			leftList = workTicketManager.findAvailableCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return SUCCESS;
		}
		
		@SkipValidation
		public String findAssignedTruckForEditingAjax(){
			rightListTruck = workTicketManager.findAssignedTruckForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);	
			return SUCCESS;
		}
	
		@SkipValidation
		public String findAvailableTruckAjax(){
			leftListTruck = workTicketManager.findAvailableTruckForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);
			return SUCCESS;
		}
		
		
		@SkipValidation
		public String saveCrewDetails(){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
			try
			{
			String[] crew = getRequest().getParameterValues("assignedCrews");
			String[] abscrew = getRequest().getParameterValues("absentCrews");
		    String[] truckNo = null;
		    if(getRequest().getParameterValues("assignedTrucks")!=null){
		    	truckNo=getRequest().getParameterValues("assignedTrucks");
		    }else{
		    	truckNo = new String[1];
		    	truckNo[0]="";
		    }
			middleList = workTicketManager.findAssignedCrewsForEditing(dSWHouse,dailySheetDate,sessionCorpID,dSTicket.toString());
			if((middleList==null || middleList.isEmpty()) && (crew==null || crew.equals("")) && (abscrew!=null && !abscrew.equals("")) && (truckNo!=null && !truckNo.equals(""))){
				String key = "Kindly assign a crew for the ticket "+dSTicket.toString()+" to continue.";
				errorMessage(getText(key));				
				searchDailySheet();
			    return SUCCESS;
			}
			
				if(abscrew!=null && !abscrew.equals("")){
				for (String absCrew1:abscrew){
					String crewVal = workTicketManager.findAbsentCrew(absCrew1, dailySheetDate, sessionCorpID);	
					if(crewVal!=null && !crewVal.equalsIgnoreCase("")){
					String key = ""+crewVal+" has already been assigned for one or more ticket so can not mark as absent.";
					errorMessage(getText(key));				
					searchDailySheet();
				    return SUCCESS;
				}
				}
			}
			absentCrewsEdit = workTicketManager.findAbsentCrewsForEditing(dSWHouse,dailySheetDate,sessionCorpID);
			Iterator itra = absentCrewsEdit.iterator();
		    while(itra.hasNext()){
		    	String AbsentCrewValue = (String) itra.next();    			    
		    	Long AbCrewId = workTicketManager.findUserDetailsFromTS(AbsentCrewValue,dailySheetDate,sessionCorpID,"0");
		    	if(AbCrewId!=null && !AbCrewId.equals("")){	
		    		try{
		    			timeSheetManager.remove(AbCrewId);
		    		}catch(Exception ex){
  					ex.printStackTrace();
  				}
		    	}
		    } 
		    String[] abscrew1 = getRequest().getParameterValues("absentCrews");
			if(abscrew1!=null && !abscrew1.equals("")){
			for (String absCrew1:abscrew1){ 
				Long crewId = workTicketManager.findUserDetailsFromCrew(absCrew1,sessionCorpID);
				payroll = payrollManager.get(crewId);
				timeSheet = new TimeSheet();
				timeSheet.setTicket(Long.parseLong("0"));
				timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
				timeSheet.setUserName(payroll.getUserName());
				timeSheet.setWarehouse(payroll.getWarehouse());
				timeSheet.setCorpID(sessionCorpID);
				timeSheet.setCrewType(payroll.getTypeofWork());
				timeSheet.setWorkDate(dailySheetDate);
				timeSheet.setShipper("");
				timeSheet.setLunch("No lunch");
				timeSheet.setAction("Y");
				if(payroll.getPayhour() == null){
					timeSheet.setRegularPayPerHour("0");
				}else{
				timeSheet.setRegularPayPerHour(payroll.getPayhour());
				}
				List<SystemDefault> sysDefault = timeSheetManager.findSystemDefault(sessionCorpID);
				for(SystemDefault systemDefault : sysDefault){
					timeSheet.setBeginHours(systemDefault.getDayAt1());
					timeSheet.setEndHours(systemDefault.getDayAt2());
				}
				String beginAbsentHrs = timeSheet.getBeginHours();
				String endAbsentHrs = timeSheet.getEndHours();
				Long totalAbsentTimeMnts =  Long.parseLong(endAbsentHrs.substring(0, endAbsentHrs.indexOf(":")))*60 + Long.parseLong(endAbsentHrs.substring(endAbsentHrs.indexOf(":")+1, endAbsentHrs.length())) - Long.parseLong(beginAbsentHrs.substring(0, beginAbsentHrs.indexOf(":")))*60 + Long.parseLong(beginAbsentHrs.substring(beginAbsentHrs.indexOf(":")+1, beginAbsentHrs.length())) ;
				String absentHr = String.valueOf(Double.parseDouble(String.valueOf(totalAbsentTimeMnts))/60);

				timeSheet.setRegularHours(new BigDecimal(absentHr));
				timeSheet.setUpdatedOn(new Date());
				timeSheet.setCreatedOn(new Date());
				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
				timeSheet.setCreatedBy(getRequest().getRemoteUser());
	 	    	timeSheetManager.save(timeSheet);
				}
			}
					
					    			      			
    			Iterator itrm = middleList.iterator();
    			    while(itrm.hasNext()){
    			    	String crewValue = (String) itrm.next();    			    
    			Long crewId = workTicketManager.findUserDetailsFromTS(crewValue,dailySheetDate,sessionCorpID,dSTicket.toString());
    			if(crewId!=null && !crewId.equals("")){
    				try{
    					timeSheetManager.remove(crewId);
    					timeSheetManager.updateWorkTicketCrew(dSTicket,getRequest().getRemoteUser(),sessionCorpID);
    				}catch(Exception ex){
    					ex.printStackTrace();
    				}				
    			}
    		}
    			
    			
    			rightListTruck = workTicketManager.findAssignedTruckForEditing(dSWHouse,dailySheetDate,sessionCorpID,dSTicket.toString());
    			Iterator itr = rightListTruck.iterator();
			    while(itr.hasNext()){
			    	String truckValue = (String) itr.next();    			    
				List truckId = workTicketManager.findTruckDetailsFromTO(truckValue,dailySheetDate,sessionCorpID,dSTicket.toString());
				Iterator ittId = truckId.iterator();
			    while(ittId.hasNext()){
			    	Long truckIdVal = (Long) ittId.next();			    
				if(truckIdVal!=null && !truckIdVal.equals("")){
					try{
						truckingOperationsManager.remove(truckIdVal);
					}catch(Exception ex){
						ex.printStackTrace();
						}				
					}
			    }
			  }
			    scId = workTicketManager.findScheduleResourceId(dSTicket.toString(),dSWHouse,dailySheetDate,sessionCorpID);
				Iterator itsc = scId.iterator();
			    while(itsc.hasNext()){
			    	Long scIdValue = (Long) itsc.next();
			    	if(scIdValue!=null && !scIdValue.equals("")){
						try{
							scheduleResourceOperationManager.remove(scIdValue);
						}catch(Exception ex){
							ex.printStackTrace();
							}				
						}
			    }			 
			
			    
			    Long wId = workTicketManager.findWorkTicketIdByTicket(dSTicket.toString(),sessionCorpID);
				workTicket = workTicketManager.get(wId);
				
				List<Billing> lst = timeSheetManager.getBilling(workTicket.getShipNumber());
				if(lst == null || lst.isEmpty() || lst.get(0) == null){
					billing = new Billing();
				}else{
					billing = (Billing) lst.get(0);
				}	
				List<PayrollAllocation> plst = timeSheetManager.findDistAndCalcCode(dSJob, dSService, sessionCorpID,workTicket.getJobMode(),workTicket.getCompanyDivision());
				if(plst == null || plst.isEmpty() || plst.get(0) == null){
					payrollAllocation = new PayrollAllocation();
				}else{
					payrollAllocation = (PayrollAllocation) plst.get(0);
				}
				
				if(crew!=null && !crew.equals("")){
	    			for (String asgCrew:crew){    				
	    				Long crewId = workTicketManager.findUserDetailsFromCrew(asgCrew,sessionCorpID);
	    				payroll = payrollManager.get(crewId);
	    				List dateForBeginHours=timeSheetManager.getTimeFromEndHours(workTicket.getTicket(), payroll.getLastName()+", "+payroll.getFirstName(), dailySheetDate);
	    				timeSheet = new TimeSheet();
	    				timeSheet.setContract(billing.getContract());
	    				timeSheet.setTicket(workTicket.getTicket());
	    				timeSheet.setWarehouse(payroll.getWarehouse());
	    				timeSheet.setDistributionCode(payrollAllocation.getCode());
	    				timeSheet.setCalculationCode(payrollAllocation.getCalculation());
	    				timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
	    				timeSheet.setShipper(workTicket.getShipNumber());
	    				timeSheet.setUserName(payroll.getUserName());
	    				timeSheet.setCrewType(payroll.getTypeofWork());
	    				timeSheet.setLunch("No lunch");
	    				timeSheet.setCreatedOn(new Date());
	    				timeSheet.setDoneForTheDay("false");
	    				timeSheet.setCreatedBy(getRequest().getRemoteUser());
	    				if((!dateForBeginHours.isEmpty()) && (dateForBeginHours.get(0)!=null)){
	    					timeSheet.setBeginHours(dateForBeginHours.get(0).toString());
	    				}
	    				
	    				if(payroll.getPayhour() == null || payroll.getPayhour().equalsIgnoreCase("")){
	    					timeSheet.setRegularPayPerHour("0");
	    					timeSheet.setOverTimePayPerHour("0");
	    					timeSheet.setDoubleOverTimePayPerHour("0");
	    					
	    				}else{
	    				timeSheet.setRegularPayPerHour(payroll.getPayhour());
	    				timeSheet.setOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*1.5)));
	    				timeSheet.setDoubleOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*2)));
	    				}
	    				
	    				if(!(timeSheet.getPaidRevenueShare() == null) && timeSheet.getPaidRevenueShare().equals("")){
	    					timeSheet.setPaidRevenueShare(new BigDecimal(0));
	    				}
	    				timeSheet.setAction("C");
	    				timeSheet.setCorpID(sessionCorpID);
	    				timeSheet.setWorkDate(dailySheetDate);
	    				timeSheet.setUpdatedOn(new Date());
	    				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
	    				timeSheet.setIsCrewUsed(true);
	    	 	    	timeSheet=timeSheetManager.save(timeSheet);
	    	 	    	timeSheetManager.updateWorkTicketCrew(workTicket.getTicket(),getRequest().getRemoteUser(),sessionCorpID);
	    	 	    	
	    			for(String truckNum:truckNo){
	    				if(!"".equals(truckNum)){
	 	    				Long truckId = workTicketManager.findTruckDetailsFromTruck(truckNum,sessionCorpID);
	 	    				truck=truckManager.get(truckId);
							truckingOperations=new TruckingOperations();
							truckingOperations.setCorpID(sessionCorpID);
							truckingOperations.setDescription(truck.getDescription());
							truckingOperations.setLocalTruckNumber(truck.getLocalNumber());
							truckingOperations.setTicket(workTicket.getTicket());
							truckingOperations.setCreatedOn(new Date());
							truckingOperations.setUpdatedOn(new Date());
							truckingOperations.setCreatedBy(getRequest().getRemoteUser());
							truckingOperations.setUpdatedBy(getRequest().getRemoteUser());
							truckingOperations.setIsTruckUsed(true);
							truckingOperations.setWorkDate(dailySheetDate);
							truckingOperations=truckingOperationsManager.save(truckingOperations);
							truckingOperationsManager.updateWorkTicketTrucks(workTicket.getTicket(),0,"",false,"",sessionCorpID);
							
							ScheduleResourceOperation scheduleResourceOperation=new ScheduleResourceOperation();
							 scheduleResourceOperation.setCorpID(sessionCorpID);
							 scheduleResourceOperation.setCrewName(payroll.getLastName()+", " +payroll.getFirstName());
							 scheduleResourceOperation.setWorkDate(dailySheetDate);
							 scheduleResourceOperation.setTicket(String.valueOf(dSTicket));
							 scheduleResourceOperation.setWorkTicketId(workTicket.getId());
							 scheduleResourceOperation.setShipNumber(workTicket.getShipNumber());
							 scheduleResourceOperation.setOriginCity(workTicket.getCity());
							 scheduleResourceOperation.setDestinationCity(workTicket.getDestinationCity());
							 scheduleResourceOperation.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
							 scheduleResourceOperation.setBeginDate(workTicket.getDate1());
							 if(workTicket.getEstimatedCubicFeet()!=null && (!(workTicket.getEstimatedCubicFeet().equals("")))){
								 scheduleResourceOperation.setEstimatedCubicFeet(workTicket.getEstimatedCubicFeet().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedCubicFeet("0");
							 }
							 if(workTicket.getEstimatedWeight()!=null && (!(workTicket.getEstimatedWeight().equals("")))){
								 scheduleResourceOperation.setEstimatedWeight(workTicket.getEstimatedWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedWeight("0");
							 }
							 if(workTicket.getActualVolume()!=null && (!(workTicket.getActualVolume().equals("")))){
								 scheduleResourceOperation.setActualVolume(workTicket.getActualVolume().toString());	 
							 }else{
								 scheduleResourceOperation.setActualVolume("0");
							 }
							 if(workTicket.getActualWeight()!=null && (!(workTicket.getActualWeight().equals("")))){
								 scheduleResourceOperation.setActualWeight(workTicket.getActualWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setActualWeight("0");
							 }
							 scheduleResourceOperation.setService(workTicket.getService());
							 scheduleResourceOperation.setWarehouse(workTicket.getWarehouse());
							 scheduleResourceOperation.setUnit1(workTicket.getUnit1());
							 scheduleResourceOperation.setUnit2(workTicket.getUnit2());					
							 scheduleResourceOperation.setTimesheetId(timeSheet.getId().toString());
							 scheduleResourceOperation.setCrewType(timeSheet.getCrewType());					 
							 scheduleResourceOperation.setDescription(truck.getDescription());
							 scheduleResourceOperation.setLocaltruckNumber(truck.getLocalNumber());
							 scheduleResourceOperation.setTruckingOpsId(truckingOperations.getId().toString());
							 scheduleResourceOperationManager.save(scheduleResourceOperation); 
	 	    			}else{
								 truckingOperationsManager.updateWorkTicketTrucks(workTicket.getTicket(),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"Assign",sessionCorpID);
				 	    	     ScheduleResourceOperation scheduleResourceOperation=new ScheduleResourceOperation();
								 scheduleResourceOperation.setCorpID(sessionCorpID);
								 scheduleResourceOperation.setCrewName(payroll.getLastName()+", " +payroll.getFirstName());
								 scheduleResourceOperation.setWorkDate(dailySheetDate);
								 scheduleResourceOperation.setTicket(String.valueOf(workTicket.getTicket()));
								 scheduleResourceOperation.setWorkTicketId(workTicket.getId());
								 scheduleResourceOperation.setShipNumber(workTicket.getShipNumber());
								 scheduleResourceOperation.setOriginCity(workTicket.getCity());
								 scheduleResourceOperation.setDestinationCity(workTicket.getDestinationCity());
								 scheduleResourceOperation.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
								 scheduleResourceOperation.setBeginDate(workTicket.getDate1());
								 if(workTicket.getEstimatedCubicFeet()!=null && (!(workTicket.getEstimatedCubicFeet().equals("")))){
									 scheduleResourceOperation.setEstimatedCubicFeet(workTicket.getEstimatedCubicFeet().toString());	 
								 }else{
									 scheduleResourceOperation.setEstimatedCubicFeet("0");
								 }
								 if(workTicket.getEstimatedWeight()!=null && (!(workTicket.getEstimatedWeight().equals("")))){
									 scheduleResourceOperation.setEstimatedWeight(workTicket.getEstimatedWeight().toString());	 
								 }else{
									 scheduleResourceOperation.setEstimatedWeight("0");
								 }
								 if(workTicket.getActualVolume()!=null && (!(workTicket.getActualVolume().equals("")))){
									 scheduleResourceOperation.setActualVolume(workTicket.getActualVolume().toString());	 
								 }else{
									 scheduleResourceOperation.setActualVolume("0");
								 }
								 if(workTicket.getActualWeight()!=null && (!(workTicket.getActualWeight().equals("")))){
									 scheduleResourceOperation.setActualWeight(workTicket.getActualWeight().toString());	 
								 }else{
									 scheduleResourceOperation.setActualWeight("0");
								 }
								 scheduleResourceOperation.setService(workTicket.getService());
								 scheduleResourceOperation.setWarehouse(workTicket.getWarehouse());
								 scheduleResourceOperation.setUnit1(workTicket.getUnit1());
								 scheduleResourceOperation.setUnit2(workTicket.getUnit2());
								 scheduleResourceOperation.setDescription("");
								 scheduleResourceOperation.setLocaltruckNumber("");
								 scheduleResourceOperation.setTimesheetId(timeSheet.getId().toString());
								 scheduleResourceOperation.setCrewType(timeSheet.getCrewType());
								 scheduleResourceOperation.setTruckingOpsId("");
								 scheduleResourceOperationManager.save(scheduleResourceOperation); 
	 	    				}
				    	}
	    			}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				searchDailySheet();
			return SUCCESS;			
		}
	
		
		@SkipValidation
		public String dailySheetDispatch(){
			try
			{
			house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
			}
			catch(Exception e)
			{e.printStackTrace();}
			return SUCCESS;
		}
		
		@SkipValidation
		public String dailySheetDispatchSearch(){			
			try
			{
			SortedMap <Integer,String> dataMap = new TreeMap <Integer,String>();
			for(int i=0; i<10; i++){
				Calendar calc = Calendar.getInstance();
				calc.setTime(dailySheetCalenderDate);
				calc.add(Calendar.DATE, i);
				Date totalTempDate = calc.getTime();								
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("EEEE, MMM d, yyyy");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(totalTempDate));
				String date = nowYYYYMMDD1.toString();					
				dataMap.put(i,date);				
			}
			dailySheetCalenderList1 = workTicketManager.getDSDispatch(dailySheetwRHouse, dailySheetCalenderDate, sessionCorpID);
			getRequest().setAttribute("dailySheetCalenderList1", dailySheetCalenderList1);
			getRequest().setAttribute("dataMap", dataMap);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return SUCCESS;
		}
		
		@SkipValidation
		public String removeTimeRequirement(){
			try
			{
		workTicketTimeManagementManager.remove(Long.parseLong(timeRequirementId));
		timeRequirementList=workTicketTimeManagementManager.getTimeManagementManagerDetails(Long.parseLong(workTicketId),Long.parseLong(ticketNo),sessionCorpID);
			}
         catch(Exception e)
            {
	           e.printStackTrace();}
			return SUCCESS;
		}
		@SkipValidation
		public String timeRequirementDetails(){
		try
		{
			if(workTicketId!=null && !workTicketId.equals("")){
				timeRequirementList=workTicketTimeManagementManager.getTimeManagementManagerDetails(Long.parseLong(workTicketId),Long.parseLong(ticketNo),sessionCorpID);	
			}else{
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return SUCCESS;
		}
		
		@SkipValidation
		public String crewCalenderView(){
			try
			{
			house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
			
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("dd-MMM-yy");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(new Date()));
			crewCalTempDate = nowYYYYMMDD2.toString();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return SUCCESS;
		}
		
		@SkipValidation
		public String crewCalenderList(){
			try
			{
			crewCalanderList = workTicketManager.findCrewCalendarList(crewCalendarwRHouse, crewCalendarDate, sessionCorpID);			
			getRequest().setAttribute("crewCalanderList", crewCalanderList);	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return SUCCESS;
		}
		
		public List<JasperPrint> getParameter(PrintDailyPackage printDailyPackage,PrintDailyPackage printChild,String flag,JasperReport reportTemplate,List<JasperPrint> jasperPrints,Object obj,Map parameters,JRParameter[] allParameters){
			
			String ticket = "";
			String ship = "";
			try
			{
			if("WorkTicket".equals(flag)){
				WorkTicket wt = (WorkTicket)obj;
				ticket = wt.getTicket().toString();
				ship = wt.getShipNumber();
			}else if("ServiceOrder".equals(flag)){
				ServiceOrder so = (ServiceOrder)obj;
				ship = so.getShipNumber();
			}
			for (JRParameter parameter : allParameters) {
				if (!parameter.isSystemDefined()) {
					String paramName = parameter.getName();
					
					if(paramName.equalsIgnoreCase("User Name")){
						parameters.put(parameter.getName(), getRequest().getRemoteUser());
					}else if(paramName.equalsIgnoreCase("Service Order Number")){
						parameters.put(parameter.getName(), ship);
					}else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						parameters.put(parameter.getName(), ticket);
					}else if(paramName.equalsIgnoreCase("Corporate ID")){
						parameters.put(parameter.getName(), sessionCorpID);
					}else if(paramName.equalsIgnoreCase("enddate")){
						parameters.put(parameter.getName(), new Date());
					}else if(paramName.equalsIgnoreCase("begindate")){
						parameters.put(parameter.getName(), new Date());
					}else{
						parameters.put(parameter.getName(), "");
					}
				}
			}
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
						
			JasperPrint jasperPrint = new JasperPrint();
			JasperPrint jasperPrint2 = new JasperPrint();
			
			Set<String> jrxmlSet = new HashSet<String>();
			try {
				jasperPrint = reportsManager.generateReport(reportTemplate, parameters);
				boolean isflag = true;
				for (int i = 1; i <= printDailyPackage.getNoOfCopies(); i++) {
					if(jrxmlSet.contains(printChild.getJrxmlName().trim()) && printDailyPackage.getNoOfCopies() > 1 &&
							jasperPrint != null && jasperPrint.getPages().size() > 1){
						if(i > 1){
							if(isflag){
								jasperPrint2 = reportsManager.generateReport(reportTemplate, parameters);
								jasperPrint2.getPages().remove(jasperPrint2.getPages().size()-1);
								isflag = false;
							}
							jasperPrints.add(jasperPrint2);
						}else{
							jasperPrints.add(jasperPrint);
						}
					}else{
						jasperPrints.add(jasperPrint);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return jasperPrints;
		}
		
		@SkipValidation
		public String printReports() throws Exception {
		 try {
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			pathSeparator = System.getProperty("file.separator");
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
			HttpServletResponse response = getResponse();
			ServletOutputStream ouputStream = response.getOutputStream();
			JasperReport reportTemplate = null;
			path = ServletActionContext.getServletContext().getRealPath("/jasper");
			Set<String> SOList = new HashSet<String>();
			List<PrintDailyPackage> printList = printDailyPackageManager.getByCorpId(sessionCorpID,"");
			if((printFlag!=null && !printFlag.equalsIgnoreCase("")) && printFlag.equalsIgnoreCase("workTicketList")){
				workTicket = workTicketManager.get(ticket);
			}else{
				workTicket = workTicketManager.get(wid);
			}
			miscellaneous = miscellaneousManager.get(workTicket.getServiceOrderId());
				for (PrintDailyPackage printDailyPackage : printList) {
					List<PrintDailyPackage> printChildList = printDailyPackageManager.findChildByParentId(printDailyPackage.getId());
					for (PrintDailyPackage printChild : printChildList) {
						if (printChild.getJrxmlName() != null && printChild.getJrxmlName().trim().length() > 0){
							if((printDailyPackage.getOnePerSOperDay() != null && "Y".equals(printDailyPackage.getOnePerSOperDay().trim())) 
									&& SOList.contains(workTicket.getShipNumber().trim()+printChild.getJrxmlName().trim())){
								break;
							}
							Map parameters = new HashMap();
							boolean flag = false;
							Reports report = reportsManager.getReportTemplateForWT(printChild.getJrxmlName(), printDailyPackage.getModule(), sessionCorpID);
							if(report != null){									
								reportTemplate = JasperCompileManager.compileReport(path+pathSeparator+sessionCorpID+pathSeparator+ report.getReportName());
								JRParameter[] allParameters = reportTemplate.getParameters();		
								
								if(!"All".equals(printDailyPackage.getWtServiceType().trim())){
									String serviceArr[] = printDailyPackage.getWtServiceType().split(",");
									
									for (String service : serviceArr) {
										serviceArrList.add(service.trim());
									}
									String wtService =workTicket.getService();
										if(serviceArrList.contains(wtService)){
												serviceflag = true;
											}else{
												serviceflag = false;
											}
										serviceArrList = new ArrayList();
									if(printDailyPackage.getJob()!=null && !printDailyPackage.getJob().equalsIgnoreCase("")){
										String jobArr[] = printDailyPackage.getJob().split(",");
										for (String job : jobArr) {
											jobArrList.add(job.trim());
										}
										String wtJob = workTicket.getJobType();
											if(jobArrList.contains(wtJob)){
												jobFlag = true;
											}else{
												jobFlag = false;
											}										
										}else{
											jobFlag = true;
										}
									jobArrList= new ArrayList();
									if(printDailyPackage.getMode() != null && !"".equals(printDailyPackage.getMode())){
										String wtMode = workTicket.getJobMode();
										if(printDailyPackage.getMode().equals(wtMode)){
											modeFlag = true;
										}else{
											modeFlag = false;
										}												
										}else{
											modeFlag = true;
										}
									if(printDailyPackage.getMilitary()!=null && !printDailyPackage.getMilitary().equalsIgnoreCase("")){
										String misMilitaryShip = miscellaneous.getMillitaryShipment();
										if(printDailyPackage.getMilitary().equalsIgnoreCase(misMilitaryShip)){
											militaryFlag = true;
										}else{
											militaryFlag = false;
										}
										}else{
											militaryFlag = true;
										}
									if(serviceflag && jobFlag && modeFlag && militaryFlag){
										flag =true;
									}
								}else if("All".equals(printDailyPackage.getWtServiceType().trim())){
									if(printDailyPackage.getJob()!=null && !printDailyPackage.getJob().equalsIgnoreCase("")){
										String jobArr[] = printDailyPackage.getJob().split(",");
										for (String job : jobArr) {
											jobArrList.add(job.trim());
										}
										String wtJob = workTicket.getJobType();
											if(jobArrList.contains(wtJob)){
												jobFlag = true;
											}else{
												jobFlag = false;
											}										
										}else{
											jobFlag = true;
										}
									jobArrList= new ArrayList();
									if(printDailyPackage.getMode() != null && !"".equals(printDailyPackage.getMode())){
										String wtMode = workTicket.getJobMode();
										if(printDailyPackage.getMode().equals(wtMode)){
											modeFlag = true;
										}else{
											modeFlag = false;
										}												
										}else{
											modeFlag = true;
										}
									if(printDailyPackage.getMilitary()!=null && !printDailyPackage.getMilitary().equalsIgnoreCase("")){
										String misMilitaryShip = miscellaneous.getMillitaryShipment();
										if(printDailyPackage.getMilitary().equalsIgnoreCase(misMilitaryShip)){
											militaryFlag = true;
										}else{
											militaryFlag = false;
										}
										}else{
											militaryFlag = true;
										}
									if(jobFlag && modeFlag && militaryFlag){
										flag =true;
									}
								}
								
								if(flag){
									try {
										jasperPrints = getParameter(printDailyPackage,printChild,"WorkTicket",reportTemplate,jasperPrints,workTicket,parameters,allParameters);
										if(printDailyPackage.getOnePerSOperDay() != null && "Y".equals(printDailyPackage.getOnePerSOperDay().trim())){
											SOList.add(workTicket.getShipNumber()+printChild.getJrxmlName());
										}
									} catch (Exception e) {
										e.printStackTrace();									
								}
								}
							}
						}
					}
				}				
			
			if (jasperPrints != null && jasperPrints.size() > 0) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
				String dateFrom = nowYYYYMMDD1.toString();
				String ticketNo = workTicket.getTicket().toString();
				String fileName = "Print_Package-"+ticketNo+"-"+dateFrom;
				
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
				
				JRExporter exporterPDF = new JRPdfExporter();
				exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, true);
				exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrints);
				exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterPDF.exportReport();
				
				ouputStream.write(byteArrayOutputStream.toByteArray());
				ouputStream.close();
			}else{
				createPDF(response,workTicket.getTicket());
			}
			
		 } catch (Exception e) {
				e.printStackTrace();									
		}
			return null;
	}
		public void createPDF(HttpServletResponse response,Long ticket){
			Document document = new Document();
		    try{
		    	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
				String dateFrom = nowYYYYMMDD1.toString();
				String fileName = "Print_Package-No-Record-Found";
		    	
		        response.setContentType("application/pdf");
		        response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
		        PdfWriter.getInstance(document, response.getOutputStream());
		        document.open();
		        document.add(new Paragraph("Ticket: "+ticket));
		        document.add(new Paragraph("Date: "+dateFrom));
		        document.add(new Paragraph("There is nothing to print for this ticket."));
		    }catch(Exception e){
		        e.printStackTrace();
		    }
		    document.close();
		}
		
		public void insertIntoCoraxLog(WorkTicket wckt,String actionName){
			coraxLog=new CoraxLog();
			coraxLog.setWorkTicketId(wckt.getId());
			coraxLog.setCorpID(sessionCorpID);
			coraxLog.setCreatedOn(new Date());
			coraxLog.setUpdatedOn(new Date());
			coraxLog.setCreatedBy(getRequest().getRemoteUser());
			coraxLog.setUpdatedBy(getRequest().getRemoteUser());
			coraxLog.setActionName(actionName);
			coraxLog.setActionStatus("ReadyToSend");
			coraxLog.setTypeOfFlow("RedSkyToCorax");
			coraxLog.setTicketNo(wckt.getTicket());
			coraxLog.setShipNumber(wckt.getShipNumber());
			coraxLog = coraxLogManager.save(coraxLog);
		}
		
		
		public String getStandardAddressDetail(){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			standardAddresses = standardAddressesManager.getStandardAddressDetail(residenceName);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
		
	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public void setBookStorageManager(BookStorageManager bookStorageManager) {
		this.bookStorageManager = bookStorageManager;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setItemsJEquipManager(ItemsJEquipManager itemsJEquipManager) {
		this.itemsJEquipManager = itemsJEquipManager;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}

	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Set getWorkTickets() {
		return workTickets;
	}

	public Set getServiceOrders() {
		return serviceOrders;
	}

	public String getShipNumberForCity() {
		return shipNumberForCity;
	}

	public String getOcityForCity() {
		return ocityForCity;
	}

	public void setShipNumberForCity(String shipNumberForCity) {
		this.shipNumberForCity = shipNumberForCity;
	}

	public void setOcityForCity(String ocityForCity) {
		this.ocityForCity = ocityForCity;
	}

	public List getStorages() {
		return storages;
	}

	public List getBookStorages() {
		return bookStorages;
	}

	public Set getLocations() {
		return locations;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public Set getSecondStorages() {
		return secondStorages;
	}

	public ItemsJEquip getItemsJEquip() {
		return itemsJEquip;
	}

	public List getServicePartners() {
		return servicePartners;
	}

	public void setItemsJEquip(ItemsJEquip itemsJEquip) {
		this.itemsJEquip = itemsJEquip;
	}

	public Map<String, String> getMeasure() {
		return measure;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public Map<String, String> getOnhandstorage() {
		return onhandstorage;
	}

	public Map<String, String> getLead() {
		return lead;
	}

	public Map<String, String> getOrganiza() {
		return organiza;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public Map<String, String> getPeak() {
		return peak;
	}

	public Map<String, String> getSchedule() {
		return schedule;
	}

	public Map<String, String> getState() {
		return state;
	}

	public Map<String, String> getSurvey() {
		return survey;
	}

	public Map<String, String> getMode() {
		return mode;
	}

	public Map<String, String> getService() {
		return service;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public Map<String, String> getDept() {
		return dept;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public Map<String, String> getConfirm() {
		return confirm;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public Map<String, String> getWoinstr() {
		return woinstr;
	}

	public Map<String, String> getQc() {
		return qc;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public void setWcontract(String wcontract) {
		this.wcontract = wcontract;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public ServiceOrder getServiceOrder() {
		return( serviceOrder!=null )?serviceOrder:serviceOrderManager.get(soid);
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	public List getItemsJbkEquips() {
		return itemsJbkEquips;
	}

	public List getItemsJEquips() {
		return itemsJEquips;
	}

	public String getCountTicketBillingNotes() {
		return countTicketBillingNotes;
	}

	public void setCountTicketBillingNotes(String countTicketBillingNotes) {
		this.countTicketBillingNotes = countTicketBillingNotes;
	}

	public String getCountTicketDestinationNotes() {
		return countTicketDestinationNotes;
	}

	public void setCountTicketDestinationNotes(String countTicketDestinationNotes) {
		this.countTicketDestinationNotes = countTicketDestinationNotes;
	}

	public String getCountTicketOriginNotes() {
		return countTicketOriginNotes;
	}

	public void setCountTicketOriginNotes(String countTicketOriginNotes) {
		this.countTicketOriginNotes = countTicketOriginNotes;
	}

	public String getCountTicketSchedulingNotes() {
		return countTicketSchedulingNotes;
	}

	public void setCountTicketSchedulingNotes(String countTicketSchedulingNotes) {
		this.countTicketSchedulingNotes = countTicketSchedulingNotes;
	}

	public String getCountTicketStaffNotes() {
		return countTicketStaffNotes;
	}

	public void setCountTicketStaffNotes(String countTicketStaffNotes) {
		this.countTicketStaffNotes = countTicketStaffNotes;
	}

	public String getCountForTicketNotes() {
		return countForTicketNotes;
	}

	public void setCountForTicketNotes(String countForTicketNotes) {
		this.countForTicketNotes = countForTicketNotes;
	}

	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soid);
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public Payroll getPayroll() {
		return payroll;
	}

	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}

	public Set<Payroll> getPayrolls() {
		return payrolls;
	}

	public void setPayrolls(Set<Payroll> payrolls) {
		this.payrolls = payrolls;
	}

	public List getContainerList() {
		return containerList;
	}

	public void setContainerList(List containerList) {
		this.containerList = containerList;
	}

	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public Map<String, String> getDstates() {
		return dstates;
	}

	public Map<String, String> getOstates() {
		return ostates;
	}

	public String getMaterialDescription() {
		return materialDescription;
	}

	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getOccupied() {
		return occupied;
	}

	public void setOccupied(String occupied) {
		this.occupied = occupied;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getItemType() {
		return itemType;
	}

	public List<MaterialDto> getItemsJEquipss() {
		return itemsJEquipss;
	}

	public void setItemsJEquipss(List<MaterialDto> itemsJEquipss) {
		this.itemsJEquipss = itemsJEquipss;
	}

	public List getMaterialsList() {
		return materialsList;
	}

	public void setMaterialsList(List list) {
		this.materialsList = list;
	}

	public Billing getBilling() {
		return( billing!=null )?billing:billingManager.get(soid); 
	}
	
	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String getCountTicketStorageNotes() {
		return countTicketStorageNotes;
	}

	public void setCountTicketStorageNotes(String countTicketStorageNotes) {
		this.countTicketStorageNotes = countTicketStorageNotes;
	}

	public ExtendedPaginatedList getWorkTicketsExt() {
		return workTicketsExt;
	}

	public void setWorkTicketsExt(ExtendedPaginatedList workTicketsExt) {
		this.workTicketsExt = workTicketsExt;
	}

	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soid); 
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public CustomerFile getCustomerFile() {
		return (customerFile!=null ?customerFile:customerFileManager.get(customerFileId));
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public List getMaterial() {
		return material;
	}

	public void setMaterial(List material) {
		this.material = material;
	}

	/**
	 * @return the revisedChargeValue
	 */
	public String getRevisedChargeValue() {
		return revisedChargeValue;
	}

	/**
	 * @param revisedChargeValue the revisedChargeValue to set
	 */
	public void setRevisedChargeValue(String revisedChargeValue) {
		this.revisedChargeValue = revisedChargeValue;
	}

	/**
	 * @return the billCompleteList
	 */
	public List getBillCompleteList() {
		return billCompleteList;
	}

	/**
	 * @param billCompleteList the billCompleteList to set
	 */
	public void setBillCompleteList(List billCompleteList) {
		this.billCompleteList = billCompleteList;
	}

	/**
	 * @return the shipnum
	 */
	public String getShipnum() {
		return shipnum;
	}

	/**
	 * @param shipnum the shipnum to set
	 */
	public void setShipnum(String shipnum) {
		this.shipnum = shipnum;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getPayMethods() {
		return payMethods;
	}

	public void setPayMethods(String payMethods) {
		this.payMethods = payMethods;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Map<String, String> getBillStatus() {
		return billStatus;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Long getId1() {
		return id1;
	}

	public Map<String, String> getLocTYPE() {
		return locTYPE;
	}

	public void setLocTYPE(Map<String, String> locTYPE) {
		this.locTYPE = locTYPE;
	}

	public String getCountCheck() {
		return countCheck;
	}

	public void setCountCheck(String countCheck) {
		this.countCheck = countCheck;
	}

	public String getExceedDate() {
		return exceedDate;
	}

	public void setExceedDate(String exceedDate) {
		this.exceedDate = exceedDate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Long getSoid() {
		return soid;
	}

	public void setSoid(Long soid) {
		this.soid = soid;
	}

	public Map<String, String> getExec() {
		return exec;
	}

	public void setExec(Map<String, String> exec) {
		this.exec = exec;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List getWorkList() {
		return workList;
	}

	public void setWorkList(List workList) {
		this.workList = workList;
	}

	public List getActSummaryList() {
		return actSummaryList;
	}

	public void setActSummaryList(List actSummaryList) {
		this.actSummaryList = actSummaryList;
	}

	/**
	 * @return the ticketSortOrder
	 */
	public String getTicketSortOrder() {
		return ticketSortOrder;
	}

	/**
	 * @param ticketSortOrder the ticketSortOrder to set
	 */
	public void setTicketSortOrder(String ticketSortOrder) {
		this.ticketSortOrder = ticketSortOrder;
	}

	/**
	 * @return the orderForTicket
	 */
	public String getOrderForTicket() {
		return orderForTicket;
	}

	/**
	 * @param orderForTicket the orderForTicket to set
	 */
	public void setOrderForTicket(String orderForTicket) {
		this.orderForTicket = orderForTicket;
	}

	public List getFullAddForOCity() {
		return fullAddForOCity;
	}

	public void setFullAddForOCity(List fullAddForOCity) {
		this.fullAddForOCity = fullAddForOCity;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public List getCustomerSO() {
		return customerSO;
	}

	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getCountTicket() {
		return countTicket;
	}

	public void setCountTicket(String countTicket) {
		this.countTicket = countTicket;
	}

	public String getMaxTicket() {
		return maxTicket;
	}

	public void setMaxTicket(String maxTicket) {
		this.maxTicket = maxTicket;
	}

	public String getMinTicket() {
		return minTicket;
	}

	public void setMinTicket(String minTicket) {
		this.minTicket = minTicket;
	}

	public Long getSidNum() {
		return sidNum;
	}

	public void setSidNum(Long sidNum) {
		this.sidNum = sidNum;
	}

	public Long getSoIdNum() {
		return soIdNum;
	}

	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}

	public long getTempTic() {
		return tempTic;
	}

	public void setTempTic(long tempTic) {
		this.tempTic = tempTic;
	}

	public List getTicketSO() {
		return ticketSO;
	}

	public void setTicketSO(List ticketSO) {
		this.ticketSO = ticketSO;
	}

	/**
	 * @return the storageOut
	 */
	public Map<String, String> getStorageOut() {
		return storageOut;
	}

	/**
	 * @return the countReleaseStorageList
	 */
	public List getCountReleaseStorageList() {
		return countReleaseStorageList;
	}

	/**
	 * @param countReleaseStorageList the countReleaseStorageList to set
	 */
	public void setCountReleaseStorageList(List countReleaseStorageList) {
		this.countReleaseStorageList = countReleaseStorageList;
	}

	/**
	 * @return the checkCompanyOriginAgent
	 */
	public String getCheckCompanyOriginAgent() {
		return checkCompanyOriginAgent;
	}

	/**
	 * @param checkCompanyOriginAgent the checkCompanyOriginAgent to set
	 */
	public void setCheckCompanyOriginAgent(String checkCompanyOriginAgent) {
		this.checkCompanyOriginAgent = checkCompanyOriginAgent;
	}

	/**
	 * @return the checkCompanyDestinationAgent
	 */
	public String getCheckCompanyDestinationAgent() {
		return checkCompanyDestinationAgent;
	}

	/**
	 * @param checkCompanyDestinationAgent the checkCompanyDestinationAgent to set
	 */
	public void setCheckCompanyDestinationAgent(String checkCompanyDestinationAgent) {
		this.checkCompanyDestinationAgent = checkCompanyDestinationAgent;
	}

	public List getStateDesc1() {
		return stateDesc1;
	}

	public void setStateDesc1(List stateDesc1) {
		this.stateDesc1 = stateDesc1;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public Map<String, String> getStateDesc1Map() {
		return stateDesc1Map;
	}

	public void setStateDesc1Map(Map<String, String> stateDesc1Map) {
		this.stateDesc1Map = stateDesc1Map;
	}

	public String getCheck() {
		return check;
	}

	public void setCheck(String check) {
		this.check = check;
	}

	public String getCheck1() {
		return check1;
	}

	public void setCheck1(String check1) {
		this.check1 = check1;
	}

	public String getShowMap() {
		return showMap;
	}

	public void setShowMap(String showMap) {
		this.showMap = showMap;
	}

	public List getDispathMapList() {
		return dispathMapList;
	}

	public void setDispathMapList(List dispathMapList) {
		this.dispathMapList = dispathMapList;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public Boolean getDestinMap() {
		return destinMap;
	}

	public void setDestinMap(Boolean destinMap) {
		this.destinMap = destinMap;
	}

	public Boolean getOriginMap() {
		return originMap;
	}

	public void setOriginMap(Boolean originMap) {
		this.originMap = originMap;
	}

	public Boolean getStateDC() {
		return stateDC;
	}

	public void setStateDC(Boolean stateDC) {
		this.stateDC = stateDC;
	}

	public Boolean getStateMD() {
		return stateMD;
	}

	public void setStateMD(Boolean stateMD) {
		this.stateMD = stateMD;
	}

	public Boolean getStateVA() {
		return stateVA;
	}

	public void setStateVA(Boolean stateVA) {
		this.stateVA = stateVA;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMapStates() {
		return mapStates;
	}

	public void setMapStates(String mapStates) {
		this.mapStates = mapStates;
	}

	public OperationsHubLimits getOperationsHubLimits() {
		return operationsHubLimits;
	}

	public void setOperationsHubLimits(OperationsHubLimits operationsHubLimits) {
		this.operationsHubLimits = operationsHubLimits;
	}

	public Map<String, String> getHub() {
		return hub;
	}

	public void setHub(Map<String, String> hub) {
		this.hub = hub;
	}

	public String getHubID() {
		return hubID;
	}

	public void setHubID(String hubID) {
		this.hubID = hubID;
	}

	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}

	/**
	 * @return the hubId
	 */
	public String getHubId() {
		return hubId;
	}

	/**
	 * @param hubId the hubId to set
	 */
	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	public void setOperationsHubLimitsManager(
			OperationsHubLimitsManager operationsHubLimitsManager) {
		this.operationsHubLimitsManager = operationsHubLimitsManager;
	}

	public void setOperationsDailyLimitsManager(
			OperationsDailyLimitsManager operationsDailyLimitsManager) {
		this.operationsDailyLimitsManager = operationsDailyLimitsManager;
	}

	public String getStoJob() {
		return stoJob;
	}

	public void setStoJob(String stoJob) {
		this.stoJob = stoJob;
	}

	public String getStorageJob() {
		return storageJob;
	}

	public void setStorageJob(String storageJob) {
		this.storageJob = storageJob;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public List<String> getOpshub() {
		return opshub;
	}

	public void setOpshub(List<String> opshub) {
		this.opshub = opshub;
	}

	public Map<String, String> getYesno() {
		return yesno;
	}

	public void setYesno(Map<String, String> yesno) {
		this.yesno = yesno;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}

	
	public Set getStorageLibraries() {
		return storageLibraries;
	}

	public void setStorageLibraries(Set storageLibraries) {
		this.storageLibraries = storageLibraries;
	}

	public String getStorageId() {
		return storageId;
	}
	/**
	 * @return the parking
	 */
	public Map<String, String> getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(Map<String, String> parking) {
		this.parking = parking;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getStorageTypeValue() {
		return storageTypeValue;
	}

	public void setStorageTypeValue(String storageTypeValue) {
		this.storageTypeValue = storageTypeValue;
	}

	public String getStorageModeType() {
		return storageModeType;
	}

	public void setStorageModeType(String storageModeType) {
		this.storageModeType = storageModeType;
	}

	public Map<String, String> getStorageMode() {
		return storageMode;
	}

	public void setStorageMode(Map<String, String> storageMode) {
		this.storageMode = storageMode;
	}

	public Map<String, String> getStorageType() {
		return storageType;
	}

	public void setStorageType(Map<String, String> storageType) {
		this.storageType = storageType;
	}

	public Map<String, String> getParkingList() {
		return parkingList;
	}

	public void setParkingList(Map<String, String> parkingList) {
		this.parkingList = parkingList;
	}

	public Map<String, String> getElevatorList() {
		return elevatorList;
	}

	public void setElevatorList(Map<String, String> elevatorList) {
		this.elevatorList = elevatorList;
	}

	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}

	public String getCountForTicketMovementIn() {
		return countForTicketMovementIn;
	}

	public void setCountForTicketMovementIn(String countForTicketMovementIn) {
		this.countForTicketMovementIn = countForTicketMovementIn;
	}

	public String getCountForTicketMovementOut() {
		return countForTicketMovementOut;
	}

	public void setCountForTicketMovementOut(String countForTicketMovementOut) {
		this.countForTicketMovementOut = countForTicketMovementOut;
	}
	
	/**
	 * @return the siteList
	 */
	public Map<String, String> getSiteList() {
		return siteList;
	}

	/**
	 * @param siteList the siteList to set
	 */
	public void setSiteList(Map<String, String> siteList) {
		this.siteList = siteList;
	}

	public List getWorkTicketStorageExt() {
		return workTicketStorageExt;
	}

	public void setWorkTicketStorageExt(List workTicketStorageExt) {
		this.workTicketStorageExt = workTicketStorageExt;
	}

	public String getService1() {
		return service1;
	}

	public void setService1(String service1) {
		this.service1 = service1;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the currencySign
	 */
	public String getCurrencySign() {
		return currencySign;
	}

	/**
	 * @param currencySign the currencySign to set
	 */
	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}

	/**
	 * @param companyManager the companyManager to set
	 */
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getDfltWhse() {
		return dfltWhse;
	}

	public void setDfltWhse(String dfltWhse) {
		this.dfltWhse = dfltWhse;
	}

	public String getHandout() {
		return handout;
	}

	public void setHandout(String handout) {
		this.handout = handout;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Long getHoTicket() {
		return hoTicket;
	}

	public void setHoTicket(Long hoTicket) {
		this.hoTicket = hoTicket;
	}

	public List getWorkTicketServiceList() {
		return workTicketServiceList;
	}

	public void setWorkTicketServiceList(List workTicketServiceList) {
		this.workTicketServiceList = workTicketServiceList;
	}

	public String getWorkTicketServiceId() {
		return workTicketServiceId;
	}

	public void setWorkTicketServiceId(String workTicketServiceId) {
		this.workTicketServiceId = workTicketServiceId;
	}

	public String getWtktServiceId() {
		return wtktServiceId;
	}

	public void setWtktServiceId(String wtktServiceId) {
		this.wtktServiceId = wtktServiceId;
	}

	public String getServiceCheck() {
		return serviceCheck;
	}

	public void setServiceCheck(String serviceCheck) {
		this.serviceCheck = serviceCheck;
	}

	public List getHandOutVolumePieces() {
		return handOutVolumePieces;
	}

	public void setHandOutVolumePieces(List handOutVolumePieces) {
		this.handOutVolumePieces = handOutVolumePieces;
	}

	public List getHandoutPiecesAndVolumeList() {
		return handoutPiecesAndVolumeList;
	}

	public void setHandoutPiecesAndVolumeList(List handoutPiecesAndVolumeList) {
		this.handoutPiecesAndVolumeList = handoutPiecesAndVolumeList;
	}

	public Integer getTotalHandoutPieces() {
		return totalHandoutPieces;
	}

	public void setTotalHandoutPieces(Integer totalHandoutPieces) {
		this.totalHandoutPieces = totalHandoutPieces;
	}

	public String getDcityForCity() {
		return dcityForCity;
	}

	public void setDcityForCity(String dcityForCity) {
		this.dcityForCity = dcityForCity;
	}

	public Double getTotalHandoutVolume() {
		return totalHandoutVolume;
	}

	public void setTotalHandoutVolume(Double totalHandoutVolume) {
		this.totalHandoutVolume = totalHandoutVolume;
	}

	public String getWeightVolumeCheck() {
		return weightVolumeCheck;
	}

	public void setWeightVolumeCheck(String weightVolumeCheck) {
		this.weightVolumeCheck = weightVolumeCheck;
	}

	public String getFlag3() {
		return flag3;
	}

	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}
	@SkipValidation
	public Map<String, String> getOriginAddress() {
		return( originAddress!=null && !(originAddress.isEmpty()) )?originAddress:adAddressesDetailsManager.getAdAddressesDropDown(customerFileId,sessionCorpID,"Origin",customerFileJob); 
	}
	public void setOriginAddress(Map<String, String> originAddress) {
		this.originAddress = originAddress;
	}

	@SkipValidation
	public Map<String, String> getDestinationAddress() {
		return( destinationAddress!=null && !(destinationAddress.isEmpty()))?destinationAddress:adAddressesDetailsManager.getAdAddressesDropDown(customerFileId,sessionCorpID,"Destination",customerFileJob); 
	}
	@SkipValidation
	public String findToolTipServiceForWorkListCity(){
		
		fullAddForOCity=workTicketManager.findFullCityAdd(shipNumberForCity, ocityForCity, sessionCorpID);
		
		return SUCCESS;
	}
	@SkipValidation
	public String findToolTipServiceForDestinationWorkListCity(){
		fullAddForOCity=workTicketManager.findFullDestinationCityAdd(shipNumberForCity, dcityForCity, sessionCorpID);
		
		return SUCCESS;
	}

	public void setDestinationAddress(Map<String, String> destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}
	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getAddType() {
		return addType;
	}

	public void setAddType(String addType) {
		this.addType = addType;
	}

	public String getDescType() {
		return descType;
	}

	public void setDescType(String descType) {
		this.descType = descType;
	}
		
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getDescp() {
		return descp;
	}
	public void setDescp(String descp) {
		this.descp = descp;
	}

	public StorageLibrary getStorageLibrary() {
		return storageLibrary;
	}

	public void setStorageLibrary(StorageLibrary storageLibrary) {
		this.storageLibrary = storageLibrary;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getStdAddId() {
		return stdAddId;
	}

	public void setStdAddId(String stdAddId) {
		this.stdAddId = stdAddId;
	}

	public String getCustomerFileJob() {
		return customerFileJob;
	}

	public void setCustomerFileJob(String customerFileJob) {
		this.customerFileJob = customerFileJob;
	}

	public String getWarehouseValue() {
		return warehouseValue;
	}

	public void setWarehouseValue(String warehouseValue) {
		this.warehouseValue = warehouseValue;
	}

	public List getCrewNameContractor() {
		return crewNameContractor;
	}

	public void setCrewNameContractor(List crewNameContractor) {
		this.crewNameContractor = crewNameContractor;
	}

	public String getWrehouse() {
		return wrehouse;
	}

	public void setWrehouse(String wrehouse) {
		this.wrehouse = wrehouse;
	}
	
	public String getWrehouseDesc() {
		return wrehouseDesc;
	}

	public void setWrehouseDesc(String wrehouseDesc) {
		this.wrehouseDesc = wrehouseDesc;
	}

	public String getDummyDistanceUnit() {
		return dummyDistanceUnit;
	}

	public void setDummyDistanceUnit(String dummyDistanceUnit) {
		this.dummyDistanceUnit = dummyDistanceUnit;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public String getWorkticketService() {
		return workticketService;
	}

	public void setWorkticketService(String workticketService) {
		this.workticketService = workticketService;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getEstimatedWeight() {
		return estimatedWeight;
	}

	public void setEstimatedWeight(Double estimatedWeight) {
		this.estimatedWeight = estimatedWeight;
	}

	public Double getEstimatedCubicFeet() {
		return estimatedCubicFeet;
	}

	public void setEstimatedCubicFeet(Double estimatedCubicFeet) {
		this.estimatedCubicFeet = estimatedCubicFeet;
	}

	public AuditTrail getAuditTrail() {
		return auditTrail;
	}

	public void setAuditTrail(AuditTrail auditTrail) {
		this.auditTrail = auditTrail;
	}

	public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
		this.auditTrailManager = auditTrailManager;
	}

	public Boolean getCheckOpsRole() {
		return checkOpsRole;
	}

	public void setCheckOpsRole(Boolean checkOpsRole) {
		this.checkOpsRole = checkOpsRole;
	}

	public String getCheckLimit() {
		return checkLimit;
	}

	public void setCheckLimit(String checkLimit) {
		this.checkLimit = checkLimit;
	}

	public String getOldTargetActual() {
		return oldTargetActual;
	}

	public void setOldTargetActual(String oldTargetActual) {
		this.oldTargetActual = oldTargetActual;
	}

	public List getResourceTransfer() {
		return resourceTransfer;
	}

	public void setResourceTransfer(List resourceTransfer) {
		this.resourceTransfer = resourceTransfer;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getBtntype() {
		return btntype;
	}

	public void setBtntype(String btntype) {
		this.btntype = btntype;
	}

	public String getResourceTicketURL() {
		return resourceTicketURL;
	}

	public void setResourceTicketURL(String resourceTicketURL) {
		this.resourceTicketURL = resourceTicketURL;
	}

	public String getTickets() {
		return tickets;
	}

	public void setTickets(String tickets) {
		this.tickets = tickets;
	}

	public String getTicketPendingStatus() {
		return ticketPendingStatus;
	}

	public void setTicketPendingStatus(String ticketPendingStatus) {
		this.ticketPendingStatus = ticketPendingStatus;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Map<String, String> getResourceTransferMapDTO() {
		return resourceTransferMapDTO;
	}

	public void setResourceTransferMapDTO(Map<String, String> resourceTransferMapDTO) {
		this.resourceTransferMapDTO = resourceTransferMapDTO;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public List getServiceDesc() {
		return serviceDesc;
	}

	public void setServiceDesc(List serviceDesc) {
		this.serviceDesc = serviceDesc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List getServiceOrderSearchType() {
		return serviceOrderSearchType;
	}

	public void setServiceOrderSearchType(List serviceOrderSearchType) {
		this.serviceOrderSearchType = serviceOrderSearchType;
	}

	public String getServiceOrderSearchVal() {
		return serviceOrderSearchVal;
	}

	public void setServiceOrderSearchVal(String serviceOrderSearchVal) {
		this.serviceOrderSearchVal = serviceOrderSearchVal;
	}

	public String getWorkTicketReviewStatus() {
		return workTicketReviewStatus;
	}

	public void setWorkTicketReviewStatus(String workTicketReviewStatus) {
		this.workTicketReviewStatus = workTicketReviewStatus;
	}

	public String getBillingCheckFlag() {
		return billingCheckFlag;
	}

	public void setBillingCheckFlag(String billingCheckFlag) {
		this.billingCheckFlag = billingCheckFlag;
	}

	public String getTargetActual() {
		return targetActual;
	}

	public void setTargetActual(String targetActual) {
		this.targetActual = targetActual;
	}

	public WorkTicket getAddCopyWorkTicket() {
		return addCopyWorkTicket;
	}

	public void setAddCopyWorkTicket(WorkTicket addCopyWorkTicket) {
		this.addCopyWorkTicket = addCopyWorkTicket;
	}

	public Long getWid() {
		return wid;
	}

	public void setWid(Long wid) {
		this.wid = wid;
	}

	public String getCopyWorkTicketFlag() {
		return copyWorkTicketFlag;
	}

	public void setCopyWorkTicketFlag(String copyWorkTicketFlag) {
		this.copyWorkTicketFlag = copyWorkTicketFlag;
	}

	public String getBillToAuthority() {
		return billToAuthority;
	}

	public void setBillToAuthority(String billToAuthority) {
		this.billToAuthority = billToAuthority;
	}

	public Date getAuthUpdated() {
		return authUpdated;
	}

	public void setAuthUpdated(Date authUpdated) {
		this.authUpdated = authUpdated;
	}

	public Map<String, String> getInstructionCodeWithEnblCountry() {
		return instructionCodeWithEnblCountry;
	}

	public void setInstructionCodeWithEnblCountry(
			Map<String, String> instructionCodeWithEnblCountry) {
		this.instructionCodeWithEnblCountry = instructionCodeWithEnblCountry;
	}

	public String getOperationChk() {
		return operationChk;
	}

	public void setOperationChk(String operationChk) {
		this.operationChk = operationChk;
	}

	public String getShowList() {
		return showList;
	}

	public void setShowList(String showList) {
		this.showList = showList;
	}

	public List getCrewNameList() {
		return crewNameList;
	}

	public void setCrewNameList(List crewNameList) {
		this.crewNameList = crewNameList;
	}

	public String getCrewName() {
		return crewName;
	}

	public void setCrewName(String crewName) {
		this.crewName = crewName;
	}

	public String getNameWithCode() {
		return nameWithCode;
	}

	public void setNameWithCode(String nameWithCode) {
		this.nameWithCode = nameWithCode;
	}

	public String getCoordinatorName() {
		return coordinatorName;
	}

	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public List getDriverTicketSchedule() {
		return driverTicketSchedule;
	}

	public void setDriverTicketSchedule(List driverTicketSchedule) {
		this.driverTicketSchedule = driverTicketSchedule;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getUserParentAgent() {
		return userParentAgent;
	}

	public void setUserParentAgent(String userParentAgent) {
		this.userParentAgent = userParentAgent;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public Map<String, String> getDistinctHubDescription() {
		return distinctHubDescription;
	}

	public void setDistinctHubDescription(Map<String, String> distinctHubDescription) {
		this.distinctHubDescription = distinctHubDescription;
	}

	public String getCustomsInTicketNumber() {
		return customsInTicketNumber;
	}

	public void setCustomsInTicketNumber(String customsInTicketNumber) {
		this.customsInTicketNumber = customsInTicketNumber;
	}

	public Date getDailySheetDate() {
		return dailySheetDate;
	}

	public void setDailySheetDate(Date dailySheetDate) {
		this.dailySheetDate = dailySheetDate;
	}

	public List getDailySheetList() {
		return dailySheetList;
	}

	public void setDailySheetList(List dailySheetList) {
		this.dailySheetList = dailySheetList;
	}

	public String getwRHouse() {
		return wRHouse;
	}

	public void setwRHouse(String wRHouse) {
		this.wRHouse = wRHouse;
	}

	public String getCrewNumList() {
		return crewNumList;
	}

	public void setCrewNumList(String crewNumList) {
		this.crewNumList = crewNumList;
	}

	public List getAvailableTrucks() {
		return availableTrucks;
	}

	public void setAvailableTrucks(List availableTrucks) {
		this.availableTrucks = availableTrucks;
	}

	public String getDailySheetDateDisplay() {
		return dailySheetDateDisplay;
	}

	public void setDailySheetDateDisplay(String dailySheetDateDisplay) {
		this.dailySheetDateDisplay = dailySheetDateDisplay;
	}

	public Long getoId() {
		return oId;
	}

	public void setoId(Long oId) {
		this.oId = oId;
	}

	public List getwTAddList() {
		return wTAddList;
	}

	public void setwTAddList(List wTAddList) {
		this.wTAddList = wTAddList;
	}

	public String getwOAddress1() {
		return wOAddress1;
	}

	public void setwOAddress1(String wOAddress1) {
		this.wOAddress1 = wOAddress1;
	}

	public String getwOAddress2() {
		return wOAddress2;
	}

	public void setwOAddress2(String wOAddress2) {
		this.wOAddress2 = wOAddress2;
	}

	public String getwOAddress3() {
		return wOAddress3;
	}

	public void setwOAddress3(String wOAddress3) {
		this.wOAddress3 = wOAddress3;
	}

	public String getwOriginCountry() {
		return wOriginCountry;
	}

	public void setwOriginCountry(String wOriginCountry) {
		this.wOriginCountry = wOriginCountry;
	}

	public String getwOState() {
		return wOState;
	}

	public void setwOState(String wOState) {
		this.wOState = wOState;
	}

	public String getwOCity() {
		return wOCity;
	}

	public void setwOCity(String wOCity) {
		this.wOCity = wOCity;
	}

	public String getwOZip() {
		return wOZip;
	}

	public void setwOZip(String wOZip) {
		this.wOZip = wOZip;
	}

	public Long getwOId() {
		return wOId;
	}

	public void setwOId(Long wOId) {
		this.wOId = wOId;
	}

	public String getwDAddress1() {
		return wDAddress1;
	}

	public void setwDAddress1(String wDAddress1) {
		this.wDAddress1 = wDAddress1;
	}

	public String getwDAddress2() {
		return wDAddress2;
	}

	public void setwDAddress2(String wDAddress2) {
		this.wDAddress2 = wDAddress2;
	}

	public String getwDAddress3() {
		return wDAddress3;
	}

	public void setwDAddress3(String wDAddress3) {
		this.wDAddress3 = wDAddress3;
	}

	public String getwDestinCountry() {
		return wDestinCountry;
	}

	public void setwDestinCountry(String wDestinCountry) {
		this.wDestinCountry = wDestinCountry;
	}

	public String getwDState() {
		return wDState;
	}

	public void setwDState(String wDState) {
		this.wDState = wDState;
	}

	public String getwDCity() {
		return wDCity;
	}

	public void setwDCity(String wDCity) {
		this.wDCity = wDCity;
	}

	public String getwDZip() {
		return wDZip;
	}

	public void setwDZip(String wDZip) {
		this.wDZip = wDZip;
	}

	public Double getwActWt() {
		return wActWt;
	}

	public void setwActWt(Double wActWt) {
		this.wActWt = wActWt;
	}

	public String getOmdCrewsNumList() {
		return omdCrewsNumList;
	}

	public void setOmdCrewsNumList(String omdCrewsNumList) {
		this.omdCrewsNumList = omdCrewsNumList;
	}

	public String getwDailySheetNotes() {
		return wDailySheetNotes;
	}

	public void setwDailySheetNotes(String wDailySheetNotes) {
		this.wDailySheetNotes = wDailySheetNotes;
	}

	public String getDailySheetNotesValue() {
		return dailySheetNotesValue;
	}

	public void setDailySheetNotesValue(String dailySheetNotesValue) {
		this.dailySheetNotesValue = dailySheetNotesValue;
	}

	public String getAssignedCrews() {
		return assignedCrews;
	}

	public void setAssignedCrews(String assignedCrews) {
		this.assignedCrews = assignedCrews;
	}

	public String getwTicket() {
		return wTicket;
	}

	public void setwTicket(String wTicket) {
		this.wTicket = wTicket;
	}

	public List getMiddleList() {
		return middleList;
	}

	public void setMiddleList(List middleList) {
		this.middleList = middleList;
	}

	public String getAbsentCrews() {
		return absentCrews;
	}

	public void setAbsentCrews(String absentCrews) {
		this.absentCrews = absentCrews;
	}

	public String getJsonCountryCodeText() {
		return jsonCountryCodeText;
	}

	public void setJsonCountryCodeText(String jsonCountryCodeText) {
		this.jsonCountryCodeText = jsonCountryCodeText;
	}

	public Map<String, String> getLocalParameter() {
		return localParameter;
	}

	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}

	public void setTimeSheetManager(TimeSheetManager timeSheetManager) {
		this.timeSheetManager = timeSheetManager;
	}

	public TimeSheet getTimeSheet() {
		return timeSheet;
	}

	public void setTimeSheet(TimeSheet timeSheet) {
		this.timeSheet = timeSheet;
	}

	public Long getdSTicket() {
		return dSTicket;
	}

	public void setdSTicket(Long dSTicket) {
		this.dSTicket = dSTicket;
	}

	public String getdSSNumber() {
		return dSSNumber;
	}

	public void setdSSNumber(String dSSNumber) {
		this.dSSNumber = dSSNumber;
	}

	public String getdSCName() {
		return dSCName;
	}

	public void setdSCName(String dSCName) {
		this.dSCName = dSCName;
	}

	public String getdSWHouse() {
		return dSWHouse;
	}

	public void setdSWHouse(String dSWHouse) {
		this.dSWHouse = dSWHouse;
	}

	public List getAvailableCrewsDetails() {
		return availableCrewsDetails;
	}

	public void setAvailableCrewsDetails(List availableCrewsDetails) {
		this.availableCrewsDetails = availableCrewsDetails;
	}

	public String getAvailableCrews() {
		return availableCrews;
	}

	public void setAvailableCrews(String availableCrews) {
		this.availableCrews = availableCrews;
	}

	public String getdSJob() {
		return dSJob;
	}

	public void setdSJob(String dSJob) {
		this.dSJob = dSJob;
	}

	public String getdSService() {
		return dSService;
	}

	public void setdSService(String dSService) {
		this.dSService = dSService;
	}

	public PayrollAllocation getPayrollAllocation() {
		return payrollAllocation;
	}

	public void setPayrollAllocation(PayrollAllocation payrollAllocation) {
		this.payrollAllocation = payrollAllocation;
	}
	
	public String getAssignedTrucks() {
		return assignedTrucks;
	}

	public void setAssignedTrucks(String assignedTrucks) {
		this.assignedTrucks = assignedTrucks;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public TruckingOperations getTruckingOperations() {
		return truckingOperations;
	}

	public void setTruckingOperations(TruckingOperations truckingOperations) {
		this.truckingOperations = truckingOperations;
	}

	public void setTruckManager(TruckManager truckManager) {
		this.truckManager = truckManager;
	}

	public void setTruckingOperationsManager(
			TruckingOperationsManager truckingOperationsManager) {
		this.truckingOperationsManager = truckingOperationsManager;
	}
	public Date getDailySheetCalenderDate() {
		return dailySheetCalenderDate;
	}

	public void setDailySheetCalenderDate(Date dailySheetCalenderDate) {
		this.dailySheetCalenderDate = dailySheetCalenderDate;
	}	

	public String getDailySheetwRHouse() {
		return dailySheetwRHouse;
	}

	public void setDailySheetwRHouse(String dailySheetwRHouse) {
		this.dailySheetwRHouse = dailySheetwRHouse;
	}

	public Map<String, List> getDailySheetCalenderList1() {
		return dailySheetCalenderList1;
	}

	public void setDailySheetCalenderList1(Map<String, List> dailySheetCalenderList1) {
		this.dailySheetCalenderList1 = dailySheetCalenderList1;
	}

	public List getLeftListTruck() {
		return leftListTruck;
	}

	public void setLeftListTruck(List leftListTruck) {
		this.leftListTruck = leftListTruck;
	}

	public List getRightListTruck() {
		return rightListTruck;
	}

	public void setRightListTruck(List rightListTruck) {
		this.rightListTruck = rightListTruck;
	}

	public List getLeftList() {
		return leftList;
	}

	public void setLeftList(List leftList) {
		this.leftList = leftList;
	}
	public boolean isNetworkAgent() {
		return networkAgent;
	}

	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}

	public String getMmValidation() {
		return mmValidation;
	}

	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}

	public void setWorkTicketTimeManagementManager(
			WorkTicketTimeManagementManager workTicketTimeManagementManager) {
		this.workTicketTimeManagementManager = workTicketTimeManagementManager;
	}

	public List getTimeRequirementList() {
		return timeRequirementList;
	}

	public void setTimeRequirementList(List timeRequirementList) {
		this.timeRequirementList = timeRequirementList;
	}

	public String getTimeManagementList() {
		return timeManagementList;
	}

	public void setTimeManagementList(String timeManagementList) {
		this.timeManagementList = timeManagementList;
	}

	public WorkTicketTimeManagement getWorkTicketTimeManagement() {
		return workTicketTimeManagement;
	}

	public void setWorkTicketTimeManagement(
			WorkTicketTimeManagement workTicketTimeManagement) {
		this.workTicketTimeManagement = workTicketTimeManagement;
	}

	public String getResourceExList() {
		return resourceExList;
	}

	public void setResourceExList(String resourceExList) {
		this.resourceExList = resourceExList;
	}

	public String getTimeRequirementId() {
		return timeRequirementId;
	}

	public void setTimeRequirementId(String timeRequirementId) {
		this.timeRequirementId = timeRequirementId;
	}

	public String getWorkTicketId() {
		return workTicketId;
	}

	public void setWorkTicketId(String workTicketId) {
		this.workTicketId = workTicketId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public void setScheduleResourceOperationManager(
			ScheduleResourceOperationManager scheduleResourceOperationManager) {
		this.scheduleResourceOperationManager = scheduleResourceOperationManager;
	}

	public List getScId() {
		return scId;
	}

	public void setScId(List scId) {
		this.scId = scId;
	}

	public ScheduleResourceOperation getScheduleResourceOperation() {
		return scheduleResourceOperation;
	}

	public void setScheduleResourceOperation(
			ScheduleResourceOperation scheduleResourceOperation) {
		this.scheduleResourceOperation = scheduleResourceOperation;
	}

	public AccessInfo getAccessInfo() {
		return accessInfo;
	}

	public void setAccessInfo(AccessInfo accessInfo) {
		this.accessInfo = accessInfo;
	}

	public Boolean getVisibilityForSSCW() {
		return visibilityForSSCW;
	}

	public void setVisibilityForSSCW(Boolean visibilityForSSCW) {
		this.visibilityForSSCW = visibilityForSSCW;
	}

	public String getWorkTicketOriginShuttle() {
		return workTicketOriginShuttle;
	}

	public void setWorkTicketOriginShuttle(String workTicketOriginShuttle) {
		this.workTicketOriginShuttle = workTicketOriginShuttle;
	}

	public String getWorkTicketDestinationShuttle() {
		return workTicketDestinationShuttle;
	}

	public void setWorkTicketDestinationShuttle(String workTicketDestinationShuttle) {
		this.workTicketDestinationShuttle = workTicketDestinationShuttle;
	}

	public List getMyFileList() {
		return myFileList;
	}

	public void setMyFileList(List myFileList) {
		this.myFileList = myFileList;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public String getMyFileListId() {
		return myFileListId;
	}

	public void setMyFileListId(String myFileListId) {
		this.myFileListId = myFileListId;
	}

	public Integer getDocCount() {
		return docCount;
	}

	public void setDocCount(Integer docCount) {
		this.docCount = docCount;
	}

	public String getHubWarehouseLimit() {
		return hubWarehouseLimit;
	}

	public void setHubWarehouseLimit(String hubWarehouseLimit) {
		this.hubWarehouseLimit = hubWarehouseLimit;
	}

	public CrewCapacity getCrewCapacity() {
		return crewCapacity;
	}

	public void setCrewCapacity(CrewCapacity crewCapacity) {
		this.crewCapacity = crewCapacity;
	}

	public List<CrewCapacity> getCrewCapacityList() {
		return crewCapacityList;
	}

	public void setCrewCapacityList(List<CrewCapacity> crewCapacityList) {
		this.crewCapacityList = crewCapacityList;
	}

	public void setCrewCapacityManager(CrewCapacityManager crewCapacityManager) {
		this.crewCapacityManager = crewCapacityManager;
	}

	public Boolean getCheckCrewListRole() {
		return checkCrewListRole;
	}

	public void setCheckCrewListRole(Boolean checkCrewListRole) {
		this.checkCrewListRole = checkCrewListRole;
	}

	public String getThreshHoldFlag() {
		return threshHoldFlag;
	}

	public void setThreshHoldFlag(String threshHoldFlag) {
		this.threshHoldFlag = threshHoldFlag;
	}

	public String getCrewCalTempDate() {
		return crewCalTempDate;
	}

	public void setCrewCalTempDate(String crewCalTempDate) {
		this.crewCalTempDate = crewCalTempDate;
	}

	public String getCrewCalendarwRHouse() {
		return crewCalendarwRHouse;
	}

	public void setCrewCalendarwRHouse(String crewCalendarwRHouse) {
		this.crewCalendarwRHouse = crewCalendarwRHouse;
	}

	public Date getCrewCalendarDate() {
		return crewCalendarDate;
	}

	public void setCrewCalendarDate(Date crewCalendarDate) {
		this.crewCalendarDate = crewCalendarDate;
	}	

	public String getWareHouseUtilization() {
		return wareHouseUtilization;
	}

	public void setWareHouseUtilization(String wareHouseUtilization) {
		this.wareHouseUtilization = wareHouseUtilization;
	}

	public Map<String, String> getCrewCalanderList() {
		return crewCalanderList;
	}

	public void setCrewCalanderList(Map<String, String> crewCalanderList) {
		this.crewCalanderList = crewCalanderList;
	}

	public String getPathSeparator() {
		return pathSeparator;
	}

	public void setPathSeparator(String pathSeparator) {
		this.pathSeparator = pathSeparator;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setPrintDailyPackageManager(
			PrintDailyPackageManager printDailyPackageManager) {
		this.printDailyPackageManager = printDailyPackageManager;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}

	public String getPrintFlag() {
		return printFlag;
	}

	public void setPrintFlag(String printFlag) {
		this.printFlag = printFlag;
	}

	public List getWtServiceTypeDyna() {
		return wtServiceTypeDyna;
	}

	public void setWtServiceTypeDyna(List wtServiceTypeDyna) {
		this.wtServiceTypeDyna = wtServiceTypeDyna;
	}

	public String getTypeService() {
		return typeService;
	}

	public void setTypeService(String typeService) {
		this.typeService = typeService;
	}

	public boolean isJobFlag() {
		return jobFlag;
	}

	public void setJobFlag(boolean jobFlag) {
		this.jobFlag = jobFlag;
	}

	public boolean isServiceflag() {
		return serviceflag;
	}

	public void setServiceflag(boolean serviceflag) {
		this.serviceflag = serviceflag;
	}

	public boolean isModeFlag() {
		return modeFlag;
	}

	public void setModeFlag(boolean modeFlag) {
		this.modeFlag = modeFlag;
	}

	public boolean isMilitaryFlag() {
		return militaryFlag;
	}

	public void setMilitaryFlag(boolean militaryFlag) {
		this.militaryFlag = militaryFlag;
	}

	public List getServiceArrList() {
		return serviceArrList;
	}

	public void setServiceArrList(List serviceArrList) {
		this.serviceArrList = serviceArrList;
	}

	public List getJobArrList() {
		return jobArrList;
	}

	public void setJobArrList(List jobArrList) {
		this.jobArrList = jobArrList;
	}

	public Map<String, String> getCountryDsc() {
		return countryDsc;
	}

	public void setCountryDsc(Map<String, String> countryDsc) {
		this.countryDsc = countryDsc;
	}

	public String getAmid() {
		return amid;
	}

	public void setAmid(String amid) {
		this.amid = amid;
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Date getTempDate() {
		return tempDate;
	}

	public void setTempDate(Date tempDate) {
		this.tempDate = tempDate;
	}

	public String getOiValue() {
		return oiValue;
	}

	public void setOiValue(String oiValue) {
		this.oiValue = oiValue;
	}

	public String getDelimeter1() {
		return delimeter1;
	}

	public void setDelimeter1(String delimeter1) {
		this.delimeter1 = delimeter1;
	}

	public String getDelimeter() {
		return delimeter;
	}

	public void setDelimeter(String delimeter) {
		this.delimeter = delimeter;
	}

	public String getWorkorder() {
		return workorder;
	}

	public void setWorkorder(String workorder) {
		this.workorder = workorder;
	}

	public void setOperationsIntelligenceManager(
			OperationsIntelligenceManager operationsIntelligenceManager) {
		this.operationsIntelligenceManager = operationsIntelligenceManager;
	}

	public Boolean getCheckForLimitInOI() {
		return checkForLimitInOI;
	}

	public void setCheckForLimitInOI(Boolean checkForLimitInOI) {
		this.checkForLimitInOI = checkForLimitInOI;
	}

	public String getDataForCheckLimit() {
		return dataForCheckLimit;
	}

	public void setDataForCheckLimit(String dataForCheckLimit) {
		this.dataForCheckLimit = dataForCheckLimit;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getTempForStatus() {
		return tempForStatus;
	}

	public void setTempForStatus(String tempForStatus) {
		this.tempForStatus = tempForStatus;
	}

	public List getOIListByWorkOrder() {
		return OIListByWorkOrder;
	}

	public void setOIListByWorkOrder(List oIListByWorkOrder) {
		OIListByWorkOrder = oIListByWorkOrder;
	}

	public String getResorceTypeOI() {
		return resorceTypeOI;
	}

	public void setResorceTypeOI(String resorceTypeOI) {
		this.resorceTypeOI = resorceTypeOI;
	}

	public String getResorceDescriptionOI() {
		return resorceDescriptionOI;
	}

	public void setResorceDescriptionOI(String resorceDescriptionOI) {
		this.resorceDescriptionOI = resorceDescriptionOI;
	}

	public Integer getResourceQuantitiyOI() {
		return resourceQuantitiyOI;
	}

	public void setResourceQuantitiyOI(Integer resourceQuantitiyOI) {
		this.resourceQuantitiyOI = resourceQuantitiyOI;
	}

	public String getWTResorceTypeOI() {
		return WTResorceTypeOI;
	}

	public void setWTResorceTypeOI(String wTResorceTypeOI) {
		WTResorceTypeOI = wTResorceTypeOI;
	}

	public String getWTResorceDescriptionOI() {
		return WTResorceDescriptionOI;
	}

	public void setWTResorceDescriptionOI(String wTResorceDescriptionOI) {
		WTResorceDescriptionOI = wTResorceDescriptionOI;
	}

	public Integer getWTResourceQuantitiyOI() {
		return WTResourceQuantitiyOI;
	}

	public void setWTResourceQuantitiyOI(Integer wTResourceQuantitiyOI) {
		WTResourceQuantitiyOI = wTResourceQuantitiyOI;
	}

	public List getCheckQuantityForOI() {
		return checkQuantityForOI;
	}

	public void setCheckQuantityForOI(List checkQuantityForOI) {
		this.checkQuantityForOI = checkQuantityForOI;
	}

	public long getLimitForThreshold() {
		return limitForThreshold;
	}

	public void setLimitForThreshold(long limitForThreshold) {
		this.limitForThreshold = limitForThreshold;
	}

	public String getMsgForThreshold() {
		return msgForThreshold;
	}

	public void setMsgForThreshold(String msgForThreshold) {
		this.msgForThreshold = msgForThreshold;
	}

	public List getPlanningCalendarViewList() {
		return planningCalendarViewList;
	}

	public void setPlanningCalendarViewList(List planningCalendarViewList) {
		this.planningCalendarViewList = planningCalendarViewList;
	}

	public Map<String, String> getHouseHub() {
		return houseHub;
	}

	public void setHouseHub(Map<String, String> houseHub) {
		this.houseHub = houseHub;
	}

	public String getDefaultHub() {
		return defaultHub;
	}

	public void setDefaultHub(String defaultHub) {
		this.defaultHub = defaultHub;
	}

	public String getwHouse() {
		return wHouse;
	}

	public void setwHouse(String wHouse) {
		this.wHouse = wHouse;
	}
	

	public Map<String, String> getItemsList() {
		return itemsList;
	}

	public void setItemsList(Map<String, String> itemsList) {
		this.itemsList = itemsList;
	}

	public CoraxLog getCoraxLog() {
		return coraxLog;
	}

	public void setCoraxLog(CoraxLog coraxLog) {
		this.coraxLog = coraxLog;
	}

	public void setCoraxLogManager(CoraxLogManager coraxLogManager) {
		this.coraxLogManager = coraxLogManager;
	}

	public boolean isUpdateValue() {
		return updateValue;
	}

	public void setUpdateValue(boolean updateValue) {
		this.updateValue = updateValue;
	}
	@SkipValidation
	public List getAddressBookList() {
		return ( addressBookList!=null && !(addressBookList.isEmpty()))?addressBookList:standardAddressesManager.getAddressLine1(customerFileJob,sessionCorpID);
	}
	
	public void setAddressBookList(List addressBookList) {
		this.addressBookList = addressBookList;
	}

	public void setStandardAddressesManager(
			StandardAddressesManager standardAddressesManager) {
		this.standardAddressesManager = standardAddressesManager;
	}

	public String getResidenceName() {
		return residenceName;
	}

	public void setResidenceName(String residenceName) {
		this.residenceName = residenceName;
	}

	public StandardAddresses getStandardAddresses() {
		return standardAddresses;
	}

	public void setStandardAddresses(StandardAddresses standardAddresses) {
		this.standardAddresses = standardAddresses;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public Boolean getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(Boolean emailFlag) {
		this.emailFlag = emailFlag;
	}

	public static Map<String, String> getLoctype_isactive() {
		return loctype_isactive;
	}

	public static void setLoctype_isactive(Map<String, String> loctype_isactive) {
		WorkTicketAction.loctype_isactive = loctype_isactive;
	}

	public String getCheckForAppendFields() {
		return checkForAppendFields;
	}

	public void setCheckForAppendFields(String checkForAppendFields) {
		this.checkForAppendFields = checkForAppendFields;
	}

	public String getCalendarViewStyle() {
		return calendarViewStyle;
	}

	public void setCalendarViewStyle(String calendarViewStyle) {
		this.calendarViewStyle = calendarViewStyle;
	}

	public String getMsgClicked() {
		return msgClicked;
	}

	public void setMsgClicked(String msgClicked) {
		this.msgClicked = msgClicked;
	}

	public String getPrintStart() {
		return printStart;
	}

	public void setPrintStart(String printStart) {
		this.printStart = printStart;
	}

	public String getPrintEnd() {
		return printEnd;
	}

	public void setPrintEnd(String printEnd) {
		this.printEnd = printEnd;
	}

	public Map<String, String> getWoinstr_isactive() {
		return woinstr_isactive;
	}

	public void setWoinstr_isactive(Map<String, String> woinstr_isactive) {
		this.woinstr_isactive = woinstr_isactive;
	}

	public Map<String, String> getOnhandstorage_isactive() {
		return onhandstorage_isactive;
	}

	public void setOnhandstorage_isactive(Map<String, String> onhandstorage_isactive) {
		this.onhandstorage_isactive = onhandstorage_isactive;
	}

	public Map<String, String> getTcktactn_isactive() {
		return tcktactn_isactive;
	}

	public void setTcktactn_isactive(Map<String, String> tcktactn_isactive) {
		this.tcktactn_isactive = tcktactn_isactive;
	}

	public Map<String, String> getTcktservc_isactive() {
		return tcktservc_isactive;
	}

	public void setTcktservc_isactive(Map<String, String> tcktservc_isactive) {
		this.tcktservc_isactive = tcktservc_isactive;
	}

	public Map<String, String> getHouse_isactive() {
		return house_isactive;
	}

	public void setHouse_isactive(Map<String, String> house_isactive) {
		this.house_isactive = house_isactive;
	}

	public Map<String, String> getStorageOut_isactive() {
		return storageOut_isactive;
	}

	public void setStorageOut_isactive(Map<String, String> storageOut_isactive) {
		this.storageOut_isactive = storageOut_isactive;
	}

	public Map<String, String> getBillStatus_isactive() { 
		return billStatus_isactive;
	}

	public void setBillStatus_isactive(Map<String, String> billStatus_isactive) {
		this.billStatus_isactive = billStatus_isactive;
	}

	public Boolean getOpsMgmtWarehouseCheck() {
		return opsMgmtWarehouseCheck;
	}

	public void setOpsMgmtWarehouseCheck(Boolean opsMgmtWarehouseCheck) {
		this.opsMgmtWarehouseCheck = opsMgmtWarehouseCheck;
	} 
	public String getfDate() {
		return fDate;
	}

	public void setfDate(String fDate) {
		this.fDate = fDate;
	}

	public String gettDate() {
		return tDate;
	}

	public void settDate(String tDate) {
		this.tDate = tDate;
	}

	public String getParaMeter1() {
		return paraMeter1;
	}

	public void setParaMeter1(String paraMeter1) {
		this.paraMeter1 = paraMeter1;
	}

	public List getWorkListDetails() {
		return workListDetails;
	}

	public void setWorkListDetails(List workListDetails) {
		this.workListDetails = workListDetails;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	public Map<String, String> getTempWoinstrMap() {
		return tempWoinstrMap;
	}

	public void setTempWoinstrMap(Map<String, String> tempWoinstrMap) {
		this.tempWoinstrMap = tempWoinstrMap;
	}
	public List getParentParameterValuesList() {
		return parentParameterValuesList;
	}

	public void setParentParameterValuesList(List parentParameterValuesList) {
		this.parentParameterValuesList = parentParameterValuesList;
	}
	public Map getSortedItemList() {
		return sortedItemList;
	}

	public void setSortedItemList(Map sortedItemList) {
		this.sortedItemList = sortedItemList;
	}

	public List getDataItemList() {
		return dataItemList;
	}

	public void setDataItemList(List dataItemList) {
		this.dataItemList = dataItemList;
	}
	public List getAjaxParameterValuesList() {
		return AjaxParameterValuesList;
	}

	public void setAjaxParameterValuesList(List ajaxParameterValuesList) {
		AjaxParameterValuesList = ajaxParameterValuesList;
	}
	
	public List getAbsentCrewsEdit() {
			return workTicketManager.findAbsentCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID);
	}

	public void setAbsentCrewsEdit(List absentCrewsEdit) {
		this.absentCrewsEdit = absentCrewsEdit;
	}
	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}

	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}
	public String getInstructionCode() {
		return instructionCode;
	}

	public void setInstructionCode(String instructionCode) {
		this.instructionCode = instructionCode;
	}


}