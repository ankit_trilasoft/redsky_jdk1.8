<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
	<title>Pricing Engine</title>
	<meta name="heading" content="Pricing Engine"/>
    <meta name="menu" content="UserIDHelp"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
</head>
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>
<%--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" type="text/javascript"></script>
--%>
<style><%@ include file="/common/calenderStyle.css"%></style>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>

<style type="text/css">

</style>

<s:form id="agentPricing" name="agentPricing" action='locatePartners1.html' method="post" validate="true">
<s:hidden name="latitude" />
<s:hidden name="logtitude" />  
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" /> 
<s:hidden name="description" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden key="pricingControl.id"/>
<s:hidden key="pricingControl.pricingID"/>
<s:hidden key="pricingControl.shipNumber"/>
<c:set var="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" />
<s:hidden name="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" /> 
<div id="Layer1" style="width:100%;"> 

		<div id="newmnav">
		  <ul>
		  <c:if test="${!param.popup}">
		  	<li><a href="agentPricingList.html"><span>Pricing Engine List</span></a></li>
		  </c:if>	
		   <c:if test="${param.popup}">
		  	<li><a href="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"><span>Pricing Engine List</span></a></li>
		  </c:if>
		  	<li id="newmnav1" ><a class="current"><span>Pricing Engine<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;">
		<tbody>
	

  <tr>
  <td valign="top" style="margin:0px;padding: 0px;width: 75%;">
										
  	<table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
		  <tbody>  	
		 <tr>
		 <td align="left" width="" class="listwhitetext">Shipment Leg:</td>
		 </tr>
		  	<tr>
		  		
		  	  	 <c:if test="${param.popup}">
		  	  		<td align="left" width="" ><s:select cssClass="list-menu" name="pricingControl.tariffApplicability" list="{'Destination','Origin'}" headerKey="" headerValue="" cssStyle="width:170px" onchange="getAddress();"/></td>
		  		</c:if>
		  		 <c:if test="${!param.popup}">
		  	  		<td align="left" width="" ><s:select cssClass="list-menu" name="pricingControl.tariffApplicability" list="{'Destination','Origin'}" headerKey="" headerValue="" cssStyle="width:170px" /></td>
		  		</c:if>
		  	</tr>
		  	
		 	<tr>
		 	<tr>
       							<td  colspan="8" width="100%" style="margin: 0px;">
       								<span onclick="swapImg(img1);">
       								<div onClick="javascript:animatedcollapse.toggle('address');" style="margin:0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img1" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Customer Location</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
									</div>
									</span>
									<div id="address" style="margin:0px;padding:0px; ">
    			<table width="100%" style="margin:0px;padding:0px;">						
				<tr>	
		  		<td align="left" class="listwhitetext"  width="90px" colspan="4">Address:</td>
		  	<tr>	
		  		<td align="left" colspan="4"><s:textfield name="pricingControl.address1"  maxlength="100" size="32" cssClass="input-text" readonly="false" /></td>
		 	</tr>
		 	<tr>
		 		<td align="left" class="listwhitetext">Country:</td>
		 		<td align="left" class="listwhitetext">State:</td>
		 	</tr>
		 	<tr>
		 		<td align="left" colspan=""><s:select cssClass="list-menu" name="pricingControl.country" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:115px"/>
		 		<td align="left" width=""><s:textfield name="pricingControl.state"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
		  	</tr>
		  	<tr>
		 		<td align="left" class="listwhitetext">City:</td>
		 		<td align="left" class="listwhitetext">Postal:</td>
		 	</tr>
		  	
		  	<tr>	
		  		<td align="left" colspan=""><s:textfield name="pricingControl.city"  maxlength="100" size="17" cssClass="input-text" readonly="false" /></td>
		  		<td align="left"><s:textfield name="pricingControl.zip"  maxlength="100" size="8" cssClass="input-text" readonly="false" /></td>
		 	</tr>
		 
		 	<tr>
		 	
		 	<td align="left" colspan="4"><input type="button" class="cssbutton" onclick="loadGoogleGeoCode();" name="Find Geo Code" value="Confirm Location on Map" style="height:25px"/></td>  
		  	</tr>
		  	<tr>
		  		<td class="listwhitetext" align="left">Latitude</td>
		  		<td align="left" class="listwhitetext">Longitude</td>
		  	</tr>
		  	<tr>
		  		<td align="left" width="20px"><s:textfield name="pricingControl.latitude"  maxlength="100" size="10" cssClass="input-textUpper" readonly="false" /></td>
		  		<td><s:textfield name="pricingControl.longitude"  maxlength="100" size="8" cssClass="input-textUpper" readonly="false" /></td>
		 	</tr>
		 	</table>
		 	</div>
		 	</td>
		 	</tr>
		 	<tr>
       							<td  colspan="8" width="100%" style="margin: 0px;">
       								<span onclick="swapImg(img2);">
       								<div onClick="javascript:animatedcollapse.toggle('info');" style="margin:0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img2" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Information</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
									</div>
									</span>
									<div id="info" style="margin:0px;padding:0px;">
    			<table width="100%" style="margin:0px;padding:0px;">						
			
		 	
		 	<tr>
		 	<td align="left"  class="listwhitetext" colspan="2">Expected Load Date</td>
		 	
		 
		 	</tr>
		 	
		 	<tr>
		 	
		 	<c:if test="${not empty pricingControl.expectedLoadDate}">
					<s:text id="expectedLoadDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="pricingControl.expectedLoadDate"/></s:text>
					<td width="20px"><s:textfield cssClass="input-text" id="expectedLoadDate" name="pricingControl.expectedLoadDate" value="%{expectedLoadDateFormattedValue}" required="true" size="10" cssStyle="" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
					<td><img id="expectedLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
			</c:if>
			<c:if test="${empty pricingControl.expectedLoadDate}">
					<td colspan="" width="20px"><s:textfield cssClass="input-text" id="expectedLoadDate" name="pricingControl.expectedLoadDate" required="true" cssStyle="" size="10" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/></td>
					<td><img id="expectedLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			</tr>
			<tr>	<td align="left" class="listwhitetext" width="">Mode:</td></tr>
			<tr>
				
		  		<td align="left" colspan="2"><s:select cssClass="list-menu" name="pricingControl.mode" list="%{mode}" headerKey="" headerValue="" cssStyle="width:170px" onchange="openSection();"/></td>
		  	</tr>
		  	
		  	<tr>
		  	<td colspan="2" rowspan="" class="listwhitetext">
		  	    
   <table style="margin: 0px;padding: 0px;"><tr>
   <c:choose>
	<c:when test="${pricingControl.unitType == 'Lbs'}" >
   		<td class="listwhitetext" style="font-weight: bold;">(pounds/cft)<INPUT type="radio" name="pricingControl.unitType" checked="checked" value="Lbs" >(metric units)<INPUT type="radio" name="pricingControl.unitType"  value="Kgs" >
   	</c:when>
   	<c:when test="${pricingControl.unitType == 'Kgs'}" >
   		<td class="listwhitetext" style="font-weight: bold;">(pounds/cft)<INPUT type="radio" name="pricingControl.unitType"  value="Lbs" >(metric units)<INPUT type="radio" name="pricingControl.unitType" checked="checked" value="Kgs" >
   	</c:when>
   	<c:otherwise>
   		<td class="listwhitetext" style="font-weight: bold;">(pounds/cft)<INPUT type="radio" name="pricingControl.unitType"  value="Lbs" checked="checked" >(metric units)<INPUT type="radio" name="pricingControl.unitType"  value="Kgs" >
   	</c:otherwise>
   	</c:choose>
   </td></tr>
   </table>
  
   </td>
   </tr>
   <tr>
   <td align="left" class="listwhitetext" >Weight:</td>
   <td align="left" class="listwhitetext">Volume:</td>
   </tr>
   <tr>
		<td align="left" colspan=""><s:textfield name="pricingControl.weight"  maxlength="100" size="10" cssClass="input-text" readonly="false" onchange="onlyFloat(this);"/></td>
		<td align="left" colspan="" width="120px"><s:textfield name="pricingControl.volume"  maxlength="100" size="11" cssClass="input-text" onchange="onlyFloat(this);"/></td>
		    </tr>
		  	
		  	<tr>
		  	<td colspan="6" style="margin: 0px;padding: 0px;width: 100%">
		  	<div id="hid1" style="margin: 0px;padding:0px;">
		  	<table border="0" style="margin: 0px;padding: 0px;">
		  	<tr>
		  	<td align="left" class="listwhitetext" width="">Packing Mode:</td>
		  	</tr>
		  	<tr>
		  		
		  		<td align="left"><s:select cssClass="list-menu" name="pricingControl.packing" list="%{pkmode}" headerKey="" headerValue="" cssStyle="width:165px;" onchange="openRadioSection();"/></td>
		  
		  </tr>
	      <tr>
		  		
		  		<td align="left" class="listwhitetext">
		  		<div id="hid2" style="margin: 0px;padding:0px;">
		  		<c:choose>
					<c:when test="${pricingControl.lclFclFlag == 'lcl'}" >
			  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag" checked="checked" value="lcl" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="fcl" >
			  		</c:when>
			  		<c:when test="${pricingControl.lclFclFlag == 'fcl'}" >
			  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="lcl" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag" checked="checked" value="fcl">
			  		</c:when>
			  		<c:otherwise>
			  				LCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="lcl" checked="checked" >FCL<INPUT type="radio" name="pricingControl.lclFclFlag"  value="fcl" >
			  		</c:otherwise>
		  		</c:choose>
		  		</div>
		  		</td>
		  		</div>
		  	</tr>
		  <tr>
		  <td align="left" class="listwhitetext">Container Size:</td>
		  </tr>
		  	 	<tr>
		  		<td align="left" colspan=""><s:select cssClass="list-menu" name="pricingControl.containerSize" list="%{EQUIP}" headerKey="" headerValue="" cssStyle=""/></td>
		   	</tr>
		   	
		   	<tr>
		   		<td colspan="7" style="margin: 0px;padding: 0px;">
		   		<div id="hid3" style="margin: 0px;padding:0px;">
		   		<table border="0" style="margin: 0px;padding: 0px;">
		   		<tr>
		   		<td align="left" class="listwhitetext" width="">Port Of Entry:</td>
		   		</tr>
		  		<tr>
		   		
		  		<td align="left"><s:textfield name="pricingControl.POE"  maxlength="100" size="10" cssClass="input-text" readonly="false"/>
		  		<img align="top" class="openpopup" width="17" height="20" onclick="putPortName();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</tr>
				
				</table>
				</div>
				</td>
		   		
				
			</tr>
		
		  </table>
		  </div>
		  </td>
		  </tr>
		  </table>
		  </div>
		  </td>
		  </tr>
		  <tr>
       							<td  colspan="8" width="100%" style="margin: 0px;">
       								<span onclick="swapImg(img3);">
       								<div onClick="javascript:animatedcollapse.toggle('pricing');" style="margin:0px;">
       									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img3" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Pricing</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
									</div>
									</span>
		<div id="pricing" style="margin:0px;padding:0px;">
    			<table width="100%" style="margin:0px;padding:0px;">						
			
			      <tr>
			     	<td align="left" class="listwhitetext" width=""><div id="partnerList" style="width: 190px;"></div></td>
			     </tr>
			      <tr>
			     	
			      </tr>
			   </table>
		</div>
			</td>
			</tr>
			
		 
		  </tbody>
	</table>
	</td>
	
	<td valign="top" style="margin:0px;padding: 0px;width: 74%;">
				<div id="map" style="width: 810px; height: 550px;margin:0px;padding: 0px;"></div>
				</td>
	</tr>
	
  </table>


		   
		  <table class="detailTabLabel" border="0" style="margin: 0px;padding: 0px;" width="100%">
		  <tbody> 	
		   	
		   <tr>
		  		<td align="right">
        		<s:submit cssClass="cssbutton1" type="button"   value="Save" onclick="return checkFields();"/>  
        		</td>
       
        		<td align="left">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        			<input type="button" class="cssbutton" onclick="return getPricing();" name="Get Pricing" value="Get Pricing" style="height:25px"/>  
        		</td>
        		
        	
       	  	</tr>		  	
		  </tbody>
		  </table>
		  
		  </div>
<div id="mydiv" style="position:absolute; !margin-top:0px;"></div>
</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
var geocoder;
var map;

function checkFields(){
	 <c:if test='${!param.popup}'>
		 	document.forms['agentPricing'].action="saveAgentPricing.html"
	  </c:if>	
	 <c:if test='${param.popup}'>
	 	document.forms['agentPricing'].action="saveAgentPricing.html?decorator=popup&popup=true"
	  </c:if>	
		document.forms['agentPricing'].submit();
}

function openSection()
{
		var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
		var packing=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	  	var e1 = document.getElementById('hid1');
	  	var e2 = document.getElementById('hid2');
	  	var e3=document.getElementById('hid3');
		if(mode == 'Sea' && packing!='BKBLK' ){
		e1.style.display = 'block';
		e2.style.display = 'none';
		e3.style.display = 'block';
		}else if(mode == 'Sea' && packing=='BKBLK'){
		e1.style.display = 'block';
		e2.style.display = 'block';
		e3.style.display = 'none';
		}else if(mode == 'Sea' && packing=='LOOSE'){
		e1.style.display = 'block';
		e2.style.display = 'none';
		e3.style.display = 'block';
		}
		else{
		e1.style.display = 'none';
		e2.style.display = 'none';
		e3.style.display = 'none';
		}
}

function openRadioSection()
{
		var packing=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	  	var e2 = document.getElementById('hid2');
	  	var e3=document.getElementById('hid3');
		if(packing == 'BKBLK'){
		e2.style.display = 'block';
		e3.style.display = 'none';
		}else if(packing == 'LOOSE'){
		e3.style.display = 'block';
		e2.style.display = 'none';
		}
		else{
		e3.style.display = 'none';
		e2.style.display = 'none';
		}
}

function loadGoogleGeoCode()
   {
   	var address1=document.forms['agentPricing'].elements['pricingControl.address1'].value;
	var city=document.forms['agentPricing'].elements['pricingControl.city'].value;
	var zip=document.forms['agentPricing'].elements['pricingControl.zip'].value;
	var state=document.forms['agentPricing'].elements['pricingControl.state'].value;
	var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
	var country = document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text;
	var address = address1+","+city+" - "+zip+","+state+","+country;
	//alert(address);
	
      // Create new map object
      map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
      geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      geocoder.getLocations(address, addToMap);
   }
   function addToMap(response)
   {
      // Retrieve the object
      place = response.Placemark[0];

      // Retrieve the latitude and longitude
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
		//alert(point);
		document.forms['agentPricing'].elements['pricingControl.latitude'].value = place.Point.coordinates[1];
		document.forms['agentPricing'].elements['pricingControl.longitude'].value = place.Point.coordinates[0];
      // Center the map on this point
      map.setCenter(point, 8);
	
	  addCoordsToMap(point, null); 
	map.setUIToDefault();

      // Create a marker
     // marker = new GMarker(point);

      // Add the marker to map
     	// map.addOverlay(marker);

      // Add address information to marker
      //marker.openInfoWindowHtml(place.address);
   }
   
   function getAddress(){
   		var serviceOrderId= document.forms['agentPricing'].elements['serviceOrderId'].value;
   		var tariffApplicability=document.forms['agentPricing'].elements['pricingControl.tariffApplicability'].value;
	 	var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
		document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text='';
		document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value='';
   		if(tariffApplicability!='')
   		{
   			var url="getAddressForPricing.html?ajax=1&decorator=simple&popup=true&serviceOrderId="+encodeURI(serviceOrderId)+"&tariffApplicability=" + encodeURI(tariffApplicability);
			http2.open("GET", url, true);
	    	http2.onreadystatechange = handleHttpResponseAddress;
	     	http2.send(null);
   		}else{
 			document.forms['agentPricing'].elements['pricingControl.address1'].value='';
			document.forms['agentPricing'].elements['pricingControl.city'].value='';
			document.forms['agentPricing'].elements['pricingControl.state'].value='';
			document.forms['agentPricing'].elements['pricingControl.zip'].value='';
			var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
			document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text='';
			document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value='';
   		
   		}
   }
   
   function handleHttpResponseAddress()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                res = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.trim();
                res = res.trim();
                res = res.split("@");
               		document.forms['agentPricing'].elements['pricingControl.address1'].value=res[0];
					document.forms['agentPricing'].elements['pricingControl.city'].value=res[1];
					document.forms['agentPricing'].elements['pricingControl.state'].value=res[2];
					document.forms['agentPricing'].elements['pricingControl.zip'].value=res[3];
					var dd = document.forms['agentPricing'].elements['pricingControl.country'].selectedIndex;
					document.forms['agentPricing'].elements['pricingControl.country'].options[dd].text=res[4];
					document.forms['agentPricing'].elements['pricingControl.country'].options[dd].value=res[5];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].text = res[4];
					//document.forms['agentPricing'].elements['pricingControl.country'].options[0].value = res[5];
             }
        }
	
	String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();

function putPortName(){ 
	var countryCode=document.forms['agentPricing'].elements['pricingControl.country'].value;
	window.open('searchHaulingPortList.html?portCode=&portName=&country='+countryCode+'&modeType=SEA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=latitude&fld_secondDescription=logtitude&fld_description=description&fld_code=pricingControl.POE','port','resizable=1,width=900,height=450');
} 
function getPricing(){
	var latitude=document.forms['agentPricing'].elements['pricingControl.originLatitude'].value;
	var longitude=document.forms['agentPricing'].elements['pricingControl.originLongitude'].value;
	if(latitude=='' || longitude=='')
	{	
		alert('Map location not confirmed');
		return false;
	}else{
		locatePartners();
		return false;
	}

} 

function locatePartners(){
	// post some data, ignore the response:
	   dojo.xhrPost({
	       form: "agentPricing", // read the url: from the action="" of the <form>
	       timeout: 3000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        	var partnerListDiv = document.getElementById('partnerList');
		   		var partnerTable = document.createElement('TABLE');
		   		partnerTable.setAttribute('class', 'table');
		   		partnerListDiv.appendChild(partnerTable);
		   		
		   		var partnerHeader =  document.createElement('THEAD');
		   		partnerTable.appendChild(partnerHeader);
	        	var partnerHeaderRow = document.createElement('TR');
	        	partnerHeader.appendChild(partnerHeaderRow);

	        	var partnerNameHeader = document.createElement('TH');
	        	partnerHeaderRow.appendChild(partnerNameHeader);
	        	var partnerNameHeaderText = document.createTextNode('Name');
	        	partnerNameHeader.appendChild(partnerNameHeaderText);

	        	var partnerQuoteHeader = document.createElement('TH');
	        	partnerHeaderRow.appendChild(partnerQuoteHeader);
	        	var partnerQuoteHeaderText = document.createTextNode('Quote');
	        	partnerQuoteHeader.appendChild(partnerQuoteHeaderText);
	        	
	           for(var i=0; i < jsonData.partners.length; i++){
	           	   var partnerRow = document.createElement('TR');
	        	   partnerTable.appendChild(partnerRow);
	        	  
	        	   var partnerNameCol = document.createElement('TD');
	        	   partnerRow.appendChild(partnerNameCol);
	        	   var partnerName = document.createTextNode(jsonData.partners[i].name);
	        	   partnerNameCol.appendChild(partnerName);
	        	 
	        	   var partnerQuoteCol = document.createElement('TD');
	        	   partnerRow.appendChild(partnerQuoteCol);
	        	   var partnerQuote = document.createTextNode(jsonData.partners[i].rate);
	        	   partnerQuoteCol.appendChild(partnerQuote);	
	        	   
	        	    var partnerRadioCol = document.createElement('TD');
	        	    partnerRow.appendChild(partnerRadioCol);
	        	    var newElem= document.createElement("input");
	        	    newElem.type = "radio";
	        	   	partnerRadioCol.appendChild(newElem);
	        	    
	        	   console.log("lat: " + jsonData.partners[i].latitude + ", long: " +  jsonData.partners[i].longitude);
	        	   point = new GLatLng(jsonData.partners[i].latitude, jsonData.partners[i].longitude);
	        	   addCoordsToMap(point, jsonData.partners[i]);
	        	   var delme = 0;
	        	}
	       }	       
	   });
   }
   
   function addCoordsToMap(point, partnerDetails){
	      // Create a marker
	      marker = new GMarker(point);
	      // Add the marker to map
	      map.addOverlay(marker);
	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	     var fn = markerClickFn(point, partnerDetails);
	      GEvent.addListener(marker, "click", fn);
  }
   
    function markerClickFn(point, partnerDetails) {
	   return function() {
		   if (!partnerDetails) return;
	       var title = partnerDetails.name;
	       var url = partnerDetails.url;
	       var fileurl = partnerDetails.locationPhotoUrl;
	       var infoHtml = '<div style="width:210px;"><h3>' + title
	         + '</h3><div style="width:200px;height:200px;line-height:200px;margin:2px 0;text-align:center;">'
	         + '<a id="infoimg" href="' + fileurl + '" target="_blank">Loading...</a></div><br/>'
	         + '<h5>Quote: $'+partnerDetails.rate+'</h5>'
	         + '<h6>'+partnerDetails.address1+'</h6>'
	         + '<h6>'+partnerDetails.address2+'</h6><br/>'
	         + '<h6>'+partnerDetails.phone+'</h6>';
	       var img = document.createElement("img");
	       GEvent.addDomListener(img, "load", function() {
	                               if ($("infoimg") == null) {
	                                 return;
	                               }
	                               img = adjustImage(img, 200, 200);
	                               img.style.cssText = "vertical-align:middle;padding:1px;border:1px solid #EAEAEA;";
	                               $("infoimg").innerHTML = "";
	                               $("infoimg").appendChild(img);
	                             });
	       img.src = "UserImage?location=" + fileurl;
	       if(img.readyState == "complete" || img.readyState == "loaded") {
	         img = adjustImage(img, 280, 200);
	         infoHtml += '<img width=' + img.width + ' height=' + img.height
	           + ' style="vertical-align:middle;padding:1px;border:1px solid #aAaAaA"></img>';
	       }
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   
   function adjustImage(img, maxwidth, maxheight) {
	     var wid = img.width;
	     var hei = img.height;
	     var newwid = wid;
	     var newhei = hei;
	     if(wid / maxwidth > hei / maxheight){
	       if(wid > maxwidth){
	         newwid = maxwidth;
	         newhei = parseInt(hei * newwid / wid);
	       }
	     } else {
	       if(hei > maxheight) {
	         newhei = maxheight;
	         newwid = parseInt(wid * newhei / hei);
	       }
	     }
	     var src = img.src;
	     img = document.createElement("img");
	     img.src = src;
	     img.width = newwid;
	     img.height = newhei;
	     return img;
	   }
</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,persist=0,hide=0')
animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('pricing', 'fade=1,persist=0,hide=1')



animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
		var mode=document.forms['agentPricing'].elements['pricingControl.mode'].value;
		var packing=document.forms['agentPricing'].elements['pricingControl.packing'].value;
	  	var e1 = document.getElementById('hid1');
	  	var e2 = document.getElementById('hid2');
	  	var e3=document.getElementById('hid3');
	  	try{
		if(mode == 'Sea' && packing!='BKBLK' ){
		e1.style.display = 'block';
		e2.style.display = 'none';
		e3.style.display = 'block';
		}else if(mode == 'Sea' && packing=='BKBLK'){
		e1.style.display = 'block';
		e2.style.display = 'block';
		e3.style.display = 'none';
		}else if(mode == 'Sea' && packing=='LOOSE'){
		e1.style.display = 'block';
		e2.style.display = 'none';
		e3.style.display = 'block';
		}else{
		e1.style.display = 'none';
		e2.style.display = 'none';
		e3.style.display = 'none';
		}
		}
		catch(e){}
	
		try{
		if(packing == 'BKBLK'){
		e2.style.display = 'block';
		e3.style.display = 'none';
		}else if(packing == 'LOOSE'){
		e3.style.display = 'block';
		e2.style.display = 'none';
		}
		else{
		e3.style.display = 'none';
		e2.style.display = 'none';
		}
		}
		catch(e){}
		try{
		loadGoogleGeoCode();
		}
		catch(e){}
		setOnSelectBasedMethods([]);
		setCalendarFunctionality();
</script>