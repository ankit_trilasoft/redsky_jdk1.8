package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RefSurveyEmailBodyDao;
import com.trilasoft.app.model.RefSurveyEmailBody;

public class RefSurveyEmailBodyDaoHibernate extends GenericDaoHibernate<RefSurveyEmailBody, Long> implements RefSurveyEmailBodyDao{
	public RefSurveyEmailBodyDaoHibernate()
	{
		super(RefSurveyEmailBody.class);
	}

}
