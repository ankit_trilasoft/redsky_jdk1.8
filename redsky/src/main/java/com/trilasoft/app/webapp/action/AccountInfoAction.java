package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;

public class AccountInfoAction extends BaseAction{
	
	private Long id;
	private AccountProfileManager accountProfileManager;
	private AccountProfile accountProfile;
	private PartnerManager partnerManager;
	private RefMasterManager refMasterManager;
	private Partner partner;
	private String sessionCorpID;
	private String countAccountProfileNotes;
	private NotesManager notesManager;
	private static List stageunits;
	private Long maxId;
	
	public AccountInfoAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public AccountProfile getAccountProfile() {
		return accountProfile;
	}
	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}
	
	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private Long id1;
	private List notess;
	private List owner;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(AccountInfoAction.class);

	 
	public String relatedNotesForProfile(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerManager.get(id);
		accountProfile=accountProfileManager.get(id1);
		//System.out.println("\n\n\n\npartnerCode-->"+accountProfile.getPartnerCode());
		notess=accountProfileManager.getRelatedNotesForProfile(sessionCorpID,accountProfile.getPartnerCode());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		//System.out.println("\n\n\n\nnotess-->"+notess);
		return SUCCESS;
	}
	
	/*public String edit(){
		if(id != null ){
			accountProfile = accountProfileManager.get(id);
		}else{
			accountProfile = new AccountProfile();
		}
		return SUCCESS;
	}*/
	
	public String editNewAccountProfile(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerManager.get(id);
		List<AccountProfile> accountProfileList = accountProfileManager.findAccountProfileByPC(partner.getPartnerCode(), sessionCorpID);
		if(accountProfileList == null || accountProfileList.isEmpty()){
			accountProfile = new AccountProfile();
			accountProfile.setPartnerCode(partner.getPartnerCode());
			accountProfile.setCorpID(sessionCorpID);
			accountProfile.setCreatedOn(new Date());
			accountProfile.setUpdatedOn(new Date());
		}else{
			accountProfile = (AccountProfile) accountProfileList.get(0);
		}
		getComboList(sessionCorpID, "a");
		getNotesForIconChange();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private static Map<String, String> ocountry;
	private static Map<String, String> lead;
	private static Map<String,String>currency;
	private static Map<String,String>industry;
	private static Map<String,String>revenue;
	public String getComboList(String corpId, String jobType) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		currency=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		lead = refMasterManager.findByParameter(corpId, "LEAD");
		industry = refMasterManager.findByParameter(corpId, "INDUSTRY");
		revenue = refMasterManager.findByParameter(corpId, "REVENUE");
		owner= accountProfileManager.getAccountOwner(sessionCorpID);
		stageunits = new ArrayList();
		stageunits.add("Prospect");
		stageunits.add("Established");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public Map<String, String> getLead() {
		return lead;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public  List getStageunits() {
		return stageunits;
	}
	public String save(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (accountProfile.getId() == null);
		if(isNew){
			accountProfile.setCreatedOn(new Date());
		}
		accountProfile.setUpdatedOn(new Date());
		accountProfileManager.save(accountProfile);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Account Profile has been added successfully" : "Account Profile has been updated successfully";
		saveMessage(getText(key));
		}
		getComboList(sessionCorpID, "a");
		getNotesForIconChange();
		if (!isNew) { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(accountProfileManager.findMaximumId().get(0).toString());
        	accountProfile = accountProfileManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
        } 
		
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getCountAccountProfileNotes() {
		return countAccountProfileNotes;
	}

	public void setCountAccountProfileNotes(String countAccountProfileNotes) {
		this.countAccountProfileNotes = countAccountProfileNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getNotesForIconChange() {
		List m = notesManager.countAccountProfileNotes(partner.getPartnerCode(), sessionCorpID);
		
		if (m.isEmpty()) {
			countAccountProfileNotes = "0";
		} else {
			countAccountProfileNotes = ((m).get(0)).toString();
		}
		
		return SUCCESS;
	}

	public static Map<String, String> getCurrency() {
		return currency;
	}

	

	public static Map<String, String> getIndustry() {
		return industry;
	}

	

	public static Map<String, String> getRevenue() {
		return revenue;
	}

	

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}
	
	private String gotoPageString;
	private String validateFormNav;
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getGotoPageString() {

	return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	this.gotoPageString = gotoPageString;

	}
	 
	public String saveOnTabChange() throws Exception {
	    //if (option enabled for this company in the system parameters table then call save) {
			validateFormNav = "OK";
	        String s = save();    // else simply navigate to the requested page)
	        return gotoPageString;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public List getNotess() {
		return notess;
	}

	public void setNotess(List notess) {
		this.notess = notess;
	}

	public List getOwner() {
		return owner;
	}

	public void setOwner(List owner) {
		this.owner = owner;
	}

}
