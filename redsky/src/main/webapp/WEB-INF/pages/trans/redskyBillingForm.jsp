
<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title>Redsky Billing</title> 
    	<meta name="heading" content="Redsky Billing"/> 
    	<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    	
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
  <script language="javascript" type="text/javascript">
  
  function validateForm(){  
if(document.forms['redskybillingForm'].elements['redSkyBillingName'].value=='')
	{
	alert("Select Group");
	return false;
	}
}
</script>
  <script language="javascript" type="text/javascript">
  
  function getSelectedField(temp){ 
	  if(temp==4){
		  if(document.forms['redskybillingForm'].elements['procMonth'].value=='')
			{
			alert("Select Billing Month");
			return false;
			}else{ 
	            var mydate=new Date(); 
	            var daym; 
	            var year=mydate.getFullYear()
   			    var y=""+year;
   				if (year < 1000)
  				year+=1900
   				var day="01"
   				var month=document.getElementById('procMonth').value; 
	            var procDateValue = day+"-"+month+"-"+y.substring(2,4); 
	           document.forms['redskybillingForm'].elements['procDate'].value=procDateValue;  
			} 
		  var beginDate = document.getElementById('procDate').value; 
		  location.href='billingByProcedure.html?proc=4&beginDate='+beginDate; 
	  }
}
</script>
 
 </head>
 
 <s:form name="redskybillingForm"   id="redskybillingForm" action="findRedskyBillingList" method="post" validate="true" onsubmit="return validateForm()">
<%-- <c:set var="buttons1">   
    <input type="button" class="cssbutton" style="width:210px;margin:6px;" 
      onclick="getSelectedField(1)"
         value="Update_RedskyBillingDate_UGJP"/>   
</c:set>  --%>
<%-- <c:set var="buttons2">   
    <input type="button" class="cssbutton" style="width:210px;margin:6px;" 
        onclick="getSelectedField(2)"
         value="Update_RedskyBillingDate_UGWW"/>   
</c:set>  --%>

<c:set var="buttons3">   
    <input type="button" class="cssbutton" style="width:210px;margin:6px;" 
   onclick="location.href='<c:url value="/billingByProcedure.html?proc=3"/>'"
         value="Update_RedskyBillingDate"/>   
</c:set>  
<%-- <c:set var="buttons4">   
    <input type="button" class="cssbutton" style="width:210px;margin:6px;" 
      onclick="location.href='<c:url value="/billingByProcedure.html?proc=4"/>'"
         value="Update_RedskyBillingDate_DECA"/>   
</c:set>    --%>

<c:set var="buttons4">   
    <input type="button" class="cssbutton" style="width:210px;margin:6px;" 
        onclick="getSelectedField(4)"
         value="Update_for_load_delivery_dates"/>   
</c:set> 
<div id="Layer1" style="width:100%;"> 
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Redsky Billing</span></a></li>
			  </ul>
			</div></div>
			<div class="spnblk">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">	
<table class="detailTabLabel" cellpadding="0" cellspacing="0" width="90%" border="0">
<tr>
<td width="20"></td>
<td>
<fieldset style="width:450px;height:90px;margin:0px;padding:0px;">
<table style="margin:0px;padding:0px;width:100%;" cellpadding="0" cellspacing="0" border="0">
<%--<tr><td height="7" align="left" colspan="5"></td></tr>
  <tr>
 <td class="listwhitetext" align="right">&nbsp;&nbsp;1st Day Of Billing Month<font color="red" size="1">* &nbsp;</font></td>
<td style="width:15%;"><s:textfield id="procDate2" cssClass="input-text" name="procDateValue2"  cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td>&nbsp;<img id="procDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 <td><c:out value="${buttons1}" escapeXml="false" /></td></tr>
  <tr>
<td colspan="3"></td>
<td><c:out value="${buttons2}" escapeXml="false" /></td>
</tr> --%>
<!-- <tr><td align="left" colspan="5" class="vertlinedata"></td></tr> -->
  <tr>
   <td class="listwhitetext" align="right">Billing Month<font color="red" size="1">* &nbsp;</font></td>
  <td><s:select cssClass="list-menu" name="procMonth" id="procMonth" list="%{monthList}" cssStyle="width:70px" headerKey="" headerValue=""/> </td>
  <s:hidden id="procDate"  name="procDate"  /> 
  <td></td><td><c:out value="${buttons4}" escapeXml="false" /></td></tr>
 <tr><td colspan="3"></td><td><c:out value="${buttons3}" escapeXml="false" /></td></tr>	 
 <tr><td height="7" align="left" colspan="5"></td></tr>							
 </table>
 
 </fieldset>
</td> 
 <td>
 <fieldset style="width:350px;height:90px;margin:0px;padding:0px;">
<table style="padding: 0px; margin: 5px auto;">
<tr>
 <c:if test="${corpIDParam == 'TSFT'}">
 <td align="right" class="listwhitetext" >RedSky&nbsp;Bill&nbsp;Group</td>
 <td> <s:select name='redSkyBillingName' cssStyle="width:190px" cssClass="list-menu" headerKey="" headerValue="" list="%{rskyBillGroup}"/></td>
 </c:if>
 <c:if test="${corpIDParam != 'TSFT'}">
	<td><s:textfield name='corpID'  id="corpID" value="%{corpIDParam}" readonly="true" cssClass="input-text" size="10" maxlength="11"/></td>
 </c:if>
</tr>
 <tr><td height="10"></td></tr>
<tr>
<td></td>
<td>
<s:submit cssClass="cssbutton"  cssStyle="width:100px;" type="button"  key="button.goToBilling" ></s:submit>
</td>
</tr>
</table>
</fieldset>
</td>
</tr> 
 </table>
 </div>
<div class="bottom-header" style="margin-top:45px;"><span></span></div>
</div>
</div> 
 </s:form>
 <script type="text/javascript">
	try{
	yearlist();	
	}
	catch(e){}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
