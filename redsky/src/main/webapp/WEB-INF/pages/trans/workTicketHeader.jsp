<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
<table  cellspacing="1" cellpadding="0" border="0" width="100%">
   <tbody>
    <tr><td align="left" class="listwhitebox">
       <table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%">
         <tbody>                  
             <tr>
               <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">
             <tr>
             <td align="right"  width="41"><fmt:message key="billing.shipper"/></td>
             <td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" size="22"  cssClass="input-textUpper" readonly="true" tabindex="1"/>
             <td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true" tabindex="1"/></td>
            
             <td align="right" width="30"><fmt:message key="billing.originCountry"/></td>
             <td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" cssStyle="margin-left:3px;" size="16" readonly="true" tabindex="1" /></td>
             <td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="4" readonly="true" tabindex="1" /></td>
             <td align="right" width="27"><fmt:message key="billing.Type"/></td>
             <td align="left"><s:textfield name="serviceOrder.job" cssClass="input-textUpper" size="5" readonly="true" tabindex="1" /></td>
             <td align="right" width="60"><fmt:message key="billing.commodity"/></td>
             <td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="5" readonly="true" tabindex="1" /></td>
             <td align="right" width="40"><fmt:message key="billing.routing"/></td>
             <td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="5" readonly="true" tabindex="1" /></td>
           <td align="right" width="30"><fmt:message key='customerFile.status'/></td>
           <td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" size="8" readonly="true" tabindex="1" /> </td>
             </tr>             
             </table>
             </td>
             </tr>
             <tr>
             <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">
             <tr> 
             <td align="right" width="41"><fmt:message key="billing.jobNo"/></td>
             <td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true" tabindex="1"/></td>
             <td align="right" width="26"><fmt:message key="billing.registrationNo"/></td>
             <td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true" tabindex="1"/></td>
                     
             <td align="right" width="31"><fmt:message key="billing.destination"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="16" readonly="true" tabindex="1"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="4" readonly="true" tabindex="1"/></td>
             <c:if test="${serviceOrder.job!='RLO'}">
             <td align="right" width="27"><fmt:message key="billing.mode"/></td>
             <td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="5" readonly="true" tabindex="1"/></td>
             </c:if>
             <td align="right"><fmt:message key="billing.AccName"/></td>
             <td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="42" readonly="true" tabindex="1"/></td>
             </tr>
             </table>
             </td>
             </tr>
            </tbody>
     </table>
     </td></tr>
     <tr>
<td align="left" colspan="18" class="vertlinedata"></td>
</tr>
     <tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="">
<tbody>


                                          <tr>
                                          <c:if test="${serviceOrder.job!='RLO'}">
                                          
                                          <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
                                           <td align="right" class="listwhitetext">Est&nbsp;Wght</td>
                               
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="7" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                         </c:if>
                                         <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeightKilo}" size="7" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeightKilo}" size="7" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="7" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="right" class="listwhitetext">Act&nbsp;Wght</td>
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="7" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeightKilo}" size="7" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeightKilo}" size="7" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="7" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
                                           </configByCorp:fieldVisibility>
                                           <configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
                                           <td align="right" class="listwhitetext">Est&nbsp;Vol.</td>
                               				 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicMtr}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicMtr}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicFeet}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="right" class="listwhitetext">Act&nbsp;Vol.</td>
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicMtr}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicMtr}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicFeet}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
                                           </configByCorp:fieldVisibility>
                                           
                                           </c:if>
                                           

                                           <configByCorp:fieldVisibility componentId="customerFile.survey">
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
											<c:if test="${not empty customerFile.survey}">
											<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="Survey" value="${customerFileSurveyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
											</c:if>
											<c:if test="${empty customerFile.survey}">
											<td><s:textfield cssClass="input-textUpper" name="Survey" value="${customerFileSurveyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
											</c:if>
											</configByCorp:fieldVisibility> 
											 <configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
											<c:if test="${not empty trackingStatus.surveyDate}">
											<s:text id="trackingStatusSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.surveyDate"/></s:text>
											<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
											</c:if>
											<c:if test="${empty trackingStatus.surveyDate}">
											<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
											</c:if>
											</configByCorp:fieldVisibility>
											 <c:if test="${serviceOrder.job!='RLO'}">	                                           
                                            <td align="right" class="listwhitetext">Pack&nbsp;Date</td>
                                            
                                           <c:if test="${empty trackingStatus.packA}">
                                           <c:if test="${empty trackingStatus.beginPacking}">
                                           <td><s:textfield cssClass="input-textUpper" name="packingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <c:if test="${not empty trackingStatus.beginPacking}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginPacking"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>  
                                                           </c:if>
                                           <c:if test="${not empty trackingStatus.packA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                            <td align="right" class="listwhitetext">Load&nbsp;Date</td>
                                           <c:if test="${empty trackingStatus.loadA}">
                                           <c:if test="${empty trackingStatus.beginLoad}">
                                           <td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${not empty trackingStatus.beginLoad}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>                                             </c:if>
                                           <c:if test="${not empty trackingStatus.loadA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           
                                           <td align="right" class="listwhitetext">Delivery</td>
                                           <c:if test="${empty trackingStatus.deliveryA}">
                                           <c:if test="${empty trackingStatus.deliveryShipper}">
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${not empty trackingStatus.deliveryShipper}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>                                             </c:if>
                                           <c:if test="${not empty trackingStatus.deliveryA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                          
												</c:if>										
											
                                           
                                           <td align="right" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
                                           <td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="30"  maxlength="250" readonly="true" tabindex="1"/></td>
                                          <configByCorp:fieldVisibility componentId="component.field.serviceOrder.coordinator">
								           <td align="right" class="listwhitetext"><fmt:message key="serviceOrder.coordinator"/></td>
							               <td align="left"><s:textfield name="serviceOrder.coordinator" cssClass="input-textUpper" size="18" readonly="true" tabindex="1"/></td>
							              </configByCorp:fieldVisibility>
                                       </tr>
                                           </tbody>
                             </table>
                             </td>
                            </tr>
                            <tr>
                              <td align="left" colspan="18" class="vertlinedata"></td>
                             </tr>
   </tbody>
</table>
</div>
<div class="bottom-header" style="!margin-top:50px;margin-top:31px;"><span style="margin-left:50px; padding-top:5px">
  <c:if test="${billing.cod}">
  <div style="position:absolute; width:95%; text-align:center;padding-top:3px; font-size:14px; font-family:Tahoma,Calibri,Verdana,Geneva,sans-serif; font-weight:bold; color:#fe0303 "> COD   <c:out value="${billing.codAmount}"/></div>
        <img id="cod1" src="${pageContext.request.contextPath}/images/cod-blue.png" width="95%" />
      </c:if>
                              
                           
</span></div>
</div>
</div>


