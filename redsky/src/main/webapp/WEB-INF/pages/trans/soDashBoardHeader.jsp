      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css">
      <link rel="stylesheet" href="styles/newredskycss/dashboard-content.css">
      <link rel="stylesheet" href="styles/newredskycss/tab-info.css">
      <link rel="stylesheet" href="styles/newredskycss/comman.css">
 


<%@ include file="/common/taglibs.jsp"%>   
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<%@page import="java.util.Iterator"%>
 

 
<s:form id="billingForm" name="" action="" method="post" validate="true" onsubmit="">
<div class="portlet card-sec general-info">
                                 <div class="portlet-body">
                                    <div class="row">
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Shipper</label> 
                                        	<c:if test="${serviceOrder.vip}"> 
                                          <img src="images/newredskyimg/vip.png" alt="logo" style="width:24px;">
                                          </c:if> 
                                       </div>
                                       
                                       <div class="col-md-6 pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label value=> ${serviceOrder.firstName } ${serviceOrder.lastName}</label> 
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Origin</label>
                                       </div>
                                       
                                       <div class="col-md-6 pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label class=" eclipse-text" data-toggle="tooltip" data-original-title="${serviceOrder.originCity}  ${serviceOrder.originCountryCode}">${serviceOrder.originCity}  ${serviceOrder.originCountryCode}</label>
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Destination</label>
                                       </div>
                                       
                                       <div class="col-md-6 pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label  class=" eclipse-text" data-toggle="tooltip" data-original-title="${serviceOrder.destinationCity} ${serviceOrder.destinationCountryCode}">${serviceOrder.destinationCity} ${serviceOrder.destinationCountryCode}</label>
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading" >Billing Party</label>
                                       </div>
                                       
                                       <div class="col-md-6 pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label class=" eclipse-text" data-toggle="tooltip" data-original-title="${billing.billToName}">${billing.billToName}</label>
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Account Name</label>
                                       </div>
                                       
                                       <div class="col-md-6  pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label class="eclipse-text" data-toggle="tooltip" data-original-title="${customerFile.accountName}">${customerFile.accountName}</label>
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading"> Booking Agent</label>
                                       </div>
                                       
                                       <div class=" pl-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                          <label class="eclipse-text" data-toggle="tooltip" data-original-title="${serviceOrder.bookingAgentName }">${serviceOrder.bookingAgentName }</label>
                                       </div>
                                       
                                    </div>
                            <div class="row">
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Initial Contact</label>
                                       </div>
                                       <div class="text-left   pl-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                          <label><fmt:formatDate pattern="dd-MMM-yyyy" value="${customerFile.initialContactDate}" timeZone="IST"/></label>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">S/O#</label>
                                       </div>
                                       <div class=" pl-0 col-md-6 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.sequenceNumber}-${serviceOrder.ship}</label>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading"> Type</label>
                                       </div>
                                       <div class=" pl-0 col-md-6 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.job }</label>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Company Division</label>
                                       </div>
                                       <div class=" pl-0 col-md-6 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.companyDivision}</label>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Contract</label>
                                       </div>
                                       <div class=" pl-0 col-md-6 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label class="eclipse-text" data-toggle="tooltip" data-original-title="${billing.contract}">${billing.contract}</label>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Coordinator</label>
                                       </div>
                                       <div class="col-md-1 pl-0 cus-pl-15 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.coordinator}</label>
                                       </div>
                                    </div> 
                           <div class="row"> 
                                      <c:if test="${serviceOrder.job!='RLO'}">
                                       <div class=" col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Commodity</label>
                                       </div>
                                       
                                       <div class=" pl-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.commodity}</label>
                                       </div>
                                       
                                       </c:if>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading"> Mode</label>
                                       </div>
                                       
                                       <div class=" pl-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.mode}</label>
                                       </div>
                                       
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Routing</label>
                                       </div>
                                       
                                       <div class=" pl-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                          <label>${serviceOrder.routing}</label>
                                       </div>
                                        <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading"> Actual Wgt/Vol</label>
                                          </div>
                                          
                                      <div class="col-md-6 col-lg-9 col-xl-1 col-sm-6 pl-0 cus-pl-15">
                                              <label class=" eclipse-text" >
                                               <c:if test="${serviceOrder.job!='RLO'}">
                                          <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">

                                              <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                             <c:if test="${!(miscellaneous.actualGrossWeight == '0.00' ||  miscellaneous.actualGrossWeight == '0' ||miscellaneous.actualGrossWeight == '' || miscellaneous.actualGrossWeight == null)}"> 
                                            <label>${miscellaneous.actualGrossWeight}</label>
                                            <label >${miscellaneous.unit1}</label>
                                            </c:if>
                                          </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                          <c:if test="${!(miscellaneous.actualGrossWeightKilo == '0.00' ||  miscellaneous.actualGrossWeightKilo == '0' ||miscellaneous.actualGrossWeightKilo == '' || miscellaneous.actualGrossWeightKilo == null)}"> 
                                         <label>${miscellaneous.actualGrossWeightKilo}</label>
                                        <label >${miscellaneous.unit1}</label>
                                         </c:if>
                                          </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                               <c:if test="${!(miscellaneous.actualNetWeightKilo == '0.00' ||  miscellaneous.actualNetWeightKilo == '0' ||miscellaneous.actualNetWeightKilo == '' || miscellaneous.actualNetWeightKilo == null)}"> 
                                            <label>${miscellaneous.actualNetWeightKilo}</label>
                                           <label >${miscellaneous.unit1}</label>
                                            </c:if>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                            <c:if test="${!(miscellaneous.actualNetWeight == '0.00' ||  miscellaneous.actualNetWeight == '0' ||miscellaneous.actualNetWeight == '' || miscellaneous.actualNetWeight == null)}"> 
                                                <label>${miscellaneous.actualNetWeight}</label>
                                             <label >${miscellaneous.unit1}</label>
                                                </c:if>
                                           </c:if>
                                           </configByCorp:fieldVisibility>
                                           <configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'>
                                                  <c:if test="${!(miscellaneous.actualCubicFeet == '0.00' ||  miscellaneous.actualCubicFeet == '0' ||miscellaneous.actualCubicFeet == '' || miscellaneous.actualCubicFeet == null)}">  
                                                    <label data-toggle="tooltip" data-original-title=" ${miscellaneous.unit2}">/${miscellaneous.actualCubicFeet}</label>
                                                    <label >${miscellaneous.unit2}</label>
                                                    </c:if>
                                         </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                            <c:if test="${!(miscellaneous.actualCubicMtr == '0.00' ||  miscellaneous.actualCubicMtr == '0' ||miscellaneous.actualCubicMtr == '' || miscellaneous.actualCubicMtr == null)}"> 
                                                   <label data-toggle="tooltip" data-original-title=" ${miscellaneous.unit2}">/${miscellaneous.actualCubicMtr}</label>
                                                   <label >${miscellaneous.unit2}</label>
                                                   </c:if>
                                        </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                                    <c:if test="${!(miscellaneous.netActualCubicMtr == '0.00' ||  miscellaneous.netActualCubicMtr == '0' ||miscellaneous.netActualCubicMtr == '' || miscellaneous.netActualCubicMtr == null)}"> 
                                                     <label data-toggle="tooltip" data-original-title=" ${miscellaneous.unit2}">/${miscellaneous.netActualCubicMtr}</label>
                                                     <label >${miscellaneous.unit2}</label>
                                                     </c:if>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                               <c:if test="${!(miscellaneous.netActualCubicFeet == '0.00' ||  miscellaneous.netActualCubicFeet == '0' ||miscellaneous.netActualCubicFeet == '' || miscellaneous.netActualCubicFeet == null)}"> 
                                                    <label data-toggle="tooltip" data-original-title=" ${miscellaneous.unit2}">/${miscellaneous.netActualCubicFeet}</label>
                                                    <label >${miscellaneous.unit2}</label>
                                                    </c:if>
                                        </c:if>
                                           </configByCorp:fieldVisibility>
                                           </c:if>
                                           
                                         </label>
                                            </div>
                                            
                                            
                                             <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Status</label>
                                          
                                          <!-- staus icon -->
                                            <c:if test="${serviceOrder.status=='NEW'}">
                               <span class="status-info Violet">${serviceOrder.status}</span>         
							</c:if>
							<c:if test="${serviceOrder.status=='TRNS'}">
							  <span class="status-info orange">${serviceOrder.status}</span>        
							</c:if>
							<c:if test="${serviceOrder.status=='CLSD'}">
							  <span class="status-info green">${serviceOrder.status}</span>            
							</c:if>
							<c:if test="${serviceOrder.status=='DLVR'}">
							  <span class="status-info light-blue">${serviceOrder.status}</span>           
							</c:if>
							<c:if test="${serviceOrder.status=='DSRV'}">
                                <span class="status-info sky-blue">${serviceOrder.status}</span>       
							</c:if>
							<c:if test="${serviceOrder.status=='OSRV'}">
				              <span class="status-info light-green">${serviceOrder.status}</span>          
							</c:if>
							<c:if test="${serviceOrder.status=='REOP'}">
					             <span class="status-info Violet">${serviceOrder.status}</span>          
							</c:if>
							<c:if test="${serviceOrder.status=='2CLS'}">
					             <span class="status-info light-blue">${serviceOrder.status}</span>          
							</c:if>
							<c:if test="${serviceOrder.status=='CLMS'}">
					             <span class="status-info  light-orange ">${serviceOrder.status}</span>            
							</c:if>
							<c:if test="${serviceOrder.status=='CNCL' || serviceOrder.status=='TCNL'}">
				               <span class="status-info red-mint">${serviceOrder.status}</span>        
							</c:if>
							<c:if test="${serviceOrder.status=='HOLD'}">
		                      <span class="status-info lavender">${serviceOrder.status}</span>       
							</c:if>
							<c:if test="${serviceOrder.status=='DSIT'}">
                              <span class="status-info yellow">${serviceOrder.status}</span> 
							</c:if>
							<c:if test="${serviceOrder.status=='STORG'}">
                              <span class="status-info lightpurple">${serviceOrder.status}</span>          
							</c:if>
							<c:if test="${serviceOrder.status=='STG'}">
                             <span class="status-info lightpurple">${serviceOrder.status}</span>          
							</c:if>
							<c:if test="${serviceOrder.status=='OSIT'}">
                           <span class="status-info yellow">${serviceOrder.status}</span>        
							</c:if>
							<c:if test="${serviceOrder.status=='CANCEL'}">
                               <span class="status-info red-mint">${serviceOrder.status}</span>        
							</c:if>
							<c:if test="${serviceOrder.status=='DWNLD'}">
                               <span class="status-info Violet">${serviceOrder.status}</span> 
							</c:if>
                                          <!-- end status-->
                                       </div>
                                       <div class=" pl-0 pr-0 cus-pl-15 col-md-6 col-lg-9 col-xl-1 col-sm-6">
                                        
                             <c:if test="${serviceOrder.status=='NEW'}">
                                <label><%-- <span class="status-info Violet">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>         
							</c:if>
							<c:if test="${serviceOrder.status=='TRNS'}">
							  <label><%-- <span class="status-info yellow-soft">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>         
							</c:if>
							<c:if test="${serviceOrder.status=='CLSD'}">
							   <label><%-- <span class="status-info yellow-casablanca">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>            
							</c:if>
							<c:if test="${serviceOrder.status=='DLVR'}">
							   <label><%-- <span class="status-info green">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>            
							</c:if>
							<c:if test="${serviceOrder.status=='DSRV'}">
                                <label><%-- <span class="status-info lightblue">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>                 
							</c:if>
							<c:if test="${serviceOrder.status=='OSRV'}">
				               <label><%-- <span class="status-info gray">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>            
							</c:if>
							<c:if test="${serviceOrder.status=='REOP'}">
					             <label><%-- <span class="status-info Violet">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>           
							</c:if>
							<c:if test="${serviceOrder.status=='2CLS'}">
					             <label><%-- <span class="status-info light-blue">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>           
							</c:if>
							<c:if test="${serviceOrder.status=='CLMS'}">
					              <label><%-- <span class="status-info  blue-dark  ">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>             
							</c:if>
							<c:if test="${serviceOrder.status=='CNCL'}">
				               <label><%-- <span class="status-info red-mint">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>         
							</c:if>
							<c:if test="${serviceOrder.status=='HOLD'}">
		                       <label><%-- <span class="status-info lavender">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>           
							</c:if>
							<c:if test="${serviceOrder.status=='DSIT'}">
                               <label><%-- <span class="status-info darkgreen">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>         
							</c:if>
							<c:if test="${serviceOrder.status=='STORG'}">
                               <label><%-- <span class="status-info lightpurple">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>          
							</c:if>
							<c:if test="${serviceOrder.status=='STG'}">
                              <label><%-- <span class="status-info lightpurple">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>           
							</c:if>
							<c:if test="${serviceOrder.status=='OSIT'}">
                            <label><%-- <span class="status-info yellow-gold">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>          
							</c:if>
							<c:if test="${serviceOrder.status=='CANCEL'}">
                                <label><%-- <span class="status-info red-mint">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>          
							</c:if>
							<c:if test="${serviceOrder.status=='DWNLD'}">
                               <label><<%-- span class="status-info Violet">${serviceOrder.status}</span> --%><fmt:formatDate pattern="dd-MMM-yyyy" value="${serviceOrder.statusDate}" timeZone="IST"/></label>    
							</c:if>
                                       </div>
                                       <div class="col-md-6 col-lg-3 col-xl-1 col-sm-6">
                                          <label class="label-heading">Registration No</label>
                                       </div>
                                       <div class="col-md-6 col-lg-9 col-xl-1 pl-0 cus-pl-15">
                                          <label>${serviceOrder.registrationNumber} </label>
                                       </div> 
                                           
                                           
                                           
                                          </div>  <!-- last row end div -->
                                          
                                          
                                      
                                      <!--testing -->
                                       
                      
                                       
                                        <!--testing -->
                                    </div>
                               </div> 




</s:form>




<!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- Popper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      
      
     
      