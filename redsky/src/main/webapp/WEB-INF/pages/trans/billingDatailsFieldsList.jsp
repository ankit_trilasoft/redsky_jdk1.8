<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Billing Details</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right"  style="width:30px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  

	
			<c:choose>
                     <c:when test="${showBillingType=='EST'  }"> 
                       <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.estCurrency}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.estValueDate, 0, 10) }<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.estExchangeRate}<br>
                       <b>Billing&nbsp;Rate&nbsp;:</b>${accountLineForBilling.estLocalRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.estLocalAmount}<br>
                     </c:when> 
                     <c:when test="${showBillingType=='ESTSELL'  }"> 
                       <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.estSellCurrency}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.estSellValueDate, 0, 10)}<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.estSellExchangeRate}<br>
                       <b>Billing&nbsp;Rate&nbsp;:</b>${accountLineForBilling.estSellLocalRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.estSellLocalAmount}<br>
                     </c:when> 
                     <c:when test="${showBillingType=='REV'  }"> 
                       <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.revisionCurrency}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.revisionValueDate, 0, 10)}<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.revisionExchangeRate}<br>
                       <b>Billing&nbsp;Rate&nbsp;:</b>${accountLineForBilling.revisionLocalRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.revisionLocalAmount}<br>
                     </c:when> 
                     <c:when test="${showBillingType=='REVSELL'  }"> 
                        <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.revisionSellCurrency}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.revisionSellValueDate, 0, 10)}<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.revisionSellExchangeRate}<br>
                       <b>Billing&nbsp;Rate&nbsp;:</b>${accountLineForBilling.revisionSellLocalRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.revisionSellLocalAmount}<br>
                     </c:when> 
                     <c:when test="${showBillingType=='REC'  }"> 
                        <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.recRateCurrency}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.racValueDate, 0, 10)}<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.recRateExchange}<br>
                       <b>Billing&nbsp;Rate&nbsp;:</b>${accountLineForBilling.recCurrencyRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.actualRevenueForeign}<br>
                     </c:when> 
                     <c:when test="${showBillingType=='PAY'  }"> 
                       <b><c:if test="${contractType}">Billing&nbsp;</c:if>Currency&nbsp;:</b>${accountLineForBilling.country}<br>
                       <b>Value&nbsp;Date&nbsp;:</b>${fn:substring(accountLineForBilling.valueDate, 0, 10) }<br>
                       <b>Exchange&nbsp;:</b>${accountLineForBilling.exchangeRate}<br>
                       <b>Billing&nbsp;Amount&nbsp;:</b>${accountLineForBilling.localAmount}<br>
                     </c:when> 
                     
                     <c:otherwise>
                     </c:otherwise>
                     </c:choose> 
	   
