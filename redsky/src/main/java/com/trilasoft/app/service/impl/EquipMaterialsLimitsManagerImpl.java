package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.EquipMaterialsLimitsDao;
import com.trilasoft.app.dao.OperationsResourceLimitsDao;
import com.trilasoft.app.model.EquipMaterialsLimits;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.service.EquipMaterialsLimitsManager;
import com.trilasoft.app.service.OperationsResourceLimitsManager;

public class EquipMaterialsLimitsManagerImpl extends GenericManagerImpl<EquipMaterialsLimits, Long> implements EquipMaterialsLimitsManager{
	EquipMaterialsLimitsDao equipMaterialsLimitsDao;
	public EquipMaterialsLimitsManagerImpl(EquipMaterialsLimitsDao equipMaterialsLimitsDao) {
		super(equipMaterialsLimitsDao);
		this.equipMaterialsLimitsDao = equipMaterialsLimitsDao;
	}
	public List getEquipMaterialLimitList(String branch,String div,String cat){
		return equipMaterialsLimitsDao.getEquipMaterialLimitList(branch,div,cat);
	}
	public void saveEquipMaterialObject(EquipMaterialsLimits obj){
		equipMaterialsLimitsDao.saveEquipMaterialObject(obj);
	}
  public EquipMaterialsLimits getByDesc(String cat,String corpid){
		return equipMaterialsLimitsDao.getByDesc(cat,corpid);
  }
  public void updateEquipMaterialObject(EquipMaterialsLimits equipMaterialsLimits,String cngres,String cngwhouse){
	  equipMaterialsLimitsDao.updateEquipMaterialObject(equipMaterialsLimits,cngres,cngwhouse);
  }
  public Map<String,String> getCompanyDivisionMap(String corpid){
	return equipMaterialsLimitsDao.getCompanyDivisionMap(corpid); 
  }

}
