ALTER TABLE `redsky`.`resourcegrid` DROP INDEX `Index_2`;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `utsiNetworkGroup` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `agentNetworkGroup` BIT(1) DEFAULT b'0';

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values('TSFT','Repatriation','Repatriation',20,'SECTIONNAME','en',now(),'subrat',now(),'subrat'),('TSFT','Car','Car',20,'SECTIONNAME','en',now(),'subrat',now(),'subrat'),('TSFT','Taxfree','Taxfree',20,'SECTIONNAME','en',now(),'subrat',now(),'subrat');

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values('TSFT','Repatriation','Terugkeer',20,'SECTIONNAME','nl',now(),'subrat',now(),'subrat'),('TSFT','Car','Auto',20,'SECTIONNAME','nl',now(),'subrat',now(),'subrat'),('TSFT','Taxfree','Taxfree',20,'SECTIONNAME','nl',now(),'subrat',now(),'subrat');