package com.trilasoft.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RateObject{
	public String terms;
	public String privacy;
	public String from;
	public BigDecimal amount;
	public String timestamp;
	public List<RateData> to= new ArrayList<RateData>();
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
	public String getPrivacy() {
		return privacy;
	}
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public List<RateData> getTo() {
		return to;
	}
	public void setTo(List<RateData> to) {
		this.to = to;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}