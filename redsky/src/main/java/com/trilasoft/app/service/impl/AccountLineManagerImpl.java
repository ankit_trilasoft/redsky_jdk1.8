package com.trilasoft.app.service.impl;
 
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map; 
import java.util.Set;
import java.util.SortedMap; 

import org.appfuse.service.impl.GenericManagerImpl; 

import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.dao.AccountLineDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTO;
import com.trilasoft.app.model.AccountLine;

 
public class AccountLineManagerImpl  extends GenericManagerImpl<AccountLine, Long> implements AccountLineManager {

	 AccountLineDao accountLineDao; 
    
    
    public AccountLineManagerImpl(AccountLineDao accountLineDao) { 
        super(accountLineDao); 
        this.accountLineDao = accountLineDao; 
    } 

    public List checkById(Long id) { 
        return accountLineDao.checkById(id); 
    }
    public List totalVatAmountSAPSG(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
    	return accountLineDao.totalVatAmountSAPSG(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
    }
    public List totalVatAmountSAPSGForUGCN(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
    	return accountLineDao.totalVatAmountSAPSGForUGCN(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
    }
    public int getAccCommissionLines(String sessionCorpID,String shipNumber, Long commId){
    	return accountLineDao.getAccCommissionLines( sessionCorpID,shipNumber,commId);
    }
    public List<AccountLine> findBySequenceNumber(String sequenceNumber) { 
        return accountLineDao.findBySequenceNumber(sequenceNumber); 
    }
    public List<AccountLine> findByShipNumber(String shipNumber,String recInvoiceNumber) { 
        return accountLineDao.findByShipNumber(shipNumber,recInvoiceNumber); 
    }
    public List<AccountLine> findByAccountLines(String shipNumber) { 
        return accountLineDao.findByAccountLines(shipNumber); 
    }
   public int  updateAccountLine(String invoiceNumber,String vendorCode ,String shipNumber )
    {
    	return accountLineDao.updateAccountLine(invoiceNumber,vendorCode,shipNumber);
    }
   /*public int generateInvoceNumber(Long newinvoceNumber,String shipNumber, String recDate,String billtoCode)
   {
	   return accountLineDao.generateInvoceNumber(newinvoceNumber, shipNumber,recDate,billtoCode);
   }*/
   public int accInvoiceExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid){
	   return accountLineDao.accInvoiceExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid);
   }
   public int genInvoiceExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid){
	   return accountLineDao.genInvoiceExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid);
   }
   public List accInvoiceExtract(String batchName,String corpid,String recPostingDate){
	   return accountLineDao.accInvoiceExtract(batchName, corpid, recPostingDate);
   }
   public List getAccInvoicePostDataList(String sessionCorpID){
	   return accountLineDao.getAccInvoicePostDataList(sessionCorpID);
   }
   public Long findInvoiceNumber(String corpId)
   {
   	return accountLineDao.findInvoiceNumber(corpId);
   }
   public List gpTotalRecivedAmount(String sameInvoice, String postDate,	String sessionCorpID){
	   return accountLineDao.gpTotalRecivedAmount(sameInvoice, postDate, sessionCorpID);
   }
   public List gpTotalRecivedAmountReg(String sameInvoice, String postDate,	String sessionCorpID, String invoiceFileName){
	   return accountLineDao.gpTotalRecivedAmountReg(sameInvoice, postDate, sessionCorpID,  invoiceFileName);
   }
   public List getInvoicePostDataListForICMG(String sessionCorpID){
	   return accountLineDao.getInvoicePostDataListForICMG(sessionCorpID);
   }
   public int getGPPaymentSentLineCountReg(String sameInvoice, String postDate,	String sessionCorpID, String invoiceFileName){
	   return accountLineDao.getGPPaymentSentLineCountReg(sameInvoice, postDate, sessionCorpID, invoiceFileName);
   } 
   public List getRecivedAccListReg(String sameInvoice, String postDate,String sessionCorpID,String invFileName){
	   return accountLineDao.getRecivedAccListReg(sameInvoice, postDate, sessionCorpID, invFileName);
   }
   public int getNumberOfAccLineReg(String invNum, String recPostDate,String corpID,String invFileName){
	   return accountLineDao.getNumberOfAccLineReg(invNum, recPostDate, corpID, invFileName);
   }
   public List getVatCodeGrpAmt(String invNum, String shipNum,	String sessionCorpID,String invoiceCompanyCode){
	   return accountLineDao.getVatCodeGrpAmt(invNum, shipNum, sessionCorpID, invoiceCompanyCode);
   }
   public List getICMGPayablePostDataList(String corpId){
	   return accountLineDao.getICMGPayablePostDataList(corpId);
   }
   public int getGPPaymentSentLineCount(String sameInvoice, String postDate,	String sessionCorpID){
	   return accountLineDao.getGPPaymentSentLineCount(sameInvoice, postDate, sessionCorpID);
   }
   public List getRecivedAccList(String sameInvoice, String postDate,String sessionCorpID){
	   return accountLineDao.getRecivedAccList(sameInvoice, postDate, sessionCorpID);
   }
   public int updatepaymentSentDate(List recivedAmtIdList,	String sessionCorpID){
	   return accountLineDao.updatepaymentSentDate(recivedAmtIdList, sessionCorpID);
   }
  
   public List getIDForPaymentSentDate(String recPostingDate, String corpid ){
	   return accountLineDao.getIDForPaymentSentDate(recPostingDate, corpid);
   }
   public int updatepaymentSentDateAndRecXfer(List newPayList,	String sessionCorpID, String btachName, String recXferUser){
	   return accountLineDao.updatepaymentSentDateAndRecXfer(newPayList, sessionCorpID, btachName, recXferUser);
   }
   public List getTotalOfInvoice(String invNo, String sessionCorpID,String invFor){
	   return accountLineDao.getTotalOfInvoice(invNo, sessionCorpID, invFor);
   }
   public List ICMGInvoiceExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate){
	   return accountLineDao.ICMGInvoiceExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
   }
   public int invoiceICMGExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid,String invoiceCompanyCode){
	   return accountLineDao.invoiceICMGExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, invoiceCompanyCode);
   }
   public int getNumberOfAccLine(String invNum, String recPostDate,String corpID){
	   return accountLineDao.getNumberOfAccLine(invNum, recPostDate, corpID);
   }
   public List findICMGPayableExtractSageLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense){
	   return accountLineDao.findICMGPayableExtractSageLevel(payCompanyCode, corpId, payPostingDate, positiveExpense);
   }
   public List getT20VisionList(String sessionCorpID){
	   return accountLineDao.getT20VisionList(sessionCorpID);
   }
	public List getPayVatCodeGrpAmt(String invNum, String shipNum,	String sessionCorpID){
		return accountLineDao.getPayVatCodeGrpAmt(invNum, shipNum, sessionCorpID);
	}
	
	public List getPayGLCodeGrpAmt(String invNum, String shipNum,String sessionCorpID){
		return accountLineDao.getPayGLCodeGrpAmt(invNum, shipNum, sessionCorpID);
	}
   public int sageICMGPayableinvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid, String payCompanyCode){
	   return accountLineDao.sageICMGPayableinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid, payCompanyCode);
   }
   public List getAliasNameFromPartnerCode(String pCode,String sessionCorpID){
	   return accountLineDao.getAliasNameFromPartnerCode(pCode, sessionCorpID);
   }
   public List getGLCodeGrpAmt(String invNum, String shipNum,String sessionCorpID,String invoiceCompanyCode){
	   return accountLineDao.getGLCodeGrpAmt(invNum, shipNum, sessionCorpID, invoiceCompanyCode);
   }
   public List getT20VisionLineList(String sessionCorpID, String vCode, String shipNumber){
	   return accountLineDao.getT20VisionLineList(sessionCorpID, vCode,shipNumber);
   }
   public List getPayAccListForTally(String invoiceCompanyCode,String sessionCorpID, String postDate){
	   return accountLineDao.getPayAccListForTally(invoiceCompanyCode, sessionCorpID, postDate);
   }
   public List<AccountLine> findOurInvoice(){
	   return accountLineDao.findOurInvoice();
   }
   public List<AccountLine> findVendorInvoice(){
	   return accountLineDao.findVendorInvoice();
   }
   public List findMaxId() {
		
		return accountLineDao.findMaxId();
	}
	public String getChargeCodeDesc(String chargeCode, String contract,	String sessionCorpID){
		return accountLineDao.getChargeCodeDesc(chargeCode, contract, sessionCorpID);
	}
   public List<AccountLine> vendorName(String partnerCode,String corpid, String pType, Boolean cmmDmmFlag)
   {
	   return accountLineDao.vendorName(partnerCode,corpid, pType, cmmDmmFlag) ;
   }
   public List gpPaymentSentList(String batchName,String corpid,String recPostingDate){
	   return accountLineDao.gpPaymentSentList(batchName, corpid, recPostingDate);
   }
   public String vendorCodeAndName(String partnerCode,String corpid){
	   return accountLineDao.vendorCodeAndName(partnerCode,corpid);
   }
   public List countNoOfActgCode(String partnerCode,String corpid){		
	   return accountLineDao.countNoOfActgCode(partnerCode, corpid);
   }
   public List getAccRefNumber(String sessionCorpID, String pType,String pCode,String extractType){
	   return accountLineDao.getAccRefNumber(sessionCorpID, pType, pCode,extractType);
   }
   public Long getDriverReversalLine(String category, String shipNumber,Long id, String sessionCorpID){
	   return accountLineDao.getDriverReversalLine(category, shipNumber, id, sessionCorpID);
   }
   public List getVendorDataList(String sessionCorpID){
	   return accountLineDao.getVendorDataList(sessionCorpID);
   }
   public List getUtsiMemberViaPartnerCode(String pCode,String sessionCorpID){
	   return accountLineDao.getUtsiMemberViaPartnerCode(pCode, sessionCorpID);
   }
   public List<AccountLine> accountName(String partnerCode,Boolean cmmDmmFlag,Boolean accountSearchValidation)
   {
	   return accountLineDao.accountName(partnerCode, cmmDmmFlag,accountSearchValidation) ;
   }
   public List getEntitleSum(String shipNumber){
	   return accountLineDao.getEntitleSum(shipNumber) ;
   }
   public List getEstimateRevSum(String shipNumber){
	   return accountLineDao.getEstimateRevSum(shipNumber) ;
   }
   public List getInvoiceAccListForTally(String invoiceCompanyCode,String sessionCorpID, String recPostDate){
	   return accountLineDao.getInvoiceAccListForTally(invoiceCompanyCode, sessionCorpID, recPostDate);
   }
   public List getEstimateExpSum(String shipNumber){
	   return accountLineDao.getEstimateExpSum(shipNumber) ;
   }
   public List getRevisionRevSum(String shipNumber){
	   return accountLineDao.getRevisionRevSum(shipNumber) ;
   }
   public List getRevisionExpSum(String shipNumber){
	   return accountLineDao.getRevisionExpSum(shipNumber) ;
   }
   public int tallyInvoiceExtractUpdate(String batchName,String accIdList,String recXferUser,String recPostingDate,String corpid){
	   return accountLineDao.tallyInvoiceExtractUpdate(batchName, accIdList, recXferUser, recPostingDate, corpid);
   }
   public List getActualExpSum(String shipNumber){
	   return accountLineDao.getActualExpSum(shipNumber) ;
   }
   public List getActualRevSum(String shipNumber){
	   return accountLineDao.getActualRevSum(shipNumber) ; 
   }
   public List  getAccountLines(String shipNumber,String invoiceBillToCode, String sessionCorpID){
	   return accountLineDao.getAccountLines(shipNumber,invoiceBillToCode,   sessionCorpID);
   }
   public List<AccountLine> getAccountLinesNew(String shipNumber,String invoiceBillToCode){
	   return accountLineDao.getAccountLinesNew(shipNumber, invoiceBillToCode);
   }
   public List invoiceExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate)
   {
	   return accountLineDao.invoiceExtract( batchName,invoiceCompanyCode,corpid,recPostingDate);
   }
   public List invoiceMergeExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate){
	   return accountLineDao.invoiceMergeExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
   }
   public List invoiceGeneralExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate)
   {
	   return accountLineDao.invoiceGeneralExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
   }
   
   public List getCompanyNameByCompanyDivision(String invoiceCompanyCode,String sessionCorpID){
	   return accountLineDao.getCompanyNameByCompanyDivision(invoiceCompanyCode, sessionCorpID);
   }
   
   public List invoiceMamutExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate)
   {
	   return accountLineDao.invoiceMamutExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
   }
   public List findEuvateDesc(String sessionCorpID, String parameter,String vatCode){
	   return accountLineDao.findEuvateDesc(sessionCorpID, parameter, vatCode);
   }
   public List totalRevenue(String invoiceNumber,String recPostingDate,String corpid)
   {
	   return accountLineDao.totalRevenue(invoiceNumber,recPostingDate,corpid);
   }
   public List getPayableMergeExtract(String payCompanyCode,String corpid,String payPostingDate){
	   return accountLineDao.getPayableMergeExtract(payCompanyCode, corpid, payPostingDate);
   }
   /*public List totalGenRevenue(String invoiceNumber,String recPostingDate,String corpid)
   {
	   return accountLineDao.totalGenRevenue(invoiceNumber,recPostingDate,corpid);
   }*/
   public List findAllInvListServiceTaxAmt(String invoiceCompanyCode,String corpId, String recPostingDates, String invNumber){
	   return accountLineDao.findAllInvListServiceTaxAmt(invoiceCompanyCode, corpId, recPostingDates, invNumber);
   }
   
   public List findAllInvListServiceTaxAmtForStar(String invoiceCompanyCode,String corpId, String recPostingDates, String invNumber){
	   return accountLineDao.findAllInvListServiceTaxAmtForStar(invoiceCompanyCode, corpId, recPostingDates, invNumber);
   }
   
   public List findAllPayInvListGlCode(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
	   return accountLineDao.findAllPayInvListGlCode(invoiceCompanyCode, corpId, postingDates, invNumber, vendorCode);
   }
   
   
   public List findAllInvListGlCode(String invoiceCompanyCode,String corpId, String recPostingDates, String invNumber){
	   return accountLineDao.findAllInvListGlCode(invoiceCompanyCode, corpId, recPostingDates, invNumber);
   }
   public int invoiceExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid)
   {
	   return accountLineDao.invoiceExtractUpdate(batchName,shipInvoiceUpdate,recXferUser,recPostingDate,corpid) ;
   }
   public int invoiceMergeExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid,String comDiv){
	   return accountLineDao.invoiceMergeExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, comDiv);
   }
   public int invoiceMamutExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid)
   {
	   return accountLineDao.invoiceMamutExtractUpdate(batchName,shipInvoiceUpdate,recXferUser,recPostingDate,corpid) ;
   }
   public List searchOurInvoice(String recInvoiceNumber ,String billToCode,String shipNumber,String corpID, String billToName, String companydivisionSetCode)
   {
	   return accountLineDao.searchOurInvoice(recInvoiceNumber, billToCode, shipNumber,corpID, billToName,companydivisionSetCode);
   }
   public List findTotalOfInvoice(String recInvoiceNumber,String shipNumber,String corpID)
   {
	   return accountLineDao.findTotalOfInvoice(recInvoiceNumber, shipNumber, corpID);
   }
   public List searchMilitaryBase(String baseCode ,String description,String terminalState,String SCACCode,String corpID)
   {
	   return accountLineDao.searchMilitaryBase(baseCode, description, terminalState,SCACCode, corpID);
   }
   public List searchVendorInvoice(String invoiceNumber ,String billToCode,String shipNumber,String corpID, String companydivisionSetCode ,Date invoiceDate ){
	   return accountLineDao.searchVendorInvoice(invoiceNumber, billToCode, shipNumber,corpID, companydivisionSetCode ,invoiceDate ) ;
   }
   
   public List<AccountLine> findByCategoryCode(String category, String vendorCode, String shipNumber){
	   return accountLineDao.findByCategoryCode(category, vendorCode, shipNumber);
   }
   
   /*public List getAgentStatus(String shipNumber){
	   return accountLineDao.getAgentStatus(shipNumber);
   }*/
   
   public void updateBillingInsurerDetail(String shipNumber,String sequenceNumber,String vendorCode,String vendorName,BigDecimal stimateQuantity,BigDecimal estimateRate,BigDecimal estimateRevenueAmount){
	   accountLineDao.updateBillingInsurerDetail( shipNumber, sequenceNumber, vendorCode, vendorName, stimateQuantity, estimateRate, estimateRevenueAmount);
   }

public List<AccountLine> getAccountLineList(String shipNumber, String accountLineStatus) {
	
	return accountLineDao.getAccountLineList(shipNumber, accountLineStatus);
}
public Map<String, List> getVanLineAccountCategoryList(String shipNumber, String accountLineStatus) {
	return accountLineDao.getVanLineAccountCategoryList(shipNumber, accountLineStatus);
}
	public List getAgentOriginStatus(Long id){
	   return accountLineDao.getAgentOriginStatus(id);
   }
	 
	public List getAgentDestStatus(Long id){
		return accountLineDao.getAgentDestStatus(id);
	}

	public List<AccountLine> findBillToCodeByShipNumber(String shipNumber, String recInvoiceNumber) {
		
		return accountLineDao.findBillToCodeByShipNumber(shipNumber, recInvoiceNumber);
	}

	public List findActiveWorkTickets(String shipNumber, String corpID) {
		return accountLineDao.findActiveWorkTickets(shipNumber, corpID);
	}

	public void updateReviewStatus(String id,String shipNumber, String corpID, String loginUser, String type) {
		 accountLineDao.updateReviewStatus(id,shipNumber, corpID, loginUser, type);
	}
	
	public int checkForAccountLineStatus(String corpID,String recInvoiceNumber){
		return accountLineDao.checkForAccountLineStatus(corpID,recInvoiceNumber);
	}

	public void updateReviewStatusUncheck(String shipNumber, String corpID, String loginUser, String type) {
		accountLineDao.updateReviewStatusUncheck(shipNumber, corpID, loginUser, type);
		
	}

	public List findCommissionList(String contract, String charge) {
		
		 return accountLineDao.findCommissionList(contract, charge);
	}

	public List findGlTypeList(String contract, String charge) {
		
		return accountLineDao.findGlTypeList(contract, charge);
	}
	
	public List findAuthorizationNo(String shipNumber) {
		
		return accountLineDao.findAuthorizationNo(shipNumber);
	}
	
	public String findExpression(String glType){
		return accountLineDao.findExpression(glType);
	}
	
	public List findDriverAgency(Long sid){
		return accountLineDao.findDriverAgency(sid);
	}
	
	public List findCommissionExpression(String glCode){
		return accountLineDao.findCommissionExpression(glCode);
	}

	public List getdistributedAmountSum(String shipNumber) { 
		return accountLineDao.getdistributedAmountSum(shipNumber);
	}

	public List getAccountLinePost(String shipNumber) {
		
		return accountLineDao.getAccountLinePost(shipNumber);
	}

	public List getAccountInvoicePost(String shipNumber) {
		return accountLineDao.getAccountInvoicePost(shipNumber);
	}

	public List getInvoiceNumberPost(String shipNumber) {
		return accountLineDao.getInvoiceNumberPost(shipNumber);
	}
   
	public int saveInvoiceDate(String shipNumber, String recDate, String recInvoiceNumber) {
		return accountLineDao.saveInvoiceDate(shipNumber, recDate, recInvoiceNumber);
	}

	public List getActualInvoiceList(String shipNumber) {
		return accountLineDao.getActualInvoiceList(shipNumber);	
	}

	public List findFromSysDefault(String userName){
		return accountLineDao.findFromSysDefault(userName);
	}


	/*public int generateInvoceWithAuthNo(Long newinvoceNumber, String shipNumber, String recDate, String billtoCode, String authorization) {
		
		return accountLineDao.generateInvoceWithAuthNo(newinvoceNumber, shipNumber, recDate, billtoCode, authorization);
	}
*/
	public List findMultiAuthorization(String partnerCode,String corpId) {
		
		return accountLineDao.findMultiAuthorization(partnerCode,corpId);
	}

	public int updateAuthorizationNo(String newinvoceNumber, String shipNumber, String authorization) {
		
		return accountLineDao.updateAuthorizationNo(newinvoceNumber, shipNumber, authorization);
	}

	public List findAccountLinePayPost(String shipNumber) {
		
		return accountLineDao.findAccountLinePayPost(shipNumber);
	}

	public List findPayableInvoiceList(String shipNumber) {
		return accountLineDao.findPayableInvoiceList(shipNumber);
	}

	public List findPayableVendorList(String shipNumber) {
		return accountLineDao.findPayableVendorList(shipNumber);
	}

	public int updatePayPostDate(String shipNumber, String payPostDate,String vendorInvoice,String vendorCode,String loginUser, String sessionCorpID, String jobType,String routing, String contract, String companyDivision) {
		return accountLineDao.updatePayPostDate(shipNumber, payPostDate ,vendorInvoice,vendorCode,loginUser, sessionCorpID,   jobType, routing,   contract, companyDivision);	
		
	}

	public List getPayableExtract(String payCompanyCode,String corpid,String payPostingDate) {
		return accountLineDao.getPayableExtract(payCompanyCode,corpid,payPostingDate);
	}
	public List getMamutPayableExtract(String payCompanyCode,String corpid,String payPostingDate) {
		return accountLineDao.getMamutPayableExtract(payCompanyCode,corpid,payPostingDate);
	}
	public List totalExpense() {
		
		return accountLineDao.totalExpense(); 
	}

	public List getPayablelevel1(String payCompanyCode,String corpid, String vendorCode,String vendorInvoice,String payPostingDate) {
		
		return accountLineDao.getPayablelevel1(payCompanyCode,corpid,vendorCode,vendorInvoice,payPostingDate);
	}

	public List getPayablelevel4(String payCompanyCode,String corpid,String vendorCode,String vendorInvoice,String payPostingDate) {
		return accountLineDao.getPayablelevel4(payCompanyCode,corpid,vendorCode,vendorInvoice, payPostingDate);	
	}

	public int payableinvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid) {
		return accountLineDao.payableinvoiceUpdate(batchName,shipnumber,payXferUser,payPostingDate,corpid);
	}
	public int payableMergeinvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid,String compDiv){
		return accountLineDao.payableMergeinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid, compDiv);
	}
	public List totalMergeDistributionAmount(String invoiceNumber,String recPostingDate,String corpid,String companyDivision){
		return accountLineDao.totalMergeDistributionAmount(invoiceNumber, recPostingDate, corpid, companyDivision);
	}
	public List totalMergeRevenue(String invoiceNumber,String recPostingDate,String corpid,String companyDivision){
		return accountLineDao.totalMergeRevenue(invoiceNumber, recPostingDate, corpid, companyDivision);
	}
	public int payableGeneralinvoiceUpdate(String batchName,String shipInvoiceUpdate,String payXferUser,String payPostingDate,String corpid) {
		return accountLineDao.payableGeneralinvoiceUpdate(batchName,shipInvoiceUpdate,payXferUser,payPostingDate,corpid);
	}
	public int payableMamutInvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid) {
		return accountLineDao.payableMamutInvoiceUpdate(batchName,shipnumber,payXferUser,payPostingDate,corpid);
	}
	
	public List findPayableChargeList(String shipNumber) {
		return accountLineDao.findPayableChargeList(shipNumber);
	}

	public List findVenderForPayable(String payCompanyCode,String corpid,String payPostingDate) {
		
		return accountLineDao.findVenderForPayable(payCompanyCode,corpid,payPostingDate);
	}
	
	public List getAccountLinesID(String shipNumber){
		return accountLineDao.getAccountLinesID(shipNumber);
	}
	
	public List getAuthrorizationNumber(String shipNumber) {
		return accountLineDao.getAuthrorizationNumber(shipNumber);
	}

	public List findNumPayPost(String shipNumber) {
		
		return accountLineDao.findNumPayPost(shipNumber);
	}

	public List findPayableGLCodeList(String shipNumber) {
		return accountLineDao.findPayableGLCodeList(shipNumber);
	}
	
	public List findVenderForExpNigative(String payCompanyCode,String corpid,String payPostingDate) {
		return accountLineDao.findVenderForExpNigative(payCompanyCode,corpid,payPostingDate);
	}

	public List paylevel1ForExpNiv(String payCompanyCode,String corpid, String vendorCode,String vendorInvoice,String payPostingDate) {
		
		return accountLineDao.paylevel1ForExpNiv( payCompanyCode, corpid, vendorCode,vendorInvoice,payPostingDate);
	}

	public List paylevel4ForExpNiv(String payCompanyCode,String corpid, String vendorCode,String vendorInvoice,String payPostingDate) {
		
		return accountLineDao.paylevel4ForExpNiv(payCompanyCode,corpid,vendorCode, vendorInvoice,payPostingDate);
	}

	public List findPayableStatusList(String shipNumber,String vendorCode) {
		return accountLineDao.findPayableStatusList(shipNumber,vendorCode);
		
	}
	
	public int updateProcessDate(Date processingDate, String invoiceNo, String uploadBillToCode){
		return accountLineDao.updateProcessDate(processingDate, invoiceNo, uploadBillToCode);
	}

	public void updateAcc(String billToCode,String billToName,Long sid){
		accountLineDao.updateAcc(billToCode, billToName, sid);
	}
	
	public String countInvoice(Long sid){
		return accountLineDao.countInvoice(sid);
	}
	public List findPayVendorList(String shipNumber) {
		return accountLineDao.findPayVendorList(shipNumber);
	}

	public List findPayStatusList(String shipNumber, String vendorCode) {
		return accountLineDao.findPayStatusList(shipNumber, vendorCode);
	}

	public List getReverseInvoiceList(String shipNumber,String corpId) { 
		return accountLineDao.getReverseInvoiceList(shipNumber,corpId);
	}
	public List getCopyInvoiceList(String shipNumber,String corpId){
		return accountLineDao.getCopyInvoiceList(shipNumber, corpId);
	}
	public List findReverseInvoiceId(String shipNumber, String invoiceNo) {
		return accountLineDao.findReverseInvoiceId(shipNumber, invoiceNo);
	}
/*
	public List<AccountLine> findActgCode(String partnerCode,String corpid) {
		return accountLineDao.findActgCode(partnerCode,corpid);
		
	}*/

	public List getdistAmtInvoiceList(String shipNumber) {
		
		return accountLineDao.getdistAmtInvoiceList(shipNumber);
	}

	public List  getDistAmtAccountList(String shipNumber, String invoiceBillToCode, String sessionCorpID) {
		return accountLineDao.getDistAmtAccountList(shipNumber, invoiceBillToCode,sessionCorpID);
		
	}
	public List<AccountLine> getDistAmtAccountListNew(String shipNumber,String invoiceBillToCode){
		return accountLineDao.getDistAmtAccountListNew(shipNumber, invoiceBillToCode);
	}

	/*public int distgenerateInvoceWithAuthNo(Long newinvoceNumber, String shipNumber, String recDate, String billtoCode, String authorization) {
		
		return accountLineDao.distgenerateInvoceWithAuthNo(newinvoceNumber, shipNumber, recDate, billtoCode, authorization);
	}*/

	/*public int distgenerateInvoceNumber(Long newinvoceNumber, String shipNumber, String recDate, String billtoCode) {
		return accountLineDao.distgenerateInvoceNumber(newinvoceNumber, shipNumber, recDate, billtoCode);
		
	}
*/
	public List totalDistributionAmount(String invoiceNumber,String recPostingDate,String corpid) {
		return accountLineDao.totalDistributionAmount(invoiceNumber,recPostingDate,corpid);
	}
	/*public List totalGenDistributionAmount(String invoiceNumber,String recPostingDate,String corpid) {
		return accountLineDao.totalGenDistributionAmount(invoiceNumber,recPostingDate,corpid);
	}*/
	public List getVedorInvoice(String shipNumber,String corpId) {
		return accountLineDao.getVedorInvoice(shipNumber,corpId);
	}

	public int updateReverseAuthNo(String shipNumber, String recInvoiceNumber) {
		return accountLineDao.updateReverseAuthNo(shipNumber, recInvoiceNumber);
		
	}

	public List getVendorInvoiceList(String payCompanyCode,String corpid,String vendorCode,String payPostingDate) {
		return accountLineDao.getVendorInvoiceList(payCompanyCode,corpid,vendorCode,payPostingDate);
		
	}

	public List getVendorInvoiceListForExpNiv(String payCompanyCode,String corpid, String vendorCode,String payPostingDate) {
		return accountLineDao.getVendorInvoiceListForExpNiv(payCompanyCode,corpid, vendorCode,payPostingDate);
		
	}

	public String updateInvoiceNumber(String shipNumber, String recDate, String billtoCode, String authNumber, boolean useDistAmt,String corpId,String loginUser, String creditInvoiceNumber, String accountInterface, String jobtype, boolean soNetworkGroup, String contractTypeValue, boolean accNetworkGroup, String routing, String contract, String bookingAgentShipNumber,String recInvoiceDateToFormat, boolean networkAgent, String payAccDateTo, String companyDivision,String SoInvoiceNumber,String invoicereportparameter) {
		return accountLineDao.updateInvoiceNumber(shipNumber, recDate, billtoCode, authNumber, useDistAmt,corpId,loginUser, creditInvoiceNumber, accountInterface,jobtype,  soNetworkGroup, contractTypeValue , accNetworkGroup, routing,  contract,  bookingAgentShipNumber,recInvoiceDateToFormat,networkAgent, payAccDateTo, companyDivision, SoInvoiceNumber,invoicereportparameter);
	}
	public Long updateInvoiceNumberCollective(String shipNumber, String recDate, String billtoCode, String authNumber, boolean useDistAmt ,String corpId,String loginUser, String creditInvoiceNumber, String accountInterface , String jobtype, boolean soNetworkGroup, String contractTypeValue , boolean accNetworkGroup, String routing, String contract, String bookingAgentShipNumber,Long lastInvoiceNumber1,String recInvoiceDateToFormat,int t, boolean networkAgent, String payAccDateTo, String companyDivision,String companyDivisionList,String pir,String companyDivisionNew,String batchInvoiceNonCMMDMMFlag, boolean creditInvoiceSequence, boolean costElement,Map<String, String> parameterMap,String billingCurrency,String collectiveInvoiceNumber,String invoicereportparameter,String accountLineIdVal){
		return accountLineDao.updateInvoiceNumberCollective(shipNumber, recDate, billtoCode, authNumber, useDistAmt, corpId, loginUser, creditInvoiceNumber, accountInterface, jobtype, soNetworkGroup, contractTypeValue, accNetworkGroup, routing, contract, bookingAgentShipNumber, lastInvoiceNumber1,recInvoiceDateToFormat,t, networkAgent, payAccDateTo,companyDivision,companyDivisionList,pir,companyDivisionNew,batchInvoiceNonCMMDMMFlag,creditInvoiceSequence, costElement,parameterMap,billingCurrency,collectiveInvoiceNumber,invoicereportparameter, accountLineIdVal);
	}
	public List getInvoicePostDataList(String corpId) {
		return accountLineDao.getInvoicePostDataList(corpId);
	}
	public List getInvoiceMergePostDataList(String corpId){
		return accountLineDao.getInvoiceMergePostDataList(corpId);
	}
	public List getgeneralInvoicePostDataList(String corpId) {
		return accountLineDao.getgeneralInvoicePostDataList(corpId);
		
	}

	public List getPaymentApplicationDataList(String corpId,String vanlineJob,String vanLineJobs){
		return accountLineDao.getPaymentApplicationDataList(corpId,vanlineJob,vanLineJobs);
	}
	
	public List getCreditMemoApplicationDataList(String corpId,String vanlineJob,String vanLineJobs){
		return accountLineDao.getCreditMemoApplicationDataList(corpId,vanlineJob,vanLineJobs);
	}
	
	public List getAuthrorizationNumberForWorldBank(String shipNumber) {
		return accountLineDao.getAuthrorizationNumberForWorldBank(shipNumber);
	}


	public List getPayablePostDataList(String corpId) {
		return accountLineDao.getPayablePostDataList(corpId);
	}
	public List getMergePayablePostDataList(String corpId){
		return accountLineDao.getMergePayablePostDataList(corpId);
	}
	public List findPayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense) {
		return accountLineDao.findPayableExtractLevel(payCompanyCode, corpId, payPostingDate,  positiveExpense);
	}
	public List findMergePayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense){
		return accountLineDao.findMergePayableExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense);
	}
	public List findPayableGeneralExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense) {
		return accountLineDao.findPayableGeneralExtractLevel(payCompanyCode, corpId, payPostingDate,  positiveExpense);
	}
	public List findMamutPayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense) {
		return accountLineDao.findMamutPayableExtractLevel(payCompanyCode, corpId, payPostingDate,  positiveExpense);
	}

	public List findInvoiceBillToCode(String invoiceFileName, String corpId) {
		return accountLineDao.findInvoiceBillToCode(invoiceFileName, corpId);
	}

	public int updateAllEstimateDate(String shipNumber, String corpId) {
		return accountLineDao.updateAllEstimateDate(shipNumber, corpId);
	}

	public List findActualSenttoDates(String shipNumber, String corpId) {
		return accountLineDao.findActualSenttoDates(shipNumber, corpId);
	}

	public int updateActualSenttoDates(String shipNumber, String invoiceActualSent, String corpId) {
		return accountLineDao.updateActualSenttoDates(shipNumber, invoiceActualSent, corpId);
	}

	public List getCostingDetailList(String shipNumber, String accountLineStatus) {
		return accountLineDao.getCostingDetailList(shipNumber, accountLineStatus);
	}
	public List getBillingWizardList(String shipNumber, String corpId) {
		return accountLineDao.getBillingWizardList(shipNumber, corpId);
	}

	public List getInvoiceDetailBilling(String shipNumber, String recInvNumBilling, String corpId) {
		return accountLineDao.getInvoiceDetailBilling(shipNumber, recInvNumBilling, corpId);
	}

	public List getAllSumAmountBilling(String shipnumberBilling, String recInvNumberBilling, String corpId) {
		return accountLineDao.getAllSumAmountBilling(shipnumberBilling, recInvNumberBilling, corpId);
	}

	public List getidListBilling(String shipnumberBilling, String recInvNumberBilling, String corpId) {
		return accountLineDao.getidListBilling(shipnumberBilling, recInvNumberBilling, corpId);
	}

	public List getNonInvoiceList(String shipNumber, String corpId) {
		return accountLineDao.getNonInvoiceList(shipNumber, corpId);
	}

	public List findListForInvoice(String shipNumber, String corpId) {
		
		return accountLineDao.findListForInvoice(shipNumber, corpId);
	}

	public int updateInvActSentToDate(String shipNumber, String corpId, String recInvoiceNumber,String sendActualToClientDate, String userName, Long id) { 
		return accountLineDao.updateInvActSentToDate(shipNumber, corpId, recInvoiceNumber, sendActualToClientDate,userName, id);
	}

	public List findmultiCurrency(String corpId) {
		return accountLineDao.findmultiCurrency(corpId);
	}

	public List searchBaseCurrency(String corpId) { 
		return accountLineDao.searchBaseCurrency(corpId);
	}

	public List getbillingCurrency(String shipNumber, String corpId) {
		
		return accountLineDao.getbillingCurrency(shipNumber, corpId);
	}   
	public List<AccountLine> pricingBillingPaybaleName(String partnerCode,String corpid,String jobType){
		return accountLineDao.pricingBillingPaybaleName(partnerCode, corpid,jobType);
	}
	
	/* added by kunal for ticket number: 6006*/
	public List findClaimPerson(String partnerCode, String sessionCorpID){
		return accountLineDao.findClaimPerson(partnerCode, sessionCorpID);
	}
	public List getTotalInvoiceList(String shipNumber, String corpId,String divisionFlag) {
		return accountLineDao.getTotalInvoiceList(shipNumber, corpId,divisionFlag);
	}

	public List getInvoiceCurrencyList(String shipNumber, String corpId,String divisionFlag) { 
		return accountLineDao.getInvoiceCurrencyList(shipNumber, corpId,divisionFlag);
	}

	public List findAssignInvoiceList(String shipNumber, String corpId,String billtoCode, boolean singleCompanyDivisionInvoicing, String companyDivisionForTicket) { 
		return accountLineDao.findAssignInvoiceList(shipNumber, corpId,billtoCode, singleCompanyDivisionInvoicing, companyDivisionForTicket);
	}

	public List getAssignInvoiceList(String shipNumber, String corpId,String billtoCode, boolean singleCompanyDivisionInvoicing, String companyDivisionForTicket) { 
		return accountLineDao.getAssignInvoiceList(shipNumber, corpId, billtoCode, singleCompanyDivisionInvoicing, companyDivisionForTicket);
	}
 
	public int updateAssignInvoiceNumber(String shipNumber, String billtoCode, String userName, String corpId,String assignInvoiceNumber, boolean creditInvoiceFlag, boolean soNetworkGroup, String jobtype, String contractTypeValue, boolean accNetworkGroup, String routing, String contract, String bookingAgentShipNumber, boolean networkAgent, String companyDivision,String invoicereportparameter) {
		return accountLineDao.updateAssignInvoiceNumber(shipNumber, billtoCode, userName, corpId,assignInvoiceNumber,creditInvoiceFlag, soNetworkGroup,  jobtype,  contractTypeValue,   accNetworkGroup,  routing,   contract,  bookingAgentShipNumber,networkAgent, companyDivision,invoicereportparameter);
	}

	public List getTotalVendorInvList(String shipNumber, String corpid) {
		
		return accountLineDao.getTotalVendorInvList(shipNumber, corpid);
	}

	public List findAssignCurrencyInvoiceList(String shipNumber, String corpid, String billtoCode, boolean singleCompanyDivisionInvoicing, String companyDivisionForTicket, boolean soNetworkGroup, boolean accNetworkGroup, String contractTypeValue, boolean collectivePage, String billingCurrencyForCollective) {
		
		return accountLineDao.findAssignCurrencyInvoiceList(shipNumber, corpid, billtoCode, singleCompanyDivisionInvoicing, companyDivisionForTicket, soNetworkGroup,  accNetworkGroup,  contractTypeValue, collectivePage, billingCurrencyForCollective);
	}

	public List getAssignCurrencyInvoiceList(String shipNumber, String corpId, String billtoCode, boolean singleCompanyDivisionInvoicing, String companyDivisionForTicket, boolean soNetworkGroup, boolean accNetworkGroup, String contractTypeValue, boolean collectivePage, String billingCurrencyForCollective) { 
		return accountLineDao.getAssignCurrencyInvoiceList(shipNumber, corpId, billtoCode, singleCompanyDivisionInvoicing, companyDivisionForTicket,  soNetworkGroup,  accNetworkGroup,  contractTypeValue, collectivePage, billingCurrencyForCollective);
	}

	public List getBranchCode(String companyDivision, String corpId) {
		
		return accountLineDao.getBranchCode(companyDivision, corpId);
	}

	public List getNotPaidInvoiceList(String corpId) {
		
		return accountLineDao.getNotPaidInvoiceList(corpId);
	}

	public int updatePaidInvoice(String recInvoiceNumber, String corpId, String userName) { 
		return accountLineDao.updatePaidInvoice(recInvoiceNumber, corpId,userName);
	}

	public List searchNotPaidInvoice(String notPaidInvoiceNumber, String notPaidBillToCode, String notPaidBillToName, String corpId) {
		
		return accountLineDao.searchNotPaidInvoice(notPaidInvoiceNumber, notPaidBillToCode, notPaidBillToName, corpId);
	}
	
	public List getBrokerStatus(Long id){
		return accountLineDao.getBrokerStatus(id);
	}

	public List findInvoiceToDelete(String shipNumber, String corpId) {
		
		return accountLineDao.findInvoiceToDelete(shipNumber, corpId);
	}

	public List getInvoiceToDeleteDitail(String shipNumber, String invoiceToDelete, String corpId) {
		return accountLineDao.getInvoiceToDeleteDitail(shipNumber, invoiceToDelete, corpId);
	}

	public int updateAccInactive(String id, String corpId, boolean soNetworkGroup, Long sid, String loginUser) {
		
		return accountLineDao.updateAccInactive(id, corpId,  soNetworkGroup, sid, loginUser);
	}

	public int updateCurrencyToInvoice(String shipNumber, String corpId, String recInvoiceNumber,BigDecimal recRateExchange, String recRateCurrency, Long accId, String racValueDate,String billtoCode,String billtoName, String userName, boolean contractType) {
		
		return accountLineDao.updateCurrencyToInvoice(shipNumber, corpId, recInvoiceNumber, recRateExchange,recRateCurrency, accId, racValueDate,billtoCode,billtoName,userName,  contractType);
	}

	public int updateReceivedInvoiceDate(String shipNumber, String corpId, String recInvoiceNumber, String receivedInvoiceDate) {
		return accountLineDao.updateReceivedInvoiceDate(shipNumber, corpId, recInvoiceNumber, receivedInvoiceDate);
		
	}
	
	public List goSOChild(Long serviceOrderId, String corpID, Long id){
		return accountLineDao.goSOChild(serviceOrderId, corpID, id);}
	public List goNextSOChild(Long serviceOrderId, String corpID, Long id){
		return accountLineDao.goNextSOChild(serviceOrderId, corpID, id);}
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long id){
		return accountLineDao.goPrevSOChild(serviceOrderId, corpID, id);}
	public List findMaximumAccountLine(String shipNm){
		return accountLineDao.findMaximumAccountLine(shipNm);}
    public List findMinimumAccountLine(String shipNm){
    	return accountLineDao.findMinimumAccountLine(shipNm);}
    public List findCountAccountLine(String shipNm){
    	return accountLineDao.findCountAccountLine(shipNm);}

	public List searchAgentInviceList(String invoiceNum, String shipNum, String sessionCorpID, String securityQuery, String regNum) {
		return accountLineDao.searchAgentInviceList(invoiceNum, shipNum, sessionCorpID, securityQuery, regNum);
	}

	public List getNumberOfWorldBankAuthorization(String sessionCorpID) {
		return accountLineDao.getNumberOfWorldBankAuthorization(sessionCorpID);
	}

	public int updatePayablesUpload(String sessionCorpID, String actgcode, String vendorInvoice, String payPayablevia, String payPayableDate, BigDecimal payPayableAmount, String shipNumber, String paygl) {
		return accountLineDao.updatePayablesUpload(sessionCorpID, actgcode, vendorInvoice, payPayablevia, payPayableDate, payPayableAmount,shipNumber, paygl);
	}

	public int updateReceivableUpload(String sessionCorpID, String recInvoiceNumber, String billToCode, String statusDate, BigDecimal receivedAmount, String systemDefaultmiscVl, String recGl, String externalReference) {
		return accountLineDao.updateReceivableUpload(sessionCorpID, recInvoiceNumber, billToCode, statusDate, receivedAmount,systemDefaultmiscVl,recGl,  externalReference);
	}

	public List getBillToAuthority(String shipNumber, String billToCodeForTicket, String sessionCorpID) {
		return accountLineDao.getBillToAuthority(shipNumber, billToCodeForTicket, sessionCorpID);
	}

	public List getCreditInvoice(String sessionCorpID, Long sid, String billToCode) { 
		return accountLineDao.getCreditInvoice(sessionCorpID, sid,billToCode);
	}

	public List getActgCode(String partnerCode, String companyDivision, String sessionCorpID) {
		return accountLineDao.getActgCode(partnerCode, companyDivision, sessionCorpID);
	}

	public List getGlCode(String contract, String chargeCode, String sessionCorpID) {
		return accountLineDao.getGlCode(contract, chargeCode, sessionCorpID);
	}

	public List getExpGlCode(String contract, String chargeCode, String sessionCorpID) {
		return accountLineDao.getExpGlCode(contract, chargeCode, sessionCorpID);
	}

	public List getBillToAuthState(String billToCode, String sessionCorpID) {
		return accountLineDao.getBillToAuthState(billToCode, sessionCorpID);
	}

	public double getSumActualRevenue(String sessionCorpID, Long sid,String billToCode) {
		return accountLineDao.getSumActualRevenue(sessionCorpID, sid,billToCode);
	}

	public List<DTO> getEmailInvoiceList(Date fromDate, Date toDate, String corpID) {
		return accountLineDao.getEmailInvoiceList(fromDate, toDate, corpID);
	}
	
	public List<DTO> getInvoiceListForMail(Date fromDate, Date toDate, String corpID,boolean isMailSend,String emailPrintOption, String storageBillingListMultiple){
		return accountLineDao.getInvoiceListForMail(fromDate, toDate, corpID, isMailSend,emailPrintOption, storageBillingListMultiple);
	}
	public Map<String,List<DTO>> getInvoiceMapListForMail(Date fromDate, Date toDate, String corpID, String storageBillingListMultiple){
		return accountLineDao.getInvoiceMapListForMail(fromDate, toDate, corpID, storageBillingListMultiple);
	}
	public Map<String,List<DTO>> getSeprateInvoiceMapListForMail(Date fromDate, Date toDate, String corpID, String storageBillingListMultiple){
		return accountLineDao.getSeprateInvoiceMapListForMail(fromDate, toDate, corpID, storageBillingListMultiple);
	}
	public List billingName(String jobType,String compDiv, String sessionCorpID) {
		return accountLineDao.billingName(jobType,compDiv, sessionCorpID);
	}

	public List payableName(String jobType,String compDiv, String sessionCorpID) {
		return accountLineDao.payableName(jobType,compDiv, sessionCorpID);
	}

	public List pricingName(String jobType,String compDiv, String sessionCorpID) {
		return accountLineDao.pricingName(jobType,compDiv, sessionCorpID);
	}
	public List auditorName(String jobType,String compDiv, String sessionCorpID){
		return accountLineDao.auditorName(jobType,compDiv, sessionCorpID);
	}
	
	public List internalBillingPerson(String jobType,String compDiv, String sessionCorpID){
		return accountLineDao.internalBillingPerson(jobType,compDiv, sessionCorpID);
	}

	public List findInternalCostVendorCode(String sessionCorpID, String companyDivision) {
		return accountLineDao.findInternalCostVendorCode(sessionCorpID, companyDivision);
	}

	public List getProjectedDistributedTotalAmountSum(String shipNumber) {
		return accountLineDao.getProjectedDistributedTotalAmountSum(shipNumber);
	}

	public List getProjectedActualExpenseSum(String shipNumber) {
		return accountLineDao.getProjectedActualExpenseSum(shipNumber);
	}

	public List getProjectedActualRevenueSum(String shipNumber) { 
		return accountLineDao.getProjectedActualRevenueSum(shipNumber);
	} 
	public List getProjectedActualRevenueSumHVY(String shipNumber)
	{
		return accountLineDao.getProjectedActualRevenueSumHVY(shipNumber);	
	}
	public BigDecimal getActualRevenue(String partnerCode, String sessionCorpID) {
		return accountLineDao.getActualRevenue(partnerCode, sessionCorpID);
	}

	public List codeCheckCredit(String partnerCode, String sessionCorpID, Boolean cmmDmmFlag,Boolean accountSearchValidation) {
		return accountLineDao.codeCheckCredit(partnerCode, sessionCorpID, cmmDmmFlag,accountSearchValidation);
	}
	public List getCreditInvoiceAmmountList(String sessionCorpID, Long sid, String billToCodeForTicket, String accountInterface) {
		return accountLineDao.getCreditInvoiceAmmountList(sessionCorpID, sid, billToCodeForTicket,accountInterface);
	}

	public List validateDiscription(String partnerCode, String sessionCorpID, String type) {
		return accountLineDao.validateDiscription(partnerCode, sessionCorpID, type);
	}

	public List getVanPayCharges(Long sid, String sessionCorpID) {
		return accountLineDao.getVanPayCharges(sid, sessionCorpID);
	}

	public List getDomCommCharges(Long sid, String sessionCorpID, String job, String systemDefaultmiscVl) {
		return accountLineDao.getDomCommCharges(sid, sessionCorpID,job, systemDefaultmiscVl);
	}

	public List findCommissionJobSystemDefault(String sessionCorpID) {
		return accountLineDao.findCommissionJobSystemDefault(sessionCorpID);
	}
	public List findVatCalculation(String sessionCorpID){
		return accountLineDao.findVatCalculation(sessionCorpID);
	}
	public List findChargeVatCalculation(String sessionCorpID,String contract,String chargeCode){
		return accountLineDao.findChargeVatCalculation(sessionCorpID, contract, chargeCode);
	}
	public List getAllAccountLine(Long sid) {
		return accountLineDao.getAllAccountLine(sid); 
	}

	public BigDecimal getAllEstimateRevenueAmount(Long sid) {
		return accountLineDao.getAllEstimateRevenueAmount(sid);
	}

	public List getActualexpenseDDSAGT(Long sid, String sessionCorpID) {
		return accountLineDao.getActualexpenseDDSAGT(sid, sessionCorpID);
	}

	public List getbookingagentCommission(String bookingAgentCode, String sessionCorpID) {
		return accountLineDao.getbookingagentCommission(bookingAgentCode, sessionCorpID);
	}

	public List getGLList(String chargeCode, String contract, String sessionCorpID) {
		return accountLineDao.getGLList(chargeCode, contract, sessionCorpID);
	}

	public String getOwnerPayTo(String sessionCorpID, String carrier) {
		return accountLineDao.getOwnerPayTo(sessionCorpID, carrier);
	}

	public int deleteAccountline(Long id) {
		return accountLineDao.deleteAccountline(id);
	}

	public int updatePricingLine(HashMap<Long, Map> pricingLineMap) {
		return accountLineDao.updatePricingLine(pricingLineMap);
		
	}

	public int updateSOTotalPricingLine(String estimatedTotalExpense, String estimatedTotalRevenue, Long sid,String estimatedGrossMargin,String estimatedGrossMarginPercentage) {
		return accountLineDao.updateSOTotalPricingLine(estimatedTotalExpense, estimatedTotalRevenue, sid, estimatedGrossMargin,estimatedGrossMarginPercentage);
	}

	public List findQuotationDiscription(String contract, String chargeCode, String sessionCorpID) {
		return accountLineDao.findQuotationDiscription(contract, chargeCode, sessionCorpID);
	}

	public int deletePricingLine(Long id, Long sid, String loginUser) {
		return accountLineDao.deletePricingLine(id,  sid, loginUser);
	}
 

	public List vendorNameRelo(String partnerCode, String sessionCorpID, String type, String jobRelo, Boolean cmmDmmFlag) {
		return accountLineDao.vendorNameRelo(partnerCode, sessionCorpID, type, jobRelo, cmmDmmFlag);
	}

	public List invoiceNavisionExtract(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate) {
		return accountLineDao.invoiceNavisionExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
	}

	public List findPayableExtractNavisionLevel(String payCompanyCode, String corpId,  boolean positiveExpense) {
		return accountLineDao.findPayableExtractNavisionLevel(payCompanyCode, corpId,  positiveExpense);
	}
	public List findPayableExtractNavisionLevelForUGCN(String payCompanyCode, String corpId,  boolean positiveExpense) {
		return accountLineDao.findPayableExtractNavisionLevelForUGCN(payCompanyCode, corpId,  positiveExpense);
	}


	public String findBilltoCodeVatNumber(String billToCode, String sessionCorpID) {
		return accountLineDao.findBilltoCodeVatNumber(billToCode, sessionCorpID);
	}

	public String findbillingCountryCode(String billToCode, String sessionCorpID) {
		return accountLineDao.findbillingCountryCode(billToCode,sessionCorpID);
	}

	public Integer checkRecInvoiceForHVY(String shipNumber) {
		return accountLineDao.checkRecInvoiceForHVY(shipNumber);
	}

	public List totalVatAmount(String invoiceNumber,String recPostingDate,String corpid) {
		return accountLineDao.totalVatAmount(invoiceNumber, recPostingDate, corpid);
	}

	public List getNavisionPayablePostDataList(String sessionCorpID) {
		return accountLineDao.getNavisionPayablePostDataList(sessionCorpID); 
	}
	public List getNavisionPayablePostDataListForUGCN(String sessionCorpID) {
		return accountLineDao.getNavisionPayablePostDataListForUGCN(sessionCorpID); 
	}
	public List getNavisionPayablePostDataListVORU(String sessionCorpID){
		return accountLineDao.getNavisionPayablePostDataListVORU(sessionCorpID);
	}

	public List getNavisionPayableExtract(String payCompanyCode, String corpid) {
		return accountLineDao.getNavisionPayableExtract(payCompanyCode, corpid);
	}
	public List getNavisionPayableExtractForUGCN(String payCompanyCode, String corpid) {
		return accountLineDao.getNavisionPayableExtractForUGCN(payCompanyCode, corpid);
	}

	public int navisionPayableinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String corpid, String payCompanyCode) {
		return accountLineDao.navisionPayableinvoiceUpdate(batchName, shipnumber, payXferUser, corpid,payCompanyCode);
		
	}
	public int navisionPayableinvoiceUpdateForUGCN(String batchName, String shipnumber, String payXferUser, String corpid, String payCompanyCode) {
		return accountLineDao.navisionPayableinvoiceUpdateForUGCN(batchName, shipnumber, payXferUser, corpid,payCompanyCode);
		
	}
	public int updateAccEstCurrency(Long aid, String estCurrency, String estValueDate, String estExchangeRate, String estLocalAmount, String estLocalRate) {
		return accountLineDao.updateAccEstCurrency(aid, estCurrency, estValueDate, estExchangeRate, estLocalAmount,estLocalRate);
	}
	public int updateAccEstSellCurrency(Long aid, String estSellCurrency, String estSellValueDate, String estSellExchangeRate, String estSellLocalAmount, String estSellLocalRate){
		return accountLineDao.updateAccEstSellCurrency(aid, estSellCurrency, estSellValueDate, estSellExchangeRate, estSellLocalAmount, estSellLocalRate);
	}
	public int updateAccEstContractSellCurrency(Long aid, String estSellCurrency, String estSellValueDate, String estSellExchangeRate, String estSellLocalAmount, String estSellLocalRate, String estimateContractCurrency, String estimateContractValueDate, String estimateContractExchangeRate, String estimateContractRateAmmount, String estimateContractRate){
		return accountLineDao.updateAccEstContractSellCurrency(aid, estSellCurrency, estSellValueDate, estSellExchangeRate, estSellLocalAmount, estSellLocalRate, estimateContractCurrency, estimateContractValueDate, estimateContractExchangeRate, estimateContractRateAmmount, estimateContractRate);
	}
	public int updateAccEstContractCurrency(Long aid,String estSellCurrency,String estSellValueDate,String estSellExchangeRate,String estSellLocalAmount,String estSellLocalRate,String estimatePayableContractCurrency,String estimatePayableContractValueDate,String estimatePayableContractExchangeRate,String estimatePayableContractRateAmmount,String estimatePayableContractRate){
		return accountLineDao.updateAccEstContractCurrency(aid, estSellCurrency, estSellValueDate, estSellExchangeRate, estSellLocalAmount, estSellLocalRate, estimatePayableContractCurrency, estimatePayableContractValueDate, estimatePayableContractExchangeRate, estimatePayableContractRateAmmount, estimatePayableContractRate);
	}
	public List validateEnteredFile(String fileName,String sessionCorpID)
	{
		return accountLineDao.validateEnteredFile(fileName, sessionCorpID);
	}
	public int updateInvoiceNavision(String fileName,String sessionCorpID)
	{
		return accountLineDao.updateInvoiceNavision(fileName, sessionCorpID);
	}
	public int updatePayableNavision(String fileName,String sessionCorpID)
	{
		return accountLineDao.updatePayableNavision(fileName, sessionCorpID);
	}

	public List totalForeignCurrencyAmount(String invoiceNumber, String recPostingDate, String corpid) {
		return accountLineDao.totalForeignCurrencyAmount(invoiceNumber, recPostingDate, corpid);
	}

	public List regenerateInvoiceNavision(String fileName,String sessionCorpID){
		return accountLineDao.regenerateInvoiceNavision(fileName,sessionCorpID);
	}
	public int regenrateInvoiceExtractUpdate(String batchName,String fileName,String recXferUser,String recPostingDate,String sessionCorpID){
		return accountLineDao.regenrateInvoiceExtractUpdate(batchName,fileName,recXferUser,recPostingDate,sessionCorpID);
	}
	public String getRecordPostingDate(String fileName, String sessionCorpID){
		return accountLineDao.getRecordPostingDate(fileName,sessionCorpID);
	}
	public int regenerateNavisionPayableinvoiceUpdate(String batchName, String fileName, String payXferUser, String corpid){
		return accountLineDao.regenerateNavisionPayableinvoiceUpdate(batchName,  fileName,  payXferUser, corpid);
	}
	public List regeneratePayableExtractNavisionLevel(String fileName, String corpId,  boolean positiveExpense){
		return accountLineDao.regeneratePayableExtractNavisionLevel(fileName, corpId, positiveExpense);
	}
	public List getRegenerateNavisionPayableExtract(String fileName, String sessionCorpID){
		return accountLineDao.getRegenerateNavisionPayableExtract(fileName,sessionCorpID);
	}
	public List regenerateTotalDistributionAmount(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalDistributionAmount(invoiceNumber,fileName,corpid);
	}
	 public List regenerateTotalRevenue(String invoiceNumber,String fileName,String corpid){
		 return accountLineDao.regenerateTotalRevenue(invoiceNumber,fileName,corpid);
	 }
	 public List regenerateTotalVatAmount(String invoiceNumber,String fileName,String corpid){
		 return accountLineDao.regenerateTotalVatAmount(invoiceNumber,fileName,corpid);
	 }
	 public List regenerateTotalForeignCurrencyAmount(String invoiceNumber, String fileName, String corpid){
		 return  accountLineDao.regenerateTotalForeignCurrencyAmount(invoiceNumber, fileName,corpid);
	 }


	public List getAccountCompanyDivision(Long sid, String companyDivisionPayPostFlag,boolean authorizedCreditNoteCorpId) {
		return accountLineDao.getAccountCompanyDivision(sid,companyDivisionPayPostFlag,authorizedCreditNoteCorpId);
	}

	public List getInvoiceNavisionPostDataList(String sessionCorpID) {
		return accountLineDao.getInvoiceNavisionPostDataList(sessionCorpID);
	}

	public String getRecordPayableNavDate(String payableFileName, String sessionCorpID) {
		return accountLineDao.getRecordPayableNavDate(payableFileName, sessionCorpID);
	}

	public int invoiceNavisionExtractUpdate(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.invoiceNavisionExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List totalDistributionNavAmount(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.totalDistributionNavAmount(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List totalForeignCurrencyAmountNav(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.totalForeignCurrencyAmountNav(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List totalRevenueNav(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.totalRevenueNav(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List totalVatAmountnav(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.totalVatAmountnav(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List getcompanydivisionList(String sessionCorpID, Long sid, String billToCodeForTicket) { 
		return accountLineDao.getcompanydivisionList(sessionCorpID, sid, billToCodeForTicket);
	}
	public List getInvoiceSAPPostDataList(String sessionCorpID){
		return accountLineDao.getInvoiceSAPPostDataList(sessionCorpID);
	}
	public List invoiceSAPExtract(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate){
		return accountLineDao.invoiceSAPExtract(batchName,invoiceCompanyCode,corpid, recPostingDate);
	}
	public List invoiceSAPExtractForUGCN(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate){
		return accountLineDao.invoiceSAPExtractForUGCN(batchName,invoiceCompanyCode,corpid, recPostingDate);
	}
	public List totalDistributionSAPAmount(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalDistributionSAPAmount(invoiceNumber,recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalDistributionSAPAmountForUGCN(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalDistributionSAPAmountForUGCN(invoiceNumber,recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalRevenueSAP(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalRevenueSAP(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalRevenueSAPForUGCN(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalRevenueSAPForUGCN(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalVatAmountSAP(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalVatAmountSAP(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalForeignCurrencyAmountSAP(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalForeignCurrencyAmountSAP(invoiceNumber,recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalForeignCurrencyAmountSAPForUGCN(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.totalForeignCurrencyAmountSAPForUGCN(invoiceNumber,recPostingDate, corpid, invoiceCompanyCode);
	}
	public int invoiceSAPExtractUpdate(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.invoiceSAPExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, invoiceCompanyCode);
	}
	public int invoiceSAPExtractUpdateForUGCN(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid, String invoiceCompanyCode){
		return accountLineDao.invoiceSAPExtractUpdateForUGCN(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, invoiceCompanyCode);
	}

	public List getSAPPayablePostDataList(String sessionCorpID){
		return accountLineDao.getSAPPayablePostDataList(sessionCorpID);
	}
	public List getSAPPayablePostDataListForUGCN(String sessionCorpID){
		return accountLineDao.getSAPPayablePostDataListForUGCN(sessionCorpID);
	}
	public List getSAPPayableExtract(String payCompanyCode, String corpid){
		return accountLineDao.getSAPPayableExtract(payCompanyCode,corpid);
	}
	public List findPayableExtractSAPLevel(String payCompanyCode, String corpId,  boolean positiveExpense){
		return accountLineDao.findPayableExtractSAPLevel(payCompanyCode,corpId,positiveExpense);
	}
	public int navisionPayableSAPUpdate(String batchName, String shipnumber, String payXferUser, String corpid ,String payCompanyCode){
		return accountLineDao.navisionPayableSAPUpdate(batchName,shipnumber,  payXferUser,corpid , payCompanyCode);
	}
	public List findSAPInvoiceBillToCode(String invoiceFileName, String corpId){
		return accountLineDao.findSAPInvoiceBillToCode(invoiceFileName,corpId);
	}
	public int updateInvoiceSAP(String fileName,String sessionCorpID){
		return accountLineDao.updateInvoiceSAP(fileName,sessionCorpID);
	}
	public int updatePayableSAP(String fileName,String sessionCorpID){
		return accountLineDao.updatePayableSAP(fileName,sessionCorpID);
	}
	public String getRecordPostingDateSAP(String fileName, String sessionCorpID){
		return accountLineDao.getRecordPostingDateSAP(fileName, sessionCorpID);
	}
	public String getRecordPostingDateGP(String fileName, String sessionCorpID){
		return accountLineDao.getRecordPostingDateGP(fileName, sessionCorpID);
	}
	public List regenrateGPInvoiceExtract(String invoiceFileName,String sessionCorpID){
		return accountLineDao.regenrateGPInvoiceExtract(invoiceFileName, sessionCorpID);
	}
	public List regenerateInvoiceSAP(String fileName,String sessionCorpID){
		return accountLineDao.regenerateInvoiceSAP(fileName,sessionCorpID);
	}
	public List regenerateInvoiceSAPSG(String fileName,String sessionCorpID){
		return accountLineDao.regenerateInvoiceSAPSG(fileName,sessionCorpID);
	}
	public List regenerateSAPTotalDistributionAmount(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateSAPTotalDistributionAmount(invoiceNumber,fileName,corpid);
	}
	public List regenerateTotalSAPRevenue(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalSAPRevenue(invoiceNumber,fileName, corpid);
	}
	public List regenerateTotalSAPVatAmount(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalSAPVatAmount(invoiceNumber,fileName,corpid);
	}
	public List regenerateTotalSAPForeignCurrencyAmount(String invoiceNumber, String fileName, String corpid){
		 return accountLineDao.regenerateTotalSAPForeignCurrencyAmount(invoiceNumber, fileName, corpid);
	 }
	public int regenrateInvoiceExtractUpdateSAP(String batchName,String fileName,String recXferUser,String recPostingDate,String sessionCorpID){
		 return accountLineDao.regenrateInvoiceExtractUpdateSAP(batchName,fileName,recXferUser,recPostingDate,sessionCorpID);
	}
	public String getRecordPayableSAPDate(String payableFileName, String sessionCorpID){
		return accountLineDao.getRecordPayableSAPDate(payableFileName,sessionCorpID);
	}
	public List getRegenerateSAPPayableExtract(String fileName, String corpid){
		return accountLineDao.getRegenerateSAPPayableExtract(fileName, corpid);
	}
	public List regeneratePayableExtractSAPLevel(String fileName, String corpId,  boolean positiveExpense){
		return accountLineDao.regeneratePayableExtractSAPLevel(fileName, corpId,  positiveExpense);
	}
	public int regenerateSAPPayableinvoiceUpdate(String batchName, String fileName, String payXferUser, String corpid){
		return accountLineDao.regenerateSAPPayableinvoiceUpdate(batchName, fileName, payXferUser, corpid);
	}
	public List getAccountLineForBatchInvoicing(String sessionCorpID,String vendorCode,String vendorInvoice,String accountInterface,String currency,String fieldName,String shipNumber,String companyDivision){
		return accountLineDao.getAccountLineForBatchInvoicing(sessionCorpID, vendorCode, vendorInvoice,accountInterface,currency,fieldName,shipNumber,companyDivision);
	}
	public String getSOInfoFromSequenceNumber(String sessionCorpID,String sequenceNumber){
		return accountLineDao.getSOInfoFromSequenceNumber(sessionCorpID, sequenceNumber);
	 }
	public Map<String, String> getVendorVatCodeMap(String sessionCorpID,String sequenceNumber){
		return accountLineDao.getVendorVatCodeMap(sessionCorpID, sequenceNumber);
	}
    public String saveAccountLineFromBatchPayable(String sessionCorpID,String invoiceNumberListServer,String actualExpenseListServer,String payingStatusListServer,Date invoiceDate,Date recievedDate,String exchangeRate){
    	return accountLineDao.saveAccountLineFromBatchPayable(sessionCorpID, invoiceNumberListServer, actualExpenseListServer, payingStatusListServer,invoiceDate,recievedDate,exchangeRate);
    }
    public String saveAccountLineBatchPayableForInvoiceNoDate(String sessionCorpID,String invoiceNumberListServer,String actualExpenseListServer,String payingStatusListServer,Date invoiceDate,Date recievedDate,String exchangeRate,String vendorInvoice, String payVatDescrListServer, String payVatPerListServer, String payVatAmtListServer,String actgCodeListServer){
    	return accountLineDao.saveAccountLineBatchPayableForInvoiceNoDate(sessionCorpID, invoiceNumberListServer, actualExpenseListServer, payingStatusListServer, invoiceDate, recievedDate, exchangeRate, vendorInvoice,   payVatDescrListServer,   payVatPerListServer,  payVatAmtListServer, actgCodeListServer);
    }
    public List getListForBatchPayablePosting(String sessionCorpID,String vendorCode,String vendorInvoice,String accountInterface,String currency,String shipNumber,String companyDivision){
    	return accountLineDao.getListForBatchPayablePosting(sessionCorpID, vendorCode, vendorInvoice,accountInterface, currency,shipNumber,companyDivision);
    }
    public int batchPayPostDate(String soNumber,String VendorCode,String invoiceNumber,String InvoicePayPostDate,String loginUser,String currency,String accountInterface,String sessionCorpID){
    	return accountLineDao.batchPayPostDate(soNumber, VendorCode, invoiceNumber, InvoicePayPostDate, loginUser, currency,accountInterface,sessionCorpID);
    }
    public List findMaxLineNumber(String shipNumber){
    	return accountLineDao.findMaxLineNumber(shipNumber);
    }
    public List checkForGroupAccount(String shipNumber, Long grpId,String chargeCode){
    	return accountLineDao.checkForGroupAccount(shipNumber,grpId,chargeCode);
    }
    public List invoiceSageExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate){
    	return accountLineDao.invoiceSageExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
    }
    public List totalDistributionSageAmount(String invoiceNumber,String recPostingDate,String corpid ){
    	return accountLineDao.totalDistributionSageAmount(invoiceNumber, recPostingDate, corpid );
    }
    public List totalRevenueSage(String invoiceNumber,String recPostingDate,String corpid ){
    	return accountLineDao.totalRevenueSage(invoiceNumber, recPostingDate, corpid );
    }
    public List totalVatAmountSage(String invoiceNumber,String recPostingDate,String corpid ){
    	return accountLineDao.totalVatAmountSage(invoiceNumber, recPostingDate, corpid );
    }
    public List calTotalVatAmountSage(String invoiceNumber,String recPostingDate,String corpid){
    	return accountLineDao.calTotalVatAmountSage(invoiceNumber, recPostingDate, corpid );
    }
    
	public List totalForeignCurrencyAmountSage(String invoiceNumber,String recPostingDate,String corpid ){
		return accountLineDao.totalForeignCurrencyAmountSage(invoiceNumber, recPostingDate, corpid );
	}
	public int invoiceSageExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid, String invoiceCompanyCode){
		return accountLineDao.invoiceSageExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List getSagePayableExtract(String payCompanyCode, String sessionCorpID, String payPostingDate){
		return accountLineDao.getSagePayableExtract(payCompanyCode, sessionCorpID, payPostingDate);
	}
	public List findPayableExtractSageLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense){
		return accountLineDao.findPayableExtractSageLevel(payCompanyCode, corpId, payPostingDate, positiveExpense);
	}
    public List findToolTipForExpense(Long aid,String fieldNames){
    	return accountLineDao.findToolTipForExpense(aid, fieldNames);
    }
    public List findToolTipForRevenue(Long aid,String fieldNames,String multiCurrency,Date statusDate){
    	return accountLineDao.findToolTipForRevenue(aid, fieldNames, multiCurrency, statusDate);
    }
    
	public int sagePayableinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String payPostingDate, String corpid) {
		return accountLineDao.sagePayableinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
	}

	public String checkContractType(String contract, String sessionCorpID) {
		return accountLineDao.checkContractType(contract, sessionCorpID);
	}

	public List networkPartnerName(String partnerCode, String sessionCorpID, String type) {
		return accountLineDao.networkPartnerName(partnerCode, sessionCorpID, type);
	}

	public String getNetworkPartnerName(String sessionCorpID) {
		return accountLineDao.getNetworkPartnerName(sessionCorpID);
	}
    public List<AccountLine> getContractAgentName(String partnerCode,String corpid, String type){
		return accountLineDao.getContractAgentName(partnerCode, corpid, type);
	}
	public List getUTSIInvoiceNavisionPostDataList(String sessionCorpID){
		return accountLineDao.getUTSIInvoiceNavisionPostDataList(sessionCorpID);
	}
	public List utsiInvoiceNavisionExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate){
		return accountLineDao.utsiInvoiceNavisionExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
	}
	public List totalDistributionNavAmountUTSI(String invoiceNumber,String recPostingDate,String corpid, String invoiceCompanyCode){
		return accountLineDao.totalDistributionNavAmountUTSI(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalRevenueNavUTSI(String invoiceNumber,String recPostingDate,String corpid, String invoiceCompanyCode){
		return accountLineDao.totalRevenueNavUTSI(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalVatAmountnavUTSI(String invoiceNumber,String recPostingDate,String corpid, String invoiceCompanyCode){
		return accountLineDao.totalVatAmountnavUTSI(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public List totalForeignCurrencyAmountNavUTSI(String invoiceNumber, String recPostingDate, String corpid, String invoiceCompanyCode) {
		return accountLineDao.totalForeignCurrencyAmountNavUTSI(invoiceNumber, recPostingDate, corpid, invoiceCompanyCode);
	}
	public int invoiceNavisionExtractUpdateUTSI(String batchName,String recXferUser,String corpid,String compCode , String Id){
		return accountLineDao.invoiceNavisionExtractUpdateUTSI(batchName, recXferUser, corpid, compCode, Id);
	}
	public List findInvoiceBillToCodeUTSI(String invoiceFileName, String corpId){
		return accountLineDao.findInvoiceBillToCodeUTSI(invoiceFileName, corpId);
	}
	public List getNavisionPayablePostDataListUTSI(String sessionCorpID){
		return accountLineDao.getNavisionPayablePostDataListUTSI(sessionCorpID);
	}
	public List getNavisionPayableExtractUTSI(String payCompanyCode,String corpid){
		return accountLineDao.getNavisionPayableExtractUTSI(payCompanyCode,corpid);
	}
	public List findPayableExtractUTSINavisionLevel(String payCompanyCode, String corpId,  boolean positiveExpense,String payPostDate){
		return accountLineDao.findPayableExtractUTSINavisionLevel(payCompanyCode, corpId, positiveExpense,payPostDate);
	}
	public int navisionPayableinvoiceUpdateUTSI(String batchName,String payXferUser,String corpid, String payCompanyCode, String Id, boolean costXfer){
		return accountLineDao.navisionPayableinvoiceUpdateUTSI(batchName, payXferUser, corpid, payCompanyCode, Id, costXfer);
	}
	public int updateInvoiceNavisionUTSI(String fileName,String sessionCorpID){
		return accountLineDao.updateInvoiceNavisionUTSI(fileName, sessionCorpID);
	}
	public int updateUTSIPayableNavision(String fileName,String sessionCorpID){
		return accountLineDao.updateUTSIPayableNavision(fileName, sessionCorpID);
	}
	public String getUTSIRecordPostingDate(String fileName,String sessionCorpID){
		return accountLineDao.getUTSIRecordPostingDate(fileName, sessionCorpID);
	}
	public List regenerateInvoiceNavisionUTSI(String fileName,String sessionCorpID){
		return accountLineDao.regenerateInvoiceNavisionUTSI(fileName, sessionCorpID);
	}
	public List regenerateTotalDistributionAmountUTSI(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalDistributionAmountUTSI(invoiceNumber, fileName, corpid);
	}
	public List regenerateTotalRevenueUTSI(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalRevenueUTSI(invoiceNumber, fileName, corpid);
	}
	public List regenerateTotalVatAmountUTSI(String invoiceNumber,String fileName,String corpid){
		return accountLineDao.regenerateTotalVatAmountUTSI(invoiceNumber, fileName, corpid);
	}
	public List regenerateTotalForeignCurrencyAmountUTSI(String invoiceNumber, String fileName, String corpid){
		return accountLineDao.regenerateTotalForeignCurrencyAmountUTSI(invoiceNumber, fileName, corpid);
	}
	public int regenrateInvoiceExtractUpdateUTSI(String batchName,String fileName,String recXferUser,String recPostingDate,String sessionCorpID){
		return accountLineDao.regenrateInvoiceExtractUpdateUTSI(batchName, fileName, recXferUser, recPostingDate, sessionCorpID);
	}
	public String getRecordPayableNavDateUTSI(String payableFileName, String sessionCorpID){
		return accountLineDao.getRecordPayableNavDateUTSI(payableFileName, sessionCorpID);
	}
	public List getRegenerateNavisionPayableExtractUTSI(String fileName, String sessionCorpID){
		return accountLineDao.getRegenerateNavisionPayableExtractUTSI(fileName, sessionCorpID);
	}
	public List regenerateUTSIPayableExtractNavisionLevel(String fileName, String corpId,  boolean positiveExpense){
		return accountLineDao.regenerateUTSIPayableExtractNavisionLevel(fileName, corpId, positiveExpense);
	}
	public int regenerateNavisionPayableinvoiceUpdateUTSI(String batchName, String fileName, String payXferUser, String corpid){
		return accountLineDao.regenerateNavisionPayableinvoiceUpdateUTSI(batchName, fileName, payXferUser, corpid);
	}
	public List getExternalActualExpSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalActualExpSum(shipNumber, externalCorpID);
	}

	public List getExternalActualRevSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalActualRevSum(shipNumber, externalCorpID);
	}

	public List getExternalEntitleSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalEntitleSum(shipNumber, externalCorpID);
	}

	public List getExternalEstimateExpSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalEstimateExpSum(shipNumber, externalCorpID);
	}

	public List getExternalEstimateRevSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalEstimateRevSum(shipNumber, externalCorpID);
	}

	public List getExternalProjectedActualExpenseSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalProjectedActualExpenseSum(shipNumber, externalCorpID);
	}

	public List getExternalProjectedActualRevenueSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalProjectedActualRevenueSum(shipNumber, externalCorpID);
	}

	public List getExternalProjectedDistributedTotalAmountSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalProjectedDistributedTotalAmountSum(shipNumber, externalCorpID);
	}

	public List getExternalRevisionExpSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalRevisionExpSum(shipNumber, externalCorpID);
	}

	public List getExternalRevisionRevSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternalRevisionRevSum(shipNumber, externalCorpID);
	}

	public List getExternaldistributedAmountSum(String shipNumber, String externalCorpID) {
		return accountLineDao.getExternaldistributedAmountSum(shipNumber, externalCorpID);
	}

	public int updateNetworkSynchedId(Long id) {
		return accountLineDao.updateNetworkSynchedId(id); 
	}

	public String getNetworkSynchedId(Long id, String sessionCorpID, String agentCode) {
		return accountLineDao.getNetworkSynchedId(id,   sessionCorpID,   agentCode);
	}

	public double getActualRevenueBaseCurrency(Long sid, String sessionCorpID, String bookingAgentCode) {
		return accountLineDao.getActualRevenueBaseCurrency(sid, sessionCorpID, bookingAgentCode);
	}

	public List getAllRemoteAccountLine(Long sid) {
		return accountLineDao.getAllRemoteAccountLine(sid);
	}

	public Integer checkRecInvoiceForMGMT(Long sid) {
		return accountLineDao.checkRecInvoiceForMGMT(sid);
	}

	public Integer gatAllGenrateInvoiceLine(Long sid) {
		return accountLineDao.gatAllGenrateInvoiceLine(sid);
	}

	public String getAgentSynchedId(Long id, String sessionCorpID, String networkPartnerCode) {
		return accountLineDao.getAgentSynchedId(id, sessionCorpID, networkPartnerCode);
	}

	public List getAllDiscounteesAccountLine(Long sid, String bookingAgentCode, String externalCorpID) {
		return accountLineDao.getAllDiscounteesAccountLine(sid, bookingAgentCode, externalCorpID);
	}

	public String getChargeCommission(String chargeCode, String contract, String sessionCorpID) {
		return accountLineDao.getChargeCommission(chargeCode, contract, sessionCorpID);
	}

	public int updateNetworkSynchedIdNetWork(SortedMap<Long, Long> accountlineIdMap) {
		return accountLineDao.updateNetworkSynchedIdNetWork(accountlineIdMap);
	}

	public int MyOBInvoiceExtractUpdate(String batchName,String accIdList,String recXferUser,String recPostingDate,String corpid){
		return accountLineDao. MyOBInvoiceExtractUpdate(batchName, accIdList, recXferUser, recPostingDate, corpid);
	}
	public List getPayableMYOBPostDataList(String sessionCorpID){
		return accountLineDao.getPayableMYOBPostDataList(sessionCorpID);
	}
	 public List findMYOBPayableExtract(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense){
		 return accountLineDao.findMYOBPayableExtract(payCompanyCode, corpId, payPostingDate, positiveExpense);
	 }
	 public int payableMYOBinvoiceUpdate(String batchName, String accIdList,String payXferUser, String payPostingDate, String sessionCorpID){
		 return accountLineDao.payableMYOBinvoiceUpdate(batchName, accIdList, payXferUser, payPostingDate, sessionCorpID);
	 }
		
	public int updateAccountlineMGMTFee(Long sid, String sessionCorpID, String bookingAgentCode,String recInvoiceNumber) {
		return accountLineDao.updateAccountlineMGMTFee(sid, sessionCorpID, bookingAgentCode, recInvoiceNumber);
	}
	public List findToolTipPartnerDetail(String sessionCorpID,String partnerCode){
		return accountLineDao.findToolTipPartnerDetail(sessionCorpID, partnerCode);
	}
	public List getVendorExtractListDataForUTSI(String sessionCorpID){
		return accountLineDao.getVendorExtractListDataForUTSI(sessionCorpID);
	}
	public List utsiCustomerExtract(String beginDate, String endDate, String companyDivision,String corpID){
		return accountLineDao.utsiCustomerExtract(beginDate, endDate, companyDivision, corpID);
	}
	public List utsiVendorExtract(String beginDate, String endDate, String companyDivision,String corpID){
		return accountLineDao.utsiVendorExtract(beginDate, endDate, companyDivision, corpID);
	}
	public List utsiVendorBankExtract(String beginDate, String endDate, String companyDivision,String corpID){
		return accountLineDao.utsiVendorBankExtract(beginDate, endDate, companyDivision, corpID);
	}
	public int navisionPayableAoountlineUpdateUTSI(String batchName,String recXferUser, String sessionCorpID,String invoiceCompanyCode, String Id){
		return accountLineDao.navisionPayableAoountlineUpdateUTSI(batchName, recXferUser, sessionCorpID, invoiceCompanyCode, Id);
	}
	public Integer getCommissionCount(Long sid,String job){
		return accountLineDao.getCommissionCount(sid,job);
	}
	public Boolean getExternalCostElement(String externalCorpID){
		return accountLineDao.getExternalCostElement(externalCorpID);
	}
	public List findPayableExtractNavisionVORULevel(String payCompanyCode,String sessionCorpID, boolean positiveExpense){
		return accountLineDao.findPayableExtractNavisionVORULevel(payCompanyCode, sessionCorpID, positiveExpense);
	}
	public List invoiceMYOBExtract(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate){
		return accountLineDao.invoiceMYOBExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
	}
	public List totalForeignCurrencyAmountMYOB(String invoiceNumber,String recPostingDate,String corpid){
		return accountLineDao.totalForeignCurrencyAmountMYOB(invoiceNumber, recPostingDate, corpid);
	}
	public List getNavisionPayableExtractVoru(String payCompanyCode,String sessionCorpID){
		return accountLineDao.getNavisionPayableExtractVoru(payCompanyCode, sessionCorpID);
	}
	public int navisionPayableinvoiceUpdateVoru(String batchName,String shipnumber,String payXferUser,String corpid, String payCompanyCode){
		return accountLineDao.navisionPayableinvoiceUpdateVoru(batchName, shipnumber, payXferUser, corpid, payCompanyCode);
	}
	public List getISO2CountryCode(String cCode, String parameter){
		return accountLineDao.getISO2CountryCode(cCode, parameter);
	}
	public String fillCurrencyByBillToCode(String billToCode,String sessionCorpID){
		return accountLineDao.fillCurrencyByBillToCode(billToCode, sessionCorpID);
	}
	public String fillCurrencyByChargeCode(String billToCode,String sessionCorpID){
		return accountLineDao.fillCurrencyByChargeCode(billToCode, sessionCorpID);
	}
	public List getInvoiceSAPSGPostDataList(String sessionCorpID){
		return accountLineDao.getInvoiceSAPSGPostDataList(sessionCorpID);
	}
	public List getForUGCNInvoiceSAPSGPostDataList(String sessionCorpID){
		return accountLineDao.getForUGCNInvoiceSAPSGPostDataList(sessionCorpID);
	}
	public List invoiceSAPSGExtract(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate){
		return accountLineDao.invoiceSAPSGExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
	}
	public List invoiceSAPSGExtractForUGCN(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate){
		return accountLineDao.invoiceSAPSGExtractForUGCN(batchName, invoiceCompanyCode, corpid, recPostingDate);
	}


	public List gpInvoiceExtract(String batchName,   String corpid, String recPostingDate) {
		return accountLineDao.gpInvoiceExtract(batchName,   corpid, recPostingDate);
	}

	public String getRecordPostingDateSoloman(String invoiceFileName,String sessionCorpID){
		return accountLineDao.getRecordPostingDateSoloman(invoiceFileName, sessionCorpID);
	}
	public List regenerateInvoiceExtract(String invoiceFileName,String sessionCorpID){
		return accountLineDao.regenerateInvoiceExtract(invoiceFileName, sessionCorpID);
	}
	public List gpTotalDistributionAmount(String invoiceNumber, String recPostingDate, String corpid) {
		return accountLineDao.gpTotalDistributionAmount(invoiceNumber, recPostingDate, corpid); 
	}
	public List regGPTotalDistributionAmount(String invoiceNumber,String recPostingDate,String corpid,String invoiceFileName){
		return accountLineDao.regGPTotalDistributionAmount(invoiceNumber, recPostingDate, corpid,invoiceFileName); 
	}

	public List gpTotalRevenue(String invoiceNumber, String recPostingDate, String corpid) {
		return accountLineDao.gpTotalRevenue(invoiceNumber, recPostingDate, corpid );
	}
	public List regGPTotalRevenue(String invoiceNumber,String recPostingDate,String corpid,String recXfer){
		return accountLineDao.regGPTotalRevenue(invoiceNumber, recPostingDate, corpid,recXfer);
	}

	public int gpInvoiceExtractUpdate(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid ) {
		return accountLineDao.gpInvoiceExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid  );
	}
	public int regGPInvoiceExtractUpdate(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid,String invoiceFileName){
		return accountLineDao.regGPInvoiceExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid,invoiceFileName );
	}
	
	public String getRecordPayableGPDate(String payableFileName,String sessionCorpID){
		return accountLineDao.getRecordPayableGPDate(payableFileName, sessionCorpID);
	}

	public List regenrateGPPayableExtract(String corpid,String payPostingDate,String payXfer){
		return accountLineDao.regenrateGPPayableExtract(corpid, payPostingDate, payXfer);
	}
	public List getGPPayableExtract(String corpid, String payPostingDate) {
		return accountLineDao.getGPPayableExtract(corpid, payPostingDate); 
	}

	public List getAccPayableExtract(String corpid,String payPostingDate){
		return accountLineDao.getAccPayableExtract(corpid, payPostingDate);
	}
	public List findGPPayableExtractLevel(String corpId, String payPostingDate, boolean positiveExpense) {
		return accountLineDao.findGPPayableExtractLevel(corpId, payPostingDate, positiveExpense); 
	}
	public int accPayableinvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid){
		return accountLineDao.accPayableinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
	}
	public List findAccPayableExtractLevel(String corpId, String payPostingDate, boolean positiveExpense){
		return accountLineDao.findAccPayableExtractLevel(corpId, payPostingDate, positiveExpense);
	}
	public List regenrateGPPayableExtractLevel(String corpId, String payPostingDate, boolean positiveExpense, String payXfer){
		return accountLineDao.regenrateGPPayableExtractLevel(corpId, payPostingDate, positiveExpense,payXfer);
	}
	
	public int gpPayableinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String payPostingDate, String corpid) {
		return accountLineDao.gpPayableinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
	}

	public String getAccountingcode(String companyDivision, String sessionCorpID) {
		return accountLineDao.getAccountingcode(companyDivision, sessionCorpID); 
	} 
	
	public int regGpPayableinvoiceUpdate(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid){
		return accountLineDao.regGpPayableinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
	}
	public List getPerlineDetailForUTSIRecExtract(String recInvNumb,String recPostDate, String compCode, String corpid){
		return accountLineDao.getPerlineDetailForUTSIRecExtract(recInvNumb, recPostDate, compCode, corpid);
	}
	public List getSOLineForBatchInvoicing(String sessionCorpID,String billCode,String companyDivs,String accountInterface,String assignType,String batchJob,String pir,Boolean dmm,String recRateCurrency){
		return accountLineDao.getSOLineForBatchInvoicing(sessionCorpID, billCode, companyDivs, accountInterface,assignType,batchJob,pir,dmm,recRateCurrency);
	}
	public List getSOLineForBatchInvoicing1(String sessionCorpID,String billCode,String companyDivs,String accountInterface,String assignType,String batchJob,String pir,Boolean dmm){
		return accountLineDao.getSOLineForBatchInvoicing1(sessionCorpID, billCode, companyDivs, accountInterface,assignType,batchJob,pir,dmm);
	}
	public double getActualRevenueMGMT(Long sid, String sessionCorpID, String bookingAgentCode) {
		return accountLineDao.getActualRevenueMGMT(sid, sessionCorpID, bookingAgentCode);
	}

	public int updateGPPayablesUpload(String sessionCorpID, String actgcode, String vendorInvoice, String payPayablevia, String payPayableDate, BigDecimal payPayableAmount, String shipNumber, String paygl, String companyDivisionCode) {
		return accountLineDao.updateGPPayablesUpload(sessionCorpID, actgcode, vendorInvoice, payPayablevia, payPayableDate, payPayableAmount, shipNumber, paygl,  companyDivisionCode);
	}

	public int updateGPReceivableUpload(String sessionCorpID, String recInvoiceNumber, String billToCode, String statusDate, BigDecimal receivedAmount, String systemDefaultmiscVl, String recGl, String externalReference, String jobtypeCode, String companyDivisionCode) {
		return accountLineDao.updateGPReceivableUpload(sessionCorpID, recInvoiceNumber, billToCode, statusDate, receivedAmount, systemDefaultmiscVl, recGl, externalReference, jobtypeCode, companyDivisionCode);
	}

	public Integer checkCountForDiscoutFee(Long sid, String bookingAgentCode) {
		return accountLineDao.checkCountForDiscoutFee(sid, bookingAgentCode);
	}

	public Integer checkCountForInvoiceDiscoutFee(Long sid, String bookingAgentCode, String externalCorpID) {
		return accountLineDao.checkCountForInvoiceDiscoutFee(sid, bookingAgentCode, externalCorpID);
	}

	public boolean findOwneroperator(String vendorCode) {
		return accountLineDao.findOwneroperator(vendorCode);
	}
	public List findAllPayInvListServiceTaxAmt(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
		return accountLineDao.findAllPayInvListServiceTaxAmt(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
	}
	public List findAllPayInvListServiceTaxAmtMulTDS(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
		return accountLineDao.findAllPayInvListServiceTaxAmtMulTDS(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
	}
	public List findAllPayInvListInputServiceTaxAmt(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
		return accountLineDao.findAllPayInvListInputServiceTaxAmt(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
	}
	 public String vendorNameNew(String partnerCode,String corpid){
		 return accountLineDao.vendorNameNew(partnerCode, corpid);
	 }
	public int tallyPayInvoiceExtractUpdate(String fileName,String invCodeList, String recXferUser, String postDate,String sessionCorpID, String accShipNumber, String accVendorCode){
		return accountLineDao.tallyPayInvoiceExtractUpdate(fileName, invCodeList, recXferUser, postDate, sessionCorpID, accShipNumber, accVendorCode);
	}
	public List findChargeDetailFromSO(String contract, String chargeCode,String sessionCorpID, String jobType, String routing1, String companyDivision){
		return accountLineDao.findChargeDetailFromSO(contract, chargeCode, sessionCorpID, jobType, routing1, companyDivision);

	}

	public List getPerlineDetailForUTSIpayExtract(String payInvNumb,String payPostDate, String payCompanyCode, String sessionCorpID){
		return accountLineDao.getPerlineDetailForUTSIpayExtract(payInvNumb, payPostDate, payCompanyCode, sessionCorpID);
	}
	public List getBillToCode(String shipNumber,Long serviceOrderId,String networkPartnerCode){
		return accountLineDao.getBillToCode(shipNumber,serviceOrderId,networkPartnerCode);
	}
	public Set getAccountLineForInvoice(Long serviceOrderId,String billToCode){
		return accountLineDao.getAccountLineForInvoice(serviceOrderId,billToCode);
	}
	public List getPayableExtractRegenerate(String payCompanyCode,String corpid,String payPostingDate,String payXfer){
		return accountLineDao.getPayableExtractRegenerate(payCompanyCode, corpid, payPostingDate, payXfer);
	}
	public String getRecordPayableSolomonDate(String payableFileName,String sessionCorpID){
		return accountLineDao.getRecordPayableSolomonDate(payableFileName, sessionCorpID);
	}
	public List regeneratePayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense, String payXfer){
		return accountLineDao.regeneratePayableExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense, payXfer);
	}
	public int regenerateSolomonPayableinvoiceUpdate(String batchName, String fileName, String payXferUser, String corpid){
		return accountLineDao.regenerateSolomonPayableinvoiceUpdate(batchName, fileName, payXferUser, corpid);
	}
	public List totalRevenueReg(String invoiceNumber,String recPostingDate,String corpid, String invoiceFileName){
		return accountLineDao.totalRevenueReg(invoiceNumber, recPostingDate, corpid,invoiceFileName);
	}

	public List getAllAccountLineDMM(Long sid) {
		return accountLineDao.getAllAccountLineDMM(sid);
	}

	public List getAllDiscounteesAccountLineUtsi(Long sid, String bookingAgentCode, String sessionCorpID) {
		return accountLineDao.getAllDiscounteesAccountLineUtsi(sid, bookingAgentCode, sessionCorpID);
	}
	
	public int setActivateAccPortal(String lineList,String sessionCorpID, boolean soNetworkGroup){
		return accountLineDao.setActivateAccPortal(lineList, sessionCorpID,  soNetworkGroup);
	}
	public List totalDistributionAmountReg(String sameInvoice, String recPostingDate, String sessionCorpID, String invoiceFileName){
		return accountLineDao.totalDistributionAmountReg(sameInvoice, recPostingDate, sessionCorpID,invoiceFileName);
	}
	public int updateTallyReset(String fileName,String sessionCorpID)
	{
		return accountLineDao.updateTallyReset(fileName, sessionCorpID);
	}
	public int updateTallyPayableNavision(String fileName,String sessionCorpID)
	{
		return accountLineDao.updateTallyPayableNavision(fileName, sessionCorpID);
	}
	public List getAllAccountLineAgent(Long sid) {
		return accountLineDao.getAllAccountLineAgent(sid);
	}
	public List findToolTipExpRevenue(Long aid,String fieldNames){
		return accountLineDao.findToolTipExpRevenue(aid, fieldNames);
	}
	
	public List getRegenerateInvoiceAccListForTally(String invoiceCompanyCode,String sessionCorpID, String fileName){
		   return accountLineDao.getRegenerateInvoiceAccListForTally(invoiceCompanyCode, sessionCorpID, fileName);
	   }
	 public List findTallyAllInvListGlCode(String invoiceCompanyCode,String corpId, String fileName, String invNumber){
		   return accountLineDao.findTallyAllInvListGlCode(invoiceCompanyCode, corpId, fileName, invNumber);
	   }
	 public List findTallyAllInvListServiceTaxAmt(String invoiceCompanyCode,String corpId, String fileName, String invNumber){
		   return accountLineDao.findTallyAllInvListServiceTaxAmt(invoiceCompanyCode, corpId, fileName, invNumber);
	   }
	 public int tallyRegenerateInvoiceExtractUpdate(String batchName,String accIdList,String recXferUser,String recPostingDate,String corpid){
		   return accountLineDao.tallyRegenerateInvoiceExtractUpdate(batchName, accIdList, recXferUser, recPostingDate, corpid);
	   }
	 public String getRecordPayableNavisionDate(String fileName, String sessionCorpID){
			return accountLineDao.getRecordPayableNavisionDate(fileName,sessionCorpID);
		}
	 public List getRegeneratePayAccListForTally(String invoiceCompanyCode,String sessionCorpID, String postDate){
		   return accountLineDao.getRegeneratePayAccListForTally(invoiceCompanyCode, sessionCorpID, postDate);
	   }
	   public List findAllRegeneratePayInvListGlCode(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
		   return accountLineDao.findAllRegeneratePayInvListGlCode(invoiceCompanyCode, corpId, postingDates, invNumber, vendorCode);
	   }
		public List findAllRegeneratePayInvListServiceTaxAmt(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
			return accountLineDao.findAllRegeneratePayInvListServiceTaxAmt(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
		}
		public List findAllRegeneratePayInvListInputServiceTaxAmt(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
			return accountLineDao.findAllRegeneratePayInvListInputServiceTaxAmt(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
		}
		public List findAllRegeneratePayInvListServiceTaxAmtMulTDS(String invoiceCompanyCode,String corpId, String postingDates, String invNumber,String vendorCode){
			return accountLineDao.findAllRegeneratePayInvListServiceTaxAmtMulTDS(invoiceCompanyCode, corpId, postingDates, invNumber,vendorCode);
		}
		public int tallyRegeneratePayInvoiceExtractUpdate(String fileName,String invCodeList, String recXferUser, String postDate,String sessionCorpID){
			return accountLineDao.tallyRegeneratePayInvoiceExtractUpdate(fileName, invCodeList, recXferUser, postDate, sessionCorpID);
		}
		public List getACInvNumber(String vendorCode,String invNumber){
			return accountLineDao.getACInvNumber(vendorCode,invNumber);
		}
		public List getPayInvNumber(String vendorCode,String invNumber){
			return accountLineDao.getPayInvNumber(vendorCode,invNumber);
		}
		public List getACRecInvNumber(String vendorName,String invNumber, String billtocode){
			return accountLineDao.getACRecInvNumber(vendorName,invNumber,  billtocode);
		}
		public List getRecFullyPaidList(String vendorName,String invNumber, String billtocode){
			return accountLineDao.getRecFullyPaidList(vendorName,invNumber,  billtocode);
		}
		public String getAccInvoiceNumber(String shipNumber, String sessionCorpID) {
			return accountLineDao.getAccInvoiceNumber(shipNumber, sessionCorpID);
		}
		
		/* Added by Kunal for ticket number: 6926*/
		public List minSequence(String beginDate, String endDate, String corpID){
			return accountLineDao.minSequence(beginDate, endDate, corpID);
		}

		public List maxSequence(String beginDate, String endDate, String corpID){
			return accountLineDao.maxSequence(beginDate, endDate, corpID);
		}
		
		public List missingInvoices(String beginDate, String endDate, String corpID){
			return accountLineDao.missingInvoices(beginDate, endDate, corpID);
		}
		/* Ends */
		public List findPoNumber(String sessionCorpID,String shipNumberForTickets){
			return accountLineDao.findPoNumber(sessionCorpID,shipNumberForTickets);
		}
		public List findShipNumByInvoiceId( String invoiceNo){
			return accountLineDao.findShipNumByInvoiceId(invoiceNo);
		}

		public int updateNetworkAgentSynchedId(Long accountLineId, Long accountLineAgentId, boolean agentInvoiceSeed, String invoiceNumber) {
			return accountLineDao.updateNetworkAgentSynchedId(accountLineId, accountLineAgentId,agentInvoiceSeed,invoiceNumber);
		}
		public void updateAccountLineField(Long aid, String accEstimate, String sessionCorpID){
			accountLineDao.updateAccountLineField(aid, accEstimate, sessionCorpID);
		}
		public void updateAccline(Long aid, String field,String type,String sessionCorpID){
			accountLineDao.updateAccline(aid, field, type, sessionCorpID);
		}
		public void updatePaymentStatusToPIR(String aidList,String sessionCorpID,String companyDiv,String pir){
			accountLineDao.updatePaymentStatusToPIR(aidList, sessionCorpID,companyDiv,pir);
		}
		public List getRemoteAccountLine(Long sid){
			return accountLineDao.getRemoteAccountLine(sid);
		}
		public List getGPInvoicePostDataList(String sessionCorpID) {
			return accountLineDao.getGPInvoicePostDataList(sessionCorpID);
		}

		public List getGPPayablePostDataList(String sessionCorpID) {
			return accountLineDao.getGPPayablePostDataList(sessionCorpID);
		}
		public List getAccPayablePostDataList(String sessionCorpID){
			return accountLineDao.getAccPayablePostDataList(sessionCorpID);
		}
		public String getNetworkSynchedIdCreate(Long id, String sessionCorpID, String agentCode) {
			return accountLineDao.getNetworkSynchedIdCreate(id, sessionCorpID, agentCode);
		}
		public List validateChargeVatExclude(String shipNumber,String sessionCorpID){
			return accountLineDao.validateChargeVatExclude(shipNumber, sessionCorpID);
		}
		public String getListOfAccList(String sessionCorpID,String vendorCode,String invoiceNumber,String invoiceDate,String currency,String idss){
			return accountLineDao.getListOfAccList(sessionCorpID, vendorCode, invoiceNumber, invoiceDate, currency,idss);
		}
		public String vendorInvoiceEnhancementList(String sessionCorpID,String vendorCode,String invoiceNumber,String idss){
			return accountLineDao.vendorInvoiceEnhancementList(sessionCorpID,vendorCode,invoiceNumber,idss);
		}
		public List checkPurchasePostingMethod(String sessionCorpID,String shipNumber){
			return accountLineDao.checkPurchasePostingMethod(sessionCorpID, shipNumber);
		}
		public String checkDriverForMoreThanOneAjax(String sessionCorpID,String shipNumber){
			return accountLineDao.checkDriverForMoreThanOneAjax(sessionCorpID, shipNumber);
		}
		public List findAccLineWithDateRanage(String sessionCorpID,	String fromDt, String toDt) {
			return accountLineDao.findAccLineWithDateRanage(sessionCorpID, fromDt,toDt);
		}
		public List<AccountLine> findAllAccountLineBySID(Long sid,String sessionCorpId){
			return accountLineDao.findAllAccountLineBySID(sid, sessionCorpId);
		}

		public List findAccountLinebyBilltocode(Long sid, String billToCode, String sessionCorpID) {
			return accountLineDao.findAccountLinebyBilltocode(sid, billToCode, sessionCorpID);
		}

		public List findAccountLinebyVendorCode(Long sid, String vendorCode, String sessionCorpID) {
			return accountLineDao.findAccountLinebyVendorCode(sid, vendorCode, sessionCorpID);
		}
		public List getDriverCommission(BigDecimal actualRevenue,String chargeCode, String vendorCode,String contract,BigDecimal accountLineDistribution,BigDecimal accountEstimateRevenueAmount,BigDecimal accountRevisionRevenueAmount,String sessionCorpID,BigDecimal actualLineHaul){
			return accountLineDao.getDriverCommission(actualRevenue,chargeCode,vendorCode,contract,accountLineDistribution,accountEstimateRevenueAmount, accountRevisionRevenueAmount,sessionCorpID,actualLineHaul);
		}
		public List getTotalDistributionInvoiceList(String shipNumber, String corpId,String divisionFlag){
			return accountLineDao.getTotalDistributionInvoiceList(shipNumber,corpId,divisionFlag);
		}

		public List findAccountLinebyContract(Long sid, String sessionCorpID) {
			return accountLineDao.findAccountLinebyContract(sid, sessionCorpID);
		}

		public String getOwnbillingAccInvoiceNumber(String shipNumber, String sessionCorpID, String billToCode) {
			return accountLineDao.getOwnbillingAccInvoiceNumber(shipNumber, sessionCorpID, billToCode);
		}
		public int updateSacom2ComDiv(String sessionCorpID,Long sid,String comDiv){
			return accountLineDao.updateSacom2ComDiv(sessionCorpID, sid, comDiv);
		}

		public List getAllUTSIAccountLine(Long id) {
			return accountLineDao.getAllUTSIAccountLine(id);
		}
		
		public List getAllPreviewLine(String sessionCorpID,Long sid){
			return accountLineDao.getAllPreviewLine(sessionCorpID, sid);
		}
		public Map<String, String> paymentApplicationInvoiceExtract(String vanlineJob,String corpId,String recPostingDates,String companyDiv,String vanLineJob){
			return accountLineDao.paymentApplicationInvoiceExtract(vanlineJob, corpId, recPostingDates,companyDiv,vanLineJob);
		}
		public Map<String, String> creditMemoApplicationInvoiceExtract(String vanlineJob,String corpId,String recPostingDates,String companyDiv,String vanLineJob)
		{
			return accountLineDao.creditMemoApplicationInvoiceExtract(vanlineJob, corpId, recPostingDates,companyDiv,vanLineJob);
		}
		public int removeZeroLineEntryForNewCommission(String sessionCorpID,Long sid,String comDiv){
			return accountLineDao.removeZeroLineEntryForNewCommission(sessionCorpID, sid, comDiv);
		}

		public String findCreditInvoice(Long soId, String billtocode, double actualRevenue, String chargeCodeList,String sessionCorpID) {
			return accountLineDao.findCreditInvoice(soId, billtocode, actualRevenue, chargeCodeList,sessionCorpID);
		}
		public void updateCommisionLockingTable(String id, String corpId,String loginUser){
			accountLineDao.updateCommisionLockingTable(id, corpId,loginUser);
		}
		public BigDecimal getProjectedSum(String shipNumber,String type){
			return accountLineDao.getProjectedSum(shipNumber, type);
		}

		public String getUTSICompanyDivision(String externalCorpID, String billToCode) {
			return accountLineDao.getUTSICompanyDivision(externalCorpID, billToCode);
		}

		public List getAccountLineForVanLine(Long sid, String accountLineNumber, String sessionCorpID) {
			return accountLineDao.getAccountLineForVanLine(sid, accountLineNumber, sessionCorpID);
		}

		public List findDSTRAccountLine(Date weekEnding, String sessionCorpID,Long sid) {
			return accountLineDao.findDSTRAccountLine(weekEnding, sessionCorpID, sid);
		}

		public List getAllDiscounteesAccountLineVat(Long sid, String bookingAgentCode, String externalCorpID) { 
			return accountLineDao.getAllDiscounteesAccountLineVat(sid, bookingAgentCode, externalCorpID);
		}

		public List invoiceSAPUGCAExtract(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate) {
			return accountLineDao.invoiceSAPUGCAExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
		}

		public List findPayableExtractNavisionLevelForUGCA( String payCompanyCode, String corpId, boolean positiveExpense) {
			return accountLineDao.findPayableExtractNavisionLevelForUGCA(payCompanyCode, corpId, positiveExpense);
		}

		public List regenerateInvoiceSAPUGCA(String fileName, String sessionCorpID) {
			return accountLineDao.regenerateInvoiceSAPUGCA(fileName, sessionCorpID);
		}

		public List regeneratePayableExtractSAPUGCALevel(String fileName, String corpId, boolean positiveExpense) {
			return accountLineDao.regeneratePayableExtractSAPUGCALevel(fileName, corpId, positiveExpense);
		}

		public List findCMMAgentBilltoCode(String sessionCorpID, Long id) {
			return accountLineDao.findCMMAgentBilltoCode(sessionCorpID, id);
		}

		public List findValidateChargeVatExclude(Long sid, String sessionCorpID) {
			return accountLineDao.findValidateChargeVatExclude(sid, sessionCorpID);
		}

		public List updateSelectiveInvoice(Long sid, Long id, boolean selectiveInvoice, String sessionCorpID) { 
			return accountLineDao.updateSelectiveInvoice(sid, id, selectiveInvoice, sessionCorpID);
		}
		public void updateMyfileVender(String venderCode,String payingStatus,String myfileId,String sessionCorpID){
			accountLineDao.updateMyfileVender(venderCode,payingStatus, myfileId, sessionCorpID);
		}
		public String getVendorInfo(String vendorCode, String sessionCorpID,String compDiv){
			return accountLineDao.getVendorInfo(vendorCode, sessionCorpID, compDiv);
		}

		public Map<String, String> findContractPayClearing(String sessionCorpID) {
			return accountLineDao.findContractPayClearing(sessionCorpID);
		}

		public void updateSelectiveInvoice(Long id, String sessionCorpID) {
			accountLineDao.updateSelectiveInvoice(id, sessionCorpID);
			
		}

		public List getDriverPayableExtract(String payCompanyCode, String corpid, String payPostingDate, String startInvoiceDate, String endInvoiceDate) {
			return 	accountLineDao.getDriverPayableExtract(payCompanyCode, corpid, payPostingDate,startInvoiceDate, endInvoiceDate);
			 
		}
		public Map<String, String> getDocumentList(String sessionCorpID,String venderCode,String sequenceNumber){
			return accountLineDao.getDocumentList(sessionCorpID,venderCode, sequenceNumber);
		}
		public List findDriverPayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense, String startInvoiceDate, String endInvoiceDate) {
			return 	accountLineDao.findDriverPayableExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense,  startInvoiceDate,  endInvoiceDate);
		}

		public int payableDriverinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String payPostingDate, String corpid) {
			return 	accountLineDao.payableDriverinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
		}

		public boolean findActivateAccPortal(String job, String companyDivision, String sessionCorpID) {
			return 	accountLineDao.findActivateAccPortal(job, companyDivision, sessionCorpID);
		}

		public List findDriverPayableExtractList(String sessionCorpID) {
			return 	accountLineDao.findDriverPayableExtractList(sessionCorpID);
		}

		public void updateWorkTicketInvoice(String userCheck, String shipNumberForTickets, String sessionCorpID, String loginUser, String newInvoiceNumber) {
				accountLineDao.updateWorkTicketInvoice(userCheck, shipNumberForTickets, sessionCorpID, loginUser, newInvoiceNumber);
			
		}

		public boolean findNetworkAgentInvoiceFlag(Long networkAgentId) {
			return 	accountLineDao.findNetworkAgentInvoiceFlag(networkAgentId);
		}
		/*public void updateSOExtFalg(Long id){
			accountLineDao.updateSOExtFalg(id);
		}*/
		public String findPrivatePartyExistInBilling(String partnerCode,String corpId){
			return accountLineDao.findPrivatePartyExistInBilling(partnerCode,corpId);
		}

		public int updateInvoicepaymentSentDate(List recivedAmtIdList, String sessionCorpID, String vanLineJob) {
			return accountLineDao.updateInvoicepaymentSentDate(recivedAmtIdList, sessionCorpID, vanLineJob);
		}
		public List claimName(String jobType,String compDiv, String sessionCorpID){
			 return accountLineDao.claimName(jobType, compDiv, sessionCorpID);
		 }
		public int updateInvoiceCreditSentDate(List recivedAmtIdList, String sessionCorpID) {
			return accountLineDao.updateInvoiceCreditSentDate(recivedAmtIdList, sessionCorpID);
		}
		public String findRollUpInvoiceValFromCharge(String contractVal, String chargeCodeval, String sessionCorpID){
			return accountLineDao.findRollUpInvoiceValFromCharge(contractVal,chargeCodeval, sessionCorpID);
		}

		public List getAFASInvoicePostDataList(String sessionCorpID) {
			return accountLineDao.getAFASInvoicePostDataList(sessionCorpID);
		}

		public List AFASInvoiceExtract(String sessionCorpID, String recPostingDate) {
			return accountLineDao.AFASInvoiceExtract(sessionCorpID, recPostingDate);
		}

		public List totalAactualRevenueSameVat(String invoiceNumber, String recPostingDate, String corpid, String sameVatNumber) {
			return accountLineDao.totalAactualRevenueSameVat(invoiceNumber, recPostingDate, corpid, sameVatNumber);
		}

		public Long findCreditInvoiceNumber(String sessionCorpID) {
			return accountLineDao.findCreditInvoiceNumber(sessionCorpID);
		}
		public void updateAccountAuditCompleteDate(Long sid,String auditComplete, String updatedBy, boolean soNetwork, boolean accNetwork,String sessionCorpID){
			accountLineDao.updateAccountAuditCompleteDate(sid,auditComplete,updatedBy,  soNetwork,  accNetwork, sessionCorpID);
		}

		public List findNonLinkedAccountline(String beginDates, String endDates) {  
			return accountLineDao.findNonLinkedAccountline(beginDates, endDates);
		}
		public List getInvoicePostDataListDASA(String corpId) {
			return accountLineDao.getInvoicePostDataListDASA(corpId);
		}
		
		
		 public List invoiceExtractDASA(String batchName,String invoiceCompanyCode,String corpid,String recPostingDate)
		   {
			   return accountLineDao.invoiceExtractDASA( batchName,invoiceCompanyCode,corpid,recPostingDate);
		   }
		 
		 public List gettotalDistributionAmountDASA(String invoiceNumber,String recPostingDate,String corpid) {
				return accountLineDao.gettotalDistributionAmountDASA(invoiceNumber,recPostingDate,corpid);
			}
		 
		 public List gettotalRevenue(String invoiceNumber,String recPostingDate,String corpid)
		   {
			   return accountLineDao.gettotalRevenue(invoiceNumber,recPostingDate,corpid);
		   } 
		  
		 public int invoiceExtractUpdateDASA(String batchName,String shipInvoiceUpdate,String recXferUser,String recPostingDate,String corpid)
		   {
			   return accountLineDao.invoiceExtractUpdateDASA(batchName,shipInvoiceUpdate,recXferUser,recPostingDate,corpid) ;
		   }
		 
		 public List findPayableExtractLevelDASA(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense) {
				return accountLineDao.findPayableExtractLevelDASA(payCompanyCode, corpId, payPostingDate,  positiveExpense);
			}
		 
		 public int payableinvoiceUpdateDASA(String batchName,String shipnumber,String payXferUser,String payPostingDate,String corpid) {
				return accountLineDao.payableinvoiceUpdateDASA(batchName,shipnumber,payXferUser,payPostingDate,corpid);
			} 
		 
		 public List getPayableExtractDASA(String payCompanyCode,String corpid,String payPostingDate) {
				return accountLineDao.getPayableExtractDASA(payCompanyCode,corpid,payPostingDate);
			}
		 
		 public List getPayablePostDataListDASA(String corpId){
			 return accountLineDao.getPayablePostDataListDASA(corpId);
		 }
		 
		 public int updateTallyResetDASA(String fileName,String sessionCorpID)
			{
				return accountLineDao.updateTallyResetDASA(fileName, sessionCorpID);
			}
		 public String getRecordPostingDateDASA(String fileName, String sessionCorpID){
				return accountLineDao.getRecordPostingDateDASA(fileName,sessionCorpID);
			}
		 public List getRegenerateInvoiceAccListForTallyDASA(String invoiceCompanyCode,String sessionCorpID, String fileName){
			   return accountLineDao.getRegenerateInvoiceAccListForTallyDASA(invoiceCompanyCode, sessionCorpID, fileName);
		   }
		 public int tallyRegenerateInvoiceExtractUpdateDASA(String batchName,String accIdList,String recXferUser,String recPostingDate,String corpid){
			   return accountLineDao.tallyRegenerateInvoiceExtractUpdateDASA(batchName, accIdList, recXferUser, recPostingDate, corpid);
		   }
		 public int updateTallyPayableNavisionDASA(String fileName,String sessionCorpID)
			{
				return accountLineDao.updateTallyPayableNavisionDASA(fileName, sessionCorpID);
			}
		 public String getRecordPayableNavisionDateDASA(String fileName, String sessionCorpID){
				return accountLineDao.getRecordPayableNavisionDateDASA(fileName,sessionCorpID);
			}
		 public List getRegeneratePayAccListForTallyDASA(String invoiceCompanyCode,String sessionCorpID, String filename){
			   return accountLineDao.getRegeneratePayAccListForTallyDASA(invoiceCompanyCode, sessionCorpID, filename);
		   }
		 public int tallyRegeneratePayInvoiceExtractUpdateDASA(String fileName,String invCodeList, String recXferUser, String postDate,String sessionCorpID){
				return accountLineDao.tallyRegeneratePayInvoiceExtractUpdateDASA(fileName, invCodeList, recXferUser, postDate, sessionCorpID);
			}
		 public String accLineList(String shipNumber){
			 return accountLineDao.accLineList(shipNumber);
		 }
		 public Map<String, String> getAccountingcodeMap(String sessionCorpID){
			 return accountLineDao.getAccountingcodeMap(sessionCorpID);
		 }

		public List getDriverPayableMergeExtract(String payCompanyCode, String corpid, String payPostingDate, String startInvoiceDate, String endInvoiceDate) {
			return accountLineDao.getDriverPayableMergeExtract(payCompanyCode, corpid, payPostingDate, startInvoiceDate, endInvoiceDate);
		}

		public List findDriverPayableMergeExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense, String startInvoiceDate, String endInvoiceDate) {
			return accountLineDao.findDriverPayableMergeExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense, startInvoiceDate, endInvoiceDate);
		}

		public List getSSCWGPInvoicePostDataList(String sessionCorpID) {
			return accountLineDao.getSSCWGPInvoicePostDataList(sessionCorpID);
		}

		public List invoiceSSCWGPExtract(String batchName, String invoiceCompanyCode, String corpid, String recPostingDate) {
			return accountLineDao.invoiceSSCWGPExtract(batchName, invoiceCompanyCode, corpid, recPostingDate);
		}

		public List totalSSCWGPDistributionAmount(String invoiceNumber, String recPostingDate, String corpid, String companyDivision) {
			return accountLineDao.totalSSCWGPDistributionAmount(invoiceNumber, recPostingDate, corpid, companyDivision); 
		}

		public List totalSSCWGPRevenue(String invoiceNumber, String recPostingDate, String corpid, String companyDivision) {
			return accountLineDao.totalSSCWGPRevenue(invoiceNumber, recPostingDate, corpid, companyDivision);
		}

		public int invoiceSSCWGPExtractUpdate(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid, String comDiv) {
			return accountLineDao.invoiceSSCWGPExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid, comDiv);
		}

		public List getSSCWGPPayablePostDataList(String sessionCorpID) {
			return accountLineDao.getSSCWGPPayablePostDataList(sessionCorpID);
		}

		public List getPayableSSCWGPExtract(String payCompanyCode, String corpid, String payPostingDate) {
			return accountLineDao.getPayableSSCWGPExtract(payCompanyCode, corpid, payPostingDate);
		}

		public List findSSCWGPPayableExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense) {
			return accountLineDao.findSSCWGPPayableExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense);
		}

		public int payableSSCWGPinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String payPostingDate, String corpid, String compDiv) {
			return accountLineDao.payableSSCWGPinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid, compDiv);
		}

		public List getDriverPayableSSCWGPExtract(String payCompanyCode, String corpid, String payPostingDate, String startInvoiceDate, String endInvoiceDate) {
			return accountLineDao.getDriverPayableSSCWGPExtract(payCompanyCode, corpid, payPostingDate, startInvoiceDate, endInvoiceDate);
		}

		public List findDriverPayableSSCWGPExtractLevel(String payCompanyCode, String corpId, String payPostingDate, boolean positiveExpense, String startInvoiceDate, String endInvoiceDate) {
			return accountLineDao.findDriverPayableSSCWGPExtractLevel(payCompanyCode, corpId, payPostingDate, positiveExpense, startInvoiceDate, endInvoiceDate);
		}

		public int payableDriverSSCWGPinvoiceUpdate(String batchName, String shipnumber, String payXferUser, String payPostingDate, String corpid) {
			return accountLineDao.payableDriverSSCWGPinvoiceUpdate(batchName, shipnumber, payXferUser, payPostingDate, corpid);
		}
		
		public List getVendorInvoicesForUnposting(String shipNumber,String corpId){
			return accountLineDao.getVendorInvoicesForUnposting(shipNumber,corpId);
		}
		
		public void updatePayaccAndPostdate(String shipNumber,String stringAid,String updatedBy){
			accountLineDao.updatePayaccAndPostdate(shipNumber,stringAid,updatedBy);
		}
		public String checkZeroforInvoice(Long sid,String invoiceBillToCode){
			return accountLineDao.checkZeroforInvoice(sid, invoiceBillToCode);
		}
		public void updateCheckBoxValueInAcListAjax(String updateQuery){
			accountLineDao.updateCheckBoxValueInAcListAjax(updateQuery);
		}
		public List<AccountLine> findAccountLineListByFilter(String accountLineQuery,String accountLineStatus,String accountLineOrderByVal){
			return accountLineDao.findAccountLineListByFilter(accountLineQuery, accountLineStatus, accountLineOrderByVal);
		}
		public List findDistinctFieldValue(String shipNumber,String fieldName,String accountLineStatus){
			return accountLineDao.findDistinctFieldValue(shipNumber,fieldName, accountLineStatus);
		}
		public void updatePayingSatus(String vendorCode,String invoiceNum ,String shipNumber,String updatedBy,String corpid){
			accountLineDao.updatePayingSatus(vendorCode,invoiceNum,shipNumber,updatedBy,corpid);
		}
		public String getAgentCompanyDivCode(String bookCode,String originCode,String destinCode,String haulCode,String corpid){
			return accountLineDao.getAgentCompanyDivCode(bookCode,originCode, destinCode,haulCode, corpid);
		}
		public List getEstimateExpenseForMarkUpCalculation(String shipNumber,String corpId){
			return accountLineDao.getEstimateExpenseForMarkUpCalculation(shipNumber,corpId);
		}
		public List getAccountLineIdList(String shipNumber){
			return accountLineDao.getAccountLineIdList(shipNumber);
		}
		public BigDecimal findSumOfEstimateExpense(String shipNumber){
			return accountLineDao.findSumOfEstimateExpense(shipNumber);
		}

		public void updateCMMAgentBillToCode(String sessionCorpID, Long serviceOrderId) {
			accountLineDao.updateCMMAgentBillToCode(sessionCorpID, serviceOrderId);
			
		}
		
		public String updateInvoiceNumberByCompanyDivision(String shipNumber, String recDate, String billtoCode, String authNumber, boolean useDistAmt, String corpId, String loginUser, String creditInvoiceNumber, String accountInterface, String jobtype, boolean soNetworkGroup, String contractTypeValue, boolean accNetworkGroup, String routing, String contract, String bookingAgentShipNumber, String recInvoiceDateToFormat, boolean networkAgent, String payAccDateTo, String companyDivision, String SoInvoiceNumber, String invoicereportparameter) {
			return accountLineDao.updateInvoiceNumberByCompanyDivision(shipNumber, recDate, billtoCode, authNumber, useDistAmt, corpId, loginUser, creditInvoiceNumber, accountInterface, jobtype, soNetworkGroup, contractTypeValue, accNetworkGroup, routing, contract, bookingAgentShipNumber, recInvoiceDateToFormat, networkAgent, payAccDateTo, companyDivision, SoInvoiceNumber, invoicereportparameter);
		}
		public List searchInvoice(String invoiceNum,String corpID)
		   {
			   return accountLineDao.searchInvoice(invoiceNum,corpID);
		   }
		public int updateAuthorizaton(String invoiceNum, String authorizationNum, String sessionCorpID) {
			   return accountLineDao.updateAuthorizaton(invoiceNum,authorizationNum,  sessionCorpID) ;

		}

		@Override
		public Date findCurrentInvoicePostingDate(Long sid, String sessionCorpID) {
			 return accountLineDao.findCurrentInvoicePostingDate(sid, sessionCorpID);
		}

		public List getInvoiceMasPostDataList(String sessionCorpID) {
			return accountLineDao.getInvoiceMasPostDataList(sessionCorpID);
		}

		public List getMasPayablePostDataList(String sessionCorpID) {
			return accountLineDao.getMasPayablePostDataList(sessionCorpID);
		}

		public List invoiceMasExtract(String sessionCorpID, String recPostingDate) {
			return accountLineDao.invoiceMasExtract(sessionCorpID, recPostingDate);
		}

		public List findPayableExtractMasLevel(String corpId, boolean positiveExpense, String postingDate) {
			return accountLineDao.findPayableExtractMasLevel(corpId, positiveExpense, postingDate);
		}

		public List getMasPayableExtract(String payPostingDateFormat, String sessionCorpID) {
			return accountLineDao.getMasPayableExtract(payPostingDateFormat, sessionCorpID);
		}

		public int masPayableinvoiceUpdate(String batchName, String accid, String payXferUser, String corpid) {
			return  accountLineDao.masPayableinvoiceUpdate(batchName, accid, payXferUser, corpid);
		}

		public int invoiceMasExtractUpdate(String batchName, String accIdUpdate, String recXferUser, String recPostingDate, String corpid) {
			return accountLineDao.invoiceMasExtractUpdate(batchName, accIdUpdate, recXferUser, recPostingDate, corpid);
		}

		public List getMasACcRecInvNumber(String companyDivision,String billtoCode, String recInvoiceNumber,String corpid) {
			return accountLineDao.getMasACcRecInvNumber(companyDivision,billtoCode, recInvoiceNumber, corpid);
		}

		public List getMasACcPayInvNumber(String companyDivision,String vendorCode, String invoiceNumber, String sessionCorpID) {
			return accountLineDao.getMasACcPayInvNumber(companyDivision,vendorCode, invoiceNumber, sessionCorpID);
		}

		public List getMasRecFullyPaidList(String companyDivision,String billtoCode, String recInvoiceNumber, String sessionCorpID) {
			return accountLineDao.getMasRecFullyPaidList(companyDivision,billtoCode, recInvoiceNumber, sessionCorpID); 
		}

		public List getMasPayFullyPaidList(String companyDivision,String vendorCode, String invoiceNumber, String sessionCorpID) {
			return accountLineDao.getMasPayFullyPaidList( companyDivision,vendorCode, invoiceNumber, sessionCorpID);
		}

		public List getCreditInvoiceNumberDatabyInvoice( String creditInvoiceNumber, String invoiceShipNumber, String sessionCorpID) {
			return accountLineDao.getCreditInvoiceNumberDatabyInvoice(creditInvoiceNumber, invoiceShipNumber, sessionCorpID);
		}
		
		//13674 - Create dashboard
		public List<AccountLine> getAccountLineList(Long id) {
			
			return accountLineDao.getAccountLineList(id);
		}
		public List<AccountLine> getTotalrevenueList(Long id) {
			
			return accountLineDao.getTotalrevenueList(id);
		}
		public List invoiceVismaExtract(String corpid, String recPostingDate) { 
			return accountLineDao.invoiceVismaExtract(corpid, recPostingDate);
		}

		public int invoiceVismaExtractUpdate(String batchName, String shipInvoiceUpdate, String recXferUser, String recPostingDate, String corpid) {
			return accountLineDao.invoiceVismaExtractUpdate(batchName, shipInvoiceUpdate, recXferUser, recPostingDate, corpid);
		}

		public List getVismRecFullyPaidList(String billtoCode, String recInvoiceNumber, String sessionCorpID) {
			return accountLineDao.getVismRecFullyPaidList(billtoCode, recInvoiceNumber, sessionCorpID);
		}

		public List getVismACcRecInvNumber(String billtoCode, String recInvoiceNumber, String sessionCorpID) {
			return accountLineDao.getVismACcRecInvNumber(billtoCode, recInvoiceNumber, sessionCorpID);
		}
		public List getAgentLineForBatchInvoicing(String sessionCorpID,String vendorCode,String vendorInvoice,String accountInterface,String currency,String fieldName,String shipNumber){
			return accountLineDao.getAgentLineForBatchInvoicing(sessionCorpID, vendorCode, vendorInvoice,accountInterface,currency,fieldName,shipNumber);
		}
		public int saveAgentLineBatchPayableForInvoiceNoDate(String sessionCorpID,String invoiceNumberListServer,String actualExpenseListServer,String payingStatusListServer,Date invoiceDate,Date recievedDate,String exchangeRate,String vendorInvoice, String payVatDescrListServer, String payVatPerListServer, String payVatAmtListServer,String actgCodeListServer)
        {
        	return accountLineDao.saveAgentLineBatchPayableForInvoiceNoDate(sessionCorpID, invoiceNumberListServer, actualExpenseListServer, payingStatusListServer, invoiceDate, recievedDate, exchangeRate, vendorInvoice, payVatDescrListServer, payVatPerListServer, payVatAmtListServer, actgCodeListServer);
			
		}
		public void updateAccountLinepayingStatusField(String aid, String payingStatusValue, String sessionCorpID,Date payDate){
			accountLineDao.updateAccountLinepayingStatusField(aid, payingStatusValue, sessionCorpID, payDate);
		}
		   public List  findByPayingStatusAccountLines(String shipNumber) { 
		        return accountLineDao.findByPayingStatusAccountLines(shipNumber); 
		    }
		 
		   public List checkRegisteredInvoice(String vendorCode,String corpid,String invoiceNumber){	
			   return accountLineDao.checkRegisteredInvoice(vendorCode, corpid, invoiceNumber);
			   
		   }

		public Map<String, String> getExchangeRateWithCurrency(String corpid) {
			return accountLineDao.getExchangeRateWithCurrency(corpid);
		}
		public int checkInvoice(String sid, String rowId,String agentCorpId ){
			return accountLineDao.checkInvoice(sid, rowId,agentCorpId);
		
		
		}

		public String checkNotesStatusAcc(String sessionCorpID, String vendorCodeNew, String invoiceNumberNew, Long sidNum) {
			return accountLineDao.checkNotesStatusAcc(sessionCorpID, vendorCodeNew, invoiceNumberNew, sidNum);
		}
		public List  getAccountLinesCorpId() {
			return accountLineDao.getAccountLinesCorpId();
		}
		public List accExtract(String sessionCorpID,String shipNumber){
			return accountLineDao.accExtract(sessionCorpID, shipNumber);
		}

		public List getInvoicePostDataListTally(String corpId) {
			return accountLineDao.getInvoicePostDataListTally(corpId);
		}
		public List  checkInvoiceNumberStatus(String vendorCode,String corpid,String invoiceNumber){
		return accountLineDao.checkInvoiceNumberStatus(vendorCode, corpid, invoiceNumber);
		}
		public String  getCurrencyAccToVendorCode(String vendorCode,String corpid){
			return accountLineDao.getCurrencyAccToVendorCode(vendorCode, corpid);
		}
		public List  getAllCorpId() {
			
			return accountLineDao.getAllCorpId();
		}

		public List agentPayInviceList(String invoiceNumber ,String billToCode,String shipNumber,String corpID, String partnerCodePopup ,Date invoiceDate) {
			return accountLineDao.agentPayInviceList(invoiceNumber, billToCode, shipNumber, corpID, partnerCodePopup, invoiceDate);
		}

		public List searchInvoiceAjax(String shipNumber, String corpID) {
			// TODO Auto-generated method stub
			return accountLineDao.searchInvoiceAjax(shipNumber, corpID);
		}

		public List regenerateInvoiceSales(String fileName, String sessionCorpID) {
			// TODO Auto-generated method stub
			return accountLineDao.regenerateInvoiceSales(fileName, sessionCorpID);
		}

		public List findVendorPayableExtract(String corpId, boolean positiveExpense, String fileName) {
			// TODO Auto-generated method stub
			return accountLineDao.findVendorPayableExtract(corpId, positiveExpense, fileName);
		}

		public List totalDistributionSageAmountSales(String invoiceNumber, String recPostingDate, String corpid) {
			// TODO Auto-generated method stub
			return accountLineDao.totalDistributionSageAmountSales(invoiceNumber, recPostingDate, corpid );
		}

		public List totalRevenueSageSales(String invoiceNumber, String recPostingDate, String corpid) {
			// TODO Auto-generated method stub
			return accountLineDao.totalRevenueSageSales(invoiceNumber, recPostingDate, corpid );
		}

		public String getVenDorPostingDate(String fileName, String sessionCorpID) {
			// TODO Auto-generated method stub
			return accountLineDao.getVenDorPostingDate(fileName,sessionCorpID);
		}

		public List getMasPayableVendorExtract(String payPostingDateFormat, String sessionCorpID,String fileName) {
			// TODO Auto-generated method stub
			return accountLineDao.getMasPayableVendorExtract(payPostingDateFormat, sessionCorpID, fileName);
		}

		public List findEmployeeExpensesList(String userCheck,String sessionCorpID) {
			return accountLineDao.findEmployeeExpensesList(userCheck, sessionCorpID);
		}
		
}
 