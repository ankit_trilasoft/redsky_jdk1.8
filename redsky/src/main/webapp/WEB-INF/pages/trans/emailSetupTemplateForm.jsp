<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Email Setup Template Details</title>   
    <meta name="heading" content="Email Setup Template Details"/>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtooltip.css'/>" />
    <style type="text/css">
    	.setBG{width: 288px;  background: transparent -moz-linear-gradient(top, #feffff 0%, #cce2ea 100%); 
    	height: auto; max-height: 200px; padding-left:3px;line-height: 18px; border: 1px solid rgb(33, 157, 209);display: inline-block;margin-top: 2px;}
    	.table thead th, .tableHeaderTable td{height:18px;}
    	div.MainFile{ padding:3px 10px; background:#fff; border:1px solid #0099cc; position:relative;
  						color:#0099cc; border-radius:2px; text-align:left;  cursor:pointer; width:110px; font-weight:bold; font-family:arial,helvetica,serif; }
  		div.MainFile i{font-size:15px;vertical-align:tex-top;}
		.hide_file { position: absolute; z-index: 1000; opacity: 0; cursor: pointer; right: 0; top: 0; height: 100%; font-size: 24px; width: 100%;}
		.hidden{display:none;}
		.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
	</style>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tinymce.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
    <script type="text/javascript">
		 tinymce.init({
			    selector: '#emailBody',
			    theme: 'modern',
			    width: 660,
			    height: 205,
			    branding: false,
			    content_style: ".mce-content-body {font-size:10px;font-family:Arial,sans-serif;}",
			    plugins: [
			      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			      'save table contextmenu directionality template paste textcolor'
			    ],
			    //content_css:'redskyditor/css/content.css',
			    toolbar: 'fontselect fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview | forecolor backcolor ',
			    font_formats: "Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Open Sans=Open Sans,helvetica,sans-serif;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva",
				fontsize_formats: "8px 10px 12px 14px 16px 18px 20px",
				
				file_picker_callback: function(callback, value, meta) {
	                
                   $('#upload').trigger('click');
                   $('#upload').on('change', function() {
                        var file = this.files[0];
                        var parts = file.name.split('.');
                        var fileExt = parts[parts.length-1];
                        fileExt = fileExt.toLowerCase();
                            if(fileExt =='jpg' || fileExt =='jpeg' || fileExt =='png' || fileExt =='gif' || fileExt =='bmp'){
                               if (file) {
                                   var data = new FormData();
                                   data.append('file', file);
                                   $.ajax({
                                           url: 'uploadImageForEmailSetupTemplateBodyAjax.html?ajax=1&decorator=simple&popup=true',
                                           data: data,
                                           processData: false,
                                           contentType: false,
                                           async: false,
                                           type: 'POST',
                                           	success: function(data){ 
       	                                      var response = data.trim();
       	                                      if(response!=''){
       	                                        //This callback will tell TinyMCE the path of the uploaded image
       	                                        callback("UserImage?location="+response, {alt: file.name});
       	                                      }else{
       	                                        alert("There is some error while uploading image.");
       	                                      }
       	                                    },
                                       })
                               	}
                   			}else{
                   				alert('Please select only Image.');
                   			}
	                    });
           		}, 
	            
	});
		
	function validateFields(){
		var saveAsDescription = document.forms['emailTemplateForm'].elements['emailSetupTemplate.saveAs'].value;
		var emailFrom = document.forms['emailTemplateForm'].elements['emailSetupTemplate.emailFrom'].value;
		var emailTo = document.forms['emailTemplateForm'].elements['emailSetupTemplate.emailTo'].value;
		var $j = jQuery.noConflict();
		if(saveAsDescription.trim()==null|| saveAsDescription.trim()==''){
			$j('#getFileErrorMsg').html("<p>Please enter values for Description</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			return false;  
		}else if(emailFrom.trim()==null|| emailFrom.trim()==''){
			$j('#getFileErrorMsg').html("<p>Please enter values for From</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}else if(emailTo.trim()==null|| emailTo.trim()==''){
			$j('#getFileErrorMsg').html("<p>Please enter values for To</p>");
			$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		var reportsId = document.forms['emailTemplateForm'].elements['reportsId'].value;
		 if(saveAsDescription.includes(" :")){
					alert("Usage of space between text and : is not allowed. Please remove the space and continue");
			  return false;
		 }
			 
		<c:forEach items="${emailSetupTemplateFormsList}" var="list1">
			var aid="${list1.id}";
			if(reportsId==''){
				reportsId = aid;
			}else{
				reportsId = reportsId+','+aid;
			}
		</c:forEach>
		 document.forms['emailTemplateForm'].elements['reportsId'].value=reportsId;
	}
   	</script>
</head>

<s:form  cssClass="form_magn" id="emailTemplateForm" name="emailTemplateForm" action="saveEmailSetupTemplate" method="post" validate="true" onsubmit="return validateFields();" enctype="multipart/form-data">
<s:hidden name="emailSetupTemplate.id" value="%{emailSetupTemplate.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="newlineid" id="newlineid" value="" />
<s:hidden name="reportsId"/>
<s:hidden name="emailSetupTemplate.enable"/>

<div id="newmnav" class="nav_tabs">
		  <ul>
		    <li><a onclick="location.href='<c:url value="/emailSetupTemplateList.html"/>'"><span>Email Setup Template List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Email Setup Template Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    
  			</ul>
</div>
<div class="spn">&nbsp;</div>
	<div id="Layer1" style="width:100%" > 
		<div id="content" align="center">
		<div id="liquid-round-top">
   		<div class="top" ><span></span></div>
   			<div class="center-content">				
							<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr><td height="4px"></td></tr>
								<tr>
								<td colspan="10">
								<table style="margin:0px;padding:0px;"  cellspacing="3" cellpadding="0" border="0">
								<tr>
									<td align="right" style="width:53px;" class="listwhitetext">Description<font color="red" size="2">*</font> </td>
									<td align="left" class="listwhitetext"><s:textfield name="emailSetupTemplate.saveAs" cssStyle="width:317px;" maxlength="100" cssClass="input-text" /></td>
									
									<td align="right" style="width:41px;" class="listwhitetext">Module </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${moduleList}" fieldValue="${emailSetupTemplate.module}" attribute="class=list-menu name=emailSetupTemplate.module  style=width:120px headerKey='' headerValue='' "/>
									</td>
								
									<td align="right" style="width:84px;" class="listwhitetext">Sub-Module </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${subModuleList}" fieldValue="${emailSetupTemplate.subModule}" attribute="class=list-menu name=emailSetupTemplate.subModule onchange='checkParentModule();' style=width:110px headerKey='' headerValue='' "/>
									</td>
									
									<td align="right" style="width:55px;" class="listwhitetext">Order# </td>
									<td align="left" class="listwhitetext"><s:textfield name="emailSetupTemplate.orderNumber" cssClass="input-text" cssStyle="width:25px;text-align:right" maxlength="3" onkeydown="return onlyNumsAllowed(event)"/></td>
									
									<c:set var="isSignatureFlag" value="false"/>
									<c:if test="${emailSetupTemplate.addSignatureFromAppUser}">
										<c:set var="isSignatureFlag" value="true"/>
									</c:if>
									<td>&nbsp;</td>
									<td align="left" ><s:checkbox name="emailSetupTemplate.addSignatureFromAppUser" value="${isSignatureFlag}" fieldValue="true" onclick="" cssStyle="margin:0px;" /></td>
									<td align="left" class="listwhitetext">Add Signature From App User</td>
								</tr>
								</table>
								</td>
								</tr>
								<tr>
								<td colspan="15">
								<fieldset style="margin-bottom:0px;">
								<legend>Filters</legend>
								<table style="margin:0px;padding:0px;width:100%;"  cellspacing="3" cellpadding="0" border="0">									
								<tr>
									<td align="right" class="listwhitetext">Job Type </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${jobsList}" fieldValue="${emailSetupTemplate.jobType}" attribute="class=list-menu name=emailSetupTemplate.jobType  style=width:180px headerKey='' headerValue='' "/>
									</td>
								
									<td align="right" class="listwhitetext" style="width:40px;">Mode </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${modeList}" fieldValue="${emailSetupTemplate.mode}" attribute="class=list-menu name=emailSetupTemplate.mode  style=width:98px headerKey='' headerValue='' "/>
									</td>
								
									<td align="right" class="listwhitetext">&nbsp;Routing </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${routingList}" fieldValue="${emailSetupTemplate.routing}" attribute="class=list-menu name=emailSetupTemplate.routing  style=width:118px headerKey='' headerValue='' "/>
									</td>
									
									<td align="right"  width="84px;" class="listwhitetext">Move&nbsp;Type </td>
									<td align="left">
									<s:select cssClass="list-menu" name="emailSetupTemplate.moveType" list="%{moveTypeList}" cssStyle="width:111px" headerKey="" headerValue=""/>
									</td>
									
									<td align="right" class="listwhitetext">Service </td>
									<td align="left" colspan="3">
										<configByCorp:customDropDown listType="map" list="${serviceList}" fieldValue="${emailSetupTemplate.service}" attribute="class=list-menu name=emailSetupTemplate.service  style=width:230px headerKey='' headerValue='' "/>
									</td>
									
									
								</tr>
								<tr>
									<td colspan="5">
								    <table style="margin:0px;padding:0px;"  cellspacing="3" cellpadding="0" border="0">
								    <tbody>
								    <tr> 
									<td align="right" valign="top" style="width:41px;" class="listwhitetext">Others</td>
		     							<td align="left" class="listwhitetext"><s:textarea  cssClass="textarea" name="emailSetupTemplate.others" cssStyle="width:319px;height:65px;" onchange="checkFieldValue();" /></td>
									</tr>
									</tbody>
									</table>
									</td>
									
									<td align="left"><input type="button" id="testCondition" class="cssbutton" method="" name="testCondition" value="Test Condition" style="width:120px; height:23px;" onclick="testOthersFieldCondition();" /></td>
									<td align="right" class="listwhitetext" >Status</td>
									<c:if test="${emailSetupTemplate.status!='Error'}">
										<td align="left" width="124px;" ><s:textfield name="emailSetupTemplate.status" id="status" cssStyle="width:110px;" readonly="true" cssClass="input-textUpper" /></td>
							  		</c:if>
							  		<c:if test="${emailSetupTemplate.status=='Error'}">
							  			<td align="left"><s:textfield name="emailSetupTemplate.status" id="status" cssStyle="width:110px;" readonly="true" cssClass="rules-textUpper" /></td>
							  		</c:if>
									<div class="modal fade" id="ajaxRunningDiv" aria-hidden="true">
										<div class="loading-indicator">
											Testing...<br><i class="fa fa-spinner fa-pulse fa-3x fa-fw"" style="font-size:54px;color:grey;" id="loading-indicator"></i>
										</div>
									</div>
									<td align="right" class="listwhitetext">Company&nbsp;Division </td>
									<td align="left"><s:select cssClass="list-menu" name="emailSetupTemplate.companyDivision" list="%{companyDivisionList}" cssStyle="width:90px" headerKey="" headerValue=""/></td>
									
									<td align="right" class="listwhitetext">Language </td>
									<td align="left">
										<configByCorp:customDropDown listType="map" list="${languageList}" fieldValue="${emailSetupTemplate.language}" attribute="class=list-menu name=emailSetupTemplate.language  style=width:85px headerKey='' headerValue='' "/>
									</td>
									<%-- <td align="right" class="listwhitetext">Trigger Field </td>
									<td align="left" class="listwhitetext" colspan="3"><s:textfield name="emailSetupTemplate.triggerField" cssStyle="width:290px" maxlength="50" cssClass="input-text" /></td> --%>
								</tr>
								</table>
								</fieldset>
								</td>
								</tr>
								
								<tr>
								<td colspan="15">								
								<fieldset style="margin-bottom:0px;">
								<legend>Attachment Type</legend>
								<table style="margin:0px;padding:0px;width:100%;"  cellspacing="3" cellpadding="0" border="0">	
								<tr>
								<td valign="top" width="40%">
								<table style="margin:0px;padding:0px;width:100%;margin-top:5px;"  cellspacing="0" cellpadding="3" border="0">
								<tr>
								<td align="right" width="58" class="listwhitetext"><b>Attach&nbsp;Files:</b></td>
				                <td colspan="0" id="attachFileDiv1" class="answer_list">
				                <div class="MainFile tooltip"><i class="fa fa-cloud-upload"></i> &nbsp;CHOOSE FILES <input style="width:158px;" id="files" class="hide_file" name="fileUpload" type="file" multiple/>
				                <span class="tooltiptextform">Note : File name should not contains special characters like ^ ~ ' | $ @</span>
				                </div>
			                    <!-- <input style="width:188px;" id="files" name="fileUpload" type="file" multiple/> -->
				                <div id="selectedFiles" > </div>
			                    </td>
								</tr>								
								<tr>
								<td colspan="2" style="padding-right:12px;">								
								<c:if test="${not empty emailSetupTemplate.attachedFileLocation}">
	     						<div id="para1" style="clear: both;">
								<table class="table" style="width:100%; margin-top: 5px;">
									<thead>
										<tr>
											<th width="10px">#</th>
											<th style="min-width: 150px;">Attached File Name</th>
											<th width="30px" class="centeralign">File</th>
											<th width="40px" class="centeralign">Remove</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty attachedFileList}">
											<c:set var = "replacedAttachedFileList" value = "${attachedFileList}"/>
											<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
											<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
											<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem" varStatus="rowCounter">
												<tr>
												<td>${rowCounter.count}</td>
													<td><s:textfield cssClass="input-textUpper"	name="fileName" title="${attachedFileItem}" value="${attachedFileItem}" readonly="true" cssStyle="width:98%;background:none;border:none;" />
													</td>
													<td class="txt-center">
														<%-- <i class='fa fa-download' style="color:#0a84ff;" onclick="downloadAttachedFile('${attachedFileItem}','${emailSetupTemplate.id}');"></i> --%>
														<a><img align="middle" onclick="downloadAttachedFile('${attachedFileItem}','${emailSetupTemplate.id}');"
																style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/download16.png" /></a>
													</td>
													<td class="txt-center">
														<div>
															<a><img align="middle"
																onclick="deleteAttachedFileName('${attachedFileItem}');"
																style="margin: 0px 0px 0px 0px;"
																src="${pageContext.request.contextPath}/images/remove16.png" />
															</a>
														</div>
													</td>	
												</tr>
											</c:forTokens>
										</c:if>
									</tbody>
								</table>
							</div>
	     					</c:if>
	     					</td>	
								<s:hidden name="emailSetupTemplate.attachedFileLocation"/>
								</tr>
								</table>
								</td>
								<td valign="top">
								<table style="margin:0px;padding:0px;"  cellspacing="3" cellpadding="0" border="0">	
								<tr>
								<td align="right" class="listwhitetext">Document Type</td>
								<td style="width:100px;height:26px;">
									<configByCorp:customDropDown listType="map" list="${docsList}" fieldValue="${emailSetupTemplate.fileTypeOfFileCabinet}" attribute="class=list-menu name=emailSetupTemplate.fileTypeOfFileCabinet  style=width:200px headerKey='' headerValue='' "/>
								</td>
								</tr>	
								</table>
								<table style="margin:0px;padding:0px;width:100%;"  cellspacing="0" cellpadding="3" border="0">
								<tr>
								<td align="right" style="padding-left:5px;">
								<div id="para1" style="clear: both;">
									<table class="table" id="dataTable"
										style="width:100%; margin:5px 0px 5px 0px;">
										<thead>
											<tr>
												<th width="10px">#</th>
												<th style="min-width: 150px;">Jrxml Name</th>
												<th style="min-width: 150px;">Form Description</th>
												<th width="30px" class="centeralign">PDF</th>
												<th width="30px" class="centeralign">Docx</th>
												<th width="40px" class="centeralign">Remove</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty emailSetupTemplateFormsList}">
												<c:forEach var="individualItem"
													items="${emailSetupTemplateFormsList}" varStatus="rowCounter">
													<tr>
														<td>${rowCounter.count}</td>
														<td><s:textfield cssClass="input-textUpper"
																name="reportName" value="${individualItem.reportName}"
																id="rep${rowCounter.count}" readonly="true"
																onkeyup="autoCompleterAjaxCall(this.value,'${rowCounter.count}','choices','rep');"
																cssStyle="width:98%;" />
															<div id="choices${rowCounter.count}" class="listwhitetext"></div>
														</td>
														<td><s:textfield cssClass="input-textUpper"
																name="reportDescription" readonly="true"
																value="${individualItem.description}"
																id="des${rowCounter.count}" cssStyle="width:98%;" />
														</td>
														<td class="txt-center">
															<c:if test="${individualItem.pdf=='true' }">
																<div>
																	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
																</div>
															</c:if>
															<c:if test="${individualItem.pdf=='false' || individualItem.pdf==''}">
																<div>
																	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
																</div>
															</c:if>
														</td>
														<td class="txt-center">
															<c:if test="${individualItem.docx=='true' }">
																<div>
																	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
																</div>
															</c:if>
															<c:if test="${individualItem.docx=='false' || individualItem.docx==''}">
																<div>
																	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
																</div>
															</c:if>
														</td>
														<td class="txt-center">
															<div>
																<a><img align="middle"
																	onclick="deleteFormsId('${individualItem.id}');"
																	style="margin: 0px 0px 0px 0px;"
																	src="${pageContext.request.contextPath}/images/remove16.png" />
																</a>
															</div>
														</td>	
														
													</tr>
												</c:forEach>
											</c:if>
										</tbody>
	
									</table>
									<table style="float:left;margin-bottom:0px;">
										<tr>
											<td><input type="button" class="cssbutton1" id="addLine"
												name="addLine" style="width: 80px; height: 25px;"
												value="Add Jrxml" onclick="addRow('dataTable');" /></td>
										</tr>
									</table>
								</div>
								</td>
								</tr>
								</table>
								</td>
								</tr>
								</table>
								
							   </fieldset>
								
								</td>
								</tr>
								
								<tr>
								<td colspan="0" valign="top" style="margin:0px;padding:0px;">
								
								<table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
								<tr>
								<td align="right" style="vertical-align:top;" class="listwhitetext" width="55">From<font color="red" size="2">*</font> </td>
								<td align="left" >
									<s:textfield name="emailSetupTemplate.emailFrom" id="emailFrom" onchange="validateFieldsName(this);" onkeydown="limitTextEmailFrom()" onkeyup="limitTextEmailFrom()" cssStyle="width:344px;box-sizing: border-box;height:19px;" maxlength="60" cssClass="input-text" />
									<div style="background:#efefef;">
										<s:textfield readonly="true" id="countEmailFrom" name="countEmailFrom" value="${sizeOfEmailFrom}" cssStyle="width: 18px; font-size: 10px; background:transparent; border:none; color:#222222;"  />characters&nbsp;left. 
									</div>
								</td>
								</tr>
								
								<tr>
								<td align="right" class="listwhitetext">To<font color="red" size="2">*</font> </td>
								<td align="left" >
									<s:textarea cssClass="textarea" id="emailTo" name="emailSetupTemplate.emailTo" cssStyle="width:340px;height:45px;" onchange="validateFieldsName(this);" onkeydown="limitTextEmailTo()" onkeyup="limitTextEmailTo()" /> 
									<div style="background:#efefef;">
										<s:textfield readonly="true" id="countEmailTo" name="countEmailTo" value="${sizeOfEmailTo}" cssStyle="width: 18px; font-size: 10px; background:transparent; border:none; color:#222222;"   />characters&nbsp;left. 
									</div>
								</td>
								</tr>
								
								<tr>
								<td align="right" class="listwhitetext">Cc </td>
								<td align="left" >
									<s:textarea cssClass="textarea" id="emailCc" name="emailSetupTemplate.emailCc" cssStyle="width:340px;height:45px;" onkeydown="limitTextEmailCc()" onkeyup="limitTextEmailCc()" /> 
									<div style="background:#efefef;">
										<s:textfield readonly="true" id="countEmailCc" name="countEmailCc" value="${sizeOfEmailCc}" cssStyle="width: 18px; font-size: 10px; background:transparent; border:none; color:#222222;"   />characters&nbsp;left. 
									</div>
								</td>
								</tr>
								<tr>
								<td align="right" class="listwhitetext">Bcc </td>
								<td align="left" >
									<s:textarea cssClass="textarea" id="emailBcc" name="emailSetupTemplate.emailBcc" cssStyle="width:340px;height:45px;" onkeydown="limitTextEmailBcc()" onkeyup="limitTextEmailBcc()" /> 
									<div style="background:#efefef;">
										<s:textfield readonly="true" id="countEmailBcc" name="countEmailBcc" value="${sizeOfEmailBcc}" cssStyle="width: 18px; font-size: 10px; background:transparent; border:none; color:#222222;"   />characters&nbsp;left. 
									</div>
								</td>
								</tr>
								<tr>
								<td align="right" class="listwhitetext">Subject </td>
								<td align="left" >
									<s:textarea cssClass="textarea" id="emailSubject" name="emailSetupTemplate.emailSubject" cssStyle="width:340px;height:80px;" onkeydown="limitTextEmailSubject()" onkeyup="limitTextEmailSubject()" /> 
									<div style="background:#efefef;">
										<s:textfield readonly="true" id="countEmailSubject" name="countEmailSubject" value="${sizeOfEmailSubject}" cssStyle="width: 18px; font-size: 10px; background:transparent; border:none; color:#222222;"   />characters&nbsp;left. 
									</div>
								</td>
								</tr>								
								</table>
								<%-- <table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
								<tr>
								<td align="right" class="listwhitetext" width="50">Distribution Method </td>
									<td align="left">
									<configByCorp:customDropDown listType="map" list="${distributionMethodList}" fieldValue="${emailSetupTemplate.distributionMethod}" attribute="class=list-menu name=emailSetupTemplate.distributionMethod  style=width:90px headerKey='' headerValue='' "/>
									</td>
									<td>
			                         <input type="button" id="cancel" class="cssbutton" method="" name="cancel" value="Test Condition" style="width:100px; height:23px;" onclick="" /></td>
									<td align="right" class="listwhitetext" width="">Status</td>
									<td align="left"><s:textfield name="emailSetupTemplate.status" cssStyle="width:89px;" readonly="true" cssClass="input-textUpper" /></td>
								</tr>								
								</table> --%>					
								</td>
															
								<td colspan="0" valign="top" style="margin:0px;padding:0px;">								
							    <table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
							    <tbody>
							    <tr> 
	     						<td align="left"><s:textarea  cssClass="textarea" id="emailBody" name="emailSetupTemplate.emailBody" cols="122" rows="10" onkeypress="" /></td>
								<input name="imageUpload" type="file" id="upload" class="hidden" onchange="">
								</tr>
								</tbody>
								</table>								
								</td>									 
								</tr>									
							</tbody>
							</table>
						</div>
					<div class="bottom-header" style="margin-top:45px;"><span></span></div>
				</div>
			</div>
			
			<table class="detailTabLabel" border="0" style="width:850px">
				<tbody>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdOn' /></b></td>
						<td style="width:120px"><fmt:formatDate var="refJobTypeCreatedOnFormattedValue" value="${emailSetupTemplate.createdOn}" pattern="${displayDateTimeEditFormat}" /> 
						<s:hidden name="emailSetupTemplate.createdOn" value="${refJobTypeCreatedOnFormattedValue}" /><fmt:formatDate value="${emailSetupTemplate.createdOn}" pattern="${displayDateTimeFormat}" /></td>
						
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdBy' /></b></td>
						<c:if test="${not empty emailSetupTemplate.id}">
							<s:hidden name="emailSetupTemplate.createdBy" />
							<td style="width:85px"><s:label name="createdBy" value="%{emailSetupTemplate.createdBy}" /></td>
						</c:if>
						<c:if test="${empty emailSetupTemplate.id}">
							<s:hidden name="emailSetupTemplate.createdBy" value="${pageContext.request.remoteUser}" />
							<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.updatedOn' /></b></td>
						<fmt:formatDate var="refJobTypeUpdatedOnFormattedValue" value="${emailSetupTemplate.updatedOn}" pattern="${displayDateTimeEditFormat}" />
						<s:hidden name="emailSetupTemplate.updatedOn" value="${refJobTypeUpdatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${emailSetupTemplate.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
						
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.updatedBy' /></b></td>
						<c:if test="${not empty emailSetupTemplate.id}">
							<s:hidden name="emailSetupTemplate.updatedBy" />
							<td style="width:85px"><s:label name="updatedBy" value="%{emailSetupTemplate.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty emailSetupTemplate.id}">
							<s:hidden name="emailSetupTemplate.updatedBy" value="${pageContext.request.remoteUser}" />
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</c:if>
					</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
			
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.save"/>   
       <s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" tabindex=""/>
       <c:if test="${not empty emailSetupTemplate.id}">
       	<input type="button" class="cssbutton1" value="Copy Template" style="width:105px; height:25px" onclick="copyEmailSetupTemplate();"/>
       </c:if>
</div>
			
	<div class="modal fade" id="showFileErrorModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				    <div class="modal-body" style="font-weight:600;">
					    <div class="alert alert-danger" id="getFileErrorMsg">
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
				    </div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="showFileErrorModalNew" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				    <div class="modal-body" style="font-weight:600;">
					    <div class="alert alert-danger" id="getFileErrorMsgNew">
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-sm btn-danger" onclick="redirectModal();" data-dismiss="modal">Close</button>
				    </div>
			</div>
		</div>
	</div>
</s:form>
<script language="javascript" type="text/javascript">
function downloadAttachedFile(fileName,emailId){
	var pageType = "";
	 var url="downloadAttachedFileFromEmailSetupTemplate.html?fileName="+encodeURI(fileName)+"&id="+emailId+"&pageType="+pageType;
	 location.href=url;
	 setTimeout("showOrHide(0)",2500);
}
function limitTextEmailFrom(){
	limitField=document.getElementById("emailFrom");
	limitCount=document.getElementById("countEmailFrom");
	limitNum=60;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function limitTextEmailTo(){
	limitField=document.getElementById("emailTo");
	limitCount=document.getElementById("countEmailTo");
	limitNum=250;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function limitTextEmailCc(){
	limitField=document.getElementById("emailCc");
	limitCount=document.getElementById("countEmailCc");
	limitNum=250;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function limitTextEmailBcc(){
	limitField=document.getElementById("emailBcc");
	limitCount=document.getElementById("countEmailBcc");
	limitNum=250;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function limitTextEmailSubject(){
	limitField=document.getElementById("emailSubject");
	limitCount=document.getElementById("countEmailSubject");
	limitNum=500;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function onlyNumsAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
}
function showAttachFileDiv(){
	 document.getElementById('attachFileDiv').style.display = "block";
}

var selDiv = "";

document.addEventListener("DOMContentLoaded", init, false);

function init() {
    document.querySelector('#files').addEventListener('change', handleFileSelect, false);
    selDiv = document.querySelector("#selectedFiles");
}
   
function handleFileSelect(e) {
	var $j = jQuery.noConflict();
	$j('#selectedFiles').addClass('setBG');
    if(!e.target.files) return;
    
    selDiv.innerHTML = "";
    
    var files = e.target.files;
    for(var i=1; i<files.length+1; i++) {
        var f = files[i-1];
        
        if(i%2==0){
      		selDiv.innerHTML += "<b style='color:#003366;'>"+i+".) "+"</b>"+f.name + "<br/>";
        }else{
        	selDiv.innerHTML += "<b style='color:#003366;'>"+i+".) "+"</b>"+ f.name + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
    }
}

function addRow(tableID) {
	var flag=checkMandatoryAddLineField(); 
	if(flag==true){
	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
		var row = table.insertRow(rowCount); 
	    var newlineid=document.getElementById('newlineid').value;
	    if(newlineid==''){
	        newlineid=rowCount;
	      }else{
	      	newlineid=newlineid+"~"+rowCount;
	      }
	    document.getElementById('newlineid').value=newlineid;
	    
	    var cell1 = row.insertCell(0);
	    var element1 = document.createElement("input");
		element1.setAttribute("style", "border:none;width:10px" );
		element1.setAttribute("readonly", "true" );
		element1.setAttribute("src","<c:url value='/images/blankWhite.png'/>");
		element1.setAttribute("value", rowCount );
		element1.id='seq'+rowCount;
		cell1.appendChild(element1);
	 	
	    var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.style.width="98%";
		element2.setAttribute("class", "input-text" );
		element2.setAttribute("placeholder", "Type For Jrxml Name" );
		element2.id='rep'+rowCount;
		element2.setAttribute("onkeyup","autoCompleterAjaxCall(this,"+rowCount+",'choices','rep')");
		
		var element5 = document.createElement("div");
		element5.id = 'choices'+rowCount;
		cell2.appendChild(element2);
		cell2.appendChild(element5);
		
		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.style.width="98%";
		element3.setAttribute("class", "input-textUpper" );
		element3.setAttribute("readonly", "true" );
		element3.id='des'+rowCount;
		cell3.appendChild(element3);
		
		var cell4 = row.insertCell(3);
		var element4 = document.createElement("img");
		element4.setAttribute("src","<c:url value='/images/blankWhite.png'/>");
		cell4.appendChild(element4);
		
		var cell5 = row.insertCell(4);
		var element5 = document.createElement("img");
		element5.setAttribute("src","<c:url value='/images/blankWhite.png'/>");
		cell5.appendChild(element5);
		
		var cell5 = row.insertCell(5);
		var element5 = document.createElement("img");
		element5.setAttribute("src","<c:url value='/images/blankWhite.png'/>");
		cell5.appendChild(element5);
	}
}
function checkMandatoryAddLineField(){
	  var newlineid=document.getElementById('newlineid').value; 	
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("~");        
	        for(var i=0;i<arrayLine.length;i++){
	   		 var questionFieldVal=document.getElementById('rep'+arrayLine[i]).value;
		       if(questionFieldVal==""){
			    	 alert("Please fill Jrxml name for line # "+arrayLine[i]);
			    	 return false;
		        }
        	}
		 return true;
   }else{
	     return true;
   }
}
var rowId;	
function autoCompleterAjaxCall(jrxmlName,id,divId,fieldName){
	rowId = id;
	var $j = jQuery.noConflict();
	var moduleName = document.forms['emailTemplateForm'].elements['emailSetupTemplate.module'].value;
	document.getElementById('emailTemplateForm').setAttribute("AutoComplete","off");
	jrxmlName =jrxmlName.value;
	if(moduleName!=null && moduleName!=''){
		if(jrxmlName.length>0){
		 new Ajax.Request('/redsky/getFormsNameAutocompleteAjax.html?ajax=1&moduleName='+moduleName+'&jrxmlName='+jrxmlName+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
			    
				      var response = transport.responseText || "no response text";
				      
	                  var mydiv = document.getElementById(divId+id);
	                  document.getElementById(divId+id).style.display = "block";
	                  mydiv.innerHTML = response;
	                  mydiv.show(); 
				    },
				    onFailure: function(){ 
					    }
				  });
		}else{
			var mydiv = document.getElementById(divId+id);
	        document.getElementById(divId+id).style.display = "none";
		}
	}else{
		$j('#getFileErrorMsg').html("<p>Please select value for Module.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		document.getElementById(fieldName+id).value='';
	}
}

function getValues(jrxmlName,description,jrxmlId){
	var $j = jQuery.noConflict();
	var formId = '${emailSetupTemplate.formsId}';
	formId = formId.trim();
	var check = formId.indexOf(jrxmlId);
	if(check >= 0){
		$j('#getFileErrorMsg').html("<p>This Jrxml has been already added, Please choose another.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
		document.getElementById('rep'+rowId).value ='';
		document.getElementById('des'+rowId).value ='';
		document.getElementById('choices'+rowId).style.display = "none";
	}else{
		document.getElementById('rep'+rowId).value=jrxmlName;
		document.getElementById('des'+rowId).value=description;
		document.getElementById('choices'+rowId).style.display = "none";
		
		var formId = document.forms['emailTemplateForm'].elements['reportsId'].value;
		if(formId==null || formId==''){
			formId = jrxmlId;
		}else{
			formId = formId +","+jrxmlId;
		}
		document.forms['emailTemplateForm'].elements['reportsId'].value = formId;
	}
}

function deleteFormsId(parentId){
	var reportsId='';
	<c:forEach items="${emailSetupTemplateFormsList}" var="list1">
		var aid="${list1.id}";
		if(aid!=parentId){
			reportsId = aid+','+reportsId;
		}else{
			reportsId = reportsId;
		}
	</c:forEach>
	var emailSetupTemplateId = '${emailSetupTemplate.id}';
	if(emailSetupTemplateId!=null){
	var agree=confirm("Are you sure you wish to remove this Jrxml ?");
	if (agree){
		 new Ajax.Request('/redsky/deleteFormsIdInEmailSetupTemplateAjax.html?ajax=1&reportsId='+reportsId+'&id='+emailSetupTemplateId+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
			    
				      var response = transport.responseText || "no response text";
				      window.location.reload();
				    },
				    onFailure: function(){ 
					    }
				  });
	 	}else{
			return false;
		}
	}
}

function deleteAttachedFileName(fileName){
	var emailSetupTemplateId = '${emailSetupTemplate.id}';
	if(emailSetupTemplateId!=null){
	var agree=confirm("Are you sure you wish to remove this file ?");
	if (agree){
		 new Ajax.Request('/redsky/deleteAttachedFileNameInEmailSetupTemplateAjax.html?ajax=1&fileName='+fileName+'&id='+emailSetupTemplateId+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      window.location.reload();
				    },
				    onFailure: function(){ 
					    }
				  });
	 	}else{
			return false;
		}
	}
}

function checkParentModule(){
	var moduleName = document.forms['emailTemplateForm'].elements['emailSetupTemplate.module'].value;
	var subModuleName = document.forms['emailTemplateForm'].elements['emailSetupTemplate.subModule'].value;
	if(((moduleName!=null && moduleName!='') && (moduleName=='ServiceOrder' || moduleName=='WorkTicket' || moduleName=='Domestic' || moduleName=='Accounting') && (subModuleName=='customerFile'))){
		alert('You can not select Sub-module as Customer File');
		document.forms['emailTemplateForm'].elements['emailSetupTemplate.subModule'].value='${emailSetupTemplate.subModule}';
	}
}

function validateFieldsName(targetElement){
	var res = targetElement.value;
	res = res.split(",");
	for(i=0;i<res.length;i++){
		var fieldVal = res[i];
		if(fieldVal.indexOf("^$")!=-1){
			var flagValue = validateTableAndFieldName(res[i]);
			if(!flagValue){
				alert('Please enter Table and Field name correctly');
				document.forms['emailTemplateForm'].elements[targetElement.name].value = '';
				document.forms['emailTemplateForm'].elements[targetElement.name].focus();
				if(targetElement.name =='emailSetupTemplate.emailFrom'){
					document.getElementById("countEmailFrom").value= 250;
				}else if(targetElement.name =='emailSetupTemplate.emailTo'){
					document.getElementById("countEmailTo").value= 250;
				}
			}
		}else{
			checkEmail(targetElement,fieldVal);
		}
	}
}

function checkEmail(targetElement,fieldVal) {
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(fieldVal)) {
			alert('Please provide a valid email address');
			document.forms['emailTemplateForm'].elements[targetElement.name].value='';
			document.forms['emailTemplateForm'].elements[targetElement.name].focus();
			if(targetElement.name =='emailSetupTemplate.emailFrom'){
				document.getElementById("countEmailFrom").value= 250;
			}else if(targetElement.name =='emailSetupTemplate.emailTo'){
				document.getElementById("countEmailTo").value= 250;
			}
		} 
}

function validateTableAndFieldName(fieldValue){
	var returnFlag= false;
	var str = fieldValue;
	str = str.split(".");
	var tableName = str[0].replace("$","").replace("^","");
	var fieldName = str[1].replace("$","").replace("^","");
	<c:forEach var="entry" items="${tableWithColumnsNameList}">
		if(tableName.toLowerCase() == "${entry.key}".toLowerCase()){
			<c:forTokens items="${entry.value}" delims="~" var="fieldNameVal">
				if(fieldName.toLowerCase() == "${fieldNameVal}".toLowerCase()){
					returnFlag= true;
				}
			</c:forTokens>
		}
	</c:forEach>
	return returnFlag;
}

function testOthersFieldCondition(){
	var othersFieldVal = document.forms['emailTemplateForm'].elements['emailSetupTemplate.others'].value;
	var moduleFieldVal = document.forms['emailTemplateForm'].elements['emailSetupTemplate.module'].value;
	var $j = jQuery.noConflict();
	if(moduleFieldVal==null || moduleFieldVal==''){
		$j('#getFileErrorMsg').html("<p>Please select value for Module.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	}else if(othersFieldVal==null || othersFieldVal==''){
		$j('#getFileErrorMsg').html("<p>Please fill value for Others.</p>");
		$j('#showFileErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	}else{
		$j("#loading-indicator").show();
		$j("#ajaxRunningDiv").modal({show:true,backdrop: 'static',keyboard: false});
		 new Ajax.Request('/redsky/testEmailSetupTestConditionQueryAjax.html?ajax=1&moduleFieldValue='+moduleFieldVal+'&othersFieldValue='+encodeURIComponent(othersFieldVal)+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      if(response.trim()=='Error'){
				      		document.forms['emailTemplateForm'].elements['emailSetupTemplate.status'].value = response.trim();
				      		$j('#status').removeClass('input-textUpper').addClass('rules-textUpper');
				      }else{
				    		document.forms['emailTemplateForm'].elements['emailSetupTemplate.status'].value = response.trim();
				    		$j('#status').removeClass('rules-textUpper').addClass('input-textUpper');
				      }
				      $j("#loading-indicator").hide();
				      $j("#ajaxRunningDiv").modal("hide");
				    },
				    onFailure: function(){ 
					    }
				  });
	}
}

function checkFieldValue(){
	var othersFieldVal = document.forms['emailTemplateForm'].elements['emailSetupTemplate.others'].value;
	if(othersFieldVal==null || othersFieldVal==''){
		document.forms['emailTemplateForm'].elements['emailSetupTemplate.status'].value = '';
	}
}

function redirectModal(){
	var id = "<%=request.getAttribute("id")%>";
	if(id!=null && id!='null'){
		var replaceURL = "editEmailSetupTemplate.html?id="+id+"";
		window.location.replace(replaceURL);
	}
}

function copyEmailSetupTemplate(){
	var url = "copyEmailSetupTemplate.html?id=${emailSetupTemplate.id}"; 
	document.forms['emailTemplateForm'].action= url;	 
	document.forms['emailTemplateForm'].submit();
}

window.onload =function ModifyPlaceHolder () {		
    var input = document.getElementById("emailFrom");
    input.placeholder = "Ex : abc@abc.com or ^$module.fieldName$^";
    var input1 = document.getElementById("emailTo");
    input1.placeholder = "Ex : abc@abc.com or ^$module.fieldName$^";
}
try{
       if('${fileError}' == 'NoFile'){
        	var $j = jQuery.noConflict();
    		$j('#getFileErrorMsgNew').html("<p>This file is currently not available.</p>");
    		$j('#showFileErrorModalNew').modal({show:true,backdrop: 'static',keyboard: false});
       }
       $j("#loading-indicator").hide();
}catch(e){}
</script>