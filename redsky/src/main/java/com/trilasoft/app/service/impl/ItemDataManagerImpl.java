package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import com.trilasoft.app.dao.ItemDataDao;
import com.trilasoft.app.service.ItemDataManager;
import com.trilasoft.app.model.ItemData;

import org.appfuse.service.GenericManager;
import org.appfuse.service.impl.GenericManagerImpl;

public class ItemDataManagerImpl extends GenericManagerImpl<ItemData, Long> implements ItemDataManager{
	
	ItemDataDao itemDataDao;
	public ItemDataManagerImpl(ItemDataDao itemDataDao) {
        super(itemDataDao);
        this.itemDataDao = itemDataDao;
	}
	
	public	String weightAndVolumeFromRfMaster(String itemDescription){
	return	itemDataDao.weightAndVolumeFromRfMaster(itemDescription);
	}
	
	public void deleteFromItemData(Long id,int workTicketId)
	{
		itemDataDao.deleteFromItemData(id , workTicketId);
	}
	
	public void updateItemDataWeightAndVolume(String totalWeight,String totalVolume, Integer workTicketId,String totalEstimatedPieces,String totalEstimatedVolume,String totalEstimatedWeight)
	{
		itemDataDao.updateItemDataWeightAndVolume(totalWeight,totalVolume,workTicketId,totalEstimatedPieces,totalEstimatedVolume,totalEstimatedWeight);
	}
	
	public List getAllHandOutList(String shipNumber){
		return itemDataDao.getAllHandOutList(shipNumber);
	}
	
	public List getAllHandOutList(String shipNumber, String itemDescription){
		return itemDataDao.getAllHandOutList(shipNumber, itemDescription);
	}
	
	public void updateHandoutList(String shipNumber, long handoutItemId,Integer ticket){
			itemDataDao.updateHandoutList(shipNumber, handoutItemId,ticket);
	}
	
	public void updateAllHoTicketInHandOutList(String shipNumber, Integer ticket, Boolean releaseAllItemsValue){
		itemDataDao.updateAllHoTicketInHandOutList(shipNumber, ticket,releaseAllItemsValue);
	}
	
	public String updateHoTicketByHo(String shipNumber, Long id, Boolean hoValue,Integer ticket){
		return itemDataDao.updateHoTicketByHo(shipNumber, id,hoValue,ticket);
	}
	
	public Object getItemDataByLine(Long ticket,Integer line,String serviceType){
		return itemDataDao.getItemDataByLine(ticket, line, serviceType);
	}
	
	public Integer getMaxLine(String shipNumber){
		return itemDataDao.getMaxLine(shipNumber);
	}
	
	public void saveNotes(Long id, Integer workTicketId, String valueOfNotes){
		itemDataDao.saveNotes(id, workTicketId, valueOfNotes);
		
	}
	public List getAllHOItemList(Long hoTicket, String shipNumber){
		return itemDataDao.getAllHOItemList(hoTicket, shipNumber);
	}
	
	public void updatetotalActQty(Integer workTicketId, String totalActualQty){
		itemDataDao.updatetotalActQty(workTicketId, totalActualQty);
	}
	
}
