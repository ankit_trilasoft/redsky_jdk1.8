<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Visa & Immigration Details</title>
    <meta name="heading" content="Visa & Immigration Details"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>
   
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>

<script language="JavaScript" type="text/javascript">
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 
		trap();
		var elementsLen=document.forms['dsVisaImmigrationForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsVisaImmigrationForm'].elements[i].type=='text')
					{
						document.forms['dsVisaImmigrationForm'].elements[i].readOnly =true;
						document.forms['dsVisaImmigrationForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{
						document.forms['dsVisaImmigrationForm'].elements[i].disabled=true;
					} 
			} 
	}
</sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.partnerScript"> 
window.onload = function() { 
		trap();
		var elementsLen=document.forms['dsVisaImmigrationForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsVisaImmigrationForm'].elements[i].type=='text')
					{
						document.forms['dsVisaImmigrationForm'].elements[i].readOnly =true;
						document.forms['dsVisaImmigrationForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsVisaImmigrationForm'].elements[i].disabled=true;
					}
			if(document.forms['dsVisaImmigrationForm'].elements[i].type=='radio')
			{
				var n=document.forms['dsVisaImmigrationForm'].elements[i].id;
				document.forms['dsVisaImmigrationForm'].elements[i].disabled=false;
			} 		
			} 
			if(document.forms['dsVisaImmigrationForm'].elements['saveButton'])
			{
				document.forms['dsVisaImmigrationForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsVisaImmigrationForm'].elements['Reset'])
			{ 
				document.forms['dsVisaImmigrationForm'].elements['Reset'].disabled=false;
			} 
	}
		
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.vendettiScript">
window.onload = function() {
       trap();
		var elementsLen=document.forms['dsVisaImmigrationForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsVisaImmigrationForm'].elements[i].type=='text')
					{
						document.forms['dsVisaImmigrationForm'].elements[i].readOnly =true;
						document.forms['dsVisaImmigrationForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsVisaImmigrationForm'].elements[i].disabled=true;
					} 
			} 
			if(document.forms['dsVisaImmigrationForm'].elements['saveButton'])
			{
				document.forms['dsVisaImmigrationForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsVisaImmigrationForm'].elements['Reset'])
			{ 
				document.forms['dsVisaImmigrationForm'].elements['Reset'].disabled=false;
			} 
	}
		
</sec-auth:authComponent>
 
<sec-auth:authComponent componentId="module.script.form.partnerScript">
window.onload = function() {
		trap();
		var elementsLen=document.forms['dsVisaImmigrationForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsVisaImmigrationForm'].elements[i].type=='text')
					{
						document.forms['dsVisaImmigrationForm'].elements[i].readOnly =true;
						document.forms['dsVisaImmigrationForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsVisaImmigrationForm'].elements[i].disabled=true;
					} 
			} 
			if(document.forms['dsVisaImmigrationForm'].elements['saveButton'])
			{
				document.forms['dsVisaImmigrationForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsVisaImmigrationForm'].elements['Reset'])
			{ 
				document.forms['dsVisaImmigrationForm'].elements['Reset'].disabled=false;
			} 
	}
</sec-auth:authComponent>
function test()
		{
			return false;
		}

function trap() 
		  {
		  if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0)
						{
							
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{ 
							
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
							    document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_empty1.jpg')>0)
						{
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
						        document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_open1.jpg')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						
											
						if(document.images[i].src.indexOf('images/nav')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
					
					}
		    }
		  }

</script>

<script>
function findAgent(position,agentType)
{
	var partnerCode = "";
	var shipNumberA="";
	if(agentType=='OA')
	{
		partnerCode = document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);	
	}
	
}
function winOpenDest(){  
	    	openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=${serviceOrder.serviceType}&decorator=popup&popup=true&fld_sixthDescription=dsVisaImmigration.vendorContact&fld_fifthDescription=thirdDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=dsVisaImmigration.vendorEmail&fld_secondDescription=secondDescription&fld_description=dsVisaImmigration.vendorName&fld_code=dsVisaImmigration.vendorCode');
 }
 
function checkVendorName(){ 
    var vendorId = document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value;
    	if(vendorId == ''){
		document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value="";
	}
	if(vendorId != ''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
} 
function checkVendorNameRelo(){
  var vendorId=document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value;
  var job='${serviceOrder.serviceType}';
  if(vendorId == '')
  {
   document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value="";
    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorContact'].value="";
     document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorEmail'].value="";
   }
  if(vendorId != '')
  {
   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);;
   http3.open("GET",url,true);
   http3.onreadystatechange = handleHttpResponse3;
   http3.send(null);
  }
  }
function handleHttpResponse3(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();                
                var res = results.split("#");                        
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           		        document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value = res[1];
		           				document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorContact'].value = res[3];
		           				document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorEmail'].value = res[4];
   		           			    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
		           		}else{
		           		    alert("Vendor Code is not approved" ); 
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value="";
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value="";
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorContact'].value ="";
		           			document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorEmail'].value ="";
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
						    
		           		}
                }else{
                     alert("Vendor Code not valid" );
                     document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value=""; 
					 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value="";
					 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorContact'].value ="";
		           	 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorEmail'].value ="";
					 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
			   }
     }
}
function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();                
                var res = results.split("#");                
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           			document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value = res[1];
		           			//document.forms['dsPreviewTripForm'].elements['trackingUrl'].value = res[5];
		           			document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
		           		}else{
		           			alert("Vendor Code is not approved" ); 
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value="";
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value="";
						    document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
		           		}
                }else{
                     alert("Vendor Code not valid" );
                     document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorName'].value=""; 
					 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].value="";
					 document.forms['dsVisaImmigrationForm'].elements['dsVisaImmigration.vendorCode'].select();
			   }
     }
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
     var http3 = getHTTPObject();
     
function autoSaveFunc(clickType)
{
progressBarAutoSave('1');
var id1 =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
	if(document.forms['dsVisaImmigrationForm'].elements['formStatus'].value=='1')
   {  
	 if ('${autoSavePrompt}' == 'No'){
	 var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	 var id1 =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
	     if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
	      if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
	    	  var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
	     if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.costing')
           {
             noSaveAction ='costingDetail.html?sid='+id1;
           }
            if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.document')
           {
             noSaveAction ='accountFiles.html?sid='+id1+'&seqNum=${serviceOrder.sequenceNumber}';
           }
                processAutoSave(document.forms['dsVisaImmigrationForm'], 'saveDsVisaImmigration!saveOnTabChange.html', noSaveAction); 
	      }else{
	         if(!(clickType == 'save'))
               {
                  var id1 =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
                  if (document.forms['dsVisaImmigrationForm'].elements['formStatus'].value == '1')
                    {
                      var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Visa & Immigration Details ");
                        if(agree)
                            {
                              document.forms['dsVisaImmigrationForm'].action ='saveDsVisaImmigration!saveOnTabChange.html';
                              document.forms['dsVisaImmigrationForm'].submit();
                             }
                        else{
                             if(id1 != '')
                               {
                                 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.serviceorder')
                                   {
                                    location.href = 'editServiceOrderUpdate.html?id='+id1;
                                    }
                                 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.billing')
                                    {
                                     location.href = 'editBilling.html?id='+id1;
          						    }
         						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.accounting')
           							{
            						  location.href = 'accountLineList.html?sid='+id1;
          							 }
	     						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.customerfile')
         						  {
	     							 var cidVal='${customerFile.id}';
            						 location.href ='editCustomerFile.html?id='+cidVal;
        						   }
	    						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.costing')
          						 {
           						  location.href ='costingDetail.html?sid='+id1;
          						 }
        					    if(document.forms['dsPreviewTripForm'].elements['gotoPageString'].value =='gototab.document')
       						     {
          						   location.href ='accountFiles.html?sid='+id1+'&seqNum=${serviceOrder.sequenceNumber}';
          						 }
                                   
                               } 
                            } 
                       } 
                   
                   }
           } 
           }else{    
       
                      if(id1 != ''){
                                   if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.serviceorder')
                                   {
                                    location.href = 'editServiceOrderUpdate.html?id='+id1;
                                    }
                                 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.billing')
                                    {
                                     location.href = 'editBilling.html?id='+id1;
          						    }
         						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.accounting')
           							{
            						  location.href = 'accountLineList.html?sid='+id1;
          							 }
	     						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.customerfile')
         						  {
	     							 var cidVal='${customerFile.id}';
            						 location.href ='editCustomerFile.html?id='+cidVal;
        						   }
	    						 if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.costing')
          						 {
           						  location.href ='costingDetail.html?sid='+id1;
          						 }
        					    if(document.forms['dsVisaImmigrationForm'].elements['gotoPageString'].value =='gototab.document')
       						     {
          						   location.href ='accountFiles.html?sid='+id1+'&seqNum=${serviceOrder.sequenceNumber}';
          						 }
                                   
                                }
                      } 
     }		
function changeStatus(){
    document.forms['dsVisaImmigrationForm'].elements['formStatus'].value = '1';
}
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURIComponent(soIdNum)+"&seqNm="+encodeURIComponent(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['dsVisaImmigrationForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURIComponent(soIdNum)+"&seqNm="+encodeURIComponent(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }

  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
          		 var results = http5.responseText
                 results = results.trim();
				 var id1=results;	 
	             findOtherServiceType(id1);
             }
             
       }     
	function findOtherServiceType(id1)      
	{
		  var soIdNum=id1;
		  var url="findOtherServiceType.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
		  http10.open("GET", url, true); 
          http10.onreadystatechange =function(){ handleHttpResponseOtherShipType(id1);}; 
          http10.send(null); 
	}
  function handleHttpResponseOtherShipType(id1){
             if (http10.readyState == 4)
             {
             			  var results = http10.responseText
			               results = results.trim();
						if(results=="")	{
						location.href = 'editTrackingStatus.html?id='+id1;
						}
						else{
						location.href = results+id1;
						} 
             } 
       }	
    var http10 = getHTTPObject();         
     var http5 = getHTTPObject();  
  function goToUrl(id)
	{
	findOtherServiceType(id);
	}
function findCustomerOtherSO(position) {
 var sid=document.forms['dsVisaImmigrationForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['dsVisaImmigrationForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURIComponent(sid)+"&soIdNum="+encodeURIComponent(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
</script> 	    
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<c:set var="fileID" value="%{serviceOrder.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="dsVisaImmigrationForm" action="saveDsVisaImmigration.html" method="post" validate="true">
<s:hidden name="serviceOrder.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/> 
<s:hidden name="minShip" />
<s:hidden name="shipSize" />
<s:hidden name="countShip" />
<s:hidden name="customerFile.id" value="%{customerFile.id}" />

<div id="Layer1"  style="width:100%;">
		<div id="newmnav" style="float: left;"> 
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		      <li><a onclick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>S/O Details</span></a></li>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
			  <li><a onclick="setReturnString('gototab.billing');return autoSaveFunc('none');"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
            </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">  
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			   </c:when> --%>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		         <li><a onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose> 
		    </sec-auth:authComponent>
			  <li  id="newmnav1" style="background:#FFF"><a class="current"><span>Status</span></a></li>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">  
			  <li><a onclick="setReturnString('gototab.customerfile');return autoSaveFunc('none');"><span>Customer File</span></a></li>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.reportTab">  
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
			  <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=dsvisaimmigration&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
			 </ul> 
		</div>
       	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}"  >
  		<a><img align="middle" id="navigation1" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" id="navigation3" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" id="navigation5" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr>
		</table>
		
 <div class="spn">&nbsp;</div>
     <div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderJobHeader.jsp"%>
     </div>
      </div>

<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" /> 
	<s:hidden name="firstDescription" />
<s:hidden name="dsVisaImmigration.id" />
<s:hidden name="gotoPageString" id="gotoPageString" value="${gotoPageString}" />
<s:hidden name="formStatus" value="" />
<s:hidden  name="validateFormNav" />
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="id" value="${dsVisaImmigration.id}"/>
<s:hidden name="dsVisaImmigration.serviceOrderId" /> 
<s:hidden id="countDSVisaImmigrationNotes" name="countDSVisaImmigrationNotes" value="<%=request.getParameter("countDSVisaImmigrationNotes") %>"/> 
<c:set var="countDSVisaImmigrationNotes" value="<%=request.getParameter("countDSVisaImmigrationNotes") %>" /> 
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.costing' }">
	<c:redirect url="/costingDetail.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.document' }">
	<c:redirect url="/accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer1" style="width:750px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Visa & Immigration Details<img id="imgId3" src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" >
<tr>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dsVisaImmigration.vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo();changeStatus();" /></td>
<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest(),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dsVisaImmigration.vendorName" readonly="true" size="39" maxlength="200"  />
<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA');" src="<c:url value='/images/address2.png'/>" />
</td></tr>
</table>
<td align="right" class="listwhitetext" width="100px">Service Start</td>
	    <c:if test="${not empty dsVisaImmigration.serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.serviceStartDate"/></s:text>
			 <td><s:textfield id="serviceStartDate" cssClass="input-text" name="dsVisaImmigration.serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].serviceStartDate,'calender',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
	    <c:if test="${empty dsVisaImmigration.serviceStartDate}">
		<td><s:textfield id="serviceStartDate" cssClass="input-text" name="dsVisaImmigration.serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].serviceStartDate,'calender',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>

</td>
<c:if test="${empty dsVisaImmigration.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dsVisaImmigration.id}">
<c:choose>
<c:when test="${countDSVisaImmigrationNotes == '0' || countDSVisaImmigrationNotes == '' || countDSVisaImmigrationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',755,500);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSVisaImmigrationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=VisaImmigration&imageId=countDSVisaImmigrationNotesImage&fieldId=countDSVisaImmigrationNotes&decorator=popup&popup=true',755,500);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>


</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dsVisaImmigration.vendorContact" readonly="false" size="57" maxlength="225" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dsVisaImmigration.serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.serviceEndDate"/></s:text>
			 <td><s:textfield id="serviceEndDate" cssClass="input-text" name="dsVisaImmigration.serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].serviceEndDate,'calender1',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
	    <c:if test="${empty dsVisaImmigration.serviceEndDate}">
		<td><s:textfield id="serviceEndDate" cssClass="input-text" name="dsVisaImmigration.serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].serviceEndDate,'calender1',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dsVisaImmigration.vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>

<table>
		<tr>
		<td align="right"  width="170px"  class="listwhitetext">Work Permit / Visa Holder Name </td>
		<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dsVisaImmigration.workPermitVisaHolderName" readonly="false"  maxlength="100" onchange="changeStatus();"/></td>
		</tr>
		
		<tr>
		<td align="right"  width="170px"  class="listwhitetext">Provider Notification Date</td>
	    <c:if test="${not empty dsVisaImmigration.providerNotificationDate}">
			 <s:text id="providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.providerNotificationDate"/></s:text>
			 <td width="65px" ><s:textfield id="providerNotificationDate" cssClass="input-text" name="dsVisaImmigration.providerNotificationDate" value="%{providerNotificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender3" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].providerNotificationDate,'calender3',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
	    <c:if test="${empty dsVisaImmigration.providerNotificationDate}">
		<td width="65px" ><s:textfield id="providerNotificationDate" cssClass="input-text" name="dsVisaImmigration.providerNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender3" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].providerNotificationDate,'calender3',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
		<td align="right"  width="203px"  class="listwhitetext">Expiry Reminder 3 Mos Prior To Expiry</td>
   	<c:if test="${not empty dsVisaImmigration.expiryReminder3MosPriorToExpiry}">
	 <s:text id="expiryReminder3MosPriorToExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.expiryReminder3MosPriorToExpiry"/></s:text>
	 <td width="65px" ><s:textfield id="expiryReminder3MosPriorToExpiry" cssClass="input-text" name="dsVisaImmigration.expiryReminder3MosPriorToExpiry" value="%{expiryReminder3MosPriorToExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender6" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].expiryReminder3MosPriorToExpiry,'calender6',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
	</c:if>
   <c:if test="${empty dsVisaImmigration.expiryReminder3MosPriorToExpiry}">
	<td width="65px" ><s:textfield id="expiryReminder3MosPriorToExpiry" cssClass="input-text" name="dsVisaImmigration.expiryReminder3MosPriorToExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender6" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].expiryReminder3MosPriorToExpiry,'calender6',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
 </c:if>
	</tr>
	<tr>			
		<td align="right"  width="170px"  class="listwhitetext">Visa Expiry Date </td>
			    <c:if test="${not empty dsVisaImmigration.visaExpiryDate}">
					 <s:text id="visaExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.visaExpiryDate"/></s:text>
					 <td width="65px" ><s:textfield id="visaExpiryDate" cssClass="input-text" name="dsVisaImmigration.visaExpiryDate" value="%{visaExpiryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender4" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].visaExpiryDate,'calender4',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
				</c:if>
			    <c:if test="${empty dsVisaImmigration.visaExpiryDate}">
				<td width="65px" ><s:textfield id="visaExpiryDate" cssClass="input-text" name="dsVisaImmigration.visaExpiryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender4" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].visaExpiryDate,'calender4',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
				</c:if>
		<td align="right"  width="170px"  class="listwhitetext">Work Permit Expiry</td>
			    <c:if test="${not empty dsVisaImmigration.workPermitExpiry}">
					 <s:text id="workPermitExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsVisaImmigration.workPermitExpiry"/></s:text>
					 <td width="65px" ><s:textfield id="workPermitExpiry" cssClass="input-text" name="dsVisaImmigration.workPermitExpiry" value="%{workPermitExpiryFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender5" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].workPermitExpiry,'calender5',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
				</c:if>
			    <c:if test="${empty dsVisaImmigration.workPermitExpiry}">
				<td width="65px" ><s:textfield id="workPermitExpiry" cssClass="input-text" name="dsVisaImmigration.workPermitExpiry" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender5" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsVisaImmigrationForm'].workPermitExpiry,'calender5',document.forms['dsVisaImmigrationForm'].dateFormat.value); return false;"/></td>
		</c:if>
	</tr>
	<tr>
	</tr>

</table>

</div></div><div class="bottom-header"><span></span></div></div>
<table width="700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${dsVisaImmigration.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="dsVisaImmigration.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${dsVisaImmigration.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty dsVisaImmigration.id}">
								<s:hidden name="dsVisaImmigration.createdBy"/>
								<td ><s:label name="createdBy" value="%{dsVisaImmigration.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dsVisaImmigration.id}">
								<s:hidden name="dsVisaImmigration.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${dsVisaImmigration.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="dsVisaImmigration.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${dsVisaImmigration.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty dsVisaImmigration.id}">
							<s:hidden name="dsVisaImmigration.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{dsVisaImmigration.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dsVisaImmigration.id}">
							<s:hidden name="dsVisaImmigration.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
<table><tr><td> 
<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	</sec-auth:authComponent>
	</td></tr></table></div>
</s:form>

<script language="JavaScript" type="text/javascript">
var fieldName = document.forms['dsVisaImmigrationForm'].elements['field'].value;
var fieldName1 = document.forms['dsVisaImmigrationForm'].elements['field1'].value;
if(fieldName!=''){
document.forms['dsVisaImmigrationForm'].elements[fieldName].className = 'rules-textUpper';
}
if(fieldName1!=''){
document.forms['dsVisaImmigrationForm'].elements[fieldName1].className = 'rules-textUpper';
}
</script>
    