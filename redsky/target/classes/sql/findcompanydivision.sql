DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findcompanydivision` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `findcompanydivision`(code varchar(50)) RETURNS varchar(10) CHARSET utf8
    DETERMINISTIC
BEGIN
DECLARE Name varchar(10) DEFAULT 0;

SELECT distinct companycode INTO Name FROM companydivision WHERE
bookingagentcode=code;

RETURN Name;

END $$

DELIMITER ;