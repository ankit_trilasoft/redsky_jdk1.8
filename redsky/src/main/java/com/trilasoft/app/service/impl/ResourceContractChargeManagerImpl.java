package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ResourceContractChargeDao;
import com.trilasoft.app.model.ResourceContractCharges;
import com.trilasoft.app.service.ResourceContractChargeManager;

public class ResourceContractChargeManagerImpl extends GenericManagerImpl<ResourceContractCharges, Long> implements ResourceContractChargeManager {   
	
	ResourceContractChargeDao resourceContractChargeDao;   
	  
	public ResourceContractChargeManagerImpl(ResourceContractChargeDao resourceContractChargeDao) {   
        super(resourceContractChargeDao);   
        this.resourceContractChargeDao = resourceContractChargeDao;   
    }
	
	public List findResourceByPartnerId(String corpId,Long parentId) {
		return resourceContractChargeDao.findResourceByPartnerId(corpId,parentId);
	}

	public ResourceContractChargeDao getResourceContractChargeDao() {
		return resourceContractChargeDao;
	}

	public void setResourceContractChargeDao(
			ResourceContractChargeDao resourceContractChargeDao) {
		this.resourceContractChargeDao = resourceContractChargeDao;
	}

	public void deleteAll(Long parentId) {
		resourceContractChargeDao.deleteAll(parentId);
		
	}

	public List createPrice(String corpId, String contract,String shipNumber,Long serviceOrderId,String divisionFlag) {
		resourceContractChargeDao.createPrice(corpId,contract,shipNumber,serviceOrderId,divisionFlag);
		return null;
	}

}
