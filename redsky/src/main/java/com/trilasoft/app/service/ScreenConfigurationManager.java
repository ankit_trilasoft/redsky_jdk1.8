package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.ScreenConfiguration;


public interface ScreenConfigurationManager extends GenericManager<ScreenConfiguration, Long> { 
       public List checkById(Long id);
       public List<ScreenConfiguration>searchByCorpIdTableAndField(String corpId,String tableName,String fieldName); 
}