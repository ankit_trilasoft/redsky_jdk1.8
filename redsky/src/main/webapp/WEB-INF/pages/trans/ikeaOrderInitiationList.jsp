<%@ include file="/common/taglibs.jsp"%>  
<head>   
    <title>Ikea Order List</title>   
    <meta name="heading" content="Ikea Order List"/>   

<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;}
div.error, span.error, li.error, div.message {width:450px;margin-top:0px; }
form {margin-top:-40px;!margin-top:-10px;}
div#main {margin:-5px 0 0;}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px;"  
        onclick="location.href='<c:url value="/editIkeaOrder.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px;" align="top" key="button.search" onclick="return goToSearchCustomerDetail();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>   
    
<s:form id="searchForm" action="searchIkeaOrders.html" method="post" validate="true">  

<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%"  border="1">
<thead>
<tr>
<th>Order #</th>
<th><fmt:message key="customerFile.lastName"/></th>
<th><fmt:message key="customerFile.firstName"/></th>
</tr>
</thead>	
<tbody>
<tr>
	<td width="25" align="left">
	    <s:textfield name="customerFile.sequenceNumber" required="true" cssClass="input-text"  size="20" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
	</td>
	<td width="25" align="left">
	    <s:textfield name="customerFile.lastName" required="true" cssClass="input-text" size="30" />
	</td>
	<td width="25" align="left">
	    <s:textfield name="customerFile.firstName" required="true" cssClass="input-text" size="30" />
	</td> 
	</tr>
	<tr>
	<td colspan="2"></td>			
	<td width="140"   align="right" style="border-left: hidden;">
	<c:out value="${searchbuttons}" escapeXml="false" />   
	</td>
</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Order List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

	<s:set name="customerFileExt" value="customerFiles" scope="request"/>   
	<display:table name="customerFileExt" class="table" requestURI="" id="customerFileList" export="true"  pagesize="25" style="width:100%;" >  
    <display:column property="sequenceNumber" sortable="true" title="Order #" url="/editIkeaOrder.html" paramId="id" paramProperty="id"/>
    <display:column property="lastName" sortable="true" titleKey="customerFile.lastName" style="width:90px" maxLength="20"/>
    <display:column property="firstName" sortable="true" titleKey="customerFile.firstName" maxLength="20"/> 
    <display:column property="coordinator" style="text-transform: uppercase;" sortable="true" titleKey="customerFile.coordinator" />
    <%-- <display:column property="quotationStatus"  sortable="true" title="Status" /> --%>
    <display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>
	<display:column property="originCity" sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"/>    
	<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"/> 
	<display:column property="destinationCity" sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"/>    
    <display:column property="createdOn" sortable="true" titleKey="customerFile.createdOn" style="width:100px" format="{0,date,dd-MMM-yyyy}"/> 
    <display:setProperty name="paging.banner.item_name" value="customerfile"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/> 
    <display:setProperty name="export.excel.filename" value="CustomerFile List.xls"/>   
    <display:setProperty name="export.csv.filename" value="CustomerFile List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="CustomerFile List.pdf"/>   
</display:table>   
<s:hidden name="componentId" value="customerFile" />  
<c:out value="${buttons}" escapeXml="false" />  
<c:set var="isTrue" value="false" scope="session"/>
</s:form>

<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['searchForm'].elements['customerFile.sequenceNumber'].value  = ""; 
	document.forms['searchForm'].elements['customerFile.lastName'].value  = "";
	document.forms['searchForm'].elements['customerFile.firstName'].value  = "";
}

function goToSearchCustomerDetail(){
	document.forms['searchForm'].action = 'searchIkeaOrders.html';
	document.forms['searchForm'].submit();
}

</script>


<script type="text/javascript">   
   try{
    document.forms['searchForm'].elements['customerFile.sequenceNumber'].focus();  
  }
  catch(e){}
   try{
    <c:if test="${detailPage == true}" >
    	<c:redirect url="/editIkeaOrder.html?from=list&id=${customerFileList.id}"  />
	</c:if>
	}
	catch(e){}
</script>	
