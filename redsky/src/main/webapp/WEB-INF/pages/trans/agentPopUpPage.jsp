<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>

	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:15px;
margin-top:-25px;
padding-left:40px;
text-align:right;
width:94%;
}
</style>

</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   
<s:form id="partnerListForm" action="searchAgentPopup.html?decorator=popup&bypass_sessionfilter=YES&popup=true" method="post" >  
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="partnerTerminalCountryCode" value="<%=request.getParameter("userCountry") %>"/>
<s:hidden name="partnerTerminalState" value="<%=request.getParameter("stateCode") %>"/>

<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>

<div id="Layer1" style="width:100%">
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">

<table class="table" style="width:99%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partnerCode" size="20" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partnerLastName" size="24" cssClass="text medium" />
			</td>
			
		</tr>
		<tr>
			<td ></td>
			<td width="" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<div id="newmnav">   
 <ul>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Agents<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
</ul>
</div><div class="spn" style="width:800px">&nbsp;</div><br>

<s:set name="agents" value="partners" scope="request"/>  
<display:table name="agents" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%;margin-top: -10px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:35px"/>
    <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:110px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>

<c:set var="isTrue" value="false" scope="session"/>
<!--

			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartner.html?id=${myMessageList.id} " >Addresses</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerDetail.html?id=${myMessageList.id} " >Details</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerRemarks.html?id=${myMessageList.id} " >Remarks</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
 -->
 
</s:form>

<script language="javascript" type="text/javascript">

function clear_fields(){
        document.forms['partnerListForm'].elements['partnerCode'].value = "";
        document.forms['partnerListForm'].elements['partnerLastName'].value = "";
		document.forms['partnerListForm'].elements['partner.lastName'].value = "";
		document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
		document.forms['partnerListForm'].elements['partner.billingState'].value = "";
		document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
}

function setAgent(){
document.forms['partnerListForm'].elements['partnerType'].value = "AG";
document.forms['partnerListForm'].elements['findFor'].value = "account";
document.forms['partnerListForm'].submit();
}

</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>


<script type="text/javascript">   
  function addPP()
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  location.href='editPartnerFormPopup.html?firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&flag=${flag}&compDiv=${compDiv}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';  	
  } 
  catch(e){}
  }  
  function PrivatePartyDetail(id)
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  location.href='viewPartner.html?id='+id+'&partnerType=${partnerType}&firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&mode=view&flag=${flag}&compDiv=${compDiv}&type=TT&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';
  } 
  catch(e){}
  }
  function partnerDetail(id)
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  //var id= encodeURI("${partnerList.id}");
  location.href='editPrivatePartner.html?id='+id+'&partnerType=${partnerType}&firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&mode=view&flag=${flag}&compDiv=${compDiv}&type=TT&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';
  }
  catch(e){}
  } 
</script>