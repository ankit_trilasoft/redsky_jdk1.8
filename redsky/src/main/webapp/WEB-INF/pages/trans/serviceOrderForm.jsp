<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="closeCompanyDivisionfn" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
 <head> 
<c:choose>
<c:when test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
<title>Quote Detail</title>
<meta name="heading" content="Quote Detail" />
</c:when>
<c:otherwise>
<title><fmt:message key="serviceOrderDetail.title" /></title>
<meta name="heading" content="<fmt:message key='serviceOrderDetail.heading'/>" />
</c:otherwise>
</c:choose>
<style>				
div#content{padding:0px 0px; min-height:50px; margin-left:0px;}
#mydiv{margin-top:-45px; }
body {padding-bottom:100px !important;}
legend {font-family:arial,verdana,sans-serif;font-size:11px;font-weight:bold;margin:0;}
#loader {position:absolute; z-index:999; margin:0px auto -1px 250px;}
.lgendIn{vertical-align: middle; margin: 0px 0px 0px 2px;!margin: 0px 0px 0px -8px;}
.upper-case{text-transform:capitalize;}
.ui-autocomplete {max-height: 250px;overflow-y: auto; /* prevent horizontal scrollbar */ overflow-x: hidden;}
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete { height: 100px; }
  .pr-f11{font-family:arial,verdana;font-size:11px;	}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>  
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>  
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" /> 
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>  	
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.min.js"></script>
  	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>   	
<script language="JavaScript" type="text/javascript">
function findUserPermission(name,position) { 
	  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
	  ajax_showTooltip(url,position);	
	  }

window.addEventListener("load", function() { 
	if(${isNetworkRecord}){
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper'; 
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].setAttribute('readonly','true');
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].setAttribute('readonly','true');
		var el = document.getElementById('serviceOrder.bookingAgentCode.img');
		el.onclick = false;
	    document.images['serviceOrder.bookingAgentCode.img'].src = 'images/navarrow.gif';
	}
},false);
    var map;
    var gdir;
    var geocoder = null;
    var addressMarker; 
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }
    function handleErrors(){
	   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
	     alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
	     alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
	     alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);
	  else if (gdir.getStatus().code == G_GEO_BAD_KEY)
	     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);
      else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
	     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
	    else alert("An unknown error occurred."); 
	} 
	function onGDirectionsLoad() { 
		var distanceInMile=gdir.getDistance().html;
		distanceInMile=distanceInMile.split('&nbsp;');
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=distanceInMile[0].replace(',','');
	if(distanceInMile[1]=='mi'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
		} } 
  </script>
  <script type="text/JavaScript">
function setQuesAns(ans,qus){
	var ansopt=ans.options[ans.options.selectedIndex].value;
	document.removalRelocationServiceForm.quesansId.value=qus+"-"+ansopt; 
}  
</script> 
<script type="text/javascript"> 
<c:if test="${serviceOrder.corpID=='STVF'}">
try{  	
	document.getElementById('status1').focus();
  	}catch(e){}
 </c:if>
function getBigPartnerAlert(code,position) {
	if(code != ''){
		var url="findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
		position = document.getElementById(position);
		ajax_showBigTooltip(url,position);
	} }  
function findCityStateNotPrimary(targetField, position){
if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	 }
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }
  function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     } 
     var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){  handleHttpResponseCityStateFlex(targetElement,targetField);};
     http99.send(null);
     } 
   function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                if(results.length>0){
                     if(results=='Y' ){
                     	findCityState(targetElement,targetField);
                     } } }  } 
 function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }
  function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value; 
       if(targetField=='OZ'){
       var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
        if(document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
          if( document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             } }  }

function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = id;
	 } else if (targetField=='DZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = id;
	} }
 function handleHttpResponseCityState(targetField){
                  if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=res[2]; 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value=res[2];
	           	 
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].focus();
	           	} } } else { }
             }
// End Of Method
function zipCode(){
        var originCountryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value; 
		var el = document.getElementById('zipCodeRequiredTrue');
		var el1 = document.getElementById('zipCodeRequiredFalse');
		if(originCountryCode == 'United States'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		} }
</script>  
<script>
function navigationVal(){
	if (window.addEventListener)
		 window.addEventListener("load", function(){
			 var imgList = document.getElementsByTagName('img');
			 for(var i = 0 ; i < imgList.length ; i++){
				if(imgList[i].src.indexOf('images/nav') > 1){
					imgList[i].style.display = 'none';
				}  }
		 }, false)
}
function right(e) {
		if (navigator.appName == 'Netscape' && e.which == 1) {
		return false;
		}
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
			return false;
		}else 
			return true;
		}
 function changeCalOpenarvalue() { 
		  	document.forms['serviceOrderForm'].elements['calOpener'].value='open';
		  }
 function trap1(){
	 if(document.images){
		 var imgList = document.getElementsByTagName('img');
		 for(var i = 0 ; i < imgList.length ; i++){
			if(imgList[i].src.indexOf('images/navarrow.gif') > 0){
				if(imgList[i].id.indexOf('-trigger') > 0){
					imgList[i].removeAttribute("id");
				} }  } 	}
	 try{
			var fieldName = document.forms['serviceOrderForm'].elements['field'].value;
			var fieldName1 = document.forms['serviceOrderForm'].elements['field1'].value;
			var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
			if(fieldName!=''){
			document.forms['serviceOrderForm'].elements[fieldName].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customerSurveyDiv', 'fade=0,persist=0,show=1')
			<c:if test="${serviceOrder.job=='INT'}">
			animatedcollapse.addDiv('postMoveSurvey', 'fade=0,persist=0,show=1')
			</c:if>
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
				animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init();
			}
			if(fieldName1!=''){
			document.forms['serviceOrderForm'].elements[fieldName1].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customerSurveyDiv', 'fade=0,persist=0,show=1')
			<c:if test="${serviceOrder.job=='INT'}">
			animatedcollapse.addDiv('postMoveSurvey', 'fade=0,persist=0,show=1')
			</c:if>
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init()
			} 	
	}catch(e){}
		 } 	  
function trap(){
		  if(document.images)  {
		    	for(i=0;i<document.images.length;i++){
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="/images/open-popup.gif"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="/redsky/images/open-popup.gif"]').attr('src','images/navarrow.gif');		    		
		    		$('#serviceOrderForm').find('img[src="images/navarrows_03.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navarrows_04.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="${pageContext.request.contextPath}/images/navarrows_05.png"]').attr('src','images/navarrow.gif');		    		
		    		$('#serviceOrderForm').find('img[src="images/navdisable_03.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navdisable_04.png"]').attr('src','images/navarrow.gif');
		    		$('#serviceOrderForm').find('img[src="images/navdisable_05.png"]').attr('src','images/navarrow.gif'); 
		      }  	} 	}
   function getOriginCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
} 
function getDestinationCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}
function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}try{	
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].selected=true;
 					}catch (e){}
				}else{ 
                 } }   }
function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='N';
				 	}try{
				 	document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].select();
				 	}catch (e){}		     	 
 				}else{ 
                 }  } }     
function findCompanyDivisionByBookAg(loadParam) {
    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCheckedBy'].value="";
	var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	var compDivision = '${serviceOrder.companyDivision}';
    var cid = '${customerFile.id}';
    var moveTypeStringValue = '${serviceOrder.moveType}';
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)+"&compDivision="+encodeURI(compDivision)+"&cid="+encodeURI(cid)+"&moveTypeStringValue="+encodeURI(moveTypeStringValue);
    http6.open("GET", url, true);
    http6.onreadystatechange = function() {handleHttpResponse4444(loadParam)};
    http6.send(null);
}
function handleHttpResponse4444(loadParamRes){
		    if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@"); 
                var companyDivision= document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value
                var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value; 
                if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){ 
                if(res.indexOf(companyDivision)!=-1){  
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
                    }
					if(res.length == 2){
						 	document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;												
						 }else{
							 document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[0].selected=true;
						 }  }
                else{
                	try
                	{
                	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value='${serviceOrder.bookingAgentCode}';
                	}
                	catch(err){}
                }  }
                else{
                	var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++){
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						if(res.length == 2){
							 	document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;												
							 }else{
								 document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[0].selected=true;
							 } } }
			if(loadParamRes!='onload')
			getJobList('unload');
			}	 }   
</script>
<script language="javascript" type="text/javascript">
 var r={
'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\=']/g,
'quotes':/['\''&'\"']/g,
'notnumbers':/[^\d]/g
};
function valid(targetElement,w){
targetElement.value = targetElement.value.replace(r[w],'');
}
</script>
<script language="javascript" type="text/javascript"> 
       function isSoExtFlag(){
	     document.forms['serviceOrderForm'].elements['isSOExtract'].value="yes";
       }
       function isUpdaterFlag(){
  	     document.forms['serviceOrderForm'].elements['updaterIntegration'].value="yes";
         } 
		function change() {
		       if( document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "BOAT" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "AUTO" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "HHG/A") {
		       	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = false ;
			  }else{
			  	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = true ;
			  } }
</script>
<SCRIPT LANGUAGE="JavaScript">
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
function checkQuotation(){ 
	var bookCode=document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	 var  quotationStatus=document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value ;
	 <c:set var="closedCompanyDivisionVar" value="${closeCompanyDivisionList}" />
	 <c:if test="${(closeCompanyDivisionfn:indexOf(closedCompanyDivisionVar,customerFile.companyDivision)>=0) || (closeCompanyDivisionfn:indexOf(closedCompanyDivisionVar,serviceOrder.companyDivision)>=0)}"> 
    if(quotationStatus=='AC'){
	alert("Quote cannot be accepted as Company Division is closed.");
	document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';	
    }else{ 
     }
 </c:if>
 <c:if test="${(closeCompanyDivisionfn:indexOf(closedCompanyDivisionVar,customerFile.companyDivision)<0) && (closeCompanyDivisionfn:indexOf(closedCompanyDivisionVar,serviceOrder.companyDivision)<0)}">  
	if(bookCode!=''&& quotationStatus=='AC'){
		var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
		http454.open("GET", url, true);
		http454.onreadystatechange = handleHttpResponse454;
		http454.send(null);	
	}
	</c:if>
	}
	function handleHttpResponse454(){
		if (http454.readyState == 4) {
	          var results = http454.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){ 
	       			if(res[2] == 'Approved'){
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");
	           			document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';	
	           		}			
	      		}else{
	           		alert("Booking Agent code not valid");
	           		document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';
		   		}  } } 
	 var http454 = getHTTPObject();
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    var http5 = getHTTPObject5();
    var http6 = getHTTPObject();
   var http9 = getHTTPObject9();
  var http33 = getHTTPObject33();
  var httpHVY = getHTTPObjectHVY();
  var http99 = getHTTPObject();
  var httpChargeDiscount = getHTTPObject();
  function getHTTPObjectHVY() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   }
    return xmlhttp;
}
  function getHTTPObject33() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
   function getHTTPObject9() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   }
    return xmlhttp;
}
    function getHTTPObject5() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   }
    return xmlhttp;
}
var http99 = getHTTPObject9();
     function getHTTPObject9() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}  
</script>
<jsp:include flush="true" page="serviceOrderFormJS.jsp"></jsp:include>
<jsp:include flush="true" page="serviceOrderFormJSSecond.jsp"></jsp:include>   
</head>
<div id="Layer5" style="width:100%">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="agentSearchValidation" />
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" /> 
<c:set var="fileID" value="%{serviceOrder.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:form id="serviceOrderForm" name="serviceOrderForm" action="saveServiceOrder" onsubmit="return toCheckCountryFields();" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
<s:hidden name="serviceOrder.accrualReadyUpdatedBy"/>
<s:hidden name="serviceOrder.recInvoiceNumber"/>
<s:hidden name="serviceOrder.additionalAssistance"/>
<s:hidden name="serviceOrder.supplementGBL"/>
<s:hidden name="serviceOrder.oIOverallProjectview"/>
<s:hidden name="serviceOrder.isUpdater"/>
<%-- <c:if test="${not empty serviceOrder.targetRevenueRecognition}">
     <s:text id="serviceorderFormattedtargetRevenueRecognition" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.targetRevenueRecognition" /></s:text>
	 <s:hidden  name="serviceOrder.targetRevenueRecognition" value="%{serviceorderFormattedtargetRevenueRecognition}" /> 
</c:if>
	 <c:if test="${empty serviceOrder.targetRevenueRecognition}">
	 <s:hidden   name="serviceOrder.targetRevenueRecognition"/> 
	 </c:if>  --%>
<c:if test="${not empty serviceOrder.accrualReadyDate}">
		 <s:text id="serviceorderFormattedValueaccrualReadyDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.accrualReadyDate" /></s:text>
			 <s:hidden  name="serviceOrder.accrualReadyDate" value="%{serviceorderFormattedValueaccrualReadyDate}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.accrualReadyDate}">
		 <s:hidden   name="serviceOrder.accrualReadyDate"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.accrueChecked}">
		 <s:text id="serviceorderFormattedValueAccrueCheckedDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.accrueChecked" /></s:text>
			 <s:hidden  name="serviceOrder.accrueChecked" value="%{serviceorderFormattedValueAccrueCheckedDate}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.accrueChecked}">
		 <s:hidden   name="serviceOrder.accrueChecked"/> 
	 </c:if> 
	 <c:if test="${not empty serviceOrder.updaterUpdatedOn}">
		 <s:text id="serviceorderFormattedValueUpdaterUpdatedOn" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.updaterUpdatedOn" /></s:text>
			 <s:hidden  name="serviceOrder.updaterUpdatedOn" value="%{serviceorderFormattedValueUpdaterUpdatedOn}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.updaterUpdatedOn}">
		 <s:hidden   name="serviceOrder.updaterUpdatedOn"/> 
	 </c:if> 
<s:hidden name="chargeCodeValidationVal"  id= "chargeCodeValidationVal" />
<s:hidden name="chargeCodeValidationMessage"  id= "chargeCodeValidationMessage" />
<s:hidden name="oldRecRateCurrency" id="oldRecRateCurrency" value=""/>
<s:hidden name="companyDivisionForTicket"/>
 <s:hidden name="buttonType" />
 <s:hidden name="billToCodeForTicket"/>
 <s:hidden name="generateMassage" />
 <s:hidden name="oldSOCompanyDivision" value="${serviceOrder.companyDivision}" />
  <s:hidden name="accUpdateAjax" />
	<input type="hidden" name="tempId1" id="tempId1" value="">
	<s:hidden name="quesansIds" id="aqid" value=""/>
	<s:hidden name="serviceOrder.isSOExtract"/>
	<s:hidden name="isSOExtract"/>
	<s:hidden name="regUpdate"/>
	<input type="hidden" name="tempId2" id="tempId2" value="">
	<s:hidden name="corpID" value="${customerFile.corpID}" />
	<s:hidden name="cfContract" value="${customerFile.contract}" />
	<s:hidden name="tempJob" />
	<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	<s:hidden name="isDecaExtract" value="1"/>
	</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
		<s:hidden name="actgCodeFlag" value="YES" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.QualitySurvey.CoordinatorEval.validation">
	<s:hidden name="CoordinatorEvalValidation" id="CoordinatorEvalValidation" value="Y"/>
	</configByCorp:fieldVisibility> 
	<s:hidden name="customerFileJob" value="%{customerFile.job}"/>  
	<c:set var="customerFileJob" value="${customerFile.job}"/>
	<s:hidden name="addAddressState" /> 
	<s:hidden name="pricingButton" value="<%=request.getParameter("pricingButton") %>"/>
    <c:set var="pricingButton"  value="<%=request.getParameter("pricingButton") %>"/>
    <s:hidden name="redirectPricingButton" value="<%=request.getParameter("redirectPricingButton") %>"/>
    <c:set var="redirectPricingButton"  value="<%=request.getParameter("redirectPricingButton") %>"/> 
	<s:hidden name="forwarding" value="<%= request.getParameter("forwarding")%>"/>
	<s:hidden name="sofid" value="<%= request.getParameter("sid")%>"/>
	<s:hidden name="unit" id ="unit" value="%{dummyDistanceUnit}" />	
	       <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
            <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
            <c:set var="relocationServicesKey" value="" />
            <c:set var="relocationServicesValue" value="" /> 
            <c:forEach var="entry" items="${relocationServices}">
            <c:if test="${relocationServicesKey==''}">
            <c:if test="${entry.key==serviceOrder.serviceType}">
            <c:set var="relocationServicesKey" value="${entry.key}" />
            <c:set var="relocationServicesValue" value="${entry.value}" /> 
            </c:if>
           </c:if> 
           </c:forEach>
     <s:hidden name="weightTab"  id= "weightTab" value="1"/>
     <s:hidden name="weightTabSub1"  id= "weightTabSub1" value="1"/>
     <s:hidden name="weightTabSub2"  id= "weightTabSub2" value="1"/>       
	<s:hidden name="serviceOrder.customerFileId" value="%{customerFile.id}"/>
	<s:hidden name="serviceOrder.isNetworkRecord" />
	<s:hidden name="serviceOrder.networkSO" />
	<s:hidden name="serviceOrder.bookingAgentShipNumber" />
	<s:hidden name="serviceOrder.ugwIntId" />
	<s:hidden name="accessAllowed" />
	<s:hidden name="transferDateFlag" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" />
	<s:hidden name="soid" value="${serviceOrder.id}" />
	<s:hidden name="serviceOrder.defaultAccountLineStatus" /> 
	<s:hidden name="serviceOrder.bookingAgentCheckedBy"value="%{serviceOrder.bookingAgentCheckedBy}" />
	<s:hidden name="vanlineseqNum"  />
	<s:hidden name="miscellaneous.status"  value="%{miscellaneous.status}" />
	<s:hidden name="shipSize" />
	<s:hidden name="minShip" />
	<s:hidden name="countShip"/>	
	<s:hidden name="serviceOrder.revised" /> 
	<c:if test="${not empty serviceOrder.revisedDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.revisedDate" /></s:text>
			 <s:hidden  name="serviceOrder.revisedDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.revisedDate}">
		 <s:hidden   name="serviceOrder.revisedDate"/> 
	 </c:if>
	<s:hidden name="serviceOrder.carrierDeparture" /> 
	<s:hidden name="serviceOrder.carrierArrival" />
	<s:hidden name="serviceOrder.brokerCode" />
	<c:if test="${serviceOrder.corpID!='STVF'}">
	<s:hidden name="serviceOrder.originAgentCode" />
	</c:if>
	<s:hidden name="serviceOrder.originSubAgentCode" />
	<s:hidden name="serviceOrder.destinationAgentCode" />	
	<s:hidden name="checkConditionForOriginDestin" />
	 <s:hidden name="stdAddDestinState" />
	 <s:hidden name="stdAddOriginState" />
	 <s:hidden name="defaultTemplate" id="defaultTemplate"/>
	 <s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
	 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
	 <configByCorp:fieldVisibility componentId="component.field.ServiceOrder.checkCompanyBAA">
	<s:hidden name="checkCompanyBA" value="YES"/>
	</configByCorp:fieldVisibility>
	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
	<s:hidden name="setDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
	<s:hidden name="setDefaultDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		<s:hidden name="rollUpInvoiceFlag" value="True" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Description.chargeCode.copywithPricing">
		<s:hidden name="chargeCodeDescriptionFlag" value="true" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="closedCompanyDivision" value="N" />
	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
		<c:set var="closedCompanyDivision" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="checkPropertyAmountComponent" value="N" />
	<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		<c:set var="checkPropertyAmountComponent" value="Y" />
	</configByCorp:fieldVisibility>
	<s:hidden name="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" />
	<s:hidden name="msgClicked" id="msgClicked"/>
	<s:hidden name="surveyAnsByUserVal" id="surveyAnsByUserVal" />
	 <c:if test="${not empty serviceOrder.hsrgSoInsertRelovision}">
		 <s:text id="serviceOrderHsrgSoInsertRelovisionDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.hsrgSoInsertRelovision" /></s:text>
			 <s:hidden  name="serviceOrder.hsrgSoInsertRelovision" value="%{serviceOrderHsrgSoInsertRelovisionDate}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.hsrgSoInsertRelovision}">
		 <s:hidden   name="serviceOrder.hsrgSoInsertRelovision"/> 
	 </c:if>	
	 <c:if test="${not empty miscellaneous.assignDate}">
		 <s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.assignDate" /></s:text>
			 <s:hidden  name="miscellaneous.assignDate" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.assignDate}">
		 <s:hidden   name="miscellaneous.assignDate"/> 
	 </c:if>
	 <c:if test="${serviceOrder.job =='RLO'}">
	 <s:hidden  name="reloServiceType" value="${serviceOrder.serviceType}" />
	 </c:if>
	 <c:if test="${serviceOrder.job !='RLO'}">
	 <s:hidden  name="reloServiceType" value="" />
	 </c:if>
      <s:hidden name="serviceOrder.grpID"/>
	 <c:if test="${not empty miscellaneous.settledDate}">
		 <s:text id="customerFileSettledDateFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.settledDate" /></s:text>
			 <s:hidden  name="miscellaneous.settledDate" value="%{customerFileSettledDateFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.settledDate}">
		 <s:hidden   name="miscellaneous.settledDate"/> 
	 </c:if>	 
	  <c:if test="${not empty serviceOrder.redskyBillDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.redskyBillDate" /></s:text>
			 <s:hidden  name="serviceOrder.redskyBillDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.redskyBillDate}">
		 <s:hidden   name="serviceOrder.redskyBillDate"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.welcomeMailSentOn}">
			<s:text id="serviceOrderWelcomeMailSentOn" name="${FormDateValue}"><s:param name="value" value="serviceOrder.welcomeMailSentOn" /></s:text>
			<s:hidden  name="serviceOrder.welcomeMailSentOn" value="%{serviceOrderWelcomeMailSentOn}" /> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.importPaperOn}">
			<s:text id="serviceOrderImportPaperOn" name="${FormDateValue}"><s:param name="value" value="serviceOrder.importPaperOn" /></s:text>
			<s:hidden  name="serviceOrder.importPaperOn" value="%{serviceOrderImportPaperOn}" /> 
	 </c:if>
	 <s:hidden name="serviceOrder.quoteNumber"/>
	 <s:hidden name="serviceOrder.quoteAccept"/> 
	 <s:hidden  name="serviceOrder.incExcServiceType"/>
	 <s:hidden  name="servIncExcDefault"/> 
	<s:hidden name="serviceOrder.destinationSubAgentCode" />
	<s:hidden name="serviceOrder.vendorCode" />
	<s:hidden name="serviceOrder.forwarderCode" />
	<s:hidden name="miscellaneous.unit1"/>
    <s:hidden name="miscellaneous.unit2"/>
    <s:hidden name="miscellaneous.totalActualRevenue" />
    <s:hidden name="miscellaneous.actualLineHaul" />	
    <s:hidden name="miscellaneous.totalDiscountPercentage" />
	<s:hidden name="oldStatus" value="%{serviceOrder.status}" /> 
	<s:hidden name="jobtypeSO" value="%{serviceOrder.job}" /> 
	<s:hidden name="routingSO" value="${serviceOrder.routing}" />
    <s:hidden name="modeSO" value="${serviceOrder.mode}"/>
    <s:hidden name="packingModeSO" value="${serviceOrder.packingMode}"/>
    <s:hidden name="commoditySO" value="${serviceOrder.commodity}"/>
    <s:hidden name="serviceTypeSO" value="${serviceOrder.serviceType}"/>
    <s:hidden name="companyDivisionSO" value="${serviceOrder.companyDivision}"></s:hidden>
    <s:hidden name="accountIdCheck"/>
	<s:hidden name="jobtypeCheck" value="" /> 
	<s:hidden name="accId" />
    <s:hidden name="accEstimateQuantity" />
    <s:hidden name="accEstimateRate" />
    <s:hidden name="accEstimateExpense" />
    <s:hidden name="accEstimateSellRate" />
    <s:hidden name="accEstimateRevenueAmount" />
    <s:hidden name="accEstSellLocalAmount" />
	<s:hidden name="oldStatusNumber" value="%{serviceOrder.statusNumber}" /> 
	<s:hidden  name="billing.contract" />
	<s:hidden name="salesCommisionRate" value="${salesCommisionRate}"/>
	<s:hidden name="grossMarginThreshold" value="${grossMarginThreshold}"/>
	<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
	<s:hidden name="inEstimatedTotalRevenue" value="${serviceOrder.estimatedTotalRevenue}"/>
	<s:hidden name="inRevisedTotalExpense" value="${serviceOrder.revisedTotalExpense}"/>
	<s:hidden name="inRevisedTotalRevenue" value="${serviceOrder.revisedTotalRevenue}"/>
	<s:hidden name="inActualExpense" value="${serviceOrder.actualExpense}"/>
	<s:hidden name="inActualRevenue" value="${serviceOrder.actualRevenue}"/>
	<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
	<s:hidden name="miscellaneous.tripNumber"/>
	<s:hidden name="buyDependSell" />
	<s:hidden name="serviceOrder.CAR_vendorCode" />
	<s:hidden name="serviceOrder.COL_vendorCode" />
	<s:hidden name="serviceOrder.TRG_vendorCode" />
	<s:hidden name="serviceOrder.HOM_vendorCode" />
	<s:hidden name="serviceOrder.RNT_vendorCode" />
	<s:hidden name="serviceOrder.SET_vendorCode" />
	<s:hidden name="serviceOrder.LAN_vendorCode" />
	<s:hidden name="serviceOrder.MMG_vendorCode" />
	<s:hidden name="serviceOrder.ONG_vendorCode" />
	<s:hidden name="serviceOrder.PRV_vendorCode" />
	<s:hidden name="serviceOrder.AIO_vendorCode" />
	<s:hidden name="serviceOrder.EXP_vendorCode" />
	<s:hidden name="serviceOrder.RPT_vendorCode" />
	<s:hidden name="serviceOrder.SCH_schoolSelected" />
	<s:hidden name="serviceOrder.TAX_vendorCode" />
	<s:hidden name="serviceOrder.TAC_vendorCode" />
	<s:hidden name="serviceOrder.TEN_vendorCode" />
	<s:hidden name="serviceOrder.VIS_vendorCode" />
	<s:hidden name="serviceOrder.WOP_vendorCode" />
	<s:hidden name="serviceOrder.REP_vendorCode" />
	<s:hidden name="serviceOrder.HOB_vendorCode" />
	<s:hidden name="serviceOrder.FLB_vendorCode" />
 <s:hidden name="serviceOrder.RLS_vendorCode" />
 <s:hidden name="serviceOrder.CAT_vendorCode" />
 <s:hidden name="serviceOrder.CLS_vendorCode" />
 <s:hidden name="serviceOrder.CHS_vendorCode" />
 <s:hidden name="serviceOrder.DPS_vendorCode" />
 <s:hidden name="serviceOrder.HSM_vendorCode" />
 <s:hidden name="serviceOrder.PDT_vendorCode" />
 <s:hidden name="serviceOrder.RCP_vendorCode" />
 <s:hidden name="serviceOrder.SPA_vendorCode" />
 <s:hidden name="serviceOrder.TCS_vendorCode" />
  <s:hidden name="serviceOrder.MTS_vendorCode" />
 <s:hidden name="serviceOrder.DSS_vendorCode" />
 <s:hidden name="serviceOrder.customerRating" />
  <s:hidden name="serviceOrder.moveType" />
 <s:hidden name="serviceOrder.FRL_vendorCode" />
 <s:hidden name="serviceOrder.APU_vendorCode" /> 
 <s:hidden name="userQuoteServices" />
 <s:hidden name="serviceOrder.projectManager"/>
 <s:hidden  name="serviceOrder.incExcDocServiceType"/>
 <s:hidden name="serviceOrder.tripNumber"  />
 <s:hidden name="serviceOrder.INS_vendorCode" />
 <s:hidden name="serviceOrder.INP_vendorCode" />
 <s:hidden name="serviceOrder.EDA_vendorCode" />
 <s:hidden name="serviceOrder.TAS_vendorCode" />
 <s:hidden name="serviceOrder.jimExtract" />
 <c:set var="checkFieldVisibilityForPricing" value="${serviceOrder.tripNumber}"/> 
	<c:if test="${not empty serviceOrder.id}">
	<c:if test="${not empty billing.billComplete}">
	 <s:text id="billingBillingComplete" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
	 <s:hidden  name="billing.billComplete" value="%{billingBillingComplete}" />  
	</c:if>
	<c:if test="${empty billing.billComplete}">
	<s:hidden  name="billing.billComplete" />
	</c:if>
	<s:hidden name="billing.billCompleteA" value="%{billing.billCompleteA}" />
	<c:if test="${not empty trackingStatus.deliveryA}">
	<s:text id="trackingStatusDeliveryA" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryA" /></s:text>
	<s:hidden  name="trackingStatus.deliveryA" value="%{trackingStatusDeliveryA}" />  
	</c:if>
	<c:if test="${empty trackingStatus.deliveryA}">
	<s:hidden  name="trackingStatus.deliveryA" />  
	</c:if>
	<s:hidden  name="claimCount" value="${claimCount}"/> 
	<s:hidden name="invoiceCount" value="${invoiceCount}"/> 
	</c:if>
	<c:if test="${not empty serviceOrder.id && serviceOrder.job=='RLO'}">
	<c:if test="${not empty dspDetails.serviceCompleteDate}">
	 <s:text id="dspDetailsServiceCompleteDate" name="${FormDateValue}"> <s:param name="value" value="dspDetails.serviceCompleteDate" /></s:text>
	 <s:hidden  name="dspDetails.serviceCompleteDate" value="%{dspDetailsServiceCompleteDate}" />
	</c:if>
	<c:if test="${empty dspDetails.serviceCompleteDate}">
	<s:hidden  name="dspDetails.serviceCompleteDate" />
	</c:if>
	</c:if>
	<s:hidden name="calOpener" value="notOPen" />
	<c:if test="${not empty serviceOrder.ETD}">
		 <s:text id="serviceOrderETD" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ETD" /></s:text>
		<s:hidden name="serviceOrder.ETD" value="%{serviceOrderETD}" />  
	 </c:if>
	 <c:if test="${empty serviceOrder.ETD}">
		 <s:hidden name="serviceOrder.ETD"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.ATD}">
		 <s:text id="serviceOrderATD" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ATD" /></s:text>
			 <s:hidden  name="serviceOrder.ATD" value="%{serviceOrderATD}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ATD}">
		 <s:hidden   name="serviceOrder.ATD"/> 
	 </c:if>
	<c:if test="${not empty serviceOrder.ETA}">
		 <s:text id="serviceOrderETA" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ETA" /></s:text>
			 <s:hidden  name="serviceOrder.ETA" value="%{serviceOrderETA}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ETA}">
		 <s:hidden name="serviceOrder.ETA"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.ATA}">
		 <s:text id="serviceOrderATA" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ATA" /></s:text>
			 <s:hidden  name="serviceOrder.ATA" value="%{serviceOrderATA}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ATA}">
		 <s:hidden   name="serviceOrder.ATA"/> 
	 </c:if>		
	<s:hidden name="vanlineCodeForRef"  />  
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<s:hidden id="countOriginDetailNotes" name="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>" />
	<s:hidden id="countDestinationDetailNotes" name="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>" />
	<s:hidden id="countWeightDetailNotes" name="countWeightDetailNotes"	 value="<%=request.getParameter("countWeightDetailNotes") %>" />
	<s:hidden id="countVipDetailNotes" name="countVipDetailNotes"  	value="<%=request.getParameter("countVipDetailNotes") %>" />
	<s:hidden id="countSurveyNotes" name="countSurveyNotes"  	value="<%=request.getParameter("countSurveyNotes") %>" />
	<s:hidden id="countServiceOrderNotes" name="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>" />
	<s:hidden id="countCustomerFeedbackNotes" name="countCustomerFeedbackNotes" value="<%=request.getParameter("countCustomerFeedbackNotes") %>" />
	<c:set var="countOriginDetailNotes"
		value="<%=request.getParameter("countOriginDetailNotes") %>" />
	<c:set var="countDestinationDetailNotes"
		value="<%=request.getParameter("countDestinationDetailNotes") %>" />
	<c:set var="countWeightDetailNotes"
		value="<%=request.getParameter("countWeightDetailNotes") %>" />
	<c:set var="countVipDetailNotes"
		value="<%=request.getParameter("countVipDetailNotes") %>" />
	<c:set var="countServiceOrderNotes"
		value="<%=request.getParameter("countServiceOrderNotes") %>" />
	<c:set var="countCustomerFeedbackNotes"
		value="<%=request.getParameter("countCustomerFeedbackNotes") %>" />
	<c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />
	<c:if test="${validateFormNav == 'OK'}">
		<c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accounting' }">
				<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.newAccounting' }">
				<c:redirect url="/pricingList.html?sid=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.billing' }">
				<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
			</c:when>
			<c:when test="${gotoPageString == 'gototab.domestic' }">
				<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
			</c:when>
			<c:when test="${gotoPageString == 'gototab.ticket' }">
				<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.claims' }">
				<c:redirect url="/claims.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.surveydetail' }">
				<c:redirect url="/inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.cartons' }">
				<c:redirect url="/cartons.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.vehicles' }">
				<c:redirect url="/vehicles.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.containers' }">
				<c:if test="${forwardingTabVal!='Y'}">
					<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
				</c:if>
			</c:when>
			
			<c:when test="${gotoPageString == 'gototab.OI' }">
			<c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
			</c:when>
			
			<c:when test="${gotoPageString == 'gototab.operationResourceOI' }">
            <c:redirect url="/operationResourceFromAcPortal.html?id=${serviceOrder.id}"/>
            </c:when>
			
			<c:when test="${gotoPageString == 'gototab.servicepartners' }">
				<c:redirect url="/servicePartnerss.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.customerfile' }">
				<c:redirect url="/editCustomerFile.html?id=${customerFile.id}" />
			</c:when>			
			<c:when test="${gotoPageString == 'gototab.criticaldate' }">			
				<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
					<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
				</sec-auth:authComponent>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
	</c:if>
 				<div id="generateInv" class="message" style="display:none;width:300px;background:none;border:none;">
 				<table style="margin:0px;padding:0px;"><tr>
 				<td>
				<font color="red" style="text-align:center;width:100%;font:12px arial,verdana;"><b>
					<script type="text/javascript"> 
					document.forms['serviceOrderForm'].elements['generateMassage'].value = getCookie('generateMassage');
					if(document.forms['serviceOrderForm'].elements['generateMassage'].value == "invoiceMassage"){
						document.getElementById('generateInv').style.display = 'block' 
						document.write("Invoice has been Generated successfully.");
						document.forms['serviceOrderForm'].elements['accUpdateAjax'].value="T";
						document.forms['serviceOrderForm'].elements['generateMassage'].value="";
					}
					   document.cookie = 'generateMassage' + "=; expires=" +  new Date().toGMTString();
					</script>
					</b></font></td></tr></table>
				</div>
	
	<div id="newmnav" style="float: left;">
	<ul><c:if test="${not empty serviceOrder.id}">

	     <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	         <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	         	  <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
       <c:if test ="${(empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove') && serviceOrder.corpID!='STVF'}">
		<li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
      </c:if>
    
		<c:if test="${(empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove') && serviceOrder.corpID=='STVF'}">
	<li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
		</c:if>
		</c:if>
		</sec-auth:authComponent>
		  </configByCorp:fieldVisibility>
		<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<c:if test ="${(empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove') && serviceOrder.corpID!='STVF'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			class="current"><span>S/O Details</span></a>
		</li>
		</c:if>
		<c:if test="${(empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove') && serviceOrder.corpID=='STVF'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="completeTimeString();return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			class="current"><span>S/O Details</span></a>
		</li>
		</c:if>
		<c:if test="${(not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.corpID!='STVF'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			class="current"><span>Quotes</span></a>
		</li>
		</c:if>
		<c:if test="${(not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.corpID=='STVF'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="completeTimeString();return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			class="current"><span>Quotes</span></a>
		</li>
		</c:if>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.billingTab">
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.billing');return saveAuto('none');"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		<c:choose>
			<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			   <li><a onmouseover="return chkSelect();" onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			</c:when> --%>
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.accounting');return saveAuto('none');"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </c:if>
	  </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
	  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		<c:choose> 
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.newAccounting');return saveAuto('none');"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </c:if>
	  </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
	  <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	  </sec-auth:authComponent>	  
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      
	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  <li><a onclick="setReturnString('gototab.OI');return saveAuto('none');"><span>O&I</span></a></li>
	  </sec-auth:authComponent>
	  </c:if>
	  	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
         <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
  	 </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
	  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	  <c:if test="${usertype!='ACCOUNT'}">
	    <c:if test="${serviceOrder.job !='RLO'}">	
<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
			<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.containers'); return saveAuto('none');"><span>Forwarding</span></a></li>
			</c:if>	  	
	  </c:if>
	  </c:if>
	  <c:if test="${usertype=='ACCOUNT'}">
	  <c:if test="${serviceOrder.job !='RLO'}">
	   <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
	   		<li><li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li></li>
	  		</c:if>
	  </c:if>
	  </c:if>
	  </c:if>
	  </sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
		<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		<c:if test="${serviceOrder.job !='RLO'}">  
			<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.domestic');return saveAuto('none');"><span>Domestic</span></a></li>
		</c:if>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
      <c:if test="${serviceOrder.job =='INT'}">
         <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.domestic');return saveAuto('none');"><span>Domestic</span></a></li>
      </c:if>
      </sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.statusTab">
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.status');return saveAuto('none');"><span>Status</span></a></li>
	</sec-auth:authComponent>	
  <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
 <%--	<c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
	<sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
	</sec-auth:authComponent>
	</c:if>
	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.ticket');return saveAuto('none');"><span>Ticket</span></a></li>
	</c:if>
	</c:if>
	</sec-auth:authComponent>	
	<configByCorp:fieldVisibility componentId="component.standard.claimTab">
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.claims');return saveAuto('none');"><span>Claims</span></a></li>
	</c:if>
	</c:if> 
	</sec-auth:authComponent>
	</configByCorp:fieldVisibility>
	<sec-auth:authComponent componentId="module.tab.serviceorder.ticketTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.ticket');return saveAuto('none');"><span>Ticket</span></a></li>
	</c:if>
	</c:if>
	</sec-auth:authComponent>	
	<configByCorp:fieldVisibility componentId="component.standard.claimTab">
	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job !='RLO'}">
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.claims');return saveAuto('none');"><span>Claims</span></a></li>
	</c:if>
	</c:if> 
	</sec-auth:authComponent>
	</configByCorp:fieldVisibility>
	<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	<c:if test="${voxmeIntergartionFlag=='true'}">		
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.surveydetail');return saveAuto('none');"><span>Survey Details</span></a></li>
	</c:if>
	<configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
	 <%-- <li><a href="newServiceOrderDetailPage.html?id=${serviceOrder.id}"><span>New Service Order Deatils</span></a></li> --%>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.customerFileTab">	
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.customerfile');return saveAuto('none');"><span>Customer File</span></a></li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
	 <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.criticaldate');return saveAuto('none');"><span>Critical Dates</span></a></li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.reportTab">
	<c:if test="${serviceOrder.moveType=='Quote'}">	
			<li><a onmouseover="return chkSelect();" onclick="validateTabs('quote');"><span>Forms</span></a></li>
	</c:if>
	<c:if test="${serviceOrder.moveType!='Quote'}">	
			<li><a onmouseover="return chkSelect();" onclick="validateTabs('notQuote');"><span>Forms</span></a></li>
	</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">			
			<li><a onmouseover="return chkSelect();" onclick="validateTabs('audit')"><span>Audit</span></a></li>
	</sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
         <li><a onclick="validateTabs('document');"><span>Document</span></a></li>
 	</sec-auth:authComponent>
 	<c:if test="${usertype=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a onclick="validateTabs('viewEmail');"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
  	</c:if>
    </c:if>
		<c:if test="${empty serviceOrder.id}">
		<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();" href="customerServiceOrders.html?id=${customerFile.id}"	class="current"><span>S/O Details</span></a></li>
		</c:if>
		<c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();" href="customerServiceOrders.html?id=${customerFile.id}"	class="current"><span>Quotes</span></a></li>
		</c:if>
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Accounting</span></a></li>
		</sec-auth:authComponent>
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Forwarding</span></a></li>
		</c:if>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Domestic</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Status</span></a></li>
		<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Ticket</span></a></li>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.standard.claimTab">
		<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Claims</span></a></li>
		</c:if>
		</configByCorp:fieldVisibility>
		<li><a href="editCustomerFile.html?id=${customerFile.id}&from=list"><span>Customer File</span></a></li>
		<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Critical Dates</span></a></li>
		</sec-auth:authComponent>
		<!--<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.customerfile');return ContainerAutoSave('none');"><span>Customer File</span></a></li>
		--><li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Forms</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Audit</span></a></li>
		</c:if>
	</ul>
	</div>
	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
	<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>		
		</c:if>
		</tr></table>
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;!padding-bottom:6px;"></div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
	<td>
  		<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%" >
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
				<tbody>
				<tr>
						<td height="5px"></td>
						</tr>
					<tr>
					&nbsp;
						<td align="right" class="listwhitetext" style="width:85px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.prefix' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom">Title / Rank</td>
						<td align="left" class="listwhitetext" width="5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.firstName' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="center" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.mi' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.lastName' /><font color="red" size="2">*</font></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber"><fmt:message key='serviceOrder.socialSecurityNumber'/></configByCorp:fieldVisibility></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
						<%-- Added by kunal for ticket number: 6092 and 6218(later) --%>
						<c:set var="isVipFlag" value="false" />
						<c:if test="${serviceOrder.vip}">
							<c:set var="isVipFlag" value="true" />
						</c:if>
						<div style="position:absolute;top:10px;margin-left:20px;!margin-left:-35px;">
						<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
						</div>
						</td>
						</tr>
					<tr>
						<td align="left" style="width:85px"></td>
						<td align="left" style="width:5px"><s:textfield cssClass="input-textUpper" id="tabindexFlag" name="serviceOrder.prefix" cssStyle="width:25px" maxlength="15" onfocus="myDate();" readonly="true" tabindex=""/></td>
						<td align="left" style="width:3px">
						<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="serviceOrder.rank" cssStyle="width:55px" maxlength="7" readonly="true" tabindex=""/> </td>
						<td align="left" style="width:3px">
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.firstName" cssStyle="width:155px" maxlength="80" readonly="true" tabindex="" /></td>
						<td align="left" style="width:3px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.mi" cssStyle="width:13px" maxlength="1" readonly="true" tabindex="" /></td>
						<td align="left" style="width:17px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.lastName" cssStyle="width:165px" maxlength="80" required="true" readonly="true" tabindex="" /></td>
						<td align="left" style="width:3px"></td>
						     <td align="left" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber">
					        <s:textfield cssClass="input-textUpper" key="serviceOrder.socialSecurityNumber" size="7" maxlength="9" readonly="true" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="" cssStyle="width:88px"/>
					        </configByCorp:fieldVisibility></td>
						<td align="right"><s:checkbox id="serviceOrder.vip" name="serviceOrder.vip" value="${isVipFlag}" fieldValue="true" onchange="changeStatus();" onclick="setVipImage();" tabindex="" /></td>
						<td align="left" class="listwhitebox" valign="middle"><fmt:message key='serviceOrder.vip' /></td>
						<td width="85px"></td>
						<c:if test="${empty serviceOrder.id}">
							<td width="90px" align="left"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
						</c:if>
						<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when
									test="${countVipDetailNotes == '0' || countVipDetailNotes == '' || countVipDetailNotes == null}">
									<td width="90px" align="left"><img id="countVipDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:when>
								<c:otherwise>
									<td width="90px" align="left"><img id="countVipDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					<tr>
					 <td>
					   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="5px"></td>
						</tr>
							<tr>
								<td style="width:85px"></td>
								<td align="left" class="listwhitetext">S/O#</td>
								<td align="right" class="listwhitetext" style="width:7px"></td>								
								<c:choose>
								<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
								<td></td>
								</c:when>
								<c:otherwise>
								<td align="left" class="listwhitetext">Status</td>
								</c:otherwise>
								</c:choose>
								<c:if test="${checkAccessQuotation}">
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" class="listwhitetext" valign="bottom"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteStatus',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.quoteStatus'/></a></td>								
								</c:if>
								</c:if>
								<td align="right" class="listwhitetext" style="width:7px"></td>
								<td align="left" class="listwhitetext">Status Date</td>
								<td align="left" class="listwhitetext" style="width:7px"></td>
								<td id="reasonHid" align="left" class="listwhitetext">Status Reason</td>
								<c:choose>
								<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
								<td id="reasonHid2" align="left" class="listwhitetext"></td>
								</c:when>
								<c:otherwise>
								<td id="reasonHid2" align="left" class="listwhitetext">On Hold Until</td>
								</c:otherwise>
								</c:choose>
								
							</tr>
							</tr>
							<tr>
								<td align="left" style="width:85px"></td>
								<td align="left" style="width:27px">
								<s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" cssStyle="width:276px" size="22" maxlength="15" readonly="true" tabindex="" /></td>
								<td align="left" style="width:17px"></td>
								<c:choose>
								<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
								<td><s:hidden name="serviceOrder.status" /></td>
								</c:when>
								<c:otherwise>
				                <c:if test="${serviceOrder.corpID!='STVF'}">
								<td><s:select cssClass="list-menu" name="serviceOrder.status" list="%{JOB_STATUS}" headerKey=""	headerValue=""  onchange=" changeStatus(), checkStatus();" cssStyle="width:132px" tabindex="" /></td>
								</c:if>
								<c:if test="${serviceOrder.corpID=='STVF'}">
								<td><s:select cssClass="list-menu" id="status1" name="serviceOrder.status" list="%{JOB_STATUS}" headerKey=""	headerValue=""  onchange=" changeStatus(), checkStatus();" cssStyle="width:132px" tabindex="" onfocus="surveyDateTime();"/></td>
								</c:if>
								</c:otherwise>
								</c:choose>
								<c:if test="${checkAccessQuotation}">							
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" style=""><s:select cssClass="list-menu" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:132px" onchange="checkQuotation();changeStatus();" tabindex="" /></td>
								</c:if>
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='BookedMove'}">
								<s:hidden name="serviceOrder.quoteStatus" />
								</c:if>
								</c:if>
								<c:if test="${not empty serviceOrder.statusDate}">
									<s:text id="statusDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.statusDate"  />
									</s:text>
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:81px" maxlength="11" readonly="true" tabindex="" /></td>
								</c:if>
								<c:if test="${empty serviceOrder.statusDate}">
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" cssStyle="width:81px" maxlength="11" readonly="true" tabindex="" /></td>
								</c:if>
								<td align="left" class="listwhitetext"></td>
								<td id="reasonHid1"><s:select cssClass="list-menu" key="serviceOrder.statusReason" list="%{statusReason}" onchange="changeStatus();" cssStyle="width:100px" tabindex=""/></td>
								<td id="reasonHid3">
									<c:choose>
								    <c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
									<s:hidden name="serviceOrder.nextCheckOn" />
									</c:when>
									<c:otherwise>
									<c:if test="${not empty serviceOrder.nextCheckOn}">
										<s:text id="trackingStatusNextCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.nextCheckOn"/></s:text>
										<s:textfield cssClass="input-text" id="nextCheckOn" name="serviceOrder.nextCheckOn" value="%{trackingStatusNextCheckOnFormattedValue}" required="true" cssStyle="width:60px;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex=""/>
											<img id="nextCheckOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>	
									<c:if test="${empty serviceOrder.nextCheckOn}">
										<s:textfield cssClass="input-text" id="nextCheckOn" name="serviceOrder.nextCheckOn" required="true" cssStyle="width:60px;" maxlength="11" onkeydown="onlyDel(event,this)" tabindex=""/><img id="nextCheckOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									</c:otherwise>
									</c:choose>
								</td>
								<script type="text/javascript">
                                 cheackStatusReasonLoad();
                             	</script>                              
							</tr>
						</tbody>
					</table>
					</td>
					</tr>
					<tr>
						<td height="5px"></td>
						</tr>
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">Origin&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
									<td align="left" class="listwhitetext" style="width:7px"></td>
									<td align="left" class="listwhitetext">Destination&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" cssStyle="width:220px" name="serviceOrder.originCityCode" size="22" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" name="serviceOrder.originCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
									<td align="right" class="listwhitetext" style="width:17px"></td>
									<td align="left" class="listwhitetext" >
									<s:textfield cssClass="input-textUpper" key="serviceOrder.destinationCityCode" cssStyle="width:220px" size="22" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="9px"></td>
									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
									<td width="55px"></td>									
									<c:if test="${not empty serviceOrder.id}">
							 		<c:if test="${jobTypes !='UVL' && jobTypes !='MVL' && jobTypes !='ULL' && jobTypes !='MLL' && jobTypes !='DOM' && jobTypes !='LOC' && serviceOrder.originCountryCode != serviceOrder.destinationCountryCode}">
									<td><a><img src="images/viewcustom2.jpg" onclick="findCustomInfo(this);" alt="Customs" title="Customs" /></a></td>
						     		</c:if>
						    		</c:if>
						    
						     		<c:if test="${not empty serviceOrder.id}">
							 		<td><a><img src="images/viewdoc2.jpg" onclick="findDocInfo(this);" alt="Docs" title="Docs" /></a></td>
						     		</c:if>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>					
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
							<tbody>
							<tr>
							<td style="width:85px; !width: 85px"></td>
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px">Name</td>
							<td></td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px"><c:if  test="${companies == 'Yes'}">Company&nbsp;Division<font color="red" size="2">*</font></c:if></td>
							</tr>
								<tr>
									<td align="right" class="listwhitetext" style="">Booking&nbsp;Agent:&nbsp;</td>
									<c:choose>
						            <c:when test="${isNetworkRecord}">
						            <td><s:textfield cssClass="input-textUpper" id="soBookingAgentCodeId" readonly="true" name="serviceOrder.bookingAgentCode" cssStyle="width:135px;" maxlength="8" onchange="" onblur="" onselect="" tabindex=""/></td>
									<td style="width:21px"><img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="" src="images/navarrow.gif" /></td>
						            <td align="left"><s:textfield cssClass="input-textUpper" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="" onchange=""  size="44" maxlength="250" cssStyle="width:220px" tabindex="" />
									<div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
						            </c:when>
						            <c:otherwise>
									<c:choose>
						            <c:when test="${hasBookingAgentDataSecuritySet}">
						            <td><s:textfield cssClass="input-text" id="soBookingAgentCodeId" name="serviceOrder.bookingAgentCode" cssStyle="width:135px;" maxlength="8" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'noteNavigations');findBookingAgentName(), getVanlineCode('AG', this);findCoordinatorByBA();" onblur="findCompanyDivisionByBookAg('onchange');showPartnerAlert('onchange',this.value,'noteNavigations');" onselect="getVanlineCode('AG', this);showPartnerAlert('onchange',this.value,'noteNavigations');" tabindex=""/></td>
									<td style="width:21px"><img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="openBookingAgentBookingAgentSetPopWindow(); document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						            </c:when>
									<c:otherwise>
									<td><s:textfield cssClass="input-text" id="soBookingAgentCodeId" name="serviceOrder.bookingAgentCode" cssStyle="width:135px;" maxlength="8" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'noteNavigations');findBookingAgentName(), getVanlineCode('AG', this);findCoordinatorByBA();" onblur="findCompanyDivisionByBookAg('onchange');showPartnerAlert('onchange',this.value,'noteNavigations');" onselect="getVanlineCode('AG', this);showPartnerAlert('onchange',this.value,'noteNavigations');" tabindex=""/></td>
									<td style="width:21px"><img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="openBookingAgentPopWindow(); document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();" src="<c:url value='/images/open-popup.gif'/>" /></td>
									</c:otherwise>
									</c:choose>
									<c:if test="${agentSearchValidation}">
									<td align="left"><s:textfield cssClass="input-text" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('soBookingAgentNameId','soBookingAgentCodeId','soBookingAgentNameDivId',' and (isAgent=true)','',event);" onchange="findPartnerDetailsByName('soBookingAgentCodeId','soBookingAgentNameId');"  size="44" maxlength="250" cssStyle="width:222px" tabindex="" />
									</c:if>
									<c:if test="${!agentSearchValidation}">
									<td align="left"><s:textfield cssClass="input-text" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('soBookingAgentNameId','soBookingAgentCodeId','soBookingAgentNameDivId',' and (isAccount=true or isAgent=true or isVendor=true )','',event);" onchange="findPartnerDetailsByName('soBookingAgentCodeId','soBookingAgentNameId');"  size="44" maxlength="250" cssStyle="width:222px" tabindex="" />
									</c:if>
									<div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
									</td>
									</c:otherwise>
									</c:choose>
									<td align="left" width="25">
										<a><img class="openpopup" id="noteNavigations" onclick="getBigPartnerAlert(document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value,'noteNavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a> 
									</td>
									
									<c:choose>
								    <c:when test="${checkAccessQuotation && serviceOrder.corpID =='HOLL' && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
												<td width="25px"></td>
												<td class="listwhitetext" style="" align="right">Quote&nbsp;Reg&nbsp;#&nbsp;</td>
												<td><s:textfield cssClass="input-textUpper" key="serviceOrder.clientId" cssStyle="width:75px" maxlength="8" readonly="true"/></td>
									</c:when>
									<c:otherwise>
									<s:hidden name="serviceOrder.clientId"/>
									</c:otherwise>
									</c:choose>
									<td width="23px"></td>
									<c:if  test="${companies == 'Yes'}">
										<td align="left">
										<c:if test="${not empty serviceOrder.id}">
										<s:select  cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:251px" onchange="getJobList('unload');changeStatus();checkTransferDate(); changebAgentCheckedBy();" headerKey="" headerValue="" tabindex="" />
										</c:if>
										<c:if test="${empty serviceOrder.id}">
										<s:select  cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:251px" onchange="populateInclusionExclusionData('default','1','F');getJobList('unload');changeStatus();checkTransferDate(); changebAgentCheckedBy();getIncExcDocDetails();" headerKey="" headerValue="" tabindex="" />
										</c:if>
										</td>
									</c:if>
									<c:if  test="${companies != 'Yes'}">
										<s:hidden name="serviceOrder.companyDivision"/>
									</c:if>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
						<tr>
							<td>
								<table id="hidVanLine" class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
									<tbody>
										<tr>
												<td height="5px"></td>
										</tr>
										<tr>
												<td style="width:85px; !width: 85px"></td>
												<td align="left" class="listwhitetext" colspan="3">Booking Agent Vanline Code</td>
										</tr>
									 	<tr>
												<td style="width:85px; !width: 85px"></td>
												<td><s:textfield cssClass="input-text" key="serviceOrder.bookingAgentVanlineCode" cssStyle="width:135px" maxlength="8" onchange="valid(this,'special')getVanlineAgent('BA');" tabindex=""/></td>
												<td colspan="2" align="left">
													<a><img class="openpopup" id="navigations" onclick="getVanlineCode('AG', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Vanline List" title="Vanline List" /></a> 
												</td>
										</tr>
										
									</tbody>
								</table>
							</td>
						</tr>
					</configByCorp:fieldVisibility>
					<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="">
							<tbody>
							<tr><td height="5px"></td></tr>
									<tr>									 
									<td align="right" class="listwhitetext" style="width:85px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td align="left" class="listwhitetext" valign="bottom" style=""><div id="registrationHideLabel">
									<fmt:message key='serviceOrder.registrationNumber' /></div></td>
									<td align="right" class="listwhitetext" style="width:15px"></td>
									<td align="left" class="listwhitetext" valign="bottom" style="">
									<fmt:message key='serviceOrder.job' /><font color="red" size="2">*</font></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="routingLabel"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.routing',this);return false" tabindex="">
										<fmt:message key='serviceOrder.routing' />
										<c:if test="${costElementFlag}">
										<font color="red" size="2">*</font></c:if></a></div></td>
									<td align="right" class="listwhitetext" style=""></td>
								</tr>
								<tr>
									<td align="right">
									<div id="hidRegNum">
									<c:if test="${empty sysDefSequenceNum}">
									<img id="rateImage"  class="openpopup" width="16" height="16" align="top" src="${pageContext.request.contextPath}/images/arrow-loadern.gif" onclick="findJobSeq()"/>
									</c:if> 
									</div>
									</td> 
									<td align="left" style=""><div id="registrationHide"><s:textfield cssClass="input-text" name="serviceOrder.registrationNumber" id="regist" onchange="validateRegistraionNumberAC(this.id)" onkeyup="valid(this,'special')"
										cssStyle="width:135px;" maxlength="20" required="true" tabindex="" /></div></td>
									<td align="right" class="listwhitetext" style="width:20px"></td>									
									<td align="left" style="width:200px">
									<c:if test="${not empty serviceOrder.id}">
									<configByCorp:customDropDown 	listType="map" list="${job_isactive}" fieldValue="${serviceOrder.job}"
		                              attribute="id=serviceOrderForm_serviceOrder_job class=list-menu name=serviceOrder.job  style=width:251px  headerKey='' headerValue='' onchange='findService();changeStatus(),validateRegistrationNumber(),getNewCoordAndSale(),checkJobType(this),getCommodity(),getServiceTypeByJob(),fillCommodity(),showMmCounselorByJob(),checkJobTypes(),checkGroupAgeJobType(),chekServiceType(),populateOpsPersonByJob();' onclick='fillCheckJob(this);' tabindex='' "/>
		    						</c:if>		
 									<c:if test="${empty serviceOrder.id}">
								     <configByCorp:customDropDown 	listType="map" list="${job_isactive}" fieldValue="${serviceOrder.job}"
		                              attribute="id=serviceOrderForm_serviceOrder_job class=list-menu name=serviceOrder.job  style=width:251px  headerKey='' headerValue='' onchange='populateInclusionExclusionData('default','1','F');findService(),changeStatus(),validateRegistrationNumber(),getNewCoordAndSale(),checkJobType(this),getCommodity(),getServiceTypeByJob(),fillCommodity(),showMmCounselorByJob(),checkJobTypes(),checkGroupAgeJobType(),chekServiceType(),getIncExcDocDetails(),populateOpsPersonByJob();'  onclick='fillCheckJob(this);' tabindex='' "/>
		    		             	</c:if>									
										</td>
									<td align="right" class="listwhitetext" style="width:10px"></td>
									<script type="text/javascript">
									try{
                                     checkJobTypeOnLoad();
									}catch(e){}
                                    </script>
									<td align="left" ><div id="routingFild"> 
									<c:if test="${not empty serviceOrder.id}">
									<configByCorp:customDropDown 	listType="map" list="${routing_isactive}" fieldValue="${serviceOrder.routing}"
		                              attribute="id=serviceOrderForm_serviceOrder_routing class=list-menu name=serviceOrder.routing  style=width:251px  headerKey='' headerValue='' onchange='changeStatus();'   tabindex='' "/>
									<%-- <s:select cssClass="list-menu" name="serviceOrder.routing" list="%{routing}" cssStyle="width:145px" onchange="changeStatus();" tabindex="" /> --%>
									</c:if>
									<c:if test="${empty serviceOrder.id}">
									<configByCorp:customDropDown 	listType="map" list="${routing_isactive}" fieldValue="${serviceOrder.routing}"
		                              attribute="id=serviceOrderForm_serviceOrder_routing class=list-menu name=serviceOrder.routing  style=width:251px  headerKey='' headerValue='' onchange='populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();'   tabindex='' "/>
									<%-- <s:select cssClass="list-menu" name="serviceOrder.routing" list="%{routing}" cssStyle="width:145px" onchange="populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();" tabindex="" /> --%>
									</c:if>
									</div></td>
								<td width="5px"></td>
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="labelEstimator"><fmt:message key='serviceOrder.estimator' />
									</div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="labelservice"><fmt:message key='serviceOrder.serviceType' /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><div id="modeLabel"> <a
										href="#" style="cursor:help"
										onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.mode',this);return false"
										><fmt:message
										key='serviceOrder.mode' /></a> </div>  </td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
									<td align="left" class="listwhitetext" id="carrierFildLabel"><fmt:message key='trackingStatus.flagCarrier'/>&nbsp</td>
									</configByCorp:fieldVisibility>	
								</tr>
								<tr>
									<td align="left" style="width:15px"></td>
									<td align="left" style="width:10px">
									<div id="EstimatorService1">
									<s:select cssClass="list-menu" name="serviceOrder.estimator"
										list="%{sale}" id="estimator" cssStyle="width:140px" headerKey=""
										headerValue="" onchange="changeStatus();" tabindex="" />
									</div>
										</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>   
									<td align="left">                                                 
									<div id="Service1">                                               
								    <configByCorp:customDropDown 	listType="map" list="${service_isactive}" fieldValue="${serviceOrder.serviceType}" attribute="id='serviceOrderForm_serviceOrder_serviceType' class=list-menu name='serviceOrder.serviceType'  style='width:251px'  headerKey='' headerValue='' onchange='changeStatus();' "/>		    	                       
								    </div>
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left">
									 <div id="modeFild">
									 <c:if test="${not empty serviceOrder.id}">
									<%-- <s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}"
										cssStyle="width:145px" onchange="validateDistanceUnit();changeStatus();getCarrier();fillUsaFlag();"
										tabindex="" /> --%>
										<configByCorp:customDropDown 	listType="map" list="${mode_isactive}" fieldValue="${serviceOrder.mode}" 
										attribute="id='serviceOrderForm_serviceOrder_mode' class=list-menu name='serviceOrder.mode'  
										style='width:251px'  headerKey='' headerValue='' onchange='validateDistanceUnit();changeStatus();getCarrier();fillUsaFlag();' "/>
										</c:if>
										<c:if test="${empty serviceOrder.id}">
										<configByCorp:customDropDown 	listType="map" list="${mode_isactive}" fieldValue="${serviceOrder.mode}" 
										attribute="id='serviceOrderForm_serviceOrder_mode' class=list-menu name='serviceOrder.mode'  
										style='width:251px'  headerKey='' headerValue='' onchange='populateInclusionExclusionData('default','1','F'),validateDistanceUnit(),changeStatus(),getCarrier(),fillUsaFlag(),getIncExcDocDetails();' "/>
										<%-- <s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}"
										cssStyle="width:145px" onchange="populateInclusionExclusionData('default','1','F'),validateDistanceUnit(),changeStatus(),getCarrier(),fillUsaFlag(),getIncExcDocDetails();"
										tabindex="" /> --%>
										
		    					         </c:if>
		    							 </div>
									</td>
									<td align="right" class="listwhitetext" style="width:20px"></td>
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">										
         							<td align="left" colspan="2">
         							<configByCorp:customDropDown 	listType="map" list="${flagCarrierList_isactive}" fieldValue="${serviceOrder.serviceOrderFlagCarrier}"   attribute="id='carrierFild' class=list-menu name='serviceOrder.serviceOrderFlagCarrier'  style='width:80px'  headerKey='' headerValue='' onchange='changeStatus();' tabindex='' "/></td>
         							</configByCorp:fieldVisibility>	
								</tr>
                                <tr><td height="2px"></td></tr>
							<tr>
							<td width="85px"></td>
							<td align="left" colspan="15" width="800">
							<table id="Service2" style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
							<tr>							
							<td class="listwhitetext" align="left">Relocation Services</td>
							</tr>
							<tr>							
							<td valign="top">
							<fieldset style="margin:0px 0 0 0; padding:2px 0 0 2px;">                      
                        									<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">
                        									<tr>
                          										<c:forEach var="entry" items="${reloService}" varStatus="rowCounter">
                          										<c:if test="${entry.value!='' && ((fn1:indexOf(entry.value,'Active')>=0 ) || (((fn1:indexOf(entry.value,'Inactive')>=0 ) && (fn1:indexOf(serviceOrder.serviceType,entry.key)>=0 ))))}">    
                          										<td class="listwhitetext" style="width:650px"><input type="checkbox" style="vertical-align:middle;" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex=""/>${fn1:substring(entry.value, 0, fn1:indexOf(entry.value,'~'))} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 4 == 0 && rowCounter.count != 0 }">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
    														   </tr>             										
                          									</table> 
                          	</fieldset>
							</td>
							</tr>
							</table>
                          	</td>
						</tr>
						<tr><td height="4px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom" ><div id="hidStatusSalesLabel"><fmt:message
										key='serviceOrder.salesMan' /> <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.salesManMandatory"><font color="red" size="2">*</font></configByCorp:fieldVisibility></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="hidStatusCommoditLabel"><fmt:message key='serviceOrder.commodity' /><font color="red" size="2">*</font></div>
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><div id="packLabel"> <fmt:message
          key='serviceOrder.packingMode' /></div></td>
          								<td align="right" class="listwhitetext" style="width:20px"></td>
										<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
									<td align="left" class="listwhitetext" valign="bottom" colspan="4">Packing Service</td>
									</configByCorp:fieldVisibility>
								</tr>
								<tr>
									<td align="left" style="width:25px"></td>
									<td ><div id="hidStatusSalesText"><s:select cssClass="list-menu"
										name="serviceOrder.salesMan" id="salesMan" list="%{estimatorList}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="changeStatus();checkSalesMan();" tabindex="" /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<c:choose>
										<c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='STL' ||serviceOrder.job =='STF' ||serviceOrder.job =='TPS'}">
											<td align="left"><div id="hidStatusCommoditText">
										<c:if test="${not empty serviceOrder.id}">
											<configByCorp:customDropDown 	listType="map" list="${commodits_isactive}" fieldValue="${serviceOrder.commodity}"      attribute="id='serviceOrderForm_serviceOrder_commodity' class=list-menu name='serviceOrder.commodity'  style='width:251px' headerKey='' headerValue='' onchange='changeStatus();' tabindex='' "/>
										</c:if>
										<c:if test="${empty serviceOrder.id}">
											<configByCorp:customDropDown 	listType="map" list="${commodits_isactive}" fieldValue="${serviceOrder.commodity}"      attribute="id='serviceOrderForm_serviceOrder_commodity' class=list-menu name='serviceOrder.commodity'  style='width:251px' headerKey='' headerValue='' onchange='populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();' tabindex='' "/>
										</c:if>
												</div></td>
										</c:when>
										<c:otherwise>
											<td align="left"><div id="hidStatusCommoditText">
											<c:if test="${not empty serviceOrder.id}">
											<configByCorp:customDropDown 	listType="map" list="${commodit_isactive}" fieldValue="${serviceOrder.commodity}"      attribute="id='serviceOrderForm_serviceOrder_commodity' class=list-menu name='serviceOrder.commodity'  style='width:251px' headerKey='' headerValue='' onchange='validAutoBoat();changeStatus();' tabindex='' "/>
											</c:if>
										<c:if test="${empty serviceOrder.id}">
										<configByCorp:customDropDown 	listType="map" list="${commodit_isactive}" fieldValue="${serviceOrder.commodity}"      attribute="id='serviceOrderForm_serviceOrder_commodity' class=list-menu name='serviceOrder.commodity'  style='width:251px' headerKey='' headerValue='' onchange='validAutoBoat();populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();' tabindex='' "/>
										</c:if>		
										</div></td>
										</c:otherwise>
									</c:choose>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left">
									<div id="packFild">
									<c:if test="${not empty serviceOrder.id}">
									
									<configByCorp:customDropDown 	listType="map" list="${pkmode_isactive}" fieldValue="${serviceOrder.packingMode}" 
										attribute="id='serviceOrderForm_serviceOrder_packingMode' class=list-menu name='serviceOrder.packingMode'  
										style='width:251px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
									<%-- <s:select cssClass="list-menu" name="serviceOrder.packingMode"
										list="%{pkmode}" cssStyle="width:145px"
										onchange="changeStatus();" tabindex="" /> --%>
										</c:if>
										<c:if test="${empty serviceOrder.id}">
										<configByCorp:customDropDown 	listType="map" list="${pkmode_isactive}" fieldValue="${serviceOrder.packingMode}" 
										attribute="id='serviceOrderForm_serviceOrder_packingMode' class=list-menu name='serviceOrder.packingMode'  
										style='width:251px'  headerKey='' headerValue='' onchange='populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();' "/>
										<%-- <s:select cssClass="list-menu" name="serviceOrder.packingMode"
										list="%{pkmode}" cssStyle="width:145px"
										onchange="populateInclusionExclusionData('default','1','F');changeStatus();getIncExcDocDetails();" tabindex="" /> --%>
										</c:if>
										</div>
								    </td>	
								    <td align="right" class="listwhitetext" style="width:20px"></td>							 	
								 	<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
								 	<td align="left" colspan="4"> 
									<configByCorp:customDropDown 	listType="map" list="${packingServiceList_isactive}" fieldValue="${serviceOrder.packingService}" 
										attribute="id='serviceOrderForm_serviceOrder_packingService' class=list-menu name='serviceOrder.packingService'  
										style='width:251px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
									<%-- <s:select cssClass="list-menu" name="serviceOrder.packingService"
										list="%{packingServiceList}" cssStyle="width:145px"
										onchange="changeStatus();" tabindex="" /> --%>
										</td>
										</configByCorp:fieldVisibility>
										
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><fmt:message
										key='serviceOrder.coordinator' />
										<c:choose>
										<c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote')}">
										</c:when>
										<c:otherwise>
										<font color="red" size="2">*</font>
										</c:otherwise>
										</c:choose>
										</td>
										<td></td>
							<c:if test="${serviceOrder.grpStatus=='Draft' || serviceOrder.grpStatus=='Finalized'}">
							<td align="left" class="listwhitetext" valign="bottom">Groupage&nbsp;Status</td>
                    		<td></td>
							<td align="left" class="listwhitetext" valign="bottom">Groupage&nbsp;Order</td>
                    		
				            </c:if> 
				            
							<c:if test="${serviceOrder.grpStatus!='Draft' && serviceOrder.grpStatus!='Finalized'}">				            
				            	 <s:hidden name="serviceOrder.grpStatus"/>        
				            </c:if>

								<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.opsPerson">
									<td align="left" class="listwhitetext" valign="bottom">Ops Person
									</td>										
									<td align="right" class="listwhitetext" style="width:5px"></td>
									</configByCorp:fieldVisibility>
									<td align="left" class="listwhitetext" style="">
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
									<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
									<tr>	
									<td align="left" class="listwhitetext" width="5"><fmt:message key='miscellaneous.millitaryShipment'/></td>
									<td  width="46"></td>
									<td align="left" class="listwhitetext" width="5">Shipment&nbsp;Type</td>	
									</tr>
									</table>
									</configByCorp:fieldVisibility>									
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									
									<td align="left" class="listwhitetext" width="" id="mmCounselorLabel">MM&nbsp;Counselor</td>
									<td></td>
									<td></td>
									<c:set var="thirdPartyServiceRequired" value="false"/>
	  		<c:if test="${serviceOrder.thirdPartyServiceRequired}">
	  			<c:set var="thirdPartyServiceRequired" value="true"/>
	  		</c:if>
								</tr>
								<tr>
									<td align="right" style="width:25px"></td>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.coordinator" id="coordinator" list="%{coordinatorList}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="changeStatus();" tabindex="" /></td>	
										<td></td>
													<c:if test="${serviceOrder.grpStatus=='Draft' || serviceOrder.grpStatus=='Finalized'}">
							
                    		<td ><s:textfield name="serviceOrder.grpStatus" cssClass="input-textUpper" cssStyle="width:60px" readonly="true" tabindex=""/></td>
							<td></td>
                    		<td ><s:textfield name="groupageMasterOrder" cssClass="input-textUpper" cssStyle="width:105px" readonly="true" tabindex=""/></td>
				            </c:if> 
							<c:if test="${serviceOrder.grpStatus!='Draft' && serviceOrder.grpStatus!='Finalized'}">				            
				            	 <s:hidden name="serviceOrder.grpStatus"/>        
				            </c:if>
										
										<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.opsPerson">
										<td><s:select cssClass="list-menu"
										name="serviceOrder.opsPerson" id="opsPerson" list="%{opsPerson}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="changeStatus();" tabindex="" /></td>										
									<td align="right" class="listwhitetext" style="width:5px"></td>
									</configByCorp:fieldVisibility>	
									<td align="left" class="listwhitetext" style="">
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
									<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
									<tr>		                            
	                                <td width="70px">
	                                <s:select cssClass="list-menu" name="miscellaneous.millitaryShipment" list="%{yesno}" cssStyle="width:113px" onchange="changeStatus();" tabindex="" />
	                                </td>
	                                <td width="19px"></td>
	                                <td width="70px">  
	                                <configByCorp:customDropDown 	listType="map" list="${shipmentType_isactive}" fieldValue="${serviceOrder.shipmentType}" 
										attribute="id='serviceOrderForm_serviceOrder_shipmentType' class=list-menu name='serviceOrder.shipmentType'  
										style='width:120px'  headerKey='' headerValue='' onchange='changeStatus();' "/>
	                                <%-- <s:select cssClass="list-menu" name="serviceOrder.shipmentType" list="%{shipmentType}" headerKey="" headerValue="" cssStyle="width:140px" onchange="changeStatus();" tabindex="" /> --%>
	                                </td>									
									</tr>
									</table>
									</configByCorp:fieldVisibility>									
									</td>
									<td align="right" class="listwhitetext" style="width:25px"></td>								
									
									
									<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
				                    <td class="listwhitetext">
				                    <s:checkbox name="serviceOrder.thirdPartyServiceRequired" value="${thirdPartyServiceRequired}" fieldValue="true" onclick="changeStatus();" cssStyle="width:12px;margin:0px;vertical-align:top;" tabindex=""/>&nbsp;3rd&nbsp;Party&nbsp;Service&nbsp;Required
				                    </td>
				                    </configByCorp:fieldVisibility>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.mmCounselor" id="mmCounselor" list="%{coordinatorList}"
										cssStyle="width:140px" headerKey="" headerValue=""
										onchange="changeStatus();" tabindex=""/></td>								
									<td align="left"></td>
									<td align="left" class="listwhitetext" >&nbsp;&nbsp;Groupage&nbsp;<s:checkbox id="serviceOrder.grpPref" name="serviceOrder.grpPref" tabindex="" cssStyle="vertical-align: middle;margin:0px;" onchange="" /></td> 
									
									<td width="88"></td>
								<c:if test="${empty serviceOrder.id}">
										<td align="right"><img
											src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
											HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
									</c:if>
									<c:if test="${not empty serviceOrder.id}">
										<c:choose>
											<c:when
												test="${countServiceOrderNotes == '0' || countServiceOrderNotes == '' || countServiceOrderNotes == null}">
												<td align="right"><img id="countServiceOrderNotesImage"
													src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
													HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:when>
											<c:otherwise>
												<td align="right"><img id="countServiceOrderNotesImage"
													src="${pageContext.request.contextPath}/images/notes_open1.jpg"
													HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:otherwise>
										</c:choose>
									</c:if>
								</tr>
								<tr><td height="5px"></td></tr>
							</tbody>
						</table>
						   <c:if test="${serviceOrder.corpID=='STVF'}">
						   <table class="detailTabLabel" border="0" width="100%">
								<tbody>
							 		<tr><td align="left" class="vertlinedata"></td></tr>
								</tbody>
							</table>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tbody>	
						<tr><td height="5px"></td></tr>					
						<tr>
							<td></td>
							<td align="left" class="listwhitetext" style="width:1px"></td>
							 <c:if test="${serviceOrder.job !='RLO'}">
							 <td align="left" class="listwhitetext" colspan="3"><fmt:message key='customerFile.survey'/></td>
							<td align="left"></td>							
							<td align="left" class="listwhitetext" style="width:10px" colspan="8"></td>
							<td align="left" class="listwhitetext" >Add to Cal.</td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:3px">Time&nbsp;Zone</td>
							<td align="left" style="width:10px"></td>
							 <c:if test="${serviceOrder.job !='RLO'}">
							<td align="left" class="listwhitetext" colspan="2">Actual&nbsp;Survey&nbsp;Date</td>
							</c:if>
							
						</tr>
						<tr>
						    <td align="right" class="listwhitetext" style="width:90px">Pre&nbsp;Move&nbsp;Survey:&nbsp;</td>
							<td align="left" style="width:1px"></td>
							<c:if test="${serviceOrder.job !='RLO'}">							
							<c:if test="${not empty serviceOrder.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.survey"/></s:text>
							<td width="71px"><s:textfield  cssClass="input-text" id="survey" name="serviceOrder.survey" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td>
							<td width="40px"><img  id="survey_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.survey}">
							<td width="71px"><s:textfield  cssClass="input-text" id="survey" name="serviceOrder.survey" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
							<td width="40px"><img id="survey_trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:34px"></td>
							 <td colspan="9" width="">
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
							<tr>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td align="left" style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="serviceOrder.surveyTime" id="surveyTime" cssStyle="width:35px;" maxlength="5"  onchange = "changeStatus();completeTimeString();" /> </td>
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td align="left" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="serviceOrder.surveyTime2" id="surveyTime2" cssStyle="width:35px;" maxlength="5" onchange = "changeStatus();completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td>
							</tr>
							</table>
							</td>
							<td class="listwhitetext" width="60" align="center">
							<img src="${pageContext.request.contextPath}/images/event_calendar.png"  align="top" onclick="calendarICSFunction();" ></td>						
							</c:if>
							<c:if test="${serviceOrder.job =='RLO'}">							
							<s:hidden cssClass="input-text" name="serviceOrder.surveyTime" id="surveyTime" />							
							<s:hidden cssClass="input-text" name="serviceOrder.surveyTime2" id="surveyTime2"/>
							</c:if>
							<td><s:textfield name="timeZone" value="${sessionTimeZone}" readonly="true" cssStyle="width:103px;" cssClass="input-textUpper" /></td>
							<c:if test="${serviceOrder.job !='RLO'}">
							<td align="left" class="listwhitetext" style="width:10px"></td>
							<c:if test="${not empty serviceOrder.actualSurveyDate}">
							<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.actualSurveyDate"/></s:text>
							<td width="70"><s:textfield cssClass="input-text" id="actualSurveyDate" name="serviceOrder.actualSurveyDate" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="actualSurveyDate_trigger" onclick="surveyDateChng();" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.actualSurveyDate}">
							<td width="70"><s:textfield cssClass="input-text" id="actualSurveyDate" name="serviceOrder.actualSurveyDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td>
							<td><img id="actualSurveyDate_trigger" style="vertical-align:bottom" onclick="surveyDateChng();" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if> 
							</c:if>
							<td align="left" style="width:10px"></td>
                            </tr>
                            </tbody>
				            </table>
					        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0" style="padding-top:5px;">
					        <tbody>
					        <tr>
							<td></td>
							<td align="left" class="listwhitetext" colspan="2" >Code</td>
							 <td align="left" class="listwhitetext" colspan="2">Name</td>
							
							<td align="left" class="listwhitetext" colspan="4">Contact</td>
							
							<td align="left" class="listwhitetext" style="width:3px"colspan="1">Email</td>
							
							<td align="left" class="listwhitetext" >Contact Number</td>
						     </tr>
							<tr>
							<td align="right" class="listwhitetext" style="width:90px">Origin Agent:&nbsp;</td>
							 <td><s:textfield cssClass="input-text" key="" name="serviceOrder.originAgentCode" id="originAgentCode" cssStyle="width:106px;" maxlength="10" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findOriginAgentName();changeStatus();"  onblur="showAddressImage();"/>
							</td>
							<td align="left" width="28px"><img id="openpopup.img" style="vertical-align:top;" class="openpopup" width="15" height="20" onclick="javascript:winOpenOrigin();document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield cssClass="input-text" name="serviceOrder.originAgentName" id="originAgentName" cssStyle="width:187px;" maxlength="250" onkeyup="findPartnerDetails('originAgentName','originAgentCode','originAgentNameDiv',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findPartnerDetailsByName('originAgentCode','originAgentName')"/>
							<div id="originAgentNameDiv" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td >
								<div id="hidTSOriginAgent" style="vertical-align:middle;width:21px;"><a><img  style="width:25px;height:19px;" class="openpopup" id="noteServOriginAgentCode" onclick="getPartnerAlert(document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value,'noteServOriginAgentCode');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
							</td>
							 <td align="left" >	
					        <s:textfield id="origincontact" cssClass="input-text" key="serviceOrder.originAgentContact" onchange="reseEmails('OA',this.value);" cssStyle="width:180px;" tabindex="" />
				            </td>
				            <td>
				            </td>
					        <td><div id="hidImagePlus" style="vertical-align:middle;float:left; "><img class="openpopup" width="17" height="20" onclick="getContactInfo('OA',document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value);" src="<c:url value='/images/plus-small.png'/>" /></div>
					        <td align="left"></td>
					        <td style="padding-right:5px;"><s:textfield cssClass="input-text" id="originAgentEmail" name="serviceOrder.originAgentEmail" cssStyle="width:150px;" maxlength="65" onchange="checkEmail('serviceOrder.originAgentEmail');"  tabindex="" /></td>
							 <td>
					<s:textfield  cssClass="input-text" key="serviceOrder.originAgentPhoneNumber" maxlength="30" cssStyle="width:160px;"  onchange="reseEmails('OA',this.value);"  tabindex="" />
					</td>
						</tr>
					</table>
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:5px;" >
				<tbody>
				<tr><td align="left" style="height:5px;"></td></tr>
						<tr>
						 	<td align="left" class="listwhitetext" colspan="20" style="">
						 	  <c:if test="${serviceOrder.job !='RLO'}"> 
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
							<tr>
							<td align="left" style="width:91px">&nbsp;</td>
							<td align="left" class="listwhitetext" style="width:90px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.priceSubmissionToAccDate',this);return false" >To&nbsp;Account</a></td>
							<td></td>
							<td align="left" ></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" width="90"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.priceSubmissionToTranfDate',this);return false" >To&nbsp;Transferee</a></td>
							<td></td>
							<td align="left"></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" colspan="3" width="90"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.priceSubmissionToBookerDate',this);return false" >To&nbsp;Booker</a></td>
							<td></td>
							<td align="left"></td>
							<td align="left" style="width:0px"></td>
							<td align="left" class="listwhitetext" colspan="3" width="81"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteAcceptenceDate',this);return false" >Quote&nbsp;Acceptance</a></td>
							<c:if test="${serviceOrder.corpID =='STVF'}">
							<c:choose>
                         <c:when test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
							<td align="left" style="width:10px"></td>
							<td align="left" class="listwhitetext" width="81">&nbsp;&nbsp;Quote&nbsp;Stop</td>
                         </c:when>
                         <c:otherwise>
                         <td align="left" style="width:0px"></td>
                         </c:otherwise>
                         </c:choose>
                         </c:if>
                         </tr>
                         </table>
							</c:if>
							<td align="left"></td>
						</tr>
				<tr>
				 <td align="left" class="listwhitetext" colspan="10">
						 	  <c:if test="${serviceOrder.job !='RLO'}"> 
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
							<tr>
							<td align="right" class="listwhitetext" style="width:92px"> Price&nbsp;Submission:&nbsp;</td>
							
							<c:if test="${not empty serviceOrder.priceSubmissionToAccDate }">
							
							<s:text id="serviceOrderSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.priceSubmissionToAccDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="serviceOrder.priceSubmissionToAccDate" value="%{serviceOrderSubmissionToAccFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true"  onkeydown="" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.priceSubmissionToAccDate}">
							<td width="62px"><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="serviceOrder.priceSubmissionToAccDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>						
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							
							<c:if test="${not empty serviceOrder.priceSubmissionToTranfDate}">
						
						    <s:text id="serviceOrderSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.priceSubmissionToTranfDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="serviceOrder.priceSubmissionToTranfDate" value="%{serviceOrderSubmissionToTranfFormattedValue}" cssStyle="width:57px" maxlength="11"  readonly="true" onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
							
							<c:if test="${empty serviceOrder.priceSubmissionToTranfDate}">
							<td width="62px"><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="serviceOrder.priceSubmissionToTranfDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							<c:if test="${not empty serviceOrder.priceSubmissionToBookerDate}">
							<s:text id="serviceOrderpriceSubmissionToBookerFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.priceSubmissionToBookerDate"/></s:text>
							<td width="62px"><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="serviceOrder.priceSubmissionToBookerDate" value="%{serviceOrderpriceSubmissionToBookerFormattedValue}" cssStyle="width:58px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.priceSubmissionToBookerDate}">
							<td width="62px"><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="serviceOrder.priceSubmissionToBookerDate" cssStyle="width:58px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left" ></td>
							<td align="left" style="width:8px"></td>
							<c:if test="${not empty serviceOrder.quoteAcceptenceDate}">
							<s:text id="serviceOrderAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.quoteAcceptenceDate"/></s:text>
							<td width="62px"><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="serviceOrder.quoteAcceptenceDate" value="%{serviceOrderAcceptenceFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.quoteAcceptenceDate}">
							<td width="62px"><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="serviceOrder.quoteAcceptenceDate" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${serviceOrder.corpID =='STVF'}">
							<c:choose>
                         <c:when test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
                            <td align="left" ></td>
							<td align="left" style="width:20px"></td>
							<c:if test="${not empty serviceOrder.quoteStopEmail}">
							<s:text id="serviceOrderquoteStopEmailFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.quoteStopEmail"/></s:text>
							<td width="62px"><s:textfield id="quoteStopEmail" cssClass="input-text" name="serviceOrder.quoteStopEmail" value="%{serviceOrderquoteStopEmailFormattedValue}" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)"  onfocus="changeStatus();"/></td><td><img id="quoteStopEmail_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty serviceOrder.quoteStopEmail}">
							<td width="62px"><s:textfield id="quoteStopEmail" cssClass="input-text" name="serviceOrder.quoteStopEmail" cssStyle="width:57px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="quoteStopEmail_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td style="width:10px"></td>
                         </c:when>
                         <c:otherwise>
                         <s:hidden name="serviceOrder.quoteStopEmail" />
                        </c:otherwise>
                        </c:choose>
						</c:if>	
							<td style="width:10px"></td>
							<td><input type="button" value="Schedule Survey" style="!width:120px;width:112px;font-size:11px;" class="cssbuttonB" onclick="scheduleSurvey();" /></td>
										
							<c:if test="${serviceOrder.surveyEmailLanguage==null || serviceOrder.surveyEmailLanguage==''}">
							<c:set var="ischecked" value="true"/>
							</c:if>
							<c:if test="${serviceOrder.surveyEmailLanguage!=null && serviceOrder.surveyEmailLanguage!=''}">
							<c:set var="ischecked" value="false"/>
							<c:set var="splittedString" value="${fn:split(serviceOrder.surveyEmailLanguage, ',')}" />
								<c:forEach var="lang" items="${splittedString}">
									<c:if test="${fn:indexOf(lang,'ENGLISH')>=0}" >
										<c:set var="ischecked" value="true"/>
									</c:if>
									
								</c:forEach>
							</c:if>					
							<td align="left" style="width:31px"></td>
							<c:choose>
								<c:when test="${usertype=='ACCOUNT' || usertype=='AGENT'}">	
								</c:when>
								<c:otherwise>
									<td align="left" class="listwhitetext" style="width:10px">English</td>
									<td align="left"><s:checkbox name="checkEnglish" value="${ischecked}" fieldValue="true" onclick="changeStatus()" /></td>
								</c:otherwise>
							</c:choose>
							
								<td style="width:10px"></td>
								<td><input type="button" name="Button1" value="Send Survey Email" style="width:108px;font-size:11px;" class="cssbuttonB" onclick="sendSurveyEmail();" /></td>
							
				            <td align="right" colspan="20" style="padding-left:20px;">
				<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
				<tr>
						<c:if test="${empty serviceOrder.id}">
							<td align="right" colspan="20" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SOSurvey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SOSurvey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SOSurvey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SOSurvey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>			
						</tr>
					 </table>
				     </td>
			        </tr>
			        </table>
			        </c:if>
			        </td>
			        </tr>
				    </tbody>
					</table>
				    </c:if>
					</tr>
					</td>
    <jsp:include flush="true" page="serviceOrderFormSec.jsp"></jsp:include>					
<c:if test="${not empty serviceOrder.id}">
<c:if test="${pricingButton=='yes'}"> 
<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}&redirectPricingButton=yes"/>
</c:if></c:if>
</s:form> 
<script type="text/javascript">
<sec-auth:authComponent componentId="module.script.form.partnerPortalScript">
navigationVal();
		 trap();
		 $('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');		
		$('#serviceOrder.general').removeAttr('disabled');
		<sec-auth:authScript tableList="serviceOrder,miscellaneous" formNameList="serviceOrderForm,serviceOrderForm" transIdList='${serviceOrder.shipNumber}'>
	    </sec-auth:authScript>
		trap1();

</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.agentScript">
navigationVal();
		 trap();
		 $('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');		
		$('#serviceOrder.general').removeAttr('disabled');
		<sec-auth:authScript tableList="serviceOrder,miscellaneous" formNameList="serviceOrderForm,serviceOrderForm" transIdList='${serviceOrder.shipNumber}'>
	    </sec-auth:authScript>
		trap1();

</sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
navigationVal();
 trap();
$('#serviceOrderForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
$('#serviceOrderForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');
trap1();
<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
document.forms['serviceOrderForm'].elements['serviceOrder.socialSecurityNumber'].readOnly=false;
document.forms['serviceOrderForm'].elements['serviceOrder.socialSecurityNumber'].className = 'input-text';
</configByCorp:fieldVisibility>

</sec-auth:authComponent>

try{
	 var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	 if(job=='RLO'){		
		checkColumn();
	 }
	 <c:if test="${empty serviceOrder.id}">
  	     fillCommodity();
	 </c:if>
	 if(document.forms['serviceOrderForm'].elements['customerFileJob'].value != ''){
	 	document.forms['serviceOrderForm'].elements['tempJob'].value  =  document.forms['serviceOrderForm'].elements['customerFileJob'].value
	 }else{
		 document.forms['serviceOrderForm'].elements['customerFileJob'].value  =  document.forms['serviceOrderForm'].elements['tempJob'].value
	}
}
catch(e){}
</script> 
<script type="text/javascript">
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
		animatedcollapse.addDiv('resources', 'fade=0,persist=0,hide=1')
	</configByCorp:fieldVisibility>
</c:if> 
	animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weight', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('volume', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=1,hide=1')
	animatedcollapse.addDiv('customerServiceSurvey', 'fade=0,persist=1,hide=1')
	
animatedcollapse.addDiv('address', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('customer', 'fade=0,persist=1,hide=1') 
animatedcollapse.addDiv('customerSurveyDiv', 'fade=0,persist=1,hide=1')
<c:if test="${serviceOrder.job=='INT'}">
animatedcollapse.addDiv('postMoveSurvey', 'fade=0,persist=1,hide=1')
</c:if>
<c:if test="${quotesToValidate=='QTG'}">
	animatedcollapse.addDiv('estpricing', 'fade=0,persist=1,hide=1')
</c:if>
<c:if test="${redirectPricingButton=='yes'}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
	</c:if>
</c:if>
<c:if test="${redirectPricingButton!='yes'}"> 
<c:if test="${not empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,hide=1')
	</c:if>
</c:if>
<c:if test="${empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,hide=1')
	</c:if>
</c:if>
</c:if>
animatedcollapse.init();

<c:if test="${usertype!='PARTNER' && usertype!='AGENT'}">
try{
accountIdCheck();
} 
catch(e){}
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:if test="${hitFlag=='1'}">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	 	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:if>

	//setTimeout("getJobList('onload')", 2000);
	try{
			var fieldName = document.forms['serviceOrderForm'].elements['field'].value;
			var fieldName1 = document.forms['serviceOrderForm'].elements['field1'].value;
			var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
			if(fieldName!=''){
			document.forms['serviceOrderForm'].elements[fieldName].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customerSurveyDiv', 'fade=0,persist=0,show=1')
			<c:if test="${serviceOrder.job=='INT'}">
			animatedcollapse.addDiv('postMoveSurvey', 'fade=0,persist=0,show=1')
			</c:if>
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')
			}
			animatedcollapse.init();
			}
			if(fieldName1!=''){
			document.forms['serviceOrderForm'].elements[fieldName1].className = 'rules-textUpper';
			animatedcollapse.addDiv('weightnvolume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weight', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('volume', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customer', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('customerSurveyDiv', 'fade=0,persist=0,show=1')
			<c:if test="${serviceOrder.job=='INT'}">
			animatedcollapse.addDiv('postMoveSurvey', 'fade=0,persist=0,show=1')
			</c:if>
			animatedcollapse.addDiv('weightnvolumesub1', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('weightnvolumesub2', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){ 
			animatedcollapse.addDiv('incexc', 'fade=0,persist=0,hide=1')	
			}
			animatedcollapse.init()
			}
			
	}catch(e){}
</c:if>	
//alert("wat");
try{
    var job = document.getElementsByName('serviceOrder.job')[0].value;
    var hidStatusEstimator1 = document.getElementById('EstimatorService1');
    var labellabelEstimator1 = document.getElementById('labelEstimator');
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var labelserviceVar = document.getElementById('labelservice');
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel');
    var weightserviceVar = document.getElementById('weightVolSection');
    var hidStatusCommoditText1=document.getElementById('hidStatusCommoditText');
    var hidStatusCommoditLabel1=document.getElementById('hidStatusCommoditLabel');
    var hidStatusSalesText11=document.getElementById('hidStatusSalesText');
    var hidStatusSalesLabel11=document.getElementById('hidStatusSalesLabel');
    var hidStatusSalesLabeTextl11=document.getElementById('hidStatusSalesText');
    var registrationHide1=document.getElementById('registrationHide');
    var registrationHide11=document.getElementById('registrationHideLabel');
    var carrierFildLabel1=document.getElementById('carrierFildLabel');
    var carrierFild1=document.getElementById('carrierFild');
   	if(job!=''){
   if(job=='RLO'){
	   hidStatusReg5.style.display = 'block';
	   hidStatusReg4.style.display = 'none';
	   hidStatusEstimator1.style.display = 'none';
	   labellabelEstimator1.style.display= 'none';
	   labelserviceVar.style.display = 'none';
	   modehidStatusReg.style.display = 'none';
	   modelabelserviceVar.style.display = 'none';
	   routinghidStatusReg.style.display = 'none';
	   routinglabelserviceVar.style.display = 'none';
	   packhidStatusReg.style.display = 'none';
	   packlabelserviceVar.style.display = 'none';
	   weightserviceVar.style.display = 'none';
	   hidStatusCommoditText1.style.display = 'none';
	   hidStatusCommoditLabel1.style.display = 'none';
	   hidStatusSalesText11.style.display = 'none';
	   <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.city">
	   hidStatusSalesLabel11.style.display = 'block';
	   hidStatusSalesLabeTextl11.style.display = 'block';
	   </configByCorp:fieldVisibility>
	   <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.notshowcity">
	   hidStatusSalesLabel11.style.display = 'none';
	   </configByCorp:fieldVisibility>
	   registrationHide1.style.display = 'block';
	   registrationHide11.style.display = 'block';
	   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
		   carrierFildLabel1.style.display = 'none';
	     carrierFild1.style.display = 'none';
	  </configByCorp:fieldVisibility>
	   <c:if test="${not empty serviceOrder.id}">
	   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=true;
	   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=true;	   
	   </c:if>
	      }else{
	   	   hidStatusReg4.style.display = 'block';
	   	   hidStatusEstimator1.style.display = 'block';
	   	   labellabelEstimator1.style.display= 'block';
		   hidStatusReg5.style.display = 'none';
		   labelserviceVar.style.display = 'block';
		   modehidStatusReg.style.display = 'block';
		   modelabelserviceVar.style.display = 'block';
		   routinghidStatusReg.style.display = 'block';
		   routinglabelserviceVar.style.display = 'block';
		   packhidStatusReg.style.display = 'block';
		   packlabelserviceVar.style.display = 'block';	
		   weightserviceVar.style.display = 'block';
		   hidStatusCommoditText1.style.display = 'block';
		   hidStatusCommoditLabel1.style.display = 'block';	  
		   hidStatusSalesText11.style.display = 'block';
		   hidStatusSalesLabel11.style.display = 'block'; 
		   registrationHide1.style.display = 'block'; 
		   registrationHide11.style.display = 'block';
		   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
			   carrierFildLabel1.style.display = 'block';
           carrierFild1.style.display = 'block';
           </configByCorp:fieldVisibility>
		   <c:if test="${not empty serviceOrder.id}">
		   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=false;
		   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=false;
		   </c:if>		   
    }
    } 
}catch(e){//alert(e);
}
</script>	
<script type="text/javascript"> 
	setOnSelectBasedMethods(["changeStatus(),hitBilling()"]);
	setCalendarFunctionality(); 
	function setVipImageReset(){
		if('${serviceOrder.vip}'.toString() == 'true' || '${customerFile.vip}'.toString() == 'true')
			document.getElementById('serviceOrder.vip').checked = true;	
		else
			document.getElementById('serviceOrder.vip').checked = false;	
		setVipImage();
	}
	function setVipImage(){ 
		var imgElement = document.getElementById('vipImage');
		if(document.getElementById('serviceOrder.vip').checked == true){
			imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
			imgElement.style.display = 'block';
		}
		else{
			if(('${serviceOrder.vip}' == 'true' && document.getElementById('serviceOrder.vip').checked == true) || ('${customerFile.vip}' == 'true' && document.getElementById('serviceOrder.vip').checked == true)){
				imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
				imgElement.style.display = 'block';
			}
			else
				imgElement.src = '';
				imgElement.style.display = 'none';
		}
	}
	setVipImage();
</script>
<script type="text/javascript">
try{
	showMmCounselorByJob();
	} 
	catch(e){}
setTimeout("setOriginCityCode()",500);
function setOriginCityCode(){
try{
	document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value ="${serviceOrder.originCityCode}";
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value ="${serviceOrder.destinationCityCode}";
}catch(e){}
}
function setFieldValue(id,result){
	document.forms['serviceOrderForm'].elements[id].value = result;
	//document.getElementById(id).value = result;
}
function appendFieldValue(id,result){
	var temp=document.forms['serviceOrderForm'].elements[id].value;
	if(temp!=''){
		if(temp.indexOf(result.split("~")[0])>-1){
			temp=removeFromList(temp,result.split("~")[0]);
			if(temp!=''){
				temp=temp+","+result;
			}else{
				temp=result;
			}
		}else{
			temp=temp+","+result;
		}
	}else{
		temp=result;
	}
	document.forms['serviceOrderForm'].elements[id].value = temp;
}
function removeFromList(temp,invNum){
	var arr=temp.split(",");
	var filterRES='';
	for(var i=0;i<arr.length;i++){
		if(arr[i].trim().indexOf(invNum)<0){
			if(filterRES==''){
				filterRES=arr[i];
			}else{
				filterRES=filterRES+","+arr[i];
			}
		}
	}
	return filterRES;
}
</script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}
</script>
<script type="text/javascript">
try{  	
		document.getElementById('tabindexFlag').focus();
	}catch(e){}
</script>
<script language="javascript" type="text/javascript">
	 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
	 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	 var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
  if((modeType=='Air'|| modeType=='Sea')&& jobType!='RLO'){
   document.getElementById('carrierFild').style.display="block";
   document.getElementById('carrierFildLabel').style.display="block";
  }else{
   document.getElementById('carrierFild').style.display="none";
   document.getElementById('carrierFildLabel').style.display="none";
  }
  </configByCorp:fieldVisibility>
	try{
  		<c:if test="${ usertype=='USER'}">
  		window.onload =function ModifyPlaceHolder () {		 
  	        var input = document.getElementById ("soBookingAgentNameId");
  		     input.placeholder = "Booking Agent Search";   
  	    }
  		</c:if>
  	}catch(e){}
  	<c:if test="${serviceOrder.corpID=='STVF'}">
	try{
		var tem1="";
		var tem2="";
		 tem1=document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
		 tem2=document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
		
		if((tem1=='' || tem1==':') && document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime']!=undefined)
		{
			document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value='00:00';
		}
		if((tem2==''|| tem2==':') && document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2']!=undefined )
		{
			document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value='00:00';	
		}
		
	}catch(e){}
   </c:if>
</script>
<script language="javascript" type="text/javascript">
var commodityForUser = '${commodityForUser}' ;
var soCommodity = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value ;
var soId = '${serviceOrder.id}';
 if( commodityForUser != '' && (soId == null || soId == ''))
		{
	 if(soCommodity == null || soCommodity == '')
		 	{
	 document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value = commodityForUser ;
			}
	 	}
 
 
try{
	// # 10461 - Default Service in Configuration Default Start
	chekServiceType();
 

}
catch(e){}


function chekServiceType() {
	 
	var serviceforUser='${serviceTypeForUser}';
	
	 <c:if test="${empty serviceOrder.id}">
	 if( serviceforUser != '')
		{
		 var job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		 
		 if(job!='RLO'){
			 
		 	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value = serviceforUser ;
		 }
			
		}
	 </c:if>
	
	}
	
try{
	var checkFieldVisibility='${checkFieldVisibilityForPricing}';
	if(checkFieldVisibility=='Y'){
		loadSoPricing();
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,show=1')
		document.forms['serviceOrderForm'].elements['formStatus'].value='1';
	}
	}catch(e){}
	
	try{
		var msgClicked='No';
		<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		if('${serviceOrder.job}' =='INT'){
		var checkMsgClickedValue = "<%=request.getParameter("msgClicked") %>";
		var checkMsgClickedDBValue = "${msgClicked}";
		if((checkMsgClickedValue=='No' || checkMsgClickedValue==null || checkMsgClickedValue=='null' || checkMsgClickedValue=='') || (checkMsgClickedDBValue=='No' || checkMsgClickedDBValue==null || checkMsgClickedDBValue=='null' || checkMsgClickedDBValue=='')){
			var hasValuation = '${billing.insuranceHas}';
			if(hasValuation==null || hasValuation==''){
				var soCreatedOn = '${serviceOrder.createdOn}';
				var todayDate = new Date();
				if(soCreatedOn!=null && soCreatedOn!=''){
				   var mySplitResult = soCreatedOn.split("-");
				   var day = mySplitResult[2];
				   day = day.substring(0,2)
				   var month = mySplitResult[1];
				   var year = mySplitResult[0];
				   if(month == 'Jan'){month = "01";
				   }else if(month == 'Feb'){month = "02";
				   }else if(month == 'Mar'){month = "03";
				   }else if(month == 'Apr'){month = "04";
				   }else if(month == 'May'){month = "05";
				   }else if(month == 'Jun'){month = "06";
				   }else if(month == 'Jul'){month = "07";
				   }else if(month == 'Aug'){month = "08";
				   }else if(month == 'Sep'){month = "09";
				   }else if(month == 'Oct'){month = "10";
				   }else if(month == 'Nov'){month = "11";
				   }else if(month == 'Dec'){month = "12";
				   }
				   var finalDate = month+"-"+day+"-"+year;
				   date1 = finalDate.split("-");
				   var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
				   var daysApart = Math.round((todayDate-sDate)/86400000);
				   if(daysApart > 7){
			  			alert('Enter insurance requirements');
			  			msgClicked='Yes';
				   }
				}
			}
			var certificateNo = '${billing.certnum}';
			if((hasValuation=='Y') && (certificateNo==null || certificateNo=='')){
				var tsTargetPackDate = '${trackingStatus.beginPacking}';
				var tsTargetLoadDate = '${trackingStatus.beginLoad}';
				var soCreatedOn = '${serviceOrder.createdOn}';
				var soDate = null;
				var tsPackDate = null;
				var tsLoadDate = null;
				
				if(tsTargetPackDate!=null && tsTargetPackDate!=''){
					   var mySplitResult = tsTargetPackDate.split("-");
					   var day = mySplitResult[2];
					   day = day.substring(0,2)
					   var month = mySplitResult[1];
					   var year = mySplitResult[0];
					   if(month == 'Jan'){month = "01";
					   }else if(month == 'Feb'){month = "02";
					   }else if(month == 'Mar'){month = "03";
					   }else if(month == 'Apr'){month = "04";
					   }else if(month == 'May'){month = "05";
					   }else if(month == 'Jun'){month = "06";
					   }else if(month == 'Jul'){month = "07";
					   }else if(month == 'Aug'){month = "08";
					   }else if(month == 'Sep'){month = "09";
					   }else if(month == 'Oct'){month = "10";
					   }else if(month == 'Nov'){month = "11";
					   }else if(month == 'Dec'){month = "12";
					   }
					   var finalDate = month+"-"+day+"-"+year;
					   var date1 = finalDate.split("-");
					   tsPackDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					   
				}else if(tsTargetLoadDate!=null && tsTargetLoadDate!=''){
					   var mySplitResult = tsTargetLoadDate.split("-");
					   var day = mySplitResult[2];
					   day = day.substring(0,2)
					   var month = mySplitResult[1];
					   var year = mySplitResult[0];
					   if(month == 'Jan'){month = "01";
					   }else if(month == 'Feb'){month = "02";
					   }else if(month == 'Mar'){month = "03";
					   }else if(month == 'Apr'){month = "04";
					   }else if(month == 'May'){month = "05";
					   }else if(month == 'Jun'){month = "06";
					   }else if(month == 'Jul'){month = "07";
					   }else if(month == 'Aug'){month = "08";
					   }else if(month == 'Sep'){month = "09";
					   }else if(month == 'Oct'){month = "10";
					   }else if(month == 'Nov'){month = "11";
					   }else if(month == 'Dec'){month = "12";
					   }
					   var finalDate = month+"-"+day+"-"+year;
					   var date1 = finalDate.split("-");
					   tsLoadDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
				}
				if(soCreatedOn!=null && soCreatedOn!=''){
					   var mySplitResult = soCreatedOn.split("-");
					   var day = mySplitResult[2];
					   day = day.substring(0,2)
					   var month = mySplitResult[1];
					   var year = mySplitResult[0];
					   if(month == 'Jan'){month = "01";
					   }else if(month == 'Feb'){month = "02";
					   }else if(month == 'Mar'){month = "03";
					   }else if(month == 'Apr'){month = "04";
					   }else if(month == 'May'){month = "05";
					   }else if(month == 'Jun'){month = "06";
					   }else if(month == 'Jul'){month = "07";
					   }else if(month == 'Aug'){month = "08";
					   }else if(month == 'Sep'){month = "09";
					   }else if(month == 'Oct'){month = "10";
					   }else if(month == 'Nov'){month = "11";
					   }else if(month == 'Dec'){month = "12";
					   }
					   var finalDate = month+"-"+day+"-"+year;
					   var date1 = finalDate.split("-");
					   soDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
				}
				if((tsPackDate!=null && tsPackDate!='') && (soDate!=null && soDate!='')){
					var daysApart = Math.round((tsPackDate-soDate)/86400000);
					if(daysApart < 7){
			  			alert('Need cert#');
			  			msgClicked='Yes';
				   	}
				}else if((tsLoadDate!=null && tsLoadDate!='') && (soDate!=null && soDate!='')){
					var daysApart = Math.round((tsLoadDate-soDate)/86400000);
					if(daysApart < 7){
			  			alert('Need cert#');
			  			msgClicked='Yes';
				   	}
				}
			}
			checkMsgClickedValue = msgClicked;
			document.forms['serviceOrderForm'].elements['msgClicked'].value = checkMsgClickedValue;
		}
	}
	</configByCorp:fieldVisibility>
	}catch(e){}
	
</script> 

<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
