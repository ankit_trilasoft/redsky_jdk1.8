// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `storageEmailType` VARCHAR(25) NULL  AFTER `email` ;
ALTER TABLE `redsky`.`billing` ADD COLUMN `storageEmailType` VARCHAR(25) NULL  AFTER `paymentReceived` ;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','STORAGEEMAILTYPE',25,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('TSFT', 'per recipient', 'One mail per recipient', 25, 'STORAGEEMAILTYPE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', 'multiple recipients', 'Multiple mails per recipient', 25, 'STORAGEEMAILTYPE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('CustomerFile','initialContactDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`contract` 
ADD COLUMN `storageBillingGroup` VARCHAR(50) NULL  AFTER `accountName` ,
ADD COLUMN `storageEmail` VARCHAR(60) NULL  AFTER `storageBillingGroup` ,
ADD COLUMN `emailPrintOption` VARCHAR(45) NULL  AFTER `storageEmail` ,
ADD COLUMN `creditTerms` VARCHAR(10) NULL  AFTER `emailPrintOption` ,
ADD COLUMN `customerFeedback` VARCHAR(20) NULL  AFTER `creditTerms` ,
ADD COLUMN `noTransfereeEvaluation` BIT(1) NULL DEFAULT b'0'  AFTER `customerFeedback` ,
ADD COLUMN `oneRequestPerCF` BIT(1) NULL DEFAULT b'0'  AFTER `noTransfereeEvaluation` ,
ADD COLUMN `ratingScale` VARCHAR(25) NULL  AFTER `oneRequestPerCF` ,
ADD COLUMN `lumpSum` BIT(1) NULL  AFTER `ratingScale` ,
ADD COLUMN `detailedList` BIT(1) NULL DEFAULT b'0'  AFTER `lumpSum` ,
ADD COLUMN `noInsurance` BIT(1) NULL DEFAULT b'0'  AFTER `detailedList` ,
ADD COLUMN `lumpSumAmount` DECIMAL(19,2) NULL DEFAULT '0.00'  AFTER `noInsurance` ,
ADD COLUMN `lumpPerUnit` VARCHAR(15) NULL  AFTER `lumpSumAmount` ,
ADD COLUMN `minReplacementvalue` VARCHAR(25) NULL  AFTER `lumpPerUnit` ,
ADD COLUMN `networkPartnerCode` VARCHAR(8) NULL  AFTER `minReplacementvalue` ,
ADD COLUMN `networkPartnerName` VARCHAR(255) NULL  AFTER `networkPartnerCode` ,
ADD COLUMN `source` VARCHAR(40) NULL  AFTER `networkPartnerName` ,
ADD COLUMN `vendorCode` VARCHAR(8) NULL  AFTER `source` ,
ADD COLUMN `vendorName` VARCHAR(255) NULL  AFTER `vendorCode` ,
ADD COLUMN `insuranceOption` VARCHAR(150) NULL  AFTER `vendorName` ,
ADD COLUMN `defaultVat` VARCHAR(30) NULL  AFTER `insuranceOption` ,
ADD COLUMN `billToAuthorization` VARCHAR(50) NULL  AFTER `defaultVat` ;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `billingMoment` VARCHAR(20) NULL  AFTER `storageEmailType` ;
ALTER TABLE `redsky`.`contract` ADD COLUMN `billingMoment` VARCHAR(20) NULL ;
ALTER TABLE `redsky`.`contract` ADD COLUMN `creditTermsCmmAgent` VARCHAR(10) NULL  AFTER `billingMoment` ;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','BILLINGMOMENT',20,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('TSFT', 'In Advance', 'In Advance', 20, 'BILLINGMOMENT','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', 'After Packing', 'After Packing', 20, 'BILLINGMOMENT','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', 'After Loading', 'After Loading', 20, 'BILLINGMOMENT','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', 'After Delivery', 'After Delivery', 20, 'BILLINGMOMENT','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active');

ALTER TABLE `redsky`.`claim` ADD COLUMN `goodWillPayment` DECIMAL(19,2) NULL  AFTER `totalPaidToCustomerAmount` ;

