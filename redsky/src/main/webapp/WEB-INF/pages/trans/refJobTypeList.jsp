<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="refJobTypeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='refJobTypeList.heading'/>"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
    display: block;
    font-size: 0.9em;
    margin-bottom: 1px;
    margin-top: -14px;        
    text-align: right;
    width:100%;
}
</style> 
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this job?");
	var did = targetElement;
	if (agree){
		location.href="deleteRefJob.html?id="+did;
	}else{
		return false;
	}
}

function clear_fields(){	
	document.forms['refJobTypeForm'].elements['cCompanydivision'].value = "";
	document.forms['refJobTypeForm'].elements['cJob'].value = "";
	document.forms['refJobTypeForm'].elements['cSurveytool'].value = "";
}
</script>
</head>
<s:form cssClass="form_magn"  id="refJobTypeForm" action="searchRefJobType" method="post" validate="true">
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="" key="button.search"/> 
	
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
   
</c:set>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="Layer1" style="width: 100%; margin-bottom:12px;">	
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
 <div class="center-content">
<table class="table" style="width:100%;">
<thead>
<tr>
<th>Job&nbsp;Type</th>
<th>Company Division</th>
<configByCorp:fieldVisibility componentId="component.standard.surveyTool">
<th>Survey Tool</th>
</configByCorp:fieldVisibility>
<th>&nbsp;</th>
</tr></thead>	
		<tbody>
			<tr>
			<td><s:select cssClass="list-menu" name="cJob" list="%{jobs}" cssStyle="width:150px" headerKey="" headerValue="" /></td>
			<td><s:select cssClass="list-menu" name="cCompanydivision" list="%{division}" cssStyle="width:150px" headerKey="" headerValue="" /></td>
			<configByCorp:fieldVisibility componentId="component.standard.surveyTool">
			<td><s:select cssClass="list-menu" name="cSurveytool" list="%{surveyToolMap}" cssStyle="width:150px" headerKey="" headerValue="" /></td>
			</configByCorp:fieldVisibility>
			<td width="230px" style="border-left:hidden;" align="right">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Ref Job Type List</span></a></li>
	  	<li><a href="refJobDocumentType1.html"><span>Ref Job Document Type List</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editRefJobType.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  
 	
				<s:set name="jobType" value="jobType" scope="request"/>  
				<display:table name="jobType" class="table" requestURI="" id="jobTypeList" export="true" defaultsort="1" pagesize="10" style="width:100%; !margin-top:5px; " >   
					<display:column property="job" sortable="true" style="width: 100px;" href="editRefJobType.html" paramId="id" paramProperty="id" title="Job" />
					<display:column property="companyDivision" sortable="true" style="width: 100px;" title="Com&nbsp;Division" />
					<display:column property="country" sortable="true" style="width: 250px;" title="Country" />
					<display:column property="coordinator" sortable="true" style="width: 150px;" title="Coord" />
					<display:column property="estimator" sortable="true" style="width: 150px;" title="Estimator" />
					<display:column property="personBilling" sortable="true" style="width: 130px;" title="Person Billing" />
					<display:column property="personPricing" sortable="true" style="width: 130px;" title="Person Pricing" />
					<display:column property="personPayable" sortable="true" style="width: 130px;" title="Person Payable" />
					<display:column property="personAuditor" sortable="true" style="width: 130px;" title="Person Auditor" />
					<display:column property="personClaims" sortable="true" style="width: 130px;" title="Person Claims" />
					<configByCorp:fieldVisibility componentId="component.standard.surveyTool">
					<display:column property="surveyTool"  style="width: 130px;" title="Survey Tool" />
					</configByCorp:fieldVisibility>
					<display:column title="Remove" style="width: 15px;">
						<a><img align="middle" title="" onclick="confirmSubmit('${jobTypeList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a>
					</display:column>	 
					
					
					<display:setProperty name="paging.banner.item_name" value="Ref Job Type"/>   
				    <display:setProperty name="paging.banner.items_name" value="Ref Job Type"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Ref Job Type List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Ref Job Type List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Ref Job Type List.pdf"/>   
				</display:table>  
			
<table> 
<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>
</s:form> 

