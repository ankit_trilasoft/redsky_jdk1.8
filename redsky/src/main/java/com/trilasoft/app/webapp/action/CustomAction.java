package com.trilasoft.app.webapp.action;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.Custom;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import org.appfuse.model.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CustomAction extends BaseAction  implements Preparable{
    private CustomManager customManager;
    private List customs;
    private List customsInList;
    private Custom custom;
    private Long id;
    private Long sid;
	private String sessionCorpID;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private RefMasterManager refMasterManager;
	private RefMaster refMaster;
	private String hitFlag;
	private String shipSize;
	private CustomerFileManager customerFileManager;
	private String minShip;
	private String countShip;
	private CustomerFile customerFile;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	private CompanyManager companyManager;
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private String countBondedGoods;
	private String county;
	private Map<String,String> customStatus;
	private List customsOutList;
	private String cid;
	private String cMove;
    public Integer countTransId;
    private List transactionIdList;
    private String returnAjaxStringValue;
	Date currentdate = new Date();
	
	static final Logger logger = Logger.getLogger(CustomAction.class);

	 

	 public CustomAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}

	 
	 public void prepare() throws Exception {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		 if(getRequest().getParameter("ajax") == null){  
			 getComboList(sessionCorpID);
		 }
		 customsDocType= refMasterManager.findByParameter(sessionCorpID, "CustomsDocType");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			 }
    public List getCustoms() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return customs;
    }
    @SkipValidation 
    public String list() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
    	serviceOrder = serviceOrderManager.get(id); 
    	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
    	customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
    	countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    @SkipValidation
	public String findDocType() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//customsDocType= refMasterManager.findByParameterCustomDoc(sessionCorpID, "CustomsDocType", county);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
    
    public void setId(Long id) {
        this.id = id;
    }

    public Custom getCustom() {
        return custom;
    }

    public void setCustom(Custom custom) {
        this.custom = custom;
    }
    @SkipValidation
    public String findCustomsIn(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	   customsInList= customManager.customsInList(sid,"In");
    	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }
    

    @SkipValidation
    public String findCustomsOut(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	countTransId=customManager.checkCustomsId(Long.parseLong(cid),cMove,sessionCorpID);            	      		
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }

    public String edit(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
        if (id != null) {
        	serviceOrder = serviceOrderManager.get(sid);
        	//customsInList= customManager.customsInList(sid,"In");
        	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
        	custom = customManager.get(id);
            custom.setCorpID(serviceOrder.getCorpID()); 
            //customsDocType= refMasterManager.findByParameterCustomDoc(serviceOrder.getCorpID(), "CustomsDocType", county);
       } else {
        	custom = new Custom();
        	serviceOrder = serviceOrderManager.get(sid);
        	//customsInList= customManager.customsInList(sid,"In");
        	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
            custom.setCorpID(sessionCorpID);
            custom.setUpdatedOn(new Date());
            custom.setCreatedOn(new Date());
            custom.setMovement("In");
            custom.setDocumentType("T-1");
            
            custom.setServiceOrderId(serviceOrder.getId());
            //customsDocType= refMasterManager.findByParameterCustomDoc(serviceOrder.getCorpID(), "CustomsDocType", county);
           }
        customsInList= customManager.customsInList(serviceOrder.getId(),"In");
        shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
        countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       	return SUCCESS;
    	}
 	 
    
    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
        boolean isNew = (custom.getId() == null); 
        custom.setServiceOrder(serviceOrder);
        //custom.setCorpID(sessionCorpID);
        custom.setUpdatedOn(new Date());
        if(isNew){
        	custom.setCreatedOn(new Date());
        }
        custom.setServiceOrderId(serviceOrder.getId());
        custom=customManager.save(custom); 
        try{
    		if(serviceOrder.getIsNetworkRecord()){
    			List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    			synchornizeCustom(serviceOrderRecords,custom,isNew ,serviceOrder);
    		}
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
        customsInList= customManager.customsInList(sid,"In");
        //customsDocType= refMasterManager.findByParameterCustomDoc(sessionCorpID, "CustomsDocType", county);
        String key = (isNew) ? "Customs has been added successfully" : "Customs has been updated successfully";   
        saveMessage(getText(key));
        //customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
        hitFlag="1";
       	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;      
      }
    
    private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
		if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		}else{
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return linkedShipNumberList;
	}
    
	private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
		Iterator it =linkedShipNumber.iterator();
		while(it.hasNext()){
			String shipNumber= it.next().toString();
			if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
    			ServiceOrder serviceOrderRemote=serviceOrderManager.get(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    			recordList.add(serviceOrderRemote);
    		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return recordList;
	}
	
	private void synchornizeCustom(List<Object> serviceOrderRecords,Custom custom, boolean isNew,ServiceOrder serviceOrderold) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){		
    			if(isNew){
    				Custom customNew= new Custom();
    				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(customNew, custom);
    				
    				customNew.setId(null);
    				customNew.setCorpID(serviceOrder.getCorpID());
    				customNew.setServiceOrderId(serviceOrder.getId());
    				customNew.setCreatedBy("Networking");
    				customNew.setUpdatedBy("Networking");
    				customNew.setCreatedOn(new Date());
    				customNew.setUpdatedOn(new Date());	
    				customNew.setServiceOrder(serviceOrder);
    				customNew.setNetworkId(custom.getId());
    				customNew = customManager.save(customNew);
    				
    				custom.setNetworkId(custom.getId());
    				customManager.save(custom);
    				
    			}else{
    				Custom customTo= customManager.getForOtherCorpid(customManager.findRemoteCustom(custom.getNetworkId(),serviceOrder.getId())); 
        			Long id=customTo.getId();
        			String createdBy=customTo.getCreatedBy();
        			Date createdOn=customTo.getCreatedOn();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(customTo, custom);
    				customTo.setId(id);
    				customTo.setCreatedBy(createdBy);
    				customTo.setCreatedOn(createdOn);
    				customTo.setCorpID(serviceOrder.getCorpID());
    				customTo.setServiceOrderId(serviceOrder.getId());
    				customTo.setUpdatedBy(custom.getCorpID()+":"+getRequest().getRemoteUser());
    				customTo.setUpdatedOn(new Date());	
    				customTo.setServiceOrder(serviceOrder); 
    				customManager.save(customTo);
    			}
    			}
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

    public String delete() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  try{
    		    custom=customManager.get(id);
    	        serviceOrder=custom.getServiceOrder();
    	        if(serviceOrder.getIsNetworkRecord()){
    	        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    				deleteCustom(serviceOrderRecords,custom.getNetworkId());
    	      }
    	      }catch(Exception ex){
    	    	  ex.printStackTrace();
    	      }
    	try {
			    customManager.remove(id);
			    saveMessage(getText("Custom has been deleted successfully."));
		} catch (Exception ex) {
			ex.printStackTrace();
			//customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
		}
		id=sid;
		list();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
    private void deleteCustom(List<Object> serviceOrderRecords, Long networkId) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		customManager.deleteNetworkCustom(serviceOrder.getId(),networkId);
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
    
    private String gotoPageString;
	private Map<String, String> customsMovement;
	private Map<String, String> customsDocType;
    public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }

    public String saveOnTabChange() throws Exception {
        String s = save(); 
        return gotoPageString;
    }
    public String getComboList(String corpId){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	customsMovement= refMasterManager.findByParameter(corpId, "CustomsMovement");
    	customStatus= refMasterManager.findByParameter(corpId, "CUSTOM_STATUS");
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }
    @SkipValidation
	public String getCustomDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
			if(!customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0)!=null){
			        countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), sessionCorpID).get(0).toString();
			}
		} catch (Exception e) {	
			e.printStackTrace();
	    	 return CANCEL;
		}
		return SUCCESS;
	}
    
    @SkipValidation
    public String getTransactionIdListAjax(){
    	custom=customManager.get(id);
    	transactionIdList=customManager.getTransactionIdCount(sessionCorpID,custom.getId());
        if(transactionIdList!=null && !transactionIdList.isEmpty()){
        	String tansSize = transactionIdList.get(0).toString();
        	if(!tansSize.equals("0")){
        		returnAjaxStringValue ="true";
	        }else{
	        	returnAjaxStringValue ="false";
	        }
        }
    	return SUCCESS;
    }
    
    @SkipValidation
    public String deleteCustomDetailsAjax() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	try {
    			custom=customManager.get(id);
    			customManager.remove(id);
    			serviceOrder=custom.getServiceOrder();
    			if(serviceOrder.getIsNetworkRecord()){
    	        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
    				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
    				deleteCustom(serviceOrderRecords,custom.getNetworkId());
    	      }
			    saveMessage(getText("Custom has been deleted successfully."));
		} catch (Exception ex) {
			ex.printStackTrace();
			//customs=customManager.findBySO(serviceOrder.getId(), sessionCorpID);
			 return CANCEL;
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
  	public void setCustomManager(
			CustomManager customManager) {
		this.customManager = customManager;
	}
	public Long getId() {
		return id;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setCustoms(List customs) {
		this.customs = customs;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public RefMaster getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefMaster refMaster) {
		this.refMaster = refMaster;
	}

	
	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Map<String, String> getCustomsMovement() {
		return customsMovement;
	}

	public void setCustomsMovement(Map<String, String> customsMovement) {
		this.customsMovement = customsMovement;
	}

	public Map<String, String> getCustomsDocType() {
	    return customsDocType;
	}

	public void setCustomsDocType(Map<String, String> customsDocType) {
		this.customsDocType = customsDocType;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public String getCountBondedGoods() {
		return countBondedGoods;
	}

	public void setCountBondedGoods(String countBondedGoods) {
		this.countBondedGoods = countBondedGoods;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}


	public List getCustomsInList() {
		return customsInList;
	}


	public void setCustomsInList(List customsInList) {
		this.customsInList = customsInList;
	}


	public Map<String, String> getCustomStatus() {
		return customStatus;
	}


	public void setCustomStatus(Map<String, String> customStatus) {
		this.customStatus = customStatus;
	}


	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}


	public CompanyManager getCompanyManager() {
		return companyManager;
	}


	public List getCustomsOutList() {
		return customsOutList;
	}


	public void setCustomsOutList(List customsOutList) {
		this.customsOutList = customsOutList;
	}

	public String getCid() {
		return cid;
	}


	public void setCid(String cid) {
		this.cid = cid;
	}


	public String getcMove() {
		return cMove;
	}


	public void setcMove(String cMove) {
		this.cMove = cMove;
	}


	public Integer getCountTransId() {
		return countTransId;
	}


	public void setCountTransId(Integer countTransId) {
		this.countTransId = countTransId;
	}


	public List getTransactionIdList() {
		return transactionIdList;
	}


	public void setTransactionIdList(List transactionIdList) {
		this.transactionIdList = transactionIdList;
	}


	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}


	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}	

}