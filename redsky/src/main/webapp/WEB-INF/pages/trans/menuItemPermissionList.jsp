<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>  
<head>   
    <title><fmt:message key="menuItemPermissionList.title"/></title>   
    <meta name="heading" content="<fmt:message key='menuItemPermissionList.heading'/>"/> 
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}
</style>  
</head>
<s:hidden name="menuItemPermission.id" value="%{menuItemPermission.id}"/>
<s:hidden name="user.id" value="%{user.id}"  /> 
<s:set name="menuItemPermissions" value="menuItemPermissions" scope="request"/>   
<display:table name="menuItemPermissions" class="table" requestURI="" id="menuItemPermissionList" export="true" pagesize="25">   
    <display:column property="menuItemId" sortable="true" titleKey="menuItemPermission.menuItemId" paramId="id" paramProperty="id"/>
    <display:column property="recipient" sortable="true" titleKey="menuItemPermission.recipient" />   
    <display:column property="createdOn" sortable="true" titleKey="menuItemPermission.createdOn" format="{0,date,dd-MMM-yyyy}"/>
    
</display:table>   
<script type="text/javascript">   
    highlightTableRows("menuItemPermissionList");       
</script>
