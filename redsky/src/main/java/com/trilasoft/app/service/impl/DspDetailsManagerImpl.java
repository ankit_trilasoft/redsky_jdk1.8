package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DspDetailsDao;
import com.trilasoft.app.model.DspDetails;
import com.trilasoft.app.service.DspDetailsManager;

public class DspDetailsManagerImpl  extends GenericManagerImpl<DspDetails, Long> implements DspDetailsManager{
	DspDetailsDao dspDetailsDao;
	public DspDetailsManagerImpl(DspDetailsDao dspDetailsDao) {
		super(dspDetailsDao); 
        this.dspDetailsDao = dspDetailsDao; 
		// TODO Auto-generated constructor stub
	}
	public List getListById(Long id){
		return dspDetailsDao.getListById(id);
	}
	 public List checkById(Long id){
		 return dspDetailsDao.checkById(id);
	 }
	 public String getListRecord(String job,String corpId){
		 return dspDetailsDao.getListRecord(job, corpId);
	 }
	 public void updateServiceOrderStatus(Long id,String shipNumber,String serviceOrderStatus,String corpId,String userName,int StatusNumber ){
		 dspDetailsDao.updateServiceOrderStatus( id, shipNumber, serviceOrderStatus, corpId,userName,StatusNumber);
	 }
	 public void updateServiceCompleteDate(Long id,String shipNumber,String latestDate,String corpId ){
		 dspDetailsDao.updateServiceCompleteDate( id, shipNumber, latestDate, corpId);
	 }
	 public String  getRloVenderCode(Long id,String corpId){
		 return dspDetailsDao.getRloVenderCode( id,corpId);
	 }
	 public List findMaximumShip(String seqNum, String job,String CorpID){
		 return dspDetailsDao.findMaximumShip( seqNum, job, CorpID);
		 
	 }
	 public List findMinimumShip(String seqNum, String job,String CorpID){
		 return dspDetailsDao.findMinimumShip( seqNum, job, CorpID);
		 
	 }
	 public String checkNetworkPartnerServiceType(String sessionCorpID,String networkVendorCode,String preFixCode,String moveTypeCheck){
		 return dspDetailsDao.checkNetworkPartnerServiceType(sessionCorpID, networkVendorCode, preFixCode,moveTypeCheck);
	 }
	 public String checkMoveTypeAgent(String sessionCorpID,String networkVendorCode){
		 return dspDetailsDao.checkMoveTypeAgent(sessionCorpID, networkVendorCode);
	 }
	 public List findAllServicesForRelo(String corpId){
		 return dspDetailsDao.findAllServicesForRelo(corpId);
	 }
/*	 public List findCountShip(String seqNum, String job,String CorpID){
		 return dspDetailsDao.findCountShip( seqNum, job, CorpID);
		 
	 }*/
	 public String getCustomerSurveyDetails(String ShipNumber,Long soId,String corpId){
		 return dspDetailsDao.getCustomerSurveyDetails(ShipNumber, soId, corpId);
	 }
	 public List getFeebackDetails(String shipnumber,String DSserviceOrderId,String serviceName,String sessionCorpID){
		 return dspDetailsDao.getFeebackDetails(shipnumber,DSserviceOrderId,serviceName, sessionCorpID);
	 }
	 public void resendEmailUpdateAjax(String updateQuery,String sessionCorpID,Long soId){
		 dspDetailsDao.resendEmailUpdateAjax(updateQuery, sessionCorpID, soId);
	 }
	 
	 public void saveTenancyMgmtUtilities(String sessionCorpID,Long serviceOrderId, String fieldName, boolean fieldValue,String updatedBy){
		 dspDetailsDao.saveTenancyMgmtUtilities(sessionCorpID, serviceOrderId, fieldName, fieldValue, updatedBy);
	 }
	 public List findUtilityServiceDetail(String sessionCorpID,Long serviceOrderId,String fieldName){
		 return dspDetailsDao.findUtilityServiceDetail(sessionCorpID,serviceOrderId,fieldName);
	 }
	 public void updateUtilityService(String updateQuery){
		 dspDetailsDao.updateUtilityService(updateQuery); 
	 }
	 public String findUtilitiesCheckBoxValues(String sessionCorpID,Long serviceOrderId){
		 return dspDetailsDao.findUtilitiesCheckBoxValues(sessionCorpID,serviceOrderId); 
	 }
	 public List tenancyBillingPreviewList(Date billThroughFrom, Date billThroughTo,String billToCode, String sessionCorpID, String tenancyBillingGrpList){
		 return dspDetailsDao.tenancyBillingPreviewList(billThroughFrom, billThroughTo,billToCode, sessionCorpID, tenancyBillingGrpList);
	 }
	 public void updateBillThroughDate(String tableName,String fieldName,String billThroughDate, Long id, String userName){
		 dspDetailsDao.updateBillThroughDate(tableName, fieldName, billThroughDate, id, userName);
	 }
}
