<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head>
<title><fmt:message key="workTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='workTicketList.heading'/>" />
<style type="text/css">
		h2 {background-color: #FBBFFF}
		div.exportlinks {
		margin:-5px 0px 10px 10px;
		padding:2px 4px 2px 0px;
		!padding:2px 4px 18px 0px;
		width:100%;
		}
		
.key_workticket {   
    !margin-top: -22px;    
}
.table th.alignCenter a {
    text-align: center;
}
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
	</style>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>


<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
   </script>
<SCRIPT LANGUAGE="JavaScript">
function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}
function findAccountingHold()
	{
	var sid = document.forms['searchForm'].elements['serviceOrder.id'].value;
	var url="findAccountingHold.html?ajax=1&decorator=simple&popup=true&sid="+ encodeURI(sid);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponse22;
     http22.send(null); 
     }
     
  function handleHttpResponse22()
     { 
             if (http22.readyState == 4)
             {
                var results = http22.responseText 
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results > 0)
                { 
                    alert('The Customer has an accounting hold so no new work tickets can be created,\nPlease contact Accounting Department to process this customer.');
                	return false;
                	
                }else
                {	
                	findBillCompleteDate();
                }
    } 
    }
  
var http5 = getHTTPObject();
var http2 = getHTTPObject();    
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http22 = getHTTPObject2();    
function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function copyDate1Date2()
{
	document.forms['searchForm'].elements['date2'].value = document.forms['searchForm'].elements['date1'].value;
}
function focusDate(target)
{
	document.forms['searchForm'].elements[target].focus();
}

  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['searchForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['searchForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['searchForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['searchForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'customerWorkTickets.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['searchForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['searchForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "customerWorkTickets.html?id="+id;
	}
function findContractPackers(ticket,position){
	var url="crewNameByContractPackers.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket);
	ajax_showTooltip(url,position);	
	 
	 var divscroll = document.getElementById("OverFlowPr").scrollLeft;
	 var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;	  
	 document.getElementById("ajax_tooltipObj").style.left = myposition+'px';
	 

}
function findWorkTicketForms(ticket,jobNumber,position){
	var url="reportBySubModuleWorkTicket.html?ajax=1&decorator=simple&popup=true&ticket=" + encodeURI(ticket)+"&id="+ticket+"&jobNumber="+jobNumber+"&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&reportModule=workTicket&reportSubModule=workTicket";
	ajax_showTooltip(url,position);	
	
}

function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer){
	window.open('viewFormParam.html?id='+id+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID='+invNum+'&custID=${custID}&reportModule=workTicket&reportSubModule==workTicket&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
	//&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&
	//window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
}
//created this method as per Bug 6535 
function findToolTipService(code,position){
	var code1=code+"";
	var url="findToolTipService.html?ajax=1&decorator=simple&popup=true&code="+code1;
	ajax_showTooltip(url,position);		
}
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
function generatePrintPackage(ticket){
	var printFlag = "workTicketList";
	 var url="generatePrintPackage.html?ticket="+encodeURI(ticket)+"&printFlag="+printFlag+"";
	 showOrHide(1);
	 location.href=url;
	 setTimeout("showOrHide(0)",2500);
}
function validatefields(id,invNum,jobNumber,reportName,docsxfer,jobType,val){
	var url='viewReportWithParam.html?formFrom=list&id='+id+'&noteID='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=workTicket&reportSubModule=workTicket&reportName='+reportName+'&docsxfer='+docsxfer+'';
	location.href=url;	  
}
function winClose(id,invNum,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&invoiceNumber="+invNum+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
}

</SCRIPT>

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}

div.error, span.error, li.error, div.message {
width:450px;
}
</style>

</head>


<s:form id="searchForm" action="customerWorkTickets" method="post">
<div id="Layer1" style="width:100%">
<c:set var="shipnum" value="${serviceOrder.shipNumber}"/>
<c:set var="jobType" value="${serviceOrder.job}"/>
<s:hidden name="jobType" value="${jobType}"/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:hidden name="shipnum" value="${shipnum}"/>
<s:hidden name="serviceOrder.id" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
 	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="closedCompanyDivision" value="N" />
<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
	<c:set var="closedCompanyDivision" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" /> 
<c:if test="${not empty serviceOrder.id}">
<div id="newmnav" style="float:left;!margin-bottom:7px;">
		  
		  
		    <ul>

	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	               <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
		    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="redskyDashboard.html?sid=${serviceOrder.id}"><span>Dashboard</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="redskyDashboard.html?sid=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Dashboard</span></a></li>
				</c:if>
					</c:if>
				</sec-auth:authComponent>
				</configByCorp:fieldVisibility>
			
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>S/O Details</span></a></li>
				</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.billingTab">
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editBilling.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Billing</span></a></li>
				</c:if>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">			  
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="accountLineList.html?sid=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Accounting</span></a></li>
					</c:if>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		           <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  			<c:if test="${checkPropertyAmountComponent!='Y'}">
			   	 	<li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="operationResource.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>O&I</span></a></li>
				</c:if>
	         </sec-auth:authComponent>
	         </c:if>
		      <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
 <%--  <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)>=0 && serviceOrder.corpID=='CWMS')}">  --%>
		      <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1  && serviceOrder.corpID=='CWMS')}"> 
			  <c:if test="${forwardingTabVal!='Y'}">
			  		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  </c:if>
			  <c:if test="${forwardingTabVal=='Y'}">
	  				<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 	<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="containersAjaxList.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Forwarding</span></a></li>
					</c:if>
	  			</c:if>
	  			</c:if>
	  			</sec-auth:authComponent>
	  		<sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">	
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  		<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
					</c:if>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                   <c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
					</c:if>
					<c:if test="${checkPropertyAmountComponent=='Y'}">
						<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
					</c:if>
               </c:if>
               </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			   <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			   <c:if test="${serviceOrder.job !='RLO'}">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Status</span></a></li>
				</c:if>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  </sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	<c:if test="${checkPropertyAmountComponent!='Y'}">
				   	 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					<li><a href="claims.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Claims</span></a></li>
				</c:if>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  					<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
				</sec-auth:authComponent>
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  <c:if test="${usertype=='USER'}">
				  <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  				<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  			  </configByCorp:fieldVisibility>
  			  </c:if>
			</ul>
		</div>			
		<table cellpadding="0" cellspacing="0" border="0" style="margin:0px; padding:0px;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left" valign="top">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>				
		<div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/workTicketHeader.jsp"%> 
	
</c:if>


		<div style=" width:100%;" class="OverFlowPr" id="OverFlowPr">
	<div id="otabs" style="width:600px;!padding-bottom:22px;">
		  <ul>
		    <li><a class="current"><span>Work Ticket List</span></a></li>
		  </ul>
		  <div class="key_workticket">&nbsp;</div>
		</div>
		
		<div class="spnblk">&nbsp;</div>
	
	<s:set name="workTickets" value="workTickets" scope="request" />
	<display:table name="workTickets" class="table" requestURI="" id="workTicketList" export="true" defaultsort="14"  defaultorder="descending"  pagesize="10"  style="width:100%;margin-top:-18px;!margin-top:0px;">
		<display:column style="width:5px" titleKey="workTicket.targetActual"><c:if test="${workTicketList.targetActual == 'T'}"><img id="target" src="${pageContext.request.contextPath}/images/target.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'A'}"><img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'C'}"><img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'D'}"><img id="target" src="${pageContext.request.contextPath}/images/dispatching.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'F'}"><img id="active" src="${pageContext.request.contextPath}/images/forecasting.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if><c:if test="${workTicketList.targetActual == 'P'}"><img id="active" src="${pageContext.request.contextPath}/images/P.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP /></c:if></display:column>
		<display:column sortable="true" style="text-align:right;width:5px;" title="Pt"><img align="top" title="Forms" onclick="findWorkTicketForms('${workTicketList.ticket}','${workTicketList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></display:column>
		<display:column property="ticket" sortable="true" titleKey="workTicket.ticket"  url="/editWorkTicketUpdate.html" paramId="id" paramProperty="id"/>
		<display:column property="shipNumber" sortable="true" titleKey="workTicket.shipNumber"  />
		<display:column property="registrationNumber" sortable="true" style="width:10px" titleKey="workTicket.registrationNumber" />
		<display:column property="lastName" sortable="true" titleKey="workTicket.lastName" style="width:10px" maxLength="12"/>
		<display:column property="firstName" sortable="true" titleKey="workTicket.firstName" />
		<display:column property="jobType" sortable="true" titleKey="workTicket.jobType" />
		<display:column property="jobMode" sortable="true" titleKey="workTicket.jobMode" />
		<display:column property="description" sortable="true" title="Description" />
		<display:column property="city" sortable="true" titleKey="workTicket.city" />
		<display:column property="destinationCity" sortable="true" titleKey="workTicket.destinationCity" />
		<display:column sortable="true" titleKey="workTicket.service" >
		<div align="left" onmouseover="findToolTipService('${workTicketList.service}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${workTicketList.service}" /></div>
		</display:column>
		<display:column property="date1" sortable="true" titleKey="workTicket.date1" style="width:100px" format="{0,date,dd-MMM-yy}"/>
		<display:column property="date2" sortable="true" titleKey="workTicket.date2" style="width:100px" format="{0,date,dd-MMM-yy}"/>
		<c:choose>
		<c:when test="${workTicketList.crews == '0'}">
		<display:column sortable="true" style="width:50px;text-align:right;" titleKey="workTicket.warehouse"><c:out value="${workTicketList.warehouse}"></c:out>
		</display:column>
		</c:when>		
		<c:otherwise>
		<display:column sortable="true" style="width:50px;text-align:right;" titleKey="workTicket.warehouse"><c:out value="${workTicketList.warehouse}"></c:out>
		<img id="target" onclick="findContractPackers('${workTicketList.ticket}',this);" src="${pageContext.request.contextPath}/images/user-v1.png" HEIGHT=14 WIDTH=14 ALIGN=TOP /></display:column>
		</c:otherwise>
		</c:choose>
		<configByCorp:fieldVisibility componentId="component.button.workTicket.printPackage">
		<display:column sortable="true" style="text-align:right;width:5px;"
			title="Pkg">
			<img align="top" title="Forms"
				onclick="generatePrintPackage('${workTicketList.id}');"
				src="${pageContext.request.contextPath}/images/printer-icon-16.png" />
		</display:column>
		</configByCorp:fieldVisibility>	
		<display:column sortable="true" title="Billed Status" headerClass="alignCenter" >
		<c:if test="${workTicketList.reviewStatus=='Billed' }">
			<div ALIGN="center">
				<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		<c:if test="${workTicketList.reviewStatus=='UnBilled' }">
			<div ALIGN="center">
				<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		</display:column>		
		<display:column property="createdBy" sortable="true" title="Created&nbsp;By"  />
	
		<display:setProperty name="paging.banner.item_name" value="workticket" />
		<display:setProperty name="paging.banner.items_name" value="workTickets" />
		
		<display:setProperty name="export.excel.filename" value="WorkTicket List.xls" />
		<display:setProperty name="export.csv.filename" value="WorkTicket List.csv" />
		<display:setProperty name="export.pdf.filename" value="WorkTicket List.pdf" />
	</display:table>
</div>


<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.registrationNumber" />
<c:if test="${not empty serviceOrder.shipNumber}">
<c:if test='${serviceOrder.status != "CNCL" }'>
				<c:if test='${billingCheckFlag == "Y" }'>
				<c:choose>					
					<c:when test="${(serviceOrder.coordinator != '' && serviceOrder.coordinator != NULL) && (billing.personBilling != '' && billing.personBilling != NULL)}">
						<input type="button" class="cssbutton" style="width:55px; height:25px" onclick = "return findBillCompleteDate();" value="<fmt:message key="button.add"/>" />
					</c:when>
					<c:otherwise>
					<input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="javascript:alert('Coordinator/ Billing Person missing for this Order. Please enter to create the Work Ticket.')" value="Add" />
					</c:otherwise>
					</c:choose>
					</c:if>
					<c:if test='${not empty workTickets && billingCheckFlag == "Y" && (jobType =="SPT" ||jobType =="LOG" ||jobType =="OFF")}'>
				<c:choose>					
					<c:when test="${(serviceOrder.coordinator != '' && serviceOrder.coordinator != NULL) && (billing.personBilling != '' && billing.personBilling != NULL)}">
						   <input type="button" class="cssbuttonA" style="width:100px; height:25px"	 
		                    onclick="return findBillCompleteDate1();" value="Add With Copy" />
					</c:when>
					<c:otherwise>
					   <input type="button" class="cssbuttonA" style="width:100px; height:25px"	 
	                        onclick="javascript:alert('Coordinator/ Billing Person missing for this Order. Please enter to create the Work Ticket.')" value="Add With Copy" />

					</c:otherwise>
					</c:choose>
					</c:if>
		<%-- <c:if test='${not empty workTickets && billingCheckFlag == "Y"}'>
	    <input type="button" class="cssbuttonA" style="width:100px; height:25px"	 
		 onclick="location.href='<c:url   value="/addWithCopyWorkTicketList.html?sid=${serviceOrder.id}"/>'" value="Add With Copy" />
		
	</c:if> --%>
	  
					<c:if test='${billingCheckFlag != "Y" }'>
        			<input type="button" class="cssbutton" style="width:55px; height:25px" onclick = "return findBillCompleteDate();" value="<fmt:message key="button.add"/>" />
    				</c:if>
				
				</c:if>
	
<c:if test='${serviceOrder.status == "CNCL"}'>
<input type="button" class="cssbuttonA" style="width:60px; height:28px" onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before add ticket.')" value="Add" />
 </c:if>    	        
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</c:if>
<c:if test="${empty serviceOrder.shipNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>

<table><tr>
	<td></td></tr>
	</table>
	</div>
		<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait<br>Print Package PDF is being populated!...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
</s:form>
<script type="text/javascript">
	 showOrHide(0);
</script>
<script type="text/javascript"> 
try{
document.forms['searchForm'].elements['workTicket.ticket'].focus(); 
    highlightTableRows("workTicketList");
     Form.focusFirstElement($("searchForm"));
}catch(e){}

function findBillCompleteDate(){
	var shipNumber = document.forms['searchForm'].elements['shipnum'].value;
	var url="billComplete.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
	 http2.open("GET", url, true);
	 http2.onreadystatechange = handleHttpResponse1;
	 http2.send(null); 
 }
 
function handleHttpResponse1(){
  var id = document.forms['searchForm'].elements['fileID'].value;

         if (http2.readyState == 4)
         {
            var results = http2.responseText
            results = results.trim(); 
            var res = results.split("#");               
            if(((res[0]=='A' && res[1]!='1') || (res[0]!='T' && res[1]!='1')) || res[2]!='1' || res[3]!='1')
            {
            	alert('No more tickets can be added as Billing is Complete.');
            	return false;
            }else{
            	<c:choose>
            	<c:when test="${closedCompanyDivision=='Y' && serviceOrder.status == 'CLSD'}">
        		findClosedCompanyDivision('${serviceOrder.companyDivision}');
				</c:when>
				<c:otherwise> 
        		location.href="editWorkTicket.html?id="+id;
        		return true;
        		</c:otherwise>
  		      </c:choose> 
            }
		}

}

function findBillCompleteDate1(){
	var shipNumber = document.forms['searchForm'].elements['shipnum'].value;
	var url="billComplete.html?ajax=1&decorator=simple&popup=true&shipnum="+ encodeURI(shipNumber);
	 http2.open("GET", url, true);
	 http2.onreadystatechange = handleHttpResponse111;
	 http2.send(null); 
}

function handleHttpResponse111(){	
var sid = document.forms['searchForm'].elements['serviceOrder.id'].value;
	if (http2.readyState == 4)
    {
       var results = http2.responseText
       results = results.trim(); 
       var res = results.split("#"); 
       if(((res[0]=='A' && res[1]!='1') || (res[0]!='T' && res[1]!='1')) || res[2]!='1' || res[3]!='1')
       {
       	alert('No more tickets can be added as Billing is Complete.');
       	return false;	       
       }else{
    	   <c:choose>
    	   <c:when test="${closedCompanyDivision=='Y' && serviceOrder.status == 'CLSD'}">
				findClosedCompanyDivision1('${serviceOrder.companyDivision}');
				</c:when>
			    <c:otherwise> 
				location.href="addWithCopyWorkTicketList.html?sid=${serviceOrder.id}";
     			return true;
     			</c:otherwise>
  		      </c:choose> 
        }
	}

}

function findClosedCompanyDivision(companyCode,bookingAgentCode,oldStatus,status){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  location.href="editWorkTicket.html?id="+id;
              			return true;
			      }else{
			    	  alert("You can not add Work Ticket, As Company Division is closed.");
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
function findClosedCompanyDivision1(companyCode){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  location.href="addWithCopyWorkTicketList.html?sid=${serviceOrder.id}";
		          		return true;
			      }else{
			    	  alert("You can not add Work Ticket, As Company Division is closed.");
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
</script>
