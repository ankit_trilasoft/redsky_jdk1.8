package com.trilasoft.app.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name = "userlogfile")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class Userlogfile  extends BaseObject{
	private Long id;
	private String corpID;
	private String userName;
	private String userType;
	private String IPaddress;
	private String DNS;
	private Date login;
	private Date logout;
	private String sessionId;
	private String ssoToken;
	private String userAgentDetails;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("userName", userName).append("userType",
				userType).append("IPaddress", IPaddress).append("DNS", DNS)
				.append("Login", login).append("Logout", logout).append(
				"sessionId", sessionId).append( "userAgentDetails", userAgentDetails).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Userlogfile))
			return false;
		Userlogfile castOther = (Userlogfile) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(userName, castOther.userName).append(
				userType, castOther.userType).append(IPaddress,
				castOther.IPaddress).append(DNS, castOther.DNS).append(login,
				castOther.login).append(logout, castOther.logout).append(
				sessionId, castOther.sessionId).append(
						userAgentDetails, castOther.userAgentDetails).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(userName)
				.append(userType).append(IPaddress).append(DNS).append(login)
				.append(logout).append(sessionId).append(userAgentDetails).toHashCode();
	}
	
	
	
	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 20)
	public String getDNS() {
		return DNS;
	}
	public void setDNS(String dns) {
		DNS = dns;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 15)
	public String getIPaddress() {
		return IPaddress;
	}
	public void setIPaddress(String paddress) {
		IPaddress = paddress;
	}
	@Column
	public Date getLogin() {
		return login;
	}
	public void setLogin(Date login) {
		this.login = login;
	}
	@Column
	public Date getLogout() {
		return logout;
	}
	public void setLogout(Date logout) {
		this.logout = logout;
	}
	@Column(length=50)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(length = 50)
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	@Column(length = 100)
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getSsoToken() {
		return ssoToken;
	}
	@Column(length = 50)
	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}
	
	@Column(length = 500)
	public String getUserAgentDetails() {
		return userAgentDetails;
	}
	public void setUserAgentDetails(String userAgentDetails) {
		this.userAgentDetails = userAgentDetails;
	}
	
}
