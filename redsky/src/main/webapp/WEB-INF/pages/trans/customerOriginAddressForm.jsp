<%@ include file="/common/taglibs.jsp"%>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>Address:</b></td>
	<td align="right"  style="width:88px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<c:if test="${not empty originAddress1}">
${originAddress1}
</c:if>
<c:if test="${not empty originAddress2}">
${originAddress2}
</c:if>
<c:if test="${not empty originAddress3}">
${originAddress3}
</c:if>
<c:if test="${not empty originCity}">
${originCity}
</c:if>
<c:if test="${not empty originState}">
${originState}
</c:if>
<c:if test="${not empty originZip}">
${originZip}
</c:if>
<c:if test="${not empty originCountry}">
${originCountry}
</c:if>

<c:if test="${not empty originFax}">
<br><b>Fax:</b>${originFax}
</c:if>
<c:if test="${not empty originHomePhone}">
<br><b>Phone:</b>${originHomePhone}
</c:if>
<c:if test="${not empty email}">
<br><b>E-mail:</b>${email}
</c:if>