<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="userListSecuritySet.title"/></title>   
    <meta name="heading" content="<fmt:message key='userListSecuritySet.heading'/>"/> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>User List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="findUserByRole" action="" method="post">  
<s:set name="findUserByRole" value="findUserByRole" scope="request"/> 
<display:table name="findUserByRole" class="table" requestURI="" id="findUserByRole" style="width:200px" defaultsort="1" pagesize="100" >   
 		 <display:column property="userName" headerClass="headerColorInv" title="User Name" style="width:40px" />
 		 
</display:table>  
</s:form>
		  	