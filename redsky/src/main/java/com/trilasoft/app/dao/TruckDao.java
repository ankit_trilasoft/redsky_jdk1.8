package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Truck;

public interface TruckDao  extends GenericDao<Truck, Long>{
	
	public List searchTruck(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus,String dotInspFlag);
	public Map<String, String> findDefaultStateList(String bucket2, String corpId);
	public List  findDriverList(String bucket2, String corpId);	
	public List  findOwnerOperatorList(String parentId,String corpId);
	public List  findTruckDriverList(String partnerCode,String truckNumber,String corpId);
	public List  findTruckDescriptionType(String truckType, String corpId);
	public List  findCarrierListInDomestic(String truckNumber, String corpId);
	public List findControlExpirationsListInDomestic(String parentId,Date deliveryLastDay,String corpId);
	public List findControlExpirationsListForDriver(String parentId,Date deliveryLastDay,String corpId);
	public List findTruckTrailer(String corpId);
	public List searchTruckTrailer(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo);
	public List  findTrailerListInDomestic(String truckNumber, String corpId);
	public List findAgentVanName(String truckVanLineCode,String corpId);
	public List  findVanListInDomestic(String truckNumber, String corpId);
	public List isExisted(String truckNo, String sessionCorpID);
	public List searchTruckDotInsp(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus);
	public List searchTruckDueSixty(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus,int myListFor);
}
