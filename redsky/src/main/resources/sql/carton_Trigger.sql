delimiter $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_carton` $$
CREATE trigger redsky.trigger_add_history_carton
BEFORE UPDATE on redsky.carton
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
DECLARE updatedon DATETIME;
  set NEW.updatedon=now();
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

IF (OLD.type <> NEW.type ) THEN
	    select CONCAT(fieldNameList,'carton.type~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.type,'~') into newValueList;
	END IF;
IF (OLD.container <> NEW.container ) THEN
	    select CONCAT(fieldNameList,'carton.container~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.container,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.container,'~') into newValueList;
	END IF;
IF (OLD.corpID <> NEW.corpID ) THEN
	    select CONCAT(fieldNameList,'carton.corpID~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
	END IF;

IF (OLD.cntnrNumber <> NEW.cntnrNumber ) THEN
	    select CONCAT(fieldNameList,'carton.cntnrNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.cntnrNumber,'~') into newValueList;
	END IF;
IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
	    select CONCAT(fieldNameList,'carton.sequenceNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
	END IF;
IF (OLD.ship <> NEW.ship ) THEN
	    select CONCAT(fieldNameList,'carton.ship~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ship,'~') into newValueList;
	END IF;
IF (OLD.shipNumber <> NEW.shipNumber ) THEN
	    select CONCAT(fieldNameList,'carton.shipNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
	END IF;
IF (OLD.idNumber <> NEW.idNumber ) THEN
	    select CONCAT(fieldNameList,'carton.idNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.idNumber,'~') into newValueList;
	END IF;
IF (OLD.unit1 <> NEW.unit1 ) THEN
	    select CONCAT(fieldNameList,'carton.unit1~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
	END IF;
IF (OLD.unit2 <> NEW.unit2 ) THEN
	    select CONCAT(fieldNameList,'carton.unit2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
	END IF;
IF (OLD.pieces <> NEW.pieces ) THEN
	    select CONCAT(fieldNameList,'carton.pieces~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.pieces,'~') into newValueList;
	END IF;
IF (OLD.totalLine <> NEW.totalLine ) THEN
	    select CONCAT(fieldNameList,'carton.totalLine~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.totalLine,'~') into newValueList;
	END IF;
IF (OLD.unit3 <> NEW.unit3 ) THEN
	    select CONCAT(fieldNameList,'carton.unit3~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit3,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit3,'~') into newValueList;
	END IF;
IF (OLD.cartonType <> NEW.cartonType ) THEN
	    select CONCAT(fieldNameList,'carton.cartonType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.cartonType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.cartonType,'~') into newValueList;
	END IF;
IF (OLD.numberOfCarton <> NEW.numberOfCarton ) THEN
	    select CONCAT(fieldNameList,'carton.numberOfCarton~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.numberOfCarton,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.numberOfCarton,'~') into newValueList;
	END IF;
IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
	    select CONCAT(fieldNameList,'carton.serviceOrderId~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
	END IF;
IF (OLD.totalPieces <> NEW.totalPieces ) THEN
	    select CONCAT(fieldNameList,'carton.totalPieces~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.totalPieces,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.totalPieces,'~') into newValueList;
	END IF;
IF (OLD.description <> NEW.description ) THEN
	    select CONCAT(fieldNameList,'carton.description~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.description,'~') into newValueList;
	END IF;
IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
	    select CONCAT(fieldNameList,'carton.ugwIntId~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
	END IF;



IF ((OLD.length <> NEW.length) or (OLD.length is null and NEW.length is not null)
      or (OLD.length is not null and NEW.length is null)) THEN
        select CONCAT(fieldNameList,'carton.length~') into fieldNameList;
        IF(OLD.length is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.length is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.length is not null) then
            select CONCAT(oldValueList,OLD.length,'~') into oldValueList;
        END IF;
        IF(NEW.length is not null) then
            select CONCAT(newValueList,NEW.length,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.volume <> NEW.volume) or (OLD.volume is null and NEW.volume is not null)
      or (OLD.volume is not null and NEW.volume is null)) THEN
        select CONCAT(fieldNameList,'carton.volume~') into fieldNameList;
        IF(OLD.volume is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.volume is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.volume is not null) then
            select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
        END IF;
        IF(NEW.volume is not null) then
            select CONCAT(newValueList,NEW.volume,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.width <> NEW.width) or (OLD.width is null and NEW.width is not null)
      or (OLD.width is not null and NEW.width is null)) THEN
        select CONCAT(fieldNameList,'carton.width~') into fieldNameList;
        IF(OLD.width is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.width is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.width is not null) then
            select CONCAT(oldValueList,OLD.width,'~') into oldValueList;
        END IF;
        IF(NEW.width is not null) then
            select CONCAT(newValueList,NEW.width,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.emptyContWeight <> NEW.emptyContWeight) or (OLD.emptyContWeight is null and NEW.emptyContWeight is not null)
      or (OLD.emptyContWeight is not null and NEW.emptyContWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.emptyContWeight~') into fieldNameList;
        IF(OLD.emptyContWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.emptyContWeight is not null) then
            select CONCAT(oldValueList,OLD.emptyContWeight,'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeight is not null) then
            select CONCAT(newValueList,NEW.emptyContWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.grossWeight <> NEW.grossWeight) or (OLD.grossWeight is null and NEW.grossWeight is not null)
      or (OLD.grossWeight is not null and NEW.grossWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.grossWeight~') into fieldNameList;
        IF(OLD.grossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.grossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.grossWeight is not null) then
            select CONCAT(oldValueList,OLD.grossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.grossWeight is not null) then
            select CONCAT(newValueList,NEW.grossWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.netWeight <> NEW.netWeight) or (OLD.netWeight is null and NEW.netWeight is not null)
      or (OLD.netWeight is not null and NEW.netWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.netWeight~') into fieldNameList;
        IF(OLD.netWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netWeight is not null) then
            select CONCAT(oldValueList,OLD.netWeight,'~') into oldValueList;
        END IF;
        IF(NEW.netWeight is not null) then
            select CONCAT(newValueList,NEW.netWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.crateit <> NEW.crateit) or (OLD.crateit is null and NEW.crateit is not null)
      or (OLD.crateit is not null and NEW.crateit is null)) THEN
        select CONCAT(fieldNameList,'carton.crateit~') into fieldNameList;
        IF(OLD.crateit is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.crateit is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.crateit is not null) then
            select CONCAT(oldValueList,OLD.crateit,'~') into oldValueList;
        END IF;
        IF(NEW.crateit is not null) then
            select CONCAT(newValueList,NEW.crateit,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.height <> NEW.height) or (OLD.height is null and NEW.height is not null)
      or (OLD.height is not null and NEW.height is null)) THEN
        select CONCAT(fieldNameList,'carton.height~') into fieldNameList;
        IF(OLD.height is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.height is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.height is not null) then
            select CONCAT(oldValueList,OLD.height,'~') into oldValueList;
        END IF;
        IF(NEW.height is not null) then
            select CONCAT(newValueList,NEW.height,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.uncrate <> NEW.uncrate) or (OLD.uncrate is null and NEW.uncrate is not null)
      or (OLD.uncrate is not null and NEW.uncrate is null)) THEN
        select CONCAT(fieldNameList,'carton.uncrate~') into fieldNameList;
        IF(OLD.uncrate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.uncrate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.uncrate is not null) then
            select CONCAT(oldValueList,OLD.uncrate,'~') into oldValueList;
        END IF;
        IF(NEW.uncrate is not null) then
            select CONCAT(newValueList,NEW.uncrate,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.totalVolume <> NEW.totalVolume) or (OLD.totalVolume is null and NEW.totalVolume is not null)
      or (OLD.totalVolume is not null and NEW.totalVolume is null)) THEN
        select CONCAT(fieldNameList,'carton.totalVolume~') into fieldNameList;
        IF(OLD.totalVolume is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalVolume is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalVolume is not null) then
            select CONCAT(oldValueList,OLD.totalVolume,'~') into oldValueList;
        END IF;
        IF(NEW.totalVolume is not null) then
            select CONCAT(newValueList,NEW.totalVolume,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.totalGrossWeight <> NEW.totalGrossWeight) or (OLD.totalGrossWeight is null and NEW.totalGrossWeight is not null)
      or (OLD.totalGrossWeight is not null and NEW.totalGrossWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.totalGrossWeight~') into fieldNameList;
        IF(OLD.totalGrossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalGrossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalGrossWeight is not null) then
            select CONCAT(oldValueList,OLD.totalGrossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.totalGrossWeight is not null) then
            select CONCAT(newValueList,NEW.totalGrossWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.totalNetWeight <> NEW.totalNetWeight) or (OLD.totalNetWeight is null and NEW.totalNetWeight is not null)
      or (OLD.totalNetWeight is not null and NEW.totalNetWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.totalNetWeight~') into fieldNameList;
        IF(OLD.totalNetWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalNetWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalNetWeight is not null) then
            select CONCAT(oldValueList,OLD.totalNetWeight,'~') into oldValueList;
        END IF;
        IF(NEW.totalNetWeight is not null) then
            select CONCAT(newValueList,NEW.totalNetWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.density <> NEW.density) or (OLD.density is null and NEW.density is not null)
      or (OLD.density is not null and NEW.density is null)) THEN
        select CONCAT(fieldNameList,'carton.density~') into fieldNameList;
        IF(OLD.density is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.density is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.density is not null) then
            select CONCAT(oldValueList,OLD.density,'~') into oldValueList;
        END IF;
        IF(NEW.density is not null) then
            select CONCAT(newValueList,NEW.density,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.grossWeightKilo <> NEW.grossWeightKilo) or (OLD.grossWeightKilo is null and NEW.grossWeightKilo is not null)
      or (OLD.grossWeightKilo is not null and NEW.grossWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'carton.grossWeightKilo~') into fieldNameList;
        IF(OLD.grossWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.grossWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.grossWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.grossWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.grossWeightKilo is not null) then
            select CONCAT(newValueList,NEW.grossWeightKilo,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.emptyContWeightKilo <> NEW.emptyContWeightKilo) or (OLD.emptyContWeightKilo is null and NEW.emptyContWeightKilo is not null)
      or (OLD.emptyContWeightKilo is not null and NEW.emptyContWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'carton.emptyContWeightKilo~') into fieldNameList;
        IF(OLD.emptyContWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.emptyContWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.emptyContWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.emptyContWeightKilo is not null) then
            select CONCAT(newValueList,NEW.emptyContWeightKilo,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.netWeightKilo <> NEW.netWeightKilo) or (OLD.netWeightKilo is null and NEW.netWeightKilo is not null)
      or (OLD.netWeightKilo is not null and NEW.netWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'carton.netWeightKilo~') into fieldNameList;
        IF(OLD.netWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.netWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.netWeightKilo is not null) then
            select CONCAT(newValueList,NEW.netWeightKilo,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.volumeCbm <> NEW.volumeCbm) or (OLD.volumeCbm is null and NEW.volumeCbm is not null)
      or (OLD.volumeCbm is not null and NEW.volumeCbm is null)) THEN
        select CONCAT(fieldNameList,'carton.volumeCbm~') into fieldNameList;
        IF(OLD.volumeCbm is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.volumeCbm is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.volumeCbm is not null) then
            select CONCAT(oldValueList,OLD.volumeCbm,'~') into oldValueList;
        END IF;
        IF(NEW.volumeCbm is not null) then
            select CONCAT(newValueList,NEW.volumeCbm,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.densityMetric <> NEW.densityMetric) or (OLD.densityMetric is null and NEW.densityMetric is not null)
      or (OLD.densityMetric is not null and NEW.densityMetric is null)) THEN
        select CONCAT(fieldNameList,'carton.densityMetric~') into fieldNameList;
        IF(OLD.densityMetric is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.densityMetric is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.densityMetric is not null) then
            select CONCAT(oldValueList,OLD.densityMetric,'~') into oldValueList;
        END IF;
        IF(NEW.densityMetric is not null) then
            select CONCAT(newValueList,NEW.densityMetric,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.totalTareWeight <> NEW.totalTareWeight) or (OLD.totalTareWeight is null and NEW.totalTareWeight is not null)
      or (OLD.totalTareWeight is not null and NEW.totalTareWeight is null)) THEN
        select CONCAT(fieldNameList,'carton.totalTareWeight~') into fieldNameList;
        IF(OLD.totalTareWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalTareWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalTareWeight is not null) then
            select CONCAT(oldValueList,OLD.totalTareWeight,'~') into oldValueList;
        END IF;
        IF(NEW.totalTareWeight is not null) then
            select CONCAT(newValueList,NEW.totalTareWeight,'~') into newValueList;
        END IF;
    END IF;

    IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'carton.status~') into fieldNameList;
      IF(OLD.status = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.status = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(OLD.status is null) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(NEW.status = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.status = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
       IF(NEW.status is null) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
   END IF;

   update serviceorder set isSOExtract=false,updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.serviceorderid;

CALL add_tblHistory (OLD.id,"carton", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpid, now());

END $$


delimiter;