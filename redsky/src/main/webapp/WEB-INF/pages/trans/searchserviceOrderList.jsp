<%@ include file="/common/taglibs.jsp"%>  
 
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading'/>"/>  
    
    <script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script> 
</head>   
  
<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editServiceOrder.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
           
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>   

     
<s:form id="serviceOrderListForm" action="searchServiceOrders" method="post" >   

<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">Search</div></td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>

<table class="table" width="70%" >
<thead>
<tr>

<th><fmt:message key="serviceOrder.shipNumber"/></th>
<th><fmt:message key="serviceOrder.firstName"/></th>
<th><fmt:message key="serviceOrder.lastName"/></th>
<th><fmt:message key="serviceOrder.status"/></th>
<th><fmt:message key="serviceOrder.statusDate"/></th>
<th><fmt:message key="serviceOrder.job"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="serviceOrder.shipNumber" size="9" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="serviceOrder.firstName" size="9" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="serviceOrder.lastName" size="9" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="serviceOrder.status" size="9" required="true" cssClass="text medium"/>
			</td>
			<c:if test="${not empty serviceOrder.statusDate}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate" value="%{customerFileMoveDateFormattedValue}" readonly="true" size="7" maxlength="11" /><img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 
WIDTH=20 onclick="cal.select(document.forms['serviceOrderListForm'].statusDate,'calender1',document.forms['serviceOrderListForm'].dateFormat.value); return false;"/></td>

			</c:if>
			<c:if test="${empty serviceOrder.statusDate}">
			<td><s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate"  size="7" maxlength="11" readonly="true"/><img id="calender1" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 
WIDTH=20 onclick="cal.select(document.forms['serviceOrderListForm'].statusDate,'calender1',document.forms['serviceOrderListForm'].dateFormat.value); return false;"/></td>
			</c:if>
			<td>
			    <s:textfield name="serviceOrder.job" size="5" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:submit cssClass="button" method="search" key="button.search"/>   
			</td>
		</tr>
		</tbody>
	</table>


<c:out value="${searchresults}" escapeXml="false" /> 


<s:set name="serviceOrders" value="serviceOrders" scope="request"/>

<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">List</div></td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>

<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="4" pagesize="10">   

<display:column property="shipNumber" sortable="true" titleKey="serviceOrder.shipNumber" href="editServiceOrderUpdate.html"    
        paramId="id" paramProperty="id"/>
 <display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber"/>  

<display:column property="firstName" sortable="true" titleKey="serviceOrder.firstName"/>
<display:column property="lastName" sortable="true" titleKey="serviceOrder.lastName"/>
<display:column property="status" sortable="true" titleKey="serviceOrder.status"/>
<display:column property="statusDate" sortable="true" titleKey="serviceOrder.statusDate" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="job" sortable="true" titleKey="serviceOrder.job"/>  
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity"/>  
<display:column property="billToCode" sortable="true" titleKey="serviceOrder.billToCode"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode"/>   
<display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry"/>  
<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry"/>  
<display:column property="createdOn" sortable="true" titleKey="serviceOrder.createdOn"  format="{0,date,dd-MMM-yyyy}"/>  




<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>


 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
  
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>

<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,dd/MM/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd/MM/yyyy"/>
<% } %>


<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<c:if test="${not empty customerFile.sequenceNumber}">
	 <a href="editServiceOrder.html?id=<%=request.getParameter("id") %> "><input type="button" value="Add" /></a>
</c:if>
 

 </s:form>
<script type="text/javascript">   
    highlightTableRows("serviceOrderList");
    Form.focusFirstElement($("serviceOrderListForm"));
    
</script>   
 
