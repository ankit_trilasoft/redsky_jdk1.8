<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  
<head>   
    
    <style type="text/css"> 

h4 {
    color: #444;
    font-size: 14px;
    line-height: 1.3em;
    margin: 0 0 0.25em;
    padding: 0 0 0 15px;
    font-weight:bold;
}
.modal-body {
    padding: 0 20px;
    max-height: calc(100vh - 140px);
    overflow-y: auto;
    margin-top: 10px
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    min-height: 5.43px;
    padding: 10px;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 10px;
    text-align: right;
}
.modal-header .close {
    margin-right: 15px;
    margin-top: -2px;
    text-align: right;
}
@media screen and (min-width: 768px) {
    .custom-class {
        width: 70%;
        /* either % (e.g. 60%) or px (400px) */
    }
}
.hide{
	display:none;
}
.show{
	display:block;
}
</style> 
</head>
<div class="modal-dialog" style="width:40%;">
 <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mark&nbsp;Up&nbsp;Detail</h4>
      </div>
<div class="modal-body">
<s:form name="markUpCalculationForm" id="markUpCalculationForm">
<s:hidden name="markupVal" id="markupVal" value="${markupVal}" />
<s:hidden name="shipNumber" id="shipNumber" value="${shipNumber}" />
<s:hidden name="aid" id="aid" />

<c:if test="${markUpCalculationList!='[]'}">
	<table> 
		<tr>
		<td align="right" class="listwhitetext" style="font-size: 12px;" width="">Mark Up % for All Lines :</td>
		<td>
		<input style="vertical-align:bottom;" type="checkbox" name="selectall" id="selectall" onclick="selectAll(this);"/>
		</td>
		<!-- <td align="right" class="listwhitetext" style="font-size: 12px;" width="">Mark Up % for One Line:</td>
		<td>
		<input style="vertical-align:bottom;" type="checkbox" id="selectone" onclick="selectOne(this);"/>
		</td> -->
		</tr>
	</table>
</c:if>

<c:if test="${markUpCalculationList=='[]'}">
<table class="table" id="dataTable" style="width:550px;margin-top:0px;">
	<thead>					 
			<tr>
					<th width="">Acc. Line No</th>
					 <th width="">Charge Code</th>
					 <th width="">Expense</th>
					 <th width="">Revenue</th>
			</tr>
	</thead>
			<tbody>
			<td  class="listwhitetextWhite" colspan="3">Nothing Found To Display.</td>
			</tbody>
</table>
</c:if>


<c:if test="${markUpCalculationList!='[]'}"> 
<s:set name="markUpCalculationList" value="markUpCalculationList" scope="request"/>
<display:table name="markUpCalculationList" class="table" requestURI="" id="markUpCalculationList" style="width:500px" pagesize="50" >   
		 <display:column property="accountLineNumber" title="Acc. Line No" style="width:100px" />		 
		 <display:column property="chargeCode" title="Charge Code" style="width:180px"/> 
		 <display:column property="estimateExpense" title="Expense" style="width:80px"/>
		 <display:column property="estimateRevenueAmount" title="Revenue" style="width:80px"/>
		 <s:hidden name="${markUpCalculationList.aid}" value="${markUpCalculationList.aid}" /> 
	    <display:column   title="" style="width:10px" >
 			<input style="vertical-align:bottom;" type="checkbox" name="radioButton" id="radioButton${markUpCalculationList.aid}" value="${markUpCalculationList.aid}" onclick="setValues('${markUpCalculationList.aid}',this)"/>
 		</display:column>
</display:table>

<div class="modal-footer">
	<button type="button" class="btn btn-primary" value="Calculate Mark Up" onclick="updateMarkUpInAccountline();">Apply</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</c:if>


</s:form>
</div></div></div>

<script type="text/javascript">
	function updateMarkUpInAccountline(){
		var aidList = document.getElementById("aid").value;
		var all = document.forms['markUpCalculationForm'].elements['selectall'].checked;
		if(aidList=='' && all==false){
			alert('Please choose any check box to contniue.')
		}else{
			var aid = document.getElementById("aid").value;
			var shipNumber = document.forms['markUpCalculationForm'].elements['shipNumber'].value;
			var markupVal = document.forms['markUpCalculationForm'].elements['markupVal'].value;
			$.ajax({
				type: "POST",
				url: "updateMarkUpInAccountLineAjax.html",
				data: {aid:aid,shipNumber:shipNumber,markupVal:markupVal},
				success: function (data, textStatus, jqXHR) {
					reloadParent();
		           },
					error: function (data, textStatus, jqXHR) {
						markUpModalClose();
					}
		         });
		}
	}

	function reloadParent() {
		window.location.reload();
  		markUpModalClose();
	}
   
   function setValues(rowId,targetElement){
	   if(targetElement.checked==false){
		   document.getElementById("aid").value='';
		}
	  if(targetElement.checked){
		  var userCheckStatus = document.getElementById("aid").value
		  if(userCheckStatus != ''){
		   	 	document.getElementById('radioButton'+userCheckStatus).checked =false;
		   	 	document.getElementById("aid").value = rowId;
		    }else{
		    	document.getElementById("aid").value = rowId;
		    }
	   }
  	}
   
   function selectAll(target){
	   document.getElementById("aid").value="";
	   if(target.checked){
			var inputs = document.getElementsByName("radioButton");
			for(var i = 0; i < inputs.length; i++) {
				inputs[i].checked = target.checked;
				inputs[i].disabled = true;
			}
	   }
	   if(target.checked==false){
		   var inputs = document.getElementsByName("radioButton");
			for(var i = 0; i < inputs.length; i++) {
				inputs[i].checked =false;
				inputs[i].disabled = false;
			}
	   }
	}
   
  </script>
 		  	