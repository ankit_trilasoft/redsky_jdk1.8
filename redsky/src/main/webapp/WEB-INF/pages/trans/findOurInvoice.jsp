<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findOurInviceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findOurInviceList.heading'/>"/> 
 <script language="javascript" type="text/javascript"> 
  function clear_fields(){ 
			    document.forms['searchForm'].elements['accountLine.recInvoiceNumber'].value = "";
			    document.forms['searchForm'].elements['accountLine.billToCode'].value = "";
		        document.forms['searchForm'].elements['accountLine.shipNumber'].value = "";
		        document.forms['searchForm'].elements['accountLine.billToName'].value="";
}

function selectSearchField(){ 
		var recInvoiceNumber=document.forms['searchForm'].elements['accountLine.recInvoiceNumber'].value; 
		var billToCode= document.forms['searchForm'].elements['accountLine.billToCode'].value;
		var shipNumber=document.forms['searchForm'].elements['accountLine.shipNumber'].value;
		var billToName=document.forms['searchForm'].elements['accountLine.billToName'].value;
		if(recInvoiceNumber=='' && billToCode=='' &&  shipNumber=='' && billToName=='')
		{
			alert('Please select any one of the search criteria!');	
			return false;	
		}
		
		
		
}
function goToSearch(){
		return selectSearchField();
        document.forms['searchForm'].action = 'searchOurInviceList.html';
        document.forms['searchForm'].submit();
}
function findTotalOfInvoice1(shipNumber,invoiceNumber,position)
{ 
var url="findTotalOfInvoice.html?ajax=1&decorator=simple&popup=true&shipNumber=" +shipNumber+ "&recInvoiceNumber="+invoiceNumber; 
  ajax_showTooltip(url,position);	
//openWindow('findTotalOfInvoice.html?shipNumber='+ShipNumber+'&recInvoiceNumber='+InvoiceNumber+'&decorator=popup&popup=true',900,300);
}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-18px;
padding:2px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>   
<s:form id="searchForm" name="searchForm" action="searchOurInviceList" method="post" validate="true" >
<c:set var="checkfromfilecabinetinvoicereport" value="N"/>
<configByCorp:fieldVisibility componentId="component.accountline.Invoice.FileCabinetInvoiceReport">
	<c:set var="checkfromfilecabinetinvoicereport" value="Y"/>
</configByCorp:fieldVisibility>
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<div id="layer4" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" method="searchOurInviceList" key="button.search" onclick="return goToSearch();" />   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th><fmt:message key="accountLine.recInvoiceNumber"/></th>
		<th>Bill To Code</th>
		<th>Bill To Name</th>
		<th>S/O#</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="accountLine.recInvoiceNumber" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="accountLine.billToCode" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="accountLine.billToName" required="true" cssClass="input-text" size="30"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="accountLine.shipNumber" required="true" cssClass="input-text" size="30"/>
					</td>
					</tr>
					<tr>
					<td colspan="3"></td>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			<s:set name="findOurInviceList" value="findOurInviceList" scope="request"/>
			
<div id="Layer1" style="width:100%" >
	<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Find Our Invoice List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

 

<display:table name="findOurInviceList" class="table" requestURI="" id="findOurInviceList" style="width:100%; margin-top: 1px;!margin-top: -1px;" defaultsort="1"  pagesize="10" >
     
     <display:column group="1" sortable="true" titleKey="accountLine.recInvoiceNumber" style="width:10%" sortProperty="recInvoiceNumber" paramProperty="recInvoiceNumber" >
     <a onclick="findTotalOfInvoice1('${findOurInviceList.shipNumber}','${findOurInviceList.recInvoiceNumber}',this)"/>
			<c:out value="${findOurInviceList.recInvoiceNumber}" /></a>
	<c:if test="${findOurInviceList.recInvoiceNumber != '' && findOurInviceList.recInvoiceNumber != null}">
    <a><img align="top" title="Forms" onclick="findUserPermission1('${findOurInviceList.recInvoiceNumber}','${findOurInviceList.companyDivision}',this,'${findOurInviceList.job}','${findOurInviceList.shipNumber}','${findOurInviceList.billToCode}');" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
    </c:if>
     </display:column>
  <%--  <display:column property="shipNumber" sortable="true" titleKey="accountLine.shipNumber"  style="width:10%" url="/accountLineList.html?sid=${findOurInviceList.sid}&id=${findOurInviceList.id}"/>--%>
    <c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column sortable="true"  title="S/O#" style="width:10%" sortProperty="shipNumber">
	   <a href="pricingList.html?sid=${findOurInviceList.sid}&id=${findOurInviceList.id}"><c:out value="${findOurInviceList.shipNumber}" /></a>
     </display:column>
</c:when>
<c:otherwise>
     <display:column sortable="true"  title="S/O#" style="width:10%" sortProperty="shipNumber">
	   <a href="accountLineList.html?sid=${findOurInviceList.sid}&id=${findOurInviceList.id}"><c:out value="${findOurInviceList.shipNumber}" /></a>
     </display:column>
 </c:otherwise>
 </c:choose>    
      <%--
      <display:column property="recInvoiceNumber" sortable="true" titleKey="accountLine.recInvoiceNumber" href="accountLineList.html?sid=${findOurInviceList.sid}"    
       paramId="id" paramProperty="id" style="width:25px"/>   
     --%>
     <display:column  property="lastName" sortable="true"  sortProperty="lastName" style="width:10%" title="Shipper"/>
     <display:column  property="receivedInvoiceDate"  sortable="true" sortProperty="receivedInvoiceDate" titleKey="accountLine.receivedInvoiceDate"  format="{0,date,dd-MMM-yyyy}" style="width:10%"/> 
   	 <display:column  property="billToCode"   sortable="true" sortProperty="billToCode" title="Bill To Code"  style="width:7%"/>
	 <display:column  property="billToName"  sortable="true" sortProperty="billToName" title="Bill To Name"  style="width:10%"/>
	 <display:column property="companyDivision" sortable="true" title="S/O Com.&nbsp;Div." sortProperty="companyDivision" style="width:5%"/>
	 <display:column property="accCompanyDivision" sortable="true" title="Acc. Com.&nbsp;Div." sortProperty="companyDivision" style="width:5%"/>
	 <%--<display:column property="oldBillToCode"   sortable="true" title="Old Code"  style="width:7%"/> --%>
	 <display:column property="totalActualRevenue" headerClass="containeralign" style="text-align: right;width:7%;" sortable="true" sortProperty="totalActualRevenue" titleKey="accountLine.actualRevenue"/>
	 <display:column property="description" sortable="true" sortProperty="description" titleKey="accountLine.description"  style="width:30%"/>
	 <display:column property="paymentStatus" sortable="true" sortProperty="paymentStatus" title="Payment&nbsp;Status"  style="width:5%"/>
	 
	 
		
</display:table>
</div>
</div>
</s:form>

<script type="text/javascript"> 
try{
document.forms['searchForm'].elements['accountLine.recInvoiceNumber'].focus(); 
}
catch(e){}
function findUserPermission1(name,AccDivision,position,job,shipNumber,billToCode) { 
	  var accountLinecompanyDivision=AccDivision;
	  var url="reportBySubModuleInvoiceOurList.html?ajax=1&decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobNumber="+encodeURI(shipNumber)+"&companyDivision="+accountLinecompanyDivision+"&jobType=" + encodeURI(job)+"&modes=${serviceOrder.mode}&billToCode ="+billToCode+"&accountLinecompanyDivision="+accountLinecompanyDivision+"&reportModule=serviceOrder&reportSubModule=Accounting";
	  ajax_showTooltip(url,position);	
	  }
function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer,jobType){
	window.open('viewFormParam.html?id='+id+'&invoiceNumber='+invNum+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID=${noteID}&custID=${custID}&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
} 
function validatefields(id,invNum,jobNumber,reportName,docsxfer,jobType,val){
	var url="";
	<c:choose>
	<c:when test="${checkfromfilecabinetinvoicereport=='Y'}">   	
	 url='viewReportWithParam.html?formFrom=list&id='+id+'&invoiceNumber='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&checkfromfilecabinetinvoicereportflag=Y';
	</c:when>
	<c:otherwise>
	 url='viewReportWithParam.html?formFrom=list&id='+id+'&invoiceNumber='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'';
	</c:otherwise>
	</c:choose>
	location.href=url;	  
}
function winClose(id,invNum,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&invoiceNumber="+invNum+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
}
</script>  
		  	