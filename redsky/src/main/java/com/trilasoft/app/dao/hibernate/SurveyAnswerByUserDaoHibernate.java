package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.SurveyAnswerByUserDao;
import com.trilasoft.app.model.SurveyAnswerByUser;

public class SurveyAnswerByUserDaoHibernate extends GenericDaoHibernate<SurveyAnswerByUser, Long>implements SurveyAnswerByUserDao{
	public SurveyAnswerByUserDaoHibernate() {
		super(SurveyAnswerByUser.class);
	}
	
	public List getRecordsByQuestionId(Long qId,Long sid,Long cid){
		List list = new ArrayList();
		try {
			if(sid!=null){
				list = this.getSession().createSQLQuery("select concat(id,'~',answerId) from surveyanswerbyuser where questionId="+qId+" and serviceOrderId="+sid+"").list();
			}else{
				list = this.getSession().createSQLQuery("select concat(id,'~',answerId) from surveyanswerbyuser where questionId="+qId+" and customerFileId="+sid+"").list();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List getRecordsBySOId(Long sid){
		List list = new ArrayList();
		try {
			list = this.getSession().createSQLQuery("select * from surveyanswerbyuser where serviceOrderId="+sid+"").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List getRecordsByCId(Long cid){
		List list = new ArrayList();
		try {
			list = this.getSession().createSQLQuery("select * from surveyanswerbyuser where customerFileId="+cid+"").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
}
