<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
</head>

<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editPartnerAddForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<s:form id="partnerListForm" action='${empty param.popup?"searchPartners.html":"searchPartners.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
</c:if>
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">Search</div></td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>

<table class="table" width="90%" >
<thead>
<tr>
<th><fmt:message key="partner.name"/></th>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.billingCountryCode"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.lastName" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.partnerCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.billingCountryCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:submit cssClass="button" method="search" key="button.search"/>   
			</td>
		</tr>
		</tbody>
	</table>
</s:form>
<s:set name="partners" value="partners" scope="request"/>  
<table class='detailTabLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody>
	    <tr>
	      <td width="4" align="right"><img width="2" height="25"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">List</td>
			<td width="4" align="left"><img width="2" height="25"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
	    </tr>
	</tbody>
</table>
<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCharges.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
           
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>  





<s:form id="chargesListForm" action='${empty param.popup?"searchChargess.html":"searchChargess.html?decorator=popup&popup=true"}' method="post" >
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
</c:if>


<s:set name="partnerQuotess" value="partnerQuotess" scope="request"/>  

<display:table name="partnerQuotess" class="table" requestURI="" id="quotationVendorList" export="${empty param.popup}" defaultsort="2" pagesize="10" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
    <c:if test="${empty param.popup}">  
		<display:column property="vendorCode" sortable="true" titleKey="quotation.vendorCode"
		href="editCharges.html" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="quotation.vendorCode"/>   
    </c:if>    
    <display:column property="vendorName" sortable="true" titleKey="quotation.vendorName"/>
    
    
    <display:setProperty name="paging.banner.item_name" value="charges"/>   
    <display:setProperty name="paging.banner.items_name" value="chargess"/>   
  
    <display:setProperty name="export.excel.filename" value="partnerQuotes List.xls"/>   
    <display:setProperty name="export.csv.filename" value="partnerQuotes List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="partnerQuotes List.pdf"/>   
</display:table>  
 
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
</s:form>
<script type="text/javascript">   
   highlightTableRows("quotationVendorList");   
</script>