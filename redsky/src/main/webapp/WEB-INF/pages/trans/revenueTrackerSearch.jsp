<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Revenue Tracker</title>   
    <meta name="heading" content="Revenue Tracker"/>   
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-18px;!margin-top:-17px;padding:2px 0px;text-align:right;width:100%;
!width:98%;}
div.error, span.error, li.error, div.message {width:450px;margin-top:0px; }
form {margin-top:-40px;!margin-top:-5px;}
div#main {margin:-5px 0 0;}
.table td, .table th, .tableHeaderTable td {padding: 0.4em;}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['revenueTrackerListForm'].elements['reveTrackJob'].value = "";
		document.forms['revenueTrackerListForm'].elements['reveTrackcompDiv'].value = "";
		document.forms['revenueTrackerListForm'].elements['reveTrackBillToName'].value = "";
		document.forms['revenueTrackerListForm'].elements['reveTrackBillingPerson'].value = "";
		document.forms['revenueTrackerListForm'].elements['billingRecogDate'].value = "";
		document.forms['revenueTrackerListForm'].elements['loadAfterDate'].value = "";
		document.forms['revenueTrackerListForm'].elements['delAfterDate'].value = "";
		document.forms['revenueTrackerListForm'].elements['serviceCompDate'].value = "";
		document.forms['revenueTrackerListForm'].elements['billingComplete'].checked = false;
	}

	function selectSearchField(){		
		var job=document.forms['revenueTrackerListForm'].elements['reveTrackJob'].value; 
		var companyDiv= document.forms['revenueTrackerListForm'].elements['reveTrackcompDiv'].value;
		var billToName=document.forms['revenueTrackerListForm'].elements['reveTrackBillToName'].value;
		var billingPerson=document.forms['revenueTrackerListForm'].elements['reveTrackBillingPerson'].value;
		var billingRecogDate=document.forms['revenueTrackerListForm'].elements['billingRecogDate'].value;
		var loadAfterDate=document.forms['revenueTrackerListForm'].elements['loadAfterDate'].value;
		var delAfterDate=document.forms['revenueTrackerListForm'].elements['delAfterDate'].value;
		var serviceComp = document.forms['revenueTrackerListForm'].elements['serviceCompDate'].value;
		
		if(job=='' && companyDiv=='' &&  billToName=='' && billingPerson=='' && billingRecogDate=='' && loadAfterDate=='' && delAfterDate=='' && serviceComp==''){
			alert('Check at least one parameter is entered');	
			return false;	
		}else{
			return true;
		}
	}

	function goToSearch(){
		var selectedSearch= selectSearchField();
		if(selectedSearch){
			 document.forms['revenueTrackerListForm'].action = 'revenueTrackerSearch.html';
			 document.forms['revenueTrackerListForm'].submit();	
		}else{
			return false;
			}			
		}

</script>
<script language="javascript" type="text/javascript">
function extractFunction(){	
 		document.forms['revenueTrackerListForm'].action = 'revenueTrackerExtract.html';
    	document.forms['revenueTrackerListForm'].submit(); 		
 	}
     
</script>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" method="" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>
<s:form id="revenueTrackerListForm" action="" method="post" > 
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:11px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>Job Type</th>
			<th>Company Division</th>
			<th>Billing Person</th>
			<th>Bill To Name</th>
			<th>Revenue Recog. After</th>
			<th>Loading After</th>
			<th>Delivery After</th>
			<th>Srv. Cmpl. After</th>
			<th>Ignore Billing <br>Cmpl.</th>
			<th>Show Records</th>
			<th>Ticket Bill Status</th>
			<th></th>			
		</tr>
	</thead>	
	<tbody>
		<tr>			
			<td width="">
			     <s:select cssClass="list-menu" name="reveTrackJob" list="%{job}" cssStyle="width:115px" headerKey="" headerValue="" />
			</td>			
			<td width="70">
			    <s:select cssClass="list-menu" name="reveTrackcompDiv" list="%{companyDivis}" cssStyle="width:115px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<td width="125">
				<s:select name="reveTrackBillingPerson" cssClass="list-menu" list="%{billingList1}" cssStyle="width:115px" headerKey="" headerValue="" />
			</td>
			<td width="130">
			    <s:textfield name="reveTrackBillToName" size="20" required="true" cssStyle="width:115px;margin-top:2px;!width:100px;" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			 <c:if test="${not empty billingRecogDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="billingRecogDate" /></s:text>
				<td width="110"><s:textfield cssClass="input-text" id="date1" name="billingRecogDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty billingRecogDate}">
				<td width="110"><s:textfield cssClass="input-text" id="date1" name="billingRecogDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			
			<c:if test="${not empty loadAfterDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="loadAfterDate" /></s:text>
				<td width="110"><s:textfield cssClass="input-text" id="date2" name="loadAfterDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty loadAfterDate}">
				<td width="110"><s:textfield cssClass="input-text" id="date2" name="loadAfterDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>		
			
			<c:if test="${not empty delAfterDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="delAfterDate" /></s:text>
				<td width="110"><s:textfield cssClass="input-text" id="date3" name="delAfterDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty delAfterDate}">
				<td width="110"><s:textfield cssClass="input-text" id="date3" name="delAfterDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date3_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			
			<c:if test="${not empty serviceCompDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceCompDate" /></s:text>
				<td width="110"><s:textfield cssClass="input-text" id="date4" name="serviceCompDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty serviceCompDate}">
				<td width="110"><s:textfield cssClass="input-text" id="date4" name="serviceCompDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			<td style="border:none;vertical-align:bottom;width:45px;" class="listwhitetext">
			<s:checkbox key="billingComplete" cssStyle="vertical-align:middle; margin:5px;"/>
			</td>
			<td width="">
			     <s:select cssClass="list-menu" name="showRecord" list="%{showRecordList}" cssStyle="width:115px" />
			</td>
			<td width="">
			     <s:select cssClass="list-menu" name="reviewStatus" list="%{billStatus}" headerKey="All" headerValue="All" cssStyle="width:115px" />
			</td>
		
		<td width="110">
			<c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false" />  
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

 <div id="otabs" style="margin-bottom:31px;">
	<ul>
		<li><a class="current"><span>Service Order List</span></a></li>
	</ul>
</div> 
<div class="spnblk">&nbsp;</div> 
<s:set name="revenueTrackerList" value="revenueTrackerList" scope="request"/>

<display:table name="revenueTrackerList" class="table"  requestURI="" id="revenueTrackerList" >
<c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column sortable="true" title="ShipNumber" >
	<a href="pricingList.html?sid=${revenueTrackerList.id}" target="_blank">
                 <c:out value="${revenueTrackerList.shipnumber}" />
                  </a> 
 </display:column>
    </c:when>
    <c:otherwise>
<display:column sortable="true" title="ShipNumber" >
	<a href="accountLineList.html?sid=${revenueTrackerList.id}" target="_blank">
                 <c:out value="${revenueTrackerList.shipnumber}" />
                  </a> 
 </display:column>
 </c:otherwise>
 </c:choose>
<display:column property="shipperName" title="Shipper Name" ></display:column>
<display:column property="billtoname" sortable="true" title="Billing Party"/>
<display:column property="job" sortable="true" title="Job"/>
<display:column property="personBilling" sortable="true" title="Billing Person"/>
<display:column property="servicetype" sortable="true" title="Service" style="width:20px"/>
<display:column property="routing" sortable="true" title="Routing"/>
<display:column sortable="true" title="Est/Revised">
<div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${revenueTrackerList.est}" />
                  </div></display:column>
<display:column sortable="true" title="Revenue">
<div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${revenueTrackerList.rev}" />
                </div> </display:column>
                  
<display:column sortable="true" title="Shortage" style="border-right:1px solid #61A5D0;padding:0px 2px 0px 0px;">
<c:if test="${revenueTrackerList.rev != '0'}">
	<div align="right" style="color:red;">
	<c:if test="${revenueTrackerList.shortage != 'n/a'}">
		<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${revenueTrackerList.shortage}" />
                  </c:if>
      <c:if test="${revenueTrackerList.shortage == 'n/a'}">  
                ${revenueTrackerList.shortage}
                </c:if>
               </div> 
</c:if>   
<c:if test="${revenueTrackerList.rev == '0'}">
<div align="right">
<c:if test="${revenueTrackerList.shortage != 'n/a'}">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${revenueTrackerList.shortage}" />
                  </c:if>
      <c:if test="${revenueTrackerList.shortage == 'n/a'}">  
                ${revenueTrackerList.shortage}
                </c:if>
               </div> 
</c:if>                           
</display:column>
<display:column sortable="true" title="Unbill Tkts" >
	<c:if test="${revenueTrackerList.workTicketCount == '0'}">
		<div align="right">
			<c:out value="${revenueTrackerList.workTicketCount}" />
		</div>
	</c:if>
	<c:if test="${revenueTrackerList.workTicketCount != '0'}">
		<div align="right" style="color:red;">
			<c:out value="${revenueTrackerList.workTicketCount}" />
		</div>
	</c:if>
</display:column>
<display:column property="revRecg" sortable="true" title="Rev Recg" format="{0,date,dd-MMM-yyyy}" />
<display:column property="days" sortable="true" title="Days" style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="loading" sortable="true" title="Loading" format="{0,date,dd-MMM-yyyy}" />
<display:column property="loadDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="delivery" sortable="true" title="Delivery" format="{0,date,dd-MMM-yyyy}" />
<display:column property="delDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="svCompDate" sortable="true" title="Srv. Cmpl." format="{0,date,dd-MMM-yyyy}" />
<display:column property="svDays" sortable="true" title="Days"  style="border-right:1px solid #61A5D0;padding:0px;"/>
<display:column property="billCompDate" sortable="true" title="Billing Cmpl." format="{0,date,dd-MMM-yyyy}" />
 <display:column property="recInvoiceNumber" sortable="true" title="Invoice #"/> 
</display:table>


<div id="showExtract">
<table style="margin-bottom:5px">
<tr><td width="10px;"></td><td class="bgblue" >Revenue Tracker Extract</td></tr>
<tr><td height="5px"></td></tr>
</table>
<table style="margin-bottom:5px">
<tr>
<td width="10px"></td>
<td><button type="button" class="cssbutton" style="width:155px;" onclick="extractFunction();"	>	
        			<span style="color:black;">Download into Excel</span></button></td>
</tr>
</table>
</div>

</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">  
try{
var st2=document.getElementById('showExtract');
var showList = '${revenueTrackerList}';
	if(showList != ''){			
		st2.style.display = 'block';
	}
	if(showList == ''){			
		st2.style.display = 'none';
	}
}catch(e){} 
</script> 
  