package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.BaseDaoHibernate;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.service.SignUpService;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.trilasoft.app.dao.CustomerFileServiceDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.service.CustomerFileService;

public class CustomerFileServiceDaoHibernate extends GenericDaoHibernate<CustomerFileService,Long> implements  CustomerFileServiceDao{
	
	public CustomerFileServiceDaoHibernate() {
		super(CustomerFileService.class);
		
	}
	
	private String massage=new String ("");
	public String saveCustomerFile(CustomerFile customerFile) { 
	massage=""; 
	Session session = null;
    //Transaction tx = null;
    SessionFactory sessionFactory = null;
    try
    {
        //Configuration configuration = new Configuration();
        // Configure from hibernate.cfg.xml at root of classpath.
        //configuration.configure();
        //sessionFactory = configuration.buildSessionFactory();
        //session = getHibernateTemplate().getSessionFactory().openSession();
        //session.setFlushMode(FlushMode.COMMIT);
        //tx = session.beginTransaction();
        this.getSession().save(customerFile);
        //getHibernateTemplate().saveOrUpdate(customerFile);
        //tx.commit();
       
        System.out.println("customerFile  saved!");
    }
    catch (Exception e)
    {
    	 massage="Some data issue in form";
        try
        {
            /*if (tx != null)
            {
                tx.rollback();
            }*/
        }
        catch (Exception ignore)
        {
            // ignore
        }
        
    }
    finally
    {
        if (session != null)
        {
            try
            {
                session.close();
            }
            catch (Exception ignore)
            {
                // ignore
            }
        }
        if (sessionFactory != null)
        {
            try
            {
                sessionFactory.close();
            }
            catch (Exception e)
            {
                // ignore
            }
        }
    }
    return massage;
	}


	public List findMaximum(String corpId) { 
		return getHibernateTemplate().find("select max(sequenceNumber) from CustomerFile where sequenceNumber like '"+corpId+"%'");
	}


	public List getSurveyExternalIPsList(String corpId) {  
		return getHibernateTemplate().find("select surveyExternalIPs from SystemDefault where surveyExternalIPs is not null and corpID=?", corpId);
	}

}
