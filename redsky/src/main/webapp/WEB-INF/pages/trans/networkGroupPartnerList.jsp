<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    

<script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
				document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value = "";
				document.forms['partnerListForm'].elements['partner.terminalState'].value = "";
				document.forms['partnerListForm'].elements['partner.terminalCountry'].value = "";
			</c:if>  
   			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
				document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
				document.forms['partnerListForm'].elements['partner.billingState'].value = "";
				document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
			</c:if> 
			<c:if test="${partnerType == 'AG'}">  
			document.forms['partnerListForm'].elements['vanlineCodeSearch'].value = "";
			</c:if>		
} 

function setAgent(){

	<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "AG";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].elements['partner.lastName'].value =document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value =document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	
	document.forms['partnerListForm'].submit();
} 

function openOriginLocation(address1,address2,city,zip,state,country) {
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
	}
	 
</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
  

 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"searchPartnerAdmin.html":"searchPartnerNetworkGroup.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="sid" value="<%= request.getParameter("sid")%>" />
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />


<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<s:hidden  name="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
	<s:hidden  name="fld_ninthDescription" value="${param.fld_ninthDescription}" />	
	<s:hidden  name="fld_tenthDescription" value="${param.fld_tenthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
    <c:set var="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
     <c:set var="fld_ninthDescription" value="${param.fld_ninthDescription}" />	
      <c:set var="fld_tenthDescription" value="${param.fld_tenthDescription}" />	    
</c:if>

<div id="layer1" style="width:100%;">
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<c:if test="${partnerType == 'AG'}">  
<th>Vanline Code</th>
</c:if>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="5" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="15" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="15" cssClass="input-text" />
			</td>
			<c:if test="${partnerType == 'AG'}">
			<td>
			    <s:textfield name="vanlineCodeSearch" size="14" cssClass="input-text" />
			</td>			
			</c:if>			
			<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'|| partnerType == 'DF'}">  
			 			<td>
						    <s:textfield name="partner.terminalCountryCode" size="7" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalCountry" size="18" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalState" size="12" cssClass="input-text"/>
						</td>
			</c:if>
			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
						<td>
						    <s:textfield name="partner.billingCountryCode" size="7" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.billingCountry" size="18" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.billingState" size="12" cssClass="input-text"/>
						</td>
			</c:if>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td width="130px" colspan="3" style="border-left: hidden;text-align:right;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
	</s:form> 
<div id="layer2" style="width:100%;">
<div id="newmnav">   
<ul>  
<c:choose> 
	<c:when test="${partnerType == 'AG'}"> 
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	 </c:when> 
	<c:otherwise>
	    <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>	 
	</c:otherwise>
</c:choose> 
</ul>
</div><div class="spn" style="width:100%">&nbsp;</div><br>

<s:set name="partners" value="partners" scope="request"/>
<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
	<c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
     <display:column  title="Alias Name" sortable="true" style="width:390px"><c:out value="${partnerList.aliasName}" /></display:column>
    <c:if test="${partnerType == 'AG'}"> 
	    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
    	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'VN'  || partnerType == 'CR' }"> 
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
	    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'PP' || partnerType == 'AC'}">
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'OO'}">
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
    	<display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    
    </c:if> 
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:120px"/>
    <c:if test="${agentClassificationShow=='Y'}">
    <display:column title="Agent Classification" style="width:45px;" >
 	<c:if test="${partnerType == 'AG'}">
    	<c:out value="${partnerList.agentClassification}" />
	</c:if>
	</display:column>
	</c:if>
    <c:if test="${param.popup}">
    	<display:column style="width:120px;cursor:pointer;"><A onclick="location.href='<c:url value="/viewPartner.html?id=${partnerList.id}&sid=${sid}&partnerType=${partnerType}&type=ZZ&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:if>
    
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 
 
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${not empty param.popup && partnerType == 'PP' && (flag==0 || flag==1 || flag==2 || flag==3 || flag==4)}">  
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td><c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/> 

<script type="text/javascript">  
   Form.focusFirstElement($("partnerListForm")); 
  try{
	  if('<%=session.getAttribute("lastName")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.lastName'].value='';
	   	}
	  else if('<%=session.getAttribute("lastName")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.lastName'].value='<%=session.getAttribute("lastName")%>';
	   		<% session.setAttribute("lastName","");%>
	   	}
   	}
   	catch(e){}
   	try{
   		if('<%=session.getAttribute("partnerCode")%>'=='null'){	
	   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='';
	   	}
   		else if('<%=session.getAttribute("partnerCode")%>'!='null'){	
	   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='<%=session.getAttribute("partnerCode")%>';
	   		<% session.setAttribute("partnerCode","");%>
	   	}
   	}
   	catch(e){}
   	try{
   	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
   	if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='';
   	}else if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
   		<% session.setAttribute("billingCountryCode","");%>
   	}
   	
   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='';
   	}else if('<%=session.getAttribute("billingCountry")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='<%=session.getAttribute("billingCountry")%>';
   		<% session.setAttribute("billingCountry","");%>
   	}
   	
   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.terminalState'].value='';
   	}else if('<%=session.getAttribute("billingStateCode")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.terminalState'].value='<%=session.getAttribute("billingStateCode")%>';
   		<% session.setAttribute("billingStateCode","");%>
   	}
   	</c:if> 
   	}
   	catch(e){} 
   try{
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
   	if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='';
   	}else if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
   		<% session.setAttribute("billingCountryCode","");%>
   	}
   	
   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='';
   	}else if('<%=session.getAttribute("billingCountry")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='<%=session.getAttribute("billingCountry")%>';
   		<% session.setAttribute("billingCountry","");%>
	   	}
   	
   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.billingState'].value='';
   	}else if('<%=session.getAttribute("billingStateCode")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.billingState'].value='<%=session.getAttribute("billingStateCode")%>';
   		<% session.setAttribute("billingStateCode","");%>
	   	}
	</c:if>
   
    }
    catch(e){}
	
	
	
       
</script>