<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
 

<SCRIPT LANGUAGE="JavaScript">

function findBookingAgentName(){
    var billToCode = document.forms['serviceForm1'].elements['sid'].value;
     var url="accountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>2)
                {
                 // var agree=confirm("Bill to name is empty for some account line,\nDo you want to create invoice?\n\nClick OK to continue.");
                 // if(agree){
                  // document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
                  // document.forms['serviceForm1'].action='updateSOfromaccountLines.html'
                   // document.forms['serviceForm1'].submit();
                // }
 				 alert("Invoices cannot be generated as Bill-to code is missing for receivable lines");
 				 document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
                 document.forms['serviceForm1'].action='updateSOfromaccountLines.html'
                 document.forms['serviceForm1'].submit();
 				 //return true;
 				 //alert("a");
 				}
                 else
                 {
                    document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
                     document.forms['serviceForm1'].action='updateSOfromaccountLines.html'
                    document.forms['serviceForm1'].submit();
					 
                 }
             }
        }        
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
</script>

<script type="text/javascript">
	function onLoad(){
	//  alert("Hi Raj");
	//alert("1");  
      onEntitledLoad();

     onExpenseLoad();
     onRevisionLoad();
      ActualExp();
      document.forms['serviceForm1'].elements['buttonType'].value = "calc";
     return false;
}

function setType(){
document.forms['serviceForm1'].elements['buttonType'].value = "invoice";
}

function ActualExp()
{
    var sumActualExpense=0;
	var sumActualRevenue=0;
	var diff=0;
	<c:forEach var="accountLine" items="${accountLines}">
	 
	  sumActualExpense+=${accountLine.actualExpense};
	  sumActualRevenue+=${accountLine.actualRevenue};
	
	</c:forEach >
	// alert(sumActualExpense);
	//alert(sumActualRevenue);
	document.forms['serviceForm1'].elements['actualExpenseTotal'].value= roundNumber(sumActualExpense*1);
	document.forms['serviceForm1'].elements['actualRevenueTotal'].value= roundNumber(sumActualRevenue*1);
	diff=(sumActualRevenue-sumActualExpense)*1;
	// alert(diff);
	document.forms['serviceForm1'].elements['actualGrossMar'].value= roundNumber(diff);
	if(diff==""||sumActualRevenue=="")
	{
	  document.forms['serviceForm1'].elements['actualGrossMarginPer'].value=0*1;
	}
	else
	{
	   document.forms['serviceForm1'].elements['actualGrossMarginPer'].value=roundNumber(((sumActualRevenue-sumActualExpense)*100)/sumActualRevenue);
	}
	
//alert(document.forms['serviceForm1'].elements['actualExpenseTotal'].value);
//alert(document.forms['serviceForm1'].elements['actualRevenueTotal'].value);
}

function onEntitledLoad()
{
    var sumRevenue=0;
   <c:forEach var="accountLine" items="${accountLines}">
     sumRevenue += ${accountLine.entitlementAmount};
     
   </c:forEach >
   document.forms['serviceForm1'].elements['entitledTotal'].value = roundNumber(sumRevenue);
   //alert(document.forms['serviceForm1'].elements['entitledTotal'].value);
 }

function onExpenseLoad(){
	
	var sumRevenue=0;
	var sumExp=0;
	var diff=0;
    
    <c:forEach var="accountLine" items="${accountLines}">
      sumRevenue += ${accountLine.estimateRevenueAmount};
	  sumExp += ${accountLine.estimateExpense};
    </c:forEach >
	
	document.forms['serviceForm1'].elements['estimateRevenueAmt'].value = roundNumber(sumRevenue*1);
	document.forms['serviceForm1'].elements['estimateExp'].value = roundNumber(sumExp*1);
	diff = (sumRevenue - sumExp)*1;
	 document.forms['serviceForm1'].elements['grossMrgn'].value=diff;
	if(diff==""||sumRevenue=="")
	{
	  document.forms['serviceForm1'].elements['gmPercent'].value=0*1;
	}
	
	else
	{
	  document.forms['serviceForm1'].elements['gmPercent'].value = roundNumber(((sumRevenue - sumExp)*100)/sumRevenue)*1;
	}
	//alert("4");
}

function onRevisionLoad(){
	
	var sumRevenue=0;
	var sumExp=0;
	var diff=0;
    
    <c:forEach var="accountLine" items="${accountLines}">
      sumRevenue += ${accountLine.revisionRevenueAmount};
	  sumExp += ${accountLine.revisionExpense};
    </c:forEach >
	
	document.forms['serviceForm1'].elements['revisedTotalRev'].value = roundNumber(sumRevenue*1);
	document.forms['serviceForm1'].elements['revisedTotal'].value = roundNumber(sumExp*1);
	diff = (sumRevenue - sumExp)*1;
	
	document.forms['serviceForm1'].elements['revisedGrossMar'].value=roundNumber(diff);
	
	if(diff==""||sumRevenue=="")
	{
	  document.forms['serviceForm1'].elements['revisedGrossMarginPer'].value=0*1;
	}
	else
	{
	  // alert(diff);
	  document.forms['serviceForm1'].elements['revisedGrossMarginPer'].value = roundNumber(((sumRevenue - sumExp)*100)/sumRevenue);
	  //  alert(document.forms['serviceForm1'].elements['revisedGrossMarginPer'].value);
	}
	// alert("5");
}

function roundNumber(numberToRound) {

	//alert("Hello from numberToRound")
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	//alert(numberToRound);
	var rnum = numberToRound;// numberField.value;
	var rlength = 2; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	}
	//numberField.value = newnumber;
	//alert("Resultant number"+newnumber)
	return newnumber;
}

function roundNumberForPercent(numberToRound) {

	//alert("Hello from numberToRound")
	var numberField = numberToRound;	//document.roundform.numberfield; // Field where the number appears
	//alert(numberToRound);
	var rnum = numberToRound;// numberField.value;
	var rlength = 1; // The number of decimal places to round to
	if (rnum > 8191 && rnum < 10485) {
		rnum = rnum-5000;
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
		newnumber = newnumber+5000;
	} else {
		var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	}
	//numberField.value = newnumber;
	//alert("Resultant number"+newnumber)
	return newnumber;
}
function showMessage()
{
	alert("Billing Information has not been created.");
}
function openActiveList()
{
	var accountLineStatusInactive = document.forms['serviceForm1'].elements['accountLineStatus'].value='true';
	document.forms['serviceForm1'].action = 'quotationAccountLineList.html';
    document.forms['serviceForm1'].submit();

}
function openAllList()
{
	var accountLineStatusInactive = document.forms['serviceForm1'].elements['accountLineStatus'].value='allStatus';
	 document.forms['serviceForm1'].action = 'quotationAccountLineList.html';
    document.forms['serviceForm1'].submit();
	

}

function findDefaultLine()
{
	var jobtypeSO=document.forms['serviceForm1'].elements['jobtypeSO'].value;
	var routingSO=document.forms['serviceForm1'].elements['routingSO'].value;
	var modeSO=document.forms['serviceForm1'].elements['modeSO'].value;
	var customerFileContract=document.forms['serviceForm1'].elements['customerFileContract'].value;
	if(customerFileContract=='')
	 { 
	 		alert("Please select  Pricing Contract from Quotation File"); 
	 }
	else
	{
	  document.forms['serviceForm1'].action = 'findDefaultLine.html?btntype=yes';
      document.forms['serviceForm1'].submit();
	}
	

}
function defaultLineMassage()
{
alert("Default accounting template have already been added.");

}
</script> 

   
</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>	 
<s:hidden name="fileID"  id= "fileID" value="%{serviceOrder.id}"/>  
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="serviceForm1" action="updateQuotationSOfromaccountLines" method="post">
<div id="layer1" style="width:100%"> 
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
 <s:hidden name="accountLineStatus"/>
 <s:hidden name="customerFileContract" value="${customerFile.contract }"/>
<div id="newmnav" >
  <ul>
	  <li><a href="editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
	  <li id="newmnav1" style="background:#FFF "><a href="quotationAccountLineList.html?sid=${serviceOrder.id}" class="current" ><span>Accounting<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
	  <li><a href="editRemoteQuotationFile.html?id=${serviceOrder.id}" ><span>Quotation File</span></a></li>
	  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quote&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
	   
</div><div class="spn"  style="width:100%">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="0" border="0"  style="width:90%">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0"  style="width:100%">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   size="21"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="4" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="6" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="4" readonly="true"/></td><td align="right">&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="4" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="4" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="6" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="26" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<c:if test="${accountLineStatus=='true'}">
  <div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a onclick="openActiveList();" class="current"><span>Active List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   
		    <li><a onclick="openAllList();"><span>All List</span></a></li>
  </ul>
		</div><div class="spn" style="width:100%">&nbsp;</div>
</c:if>
<%--<c:if test="${accountLineStatus=='false'}">
  <div id="newmnav">
		  <ul>
		    <li><a onclick="openActiveList();" ><span>Active List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a onclick="openInactiveList();" class="current"><span>Inactive List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="openAllList();"><span>All List</span></a></li>
  </ul>
		</div><div class="spn">&nbsp;</div>
</c:if>
--%>
<c:if test="${accountLineStatus=='allStatus'}">
  <div id="newmnav">
		  <ul>
		    <li><a onclick="openActiveList();" ><span>Active List</span></a></li>
		    
		    <li id="newmnav1" style="background:#FFF"><a onclick="openAllList();" class="current"><span>All List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
		</div><div class="spn" style="width:100%">&nbsp;</div>
</c:if>
  
  
  <c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
  
  <c:set var="buttons">  
     
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  onclick="location.href='<c:url value="/editQuotaionAccountLine.html?sid=${ServiceOrderID}"/>'" value="<fmt:message key="button.add"/>"/>   
	
	<!--  
		<c:if test="${billingFlag==1}"> </c:if>
	    <c:if test="${billingFlag==0}"> 
	    	<input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="showMessage();" value="<fmt:message key="button.add"/>"/>   
		</c:if>
	-->
	
</c:set>   
  
  
  <fmt:setLocale value="en-US" />
 
<s:set name="accountLineList" value="accountLineList" scope="request"/>

<display:table name="accountLineList" class="table" requestURI="" id="accountLineListRaj" style="width:99%;margin-top:1px;margin-left:5px;" defaultsort="1" pagesize="10" >   
		<display:column property="accountLineNumber" sortable="true" titleKey="accountLine.accountLineNumber"  style="width:20px" href="editQuotaionAccountLine.html?sid=${ServiceOrderID}" paramId="id" paramProperty="id"/> 
		 <display:column  sortable="true" titleKey="accountLine.status"   style="width:30px"> 
		 <c:if test="${accountLineListRaj.status}">
		  <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
		 <c:if test="${accountLineListRaj.status==false}">
		<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
		 </display:column>
		 <display:column property="category" sortable="true" titleKey="accountLine.category" maxLength="7"  style="width:40px"/> 
		 <display:column property="chargeCode" sortable="true" title="Charge &nbsp;Code" style="width:50px" />
		 <display:column property="estimateVendorName" sortable="true" title="Vendor &nbsp;Name" maxLength="7"  style="width:60px"/>
	     <display:column sortable="true"  title="Entitled &nbsp;Amount" style="width:70px">
	       <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineListRaj.entitlementAmount}" /></div>
        </display:column>
	    <display:column   sortable="true"  title="Estimate &nbsp;Expense" style="width:80px">
	        <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineListRaj.estimateExpense}" /></div>
        </display:column>
	    <display:column   sortable="true"  title="Estimate &nbsp;Revenue" style="width:80px">
		  <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0" 
                  groupingUsed="true" value="${accountLineListRaj.estimateRevenueAmount}" /></div>
        </display:column>
		<%-- <display:column   sortable="true"  title="Revision &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expense" style="width:90px">
		  <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineListRaj.revisionExpense}" /></div>
        </display:column>
		<display:column  sortable="true"  title="Revision &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revenue" style="width:90px">
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineListRaj.revisionRevenueAmount}" /></div>
        </display:column>
		<display:column sortable="true"  title="Actual &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expense" style="width:90px">
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
            groupingUsed="true" value="${accountLineListRaj.actualExpense}" /></div>
        </display:column>
		<display:column  sortable="true"   title="Actual &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revenue" style="width:90px">
            <div align="right"><fmt:formatNumber type="number"  
            value="${accountLineListRaj.actualRevenue}"  maxFractionDigits="0" groupingUsed="true"/>
           </div>
       </display:column>
       <display:column property="recInvoiceNumber" sortable="true" titleKey="accountLine.recInvoiceNumber"  style="width:50px" />
       <display:column property="billToName" sortable="true" title="Bill &nbsp;To" maxLength="6" style="width:70px" />
       --%>
       <display:footer>
<tr>
		         
		          
 	  	          <td align="right" colspan="5"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
		          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.entitledTotalAmount}" /></div></td>
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.estimatedTotalExpense}" /></div></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.estimatedTotalRevenue}" /></div></td>
		  	      <%-- <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.revisedTotalExpense}" /></div></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.revisedTotalRevenue}" /></div></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.actualExpense}" /></div></td>
		  	      <td align="center" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.actualRevenue}" /></div></td>
		  	      <td colspan="2"></td> --%>
		  	   
		  	</tr>
		  	<tr>
		  	    <td align="right" colspan="5"><b><div align="right">Gross&nbsp;Margin</div></b></td>
		  	    <td></td><td></td> 
		  	    <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.estimatedGrossMargin}" /></div></td>
		  	    <%-- <td></td>
		  	    <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.revisedGrossMargin}" /></div></td>
   	  	        <td></td> 
		  	    <td align="center"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.actualGrossMargin}" /></div></td>
                  <td colspan="2"></td> --%>
		  	</tr>
		  	<tr>
		  		<td align="right" colspan="5"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
		  	    <td></td> 
 	  	        <td></td>
		  	    <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.estimatedGrossMarginPercentage}" /></div></td>
  				<%-- <td></td> 
		  	    <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.revisedGrossMarginPercentage}" /></div></td>
 		  	  	<td></td>
		  	    <td align="center"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.actualGrossMarginPercentage}" /></div></td>
                  <td colspan="2"></td> --%>
		  	</tr>
</display:footer>
       
</display:table> 
<%-- 
     <table class="mainDetailTable" cellspacing="1" style="width:900px;" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" style="width:920px;">
		  <tbody>  
		    <tr><td align="left" height="2px" width="260px">&nbsp;</td></tr>
		      <tr>
		         
		          <td align="right"><h5><fmt:message key="serviceOrder.entitledTotalAmounted"/></h5></td>
 	  	          <td align="right" width="70px">
 	  	          	<input type="text" style="text-align:right" name="soentitledTotalAmount" size="9" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.entitledTotalAmount}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
 	  	          <td align="right" width="70px"><input type="text" style="text-align:right" name="soestimatedTotalExpense" size="10" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.estimatedTotalExpense}" minFractionDigits="0" maxFractionDigits="0" type="number" />' readonly="true"/></td>
		  	      <td align="right" width="70px"><input type="text" style="text-align:right" name="soestimatedTotalRevenue"  size="11" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.estimatedTotalRevenue}" minFractionDigits="0" maxFractionDigits="0" type="number"/>'  readonly="true"/></td>
		  	      <td align="right" width="70px"><input type="text" style="text-align:right" name="sorevisedTotalExpense" size="11" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.revisedTotalExpense}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	      <td align="right" width="70px"><input type="text" style="text-align:right" name="sorevisedTotalRevenue" size="11" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.revisedTotalRevenue}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	      <td align="right" width="70px"><input type="text" style="text-align:right" name="soactualExpense" size="11" maxlength="15" class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.actualExpense}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	      <td align="center" width="70px"><input type="text" style="text-align:right" name="soactualRevenue" size="11" maxlength="15"  class="input-textUpper" value='<fmt:formatNumber value="${serviceOrder.actualRevenue}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	      
		  	    <td width="60px"></td>
		  	</tr>
		  	<tr><td colspan="6" height="2"></td></tr>
		  	<tr>
		  	    <td align="right"><h5><fmt:message key="serviceOrder.estimatedGrossMargined"/></h5></td>
		  	    <td></td> 
  	  	        <td></td> 
		  	    <td align="right"><input type="text" name="soestimatedGrossMargin" style="text-align:right" class="input-textUpper" size="11" maxlength="15" value='<fmt:formatNumber value="${serviceOrder.estimatedGrossMargin}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	    <td></td>
		  	    <td align="right"><input type="text" name="sorevisedGrossMargin" style="text-align:right" class="input-textUpper" size="11" maxlength="15" value='<fmt:formatNumber value="${serviceOrder.revisedGrossMargin}" minFractionDigits="0" maxFractionDigits="0" type="number"/>'  readonly="true"/></td>
   	  	        <td></td> 
		  	    <td align="center"><input type="text" name="soactualGrossMargin" style="text-align:right" class="input-textUpper" size="11" maxlength="15" value='<fmt:formatNumber value="${serviceOrder.actualGrossMargin}" minFractionDigits="0" maxFractionDigits="0" type="number"/>' readonly="true"/></td>
		  	</tr>
		  	<tr><td colspan="6" height="2"></td></tr>
		  	<tr>
		  	    <td align="right" style="width:180px"><h5><fmt:message key="serviceOrder.estimatedGrossMarginPercentageed"/></h5></td>
		  	    <td></td> 
 	  	        <td></td>
		  	    <td align="right"><input type="text" name="soestimatedGrossMarginPercentage" style="text-align:right" class="input-textUpper" size="11" maxlength="15" value='<fmt:formatNumber value="${serviceOrder.estimatedGrossMarginPercentage}" minFractionDigits="0" maxFractionDigits="2" type="number"/>' readonly="true"/></td>
  				<td></td> 
		  	    <td align="right"><input type="text" name="sorevisedGrossMarginPercentage" style="text-align:right" class="input-textUpper"   size="11"   maxlength="15"  value="${serviceOrder.revisedGrossMarginPercentage}"  readonly="true"/></td>
 		  	  	<td></td>
		  	    <td align="center"><input type="text" name="soactualGrossMarginPercentage" style="text-align:right" class="input-textUpper"  size="11"    maxlength="15" value="${serviceOrder.actualGrossMarginPercentage}"  readonly="true"/></td>
		  	</tr>
		  	
		  	
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

--%>

<%-- <input type="submit" class="cssbutton1" style="width:55px; height:25px" value="Calc" onclick="onLoad();" />--%>
<s:hidden name="serviceOrder.entitledTotalAmount" />
<s:hidden name="serviceOrder.estimatedTotalExpense" />
<s:hidden name="serviceOrder.estimatedTotalRevenue" />
<s:hidden name="serviceOrder.revisedTotalExpense" />
<s:hidden name="serviceOrder.revisedTotalRevenue" />
<s:hidden name="serviceOrder.actualExpense" />
<s:hidden name="serviceOrder.actualRevenue" />
<s:hidden name="serviceOrder.estimatedGrossMargin" />
<s:hidden name="serviceOrder.revisedGrossMargin" />
<s:hidden name="serviceOrder.actualGrossMargin" />
<s:hidden name="serviceOrder.estimatedGrossMarginPercentage" />
<s:hidden name="serviceOrder.revisedGrossMarginPercentage" />
<s:hidden name="serviceOrder.actualGrossMarginPercentage" />
		<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
		<s:hidden name="serviceOrder.projectedGrossMargin" />

    <s:hidden name="entitledTotal" />
    <s:hidden name="estimateRevenueAmt" />
    <s:hidden name="estimateExp" />
    <s:hidden name="grossMrgn" />
    <s:hidden name="gmPercent" />
    <s:hidden name="revisedTotalRev"/>
    <s:hidden name="revisedTotal"/>
    <s:hidden name="revisedGrossMar"/>
    <s:hidden name="revisedGrossMarginPer"/>
    <s:hidden name="actualExpenseTotal" />
    <s:hidden name="actualRevenueTotal" />
    <s:hidden name="actualGrossMar" />
    <s:hidden name="actualGrossMarginPer" />
    <s:hidden name="jobtypeSO" value="${serviceOrder.job}"/>
    <s:hidden name="routingSO" value="${serviceOrder.routing}" />
    <s:hidden name="modeSO" value="${serviceOrder.mode}"/>
    <s:hidden name="quotationContract" />
    <s:hidden name="quotationBillToCode" />
    
    <s:hidden name="buttonType" />
    <c:out value="${buttons}" escapeXml="false" />
    <s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
    <c:if test="${serviceOrder.defaultAccountLineStatus=='false'}">
         <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="findDefaultLine();" 	value="Add Default Template" />
	</c:if>
	<c:if test="${serviceOrder.defaultAccountLineStatus=='true'}">
         <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="defaultLineMassage()" value="Add Default Template" />
	</c:if>
	<%-- 
		<input type="button" class="cssbuttonA" style="width:110px; height:25px" value="Generate Invoice" onclick="findBookingAgentName();"/>
			
		<input type="text" name="totalexp" value='<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="<%=session.getAttribute("estimateExp") %>" />'/>
	                  
	    <%=session.getAttribute("estimateExp") %>	
	--%>
<c:if test="${empty sid}">
	<c:redirect url="/quotationAccountLineList.html?sid=${serviceOrder.id}"></c:redirect>
</c:if>
<c:if test="${btntype=='yes'}"> 
<c:redirect url="/quotationAccountLineList.html?sid=${serviceOrder.id}"></c:redirect>
</c:if>
</div>
</s:form>
<script type="text/javascript">   
    highlightTableRows("accountLineListRaj");  
    Form.focusFirstElement($("serviceForm1")); 
</script>  
		  	