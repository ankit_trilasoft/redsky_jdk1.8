package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TruckingOperations;

public interface TruckingOperationsManager extends GenericManager<TruckingOperations, Long> { 
	public List workTicketTruck(Long ticket,String corpID);
	public List findWorkTicketTruck(String localTruckNumber,String corpID);
	public int updateDeleteStatus(Long id);
	public void updateWorkTicketTrucks(Long ticket, int trucks,String TargetActual,boolean workticketQueue,String resourceValue, String sessionCorpID);
	public String findSurveyTool(String corpID, String job, String companyDivision);
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber);
}