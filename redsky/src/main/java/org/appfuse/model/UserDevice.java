package org.appfuse.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "userdevice")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class UserDevice extends BaseObject {

	private Long id;
	private String userName;
	private String deviceId;
	private Date createdOn;
	private Long userLogFileId;
	private String corpID;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("id", id)
			.append("userName", userName)
			.append("deviceId", deviceId)
			.append("createdOn", createdOn)
			.append("userLogFileId", userLogFileId)
			.append("corpID", corpID)
			.toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof User))
			return false;
		UserDevice castOther = (UserDevice) other;
		return new EqualsBuilder().append(id, castOther.id)
			.append(userName, castOther.userName)
			.append(deviceId, castOther.deviceId)
			.append(createdOn, castOther.createdOn)
			.append(userLogFileId, castOther.userLogFileId)
			.append(corpID, castOther.corpID)
			.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(id).append(userName)
			.append(deviceId).append(createdOn)
			.append(userLogFileId).append(corpID)
			.toHashCode();
	}

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}	

	public Long getUserLogFileId() {
		return userLogFileId;
	}

	public void setUserLogFileId(Long userLogFileId) {
		this.userLogFileId = userLogFileId;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

}
