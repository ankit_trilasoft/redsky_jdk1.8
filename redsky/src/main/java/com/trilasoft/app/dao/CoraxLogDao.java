package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CoraxLog;


public interface CoraxLogDao extends GenericDao<CoraxLog, Long> {
	public List getByCorpID( String sessionCorpID);
	

}
