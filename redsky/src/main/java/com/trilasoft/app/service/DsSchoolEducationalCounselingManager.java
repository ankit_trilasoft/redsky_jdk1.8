package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsSchoolEducationalCounseling;

public interface DsSchoolEducationalCounselingManager extends GenericManager<DsSchoolEducationalCounseling, Long>{
	List getListById(Long id);
}
