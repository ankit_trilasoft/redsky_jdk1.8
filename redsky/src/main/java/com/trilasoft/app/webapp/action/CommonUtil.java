/**
 * @Class Name	CommonUtil.java
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Date        08-Aug-2012
 */

package com.trilasoft.app.webapp.action;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

// Code Commented by Ankit for Java 8 Upgrade
// import com.sun.org.apache.regexp.internal.recompile;

/**
 * @author  <a>Ashis Mohanty</a>
 */
public class CommonUtil {
	
	/**
	 * This Method is used to get all Manager reference of a particular action.<p>
	 */
	public static Object getAllSOActionManager(Object objAction,String parameter,HttpServletRequest request){
		ServletContext context = request.getSession().getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		return objAction = (Object)ctx.getBean(parameter);
	}
	
	public static String removeSpecialChar(String input){
		return input.replaceAll("[~` !$'%&*+;<>=#.|/^:,_@()?-]","");
	}
	
	public static String getRegistrationNum(String regs,String searchType){
		if((regs !=null) && (!"".equals(regs))){
			String temp ="";
			StringBuffer sb;
			for (int i = 0; i < regs.length(); i++) {
				char ch = regs.charAt(i);
				boolean flag = Character.isLetter(regs.charAt(i));
				if (flag) {
					temp = temp + "%"+ch+"%";
				}else{
					temp = temp+ch;
				}
			}
			temp = temp.replaceAll("%%", "").replaceAll("-", "%-%");
			temp = temp.replaceAll("-%%", "-%").replaceAll("%%-", "%-");
			sb = new StringBuffer(temp);
			if ("%".equals(String.valueOf(sb.charAt(0)))) {
				sb = sb.deleteCharAt(0);
			}
			if ("%".equals(String.valueOf(sb.charAt(sb.length()-1)))) {
				sb = sb.deleteCharAt(sb.length()-1);
			}
			temp = sb.toString();
			
			if("Start With".equals(searchType)){
				temp = "%"+temp;
			}else if("End With".equals(searchType)){
				temp = temp+"%";
				return temp;
			}
			return "%"+temp+"%";
		}else{
			return "";
		}
	}
	
	public static Date getDateFormat(String format,String date){
		if(format == null || "".equals(format) || date == null || "".equals(date)){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String nullCheck(String obj){
		if (obj != null) {
			return obj.trim();
		}
		return "";
	}
	
	//Comment Method for Bugzilla ticket #14274
	/*public static boolean deleteDir(File dir) {
        try {
			if (dir.isDirectory()) {
				File[] files= dir.listFiles();
			    for (File file : files) {
			    	file.delete();
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return false;
	 }*/
	
	
	//Comment Method for Bugzilla ticket #14274
   /* public static void deleteFilesOlderThanNdays(int daysBack, String dirWay) {  
    	try{ 
	        File directory = new File(dirWay);  
	        if(directory.exists()){  
	  
	            File[] listFiles = directory.listFiles();              
	            long purgeTime = System.currentTimeMillis() - (daysBack * 24 * 60 * 60 * 1000);  
	            for(File listFile : listFiles) {  
	                if(listFile.lastModified() < purgeTime) {  
	                    if(!listFile.delete()) {  
	                        System.err.println("Unable to delete file: " + listFile);  
	                    }  
	                }  
	            }  
	        }
        }catch(Exception e){e.getStackTrace();}
    }*/
    private static Date tryToParse(String input, SimpleDateFormat format) {
        Date date  = null;
        try {
            date = format.parse(input);
        } catch (ParseException e) {

        }

        return date;
    }
        public static List<SimpleDateFormat> dateUtils() {
        	 List<SimpleDateFormat> dateFormats = new ArrayList<SimpleDateFormat>();
        	 dateFormats.add(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss Z yyyy"));
     		dateFormats.add(new SimpleDateFormat("E MMM dd hh:mm:ss Z yyyy"));
    		dateFormats.add(new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy"));
    		dateFormats.add( new SimpleDateFormat("yyyy-MM-dd"));
    		dateFormats.add( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S"));
    		dateFormats.add( new SimpleDateFormat("dd-MMM-yy"));
    		dateFormats.add( new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));
    		return dateFormats;
        }
        public static Date multiParse(String input)  {
            Date date = null;
            for (SimpleDateFormat format : dateUtils()) {
                date = tryToParse(input, format);
                if (date != null) break;
            }
            return date;
        }
        public static String multiFormat(Date input,String format)  {
        	SimpleDateFormat dateFormatObj = new SimpleDateFormat(format);
        	StringBuilder date = new StringBuilder(dateFormatObj.format(input));
            return date.toString();
        }   
    	public static boolean isInteger(String str) {
    	    return str.matches("^-?[0-9]+(\\.[0-9]+)?$");
    	}
		 public static Map<String,String> sortByKey(Map<String,String> map){
			 Map<String,String> mapLink=new LinkedHashMap<String,String>();
			 SortedSet<Map.Entry<String, String>> sortedset = new TreeSet<Map.Entry<String, String>>(
			            new Comparator<Map.Entry<String, String>>() {
			                public int compare(Map.Entry<String, String> e1,Map.Entry<String, String> e2) {	
			                	if(isInteger(e1.getKey())){
			                		return Integer.parseInt(e1.getKey())-(Integer.parseInt(e2.getKey()));	
			                	}else{
			                		return e1.getKey().compareToIgnoreCase(e2.getKey());	
			                	}
			                }
			            });
	
			 sortedset.addAll(map.entrySet());
			 for(Map.Entry<String, String> entry : sortedset)
			    {
				 mapLink.put(entry.getKey(), entry.getValue());
			    }
			    return mapLink;
		 }
	
		public static String getOriginZip(String regs,String searchType){
			if((regs !=null) && (!"".equals(regs))){
				String temp ="";
				StringBuffer sb;
				for (int i = 0; i < regs.length(); i++) {
					char ch = regs.charAt(i);
					boolean flag = Character.isLetter(regs.charAt(i));
					if (flag) {
						temp = temp + "%"+ch+"%";
					}else{
						temp = temp+ch;
					}
				}
				temp = temp.replaceAll("%%", "").replaceAll("-", "%%").replaceAll(" ", "%%");			
				temp = temp.replaceAll("-%%", "-%").replaceAll("%%-", "%-");
				sb = new StringBuffer(temp);
				if ("%".equals(String.valueOf(sb.charAt(0)))) {
					sb = sb.deleteCharAt(0);
				}
				if ("%".equals(String.valueOf(sb.charAt(sb.length()-1)))) {
					sb = sb.deleteCharAt(sb.length()-1);
				}
				temp = sb.toString();
				
				if("Start With".equals(searchType)){
					temp = temp+"%";
				}else if("End With".equals(searchType)){
					temp = "%"+temp;
					return temp;
				}else if("Exact Match".equals(searchType)){
					temp = regs;
					return temp;
				}
				return temp;
			}else{
				return "";
			}
		}
		
		public static String getDestinationZip(String regs,String searchType){
			if((regs !=null) && (!"".equals(regs))){
				String temp ="";
				StringBuffer sb;
				for (int i = 0; i < regs.length(); i++) {
					char ch = regs.charAt(i);
					boolean flag = Character.isLetter(regs.charAt(i));
					if (flag) {
						temp = temp + "%"+ch+"%";
					}else{
						temp = temp+ch;
					}
				}
				temp = temp.replaceAll("%%", "").replaceAll("-", "%%").replaceAll(" ", "%%");
				temp = temp.replaceAll("-%%", "-%").replaceAll("%%-", "%-");
				sb = new StringBuffer(temp);
				if ("%".equals(String.valueOf(sb.charAt(0)))) {
					sb = sb.deleteCharAt(0);
				}
				if ("%".equals(String.valueOf(sb.charAt(sb.length()-1)))) {
					sb = sb.deleteCharAt(sb.length()-1);
				}
				temp = sb.toString();
				
				if("Start With".equals(searchType)){
					temp = temp+"%";
				}else if("End With".equals(searchType)){
					temp = "%"+temp;
					return temp;
				}else if("Exact Match".equals(searchType)){
					temp = regs;
					return temp;
				}
				return temp;
			}else{
				return "";
			}
		}

}
