package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RefSurveyEmailSignatureDao;
import com.trilasoft.app.model.RefSurveyEmailSignature;

public class RefSurveyEmailSignatureDaoHibernate extends GenericDaoHibernate<RefSurveyEmailSignature, Long> implements RefSurveyEmailSignatureDao{
public RefSurveyEmailSignatureDaoHibernate()
{
	super(RefSurveyEmailSignature.class);
}
}
