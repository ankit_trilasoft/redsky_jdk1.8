package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsHomeRentalDao;
import com.trilasoft.app.model.DsHomeRental;

public class DsHomeRentalDaoHibernate extends GenericDaoHibernate<DsHomeRental,Long> implements DsHomeRentalDao {

	public DsHomeRentalDaoHibernate() {
		super(DsHomeRental.class);
		
	}

	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsHomeRental where id = "+id+" ");
	}
}
