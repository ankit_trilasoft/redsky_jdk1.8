package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InventoryDataDao;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.WorkTicket;


public class InventoryDataManagerImpl extends GenericManagerImpl<InventoryData, Long> implements InventoryDataManager {
	InventoryDataDao inventoryDataDao;
	public InventoryDataManagerImpl(InventoryDataDao inventoryDataDao) {
		super(inventoryDataDao);
		this.inventoryDataDao=inventoryDataDao;
	}
	public List getListByType(String type,Long id) {
		return inventoryDataDao.getListByType(type, id);
	}
	public List getSumOfListByType(String type,Long id,Long soid) {
		return inventoryDataDao.getSumOfListByType(type, id,soid);
	}
	public String updateEstimatedVolumeCft(int volumeAir,int volumeSea,int volumeRoad,int volumeStorage,Long cid){
		return inventoryDataDao.updateEstimatedVolumeCft(volumeAir, volumeSea, volumeRoad, volumeStorage, cid);
	}
	public List getInventoryListForSO(Long id,Long cid){
		return inventoryDataDao.getInventoryListForSO(id, cid);
	}
	 public Boolean getCountForSameModeTypeSO(Long cid){
		 return inventoryDataDao.getCountForSameModeTypeSO(cid);
	 }
	 public List getSOListbyCF(Long cid){
		 return inventoryDataDao.getSOListbyCF(cid);
	 }
	 public List getDistinctMode(Long cid){
		 return inventoryDataDao.getDistinctMode(cid);
	 }
	 public int countByIndividualMode(Long cid,String mode){
		 return inventoryDataDao.countByIndividualMode(cid, mode);
	 }
	  public void InventorySOMapping(Long cid,String modeSORelation,String modeWithOneSO ,String sessionCorpID, String userName){
		  inventoryDataDao.InventorySOMapping(cid, modeSORelation, modeWithOneSO,sessionCorpID, userName);
	  }
	   public void updateOtherModeList(Long cid,String otherModeItem){
		   inventoryDataDao.updateOtherModeList(cid, otherModeItem);
	   }
	   public int workTicketCount(Long id,String sessionCorpID){
		   return inventoryDataDao.workTicketCount(id, sessionCorpID);
	   }
	   public void tranferInventoryMaterial(Long id,Long cid,String sessionCorpID){
		   inventoryDataDao.tranferInventoryMaterial(id, cid, sessionCorpID);
	   }
	   public List getWorkTicket(Long id,String sessionCorpID){
		   return inventoryDataDao.getWorkTicket(id, sessionCorpID);
	   }
	   public List getInventoryListByType(Long id,Long cid,String type){
		   return inventoryDataDao.getInventoryListByType(id, cid, type);
	   }
	   public List getWorkTicketList(Long id,String sessionCorpID){
		   return inventoryDataDao.getWorkTicketList(id, sessionCorpID);
	   }
	   public void setOtherModeInventorySOid(Long cid,String otherModeItemList){
		   inventoryDataDao.setOtherModeInventorySOid(cid, otherModeItemList);
	   }
	   public List getInventoryListForSOByType(Long id,Long cid,String type){
		   return inventoryDataDao.getInventoryListForSOByType(id, cid, type);
	   }
	   public String getSOTransferStatus(String type,Long cid,String sessionCorpID){
		   return inventoryDataDao.getSOTransferStatus(type, cid, sessionCorpID);
	   }
	   public List getDistinctArticleInventoryListByType(Long id,Long cid,String type){
		   return inventoryDataDao.getDistinctArticleInventoryListByType(id, cid, type);
	   }
	   public void updateWeghtAndVol(Long cid, Long sid,String sessionCorpID){
		   inventoryDataDao.updateWeghtAndVol(cid, sid, sessionCorpID);
	   }
	   public String findServiceOrderId(String tempOrgId, String tempEmfId){
		   return inventoryDataDao.findServiceOrderId(tempOrgId, tempEmfId);
	   }
	   public List getItemList(Long sid,String sessionCorpID){
		   return inventoryDataDao.getItemList(sid,sessionCorpID);
	   }
		public List getVehicle(Long sid,String sessionCorpID){
			return inventoryDataDao.getVehicle(sid,sessionCorpID);
		}
		public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID){
			inventoryDataDao.updateInsurance( newField,fieldValue, id, sid, cid, sessionCorpID);
		}
		public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID){
			inventoryDataDao.updateVehicle(newField,fieldValue, id, sid, vehicleIdNumber, sessionCorpID);
		}
		public AccessInfo getAccessinfo(Long cid, String sessionCorpID ,Long sid){
			return inventoryDataDao.getAccessinfo(cid, sessionCorpID,  sid);
		}
		public WorkTicket getWorkTicketByTicket(String ticket, String sessionCorpID){
			return inventoryDataDao.getWorkTicketByTicket(ticket, sessionCorpID);
		}
		public List<Long> getWorkTicketIdList(String shipno, String sessionCorpID){
			return inventoryDataDao.getWorkTicketIdList(shipno, sessionCorpID);
		}
		public InventoryData getLinkedInventoryData(Long invId, Long cId, Long sId){
			return inventoryDataDao.getLinkedInventoryData(invId, cId, sId);
		}
		public void deleteInventoryData(Long sid, Long networkSynchedId) {
			inventoryDataDao.deleteInventoryData(sid, networkSynchedId);
			
		}
		public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid, Long cid) {
			inventoryDataDao.updateLinkedInsurance(newField, fieldValue, networkSynchedId, sid, cid);
			
		}
		public void updateLinkedVehicleData(String newField, String fieldValue, Long sid, String idNumber) {
			inventoryDataDao.updateLinkedVehicleData(newField, fieldValue, sid, idNumber);
			
		}
		public InventoryData getSyncInventoryData(Long cid, String corpID,Long networkSynchedId){
			return inventoryDataDao.getSyncInventoryData(cid, corpID, networkSynchedId);
		}
		public void removeLinkedInventories(Long id){
			inventoryDataDao.removeLinkedInventories(id);
		}
		public AccessInfo getAccessinfoByCid(Long id, String corpID){
			return inventoryDataDao.getAccessinfoByCid(id, corpID);
		}
		public AccessInfo getAccessinfoByNetworkSyncId(Long id, String corpID){
			return inventoryDataDao.getAccessinfoByNetworkSyncId(id, corpID);
		}
		 public List getInventoryItemForMss(Long sid, Long cid, String sessionCorpID){
			 return inventoryDataDao.getInventoryItemForMss(sid, cid, sessionCorpID);
		 }
}

