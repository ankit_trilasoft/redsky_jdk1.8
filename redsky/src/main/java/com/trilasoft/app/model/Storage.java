package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;   

import java.math.BigDecimal;
import java.util.*;
import javax.persistence.Entity;
import javax.persistence.GenerationType;   
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="storage")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Storage extends BaseObject/* implements Comparable<Storage>*/ {
	private Long id;
	private Long serviceOrderId;
	private Long idNum;
	private String shipNumber;
	private String containerId ;
	private String description;
	private String itemTag;
	private String jobNumber;
	private String locationId;
	private String corpID;
	private Integer pieces;
	private Integer originalPieces;
	private Double price;
	private Long toRelease;
	private String itemNumber;
	private Long ticket;
	private String serial;
	private String model;
	private String unit;
	private BigDecimal measQuantity;
	private BigDecimal originalMeasQuantity;
	private Date releaseDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private BigDecimal volume;
	private BigDecimal originalVolume;
	private String volUnit;
	private String storageId;
	private String handoutStatus;
	private Long hoTicket;
	private String warehouse;
	//private WorkTicket workTicket;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("idNum", idNum).append("shipNumber", shipNumber)
				.append("containerId", containerId)
				.append("description", description).append("itemTag", itemTag)
				.append("jobNumber", jobNumber)
				.append("locationId", locationId).append("corpID", corpID)
				.append("pieces", pieces).append("price", price)
				.append("toRelease", toRelease)
				.append("itemNumber", itemNumber).append("ticket", ticket)
				.append("serial", serial).append("model", model)
				.append("unit", unit).append("measQuantity", measQuantity)
				.append("releaseDate", releaseDate)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("volume", volume).append("volUnit", volUnit)
				.append("storageId", storageId)
				.append("handoutStatus", handoutStatus)
				.append("hoTicket", hoTicket)
				.append("warehouse", warehouse)
				.append("originalPieces", originalPieces)
				.append("originalMeasQuantity", originalMeasQuantity)
				.append("originalVolume", originalVolume).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Storage))
			return false;
		Storage castOther = (Storage) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(idNum, castOther.idNum)
				.append(shipNumber, castOther.shipNumber)
				.append(containerId, castOther.containerId)
				.append(description, castOther.description)
				.append(itemTag, castOther.itemTag)
				.append(jobNumber, castOther.jobNumber)
				.append(locationId, castOther.locationId)
				.append(corpID, castOther.corpID)
				.append(pieces, castOther.pieces)
				.append(price, castOther.price)
				.append(toRelease, castOther.toRelease)
				.append(itemNumber, castOther.itemNumber)
				.append(ticket, castOther.ticket)
				.append(serial, castOther.serial)
				.append(model, castOther.model).append(unit, castOther.unit)
				.append(measQuantity, castOther.measQuantity)
				.append(releaseDate, castOther.releaseDate)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(volume, castOther.volume)
				.append(volUnit, castOther.volUnit)
				.append(storageId, castOther.storageId)
				.append(handoutStatus, castOther.handoutStatus)
				.append(hoTicket, castOther.hoTicket).append(warehouse, castOther.warehouse)
				.append(originalPieces, castOther.originalPieces)
				.append(originalMeasQuantity, castOther.originalMeasQuantity)
				.append(originalVolume, castOther.originalVolume).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId)
				.append(idNum).append(shipNumber).append(containerId)
				.append(description).append(itemTag).append(jobNumber)
				.append(locationId).append(corpID).append(pieces).append(price)
				.append(toRelease).append(itemNumber).append(ticket)
				.append(serial).append(model).append(unit).append(measQuantity)
				.append(releaseDate).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(volume)
				.append(volUnit).append(storageId).append(handoutStatus)
				.append(hoTicket).append(warehouse)
				.append(originalPieces)
				.append(originalMeasQuantity)
				.append(originalVolume).toHashCode();
	}

	/*public int compareTo(final Storage other) {
		return new CompareToBuilder().append(id, other.id).append(
				serviceOrderId, other.serviceOrderId)
				.append(idNum, other.idNum)
				.append(shipNumber, other.shipNumber).append(containerId,
						other.containerId).append(description,
						other.description).append(itemTag, other.itemTag)
				.append(jobNumber, other.jobNumber).append(locationId,
						other.locationId).append(corpID, other.corpID).append(
						pieces, other.pieces).append(price, other.price)
				.append(toRelease, other.toRelease).append(itemNumber,
						other.itemNumber).append(ticket, other.ticket).append(
						serial, other.serial).append(model, other.model)
				.append(unit, other.unit).append(measQuantity,
						other.measQuantity).append(releaseDate,
						other.releaseDate).append(createdBy, other.createdBy)
				.append(updatedBy, other.updatedBy).append(createdOn,
						other.createdOn).append(updatedOn, other.updatedOn)
				.toComparison();
	}*/
	
	@Column(length=10)
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	@Column(length=1000)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public Long getIdNum() {
		return idNum;
	}
	public void setIdNum(Long idNum) {
		this.idNum = idNum;
	}
	
	
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	
	@Column(length=15)
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	@Column(length=10)
	public String getItemTag() {
		return itemTag;
	}
	public void setItemTag(String itemTag) {
		this.itemTag = itemTag;
	}
	
	@Column(length=15)
	public String getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	
	@Column(length=20)
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@Column
	public Integer getPieces() {
		return pieces;
	}
	public void setPieces(Integer pieces) {
		this.pieces = pieces;
	}
	@Column
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	//@Index(name = "releaseDate")
	@Column
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	@Column(length=20)
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column
	public Long getToRelease() {
		return toRelease;
	}
	public void setToRelease(Long toRelease) {
		this.toRelease = toRelease;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)  
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=40)
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Column(length=10)
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Column(length=20)  
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	
	public BigDecimal getMeasQuantity() {
		return measQuantity;
	}

	public void setMeasQuantity(BigDecimal measQuantity) {
		this.measQuantity = measQuantity;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	@Column
	public String getVolUnit() {
		return volUnit;
	}

	public void setVolUnit(String volUnit) {
		this.volUnit = volUnit;
	}
	
	@Column(length=65)
	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}
	@Column(length=1)
	public String getHandoutStatus() {
		return handoutStatus;
	}

	public void setHandoutStatus(String handoutStatus) {
		this.handoutStatus = handoutStatus;
	}

	public Long getHoTicket() {
		return hoTicket;
	}

	public void setHoTicket(Long hoTicket) {
		this.hoTicket = hoTicket;
	}
	@Column(length=20)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column
	public BigDecimal getOriginalVolume() {
		return originalVolume;
	}

	public void setOriginalVolume(BigDecimal originalVolume) {
		this.originalVolume = originalVolume;
	}
	@Column
	public Integer getOriginalPieces() {
		return originalPieces;
	}

	public void setOriginalPieces(Integer originalPieces) {
		this.originalPieces = originalPieces;
	}
	@Column
	public BigDecimal getOriginalMeasQuantity() {
		return originalMeasQuantity;
	}

	public void setOriginalMeasQuantity(BigDecimal originalMeasQuantity) {
		this.originalMeasQuantity = originalMeasQuantity;
	}

	
	
}