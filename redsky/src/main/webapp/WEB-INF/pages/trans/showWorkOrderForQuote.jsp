<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="operationResource.title"/></title>   
    <meta name="heading" content="<fmt:message key='O&I.heading' />"/>   
    <style> #ajax_tooltipObj .ajax_tooltip_content {background-color:#FFF !important;}.table thead th {height:13px;}</style>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	 <td align="left"><b>W/O List</b> <input type="button" class="skybtn" style="width:auto;padding:0px;font-size:9px;border-radius:1px;" value="Check All" onclick="checkAll(this)" />
 <input type="button" class="skybtn" style="width:auto;padding:0px;font-size:9px;border-radius:1px;"  value="Uncheck All" onclick="uncheckAll(this)" /> </td>
	<td align="right"  style="padding-left:3px;">
	<img  valign="top"align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="showDistinctWorkOrder" class="table" requestURI="" id="showDistinctWorkOrder" style="margin:0px;margin-bottom:2px">
	
	<display:column title=" " style="width:3px;"><input type="checkbox" id="checkboxId${showDistinctWorkOrder.id}" name="checkBoxWO" checked/></display:column>
		 <display:column  title="WO#"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${showDistinctWorkOrder.id}" id="workOrderOI${showDistinctWorkOrder.id}"  value="${showDistinctWorkOrder.workorder}" style="width:40px" disabled="true"/> 
    </display:column>
 </display:table> 

	
	 <c:if test="${showDistinctWorkOrder!='[]'}"> 
  <input type="button"  class="skybtn" align="center" style="font-size:11px;" id="okButton"  value="Download Quote for Work Order" onclick="downloadQuoteForOI();"/>
  <input type="button"  class="skybtn" align="center" style="font-size:11px;" id="okButtons"  value="Download Revised Quote" onclick="downloadRevsQuoteForOI();"/>
	</c:if>
	
	<c:if test="${showDistinctWorkOrder=='[]'}"> 
  <input type="button"  class="skybtn" align="center" style="font-size:11px;" id="okButton" readonly="readonly" disabled="true" value="Open WorkOrder" onclick="findAllResource('flag');"/>
	</c:if> 