<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<head>   
    <title>Document Bundle List</title>   
    <meta name="heading" content="Document Bundle List"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
 <style>
 span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
</style>

<script type="text/javascript">
function clear_fields(){
	document.getElementById('bundleName').value = '';
	document.getElementById('serviceType').value = '';
	document.getElementById('serviceMode').value = '';
	document.getElementById('job').value = '';
	document.getElementById('tableName').value = '';
	document.getElementById('fieldName').value = '';
	document.getElementById('routingType').value = '';
}

function getFieldList(targetElement) {
	var tableNames = targetElement.value;
	var url="findfieldListAudit.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse3;
    http2.send(null);
}

function handleHttpResponse3(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.getElementById('fieldName');
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.getElementById('fieldName').options[i].text = '';
						document.getElementById('fieldName').options[i].value = '';
					}else{
						document.getElementById('fieldName').options[i].text = res[i];
						document.getElementById('fieldName').options[i].value = res[i];
					}
					document.getElementById("fieldName").value = '${documentBundle.sendPointFieldName}';
				}
       }
 }
        
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject();
</script>

</head>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:60px; height:25px"  
        onclick="location.href='<c:url value="/editDocumentBundle.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:60px; height:25px;margin-right:2px;" align="top" key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="searchForm" name="searchForm" action="searchDocumentBundle.html" > 
<div id="layer1" style="width:100%"> 
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" style="width:99%;" border="0">
<thead>
<tr>
<th>Bundle Name</th>
<th>Service Type</th>
<th>Routing</th>
<th>Mode</th>
<th>Job</th>
<th>Send Point Table Name</th>
<th>Send Point Field Name</th>
</tr>
</thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <input type="text" id="bundleName" name="bundleName" class="input-text" size="35"/>
			</td>
			<td width="" align="left">
			    <s:select id="serviceType" name="serviceType" list="%{service}" headerKey=""	headerValue="" cssStyle="width:200px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			    <s:select id="routingType" name="routingType" list="%{routing}" headerKey=""	headerValue="" cssStyle="width:200px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			    <s:select id="serviceMode" name="serviceMode" list="%{mode}" cssStyle="width:90px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			   <s:select id="job" name="job" list="%{jobs}" cssClass="list-menu" cssStyle="width:150px" headerKey="" headerValue=""/>
			</td>
			<td width="" align="left">
			    <s:select name="tableName" id="tableName" list="%{tableList}" onchange="getFieldList(this)" cssStyle="width:120px;" cssClass="list-menu" headerKey="" headerValue=""/>
			</td>
			<td width="" align="left">
			    <s:select name="fieldName" id="fieldName" list="%{fieldList}" cssStyle="width:120px;" cssClass="list-menu" headerKey="" headerValue=""/>
			</td>
						
			</tr>
			<tr>
				<td width="" align="left" colspan="6"></td>	
							
				<td style="border-left:hidden;text-align:right;padding:0px;" class="listwhitetext">
				<table style="margin:0px;padding:0px;border:none; float:right;">
				<tr>
				
				<td style="border:none;vertical-align:bottom;">
				    <c:out value="${searchbuttons}" escapeXml="false" />   
				    </td>
				    </tr>
				    </table>
				</td>
			</tr>	
			
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>


<c:out value="${searchresults}" escapeXml="false" />  
		<div id="newmnav" style="margin-bottom:0px;!margin-bottom:-13px;">
		  
		  	<ul>
		    	<li id="newmnav1"><a class="current"><span>Setup</span></a></li>
		  	</ul>
		  	<ul>
		    	<li><a href="documentBundleWasteBasketList.html?tabId=wasteBasket" /><span>Waste Basket</span></a></li>
		  	</ul>
		  
		</div>
		<div class="spnblk">&nbsp;</div>
		
<display:table name="documentBundleList" class="table" requestURI="" id="documentBundleList" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >
	<display:column  style="width:5px;">	
		<img id="show-${documentBundleList.id}" onclick ="showChild('${documentBundleList.id}','child${documentBundleList.id}',this,this.id,'${documentBundleList.formsId}');" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		<img id="hide-${documentBundleList.id}" onclick ="hideChild('${documentBundleList.id}','${documentBundleList_rowNum}',this.id);" src="${pageContext.request.contextPath}/images/minus1-small.png" style="display: none;" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<display:column title="Bundle Name" style="width: 90px;">
		<a href="editDocumentBundle.html?id=${documentBundleList.id}"> ${documentBundleList.bundleName}</a>
	</display:column>
	<display:column title="Send Point" style="width: 90px;">
		<c:if test="${not empty documentBundleList.sendPointTableName && not empty documentBundleList.sendPointFieldName}">
			<c:out value="${documentBundleList.sendPointTableName}.${documentBundleList.sendPointFieldName}"></c:out>
		</c:if>
		<c:if test="${not empty documentBundleList.sendPointTableName && empty documentBundleList.sendPointFieldName}">
			<c:out value="${documentBundleList.sendPointTableName}."></c:out>
		</c:if>
		<c:if test="${empty documentBundleList.sendPointTableName && not empty documentBundleList.sendPointFieldName}">
			<c:out value=".${documentBundleList.sendPointFieldName}"></c:out>
		</c:if>
		<c:if test="${empty documentBundleList.sendPointTableName && empty documentBundleList.sendPointFieldName}">
			<c:out value=""></c:out>
		</c:if>
	</display:column>
	<display:column property="printSeq" title="Print Seq" style="width:50px;"></display:column>
	<display:column property="serviceType" title="Service" style="width:50px;"></display:column>
	<display:column property="routing" title="Routing" style="width:50px;"></display:column>
	<display:column property="job" title="Job Type" style="width:50px;"></display:column>
	<display:column property="mode" title="Mode" style="width:50px;"></display:column>
	<display:column title="Remove" style="width: 10px;">
		<a><img align="middle" onclick="confirmSubmit('${documentBundleList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	
</display:table>

<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</div>
</s:form>
<script type="text/javascript">
function confirmSubmit(documentId){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var url = 'moveDocumentBundleWasteBasket.html?&id='+documentId;
		location.href = url;
	 }else{
		return false;
	}
}

function showChild(id,divId,position,rowId,formsId){
	document.getElementById('show-'+id).style.display='none';
	document.getElementById('hide-'+id).style.display='block';
	
	var table=document.getElementById("documentBundleList");
	var rownum = document.getElementById(rowId);
	var myrow = rownum.parentNode;
	var newrow = myrow.parentNode.rowIndex;
	val = newrow+1;
	var row=table.insertRow(val);
	row.setAttribute("id","ch"+id);
	row.setAttribute("name",id);
	row.innerHTML = "<td colspan=\"10\"><div id="+id+"></div></td>";
	
	if(formsId!=null && !formsId==''){
	var url='documentBundleChildListAjax.html?ajax=1&reportsId='+formsId+'&decorator=simple&popup=true';
	new Ajax.Request(url,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			       var mydiv = document.getElementById(id);
			      mydiv.innerHTML = response;
			      mydiv.style.display='block';
			    },
			    onFailure: function(){
				    alert('Something went wrong...') 
				    }
			  });
	}else{
		  var mydiv = document.getElementById(id);
	      mydiv.innerHTML = 'Nothing Found To Display.';
	      mydiv.style.display='block';
	}
	     return 0;
}

function hideChild(id,rowNum,rowId){
	val = "ch"+id;
	var table = document.getElementById('documentBundleList');
    var rowCount = table.rows.length;
    for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        if(row != undefined && row != null && val == row.id){
        	table.deleteRow(i);
        	document.getElementById('show-'+id).style.display='block';
        	document.getElementById('hide-'+id).style.display='none';
        }
    }
}
</script>
