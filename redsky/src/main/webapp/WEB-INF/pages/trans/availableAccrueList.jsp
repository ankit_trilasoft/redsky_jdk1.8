<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <head>  
<title>Accrue Available List</title>   
 <meta name="heading" content="Accrue Available List"/> 
<style>
span.pagelinks { display:block;font-size:0.95em;margin-bottom:1px;!margin-bottom:2px;margin-top:-4px; padding:0px;text-align:right;width:100%; }
</style>
</head>
<div id="availableAccrueRows"> 
<s:form>
<s:hidden name="accrueCheckedId"  id= "accrueCheckedId" value=""/>
<s:set name="accrueAvailableList" value="accrueAvailableList" scope="request"/>
 <div id="newmnav" >
	<ul>
	<li><a><span>Accrue Available List</span></a></li>
	</ul>
</div>
<table cellspacing="0" cellpadding="0" style="margin:0px;float:left;">
<tr>
<td width="60"></td>
<td align="right" class="listwhitetext"><b>Check All</b></td>
<td>
	<input type="checkbox" id="selectall" onclick="selectAll(this);"/>
</td>
<td width="5"></td>
<td align="right" class="listwhitetext"><b>UnCheck All</b></td>
<td>
<input type="checkbox" id="unselectall" onclick="unSelectAll(this);"/>
</td>
<td width="7"></td>
<td>
<input type="button" class="cssbutton" style="width:110px; height:21px; border-radius:3px;"  name="Accrue Checked"  value="Accrue Checked" onclick="updateSoAccrueChecked()"/>
</td>
</tr>
</table>
<div class="spnblk">&nbsp;</div>  
 <display:table name="accrueAvailableList" class="table" id="accrueAvailableList" requestURI="" pagesize="10"> 
   	<display:column title="Order #" sortable="true">
   	<a href="pricingList.html?sid=${accrueAvailableList.sid}" target="_blank">
     <c:out value="${accrueAvailableList.shipNumber}" />
    </a> 
   	</display:column>
  	<display:column property="accrualReadyDate" title="Ready To Accrue&nbsp;Date" sortable="true"></display:column>
	<display:column property="packA" title="Pack Date" sortable="true"></display:column>
	<display:column property="coordinator" title="Move Manager" sortable="true"></display:column>
	<display:column property="billToCode" title="Corp Account" sortable="true"></display:column>
	<display:column property="billToName" title="Corp Account Name" sortable="true"></display:column>
	<display:column property="serviceType" title="Service" sortable="true"></display:column>
	<display:column property="mode" title="Mode" sortable="true"></display:column>
	<display:column property="actualNetWeight" headerClass="containeralign" style="text-align:right;" title="Net Weight" sortable="true"></display:column>
	<display:column property="actualGrossWeight" headerClass="containeralign" style="text-align:right;" title="Gross Weight" sortable="true"></display:column>
	<display:column property="accrueRevenue" headerClass="containeralign" style="text-align:right;" title="Accrue Revenue" sortable="true"></display:column>
	<display:column property="accrueProfit" headerClass="containeralign" style="text-align:right;" title="Accrue profit" sortable="true"></display:column>
	<c:if test="${accrueAvailableList!='[]'}">
		<c:choose> 
		<c:when test="${accrueAvailableList.minimumMargin > accrueAvailableList.marginPercentage}">
			<display:column headerClass="containeralign" style="text-align:right;color:#ff0000;" title="Margin %" sortable="true">
				<div align="right">
				 	<c:out value="${accrueAvailableList.marginPercentage}" />                
		     	</div>
			</display:column>
		</c:when>
		<c:when test="${accrueAvailableList.minimumMargin <= accrueAvailableList.marginPercentage}">
			<display:column headerClass="containeralign" style="text-align:right;" title="Margin %" sortable="true">
				<div align="right">
				 	<c:out value="${accrueAvailableList.marginPercentage}" />                
		     	</div>
			</display:column>
		</c:when>
		<c:otherwise></c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${accrueAvailableList=='[]'}">
	<display:column property="marginPercentage" headerClass="containeralign" style="text-align:right;" title="Margin %" sortable="true"></display:column>
	</c:if>
	<display:column property="minimumMargin" headerClass="containeralign" style="text-align:right;" title="Min. Margin&nbsp;%" sortable="true"></display:column>
	<display:column title="Accrue Checked" style="text-align:center; vertical-align:middle;">
	      	<input type="checkbox" name="accrueChecked" id="accrueChecked${accrueAvailableList.sid}" value="${accrueAvailableList.sid}" onclick="accrueCheck('${accrueAvailableList.sid}',this);"/>
	</display:column>
</display:table>
</s:form>
</div>
<script type="text/javascript">
function selectAll(target){
	var inputs = document.getElementsByName("accrueChecked");
	for(var i = 0; i < inputs.length; i++) {
		inputs[i].checked = target.checked; 
		var userCheckStatus = document.getElementById("accrueCheckedId").value;
		if(userCheckStatus == ''){
	    	 document.getElementById("accrueCheckedId").value = inputs[i].value;
	     }else{
	     	 userCheckStatus=	document.getElementById("accrueCheckedId").value = userCheckStatus + ',' + inputs[i].value;  
		}
	}
	document.getElementById("unselectall").checked=false;
}
function unSelectAll(target){
	var inputs = document.getElementsByName("accrueChecked");
	for(var i = 0; i < inputs.length; i++) {
		inputs[i].checked = false; 
		 document.getElementById("accrueCheckedId").value = '';
	}
	document.getElementById("selectall").checked=false;
}
function accrueCheck(rowId,targetElement){
	if(targetElement.checked==false){
     var userCheckStatus = document.getElementById("accrueCheckedId").value;
     if(userCheckStatus == ''){
    	 document.getElementById("accrueCheckedId").value = rowId;
     }else{
    	 var check = userCheckStatus.indexOf(rowId);
 		if(check > -1){
 			var values = userCheckStatus.split(",");
 		   for(var i = 0 ; i < values.length ; i++) {
 		      if(values[i]== rowId) {
 		    	 values.splice(i, 1);
 		    	 userCheckStatus = values.join(",");
 		        document.getElementById("accrueCheckedId").value = userCheckStatus;
 		      }
 		   }
 		}else{
       		userCheckStatus=	document.getElementById("accrueCheckedId").value = userCheckStatus + ',' + rowId;
      		document.getElementById("accrueCheckedId").value = userCheckStatus.replace( ',,' , ',' );
 		}
     }
   }
  if(targetElement.checked){
    var userCheckStatus = document.getElementById("accrueCheckedId").value;
	    if(userCheckStatus == ''){
	   	 	document.getElementById("accrueCheckedId").value = rowId;
	    }else{
	    	userCheckStatus = document.getElementById("accrueCheckedId").value = userCheckStatus + ',' + rowId;
	    	document.getElementById("accrueCheckedId").value = userCheckStatus.replace( ',,' , ',' );
	    }
    } 
}

function updateSoAccrueChecked(){
	var sid = document.getElementById("accrueCheckedId").value;
	new Ajax.Request('updateSoAccrueCheckedAjax.html?ajax=1&decorator=simple&popup=true&sidList='+sid,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			var availableAccrueDiv = document.getElementById("availableAccrueRows");
			availableAccrueDiv.innerHTML = response;
		},
		onFailure: function(){ 
		}
	});
}
</script>