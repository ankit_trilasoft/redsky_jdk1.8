<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script LANGUAGE="JavaScript"> 
function trackingstatus_tab() {
		 var url = 'editTrackingStatus.html?id=${serviceOrder.id}';
		 window.open(url);			
	}
function trackingstatus_Relojob() {
	 var url = 'editDspDetails.html?id=${serviceOrder.id}';
	 window.open(url);			
}
function workTicket_tab() {
	 var url = 'customerWorkTickets.html?id=${serviceOrder.id}';
	 window.open(url);			
}
	function notes_Origin() {
 	    <c:if test="${serviceOrder.shipNumber !=''}">
 	   var url = 'noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.originAgent';
		 window.open(url); 
		</c:if>
	    <c:if test="${serviceOrder.shipNumber ==''}">
	 	   var url = 'editNewNoteForServiceOrder.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&notesList=YES&NetwokAccess=notes.originAgent';
		window.open(url); 
		</c:if>
		}
	
	function notes_Destination() {
 	    <c:if test="${serviceOrder.shipNumber !=''}">
 	   var url = 'noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.destinationAgent';
		 window.open(url); 
		</c:if>
	    <c:if test="${serviceOrder.shipNumber ==''}">
	 	   var url = 'editNewNoteForServiceOrder.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&notesList=YES&NetwokAccess=notes.destinationAgent';
		window.open(url); 
		</c:if>
		}
	function forWording_tab_tab() {
	  var url = 'containersAjaxList.html?id=${serviceOrder.id}';
		 window.open(url);  
		}
	function AccountLine_tab() {
	  var url = 'accountLineList.html?sid=${serviceOrder.id}';
		 window.open(url);  
		}
	function task_tab() {
		  var url ='tasks.html?id=${serviceOrder.id}&tableName=ServiceOrder';
			 window.open(url);
			}
	function document_tab() {
		 var url ='myFilesDocType.html?id=${serviceOrder.id}&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false';
		 window.open(url); 
		}
	function redskyservice(id) {
		 if(id=='editServiceOrderUpdate')
			 {
			 var url ='editServiceOrderUpdate.html?id=${serviceOrder.id}';
			 window.open(url,"_self")
			 }
		 else if(id=='editBilling')
		 {
		 var url ='editBilling.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='accountLineList')
		 {
		 var url ='accountLineList.html?sid=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='containersAjaxList')
		 {
		 var url ='containersAjaxList.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='editTrackingStatus')
		 {
		 var url ='editTrackingStatus.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='customerWorkTickets')
		 {
		 var url ='customerWorkTickets.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='claims')
		 {
		 var url ='claims.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='editCustomerFile')
		 {
		 var url ='editCustomerFile.html?id=${customerFile.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='inventoryDataList')
		 {
		 var url ='inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='operationResource')
		 {
		 var url ='operationResource.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		 else if(id=='editMiscellaneous')
		 {
		 var url ='editMiscellaneous.html?id=${serviceOrder.id}';
		 window.open(url,"_self")
		 }
		}

</script>
