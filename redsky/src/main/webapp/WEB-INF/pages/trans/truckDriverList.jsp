<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Driver</title>   
    <meta name="heading" content="Driver"/>   
    
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			var i;
			for(i=0;i<=4;i++) {
					document.forms['searchForm'].elements[i].value = "";
			}
		}
		

function addDriverList(){
    var truckId = document.forms['searchForm'].elements['truckId'].value;
	var truckNumber = document.forms['searchForm'].elements['truckNumber'].value;
	var partnerCode = document.forms['searchForm'].elements['partnerCode'].value;
	var truckDescription = document.forms['searchForm'].elements['truckDescription'].value;
	var truckDriverType = document.forms['searchForm'].elements['truckDriverType'].value;
	
	var url="editDriverDetail.html?truckId=" + encodeURI(truckId) + "&truckNumber="+ encodeURI(truckNumber)+ "&partnerCode="+ encodeURI(partnerCode)+ "&truckDescription="+ encodeURI(truckDescription)+ "&truckDriverType="+ encodeURI(truckDriverType);
	location.href = url;
	
}  		
	
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this Driver?");	
	var truckId = document.forms['searchForm'].elements['truckId'].value;
	var truckNumber = document.forms['searchForm'].elements['truckNumber'].value;
	var partnerCode = document.forms['searchForm'].elements['partnerCode'].value;
	var truckDescription = document.forms['searchForm'].elements['truckDescription'].value;
	var truckDriverType = document.forms['searchForm'].elements['truckDriverType'].value;
	
	var tdid = targetElement;
	if (agree){
	    location.href="deleteTruckDriver.html?tdid="+tdid+"&truckNumber="+encodeURI(truckNumber)+"&partnerCode="+encodeURI(partnerCode)+"&truckId="+encodeURI(truckId)+"&truckDriverType="+encodeURI(truckDriverType)+"&truckDescription="+encodeURI(truckDescription);
	    }else{
		return false;
	}
}
		
	</script>
	
	<style>
	
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-17px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
font-size:.85em;
}
	</style>
</head>
<s:form id="searchForm" cssStyle="margin-top:0px !important;">
<s:hidden name="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<c:set var="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<s:hidden name="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<c:set var="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<s:hidden name="truckId" value="<%=request.getParameter("truckId") %>" />
<c:set var="truckId" value="<%=request.getParameter("truckId") %>" />

<s:hidden name="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<c:set var="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<s:hidden name="truckDescription" value="<%=request.getParameter("truckDescription") %>" />
<c:set var="truckDescription" value="<%=request.getParameter("truckDescription") %>" />


<c:set var="buttons">   
    <input type="button" value="Add" class="cssbuttonA" style="width:55px; height:25px" onclick="addDriverList();"/>   
</c:set>
</s:form>
<c:if test="${param.popup}">
<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Driver List</span></a></li>
		<li><a href="soAllDriversList.html?truckNumber=${truckNumber}&decorator=popup&popup=true&fld_sixthDescription=${param.fld_sixthDescription}&fld_fifthDescription=${param.fld_fifthDescription}&fld_fourthDescription=${param.fld_fourthDescription}&fld_thirdDescription=${param.fld_thirdDescription}&fld_secondDescription=${param.fld_secondDescription}&fld_description=${param.fld_description}&fld_code=${param.fld_code}"><span>Show All Drivers List</span></a></li>
	</ul>
</div>
<div class="spnblk" style="margin-top:45px;">&nbsp;</div>
<div  class="spnblk" style="margin-top:12px;"></div>
</c:if>

<c:if test="${empty param.popup}">
<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Driver List</span></a></li>
		<li><a href="editTruck.html?id=${truckId}"><span>Truck Details</span></a></li>
		<li><a href="truckList.html"><span>Truck List</span></a></li>
	</ul>
</div>
<div class="spn" >&nbsp;</div>
<div id="content" align="center" style="width: 100%;">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
   		<table style="margin-bottom: 3px">
			<tr>
				
				<td align="right" class="listwhitetext">Truck #</td>
				<td><s:textfield  value="${truckNumber}" cssClass="input-textUpper" readonly="true" size="40"/></td>
				<td width="25"></td>
				<td align="right" class="listwhitetext">Truck&nbsp;Description</td>
				<td><s:textfield value="${truckDescription}" cssClass="input-textUpper" readonly="true" size="40"/></td>
				
			</tr>
			<tr>
				  			
				<td align="right" class="listwhitetext">Truck&nbsp;Type</td>
				<td><s:textfield  value="${truckDriverType}" cssClass="input-textUpper" readonly="true" size="40"/></td>
				<td width="25"></td>
		  		<td align="right" class="listwhitetext">Owner/Pay To</td>
				<td><s:textfield value="${partnerCode}" cssClass="input-textUpper" readonly="true" size="40" /></td>
			</tr>
		</table>
	</div>
<div class="bottom-header" style="margin-top:45px;"><span></span></div>
</div>
</div>
</c:if>
<div id="Layer1" style="width: 100%;">  
<display:table name="truckDriverList" class="table" requestURI="" id="truckDriverList"  export="${empty param.popup}" pagesize="10" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>
    <c:if test="${empty param.popup}">
    <display:column property="driverCode" sortable="true" title="Code" href="editDriverDetail.html?truckId=${truckId}&truckNumber=${truckNumber}&partnerCode=${partnerCode}&truckDescription=${truckDescription}&truckDriverType=${truckDriverType}" paramId="id" paramProperty="id"/>
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" title="Code"/>
    </c:if>
    <display:column property="driverName" sortable="true" title="Name"/>
    <display:column  title="Primary">
		<c:if test="${truckDriverList.primaryDriver==true }">
		<img src="${pageContext.request.contextPath}/images/tick01.gif"  /> 
		</c:if>
		<c:if test="${truckDriverList.primaryDriver!=true }">
		<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
		</c:if>
		</display:column>
    <c:if test="${empty param.popup}">    
    <display:column title="Remove" style="width:45px;">
	<a><img align="middle" onclick="confirmSubmit(${truckDriverList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
    </c:if>
</display:table>   
  </tr>
  </tbody>
  </table>
</div>
<c:if test="${empty param.popup}"> 
  <c:out value="${buttons}" escapeXml="false" />
  </c:if>
<script type="text/javascript"> 
    <c:if test="${hitFlag == 1}">
    	<c:redirect url="/driverList.html?truckNumber=${truckNumber}&partnerCode=${partnerCode}&truckId=${truckId}&truckDriverType=${truckDriverType}&truckDescription=${truckDescription}" ></c:redirect>
    </c:if>
</script>  
