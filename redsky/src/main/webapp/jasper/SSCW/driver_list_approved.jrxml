<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="driver_list_approved"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
     "driver_list_approved.jrxml",
     CONCAT_WS(', ',crew.`lastName`,NULL,crew.`firstName`) AS Name,
     crew.`cdl` AS Cdl,
     Left(crew.`cdlExpires`,10) AS CdlExpires,
     Left(crew.`doTPhysical`,10) AS DoTPhysical,
     CONCAT_WS('   ',crew.`drivingClass`,NULL,refmaster.`description`) AS DrivingClass,
     Left(crew.`suspended`,10) AS Suspended,
     crew.`warehouse` AS Warehouse,
     refmaster.`code` AS Code
FROM
     `crew` crew LEFT OUTER JOIN `refmaster` refmaster ON crew.`drivingClass`=refmaster.`code` AND refmaster.`parameter`='DRVRCLS'
WHERE crew.`corpId`=$P{Corporate ID}
AND crew.`terminatedDate` is  null
AND crew.`drivingClass`!=''
AND crew.`warehouse`>0
group by crew.`warehouse`,crew.`drivingClass`,CONCAT_WS(', ',crew.`lastName`,NULL,crew.`firstName`)
order by crew.`warehouse`,crew.`drivingClass`,CONCAT_WS(', ',crew.`lastName`,NULL,crew.`firstName`)]]></queryString>

	<field name="driver_list_approved.jrxml" class="java.lang.String"/>
	<field name="Name" class="java.lang.String"/>
	<field name="Cdl" class="java.lang.String"/>
	<field name="CdlExpires" class="java.sql.Timestamp"/>
	<field name="DoTPhysical" class="java.sql.Timestamp"/>
	<field name="DrivingClass" class="java.lang.String"/>
	<field name="Suspended" class="java.sql.Timestamp"/>
	<field name="Warehouse" class="java.lang.String"/>
	<field name="Code" class="java.lang.String"/>


		<group  name="Warehouse" >
			<groupExpression><![CDATA[$F{Warehouse}]]></groupExpression>
			<groupHeader>
			<band height="25"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="6"
						width="72"
						height="17"
						forecolor="#3333FF"
						key="staticText-3"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Warehouse:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="72"
						y="6"
						width="66"
						height="17"
						forecolor="#3333FF"
						key="textField"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Warehouse}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="25"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="8"
						width="155"
						height="16"
						forecolor="#3333FF"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Total Drivers For Warehouse]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="155"
						y="8"
						width="18"
						height="16"
						forecolor="#3333FF"
						key="textField-2"/>
					<box leftPadding="3"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Warehouse}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="181"
						y="8"
						width="100"
						height="16"
						forecolor="#3333FF"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{Warehouse_COUNT}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="173"
						y="8"
						width="8"
						height="16"
						forecolor="#3333FF"
						key="staticText-11"/>
					<box></box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<text><![CDATA[-]]></text>
				</staticText>
			</band>
			</groupFooter>
		</group>
		<group  name="DrivingClass" >
			<groupExpression><![CDATA[$F{DrivingClass}]]></groupExpression>
			<groupHeader>
			<band height="25"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="6"
						width="86"
						height="17"
						forecolor="#000000"
						key="staticText-8"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Driving Class:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="86"
						y="6"
						width="257"
						height="17"
						forecolor="#000000"
						key="textField"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{DrivingClass}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="25"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="117"
						y="3"
						width="235"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="1.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="107"
						y="8"
						width="31"
						height="16"
						forecolor="#000000"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{DrivingClass_COUNT}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="138"
						y="8"
						width="121"
						height="16"
						forecolor="#000000"
						key="staticText-9"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Total Drivers for Class ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="259"
						y="8"
						width="77"
						height="16"
						forecolor="#000000"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Code}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="32"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="7"
						width="535"
						height="25"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Driver List-Approved]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="30"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="6"
						width="138"
						height="17"
						key="staticText-2"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Driver]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="138"
						y="6"
						width="121"
						height="17"
						key="staticText-4"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CDL#]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="412"
						y="6"
						width="123"
						height="17"
						key="staticText-5"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[DOT Physical Due]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="259"
						y="6"
						width="84"
						height="17"
						key="staticText-6"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CDL Expires]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="343"
						y="6"
						width="69"
						height="17"
						key="staticText-7"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Suspended]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="138"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="138"
						y="1"
						width="121"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Cdl}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="259"
						y="1"
						width="84"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{CdlExpires}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="343"
						y="1"
						width="69"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{Suspended}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="412"
						y="1"
						width="123"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{DoTPhysical}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="499"
						y="4"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="406"
						y="4"
						width="93"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="2"
						y="4"
						width="137"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="535"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="185"
						y="6"
						width="172"
						height="17"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<text><![CDATA[driver_list_approved.jrxml]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
