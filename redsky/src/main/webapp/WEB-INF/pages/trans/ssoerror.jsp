<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>RedSky : Unauthorized </title>
    <meta name="heading" content="RedSky : Unauthorized"/>  
    
 <style>
#errorDiv {background:url(/redsky/images/404-bg-opt.png) top left no-repeat; height:248px; width:703px; position:relative; margin:10% auto 0;} 
#headMessage {
	font-family:Arial, Helvetica, sans-serif;
	font-size:30px;
	font-weight:normal;
	color:#1C53CB;
	line-height:1;
	position:absolute;
	top:82px;
	text-align:center;
	left:170px;
}

#subheadMessage {
	font-family:Arial, Helvetica, sans-serif;
	font-size:18px;
	font-weight:normal;
	line-height:1.2;
	color:#3B3B3B;
	position:absolute;
	top:145px;
	left:142px;
	text-align:center;
}
#subheadMessage a{	
	color:#1C53CB;
	text-decoration:underline;	
}
</style>
</head>
<div id="errorDiv">
<div id="headMessage">
There was a problem accessing the Dispatch Console.   
</div>
<div id="subheadMessage">
Please refresh the page and try again. <br> If the problem persists, please contact support at support@redskymobility.com.
</div>
</div>
</html>