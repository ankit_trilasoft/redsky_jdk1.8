package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="systemconfiguration")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class SystemConfiguration extends BaseObject{
	
	private Long id;
	private String corpId;
	private String tableName;
	private String fieldName;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpId",
				corpId).append("tableName", tableName).append("fieldName",
				fieldName).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SystemConfiguration))
			return false;
		SystemConfiguration castOther = (SystemConfiguration) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpId,
				castOther.corpId).append(tableName, castOther.tableName)
				.append(fieldName, castOther.fieldName).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(tableName).append(fieldName).append(createdBy).append(
						updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=25)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=50)
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=25)
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=25)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=25)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	
}
