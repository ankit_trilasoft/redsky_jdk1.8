package com.trilasoft.app.webapp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.dao.hibernate.dto.AgentUpdateSchedularDTO;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.webapp.action.ExcelCreator;

import java.util.Calendar;

public class AgentUpdateSchedular extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private PartnerPublicManager partnerPublicManager;
	private UserManager userManager;
	static final Logger logger = Logger.getLogger(AgentUpdateSchedular.class);
	
	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	//Now calling of this method in  applicationContext.xml has been blocked. 
	// Now this method not in use. Operation of this method will do from SQL Extract Scheduler.
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException {
		String UserEmailId="";
		String executionTime="";
		StringBuilder partnerList = new StringBuilder("");
		try{
			appCtx = getApplicationContext(context); 
			partnerPublicManager = (PartnerPublicManager)appCtx.getBean("partnerPublicManager");
			UserManager userManager=(UserManager)appCtx.getBean("userManager");  
			List agentUpdationList = partnerPublicManager.agentUpdatedIn24Hour();
			//if(agentUpdationList!=null){
			  String host = "localhost";
			   String from = "support@redskymobility.com";
			   //String to[]= UserEmailId.split(",");
			   String tempEmailTo="agents@redskymobility.com";
				String tempRecipient="";
				String tempRecipientArr[]=tempEmailTo.split(",");
				for(String str1:tempRecipientArr){
					if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
						if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
						tempRecipient += str1;
					}
				}
				tempEmailTo=tempRecipient;
			   String to[] =  new String[]{tempEmailTo};
			   //String filename = file.getName() + ".xls";
			   // Get system properties
			   System.out.println("\n\n\n\n\n\n\n Email5 ex    ");
			   Properties props = System.getProperties();
			   props.put("mail.smtp.host", host);
			   Session session = Session.getInstance(props, null);
			   //System.out.println(session.getProperties());
			   Message message = new MimeMessage(session);
			   message.setFrom(new InternetAddress(from)); 
			   InternetAddress[] toAddress = new InternetAddress[to.length];
			   for (int i = 0; i < to.length; i++)
			     toAddress[i] = new InternetAddress(to[i]);
			   message.setRecipients(Message.RecipientType.TO, toAddress);    
			   message.setSubject("Redsky Updated Agents List");
			   BodyPart messageBodyPart = new MimeBodyPart();
			   SimpleDateFormat format1 = new SimpleDateFormat("MMM-dd-yyyy");
			   StringBuilder systemDate1 = new StringBuilder(format1.format(new Date()));
				   String msgText1 = "Dear Requester,\n\nThis email was sent by the RedSky.Reports Scheduler that was setup to run on Daily\n\nSchedule Details\nFile Name: AgentUpdates.xls\n\n\n\n Regards, "+"\n RedSky Support Team.";
			   messageBodyPart.setText(msgText1);
			   Multipart multipart = new MimeMultipart();
			   multipart.addBodyPart(messageBodyPart);
			   messageBodyPart = new MimeBodyPart();
			   String file_name = "AgentUpdate.xls";
			   String uploadDir =("/resources");
			   uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/agentExtract" + "/" + "TSFT" + "/");
			   try{
				 File dirPath = new File(uploadDir);
			        if (!dirPath.exists()) {
			            dirPath.mkdirs();
			        }
			   }catch(Exception e){}
			   file_name=uploadDir + file_name;
			   ExcelCreator.getExcelCreator().createExcelBook(file_name, new String[]{"Partner Code", "LastName", "Updatedon","Updated By"}, agentUpdationList, "Agent List", "seprator");
			   DataSource source = new FileDataSource(file_name);
			   messageBodyPart.setDataHandler(new DataHandler(source));
			   messageBodyPart.setFileName("AgentUpdates.xls");
			   multipart.addBodyPart(messageBodyPart);
			   message.setContent(multipart);
			  try{
			       Transport.send(message);
			       logger.warn("mail send successfully to user : " + to[0]) ; 
			  }
			  catch(SendFailedException sfe)
			   {
				  logger.warn("Exception Occurred: " + sfe);
			 	 message.setRecipients(Message.RecipientType.TO,  sfe.getValidUnsentAddresses());
			 	 Transport.send(message);
			 	 String emailmessage=sfe.toString();
			 	 if(emailmessage.length()>200){
			 		emailmessage=emailmessage.substring(200);	 
			 	 }
		}
			  catch(Exception e){
				  logger.warn("Exception :: " + e);
			  }
		//}
		}catch(Exception e){
			 logger.warn("Main Exception :: " + e);
		}
	}	
		

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	
}
