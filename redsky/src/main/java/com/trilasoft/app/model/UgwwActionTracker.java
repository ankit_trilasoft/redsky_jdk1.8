package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;


	
	    @Entity
	    @Table(name="ugwwactiontracker")
	    public class UgwwActionTracker extends BaseObject {
		private Long id;
		private String sonumber;
		private String action;
		private String username;
		private Date actiontime;
		private String corpid;
		private String createdBy;
		private Date createdOn;
		private String updatedBy;
		private Date updatedOn;
		private UgwwActionTracker ugwwActionTracker;

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}
		
		
		public String getSonumber() {
			return sonumber;
		}

		public void setSonumber(String sonumber) {
			this.sonumber = sonumber;
		}

		@Column
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
		
		@Column
		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}
		@Column
		public String getCorpid() {
			return corpid;
		}

		public void setCorpid(String corpid) {
			this.corpid = corpid;
		}
		
		
		@Column
		public Date getActiontime() {
			return actiontime;
		}

		public void setActiontime(Date actiontime) {
			this.actiontime = actiontime;
		}
		

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Date getUpdatedOn() {
			return updatedOn;
		}

		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("id",id)
			.append("sonumber",sonumber)
			.append("createdOn",createdOn)
			.append("createdBy",createdBy)
			.append("updatedOn",updatedOn)
			.append("updatedBy",updatedBy)
			.append("action",action)
			.append("username",username)
			.append("actiontime",actiontime)
			.append("corpid",corpid).toString();
		}

		 public boolean equals(final Object  other){
		    	if(!(other instanceof UgwwActionTracker))
		    		return false;
		    	UgwwActionTracker castOther = (UgwwActionTracker)other;
				return new EqualsBuilder()
				.append("id",castOther.id)
				.append("sonumber",castOther.sonumber)
				.append("createdOn",castOther.createdOn)
			.append("createdBy",castOther.createdBy)
			.append("updatedOn",castOther.updatedOn)
			.append("updatedBy",castOther.updatedBy)
				.append("action",castOther.action)
				.append("username",castOther.username)
				.append("actiontime",castOther.actiontime)
				.append("corpid",castOther.corpid)
				.isEquals();
		    	
		    }
		    public int hashCode() {
				return new HashCodeBuilder().append(id)
						.append(sonumber)
						.append(action)
						.append(username)
						.append(actiontime)
						.append(corpid)
						.append(createdOn)
			            .append(createdBy)
		            	.append(updatedOn)
			            .append(updatedBy)
						
						.toHashCode();
		    }
		    

}
