<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <head>  

</head>   
<s:form id="driverDBChildForm" method="post" >

<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr> 
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
 <display:table name="driverDashBoardChildList" class="table" id="driverChildList" style="width:100%;margin-bottom:0px;"> 
   	<display:column property="glcode" title="Rev Code" ></display:column>
  	<display:column property="approved" title="Approved" format="{0,date,dd-MMM-yyyy}"></display:column>
	<display:column property="description" title="Description" ></display:column>
	<display:column title="Advance / Charge Back" style="width:125px;">	
	<div align="right">
		<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverChildList.amount}" />  
     </div> 	
	</display:column>
</display:table>
</s:form>