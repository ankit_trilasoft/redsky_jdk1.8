package com.trilasoft.app.service.impl;

import java.util.List;


import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DsCrossCulturalTrainingDao;
import com.trilasoft.app.model.DsCrossCulturalTraining;
import com.trilasoft.app.service.DsCrossCulturalTrainingManager;

public class DsCrossCulturalTrainingManagerImpl extends GenericManagerImpl<DsCrossCulturalTraining, Long> implements DsCrossCulturalTrainingManager{
	
	DsCrossCulturalTrainingDao dsCrossCulturalTrainingDao;
	
	public DsCrossCulturalTrainingManagerImpl(DsCrossCulturalTrainingDao dsCrossCulturalTrainingDao) {
		super(dsCrossCulturalTrainingDao);
		this.dsCrossCulturalTrainingDao=dsCrossCulturalTrainingDao;
	}

	public List getListById(Long id) {
		
		return dsCrossCulturalTrainingDao.getListById(id);
	}

	
}
