<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataSecurityFilterForm.title"/></title> 
<meta name="heading" content="<fmt:message key='dataSecurityFilterForm.heading'/>"/>
<script type="text/javascript">
	
	function copyToName()
	{
	
		var name=document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.name'].value;
		var tableName=document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.tableName'].value; 
		var fieldName=document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'].value;
		var values=document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.filterValues'].value;
		if(tableName=='serviceorder')
		{
			tableName='SO';
		}
		if(tableName=='customerfile')
		{
			tableName='CF';
		}
		if(tableName=='notes')
		{
			tableName='notes';
		} 
		var genName='DATA_SECURITY_FILTER_' +tableName.toUpperCase()+'_'+fieldName.toUpperCase()+'_'+values;
		document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.name'].value=genName;
	}
	
	
	
	function getFieldList(target)
	{
		var tableName= target.value;
		var url="getFieldList.html?ajax=1&decorator=simple&popup=true&tableName=" + encodeURI(tableName);
		http2.open("GET", url, true);
    	http2.onreadystatechange = handleHttpResponseFieldList;
     	http2.send(null);
	}
	
	function handleHttpResponseFieldList()
        {
            
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                // alert(results);
                res = results.split("@");
                
                targetElement = document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'];
				targetElement.length = res.length;
				
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					
					document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'].options[i].text = '';
					document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'].options[i].value = '';
					}else{
					
					// contractVal = res[i].split("#");
					document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'].options[i].text = res[i];
					document.forms['dataSecurityFilterForm'].elements['dataSecurityFilter.fieldName'].options[i].value = res[i];
					}
					document.getElementById("fieldName").value = '${dataSecurityFilter.fieldName}';
					}
					
             }
           
        }
	
	
	
	
	String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();

</script>
<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
</style>
</head>

 <s:form cssClass="form_magn" id="dataSecurityFilterForm" name="dataSecurityFilterForm" action="saveDataSecurityFilterForm" method="post" validate="true">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="dataSecurityFilter.id"/>
 
 <div id="Layer1" style="width:100%"> 

		<div id="newmnav" class="nav_tabs"><!-- sandeep -->
		  <ul>
		  	
		  	<li><a href="dataSecurityFilterList.html"><span>Data Security Filter List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Data Security Filter Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	 <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${dataSecurityFilter.id}&tableName=datasecurityfilter&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
				<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">

  	<table border="0" cellpadding="2" cellspacing="1">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="70px" class="listwhitetext">Name</td>
		  		<td align="left"><s:textfield name="dataSecurityFilter.name"  maxlength="100" size="100" cssClass="input-textUpper" readonly="true" cssStyle="width:273px;"/></td>
		  		
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Table Name</td>
		  		<td align="left"><s:select cssClass="list-menu" id="tableName" name="dataSecurityFilter.tableName" list="%{tableList}" headerKey="" headerValue="" cssStyle="width:276px" onchange="getFieldList(this); copyToName();"/>
		  		
			</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Field Name</td>
		  		<td align="left"><s:select cssClass="list-menu" id="fieldName" name="dataSecurityFilter.fieldName" headerKey="" headerValue="" list="%{fieldList}" cssStyle="width:276px" onchange="copyToName();"/>
		  		
		    </tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Filter Values</td>
		  		<td align="left"><s:textfield name="dataSecurityFilter.filterValues"  maxlength="100" size="50" cssClass="input-text" readonly="false" onchange="copyToName();" cssStyle="width:273px;"/></td>
		  	</tr>
		  	 	<tr>
		  		<td align="left" height="15px"></td>
		  	</tr>
		  	</tbody>
	</table>
  
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="dataSecurityFilterCreatedOnFormattedValue" value="${dataSecurityFilter.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="dataSecurityFilter.createdOn" value="${dataSecurityFilterCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${dataSecurityFilter.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty dataSecurityFilter.id}">
								<s:hidden name="dataSecurityFilter.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{dataSecurityFilter.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dataSecurityFilter.id}">
								<s:hidden name="dataSecurityFilter.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="dataSecurityFilterupdatedOnFormattedValue" value="${dataSecurityFilter.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="dataSecurityFilter.updatedOn" value="${dataSecurityFilterupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${dataSecurityFilter.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty dataSecurityFilter.id}">
								<s:hidden name="dataSecurityFilter.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{dataSecurityFilter.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dataSecurityFilter.id}">
								<s:hidden name="dataSecurityFilter.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button"  key="button.save"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty dataSecurityFilter.id}">
		       <input type="button" class="cssbutton1" value="Add"  onclick="location.href='<c:url value="/dataSecurityFilterForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table></td></tr></tbody></table></div>

</s:form>