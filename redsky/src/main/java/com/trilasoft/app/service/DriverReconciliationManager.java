package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.VanLine;

public interface DriverReconciliationManager extends GenericManager<VanLine, Long>  {
	public List getVanLineCodeCompDiv(String sessionCorpID);
	public List getWeekendingList(String agent,String ownerCode, String corpId);
	public List getDriverPreview(String agent,String ownerCode,Date weekending,String sessionCorpID);
	public List getDriverAccPreview(String vanRegNum,String agent,String ownerCode,Date weekending,String sessionCorpID);
	public List findPartnerDetailsForAutoComplete(String partnerCodeAutoCopmlete,String sessionCorpID);
}
