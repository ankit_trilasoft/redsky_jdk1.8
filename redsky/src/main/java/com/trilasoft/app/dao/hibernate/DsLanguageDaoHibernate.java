package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsLanguageDao;
import com.trilasoft.app.model.DsLanguage;

public class DsLanguageDaoHibernate extends GenericDaoHibernate<DsLanguage,Long> implements DsLanguageDao{

	public DsLanguageDaoHibernate() {
		super(DsLanguage.class);
	}

	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsLanguage where id = "+id+" ");
	}

	

}
