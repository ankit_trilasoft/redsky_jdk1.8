package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsRelocationExpenseManagementDao;
import com.trilasoft.app.model.DsRelocationExpenseManagement;
public class DsRelocationExpenseManagementDaoHibernate extends GenericDaoHibernate<DsRelocationExpenseManagement, Long> implements DsRelocationExpenseManagementDao {	
	public DsRelocationExpenseManagementDaoHibernate(){
		super(DsRelocationExpenseManagement.class);
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsRelocationExpenseManagement where id = "+id+" ");
	}
	
}

