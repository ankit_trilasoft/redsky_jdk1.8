<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/common/taglibs.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <%@ include file="/common/meta.jsp" %>
        <title><decorator:title/> | <fmt:message key="webapp.name"/></title>

        <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/theme.css'/>" />
        <link rel="stylesheet" type="text/css" media="print" href="<c:url value='/styles/${appConfig["csstheme"]}/print.css'/>" />
		
		<script language="javascript" type="text/javascript">
		
  //function loadjscssfile(filename, filetype){
  //if (filetype=="css"){ //if filename is an external CSS file
  //var fileref=document.createElement("link")
  //fileref.setAttribute("rel", "stylesheet")
  //fileref.setAttribute("type", "text/css")
  //var defaultCss = '${defaultCss}';
  //fileref.setAttribute("href", 'styles/'+defaultCss+'/'+defaultCss+'.css')
 //}
 //if (typeof fileref!="undefined")
  //document.getElementsByTagName("head")[0].appendChild(fileref)
  //document.write(getCalendarStyles());
//}
</script>
  <script type="text/javascript" src="scripts/ajax.js"></script>
   <script type="text/javascript" src="<c:url value='/scripts/prototype.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/scripts/scriptaculous.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/scripts/global.js'/>"></script>
      <script type="text/javascript" src="<c:url value='/scripts/listDetail.js'/>"></script>        
	<script type="text/javascript" src="<c:url value='/scripts/util.js'/>"></script>        
        <decorator:head/>
        
    </head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class" writeEntireProperty="true"/>>

    <div id="page">
        <div id="content" class="clearfix">
        	<p></p>
        	<div id="mainPopup" class="clearfix">
			<authz:authorize ifNotGranted="ROLE_ANONYMOUS">
	                <decorator:body/>
			</authz:authorize>
			<authz:authorize ifAnyGranted="ROLE_ANONYMOUS">
	                <%@ include file="/common/messages.jsp" %>
	                <decorator:body/>
			</authz:authorize>
			</div>
    </div>
</body>
</html>

