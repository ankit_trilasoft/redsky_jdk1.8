<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
 <script language="javascript" type="text/javascript">
  <sec-auth:authComponent componentId="module.script.form.agentScript">
  window.onload = function() { 
	trap();
	<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm3" transIdList='${serviceOrder.shipNumber}'>
	
	</sec-auth:authScript>
	
	}
 </sec-auth:authComponent>
 
 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 

		
		//trap();
					
	}
</sec-auth:authComponent>
 
 function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		        	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		        	 

		      }
		    }
		  }
		  
		  function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script>
<head>   
    <title><fmt:message key="vehicleList.title"/></title>   
    <meta name="heading" content="<fmt:message key='vehicleList.heading'/>"/> 
    <style type="text/css">


/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>



<script>
function goToCustomerDetail(targetValue){
        document.forms['serviceForm3'].elements['id'].value = targetValue;
        document.forms['serviceForm3'].action = 'editVehicle.html?from=list';
        document.forms['serviceForm3'].submit();
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	     location.href = "updateVehicleStatus.html?id="+encodeURI(targetElement)+"&sid=${ServiceOrderID}";
     }else{
		return false;
	}
}
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm3'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm3'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm3'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm3'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'vehicles.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceForm3'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceForm3'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
function goToUrl(id)
	{
		location.href = "vehicles.html?id="+id;
	}
	
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();
</script>     
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="serviceForm3">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<c:set var="buttons">  
<c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
 
     <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVehicle.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
         
     
</c:set>
  <s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
  <div id="Layer3" style="width:100%;">
<div id="newmnav" style="float: left;">
  <ul>
  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
   <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
   </sec-auth:authComponent>
     <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
      <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
    </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
  	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	  <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
	  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose>
	
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
	  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
	  <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	  </sec-auth:authComponent>	
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      
	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab"> 
  <li id="newmnav1" style="background:#FFF "><a href="containers.html?id=${serviceOrder.id}" class="current" ><span>Forwarding</span></a></li>
   </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
  <c:if test="${serviceOrder.job =='INT'}">
    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.statusTab">
   <c:if test="${serviceOrder.job =='RLO'}"> 
	 <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
</c:if>
<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
</c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
	</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  </sec-auth:authComponent>
   <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
  	  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </sec-auth:authComponent>
  </configByCorp:fieldVisibility>
   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
       <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
   </sec-auth:authComponent>
</ul></div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
		
		</c:if></tr></table>
<div class="spn">&nbsp;</div>


 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 <div id="Layer4" style="width:100%">
<div id="newmnav">
  <ul>
    <sec-auth:authComponent componentId="module.tab.container.sSContainertTab">
  <li><a  href="containers.html?id=${serviceOrder.id}" ><span>SS Container</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.pieceCountTab">
  <li><a  href="cartons.html?id=${serviceOrder.id}" ><span>Piece Count</span></a></li>
  </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.container.vehicleTab">
  <li id="newmnav1" style="background:#FFF "><a  href="vehicles.html?id=${serviceOrder.id}" class="current" ><span>Vehicle</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.routingTab">
  <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Routing</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
  <li><a href="editConsignee.html?sid=${serviceOrder.id}"><span>Consignee Instructions</span></a></li>
  </sec-auth:authComponent>
  
  <c:if test="${countBondedGoods >= 0}" >
  <li><a href="customs.html?id=${serviceOrder.id}"><span>Customs</span></a></li>
  </c:if>
  
   <sec-auth:authComponent componentId="module.tab.container.auditTab">
  <li>
    <a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=vehicle&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
    <span>Audit</span></a></li></sec-auth:authComponent>
  </ul>
</div><div class="spn">&nbsp;</div>

  </div>
<s:set name="vehicles" value="vehicles" scope="request"/>   
<display:table name="vehicles" class="table" requestURI="" id="vehicleList" export="false"  defaultsort="1" pagesize="100" style="width:99%; margin-left:5px;margin-top:2px;">   
    <display:column sortable="true" titleKey="vehicle.idNumber">
    <sec-auth:authComponent componentId="module.link.vehicle.id">
    <a onclick="goToCustomerDetail(${vehicleList.id});" style="cursor:pointer">
   </sec-auth:authComponent>
   <c:out value="${vehicleList.idNumber}"/></a></display:column>
    <display:column property="year" headerClass="containeralign" style="text-align: right;"  sortable="true" titleKey="vehicle.year"></display:column>
    <display:column property="cntnrNumber" sortable="true" titleKey="container.containerNumber"></display:column>
    <display:column property="make" sortable="true" titleKey="vehicle.make"></display:column>
    <display:column property="model" sortable="true" titleKey="vehicle.model"></display:column>
    <display:column sortable="true" headerClass="containeralign" style="text-align: right;" titleKey="vehicle.volume">
    <fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${vehicleList.volume}" />
                  </display:column>
    <display:column  sortable="true" headerClass="containeralign" style="text-align: right;" titleKey="vehicle.length">   
    <fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${vehicleList.length}" />
                  </display:column>
    <display:column  sortable="true" headerClass="containeralign" style="text-align: right;"  titleKey="vehicle.width">
    <fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${vehicleList.width}" />
                  </display:column>
    <display:column  sortable="true" headerClass="containeralign" style="text-align: right;" titleKey="vehicle.weight">
    <fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${vehicleList.weight}" />
                  </display:column>
    <display:column property="doors" headerClass="containeralign" style="text-align: right;" sortable="true" titleKey="vehicle.doors"/>
    <display:column property="cylinders" headerClass="containeralign" style="text-align: right;"  sortable="true" titleKey="vehicle.cylinders"/> 
     <c:if test="${usertype!='USER'}"> 
     <display:column title="Status" style="width:45px; text-align: center;">
     <c:if test="${vehicleList.status}">
      <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
     </c:if>
     <c:if test="${vehicleList.status==false}">
     <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
      
    </c:if>
    </display:column>
     </c:if>
    <display:setProperty name="paging.banner.item_name" value="vehicle"/>  
    <display:setProperty name="paging.banner.items_name" value="vehicle"/>
  
    <display:setProperty name="export.excel.filename" value="Vehicle List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Vehicle List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Vehicle List.pdf"/>   
</display:table>   
 <sec-auth:authComponent componentId="module.button.vehicle.addButton">
<c:out value="${buttons}" escapeXml="false" />
</sec-auth:authComponent>
</div>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<s:hidden name="id"/>
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("serviceForm3"));   
</script> 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/vehicles.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
		function navigationVal(){
			if (window.addEventListener)
				 window.addEventListener("load", function(){
					 var imgList = document.getElementsByTagName('img');
					 for(var i = 0 ; i < imgList.length ; i++){
						if(imgList[i].src.indexOf('images/nav') > 1){
							imgList[i].style.display = 'none';
						}
					 }
				 }, false)
		}
		  <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
		   navigationVal();
		 	window.onload = function() { 	 		
		 			var elementsLen=document.forms['serviceForm3'].elements.length;
		 			for(i=0;i<=elementsLen-1;i++){
		 				if(document.forms['serviceForm3'].elements[i].type=='text'){
		 						document.forms['serviceForm3'].elements[i].readOnly =true;
		 						document.forms['serviceForm3'].elements[i].className = 'input-textUpper';
		 					}else{
		 						document.forms['serviceForm3'].elements[i].disabled=true;
		 					}
		 			}
		 	}
		 	</sec-auth:authComponent>
</script>  
