package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.appfuse.model.User;
import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PasswordOTP;
 
public interface PasswordOTPManager extends GenericManager<PasswordOTP, Long> { 
	 public List checkById(Long id);
	 public List<User> loadUserByUsername(String username) ;
	 public List checkUserOTP(Long id, String verificationCode);
	} 
