<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
 <%-- <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" type="text/javascript"></script>
--%>
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>


	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
		<title><fmt:message key="partnerDetailAgent.title"/></title>   
    	<meta name="heading" content="<fmt:message key='partnerDetailAgent.heading'/>"/> 
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
		<title><fmt:message key="partnerDetail.title"/></title>   
    	<meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/> 
    </sec-auth:authComponent>	 
   

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>

</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

<script language="javascript" type="text/javascript">
function getHTTPObject() {

  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {

    try {

      xmlhttp = new XMLHttpRequest();

    } catch (e) {

      xmlhttp = false;

    }

  }

  return xmlhttp;

}

var http = getHTTPObject(); 
</script>

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script type="text/javascript">

animatedcollapse.addDiv('partnerportal', 'fade=1,hide=1')
animatedcollapse.addDiv('pics', 'fade=1,hide=1')
animatedcollapse.addDiv('description', 'fade=1,hide=1')
animatedcollapse.addDiv('info', 'fade=1,hide=1')
animatedcollapse.init()

</script>

<script language="javascript" type="text/javascript">
function onLoad() {
	//document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.billingInstructionCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingInstruction'].value;
	/*if(navigator.appName == 'Microsoft Internet Explorer'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}
		if(navigator.appName == 'Netscape'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}*/
	var f = document.getElementById('partnerAddForm'); 
	f.setAttribute("autocomplete", "off"); 
	
}
</script>
	<script type="text/javascript"> 
		function copyBillToMail(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingTelex'].value=document.forms['partnerAddForm'].elements['partner.billingTelex'].value;	
			
			//autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);        
			
			
			var oriCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
			if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text=billingStateText;
		    	x.options[x.selectedIndex].value=billingStateValue;
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
			}else{
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			}
			//alert(document.forms['partnerAddForm'].elements['partner.mailingState'].value);
			//getMailingState(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
		}
		function copyTermToMail(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.terminalEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.terminalCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.terminalFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.terminalZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.terminalPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingTelex'].value=document.forms['partnerAddForm'].elements['partner.terminalTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingState'].value=document.forms['partnerAddForm'].elements['partner.terminalState'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value;		
			
			var oriCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
			if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.terminalState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.terminalState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.terminalState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text=billingStateText;
		    	x.options[x.selectedIndex].value=billingStateValue;
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
			}else{
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.terminalState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.terminalState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.terminalState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			}
			
			
		}
		function copyBillToTerm(){ 
			document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCountry'].value=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalTelex'].value=document.forms['partnerAddForm'].elements['partner.billingTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalState'].value=document.forms['partnerAddForm'].elements['partner.billingState'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value;		
			
			var oriCountry = document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
			if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text=billingStateText;
		    	x.options[x.selectedIndex].value=billingStateValue;
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = false;
			}else{
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = true;
			
			}
		}
		function copyMailToTerm(){ 
			document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalEmail'].value=document.forms['partnerAddForm'].elements['partner.mailingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCity'].value=document.forms['partnerAddForm'].elements['partner.mailingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalFax'].value=document.forms['partnerAddForm'].elements['partner.mailingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalZip'].value=document.forms['partnerAddForm'].elements['partner.mailingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCountry'].value=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalPhone'].value=document.forms['partnerAddForm'].elements['partner.mailingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalTelex'].value=document.forms['partnerAddForm'].elements['partner.mailingTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalState'].value=document.forms['partnerAddForm'].elements['partner.mailingState'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value;		
			var oriCountry = document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
			if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.mailingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text=billingStateText;
		    	x.options[x.selectedIndex].value=billingStateValue;
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = false;
			}else{
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.mailingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = true;
			}
		}
	</script>
	<script type="text/javascript">
	function autoPopulate_partner_billingCountry1(targetElement) {
		
			var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		function autoPopulate_partner_terminalCountry1(targetElement) {
		
			var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
		}
		function autoPopulate_partner_mailingCountry1(targetElement) {
			
			var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
	   }
 
	function autoPopulate_partner_billingInstruction1(targetElement) {
		//var billingInstructionCode=targetElement.options[targetElement.selectedIndex].value;
	    //document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value=targetElement.options[targetElement.selectedIndex].value;
		//targetElement.form.elements['partner.billingInstruction'].value=billingInstructionCode.substring(billingInstructionCode.indexOf(":")+2,billingInstructionCode.length);
	}
	</script>
	<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>	
<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		//document.forms['partnerAddForm'].elements[targetElelemnt].focus();
		//targetElement.focus = true;
		return false;
	}
	return true;
 }
</script>
<script type="text/javascript">
function autoPopulate_partner_billingCountry(targetElement) {	
   	var oriCountry = targetElement.value;
	if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.billingState'].value = '';
			//autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
		//targetElement.form.elements['customerFile.destinationCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
	}
function autoPopulate_partner_terminalCountry(targetElement) {   
		var dCountry = targetElement.value;
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = false;
			
		}else{
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.terminalState'].value = '';
			//autoPopulate_customerFile_originCityCode(dCountryCode,'special');
		}
	}
	
	
	
function autoPopulate_partner_mailingCountry(targetElement) {   
	var dCountry = targetElement.value;
	 if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
			
		}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.mailingState'].value = '';
			//autoPopulate_customerFile_originCityCode(dCountryCode,'special');
		}
	}

var geocoder;
var map;
function loadGoogleGeoCode()
   {
   	var address1=document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value;
	var address2=document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value;
	var terminalCity=document.forms['partnerAddForm'].elements['partner.terminalCity'].value;
	var terminalZip=document.forms['partnerAddForm'].elements['partner.terminalZip'].value;
	var terminalState=document.forms['partnerAddForm'].elements['partner.terminalState'].value;
	var terminalCountry=document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
	
	var address = address1+","+address2+","+terminalCity+" - "+terminalZip+","+terminalState+","+terminalCountry;
	
	
      // Create new map object
      map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
      geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      geocoder.getLocations(address, addToMap);
   }
   function addToMap(response)
   {
      // Retrieve the object
      place = response.Placemark[0];

      // Retrieve the latitude and longitude
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
		//alert(point);
		document.forms['partnerAddForm'].elements['partner.latitude'].value = place.Point.coordinates[1];
		document.forms['partnerAddForm'].elements['partner.longitude'].value = place.Point.coordinates[0];
      // Center the map on this point
      map.setCenter(point, 13);

      // Create a marker
      marker = new GMarker(point);

      // Add the marker to map
      map.addOverlay(marker);

      // Add address information to marker
      marker.openInfoWindowHtml(place.address);
   }
   
function getBillingCountryCode(targetElement){
	var countryName=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}

function handleHttpResponseBillingCountry()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value = results;
 					//document.forms['partnerAddForm'].elements['partner.billingCountryCode'].select();
					 }
                 else
                 {
                     
                 }
             }
        }
        
        
        
 function getMailingCountryCode(targetElement){
	var countryName=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponseMailingCountry;
    http5.send(null);
}



	function handleHttpResponseMailingCountry()
        {

             if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                if(results.length>=1)
                	{
 					document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value = results;
 					//document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].select();
 					 }
                 else
                 {
                     
                 }
             }
        }
        
        
 
        
function getTerminalCountryCode(){
	var countryName=document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponseTerminalCountry;
    http6.send(null);
}

function handleHttpResponseTerminalCountry()
        {

             if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value = results;
 					//document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].select();
					 }
                 else
                 {
                     
                 }
             }
        }
        





</script>
<SCRIPT LANGUAGE="JavaScript">
function autoPopulate_partner_billingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.billingState'].value = '';
		}
	}
	function autoPopulate_partner_terminalCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.terminalState'].value = '';
		}
	}
	function autoPopulate_partner_mailingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.mailingState'].value = '';
		}
	}
</script>
<SCRIPT LANGUAGE="JavaScript">
function getBillingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
function getMailingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
function getTerminalState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}
	
</script>
<SCRIPT LANGUAGE="JavaScript">	

function getBillingState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
	
function getMailingState(targetElement) {

	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
	
function getTerminalState(targetElement) {

	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}	


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}


 function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerAddForm'].elements['partner.billingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = stateVal[0];
					}
					}
             }
        }  
        
function handleHttpResponse3()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerAddForm'].elements['partner.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
					}
					}
             }
        }  

function handleHttpResponse4()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerAddForm'].elements['partner.terminalState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.terminalState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.terminalState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.terminalState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.terminalState'].options[i].value = stateVal[0];
					}
					}
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();

function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    var http5 = getHTTPObject1()
    var http6 = getHTTPObject1()
function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();    
</script>
<script>
function checkdate(clickType){
	if(document.forms['partnerAddForm'].elements['partner.lastName'].value == '' && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
			alert('Please enter the last name');
			document.forms['partnerAddForm'].elements['partner.lastName'].focus();
			return false;
	}
	
	var comDivision = document.forms['partnerAddForm'].elements['partner.corpID'].value;
	if(comDivision == 'SSCW'){
		if('${partnerType}' == 'PP' || '${partnerType}' == 'AC'){
			if(document.forms['partnerAddForm'].elements['partner.companyDivision'].value == ''  && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
				alert('Please select the company division');
				document.forms['partnerAddForm'].elements['partner.companyDivision'].focus();
				return false;
			}
		}
	}
	if(document.forms['partnerAddForm'].elements['partner.billingAddress1'].value == ''  && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
			alert('Please enter the billing address');
			document.forms['partnerAddForm'].elements['partner.billingAddress1'].focus();
			return false;
	}
	if(document.forms['partnerAddForm'].elements['partner.billingCountry'].value == ''  && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
			alert('Please select the billing country');
			document.forms['partnerAddForm'].elements['partner.billingCountry'].focus();
			return false;
	}
	var billingCountry = document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	if(billingCountry == 'United States' || billingCountry == 'Canada' || billingCountry == 'India' ){
		if(document.forms['partnerAddForm'].elements['partner.billingState'].value == ''){
			alert('Please select the billing state.');
			document.forms['partnerAddForm'].elements['partner.billingState'].focus();
			return false;
		}
	}
	if('${partnerType}' == 'AG' || '${partnerType}' == 'VN' || '${partnerType}' == 'CR'){
	var terminalCountry = document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
		if(terminalCountry == 'United States'){
		if(document.forms['partnerAddForm'].elements['partner.terminalState'].value == ''){
			alert('Please select the terminal state.');
			document.forms['partnerAddForm'].elements['partner.terminalState'].focus();
			return false;
		}
		}
		}
	var mailingCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	if(mailingCountry == 'United States'){
		if(document.forms['partnerAddForm'].elements['partner.mailingState'].value == ''){
			alert('Please select the mailing state.');
			document.forms['partnerAddForm'].elements['partner.mailingState'].focus();
			return false;
		}
	}
	if(document.forms['partnerAddForm'].elements['partner.billingCity'].value == ''  && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
			alert('Please select the billing city');
			document.forms['partnerAddForm'].elements['partner.billingCity'].focus();
			return false;
	}
	if(document.forms['partnerAddForm'].elements['partner.billingPhone'].value == ''  && document.forms['partnerAddForm'].elements['formStatus'].value =='1'){
			alert('Please select the billing phone');
			document.forms['partnerAddForm'].elements['partner.billingPhone'].focus();
			return false;
	}
	document.forms['partnerAddForm'].elements['partner.status'].disabled = false;
	
progressBarAutoSave('1');	
if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${partner.id}"/>';
	var id1 = document.forms['partnerAddForm'].elements['partner.id'].value;
	var partnerType = document.forms['partnerAddForm'].elements['partnerType'].value;
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.forwardingdtl'){
				noSaveAction = 'forwardingDetails.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentinfo'){
				noSaveAction = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendorinfo'){
				noSaveAction = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				noSaveAction = 'baseList.html?id='+id1;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				noSaveAction = 'partnerRateGrids.html?partnerId='+id1;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
				noSaveAction = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountinfo'){
				noSaveAction = 'accountInfoPage.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
				noSaveAction = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
				noSaveAction = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
			}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					noSaveAction = 'searchPartnerView.html';
				}else{
					noSaveAction = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
			}	
	processAutoSave(document.forms['partnerAddForm'],'savePartner!saveOnTabChange.html', noSaveAction );
}else{
	if(!(clickType == 'save')){
	var id1 = document.forms['partnerAddForm'].elements['partner.id'].value;
	var partnerType = document.forms['partnerAddForm'].elements['partnerType'].value;
	
	if (document.forms['partnerAddForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='partnerDetail.heading'/>");
		if(agree){
			document.forms['partnerAddForm'].action = 'savePartner!saveOnTabChange.html';
			document.forms['partnerAddForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.forwardingdtl'){
				location.href = 'forwardingDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentinfo'){
				location.href = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendorinfo'){
				location.href = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				location.href = 'baseList.html?id='+id1;
			}	
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				location.href = 'partnerRateGrids.html?partnerId='+id1;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
				location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountinfo'){
				location.href = 'accountInfoPage.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
				location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
				location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
			}
				
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.forwardingdtl'){
				location.href = 'forwardingDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentinfo'){
				location.href = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendorinfo'){
				location.href = 'agentDetails.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				location.href = 'baseList.html?id='+id1;
			}		
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				location.href = 'partnerRateGrids.html?partnerId='+id1;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
				location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountinfo'){
				location.href = 'accountInfoPage.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
				location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
				location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
			}
		}
	}
}
}
}
function changeStatus(){
	document.forms['partnerAddForm'].elements['formStatus'].value = '1';
}

function validateForm(){
	if(document.forms['partnerAddForm'].elements['partner.lastName'].value == '' && document.forms['partnerAddForm'].elements['formStatus'].value !='1'){
			alert('Please enter the last name');
			document.forms['partnerAddForm'].elements['partner.lastName'].focus();
			return false;
	}
	
	var comDivision = document.forms['partnerAddForm'].elements['partner.corpID'].value;
	if(comDivision == 'SSCW'){
		if('${partnerType}' == 'PP' || '${partnerType}' == 'AC'){
			if(document.forms['partnerAddForm'].elements['partner.companyDivision'].value == ''){
				alert('Please select the company division');
				document.forms['partnerAddForm'].elements['partner.companyDivision'].focus();
				return false;
			}
		}
	}
	
	if(document.forms['partnerAddForm'].elements['partner.billingAddress1'].value == ''){
			alert('Please enter the billing address');
			document.forms['partnerAddForm'].elements['partner.billingAddress1'].focus();
			return false;
	}
	if(document.forms['partnerAddForm'].elements['partner.billingCountry'].value == ''){
			alert('Please select the billing country');
			document.forms['partnerAddForm'].elements['partner.billingCountry'].focus();
			return false;
	}
	var billingCountry = document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	if(billingCountry == 'United States' || billingCountry == 'Canada' || billingCountry == 'India' ){
		if(document.forms['partnerAddForm'].elements['partner.billingState'].value == ''){
			alert('Please select the billing state.');
			document.forms['partnerAddForm'].elements['partner.billingState'].focus();
			return false;
		}
	}
	if('${partnerType}' == 'AG' || '${partnerType}' == 'VN' || '${partnerType}' == 'CR'){
	var terminalCountry = document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
		if(terminalCountry == 'United States'){
		if(document.forms['partnerAddForm'].elements['partner.terminalState'].value == ''){
			alert('Please select the terminal state.');
			document.forms['partnerAddForm'].elements['partner.terminalState'].focus();
			return false;
		}
		}
		}
	var mailingCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	if(mailingCountry == 'United States'){
		if(document.forms['partnerAddForm'].elements['partner.mailingState'].value == ''){
			alert('Please select the mailing state.');
			document.forms['partnerAddForm'].elements['partner.mailingState'].focus();
			return false;
		}
	}
	if(document.forms['partnerAddForm'].elements['partner.billingCity'].value == ''){
			alert('Please select the billing city');
			document.forms['partnerAddForm'].elements['partner.billingCity'].focus();
			return false;
	}
	if(document.forms['partnerAddForm'].elements['partner.billingPhone'].value == ''){
			alert('Please select the billing phone');
			document.forms['partnerAddForm'].elements['partner.billingPhone'].focus();
			return false;
	}
	document.forms['partnerAddForm'].elements['partner.status'].disabled = false;
	return checkLength();
}

function removePhotograph1(){
	document.forms['partnerAddForm'].elements['partner.location1'].value = '';
	document.getElementById("userImage1").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph2(){
	document.forms['partnerAddForm'].elements['partner.location2'].value = '';
	document.getElementById("userImage2").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph3(){
	document.forms['partnerAddForm'].elements['partner.location3'].value = '';
	document.getElementById("userImage3").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph4(){
	document.forms['partnerAddForm'].elements['partner.location4'].value = '';
	document.getElementById("userImage4").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
	
function isURL(){
	var urlStr=document.forms['partnerAddForm'].elements['partner.url'].value;
	alert(urlStr);
	if (urlStr.indexOf(" ") != -1) {
		alert("Spaces are not allowed in a URL");
		document.forms['partnerAddForm'].elements['partner.url'].value="";
		return false;
	}

	if (urlStr.value == "" || urlStr.value == null) {
		return true;
	}

  	urlStr = urlStr.value.toLowerCase();

	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var urlPat=/^http:\/\/(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
	var matchArray=urlStr.match(urlPat);

	if(matchArray == null){
		alert("The URL seems incorrect check it begins with http:/\/ and it has 2 .'s");
		document.forms['partnerAddForm'].elements['partner.url'].value="";
		return false;
	}
	return true;
}
	
	

function openTestPage(){
	//isURL();
	var url=document.forms['partnerAddForm'].elements['partner.url'].value;
	//window.open(url);
	window.open().location.href='http://'+url;

}

function  checkLength(){
	var  partnerType=document.forms['partnerAddForm'].elements['partnerType'].value;
 	if(partnerType=='AG')
	{
	var txt = document.forms['partnerAddForm'].elements['partner.companyProfile'].value;
	var txt1 = document.forms['partnerAddForm'].elements['partner.companyFacilities'].value;
	var txt2 = document.forms['partnerAddForm'].elements['partner.companyCapabilities'].value;
	var txt3 = document.forms['partnerAddForm'].elements['partner.companyDestiantionProfile'].value;
	if(txt.length >1500){
		alert('You can not enter more than 1500 characters in companyProfile');
		document.forms['partnerAddForm'].elements['partner.companyProfile'].value = txt.substring(0,1498);
		return false;
	}
	if(txt1.length >1500){
		alert('You can not enter more than 1500 characters in companyFacilities');
		document.forms['partnerAddForm'].elements['partner.companyFacilities'].value = txt1.substring(0,1498);
		return false;
	}
	if(txt2.length >1500){
		alert('You can not enter more than 1500 characters in companyCapabilities');
		document.forms['partnerAddForm'].elements['partner.companyCapabilities'].value = txt2.substring(0,1498);
		return false;
	}
	if(txt3.length >1500){
		alert('You can not enter more than 1500 characters in companyDestiantionProfile');
		document.forms['partnerAddForm'].elements['partner.companyDestiantionProfile'].value = txt3.substring(0,1498);
		return false;
	}
	}
}

function findGeoCode(){
	window.open('http://www.gpsvisualizer.com/geocode','','width=750,height=570') ;
}
function mapLocation(){
	var latitude = document.forms['partnerAddForm'].elements['partner.latitude'].value;
	var longitude = document.forms['partnerAddForm'].elements['partner.longitude'].value
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+latitude+','+longitude);
}

function convertToMiles(){
	var km=document.forms['partnerAddForm'].elements['partner.serviceRangeKms'].value;
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['partnerAddForm'].elements['partner.serviceRangeMiles'].value=rounded;
}
function convertToKm(){
	var miles=document.forms['partnerAddForm'].elements['partner.serviceRangeMiles'].value;
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['partnerAddForm'].elements['partner.serviceRangeKms'].value=rounded;
}

function convertToFeet(){
	var meters=document.forms['partnerAddForm'].elements['partner.facilitySizeSQMT'].value;
	var feet=10.77*meters;
	var rounded=Math.round(feet);
	document.forms['partnerAddForm'].elements['partner.facilitySizeSQFT'].value=rounded;
}

function convertToMeters(){
	var feet=document.forms['partnerAddForm'].elements['partner.facilitySizeSQFT'].value;
	var meters=0.093*feet;
	var rounded=Math.round(meters);
	document.forms['partnerAddForm'].elements['partner.facilitySizeSQMT'].value=rounded;
}



function openWebSite(){
var  www="www.";
var	http= "http://";
var theUrl = document.forms['partnerAddForm'].elements['partner.url'].value;
	if(theUrl.substring(0,4)==www)
	{
		theUrl=http+theUrl;
	}
if(theUrl.match(/^(http|ftp)\:\/\/\w+([\.\-]\w+)*\.\w{2,4}(\:\d+)*([\/\.\-\?\&\%\#]\w+)*\/?$/i)){
   if(theUrl.substring(7,11)==www)
    {
	   	window.open(theUrl);
	}
	else{
		alert("Please enter a valid URL");
	  	theUrl.select();
    	theUrl.focus();
     	document.forms['partnerAddForm'].elements['partner.url'].value='';
	  	return false;
		}
}
else
{
	alert("Please enter a valid URL");
    theUrl.select();
    theUrl.focus();
     document.forms['partnerAddForm'].elements['partner.url'].value='';
    return false;
}
}
</script>

<style type="text/css">
.myClass{ height:150px; width:auto}

.urClass{ height:auto; width:150px; }

/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none;} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>
<script language="javascript">
function secImage1(){


if(document.getElementById("userImage1").height > 150)
{
 document.getElementById("userImage1").style.height = "120px";
 document.getElementById("userImage1").style.width = "auto";
}
}
function secImage2(){

if(document.getElementById("userImage2").height > 150 )
{
 document.getElementById("userImage2").style.height = "120px";
 document.getElementById("userImage2").style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById("userImage3").height > 150)
{
 document.getElementById("userImage3").style.height = "120px";
 document.getElementById("userImage3").style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById("userImage4").height > 150)
{
 document.getElementById("userImage4").style.height = "120px";
 document.getElementById("userImage4").style.width = "auto";
} 
}

</script>
	
</head> 

<s:form id="partnerAddForm" name="partnerAddForm" enctype="multipart/form-data" action='${empty param.popup?"savePartner.html":"savePartner.html?decorator=popup&popup=true"}' method="post" validate="true">   
<s:hidden name="partner.partnerPortalActive" />
<s:hidden name="partner.associatedAgents" />
<s:hidden name="partner.viewChild" />
<s:hidden name="partner.id" />
<s:hidden name="partner.corpID" />
<s:hidden name="partner.isAccountParty" />
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.agentParent" />
<s:hidden name="partner.location1" />
<s:hidden name="partner.location2" />
<s:hidden name="partner.location3" />
<s:hidden name="partner.location4" />
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />


	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="popupval" value="${papam.popup}"/>
	
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<c:if test="${validateFormNav == 'OK' }">
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.forwardingdtl' }">
		<c:redirect url="/forwardingDetails.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.agentinfo' }">
		<c:redirect url="/agentDetails.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.vendorinfo' }">
		<c:redirect url="/agentDetails.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.base' }">
		<c:redirect url="/baseList.html?id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.ratematrix' }">
		<c:redirect url="/partnerRateGrids.html?partnerId=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accountprofile' }">
		<c:redirect url="/editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accountinfo' }">
		<c:redirect url="/accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accountcontact' }">
		<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.contractpolicy' }">
		<c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"/>
	</c:when>
	
	
	<c:when test="${gotoPageString == 'gototab.partnerlist' }">
		<c:if test="${paramValue == 'View'}">
			<c:redirect url="/searchPartnerView.html"/>
		</c:if>
		<c:if test="${paramValue != 'View'}">
			<c:redirect url="/searchPartnerAdmin.html?partnerType=${partnerType}"/>
		</c:if>
	</c:when>
	
	<c:otherwise>
	</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty partner.id}">
<div id="layer4" style="width:100%;">
<div id="newmnav">
				  <ul>
				   <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				  	<c:if test="${partnerType == 'AC'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Info</span></a></li>
			  			<li><a onclick="setReturnString('gototab.accountprofile');return checkdate('none');"><span>Account Profile</span></a></li>
			  			<li><a onclick="setReturnString('gototab.accountcontact');return checkdate('none');"><span>Account Contact</span></a></li>
			  			<li><a onclick="setReturnString('gototab.contractpolicy');return checkdate('none');"><span>Policy</span></a></li>
			  		</c:if>
				    <c:if test="${partnerType == 'AG'}">
					    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					    
                        <li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
    
					    <li><a onclick="setReturnString('gototab.agentinfo');return checkdate('none');"><span>Agent Info</span></a></li>
					    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					    <li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.base');return checkdate('none');"><span>Base</span></a></li>
			  	        
				    </c:if>
				    <c:if test="${partnerType == 'VN'}">
					    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Vendor Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					    <li><a onclick="setReturnString('gototab.vendorinfo');return checkdate('none');"><span>Vendor Info</span></a></li>
				  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'PP'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Private Party Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'CR'}">
					    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Carrier Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'OO'}">
					    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Owner Ops<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				    </c:if>
				    <li><a onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
				   </sec-auth:authComponent>
				   
				   <sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
					   	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					   	 <li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
    
					   	<c:if test="${partner.partnerPortalActive == true}">
				  			<li><a href="partnerUsersList.html?id=${partner.id}"><span>Users & Contacts</span></a></li>
						</c:if>
					    	<!--<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
					    --><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
				  </sec-auth:authComponent>
				  <c:if test="${partnerType == 'AG'}">
				        <!--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
			              <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			            </c:if>
			            <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
			             <li><a onclick="setReturnString('gototab.ratematrix');return checkdate('none');"><span>Rate Matrix</span></a></li>
			            </c:if>
			          --></c:if>
				  </ul>
		</div><div class="spn">&nbsp;</div>
	
		</div> 
</c:if>

<c:if test="${empty partner.id}">
<div id="layer4" style="width:100%;">
<div id="newmnav">
				<ul>
					 <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				    <c:if test="${partnerType == 'AC'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail</span></a></li>
				    	<li><a><span>Account Profile</span></a></li>
			  			<li><a><span>Policy</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'AG'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail</span></a></li>
				    	<%--<li><a><span>Rate Matrix</span></a></li>
				    --%></c:if>
				    <c:if test="${partnerType == 'VN'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Vendor Detail</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'PP'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Private Party Detail</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'CR'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Carrier Detail</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'OO'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Owner Ops</span></a></li>
				    </c:if>
				    <li><a onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
				    </sec-auth:authComponent>
				    
				   	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
					   	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					   	 	<!--<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
					   	--><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
				   	</sec-auth:authComponent>
			  	</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div> 
</c:if>

<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
				<c:choose>
					<c:when test="${partnerType == 'PP'}">
						<table cellspacing="2" cellpadding="2" border="0">
							<tbody>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.firstName'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.lastName'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext">Company Division<c:if test="${partner.corpID == 'SSCW'}"><font color="red" size="2">*</font></c:if></td>
								</tr>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext"><s:textfield name="partner.partnerPrefix" cssStyle="width:25px" required="true"
									cssClass="input-text" maxlength="3"  onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext"> <s:textfield name="partner.firstName" required="true" cssClass="input-text" cssStyle="width:145px" maxlength="15" tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.middleInitial" cssClass="input-text"
									required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text" size="37" maxlength="80"  tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" cssStyle="width:30px" required="true"
									cssClass="input-text" maxlength="10" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
									required="true" cssClass="input-textUpper" size="15"  maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="1"/> </td>
									<td><s:select cssClass="list-menu" name="partner.companyDivision" list="%{companyDivis}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3" /></td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${partnerType == 'OO'}">
						<table cellspacing="2" cellpadding="2" border="0">
							<tbody>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.firstName'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.lastName'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
									<td  align="left" class="listwhitetext"><fmt:message key='partner.status'/></td>
									
								</tr>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext"><s:textfield name="partner.partnerPrefix" cssStyle="width:25px" required="true" cssClass="input-text" maxlength="3"  onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext"> <s:textfield name="partner.firstName" required="true" cssClass="input-text" cssStyle="width:145px" maxlength="15" tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.middleInitial" cssClass="input-text" required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text" size="37" maxlength="80"  tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" cssStyle="width:30px" required="true" cssClass="input-text" maxlength="10" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" size="15"  maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="1"/> </td>
									<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:100px"  onchange="changeStatus();" tabindex="2" /></td>
									
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${partnerType == 'AC'}">
						<table border="0" cellpadding="3" cellspacing="0" >
							<tbody>
								<tr>
									<td width="13"></td>
									<td  align="left" class="listwhitetext">Name<font color="red" size="2">*</font></td>
								  	<td  align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
								  	<td  align="left" class="listwhitetext"><fmt:message key='partner.status'/></td>
									<td  align="left" class="listwhitetext">Company Division<c:if test="${partner.corpID == 'SSCW'}"><font color="red" size="2">*</font></c:if></td>
								</tr>
								<tr>
									<td width="13"></td>
									<s:hidden name="partner.firstName"/>
									<s:hidden name="partner.partnerSuffix"/>
									<s:hidden name="partner.partnerPrefix"/>
									<s:hidden name="partner.middleInitial"/>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text" size="50" maxlength="80"   tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="25" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
									<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:100px"  onchange="changeStatus();" tabindex="2" /></td>
									<td><s:select cssClass="list-menu" name="partner.companyDivision" list="%{companyDivis}" cssStyle="width:125px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3" /></td>
								</tr>
								
							</tbody>
					</table>
					</c:when>
					<c:otherwise>
						<table cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr>
									<td width="10px"></td>
									<td align="left" class="listwhitetext">Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.status'/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<s:hidden name="partner.firstName"/>
									<s:hidden name="partner.partnerSuffix"/>
									<s:hidden name="partner.partnerPrefix"/>
									<s:hidden name="partner.middleInitial"/>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text" size="89" maxlength="80"   tabindex="1"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="15" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
									<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:137px"  onchange="changeStatus();" tabindex="2" /></td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
					</c:choose>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
						<tbody>
							<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
								<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
							</tbody>
						</table>
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
						
							<tr><td width="10px"></td><td class="listwhitetext"><b>Billing Address</b><font color="red" size="2">*</font></td>
							<tr><td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress1"  size="40" maxlength="30" tabindex="4" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/><font color="red" size="2">*</font></td>
								<td ><s:select cssClass="list-menu" name="partner.billingCountry" list="%{countryDesc}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();getBillingCountryCode(this);autoPopulate_partner_billingCountry(this);getBillingState(this);" tabindex="8" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="12" /></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress2" size="40" maxlength="30" tabindex="5"/></td>
								
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingState'/><font color="red" size="2">*</font></td>
								<td><s:select name="partner.billingState" cssClass="list-menu"list="%{bstates}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td>
								
								
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/><font color="red" size="2">*</font></td>
								<td><s:textfield cssClass="input-text" name="partner.billingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="13" /></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress3" size="40" maxlength="30" tabindex="6" /></td>
								
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/><font color="red" size="2">*</font></td>
								<td><s:textfield cssClass="input-text" name="partner.billingCity" cssStyle="width:160px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" tabindex="10" /></td>
															
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingTelex'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingTelex" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="14"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress4" size="40" maxlength="30" tabindex="7" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="11" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingEmail"  size="30" maxlength="65" tabindex="15" /></td>
							</tr>
						</tbody>
					</table>
					<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG' || partnerType == '' }">
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
							
							<tr><td width="10px"></td><td class="listwhitetext"> <b>Terminal Address</b></td></tr>
							<tr>
							<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress1"  size="40" maxlength="30" tabindex="16" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.terminalCountry" list="%{countryDesc}" cssStyle="width:162px"   headerKey="" headerValue="" onchange="changeStatus();getTerminalCountryCode(this);autoPopulate_partner_terminalCountry(this);getTerminalState(this);" tabindex="20" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="24" /></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress2" size="40" maxlength="30" tabindex="17"/></td>
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState'/></td>
									<td><s:select id="partnerAddForm_partner_terminalState" name="partner.terminalState" list="%{tstates}" cssClass="list-menu" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="21" /></td>
									
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="25"/></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress3" size="40" maxlength="30" tabindex="18" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalCity" cssStyle="width:160px"  maxlength="15" onkeydown="return onlyCharsAllowed(event)" tabindex="22"/></td>
									
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalTelex" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="26"/></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress4" size="40" maxlength="30" tabindex="19" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="23" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalEmail"  size="30"  maxlength="65" tabindex="27" /></td>
									
							</tr>
							<tr>
							<td width="10px"></td>
								<td colspan="1"></td>
								<td colspan="3" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" onClick="copyBillToTerm();changeStatus();" /></td>
								<td colspan="2" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from mailing address" onClick="copyMailToTerm();changeStatus();"  /></td>
							</tr>
						
						</tbody>
					</table>
					</c:if>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
							<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
								<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
							</tbody>
						</table>
						<table border="0" style="margin:0px;padding: 0px;">
						<tr>
						<td>
					<table cellspacing="3" cellpadding="2" border="0" style="margin-bottom: 0px;">
						<tbody>
								<tr><td width="10px"></td>
								<td class="listwhitetext"><b>Mailing Address</b></td></tr>
								<tr><td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress1"  size="40" maxlength="30"  tabindex="28" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.mailingCountry" list="%{countryDesc}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();getMailingCountryCode(this);autoPopulate_partner_mailingCountry(this);getMailingState(this);"  tabindex="32"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="36" /></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress2" size="40" maxlength="30"  tabindex="29" /></td>
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState'/></td>
									<td><s:select id="partnerAddForm_partner_mailingState"  name="partner.mailingState" cssClass="list-menu"list="%{mstates}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();"  tabindex="33" /></td>
									
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)"  tabindex="37"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress3" size="40" maxlength="30"  tabindex="30" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingCity" cssStyle="width:160px" maxlength="15" onkeydown="return onlyCharsAllowed(event)"  tabindex="34" /></td>
									
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingTelex" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="38" /></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress4" size="40" maxlength="30" tabindex="31" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)"  tabindex="35" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="30" maxlength="65" tabindex="39" /></td>
									
								</tr>
								<tr>
								<td width="10px"></td>
								<c:if test="${partnerType == 'AG'}">	
								<td align="left" class="listwhitetext" >
								<input type="button" class="cssbutton" onclick="findGeoCode();" name="Find Geo Code" value="Manually Find Geo Code" style="width:175px; height:25px"/>
								</td>
								</c:if>
								<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG'}">
								<td colspan="3" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from terminal address" onClick="copyTermToMail();changeStatus();" /></td>
								<td colspan="2" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></td>
								</c:if>		
						<c:if test="${partnerType == 'AG'}">			
							<tr>
							
					    	<td></td>
					    	<td colspan="6" style="margin: 0px;padding: 0px;" >
					    	<table style="margin: 0px" >
					    	<tr>
						    <td align="left" class="listwhitetext" >
								<input type="button" class="cssbutton" onclick="loadGoogleGeoCode();" name="Find Geo Code" value="Find Geo Code" style="width:105px; height:25px"/>
							</td>
							<td align="right" class="listwhitetext" width="">Latitude</td>
						    <td><s:textfield cssClass="input-text" name="partner.latitude"  size="18"/></td>
						    <td align="right" class="listwhitetext" width="">Longitude</td>
						    <td><s:textfield cssClass="input-text" name="partner.longitude"  size="18"/></td>
						    <td align="right" class="listwhitetext" width=""></td>
						    <td align="left" class="listwhitetext">
								<input type="button" class="cssbutton" onclick="mapLocation();" name="Confirm Map Location" value="Confirm Map Location" style="width:145px; height:25px"/>
							</td>
							</tr>
						
							</table>
							</td>
						</tr>
						<tr>
						<td></td>
						<td colspan="8" style="margin: 0px;padding: 0px;"><table style="margin: 0px">
							<tr>
							<td align="right" class="listwhitetext" width="">Website Url: </td>
							<td align="left"><s:textfield cssClass="input-text" name="partner.url"  size="60"/></td>
							<td align="left"><input type="button" class="cssbutton" onclick="return openWebSite();"  value="Go to site" style="width:75px; height:25px" />
							<td align="right" class="listwhitetext" width="28">&nbsp;</td>
							<td align="right" class="listwhitetext" width="102">Year Established</td>
							<td align="left"><s:textfield cssClass="input-text" name="partner.yearEstablished"  size="5" maxlength="4" onchange="onlyNumeric(this)"/></td>
							</tr>
						</table>
						</td></tr>
						</c:if>
								<c:if test="${partnerType == 'PP' || partnerType == 'AC'  || partnerType == 'OO' || partnerType == ''}">
								<td colspan="5" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></td>
								</c:if>
							    <tr>
							    
							    
							
							<c:if test="${partnerType == 'VN'}">	
							<tr>
							<td width="10px"></td>
								<td align="left" class="listwhitetext" colspan="6">Type&nbsp;of&nbsp;Vendor&nbsp;
								<s:select cssClass="list-menu" name="partner.typeOfVendor" list="%{typeOfVendor}" cssStyle="width:197px"  headerKey="" headerValue="" onchange="changeStatus();" /></td>
								
							</tr>
							</c:if>
							<c:if test="${partnerType == 'PP'}">
								<tr>
								<td width="10px"></td>
									
									<td align="left" class="listwhitetext"><fmt:message key='partner.storageBillingGroup'/>
									<s:select cssClass="list-menu" name="partner.storageBillingGroup" list="%{storageBillingGroup}" cssStyle="width:197px"  headerKey="" headerValue="" onchange="changeStatus();" /></td>
								</tr>
							</c:if>
						</tbody>
					</table>
					</td>
					<td valign="top">
					<div id="map" style="width: 200px; height: 200px"></div>
					</td>
					</tr>
					</table>
					
					
					
					
					<table class="detailTabLabel" border="0" cellpadding="3" cellspacing="0" width="100%">
						<tbody>
								<c:if test="${partnerType == 'OO'}" >
								<tr><td align="center" colspan="7" class="vertlinedata"></td></tr>
								<tr><td class="subcontenttabChild" colspan="7">&nbsp;Driver</td></tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" width="80px"><fmt:message key='partner.driverAgency'/></td>
									<td><s:textfield cssClass="input-text" name="partner.driverAgency"  size="15" maxlength="8" /></td>
									<td align="right" class="listwhitetext">VL&nbsp;Code</td>
									<td><s:textfield cssClass="input-text" name="partner.validNationalCode" maxlength="15" size="15" /></td>
								
								</tr>
								
								<tr>
										
									<td align="right" class="listwhitetext">Account&nbsp;Owner</td>
									<td><s:select cssClass="list-menu"  name="partner.salesMan" headerKey=" " headerValue=" " list="%{sale}" cssStyle="width:130px" /></td>
									
									<c:set var="ischecked" value="false"/>
									<c:if test="${partner.invoiceUploadCheck}">
										<c:set var="ischecked" value="true"/>
									</c:if>
									<td align="right" class="listwhitetext">Don't send to invoice upload</td>
									<td  class="listwhitetext" width="8px" ><s:checkbox key="partner.invoiceUploadCheck" onclick="changeStatus();"  value="${ischecked}" fieldValue="true" tabindex="18"/></td>
					    			
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Long%</td>
									<td><s:textfield cssClass="input-text" name="partner.longPercentage" size="15" maxlength="10" /></td>
										
									<c:set var="ischeck" value="false"/>
									<c:if test="${partner.payableUploadCheck}">
										<c:set var="ischeck" value="true"/>
									</c:if>
									<td align="right" class="listwhitetext">Don't send to payable upload</td>
									<td  class="listwhitetext" width="8px" ><s:checkbox key="partner.payableUploadCheck" onclick="changeStatus();"  value="${ischeck}" fieldValue="true" tabindex="18"/></td>
								</tr>
								<tr>
			                     <td></td>
			                     <td></td>
			                      <td align="right" class="listwhitetext" width="">Don't generate unauthorized invoice</td>
		                          <td  class="listwhitetext" width="" ><s:checkbox key="partner.stopNotAuthorizedInvoices" onclick="changeStatus();"  value="${partner.stopNotAuthorizedInvoices}" fieldValue="true" tabindex="18"/></td>
			                      </tr>
			                      <tr>
			                      <td></td>
			                     <td></td>
			                      <td align="right" class="listwhitetext" width="">Don't copy auth # from previous S/O#</td>
		                          <td  class="listwhitetext" width="" ><s:checkbox key="partner.doNotCopyAuthorizationSO" onclick="changeStatus();"  value="${partner.doNotCopyAuthorizationSO}" fieldValue="true" tabindex="18"/></td>
			                    </tr>
								
								<tr><td height="5px"></td></tr>		
													
							</c:if>
						</tbody>				
					</table>
					<table width="100%" style="margin-bottom:0px;">
					<tbody>
					<c:if test="${partnerType == 'OO' }" >
					<tr>
							<td align="center" colspan="15" class="vertlinedata"></td>
						</tr> 
						<tr><td height="10px"></td></tr>
					<tr>
				
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isPrivateParty}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext"  width="100px">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
										
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAccount}">
						<c:set var="ischecked" value="true"/>
						</c:if>	
											
						<td align="center" class="listwhitetext"  width="100px"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
						
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAgent}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext"  width="100px"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
													
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isVendor}">
						<c:set var="ischecked" value="true"/>
						</c:if>					
						<td align="center" class="listwhitetext"  width="100px"><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
														
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isCarrier}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext"  width="100px"><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
								
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isOwnerOp}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext"  width="100px"><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
						</tr>
					
				</c:if>						
						</tbody>			
					</table>
		

<tr>
<td height="" align="left" class="listwhitetext" colspan="2">
					<c:set var="isAccountFlag" value="false"/>
					<c:set var="isAgentFlag" value="false"/>
					<c:set var="isBrokerFlag" value="false"/>
					<c:set var="isVendorFlag" value="false"/>
					<c:set var="isCarrierFlag" value="false"/>
					<c:set var="isOwnerOpFlag" value="false"/>
					<c:set var="qcFlag" value="false"/>
					<c:set var="privateFlag" value="false"/>
					<c:set var="seaFlag" value="false" />
					<c:set var="surfaceFlag" value="false" />
					<c:set var="airFlag" value="false" />
					<c:if test="${partner.isAccount}">
						 <c:set var="isAccountFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isAgent}">
						 <c:set var="isAgentFlag" value="true"/>
					</c:if>
					
					<c:if test="${partner.isVendor}">
						 <c:set var="isVendorFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isCarrier}">
						 <c:set var="isCarrierFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isOwnerOp}">
						 <c:set var="isOwnerOpFlag" value="true"/>
					</c:if>
					<c:if test="${partner.qc}">
						 <c:set var="qcFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isPrivateParty}">
						 <c:set var="privateFlag" value="true"/>
					</c:if>
					<c:if test="${partner.sea}">
						 <c:set var="seaFlag" value="true"/>
					</c:if>
					<c:if test="${partner.surface}">
						 <c:set var="surfaceFlag" value="true"/>
					</c:if>
					<c:if test="${partner.air}">
						 <c:set var="airFlag" value="true"/>
					</c:if>
<c:choose>
	<c:when test="${partnerType == 'CR'}">		
		<s:hidden name="partner.isCarrier" value="true" />
		<s:hidden name="partner.companyDivision" />
		<s:hidden name="partner.typeOfVendor" />
		<table style="margin-bottom: 0px;width: 100%;">
		<tbody>
		<tr>
		<td colspan="4" align="left">
						<table border="0" style="">
						<tr>
							<td align="right" class="listwhitetext" width="102">Tracking Url: </td>
							<td align="left"><s:textfield cssClass="input-text" name="partner.trackingUrl"  size="60" maxlength="150"/></td>
						</tr>
							<tr>
								
								<!--<td align="right" class="listwhitetext"><fmt:message key='partner.client7'/></td>
								<td><s:textfield cssClass="input-text" name="partner.client7"  size="25" maxlength="15"/></td>
							--></tr>
						</table>
						<table style="">
								<tr>
								<td width="10px"></td>
								<td class="listwhitetext"><b>Carrier Detail</b></td>
								<td style="margin-left:30px" class="listwhitetext"><s:checkbox key="partner.sea" value="${seaFlag}" fieldValue="true" tabindex="41" />Sea</td>
								<td style="margin-left:10px" class="listwhitetext"><s:checkbox key="partner.surface" value="${surfaceFlag}" fieldValue="true" tabindex="42" />Surface</td>
								<td style="margin-left:10px" class="listwhitetext"><s:checkbox key="partner.air" value="${airFlag}" fieldValue="true" tabindex="43"/>Air</td>
							</tr>
							
						</table>
			
			<c:if test="${ partnerType == 'CR'}" >
				
			<tr>
									<td align="center" colspan="15" class="vertlinedata"></td>
								</tr>
								<tr> <td height="10px"></td></tr>
							<tr>
								
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isPrivateParty}">
								<c:set var="ischecked" value="true"/>
								</c:if>						
								<td align="center" class="listwhitetext" >Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
												
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isAccount}">
								<c:set var="ischecked" value="true"/>
								</c:if>						
								<td align="left" class="listwhitetext2" ><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
								
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isAgent}">
								<c:set var="ischecked" value="true"/>
								</c:if>						
								<td align="left" class="listwhitetext2" ><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
															
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isVendor}">
								<c:set var="ischecked" value="true"/>
								</c:if>					
								<td align="left" class="listwhitetext2" ><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
																
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isCarrier}">
								<c:set var="ischecked" value="true"/>
								</c:if>						
								<td align="left" class="listwhitetext2" ><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
										
								<c:set var="ischecked" value="false"/>
								<c:if test="${partner.isOwnerOp}">
								<c:set var="ischecked" value="true"/>
								</c:if>						
								<td align="center" class="listwhitetext2" ><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
							</tr>
							
							
						</c:if>
			
						
		</td>
		</tr>
		
		</tbody>
		</table>
	</c:when>
	<c:when test="${partnerType == 'AC'}">
		<s:hidden name="partner.isPrivateParty"/>
		<s:hidden name="partner.isAccount" value="true" />
		<s:hidden name="partner.isAgent"  />
		<s:hidden name="partner.isVendor"  />
		<s:hidden name="partner.isCarrier" />
		<s:hidden name="partner.isOwnerOp" />	
			
		<s:hidden name="partner.qc"/>
		<s:hidden name="partner.sea"/>
		<s:hidden name="partner.surface"/>
		<s:hidden name="partner.air"/>
		<s:hidden name="partner.typeOfVendor" />
	</c:when>
	<c:when test="${partnerType == 'PP'}">
		<s:hidden name="partner.isAccount"  />
		<s:hidden name="partner.isAgent"  />
		<s:hidden name="partner.isVendor"  />
		<s:hidden name="partner.isCarrier"  />
		<s:hidden name="partner.isOwnerOp"  />
		<s:hidden name="partner.isPrivateParty" value="true" />
		<s:hidden name="partner.status" value="Approved" />
		
		<s:hidden name="partner.typeOfVendor" />
			
		<s:hidden name="partner.qc"/>
		<s:hidden name="partner.sea"/>
		<s:hidden name="partner.surface"/>
		<s:hidden name="partner.air"/>
	</c:when>
	<c:when test="${partnerType == 'OO'}">
		<s:hidden name="partner.isOwnerOp" value="true" />
		<s:hidden name="partner.companyDivision"/>
		<s:hidden name="partner.creditAuthorizedBy"/>
		<s:hidden name="partner.abbreviation"/>
		<s:hidden name="partner.accountHolder"/>
		<s:hidden name="partner.paymentMethod"/>
		<s:hidden name="partner.billingInstruction"/>
		
		<s:hidden name="partner.payOption"/>
		<s:hidden name="partner.multiAuthorization"/>
		<s:hidden name="partner.pricingUser"/>
		<s:hidden name="partner.billingUser"/>
		<s:hidden name="partner.payableUser"/>
		<s:hidden name="partner.creditTerms"/>
		<c:if test="${not empty partner.effectiveDate}">
			<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
			<td><s:hidden cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
		</c:if>
		
		<s:hidden name="partner.billPayType"/>
		<s:hidden name="partner.billPayOption"/>
		
			
		<s:hidden name="partner.qc"/>
		<s:hidden name="partner.sea"/>
		<s:hidden name="partner.surface"/>
		<s:hidden name="partner.air"/>
		<s:hidden name="partner.typeOfVendor" />
	</c:when>
	
	<c:when test="${partnerType == 'AG' || partnerType == 'VN'}">
				
		<s:hidden name="partner.nationalAccount"/>
		<s:hidden name="partner.billingInstructionMessage"/>
		
		<s:hidden name="partner.warehouse"/>
		
		<s:hidden name="partner.commitionType"/>
		<s:hidden name="partner.coordinator"/>
		<s:hidden name="partner.billToGroup"/>
		<c:if test="${partnerType == 'VN'}">
			<s:hidden name="partner.isAgent" />
			<s:hidden name="partner.isAccount" />
			<s:hidden name="partner.isVendor" value="true" />
			<s:hidden name="partner.isCarrier" />
			<s:hidden name="partner.isOwnerOp"  />
		</c:if>
		<c:if test="${partnerType == 'AG'}">
			<s:hidden name="partner.isAgent" value="true" />
			<s:hidden name="partner.isVendor"/>
			<s:hidden name="partner.isCarrier" />
			<s:hidden name="partner.isOwnerOp"  />
			<s:hidden name="partner.isAccount" />
			<s:hidden name="partner.typeOfVendor" />
		</c:if>
		<s:hidden name="partner.isPrivateParty"/>
		<s:hidden name="partner.isCarrier"  />
		<s:hidden name="partner.isOwnerOp"  />

		<s:hidden name="partner.companyDivision"/>
		
			
		<s:hidden name="partner.qc"/>
		<s:hidden name="partner.sea"/>
		<s:hidden name="partner.surface"/>
		<s:hidden name="partner.air"/>
	</c:when>
	<c:otherwise>
	
	</c:otherwise>
</c:choose>
</td>
</tr>
<c:if test="${partnerType == 'AG'}">
<tr>

 <td height="10" align="left" class="listwhitetext" style="margin: 0px;">
 <div  onClick="javascript:animatedcollapse.toggle('pics'); secImage1();secImage2();secImage3(); secImage4()" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Photographs
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
					
<div id="pics" >


	<table border="0" cellpadding="1" cellspacing="1" style="margin:0px;" >
		<tbody>
			<tr>
				<td colspan="2">
					<table style="margin:0px;" border="0" >
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<c:if test="${!(partner.location1 == null || partner.location1 == '')}">
								<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" class="urClass" src="UserImage?location=${partner.location1}" alt="" style="border:thin solid #219DD1;"/></td>
							</c:if>
							<c:if test="${partner.location1 == null || partner.location1 == ''}">
							 	<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110"  style="border:thin solid #219DD1;"/></td>
							</c:if>
						</tr>
						<tr>
							<c:if test="${partner.location1 == null || partner.location1 == ''}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph1</td>
							</c:if>
							<c:if test="${!(partner.location1 == null || partner.location1 == '')}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph1</td>
							</c:if>
							<td align="left" valign="bottom" class="listwhitetext" ><s:file name="file" cssClass="text file" required="true"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();"/></td>				
					  		
					    </tr>
				   </table>
			   </td>
			  <td>
				  <table>
			 		<tr>
						<c:if test="${!(partner.location2 == null || partner.location2 == '')}">
							<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" class="urClass" src="UserImage?location=${partner.location2}" alt=""  style="border:thin solid #219DD1;"/></td>
						</c:if>
						<c:if test="${partner.location2 == null || partner.location2 == ''}">
						 	<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110"  style="border:thin solid #219DD1;"/></td>
						</c:if>
					</tr>
					<tr>
						<c:if test="${partner.location2 == null || partner.location2 == ''}">
						 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph2</td>
						</c:if>
						<c:if test="${!(partner.location2 == null || partner.location2 == '')}">
						 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph2</td>
						</c:if>
						<td align="left" valign="bottom" class="listwhitetext" ><s:file name="file1" cssClass="text file" required="true"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph2();"/></td>				
				  	</tr>
				 </table>
			  </td>
			</tr>
			    
			<tr>
			  	<td>
				  	<table>
				    	<tr>
							<c:if test="${!(partner.location3 == null || partner.location3 == '')}">
								<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" class="urClass" src="UserImage?location=${partner.location3}" alt="" style="border:thin solid #219DD1;"/></td>
							</c:if>
							<c:if test="${partner.location3 == null || partner.location3 == ''}">
							 	<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110"  style="border:thin solid #219DD1;"/></td>
							</c:if>
						</tr>
						<tr>
							<c:if test="${partner.location3 == null || partner.location3 == ''}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph3</td>
							</c:if>
							<c:if test="${!(partner.location3 == null || partner.location3 == '')}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph3</td>
							</c:if>
							<td align="left" valign="bottom" class="listwhitetext" ><s:file name="file2" cssClass="text file" required="true"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph3();"/></td>				
					  		
					    </tr>
				   	</table>
				</td>
				<td>
			    	<table>
					    <tr>
							<c:if test="${!(partner.location4 == null || partner.location4 == '')}">
								<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" class="urClass" src="UserImage?location=${partner.location4}" alt=""  style="border:thin solid #219DD1;"/></td>
							</c:if>
							<c:if test="${partner.location4 == null || partner.location4 == ''}">
							 	<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110"  style="border:thin solid #219DD1;"/></td>
							</c:if>
						</tr>
						<tr>
							<c:if test="${partner.location4 == null || partner.location4 == ''}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph4</td>
							</c:if>
							<c:if test="${!(partner.location4 == null || partner.location4 == '')}">
							 	<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph4</td>
							</c:if>
							<td align="left" valign="bottom" class="listwhitetext" ><s:file name="file3" cssClass="text file" required="true"/><input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph4();"/></td>				
					  	</tr> 
				    </table>
			    </td>
			</tr>
			</table>
			</div>
			</td>
			</tr>
			
			
			<tr>

 <td height="10" align="left" class="listwhitetext" style="margin: 0px;">
 <div  onClick="javascript:animatedcollapse.toggle('description')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Description
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
					
<div id="description" >
				    <table>
					    <tr>
						    <td align="right" valign="top" class="listwhitetext" width="100">Company Profile</td>
						    <td align="left" ><s:textarea cssClass="textarea" name="partner.companyProfile"  rows="4" cols="118" readonly="false" onkeydown="return checkLength();"/></td>
						</tr>
						<tr>
						    <td align="right" valign="top" class="listwhitetext" width="100">Facilities</td>
						    <td align="left" ><s:textarea cssClass="textarea" name="partner.companyFacilities"  rows="4" cols="118" readonly="false" onkeydown="return checkLength();"/></td>
						</tr>
						<tr>
						    <td align="right" valign="top" class="listwhitetext" width="100">Capabilities</td>
						    <td align="left" ><s:textarea cssClass="textarea" name="partner.companyCapabilities"  rows="4" cols="118" readonly="false" onkeydown="return checkLength();"/></td>
						</tr>
						<tr>
						    <td align="right" valign="top" class="listwhitetext" width="100">Destination Profile</td>
						    <td align="left" ><s:textarea cssClass="textarea" name="partner.companyDestiantionProfile"  rows="4" cols="118" readonly="false" onkeydown="return checkLength();"/></td>
						</tr>
					</table>
					</div>
				</td>
			</tr>	
			<tr>
			    <td height="10" align="left" class="listwhitetext" style="margin: 0px;">
 <div  onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Information
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
					
<div id="info" >
<table width="100%">
<tr><td valign="top">
				    <table border="0">
					    
				
				
			<tr>
			<td colspan="9" width="100%" valign="top">
				    <table width="100%" cellspacing="0" cellpadding="2">
					   	
						 <tr height="5px"></tr>
						<tr>
							<%-- 
							<td width="150px" class="listwhitetext" align="right" >Service Range</td>
						    <td width=""><s:textfield cssClass="input-text" name="partner.serviceRangeKms"  size="18" onchange="onlyFloat(this);convertToMiles()"/><b>(in Kms)</b></td>
						    <td width=""></td>
						    <td><s:textfield cssClass="input-text" name="partner.serviceRangeMiles"  size="18" onchange="onlyFloat(this);convertToKm()" onkeydown=""/><b>(in Miles)</b></td>
						    --%>
						    <td align="right" valign="top" class="listwhitetext" width="">Facility Size</td>
						    <td><s:textfield cssClass="input-text" name="partner.facilitySizeSQMT"  size="18" onchange="onlyFloat(this);convertToFeet()"/><b>(in SQMT)</b></td>
						    <td align="right" valign="top" class="listwhitetext" width="">Facility Size</td>
							<td><s:textfield cssClass="input-text" name="partner.facilitySizeSQFT"  size="18" onchange="onlyFloat(this);convertToMeters()"/><b>(in SQFT)</b></td>
						    
						</tr>
						<tr>
						    <td align="right" valign="top" class="listwhitetext" width="">FIDI</td>
						    <td><s:select cssClass="list-menu" name="partner.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
						    <td align="right" valign="top" class="listwhitetext" width="">OMNI Number</td>
						    <td><s:textfield cssClass="input-text" name="partner.OMNINumber"  size="18"/></td>
						  	
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="">AMSA</td>
						    <td><s:select cssClass="list-menu" name="partner.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
						    <td align="right" valign="top" class="listwhitetext" width="">WERC</td>
						    <td><s:select cssClass="list-menu" name="partner.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
						    <td align="right" valign="top" class="listwhitetext" width="">IAM</td>
						    <td><s:select cssClass="list-menu" name="partner.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="">VanLine Affiliation(s)</td>
							<td><s:select cssClass="list-menu" name="partner.vanLineAffiliation" list="%{vanlineAffiliations}" value="%{multiplVanlineAffiliations}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3" /></td>
							<td align="right" valign="top" class="listwhitetext" width="">Service Lines</td>
							<td><s:select cssClass="list-menu" name="partner.serviceLines" list="%{serviceLines}" multiple="true" value="%{multiplServiceLines}" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3" /></td>
							<td align="right" valign="top" class="listwhitetext" width="">Quality Certifications</td>
							<td><s:select cssClass="list-menu" name="partner.qualityCertifications" list="%{qualityCertifications}" value="%{multiplequalityCertifications}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3"/></td>
						</tr>
					</table>
					</td>
					
					</tr>
					</table>
					</div>
				</td>
			</tr>
			 
		</tbody>
	</table>
</c:if>
<tr>
	<td height="20" align="left" class="listwhitetext"></td>
</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>


<table border="0" style="width:750px;">
	<tbody>
		<tr>
		<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
			<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partner.createdOn}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="partner.createdOn" value="${containerCreatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>	
				
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
			<c:if test="${not empty partner.id}">
					<s:hidden name="partner.createdBy"/>
					<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
			</c:if>
			<c:if test="${empty partner.id}">
					<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
					<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
			<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partner.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="partner.updatedOn" value="${containerUpdatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
					
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
			<c:if test="${not empty partner.id}">
				<s:hidden name="partner.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty partner.id}">
				<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>  

<c:set var="button1">
         <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:70px; height:25px " onclick="return validateForm();"/>
        <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
	        <c:if test="${not empty partner.id}">
	         	<input type="button" class="cssbutton" onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'" value="<fmt:message key="button.add"/>" style="width:70px; height:25px"/>
	       	</c:if>
       		<c:if test="${empty partner.id}">
	         	<input type="button" class="cssbutton" disabled onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  value="<fmt:message key="button.add"/>" style="width:70px; height:25px" />
	       	</c:if>
       	</sec-auth:authComponent>
        <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px "/>   
</c:set>
    
</div>
<c:out value="${button1}" escapeXml="false" /> 
<c:set var="isTrue" value="false" scope="session"/>
<s:hidden name="partner.acctDefaultJobType" />
<s:hidden name="partner.accountingDefault" />
<c:if test="${partnerType == 'AG' || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN' || partnerType == 'AC'}">
	<s:hidden name="partner.invoiceUploadCheck"/>
	<s:hidden name="partner.payableUploadCheck"/>
	<s:hidden name="partner.stopNotAuthorizedInvoices"/>
	<s:hidden name="partner.doNotCopyAuthorizationSO"/> 
	<s:hidden name="partner.creditAuthorizedBy"/>
	<s:hidden name="partner.abbreviation"/>
	<s:hidden name="partner.accountHolder"/>
	<s:hidden name="partner.paymentMethod"/>
	<s:hidden name="partner.billingInstruction"/>
	
	<s:hidden name="partner.validNationalCode"/>
	
	<s:hidden name="partner.payOption"/>
	<s:hidden name="partner.multiAuthorization"/>
	<s:hidden name="partner.pricingUser"/>
	<s:hidden name="partner.billingUser"/>
	<s:hidden name="partner.payableUser"/>
	<s:hidden name="partner.creditTerms"/>
	<c:if test="${not empty partner.effectiveDate}">
		<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
		<td><s:hidden cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
	</c:if>
	<s:hidden name="partner.rank"/>
	<s:hidden name="partner.billPayType"/>
	<s:hidden name="partner.billPayOption"/>
	
	<s:hidden name="partner.driverAgency"/>
	
	<s:hidden name="partner.longPercentage"/>
	<s:hidden name="partner.salesMan"/>	

</c:if>
<c:if test="${partnerType == 'AG' || partnerType == 'CR' || partnerType == 'AC' || partnerType == 'VN' || partnerType == 'OO'}">
	<s:hidden name="partner.storageBillingGroup"/>
</c:if>

<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO'}">
 <s:hidden name="partner.terminalAddress1" />
 <s:hidden name="partner.terminalAddress2" />
 <s:hidden name="partner.terminalAddress3" /> 
 <s:hidden name="partner.terminalAddress4" />
 <s:hidden name="partner.terminalEmail" />
 <s:hidden name="partner.terminalCity" />
 <s:hidden name="partner.terminalFax" />
 <s:hidden name="partner.terminalZip" />
 <s:hidden name="partner.terminalCountry" />
 <s:hidden name="partner.terminalPhone" />
 <s:hidden name="partner.terminalTelex" />
 <s:hidden name="partner.terminalState" />
</c:if>
<c:if test="${partnerType == 'OO' || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN' || partnerType == 'AC'}">
<s:hidden name="partner.url" />
<s:hidden name="partner.latitude" />
<s:hidden name="partner.longitude" />

<s:hidden name="partner.companyProfile" />
<s:hidden name="partner.yearEstablished" />
<s:hidden name="partner.companyFacilities" />
<s:hidden name="partner.companyCapabilities" />
<s:hidden name="partner.companyDestiantionProfile" />
<s:hidden name="partner.serviceRangeKms" />
<s:hidden name="partner.serviceRangeMiles" />
<s:hidden name="partner.fidiNumber" />
<s:hidden name="partner.OMNINumber" />
<s:hidden name="partner.IAMNumber" />
<s:hidden name="partner.AMSANumber" />
<s:hidden name="partner.WERCNumber" />
<s:hidden name="partner.facilitySizeSQFT" />
<s:hidden name="partner.facilitySizeSQMT" />
<s:hidden name="partner.qualityCertifications" />
<s:hidden name="partner.vanLineAffiliation" />
<s:hidden name="partner.serviceLines" />
</c:if>
  	 
</s:form>
<script type="text/javascript">
try{
secImage1();
}
catch(e){}
try{
secImage2();
}
catch(e){}
try{
secImage3();
}
catch(e){}

try{
secImage4();
}
catch(e){}
//loadGoogleGeoCode();
try{
var param = '<%=session.getAttribute("paramView")%>';
if(param == 'View'){
	document.forms['partnerAddForm'].elements['partner.status'].disabled = true;
}
}
catch(e){}
try{
var  permissionTest  = "";
	<sec-auth:authComponent componentId="module.field.partner.status">  
		  permissionTest  = <%=(Integer)request.getAttribute("module.field.partner.status" + "Permission")%>;
	</sec-auth:authComponent>
	if(permissionTest > 2){
		document.forms['partnerAddForm'].elements['partner.status'].disabled = false;
	}else{
		document.forms['partnerAddForm'].elements['partner.status'].disabled = true;
	}
}
catch(e){}
</script>
<script type="text/javascript">
	try{
	autoPopulate_partner_billingCountry(document.forms['partnerAddForm'].elements['partner.billingCountry']);
	}
	catch(e){}
	try{
	getBillingState(document.forms['partnerAddForm'].elements['partner.billingCountry']); 
    }
    catch(e){}
 </script> 
 <script type="text/javascript">  
 	try{
 	autoPopulate_partner_terminalCountry(document.forms['partnerAddForm'].elements['partner.terminalCountry']);
	}
	catch(e){}
	try{
	getTerminalState(document.forms['partnerAddForm'].elements['partner.terminalCountry']); 
	}
	catch(e){}
</script>     
<script type="text/javascript">  
	try{
	autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);        
	}
	catch(e){}
	try{
	getMailingState(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
	}
	catch(e){}
</script>

