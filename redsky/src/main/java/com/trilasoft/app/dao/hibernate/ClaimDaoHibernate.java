/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "Claim" objects in Redsky that allows for Claim management.
 * @Class Name	ClaimDaoHibernate
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * Extended by GenericDaoHibernate to  implement ClaimDao
 */

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import com.trilasoft.app.dao.ClaimDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Claim;
   

public class ClaimDaoHibernate extends GenericDaoHibernate<Claim, Long> implements ClaimDao {   
	private List claims;
	private HibernateUtil hibernateUtil;

    public ClaimDaoHibernate() {   
        super(Claim.class);   
    }
    public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
    
    protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
    
   public Long findMaximum(String corpId) {
	   Long newClaimNumber = null;
   	//Transaction tx = null;
       try {
           //tx = getHibernateTemplate().getSessionFactory().getCurrentSession().beginTransaction();

           Long lastClaimNumber = (Long) this.getSession().createSQLQuery(
                   "Select lastClaimNumber From company where corpID = '"
                           + corpId + "' FOR UPDATE;").addScalar(
                   "lastClaimNumber", Hibernate.LONG).list().get(0);

           getHibernateTemplate().bulkUpdate(
                   "Update Company set lastClaimNumber="
                           + (lastClaimNumber + 1) + " where corpID = '"
                           + corpId + "'");
           if (lastClaimNumber == null || lastClaimNumber==0 || lastClaimNumber.equals("")) {
        	   newClaimNumber = Long.parseLong("1");
           } else {
           	newClaimNumber = lastClaimNumber + 1;
           }  
   }
       catch (Exception e) { 
           e.printStackTrace();
       }
       return newClaimNumber;
    	//return getHibernateTemplate().find("select max(claimNumber) from Claim");
    }
   
   public Long findMaximumForOtherCorpid(String corpId){
	   Long newClaimNumber = null;
	   Long lastClaimNumber = null;
	   //	Transaction tx = null;
	       try {
	          // tx = getHibernateTemplate().getSessionFactory().getCurrentSession().beginTransaction();

	           List l1 =  this.getSession().createSQLQuery("Select lastClaimNumber From company where corpID = '" + corpId + "' ").list();
	           if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
	        	   lastClaimNumber = new Long (l1.get(0).toString()); 
	           }
	          
	           int i =  this.getSession().createSQLQuery("Update company set lastClaimNumber="+ (lastClaimNumber + 1) + " where corpID = '"+ corpId + "'").executeUpdate();
	           if (lastClaimNumber == null || lastClaimNumber==0 || lastClaimNumber.equals("")) {
	        	   newClaimNumber = Long.parseLong("1");
	           } else {
	           	newClaimNumber = lastClaimNumber + 1;
	           }  
	   }
	       catch (Exception e) { 
	           e.printStackTrace();
	       }
	       return newClaimNumber;
	    	//return getHibernateTemplate().find("select max(claimNumber) from Claim");
	    }
   public String getClaimPersonName(String claimPerson, String sessionCorpID){
	   String username ="";
	   List l1 = this.getSession().createSQLQuery("SELECT alias FROM app_user where username = '"+claimPerson+"' ").list();
	   if(l1!=null && !l1.isEmpty() && l1.get(0)!=null && !l1.get(0).toString().equals("") ){
		   username = l1.get(0).toString();
	   }else{
		   username = claimPerson;
	   }
	   return username;
   }
   
    public List<Claim> findByShipNumber(String shipNumber) {
    	List claims= null;
        String columns = "id,claimNumber,lastName,firstName,shipNumber,createdOn,formSent,formRecived,cancelled,reciveReimbursement";
        claims= getHibernateTemplate().find("select " + columns +  " from Claim ");
        try {
            claims= hibernateUtil.convertScalarToComponent(columns, claims, Claim.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return claims;
    }  
    public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from Claim");
    } 
    public List<Claim>  findClaims(Long claimNumber, String lastName, String shipNumber , Date closeDate,String registrationNumber,String claimPerson,BigInteger origClmsAmt,String corpId) {
    	lastName = (lastName == null) ? "" : lastName.replaceAll("'", "''").replaceAll(":", "''");
    	shipNumber = (shipNumber == null)? "" : shipNumber.replaceAll("'", "").replaceAll(":", "").replaceAll("-", "").trim();
    	String claimNum = (claimNumber != null)? " and claimNumber like '%"+claimNumber+"%'" : "";
    	registrationNumber = (registrationNumber == null)? "" : registrationNumber;
    	//origClmsAmt = (origClmsAmt == null)? new BigInteger("0") : origClmsAmt;
    	claimPerson = (claimPerson == null) ? "" : " and claimPerson like '%"+claimPerson+"%' ";
    	
    	String closeDt="";
    	if(closeDate != null){
    		StringBuilder formatedDate1=new StringBuilder() ;
    		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
    		formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( closeDate ) );
    		
    		closeDt = " AND closeDate like '"+formatedDate1+"%'";
    	}
    	if(origClmsAmt == null)	{
    		return getHibernateTemplate().find("from Claim where corpId='"+corpId+"' and registrationNumber like '%"+registrationNumber+"%'  AND lastName like '%"+lastName+ "%' AND shipNumber like '%"+shipNumber+ "%' "+closeDt+claimNum+claimPerson);
    		
    	}else{
    		return getHibernateTemplate().find("from Claim where corpId='"+corpId+"' and registrationNumber like '%"+registrationNumber+"%'  AND lastName like '%"+lastName+ "%' AND shipNumber like '%"+shipNumber+ "%' "+closeDt+claimNum+claimPerson+" AND clmsClaimNbr like '%"+origClmsAmt+"%'");
    		
    	}
    	
	}
    
	public List claimList(Long sid){
		return getHibernateTemplate().find("from Claim where serviceOrderId='"+sid+"'");
	}
    
	public Long findRemoteClaim(String idNumber, Long serviceOrderId) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from claim where idNumber='"+idNumber+"' and serviceOrderId="+serviceOrderId+"").list().get(0).toString());
	}
	
	
    /* Added By Kunal */
    public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID) {
		return getHibernateTemplate().find("select CONCAT(personClaims,'#') from RefJobType where job = '" + custJobType + "' and companyDivision='" + companyDivision + "'");
	}
    public List checkById(Long id) {
			return getHibernateTemplate().find("select id from Claim where id=?",id);
	}
	public List findUserDetails(String userName){
		return getHibernateTemplate().find("from User where username=?",userName);
	}
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName){
		getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"' , updatedOn = now() ,  password = '"+ passwordNew +"', confirmPassword = '"+ confirmPasswordNew +"', passwordHintQues = '"+ pwdHintQues +"', passwordHint = '"+ pwdHint +"',passwordReset = false where username=?",userName);
	}
	public List findPwordHintQ(String pwdHintQues,String userName){
		return getHibernateTemplate().find("from User where  passwordHintQues = '"+ pwdHintQues +"' and username=?",userName);

	}
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName){
	 getHibernateTemplate().find("from User where passwordHintQues = '"+ pwdHintQues +"', passwordHint = '"+ pwdHint +"'  username=?",userName);
			}
	public List<Claim> findAllClaimList(){
		List claims= null;
        String columns = "id,claimNumber,lastName,firstName,shipNumber,createdOn,formSent,formRecived,cancelled,reciveReimbursement";
        claims= getHibernateTemplate().find("select " + columns +  " from Claim ");
        try {
            claims= hibernateUtil.convertScalarToComponent(columns, claims, Claim.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return claims;
	}
	public List checkByShipNumber(String ship) {
    	return getHibernateTemplate().find("select shipNumber from Claim where shipNumber=?",ship);
    }
	public List checkByCloseDate(String ship) {
    	return getHibernateTemplate().find("select shipNumber from Claim where shipNumber=? and (closeDate is not null or cancelled is not null)",ship);
    }
	public List findMaximumIdNumber(String shipNum) {
		List list=  this.getSession().createSQLQuery("select max(idNumber) from claim where shipNumber='"+shipNum+"'").list();
		return list;
	}
	public int updateSurveyCalData(String startdate,String id)
	{
		  Long idd=new Long(id);
		  int i=0;
		  Date statusDateNew=new Date();
    		try {
    		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");		 	
    		 	statusDateNew =  dateformatYYYYMMDD.parse(startdate);    		 		 	
    	    }catch (Exception e) {
    			e.printStackTrace();
    		} 
    	    try{
    	    	System.out.println("date......................"+startdate);
    	    	System.out.println("id......................"+idd);
    	    	String qry="update calendarfile set surveyDate='"+startdate+"' where id="+idd;
    	    	System.out.println(qry);
    	    	String q1="select surveyDate from calendarfile where   id="+idd;
    	    	List list= this.getSession().createSQLQuery(q1).list();
    	    	System.out.println(q1);
    	    	if(list!=null && !(list.isEmpty())){
    	    		System.out.println("its calander idddd to update............");
		        i=this.getSession().createSQLQuery(qry).executeUpdate();
    	    	}
    	    	String q2="select survey from customerfile where  id="+idd;
    	    	System.out.println(q2);
    	    	List list1= this.getSession().createSQLQuery(q2).list();
    	    	if(list1!=null && !(list1.isEmpty())){
    	    		System.out.println("its CFFFFFF idddd to update............");
    	    		qry="update customerfile set survey='"+startdate+"' where id="+idd;
		        i=this.getSession().createSQLQuery(qry).executeUpdate();
    	    	}
    	    }catch(Exception e){
    	    	System.out.println(e.getMessage());
    	    }
    	    return i;
	}
	
	public List findPartnerCodeOtherCorpID(String insurerCode,String sessionCorpID){
		List list=  this.getSession().createSQLQuery("select isAgent from partner where status <> 'Inactive' and  partnerCode='"+insurerCode+"' and corpid in ('TSFT','"+sessionCorpID+"' ,'"+hibernateUtil.getParentCorpID(sessionCorpID)+"')").list();
		return list;
	}
	
	
	public List userIDHelps(String first, String last, String phone, String email){
		///return getHibernateTemplate().find("SELECT username FROM app_user where first_name like 's%' and last_name like 'd%'and phone_number like '44%'and email like 's%'");
		return getHibernateTemplate().find("SELECT username FROM User where firstName like 's%' and lastName like 'd%'and phoneNumber like '44%'and email like 's%'");
		///return getHibernateTemplate().find("SELECT username FROM User where firstName like '" + first.replaceAll("'", "''").replaceAll(":", "''") + "%' and lastName like '" + last.replaceAll("'", "''").replaceAll(":", "''") + "%' and phoneNumber like '" + phone.replaceAll("'", "''").replaceAll(":", "''") + "%' and email like '" + email.replaceAll("'", "''").replaceAll(":", "''") + "%'");
    }
	public List checkBySirviceOrderId(Long serviceOrderId, String corpId) {
		
		return getHibernateTemplate().find("SELECT count(*) from Claim where corpID='"+corpId+"' and status not in('Cancelled','Closed') and serviceOrderId=?",serviceOrderId);
	}
	public List claimSortOrderByUser(String username){
		return this.getSession().createSQLQuery("select concat(if(defaultSortForClaim is null || defaultSortForClaim='' ,1, defaultSortForClaim),'#', if(sortOrderClaim is null || sortOrderClaim='',1,sortOrderClaim)) from app_user where username='"+username+"'").list();
	}
	public Integer findClaimByValue(String corpId,String shipNumber){
		Integer i = 0;
		String billtocode="";
		List billtocodeValue=new ArrayList();
		String query="select billtocode from customerfile where corpID = '"+corpId+"' and sequencenumber = '"+shipNumber+"' ";
		billtocodeValue  = this.getSession().createSQLQuery(query).list();
		if(billtocodeValue!=null && !billtocodeValue.isEmpty() && billtocodeValue.get(0)!=null){
			billtocode= billtocodeValue.get(0).toString();
		}
		List l1 = getSession().createSQLQuery("select claimBy from partnerprivate where  corpID='"+corpId+"' and partnercode='"+billtocode+"'").list();
		if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
			i = Integer.parseInt( l1.get(0).toString());
		}
		return i;		
	}
	public List findUserByBasedAt(String basedAt,String corpId)
	{
		return this.getSession().createSQLQuery("select location1 from partnerpublic where partnerCode='"+basedAt+"' and corpid='"+corpId+"' and location1 is not null and location1!=''").list();
	}
	public List findUserCompanyDivision(String basedAt,String corpId)
	{
		return this.getSession().createSQLQuery("select companyDivision from partnerprivate where partnerCode='"+basedAt+"' and corpid='"+corpId+"' and companyDivision is not null and companyDivision!=''").list();
	}
}