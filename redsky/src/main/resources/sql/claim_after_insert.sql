delimiter $$

CREATE trigger redsky.trigger_add_history_claim_after AFTER INSERT on redsky.claim

for each row BEGIN

DECLARE fieldNameList LONGTEXT;
DECLARE oldValueList LONGTEXT;
DECLARE newValueList LONGTEXT;
DECLARE BAcode LONGTEXT;
DECLARE HAcode LONGTEXT;
DECLARE OAcode LONGTEXT;
DECLARE DAcode LONGTEXT;
DECLARE OSAcode LONGTEXT;
DECLARE DSAcode LONGTEXT;
DECLARE BRcode LONGTEXT;
DECLARE FRcode LONGTEXT;
DECLARE NPcode LONGTEXT;
DECLARE countso LONGTEXT;


SET fieldNameList = " ";
SET oldValueList = " ";
SET newValueList = " ";
SET BAcode = " ";
SET HAcode = " ";
SET OAcode = " ";
SET DAcode = " ";
SET OSAcode = " ";
SET DSAcode = " ";
SET BRcode = " ";
SET FRcode = " ";
SET NPcode = " ";
SET countso = " ";

call ServiceOrder_UpdateStatus(NEW.shipnumber,NEW.updatedBy);
update sodashboard set claims=NEW.claimNumber where serviceorderid=NEW.serviceorderid;

END $$

delimiter;
