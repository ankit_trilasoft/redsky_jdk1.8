package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ScheduleResourceOperation;

public interface ScheduleResourceOperationDao extends GenericDao<ScheduleResourceOperation, Long>{
	public List findTimeSheet(ScheduleResourceOperation scheduleResourceOperation);
	public List findTruckingOperation(ScheduleResourceOperation scheduleResourceOperation); 

}
