package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TenancyUtilityServiceDao;
import com.trilasoft.app.model.TenancyUtilityService;
import com.trilasoft.app.service.TenancyUtilityServiceManager;


public class TenancyUtilityServiceManagerImpl extends GenericManagerImpl<TenancyUtilityService, Long> implements TenancyUtilityServiceManager{
	TenancyUtilityServiceDao tenancyUtilityServiceDao;
	public TenancyUtilityServiceManagerImpl(TenancyUtilityServiceDao tenancyUtilityServiceDao) {
		super(tenancyUtilityServiceDao);
		this.tenancyUtilityServiceDao = tenancyUtilityServiceDao;
	}
}
