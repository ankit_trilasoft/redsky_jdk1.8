<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />

<script type="text/javascript">
		function setFlagValueMo(){
		  	document.forms['assignItemsForm'].elements['hitFlag'].value = '1';
		}
		
		
		function clear_fields(){
		var i;
			for(i=0;i<=2;i++){
				document.forms['locationList1'].elements['locationId'].value = "";
				document.forms['locationList1'].elements['warehouse'].value = "";
				document.forms['locationList1'].elements['type'].value = "";
				document.forms['locationList1'].elements['occupied'].checked = false;
			}
		}
</script>

<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:1px;margin-top:-8px;!margin-top:-14px;padding:2px 0;text-align:right;width:100%;}
#otabs{margin-bottom:0px;!margin-bottom:-15px;}
</style>
</head>
<s:form id="locationformList" name="locationList1" action="locationRearrange" method="post">
<s:hidden name="id1" value="<%=request.getParameter("id1") %>"/> 
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="userCheck" value="<%=request.getParameter("userCheck") %>" />
	<div id="Layer1" style="width: 100%">
	<s:hidden name="serviceOrder.id" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
			

<table class="" width="100%"  cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>  
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

	<table class="table" width="90%">
		<thead>
			<tr>
				<th><fmt:message key="location.locationId" /></th>
				<th><fmt:message key="location.warehouse" /></th>
				<th><fmt:message key="location.type" /></th>
				<th ></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield name="locationId" required="true" cssClass="input-text" /></td>
				<td><s:textfield name="warehouse" required="true" maxlength="3" cssClass="input-text" /></td>
				<td><s:select name="type" cssClass="list-menu" cssStyle="width:162px" list="%{storageTypeList}" headerKey=""  headerValue=""/></td>
				<s:hidden key="occupied" cssClass="text medium" value="false"/>	
				<td width="130px" align="center"><c:out value="${searchbuttons}" escapeXml="false" /></td>
					
					
			</tr>
		</tbody>
	</table>
	

	<s:set name="locations" value="locations" scope="request" />
	<div id="otabs">
				  <ul>
				    <li><a class="current"><span>List</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
	<display:table name="locations" class="table" requestURI="" id="locationList" defaultsort="2" export="true" pagesize="10">
		<display:column> <input type="radio" name="dd" onclick="check(this)" value="${locationList.locationId}" /></display:column>
		<display:column property="locationId" sortable="true" titleKey="location.locationId" />
	 	<display:column property="capacity" headerClass="containeralign" style="text-align: right;"  sortable="true" titleKey="location.capacity" />
		<display:column property="cubicFeet" headerClass="containeralign" style="text-align: right;"  sortable="true" titleKey="location.cubicFeet" />
		<display:column property="type" sortable="true" titleKey="location.type" />
		
		<display:setProperty name="paging.banner.item_name" value="location" />
		<display:setProperty name="paging.banner.items_name" value="location" />
		<display:setProperty name="export.excel.filename" value="Location List.xls" />
		<display:setProperty name="export.csv.filename" value="Location List.csv" />
		<display:setProperty name="export.pdf.filename" value="Location List.pdf" />
	</display:table>
	</td>
   </tr>
  </tbody>
</table>
</div> 
	<table class="detailTabLabel">
	<tr>
	<td>
	<c:out value="${buttons}" escapeXml="false" />	
</s:form>
<s:form id="assignItemsForm" action="moveStorageRearrange" method="post">
	<s:hidden name="id" value="<%=request.getParameter("id1") %>"/> 
	<s:hidden name="id1" value="<%=request.getParameter("id") %>" />
	<s:hidden name="locationId" />
	<s:hidden name="userCheck" value="<%=request.getParameter("userCheck") %>" />
	<s:hidden name="ticket" value="%{workTicket.ticket}"  required="true" cssClass="text medium"/>
	
	<input type="submit" name="forwardBtn2" class="cssbutton1" disabled value="Move" style="width:67px;" onclick="setFlagValueMo();" />
	
	<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
	<s:hidden name="hitFlag" />
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/bookStorages.html?id=${workTicket.ticket}"  />
	</c:if>
	
</s:form>  
<td></td>
</tr>
</table>

<script type="text/javascript"> 
    highlightTableRows("locationList"); 
</script>
<script type="text/javascript">
		function check(targetElement)
		  {
		  try{
		  document.forms['assignItemsForm'].elements['locationId'].value=targetElement.value;
		  document.forms['assignItemsForm'].elements['forwardBtn2'].disabled = false ;
	
		  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		  }
		  catch(e){}
		  }
	</script>

