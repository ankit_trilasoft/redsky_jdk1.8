package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.InventoryPath;

public interface InventoryPathDao extends GenericDao<InventoryPath, Long>{
	
	public  List getListByInventoryId(Long id);
	public  List getListByAccessInfoId(Long id,String type);
	public List getInventoryPackingImg(Long id);
	public void deleteAllWorkTicketInventory(String field, String corpID,Long fieldVal);
	public List getListByLocId(Long id, String fieldName);
	public void deleteLInkedImgs(Long id);
}
