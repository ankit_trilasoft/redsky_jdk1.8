package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.model.PartnerRateGridContracts;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PartnerRateGridContractsManager;
import com.trilasoft.app.service.PartnerRateGridManager;

public class PartnerRateGridContractsAction extends BaseAction implements Preparable {
	private Long id;
	private Partner partner;
	private PartnerManager partnerManager;
	private PartnerPublicManager partnerPublicManager;
	private PartnerPublic partnerPublic;
	private String sessionCorpID;
	private String corpid;
	private Long partnerRateGridID;
	private Long partnerId;
	private  PartnerRateGrid  partnerRateGrid;
	private  PartnerRateGridManager  partnerRateGridManager;
	private List partnerRateGridContractsList;
	private  PartnerRateGridContracts partnerRateGridContracts;
	private  PartnerRateGridContractsManager partnerRateGridContractsManager;
	private List discountCompanyList=new ArrayList();
	private List contractList=new ArrayList();
	private List contractBillList=new ArrayList();
	private  List tariffScopeList= new ArrayList();
	private  List multipleTariffScope= new ArrayList();
	private  List saveMultipleTariffScope= new ArrayList();
	private  List multipleContractList= new ArrayList();
	private int uniqueCount;
	private String effectiveDate;
	
	
public void prepare() throws Exception { 
		
	}
public String getComboList(String corpID){
	discountCompanyList=partnerRateGridContractsManager.getdDiscountCompanyList();
	contractList=partnerRateGridContractsManager.getContractList(corpID);
	return SUCCESS;
}
    public PartnerRateGridContractsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
    
    public String edit() {  
    	partnerPublic=partnerPublicManager.get(partnerId);
    	partnerRateGrid=partnerRateGridManager.get(partnerRateGridID); 
		if (id != null) {
			partnerRateGridContracts = partnerRateGridContractsManager.get(id);
			multipleContractList=new ArrayList();
			String contract=partnerRateGridContracts.getContract();
			if(contract==null){
				contract="";
			}
			String[] arrayContract = contract.split(",");
			int arrayLength = arrayContract.length;
			for (int i = 0; i < arrayLength; i++) { 
				multipleContractList.add(arrayContract[i].trim()); 
			}
			
			if(partnerRateGridContracts.getDiscountCompany()!=null && (!(partnerRateGridContracts.getDiscountCompany().toString().equals("")))){
				getComboList(partnerRateGridContracts.getDiscountCompany());
			}else{
				getComboList(sessionCorpID);
			}
		} else { 
			getComboList(sessionCorpID);
			partnerRateGridContracts = new PartnerRateGridContracts(); 
			partnerRateGridContracts.setDiscountCompany(sessionCorpID);
			partnerRateGridContracts.setCreatedOn(new Date());
			partnerRateGridContracts.setUpdatedOn(new Date());
			partnerRateGridContracts.setPartnerRateGridID(partnerRateGridID);
		}  
		return SUCCESS;
	}
	public String save() throws Exception { 
		partnerPublic=partnerPublicManager.get(partnerId);
		partnerRateGrid=partnerRateGridManager.get(partnerRateGridID); 
		boolean isNew = (partnerRateGridContracts.getId() == null); 
		if(partnerRateGridContracts.getDiscountCompany()!=null && (!(partnerRateGridContracts.getDiscountCompany().toString().equals("")))){
			getComboList(partnerRateGridContracts.getDiscountCompany());
		}else{
			getComboList(sessionCorpID);
		}
		if (isNew) { 
					partnerRateGridContracts.setCreatedOn(new Date()); 
				}
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		String S= df.format(partnerRateGridContracts.getEffectiveDate()); 
        Date du = new Date();
        du = df.parse(S);
        df = new SimpleDateFormat("yyyy-MM-dd");
        String effectiveDate = df.format(du); 
        StringBuffer contractBuffer = new StringBuffer("");
        String contractCheck=partnerRateGridContracts.getContract();
		if (contractCheck == null || contractCheck.equals("") || contractCheck.equals(",")) {
		} else {
			if (contractCheck.indexOf(",") == 0) {
				contractCheck = contractCheck.substring(1);
			}
			if (contractCheck.lastIndexOf(",") == contractCheck.length() - 1) {
				contractCheck = contractCheck.substring(0, contractCheck.length() - 1);
			}
			String[] arrayContractCheck = contractCheck.split(",");
			int contractArrayLength = arrayContractCheck.length;
			for (int i = 0; i < contractArrayLength; i++) {
				contractBuffer.append("'");
				contractBuffer.append(arrayContractCheck[i].trim());
				contractBuffer.append("'");
				contractBuffer.append(",");
			}
		}
		String contractCheckMultiple = new String(contractBuffer);
		if (!contractCheckMultiple.equals("")) {
			contractCheckMultiple = contractCheckMultiple.substring(0, contractCheckMultiple.length() - 1);
		} else if (contractCheckMultiple.equals("")) {
			contractCheckMultiple = new String("''");
		}
		if (isNew) { 
	    uniqueCount=partnerRateGridContractsManager.findUniqueCount(partnerRateGrid.getId(),sessionCorpID,partnerRateGridContracts.getDiscountCompany(),contractCheckMultiple,effectiveDate);
		}else{
			 uniqueCount=partnerRateGridContractsManager.findUniqueCount(partnerRateGrid.getId(),sessionCorpID,partnerRateGridContracts.getDiscountCompany(),contractCheckMultiple,effectiveDate,partnerRateGridContracts.getId());	
		}
	    if(uniqueCount > 0){
		   String key="This Contract Rate cannot be saved as an existing Contract Rate exists for the same <b>effective date</b> and/or <b>"+partnerRateGridContracts.getContract()+"</b> combination. ";
		   errorMessage(getText(key));
		   multipleContractList=new ArrayList();
		     String contract=partnerRateGridContracts.getContract();
			 if(contract==null){
				 contract="";
			 }
			 String[] arrayContract = contract.split(",");
			 int arrayLength = arrayContract.length;
			 for (int i = 0; i < arrayLength; i++) { 
				 multipleContractList.add(arrayContract[i].trim()); 
		   } 
		   return INPUT;  
	   } else{ 
	   if (contractCheck == null || contractCheck.equals("") || contractCheck.equals(",")) {
				 partnerRateGridContracts.setCorpID(sessionCorpID);
				 partnerRateGridContracts.setUpdatedOn(new Date());
				 partnerRateGridContracts.setUpdatedBy(getRequest().getRemoteUser()); 
				 partnerRateGridContracts=partnerRateGridContractsManager.save(partnerRateGridContracts);  
				 String key = (isNew) ? "Partner Rate matrix - Contract Rates has been added successfully." : "Partner Rate matrix - Contract Rates has been updated successfully.";
				 saveMessage(getText(key));
	   } else {
		        if (contractCheck.indexOf(",") == 0) {
					 contractCheck = contractCheck.substring(1);
			    }
			    if (contractCheck.lastIndexOf(",") == contractCheck.length() - 1) {
					     contractCheck = contractCheck.substring(0, contractCheck.length() - 1);
				 }
				 String[] arrayContractCheck = contractCheck.split(",");
				 int contractArrayLength = arrayContractCheck.length;
				 for (int i = 0; i < contractArrayLength; i++) {
				   partnerRateGridContracts.setCorpID(sessionCorpID);
				   partnerRateGridContracts.setUpdatedOn(new Date());
				   partnerRateGridContracts.setUpdatedBy(getRequest().getRemoteUser());
				   partnerRateGridContracts.setContract(arrayContractCheck[i].trim());
				   partnerRateGridContractsManager.save(partnerRateGridContracts);  
				 }
				 String key = (isNew) ? "Partner Rate matrix - Contract Rates has been added successfully." : "Partner Rate matrix - Contract Rates has been updated successfully.";
				 saveMessage(getText(key));
		   } 
		 multipleContractList=new ArrayList();
	     String contract=partnerRateGridContracts.getContract();
		 if(contract==null){
			 contract="";
		 }
		 String[] arrayContract = contract.split(",");
		 int arrayLength = arrayContract.length;
		 for (int i = 0; i < arrayLength; i++) { 
			 multipleContractList.add(arrayContract[i].trim()); 
		 } 
		list();
		return SUCCESS; 
	   }	
	}
	
	public String list(){
		getComboList(sessionCorpID);
		partnerPublic=partnerPublicManager.get(partnerId);
		partnerRateGrid=partnerRateGridManager.get(partnerRateGridID);
		partnerRateGridContractsList=partnerRateGridContractsManager.getRateGridContractsList(sessionCorpID,partnerRateGridID);
		return SUCCESS; 	
	}
	public String findAgentTariffContract(){
		contractBillList=new ArrayList();
		contractBillList=partnerRateGridContractsManager.getContractList(corpid);
		return SUCCESS; 
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PartnerRateGridContracts getPartnerRateGridContracts() {
		return partnerRateGridContracts;
	}

	public void setPartnerRateGridContracts(
			PartnerRateGridContracts partnerRateGridContracts) {
		this.partnerRateGridContracts = partnerRateGridContracts;
	}

	public Long getPartnerRateGridID() {
		return partnerRateGridID;
	}

	public void setPartnerRateGridID(Long partnerRateGridID) {
		this.partnerRateGridID = partnerRateGridID;
	} 

	public void setPartnerRateGridContractsManager(
			PartnerRateGridContractsManager partnerRateGridContractsManager) {
		this.partnerRateGridContractsManager = partnerRateGridContractsManager;
	}

	public List getPartnerRateGridContractsList() {
		return partnerRateGridContractsList;
	}

	public void setPartnerRateGridContractsList(List partnerRateGridContractsList) {
		this.partnerRateGridContractsList = partnerRateGridContractsList;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public PartnerRateGrid getPartnerRateGrid() {
		return partnerRateGrid;
	}

	public void setPartnerRateGrid(PartnerRateGrid partnerRateGrid) {
		this.partnerRateGrid = partnerRateGrid;
	}

	public void setPartnerRateGridManager(
			PartnerRateGridManager partnerRateGridManager) {
		this.partnerRateGridManager = partnerRateGridManager;
	}
	public List getDiscountCompanyList() {
		return discountCompanyList;
	}
	public void setDiscountCompanyList(List discountCompanyList) {
		this.discountCompanyList = discountCompanyList;
	}
	public List getContractList() {
		return contractList;
	}
	public void setContractList(List contractList) {
		this.contractList = contractList;
	}
	public String getCorpid() {
		return corpid;
	}
	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}
	public List getContractBillList() {
		return contractBillList;
	}
	public void setContractBillList(List contractBillList) {
		this.contractBillList = contractBillList;
	}
	public List getTariffScopeList() {
		return tariffScopeList;
	}
	public void setTariffScopeList(List tariffScopeList) {
		this.tariffScopeList = tariffScopeList;
	}
	public List getMultipleTariffScope() {
		return multipleTariffScope;
	}
	public void setMultipleTariffScope(List multipleTariffScope) {
		this.multipleTariffScope = multipleTariffScope;
	}
	public List getSaveMultipleTariffScope() {
		return saveMultipleTariffScope;
	}
	public void setSaveMultipleTariffScope(List saveMultipleTariffScope) {
		this.saveMultipleTariffScope = saveMultipleTariffScope;
	}
	public List getMultipleContractList() {
		return multipleContractList;
	}
	public void setMultipleContractList(List multipleContractList) {
		this.multipleContractList = multipleContractList;
	}
	public int getUniqueCount() {
		return uniqueCount;
	}
	public void setUniqueCount(int uniqueCount) {
		this.uniqueCount = uniqueCount;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}
	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}
	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}
	
}
