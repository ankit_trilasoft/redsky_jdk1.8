<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>
<script language="javascript" type="text/javascript">
function myDate() {
	
	
		if(document.forms['refMasterForm'].elements['refMaster.stopDate'].value == "null" ){
		document.forms['refMasterForm'].elements['refMaster.stopDate'].value="";
	}
	

	}
</script>

</head>

<s:form id="refMasterForm" action="saveRefMaster" method="post"
	validate="true">
	<s:hidden name="refMaster.id" value="%{refMaster.id}" />
	<div id="Layer1">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0"
		border="0">
		<tbody>
			<tr>
			<td width="0" align="right"><img width="2" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
					<td width="100" class="detailActiveTabLabel content-tab"><a
					href="refMasters.html">Refmaster List</a></td>
				
				<td width="0" align="left"><img width="2" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
			
			
			
				
				
				<td width="4"></td>



			</tr>
		</tbody>
	</table>

	<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>


			<tr>
				<td colspan="5">
				<table cellspacing="3" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td class="listwhite" colspan="2"><FONT color="#0000ff">Set
							up "Look up Item"</FONT></td>
						</tr>
						<tr>
							<td class="listwhite">Code</td>
							<td align="left" ><s:textfield
								name="refMaster.code" size="10" maxlength="20" onfocus="myDate();"
								 /> 
								 <!-- 
				 <s:textfield name="workTicket.date2" size="15"  readonly="true" required="true"
						cssClass="text medium" />
						 --></td>
							<td align="left" class="listwhite">Description</td>
							<td ><s:textfield
								name="refMaster.description" size="50" maxlength="51" 
								 /></td>
							<td align="left" class="listwhite"></td>



						</tr>

						<tr>
						<td align="left" class="listwhite">Field Length</td>
							<td ><s:textfield
								name="refMaster.fieldLength" size="20" maxlength="10" onkeydown="return onlyNumsAllowed(event)"
								 /></td>
							

						</tr>
						<tr>
							<td align="left" class="listwhite">Bucket</td>
							<td ><s:textfield name="refMaster.bucket"
								size="20" maxlength="20" /></td>
							<td align="left" class="listwhite"></td>


						</tr>
						<tr>
							<td align="left" class="listwhite">Bucket2</td>
							<td ><s:textfield name="refMaster.bucket2"
								size="20" maxlength="20" /></td>
							<td align="left" class="listwhite">Number</td>
							<td ><s:textfield name="refMaster.number"
								size="20" maxlength="20" onkeydown="return onlyFloatNumsAllowed(event)"/></td>

						</tr>

<tr>
				<td colspan="5">
				<table cellspacing="3" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td class="listwhite" colspan="2"><FONT color=#0000ff>Other
							Detail</FONT></td>
						</tr>
						<tr>
							<td align="right" class="listwhite">Address</td>
							<td align="left" ><s:textfield
								name="refMaster.address1" size="30" maxlength="30" />
						</tr>

						<tr>
							<td class="listwhite"></td>
							<td align="left" ><s:textfield
								name="refMaster.address2" size="30" maxlength="30" />
							</td>

						</tr>
						<tr>
							<td align="right" class="listwhite">City</td>
							<td ><s:textfield name="refMaster.city"
								size="20" maxlength="20" /></td>
							<td align="left" class="listwhite"></td>


						</tr>
						<tr>
							<td align="right" class="listwhite">State</td>
							<td><s:select name="refMaster.state" 
																	list="%{state}" cssStyle="width:45px" /></td>
							
							<td align="left" class="listwhite">Zip</td>
							<td ><s:textfield name="refMaster.zip"
								size="11" maxlength="10" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhite">Branch</td>
							<td ><s:textfield name="refMaster.branch"
								size="11" maxlength="8" /></td>
							<td align="left" class="listwhite"></td>


						</tr>
						<tr>
							<td align="left" class="listwhite">Allow No More Payroll
							Entry on or before</td>
							<s:text id="customerFileMoveDateFormattedValue" name="FormDateValue"><s:param name="value" value="refMaster.stopDate"/></s:text>
												<td><s:textfield name="refMaster.stopDate" value="%{customerFileMoveDateFormattedValue}" required="true" size="10" maxlength="11" onclick="displayDatePicker(this, this);" readonly="true"/></td>
												
							

						</tr>


					</tbody>
				</table>


	<table>
				<tbody>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='customerFile.createdOn'/></td>
						<s:hidden name="refMaster.createdOn" />
						<td><s:date name="refMaster.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhite"><fmt:message key='customerFile.createdBy' /></td>
						<s:hidden name="createdBy"/>
						<td><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
						<td align="right" class="listwhite"><fmt:message key='customerFile.updatedOn'/></td>
						<s:hidden name="refMaster.updatedOn"/>
						<td><s:date name="refMaster.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhite"><fmt:message key='customerFile.updatedBy' /></td>
						<s:hidden name="refMaster.updatedBy" />
						<td><s:label name="customerFile.updatedBy" /></td>
					</tr>
				</tbody>
			</table>

				</td>
			</tr>
		</tbody>
	</table>
	</div>
	






	

	<li class="buttonBar bottom"><s:submit cssClass="button"
		method="save" key="button.save" /> <c:if
		test="${not empty refMaster.id}">
		<s:submit cssClass="button" method="delete" key="button.delete"
			onclick="return confirmDelete('refMaster')" />
	</c:if> <s:reset type="button" key="Reset" onmouseout="myDate();"/></li>
	
</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("refMasterForm")); 
</script>


