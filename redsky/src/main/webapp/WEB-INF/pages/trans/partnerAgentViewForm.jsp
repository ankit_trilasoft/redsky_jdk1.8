<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetailAgent.title"/></title>   
    	<meta name="heading" content="<fmt:message key='partnerDetailAgent.heading'/>"/> 
    
<style type="text/css">
		h2 {background-color: #FBBFFF}

.headtab_left{background:url(images/headtab_left.png) no-repeat ; height:21px; width:22px; cursor: pointer; }
.headtab_center{background:url(images/left-left.gif) repeat-x;
height:21px;color: #484848;font:12px Arial, Helvetica, sans-serif ; font-weight:bold; cursor: pointer; }
.headtab_bg{background:url(images/tab-left-bg.gif) no-repeat ; height:21px; width:28px; cursor: pointer; text-align: right;}
.headtab_bg_special{background:url(images/headtab_bg.png) repeat-x; height:21px; width:60%; cursor: pointer; text-align: right;}
.headtab_bg_center{background:url(images/headtab_bg.png) repeat-x; height:21px; width:80%; cursor: pointer; text-align: right;}
.headtab_right{background:url(images/headtab_right.png) no-repeat; height:21px; width:8px;cursor: pointer;} 

div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('agent', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('base', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('rate', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('account', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('profile', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('contact', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('policy', 'fade=0,persist=1,hide=1')

animatedcollapse.addDiv('forwarding', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('owner', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('details', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('sitdestination', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=1,hide=1')

animatedcollapse.init()

</script>
	
	<style><%@ include file="/common/calenderStyle.css"%></style>
  <script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns(); 
	</script>

<script language="javascript" type="text/javascript">
function onLoad() {
	var f = document.getElementById('partnerViewForm'); 
	f.setAttribute("autocomplete", "off"); 
	
}
</script>
<script type="text/javascript"> 
function copyBillToMail(){ 
	document.forms['partnerViewForm'].elements['partner.mailingAddress1'].value=document.forms['partnerViewForm'].elements['partner.billingAddress1'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress2'].value=document.forms['partnerViewForm'].elements['partner.billingAddress2'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress3'].value=document.forms['partnerViewForm'].elements['partner.billingAddress3'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress4'].value=document.forms['partnerViewForm'].elements['partner.billingAddress4'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingEmail'].value=document.forms['partnerViewForm'].elements['partner.billingEmail'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingCity'].value=document.forms['partnerViewForm'].elements['partner.billingCity'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingFax'].value=document.forms['partnerViewForm'].elements['partner.billingFax'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingZip'].value=document.forms['partnerViewForm'].elements['partner.billingZip'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingCountry'].value=document.forms['partnerViewForm'].elements['partner.billingCountry'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingPhone'].value=document.forms['partnerViewForm'].elements['partner.billingPhone'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingTelex'].value=document.forms['partnerViewForm'].elements['partner.billingTelex'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingState'].value=document.forms['partnerViewForm'].elements['partner.billingState'].value;		
	//document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerViewForm'].elements['partner.billingCountryCode'].value;		
	//document.forms['partnerViewForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerViewForm'].elements['partner.billingCountryCodeTemp'].value;		
	
}
function copyTermToMail(){ 
	document.forms['partnerViewForm'].elements['partner.mailingAddress1'].value=document.forms['partnerViewForm'].elements['partner.terminalAddress1'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress2'].value=document.forms['partnerViewForm'].elements['partner.terminalAddress2'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress3'].value=document.forms['partnerViewForm'].elements['partner.terminalAddress3'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingAddress4'].value=document.forms['partnerViewForm'].elements['partner.terminalAddress4'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingEmail'].value=document.forms['partnerViewForm'].elements['partner.terminalEmail'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingCity'].value=document.forms['partnerViewForm'].elements['partner.terminalCity'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingFax'].value=document.forms['partnerViewForm'].elements['partner.terminalFax'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingZip'].value=document.forms['partnerViewForm'].elements['partner.terminalZip'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingCountry'].value=document.forms['partnerViewForm'].elements['partner.terminalCountry'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingPhone'].value=document.forms['partnerViewForm'].elements['partner.terminalPhone'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingTelex'].value=document.forms['partnerViewForm'].elements['partner.terminalTelex'].value;		
	document.forms['partnerViewForm'].elements['partner.mailingState'].value=document.forms['partnerViewForm'].elements['partner.terminalState'].value;		
	//document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].value;		
	//document.forms['partnerViewForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerViewForm'].elements['partner.terminalCountryCodeTemp'].value;		
}
function copyBillToTerm(){ 
	document.forms['partnerViewForm'].elements['partner.terminalAddress1'].value=document.forms['partnerViewForm'].elements['partner.billingAddress1'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress2'].value=document.forms['partnerViewForm'].elements['partner.billingAddress2'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress3'].value=document.forms['partnerViewForm'].elements['partner.billingAddress3'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress4'].value=document.forms['partnerViewForm'].elements['partner.billingAddress4'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalEmail'].value=document.forms['partnerViewForm'].elements['partner.billingEmail'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalCity'].value=document.forms['partnerViewForm'].elements['partner.billingCity'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalFax'].value=document.forms['partnerViewForm'].elements['partner.billingFax'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalZip'].value=document.forms['partnerViewForm'].elements['partner.billingZip'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalCountry'].value=document.forms['partnerViewForm'].elements['partner.billingCountry'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalPhone'].value=document.forms['partnerViewForm'].elements['partner.billingPhone'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalTelex'].value=document.forms['partnerViewForm'].elements['partner.billingTelex'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalState'].value=document.forms['partnerViewForm'].elements['partner.billingState'].value;		
	//document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerViewForm'].elements['partner.billingCountryCode'].value;		
	//document.forms['partnerViewForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerViewForm'].elements['partner.billingCountryCodeTemp'].value;		
}
function copyMailToTerm(){ 
	document.forms['partnerViewForm'].elements['partner.terminalAddress1'].value=document.forms['partnerViewForm'].elements['partner.mailingAddress1'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress2'].value=document.forms['partnerViewForm'].elements['partner.mailingAddress2'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress3'].value=document.forms['partnerViewForm'].elements['partner.mailingAddress3'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalAddress4'].value=document.forms['partnerViewForm'].elements['partner.mailingAddress4'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalEmail'].value=document.forms['partnerViewForm'].elements['partner.mailingEmail'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalCity'].value=document.forms['partnerViewForm'].elements['partner.mailingCity'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalFax'].value=document.forms['partnerViewForm'].elements['partner.mailingFax'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalZip'].value=document.forms['partnerViewForm'].elements['partner.mailingZip'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalCountry'].value=document.forms['partnerViewForm'].elements['partner.mailingCountry'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalPhone'].value=document.forms['partnerViewForm'].elements['partner.mailingPhone'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalTelex'].value=document.forms['partnerViewForm'].elements['partner.mailingTelex'].value;		
	document.forms['partnerViewForm'].elements['partner.terminalState'].value=document.forms['partnerViewForm'].elements['partner.mailingState'].value;		
	//document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].value;		
	//document.forms['partnerViewForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerViewForm'].elements['partner.mailingCountryCodeTemp'].value;		
}
</script>
<script type="text/javascript">
function autoPopulate_partner_billingCountry1(targetElement) {
	
		var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['partnerViewForm'].elements['partner.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
		targetElement.form.elements['partner.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
	}
	function autoPopulate_partner_terminalCountry1(targetElement) {
	
		var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
		targetElement.form.elements['partner.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
	}
	function autoPopulate_partner_mailingCountry1(targetElement) {
		
		var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
		targetElement.form.elements['partner.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
   }

function autoPopulate_partner_billingInstruction1(targetElement) {
	//var billingInstructionCode=targetElement.options[targetElement.selectedIndex].value;
    //document.forms['partnerViewForm'].elements['partner.billingInstructionCode'].value=targetElement.options[targetElement.selectedIndex].value;
	//targetElement.form.elements['partner.billingInstruction'].value=billingInstructionCode.substring(billingInstructionCode.indexOf(":")+2,billingInstructionCode.length);
}

function disabledAll(){
	if(!(document.forms['partnerViewForm'].elements['partner.id'].value == '')){

		var elementsLen=document.forms['partnerViewForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['partnerViewForm'].elements[i].type=='text'){
				document.forms['partnerViewForm'].elements[i].readOnly =true;
				document.forms['partnerViewForm'].elements[i].className = 'input-textUpper';
			}else{
				document.forms['partnerViewForm'].elements[i].disabled=true;
			}
		}
	}
}			
</script>
<script language="JavaScript">


function isNumeric(targetElement)
{   var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid Number");
        targetElement.value="";
        
        return false;
        }
    }
    //alert("Enter valid Number");
    // All characters are numbers.
    return true;
}
</script>	
<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		//document.forms['partnerViewForm'].elements[targetElelemnt].focus();
		//targetElement.focus = true;
		return false;
	}
	return true;
 }
</script>
<script type="text/javascript">
function autoPopulate_partner_billingCountry(targetElement) {	
   	var oriCountry = targetElement.value;
	if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['partnerViewForm'].elements['partner.billingState'].disabled = false;
		}else{
			document.forms['partnerViewForm'].elements['partner.billingState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.billingState'].value = '';
			//autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
		//targetElement.form.elements['customerFile.destinationCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
	}
function autoPopulate_partner_terminalCountry(targetElement) {   
		var dCountry = targetElement.value;
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['partnerViewForm'].elements['partner.terminalState'].disabled = false;
			
		}else{
			document.forms['partnerViewForm'].elements['partner.terminalState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.terminalState'].value = '';
			//autoPopulate_customerFile_originCityCode(dCountryCode,'special');
		}
	}
	
	
	
function autoPopulate_partner_mailingCountry(targetElement) {   
	var dCountry = targetElement.value;
	 if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['partnerViewForm'].elements['partner.mailingState'].disabled = false;
			
		}else{
			document.forms['partnerViewForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.mailingState'].value = '';
			//autoPopulate_customerFile_originCityCode(dCountryCode,'special');
		}
	}


function getBillingCountryCode(targetElement){
	var countryName=document.forms['partnerViewForm'].elements['partner.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}

function handleHttpResponseBillingCountry()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['partnerViewForm'].elements['partner.billingCountryCode'].value = results;
 					//document.forms['partnerViewForm'].elements['partner.billingCountryCode'].select();
					 }
                 else
                 {
                     
                 }
             }
        }
        
        
        
 function getMailingCountryCode(targetElement){
	var countryName=document.forms['partnerViewForm'].elements['partner.mailingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponseMailingCountry;
    http5.send(null);
}



	function handleHttpResponseMailingCountry()
        {

             if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                if(results.length>=1)
                	{
 					document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].value = results;
 					//document.forms['partnerViewForm'].elements['partner.mailingCountryCode'].select();
 					 }
                 else
                 {
                     
                 }
             }
        }
        
        
 
        
function getTerminalCountryCode(){
	var countryName=document.forms['partnerViewForm'].elements['partner.terminalCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponseTerminalCountry;
    http6.send(null);
}

function handleHttpResponseTerminalCountry()
        {

             if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].value = results;
 					//document.forms['partnerViewForm'].elements['partner.terminalCountryCode'].select();
					 }
                 else
                 {
                     
                 }
             }
        }
        





</script>
<SCRIPT LANGUAGE="JavaScript">
function autoPopulate_partner_billingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerViewForm'].elements['partner.billingState'].disabled = false;
		}else{
			document.forms['partnerViewForm'].elements['partner.billingState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.billingState'].value = '';
		}
	}
	function autoPopulate_partner_terminalCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerViewForm'].elements['partner.terminalState'].disabled = false;
		}else{
			document.forms['partnerViewForm'].elements['partner.terminalState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.terminalState'].value = '';
		}
	}
	function autoPopulate_partner_mailingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerViewForm'].elements['partner.mailingState'].disabled = false;
		}else{
			document.forms['partnerViewForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerViewForm'].elements['partner.mailingState'].value = '';
		}
	}
</script>
<SCRIPT LANGUAGE="JavaScript">
function getBillingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
function getMailingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
function getTerminalState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}
	
</script>
<SCRIPT LANGUAGE="JavaScript">	

function getBillingState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
	
function getMailingState(targetElement) {

	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
	
function getTerminalState(targetElement) {

	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States')
	{
		countryCode = "USA";
	}
	if(country == 'India')
	{
		countryCode = "IND";
	}
	if(country == 'Canada')
	{
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}	


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}


 function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerViewForm'].elements['partner.billingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerViewForm'].elements['partner.billingState'].options[i].text = '';
					document.forms['partnerViewForm'].elements['partner.billingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerViewForm'].elements['partner.billingState'].options[i].text = stateVal[1];
					document.forms['partnerViewForm'].elements['partner.billingState'].options[i].value = stateVal[0];
					}
					}
             }
        }  
        
function handleHttpResponse3()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerViewForm'].elements['partner.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerViewForm'].elements['partner.mailingState'].options[i].text = '';
					document.forms['partnerViewForm'].elements['partner.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerViewForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerViewForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
					}
					}
             }
        }  

function handleHttpResponse4()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerViewForm'].elements['partner.terminalState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerViewForm'].elements['partner.terminalState'].options[i].text = '';
					document.forms['partnerViewForm'].elements['partner.terminalState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerViewForm'].elements['partner.terminalState'].options[i].text = stateVal[1];
					document.forms['partnerViewForm'].elements['partner.terminalState'].options[i].value = stateVal[0];
					}
					}
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();

function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    var http5 = getHTTPObject1()
    var http6 = getHTTPObject1()
function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();    
</script>
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}
.listwhitetext2{
background-color:#fff;
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;}
.subcontenttabChild {
background:#DCDCDC url(images/greylinebg.gif) repeat scroll 0% 0%;
border-color:#DCDCDC -moz-use-text-color;
border-style:solid none;
border-width:1px medium;
color:#15428B;
font-family:Arial,Helvetica,sans-serif;
font-size:12px;
font-weight:bold;
height:22px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
</style>	
</head> 

<s:form id="partnerViewForm" name="partnerViewForm" action='${empty param.popup?"savePartner.html":"savePartner.html?decorator=popup&popup=true"}' method="post" validate="true">   
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<s:hidden name="partner.id" />
<s:hidden name="partner.corpID" />
<s:hidden name="partner.isAccountParty" />
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.terminalCountryCode"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<c:set var="type" value="<%= request.getParameter("type")%>" />
<s:hidden name="type" value="<%= request.getParameter("type")%>" />

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="popupval" value="${papam.popup}"/>
	
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<div id="layer4" style="100%">
<div id="newmnav">
			<ul>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				<li><a href="partnerAgent.html"><span>Agent List</span></a></li>
			</ul>
	</div><div class="spn">&nbsp;</div>

	</div> 

<div id="Layer1" style="width:100%;margin:0px; padding:0px;" onkeydown="changeStatus();">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content" >
<table  cellspacing="1" cellpadding="0" border="0" width="98%" style="margin:0px;padding:0px;">
	<tbody>
		<tr>
			<td>
				<c:choose>
					<c:when test="${partnerType == 'PP'}">
						<table cellspacing="0" cellpadding="2" border="0" style="margin-bottom:0px" >
							<tbody>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.partnerPrefix'/></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.firstName'/></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.middleInitial'/></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.lastName'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.partnerSuffix'/></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left" class="listwhitetext2"><s:textfield name="partner.partnerPrefix" cssStyle="width:25px" required="true"
									cssClass="input-text" maxlength="3"  onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext2"> <s:textfield name="partner.firstName" required="true" cssClass="input-text"
									cssStyle="width:160px" maxlength="15" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.middleInitial" cssClass="input-text"
									required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.lastName" required="true" cssClass="input-text"
									size="38" maxlength="80"  onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.partnerSuffix" cssStyle="width:30px" required="true"
									cssClass="input-text" maxlength="10" onkeydown="return onlyCharsAllowed(event)" tabindex="1"/></td>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.partnerCode"
									required="true" cssClass="input-textUpper" maxlength="8" size="25" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="1"/> </td>
								</tr>
								<tr><td height="8px"></td></tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<table cellspacing="0" cellpadding="1" border="0" style="margin-bottom:0px ">
							<tbody>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext2">Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext2"><fmt:message key='partner.status'/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<s:hidden key="partner.firstName" value=""/>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.lastName" required="true" cssClass="input-text" size="79" maxlength="80"  onkeydown="return onlyCharsAllowed(event)" tabindex="1"/> </td>
									<td align="left" class="listwhitetext2"> <s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="15" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
									<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:103px"  onchange="changeStatus();"/></td>
								</tr>
								<tr><td height="8px"></td></tr>
							</tbody>
						</table>
					</c:otherwise>
					</c:choose>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
						<tbody>
							<tr><td align="center" colspan="10"><img height="1px" width="100%" src="<c:url value='/images/vertlinedata.jpg'/>"/></td></tr>
								<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
							</tbody>
						</table>
					<table cellspacing="0" cellpadding="2" border="0" style="margin-bottom:2px">
						<tbody>
						
							<tr><td width="10px"></td><font color="#003366">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Billing Address</font>
								<td><s:textfield cssClass="input-text" name="partner.billingAddress1"  size="50" maxlength="30" tabindex="2"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingCity" cssStyle="width:130px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" tabindex="3"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="4"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td><s:textfield cssClass="input-text" name="partner.billingAddress2" size="50" maxlength="30" tabindex="5"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingState'/></td>
								<td><s:select name="partner.billingState" cssClass="list-menu"list="%{bstates}" cssStyle="width:132px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="6"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="7"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td><s:textfield cssClass="input-text" name="partner.billingAddress3" size="50" maxlength="30" tabindex="8"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingZip" cssStyle="width:130px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)" tabindex="9"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingTelex'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="10"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td><s:textfield cssClass="input-text" name="partner.billingAddress4" size="50" maxlength="30" tabindex="11"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/></td>
								<td><s:select cssClass="list-menu" name="partner.billingCountry" list="%{countryDesc}" cssStyle="width:132px"  headerKey="" headerValue="" onchange="changeStatus();getBillingCountryCode(this);autoPopulate_partner_billingCountry(this);getBillingState(this);" tabindex="12"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/></td>
								
								<c:if test="${not empty partner.id}" >
									<td><s:textfield cssClass="input-text" name="partner.billingEmail"  size="17" maxlength="65" tabindex="13" readonly="true"/>
										<c:if test="${not empty partner.billingEmail}" >
											<a href="mailto:${partner.billingEmail}">
												<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
											</a>
										</c:if>	
									</td>
								</c:if>	
								<c:if test="${empty partner.id}" >
									<td><s:textfield cssClass="input-text" name="partner.billingEmail"  size="17" maxlength="65" tabindex="13"/>
										<c:if test="${not empty partner.billingEmail}" >
											<a href="mailto:${partner.billingEmail}">
												<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
											</a>
										</c:if>	
									</td>
								</c:if>	
							</tr>
							<tr><td height="5px"></td></tr>
						</tbody>
					</table>
					<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG' || partnerType == '' }">
					<table cellspacing="0" cellpadding="2" border="0" style="margin-bottom:2px">
						<tbody>
							
							<tr>
							<td width="10px"></td>
							<td><font color="#003366">Terminal Address</font></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td ><s:textfield cssClass="input-text" name="partner.terminalAddress1"  size="50" maxlength="30" tabindex="14"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalCity" cssStyle="width:130px"  maxlength="15" onkeydown="return onlyCharsAllowed(event)" tabindex="15"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="16"/></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalAddress2" size="50" maxlength="30" tabindex="17"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState'/></td>
									<td><s:select name="partner.terminalState" list="%{tstates}" cssClass="list-menu" cssStyle="width:132px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="18"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="19"/></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalAddress3" size="50" maxlength="30" tabindex="20"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalZip" cssStyle="width:130px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)" tabindex="21"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="22"/></td>
							</tr>
							<tr>
							<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalAddress4" size="50" maxlength="30" tabindex="23"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.terminalCountry" list="%{countryDesc}" cssStyle="width:132px"   headerKey="" headerValue="" onchange="changeStatus();getTerminalCountryCode(this);autoPopulate_partner_terminalCountry(this);getTerminalState(this);" tabindex="24"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail'/></td>
									
									<c:if test="${not empty partner.id}" >
										<td><s:textfield cssClass="input-text" name="partner.terminalEmail"  size="17"  maxlength="65" tabindex="25" readonly="true"/>
											<c:if test="${not empty partner.terminalEmail}" >
												<a href="mailto:${partner.terminalEmail}">
													<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
												</a>
											</c:if>
										</td>
									</c:if>
									<c:if test="${empty partner.id}" >
										<td><s:textfield cssClass="input-text" name="partner.terminalEmail"  size="17"  maxlength="65" tabindex="25"/>
											<c:if test="${not empty partner.terminalEmail}" >
												<a href="mailto:${partner.terminalEmail}">
													<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
												</a>
											</c:if>
										</td>
									</c:if>
							</tr>
							<tr>
							<td width="10px"></td>
								
								<td colspan="3" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" disabled onClick="copyBillToTerm();changeStatus();" tabindex="26"/></td>
								<td colspan="2" ><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from mailing address" disabled onClick="copyMailToTerm();changeStatus();"  tabindex="27"/></td>
							</tr>
							<tr><td height="5px"></td></tr>
						</tbody>
					</table>
					</c:if>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
						<tbody>
							<tr><td align="center" colspan="10"><img height="1px" width="100%" src="<c:url value='/images/vertlinedata.jpg'/>"/></td></tr>
								<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
							</tbody>
						</table>
					<table cellspacing="0" cellpadding="2" border="0" style="margin-bottom:2px">
						<tbody>
						<tr>
							<td width="10px"></td>
							<td><font color="#003366">Mailing Address</font></td>
							</tr>
								
								
								<tr>
								<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingAddress1"  size="50" maxlength="30" tabindex="28"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingCity" cssStyle="width:130px" maxlength="15" onkeydown="return onlyCharsAllowed(event)" tabindex="29"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="30"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingAddress2" size="50" maxlength="30" tabindex="31"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState'/></td>
									<td><s:select name="partner.mailingState" cssClass="list-menu"list="%{mstates}" cssStyle="width:132px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="32"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="33"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingAddress3" size="50" maxlength="30" tabindex="34"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingZip" cssStyle="width:130px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)" tabindex="35"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="36"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="1"><s:textfield cssClass="input-text" name="partner.mailingAddress4" size="50" maxlength="30" tabindex="37"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.mailingCountry" list="%{countryDesc}" cssStyle="width:132px"  headerKey="" headerValue="" onchange="changeStatus();getMailingCountryCode(this);autoPopulate_partner_mailingCountry(this);getMailingState(this);" tabindex="38"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail'/></td>
									
									<c:if test="${not empty partner.id}" >
										<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="17" maxlength="65" tabindex="39" readonly="true"/>
											<c:if test="${not empty partner.mailingEmail}" >
												<a href="mailto:${partner.mailingEmail}">
													<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
												</a>
											</c:if>	
										</td>
									</c:if>
									<c:if test="${empty partner.id}" >
										<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="17" maxlength="65" tabindex="39"/>
											<c:if test="${not empty partner.mailingEmail}" >
												<a href="mailto:${partner.mailingEmail}">
													<img align="top" class="openpopup" width="20" height="20" onclick="" src="<c:url value='/images/email.png'/>" />
												</a>
											</c:if>	
										</td>
									</c:if>
									
								</tr>
								<tr>
								<td width="10px"></td>
								
								<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG'}">
								<td colspan="3" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from terminal address" disabled onClick="copyTermToMail();changeStatus();" tabindex="40"/></td>
								<td colspan="2"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" disabled onClick="copyBillToMail();changeStatus();" tabindex="41"/></td>
								</c:if>
								<c:if test="${partnerType == 'PP' || partnerType == 'AC'  || partnerType == 'OO' || partnerType == ''}">
								<td colspan="5" align="right"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" disabled onClick="copyBillToMail();changeStatus();" tabindex="40"/></td>
								</c:if>
							    <tr>
							<tr><td height="5px"></td></tr>
							<!--<c:if test="${partnerType == 'AC'}">	
							<tr>
								<td align="left" class="listwhitetext" colspan="6">Storage&nbsp;Billing&nbsp;Group&nbsp;
								<s:select cssClass="list-menu" name="partner.storageBillingGroup" list="%{storageBillingGroup}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
								
							</tr>
							</c:if>
							
							--><c:if test="${partnerType == 'VN'}">	
							<tr>
							<td width="10px"></td>
								<td align="left" class="listwhitetext" colspan="6">Type&nbsp;of&nbsp;Vendor&nbsp;
								<s:select cssClass="list-menu" name="partner.storageBillingGroup" list="%{typeOfVendor}" cssStyle="width:197px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="41"/></td>
								
							</tr>
							<tr><td height="5px"></td></tr>
							</c:if>
							<c:if test="${partnerType == 'PP'}">
								<tr>
									<td width="2px"></td>
									<td>
									<table style="margin-bottom:0px"><tr>
									<td align="right" class="listwhitetext"><fmt:message key='partner.storageBillingGroup'/></td>
									<td><s:select cssClass="list-menu" name="partner.storageBillingGroup" list="%{storageBillingGroup}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="38"/></td>
								</tr>
								</table>
								</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					
					
<c:if test="${partnerType == 'AC'}" >
<tr>
	<td height="5" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('account')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Account Info
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>									
  		<div id="account">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
	
<tr>
				<td align="right" class="listwhitetext" width="132px"><fmt:message key='accountInfo.authorized'/></td>	
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.creditAuthorizedBy" size="25" maxlength="15" tabindex="2"/></td>	
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.limit'/></td>	
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.limitInfo" size="25" maxlength="15" tabindex="3"/></td>			
			</tr>				
			
			<tr>				
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.storage'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.storageBillingGroup" list="%{billgrp}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="4"/></td>
				<td align="right" class="listwhitetext" width="115px"><fmt:message key='accountInfo.nationalAccount'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.validNationalCode" size="25" maxlength="15" tabindex="5"/></td>
					
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.abbreviation'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.abbreviation" size="25" maxlength="15" tabindex="6"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.idCode'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.idCode" size="25" maxlength="15" tabindex="7"/></td>
				
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.accountHolder'/></td>
				<td class="listwhitetext" colspan="2">
				<s:select cssClass="list-menu"  name="partner.accountHolder" list="%{sale}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="8"/>
				
				<!--<s:textfield cssClass="input-text"  name="partner.accountHolder" cssStyle="width:160px" maxlength="15"/></td>
			--></tr>
			
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.paymentMethod'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.paymentMethod" list="%{paytype}" cssStyle="width:236px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.payOption'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="partner.payOption" list="%{payopt}" cssStyle="width:158px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.billingInstruction'/></td>
				<td class="listwhitetext" colspan="4"><s:select cssClass="list-menu"  name="partner.billingInstruction" list="%{billinst}" cssStyle="width:370px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="11"/></td>
				<!--<td align="right" class="listwhitetext"></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="partner.billingInstructionMessage" cssStyle="width:160px" maxlength="15"/></td>
			-->
			</tr>
			<tr>				
				<c:set var="ischecked" value="false"/>						
				<c:set var="ischecked" value="true"/>
				<td align="right" class="listwhitetext"><fmt:message key='accountInfo.multiAuthorization'/></td>
				<td class="listwhitetext" colspan="4"><s:checkbox key="partner.multiAuthorization" fieldValue="true" onchange="changeStatus();" tabindex="12"/></td>
			</tr>
			<tr>
										<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
										<td align="left" class="listwhitetext">Pricing</td>
										
										<td align="left" class="listwhitetext">Billing</td>
										
										<td align="left" class="listwhitetext">Payable</td>
								</tr>
							<tr>
									<td align="right" class="listwhitetext" style="width:200px">Account setup</td>
									<td><s:select name="partner.pricingUser" cssClass="list-menu" list="%{pricing}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									
									<td><s:select name="partner.billingUser" cssClass="list-menu" list="%{billing}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									
									<td colspan="2"><s:select cssClass="list-menu" name="partner.payableUser" list="%{payable}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
									<td align="left" class="listwhitetext" style="width:10px"></td>
							</tr>
			  <tr>
			    <td height="5px"></td>
			     </tr>
</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 





<tr>
	<td height="5" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('profile')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Account Profile
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
		<div id="profile">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  
 		  	
			<tr>
				<td align="right" class="listwhitetext" width="132px"><fmt:message key='accountProfile.stage'/>:</td>
				<td class="listwhitetext" width="150px" colspan="3"><s:radio id="stage" name="accountProfile.stage" list="%{stageunits}" onclick="changeStatus();" tabindex="1"/></td>
				
			</tr>
			<tr>
			<td height="8px"></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.type'/></td>
				<td><s:select cssClass="list-menu" name="accountProfile.type"  cssStyle="width:160px" list="{'Private','Public'}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="2"/></td>
				
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.industry'/></td>
				<td><s:select cssClass="list-menu" name="accountProfile.industry" cssStyle="width:170px" list="%{industry}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3"/></td>
				
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.employees'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountProfile.employees" cssStyle="width:160px" maxlength="15" tabindex="4"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.currency'/></td>
				<td align="left" class="listwhitetext"><s:select cssClass="list-menu"  key="accountProfile.currency" cssStyle="width:170px" list="%{currency}" headerKey="USD" headerValue="United States Dollars" onchange="changeStatus();" tabindex="5"/></td>
				
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.source'/></td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="accountProfile.source" list="%{lead}" cssStyle="width:245px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="6"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.revenue'/></td>
				<td><s:select cssClass="list-menu" name="accountProfile.revenue"  cssStyle="width:170px" list="%{revenue}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="7"/></td>
				</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.country'/></td>
				<td colspan=""><s:select cssClass="list-menu" name="accountProfile.country" cssStyle="width:245px" list="%{ocountry}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="8"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountProfile.owner'/></td>
				<td><s:select cssClass="list-menu" name="accountProfile.owner"  cssStyle="width:170px" list="{'a','b'}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9"/></td>
				</tr>
 		  

</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 




<tr>
	<td height="5px" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('contact')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Account Contact
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>										
  		<div  id="contact">
		<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
 		  <tr>
 <td>
 <div style=" width:100%; height:150px;overflow-x:auto; overflow-y:auto;">		  
<display:table name="accountContacts" class="table" requestURI="" id="accountContactList" defaultsort="1" pagesize="10" >   
	<display:column property="jobType" sortable="true" titleKey="accountContact.jobType" />
	<display:column property="jobTypeOwner" sortable="true" titleKey="accountContact.jobTypeOwner" />
	<display:column property="contractType" sortable="true" titleKey="accountContact.contractType" />
	<display:column property="contactName" sortable="true" titleKey="accountContact.contactName" />
	<display:column property="department" sortable="true" titleKey="accountContact.department" />
	<display:column property="primaryPhone" sortable="true" titleKey="accountContact.primaryPhone" />
</display:table> 
</div> 
</td>
</tr>
</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 


<tr>
	<td height="5px" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('policy')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Contract Policy
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>											
  		<div id="policy">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  
 	<tr>
					<td align="right" class="listwhitetext" valign="top">&nbsp;&nbsp;</td>
					<td align="left" class="listwhite" colspan="6"><s:textarea name="contractPolicy.policy"  cols="80" rows="20" cssClass="detailborder" cssStyle="border:1px solid #99bbe8; background-color: #e3f1fe"/></td>
	</tr>


</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td>
</tr>
</c:if>








<c:if test="${partnerType == 'AG' || partnerType == 'VN' }" >

<tr>
	<td height="5px" width="100%" align="left" style="">
	<c:if test="${partnerType == 'AG'}" >
<div  onClick="javascript:animatedcollapse.toggle('agent')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Agent Info
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
	</c:if>		
								
  	 <c:if test="${partnerType == 'VN'}" >
		<div  onClick="javascript:animatedcollapse.toggle('agent')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Vendor Info
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div></c:if>	
	
								
  		<div id="agent" >
		<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0"  >
 		  <tbody>
 		  
 		  
 		  <tr><td align="left" height="5px"></td></tr>
 		 		<!--<s:hidden name="partner.isAccount" value="false" />
					--><c:if test="${partnerType == 'VN'}">
					<!--<s:hidden name="partner.isAgent" value="false" />
					<s:hidden name="partner.isVendor" value="true" />
					--></c:if>
					<c:if test="${partnerType == 'AG'}">
					<!--<s:hidden name="partner.isAgent" value="true" />
					<s:hidden name="partner.isVendor" value="false" />
					--></c:if>
					<s:hidden name="partner.isBroker" value="false" />
					<!--<s:hidden name="partner.isCarrier" value="false" />
					--><s:hidden name="partner.qc" value="false" />
					<s:hidden name="partner.isPrivateParty" value="false" />
					<s:hidden name="partner.sea" value="false" />
					<s:hidden name="partner.surface" value="false" />
					<s:hidden name="partner.air" value="false" />
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate'/></td>
						
						<c:if test="${not empty partner.effectiveDate}">
						<s:text id="partnerEffectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
						<td width="100px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partner.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="25" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
					</c:if>
					<c:if test="${empty partner.effectiveDate}">
					<td width="100px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partner.effectiveDate" size="25" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
					</c:if>
					<!--<td align="right" class="listwhitetext"><fmt:message key='partner.zone'/></td><td><s:textfield cssClass="input-text" name="partner.zone"  cssStyle="width:115px" maxlength="2"/></td>
					-->
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.rank'/></td>
						<td ><s:select name="partner.rank"  list="%{rank}" cssStyle="width:155px"   onchange="changeStatus();" tabindex="1"/></td>
					</tr>
					<tr>	
						<td align="right" class="listwhitetext" width="140px"><fmt:message key='partner.billPayType'/></td>
						<td><s:select name="partner.billPayType" list="%{paytype}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="2"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayOption'/></td>
						<td colspan="3" ><s:select name="partner.billPayOption"  list="%{payopt}" cssStyle="width:155px"   onchange="changeStatus();" tabindex="3"/></td>
					</tr>
					<tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.abbreviation'/></td><td colspan="2"><s:textfield cssClass="input-text" name="partner.abbreviation"  cssStyle="width:115px" maxlength="15"/></td>
						-->
						<td align="right" class="listwhitetext"><fmt:message key='partner.invoicePercentage'/></td>
						<td><s:textfield cssClass="input-text" name="partner.invoicePercentage"  cssStyle="width:154px" maxlength="7" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this)" onkeyup="valid(this,'special')" tabindex="4"/></td>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.warehouse'/></td><td><s:select name="partner.warehouse" list="%{house}" cssStyle="width:115px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
					--></tr>
					<!--<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.scheduleCode'/></td><td colspan="5"><s:select name="partner.scheduleCode" list="%{schedule}" cssStyle="width:300px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.commitionType'/></td><td><s:select name="partner.commitionType" list="%{psh}" cssStyle="width:115px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
					</tr>
					--><tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.coordinator'/></td><td colspan="2"><s:select name="partner.coordinator"  list="%{coord}" cssStyle="width:115px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
						<td align="right" class="listwhitetext">Account&nbsp;Owner</td><td colspan="2"><s:select name="partner.salesMan"  list="%{sale}" cssStyle="width:115px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
						--><td align="right" class="listwhitetext"><fmt:message key='partner.billToGroup'/></td>
						<td ><s:select name="partner.billToGroup"  list="%{billgrp}" cssStyle="width:155px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="5"/></td>
						</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingInstructionCode'/></td>
					<td colspan="7" align="left"><s:select cssClass="list-menu" id="billingInstructionCode" name="partner.billingInstruction"  list="%{billinst}" cssStyle="width:453px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="6"/></td>
					</tr>
					
										
					<tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.validNationalCode'/></td><td><s:textfield cssClass="input-text" name="partner.validNationalCode"  size="25" maxlength="15"/></td>
						--><td align="right" class="listwhitetext"><fmt:message key='partner.client5'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client5"  size="25" maxlength="15" tabindex="8"/></td>
					</tr>
					<!--<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.idCode'/></td><td><s:textfield cssClass="input-text" name="partner.idCode"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client6'/></td><td><s:textfield cssClass="input-text" name="partner.client6"  size="25" maxlength="15"/></td>
					</tr>
					-->
					
					<tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.client1'/></td><td><s:textfield cssClass="input-text" name="partner.client1"  size="25" maxlength="15"/></td>
						-->
						<td align="right" class="listwhitetext"><fmt:message key='partner.client8'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client8"  size="25" maxlength="15" tabindex="10"/></td>
					</tr>
					<tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.client2'/></td><td><s:textfield cssClass="input-text" name="partner.client2"  size="25" maxlength="15"/></td>
						-->
						<td align="right" class="listwhitetext"><fmt:message key='partner.client9'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client9"  size="25" maxlength="15"tabindex="11" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client3'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client3"  size="25" maxlength="15" tabindex="12"/></td>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.client10'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client10"  size="25" maxlength="15" tabindex="13"/></td>
					
					<td align="right" class="listwhitetext"><fmt:message key='partner.bases'/></td>
						<td align="left" colspan="3"><s:textfield cssClass="input-text" name="partner.bases"  size="25" maxlength="50" tabindex="13"/></td>
					</tr>
					-->
					</tr>


</tbody>
</table>
</div>
</td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 
</c:if>

<c:if test="${partnerType == 'AG'}" >
<tr>
	<td height="5" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('base')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Base
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  		<div id="base">
		<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  <td>
<div style=" width:100%; height:150px;overflow-x:auto; overflow-y:auto;">		  
 		  <display:table name="baseAgent" class="table" requestURI="" id="baseAgentList" defaultsort="1" pagesize="10" >   
			<display:column property="baseCode" sortable="true" style="width: 150px;" paramId="id" paramProperty="id" titleKey="agentBase.baseCode" />
			<display:column property="description" sortable="true" titleKey="agentBase.description"/>
		  </display:table>  
</div>
</td>
</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 


<tr>
	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('rate')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Rate Matrix
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>									
  		<div id="rate">
		<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<td>
<div style=" width:100%; height:150px;overflow-x:auto; overflow-y:auto;">		  
<display:table name="partnerRatess" class="table" requestURI="" id="partnerRatesList" defaultsort="1" pagesize="10" >   
	<display:column property="type" sortable="true" titleKey="partnerRates.type" />   
	<display:column property="effective" sortable="true" titleKey="partnerRates.effective" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="endDate" sortable="true" titleKey="partnerRates.endDate" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="l1To999" sortable="true" titleKey="partnerRates.l1To999"/>
    <display:column property="v1To999" sortable="true" titleKey="partnerRates.v1To999"/>
    <display:column property="lcl499" sortable="true" titleKey="partnerRates.lcl499"/>
    <display:column property="air499" sortable="true" titleKey="partnerRates.air499"/>
   
</display:table>  
</div>
</td>
</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 
</c:if>
<c:if test="${partnerType == 'OO'}" >
<tr>
	<td height="10" width="100%" align="left" >
		<div  onClick="javascript:animatedcollapse.toggle('owner')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Owner Info
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>										
  		<div  id="owner">
<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0" width="100%">
<tbody>
<tr><td align="center" colspan="7"><img height="0px " width="100%" src="<c:url value='/images/vertlinedata.jpg'/>"/></td></tr>
								<tr><td class="subcontenttabChild" colspan="6"><font color="black"><b>&nbsp;Driver</b></font></td></tr>
								<tr><td align="left" height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='partner.driverAgency'/></td>
									<td><s:textfield cssClass="input-text" name="partner.driverAgency"  size="15" maxlength="8" tabindex="42"/></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Tax&nbsp;ID#</td>
									<td><s:textfield cssClass="input-text" name="partner.client1" size="15" maxlength="15" tabindex="44"/></td>
									<td align="right" class="listwhitetext">VL&nbsp;Code</td>
									<td><s:textfield cssClass="input-text" name="partner.validNationalCode" maxlength="15" size="15" tabindex="45"/></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Short%</td>
									<td><s:textfield cssClass="input-text" name="partner.shortPercentage" size="15" maxlength="10" tabindex="46"/></td>
									<!--<td align="right" class="listwhitetext">Social&nbsp;Security&nbsp;#</td>
									<td><s:textfield cssClass="input-text" name="partner.client2" size="15" maxlength="15"/></td>
									--><td align="right" class="listwhitetext">Account&nbsp;Owner</td>
									<td><s:select cssClass="list-menu"  name="partner.salesMan" headerKey=" " headerValue=" " list="%{sale}" cssStyle="width:105px" tabindex="47"/></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Long%</td>
									<td width="70px"><s:textfield cssClass="input-text" name="partner.longPercentage" size="15" maxlength="10" tabindex="48"/></td>
									<td align="right" class="listwhitetext" width="90px">1099</td>
									<td width="70px"><s:checkbox key="partner.client3" tabindex="49"/></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext">Local%</td>
									<td><s:textfield cssClass="input-text" name="partner.localPercentage" size="15" maxlength="10" tabindex="51"/></td>
								</tr>	
								<tr><td height="5px"></td></tr>
</tbody>
</table>
</div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> 							
</c:if>



<tr>
<td height="5" align="left" class="listwhitetext" colspan="2">
					<c:set var="isAccountFlag" value="false"/>
					<c:set var="isAgentFlag" value="false"/>
					<c:set var="isBrokerFlag" value="false"/>
					<c:set var="isVendorFlag" value="false"/>
					<c:set var="isCarrierFlag" value="false"/>
					<c:set var="isOwnerOpFlag" value="false"/>
					<c:set var="qcFlag" value="false"/>
					<c:set var="fidiFlag" value="false"/>
					<c:set var="privateFlag" value="false"/>
					<c:set var="seaFlag" value="false" />
					<c:set var="surfaceFlag" value="false" />
					<c:set var="airFlag" value="false" />
					<c:if test="${partner.isAccount}">
						 <c:set var="isAccountFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isAgent}">
						 <c:set var="isAgentFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isBroker}">
						 <c:set var="isBrokerFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isVendor}">
						 <c:set var="isVendorFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isCarrier}">
						 <c:set var="isCarrierFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isOwnerOp}">
						 <c:set var="isOwnerOpFlag" value="true"/>
					</c:if>
					<c:if test="${partner.qc}">
						 <c:set var="qcFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isPrivateParty}">
						 <c:set var="privateFlag" value="true"/>
					</c:if>
					<c:if test="${partner.sea}">
						 <c:set var="seaFlag" value="true"/>
					</c:if>
					<c:if test="${partner.surface}">
						 <c:set var="surfaceFlag" value="true"/>
					</c:if>
					<c:if test="${partner.air}">
						 <c:set var="airFlag" value="true"/>
					</c:if>
<c:choose>
<c:when test="${partnerType == 'CR'}">
<s:hidden name="partner.isAccount" value="false" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isOwnerOp" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.isCarrier" value="true" />
<s:hidden key="partner.perDiemFreeDays"  />
<s:hidden key="partner.perDiemDays2"  />
<s:hidden key="partner.perDiemCost1"  />
<s:hidden key="partner.perDiemCost2"  />
<s:hidden key="partner.remarks1"  />
<s:hidden key="partner.remarks2"  />
<s:hidden key="partner.remarks3"  />
<s:hidden key="partner.remarks4"  />
<s:hidden key="partner.remarks5"  />
<table>
<tbody>
<tr>
<td width="10px"></td>
<td colspan="4" align="left">
				<table>
					<tr>
						<!--<td align="right" class="listwhitetext"><fmt:message key='partner.client7'/></td>
						<td><s:textfield cssClass="input-text" name="partner.client7"  size="25" maxlength="15"/></td>
					--></tr>
				</table>
				<table width="100%">
						<tr>
						
						<td>Carrier Detail</td>
						<td class="listwhitetext"><s:checkbox key="partner.sea" value="${seaFlag}" fieldValue="true" tabindex="58"/>Sea</td>
						<td class="listwhitetext"><s:checkbox key="partner.surface" value="${surfaceFlag}" fieldValue="true"  tabindex="59"/>Surface</td>
						<td class="listwhitetext"><s:checkbox key="partner.air" value="${airFlag}" fieldValue="true" tabindex="60"/>Air</td>
					</tr>
					
				</table>
	
</td>
</tr>
</tbody>
</table>
</c:when>
<c:when test="${partnerType == 'AC'}">


<!--
<s:hidden name="partner.createdBy"/>
<s:hidden name="partner.updatedBy"/>
-->

<s:hidden name="partner.isBroker" value="false" />

<s:hidden name="partner.isAccount" value="true" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.isOwnerOp" value="false" />

<s:hidden name="partner.qc" value="false" />

<s:hidden name="partner.creditAuthorizedBy" />
<s:hidden name="partner.storageBillingGroup" />
<s:hidden name="partner.limitInfo" />
<s:hidden name="partner.billingInstruction" />

<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />

</c:when>
<c:when test="${partnerType == 'PP'}">
<s:hidden name="partner.isAccount" value="false" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.isOwnerOp" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="true" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />
<s:hidden name="partner.status" value="Approved" />
</c:when>
<c:when test="${partnerType == 'OO'}">
<s:hidden name="partner.isAccount" value="false" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.isOwnerOp" value="true" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />
</c:when>

<c:when test="${partnerType == 'AG' || partnerType == 'VN'}">
<c:if test="${not empty partner.doNotUse}">
<s:text id="doNotUseFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.doNotUse"/></s:text>
<td><s:hidden cssClass="input-text" id="doNotUse" name="partner.doNotUse" value="%{doNotUseFormattedValue}" /></td>
</c:if>
<c:if test="${not empty partner.effectiveDate}">
1
<s:text id="effectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
<td><s:hidden cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{effectiveDateFormattedValue}" /></td>
</c:if>
<s:hidden key="partner.nationalAccount"/>
<s:hidden key="partner.accountHolder"/>
<s:hidden key="partner.paymentMethod"/>
<s:hidden key="partner.payOption"/>
<s:hidden key="partner.billingInstructionMessage"/>


<s:hidden key="partner.rank"/>
<s:hidden key="partner.billPayType"/>
<s:hidden key="partner.billPayOption"/>
<s:hidden key="partner.abbreviation"/>
<s:hidden key="partner.invoicePercentage"/>
<s:hidden key="partner.warehouse"/>
<s:hidden key="partner.scheduleCode"/>
<s:hidden key="partner.commitionType"/>
<s:hidden key="partner.coordinator"/>
<s:hidden key="partner.client3"/>
<s:hidden key="partner.billToGroup"/>
<s:hidden key="partner.billingInstruction"/>
<s:hidden key="partner.idCode"/>
<s:hidden key="partner.client4"/>
<s:hidden key="partner.client5"/>
<s:hidden key="partner.client6"/>
<s:hidden key="partner.client7"/>
<s:hidden key="partner.client8"/>
<s:hidden key="partner.client9"/>
<s:hidden key="partner.client10"/>
<s:hidden key="partner.bases"/>
<c:if test="${partnerType == 'VN'}">
	<s:hidden name="partner.isAgent" value="false" />
	<s:hidden name="partner.isVendor" value="true" />
</c:if>
<c:if test="${partnerType == 'AG'}">
	<s:hidden name="partner.isAgent" value="true" />
	<s:hidden name="partner.isVendor" value="false" />
	<s:hidden name="partner.isCarrier" value="false" />
	<s:hidden name="partner.isOwnerOp" value="false" />
	<s:hidden name="partner.isAccount" value="false" />
</c:if>
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.isOwnerOp" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />
</c:when>
<c:otherwise>

						

<div  onClick="javascript:animatedcollapse.toggle('details')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Details
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>				
  
<div  id="details" style="width:698px">

<table class="detailTabLabel" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				
				
					<table width="98%">
					
					<tr><font size="2"><b>Participates as</b></font>
					<s:hidden name="partner.isAccount" value="false" />
					<c:if test="${partnerType == 'VN'}">
					<s:hidden name="partner.isAgent" value="false" />
					<s:hidden name="partner.isVendor" value="true" />
					</c:if>
					<c:if test="${partnerType == 'AG'}">
					<s:hidden name="partner.isAgent" value="true" />
					<s:hidden name="partner.isVendor" value="false" />
					</c:if>
					<s:hidden name="partner.isBroker" value="false" />
					<s:hidden name="partner.isCarrier" value="false" />
					<s:hidden name="partner.qc" value="false" />
					<s:hidden name="partner.isPrivateParty" value="false" />
					<s:hidden name="partner.sea" value="false" />
					<s:hidden name="partner.surface" value="false" />
					<s:hidden name="partner.air" value="false" />
					<tr>
					
						<td align="right" class="listwhitetext"><fmt:message key='partner.doNotUse'/></td>
						<c:if test="${not empty partner.doNotUse}">
						<s:text id="partnerDoNotUseFormattedValue" name="FormDateValue"><s:param name="value" value="partner.doNotUse"/></s:text>
						<td><s:textfield cssClass="input-text" id="doNotUseAfter" name="partner.doNotUse" value="%{partnerDoNotUseFormattedValue}" size="13" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						</c:if>
						<c:if test="${empty partner.doNotUse}">
						<td><s:textfield cssClass="input-text" id="doNotUseAfter" name="partner.doNotUse" size="13" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						</c:if>
						<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate'/></td>
						<c:if test="${not empty partner.effectiveDate}">
						<s:text id="partnerEffectiveDateFormattedValue" name="FormDateValue"><s:param name="value" value="partner.effectiveDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="13" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						</c:if>
					<c:if test="${empty partner.effectiveDate}">
					<td><s:textfield cssClass="input-text" id="effectiveDate" name="partner.effectiveDate" size="13" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
					</c:if>
					<td align="right" class="listwhitetext"><fmt:message key='partner.zone'/></td>
					<td><s:textfield cssClass="input-text" name="partner.zone"  cssStyle="width:115px" maxlength="2"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.rank'/></td>
						<td colspan=""><s:select name="partner.rank"  list="%{rank}" cssStyle="width:115px"  headerKey="" headerValue=""/></td>
					</tr>
					<tr>	
						<td align="right" class="listwhitetext" width="140px"><fmt:message key='partner.billPayType'/></td>
						<td><s:select name="partner.billPayType" list="%{paytype}" cssStyle="width:145px" headerKey="" headerValue="" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayOption'/></td>
						<td colspan="3"><s:select name="partner.billPayOption"  list="%{payopt}" cssStyle="width:145px"  headerKey="" headerValue=""/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.abbreviation'/></td>
						<td colspan="2"><s:textfield cssClass="input-text" name="partner.abbreviation"  cssStyle="width:115px" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.invoicePercentage'/></td>
						<td colspan="2"><s:textfield cssClass="input-text" name="partner.invoicePercentage"  cssStyle="width:115px" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.warehouse'/></td>
						<td><s:select name="partner.warehouse" list="%{house}" cssStyle="width:115px"  headerKey="" headerValue=""/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.scheduleCode'/></td>
						<td colspan="5"><s:select name="partner.scheduleCode" list="%{schedule}" cssStyle="width:300px"  headerKey="" headerValue=""/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.commitionType'/></td>
						<td><s:select name="partner.commitionType" list="%{psh}" cssStyle="width:115px"  headerKey="" headerValue=""/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.coordinator'/></td>
						<td colspan="2"><s:select name="partner.coordinator"  list="%{coord}" cssStyle="width:115px" headerKey="" headerValue="" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.salesMan'/></td>
						<td colspan="2"><s:select name="partner.salesMan"  list="%{sale}" cssStyle="width:115px" headerKey="" headerValue="" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.billToGroup'/></td>
						<td colspan="2"><s:select name="partner.billToGroup"  list="%{billgrp}" cssStyle="width:115px"  headerKey="" headerValue=""/></td>
						</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingInstructionCode'/></td>
					<td colspan="7" align="left"><s:select cssClass="list-menu" id="billingInstructionCode" name="partner.billingInstruction"  list="%{billinst}" cssStyle="width:450px"  headerKey="" headerValue=""/></td>
					</tr>
					</table>
				
					<table>
					<tr>
						<td colspan="3"><font size="2"><b>Cross-reference....</b></font></td>
						<td><s:textfield cssClass="input-text" name="partner.client4"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.validNationalCode'/></td><td><s:textfield cssClass="input-text" name="partner.validNationalCode"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client5'/></td><td><s:textfield cssClass="input-text" name="partner.client5"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.idCode'/></td><td><s:textfield cssClass="input-text" name="partner.idCode"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client6'/></td><td><s:textfield cssClass="input-text" name="partner.client6"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						
						<td align="right" class="listwhitetext"><fmt:message key='partner.client7'/></td><td><s:textfield cssClass="input-text" name="partner.client7"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client1'/></td><td><s:textfield cssClass="input-text" name="partner.client1"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client8'/></td><td><s:textfield cssClass="input-text" name="partner.client8"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client2'/></td><td><s:textfield cssClass="input-text" name="partner.client2"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client9'/></td><td><s:textfield cssClass="input-text" name="partner.client9"  size="25" maxlength="15"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client3'/></td><td><s:textfield cssClass="input-text" name="partner.client3"  size="25" maxlength="15"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='partner.client10'/></td><td><s:textfield cssClass="input-text" name="partner.client10"  size="25" maxlength="15"/></td>
					</tr>
					</table>
				
				
				</td>
			</tr>
		</tbody>
	</table>
</div>
</c:otherwise>
</c:choose>
</td>
</tr>


<tr>

<td height="5" align="left" class="listwhitetext" colspan="2">
<c:if test="${partnerType == 'CR'}">
<div  onClick="javascript:animatedcollapse.toggle('forwarding')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;Forwarding Details
</td>
<td width="28px" valign="top"><img src="images/tab-left-bg.gif"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>				
  
<div id="forwarding" style="100%">

<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="100%">
	<tbody>
	<tr><td align="center" colspan="6"><img height="0px " width="100%" src="<c:url value='/images/vertlinedata.jpg'/>"/></td></tr>
		<tr><td class="subcontenttabChild" colspan="4"><font color="black"><b>&nbsp;Remarks/Fees....</b></font></td></tr>
					
					</tr>
					<tr><td height="5px"></td></tr>
					<tr>
					<td></td>
					<td align="left" class="listwhitetext"><b>Per Diem Costs</b></td>
					<td></td>
					<td align="left" class="listwhitetext"><b>Five Lines Of Remarks</b></td>
					</tr>
					<tr><td height="3px"></td></tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemFreeDays'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemFreeDays"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td>
					<td width="70px"></td>
					<td><s:textfield name="partner.remarks1"  cssClass="text large" maxlength="50"/></td></tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemDays2'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemDays2"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks2"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost1'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemCost1"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks3"  cssClass="text large" maxlength="50"/></td></tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost2'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemCost2"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks4"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><s:textfield name="partner.remarks5"  cssClass="text large" maxlength="50"/></td></tr>
					<tr height="9" />
					<%-- <tr><td>Five Lines Of Remarks</td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks1"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks2"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks3"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks4"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks5"  cssClass="text large" maxlength="50"/></td></tr>
					--%>
				
	</tbody>
</table>

</div>
</c:if>
</td>
</tr>
<tr>
	<td height="10" align="left" class="listwhitetext"></td>
</tr>
<c:if test="${partnerType != 'PP'}">
<tr>
<td align="center" colspan="15">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr><td align="center" colspan="10"><img height="1px" width="100%" src="<c:url value='/images/vertlinedata.jpg'/>"/></td></tr>
<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="10px"></td>
</tr>
<tr>
<td  colspan="5"  style="width:100%">
<table style="width:100%"  cellspacing="0" cellpadding="0">	
<tr>
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isPrivateParty}">
<c:set var="ischecked" value="true"/>
</c:if>						
<td align="right" class="listwhitetext2">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isAccount}">
<c:set var="ischecked" value="true"/>
</c:if>						
<td align="right" class="listwhitetext2"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isAgent}">
<c:set var="ischecked" value="true"/>
</c:if>						
<td align="right" class="listwhitetext2" ><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isVendor}">
<c:set var="ischecked" value="true"/>
</c:if>					
<td align="center" class="listwhitetext2" ><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isCarrier}">
<c:set var="ischecked" value="true"/>
</c:if>						
<td align="center" class="listwhitetext2" ><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
<c:set var="ischecked" value="false"/>
<c:if test="${partner.isOwnerOp}">
<c:set var="ischecked" value="true"/>
</c:if>						
<td align="center" class="listwhitetext2" ><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="17"/></td>
</tr>
<tr><td align="left" height="5px"></td></tr>
</tbody>
</table>
</td>
</tr>
</c:if>
</tbody>
</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<table border="0" style="width:710px;">
<tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='partner.createdOn'/></b></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='partner.createdBy' /></b></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:110px"><b><fmt:message key='partner.updatedOn'/></b></td>
							<s:text id="updatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.updatedOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="updatedOn" name="partner.updatedOn" value="%{updatedOnFormattedValue}" /></td>
						<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partner.updatedBy' /></b></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.updatedBy"/>
								<td><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>				
						
					</tr>
</tbody>
</table>  
</div>
<c:out value="${button1}" escapeXml="false" /> 
<c:set var="isTrue" value="false" scope="session"/>
<div id="mydiv" style="position:absolute"></div>
</s:form>
<script type="text/javascript">
try{
	autoPopulate_partner_billingCountry(document.forms['partnerViewForm'].elements['partner.billingCountry']);
	}
	catch(e){}
	try{
	getBillingState(document.forms['partnerViewForm'].elements['partner.billingCountry']); 
    }
    catch(e){}
 </script> 
 <script type="text/javascript">  
 	try{
 	autoPopulate_partner_terminalCountry(document.forms['partnerViewForm'].elements['partner.terminalCountry']);
	}
	catch(e){}
	try{
	getTerminalState(document.forms['partnerViewForm'].elements['partner.terminalCountry']); 
	}
	catch(e){}
</script>     
<script type="text/javascript">  
	try{
	autoPopulate_partner_mailingCountry(document.forms['partnerViewForm'].elements['partner.mailingCountry']);        
	}
	catch(e){}
	try{
	getMailingState(document.forms['partnerViewForm'].elements['partner.mailingCountry']);
	}
	catch(e){}
</script>
<script type="text/javascript">
try{
disabledAll();
}
catch(e){}
</script>