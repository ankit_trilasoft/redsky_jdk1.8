package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

@Entity
@Table(name = "documentbundle")

public class DocumentBundle extends BaseObject{
	
	private Long id;
	
	private String corpId;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;
	
	private String bundleName;
	
	private String sendPointTableName;
	
	private String sendPointFieldName;
	
	private String routing;
	
	private String mode;
	
	private String job;
	
	private String serviceType;
	
	private Integer printSeq;
	
	private String description;
	
	private String formsId;
	
	private String email;
	
	private Boolean status;
	

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("updatedBy", updatedBy)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("bundleName", bundleName)
				.append("sendPointTableName", sendPointTableName)
				.append("sendPointFieldName", sendPointFieldName)
				.append("routing", routing).append("mode", mode)
				.append("job", job).append("printSeq", printSeq)
				.append("description", description).append("formsId", formsId)
				.append("email", email).append("status", status)
				.append("serviceType", serviceType).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DocumentBundle))
			return false;
		DocumentBundle castOther = (DocumentBundle) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(bundleName, castOther.bundleName)
				.append(sendPointTableName, castOther.sendPointTableName)
				.append(sendPointFieldName, castOther.sendPointFieldName)
				.append(routing, castOther.routing)
				.append(mode, castOther.mode).append(job, castOther.job)
				.append(printSeq, castOther.printSeq)
				.append(description, castOther.description)
				.append(formsId, castOther.formsId)
				.append(email, castOther.email)
				.append(status, castOther.status)
				.append(serviceType, castOther.serviceType).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(updatedBy).append(createdBy).append(createdOn)
				.append(updatedOn).append(bundleName)
				.append(sendPointTableName).append(sendPointFieldName)
				.append(routing).append(mode).append(job).append(printSeq)
				.append(description).append(formsId).append(email)
				.append(status).append(serviceType).toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public String getSendPointTableName() {
		return sendPointTableName;
	}

	public void setSendPointTableName(String sendPointTableName) {
		this.sendPointTableName = sendPointTableName;
	}

	public String getSendPointFieldName() {
		return sendPointFieldName;
	}

	public void setSendPointFieldName(String sendPointFieldName) {
		this.sendPointFieldName = sendPointFieldName;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Integer getPrintSeq() {
		return printSeq;
	}

	public void setPrintSeq(Integer printSeq) {
		this.printSeq = printSeq;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormsId() {
		return formsId;
	}

	public void setFormsId(String formsId) {
		this.formsId = formsId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}	


}
