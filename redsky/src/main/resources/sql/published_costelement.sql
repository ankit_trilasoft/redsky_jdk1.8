DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`published_costelement`$$
CREATE DEFINER=`root`@`%` PROCEDURE  `redsky`.`published_costelement`(l_contract varchar(500), fromcorpid varchar(4), tocorpid varchar(4),l_createdby varchar(100))
BEGIN
DECLARE bDone INT;
DECLARE l_recGL VARCHAR(16);
DECLARE l_paygl VARCHAR(16);
DECLARE l_costelement VARCHAR(50);
DECLARE l_description VARCHAR(50);
DECLARE l_reportingAlias VARCHAR(50);


declare curs CURSOR FOR  select recGl,payGl,costelement,description,reportingAlias from costelement where corpId=tocorpid and costElement <>'' and costElement is not null;

declare CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

OPEN curs;

  SET bDone = 0;
  REPEAT
    FETCH curs INTO l_recGL,l_paygl, l_costelement,l_description,l_reportingAlias;

    update charges set gl=l_recGL,expgl=l_paygl where contract=l_contract and corpid=tocorpid and costElement=l_costElement;

    UNTIL bDone END REPEAT;

  CLOSE curs;


END $$

DELIMITER ;