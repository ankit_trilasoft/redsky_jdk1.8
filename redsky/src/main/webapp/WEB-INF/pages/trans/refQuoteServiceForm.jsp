<%@ include file="/common/taglibs.jsp"%> 
<head>
    <title>RefQuoteService Form</title>
    <meta name="heading" content="RefQuoteService Form"/>
<script>
</script>
<script type="text/javascript" >
function setSelection(){
	try{
		 if(document.forms['refQuoteServicesform'].elements['refQuoteServices.refQuoteServicesDefault'].checked){
			 document.forms['refQuoteServicesform'].elements['refQuoteServicesDefaultCheck'].value='yes';
		 }else{
			 document.forms['refQuoteServicesform'].elements['refQuoteServicesDefaultCheck'].value='no';
		 }
	}catch(e){
		//document.forms['refQuoteServicesform'].elements['refQuoteServices.checkIncludeExclude'].value='N';
		//document.forms['refQuoteServicesform'].elements['refQuoteServicesDefaultCheck'].value='no';
	}
	 return true;
}
</script>
<script type="text/javascript" >
function langaugeMand(){
	var str="0";
	<configByCorp:fieldVisibility componentId="component.field.refQuote.document">
		str="1";
	</configByCorp:fieldVisibility>
	var description=document.forms['refQuoteServicesform'].elements['refQuoteServices.description'].value;
	var langauge=document.forms['refQuoteServicesform'].elements['refQuoteServices.langauge'].value;
	var checkIncludeExclude='';
	try{
		checkIncludeExclude=document.forms['refQuoteServicesform'].elements['refQuoteServices.checkIncludeExclude'].value;
	}catch(e){}
	var userQuoteServices=document.forms['refQuoteServicesform'].elements['userQuoteServices'].value;
	 
	if(langauge=='' || langauge==null ){
    	alert("Please Select Langauge");
    	document.forms['refQuoteServicesform'].elements['refQuoteServices.langauge'].focus();
    	return false;
  	}else if((str=="0") && (userQuoteServices!=null && userQuoteServices!='' && userQuoteServices!='N/A' && userQuoteServices!='Single section') && (checkIncludeExclude=='' || checkIncludeExclude==null)){
	    alert("Please Select Include/Exclude");
	    document.forms['refQuoteServicesform'].elements['refQuoteServices.checkIncludeExclude'].focus();
	    return false;	  
  	}else if(description==null || description=='' || description.trim()==''){
	    alert("Description is a required field");
	    document.forms['refQuoteServicesform'].elements['refQuoteServices.description'].value="";
	    document.forms['refQuoteServicesform'].elements['refQuoteServices.description'].focus();
	    return false;
  	}else if((str=="1") && (userQuoteServices!=null && userQuoteServices!='' && (userQuoteServices=='Single section' || userQuoteServices=='Separate section')) && (checkIncludeExclude=='' || checkIncludeExclude==null)){
	    alert("Please Select Include/Exclude/Document");
	    document.forms['refQuoteServicesform'].elements['refQuoteServices.checkIncludeExclude'].focus();
	    return false;
  	}else{
	  	return setSelection();
  }
}
function onlyNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
}
function isNumeric(targetElement)
{   var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid Number");
        targetElement.value="";
        return false;
        }
    }
    return true;
}
</script>
</head>

<s:form id="refQuoteServicesform" name  ="refQuoteServicesform"   action="saveRefQuoteService" onsubmit="return langaugeMand();" method="post"  >
<s:hidden name="id" value="<%= request.getParameter("id") %>"/>
<c:set var = "id" value="<%= request.getParameter("id") %>" />
<s:hidden name="refQuoteServices.corpID"/>
<s:hidden name="refQuoteServices.id"/>
<s:hidden name="refQuoteServices.createdBy"/>
<s:hidden name="refQuoteServices.createdOn"/>
<s:hidden name="refQuoteServicesDefaultCheck" />
 <s:hidden name="userQuoteServices" />
 <c:set var="documentVal" value="N" />
<configByCorp:fieldVisibility componentId="component.field.refQuote.document">		
		<c:set var="documentVal" value="Y" />
</configByCorp:fieldVisibility>

<div id="layer4" style="width:100%;">
<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF;"><a class="current"><span>Service Details Entry</span></a></li>
		<li ><a href="refQuoteServicesList.html?true&corpID=${sessionCorpID}"><span>Service List</span></a></li>
	</ul>
</div>
</div>
<div class="spn" style="!margin-top:2px;">&nbsp;</div>
<div id="content" align="center" style="margin-bottom:10px;">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0">
	<tbody>
		<tr>
			<td class="listwhitetext" align="left" colspan="35">
				<fieldset style="width:875px;">
					<legend>Control&nbsp;Type</legend>  
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
						<tr>							
							<td class="listwhitetext" align="right" >Job&nbsp;Type</td>							
							<td> <s:select cssClass="list-menu" name="refQuoteServices.job" list="%{job}" cssStyle="width:140px" headerKey="" headerValue="" /></td>
							&nbsp;&nbsp;<td class="listwhitetext" align="right" >Pack&nbsp;Mode</td>	
							<td> <s:select cssClass="list-menu" name="refQuoteServices.packingMode" list="%{pkmode}" cssStyle="width:150px" /></td>
							&nbsp;&nbsp;<td class="listwhitetext" align="right" >Service&nbsp;Type</td>	
							<td><s:select cssClass="list-menu" name="refQuoteServices.serviceType" list="%{service}" cssStyle="width:150px"  /></td>
							
							
							<%-- <c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">								
							<td class="listwhitetext" align="right" >Default</td>
							<td>
							<c:if test="${refQuoteServices.refQuoteServicesDefault}">
							<input type="checkbox" style="margin:0px;" id="refQuoteServices.refQuoteServicesDefault" name="refQuoteServices.refQuoteServicesDefault"   value="${refQuoteServices.refQuoteServicesDefault}"   checked="checked" />
							</c:if>
							<c:if test="${refQuoteServices.refQuoteServicesDefault eq null || refQuoteServices.refQuoteServicesDefault == false}">
							<input type="checkbox" style="margin:0px;" id="refQuoteServices.refQuoteServicesDefault" name="refQuoteServices.refQuoteServicesDefault" value="${refQuoteServices.refQuoteServicesDefault}"  />
							</c:if>
							</td>
							</c:if> --%>
							</tr>
                          
                          <tr><td height="5"></td></tr>
                          
							<tr>							
							<td class="listwhitetext" align="right" >Route</td>									
							<td> <s:select cssClass="list-menu" name="refQuoteServices.Routing" list="%{routing}" cssStyle="width:140px" /></td>	
							<td class="listwhitetext" align="right" >Commodity</td>	
							<td> <s:select cssClass="list-menu" name="refQuoteServices.commodity" list="%{commodits}" cssStyle="width:150px" /></td>
							<td class="listwhitetext" align="right" >Contract</td>									
							<td> <s:select cssClass="list-menu" name="refQuoteServices.contract" list="%{contract}" cssStyle="width:140px" headerKey="" headerValue=""/></td>	
						</tr>
							 <tr><td height="5"></td></tr>
							<tr>
							<td class="listwhitetext" align="right" >Mode</td>							
							<td> <s:select cssClass="list-menu" name="refQuoteServices.mode" list="%{modeMap}" cssStyle="width:140px"  /></td>
							<td class="listwhitetext" align="right" >Origin&nbsp;Country</td>	
							<td> <s:select cssClass="list-menu" name="refQuoteServices.originCountry" list="%{ocountry}" cssStyle="width:150px" headerKey="" headerValue=""/></td>
							<td class="listwhitetext" align="right" >Langauge<font color="red" size="2">*</font></td></td>
							<td> <s:select cssClass="list-menu" name="refQuoteServices.langauge" list="%{lang}" required="true" cssStyle="width:80px" headerKey="" headerValue="" /></td>
							</tr>
							
							 <tr><td height="5"></td></tr>
							<tr>
							<td class="listwhitetext" align="right" >Company&nbsp;Division</td>							
							<td> <s:select cssClass="list-menu" name="refQuoteServices.companyDivision" list="%{companyDivis}" cssStyle="width:140px" headerKey="" headerValue=""/></td>
							<td class="listwhitetext" align="right" >&nbsp;&nbsp;Destination&nbsp;Country</td>	
							<td> <s:select cssClass="list-menu" name="refQuoteServices.destinationCountry" list="%{ocountry}" cssStyle="width:150px" headerKey="" headerValue=""/></td>
							<c:if test="${documentVal=='N'}">
							<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">							
							<td class="listwhitetext" align="right" >&nbsp;&nbsp;Include/Exclude<font color="red" size="2">*</font></td>
							<td> <s:select cssClass="list-menu" name="refQuoteServices.checkIncludeExclude" list="%{checkIncludeExcludeList}" cssStyle="width:80px" headerKey="" headerValue="" /></td>
							</c:if></c:if>
							<c:if test="${documentVal=='Y'}">
							<c:if test="${userQuoteServices!=null && userQuoteServices!='' && (userQuoteServices=='Separate section' || userQuoteServices=='Single section')}">
							<td class="listwhitetext" align="right" >&nbsp;&nbsp;Include/Exclude/Document<font color="red" size="2">*</font></td>
							<td> <s:select cssClass="list-menu" name="refQuoteServices.checkIncludeExclude" list="%{checkIncludeExcludeDocumentList}" cssStyle="width:80px" headerKey="" headerValue="" /></td>
							</c:if></c:if>
						    <td class="listwhitetext" align="right" >Display&nbsp;order</td>
							<td> <s:textfield cssClass="input-text" cssStyle="width:75px" id="refQuoteServices.displayOrder" name="refQuoteServices.displayOrder" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" tabindex=""/></td>
							
							</tr>
							<tr>
							<td colspan="6">
							<fieldset style="margin-left:87px;">
							<legend>Reference&nbsp;Data</legend>  
							<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
							<tr>
							<td class="listwhitetext" align="right" >In German</td>
							<td> <s:textfield cssClass="input-text" cssStyle="width:75px" id="refQuoteServices.germanId" name="refQuoteServices.germanId" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" tabindex=""/></td>
							<td class="listwhitetext" align="right" style="padding-left:10px;">In English</td>
							<td> <s:textfield cssClass="input-text" cssStyle="width:75px" id="refQuoteServices.englishId" name="refQuoteServices.englishId" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" tabindex=""/></td>
							<td class="listwhitetext" align="right" style="padding-left:10px;">In French</td>
							<td> <s:textfield cssClass="input-text" cssStyle="width:75px" id="refQuoteServices.frenchId" name="refQuoteServices.frenchId" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" tabindex=""/></td>
							<td class="listwhitetext" align="right" style="padding-left:10px;">In Dutch</td>
							<td> <s:textfield cssClass="input-text" cssStyle="width:75px" id="refQuoteServices.dutchId" name="refQuoteServices.dutchId" onkeydown="return onlyNumsAllowed(event)" onchange="isNumeric(this);" tabindex=""/></td>
							
							</tr>
							</table>
							</fieldset>
							</td>
							<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">								
							<td class="listwhitetext" align="right" >Default</td>
							<td>
							<c:if test="${refQuoteServices.refQuoteServicesDefault}">
							<input type="checkbox" style="margin:0px;" id="refQuoteServices.refQuoteServicesDefault" name="refQuoteServices.refQuoteServicesDefault"   value="${refQuoteServices.refQuoteServicesDefault}"   checked="checked" />
							</c:if>
							<c:if test="${refQuoteServices.refQuoteServicesDefault eq null || refQuoteServices.refQuoteServicesDefault == false}">
							<input type="checkbox" style="margin:0px;" id="refQuoteServices.refQuoteServicesDefault" name="refQuoteServices.refQuoteServicesDefault" value="${refQuoteServices.refQuoteServicesDefault}"  />
							</c:if>
							</td>
							</c:if>
							</tr>								  
				 </table>
	 		</fieldset>
			</td>
	 	</tr>
	 <tr><td colspan="35" height="10px"></td></tr>
 		<tr>
			<td class="listwhitetext" align="left" colspan="35">
				<fieldset style="width:875px;">
					<legend>Service</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
<%-- <td class="listwhitetext" align="left" >code&nbsp;&nbsp;<font color="red" size="2">*</font></td>	
<td align="left" class="listwhitetext" colspan="2"><s:textfield  name="refQuoteServices.code" required="true" size="17"  maxlength="40" tabindex="1"/></td>
<td align="left" style="width:5px"><s:textfield cssClass="input-text" name="refQuoteServices.code" required="true" size="57" maxlength="45" /></td>
</tr> --%>
			<tr> 
	  			<td class="listwhitetext" align="right" width="85">Description<font color="red" size="2">*</font></td>
    			<td colspan="4"><s:textarea name="refQuoteServices.description" cssStyle="height:60px;width:392px;" cssClass="textarea" /></td>
	  		   
	  		</tr>
		</table>
 		</fieldset>
		   	</td>
		   	</tr>		   		
		</table>	
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
 <s:submit cssClass="cssbuttonA" method="save" cssStyle="margin-left:18px;" key="button.save" theme="simple" />
   </s:form>