package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.UserGuides;

public interface UserGuidesDao extends GenericDao<UserGuides, Long>{
	
	 public List findListByModuleDocStatus(String module,String documentName,Boolean activeStatus);
	 public List findDistinct();
	 public List findListByModule(String module,String corpId);
	 public List<UserGuides> findListByModuleAndDisplayOrder(String module,int displayOrder,Integer prevDisplayOrder);
	 public List<String> findListByDistinctModule(String corpId);
	 public List<UserGuides> findListByDocumentName(String fileName,String UserType,Boolean cmmdmmAgent,Boolean vanlineEnabled,String corpId);
	 public List findModuleCount(String corpId);

}
