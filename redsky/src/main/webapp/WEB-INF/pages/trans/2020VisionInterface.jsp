<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
	<title>2020 Vision Extract Interface</title>
	<meta name="heading" content="2020 Vision Extract Interface"/> 
	<script language="javascript" type="text/javascript" >
	function t20VisionExtract(){
		 document.forms['t20VisionForm'].action = 't20VisionExtract.html';
	     document.forms['t20VisionForm'].submit();
	}
	
	function t20VisionImport(){
		 document.forms['t20VisionImportForm'].action = 't20VisionImport.html';
	     document.forms['t20VisionImportForm'].submit();
	}
	</script>
</head>
<div style="width:70%;" id="layer1">
<%-- <div id="otabs">
	<ul>
	<li><a class="current"><span>20/20 Interface</span></a></li>
	</ul>
</div> --%>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px"><span></span></div>
   <div class="center-content">
<table>
	<tr><td height="5px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >20/20 Interface</td>
</tr>
<tr><td height="20px"></td></tr>
	<tr>
		<td width="10px;"></td>
		<td>
			<form name="t20VisionForm" id="t20VisionForm" >
				<input type="button" name="2020Vision" value="Orders to 20/20" onclick ="t20VisionExtract()" class="cssbutton" />
			</form>
		</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
	
	<tr>
	<td width="10px;"></td>
		<td>
			<form name="t20VisionImportForm" id="t20VisionImportForm" >
				<input type="button" name="2020VisionImport" value="Invoice from 20/20 " onclick ="t20VisionImport()" class="cssbutton" />
			</form>
		</td>
	</tr>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
